$(function() {
	$('#sendMessage').on('click', function(e) {
		e.preventDefault();
		$('#name, #email, #number, #subject, #message').blur();
		if($('.has-error').length == 0) {
			var req = {};
			var res;
			req.action = 'contact-us-email';
			req.name = $('#name').val();
			req.email = $('#email').val();
			req.number = $('#number').val();
			req.subject = $('#subject').val();
			req.message = $('#message').val();
			$.ajax({
				'type'	:	'post',
				'url'	:	'api/index.php',
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				$('#cnf').text(res.message).show();
			else {
				$('#cnf').text('Message send successfully').show();
				$('#name, #email, #number, #subject, #message').val('');
			}
			});
		}
	});

	$('#name').on('blur', function() {
		unsetError($(this));
		if($(this).val().length < 4)
			setError($(this), 'Please enter a name greater that 3 chars');
	});
	$('#email').on('blur', function() {
		unsetError($(this));
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!regex.test($(this).val()))
			setError($(this), 'Please enter a valid email');
	});
	$('#number').on('blur', function() {
		unsetError($(this));
		var regex = /^[1-9]\d*$/;
		if(!regex.test($(this).val()))
			setError($(this), 'Please enter a your correct phone number');
	});
	$('#subject').on('blur', function() {
		unsetError($(this));
		if($(this).val().length < 4)
			setError($(this), 'Please enter a subject greater that 10 chars');
	});
	$('#message').on('blur', function() {
		unsetError($(this));
		if($(this).val().length < 4)
			setError($(this), 'Please enter a name greater that 100 chars');
	});
});
function setError(where, what) {
	unsetError(where);
	where.parents('div:eq(0)').addClass('has-error');
	where.parents('div:eq(0)').append('<span class="help-block">' + what + '</span>');
}

function unsetError(where) {
	if (where.parents('div:eq(0)').hasClass('has-error')) {
		where.parents('div:eq(0)').find('.help-block').remove();
		where.parents('div:eq(0)').removeClass('has-error');
	}
}