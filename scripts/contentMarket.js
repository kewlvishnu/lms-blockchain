var ApiEndpoint = 'api/index.php';
var currency=2;
$(document).ready(function() {
	Checklogin();
});

function getTemplateCousres(){
	var req = {};
	var res;
	req.action = 'content-template-courses';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
			alert(res.message);
		else
			fillTemplateCourses(res);
		//console.log(res);
	});
}

function fillTemplateCourses(data){
	var html='';
	var licenseHtml='';
	var displayCurrency='<i class="fa fa-dollar"></i>';
	if(data.contentMarketCourses.length > 0) {
		$.each(data.contentMarketCourses, function(i, course) {
			if(currency==2){
				course.studentPrice=course.studentPriceINR;
				displayCurrency='<i class="fa fa-rupee"></i>';
			}
			if(course.name.length > 20)
				course.name = course.name.substring(0, 20) + '...';
			licenseHtml += '<div class="col-xs-6 col-md-3 xyz course1">'
						+ '<section class="panel">'
						+    '<div class="pro-img-box">'
						+ '<a href="courseDetailsInstitute.php?courseId='+course.id+'" class="pro-title course-name"><img height=200px; witdh=260px; src=" '+course.image+'" onerror="this.src=\'student/user-data/images/course.png\'" alt=""></a> '
						+ '<a href="courseDetailsInstitute.php?courseId='+course.id+'" class="adtocart">'
						+         '<i class="fa fa-user"></i>'
						+     '</a>'
						+ '</div>'
						+	'<div class="panel-body">'
						+     '<h4 class="text-center">'
						+        '<a href="courseDetailsInstitute.php?courseId='+course.id+'" class="pro-title course-name">'
						+            course.name
						+        '</a>'
						+   '</h4>'
						+    '<p class="price"><span class="course-price">'+displayCurrency+course.studentPrice+'</span><span class="pull-right course-joined"><i class="fa fa-group"></i>300</span> </p>'
						+  '</div>'
						+  '</section>'
						+'</div>';
		});
		$('#license-course').html(licenseHtml);
	}
	else
		$('#license-course').html('No courses in Market Place.');
}
function Checklogin(){
		var req = {};
		var res;
		req.action = 'check-login';
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status==1){
				checkCurrency();
			}else{
				getTemplateCousres();
			}
		});
  }
function checkCurrency(){
		var req = {};
		var res;
		req.action = 'get-currency';
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status==1){
				currency=res.currency[0].currency;//'<i class="fa fa-rupee"></i>';
			} 
			getTemplateCousres();
		});
  
}

function getUrlParameter(sParam)
{
	sParam = sParam.toLowerCase();
    var sPageURL = window.location.search.substring(1).toLowerCase();
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}