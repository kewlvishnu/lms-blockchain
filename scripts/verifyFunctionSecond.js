var userInstituteTypes = [ ],stucturedInsTypes = { };
var management = [ ], engineering = [ ], medical = [ ], schoolLevel = [ ], misc = [ ], college = [ ], trainingInstitute = [ ]; 
var categoryHierarchy = {
	"1": [5,6,7],
	"2": [8,9,10,11,12],
	"3": [46,47,48,49,50,51,52,53,-1],
	"4": [54,55,56,57,58,59,60,61,62,63,-1],
	"8": ['13','14','15','16','17','-1'],
	"9": ['18','19','21','22','-1'],
	"10": ['23','24','25','26','27','-1'],
	"11": ['28','29','30','31','32','33','34','-1'],
	"12": ['35','36','37','38','39','40','41','42','43','44','45','-1']
};
function selectCountry(element) {
	var parent=element.parent( );
	if(element.prop('value') == 0) {
		var errorMsg = "You must select a country.";
		parent.addClass('has-error');
		element.next( ).remove( );
		parent.append("<span class='help-block'>"+errorMsg+"</span>");
	}
	else {
		parent.removeClass('has-error');
		element.next( ).remove( );
	}
}
function changeCountry(element) {
	if(element.prop('value')==1) {
		$("#nonIndian").hide();
		$("#indian").show();
		$('#state').parents('div:eq(0)').show();
	}
	else {
		$("#nonIndian").show();
		$("#indian").hide();
		$('#state').parents('div:eq(0)').hide();
	}
}
function next() {
	if(validateStep1())
	{
		$("#step1").hide();
		$("#step2").show();
		$("#school").hide();
		$("#coaching").hide();
		$("#college").hide();
		$("#trainingInstitute").hide();
		$('body').animate({ scrollTop: 0 }, 300);
	}
}
function showOtherFieldBoard(element) {
	if(element.prop('value')=="7") {
		$("#schoolBoardOther").hide();
		$("#schoolBoardState").show();
	}
	else if(element.prop('value')=="-1") {
		$("#schoolBoardState").hide();
		$("#schoolBoardOther").show();
	}
	else {
		$("#schoolBoardState").hide();
		$("#schoolBoardOther").hide();
	}
}
function showOption(element) {
	var text=element.attr('data-text');
	$("#"+text+"TypeOther").hide();
	$("#"+text+"Options").slideToggle();
}
function afterSelect(value,element) {
	setError("",element);
	var text=element.attr('data-text');
	if(value=='-1')
	$("#"+text+"TypeOther").show();
	if(text=='management')
		management.push(value[0]);
	if(text=='engineering')
		engineering.push(value[0]);
	if(text=='medical')
		medical.push(value[0]);
	if(text=='schoolLevel')
		schoolLevel.push(value[0]);
	if(text=='misc')
		misc.push(value[0]);
	if(text=='college')
		college.push(value[0]);
	if(text=='trainingInstitute')
		trainingInstitute.push(value[0]);
}
function afterDeselect(value,element) {
	setError("",element);
	var text=element.attr('data-text');
	if(value=='-1') {
		$("#"+text+"TypeOther").hide();
		$("#"+text+"Other").val("");
	}
	if(text=='management')
		management.splice(management.indexOf(value+""),1);
	if(text=='engineering')
		engineering.splice(engineering.indexOf(value+""),1);
	if(text=='medical')
		medical.splice(medical.indexOf(value+""),1);
	if(text=='schoolLevel')
		schoolLevel.splice(schoolLevel.indexOf(value+""),1);
	if(text=='misc')
		misc.splice(misc.indexOf(value+""),1);
	if(text=='college')
		college.splice(college.indexOf(value+""),1);
	if(text=='trainingInstitute')
		trainingInstitute.splice(trainingInstitute.indexOf(value+""),1);
}
function validateStep2( ) {
	$("#newTypeError").remove();
	stucturedInsTypes = { };
	var schoolB=coachingB=collegeB=trainingInstituteB=true;
	if(userInstituteTypes[0] != undefined){
		if($.inArray(1,userInstituteTypes)!=-1) {
			schoolB=false;
			if(schoolValidation()) {
				schoolB=true;
			}
		}
		if($.inArray(2,userInstituteTypes)!=-1) {
			coachingB=false;
			if(coachingValidation()) {
				coachingB=true;
			}
		}
		if($.inArray(3,userInstituteTypes)!=-1) {
			collegeB=false;
			if(collegeValidation()) {
				collegeB=true;
			}
		}
		if($.inArray(4,userInstituteTypes)!=-1) {
			trainingInstituteB=false;
			if(trainingInstituteValidation()) {
				trainingInstituteB=true;
			}
		}
		if(schoolB && coachingB && collegeB && trainingInstituteB){
			form['userInstituteTypes']=userInstituteTypes;
			form['stucturedInsTypes']=stucturedInsTypes;
			if($("#collegeCheckbox").prop("checked")) {
				form.userCollegeType = college;
				if(jQuery.inArray('-1', college) != -1)
					form.newCollegeType = $("#collegeOther").val();
			}
			if($("#trainingInstituteCheckbox").prop("checked")) {
				form.userTrainingInsType = trainingInstitute;
				if(jQuery.inArray('-1', trainingInstitute) != -1)
					form.newTrainingInsType = $("#trainingInstituteOther").val();
			}
			return true;
		}
		else
		return false;
	}
	else {
		var errorMsg="You must select any one category.";
		$(".errorDisplay").text(errorMsg);
		$(".errorDisplay").show();
	}
}
function addRemoveInstituteType(element) {
	var value=parseInt(element.attr('value'));
	if(jQuery.inArray(value,userInstituteTypes)!='-1') {
		userInstituteTypes.splice(userInstituteTypes.indexOf(value),1);
		removeAllSubCategory(element);
	}
	else {
		userInstituteTypes.push(value);
	}
}
function removeAllSubCategory( element ) {
	var subcat = categoryHierarchy[element.attr('value')];
	if(typeof subcat === "undefined")
		return;
	userInstituteTypes = userInstituteTypes.filter(function( n ) {
		if(subcat.indexOf(n) == -1)
		return subcat.indexOf(n) == -1
	});
	var text=element.attr('data-text');
	if(text=='management')
		management = [ ];
	if(text=='engineering')
		engineering = [ ];
	if(text=='medical')
		medical = [ ];
	if(text=='schoolLevel')
		schoolLevel = [ ];
	if(text=='misc')
		misc = [ ];
	if(text=='college')
		college = [ ];
	if(text=='trainingInstitute')
		trainingInstitute = [ ];
	if(text=='coaching')
		uncheckAllSubCategory(element);
	else if(text=='college' || text=='trainingInstitute') {
		var t=element.attr('data-text');
		releaseSelectedOptions(t,0);
	}
	else if(text=='school')
		resetSchool();
}
function resetSchool() {
	$("#selectBoard").prop('value',0);
	$("#selectClassesUpto").prop('value',0);
	$("#boardState").prop('value',"");
	$("#boardOther").prop('value',"");
}
function uncheckAllSubCategory(element) {
	$('#'+element.attr('data-text')).find('input[type=checkbox]').prop('checked', false);
	var options = $('#'+element.attr('data-text')).find('input[type=checkbox]');
	options.each(function(value){
		releaseSelectedOptions(options[value]['id'],1);
	});
}
function releaseSelectedOptions(t,ex) {
	if(ex==0)
		var extra="Checkbox";
	if(ex==1)
		var extra="";
	var value = $('#'+t+extra).attr('value');
	var id = t[0].toUpperCase()+t.slice(1);
	temp = [ ];
	for(i=0;i<categoryHierarchy[value].length;i++) {
		temp[i]=categoryHierarchy[value][i]+"";
	}
	$("#select"+id).multiSelect('deselect',temp);
	$("#"+t+"Other").val("");
}
function schoolValidation() {
	stucturedInsTypes['1'] = { };
	var board=$("#selectBoard").val();
	if(board=="0") {
		setError("Must select any Board",$("#selectBoard"));
		return false;
	}
	else {
		var temp = {
			'boardId' : board
		};
	stucturedInsTypes['1'] = temp;
	}
	if(board=="7") {
		if($('#boardState').val().length<2) {
			setError("Invalid State name.",$("#boardState"));
			return false;
		}
		else {
			var temp = {
				'boardId' : 7,
				'state'	:	$("#boardState").val()
			};
			stucturedInsTypes['1'] = temp;
		}
	}
	if(board=="-1") {
		if($('#boardOther').val().length==0 || typeof $('#boardOther').val() == 'undefined') {
			setError("Invalid Board name.",$("#boardOther"));
			return false;
		}
		else {
			var temp = {
				'boardId' : 0,
				'other'	:	$("#boardOther").val()
			};
			stucturedInsTypes['1'] = temp;
		}
	}
	if($("#selectClassesUpto").val()=="0") {
		setError("Must select classes up to.",$("#selectClassesUpto"));
		return false;
	}
	else {
		temp=stucturedInsTypes['1'];
		temp['classes']	= $("#selectClassesUpto").val();
		stucturedInsTypes['1'] = temp;
	}
	return true;
}
function coachingValidation() {
	stucturedInsTypes['2'] = { };
	if(!($("#management").prop('checked') || $("#engineering").prop('checked') || $("#medical").prop('checked') || $("#schoolLevel").prop('checked') || $("#misc").prop('checked'))) {
		setError("Must select a coaching group.");
		$(".errorDisplay").show();
		return false;
	}
	if($("#management").prop("checked") && management[0] == undefined) {
		setError("Please select one sub-category in management group.",$("#selectManagement"));
		return false;
	}
	if($("#engineering").prop("checked") && engineering[0] == undefined) {
		setError("Please select one sub-category in engineering group.",$("#selectEngineering"));
		return false;
	}
	if($("#medical").prop("checked") && medical[0] == undefined) {
		setError("Please select one sub-category in medical group.",$("#selectMedical"));
		return false;
	}
	if($("#schoolLevel").prop("checked") && schoolLevel[0] == undefined) {
		setError("Please select one sub-category in school level group.",$("#selectSchoolLevel"));
		return false;
	}
	if($("#misc").prop("checked") && misc[0] == undefined) {
		setError("Please select one sub-category in miscellaneous group.",$("#selectMisc"));
		return false;
	}
	if(management[0] != undefined) {
		stucturedInsTypes['8'] = { };
		if(jQuery.inArray('-1', management) != -1) {
			if(checkOtherField("management"))
				return false;
			else {
				var temp = {
				'Other' : $("#managementOther").val()
				};
				stucturedInsTypes['8']=temp;
			}
		}
		for(i=0;i<management.length;i++) {
			temp = { };
			stucturedInsTypes[management[i]]=temp;
		}
	}
	if(engineering[0] != undefined) {
		stucturedInsTypes['9'] = { };
		if(jQuery.inArray('-1', engineering) != -1) {
			if(checkOtherField("engineering"))
				return false;
			else {
				var temp = {
				'Other' : $("#engineeringOther").val()
				};
				stucturedInsTypes['9']=temp;
			}
		}
		for(i=0;i<engineering.length;i++) {
			temp = { };
			stucturedInsTypes[engineering[i]]=temp;
		}
	}
	if(medical[0] != undefined) {
		stucturedInsTypes['10'] = { };
		if(jQuery.inArray('-1', medical) != -1) {
			if(checkOtherField("medical"))
				return false;
			else {
				var temp = {
				'Other' : $("#medicalOther").val()
				};
				stucturedInsTypes['10']=temp;
			}
		}
		for(i=0;i<medical.length;i++) {
			temp = { };
			stucturedInsTypes[medical[i]]=temp;
		}
	}
	if(schoolLevel[0] != undefined) {
		stucturedInsTypes['11'] = { };
		if(jQuery.inArray('-1', schoolLevel) != -1) {
			if(checkOtherField("schoolLevel"))
				return false;
			else {
				var temp = {
				'Other' : $("#schoolLevelOther").val()
				};
				stucturedInsTypes['11']=temp;
			}
		}
		for(i=0;i<schoolLevel.length;i++) {
			temp = { };
			stucturedInsTypes[schoolLevel[i]]=temp;
		}
	}
	if(misc[0] != undefined) {
		stucturedInsTypes['12'] = { };
		if(jQuery.inArray('-1', misc) != -1) {
			if(checkOtherField("misc"))
				return false;
			else {
				var temp = {
				'Other' : $("#miscOther").val()
				};
				stucturedInsTypes['12']=temp;
			}
		}
		for(i=0;i<misc.length;i++) {
			temp = { };
			stucturedInsTypes[misc[i]]=temp;
		}
	}	
	return true;
}
function collegeValidation() {
	stucturedInsTypes['3'] = { };
	if(typeof college[0]==='undefined') {
	setError("You must select a College Type.",$("#selectCollege"));
	return false;
	}
	else if(jQuery.inArray('-1', college) != -1) {
		if($("#collegeOther").val().length<2) {
			setError("Invalid Other Type",$("#collegeOther"));
			return false;
		}
		else {
			var temp = {
				'other': $("#collegeOther").val()
			};
			stucturedInsTypes["3"]=temp;
		}
	}
	if(typeof college[0] !== 'undefined') {
		for(i=0;i<college.length;i++) {
			temp = { };
			stucturedInsTypes[college[i]]=temp;
		}
	}
	return true;
}
function trainingInstituteValidation() {
	stucturedInsTypes['4'] = { };
	if(typeof trainingInstitute[0]==='undefined') {
	setError("You must select a Institute Type.",$("#selectTrainingInstitute"));
	return false;
	}
	else if(jQuery.inArray('-1', trainingInstitute)!=-1) {
		if($("#trainingInstituteOther").val().length<2) {
			setError("Invalid Other Type",$("#trainingInstituteOther"));
			return false;
		}
		else {
			var temp = {
				'other': $("#trainingInstituteOther").val()
			};
			stucturedInsTypes["4"]=temp;
		}
	}
	if(typeof trainingInstitute[0] !== 'undefined') {
		for(i=0;i<trainingInstitute.length;i++) {
			temp = { };
			stucturedInsTypes[trainingInstitute[i]]=temp;
		}
	}
	return true;
}
function setError(str,element) {
	if(element==undefined)
		$(".errorDisplay").text(str);
	else {
		var parent=element.parent();
		$("#newTypeError").remove();
		parent.append("<span class='help-block' id='newTypeError' style='color:red;'>"+str+"</span>");
	}
}
function checkOtherField(field) {
	var len=$("#"+field+"Other").val().length;
	if(len<2) {
		setError("Invalid Other Option in "+field+" group.",$("#"+field+"Other"));
		return true;
	}
	return false;
}