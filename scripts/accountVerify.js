var geoCurrency = 2;
$(document).ready(function() {
	getGeoCurrency();
});

function getGeoCurrency() {
	var res;
	$.ajax({
		'type'	: 'get',
		'url'	: 'http://ip-api.com/json/'
	}).done(function(res) {
		if(res.countryCode == 'IN')
			geoCurrency = 2;
		else
			geoCurrency = 1;
		otherStuff();
	});
}

function otherStuff() {
	if(getUrlParameter('token') != undefined && getUrlParameter('token').length != 0) {
		$('#verify-modal').modal('show');
		var req = {};
		req.action = 'account-validation';
		req.userId = getUrlParameter("id");
		req.token = getUrlParameter("token");
		req.currency = geoCurrency;
		if(req.userId != undefined && req.token != undefined) {
			req = JSON.stringify(req);
			$.post('api/', req).done(function(resp) {
				res = jQuery.parseJSON(resp);
				if(res.status == 0)
					$('#verifyError').html(res.message);
				if(res.status == 1)
					$('#verifyError').html("Account has been verified. Please <a href='#login-modal' data-toggle='modal' data-dismiss='modal'>click here to Login</a>");
			});
		}
		else
			$('#verifyError').html("Your Verification link seems to be invalid");
	}
	
	if(getUrlParameter('notify') != undefined && getUrlParameter('notify').length != 0) {
		$('#notify-modal').modal('show');
		var req = {};
		req.action = 'student-session-check';
		req = JSON.stringify(req);
		$.post('api/', req).done(function(resp) {
			res = jQuery.parseJSON(resp);
			if(res.status == 0) {
				//set login and register form
				var html = 'Please <a href="#login-modal" data-toggle="modal" data-dismiss="modal">login</a> or <a href="signup-student.php#student">register</a> to enroll in this course.';
				$('#notify-section').html(html);
				console.log('no no no');
			}
			if(res.status == 1) {
				window.location = 'student/notification.php';
			}
		});
	}
}

function getUrlParameter(sParam)
{
	sParam = sParam.toLowerCase();
    var sPageURL = window.location.search.substring(1).toLowerCase();
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}