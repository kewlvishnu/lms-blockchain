$(document).ready(function(){
	middle();
	
	$(window).resize(function(){
		middle();
	});
	
	$('#mapPicker').on('click', function() {
		$('#mapContainer').css('height','100%').css('width','100%');
		$('#mapview').css('height','600').css('width','500');
	});
	
	$('#mapOK').on('click', function() {
		$('#mapContainer').css('height','0%').css('width','0%');
		$('#mapview').css('height','0').css('width','0');
		$('#street').val($('#street-tmp').val());
		$('#pin').val($('#pin-tmp').val());
		$('#city').val($('#city-tmp').val());
		$('#state').val($('#state-tmp').val());
	});
	
	$('#mapCancel').on('click', function() {
		$('#mapContainer').css('height','0%').css('width','0%');
		$('#mapview').css('height','0').css('width','0');
	});
	
	function middle() {
		$('#mapview').css('top',($(window).height()/2)-($('#mapview').height()/2)).css('left',($(window).width()/2)-($('#mapview').width()/2));
	}
	
	function showCallback() {
		address = $('#address').val();
		street = address.substring(0,address.indexOf(','));
		$('#street-tmp').val(street);
	}
	
	var addressPicker = $("#address").addresspicker({
	regionBias: "IN",
	language: "en",
	reverseGeocode: true,
	updateCallback: showCallback,
	mapOptions: {
		zoom: 4,
		center: new google.maps.LatLng(28, 77),
		scrollwheel: false,
		mapTypeId: google.maps.MapTypeId.HYBRID
	},
	elements: {
		map:      "#map",
		postal_code:  '#pin-tmp',
		administrative_area_level_2:	"#city-tmp",
		administrative_area_level_1:	"#state-tmp"
	}
	});
	var gMarker = addressPicker.addresspicker("marker");
	gMarker.setVisible(true);
	addressPicker.addresspicker("updatePosition");
});