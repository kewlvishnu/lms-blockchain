var MONTH = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var ApiEndpoint = 'api/index.php';
var professors = '';
var currency = 2;
var loginstatus = 0;
var userId = 0;  
var userRole = 0;
var courseId = getUrlParameter("courseId");
var paymentPageStudent = true;
var subjectCount = 0;
var api = false;
var couStatus=0;
var couponCode="";
var originalPrice="";
var dispcurrency='<i class="fa fa-dollar"></i>';
var couponprice=0;
var curtype=1;
var enrollstatus=0;
var instituteId = 0;
var instituteName = '';
var errmsg=0;


$(document).ready(function () {
	Checklogin();
	CourseEnrolled();
	//var enrollstatus=CourseEnrolled();
	//event listener for pay u button click
	$('#couponapply').on('click', function() {
		var req = {};
		var res=null;

		var couponcode=  $('#couponcode').val().toUpperCase().trim();
		if(couponcode == '')
		{	 	//unsetError($('#couponswid'));
		if(errmsg == 0)
				{	// unsetError($('#couponswid'));
			$('#couponcode').parent().addClass('has-error').append('<span class="help-block"> Invalid Coupon Code</span>');
									errmsg=1;
				}
				return false;
									
		}

		var couponcode=  $('#couponcode').val().toUpperCase();

		var today= new Date().getTime();
		req.courseId =courseId;
		req.couponCode =couponcode;
		req.date =today;
		req.action = 'redeemCoupon';
		couponCode=couponcode;
		if(req.courseId != undefined) {
		 $.ajax({
	                                    'type'  : 'post',
	                                    'url'   : ApiEndpoint,
	                                    'data'  : JSON.stringify(req)
	                                 }).done(function (res){
										data = $.parseJSON(res);
									if(data.endDate > today && data.startDate < today && (data.noOfCoupons-data.couponsUsed)>0)
										{
											couStatus=1;
											coupons= data;
											couponprice= (originalPrice*((100-data.DiscountPer)/100)).toFixed(2);
											couponpriceINPR=(originalPrice*((100-data.DiscountPerINR)/100)).toFixed(2);								$('#coursePrice').html('<span class="price-now">' +' <strike class= "pricedepreceated"> '+ dispcurrency +'' + originalPrice + '</strike>'+ ' &nbsp;&nbsp; '+dispcurrency +'  '+ couponprice+'</span>');
											
											$('#couponswid').html('<div class = "removeCoupon">Coupon Applied sucessfully <a id="removeCoupon" onClick="window.location.reload();return false;"> Remove Coupon</a></div>');
											
										}	
										else{
											if(errmsg == 0)
											{	// unsetError($('#couponswid'));
					$('#couponcode').parent().addClass('has-error').append('<span class="help-block"> Invalid Coupon Code</span>');
									errmsg=1;
									return false;
											}
										
							
				}

				

		 								});
		
		}
		else
		alert("No course Details");
		
	});
	$('#removeCoupon').on('click', function(){
			//console.log("HELLO");
		$('#couponswid').html('<ul class="list-inline"><div class="col-md-5"><button type="button" id="couponapply" class="btn btn-info btn-xs">Apply</button></div><div class="col-md-5"><input type="text" class="form-control" id="couponcode"></li></div></ul>');
	});
	
	
	$('#payUMoneyButton').on('click', function() {
		var today= new Date().getTime();
		var req = {};
		var res;
		req.action = 'purchaseCourseViaPayU';
		req.courseId = getUrlParameter('courseId');
		req.date= today;
		req.couponCode=couponCode;
		req.couStatus=couStatus;
		$.ajax({
			'type' : 'post',
			'url'  : ApiEndpoint,
			'data' : JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				if(res.paymentSkip == 1)
					window.location = 'student/';
				else if(res.method == 2){
					var html = '';
					html = '<form action="'+res.url+'" method="post" id="payUForm">'
						+ '<input type="hidden" name="txnid" value="'+res.txnid+'">'
						+ '<input type="hidden" name="key" value="'+res.key+'">'
						+ '<input type="hidden" name="amount" value="'+res.amount+'">'
						+ "<input type='hidden' name='productinfo' value='"+res.productinfo+"'>"
						+ '<input type="hidden" name="firstname" value="'+res.firstname+'">'
						+ '<input type="hidden" name="email" value="'+res.email+'">'
						+ '<input type="hidden" name="surl" value="'+res.surl+'">'
						+ '<input type="hidden" name="furl" value="'+res.furl+'">'
						+ '<input type="hidden" name="curl" value="'+res.curl+'">'
						+ '<input type="hidden" name="furl" value="'+res.furl+'">'
						+ '<input type="hidden" name="hash" value="'+res.hash+'">'
						+ '<input type="hidden" name="service_provider" value="'+res.service_provider+'">';
						+ '</form>'
					$('body').append(html);
					$('#payUForm').submit();
				}
			else if(res.method == 1) {
				var html = '';
				html += '<form action="' + res.url + '" method="post" id="paypalForm">'
					+ '<!-- Identify your business so that you can collect the payments. -->'
					+ '<input type="hidden" name="business" value="' + res.business + '">'
					+ '<!-- Specify a Buy Now button. -->'
					+ '<input type="hidden" name="cmd" value="_xclick">'
					+ '<!-- Specify details about the item that buyers purchase. -->'
					+ '<input type="hidden" name="item_name" value="' + res.item_name + '">'
					+ '<input type="hidden" name="amount" value="' + res.amount + '">'
					+ '<input type="hidden" name="currency_code" value="USD">'
					+ '<input type="hidden" name="quantity" value="1">'
					+ '<input type="hidden" name="invoice" value="' + res.orderId + '">'
					+ '<input type="hidden" name="item_number" value="' + res.orderId + '">'
					+ '<input type="hidden" name="return" value="' + res.surl + '" />'
					+ '<input type="hidden" name="cancel_return" value="' + res.curl + '" />'
					+ '<input type="hidden" name="notify_url" value="' + res.nurl + '" />'
					+ '<input type="hidden" name="lc" value="US" />'
					+ '<!-- Display the payment button. -->'
					+ '<input type="image" name="submit" border="0" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online">'
					+ '<img alt="" border="0" width="1" height="1"	src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >'
				+ '</form>';
				$('body').append(html);
				$('#paypalForm').submit();
			}
			}else{
				alert(res.message);
				$('#purchaseNowModal').modal('hide');
			}
		});
	});
	$('.course-showcase').on('click', '#showVideo', function(){
		var videoPath = $(this).attr("data-video");
		var imgHeight = $('.course-img-preview').height();
		$('#showVideo').hide();
		$('#showcaseBlock').removeClass('hide');
		var playerWidth = $(".course-showcase").width();
		player = new MediaElementPlayer('#videoShowcasePlayer', {type: 'video/mp4',videoWidth: playerWidth,videoHeight: imgHeight});
		var sources = [
			{ src: videoPath, type: 'video/mp4' }
		];

		player.setSrc(sources);
		player.load();
		player.play();
	});
});
function loadCourseDetails(){
	var req = {};
	var startDate;
	var catIds = [], catNames = [];
	req.action = 'loadCourseDetails';
	req.courseId =courseId
	if(req.courseId != undefined) {
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			if (data.status == 1) {			
				var newpriceINR="";
				var newprice="";
				var today= new Date().getTime();
				var discountenddate = data.courseDetails.discount.endDate;
				if(data.courseDetails.discount.endDate > today )
				{
					newpriceINR=data.courseDetails.studentPriceINR*((100 - data.courseDetails.discount.discountINR) / 100);
					newprice=data.courseDetails.studentPrice*((100 - data.courseDetails.discount.discount) / 100);
					newpriceINR=newpriceINR.toFixed(2);
					newprice=newprice.toFixed(2);
					
				}
				var price="";
				var displayCurrency='<i class="fa fa-dollar"></i>';
				originalPrice=data.courseDetails.studentPrice;
		        //alert(data.courseDetails.demoVideo);
				//filling demo video
		        if(data.courseDetails.demoVideo != '') {
					$('.course-showcase').html('<a href="javascript:void(0)" id="showVideo" data-video="'+data.courseDetails.demoVideo+'">'+
													'<span class="course-img-preview">'+
														'<img src="'+data.courseDetails.image+'" class="btn-block" />'+
														'<i class="fa fa-play-circle-o"></i></a>'+
													'</span>'+
												'</a>'+
												'<div id="showcaseBlock" class="hide"><video id="videoShowcasePlayer"></video></div>');
		            /*var playerWidth = $(".course-showcase").width();
					player = new MediaElementPlayer('#videoShowcasePlayer', {type: 'video/mp4',videoWidth: playerWidth,videoHeight: '100%'});
					var sources = [
						{ src: data.courseDetails.demoVideo, type: 'video/mp4' }
					];

					player.setSrc(sources);*/
					//player.load();
					//player.play();
		            /*$('.flow source').attr('src', data.courseDetails.demoVideo);
		            //$('.flow source').attr('src', 'http://stream.flowplayer.org/bauhaus.mp4');
		            api = flowplayer(".flow .player",{
					    clip: {
					        sources: [
					              { type: "video/mp4",
					                src:  data.courseDetails.demoVideo }
					        ]
					    }
					});
		            $('#block .icon').show();
		            $('#block').css('margin-bottom', '-80px');
		            $('#course_img, #block .icon').on('click', function() {
		            	$('#course_img').hide();
		            	$('.flow').show();
		            	api.play();
		            	$('.flow .player').removeClass('is-paused').addClass('is-playing');
		            }).css('cursor', 'pointer');*/
		        } else {
					$('.course-showcase').html('<img src="'+data.courseDetails.image+'" class="btn-block" />');
		        }
				if(currency==2){
					 displayCurrency='<i class="fa fa-rupee"></i>';
					 data.courseDetails.studentPrice=data.courseDetails.studentPriceINR;
					 newprice=newpriceINR;
					 curtype=2;
					 
				}
				if(data.courseDetails.studentPrice == '0' || data.courseDetails.studentPrice == '0.00') {
					displayCurrency = '';
					data.courseDetails.studentPrice = 'Free';
				}
				//newprice> 0 $$ newprice< data.courseDetails.studentPrice
				originalPrice=data.courseDetails.studentPrice;
				dispcurrency=displayCurrency;
				
				
				if(newprice> 0)
				{
					$('#coursePrice').html(/*'<span class="price-discount">' + displayCurrency + ' ' + data.courseDetails.studentPrice + '</span>'+*/
										'<span class="price-now">' +' <strike class= "pricedepreceated"> '+ displayCurrency +'' + data.courseDetails.studentPrice + '</strike>'+ ' &nbsp;&nbsp; '+displayCurrency +'  '+ newprice+'</span>');
					
				}else
				{	
					$('#coursePrice').html(/*'<span class="price-discount">' + displayCurrency + ' ' + data.courseDetails.studentPrice + '</span>'+*/
										'<span class="price-now">' + displayCurrency + ' ' + data.courseDetails.studentPrice +'</span>');
				}
				$('#courseName').html(data.courseDetails.name);
				$('#subtitle').html(data.courseDetails.subtitle);
				$('#description').html('<p>'+data.courseDetails.description+'<p>');
				if($('#description p').height()>86) {
					$('.course-description .js-course-details').removeClass("hide");
				}
				$('.js-share-facebook').attr('href','http://www.facebook.com/share.php?u='+encodeURI(window.location.href)+'&title='+data.courseDetails.name);
				$('.js-share-twitter').attr('href','http://twitter.com/intent/tweet?status='+data.courseDetails.name+' '+encodeURI(window.location.href));
				$('.js-share-google').attr('href','https://plus.google.com/share?url='+encodeURI(window.location.href));
				$('.js-share-facebook, .js-share-twitter, .js-share-google').click(function(e){
					e.preventDefault();
					var left = (window.screen.width / 2) - ((400 / 2) + 10);
				    var top = (window.screen.height / 2) - ((500 / 2) + 50);

					window.open($(this).attr('href'), "MsgWindow", "top="+top+", left="+left+", width=500, height=400");
				});
				//$('#course_img').attr("src", data.courseDetails.image);
				if (data.courseDetails.targetAudience != null) {
					var targetAudience = data.courseDetails.targetAudience.split(",");
					for (var i = targetAudience.length - 1; i >= 0; i--) {
						$('#targetAudience').append('<li>'+targetAudience[i]+'</li>');
					};
				} else {
					$('#targetAudience').append('<li>Anybody</li>');
				}
				/*if(data.institute.description.length > 212) {
					var first = data.institute.description.substring(0, 212);
					var second = '<span id="moreBack" style="display: none;">' + data.institute.description.substring(212, data.institute.description.length) + '</span>';
					$('#institute-background').html(first + second + '<a data-hidden="0" href="#" id="viewMoreBack">... View More</a>');
					$('#viewMoreBack').on('click', function(e) {
						e.preventDefault();
						if($(this).attr('data-hidden') == 0) {
							$(this).text(' Hide');
							$(this).attr('data-hidden', "1")
							$('#moreBack').slideDown();
						}
						else {
							$(this).text('... View More');
							$(this).attr('data-hidden', "0")
							$('#moreBack').slideUp();
						}
					});
				}
				else
					$('#institute-background').html(data.institute.description);*/
				if(data.courseCategories.length == 0) {
					$('#courseCategory').html('No Categories Selected');
				} else {
					$.each(data.courseCategories, function (k, cat) {
						//catNames.push(cat.catName);
						catIds.push(cat.categoryId);
						catNames.push(cat.category);
						$('#courseCategory').append('<a href="#" class="btn btn-category">'+cat.category+'</a>');
					});
				}
				/*if(catNames.length == 0) {
					$('#courseCategory').html('No Categories Selected');
				} else {
					$.each(data.courseCategories, function (key, val) {
						//catNames.push(cat.catName);
						$('#courseCategory').append('<a href="#" class="btn btn-category">'+val.category+'</a>');
					});
				}*/
				instituteName = data.institute.name;
				link = 'student/instituteProfile.php?instituteId=';
				switch(parseInt(data.courseDetails.roleId)) {
					case 1:
						instituteName = data.institute.name;
						link = 'student/instituteProfile.php?instituteId=';
						break;
					case 2:
					case 3:
						instituteName = data.institute.firstName + ' ' + data.institute.lastName;
						link = 'student/professorProfile.php?professorId=';
						break;
				}
				$('.js-institute-name').attr('href', link + data.institute.userId);
				$('.js-institute-name').html(instituteName);
				$('#instituteImg').attr("src",  data.institute.profilePic);
				$('#instituteDescription').html(data.institute.description);
				if($('#instituteDescription').height()>102) {
					$('.about-instructor .js-instructor-details').removeClass("hide");
				}
				if(!data.institute.tagline) {
					$('#tagline').remove();
				} else {
					$('#tagline').html(data.institute.tagline);
				}
				$('#courseID').html('C00' + data.courseDetails.id);
				$('.js-enrolled').html(data.studentCount);
				//now working on dates
				var dateHTML = '';
				var currentDate = new Date(parseInt(data.currentDate));
				var liveDate = new Date(parseInt(data.courseDetails.liveDate));
				var calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
				var endDate = (data.courseDetails.endDate == '')?'':new Date(parseInt(data.courseDetails.endDate));
				//console.log(currentDate, liveDate, endDate);
				if(endDate == '') {
					if(liveDate.valueOf() < currentDate.valueOf()) {
						/*dateHTML += '<tr>'
							+ '<td>'
								+ '<i class="fa fa-book text-info big-font"></i>'
								+ '<strong> Course Id</strong><br>&emsp;&emsp;C00' + data.courseDetails.id
							+ '</td>'
							+ '<td colspan="2">'
								+ '<i class="fa fa-calendar text-success big-font"></i>'
								+ '<span> Course Available till 1 year from date of purchase</span>'
							+ '</td>'
							+ '<td style="border-left: 1px solid rgb(153, 150, 150);padding: 2% 0% 0% 2%;">'
								+ '<h2 class="no-margin price">' + displayCurrency + ' ' + data.courseDetails.studentPrice + '</h2>'
							+ '</td>'
							+ '<td style="text-align: center;">'
								+ '<a data-toggle="modal" href="#login-modal" id="takeCourse" class="btn btn-success btn-block btn-custom">Enroll Now</a>'
								+ '<a data-toggle="modal" href="#refundModal">Refund Policy</a><br>'
							+ '</td>'
							+ '<td style="width: 12%;border-left: 5px solid #f1f1f1;text-align: center;">'
								+ '<i class="fa fa-users big-font"></i><br>'
								+ '<strong>' + data.studentCount + '</strong> users currently'
							+ '</td>'
						+ '</tr>';*/
						$('#liveEndDates').remove();
						$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Course Available till 1 year from date of purchase');
					}
					else {
						/*dateHTML += '<tr>'
							+ '<td>'
								+ '<i class="fa fa-book text-info big-font"></i>'
								+ '<strong> Course Id</strong><br>&emsp;&emsp;C00' + data.courseDetails.id
							+ '</td>'
							+ '<td colspan="2">'
								+ '<i class="fa fa-calendar text-success big-font"></i>'
								+ '<strong> Live Date</strong><br>&emsp;&emsp;' + calcLiveDate
							+ '</td>'
							+ '<td>'
								+ '<i class="fa fa-calendar text-danger big-font"></i>'
								+ '<span>Course available till 1 year from Start date</span>'
							+ '</td>'
							+ '<td style="border-left: 1px solid rgb(153, 150, 150);padding: 2% 0% 0% 2%;">'
								+ '<h2 class="no-margin price">' + displayCurrency + ' ' + data.courseDetails.studentPrice + '</h2>'
							+ '</td>'
							+ '<td style="text-align: center;">'
								+ '<a data-toggle="modal" href="#login-modal" id="takeCourse" class="btn btn-success btn-block btn-custom">Enroll Now</a>'
								+ '<a data-toggle="modal" href="#refundModal">Refund Policy</a><br>'
							+ '</td>'
							+ '<td style="width: 12%;border-left: 5px solid #f1f1f1;text-align: center;">'
								+ '<i class="fa fa-users big-font"></i><br>'
								+ '<strong>' + data.studentCount + '</strong> users currently'
							+ '</td>'
						+ '</tr>';*/
						$('#liveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>');
						$('#liveEndDates').remove();
						$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Course available till 1 year from Launch date');
					}
				}else {
					var futureDate = new Date(currentDate.getFullYear() + 1, currentDate.getMonth(), currentDate.getDate(), 0, 0, 0, 0);
					var calcEndDate = endDate.getDate()+ ' ' +MONTH[endDate.getMonth()]+ ' ' + endDate.getFullYear();
					if(endDate.valueOf() > futureDate.valueOf() && liveDate.valueOf() < currentDate.valueOf()) {
						/*dateHTML += '<tr>'
							+ '<td>'
								+ '<i class="fa fa-book text-info big-font"></i>'
								+ '<strong> Course Id</strong><br>&emsp;&emsp;C00' + data.courseDetails.id
							+ '</td>'
							+ '<td colspan="2">'
								+ '<i class="fa fa-calendar text-success big-font"></i>'
								+ '<strong> Live Date</strong><br>&emsp;&emsp;' + calcLiveDate
							+ '</td>'
							+ '<td>'
								+ '<i class="fa fa-calendar text-danger big-font"></i>'
								+ '<strong> End Date</strong><br>&emsp;&emsp;Course available till 1 year from Start date'
							+ '</td>'
							+ '<td style="border-left: 1px solid rgb(153, 150, 150);padding: 2% 0% 0% 2%;">'
								+ '<h2 class="no-margin price">' + displayCurrency + ' ' + data.courseDetails.studentPrice + '</h2>'
							+ '</td>'
							+ '<td style="text-align: center;">'
								+ '<a data-toggle="modal" href="#login-modal" id="takeCourse" class="btn btn-success btn-block btn-custom">Purchase</a>'
								+ '<a data-toggle="modal" href="#refundModal">Refund Policy</a><br>'
							+ '</td>'
							+ '<td style="width: 12%;border-left: 5px solid #f1f1f1;text-align: center;">'
								+ '<i class="fa fa-users big-font"></i><br>'
								+ '<strong>' + data.studentCount + '</strong> users currently'
							+ '</td>'
						+ '</tr>';*/
						$('#liveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>');
						$('#liveEndDates').remove();
						$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Course available till 1 year from Launch date');
					} else if(endDate.valueOf() > futureDate.valueOf() && liveDate.valueOf() > currentDate.valueOf()) {
						/*dateHTML += '<tr>'
							+ '<td>'
								+ '<i class="fa fa-book text-info big-font"></i>'
								+ '<strong> Course Id</strong><br>&emsp;&emsp;C00' + data.courseDetails.id
							+ '</td>'
							+ '<td colspan="2">'
								+ '<i class="fa fa-calendar text-success big-font"></i>'
								+ '<strong> Live Date</strong><br>&emsp;&emsp;' + calcLiveDate
							+ '</td>'
							+ '<td>'
								+ '<i class="fa fa-calendar text-danger big-font"></i>'
								+ '<strong> End Date</strong><br>&emsp;&emsp;Course available till 1 year from Start date'
							+ '</td>'
							+ '<td style="border-left: 1px solid rgb(153, 150, 150);padding: 2% 0% 0% 2%;">'
								+ '<h2 class="no-margin price">' + displayCurrency + ' ' + data.courseDetails.studentPrice + '</h2>'
							+ '</td>'
							+ '<td style="text-align: center;">'
								+ '<a data-toggle="modal" href="#login-modal" id="takeCourse" class="btn btn-success btn-block btn-custom">Purchase</a>'
								+ '<a data-toggle="modal" href="#refundModal">Refund Policy</a><br>'
							+ '</td>'
							+ '<td style="width: 12%;border-left: 5px solid #f1f1f1;text-align: center;">'
								+ '<i class="fa fa-users big-font"></i><br>'
								+ '<strong>' + data.studentCount + '</strong> users currently'
							+ '</td>'
						+ '</tr>';*/
						$('#liveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>');
						$('#liveEndDates').remove();
						$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Course available till 1 year from Launch date');
					}else {
						/*dateHTML += '<tr>'
							+ '<td>'
								+ '<i class="fa fa-book text-info big-font"></i>'
								+ '<strong> Course Id</strong><br>&emsp;&emsp;C00' + data.courseDetails.id
							+ '</td>'
							+ '<td colspan="2">'
								+ '<i class="fa fa-calendar text-success big-font"></i>'
								+ '<strong> Live Date</strong><br>&emsp;&emsp;' + calcLiveDate
							+ '</td>'
							+ '<td>'
								+ '<i class="fa fa-calendar text-danger big-font"></i>'
								+ '<strong> End Date</strong><br>&emsp;&emsp;' + calcEndDate
							+ '</td>'
							+ '<td style="border-left: 1px solid rgb(153, 150, 150);padding: 2% 0% 0% 2%;">'
								+ '<h2 class="no-margin price">' + displayCurrency + ' ' + data.courseDetails.studentPrice + '</h2>'
							+ '</td>'
							+ '<td style="text-align: center;">'
								+ '<a data-toggle="modal" href="#login-modal" id="takeCourse" class="btn btn-success btn-block btn-custom">Purchase</a>'
								+ '<a data-toggle="modal" href="#refundModal">Refund Policy</a><br>'
							+ '</td>'
							+ '<td style="width: 12%;border-left: 5px solid #f1f1f1;text-align: center;">'
								+ '<i class="fa fa-users big-font"></i><br>'
								+ '<strong>' + data.studentCount + '</strong> users currently'
							+ '</td>'
						+ '</tr>';*/
						$('#liveEndDates').after('<li class="list-item-short">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>'+
												'<li class="list-item-short">'+
													'<label for=""><i class="fa fa-calendar"></i> End Date :</label>'+
													'<span class="value" id="endDate"> '+calcEndDate+'</span>'+
												'</li>');
						$('#liveEndDates').remove();
						$('#msgValidity').remove();
					}
				}
				//$('#courseDetail').append(dateHTML);
				$('.page-load-hidden').removeClass("invisible");

				if(!!data.institute.userId) {
					instituteId = data.institute.userId;
					otherCoursesByInstitute(0);
				}

				//adding ratings
				if(data.courseDetails.rating.total == 0) {
					rating = 0;
				}
				else {
					rating = parseFloat(data.courseDetails.rating.rating).toFixed(1);
				}
				$('.ratingStars').rating('rate', rating);
				$('.ratingTotal').text(data.courseDetails.rating.total + ' Ratings');
				$('.avg-rate').text(rating);

				if(loginstatus==1) {
					//$('#takeCourse').removeAttr('data-toggle');
					$('#takeCourse').attr('href','');
					$('#takeCourse').on('click', function() {
							if(userRole != 4) {
							alert('You are not authorised to purchase this course. Please login with student account');
							retun;
						}
						$('#purchaseNowModal .course-name').text($('#courseName').text());
						$('#purchaseNowModal .amount').html($('.price-now').html());
						$('#purchaseNowModal').modal('show');
					});
				}
			}
			else {
				alert(data.message);
			}
		});
	}
	else
		alert("No course Details");
	getsubjectDetails();
 }

function getUrlParameter(sParam)
{
	sParam = sParam.toLowerCase();
	var sPageURL = window.location.search.substring(1).toLowerCase();
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++)
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam)
		{
			return sParameterName[1];
		}
	}
}

//CourseEnrolled
function CourseEnrolled() {
	var req = {};
	var res;
	req.courseId = courseId;
	req.userId = this.userId;
	req.userRole = this.userRole; 
	req.action = 'check-courseEnrolled';
	//console.log(req);
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status== "1")
		{
			//console.log(res.sitebase);
			window.location.replace("student/MyExam.php?courseId="+courseId);

		}
		this.enrollstatus=res;
		//console.log(enrollstatus);
		/*if(res.status==1){
			loginstatus = 1;
			enrollstatus = 1;
			//checkCurrency();
		}else{
			
			loginstatus = 0;
		}*/
	});
}

function Checklogin() {
	var req = {};
	var res;
	req.action = 'check-login';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status==1){
			loginstatus = 1;
			userId = res.userId;
			userRole = res.userRole;
			checkCurrency();
		}else{
			//loadCourseDetails();
			getCountryViaIp();
		}
	});
}
function checkCurrency(){
	var req = {};
	var res;
	req.action = 'get-currency';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status==1){
			currency=res.currency[0].currency;//'<i class="fa fa-rupee"></i>';
		} 
		loadCourseDetails();
	});
}

function getCountryViaIp() {
	var res;
	$.ajax({
		'type'	: 'get',
		'url'	: 'http://ip-api.com/json/'
	}).done(function(res) {
		if(res.countryCode == 'IN')
			currency = 2;
		else
			currency = 1;
		
	})
	loadCourseDetails();
}
$('#listOtherCourses').on("click", ".more-item a", function(){
	var lastId = $(this).parent().prev().find('.js-other-course').attr("data-id");
	otherCoursesByInstitute(lastId);
});
function otherCoursesByInstitute(start) {
	var req = {};
	req.action = 'loadOtherCoursesInstitute';
	req.courseId = getUrlParameter("courseId");
	req.instituteId = instituteId;
	req.start = start;
	if (req.instituteId != undefined || req.courseId != undefined) {
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			if (data.status == 1) {
				var price="";
				var displayCurrency='<i class="fa fa-dollar"></i>';
				if(data.courses.length > 0) {
					if(data.courses.length > 1) {
						$('.other-courses-title').html('Courses by <a href="'+link + instituteId+'" class="js-institute-name">'+instituteName+'</a>');
					} else {
						$('.other-courses-title').html('Course by <a href="'+link + instituteId+'" class="js-institute-name">'+instituteName+'</a>');
					}
					if(start == 0) {
						$('#listOtherCourses').append('<ul class="list-unstyled list-courses">');
					}
					//for (var i = data.courses.length - 1; i >= 0; i--) {
					for (var i = 0; i < data.courses.length; i++) {
						if(currency==2){
							 displayCurrency='<i class="fa fa-rupee"></i>';
							 data.courses[i].studentPrice=data.courses[i].studentPriceINR;
						}
						if(data.courses[i].studentPrice == '0' || data.courses[i].studentPrice == '0.00') {
							displayCurrency = '';
							data.courses[i].studentPrice = 'Free';
						}
						$('#listOtherCourses .more-item').remove();
						$('#listOtherCourses .list-courses').append(
							'<li class="other-item">'+
								'<div class="row">'+
									'<div class="col-md-6">'+
										'<a href="courseDetails.php?courseId='+data.courses[i].id+'" class="js-other-course" data-id="'+data.courses[i].id+'">'+
											'<img class="btn-block" src="'+data.courses[i].image+'" alt="">'+
										'</a>'+
									'</div>'+
									'<div class="col-md-6 pl-clr">'+
										'<div>'+
											'<a href="courseDetails.php?courseId='+data.courses[i].id+'" class="other-title">'+data.courses[i].name+'</a>'+
										'</div>'+
										'<div>'+
											'<span class="cost">' + displayCurrency + ' ' + data.courses[i].studentPrice + '</span>'+
										'</div>'+
										'<div>'+
											'<input type="hidden" class="courseRating" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="' + data.courses[i].rating.rating + '" DISABLED/>'+
										'</div>'+
									'</div>'+
								'</div>'+
							'</li>');
						
					};
					$('.courseRating').rating();
				}
				if($('#listOtherCourses .list-courses .other-item').length!=data.noOfCourses) {
					$('#listOtherCourses .list-courses').append('<li class="more-item text-center"><a href="javascript:void(0)" class="btn btn-default btn-block">View More</a></li>');
				}
			}
		});
	}
	else
		alert("No Subject Found Details");
}

function getsubjectDetails() {
	var req = {};
	req.action = 'loadSubjectDetails';
	req.courseId = getUrlParameter("courseId");
	if (req.courseId != undefined) {
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			if (data.status == 1) {
				var html = "";
				var subject_html = '';
				var professor = '';
				var subjectHeading='';
				var noOfLectures = 0;
				var noOfSubjects = data.subjects.length;
				var totalDuration = 0;
				var noOfExams = 0;
				var noOfAssignments = 0;
				var currentDate = 0;
				var liveDate = 0;
				var calcLiveDate = 0;
				var endDate = 0;
				var liveHTML = '';
				for (var i = 0; i < data.subjects.length; i++) {
					/*subject_html += '<div class="row"><div class="col-md-2 about">'
						+ '<img class="img-responsive" style="width: 100%;" alt="No Image" src="' + data.subjects[i].image + '">'
						+ '<div style="text-align: center;"><strong>' + data.subjects[i].name + '</strong></div>'
						+ '<br>'
					+ '</div>'
					+ '<div class="col-md-10 about">'
						+ '<table class="table" style="margin-bottom: 5px;width: 98%;" data-subjectId=' + data.subjects[i].id + '>'
							+ '<tbody>';*/

					if(data.subjects[i].chapters.length>0 || data.subjects[i].independentExam.length || data.subjects[i].independentAssignment.length) {
						subject_html += '<table class="table table-responsive">'+
											'<tr class="list-row-subject">'+
												'<th class="course-subject">'+
													'<h2 class="subject-title">'+
														'<div class="subject-avatar">'+
															'<img src="' + data.subjects[i].image + '" alt="" class="img-responsive">'+
														'</div>Subject: ' + data.subjects[i].name + ''+
													'</h2>'+
												'</th>'+
											'</tr>';
						if(data.subjects[i].independentExam == null)
							data.subjects[i].independentExam = 0;
						if(data.subjects[i].independentAssignment == null)
							data.subjects[i].independentAssignment = 0;

						if(data.subjects[i].chapters.length>0) {
						
							for (var j = 0; j < data.subjects[i].chapters.length; j++) {
								var examCount;
								var assCount;
								if(data.subjects[i].chapters[j].Exam > 0) {
									examCount = data.subjects[i].chapters[j].Exam + ' Exams';
								}
								else
									examCount = '--';
								if(data.subjects[i].chapters[j].Assignment > 0) {
									assCount = data.subjects[i].chapters[j].Assignment + ' Assignments';
								}
								else
									assCount = '--';
								if(data.subjects[i].chapters[j].Exam == null)
									data.subjects[i].chapters[j].Exam = 0; 
								if(data.subjects[i].chapters[j].Assignment == null)
									data.subjects[i].chapters[j].Assignment = 0;
								/*subject_html += '<tr class="chapter" data-chapterId="' + data.subjects[i].chapters[j].id + '">'
											+ '<td style="width: 10%;"><strong>Section ' + (j+1) + ':</strong></td><td style="width: 37%;"><strong>' + data.subjects[i].chapters[j].name + '</strong></td>';
								subject_html += '<td style="width: 13%;"></td>';
								subject_html += '<td style="width: 20%;text-align: center;"><span class="soup">' + examCount + '</span></td>'
											+ '<td style="width: 20%;text-align: center;"><span class="soup">' + assCount + '</span></td>'
										+ '</tr>';*/
								if(data.subjects[i].chapters[j].content.length>0 || data.subjects[i].chapters[j].Exam.length || data.subjects[i].chapters[j].Assignment.length) {
									subject_html += '<tr class="list-row-section">'+
														'<th class="ssection">'+
															'<h3 class="ssection-title">'+
																'<span class="ssection-title-txt">Section ' + (j+1) + ': ' + data.subjects[i].chapters[j].name + '</span>'+
																'<span class="ssection-meta"><p>' + examCount + ' Exams</p><p>' + assCount + ' Assignments</p><p>'+data.subjects[i].chapters.length+' lectures</p></span>'+
															'</h3>'+
														'</th>'+
													'</tr>';
									if(data.subjects[i].chapters[j].content.length>0) {
										subject_html += '<tr class="list-row-section">'+
																'<th class="ssection-content">'+
																	'<h3 class="ssection-title">'+
																		'<span class="ssection-title-txt">Section Content</span>'+
																		'<span class="ssection-meta"><p>' + examCount + ' Exams</p></span>'+
																	'</h3>'+
																'</th>'+
															'</tr>';

										//now adding lectures of the exam
										for(var k = 0; k < data.subjects[i].chapters[j].content.length; k++) {
											var type = '';
											/*switch(data.subjects[i].chapters[j].content[k].lectureType) {
												case 'Youtube':
													type = '<i class="fa fa-youtube-square" style="font-size: 2em;"></i>';
													break;
												case 'Vimeo':
													type = '<i class="fa fa-vimeo-square" style="font-size: 2em;"></i>';
													break;
												case 'video':
													type = '<i class="fa fa-video-camera" style="font-size: 2em;"></i>';
													break;
												case 'doc':
													type = '<i class="fa fa-book" style="font-size: 2em;"></i>';
													break;
												case 'ppt':
													type = '<i class="fa fa-file-powerpoint-o" style="font-size: 2em;"></i>';
													break;
												case 'text':
													type = '<i class="fa fa-file-text-o" style="font-size: 2em;"></i>';
													break;
												case 'dwn':
													type = '<i class="fa fa-cloud-download" style="font-size: 2em;"></i>';
													break;
											}
											subject_html += '<tr class="chapter" data-chapterId="' + data.subjects[i].chapters[j].id + '">'
													+ '<td style="width: 10%;text-align: right;">' + type +'</td>'
													+ '<td style="width: 37%;">Lecture ' + (k + 1) + ': ' + data.subjects[i].chapters[j].content[k].lectureName + '</td>';
											if(data.subjects[i].chapters[j].content[k].demo == 1)
												subject_html += '<td style="width: 13%;"><a class="btn btn-info btn-custom btn-xs viewDemo">Free Demo</a></td>';
											else
												subject_html += '<td style="width: 13%;"></td>';
											subject_html += '<td style="width: 20%;"></td>'
													+ '<td style="width: 20%;"></td>'
												+ '</tr>';*/
											var freePreview = '';
											var durationTime = '';
											switch(data.subjects[i].chapters[j].content[k].lectureType) {
												case 'Youtube':
													type = '<i class="fa fa-play-circle-o"></i>';
													break;
												case 'Vimeo':
													type = '<i class="fa fa-play-circle-o"></i>';
													break;
												case 'video':
													type = '<i class="fa fa-play-circle-o"></i>';
													if(data.subjects[i].chapters[j].content[k].demo == 1) {
														if(data.subjects[i].chapters[j].content[k].duration>0) {
															durationTime = '<span class="lecture-duration pull-right"></span>';
															freePreview = '<div class="lecture-play-btn">'+
																				'<button class="btn btn-primary btn-preview" data-src="' + data.subjects[i].chapters[j].content[k].metastuff + '">Free Demo (' + calcDisplayTime(data.subjects[i].chapters[j].content[k].duration) + ')</button>'+
																			'</div>';
														} else {
															freePreview = '<div class="lecture-play-btn">'+
																				'<button class="btn btn-primary btn-preview" data-src="' + data.subjects[i].chapters[j].content[k].metastuff + '">Free Demo</button>'+
																			'</div>';
														}
													}
													break;
												case 'doc':
													type = '<i class="fa fa-book"></i>';
													break;
												case 'ppt':
													type = '<i class="fa fa-file-powerpoint-o"></i>';
													break;
												case 'text':
													type = '<i class="fa fa-file-text-o"></i>';
													break;
												case 'dwn':
													type = '<i class="fa fa-cloud-download"></i>';
													break;
											}

											/*subject_html += '<tr class="chapter" data-chapterId="' + data.subjects[i].chapters[j].id + '">'
													+ '<td style="width: 10%;text-align: right;">' + type +'</td>'
													+ '<td style="width: 37%;">Lecture ' + (k + 1) + ': ' + data.subjects[i].chapters[j].content[k].lectureName + '</td>';
											if(data.subjects[i].chapters[j].content[k].demo == 1)
												subject_html += '<td style="width: 13%;"><a class="btn btn-info btn-custom btn-xs viewDemo">Free Demo</a></td>';
											else
												subject_html += '<td style="width: 13%;"></td>';
											subject_html += '<td style="width: 20%;"></td>'
													+ '<td style="width: 20%;"></td>'
												+ '</tr>';*/
											if(data.subjects[i].chapters[j].content[k].duration>0 && data.subjects[i].chapters[j].content[k].demo != 1) {
												durationTime = '<span class="lecture-duration pull-right">' + calcDisplayTime(data.subjects[i].chapters[j].content[k].duration) + '</span>';
											}
											subject_html += '<tr class="list-row-lecture">'+
																'<td class="lecture">'+
																	'<h4 class="lecture-title">'+
																		'<a href="javascript:void(0)" class="lecture-link">'+
																			'<span class="lecture-play">' + type +'</span>'+
																			'<div class="lecture-title-blk">'+
																				'<span class="lecture-number">' + (j+1) + '.' + (k+1) + '</span>'+
																				'<span class="lecture-title-txt">' + data.subjects[i].chapters[j].content[k].lectureName + '</span>'+
																				durationTime +
																			'</div>' + freePreview +
																		'</a>'+
																	'</h4>'+
																'</td>'+
															'</tr>';
											noOfLectures++;
										}
									}
									if(data.subjects[i].chapters[j].Exam.length) {
										subject_html += '<tr class="list-row-section">'+
															'<th class="ssection-content">'+
																'<h3 class="ssection-title">'+
																	'<span class="ssection-title-txt">Section Exams</span>'+
																	'<span class="ssection-meta"><p>' + examCount + ' Exams</p></span>'+
																'</h3>'+
															'</th>'+
														'</tr>';
										for(var k = 0; k < data.subjects[i].chapters[j].Exam.length; k++) {
											currentDate = new Date(parseInt(data.currentDate));
											liveDate = new Date(parseInt(data.subjects[i].chapters[j].Exam[k].startDate));
											calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
											if(liveDate.valueOf() > currentDate.valueOf()) {
												liveHTML = '<span class="lecture-duration pull-right">Live date : '+calcLiveDate+'</span>';
											} else {
												liveHTML = '';
											}
											//endDate = (data.subjects[i].chapters[j].Exam[k].endDate == '')?'':new Date(parseInt(data.subjects[i].chapters[j].Exam[k].endDate));
											subject_html += '<tr class="list-row-lecture">'+
															'<td class="lecture">'+
																'<h4 class="lecture-title">'+
																	'<a href="javascript:void(0)" class="lecture-link">'+
																		'<span class="lecture-play"><i class="fa fa-pencil-square-o"></i></span>'+
																		'<div class="lecture-title-blk">'+
																			'<span class="lecture-number">' + (j+1) + '.' + (k+1) + '</span>'+
																			'<span class="lecture-title-txt">' + data.subjects[i].chapters[j].Exam[k].name + '</span>'+
																			liveHTML +
																		'</div>'
																	'</a>'+
																'</h4>'+
															'</td>'+
														'</tr>';
											noOfExams++;
										}
									}
									if(data.subjects[i].chapters[j].Assignment.length) {
										subject_html += '<tr class="list-row-section">'+
															'<th class="ssection-content">'+
																'<h3 class="ssection-title">'+
																	'<span class="ssection-title-txt">Section Assignments</span>'+
																	'<span class="ssection-meta"><p>' + assCount + ' Assignments</p></span>'+
																'</h3>'+
															'</th>'+
														'</tr>';
										for(var k = 0; k < data.subjects[i].chapters[j].Assignment.length; k++) {
											currentDate = new Date(parseInt(data.currentDate));
											liveDate = new Date(parseInt(data.subjects[i].chapters[j].Assignment[k].startDate));
											calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
											if(liveDate.valueOf() > currentDate.valueOf()) {
												liveHTML = '<span class="lecture-duration pull-right">Live date : '+calcLiveDate+'</span>';
											} else {
												liveHTML = '';
											}
											//endDate = (data.subjects[i].chapters[j].Assignment[k].endDate == '')?'':new Date(parseInt(data.subjects[i].chapters[j].Assignment[k].endDate));
											subject_html += '<tr class="list-row-lecture">'+
															'<td class="lecture">'+
																'<h4 class="lecture-title">'+
																	'<a href="javascript:void(0)" class="lecture-link">'+
																		'<span class="lecture-play"><i class="fa fa-book"></i></span>'+
																		'<div class="lecture-title-blk">'+
																			'<span class="lecture-number">' + (j+1) + '.' + (k+1) + '</span>'+
																			'<span class="lecture-title-txt">' + data.subjects[i].chapters[j].Assignment[k].name + '</span>'+
																			liveHTML +
																		'</div>'
																	'</a>'+
																'</h4>'+
															'</td>'+
														'</tr>';
											noOfAssignments++;
										}
									}
								}
							}
						}

						if(data.subjects[i].independentExam.length) {
							subject_html += '<tr class="list-row-section">'+
												'<th class="ssection">'+
													'<h3 class="ssection-title">'+
														'<span class="ssection-title-txt">Independent Exams</span>'+
														'<span class="ssection-meta"><p>' + data.subjects[i].independentExam.length + ' Exams</p></span>'+
													'</h3>'+
												'</th>'+
											'</tr>';
							for(var k = 0; k < data.subjects[i].independentExam.length; k++) {
								currentDate = new Date(parseInt(data.currentDate));
								liveDate = new Date(parseInt(data.subjects[i].independentExam[k].startDate));
								calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
								if(liveDate.valueOf() > currentDate.valueOf()) {
									liveHTML = '<span class="lecture-duration pull-right">Live date : '+calcLiveDate+'</span>';
								} else {
									liveHTML = '';
								}
								subject_html += '<tr class="list-row-lecture">'+
												'<td class="lecture">'+
													'<h4 class="lecture-title">'+
														'<a href="javascript:void(0)" class="lecture-link">'+
															'<span class="lecture-play"><i class="fa fa-pencil-square-o"></i></span>'+
															'<div class="lecture-title-blk">'+
																'<span class="lecture-number">' + (k+1) + '</span>'+
																'<span class="lecture-title-txt">Exam : ' + data.subjects[i].independentExam[k].name + '</span>'+
																liveHTML +
															'</div>'
														'</a>'+
													'</h4>'+
												'</td>'+
											'</tr>';
								noOfExams++;
							}
						}
						if(data.subjects[i].independentAssignment.length) {
							subject_html += '<tr class="list-row-section">'+
												'<th class="ssection">'+
													'<h3 class="ssection-title">'+
														'<span class="ssection-title-txt">Independent Assignments</span>'+
														'<span class="ssection-meta"><p>' + data.subjects[i].independentAssignment.length + ' Assignments</p></span>'+
													'</h3>'+
												'</th>'+
											'</tr>';
							for(var k = 0; k < data.subjects[i].independentAssignment.length; k++) {
								currentDate = new Date(parseInt(data.currentDate));
								liveDate = new Date(parseInt(data.subjects[i].independentAssignment[k].startDate));
								calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
								if(liveDate.valueOf() > currentDate.valueOf()) {
									liveHTML = '<span class="lecture-duration pull-right">Live date : '+calcLiveDate+'</span>';
								} else {
									liveHTML = '';
								}
								subject_html += '<tr class="list-row-lecture">'+
												'<td class="lecture">'+
													'<h4 class="lecture-title">'+
														'<a href="javascript:void(0)" class="lecture-link">'+
															'<span class="lecture-play"><i class="fa fa-book"></i></span>'+
															'<div class="lecture-title-blk">'+
																'<span class="lecture-number">' + (k+1) + '</span>'+
																'<span class="lecture-title-txt">Assignment : ' + data.subjects[i].independentAssignment[k].name + '</span>'+
																liveHTML +
															'</div>'
														'</a>'+
													'</h4>'+
												'</td>'+
											'</tr>';
								noOfAssignments++;
							}
						}
						
						if(data.subjects[i].totalDuration>0) {
							totalDuration = totalDuration + parseInt(data.subjects[i].totalDuration);
						}
						subject_html += '</table>';
						subjectCount++;
					}
				}
				if(totalDuration>0) {
					var result = calcDisplayTimeWords(totalDuration);
					$('#courseTest').after(
										'<li class="list-item-short">'+
											'<label for=""><i class="fa fa-youtube-play"></i> Duration :</label>'+
											'<span class="value" id="totalDuration"> '+result+'</span>'+
										'</li>');
					$('#courseTest').remove();
				} else {
					$('#courseTest').after(
										'<li class="list-item-short">'+
											'<label for=""><i class="fa fa-question-circle"></i> Questions :</label>'+
											'<span class="value" id="totalDuration"> '+data.totalQuestions+'</span>'+
										'</li>');
					$('#courseTest').remove();
				}
				//$('#course-append').append(subject_html);
				$('#curriculum').append(subject_html);
				$('.viewDemo').on('click', function() {
					window.location = 'student/chapterContent.php?courseId=' + getUrlParameter('courseId') + '&subjectId=' + $(this).parents('table').attr('data-subjectId') +'&chapterId=' + $(this).parents('tr').attr('data-chapterId') + '&demo=1';
				});
				$.each($('#course-append table'), function() {
					$(this).find('.soup').hide();
					$.each($(this).find('.soup'), function() {
						if($(this).text() != '--') {
							$(this).parents('table').find('.soup').show();
							return;
						}
					});
					$(this).find('tr.chapter:eq(0), tr.chapter:eq(1), tr.chapter:eq(2)').show();
					if($(this).find('tr.chapter').length > 3)
						$(this).parents('.about').find('.viewMoreChap').show();
					$('.viewMoreChap').off('click');
					$('.viewMoreChap').on('click', function() {
						if($(this).attr('data-hidden') == 0) {
							$(this).parents('.about').find('table').find('tr').show();
							$(this).text('Hide');
							$(this).attr('data-hidden', '1');
						}
						else {
							$(this).parents('.about').find('table').find('tr').hide();
							$(this).parents('.about').find('table').find('tr.chapter:eq(0), tr.chapter:eq(1), tr.chapter:eq(2)').show();
							$(this).parents('.about').find('table').find('tr.independent').show();
							$(this).text('View More');
							$(this).attr('data-hidden', '0');
						}
					});
				});
			}
			$('#professors').html(professors);
			$('#noOfSubjects').html(noOfSubjects);
			/*$('#noOfLectures').html(noOfLectures);
			$('#noOfExams').html(noOfExams);
			$('#noOfAssignments').html(noOfAssignments);*/
			var contentBuild = 0;
			var contentClass = 0;
			var contentHTML	 = "";
			if(noOfLectures>0) {contentBuild++;}
			if(noOfExams>0) {contentBuild++;}
			if(noOfAssignments>0) {contentBuild++;}
			if(contentBuild>0) {
				switch(contentBuild) {
					case 1: contentClass = "list-item-long";break;
					case 2: contentClass = "list-item-short";break;
					case 3: contentClass = "list-item-xshort";break;
				}
				if(noOfLectures>0) {
					contentHTML+=  '<li class="'+contentClass+'">'+
										'<label for=""><i class="fa fa-youtube-play"></i> Lectures :</label>'+
										'<span class="value" id="noOfLectures"> '+noOfLectures+'</span>'+
									'</li>';
				}
				if(noOfExams>0) {
					contentHTML+=  '<li class="'+contentClass+'">'+
										'<label for=""><i class="fa fa-pencil-square-o"></i> Exams :</label>'+
										'<span class="value" id="noOfExams"> '+noOfExams+'</span>'+
									'</li>';
				}
				if(noOfAssignments>0) {
					contentHTML+=  '<li class="'+contentClass+'">'+
										'<label for=""><i class="fa fa-book"></i> Assignments :</label>'+
										'<span class="value" id="noOfAssignments"> '+noOfAssignments+'</span>'+
									'</li>';
				}
				$('#contentStats').after(contentHTML);
				$('#contentStats').remove();
			}
		});
	}
	else
		alert("No Subject Found Details");
	fetchCourseReviews();
}


/**
* funtion to fetch all reviews for all subjects of the courseDetails
*
* @author Rupesh Pandey
* @date 23/05/2015
*/
function fetchCourseReviews() {
	var req = {};
	var res;
	req.action = 'get-all-reviews-for-course';
	req.courseId = getUrlParameter('courseId');
	$.ajax({
		type: "post",
		url: ApiEndpoint,
		data: JSON.stringify(req) 
	}).done(function(res) {
		res = $.parseJSON(res);
		var html = '';
		var c1=c2=c3=c4=c5=0;
		$.each(res.reviews, function(i, review) {
			//counting reviews
			switch(review.rating) {
				case '1.00':
					c1++;
					break;
				case '2.00':
					c2++;
					break;
				case '3.00':
					c3++;
					break;
				case '4.00':
					c4++;
					break;
				case '5.00':
					c5++;
					break;
			}
			var date = '';
			var dateobj = new Date(parseInt(review.time + '000'));
			date = format(dateobj.getDate()) + ' ' + MONTH[dateobj.getMonth()] + ' ' + dateobj.getFullYear() + ' ' + format(dateobj.getHours()) + ':' + format(dateobj.getMinutes());
			html += '<li class="comment-item">'
						+ '<div class="row">'
							+ '<div class="col-sm-3">'
								+ '<div class="user-block">'
									+ '<div class="user-avatar">'
										+ '<img src="' + review.studentImage + '" alt="">'
									+ '</div>'
									+ '<span>' + review.studentName + '</span>'
								+ '</div>'
							+ '</div>'
							+ '<div class="col-sm-9">'
								+ '<div class="rating">'
									+ '<input type="hidden" class="studentRating" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="' + review.rating + '" DISABLED/>'
									+ '<span class="">&emsp;' + date + '</span>';
								if(subjectCount > 1)
									html += '<span class="subject"> (Subject: ' + review.subjectName + ')</span>';
								html += '</div>'
								+ '<h4>' + review.title + '</h4>'
								+ '<div class="comment">'
									+ '<p>' + review.review + '</p>'
								+ '</div>'
							+ '</div>'
						+ '</div>'
					+ '</li>';
		});
		$('#comments ul.list-comments').append(html);
		$('.studentRating').rating();
		var total = c1 + c2 + c3 + c4 + c5;
		if(total == 0)
			$('#comments ul').html('<li class="comment-item"><div class="user-block"><span style="margin-left: 0px;">No Reviews Found</span></div></li>');
		$('.progress1').css('width', percentage(c1, total) + '%');
		$('.progress2').css('width', percentage(c2, total) + '%');
		$('.progress3').css('width', percentage(c3, total) + '%');
		$('.progress4').css('width', percentage(c4, total) + '%');
		$('.progress5').css('width', percentage(c5, total) + '%');
		$('.count1').text(c1);
		$('.count2').text(c2);
		$('.count3').text(c3);
		$('.count4').text(c4);
		$('.count5').text(c5);
	});
}

/**
* Funtion to format single digit number into double digit i.e. append one before item
*
* @author Rupesh Pandey
* @date 23/05/2015
* @param integer input number
* @return integer two digit number
*/
function format(number) {
	if(number < 10)
		return '0' + number;
	else
		return number;
}

function percentage(number, total) {
	if(total == 0)
		return 0;
	return (parseInt(number)/parseInt(total)) * 100;
}

function calcDisplayTimeWords(totalDuration) {
	var hours = parseInt( totalDuration / 3600 ) % 24;
	var minutes = parseInt( totalDuration / 60 ) % 60;
	var seconds = totalDuration % 60;

	if(hours>0) {
		if(minutes>0 && minutes<=15) {
			return hours+".25 hours";
		} else if(minutes>15 && minutes<=30) {
			return hours+".5 hours";
		} else if(minutes>30 && minutes<=45) {
			return hours+".75 hours";
		} else {
			return hours+" hours";
		}
	} else if(minutes>0) {
		if(seconds>0 && seconds<=15) {
			return minutes+".25 min";
		} else if(seconds>15 && seconds<=30) {
			return minutes+".5 min";
		} else if(seconds>30 && seconds<=45) {
			return minutes+".75 min";
		} else {
			return minutes+" min";
		}
	} else {
		return seconds+" sec";
	}
}

function calcDisplayTime(totalSec) {
	var hours = parseInt( totalSec / 3600 ) % 24;
	var minutes = parseInt( totalSec / 60 ) % 60;
	var seconds = totalSec % 60;

	if(hours>0) {
		return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
	} else {
		return (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
	}
}


function addEventHandlers() {


}