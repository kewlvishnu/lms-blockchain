function min(element,len) {
	var parent=element.parent();
	if(element.val().length<len) {
		var errorMsg=element.attr('placeholder')+" must be at least "+len+" characters.";
		parent.addClass('has-error');
		element.next().remove();
		parent.append("<span class='help-block'>"+errorMsg+"</span>");
	}
	else {
		parent.removeClass('has-error');
		element.next().remove();
	}
}
function validateUsername(element) {
	parent=element.parents('div:eq(0)');
	//console.log(element);
	if(element.val().length>=4) {
		if(element.val().length>20) {
			var errorMsg=element.attr('placeholder')+" must be less than 20 characters.";
			element.parents('div:eq(0)').addClass('has-error');
			element.next().remove();
			element.parents('div:eq(0)').append("<span class='help-block'>"+errorMsg+"</span>");
		}
		else {
			regex=/^[a-zA-Z0-9_.-]{3,20}$/;
			if(regex.test(element.val())) {
				element.parents('div:eq(0)').removeClass('has-error');
				element.next().remove();
				var req = {};
				req.action = 'validate-username';
				req.username = element.val();
				req=JSON.stringify(req);
				$.post('api/', req).success(function(resp) {
					res=jQuery.parseJSON(resp);
					if(res.valid == false) {
						element.parents('div:eq(0)').addClass('has-error');
						element.next().remove();
						element.parents('div:eq(0)').append("<span class='help-block'>Username not available. Please select a different username.</span>");
					}
					else {
						element.parents('div:eq(0)').removeClass('has-error');
						element.next().remove();
						element.parents('div:eq(0)').addClass('has-success');
						element.parents('div:eq(0)').append("<span class='help-block'>Username available.</span>");
					}
				});
			}
			else {
				var errorMsg=element.attr('placeholder')+" must not contain white spaces and symbols other than underscore, hyphen and dot.";
				element.parents('div:eq(0)').addClass('has-error');
				element.next().remove();
				element.parents('div:eq(0)').append("<span class='help-block'>"+errorMsg+"</span>");
			}
		}
	}
	else {
		var errorMsg=element.attr('placeholder')+" must be at least 4 characters.";
		element.parents('div:eq(0)').addClass('has-error');
		element.next().remove();
		element.parents('div:eq(0)').append("<span class='help-block'>"+errorMsg+"</span>");
	}
}
function emailValidation(element) {
	var parent=element.parent();
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(regex.test(element.val())) {
		parent.removeClass('has-error');
		element.next().remove();
		var req = {};
		req.action = 'validate-email';
		req.email = element.val();
		req.role = form.userRole;
		req=JSON.stringify(req);
		$.post('api/', req).success(function(resp) {
			res=jQuery.parseJSON(resp);
			if(res.valid == false) {
				parent.addClass('has-error');
				element.next().remove();
				parent.append("<span class='help-block'>Email address already registered please select a different one.</span>");
			}
			else {
				parent.removeClass('has-error');
				element.next().remove();
			}
		});
	}
	else {
		parent.addClass('has-error');
		element.next().remove();
		parent.append("<span class='help-block'>Doesn't seems to be a valid email address</span>");
	}
}
function rePassword(element) {
	var parent=element.parent();
	if(element.val()!=$("#password").val()) {
		var errorMsg="Passwords do not match.";
		parent.addClass('has-error');
		element.next().remove();
		parent.append("<span class='help-block'>"+errorMsg+"</span>");
	}
	else {
		parent.removeClass('has-error');
		element.next().remove();
	}
}
function validatePin() {
	if($('#country').val() != 1)
		return;
	pin=$('#pin').val();
	parent=$("#pin").parent();
	regex=/(\d{6})/;
	if(regex.test(pin)) {
		parent.removeClass('has-error');
		$('#pin').next().remove();
	}
	else {
		parent.addClass('has-error');
		$('#pin').next().remove();
		parent.append("<span class='help-block'>Invalid Pin number.</span>");
	}
}
function validateStep1() {
	$(".errorDisplay").text('');
	$("#firstName, #lastName, #password, #rePassword, #name, #contactMobile, #pin").blur();
	if($('#email').val() == '' || $('#username').val() == '')
		$('#email, #username').blur();
	if(typeof $('.has-error')[0] == 'undefined') {
		form.email=$("#email").val();
		form.firstName=$("#firstName").val();
		form.lastName=$("#lastName").val();
		form.username=$("#username").val();
		form.password=$("#password").val();
		form.rePassword=$("#rePassword").val();
		
		form.contactMobilePrefix = $('#prefixContactMobile').val();
		if($('#prefixContactMobile').val() == '')
			form.contactMobilePrefix="+91";
		form.contactMobile=$('#contactMobile').val();
		form.contactLandlinePrefix=$('#prefixContactLandline').val();
		form.contactLandline=$('#contactLandline').val();
		form.addressCountryId=$("#country").val();
		
		if(form.userType != 'institute' || form.userType != 'student')
			form.gender = $("#gender").val();
		if(form.userType == 'institute')
			form.instituteName = $("#name").val();
		if(form.userType != 'student') {
			form.addressCity=$("#city").val();
			form.addressPin=$("#pin").val();
			form.addressState=$("#state").val();
			form.addressStreet=$("#street").val();
		}
		if(form.userType == 'student') {
			form.contactMobilePrefix = $('#sprefixContactMobile').val();
			if($('#sprefixContactMobile').val() == '')
				form.contactMobilePrefix="+91";
			form.contactMobile=$('#scontactMobile').val();
			form.contactLandlinePrefix=$('#sprefixContactLandline').val();
			form.contactLandline=$('#scontactLandline').val();
		}
		return true;
	}
	else {
		var parent=$("#submit").parent();
		var errorMsg="Your form contain errors.";
		$(".errorDisplay").text(errorMsg);
		return false;
	}
	return false;
}
function validateAgree(element) {
	if(element.prop('checked')) {
		return true;
	}
	else {
		return false;
	}
}
function errorContact(str) {
	$("#contact").addClass('has-error');
	$("#error").text(str);
	$("#error").show();
}
function checkContact() {

	//commenting this as discussed with vishnu on phone
	/*if($("#prefixContactMobile").val().length>0 || $("#contactMobile").val().length>0) {
		if(validateMobile())
			return true;
		else {
			errorContact("Please provide correct Contact Number.");
			return false;
		}
	}
	if($("#prefixContactLandline").val().length>0 || $("#contactLandline").val().length>0) {
		if(validateLandline())
			return true;
		else {
			errorContact("Please provide correct Contact Number.");
			return false;
		}
	}*/
	if($("#prefixContactMobile").val() == '' && $("#contactMobile").val() == '' && $("#prefixContactLandline").val() == '' && $("#contactLandline").val() == '') {
		errorContact("Please specify at least one contact number.");
		return false;
	}
	return true;
}
function validateMobile() {
	preMobile=Mobile=false;
	pre=$("#prefixContactMobile").val();
	preN=pre.substring(1);
	regex=/^(\d{1,5})$/;
	if(regex.test(preN) && pre[0]=='+')
		preMobile=true;
	mobile=$("#contactMobile").val();
	regex=/^(\d{10})$/;
	if(regex.test(mobile))
		Mobile=true;
	if(preMobile && Mobile)
		return true;
	if(pre == "+91" && mobile == '' && validateLandline())
		return true;
	return false;
}
function validateLandline() {
	preLandline=Landline=false;
	pre=$("#prefixContactLandline").val();
	preN=pre.substring(1);
	regex=/^(\d{1,5})$/;
	if(regex.test(preN) && pre[0]=='0')
		preLandline=true;
	landline=$("#contactLandline").val();
	regex=/^(\d{7})$/;
	if(regex.test(landline))
		Landline=true;
	if(preLandline && Landline)
		return true;
	return false;
}
