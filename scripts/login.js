var roleuser=0;
$(document).ready(function() {
	addResetListener(getUrlParameter('id'));
	
	var notify = getUrlParameter("notify");
	$("#signIn").click(function(e) {
		e.preventDefault();
		$('.error-msg').hide();
		var user=$('#user').val();
		var pwd=$('#pwd').val();
		if(user.length<3)
			$('#userError').html("A valid username is required.").show();
		if(pwd.length<6)
			$('#pwdError').html("A valid password is required.").show();
		if(user.length>=3 && pwd.length>=6) {
			req = {
					'action' : 'login',
					'username' : user,
					'password' : pwd,
					'userRole' : $('#userRole').val() 
				};
			req=JSON.stringify(req);
			$.post('api/', req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.valid == false) {
					if(res.reset == 1) {
						$('#signInError').html(res.reason + '<a href="#" class="resend-link">Resend email</a>').show();
						addResetListener(res.userId);
					}
					else
						$('#signInError').html(res.reason).show();
				}
				if(res.valid == true && res.userRole == 4) {
					if(typeof(notify) === "undefined") {
						if(typeof(paymentPageStudent) === "undefined" ){
							window.location = "student/";
						}else{
							window.location = "courseDetails.php?courseId="+courseId;
						}
					}
					else {
						window.location = "student/notification.php";
					}
				}
				else if(res.valid == true)
				{
					if(typeof (InstitutePurchasePage) === 'undefined'){
						window.location = "admin/dashboard.php";
//                         switch(res.userRole){
//                          case '1': window.location = "admin/instituteDashboard.php";break;
//                          case '2': window.location = "professor/profile.php";break;
//                          case '3': window.location = "publisher/profile.php";break;
//                      }
					}else{
						window.location = 'courseDetailsInstitute.php?courseId='+courseId;
					}
				}
		});
	}
});
        $('#userRole').on('change',function(){
            $('#signInError').html('').hide();
        });
   	$('#resetPassword').click(function() {
		$('.error-msg').hide();
		var fuser=$('#fuser').val();
		if(fuser.length < 6)
			$('#fuserError').html("A valid username is required.").show();
		if(fuser.length >= 6) {
			var req = {};
			req.action = 'send-reset-password-link';
			req.username = fuser;
			req.userRole = $('#fuserRole').val();
			req = JSON.stringify(req);
			$.post('api/', req).success(function(resp) {
				res = jQuery.parseJSON(resp);
				$('#forgetError').html(res.message).show();
			});
		}
	});
	
	function getUrlParameter(sParam)
	{
		sParam = sParam.toLowerCase();
		var sPageURL = window.location.search.substring(1).toLowerCase();
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++) 
		{
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam) 
			{
				return sParameterName[1];
			}
		}
	}
	
	function addResetListener(userId) {
		$('.resend-link').off('click');
		//resend verification link
		$('.resend-link').on('click', function() {
			var req = {};
			var res;
			req.id = userId;
			if(req.id == undefined)
				alert("Some error occurred contact admin.");
			req.action = 'resend-verification-link';
			$.ajax({
				'type'	:	'post',
				'url'	:	'api/index.php',
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else
					alert("Verification mail sent. Please check your mailbox.");
			});
		});
	}
	
});