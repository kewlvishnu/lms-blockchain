var ApiEndpoint = 'api/index.php';
var currency = 2;

$(document).ready(function() {
	Checklogin();
	
	$('.fa.fa-search').on('click', function() {
		$('#searchForm').submit();
	});
	/*$('#searchBox').autocomplete({
		'lookup': []
	});*/
	
	//adding search click listener
	$('#searchBox').on('keyup', function() {
		if($(this).val() == '') {
			$('#searchDiv1').html('');
			return;
		}
		var req = {};
		var res;
		req.action = 'search-course';
		req.text = $('#searchBox').val();
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			/*$('#searchBox').autocomplete('disable');
			$('#searchBox').autocomplete('setOptions', {
				'lookup': ['qwerty', 'qwer', 'qw', 'qwertyu', 'qwertyui']
			});
			return;*/
			$('#searchDiv1').html('');
			res = $.parseJSON(res);
			if(res.status == 1) {
				var html = '';
				if(res.names.length > 0) {
					$.each(res.names, function(i, name) {
						html += '<li class="searchLi">' + name.name + '</li>';
					});
				}
				if(res.tags.length > 0) {
					$.each(res.tags, function(i, tag) {
						html += '<li class="searchLi">' + tag + '</li>';
					});
				}
				$('#searchDiv1').html(html);
				$('.searchLi').on('click', function() {
					$('#searchBox').val($(this).text());
					window.location = 'search.php?text=' + $(this).text();
				});
			}
			else
				alert(res.message);
		});
	});
});

function getTemplateCousres(){
	var req = {};
	var res;
	req.userId = $('#inputUserId').val();
	req.action = 'page-template-courses';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		fillTemplateCourses(res);
		//console.log(res);
	});
}
function fillTemplateCourses(data){
	var html='';
	var licenseHtml='';
	var displayCurrency='<i class="fa fa-dollar"></i>';
	$.each(data.courses, function(i, course) {
		displayCurrency = '<i class="fa fa-dollar"></i>';
		if(currency==2) {
			course.studentPrice=course.studentPriceINR;
			displayCurrency = '<i class="fa fa-rupee"></i>';
		}
		/*if(course.name.length > 60)
			course.name = course.name.substring(0, 58) + '...';
		else if(course.name.length < 31)
			course.name += '<br><br>';*/
		if(course.studentPrice == '0' || course.studentPrice == '0.00') {
			displayCurrency = '';
			course.studentPrice = 'Free';
		}
		link = '';
		name = '';
		switch(parseInt(course.owner.role)) {
			case 1:
				link = 'student/instituteProfile.php?instituteId=';
				name = course.owner.detail.name;
				break;
			case 2:
			case 3:
				link = 'student/professorProfile.php?professorId=';
				name = course.owner.detail.firstName + ' ' + course.owner.detail.lastName;
				break;
		}
		/*if(name.length > 27)
			name = name.substr(0, 27) + '...';*/
		if(course.rating.total == 0)
			rating = 0;
		else
			rating = parseFloat(course.rating.rating).toFixed(1);
		html += '<div class="col-xs-12 col-sm-6 col-md-3 xyz course1">'
			+ '<section class="panel" style="min-height: 370px;">'
			+    '<div class="pro-img-box">'
			+       '<a href="courseDetails.php?courseId='+course.id+'" class="pro-title"><img src=" '+course.image+'" alt="" class="course-img"></a> '
			+ 		'<a href="' + link + course.owner.detail.userId + '" class="adtocart">'
			+         '<img src="' + course.owner.detail.profilePic + '" style="border-radius: 50%;">'
			+     	'</a>'
			+ '</div>'
			+	'<div class="panel-body">'
			+     '<h4 class="text-center">'
			+        '<a href="courseDetails.php?courseId='+course.id+'" class="pro-title course-name">'
			+            course.name
			+        '</a>'
			+   '</h4>'
			+    '<span class="price"><span class="course-price">'+displayCurrency+course.studentPrice+'</span>'
			+ '<span class="pull-right course-joined"><i class="fa fa-group"></i> ' + course.studentCount + '</span> </span>'
			+ '<div class="author-by"><span class="txt-by">By : </span><a href="' + link + course.owner.detail.userId + '">' + name + '</a></div>'
			+ '<div class="text-center"><input type="hidden" class="rating" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="' + rating + '" DISABLED /></div>'
			+  '</div>'
			+  '</section>'
			+'</div>';
	});
	$('#sample-course').html(html);
	$('.rating').rating();
	$('.self-hidden').show();
	//$('#license-course').html(licenseHtml);
  
}
function Checklogin(){
	var req = {};
	var res;
	req.action = 'check-login';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status==1){
			checkCurrency();
		}else{
			getCountryViaIp();
			//getTemplateCousres();
		}
	});
}
function checkCurrency(){
	var req = {};
	var res;
	req.action = 'get-currency';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status==1){
			currency=res.currency[0].currency;//'<i class="fa fa-rupee"></i>';
		} 
		getTemplateCousres();
	});
}

function getCountryViaIp() {
	var res;
	$.ajax({
		'type'	: 'get',
		'url'	: 'http://ip-api.com/json/'
	}).done(function(res) {
		if(res.countryCode == 'IN')
			currency = 2;
		else
			currency = 1;
		getTemplateCousres();
	})
}

function getUrlParameter(sParam)
{
	sParam = sParam.toLowerCase();
	var sPageURL = window.location.search.substring(1).toLowerCase();
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) 
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) 
		{
			return sParameterName[1];
		}
	}
}