

<script>
var postVariables= <?php echo ((!empty($_POST))?json_encode($_POST['item_name']):"null"); ?>;

$(function() {
	/*//getting pdt from paypal
	data = 'cmd=_notify-synch&tx=46B21466MG937540D&at=rlSIwjzFIRnUnBU-paOJXUzzr2_QKuhrdUB7HHhg6HYVH7I87XStv7o1Ds4';
	$.ajax({
		type	: 'POST',
		url		: 'https://www.sandbox.paypal.com/cgi-bin/webscr',
		crossDomain: true,
		data	: data
	}).done(function(res) {
		console.log(res);
	});*/

	//checking if transaction is valid or not
	var paypalId = getUrlParameter('tx');
	var item = getUrlParameter('item_number');
	var amount = getUrlParameter('amt');
	var payment_Status=getUrlParameter('st');

		
		
	/*var paypalId = '0TX80024ML680520D';
	var item = 235;
	var amount = 11110.10;
	var payment_Status='completed';
	console.log("hello");
	console.log(amount);*/
	if(paypalId != undefined && item != undefined && amount != undefined) {
		var req = {};
		var res;
		req.action = 'paypal-return';
		req.paypalId = paypalId;
		req.orderId = item;
		req.amount = amount;
		req.iteminfo = postVariables;
		req.payment_Status = payment_Status;
		$.ajax({
			'type'	: 'post',
			'url'	: 'api/index.php',
			'data'	: JSON.stringify(req)
		}).done(function(res) {
			//console.log(res);
			$('#status').show();
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status == 1) {			
			$('#orderId').text(item);
				if(res.type == 1) {
					$('#admin').show();
					$('#quantity').text(res.purchase_detail);
					$('#rate').text(res.rate);
					$('#itemName').text('Course Keys');
				}
				else {
					$('#itemName').text('C00' + res.purchase_detail + ' ' + res.name);
				}
				$('.total').text(res.amount);
				$('#dateTime').text(res.date);
				if(res.userRole != 4)
					$('#account').attr('href', 'admin/CourseKey.php');
				else
					$('#account').attr('href', 'student/');

			}else {
				console.log('Transaction Failed');
				$('#dateTime').text(res.date);
				$('#status').html('<span class="pull-right custom-danger-badge">Payment Failed</span>');
				if(res.userRole != 4)
					$('#account').attr('href', 'admin/CourseKey.php');
				else
					$('#account').attr('href', 'student/');
			}
		});
	}else {
		console.log('Transaction Failed');
		$('#status').show();
		$('#dateTime').text(new Date());
		$('#status').html('<span class="pull-right custom-danger-badge">Payment Failed</span>');
		$('#account').remove();
	}
});
/*
// Create the XHR object.
function createCORSRequest(method, url) {
  var xhr = new XMLHttpRequest();
  if ("withCredentials" in xhr) {
    // XHR for Chrome/Firefox/Opera/Safari.
    xhr.open(method, url, true);
  } else if (typeof XDomainRequest != "undefined") {
    // XDomainRequest for IE.
    xhr = new XDomainRequest();
    xhr.open(method, url);
  } else {
    // CORS not supported.
    xhr = null;
  }
  return xhr;
}

// Helper method to parse the title tag from the response.
function getTitle(text) {
  return text.match('<title>(.*)?</title>')[1];
}

// Make the actual CORS request.
function makeCorsRequest() {
  // All HTML5 Rocks properties support CORS.
  var url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';

  var xhr = createCORSRequest('POST', url);
  if (!xhr) {
    alert('CORS not supported');
    return;
  }

  // Response handlers.
  xhr.onload = function() {
    var text = xhr.responseText;
    var title = getTitle(text);
    alert('Response from CORS request to ' + url + ': ' + title);
  };

  xhr.onerror = function() {
    alert('Woops, there was an error making the request.');
  };

  xhr.send();
}

makeCorsRequest();*/

function getUrlParameter(sParam)
{
	sParam = sParam.toLowerCase();
    var sPageURL = window.location.search.substring(1).toLowerCase();
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

</script>