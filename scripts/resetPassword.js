$(function() {
	$('#changePwd').on('click', function() {
		$('#error').html('');
		unsetError($('#password1'));
		if($('#password1').val().length < 6) {
			setError($('#password1'), 'Please provide a password minimum 6 characters.')
			return;
		}
		unsetError($('#password2'));
		if($('#password2').val() != $('#password1').val()) {
			setError($('#password2'), 'Passwords do not match.')
			return;
		}
		var req = {};
		var res;
		req.action = 'reset-password';
		req.userId = getUrlParameter('userId');
		req.token = getUrlParameter('token');
		req.password = $('#password1').val();
		if(req.token != undefined && req.userId != undefined) {
			$.ajax({
				'type'	:	'post',
				'url'	:	'api/',
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else
					$('#error').html('Your password has been updated. Please <a href="#login-modal" data-toggle="modal">login here</a>.').css('color', 'green');
			});
		}
		else
			$('#error').html('Your password rest link has expired or it never existed.');
	});
	
	//function to unset error
	function unsetError(where) {
		if(where.parents('div:eq(0)').hasClass('has-error')) {
			where.parents('div:eq(0)').removeClass('has-error');
			where.parents('div:eq(0)').find('.help-block').remove();
		}
	}
	
	//function to set error
	function setError(where, what) {
		where.parents('div:eq(0)').addClass('has-error');
		where.parents('div:eq(0)').append('<span class="help-block">'+what+'</span>');
	}
	
	//function to fetch data from url
	function getUrlParameter(sParam) {
		sParam = sParam.toLowerCase();
		var sPageURL = window.location.search.substring(1).toLowerCase();
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++) 
		{
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam) 
			{
				return sParameterName[1];
			}
		}
	}
	
});