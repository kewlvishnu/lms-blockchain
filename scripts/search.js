var ApiEndpoint = 'api/index.php';
var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var geoCurrency = 1;
$(function() {
	Checklogin();	
		
	$( document ).ajaxStart(function() {
		$('#loader').show();
		
	});
	
	$( document ).ajaxComplete(function() {
		$('#loader').hide();
	});
	
	$('#searchBox').on('blur', function() {
		$('#searchDiv').hide();
	});

	$('#searchForm').on('submit', function(e) {
		e.preventDefault();	
		//Checklogin();
		search($('#searchBox').val());
	});
	
	
	if(getUrlParameter('category') != undefined) {
		var category = getUrlParameter('category');
		var catId = category.split(',');
		$.each(catId, function(i,v) {
			$('.catCheck[data-id="'+v+'"]').prop('checked', true);
		})
	}
	
	if(getUrlParameter('price') != undefined) {
		var price = getUrlParameter('price');
		var priceId = price.split(',');
		$.each(priceId, function(i,v) {
			$('.priceCheck[data-id="'+v+'"]').prop('checked', true);
		})
	}
	
	if(getUrlParameter('text') != undefined) {
		$('#searchBox').val(decodeURI(getUrlParameter('text')).replace(/\+/g, ' '));
	}
	//search(getUrlParameter('text'));
	$('.catCheck, .priceCheck').on('click', function() {
		search(getUrlParameter('text'));
	});
	$('#searchBox1').on('keyup', function() {
		if($(this).val() == '') {
			$('#searchDiv').html('');
			return;
		}
		
		var req = {};
		var res;
		req.action = 'search-course';
		req.text = $('#searchBox').val().trim().replace(/\+/g, ' ');
		//console.log("req");
		$.ajax({
			'type'	:	'post',
			'url'	:	'api/index.php',
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			/*$('#searchBox').autocomplete('disable');
			$('#searchBox').autocomplete('setOptions', {
				'lookup': ['qwerty', 'qwer', 'qw', 'qwertyu', 'qwertyui']
			});
			return;*/
			$('#searchDiv').html('');
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status == 1) {
				var html = '';
				if(res.names.length > 0) {
					$.each(res.names, function(i, name) {
						html += '<li class="searchLi">' + name.name + '</li>';
					});
				}
				if(res.tags.length > 0) {
					$.each(res.tags, function(i, tag) {
						html += '<li class="searchLi">' + tag + '</li>';
					});
				}
				//html += '</ul>';
				$('#searchDiv').html(html);
				$('.searchLi').on('click', function() {
					$('#searchBox').val($(this).text());
					$('#searchDiv').html('');
					search($('#searchBox').val());
				});
			}
			else
				alert(res.message);
		});
	});

});
function search(text) {

	var req = {};
	var res;
	req.action = 'search-page';
	req.text = decodeURI(text).trim().replace(/\+/g, ' ');
	req.currency=2;
	
	var catId = [];
	$.each($('.catCheck'), function() {
		if($(this).prop('checked'))
			catId.push($(this).attr('data-id'));
	});
	req.cat = catId.join(',');
	var priceId = [];
	$.each($('.priceCheck'), function() {
		if($(this).prop('checked'))
			priceId.push($(this).attr('data-id'));
	});
	req.price = priceId.join(',');
	window.history.pushState({"html": '',"pageTitle": 'Searching'},"Search State", 'search.php?text=' + $('#searchBox').val() + '&category=' + catId.join(',') + '&price=' + priceId.join(','));
	//console.log(req);
	if(req.text=='')
	{
		//window.location = '/courses';
	}
	else{	
	$.ajax({
		'type'	:	'post',
		'url'	:	'api/index.php',
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		$('#text').text(decodeURI(text).trim().replace(/\+/g, ' '));
		$('#loadCourse').find('*').remove();
		var html = '';
		if(res.courses.length > 0) {
			$.each(res.courses, function(i, course) {
				displayCurrency = '<i class="fa fa-rupee"></i>';
				var startdate = new Date(parseInt(course.liveDate));	
				var endDate = (course.endDate == '')?'No Expiry':new Date(parseInt(course.endDate));
				if(course.studentPriceINR == '0' || course.studentPriceINR == '0.00') {
					course.studentPriceINR = 'Free';
					displayCurrency = '';
				}
				var owner="";
				if(course.owner1!=null)
				owner=course.owner1;
				else
				 owner= course.owner+" "+course.lastName;
				html += '<div class="col-md-12 col-sm-12">'
						+ '<div>'
						+ '<div class="row">'
						+ '<div class="col-md-3"><a  href="courseDetails.php?courseId=' + course.id + '" ><img style="width: 100%;height: 165px;" onerror="this.src=\'img/course.png\'" class="img-responsive" src="' + course.image + '" alt=""></a>'
						+ '</div>'
						+ '<div class="col-md-9">'
						+'<div class="col-md-12"><div style="float:left"> <h3 class=""> <a href="courseDetails.php?courseId=' + course.id + '" > ' + course.name + '<span style="font-size:12px;font-weight:600;"> (ID: C00' + course.id + ')</span> </a></h3><strong>By</strong>: ' + owner 
						+ '</div>'
						+ '<div style="float:right;">'
						+ '<div class="text-right">'
						+ '<h4>' + displayCurrency + ' ' + course.studentPriceINR + '</h4>'
						+ '<a href="courseDetails.php?courseId='+course.id+'" class="btn btn-md btn-success course" data-courseId="' + course.id + '">Purchase now</a>'
						+ '</div>'
						+ '</div>'
						+'</div>'
						+'<div class="col-sm-6" style="padding-top:10px;">'
						+ '<div class="col-xs-2"><i style="font-size:35px; color:orange;margin:5px 10px 0px 0px;" class="fa fa-clock-o"></i></div>'
						+'<div class="col-xs-10">'                  
						+ '<span><strong>Start Date</strong>&nbsp;' + startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + '</span><br/><span><strong>End Date</strong>&nbsp;&nbsp;&nbsp;';
						if(endDate == 'No Expiry')
							html += endDate;
						else
							html+= endDate.getDate() + ' ' + month[endDate.getMonth()] + ' ' + endDate.getFullYear();
						html += '</span>'
						+ '</div>'
						+ '</div>'
					
						+'<div class="col-md-6" style="padding-top:10px;">'
						+ '<div class="col-xs-1"><i style="font-size: 30px; color: orange;margin: 5px 15px 0px 0px;" class="fa fa-th-list"></i></div>'
						+'<div class="col-xs-10" style="padding-left: 10%;">'  
						+ '<strong>Subjects </strong><br/>'
					subjectHtml='';
			    $.each(course.subjects, function (j, subject) {
					subjectHtml += '<a href="courseDetails.php?courseId=' + course.id + '">' + subject.name + ', </a>';
				});
				subjectHtml = subjectHtml.substring(0, subjectHtml.length - 6) + '</a>';
				html = html + subjectHtml + '</div>'
						+ '</div>'
						+'</div>'
						+ '</div>'
				+'<hr class="divider" />'
						+ '</div>'
			
						+ '</div>';
			});
			$('#loadCourse').append(html);
		}
		else
			$('#loadCourse').append('<div class="col-md-12 col-sm-12"><h4>No such courses found. Please Modify search</h4></div>');
	});
}
}
function getUrlParameter(sParam) {
	sParam = sParam.toLowerCase();
    var sPageURL = window.location.search.substring(1).toLowerCase();
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}
function fillpriceINR(){
	var html="";
	/*html+='<li><label><input type="checkbox" class="priceCheck" data-id="1"> Below <i class="fa fa-rupee"></i> 100</label></li>'
			+'<li><label><input type="checkbox" class="priceCheck" data-id="2"> <i class="fa fa-rupee"></i> 101 to <i class="fa fa-rupee"></i> 1000</label></li>'
			+'<li><label><input type="checkbox" class="priceCheck" data-id="3"> <i class="fa fa-rupee"></i> 1001 to <i class="fa fa-rupee"></i> 2000</label></li>'				
			+'<li><label><input type="checkbox" class="priceCheck" data-id="4"> <i class="fa fa-rupee"></i> 2001 to <i class="fa fa-rupee"></i> 3000</label></li>'
			+'<li><label><input type="checkbox" class="priceCheck" data-id="5"> <i class="fa fa-rupee"></i> 3001 to <i class="fa fa-rupee"></i> 4000</label></li>'
			+'<li><label><input type="checkbox" class="priceCheck" data-id="6"> <i class="fa fa-rupee"></i> 4001 to <i class="fa fa-rupee"></i> 5000</label></li>'
			+'<li><label><input type="checkbox" class="priceCheck" data-id="7"> Above <i class="fa fa-rupee"></i> 5000</label></li>';
			$('.priceselect').html(html);*/

		html +=' Below <i class="fa fa-rupee"></i> 100';
		$('.onep').append(html);
}
function Checklogin(){
	//var status=1;
	var req = {};
	var res;
	req.action = 'check-login';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status==1){
			checkCurrency();
		}else{
			getCountryViaIp();
			//getTemplateCousres();
		}
	});
	//return status;
}
function checkCurrency(){
	var req = {};
	var res;
	req.action = 'get-currency';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status==1){
			geoCurrency=res.currency[0].currency;//'<i class="fa fa-rupee"></i>';
			checkOutside(geoCurrency);
		} 
	});
	
}
function getCountryViaIp() {
	var currency = 1;
	var res;
	$.ajax({
		'type'	: 'get',
		'url'	: 'http://ip-api.com/json/'
	}).done(function(res) {
		if(res.countryCode == 'IN'){
		geoCurrency = 2;
		checkOutside(geoCurrency);
		}
		else{
			geoCurrency = 1;
			checkOutside(geoCurrency);
		}
		
	})
	.fail( function(XMLHttpRequest, textStatus, errorThrown) {
       checkOutside(geoCurrency);
    });
	//checkOutside(geoCurrency);
}
function checkOutside(geoCurrency) {
	if(geoCurrency==1) 
	{
		window.location = 'outsideSearch.php?text=' + getUrlParameter('text');
	}
	else{
		
		search(getUrlParameter('text'));
	}
	
	
}