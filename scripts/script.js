var player;
$(document).ready(function(){
	$(document).on('click','.scrolltodiv', function(event) {
		event.preventDefault();
		var target = "#" + this.getAttribute('data-target');
		$('html, body').animate({
			scrollTop: $(target).offset().top
		}, 1000);
	});
	$(".js-course-details").click(function(){
		toggleDataMore($(this),$(".js-full-course-details"));
	});
	$(".js-instructor-details").click(function(){
		toggleDataMore($(this),$(".js-full-instructor-details"));
	});
	var toggleDataMore = function(objLink,objData) {
		var dataMore = objLink.attr("data-more");
		if(dataMore == "full") {
			objData.css("height","auto");
			objLink.attr("data-more","less").html("Hide details");
		} else {
			if(objData.hasClass('js-full-instructor-details')) {
				objData.css("height","100px");
			} else {
				objData.css("height","84px");
			}
			objLink.attr("data-more","full").html("Full details");
		}
	}
	$('.course-sublinks').click(function(){
		event.preventDefault();
		var target = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(target).offset().top
		}, 1000);
	});
	$('#curriculum').on("click", ".btn-preview", function(){
		//event.preventDefault();
		var src = $(this).attr('data-src');
		$("#playerModal").modal('show');
		setTimeout(function(){
			var playerWidth = $("#playerModal .modal-dialog").width();
			player = new MediaElementPlayer('#videoPlayer', {type: 'video/mp4',videoWidth: playerWidth,videoHeight: '100%'});
			var sources = [
				{ src: src, type: 'video/mp4' }
			];

			player.setSrc(sources);
			player.load();
			player.play();
		},500);
		
	});
	$('#dismissVideo').click(function(){
		player.pause();
	});
});