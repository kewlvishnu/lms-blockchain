$(document).ready(function() {
	$("#firstName").blur(function() {
		$(".errorDisplay").text('');
		min($(this),3);
	});
    $("#lastName").blur(function() {
		$(".errorDisplay").text('');
		min($(this),1);
	});
	$("#username").blur(function(){
		$(".errorDisplay").text('');
		validateUsername($(this));
	});
	$('#pin').blur(function() {
		$(".errorDisplay").text('');
		parent=$(this).parents('div:eq(0)');
		if($(this).val() != '')
			validatePin();
		else {
			$(this).parents('div:eq(0)').removeClass('has-error');
			$(this).next().remove();
		}
	});
	//fields where only numbers are allowed
	$("#contactMobile, #prefixContactLandline, #contactLandline, #pin, #sprefixContactMobile, #scontactMobile, #prefixContactLandline, #scontactLandline").keypress(function (e) {
		//if the letter is not digit then don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	});
	$('#dob').on('keypress', function() {
		return false;
	});
	$('#dob').datetimepicker({
		format: 'd F Y',
		timepicker: false,
		closeOnDateSelect: true,
        maxDate: 0,
		minDate: '1950/01/01'
	});
	$("#contactMobile, #contactLandline").blur(function() {
		$(".errorDisplay").text('');
		$("#contact").removeClass("has-error");
		$("#error").text("");
		checkContact();
	});
	$("#email").blur(function() {
		$(".errorDisplay").text('');
		emailValidation($(this));
	});
	$("#password").blur(function() {
		$(".errorDisplay").text('');
		min($(this),6);
		parent = $(this).parents('.form-group');
		if($(this).val() == $('#email').val() && $(this).val() != '') {
			$(this).parents('.form-group').addClass('has-error');
			$(this).parents('.form-group').append('<span class="help-block">Please select a different password other than email.</span>')
		}
		else if($(this).val() == $('#username').val() && $(this).val() != '') {
			$(this).parents('.form-group').addClass('has-error');
			$(this).parents('.form-group').append('<span class="help-block">Please select a different password other than username.</span>')
		}
	});
	$("#rePassword").blur(function() {
		$(".errorDisplay").text('');
		rePassword($(this));
	});
	$("#submit").click(function() {
		if(validateStep1()) {
			if(validateAgree($("#agree"))) {
				form.agree=$("#agree").prop("checked");
				form=JSON.stringify(form);
				$.post('api/', form).success(function(resp) {
					res=jQuery.parseJSON(resp);
					if(res.status == 1)
						window.location="signup-complete.php?id=" + res.userId;
					if(res.status == 0)
						alert(res.message);
				});
			}
			else {
				var errorMsg="You must agree terms and conditions.";
				$(".errorDisplay").text(errorMsg);
			}
		}
	});
});