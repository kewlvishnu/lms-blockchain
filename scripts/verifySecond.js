$(document).ready(function() {
	$("#step2").hide();
	$("#nonIndian").hide();
	$("#name").blur(function(){
		$(".errorDisplay").text('');
		min($(this),5);
	});
	$("#country").change(function() {
		$("#errorDisplay").text('');
		changeCountry($(this));
		selectCountry($(this));
	});
	$("#next").click(function() {
		next();
	});
	$("#previous").click(function() {
		$("#step2").hide();
		$("#step1").show();
	});
	$("#schoolCheckbox").click(function() {
		$(".errorDisplay").text('');
		addRemoveInstituteType($(this));
		$("#school").slideToggle();
		$("#schoolBoardState, #schoolBoardOther").hide();
	});
	$("#coachingCheckbox").click(function() {
		$(".errorDisplay").text('');
		addRemoveInstituteType($(this));
		$("#managementOptions, #engineeringOptions, #medicalOptions, #schoolLevelOptions, #miscOptions").hide();
		$("#coaching").slideToggle();
	});
	$("#management, #engineering, #medical, #schoolLevel, #misc").click(function() {
		$(".errorDisplay").text('');
		var t=$(this).attr('data-text');
		releaseSelectedOptions(t,1);
	});
	$("#collegeCheckbox").click(function() {
		$(".errorDisplay").text('');
		addRemoveInstituteType($(this));
		$("#college").slideToggle();
		$("#collegeTypeOther").hide();
	});
	$("#trainingInstituteCheckbox").click(function() {
		$(".errorDisplay").text('');
		addRemoveInstituteType($(this));
		$("#trainingInstitute").slideToggle();
		$("#trainingInstituteTypeOther").hide();
	});
	$("#selectBoard").change(function() {
		$(".errorDisplay").text('');
		showOtherFieldBoard($(this));
	});
	$("#management, #engineering, #medical, #schoolLevel, #misc").click(function() {
		$(".errorDisplay").text('');
		addRemoveInstituteType($(this));
		showOption($(this));
	});
	$("#selectManagement").multiSelect( {
		afterSelect: function(value) {
			afterSelect(value,$("#selectManagement"));
		},
		afterDeselect: function(value) {
			afterDeselect(value,$("#selectManagement"));
		}
	});
	$("#selectEngineering").multiSelect( {
		afterSelect: function(value) {
			afterSelect(value,$("#selectEngineering"));
		},
		afterDeselect: function(value) {
			afterDeselect(value,$("#selectEngineering"));
		}
	});
	$("#selectMedical").multiSelect( {
		afterSelect: function(value) {
			afterSelect(value,$("#selectMedical"));
		},
		afterDeselect: function(value) {
			afterDeselect(value,$("#selectMedical"));
		}
	});
	$("#selectSchoolLevel").multiSelect( {
		afterSelect: function(value) {
			afterSelect(value,$("#selectSchoolLevel"));
		},
		afterDeselect: function(value) {
			afterDeselect(value,$("#selectSchoolLevel"));
		}
	});
	$("#selectMisc").multiSelect( {
		afterSelect: function(value) {
			afterSelect(value,$("#selectMisc"));
		},
		afterDeselect: function(value) {
			afterDeselect(value,$("#selectMisc"));
		}
	});
	$("#selectCollege").multiSelect( {
		afterSelect: function(value) {
			afterSelect(value,$("#selectCollege"));
		},
		afterDeselect: function(value) {
			afterDeselect(value,$("#selectCollege"));
		}
	});
	$("#selectTrainingInstitute").multiSelect( {
		afterSelect: function(value) {
			afterSelect(value,$("#selectTrainingInstitute"));
		},
		afterDeselect: function(value) {
			afterDeselect(value,$("#selectTrainingInstitute"));
		}
	});
});
$("#submit1").click(function() {
	if(validateStep1()) {
		if(validateAgree($("#agree1"))) {
			form.agreeOn1=$("#agree1").prop("checked");
			form=JSON.stringify(form);
			$.post('api/', form).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.status == 1)
					window.location="signup-complete.php";
			});
		}
		else {
			var errorMsg="You must agree terms and conditions.";
			$(".errorDisplay").text(errorMsg);
		}
	}
});
$("#submit2").click(function() {
	$(".errorDisplay").hide();
	if(validateStep1()) {
		if(validateStep2()) {
			if(validateAgree($("#agree2"))) {
				form.agreeOn2=$("#agree2").prop("checked");
				form=JSON.stringify(form);
				$('#submit2').attr('disabled', true);
				$.post('api/', form).success(function(resp) {
					$('#submit2').attr('disabled', false);
					res=JSON.parse(resp);
						if(res.status == 1)
							window.location="signup-complete.php?id=" + res.userId;
				});
			}
			else {
				var errorMsg="You must agree terms and conditions.";
				$(".errorDisplay").text(errorMsg);
				$(".errorDisplay").show();
			}
		}
	}
	else {
		$("#step2").hide();
		$("#step1").show();
	}
});