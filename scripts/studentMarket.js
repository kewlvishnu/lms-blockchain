var ApiEndpoint = 'api/index.php';
var currency=2;
$(document).ready(function() {
	Checklogin();
});

function getTemplateCousres(){
	var req = {};
	var res;
	req.action = 'student-template-courses';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
			alert(res.message);
		else
			fillTemplateCourses(res);
		//console.log(res);
	});
}

function fillTemplateCourses(data){

	var html='';
	var licenseHtml='';
	var flag=0;
	var newprice=0;
	var newpriceINR=0;
	var today= new Date().getTime();
	var displayCurrency='<i class="fa fa-dollar"></i>';
	var discount='';
	if(data.courses.length > 0) {
		$.each(data.courses, function(i, course) {
			var flag=0;
		if(course.discount.discount  != null){			
			var discountenddate = course.discount.endDate;
		
				if(discountenddate > today )
				{	flag=1;
					newpriceINR=course.studentPriceINR*((100 - course.discount.discountINR) / 100);
					newprice=course.studentPrice*((100 - course.discount.discount) / 100);
					newpriceINR=newpriceINR.toFixed(2);
					newprice=newprice.toFixed(2);

					discount= Math.round(course.discount.discountINR);
				}
				
		}
				

	
		
			displayCurrency='<i class="fa fa-dollar"></i>';
			if(currency==2) {
				course.studentPrice=course.studentPriceINR;
				newprice=newpriceINR;
				displayCurrency='<i class="fa fa-rupee"></i>';
			}
			/*if(course.name.length > 60)
				course.name = course.name.substring(0, 58) + '...';
			else if(course.name.length < 33)
				course.name += '<br><br>';*/
			if(course.studentPrice == '0' || course.studentPrice == '0.00') {
				displayCurrency = '';
				course.studentPrice = 'Free';
				
			}
			link = '';
			name = '';
			switch(parseInt(course.owner.role)) {
				case 1:
					link = 'student/instituteProfile.php?instituteId=';
					name = course.owner.detail.name;
					break;
				case 2:
				case 3:
					link = 'student/professorProfile.php?professorId=';
					name = course.owner.detail.firstName + ' ' + course.owner.detail.lastName;
					break;
			}
			/*if(name.length > 29)
				name = name.substr(0, 29) + '...';*/
			if(course.rating.total == 0)
				rating = 0;
			else
				rating = parseFloat(course.rating.rating).toFixed(1);
			html += '<div class="col-xs-12 col-sm-6 col-md-3 xyz course1">'
				+ '<section class="panel">'
				+ '<div class="pro-img-box">'
				+   '<span class="price"><span class="course-price">'+((flag>0)?'<div class="col-xs-4  Couponstag border-radius COuponsShow"> '+ discount +' % OFF</div>' : '') +'</span>'
				+ '<a href="courseDetails.php?courseId='+course.id+'" class="pro-title"><img src=" '+course.image+'" alt="' + course.name + '" class="course-img"></a> '
				+ '<a href="' + link + course.owner.detail.userId + '" class="adtocart">'
				+         '<img src="' + course.owner.detail.profilePic + '" style="border-radius: 50%;">'
				+     '</a>'
				+ '</div>'
				+	'<div class="panel-body">'
				+     '<h4 class="text-center">'
				+        '<a href="courseDetails.php?courseId='+course.id+'" class="pro-title course-name">'
				+            course.name
				+        '</a>'
				+   '</h4>'
				+ '<span class="price"><span class="course-price">'+((flag>0)?'<strike class= "pricedlandingPage">'+displayCurrency+course.studentPrice+'</strike>'+' '+displayCurrency+newprice : displayCurrency+course.studentPrice)+'</span>'
				+'<span class="pull-right course-joined"><i class="fa fa-group"></i>' + course.studentCount + '</span> </span>'
				+ '<div class="author-by"><span class="txt-by">By : </span><a href="' + link + course.owner.detail.userId + '">' + name + '</a></div>'
				+ '<div class="text-center"><input type="hidden" class="rating" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="' + rating + '" DISABLED /></div>'
				+  '</div>'
				+  '</section>'
				+'</div>';
		});
		$('#sample-course').html(html);
		$('.rating').rating();
	}
	else
		$('#sample-course').html('No courses in Market Place.');
}
function Checklogin(){
                 var req = {};
                 var res;
        	req.action = 'check-login';
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status==1) {
				checkCurrency();
			}else{
				getCountryViaIp();
				//getTemplateCousres();
			}
		});
  }
function checkCurrency(){
	var req = {};
	var res;
    req.action = 'get-currency';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status==1){
			currency=res.currency[0].currency;//'<i class="fa fa-rupee"></i>';
		} 
		getTemplateCousres();
	});
}
function getCountryViaIp() {
	var res;
	$.ajax({
		'type'	: 'get',
		'url'	: 'http://ip-api.com/json/'
	}).done(function(res) {
		if(res.countryCode == 'IN')
			currency = 2;
		else
			currency = 1;
		
	})
	getTemplateCousres();
}

function getUrlParameter(sParam)
{
	sParam = sParam.toLowerCase();
    var sPageURL = window.location.search.substring(1).toLowerCase();
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}