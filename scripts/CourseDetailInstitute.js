var ApiEndpoint = 'api/index.php';
var professors = '';
var currency=2;
var loginstatus=0;
var courseId=getUrlParameter("courseId");
var selfCreatedBy=0;
var InstitutePurchasePage=true;
var license=0;
$(document).ready(function () {
	Checklogin();
});
function loadCourseDetails(){
   var req = {};
    var startDate,data;
    var catIds = [], catNames = [];
    req.action = 'loadCourseDetails';
    req.courseId =courseId
    if (req.courseId != undefined) {
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            if (data.status == 1) {
                var price="";
                $('#courseName').html(data.courseDetails.name);
                $('#subtitle').html(data.courseDetails.subtitle);
				if(data.courseDetails.description.length > 155) {
					var first = data.courseDetails.description.substring(0, 155);
					var second = '<span id="moreDesc" style="display: none;">' + data.courseDetails.description.substring(155, data.courseDetails.description.length) + '</span>';
					$('#description').html(first + second + '<a data-hidden="0" href="#" id="viewMoreDesc">... View More</a>');
					$('#viewMoreDesc').off('click');
					$('#viewMoreDesc').on('click', function(e) {
						e.preventDefault();
						if($(this).attr('data-hidden') == 0) {
							$(this).text(' Hide');
							$(this).attr('data-hidden', "1")
							$('#moreDesc').slideDown();
						}
						else {
							$(this).text('... View More');
							$(this).attr('data-hidden', "0")
							$('#moreDesc').slideUp();
						}
					});
				}
				
				else
					$('#description').html(data.courseDetails.description);
               // startDate = new Date(parseInt(data.courseDetails.liveDate));
               // $('#live-date').html(startDate.getDate() + '/' + (startDate.getMonth() + 1) + '/' + startDate.getFullYear());
				$('#live-date').html(data.courseDetails.liveDate);
                $('#course_img').attr("src", data.courseDetails.image);
                if (data.courseDetails.targetAudience != null) {
                    $('#target-audience').text(data.courseDetails.targetAudience);
                } else {
                    $('#target-audience').prev().remove();
                    $('#target-audience').remove();
                }
                $('#end-date').html(data.courseDetails.endDate);
                $('#course-id').html('C00' + data.courseDetails.id);
				//$('#price').append(displayCurrency+data.courseDetails.studentPrice);
                $('#institute-background').html(data.institute[0].description);
                $.each(data.courseCategories, function (k, cat) {
                    //catNames.push(cat.catName);
                    catIds.push(cat.categoryId);
                    catNames.push(cat.category);
                });
				if(catNames.length == 0)
					$('#courseCategory').html('No Categories Selected');
				else
					$('#courseCategory').html(catNames.join(', '));
				$('#institute-name').html('<a href="#">' + data.institute[0].name + '</a>');
				$('#institute-img').attr("src",  data.institute[0].profilePic);
				$('#tagline').html(data.institute[0].tagline);
				if(data.selfCreated==1){
					selfCreatedBy=1;
				}
                if (loginstatus == 1) {
                    $('#takeCourse').removeAttr('data-toggle');
                    // $('#takeCourse').attr('href','admin/paymentDetails.php?courseId='+courseId+'&license');
					$('#takeCourse').on('click', function () {
						if($('a.selectedOption').length == 0) {
							alert('Please select License type');
							return false;
						}
						if(selfCreatedBy == 1){
							alert('This course is self created');
							return false;
						}
						var type = $('a.selectedOption').attr('data-license');
						window.location = 'admin/paymentDetails.php?courseId=' + courseId + '&license=' + type;//license;
					});
				}
            }
			loadLicenseDetails();
		});
    }
    else
        alert("No course Details");
	getsubjectDetails();
 }
 

function getUrlParameter(sParam)
{
    sParam = sParam.toLowerCase();
    var sPageURL = window.location.search.substring(1).toLowerCase();
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}
function Checklogin(){
	var req = {};
	var res;
	req.action = 'check-login';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status==1) {
			checkCurrency();
			loginstatus=1;
		}else {
			loadCourseDetails();
		}
	});
}
function checkCurrency(){
	var req = {};
	var res;
	req.action = 'get-currency';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status==1) {
			currency=res.currency[0].currency;//'<i class="fa fa-rupee"></i>';
		}
		loadCourseDetails();
	});
}
function getsubjectDetails() {
    var req = {};
    req.action = 'loadSubjectDetails';
    req.courseId = getUrlParameter("courseId");
    if (req.courseId != undefined) {
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            if (data.status == 1) {
                var html = "";
                var subject_html = '';
                var professor = '';
                var subjectHeading='';
                for (var i = 0; i < data.subjects.length; i++) {
					subject_html += '<div class="row"><div class="col-md-2 about"><br>'
						+ '<img class="img-responsive" style="width:130px;height:130px;margin-left:10px;" alt="No Image" src="' + data.subjects[i].image + '">'
						+ '<div style="text-align: center;"><strong>' + data.subjects[i].name + '</strong></div>'
						+ '<br>'
					+ '</div>'
					+ '<div class="col-md-10 about"><br>'
						+ '<table class="table" style="margin-bottom: 5px;">'
							+ '<tbody>';
					if(data.subjects[i].independentExam == null)
						data.subjects[i].independentExam = 0;
					if(data.subjects[i].independentAssignment == null)
						data.subjects[i].independentAssignment = 0;
                    for (var j = 0; j < data.subjects[i].chapters.length; j++) {
						var examCount;
						var assCount;
						if(data.subjects[i].chapters[j].Exam > 0) {
							examCount = data.subjects[i].chapters[j].Exam + ' Exams';
						}
						else
							examCount = '--';
						if(data.subjects[i].chapters[j].Assignment > 0) {
							assCount = data.subjects[i].chapters[j].Assignment + ' Assignments';
						}
						else
							assCount = '--';
			 			if(data.subjects[i].chapters[j].Exam == null)
							data.subjects[i].chapters[j].Exam = 0; 
						if(data.subjects[i].chapters[j].Assignment == null)
							data.subjects[i].chapters[j].Assignment = 0;
						subject_html += '<tr style="display: none;" class="chapter">'
									+ '<td style="width: 47%;">Chapter ' + (j+1) + ': ' + data.subjects[i].chapters[j].name + '</td>';
						if(data.subjects[i].chapters[j].demo == 1)
							subject_html += '<td style="width: 13%;"><a class="btn btn-info btn-custom btn-xs viewDemo">Free Demo</a></td>';
						else
							subject_html += '<td style="width: 13%;"></td>';
						subject_html += '<td style="width: 20%;text-align: center;"><span class="soup">' + examCount + '</span></td>'
									+ '<td style="width: 20%;text-align: center;"><span class="soup">' + assCount + '</span></td>'
								+ '</tr>';
					}
					var examCount;
					var assCount;
					if(data.subjects[i].independentExam > 0)
						examCount = data.subjects[i].independentExam + ' Exams';
					else
						examCount = '--';
					if(data.subjects[i].independentAssignment > 0)
						assCount = data.subjects[i].independentAssignment + ' Assignments';
					else
						assCount = '--';
					if(!(examCount == '--' && assCount == '--')) {
						subject_html += '<tr class="independent">'
										+ '<td style="width: 47%;">Independent Exam/Assignment</td>'
										+ '<td style="width: 13%;"></td>'
										+ '<td style="width: 20%;text-align: center;"><span class="soup">' + examCount + '</span></td>'
										+ '<td style="width: 20%;text-align: center;"><span class="soup">' + assCount + '</span></td>'
									+ '</tr>'
					}
					subject_html += '</tbody>'
						+ '</table>'
						+ '<button class="btn btn-info btn-xs viewMoreChap" data-hidden="0" style="display: none;">View More</button>'
					+ '</div></div>';
                    /*subject_html += ''
                            + '<div class="col-md-12 about">'
                            + '<div class="custom-bg">'
                            + '<h5 id="subject-name" class="pull-left">' + data.subjects[i].name + '</h5>'
                            + '<a class="btn btn-xs btn-danger pull-right" href="#" style="margin: 6px 0px 0px;">View All Chapter</a>'
                            + '</div>'
                            + '</div>'
                            + '<div class="col-md-2 about"><br/>'
                            + ' <img class="img-responsive" style="width:130px;height:130px;margin-left:10px;" alt="No Image" src="' + data.subjects[i].image + '"><br/>'
                            + '</div>'
                            + '<div class="col-md-10 custom-font about"><br/>'
                            + '<div id="accordion" class="panel-group mbot20">';
                    var chapter_html = '';*/
                        /*var exam = data.subjects[i].chapters[j].Exam == null ? 0 : data.subjects[i].chapters[j].Exam;
                        var assignment = data.subjects[i].chapters[j].Assignment == null ? 0 : data.subjects[i].chapters[j].Assignment;

                        chapter_html += '<div class="panel panel-default ">'
                                + '<div class="panel-heading">'
                                + '<ul class="custom-list">'
                                + '<li>'
                                + ' <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">'
                                + ' <i id="heading" class="fa fa-plus"></i> Chapter ' + j + 1 + '. ' + data.subjects[i].chapters[j].name + '</a>'
                                + '</li>'
                                + '<li>'
                                + '<a href="#">Exam ' + exam + '</a></li>'
                                + '<li><a href="#">Assignment ' + assignment + '</a></li>'
                                + '<li class="text-right"><a class="btn btn-xs btn-info">Preview</a></li>'
                                + '</ul>'
                                + '</div>'
                                + '<div class="panel-collapse collapse in" id="collapseOne" style="height: auto;">'
                                + '<div class="panel-body" style="padding-top: 0px; border: medium none;">'
                                + ' <table id="tbl_heading" class="table">'
                                + '<tbody>';

                        var content_html = '';
                        for (var k = 0; k < data.subjects[i].chapters[j].content.length; k++) {
                            content_html += '<tr>'
                                    + '<td> ' + k + 1 + '. ' + data.subjects[i].chapters[j].content[k].title + '</td>'

                                    + '</tr>';
                        }
                        chapter_html = chapter_html + content_html + '</tbody>'
                                + '</table>'
                                + '</div>'
                                + '</div>'
                                + '</div>';
                    }
                    for (var l = 0; l < data.subjects[i].professors.length; l++) {
                        professors += data.subjects[i].professors[l].firstName + ' ' + data.subjects[i].professors[l].lastName + ', ';
                    }
                      subject_html = subject_html + chapter_html
                            + '</div>'
                            + '</div>'
                            + '';*/
                }
                $('#course-append').append(subject_html);
				$.each($('#course-append table'), function() {
					$(this).find('.soup').hide();
					$.each($(this).find('.soup'), function() {
						if($(this).text() != '--') {
							$(this).parents('table').find('.soup').show();
							return;
						}
					});
					$(this).find('tr.chapter:eq(0), tr.chapter:eq(1)').show();
					if($(this).find('tr.chapter').length > 2)
						$(this).parents('.about').find('.viewMoreChap').show();
					$('.viewMoreChap').on('click', function() {
						if($(this).attr('data-hidden') == 0) {
							$(this).parents('.about').find('table').find('tr').show();
							$(this).text('Hide');
							$(this).attr('data-hidden', '1');
						}
						else {
							$(this).parents('.about').find('table').find('tr').hide();
							$(this).parents('.about').find('table').find('tr.chapter:eq(0), tr.chapter:eq(1)').show();
							$(this).parents('.about').find('table').find('tr.independent').show();
							$(this).text('View More');
							$(this).attr('data-hidden', '0');
						}
					});
				});
            }
            $('#professors').html(professors);

        });
    }
    else
        alert("No Subject Found Details");
}

function loadLicenseDetails(){
	var req = {};
	var res;
	req.action = 'fetch-applicable-licenses-FrontPage';
	req.courseId=courseId;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status==1){
				var html='';
				var pricehtml='';
				var displayCurrency='<i class="fa fa-dollar"></i>';
				$.each(res.savedLicenses, function( i, LicenseI ) {
					if(currency==2){
						LicenseI.priceUSD=LicenseI.priceINR;
						displayCurrency='<i class="fa fa-rupee"></i>';
					}
					html += '<li>'
							+ '<a class="licenseOption" data-license="' + LicenseI.licensingType + '">'
								+ '<span><i class="fa fa-calendar text-danger"></i> ' + displayCurrency + '' + LicenseI.priceUSD + '</br>'
								+ license(LicenseI.licensingType) + '</span>'
							+ '</a>'
						+ '</li>'
					/*html+='<option value='+LicenseI.licensingType+'>'+license(LicenseI.licensingType)+'</option>';
					pricehtml+='<h4 class="label text-danger" >'+license(LicenseI.licensingType)+' Price</h4> <h4 class="text-danger">'+LicenseI.priceUSD+' '+displayCurrency+'</h4>';*/
				});
			}
             $('#licenseType').append(html);
			 $('a.licenseOption').on('click', function() {
				 $('a.licenseOption').removeClass('selectedOption');
				 $(this).addClass('selectedOption');
			 });
         });
        function license(licensingType){
                    var text='';
                    switch (licensingType){
                        case '1':text='One Year Licensing';break;
                        case '2':text='One Time Transfer';break;
                        case '3':text='Select IP Transfer';break;
                        case '4':text='Commission based licensing';break;
                    }
                    return text;
                }           
             
}