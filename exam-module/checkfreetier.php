<?php
	require_once "html-template/cookieCheck.php";
	require_once "../api/Vendor/ArcaneMind/Api.php";
	require_once "../api/Vendor/ArcaneMind/AccessApi.php";
	//$access = AccessApi::checkAccess();
	require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Results</title>
	<?php include 'html-template/general/admin-header.html.php'; ?>
	<link href="../admin/assets/morris.js-0.4.3/morris.css" rel="stylesheet" />
	 <link id="theme-style" rel="stylesheet" href="css/style_s.css">
	  

</head>
<body>

	
		<!--header start-->
			<?php include 'html-template/general/header.html.php'; ?>
		<!--header end-->
		
	<section class="wrapper panel bgwhite"><div class="alert alert-danger fade in radius-bordered alert-shadowed freetierMsgBox panel-body" >	 <a href="#" class="close" data-dismiss="alert">&times;</a>
				            
				             <h4> You Cannot view the Result of the exam. </h4> 
				       </div>
		
		 <div class="row panel-body color2">
                    <div class="col-md-5">
                        <div class="custom-heading">
                            <h2>Exam Result will have </h2>
                        </div><!-- .custom-heading end -->

                        <ul class="fa-ul large-icons" id="section1">
                            <li>
                                <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                    <i class="fa fa-check-circle"></i>
                                </div>
                                <div class="li-content ">
                                    <h3>What it is?</h3>

                                    <p > Graph contains class avergae ,class highest and your score                                       
										                                     </p>
                                </div><!-- .li-content end -->
                            </li>

                            <li>
                                <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                    <i class="fa fa-check-circle"></i>
                                </div>

                                <div class="li-content animated fadeInUp">
                                    <h3>How it will help you?</h3>

                                    <p class="animated fadeInUp" >
                                       You can get your performance w.r.t to class over time.
                                    </p>
                                </div><!-- .li-content end -->
                            </li>

                            <li>
                                <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                    <i class="fa fa-check-circle"></i>
                                </div>

                                <div class="li-content animated fadeInUp">
                                    <h3>What our students says?</h3>

                                    <p class="animated fadeInUp">
                                        80% students says that it has helped them increase their performance 
                                    </p>
                                </div><!-- .li-content end -->
                            </li>
                        </ul><!-- .fa-ul .fa-ul-large end -->
                    </div><!-- .col-md-5 end -->

                    <div class="col-md-7 triggerAnimation animated padTop50" data-animate="fadeInRight">
                        <img src="img/section3.png" alt="trucking"/>
                    </div><!-- .col-md-7 end -->
                </div><!-- .row end -->


			 <div class="row panel-body color1 ">
		 			<div class="col-md-8 triggerAnimation animated" data-animate="fadeInRight">
                        <img src="img/section4.png" alt="trucking"/>
                    </div><!-- .col-md-7 end -->
                   <div class="col-md-4 padTop25 dispnone" id="section2" >
                     
                        <ul class="fa-ul large-icons" >
                            <li>
                                <div class="icon-container " data-animate="zoomIn">
                                    <i class="fa fa-check-circle"></i>
                                </div>

                                <div class="li-content">
                                    <h3>What it is?</h3>

                                    <p>This section contains answers to the all Questions asked in exam                                     
										                                     </p>
                                </div><!-- .li-content end -->
                            </li>

                            <li>
                                <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                    <i class="fa fa-check-circle"></i>
                                </div>

                                <div class="li-content">
                                    <h3>How it will help you?</h3>

                                    <p>
                                      Correct answer with proper explaination is given in this section. Also stating that your answer is correct or not.
                                    </p>
                                </div><!-- .li-content end -->
                            </li>

                            <li>
                                <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                    <i class="fa fa-check-circle"></i>
                                </div>

                                <div class="li-content">
                                    <h3>What our students says?</h3>

                                    <p>
                                        99% students says explanations are quite good and they gets where they had made mistakes.
                                    </p>
                                </div><!-- .li-content end -->
                            </li>
                        </ul><!-- .fa-ul .fa-ul-large end -->
                    </div><!-- .col-md-5 end -->

                    
                </div><!-- .row end -->
				
				
				  <div class="row panel-body color2">
                    <div class="col-md-6 padT50 ">
                       <ul class="fa-ul   large-icons" id="section3" >
                            <li>
                                <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                    <i class="fa fa-check-circle"></i>
                                </div>

                                <div class="li-content">
                                    <h3>What it is?</h3>

                                    <p> Section conatins Analytics of particualar question                                       
										                                     </p>
                                </div><!-- .li-content end -->
                            </li>

                            <li>
                                <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                    <i class="fa fa-check-circle"></i>
                                </div>

                                <div class="li-content">
                                    <h3>How it will help you?</h3>

                                    <p>
                                       You wil get to know how much percenatge of students attempted wrong, how much did correclty.Also you will get the toper's time ,average time and time spent by you in this question.
                                    </p>
                                </div><!-- .li-content end -->
                            </li>

                            <li>
                                <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                    <i class="fa fa-check-circle"></i>
                                </div>

                                <div class="li-content">
                                    <h3>What our students says? </h3>

                                    <p>
                                        "It gives me confidence whenever i see time taken by me for particluar question is less than topper " - Jitu
                                    </p>
                                </div><!-- .li-content end -->
                            </li>
                        </ul><!-- .fa-ul .fa-ul-large end -->
                    </div><!-- .col-md-5 end -->

                    <div class="col-md-6 triggerAnimation animated" data-animate="fadeInRight">
                        <img src="img/section5.png" alt="trucking"/>
                    </div><!-- .col-md-7 end -->
                </div><!-- .row end -->
				
				
				 <div class="row panel-body color1">
		 			<div class="col-md-7 padBottom30 triggerAnimation animated" data-animate="fadeInRight">
                        <img src="img/section2.png" alt="trucking"/>
                    </div><!-- .col-md-7 end -->
                   <div class="col-md-4 ">
                     
                        <ul class="fa-ul large-icons"   id="section4">
                            <li>
                                <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                    <i class="fa fa-check-circle"></i>
                                </div>

                                <div class="li-content">
                                    <h3>What it is?</h3>

                                    <p>This section contains Summary of an attempt for exam                                     
										                                     </p>
                                </div><!-- .li-content end -->
                            </li>

                            <li>
                                <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                    <i class="fa fa-check-circle"></i>
                                </div>

                                <div class="li-content">
                                    <h3>How it will help you?</h3>

                                    <p >
                                    This section overall rank ,time taken ,start date, end date for an exam attempt
                                    </p>
                                </div><!-- .li-content end -->
                            </li>

                            <li>
                                <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                    <i class="fa fa-check-circle"></i>
                                </div>

                                <div class="li-content">
                                    <h3>What our students says?</h3>

                                    <p>
                                        99% students says explanations are quite good and they gets where they had made mistakes.
                                    </p>
                                </div><!-- .li-content end -->
                            </li>
                        </ul><!-- .fa-ul .fa-ul-large end -->
                    </div><!-- .col-md-5 end -->

                    
                </div><!-- .row end -->
				
				
				
				  <div class="row panel-body color2">
                    <div class="col-md-5 padTop25">
                        
                        <ul class="fa-ul large-icons " id ="waypoint">
                            <li>
                                <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                    <i class="fa fa-check-circle"></i>
                                </div>

                                <div class="li-content">
                                    <h3>What it is?</h3>

                                    <p> Graph contains the percentage distribution of score of students                                        
										                                     </p>
                                </div><!-- .li-content end -->
                            </li>

                            <li>
                                <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                    <i class="fa fa-check-circle"></i>
                                </div>

                                <div class="li-content">
                                    <h3>How it will help you?</h3>

                                    <p>
                                       You can get your percenatge wise distribution of class.
                                    </p>
                                </div><!-- .li-content end -->
                            </li>

                            <li>
                                <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                    <i class="fa fa-check-circle"></i>
                                </div>

                                <div class="li-content">
                                    <h3>What our students says?</h3>

                                    <p>
                                        80% students says that it has helped them increase their performance 
                                    </p>
                                </div><!-- .li-content end -->
                            </li>
                        </ul><!-- .fa-ul .fa-ul-large end -->
                    </div><!-- .col-md-5 end -->

                    <div class="col-md-7 triggerAnimation animated padTop25" data-animate="fadeInRight">
                        <img src="img/section1.png" alt="trucking"/>
                    </div><!-- .col-md-7 end -->
                </div><!-- .row end -->
				
			  <div class="row panel-body ">		
				 <div class="wanttopurcase"> <h2>Want to See the Exam Results ?</h2></div>
				 <div class="wanttopurcaseOuter">
		
		 <div class="btnpurcase"><button type="button" class="btn btn-success btn-md"><i class="fa fa-shopping-cart"></i>
			                                          Pay for this exam result only
			                                      </button>
		<button  type="button" class="btn btn-success allexam"><i class="fa fa-shopping-cart"></i>
			                                          Pay for all exams result
			                                      </button></div>
		 
		 
		  </div>	
			</div>																			
	</section>
    <?php //include "html-template/pageCheckFooter.php"; ?>
	<?php include 'html-template/general/admin-footer.html.php'; ?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.5/waypoints.min.js"></script>

	<script>
	   // ANIMATED CONTENT
        if ($(".animated")[0]) {
            jQuery('.animated').css('opacity', '0');
        }

        var currentRow = -1;
        var counter = 1;

        $('.triggerAnimation').waypoint(function () {
            var $this = $(this);
            var rowIndex = $('.row').index($(this).closest('.row'));
            if (rowIndex !== currentRow) {
                currentRow = rowIndex;
                $('.row').eq(rowIndex).find('.triggerAnimation').each(function (i, val) {
                    var element = $(this);
                    setTimeout(function () {
                        var animation = element.attr('data-animate');
                        element.css('opacity', '1');
                        element.addClass("animated " + animation);
                    }, (i * 250));
                });

            }

            //counter++;

        },
                {
                    offset: '70%',
                    triggerOnce: true
                }
        );
		$('#waypoint').waypoint(function() {
			$("#waypoint").removeClass("hidden");
			$("#waypoint").addClass("animated fadeInUp");
			}, {
			offset: '90%'
		});
		
		$('#section1').waypoint(function() {
			$("#section1").removeClass("hidden");
			$("#section1").addClass("animated fadeInUp");
			}, {
			offset: '90%'
		});
		
		$('#section2').waypoint(function() {
			$("#section2").removeClass("hidden");
			$("#section2").addClass("animated fadeInUp");
			}, {
			offset: '90%'
		});
		$('#section3').waypoint(function() {
			$("#section3").removeClass("hidden");
			$("#section3").addClass("animated fadeInUp");
			}, {
			offset: '90%'
		});
		$('#section4').waypoint(function() {
			$("#section4").removeClass("hidden");
			$("#section4").addClass("animated fadeInUp");
			}, {
			offset: '90%'
		});
	
		
	</script>
	 
		

	
  </body>
  	
</html>
