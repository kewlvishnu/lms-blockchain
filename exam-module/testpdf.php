<!DOCTYPE html>
<html lang="en">
  <head>
	<title>View Chapter</title>
	<?php include 'html-template/general/admin-header.html.php'; ?>
	<link rel="stylesheet" type="text/css" href="flowplayer/skin/minimalist.css" />
	<link rel="stylesheet" href="mediaelement/build/mediaelementplayer.min.css" />
        <script language="JavaScript">
                        //script to disable f12 key
                        document.onkeypress = function (event) {
                                event = (event || window.event);
                                if (event.keyCode == 123 || event.keyCode == 18) {
                    //alert('No F-12');
                    return false;
                }
            }
            document.onmousedown = function (event) {
                event = (event || window.event);
                if (event.keyCode == 123 || event.keyCode == 18) {
                    //alert('No F-keys');
                    return false;
                }
            }
            document.onkeydown = function (event) {
                event = (event || window.event);
                if (event.keyCode == 123 || event.keyCode == 18) {
                    //alert('No F-keys');
                    return false;
                }
            }
         //script to disable right mouse click key
            function clickIE4() {
                if (event.button == 2) {
                    return false;
                }
            }

            function clickNS4(e) {
                if (document.layers || document.getElementById && !document.all) {
                    if (e.which == 2 || e.which == 3) {
                        return false;
                    }
                }
            }

            if (document.layers) {
                document.captureEvents(Event.MOUSEDOWN);
                document.onmousedown = clickNS4;
            }
            else if (document.all && !document.getElementById) {
                document.onmousedown = clickIE4;
            }
            document.oncontextmenu = new Function("return false;");

            //disabling ctrl keys
            var isCtrl = false;
            document.onkeyup = function (e) {
                if (e.which == 17)
                    isCtrl = false;
            }

            document.onkeydown = function (e) {
                if (e.which == 17)
                    isCtrl = true;
                if (((e.which == 85) || (e.which == 117) || (e.which == 65) || (e.which == 97) || (e.which == 67) || (e.which == 99)) && isCtrl == true) {
                    //alert('Keyboard shortcuts are cool!');
                    return false;
                }
            }
        </script>
  </head>

  <body class="custom-container">

  <section id="container">
  <!--header start-->
	  <header>
          <div class="sidebar-toggle-box custom-toggle-box">
              <div data-placement="right" class="fa fa-plus"></div>
          </div>
      </header>

	  <!--header end-->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse custom-sidebar ">		  
              <!-- sidebar menu start-->
              <ul class="sidebar-menu custom-sidebar-menu" id="nav-accordion">				
				<div class="panel-group m-bot20 nav-accordion" id="accordion">
					
				</div>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      <!--main content start-->
	<section id="main-content">
		<div class="panel-body custom-course"  >
			<div class="panel-body custom-brad" id="breadcrumb">
				<span class="btc">Course Name</span>  &nbsp; /&nbsp; 
				<span class="btc">Subject</span> &nbsp; / &nbsp;
				<span class="btc">Introduction to marketing</span>
			</div>
			<section class="panel" id="content-view">
				<header class="panel-heading" id="contentHeading">
				</header>
				<div class="panel-body custom-content-part">
					
				</div>
			</section>
			<div class="panel-body" style="padding-top: 0px;">
				<button type="button" class="pull-left btn btn-success btn-xs" id="previous"> <i class="fa fa-angle-left"></i><i class="fa fa-angle-left"></i> Previous</button><button type="button" class="btn btn-success btn-xs pull-right" id="next">Next <i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></button>  
			</div>
		</div>
	</section>
	<section id="block-everything">
	</section>
      <!--main content end-->
      <!--footer start
      <footer class="site-footer">
          <div class="text-center">
              2013 &copy; FlatLab by VectorLab.
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>
	<?php include 'html-template/general/admin-footer.html.php'; ?>
	<script type="text/javascript" src="flowplayer/flowplayer.min.js"></script>
	<script src="mediaelement/build/mediaelement-and-player.min.js"></script>
	<script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
	<script src="js/custom/testpdf.js" type="text/javascript"></script>
  </body>
</html>
