<?php
    require_once "html-template/cookieCheck.php";
	//code to update any paused exam or assignment
	// Loading Zend
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
	$access = AccessApi::checkAccess();
        // 
        // 
//		include '../api/Vendor/Zend/Loader/AutoloaderFactory.php';
//		Zend\Loader\AutoloaderFactory::factory(array(
//				'Zend\Loader\StandardAutoloader' => array(
//						'autoregister_zf' => true,
//						'db' => 'Zend\Db\Sql'
//				)
//		));
//	require_once '../api/Vendor/ArcaneMind/StudentExam.php';
//	$se = new StudentExam();
//	$se->completeAllExams();
require_once "html-template/pageCheck.php";
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Chapters</title>
        <?php include "html-template/general/admin-header.html.php"; ?>
        <link rel="stylesheet" href="../admin/assets/bootstrap-rating/bootstrap-rating.css">
    </head>
    <body>
        <section id="container" class="">
            <!--header start-->
          <?php include "html-template/general/header.html.php"; ?>
	 <?php include 'html-template/general/sidebar.html.php'; ?>
            <!--main content start-->
            <section>
                <section class="wrapper site-min-height">
                    <!-- page start-->
                    <div class="row">
                          <div class="col-md-12">                  
                            <?php include 'html-template/chapter/chapter.html.php'; ?>
                        </div> 
                    </div>			
                    <!-- page end-->
                </section>
            </section>
            <!--main content end-->
            <?php include "html-template/general/footer.html.php"; ?>
            <?php include "html-template/pageCheckFooter.php"; ?>
            <?php include "html-template/chapter/review-moadal.php"; ?>
			<?php include "html-template/chapter/syllabus-moadal.php"; ?>
            <?php include "html-template/exam/subjective-checked-modal.php"; ?>
			
        </section>
        <?php include "html-template/general/admin-footer.html.php"; ?>
        <?php include "html-template/profile/image-upload-modals.html.php"; ?>
        <script src="../admin/js/custom/scripts.js" type="text/javascript"></script>
        <script src="../admin/assets/bootstrap-rating/bootstrap-rating.min.js" type="text/javascript"></script>
        <script src="js/custom/chapter.js" type="text/javascript"></script>
    </body>
</html>
