<?php

require_once "html-template/cookieCheck.php";
require_once "../api/Vendor/ArcaneMind/Api.php";
require_once "../api/Vendor/ArcaneMind/AccessApi.php";
$access = AccessApi::checkAccess();
require_once "html-template/pageCheck.php";

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <link rel="shortcut icon" href="img/favicon.png">
        <title>My Course</title>
        <?php include "html-template/general/admin-header.html.php"; ?>
        <link href="../admin/assets/morris.js-0.4.3/morris.css" rel="stylesheet" />
    </head>
    <body>
        <section id="container" class="">
            <?php include "html-template/general/header.html.php"; ?>
			<?php include 'html-template/general/sidebar.html.php'; ?>
             <!--main content start-->
            <section>
                <section class="wrapper site-min-height">
                    <!-- page start-->
                    <div class="row">    
                        <div class="col-md-12">
                        <?php include "html-template/subjective/showResult.html.php"; ?>
                        </div>	
                    </div>
                    <!-- page end-->
                </section>
            </section>
            <!--main content end-->
            <!--footer start-->
            <?php include "html-template/general/footer.html.php"; ?>
            <?php include "html-template/pageCheckFooter.php"; ?>
            <!--footer end-->
        </section>
        <?php include "html-template/course/payment-modal.html.php"; ?>
        <?php include "html-template/general/admin-footer.html.php"; ?>
        <?php include "html-template/profile/image-upload-modals.html.php"; ?>
        <script src="https://code.highcharts.com/stock/highstock.js"></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/stock/modules/heatmap.js"></script>
        <script src="https://code.highcharts.com/stock/modules/data.js"></script>
        <script src="../admin/assets/morris.js-0.4.3/morris.min.js" type="text/javascript"></script>
        <script src="../admin/assets/morris.js-0.4.3/raphael-min.js" type="text/javascript"></script>
        <script src="../admin/js/custom/scripts.js" type="text/javascript"></script>
        <script src="js/custom/showSubjectiveResult.js" type="text/javascript"></script>
    </body>
</html>