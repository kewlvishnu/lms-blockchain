<?php
include 'html-template/signup.header.php';
?>
<div class="contact-form form-step-1 form_bg">
	<div>
		<h4 id="verifyError">Please Wait while we try to activate your account..</h4>
	</div>
</div>
<?php include 'html-template/footer.php'; ?>
<script src="scripts/verifyInvitation_mail.js"></script>		
</body>
</html>