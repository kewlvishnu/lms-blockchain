<?php
    require_once "html-template/cookieCheck.php";
    //code to update any paused exam or assignment
    // Loading Zend
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();

    require_once __DIR__.'/../api/Vendor/ArcaneMind/StudentExam.php';
    $se = new StudentExam();
    $se->completeAllExams();
    
    //to track opened courses
    require_once __DIR__.'/../api/Vendor/ArcaneMind/Course.php';
    $req = new stdClass();
    @session_start();
    $req->userId = $_SESSION['userId'];
    $req->courseId = $_GET['courseId'];
    $c = new Course();
    $c->updateOpenedCourses($req);
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>My Course Inner</title>
        <!-- Bootstrap core CSS   -->
        <?php include "html-template/general/admin-header.html.php"; ?>
    </head>
    <body>
        <section id="container" class="">
            <!--header start-->
            <?php include "html-template/general/header.html.php"; ?>
            <?php include 'html-template/general/sidebar.html.php'; ?>
            <!--sidebar end-->
            <!--main content start-->
            <section>
                <section class="wrapper site-min-height">
                    <!-- page start-->
                    <div class="row">
                        <div class="col-lg-12">
                            <!--breadcrumbs start -->
<!--                            <ul class="breadcrumb" id="main-content">
                                <li><a href=""><i class="fa fa-home"></i> Student</a></li>
                                <li><a href="">My Course</a></li>
                                <li><a href="">Content</a></li>
                            </ul>-->
                            <!--breadcrumbs end -->
                        </div>
                        <div class="col-md-12">                  
                            <?php include 'html-template/exam/MyExam.html.php'; ?>
                        </div>          
                    </div>          
                    <!-- page end-->
                </section>
            </section>
            <!--main content end-->
            <!--footer start-->
            <?php include "html-template/general/footer.html.php"; ?>
            <?php include "html-template/pageCheckFooter.php"; ?>
            <!--footer end-->
			<?php include "html-template/course/course-doc-modal.php"; ?>
            <?php include "html-template/exam/subjective-checked-modal.php"; ?>
        </section>
        <?php include "html-template/general/admin-footer.html.php"; ?>
        <?php include "html-template/profile/image-upload-modals.html.php"; ?>
        <script src="../admin/js/custom/scripts.js" type="text/javascript"></script>
        <script src="js/custom/MyExam.js" type="text/javascript"></script>
    </body>
</html>
