<?php
    require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
	require_once "../api/Vendor/ArcaneMind/AccessApi.php";
	AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Notifications</title>
        <?php include "html-template/general/admin-header.html.php"; ?>
		<link rel="stylesheet" href="../admin/css/tasks.css">

       </head>
    <body>

        <section id="container" class="">
            <?php include "html-template/general/header.html.php"; ?>
			<?php include "html-template/general/sidebar.html.php"; ?>
            <!--main content start-->
            <section>
                <section class="wrapper site-min-height">
                    <!-- page start-->
                    <div class="row">
                        <div class="col-md-12">
                            <?php include "html-template/payment/paymentDetails.html.php"; ?> 
                        </div>
                    </div>
                    <!-- page end-->
                </section>
            </section>
            <?php include "html-template/general/footer.html.php"; ?>
            <?php include "html-template/pageCheckFooter.php"; ?>
        </section>
		<?php include 'html-template/general/admin-footer.html.php'; ?>
		<script src="js/custom/paymentDetails.js" type="text/javascript"></script>
    </body>
</html>
