﻿<?php
	require_once "html-template/cookieCheck.php";
	//code to update any paused exam or assignment
	// Loading Zend
    require_once "../api/Vendor/ArcaneMind/Api.php";
	require_once "../api/Vendor/ArcaneMind/AccessApi.php";
	$access = AccessApi::checkAccess();
	/*include '../api/Vendor/Zend/Loader/AutoloaderFactory.php';
	Zend\Loader\AutoloaderFactory::factory(array(
			'Zend\Loader\StandardAutoloader' => array(
					'autoregister_zf' => true,
					'db' => 'Zend\Db\Sql'
			)
	));
	require_once '../api/Vendor/ArcaneMind/StudentExam.php';
	$se = new StudentExam();
	$se->completeAllExams();*/
	require_once "html-template/pageCheck.php";
	//session_start();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		
		<title>Exam Module</title>
		<?php include "html-template/general/admin-header.html.php"; ?>
		<?php include 'html-template/student-exam-head.html.php'; ?>
        <meta charset="utf-8">
        <script language="JavaScript">
			//script to disable f12 key
			document.onkeypress = function (event) {
				event = (event || window.event);
				if (event.keyCode == 123 || event.keyCode == 18) {
					//alert('No F-12');
					return false;
				}
			}
			document.onmousedown = function (event) {
				event = (event || window.event);
				if (event.keyCode == 123 || event.keyCode == 18) {
					//alert('No F-keys');
					return false;
				}
			}
			document.onkeydown = function (event) {
				event = (event || window.event);
				if (event.keyCode == 123 || event.keyCode == 18) {
					//alert('No F-keys');
					return false;
				}
			}

			//script to disable right mouse click key
			function clickIE4() {
				if (event.button == 2) {
					return false;
				}
			}

			function clickNS4(e) {
				if (document.layers || document.getElementById&&!document.all) {
					if (e.which==2||e.which==3) {
						return false;
					}
				}
			}

			if (document.layers){
				document.captureEvents(Event.MOUSEDOWN);
				document.onmousedown = clickNS4;
			}
			else if (document.all && !document.getElementById){
				document.onmousedown = clickIE4;
			}
			document.oncontextmenu = new Function("return false;");

			//disabling ctrl keys
			var isCtrl = false;
			document.onkeyup=function(e) {
				if(e.which == 17)
					isCtrl = false;
			}

			document.onkeydown=function(e) {
				if(e.which == 17)
					isCtrl = true;
				if(((e.which == 85) || (e.which == 117) || (e.which == 65) || (e.which == 97) || (e.which == 67) || (e.which == 99)) && isCtrl == true) {
					//alert('Keyboard shortcuts are cool!');
					return false;
				}
				if (e.which == 123 || e.which == 18) {
					//alert('No F-keys');
					return false;
				}
			}
		</script>
	</head>
	<body>
		<div class="ajax-loader" id="loader"><img src="img/ajax-loader.gif" alt=""></div>
		<?php include "html-template/general/header.html.php"; ?>
		<section class="wrapper subjective-module" id="container">
			<div id="instructions" class="instruction-box">
				<div class="row">
					<div class="col-md-12">
						<table class="table table-bordered">
							<tr>
								<th colspan="4" class="text-center"><h2>Instructions</h2></th>
							</tr>
							<tr>
								<th colspan="4" class="text-center">Subjective Questions</th>
							</tr>
							<tr>
								<td width="25%" rowspan="3">
								<td width="25%">Total Questions</td>
								<td width="25%" id="totalQuestions"></td>
								<td width="25%" rowspan="3">
							</tr>
							<tr>
								<td>Total Marks</td>
								<td id="totalMarks"></td>
							</tr>
							<tr>
								<td>Total Time</td>
								<td id="totalExamTime"></td>
							</tr>
							<tr>
								<th colspan="4" class="text-center">Guidelines</th>
							</tr>
							<tr>
								<td colspan="4">You can attempt this Exam <span id="totalAttempts">10</span> times and your highest score will be considered towards your rank.</td>
							</tr>
							<tr>
								<td colspan="4">
									<p>You are suggested to use your time judiciously</p>
									<p>You will be able to leave the exam before the time completes.</p>
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<p><strong>If power goes off or the Browser Closes, Answers will be lost.</strong></p>
									<p><strong>Time would continue to tick. You will lose the time during which interruption happens.</strong></p>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="btn btn-primary btn-lg" id="btnStartExam" disabled>Start Subjective Exam</div>
					</div>
				</div>
			</div>
			<div id="questions" class="hide questions-box">
				<!--<i id="saveLoader" class="ajax-loader hide"><img src="<?php //echo $sitePath."student/img/ajax-loader1.gif"; ?>" alt=""></i>-->
				<h2 class="text-center" id="examName"></h2>
				<div class="row" id="mainExam">
					<div class="col-md-8">
						<table class="table table-bordered table-question" id="tblQuestion" data-question="0">
							<tr>
								<th id="jsQNo"></th>
							</tr>
							<tr>
								<td class="sub-question">
									<div class="question-detail" id="txtQuestion"></div>
								</td>
							</tr>
							<tr class="js-qtype js-question hide">
								<td class="js-form-con">
									<!-- <div class="form-group">
										<label for="">Answer :</label>
										<textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
									</div>
									<div class="form-group">OR</div>
									<form class="frm-upload" action="<?php //echo $sitePath.'api/files1.php'; ?>" method="post">
										<button type="button" class="btn btn-warning btn-upload">Upload PDF</button>
										<input type="file" name="subjective-upload" class="js-uploader hide">
										<p class="js-file-path"></p>
									</form> -->
								</td>
							</tr>
							<tr class="js-qtype js-questiongroup hide">
								<td>
									<!-- <div class="sub-cover">
										<div class="sub-question">
											<p>Which of the following is not allowed in php variable defining?</p>
										</div>
										<div class="sub-answer">
											<div class="form-group">
												<label for="">Answer :</label>
												<textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
											</div>
											<div class="form-group">OR</div>
											<div>
												<button class="btn btn-warning btn-upload">Upload PDF</button>
												<input type="file" class="hide">
											</div>
										</div>
									</div> -->
								</td>
							</tr>
							<tr class="js-qtype js-subQuestions hide">
								<td class="js-form-con">
									<!-- <div class="sub-cover">
										<div class="sub-qno" id="jsSubQNo"></div>
										<div class="sub-question">
											<p>Which of the following is not allowed in php variable defining?</p>
										</div>
										<div class="sub-answer">
											<div class="form-group">
												<label for="">Answer :</label>
												<textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
											</div>
											<div class="form-group">OR</div>
											<div>
												<button class="btn btn-warning btn-upload">Upload PDF</button>
												<input type="file" class="hide">
											</div>
										</div>
									</div>
									<div class="sub-nav">
										<button class="btn btn-info">Previous</button>
										<button class="btn btn-info pull-right">Next</button>
									</div> -->
								</td>
							</tr>
						</table>
					</div>
					<div class="col-md-4">
						<table class="table table-bordered table-legend">
							<tr>
								<th>Exam Timer</th>
							</tr>
							<tr>
								<td>
									<div id="examTimer">
										<span id="timeHour">00</span>:<span id="timeMinute">00</span>:<span id="timeSecond">00</span>
									</div>
								</td>
							</tr>
							<tr>
								<th>Questions Palette</th>
							</tr>
							<tr>
								<td>
									<ul class="list-inline questions-list">
										<!-- <li><button class="btn btn-default btn-unvisited">1</button></li>
										<li><button class="btn btn-default btn-answered">2</button></li>
										<li><button class="btn btn-default btn-marked">3</button></li>
										<li><button class="btn btn-default btn-answeredmarked">4</button></li>
										<li><button class="btn btn-default btn-unanswered">5</button></li> -->
									</ul>
								</td>
							</tr>
							<tr>
								<td>
									<ul class="list-unstyled">
										<li><button class="btn btn-default btn-legend btn-unvisited"></button>Not visited</li>
										<li><button class="btn btn-default btn-legend btn-answered"></button>Answered</li>
										<!-- <li><button class="btn btn-default btn-legend btn-marked"></button>Marked</li>
										<li><button class="btn btn-default btn-legend btn-answeredmarked"></button>Answered &amp; marked</li> -->
										<li><button class="btn btn-default btn-legend btn-unanswered"></button>Not Answered</li>
									</ul>
								</td>
							</tr>
							<!-- <tr>
								<td>
									<div class="row">
										<div class="col-sm-12 text-center">
											<div class="btn-group">
												<a href="javascript:void(0)" class="btn btn-info">Check Instructions</a>
											</div>
										</div>
									</div>
								</td>
							</tr> -->
							<tr>
								<td>
									<div class="row">
										<div class="col-sm-12">
											<a href="javascript:void(0)" class="btn btn-primary js-btn-qnav" id="btnQuestionNavPrev">Previous</a>
											<a href="javascript:void(0)" class="btn btn-primary js-btn-qnav pull-right" id="btnQuestionNavNext"> Next</a>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="row answer-btns">
										<div class="col-sm-12">
											<a href="javascript:void(0)" class="btn btn-success btn-lg" id="btnQuestionSave">Save Answer</a>
											<a href="javascript:void(0)" class="btn btn-success btn-lg pull-right" id="btnSubmitExam">Submit Exam</a>
										</div>
									</div>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div id="timeUp" class="row hide">
					<div class="col-md-12">
						<div class="bigpad">
							<p class="lead">Time's Up</p>
							<p>Submitting your answers, please wait...</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php include 'html-template/student-exam-footer.html.php'; ?>
        <?php include "html-template/pageCheckFooter.php"; ?>
		<script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
		<script src="js/jquery.form.min.js" type="text/javascript"></script>
		<script src="../admin/assets/ckeditor/ckeditor.js"></script>
		<script src="js/custom/subjectiveExamModule.js" type="text/javascript"></script>
	</body>
</html>
