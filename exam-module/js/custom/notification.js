var ApiEndPoint = '../api/index.php';
var details;

$(function() {
	fetchUserDetails();
	fetchNotifications();

	//function to fetch user details for change password page
	function fetchUserDetails() {
		var req = {};
		var res;
		req.action = 'get-student-details-for-changePassword';
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				details = res;
				fillUserDetails(res);
			}
		});
	}

	//function to fill user details
	function fillUserDetails(data) {
		$('.notify.badge').text(data.notiCount);
		$('.fullName').text(data.details.firstName + ' ' + data.details.lastName);
		$('.username').text(data.details.username);
		$('.user-image').attr('src', data.details.profilePic);
		$('#btn_rupee, #btn_dollar').removeClass('btn-white btn-info');
		if(data.details.currency == 1) {
			$('#btn_dollar').addClass('btn-info');
			$('#btn_rupee').addClass('btn-white');
		}
		else if(data.details.currency == 2) {
			$('#btn_dollar').addClass('btn-white');
			$('#btn_rupee').addClass('btn-info');
		}
	}

	//function to fetch notifications
	function fetchNotifications() {
		var req = {};
		var res;
		req.action = 'get-student-notifications';
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				details = res;
				fillNotifications(res);
			}
		});
	}

	//function to fill notifications table
	function fillNotifications(data) {
		var html = '';
		$('#notify-list').find('li').remove();
		for(var i = 0; i < data.sitenotifications.length; i++) {
			var link = '';
			switch(data.sitenotifications[i].roleId) {
				case '1':
					link = '../institute/' + data.sitenotifications[i].userId;
					break;
				case '2':
					link = '../instructor/' + data.sitenotifications[i].userId;
					break;
				case '3':
					link = '../institute/' + data.sitenotifications[i].userId;
					break;
			}
			html += '<li data-id="'+data.sitenotifications[i].id+'">'
					+ '<div class="task-title">'
						+ '<span class="task-title-sp">You have been invited for joinig <strong>'+data.sitenotifications[i].courseName+'</strong> by <a href="'+link+'"><strong>'+data.sitenotifications[i].inviter+'</strong></a></span>'
						+ '<div class="pull-right hidden-phone">'
							+ '<button class="btn btn-success btn-xs accept-button"><i class=" fa fa-check"></i> Accept</button>&emsp;'
							+ '<button class="btn btn-danger btn-xs reject-button"><i class="fa fa-times"></i> Reject</button>'
						+ '</div>'
					+ '</div>'
				+ '</li>';
		}
		for(var i = 0; i < data.chatnotifications.length; i++) {
			var link = '';
			switch(data.chatnotifications[i].roleId) {
				case '1':
					link = '../institute/' + data.chatnotifications[i].userId;
					link = "<a href='"+link+"' target='_blank'><strong>"+data.chatnotifications[i].senderName+"</strong></a>";
					break;
				case '2':
					link = '../instructor/' + data.chatnotifications[i].userId;
					link = "<a href='"+link+"' target='_blank'><strong>"+data.chatnotifications[i].senderName+"</strong></a>";
					break;
				case '3':
					link = '../institute/' + data.chatnotifications[i].userId;
					link = "<a href='"+link+"' target='_blank'><strong>"+data.chatnotifications[i].senderName+"</strong></a>";
					break;
				case '4':
					link = "<strong>"+data.chatnotifications[i].senderName+"</strong>";
					break;
			}
			var message = "";
			switch(data.chatnotifications[i].type) {
				case '0':
					if (data.chatnotifications[i].room_type == "public") {
						message = '<span class="task-title-sp"> '+link+' just started conversation in <strong>Subject : '+data.chatnotifications[i].courseName+'</strong> of <strong>Course : '+data.chatnotifications[i].subjectName+'</strong></span>';
					} else {
						message = '<span class="task-title-sp"> '+link+' just started conversation with you in <strong>Subject : '+data.chatnotifications[i].courseName+'</strong> of <strong>Course : '+data.chatnotifications[i].subjectName+'</strong></span>';
					}
					break;
				case '1':
					if (data.chatnotifications[i].room_type == "public") {
						message = '<span class="task-title-sp"> '+link+' just sent a message in <strong>Subject : '+data.chatnotifications[i].courseName+'</strong> of <strong>Course : '+data.chatnotifications[i].subjectName+'</strong></span>';
					} else {
						message = '<span class="task-title-sp"> '+link+' just messaged you in <strong>Subject : '+data.chatnotifications[i].courseName+'</strong> of <strong>Course : '+data.chatnotifications[i].subjectName+'</strong></span>';
					}
					break;
			}
			html += '<li data-id="'+data.chatnotifications[i].id+'">'
					+ '<div class="task-title">'
						+ message
						+ '<div class="pull-right hidden-phone">'
							+ '<button class="btn btn-success btn-xs chat-button" data-chat="1"><i class=" fa fa-check"></i> Start Chat</button>&emsp;'
							+ '<button class="btn btn-danger btn-xs chat-button" data-chat="0"><i class="fa fa-times"></i> Mark read</button>'
						+ '</div>'
					+ '</div>'
				+ '</li>';
		}
		if(html == '')
			html = 'No new notification found.';
		$('#notify-list').append(html);

		//adding event handlers for accept button
		$('.chat-button').on('click', function() {
			var id = $(this).parents('li').attr('data-id');
			var req = {};
			var li = $(this).parents('li');
			var res;
			req.action = 'mark-chat-notification';
			req.notificationId  = id;
			req.chatAction = $(this).attr('data-chat');
			$.ajax({
					'type'	:	'post',
					'url'	:	ApiEndPoint,
					'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else {
					if (!res.roomId) {
						alertMsg("Notification marked as Read.");
						li.remove();
						fetchUserDetails();
					} else {
						li.remove();
						fetchUserDetails();
						window.open('../messenger/'+res.roomId);
					}
				}
			});
		});
		
		//adding event handlers for accept button
		$('.accept-button').on('click', function() {
			var elem = $(this);
			var id = $(this).parents('li').attr('data-id');
			var req = {};
			var res;
			req.action = 'accepted_student_invitation';
			req.link_id = id;
			$.ajax({
					'type'	:	'post',
					'url'	:	ApiEndPoint,
					'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0) {
					//alert(res.message);
					alertMsg("Invitation Accepted successfully.");
					//elem.parents('li:eq(0)').find('.accept-button, .reject-button').remove();
					elem.parents('li:eq(0)').remove();
					fetchUserDetails();
				} else {
					alertMsg("Invitation Accepted successfully.");
					//deleting buttons of the user
					//elem.parents('li:eq(0)').find('.accept-button, .reject-button').remove();
					elem.parents('li:eq(0)').remove();
					fetchUserDetails();
				}
			});
		});

		//adding event handlers for reject button
		$('.reject-button').on('click', function() {
			var elem = $(this);
			var id = $(this).parents('li').attr('data-id');
			var req = {};
			var res;
			req.action = 'rejected_student_invitation';
			req.link_id = id;
			$.ajax({
					'type'	:	'post',
					'url'	:	ApiEndPoint,
					'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else {
					alertMsg("Invitation rejected successfully.");
					//deleting buttons of the user
					elem.parents('li:eq(0)').find('.accept-button, .reject-button').remove();
					fetchUserDetails();
				}
			});
		});
	}
	
});