var ApiEndPoint = '../api/index.php';
var packagePageUrl='../packageDetails.php?packageId=';
var details;
var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var currency=2;
var subjectCount = 0;
var loginstatus = 0;
var professors = '';
var initdt=0;
var packagedata= [];  
var packagedatai=null;
var packend=[];
var packendi=null;
var courseId=[];
var courseIdi=null;
var previousId=null;
var packinfo=[];
// for all packages
var packagedatap= []; 
var packendp=[];
var packendip=null;
var packagedataip=null;
var courseIdip=null;
var amount=0;
var amountINR=0;
var dispcurrency='<i class="fa fa-dollar"></i>';

$(function () {
	checkCurrency();
	fetchUserDetails();
	getStudentPackages();
	getPackages();

	$('#takeCourse').on('click', function() {
		var req = {};
		var res;
		req.courseId=courseIdi;
		req.packageId=packagedatai;
		req.enddate=packendi;
		req.action = 'package-course-enroll';
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status == 0) {
				alert(res.message);	
			}
			else {
				alertMsg("You had sucessfully subscribed the course");
				$('.takecourseCont').html('<a data-toggle="modal" href="MyExam.php?courseId='+courseIdi+'" id="takeCourse" class="btn btn-lg btn-ssuccess btn-block">Start Learning</a>');	
			}
		});
		
	});

	function fetchUserDetails() {
		var req = {};
		var res;
		req.action = 'get-student-details-for-changePassword';
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			if (res.status == 0)
				alert(res.message);
			else {
				details = res;
				fillUserDetails(res);
			}
		});
	}
	//for subscribed packages courses...
	function loadCourseDetails(){	
		var req = {};
		var startDate;
		var catIds = [], catNames = [];
		req.action = 'loadCourseDetails';
		req.courseId =courseIdi
		if(req.courseId != undefined) {
			$.ajax({
				'type': 'post',
				'url': ApiEndpoint,
				'data': JSON.stringify(req)
			}).done(function (res) {
				//console.log(res);
				//console.log('hello');
				//takecourseCont
				data = $.parseJSON(res);
				if (data.status == 1) {	
					$('#courseDetail').modal('show');
					//$('#courseCategory').html('');		
					var newpriceINR="";
					var newprice="";
					var today= new Date().getTime();
					var discountenddate = data.courseDetails.discount.endDate;
					if(data.courseDetails.discount.endDate > today )
					{
						newpriceINR=data.courseDetails.studentPriceINR*((100 - data.courseDetails.discount.discountINR) / 100);
						newprice=data.courseDetails.studentPrice*((100 - data.courseDetails.discount.discount) / 100);
						newpriceINR=newpriceINR.toFixed(2);
						newprice=newprice.toFixed(2);
						
					}
					var price			=	"";
					var displayCurrency	=	'<i class="fa fa-dollar"></i>';
					originalPrice=data.courseDetails.studentPrice;
					//filling demo video
					$('.takecourseCont').hide();
					$('.buy-course-content').hide();					
					if(data.courseDetails.demoVideo != '') {
						$('.course-showcase').html('<a href="javascript:void(0)" id="showVideo" data-video="'+data.courseDetails.demoVideo+'">'+
														'<span class="course-img-preview">'+
															'<img src="'+data.courseDetails.image+'" class="btn-block" />'+
															'<i class="fa fa-play-circle-o"></i></a>'+
														'</span>'+
													'</a>'+
													'<div id="showcaseBlock" class="hide"><video id="videoShowcasePlayer"></video></div>');
						
					} else {
						$('.course-showcase').html('<img src="'+data.courseDetails.image+'" class="btn-block" />');
					}
					if(currency==2){
						displayCurrency='<i class="fa fa-rupee"></i>';
						data.courseDetails.studentPrice=data.courseDetails.studentPriceINR;
						newprice=newpriceINR;
						curtype=2;
					}
					if(data.courseDetails.studentPrice == '0' || data.courseDetails.studentPrice == '0.00') {
						displayCurrency = '';
						data.courseDetails.studentPrice = 'Free';
					}
					//newprice> 0 $$ newprice< data.courseDetails.studentPrice
					originalPrice=data.courseDetails.studentPrice;
					dispcurrency=displayCurrency;
					$('#courseCategory').html('');
					$('#targetAudience').html('');
					$('#courseName').html(data.courseDetails.name);
					$('#subtitle').html(data.courseDetails.subtitle);
					$('#description').html('<p>'+data.courseDetails.description+'<p>');
					if($('#description p').height()>86) {
						$('.course-description .js-course-details').removeClass("hide");
					}
					$('.js-share-facebook').attr('href','http://www.facebook.com/share.php?u='+encodeURI(window.location.href)+'&title='+data.courseDetails.name);
					$('.js-share-twitter').attr('href','http://twitter.com/intent/tweet?status='+data.courseDetails.name+' '+encodeURI(window.location.href));
					$('.js-share-google').attr('href','https://plus.google.com/share?url='+encodeURI(window.location.href));
					$('.js-share-facebook, .js-share-twitter, .js-share-google').click(function(e){
						e.preventDefault();
						var left = (window.screen.width / 2) - ((400 / 2) + 10);
						var top = (window.screen.height / 2) - ((500 / 2) + 50);

						window.open($(this).attr('href'), "MsgWindow", "top="+top+", left="+left+", width=500, height=400");
					});
					//$('#course_img').attr("src", data.courseDetails.image);
					if (data.courseDetails.targetAudience != null) {
						var targetAudience = data.courseDetails.targetAudience.split(",");
						for (var i = targetAudience.length - 1; i >= 0; i--) {
							$('#targetAudience').append('<li>'+targetAudience[i]+'</li>');
						};
					} else {
						$('#targetAudience').append('<li>Anybody</li>');
					}
					/*if(data.institute.description.length > 212) {
						var first = data.institute.description.substring(0, 212);
						var second = '<span id="moreBack" style="display: none;">' + data.institute.description.substring(212, data.institute.description.length) + '</span>';
						$('#institute-background').html(first + second + '<a data-hidden="0" href="#" id="viewMoreBack">... View More</a>');
						$('#viewMoreBack').on('click', function(e) {
							e.preventDefault();
							if($(this).attr('data-hidden') == 0) {
								$(this).text(' Hide');
								$(this).attr('data-hidden', "1")
								$('#moreBack').slideDown();
							}
							else {
								$(this).text('... View More');
								$(this).attr('data-hidden', "0")
								$('#moreBack').slideUp();
							}
						});
					}
					else
						$('#institute-background').html(data.institute.description);*/
					if(data.courseCategories.length == 0) {
						$('#courseCategory').html('No Categories Selected');
					} else {
						$.each(data.courseCategories, function (k, cat) {
							//catNames.push(cat.catName);
							catIds.push(cat.categoryId);
							catNames.push(cat.category);
							$('#courseCategory').append('<a href="#" class="btn btn-category">'+cat.category+'</a>');
						});
					}
					/*if(catNames.length == 0) {
						$('#courseCategory').html('No Categories Selected');
					} else {
						$.each(data.courseCategories, function (key, val) {
							//catNames.push(cat.catName);
							$('#courseCategory').append('<a href="#" class="btn btn-category">'+val.category+'</a>');
						});
					}*/
					instituteName = data.institute.name;
					link = 'institute/';
					switch(parseInt(data.courseDetails.roleId)) {
						case 1:
							instituteName = data.institute.name;
							link = 'institute/';
							break;
						case 2:
						case 3:
							instituteName = data.institute.firstName + ' ' + data.institute.lastName;
							link = 'instructor/';
							break;
					}
					$('.js-institute-name').attr('href', link + data.institute.userId);
					$('.js-institute-name').html(instituteName);
					$('#instituteImg').attr("src",  data.institute.profilePic);
					$('#instituteDescription').html(data.institute.description);
					if($('#instituteDescription').height()>102) {
						$('.about-instructor .js-instructor-details').removeClass("hide");
					}
					if(!data.institute.tagline) {
						$('#tagline').remove();
					} else {
						$('#tagline').html(data.institute.tagline);
					}
					$('#courseID').html('C00' + data.courseDetails.id);
					$('.js-enrolled').html(data.studentCount);
					//now working on dates
					var dateHTML = '';
					var currentDate = new Date(parseInt(data.currentDate));
					var liveDate = new Date(parseInt(data.courseDetails.liveDate));
					var calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
					var endDate = (data.courseDetails.endDate == '')?'':new Date(parseInt(data.courseDetails.endDate));
					//console.log(currentDate, liveDate, endDate);
					/*if(endDate == '') {
						if(liveDate.valueOf() < currentDate.valueOf()) {
							$('#liveEndDates').remove();
							$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Course Available till 1 year from date of purchase');
						}
						else {
							$('#liveEndDates').after('<li class="list-item-long">'+
														'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
														'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
													'</li>');
							$('#liveEndDates').remove();
							$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Course available till 1 year from Launch date');
						}
					} else {*/
					if(endDate == '') {
						var futureDate = new Date(currentDate.getFullYear() + 1, currentDate.getMonth(), currentDate.getDate(), 0, 0, 0, 0);
						var calcEndDate = endDate.getDate()+ ' ' +MONTH[endDate.getMonth()]+ ' ' + endDate.getFullYear();
						if(endDate.valueOf() > futureDate.valueOf() && liveDate.valueOf() < currentDate.valueOf()) {
							$('#liveEndDates').after('<li class="list-item-long">'+
														'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
														'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
													'</li>');
							$('#liveEndDates').remove();
							//$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Course available till 1 year from Launch date');
							$('#msgValidity').remove();
						} else if(endDate.valueOf() > futureDate.valueOf() && liveDate.valueOf() > currentDate.valueOf()) {
							$('#liveEndDates').after('<li class="list-item-long">'+
														'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
														'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
													'</li>');
							$('#liveEndDates').remove();
							//$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Course available till 1 year from Launch date');
							$('#msgValidity').remove();
						} else {
							$('#liveEndDates').after('<li class="list-item-short">'+
														'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
														'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
													'</li>'+
													'<li class="list-item-short">'+
														'<label for=""><i class="fa fa-calendar"></i> End Date :</label>'+
														'<span class="value" id="endDate"> '+calcEndDate+'</span>'+
													'</li>');
							$('#liveEndDates').remove();
							$('#msgValidity').remove();
						}
					} else {
						$('#liveEndDates').remove();
						$('#msgValidity').remove();
					}
					//$('#courseDetail').append(dateHTML);
					$('.page-load-hidden').removeClass("invisible");

					if(!!data.institute.userId) {
						instituteId = data.institute.userId;
						otherCoursesByInstitute(0);
					}

					//adding ratings
					if(data.courseDetails.rating.total == 0) {
						rating = 0;
					}
					else {
						rating = parseFloat(data.courseDetails.rating.rating).toFixed(1);
					}
					$('.ratingStars').rating('rate', rating);
					$('.ratingTotal').text(data.courseDetails.rating.total + ' Ratings');
					$('.avg-rate').text(rating);

					if(loginstatus==1) {
						//$('#takeCourse').removeAttr('data-toggle');
						$('#takeCourse').attr('href','');
						$('#takeCourse').on('click', function() {
								if(userRole != 4) {
								alert('You are not authorised to purchase this course. Please login with student account');
								retun;
							}
							$('#purchaseNowModal .course-name').text($('#courseName').text());
							$('#purchaseNowModal .amount').html($('.price-now').html());
							$('#purchaseNowModal').modal('show');
						});
					}
				}
				else {
					alert(data.message);
					return;
				}
			});
		}
		else
			alert("No course Details");
		getsubjectDetails();
	}

	function getsubjectDetails() {
		var req = {};
		req.action = 'loadSubjectDetails';
		req.courseId = courseIdi;
		if (req.courseId != undefined) {
			$.ajax({
				'type': 'post',
				'url': ApiEndpoint,
				'data': JSON.stringify(req)
			}).done(function (res) {
				data = $.parseJSON(res);
				if (data.status == 1) {
					var html = "";
					var subject_html = '';
					var professor = '';
					var subjectHeading='';
					var noOfLectures = 0;
					var noOfSubjects = data.subjects.length;
					var totalDuration = 0;
					var noOfExams = 0;
					var noOfAssignments = 0;
					var currentDate = 0;
					var liveDate = 0;
					var calcLiveDate = 0;
					var endDate = 0;
					var liveHTML = '';
					for (var i = 0; i < data.subjects.length; i++) {
						/*subject_html += '<div class="row"><div class="col-md-2 about">'
							+ '<img class="img-responsive" style="width: 100%;" alt="No Image" src="' + data.subjects[i].image + '">'
							+ '<div style="text-align: center;"><strong>' + data.subjects[i].name + '</strong></div>'
							+ '<br>'
						+ '</div>'
						+ '<div class="col-md-10 about">'
							+ '<table class="table" style="margin-bottom: 5px;width: 98%;" data-subjectId=' + data.subjects[i].id + '>'
								+ '<tbody>';*/

						if(data.subjects[i].chapters.length>0 || data.subjects[i].independentExam.length || data.subjects[i].independentAssignment.length) {
							subject_html += '<table class="table table-responsive">'+
												'<tr class="list-row-subject">'+
													'<th class="course-subject">'+
														'<h2 class="subject-title">'+
															'<div class="subject-avatar">'+
																'<img src="' + data.subjects[i].image + '" alt="" class="img-responsive">'+
															'</div>Subject: ' + data.subjects[i].name + ''+
														'</h2>'+
													'</th>'+
												'</tr>';
							if(data.subjects[i].independentExam == null)
								data.subjects[i].independentExam = 0;
							if(data.subjects[i].independentAssignment == null)
								data.subjects[i].independentAssignment = 0;

							if(data.subjects[i].chapters.length>0) {
							
								for (var j = 0; j < data.subjects[i].chapters.length; j++) {
									var examCount;
									var assCount;
									if(data.subjects[i].chapters[j].Exam > 0) {
										examCount = data.subjects[i].chapters[j].Exam + ' Exams';
									}
									else
										examCount = '--';
									if(data.subjects[i].chapters[j].Assignment > 0) {
										assCount = data.subjects[i].chapters[j].Assignment + ' Assignments';
									}
									else
										assCount = '--';
									if(data.subjects[i].chapters[j].Exam == null)
										data.subjects[i].chapters[j].Exam = 0; 
									if(data.subjects[i].chapters[j].Assignment == null)
										data.subjects[i].chapters[j].Assignment = 0;
									/*subject_html += '<tr class="chapter" data-chapterId="' + data.subjects[i].chapters[j].id + '">'
												+ '<td style="width: 10%;"><strong>Section ' + (j+1) + ':</strong></td><td style="width: 37%;"><strong>' + data.subjects[i].chapters[j].name + '</strong></td>';
									subject_html += '<td style="width: 13%;"></td>';
									subject_html += '<td style="width: 20%;text-align: center;"><span class="soup">' + examCount + '</span></td>'
												+ '<td style="width: 20%;text-align: center;"><span class="soup">' + assCount + '</span></td>'
											+ '</tr>';*/
									if(data.subjects[i].chapters[j].content.length>0 || data.subjects[i].chapters[j].Exam.length || data.subjects[i].chapters[j].Assignment.length) {
										subject_html += '<tr class="list-row-section">'+
															'<th class="ssection">'+
																'<h3 class="ssection-title">'+
																	'<span class="ssection-title-txt">Section ' + (j+1) + ': ' + data.subjects[i].chapters[j].name + '</span>'+
																	'<span class="ssection-meta"><p>' + examCount + ' Exams</p><p>' + assCount + ' Assignments</p><p>'+data.subjects[i].chapters.length+' lectures</p></span>'+
																'</h3>'+
															'</th>'+
														'</tr>';
										if(data.subjects[i].chapters[j].content.length>0) {
											subject_html += '<tr class="list-row-section">'+
																	'<th class="ssection-content">'+
																		'<h3 class="ssection-title">'+
																			'<span class="ssection-title-txt">Section Content</span>'+
																			'<span class="ssection-meta"><p>' + examCount + ' Exams</p></span>'+
																		'</h3>'+
																	'</th>'+
																'</tr>';

											//now adding lectures of the exam
											for(var k = 0; k < data.subjects[i].chapters[j].content.length; k++) {
												var type = '';
												/*switch(data.subjects[i].chapters[j].content[k].lectureType) {
													case 'Youtube':
														type = '<i class="fa fa-youtube-square" style="font-size: 2em;"></i>';
														break;
													case 'Vimeo':
														type = '<i class="fa fa-vimeo-square" style="font-size: 2em;"></i>';
														break;
													case 'video':
														type = '<i class="fa fa-video-camera" style="font-size: 2em;"></i>';
														break;
													case 'doc':
														type = '<i class="fa fa-book" style="font-size: 2em;"></i>';
														break;
													case 'ppt':
														type = '<i class="fa fa-file-powerpoint-o" style="font-size: 2em;"></i>';
														break;
													case 'text':
														type = '<i class="fa fa-file-text-o" style="font-size: 2em;"></i>';
														break;
													case 'dwn':
														type = '<i class="fa fa-cloud-download" style="font-size: 2em;"></i>';
														break;
												}
												subject_html += '<tr class="chapter" data-chapterId="' + data.subjects[i].chapters[j].id + '">'
														+ '<td style="width: 10%;text-align: right;">' + type +'</td>'
														+ '<td style="width: 37%;">Lecture ' + (k + 1) + ': ' + data.subjects[i].chapters[j].content[k].lectureName + '</td>';
												if(data.subjects[i].chapters[j].content[k].demo == 1)
													subject_html += '<td style="width: 13%;"><a class="btn btn-info btn-custom btn-xs viewDemo">Free Demo</a></td>';
												else
													subject_html += '<td style="width: 13%;"></td>';
												subject_html += '<td style="width: 20%;"></td>'
														+ '<td style="width: 20%;"></td>'
													+ '</tr>';*/
												var freePreview = '';
												var durationTime = '';
												switch(data.subjects[i].chapters[j].content[k].lectureType) {
													case 'Youtube':
														type = '<i class="fa fa-play-circle-o"></i>';
														break;
													case 'Vimeo':
														type = '<i class="fa fa-play-circle-o"></i>';
														break;
													case 'video':
														type = '<i class="fa fa-play-circle-o"></i>';
														if(data.subjects[i].chapters[j].content[k].demo == 1) {
															if(data.subjects[i].chapters[j].content[k].duration>0) {
																durationTime = '<span class="lecture-duration pull-right"></span>';
																freePreview = '<div class="lecture-play-btn">'+
																					'<button class="btn btn-primary btn-preview" data-src="' + data.subjects[i].chapters[j].content[k].metastuff + '">Free Demo (' + calcDisplayTime(data.subjects[i].chapters[j].content[k].duration) + ')</button>'+
																				'</div>';
															} else {
																freePreview = '<div class="lecture-play-btn">'+
																					'<button class="btn btn-primary btn-preview" data-src="' + data.subjects[i].chapters[j].content[k].metastuff + '">Free Demo</button>'+
																				'</div>';
															}
														}
														break;
													case 'doc':
														type = '<i class="fa fa-book"></i>';
														break;
													case 'ppt':
														type = '<i class="fa fa-file-powerpoint-o"></i>';
														break;
													case 'text':
														type = '<i class="fa fa-file-text-o"></i>';
														break;
													case 'dwn':
														type = '<i class="fa fa-cloud-download"></i>';
														break;
												}

												/*subject_html += '<tr class="chapter" data-chapterId="' + data.subjects[i].chapters[j].id + '">'
														+ '<td style="width: 10%;text-align: right;">' + type +'</td>'
														+ '<td style="width: 37%;">Lecture ' + (k + 1) + ': ' + data.subjects[i].chapters[j].content[k].lectureName + '</td>';
												if(data.subjects[i].chapters[j].content[k].demo == 1)
													subject_html += '<td style="width: 13%;"><a class="btn btn-info btn-custom btn-xs viewDemo">Free Demo</a></td>';
												else
													subject_html += '<td style="width: 13%;"></td>';
												subject_html += '<td style="width: 20%;"></td>'
														+ '<td style="width: 20%;"></td>'
													+ '</tr>';*/
												if(data.subjects[i].chapters[j].content[k].duration>0 && data.subjects[i].chapters[j].content[k].demo != 1) {
													durationTime = '<span class="lecture-duration pull-right">' + calcDisplayTime(data.subjects[i].chapters[j].content[k].duration) + '</span>';
												}
												subject_html += '<tr class="list-row-lecture">'+
																	'<td class="lecture">'+
																		'<h4 class="lecture-title">'+
																			'<a href="javascript:void(0)" class="lecture-link">'+
																				'<span class="lecture-play">' + type +'</span>'+
																				'<div class="lecture-title-blk">'+
																					'<span class="lecture-number">' + (j+1) + '.' + (k+1) + '</span>'+
																					'<span class="lecture-title-txt">' + data.subjects[i].chapters[j].content[k].lectureName + '</span>'+
																					durationTime +
																				'</div>' + freePreview +
																			'</a>'+
																		'</h4>'+
																	'</td>'+
																'</tr>';
												noOfLectures++;
											}
										}
										if(data.subjects[i].chapters[j].Exam.length) {
											subject_html += '<tr class="list-row-section">'+
																'<th class="ssection-content">'+
																	'<h3 class="ssection-title">'+
																		'<span class="ssection-title-txt">Section Exams</span>'+
																		'<span class="ssection-meta"><p>' + examCount + ' Exams</p></span>'+
																	'</h3>'+
																'</th>'+
															'</tr>';
											for(var k = 0; k < data.subjects[i].chapters[j].Exam.length; k++) {
												currentDate = new Date(parseInt(data.currentDate));
												liveDate = new Date(parseInt(data.subjects[i].chapters[j].Exam[k].startDate));
												calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
												if(liveDate.valueOf() > currentDate.valueOf()) {
													liveHTML = '<span class="lecture-duration pull-right">Live date : '+calcLiveDate+'</span>';
												} else {
													liveHTML = '';
												}
												//endDate = (data.subjects[i].chapters[j].Exam[k].endDate == '')?'':new Date(parseInt(data.subjects[i].chapters[j].Exam[k].endDate));
												subject_html += '<tr class="list-row-lecture">'+
																'<td class="lecture">'+
																	'<h4 class="lecture-title">'+
																		'<a href="javascript:void(0)" class="lecture-link">'+
																			'<span class="lecture-play"><i class="fa fa-pencil-square-o"></i></span>'+
																			'<div class="lecture-title-blk">'+
																				'<span class="lecture-number">' + (j+1) + '.' + (k+1) + '</span>'+
																				'<span class="lecture-title-txt">' + data.subjects[i].chapters[j].Exam[k].name + '</span>'+
																				liveHTML +
																			'</div>'
																		'</a>'+
																	'</h4>'+
																'</td>'+
															'</tr>';
												noOfExams++;
											}
										}
										if(data.subjects[i].chapters[j].Assignment.length) {
											subject_html += '<tr class="list-row-section">'+
																'<th class="ssection-content">'+
																	'<h3 class="ssection-title">'+
																		'<span class="ssection-title-txt">Section Assignments</span>'+
																		'<span class="ssection-meta"><p>' + assCount + ' Assignments</p></span>'+
																	'</h3>'+
																'</th>'+
															'</tr>';
											for(var k = 0; k < data.subjects[i].chapters[j].Assignment.length; k++) {
												currentDate = new Date(parseInt(data.currentDate));
												liveDate = new Date(parseInt(data.subjects[i].chapters[j].Assignment[k].startDate));
												calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
												if(liveDate.valueOf() > currentDate.valueOf()) {
													liveHTML = '<span class="lecture-duration pull-right">Live date : '+calcLiveDate+'</span>';
												} else {
													liveHTML = '';
												}
												//endDate = (data.subjects[i].chapters[j].Assignment[k].endDate == '')?'':new Date(parseInt(data.subjects[i].chapters[j].Assignment[k].endDate));
												subject_html += '<tr class="list-row-lecture">'+
																'<td class="lecture">'+
																	'<h4 class="lecture-title">'+
																		'<a href="javascript:void(0)" class="lecture-link">'+
																			'<span class="lecture-play"><i class="fa fa-book"></i></span>'+
																			'<div class="lecture-title-blk">'+
																				'<span class="lecture-number">' + (j+1) + '.' + (k+1) + '</span>'+
																				'<span class="lecture-title-txt">' + data.subjects[i].chapters[j].Assignment[k].name + '</span>'+
																				liveHTML +
																			'</div>'
																		'</a>'+
																	'</h4>'+
																'</td>'+
															'</tr>';
												noOfAssignments++;
											}
										}
									}
								}
							}

							if(data.subjects[i].independentExam.length) {
								subject_html += '<tr class="list-row-section">'+
													'<th class="ssection">'+
														'<h3 class="ssection-title">'+
															'<span class="ssection-title-txt">Independent Exams</span>'+
															'<span class="ssection-meta"><p>' + data.subjects[i].independentExam.length + ' Exams</p></span>'+
														'</h3>'+
													'</th>'+
												'</tr>';
								for(var k = 0; k < data.subjects[i].independentExam.length; k++) {
									currentDate = new Date(parseInt(data.currentDate));
									liveDate = new Date(parseInt(data.subjects[i].independentExam[k].startDate));
									calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
									if(liveDate.valueOf() > currentDate.valueOf()) {
										liveHTML = '<span class="lecture-duration pull-right">Live date : '+calcLiveDate+'</span>';
									} else {
										liveHTML = '';
									}
									subject_html += '<tr class="list-row-lecture">'+
													'<td class="lecture">'+
														'<h4 class="lecture-title">'+
															'<a href="javascript:void(0)" class="lecture-link">'+
																'<span class="lecture-play"><i class="fa fa-pencil-square-o"></i></span>'+
																'<div class="lecture-title-blk">'+
																	'<span class="lecture-number">' + (k+1) + '</span>'+
																	'<span class="lecture-title-txt">Exam : ' + data.subjects[i].independentExam[k].name + '</span>'+
																	liveHTML +
																'</div>'
															'</a>'+
														'</h4>'+
													'</td>'+
												'</tr>';
									noOfExams++;
								}
							}
							if(data.subjects[i].independentAssignment.length) {
								subject_html += '<tr class="list-row-section">'+
													'<th class="ssection">'+
														'<h3 class="ssection-title">'+
															'<span class="ssection-title-txt">Independent Assignments</span>'+
															'<span class="ssection-meta"><p>' + data.subjects[i].independentAssignment.length + ' Assignments</p></span>'+
														'</h3>'+
													'</th>'+
												'</tr>';
								for(var k = 0; k < data.subjects[i].independentAssignment.length; k++) {
									currentDate = new Date(parseInt(data.currentDate));
									liveDate = new Date(parseInt(data.subjects[i].independentAssignment[k].startDate));
									calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
									if(liveDate.valueOf() > currentDate.valueOf()) {
										liveHTML = '<span class="lecture-duration pull-right">Live date : '+calcLiveDate+'</span>';
									} else {
										liveHTML = '';
									}
									subject_html += '<tr class="list-row-lecture">'+
													'<td class="lecture">'+
														'<h4 class="lecture-title">'+
															'<a href="javascript:void(0)" class="lecture-link">'+
																'<span class="lecture-play"><i class="fa fa-book"></i></span>'+
																'<div class="lecture-title-blk">'+
																	'<span class="lecture-number">' + (k+1) + '</span>'+
																	'<span class="lecture-title-txt">Assignment : ' + data.subjects[i].independentAssignment[k].name + '</span>'+
																	liveHTML +
																'</div>'
															'</a>'+
														'</h4>'+
													'</td>'+
												'</tr>';
									noOfAssignments++;
								}
							}
							
							if(data.subjects[i].totalDuration>0) {
								totalDuration = totalDuration + parseInt(data.subjects[i].totalDuration);
							}
							subject_html += '</table>';
							subjectCount++;
						}
					}
					if(totalDuration>0) {
						var result = calcDisplayTimeWords(totalDuration);
						$('#courseTest').after(
											'<li class="list-item-short">'+
												'<label for=""><i class="fa fa-youtube-play"></i> Duration :</label>'+
												'<span class="value" id="totalDuration"> '+result+'</span>'+
											'</li>');
						$('#courseTest').remove();
					} else {
						$('#courseTest').after(
											'<li class="list-item-short">'+
												'<label for=""><i class="fa fa-question-circle"></i> Questions :</label>'+
												'<span class="value" id="totalDuration"> '+data.totalQuestions+'</span>'+
											'</li>');
						$('#courseTest').remove();
					}
					//$('#course-append').append(subject_html);
					$('#curriculum').html('');
					$('#curriculum').append(subject_html);
					$('.viewDemo').on('click', function() {
						window.location = 'student/chapterContent.php?courseId=' + getUrlParameter('courseId') + '&subjectId=' + $(this).parents('table').attr('data-subjectId') +'&chapterId=' + $(this).parents('tr').attr('data-chapterId') + '&demo=1';
					});
					$.each($('#course-append table'), function() {
						$(this).find('.soup').hide();
						$.each($(this).find('.soup'), function() {
							if($(this).text() != '--') {
								$(this).parents('table').find('.soup').show();
								return;
							}
						});
						$(this).find('tr.chapter:eq(0), tr.chapter:eq(1), tr.chapter:eq(2)').show();
						if($(this).find('tr.chapter').length > 3)
							$(this).parents('.about').find('.viewMoreChap').show();
						$('.viewMoreChap').off('click');
						$('.viewMoreChap').on('click', function() {
							if($(this).attr('data-hidden') == 0) {
								$(this).parents('.about').find('table').find('tr').show();
								$(this).text('Hide');
								$(this).attr('data-hidden', '1');
							}
							else {
								$(this).parents('.about').find('table').find('tr').hide();
								$(this).parents('.about').find('table').find('tr.chapter:eq(0), tr.chapter:eq(1), tr.chapter:eq(2)').show();
								$(this).parents('.about').find('table').find('tr.independent').show();
								$(this).text('View More');
								$(this).attr('data-hidden', '0');
							}
						});
					});
				}
				$('#professors').html(professors);
				$('#noOfSubjects').html(noOfSubjects);
				/*$('#noOfLectures').html(noOfLectures);
				$('#noOfExams').html(noOfExams);
				$('#noOfAssignments').html(noOfAssignments);*/
				var contentBuild = 0;
				var contentClass = 0;
				var contentHTML	 = "";
				if(noOfLectures>0) {contentBuild++;}
				if(noOfExams>0) {contentBuild++;}
				if(noOfAssignments>0) {contentBuild++;}
				if(contentBuild>0) {
					switch(contentBuild) {
						case 1: contentClass = "list-item-long";break;
						case 2: contentClass = "list-item-short";break;
						case 3: contentClass = "list-item-xshort";break;
					}
					if(noOfLectures>0) {
						contentHTML+=  '<li class="'+contentClass+'">'+
											'<label for=""><i class="fa fa-youtube-play"></i> Lectures :</label>'+
											'<span class="value" id="noOfLectures"> '+noOfLectures+'</span>'+
										'</li>';
					}
					if(noOfExams>0) {
						contentHTML+=  '<li class="'+contentClass+'">'+
											'<label for=""><i class="fa fa-pencil-square-o"></i> Exams :</label>'+
											'<span class="value" id="noOfExams"> '+noOfExams+'</span>'+
										'</li>';
					}
					if(noOfAssignments>0) {
						contentHTML+=  '<li class="'+contentClass+'">'+
											'<label for=""><i class="fa fa-book"></i> Assignments :</label>'+
											'<span class="value" id="noOfAssignments"> '+noOfAssignments+'</span>'+
										'</li>';
					}
					$('#contentStats').after(contentHTML);
					$('#contentStats').remove();
				}
			});
		}
		else
			alert("No Subject Found Details");
		//fetchCourseReviews();
	}


	function fetchCourseReviews() {
		var req = {};
		var res;
		req.action = 'get-all-reviews-for-course';
		req.courseId = courseIdi;
		$.ajax({
			type: "post",
			url: ApiEndpoint,
			data: JSON.stringify(req) 
		}).done(function(res) {
			res = $.parseJSON(res);
			var html = '';
			var c1=c2=c3=c4=c5=0;
			$.each(res.reviews, function(i, review) {
				//counting reviews
				switch(review.rating) {
					case '1.00':
						c1++;
						break;
					case '2.00':
						c2++;
						break;
					case '3.00':
						c3++;
						break;
					case '4.00':
						c4++;
						break;
					case '5.00':
						c5++;
						break;
				}
				var date = '';
				var dateobj = new Date(parseInt(review.time + '000'));
				date = format(dateobj.getDate()) + ' ' + MONTH[dateobj.getMonth()] + ' ' + dateobj.getFullYear() + ' ' + format(dateobj.getHours()) + ':' + format(dateobj.getMinutes());
				html += '<li class="comment-item">'
							+ '<div class="row">'
								+ '<div class="col-sm-3">'
									+ '<div class="user-block">'
										+ '<div class="user-avatar">'
											+ '<img src="' + review.studentImage + '" alt="">'
										+ '</div>'
										+ '<span>' + review.studentName + '</span>'
									+ '</div>'
								+ '</div>'
								+ '<div class="col-sm-9">'
									+ '<div class="rating">'
										+ '<input type="hidden" class="studentRating" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="' + review.rating + '" DISABLED/>'
										+ '<span class="">&emsp;' + date + '</span>';
									if(subjectCount > 1)
										html += '<span class="subject"> (Subject: ' + review.subjectName + ')</span>';
									html += '</div>'
									+ '<h4>' + review.title + '</h4>'
									+ '<div class="comment">'
										+ '<p>' + review.review + '</p>'
									+ '</div>'
								+ '</div>'
							+ '</div>'
						+ '</li>';
			});
			$('#comments ul.list-comments').append(html);
			//$('.studentRating').rating();
			var total = c1 + c2 + c3 + c4 + c5;
			if(total == 0)
				$('#comments ul').html('<li class="comment-item"><div class="user-block"><span style="margin-left: 0px;">No Reviews Found</span></div></li>');
			$('.progress1').css('width', percentage(c1, total) + '%');
			$('.progress2').css('width', percentage(c2, total) + '%');
			$('.progress3').css('width', percentage(c3, total) + '%');
			$('.progress4').css('width', percentage(c4, total) + '%');
			$('.progress5').css('width', percentage(c5, total) + '%');
			$('.count1').text(c1);
			$('.count2').text(c2);
			$('.count3').text(c3);
			$('.count4').text(c4);
			$('.count5').text(c5);
		});
	}

	function format(number) {
		if(number < 10)
			return '0' + number;
		else
			return number;
	}

	function percentage(number, total) {
		if(total == 0)
			return 0;
		return (parseInt(number)/parseInt(total)) * 100;
	}

	function calcDisplayTimeWords(totalDuration) {
		var hours = parseInt( totalDuration / 3600 ) % 24;
		var minutes = parseInt( totalDuration / 60 ) % 60;
		var seconds = totalDuration % 60;

		if(hours>0) {
			if(minutes>0 && minutes<=15) {
				return hours+".25 hours";
			} else if(minutes>15 && minutes<=30) {
				return hours+".5 hours";
			} else if(minutes>30 && minutes<=45) {
				return hours+".75 hours";
			} else {
				return hours+" hours";
			}
		} else if(minutes>0) {
			if(seconds>0 && seconds<=15) {
				return minutes+".25 min";
			} else if(seconds>15 && seconds<=30) {
				return minutes+".5 min";
			} else if(seconds>30 && seconds<=45) {
				return minutes+".75 min";
			} else {
				return minutes+" min";
			}
		} else {
			return seconds+" sec";
		}
	}

	function calcDisplayTime(totalSec) {
		var hours = parseInt( totalSec / 3600 ) % 24;
		var minutes = parseInt( totalSec / 60 ) % 60;
		var seconds = totalSec % 60;

		if(hours>0) {
			return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
		} else {
			return (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
		}
	}

	function otherCoursesByInstitute(start) {
		var req = {};
		req.action = 'loadOtherCoursesInstitute';
		req.courseId = getUrlParameter("courseId");
		req.instituteId = instituteId;
		req.start = start;
		if (req.instituteId != undefined || req.courseId != undefined) {
			$.ajax({
				'type': 'post',
				'url': ApiEndpoint,
				'data': courseIdi
			}).done(function (res) {
				data = $.parseJSON(res);
				if (data.status == 1) {
					var price="";
					var displayCurrency='<i class="fa fa-dollar"></i>';
					if(data.courses.length > 0) {
						if(data.courses.length > 1) {
							$('.other-courses-title').html('Courses by <a href="'+link + instituteId+'" class="js-institute-name">'+instituteName+'</a>');
						} else {
							$('.other-courses-title').html('Course by <a href="'+link + instituteId+'" class="js-institute-name">'+instituteName+'</a>');
						}
						if(start == 0) {
							$('#listOtherCourses').append('<ul class="list-unstyled list-courses">');
						}
						//for (var i = data.courses.length - 1; i >= 0; i--) {
						for (var i = 0; i < data.courses.length; i++) {
							if(currency==2){
								 displayCurrency='<i class="fa fa-rupee"></i>';
								 data.courses[i].studentPrice=data.courses[i].studentPriceINR;
							}
							if(data.courses[i].studentPrice == '0' || data.courses[i].studentPrice == '0.00') {
								displayCurrency = '';
								data.courses[i].studentPrice = 'Free';
							}
							$('#listOtherCourses .more-item').remove();
							$('#listOtherCourses .list-courses').append(
								'<li class="other-item">'+
									'<div class="row">'+
										'<div class="col-md-6">'+
											'<a href="courseDetails.php?courseId='+data.courses[i].id+'" class="js-other-course" data-id="'+data.courses[i].id+'">'+
												'<img class="btn-block" src="'+data.courses[i].image+'" alt="">'+
											'</a>'+
										'</div>'+
										'<div class="col-md-6 pl-clr">'+
											'<div>'+
												'<a href="courseDetails.php?courseId='+data.courses[i].id+'" class="other-title">'+data.courses[i].name+'</a>'+
											'</div>'+
											'<div>'+
												'<span class="cost">' + displayCurrency + ' ' + data.courses[i].studentPrice + '</span>'+
											'</div>'+
											'<div>'+
												'<input type="hidden" class="courseRating" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="' + data.courses[i].rating.rating + '" DISABLED/>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</li>');
							
						};
						$('.courseRating').rating();
					}
					if($('#listOtherCourses .list-courses .other-item').length!=data.noOfCourses) {
						$('#listOtherCourses .list-courses').append('<li class="more-item text-center"><a href="javascript:void(0)" class="btn btn-default btn-block">View More</a></li>');
					}
				}
			});
		}
		else
			alert("No Subject Found Details");
	}

	//get-packageCourses get-packageCourses student-joined-courses
	function getEnrolledPackageCourse() {
		var req = {};
		var res;
		req.packId=packagedatai;
		req.action = 'get-packageCourses';
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			//console.log(res);
			if (res.status == 0)
				alert(res.message);
			else {
				fillEnrolledPackageCourse(res);
			}
		});
	}

	function fillEnrolledPackageCourse(data) {
		$("#pacSubstable").dataTable().fnDestroy();
		var presentcourse='';
		var subjectHtml = '', html = '';
		var courseStartDate = '';
		$.each(data.courses, function (i, course) {
			//variable to detect payment can be made or not
			courseId[i]=	course.id;
			//var paymentStatus = 1;
			var startdate = new Date(course.startDate);
			var today = new Date();
			var enddate = new Date(startdate);
			enddate.setFullYear(enddate.getFullYear() + 1);
			
			// var percentage = (today.valueOf() - startdate.valueOf()) / (enddate.valueOf() - startdate.valueOf()) * 100;
			// var percent = parseFloat(percentage);
			//var display = percent.toFixed(2) + '%';
			var expired="";
			//if(enddate.valueOf()<today.valueOf()){
			//  expired="disable-mycourse";
			//}
			// alert(display);
			//console.log(course);
			html +="<tr data-cid='" + i + "'" + "" + ">" 
				+'<td><div class="col-md-12 col-sm-12">'
				+ '<div class="row">'
				+ '<div class="col-md-3"><a  class="courseDetailModal" id="'+i+'" ><img class="img-responsive imgbox120" src="' + course.image + '" alt=""></a>'
				+ '</div>'
				+ '<div class="col-md-9">'
				+'<div class="col-md-12"><div style="float:left"> <h3 class=""> <a  class="courseDetailModal" id="'+i+'"  > ' + course.name + '<span style="font-size:12px;font-weight:600;"> (ID: C00' + course.id + ')</span> </a></h3><a href="#" class="institute" data-instituteId="' + '#' + '"><strong>By</strong>: ' + course.username + '</a>'
				+ courseStartDate
				+ '</div>'
				+ '<div style="float:right;">'
				+ '<div class="text-right id="afterenroll'+i+'">'+((course.enroll>0)?'<a href="MyExam.php?courseId='+course.id+'" class="btn btn-md btn-success course" data-courseId="' + i + '">Start Learning</a>' : '<button class="btn btn-md btn-success course1" id="'+i+'" data-courseId="' + course.id + '">Enroll Now </button>')+ '</div>'
				+ '</div>'
				+'</div>'
				+'<div class="col-sm-8 mbT10">'
				+ '<div class="col-xs-1"><i style="font-size:30px; color:orange;margin:5px 15px 0px 0px;" class="fa fa-th-list"></i></div>'
				+'<div class="col-xs-11">'  
				+ '<strong>Subjects </strong><br/>';
			subjectHtml='';
			$.each(course.subjects, function (j, subject) {
				subjectHtml +='<a href="chapter.php?courseId='+course.course_id+'&subjectId='+subject.subjectId+'">' + subject.subjectName + ', </a>';
			});
			subjectHtml = subjectHtml.substring(0, subjectHtml.length - 6) + '</a>';
			html = html + subjectHtml 
					+ '</div>'
					+ '<div class="col-xs-10">'                 
					+ '</div>'                   
					+ '</div>'
					+ '<div class="col-md-4 mbT10">'
					+ '<div style="float:right;">'
					+ '<div class="text-right enrollt'+i+'">'+ '<div class="text-right">'+((course.enroll>0)?'' : '<button class="btn btn-md btn-primary course courseDetailModal" id="'+i+'" data-courseId="' + course.course_id + '">View Course</button>')+ '</div>'
					+ '</div>' 
					+ '</div>'
					+ '</div>'
					+ '</div>'	
					+ '<hr style="color: #CBC8C8;"/>'
					+ '</div>'
					+ '</div></td>"';
		});//pacSubstable
		// $('#div-student-courseert').html(html);
		$('#pacSubstable tbody').html('').html(html);
		//if(initdt ==0){
			
		
		$('#pacSubstable').dataTable( {
			"aaSorting": [[ 4, "desc" ]]
		});
		//initdt=1;
		//}
		
		//event listener for handling expired courses
		$('.disable-mycourse a:not(.institute)').on('click', function(e) {
			e.preventDefault();
			var instituteId = $(this).parents('.disable-mycourse').find('.institute').attr('data-instituteId');
			var courseId = $(this).parents('.disable-mycourse').find('.course').attr('data-courseId');
			$('#instituteId').val(instituteId);
			$('#courseId').val(courseId);
			$('#payYourself').attr('href', 'paymentDetails.php?courseId=' + courseId);
			if($(this).parents('.disable-mycourse').attr('data-paymentStatus') == 0) {
				//now removing self pay button and changing the text of other button
				$('#payYourself').hide();
				$('#showContact').text('Contact Institute/Professor for extending the course');
				$('#payment .message').text('This course has expired. Contact respective Institute/Professor to extend this course.');
			}
			$('#payment').modal('show');
		});

		$('#pacSubstable').on( "click",'.courseDetailModal', function() {		
			var getId=$(this).attr('id');
			courseIdi=courseId[getId];
			loadCourseDetails();
			//getStudentCourses();
		});
		
		
		
		
		$('#pacSubstable').on('click','.course1' ,function() {	
			var getId=$(this).attr('id');	
			courseIdi=courseId[getId];		
			previousId=getId;
			var req = {};
			var res;
			req.courseId=courseIdi;
			req.packageId=packagedatai;
			req.enddate=packendi;
			req.action = 'package-course-enroll';
			//console.log(req);
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0) {
					//alertMsg(res.message);
					alert(res.message);
				}
				else {
					alertMsg("You had sucessfully subscribed the course");
					$('#'+getId).parent().html('<a href="MyExam.php?courseId='+courseId[getId]+'" class="btn btn-md btn-success course" data-courseId="' +courseId[getId] + '">Start Learning</a>');
					$('.enrollt'+getId).parent().html('');
				 	$('.courseDetailModal').modal('hide');
					
				}
			});
		});
    }
	$('#courseDetail').on('hidden.bs.modal', function() {
		//console.log("i-ooooutside");
		//$('#pacSubs').reset();
		$('#targetAudience').html('');
		$(this).removeData('bs.modal');
		$(this).data('bs.modal', null);
		$(this).removeData("modal");
		//$(this).removeData('modal');
		//$('.modal-body').html("");
		//$('.modal-body').hide();
		//reverting buttons to original on modal hidden
	});

	$('#pacSubs').on('hidden.bs.modal', function() {
		//console.log("outside");
		//$('#pacSubs').reset();
		$(this).removeData('bs.modal');
		$(this).data('bs.modal', null);
		$(this).removeData("modal");
		//$(this).removeData('modal');
		//$('.modal-body').html("");
		//$('.modal-body').hide();
		//reverting buttons to original on modal hidden
		
	});

	//function to fill user details
	function fillUserDetails(data) {
		$('.notify.badge').text(data.notiCount);
		$('.fullName').text(data.details.firstName + ' ' + data.details.lastName);
		$('.username').text(data.details.username);
		$('.user-image').attr('src', data.details.profilePic);
		$('#btn_rupee, #btn_dollar').removeClass('btn-white btn-info');
		if (data.details.currency == 1) {
			$('#btn_dollar').addClass('btn-info');
			$('#btn_rupee').addClass('btn-white');
		}
		else if (data.details.currency == 2) {
			$('#btn_dollar').addClass('btn-white');
			$('#btn_rupee').addClass('btn-info');
		}
	}

	//used to get packages in which student  Subscription Available in student/subscription.php
	function getStudentPackages() {
		//console.log("getStudentPackages");
		var req = {};
		var res;
		req.action = 'student-package';
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			//console.log(res);
			if (res.status == 0)
			{
				//alert(res.message);
			}
			else {
				//console.log(res.packages);
				if (res.packages.length>0) {
					fillStudentPackages(res);
				} else {
					$("#div-student-course").html('<div class="col-md-12"><em>'+res.message+'</em></div>');
				}
			}
		});
	}
	function fillStudentPackages(data) {
		var subjectHtml = '', html = '';
		var courseStartDate = '';
		$.each(data.packages, function (i, packages) {
			//variable to detect payment can be made or not
			packend[i]=packages.endDate;
			packagedata[i]= packages.packId;
			var paymentStatus = 1;
			var startdate = new Date(packages.startDate);
			var today = new Date();
			var enddate = new Date(startdate);
			enddate.setFullYear(enddate.getFullYear() + 1);
			/*if (course.endDate != null && course.endDate != '') {
				var enddatedb = new Date(parseInt(course.endDate));
				var origStart = new Date(parseInt(course.origStartDate));
				if (enddatedb.valueOf() < enddate.valueOf()) {
					//enddate = new Date(enddatedb);
					enddate = enddatedb;
					//disabling self payment
					paymentStatus = 0;
			    }
				//this will show the course starting date
				//courseStartDate = '<br><i class="fa fa-clock-o"></i> <strong>Start Date</strong> ' + origStart.getDate() + ' ' + month[origStart.getMonth()] + ' ' + origStart.getFullYear();
			}
			*/
			var desc=packages.packDescription;
			if(desc.length>150)
			{
				desc = desc.substr(0,150) + '...';
			}
			var expired="";
			if(enddate.valueOf()<today.valueOf()){
				expired="disable-mycourse";
			}
			// alert(display);
			//console.log(packages);
		html += '<div class="col-xs-12 col-sm-6 col-md-3 xyz course1" >'
			+ '<section class="panel package-panel" id="subscriptionPanel">'
			+ 	'<div class="pro-img-box">'
			+ 		'<a class="purchasedPackage" id="'+i+'"><img class="img-responsive package-img" src="' + packages.image + '" id="subscriptionImageBox" alt=""></a>'
			+ 	'</div>'
			+ 	'<div class="panel-body">'
			+ 		'<h4 class="text-center" id="packname">'+packages.packName+'</h4>'
			+ 		'<div class="author-by form-group text-primary"><small>By: ' + packages.username + '</small></div>'
			+ 		'<div class="text-center"><input type="hidden" class="rating" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="" DISABLED /></div>'
			+ 		'<div>'
			+ 			'<p class="text-center"><strong class="text-uppercase">About Package </strong></p>'
			+ 		'</div>'
			+ 		'<div class="text-center">'
			+ 			'<p id ="packdesk">'+desc+' </p>'
			+ 		'</div>' 
			+ 		'<div class="col-xs-8 text-center" id ="package">'
			+ 			'<a class="btn btn-md btn-success purchasedPackage" id="'+i+'" > View Package</a>'
			+ 		'</div>'
			+ 	'</div>'
			+ '</section>'
			+ '</div>';
		});
		$('#div-student-course').append(html);
		//event listener for handling expired courses
		$('.disable-mycourse a:not(.institute)').on('click', function(e) {
			e.preventDefault();
			var instituteId = $(this).parents('.disable-mycourse').find('.institute').attr('data-instituteId');
			var courseId = $(this).parents('.disable-mycourse').find('.course').attr('data-courseId');
			$('#instituteId').val(instituteId);
			$('#courseId').val(courseId);
			$('#payYourself').attr('href', 'paymentDetails.php?courseId=' + courseId);
			if($(this).parents('.disable-mycourse').attr('data-paymentStatus') == 0) {
				//now removing self pay button and changing the text of other button
				$('#payYourself').hide();
				$('#showContact').text('Contact Institute/Professor for extending the course');
				$('#payment .message').text('This course has expired. Contact respective Institute/Professor to extend this course.');
			}
			$('#payment').modal('show');
		});
		
		
		// @ayush -- event listener for View Package 
		$('.purchasedPackage').on( "click", function() {
			var getId=$(this).attr('id');
			packagedatai=packagedata[getId];
			packendi=packend[getId];
			//console.log("package click packageId  " +packendi);
			getEnrolledPackageCourse();
			$('.notpurchases').hide();
			$('#pacSubs').modal('show');
		});

		$('.imgPop').on( "click", function() {
			var getId=$(this).attr('id');
			packagedatai=packagedata[getId];
			packendi=packend[getId];
			getEnrolledPackageCourse();
			$('.notpurchases').hide();
			$('#pacSubs').modal('show');
			//getStudentCourses();	
		});

		$('.modelclose').on("click", function(e)
		{
			$(this).removeData();
		});
	}

	//get all packages
	
	//this function is used to get al the available packages in marketplace Packages Available section
	function getPackages() {
		var req = {};
		var res;
		req.action = 'all-package';
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			//console.log(res);
			if (res.status == 0){
				//alert(res.message);
			}				
			else {
				//console.log(res.packages);
				if (res.packages.length>0) {
					fillPackages(res);
				} else {
					$("#package-available").html('<div class="col-md-12"><em>'+res.message+'</em></div>');
				}
			}
		});
	}
	function fillPackages(data) {
		var subjectHtml = '', html = '';
		var courseStartDate = '';
		$.each(data.packages, function (i, packages) {
			//variable to detect payment can be made or not
			packendp[i]=packages.endDate;
			packagedatap[i]= packages.packId;
			packinfo[i]=packages;
			var paymentStatus = 1;
			var startdate = new Date(packages.startDate);
			var today = new Date();
			var enddate = new Date(startdate);
			enddate.setFullYear(enddate.getFullYear() + 1);
			/*if (course.endDate != null && course.endDate != '') {
				var enddatedb = new Date(parseInt(course.endDate));
				var origStart = new Date(parseInt(course.origStartDate));
				if (enddatedb.valueOf() < enddate.valueOf()) {
					//enddate = new Date(enddatedb);
					enddate = enddatedb;
					//disabling self payment
					paymentStatus = 0;
				}
				//this will show the course starting date
				//courseStartDate = '<br><i class="fa fa-clock-o"></i> <strong>Start Date</strong> ' + origStart.getDate() + ' ' + month[origStart.getMonth()] + ' ' + origStart.getFullYear();
            }
           */
			var desc=packages.packDescription;
			if(desc.length>150)
			{
				desc = desc.substr(0,150) + '...';
			}
			var expired="";
			if(enddate.valueOf()<today.valueOf()){
				expired="disable-mycourse";
			}
			// alert(display);
			html += '<div class="col-xs-12 col-sm-6 col-md-3 xyz course1" >'
			+ '<section class="panel package-panel" id="packagePanel">'
			+ 	'<div class="pro-img-box">'
			+ 		'<a class="packagePur" id="'+i+'"><img class="img-responsive package-img" src="' + packages.image + '" id="packageImageBox" alt=""></a> '
			+ 	'</div>'
			+ 	'<div class="panel-body">'
			+ 		'<h4 class="text-center" id ="packname">'+packages.packName+'</h4>'
			+ 		'<div class="author-by form-group text-primary"><small>By: ' + packages.username + '</small></div>'
			+ 		'<div class="text-center"><input type="hidden" class="rating" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="" DISABLED /></div>'
			+ 		'<div>'
			+ 			'<p class="text-center"><strong class="text-uppercase">About Package </strong></p>'
			+ 		'</div>'
			+ 		'<div class="text-center">'
			+ 			'<p id ="packdesk">'+desc+' </p>'
			+ 		'</div>' 
			+ 		'<div class="text-center" id="package">' 
			+ 			'<a class="btn btn-md btn-success packagePur"  id="'+i+'" > View Package</a>'
			+ 		'</div>'
			+ 	'</div>'
			+ '</section>'
			+ '</div>';
		});
		$('#package-available').append(html);
		//event listener for handling expired courses
		$('.disable-mycourse a:not(.institute)').on('click', function(e) {
			e.preventDefault();
			var instituteId = $(this).parents('.disable-mycourse').find('.institute').attr('data-instituteId');
			var courseId = $(this).parents('.disable-mycourse').find('.course').attr('data-courseId');
			$('#instituteId').val(instituteId);
			$('#courseId').val(courseId);
			$('#payYourself').attr('href', 'paymentDetails.php?courseId=' + courseId);
			if($(this).parents('.disable-mycourse').attr('data-paymentStatus') == 0) {
				//now removing self pay button and changing the text of other button
				$('#payYourself').hide();
				$('#showContact').text('Contact Institute/Professor for extending the course');
				$('#payment .message').text('This course has expired. Contact respective Institute/Professor to extend this course.');
			}
			$('#payment').modal('show');
		});
		
		// @ayush -- event listener for View Package 
		$('.packagePur').on( "click", function() {
			//$('#pacSubs').html("");
			var getId=$(this).attr('id');
			packagedataip=packagedatap[getId];
			packendip=packendp[getId];
			//console.log(packinfo[getId].price);
			amount=packinfo[getId].price;
			amountINR=packinfo[getId].priceINR;
			packagedatai=packagedataip;
			getAllStudentCourses();
			$('.notpurchases').show();
			$('#pacSubs').modal('show');
			//getStudentCourses()	
		});

		$('.imagPop').on( "click", function() {
			//$('#pacSubs').html("");
			var getId=$(this).attr('id');
			packagedataip=packagedatap[getId];
			packendip=packendp[getId];
			amount=packinfo[getId].price;
			amountINR=packinfo[getId].priceINR;
			packagedatai=packagedataip;
			getAllStudentCourses();
			$('.notpurchases').show();
			$('#pacSubs').modal('show');
			//getStudentCourses();
		});
    }
	function getAllStudentCourses() {
		var req = {};
		var res;
		req.packId=packagedataip;
		req.action = 'get-AllpackageCourses';
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			if (res.status == 0)
				alert(res.message);
			else {
				fillAllStudentCourses(res);
			}
		});
	}

	function fillAllStudentCourses(data) {
		$("#pacSubstable").dataTable().fnDestroy();
		var presentcourse='';
		var subjectHtml = '', html = '';
		var courseStartDate = '';
		var currency='<i class="fa fa-inr"></i>';
		if(amount ==0 || amountINR==0 )
		{
			amount=' Free';
			amountINR=' Free';
			currency = ' ';
		}
		if(currency==1)
					{	currency='<i class="fa fa-dollar"></i>';
						$('#totalAmount').html('<strong>Total Amount </strong>'+currency+'  '+amount);
						//console.log("amount"+amount);
					}else{
						
						$('#totalAmount').html('<strong>Total Amount </strong>'+currency+'  '+amountINR);
					}
		
		$.each(data.courses, function (i, course) {
			//console.log(course.image);
			//variable to detect payment can be made or not
			courseId[i]=	course.id;
			//var paymentStatus = 1;
			var startdate = new Date(course.startDate);
			var today = new Date();
			var enddate = new Date(startdate);
				enddate.setFullYear(enddate.getFullYear() + 1);

			// var percentage = (today.valueOf() - startdate.valueOf()) / (enddate.valueOf() - startdate.valueOf()) * 100;
			// var percent = parseFloat(percentage);
			//var display = percent.toFixed(2) + '%';
			var expired="";
			//if(enddate.valueOf()<today.valueOf()){
			//  expired="disable-mycourse";
			//}
			// alert(display);
			html +="<tr data-cid='" + i + "'" + "" + ">" 
					+ '<td><div class="col-md-12 col-sm-12">'
					+ '<div class="row">'
					+ '<div class="col-md-3"><a class="courseDetailModal" id="'+i+'" ><img style="width: 100%;height: 120px;"   class="img-responsive" src="' + course.image + '" alt=""></a>'
					+ '</div>'
					+ '<div class="col-md-9">'
					+ '<div class="col-md-12"><div style="float:left"> <h3 class=""> <a class="courseDetailModal"   id="'+i+'" > ' + course.name + '<span style="font-size:12px;font-weight:600;"> (ID: C00' + course.id + ')</span> </a></h3><a href="#" class="institute" data-instituteId="' + '#' + '"><strong>By</strong>: ' + course.username + '</a>'
					+ courseStartDate
					+ '</div>'
					+ '<div style="float:right;">'
					/*+ '<div class="text-right id="afterenroll'+i+'">'+((course.enroll>0)?'<a href="MyExam.php?courseId='+course.id+'" class="btn btn-md btn-success course" data-courseId="' + i + '">Start Learning</a>' : '<button class="btn btn-md btn-success course1" id="'+i+'" data-courseId="' + course.id + '">Enroll Now</button>')+ '</div>'*/
					+ '</div>'
					+ '</div>'
					+ '<div class="col-sm-8 mbT10">'
					+ '<div class="col-xs-1"><i style="font-size:30px; color:orange;margin:5px 15px 0px 0px;" class="fa fa-th-list"></i></div>'
					+ '<div class="col-xs-11">'  
					+ '<strong>Subjects </strong><br/>';
			subjectHtml='';
			$.each(course.subjects, function (j, subject) {
				subjectHtml +='<a href="chapter.php?courseId='+course.course_id+'&subjectId='+subject.subjectId+'">' + subject.subjectName + ', </a>';
			});
			subjectHtml = subjectHtml.substring(0, subjectHtml.length - 6) + '</a>';
			html = html + subjectHtml 
					+ '</div>'
					+ '<div class="col-xs-10">'                 
					+ '</div>'                   
					+ '</div>'
					+ '<div class="col-md-4 mbT10">'
					+ '<div style="float:right;">'
					+ '<div class="text-right enrollt'+i+'">'+ '<div class="text-right"><button class="btn btn-md btn-primary course courseDetailModal" id="'+i+'" data-courseId="' + course.course_id + '">View Details</button></div>'
					+ '</div>' 
					+ '</div>'
					+ '</div>'
					+ '</div>'	
					+ '<hr style="color: #CBC8C8;"/>'
					+ '</div>'         
					+ '</div></td>"';
		});//pacSubstable
		// $('#div-student-courseert').html(html);
		$('#pacSubstable tbody').html('').html(html);
		//if(initdt ==0){
		$("#pacSubstable").dataTable().fnDestroy();
		//$("#pacSubstable").dataTable().fnClearTable();
		$('#pacSubstable').dataTable( {
			"aaSorting": [[ 4, "desc" ]]
		});
		initdt=1;
		//event listener for handling expired courses
		$('.disable-mycourse a:not(.institute)').on('click', function(e) {
			e.preventDefault();
			var instituteId = $(this).parents('.disable-mycourse').find('.institute').attr('data-instituteId');
			var courseId = $(this).parents('.disable-mycourse').find('.course').attr('data-courseId');
			$('#instituteId').val(instituteId);
			$('#courseId').val(courseId);
			$('#payYourself').attr('href', 'paymentDetails.php?courseId=' + courseId);
			if($(this).parents('.disable-mycourse').attr('data-paymentStatus') == 0) {
				//now removing self pay button and changing the text of other button
				$('#payYourself').hide();
				$('#showContact').text('Contact Institute/Professor for extending the course');
				$('#payment .message').text('This course has expired. Contact respective Institute/Professor to extend this course.');
			}
			$('#payment').modal('show');
		});

		$('#pacSubstable').on( "click",'.courseDetailModal', function() {
			var getId=$(this).attr('id');
			courseIdi=courseId[getId];
			//console.log("lkk"+courseIdi);
			loadPackageCourseDetails();
			$('#courseDetail').modal('show');
			//getStudentCourses();
		});
	
		$('#pacSubstable').on('click','.course1' ,function() {	
			var getId=$(this).attr('id');	
			courseIdi=courseId[getId];		
			previousId=getId;
			var req = {};
			var res;
			req.courseId=courseIdi;
			req.packageId=packagedatai;
			req.enddate=packendi;
			req.action = 'package-course-enroll';
			//console.log(req);
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0) {
					//alertMsg(res.message);
					alert(res.message);
				}
				else {
					alertMsg("You had sucessfully subscribed the course");
					$('#'+getId).parent().html('<a href="MyExam.php?courseId='+courseId[getId]+'" class="btn btn-md btn-success course" data-courseId="' +courseId[getId] + '">Start Learning</a>');
					$('.enrollt'+getId).parent().html('');
				 	$('.courseDetailModal').modal('hide');
					
				}
			});	
		});

		$('#pacSubs').on('hidden.bs.modal', function() {
			$(this).data('bs.modal', null);
		});
	
	}
	
	function fetchpackageInfo() {
		var amount=0;
		var courses=1;
		var req = {};
		var res;
		req.packId=packagedataip;
		req.action = 'get-package-price';
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			//console.log(res);
			res =  $.parseJSON(res);
			if(res.status == 0) {
				alert(res.message);
			}
			else {
				console.log(res);
				var currency='<i class="fa fa-dollar"></i>';
				if(currency==1)
				{	amount=res.packages[0]['price'];
					if(amount == 0){
						amount= ' Free';
						currency=' ';
						
					}
					$('#totalAmount').html('<strong>Total Amount </strong>'+currency+'  '+amount);
					//console.log(amount);
					$('#purchaseNowModal .amount').html('<strong>Total Amount </strong>'+currency+'  '+amount);
				}else{
					amount=res.packages[0]['priceINR'];
					currency='<i class="fa fa-inr"></i>';
					if(amount == 0){
						amount= ' Free';
						currency=' ';
						
					}
					$('#totalAmount').html('<strong>Total Amount </strong>'+currency+'  '+amount);
					$('#purchaseNowModal .amount').html('<strong>Total Amount </strong>'+currency+'  '+amount);
				}
				
				$('#purchaseNowModal').modal('show');
			}

		});
	}
	
	$('#purchasePackage').on('click', function(){
		
		fetchpackageInfo();
	});
	
	
	$('#payUMoneyButton').on('click', function() {
		var today= new Date().getTime();
		var req = {};
		var res;
		req.action = 'purchaseStudentPackage';
		req.packId = packagedataip;
		req.date= today;
		$.ajax({
			'type' : 'post',
			'url'  : ApiEndpoint,
			'data' : JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				if(res.paymentSkip == 1)
					window.location = 'subscription.php';
				else if(res.method == 2){
					var html = '';
					html = '<form action="'+res.url+'" method="post" id="payUForm">'
						+ '<input type="hidden" name="txnid" value="'+res.txnid+'">'
						+ '<input type="hidden" name="key" value="'+res.key+'">'
						+ '<input type="hidden" name="amount" value="'+res.amount+'">'
						+ "<input type='hidden' name='productinfo' value='"+res.productinfo+"'>"
						+ '<input type="hidden" name="firstname" value="'+res.firstname+'">'
						+ '<input type="hidden" name="email" value="'+res.email+'">'
						+ '<input type="hidden" name="surl" value="'+res.surl+'">'
						+ '<input type="hidden" name="furl" value="'+res.furl+'">'
						+ '<input type="hidden" name="curl" value="'+res.curl+'">'
						+ '<input type="hidden" name="furl" value="'+res.furl+'">'
						+ '<input type="hidden" name="hash" value="'+res.hash+'">'
						+ '<input type="hidden" name="service_provider" value="'+res.service_provider+'">';
						+ '</form>'
					$('body').append(html);
					$('#payUForm').submit();
				}
			else if(res.method == 1) {
				var html = '';
				html += '<form action="' + res.url + '" method="post" id="paypalForm">'
					+ '<!-- Identify your business so that you can collect the payments. -->'
					+ '<input type="hidden" name="business" value="' + res.business + '">'
					+ '<!-- Specify a Buy Now button. -->'
					+ '<input type="hidden" name="cmd" value="_xclick">'
					+ '<!-- Specify details about the item that buyers purchase. -->'
					+ '<input type="hidden" name="item_name" value="' + res.item_name + '">'
					+ '<input type="hidden" name="amount" value="' + res.amount + '">'
					+ '<input type="hidden" name="currency_code" value="USD">'
					+ '<input type="hidden" name="quantity" value="1">'
					+ '<input type="hidden" name="invoice" value="' + res.orderId + '">'
					+ '<input type="hidden" name="item_number" value="' + res.orderId + '">'
					+ '<input type="hidden" name="return" value="' + res.surl + '" />'
					+ '<input type="hidden" name="cancel_return" value="' + res.curl + '" />'
					+ '<input type="hidden" name="notify_url" value="' + res.nurl + '" />'
					+ '<input type="hidden" name="lc" value="US" />'
					+ '<!-- Display the payment button. -->'
					+ '<input type="image" name="submit" border="0" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online">'
					+ '<img alt="" border="0" width="1" height="1"	src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >'
				+ '</form>';
				$('body').append(html);
				$('#paypalForm').submit();
			}
			}else{
				alert(res.message);
				$('#purchaseNowModal').modal('hide');
			}
		});
	});
	
	/*
	this function is used to purchase course only seperatly from package
	*/
	$('#buySeper').on('click', function() {
		window.location = '../courseDetails.php?courseId='+courseIdi
	});
	
	$('#buyPackage').on('click', function() {
		fetchpackageInfo();
	});

	$('#coursepayUMoneyButton').on('click', function() {
		var today= new Date().getTime();
		var req = {};
		var res;
		req.action = 'purchaseCourseViaPayU';
		req.courseId = courseIdi;
		req.date= today;
		req.couponCode="";
		req.couStatus="";
		$.ajax({
			'type' : 'post',
			'url'  : ApiEndpoint,
			'data' : JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status == 1) {
				if(res.paymentSkip == 1)
					window.location = 'MyCourse.php';
				else if(res.method == 2){
					var html = '';
					html = '<form action="'+res.url+'" method="post" id="payUForm">'
						+ '<input type="hidden" name="txnid" value="'+res.txnid+'">'
						+ '<input type="hidden" name="key" value="'+res.key+'">'
						+ '<input type="hidden" name="amount" value="'+res.amount+'">'
						+ "<input type='hidden' name='productinfo' value='"+res.productinfo+"'>"
						+ '<input type="hidden" name="firstname" value="'+res.firstname+'">'
						+ '<input type="hidden" name="email" value="'+res.email+'">'
						+ '<input type="hidden" name="surl" value="'+res.surl+'">'
						+ '<input type="hidden" name="furl" value="'+res.furl+'">'
						+ '<input type="hidden" name="curl" value="'+res.curl+'">'
						+ '<input type="hidden" name="furl" value="'+res.furl+'">'
						+ '<input type="hidden" name="hash" value="'+res.hash+'">'
						+ '<input type="hidden" name="service_provider" value="'+res.service_provider+'">';
						+ '</form>'
					$('body').append(html);
					$('#payUForm').submit();
				}
				else if(res.method == 1) {
					var html = '';
					html += '<form action="' + res.url + '" method="post" id="paypalForm">'
						+ '<!-- Identify your business so that you can collect the payments. -->'
						+ '<input type="hidden" name="business" value="' + res.business + '">'
						+ '<!-- Specify a Buy Now button. -->'
						+ '<input type="hidden" name="cmd" value="_xclick">'
						+ '<!-- Specify details about the item that buyers purchase. -->'
						+ '<input type="hidden" name="item_name" value="' + res.item_name + '">'
						+ '<input type="hidden" name="amount" value="' + res.amount + '">'
						+ '<input type="hidden" name="currency_code" value="USD">'
						+ '<input type="hidden" name="quantity" value="1">'
						+ '<input type="hidden" name="invoice" value="' + res.orderId + '">'
						+ '<input type="hidden" name="item_number" value="' + res.orderId + '">'
						+ '<input type="hidden" name="return" value="' + res.surl + '" />'
						+ '<input type="hidden" name="cancel_return" value="' + res.curl + '" />'
						+ '<input type="hidden" name="notify_url" value="' + res.nurl + '" />'
						+ '<input type="hidden" name="lc" value="US" />'
						+ '<!-- Display the payment button. -->'
						+ '<input type="image" name="submit" border="0" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online">'
						+ '<img alt="" border="0" width="1" height="1"	src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >'
					+ '</form>';
					$('body').append(html);
					$('#paypalForm').submit();
				}
			}
			else{
				alertMsg(res.message);
				$('#purchaseCourseNowModal').modal('hide');
			}
		});
	});
	
	function loadPackageCourseDetails(){	
	var req = {};
	var startDate;
	var catIds = [], catNames = [];
	req.action = 'loadPackageCourseDetails';
	req.courseId =courseIdi;
	if(req.courseId != undefined) {
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			if (data.status == 1) {			
				var newpriceINR=0;
				var newprice=0;
				var today= new Date().getTime();
				var discountenddate = data.courseDetails.discount.endDate;
				if(data.courseDetails.discount.endDate > today )
				{
					newpriceINR=data.courseDetails.studentPriceINR*((100 - data.courseDetails.discount.discountINR) / 100);
					newprice=data.courseDetails.studentPrice*((100 - data.courseDetails.discount.discount) / 100);
					newpriceINR=newpriceINR.toFixed(2);
					newprice=newprice.toFixed(2);
					
				}
				var price="";
				var displayCurrency='<i class="fa fa-dollar"></i>';
				originalPrice=data.courseDetails.studentPrice;
				//alert(data.courseDetails.demoVideo);
				//filling demo video
				if(data.courseDetails.demoVideo != '') {
					$('.course-showcase').html('<a href="javascript:void(0)" id="showVideo" data-video="'+data.courseDetails.demoVideo+'">'+
													'<span class="course-img-preview">'+
														'<img src="'+data.courseDetails.image+'" class="btn-block" />'+
														'<i class="fa fa-play-circle-o"></i></a>'+
													'</span>'+
												'</a>'+
												'<div id="showcaseBlock" class="hide"><video id="videoShowcasePlayer"></video></div>');
					} else {
					$('.course-showcase').html('<img src="'+data.courseDetails.image+'" class="btn-block" />');
				}
				if(currency==2){
					displayCurrency='<i class="fa fa-rupee"></i>';
					data.courseDetails.studentPrice=data.courseDetails.studentPriceINR;
					newprice=newpriceINR;
					amount=amountINR;
					curtype=2;
				}
				
				//newprice> 0 $$ newprice< data.courseDetails.studentPrice
				originalPrice=data.courseDetails.studentPrice;
				dispcurrency=displayCurrency;
				
				if(currency==1)
				{
					displayCurrency='<i class="fa fa-dollar"></i>';
					
				}
				else{
					
					displayCurrency='<i class="fa fa-rupee"></i>';
					
				}
				if(data.courseDetails.studentPrice == '0' || data.courseDetails.studentPrice == '0.00') {
					displayCurrency = '';
					data.courseDetails.studentPrice = 'Free';
				}
				
				if(data.courseDetails.liveForStudent == 1){
				
					if(newprice> 0)
					{
						//$('#course-only-price').html(displayCurrency);
						$('#course-only-price').html(/*'<span class="price-discount">' + displayCurrency + ' ' + data.courseDetails.studentPrice + '</span>'+*/
											'<span class="price">' +'Want to buy only this course at '+' <strike class= "pricedepreceated"> '+ displayCurrency +'' + data.courseDetails.studentPrice + '</strike>'+ ' &nbsp;&nbsp; '+displayCurrency +'  '+ newprice+' <a data-toggle="modal" href="#login-modal" id="buycourseonly" > Click here </a>   '+'</span>');
						newprice=" "+displayCurrency +""+ newprice;
						//console.log(newprice);
					}else
					{
						//newprice="Free";
						newprice=" "+displayCurrency +" "+ data.courseDetails.studentPrice;
						$('#course-only-price').html(/*'<span class="price-discount">' + displayCurrency + ' ' + data.courseDetails.studentPrice + '</span>'+*/
											'<span class="price">' + 'Want to buy only this course for  ' +displayCurrency+ ' ' + data.courseDetails.studentPrice +' <a data-toggle="modal" href="#login-modal" id="buycourseonly" > Click here </a>  '+'</span>');
					}

				}
				if(amount> 0)
				{	
					$('#coursePrice').html('<span class="price-now">' + displayCurrency + ' ' + amount + '</span>');
					
				}else
				{	
					$('#coursePrice').html('<span class="price-now">' + displayCurrency + ' ' +amount +'</span>');
				}
				$('#courseName').html(data.courseDetails.name);
				$('#subtitle').html(data.courseDetails.subtitle);
				$('#description').html('<p>'+data.courseDetails.description+'<p>');
				if($('#description p').height()>86) {
					$('.course-description .js-course-details').removeClass("hide");
				}
				$('.js-share-facebook').attr('href','http://www.facebook.com/share.php?u='+encodeURI(window.location.href)+'&title='+data.courseDetails.name);
				$('.js-share-twitter').attr('href','http://twitter.com/intent/tweet?status='+data.courseDetails.name+' '+encodeURI(window.location.href));
				$('.js-share-google').attr('href','https://plus.google.com/share?url='+encodeURI(window.location.href));
				$('.js-share-facebook, .js-share-twitter, .js-share-google').click(function(e){
					e.preventDefault();
					var left = (window.screen.width / 2) - ((400 / 2) + 10);
				    var top = (window.screen.height / 2) - ((500 / 2) + 50);

					window.open($(this).attr('href'), "MsgWindow", "top="+top+", left="+left+", width=500, height=400");
				});
				//$('#course_img').attr("src", data.courseDetails.image);
				$('#courseCategory').html('');
				$('#targetAudience').html('');
				//$('#curriculum').html('');
				//curriculum
				if (data.courseDetails.targetAudience != null) {
					var targetAudience = data.courseDetails.targetAudience.split(",");
					for (var i = targetAudience.length - 1; i >= 0; i--) {
						$('#targetAudience').append('<li>'+targetAudience[i]+'</li>');
					};
				} else {
					$('#targetAudience').append('<li>Anybody</li>');
				}

				if(data.courseCategories.length == 0) {
					$('#courseCategory').html('No Categories Selected');
				} else {
					$.each(data.courseCategories, function (k, cat) {
						//catNames.push(cat.catName);
						catIds.push(cat.categoryId);
						catNames.push(cat.category);
						$('#courseCategory').append('<a href="#" class="btn btn-category">'+cat.category+'</a>');
					});
				}
				instituteName = data.institute.name;
				link = 'institute/';
				switch(parseInt(data.courseDetails.roleId)) {
					case 1:
						instituteName = data.institute.name;
						link = 'institute/';
						break;
					case 2:
					case 3:
						instituteName = data.institute.firstName + ' ' + data.institute.lastName;
						link = 'instructor/';
						break;
				}
				$('.js-institute-name').attr('href', link + data.institute.userId);
				$('.js-institute-name').html(instituteName);
				$('#instituteImg').attr("src",  data.institute.profilePic);
				$('#instituteDescription').html(data.institute.description);
				if($('#instituteDescription').height()>102) {
					$('.about-instructor .js-instructor-details').removeClass("hide");
				}
				if(!data.institute.tagline) {
					$('#tagline').remove();
				} else {
					$('#tagline').html(data.institute.tagline);
				}
				$('#courseID').html('C00' + data.courseDetails.id);
				$('.js-enrolled').html(data.studentCount);
				//now working on dates
				var dateHTML = '';
				var currentDate = new Date(parseInt(data.currentDate));
				var liveDate = new Date(parseInt(data.courseDetails.liveDate));
				var calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
				var endDate = (data.courseDetails.endDate == '')?'':new Date(parseInt(data.courseDetails.endDate));
				//console.log(currentDate, liveDate, endDate);
				/*if(endDate == '') {
					if(liveDate.valueOf() < currentDate.valueOf()) {
						$('#liveEndDates').remove();
						$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Course Available till 1 year from date of purchase');
					}
					else {
						$('#liveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>');
						$('#liveEndDates').remove();
						$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Course available till 1 year from Launch date');
					}
				}else {*/
				if(endDate != '') {
					var futureDate = new Date(currentDate.getFullYear() + 1, currentDate.getMonth(), currentDate.getDate(), 0, 0, 0, 0);
					var calcEndDate = endDate.getDate()+ ' ' +MONTH[endDate.getMonth()]+ ' ' + endDate.getFullYear();
					if(endDate.valueOf() > futureDate.valueOf() && liveDate.valueOf() < currentDate.valueOf()) {
						$('#liveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>');
						$('#liveEndDates').remove();
						//$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Course available till 1 year from Launch date');
						$('#msgValidity').remove();
					} else if(endDate.valueOf() > futureDate.valueOf() && liveDate.valueOf() > currentDate.valueOf()) {
						
						$('#liveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>'+
												'<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> End Date :</label>'+
													'<span class="value" id="endDate"> '+calcEndDate+'</span>'+
												'</li>');
						$('#liveEndDates').remove();
						$('#msgValidity').remove();
					} else {
						$('#liveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>'+
												'<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> End Date :</label>'+
													'<span class="value" id="endDate"> '+calcEndDate+'</span>'+
												'</li>');
						$('#liveEndDates').remove();
						$('#msgValidity').remove();
					}
				} else {
					$('#liveEndDates').remove();
					$('#msgValidity').remove();
				}
				//$('#courseDetail').append(dateHTML);
				$('.page-load-hidden').removeClass("invisible");
				$('#buycourseonly').on('click', function() {
	
							/*if(userRole != 4) {
							alert('You are not authorised to purchase this course. Please login with student account');
							retun;
						}*/
						$('#purchaseCourseNowModal .course-name').text($('#courseName').text());
						$('#purchaseCourseNowModal .amount').html(newprice);
						//console.log(newprice);
						$('#purchaseCourseNowModal').modal('show');
					});
				

				if(!!data.institute.userId) {
					instituteId = data.institute.userId;
					otherCoursesByInstitute(0);
				}

				//adding ratings
				if(data.courseDetails.rating.total == 0) {
					rating = 0;
				}
				else {
					rating = parseFloat(data.courseDetails.rating.rating).toFixed(1);
				}
				//$('.ratingStars').rating('rate', rating);
				$('.ratingTotal').text(data.courseDetails.rating.total + ' Ratings');
				$('.avg-rate').text(rating);

				if(loginstatus==1) {
					//$('#takeCourse').removeAttr('data-toggle');
					$('#takeCourse').attr('href','');
					$('#takeCourse').on('click', function() {
							if(userRole != 4) {
							alert('You are not authorised to purchase this course. Please login with student account');
							retun;
						}
						$('#purchaseNowModal .course-name').text($('#courseName').text());
						$('#purchaseNowModal .amount').html($('.price-now').html());
						$('#purchaseNowModal').modal('show');
					});
				}
			}
			else {
				alert(data.message);
			}
		});
	}
	else
		alert("No course Details");
	getsubjectDetails();
 }

	//event listener for payment modal
	$('#showContact').on('click', function(){
		$('#contactContainer').show();
	}/*, function() {
		$('#contactContainer').hide();
	}*/);

	$('#sendContact').on('click', function() {
		unsetError($('#contact'));
		if($('#contact').val() == '') {
			setError($('#contact'), 'Please fill the message box.');
			return;
		}		
		var req = {};
		var res;
		req.action = 'send-renew request';
		req.courseId = $('#courseId').val();
		req.instituteId = $('#instituteId').val();
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			$('#payment').modal('hide');
			if(res.status == 0) {
				alert(res.message);
			}
			else {
				alertMsg("Your request send to the respective Institute/Professor.");
			}
		});
	});
	
	$('#payment').on('hidden.bs.modal', function() {
		$('#contact').val('Type your message here.');
		$('#contactContainer').hide();
		//reverting buttons to original on modal hidden
		$('#payYourself').show();
		$('#showContact').text('Contact Institute/Professor for key access.');
		$('#payment .message').text('Your access to this course has expired. Please renew your subscription by selecting any one payment method.');
	});

	function checkCurrency(){
		var req = {};
		var res;
		req.action = 'get-currency';
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status==1){
				currency=res.currency[0].currency;
				
			}
		});
	}
});