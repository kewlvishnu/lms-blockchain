var ApiEndpoint = '../api/index.php';
var DataSource = function (options) {
this._formatter = options.formatter;
this._columns = options.columns;
this._data = options.data;
};

DataSource.prototype = {
	
	columns: function () {
		return this._columns;
	},

	data: function (options, callback) {

		var self = this;
		if (options.search) {
			callback({ data: self._data, start: start, end: end, count: count, pages: pages, page: page });
        } else if (options.data) {
			callback({ data: options.data, start: 0, end: 0, count: 0, pages: 0, page: 0 });
		}  else {
			callback({ data: self._data, start: 0, end: 0, count: 0, pages: 0, page: 0 });
		}
	}
};
var req = {};
var string = '{ "data": [ ';
$(document).ready(function(){
	req.portfolioSlug = $('#slug').val();
	req.pageUserId 	  = $('#inputUserId').val();
	req.pageUserRole  = $('#inputUserRole').val();
	req.action = 'get-courses-for-student-tree';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		for(i=0; i<res.courses.length; i++) {
			if(res.courses[i].subjects.length <= 0)
				continue;
			string+='{ "name": "<span class=\'navigationNodes\' data-target=\'MyExam.php?courseId='+res.courses[i]['id']+'\' id=\'C'+res.courses[i]['id']+'\'>'+res.courses[i]['name']+'</span>", "type": "';
			if(res.courses[i]['subjects'].length > 0) {
				string+='folder", "data" : [ ';
				for(j=0 ; j<res.courses[i].subjects.length ; j++) {
					/*if(res.courses[i].subjects[j]['chapters'].length <= 0)
						continue;*/
					string+='{ "name" : "<span class=\'navigationNodes\' data-target=\'chapter.php?courseId='+res.courses[i]['id']+'&subjectId='+res.courses[i].subjects[j]['id']+'\' id=\'B'+res.courses[i].subjects[j]['id']+'\'>'+res.courses[i].subjects[j]['name']+'</span>", "type" : "';
					if(res.courses[i].subjects[j]['chapters'].length > 0) {
						string+='folder", "data" : [ ';
						for(k=0 ; k<res.courses[i].subjects[j].chapters.length ; k++) {
							string+='{ "name" : "<span class=\'navigationNodes\' data-target=\'chapterContent.php?courseId='+res.courses[i]['id']+'&subjectId='+res.courses[i].subjects[j]['id']+'&chapterId='+res.courses[i].subjects[j].chapters[k]['id']+'\'>'+res.courses[i].subjects[j].chapters[k]['name']+'</span>", "type" : "item", "additionalParameters" : { "id" : "A'+res.courses[i].subjects[j].chapters[k]['id']+'" } },';
								/*for(var l = 0; l < res.chapters[k].exams.length; l++) {
									string += '{ "name" : "<span class=\'navigationNodes\' data-target=\'add-assignment-2.php?examId=' + res.chapters[k].exams[l].id + '\'>' + res.chapters[k].exams[l].name;
									if(res.chapters[k].exams[l].type == "Exam")
										string += ' (E)';
									else
										string += ' (A)';
									string += '</span>", "type" : "item" }, ';
								}*/
						}
						/*for(var k = 0; k < res.subjects[j].exams.length; k++) {
							string += '{ "name" : "<span class=\'navigationNodes\' data-target=\'add-assignment-2.php?examId=' + res.subjects[j].exams[k].id + '\'>' + res.subjects[j].exams[k].name;
							if(res.subjects[j].exams[k].type == "Exam")
								string += ' (E)';
							else
								string += ' (A)';
							string += '</span>", "type" : "item" }, ';
						}*/
						string = string.substring(0, string.length - 1);
						string += ' ],';
					}
					else
						string+='item", ';
					string+='"additionalParameters" : { "id" : "B'+res.courses[i].subjects[j]['id']+'" } },';
				}
				string = string.substring(0, string.length - 1);
				string+=' ], ';
			}
			else
				string+='item", ';
			string+='"additionalParameters" : { "id" : "C'+res.courses[i]['id']+'" } },';
		}
		string = string.substring(0, string.length - 1);
		string+=' ], "delay": 0 }';
		//console.log(string);
		data = $.parseJSON(string);
		var treeDataSource = new DataSource(data);
		$('#Div1').tree({dataSource: treeDataSource});
		$('.navigationNodes').click(function() {
			if(!$(this).parents('.tree-folder-header').hasClass('clicked')) {
					$(this).parents('.tree-folder-header').addClass('clicked');
					$(this).parents('.tree-folder-header').click();
				}
			window.location = $(this).attr('data-target');
		});
		$('#Div1').on('loaded', function (evt, data) {
			$('.navigationNodes').click(function() {
				if(!$(this).parents('.tree-folder-header').hasClass('clicked')) {
					$(this).parents('.tree-folder-header').addClass('clicked');
					$(this).parents('.tree-folder-header').click();
				}
				window.location = $(this).attr('data-target');
			});
		});
		path = window.location.pathname.toLowerCase();
		if(path.search("mycourse.php") != -1)
			$('.course').parents('a').click();
		if(path.search("myexam.php") != -1) {
			$('.course').parents('a').click();
			var courseId = getUrlParameter('courseId');
			$('#C'+courseId).parents('.tree-folder-header').click();
		}
		if(path.search("chapter.php") != -1) {
			$('.course').parents('a').click();
			var courseId = getUrlParameter('courseId');
			var subjectId = getUrlParameter('subjectId');
			$('#C'+courseId).parents('.tree-folder-header').click();
			$('#B'+subjectId).parents('.tree-folder-header').click();
		}
		/*$('#Div1').on('opened', function (evt, data) {
			console.log('sub-folder opened: ', data);
		});
		$('#Div1').on('closed', function (evt, data) {
			console.log('sub-folder closed: ', data);
		});
		$('#Div1').on('selected', function (evt, data) {
			console.log('item selected: ', data);
		});*/
	});
	$('.course').on('click', function() {
		window.location = 'MyCourse.php';
	});
});