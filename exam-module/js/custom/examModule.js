var ApiEndPoint = '../api/index.php';
var MONTH = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

//variables to store exam details
var letters = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
var timer;
var totalTime = 0;
var examType = 0;
var endBeforeTime = 1;
var eachQuestionFlag = 0;
var switchFlag = false;
var gapTime = 0;
var gapTimer;
var sectionCount = 0;
var sectionSwitch = 0;
var sections = [];
var questions = [];
var answers = {};
var selectedQuestion = 0;
var questionTimer;
var attemptId;
var order = 0;
var resumeMode = 0;
var secondTimer = 0;
var autoSaveCounter = 60;
var timePauseFlag = false;
var showResult=1;
var showTill=1;
var showFrom=1;
var showdatetill='';
var showdateFrom='';
var courseId=0;
var s2MinTimer;
//var categories = [];
var ctrlKeyDown = false;
(function (global) {
    if(typeof (global) === "undefined") {
        throw new Error("window is undefined");
    }
    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";

        // making sure we have the fruit available for juice (^__^)
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };
    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };
	global.onload = function () {
		noBackPlease();

		// disables backspace on page except on input fields and textarea..
		document.body.onkeydown = function (e) {
			var elm = e.target.nodeName.toLowerCase();
			if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
				e.preventDefault();
			}
			/*if (
					(e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) || 
					(
						(e.which == 123 // F12 Key
						|| e.which == 116 // F5 Key
						|| e.which == 27 // Esc Key
						|| (ctrlKeyDown && e.which == 82)
						|| (ctrlKeyDown && e.which == 116)
						)
					)
				) {
				alert("You are not allowed to use refresh, ctrl or any function keys during the exam.");
				e.preventDefault();
			} else if (e.which == 17) {
				ctrlKeyDown=true;
			}*/
			// stopping event bubbling up the DOM tree..
			e.stopPropagation();
		};
	}
})(window);
$(function() {
	$(document).bind("contextmenu",function(e){
		e.preventDefault();
	});
	/*$(document).keydown(function(evtobj) {
		//alert(evtobj.keyCode);
		//if (evtobj.altKey || evtobj.ctrlKey){
			if (evtobj.keyCode == 123 // F12 Key
				|| evtobj.keyCode == 116 // F5 Key
				|| evtobj.keyCode == 27 // Esc Key
				|| (ctrlKeyDown && evtobj.keyCode == 82)
				|| (ctrlKeyDown && evtobj.keyCode == 116)
				) {
				alert("You are not allowed to use refresh, ctrl or any function keys during the exam.");
				return false;
			} else if (evtobj.keyCode == 17) {
				ctrlKeyDown=true;
			}
		//}
	});*/
	$(document).keyup(function(evtobj) {
		if (evtobj.keyCode == 17) {
			ctrlKeyDown = false;
		}
	});

	//fetchStudentDetail();
	checkExam();	
	
	function checkExam() {
		var req = {};
		var res;
		req.action = 'check-exam';
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0){
				alert(res.message);
				window.location = sitepathStudent+'subjects/' + res.subjectId;
			}				
			else {
				fetchResumeStatus();
				fetchCourseIds();
			}
		});
		/*try {
			$.ajax({
				url: ApiEndPoint,
				type: 'post',
				data: JSON.stringify(req),
			})
			.done(function() {
				res = $.parseJSON(res);
				if(res.status == 0){
					alert(res.message);
					window.location = sitepathStudent+'subjects/' + res.subjectId;
				}				
				else {
					fetchResumeStatus();
					fetchCourseIds();
				}
			})
			.fail(function() {
				alert("There was some error, please try again!");
			})
			.always(function() {
				alert("complete");
			});
		} catch (e) {
			alert('a new error');
		}*/
	}
	
	$('.tooltips').tooltip();
	//event listener for back question paper
	$('.question-paper-back').on('click', function() {
		$('#questionPaper').hide();
		$('#questions').show();
	});
	
	//event listener to show question paper
	$('#qppr').on('click', function() {
		$('#questionPaper').show();
		$('#questions').hide();
	});
	
	//event handler for submit exam button
	$('#submitExam').on('click', function() {
		if(endBeforeTime == 0) {
			alert('Please wait for the entire Exam time to end.');
			return;
		}
		var ex = (examType == 0)?'Exam':'Assignment';
		var con = confirm("Are you sure you want to submit this " + ex + "?");
		if(con) {
			/*if(examType == 1 && !eachQuestionFlag) {
				showAllAnswers();
			}*/
			submitExam();
		}
	});
	
	//event handler for yes and no radio buttons for assignment
	$('#yes').on('click', function() {
		if($(this).prop('checked')) {
			$('#startAssignment').attr('disabled', false);
			eachQuestionFlag = 1;
		}
		else
			$('#startAssignment').attr('disabled', true);
	});
	
	$('#no').on('click', function() {
		if($(this).prop('checked')) {
			$('#startAssignment').attr('disabled', false);
			eachQuestionFlag = 0;
		}
		else
			$('#startAssignment').attr('disabled', true);
	});
	
	//event handler for back to exam button
	$('#backToExam').on('click', function() {
		$('#instructions').hide();
		$('#questions').show();
	});
	
	//event handler for show instructions button
	$('#showInstructions').on('click', function() {
		$('#questions').hide();
		$('#instructions').show();
	});
	
	//event handler for clear responses button
	$('#clearResponses').on('click', function() {
		$('.question-container, .options, .optionsMul, .true_false').find('label').css('font-size', '');
		var needle = $('#questionId').val();
		if($('.question-button[data-id="'+needle+'"]').hasClass('btn-info')) {
			$('.question-button[data-id="'+needle+'"]').removeClass('btn-success');
			answers[needle].status = 3;
		}
		else {
			$('.question-button[data-id="'+needle+'"]').removeClass('btn-success btn-danger').addClass('btn-danger');
			answers[needle].status = 1;
		}
		answers[needle].answer = [];
		clearResponses();
	});
	
	//event handler for mark for review and next button
	$('#markReview').on('click', function() {
		if(!eachQuestionFlag || examType == 0)
			saveAnswer();
		var needle = $('#questionId').val();
		$('.question-button[data-id="'+needle+'"]').removeClass('btn-danger').addClass('btn-info');
		if($('.question-button[data-id="'+needle+'"]').hasClass('btn-success'))
			answers[needle].status = 4;
		else
			answers[needle].status = 3;
		$('.question-container, .options, .optionsMul, .true_false').find('label').css('font-size', '');
		$('input').prop('checked', false).val('');
		$('select').val(0);
		sendAnswerToServer();
		showNextQuestion(selectedQuestion + 1);
	});
	
	//event handler for start exam button
	$('#startExam').on('click', function() {
		var agreeChecked = $("#optionInstructions");
		if (!agreeChecked[0].checked) {
			alert("Please agree to exam instructions to proceed with the exam.");
		} else {
			startExamOps();
		}
	});

	function startExamOps() {
		$('#startExam').remove();
		increaseAttempt();
		$('#startAssignment').remove();
		$('#backToExam').show();
		$('#instructions').hide();
		$('#questions').show();
	}
	
	//event handler for start assignment button
	$('#startAssignment').on('click', function() {
		var agreeChecked = $("#optionInstructions");
		if (!agreeChecked[0].checked) {
			alert("Please agree to exam instructions to proceed with the exam.");
		} else {
			startAssignmentOps();
		}
	});

	function startAssignmentOps() {
		$('#startAssignment').remove();
		increaseAttempt();
		$('#askAnswer').remove();
		$('#startExam').remove();
		$('#backToExam').show();
		$('#instructions').hide();
		$('#questions').show();
		$('#markReview').text('Skip');
		$('#legend').text('Skipped');
		$('.toBeRemoved').remove();
		if(eachQuestionFlag) {
			$('#saveAndNext').html('Next');
			$('#clearResponses').hide();
		}
	}
	
	//event handler for save and next button
	$('#saveAndNext').on('click', function() {
		if(!eachQuestionFlag || examType == 0)
			saveAnswer();
		showNextQuestion(selectedQuestion+1);
		clearResponses();
		fillAnswer();
	});
	
	//event handler next button in assignment case of show answer section
	$('#showAnswer').on('click', function() {
		var flag = false;
		$.each($('input[type="checkbox"], input[type="radio"]'), function() {
			if($(this).prop('checked')) {
				flag = true;
				return;
			}
		});
		if(!flag) {
			$.each($('input[type="text"]'), function() {
				if($(this).val() != '') {
					flag = true;
					return;
				}
			});
			$.each($('select'), function() {
				if($(this).val() != 0) {
					flag = true;
					return;
				}
			});
		}
		if(!flag) {
			alert('Please select a answer or skip the question');
			return;
		}
		saveAnswer();
		showAnswerForQuestion();
		$(this).hide();
		if(eachQuestionFlag)
			$('#saveAndNext').text('Next');
		$('#saveAndNext').show();
	});
	
	//event handler for previous button
	$('#previous').on('click', function() {
		if(!eachQuestionFlag || examType == 0)
			saveAnswer();
		showNextQuestion(selectedQuestion-1);
		clearResponses();
		fillAnswer();
	});
	
	//getting student name
	function fetchStudentDetail() {
		var req = {};
		var res;
		req.action = 'get-student-name';
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message)
			else
				$('.username').text(res.name); //setting student name on page.
		});
	}
	
	//getting exam details to initiate view.
	function fetchExamDetails() {
		var req = {};
		var res;
		req.action = 'get-exam-for-student';
		req.examId = getUrlParameter('examId');
		req.resumeMode = resumeMode;
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status == 0)
				alert(res.message);
			else {
				if(res.exam.type == "Exam")
					handlerExam(res);
				else
					handlerAssignment(res);
			}
		});
	}
	
	//function to fetch resume status of the exam or assignment
	function fetchResumeStatus() {
		var req = {};
		var res;
		req.action = 'get-exam-resume-status';
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				if(res.data) {
					eachQuestionFlag = parseInt(res.data.eachQuestionFlag);
					attemptId = res.data.id;
				}
				resumeMode = res.resumeMode;
				fetchExamDetails();
			}
		});
	}
	//function to fetch a courseId
	function fetchCourseIds() {
		var req = {};
		var res;
		req.action = 'get-exam-courseId';
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				courseId=res.courseId;
			}
		});
	}
	
	//function to increase number of attempt in database
	function increaseAttempt() {
		var req = {};
		var res;
		req.action = "increase-attempt";
		req.examId = getUrlParameter('examId');
		req.resumeMode = resumeMode;
		req.attemptId = attemptId;
		req.eachQuestionFlag = eachQuestionFlag;
		$('#loader').show();
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				//console.log(sections);
				attemptId = res.attemptId;
				if(examType == 0)
					if(resumeMode == 0)
						fetchQuestions(sections[sectionCount].id);
					else
						fetchQuestionsInResume(sections[sectionCount].id);
				else
					if(resumeMode == 0)
						fetchQuestionsForAssignment(sections[sectionCount].id);
					else
						fetchQuestionsForAssignmentInResume(sections[sectionCount].id);
			}
		});
	}
	
	//function to handle exams
	function handlerExam(data) {
		//console.log(data);
		var today= $.now();
		if((data.exam.showResultTill == 2 && today< data.exam.showResultTillDate)||data.exam.showResultTill == 1)
					{	
						showResult=1;
						
					}
		else{
			showResult=2;
			showTill=2;
			showdatetill=data.exam.showResultTillDate;
			var tillDate = new Date(parseInt(showdatetill));
			if(tillDate!='' || tillDate!=null)
			{
				var tsd = tillDate.getDate() + ' ' + MONTH[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
				if(tillDate.getHours() < 10)
					tsd += '0' + tillDate.getHours();
				else
					tsd += tillDate.getHours();
				tsd += ':';
				if(tillDate.getMinutes() < 10)
					tsd += '0' + tillDate.getMinutes();
				else
					tsd += tillDate.getMinutes();
			}
			showdatetill=tsd;
		}
		
		if(data.exam.showResult == 'after' && data.exam.endDate!="")
		{
			if(today> data.exam.endDate)
			{
				showResult=1;
			}			
			else{
				showResult=2;
				showFrom=2;
				var tillDate = new Date(parseInt(data.exam.endDate));
				if(tillDate!='' || tillDate!=null)
				{
					var tsd = tillDate.getDate() + ' ' + MONTH[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
					if(tillDate.getHours() < 10)
						tsd += '0' + tillDate.getHours();
					else
						tsd += tillDate.getHours();
					tsd += ':';
					if(tillDate.getMinutes() < 10)
						tsd += '0' + tillDate.getMinutes();
					else
						tsd += tillDate.getMinutes();
				}
				showdateFrom=tsd;
			}
		}
		
		$('#startExam').show();
		examType = 0;
		if(data.powerOption == 0)
			timePauseFlag = true;
		$('#name').text(data.exam.name);
		var htmlInstructions = data.exam.instructions;
		if(htmlInstructions.indexOf("Instructions:") >= 0) {
			htmlInstructions = htmlInstructions.replace("Instructions:", "<h2>Instructions:</h2>");
		}
		while(htmlInstructions.indexOf("<br><br>") >= 0) {
			htmlInstructions = htmlInstructions.replace("<br><br>", "<br>");
		}
		while(htmlInstructions.indexOf("<hr><br>") >= 0) {
			htmlInstructions = htmlInstructions.replace("<hr><br>", "<hr>");
		}
		while(htmlInstructions.indexOf("<br><hr>") >= 0) {
			htmlInstructions = htmlInstructions.replace("<br><hr>", "<hr>");
		}
		$('#inst').html(htmlInstructions);
		$('#instructions').addClass('box-instructions');
		sectionSwitch = parseInt(data.exam.tt_status);
		if(data.exam.tt_status == 0) {
			totalTime = parseInt(data.exam.totalTime);
			switchFlag = true;
		}
		else {
			gapTime = parseInt(data.exam.gapTime);
			totalTime = parseInt(data.sections[sectionCount].time);
		}
		if(totalTime != 0)
			formatTime(totalTime);
		$('#timeText').attr('data-original-title', 'Time Left');
		sections = data.sections;
		if(data.exam.endBeforeTime == 0) {
			endBeforeTime = 0;
			//according to ARC-452
 			//$('#submitExam').attr('disabled', true);
		}
		else
			endBeforeTime = 1;
		var html = '';
		for(var i=0; i<sections.length; i++) {
			html += ''
			+ '<a style="border-radius: 4px 4px 0px 0px;" class="btn-space btn btn-md disable-course section-switch" data-number="'+i+'" data-id="' + sections[i].id + '" DISABLED>' + sections[i].name + '</a>';
		}
		$('#sectionButtons').append(html);
		
		//event handler for section buttons
		$('.section-switch').on('click', function() {
			if(!switchFlag) {
				$('.section-switch').attr('disabled', true);
				$(this).attr('disabled', false);
			}
			var number = parseInt($(this).attr('data-number'));
			$('.section[data-id="'+sectionCount+'"]').hide();
			sectionCount = number;
			$('.section-switch').removeClass('btn-danger').addClass('disable-course');
			$(this).addClass('btn-danger').removeClass('disable-course');
			$('.section[data-id="'+sectionCount+'"]').show();
			$('#sectionName').text($('.section-switch:eq('+sectionCount+')').text());
			showNextQuestion(0);
			clearResponses();
			fillAnswer();
		});
		
		if(sectionSwitch == 0)
			$('.section-switch').attr('disabled', false);
		else
			$('.section-switch:eq(' + sectionCount + ')').attr('disabled', false);
		if(resumeMode == 1) {
			//$('#startExam').click();
			startExamOps();
		}
	}
	
	//function to handle assignments
	function handlerAssignment(data) {
		var today= $.now();
		if((data.exam.showResultTill == 2 && today< data.exam.showResultTillDate)||data.exam.showResultTill == 1)
					{
						showResult=1;
						showTill=2;
					}
		else{
			showResult=2;
			showdatetill=data.exam.showResultTillDate;
			var tillDate = new Date(parseInt(showdatetill));
			if(tillDate!='' || tillDate!=null)
			{
				var tsd = tillDate.getDate() + ' ' + MONTH[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
				if(tillDate.getHours() < 10)
					tsd += '0' + tillDate.getHours();
				else
					tsd += tillDate.getHours();
				tsd += ':';
				if(tillDate.getMinutes() < 10)
					tsd += '0' + tillDate.getMinutes();
				else
					tsd += tillDate.getMinutes();
			}
			showdatetill=tsd;
		}
		
		if(data.exam.showResult == 'after' && data.exam.endDate!="")
		{
			if(today> data.exam.endDate)
			{
				showResult=1;
			}			
			else{
				showResult=2;
				showFrom=2;
				var tillDate = new Date(parseInt(data.exam.endDate));
				if(tillDate!='' || tillDate!=null)
				{
					var tsd = tillDate.getDate() + ' ' + MONTH[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
					if(tillDate.getHours() < 10)
						tsd += '0' + tillDate.getHours();
					else
						tsd += tillDate.getHours();
					tsd += ':';
					if(tillDate.getMinutes() < 10)
						tsd += '0' + tillDate.getMinutes();
					else
						tsd += tillDate.getMinutes();
				}
				showdateFrom=tsd;
			}
		}
		examType = 1;
		$('#startExam').hide();
		$('#startAssignment').show().attr('disabled', true);
		$('#askAnswer').parent().show();
		$('#name').text(data.exam.name);
		var htmlInstructions = data.exam.instructions;
		if(htmlInstructions.indexOf("Instructions:") >= 0) {
			htmlInstructions = htmlInstructions.replace("Instructions:", "<h2>Instructions:</h2>");
		}
		while(htmlInstructions.indexOf("<br><br>") >= 0) {
			htmlInstructions = htmlInstructions.replace("<br><br>", "<br>");
		}
		while(htmlInstructions.indexOf("<hr><br>") >= 0) {
			htmlInstructions = htmlInstructions.replace("<hr><br>", "<hr>");
		}
		while(htmlInstructions.indexOf("<br><hr>") >= 0) {
			htmlInstructions = htmlInstructions.replace("<br><hr>", "<hr>");
		}
		$('#inst').html(htmlInstructions);
		$('#instructions').addClass('box-instructions');
		$('#timeText').attr('data-original-title', 'Time Taken');
		formatTime(0);
		switchFlag = true;
		sections = data.sections;
		var html = '';
		for(var i=0; i<sections.length; i++) {
			html += ''
						+ '<a style="border-radius: 4px 4px 0px 0px;" class="btn-space btn btn-md disable-course section-switch" data-number="'+i+'" data-id="' + sections[i].id + '">' + sections[i].name + '</a> '
					+ '';
		}
		$('#sectionButtons').append(html);
		
		//event handler for section buttons
		$('.section-switch').on('click', function() {
			var number = parseInt($(this).attr('data-number'));
			$('.section[data-id="'+sectionCount+'"]').hide();
			sectionCount = number;
			$('.section-switch').removeClass('btn-danger').addClass('disable-course');
			$(this).addClass('btn-danger').removeClass('disable-course');
			$('.section[data-id="'+sectionCount+'"]').show();
			$('#sectionName').text($('.section-switch:eq('+sectionCount+')').text());
			showNextQuestion(0);
			clearResponses();
			fillAnswer();
		});
		if(resumeMode == 1) {
			//$('#startAssignment').click();
			startAssignmentOps();
		}
	}
	
	//function to format time
	function formatTime(time) {
		if(time < 0)
                    return;
		var hours = 0;
		var minutes = 0;
		if(time >= 60) {
                    hours = Math.floor( time / 60 );
                    minutes = time - ( hours * 60 )
		}
		else
			minutes = time;
		var formatHour = (hours > 9) ? hours + '' : '0' + hours;
		var formatMinute = (minutes > 9) ? minutes + '' : '0' + minutes;
		$('#timeNow').text(formatHour + ':' + formatMinute + ':');
		$('#timeSecond').text('00');
	}
		
	//function to fetch questions and categories for exam
	function fetchQuestions(sectionId) {
		var randomQuestions = false;
		if(sections[sectionCount].randomQuestions == 1)
			randomQuestions = true;
		var req = {};
		var res;
		req.action = 'get-questions-for-exam';
		req.random = randomQuestions;
		req.sectionId = sectionId;
		req.resumeMode = resumeMode;
		req.attemptId = attemptId;
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message)
			else {
				questions[sectionCount] = [];
				for(var i=0; i<res.questions.length; i++) {
					for(var j=0; j<res.questions[i].length; j++) {
						res.questions[i][j].correct = res.categories[i].correct;
						res.questions[i][j].wrong = res.categories[i].wrong;
						questions[sectionCount].push(res.questions[i][j]);
						var l = res.questions[i][j].id;
						answers[l] = {};
						answers[l].questionId = l;
						answers[l].questionType = res.questions[i][j].questionType;
						answers[l].timer = 0;
						answers[l].status = 0;
						answers[l].parentId = res.questions[i][j].parentId;
						answers[l].correct = res.questions[i][j].correct;
						answers[l].wrong = res.questions[i][j].wrong;
						answers[l].sectionId = sections[sectionCount].id
						answers[l].order = order;
						order++;
					}
				}
				//console.log(questions[sectionCount]);
				var html = '<div class="section'+sectionCount+' section" data-id="'+sectionCount+'">';
				for(var i=0; i<questions[sectionCount].length; i++) {
					html += '<a class="btn btn-white question-button" data-id="' + questions[sectionCount][i].id + '">' + (i+1) + '</a>';
				}
				html += '</div>';
				$('#questionButtons').append(html);
				
				//creating event handlers for each question
				$('.question-button').on('click', function() {
					if(!eachQuestionFlag || examType == 0)
						saveAnswer();
					var qNum = parseInt($(this).text()) - 1;
					showNextQuestion(qNum);
					clearResponses();
					fillAnswer();
				});
				$('.section-switch:eq(' + sectionCount + ')').removeClass('btn-danger').addClass('disable-course');
				sectionCount++;
				$('.section-switch:eq(' + sectionCount + ')').removeClass('disable-course').addClass('btn-danger');
				if(sectionCount < sections.length && sectionCount >= 0) {
					fetchQuestions(sections[sectionCount].id);
				}
				else {
					sectionCount = 0;
					$('#container-for-questions').show();
					startExam();
				}
			}
		});
	}
	
	//function to fetch questions in resume mode
	function fetchQuestionsInResume(sectionId) {
		var req = {};
		var res;
		req.action = 'get-questions-for-exam-in-resume';
		req.sectionId = sectionId;
		req.attemptId = attemptId;
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message)
			else {
				questions[sectionCount] = [];
				for(var i=0; i<res.questions.length; i++) {
					res.questions[i].id = res.questions[i].questionId;
					questions[sectionCount].push(res.questions[i]);
					var l = res.questions[i].id;
					answers[l] = {};
					answers[l].questionId = l;
					answers[l].questionType = res.questions[i].questionType;
					answers[l].timer = parseInt(res.questions[i].time);
					answers[l].status = parseInt(res.questions[i].status);
					answers[l].parentId = res.questions[i].parentId;
					answers[l].correct = res.questions[i].correct;
					answers[l].wrong = res.questions[i].wrong;
					answers[l].sectionId = sections[sectionCount].id;
					if(res.questions[i].status == 2 || res.questions[i].status == 4) {
						if(answers[l].questionType == 0 || answers[l].questionType == 1 || answers[l].questionType == 7) {
							//console.log(res.questions[i]);
							if(res.questions[i].userAns/*.length > 0*/)
								answers[l].answer = res.questions[i].userAns[0].answer;
						}
						else if(answers[l].questionType == 2 || answers[l].questionType == 3 || answers[l].questionType == 6) {
							if(res.questions[i].userAns/*.length > 0*/) {
								answers[l].answer = [];
								for(var j = 0; j < res.questions[i].userAns.length; j++)
									answers[l].answer.push(res.questions[i].userAns[j].answer);
							}
						}
						else if(answers[l].questionType == 5) {
							if(res.questions[i].childQuestions.length > 0) {
								answers[l].childs = [];
								for(var j = 0; j < res.questions[i].childQuestions.length; j++) {
									var temp = {};
									if(res.questions[i].childQuestions[j].userAns/*.length > 0*/) {
										if(res.questions[i].childQuestions[j].questionType == 0 || res.questions[i].childQuestions[j].questionType == 1) {
											temp.answer = res.questions[i].childQuestions[j].userAns[0].answer;
										}
										else if(res.questions[i].childQuestions[j].questionType == 2 || res.questions[i].childQuestions[j].questionType == 6) {
											temp.answer = [];
											for(var k = 0; k < res.questions[i].childQuestions[j].userAns.length; k++) {
												temp.answer.push(res.questions[i].childQuestions[j].userAns[k].answer);
											}
										}
									}
									temp.id = res.questions[i].childQuestions[j].id;
									temp.questionType = res.questions[i].childQuestions[j].questionType;
									temp.parentId = res.questions[i].childQuestions[j].parentId;
									answers[l].childs.push(temp);
								}
							}
						}
					}
				}
				console.log(questions[sectionCount]);
				var html = '<div class="section'+sectionCount+' section" data-id="'+sectionCount+'">';
				var buttonClass = '';
				for(var i=0; i<questions[sectionCount].length; i++) {
					if(questions[sectionCount][i].status ==  0)
						buttonClass = 'btn-white';
					else if(questions[sectionCount][i].status ==  1)
						buttonClass = 'btn-danger';
					else if(questions[sectionCount][i].status ==  2)
						buttonClass = 'btn-success';
					else if(questions[sectionCount][i].status ==  3)
						buttonClass = 'btn-info';
					else if(questions[sectionCount][i].status ==  4)
						buttonClass = 'btn-success btn-info';
					html += '<a class="btn question-button ' + buttonClass + '" data-id="' + questions[sectionCount][i].id + '">' + (i+1) + '</a>';
				}
				html += '</div>';
				$('#questionButtons').append(html);
				
				//creating event handlers for each question
				$('.question-button').on('click', function() {
					if(!eachQuestionFlag || examType == 0)
						saveAnswer();
					var qNum = parseInt($(this).text()) - 1;
					showNextQuestion(qNum);
					clearResponses();
					fillAnswer();
				});
				//$('.section-switch:eq(' + sectionCount + ')').removeClass('btn-success').addClass('disable-course');
				$('.section-switch:eq(' + sectionCount + ')').removeClass('btn-danger').addClass('disable-course');
				sectionCount++;
				$('.section-switch:eq(' + sectionCount + ')').removeClass('disable-course').addClass('btn-danger');
				if(sectionCount < sections.length && sectionCount >= 0) {
					fetchQuestionsInResume(sections[sectionCount].id);
				}
				else {
					sectionCount = 0;
					fetchAndSetTime();
				}
			}
		});
	}
	
	//function to fetch questions and categories for assignment
	function fetchQuestionsForAssignment(sectionId) {
		var randomQuestions = false;
		if(sections[sectionCount].randomQuestions == 1)
			randomQuestions = true;
		var req = {};
		var res;
		req.action = 'get-questions-for-assignment';
		req.random = randomQuestions;
		req.sectionId = sectionId;
		req.resumeMode = resumeMode;
		req.attemptId = attemptId;
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message)
			else {
				questions[sectionCount] = [];
				for(var i=0; i<res.questions.length; i++) {
					for(var j=0; j<res.questions[i].length; j++) {
						res.questions[i][j].correct = res.categories[i].correct;
						res.questions[i][j].wrong = res.categories[i].wrong;
						questions[sectionCount].push(res.questions[i][j]);
						var l = res.questions[i][j].id;
						answers[l] = {};
						answers[l].questionId = l;
						answers[l].questionType = res.questions[i][j].questionType;
						answers[l].timer = 0;
						answers[l].status = 0;
						answers[l].parentId = res.questions[i][j].parentId;
						answers[l].correct = res.questions[i][j].correct;
						answers[l].wrong = res.questions[i][j].wrong;
						answers[l].sectionId = sections[sectionCount].id
						answers[l].order = order;
						order++;
					}
				}
				var html = '<div class="section'+sectionCount+' section" data-id="'+sectionCount+'">';
				for(var i=0; i<questions[sectionCount].length; i++) {
					html += '<a class="btn btn-white question-button" data-id="' + questions[sectionCount][i].id + '">' + (i+1) + '</a>';
				}
				html += '</div>';
				$('#questionButtons').append(html);
				
				//creating event handlers for each question
				$('.question-button').on('click', function() {
					if(!eachQuestionFlag || examType == 0)
						saveAnswer();
					var qNum = parseInt($(this).text()) - 1;
					showNextQuestion(qNum);
					clearResponses();
					fillAnswer();
				});
				$('.section-switch:eq(' + sectionCount + ')').removeClass('btn-danger').addClass('disable-course');
				sectionCount++;
				$('.section-switch:eq(' + sectionCount + ')').removeClass('disable-course').addClass('btn-danger');
				if(sectionCount < sections.length && sectionCount >= 0) {
					fetchQuestionsForAssignment(sections[sectionCount].id);
				}
				else {
					sectionCount = 0;
					$('#container-for-questions').show();
					startAssignment();
				}
			}
		});
	}
	
	//function to fetch questions in resume mode
	function fetchQuestionsForAssignmentInResume(sectionId) {
		var req = {};
		var res;
		req.action = 'get-questions-for-assignment-in-resume';
		req.sectionId = sectionId;
		req.attemptId = attemptId;
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message)
			else {
				questions[sectionCount] = [];
				for(var i=0; i<res.questions.length; i++) {
					res.questions[i].id = res.questions[i].questionId;
					questions[sectionCount].push(res.questions[i]);
					var l = res.questions[i].id;
					answers[l] = {};
					answers[l].questionId = l;
					answers[l].questionType = res.questions[i].questionType;
					answers[l].timer = parseInt(res.questions[i].time);
					answers[l].status = parseInt(res.questions[i].status);
					answers[l].parentId = res.questions[i].parentId;
					answers[l].correct = res.questions[i].correct;
					answers[l].wrong = res.questions[i].wrong;
					answers[l].sectionId = sections[sectionCount].id;
					if(res.questions[i].status == 2 || res.questions[i].status == 4) {
						if(answers[l].questionType == 0 || answers[l].questionType == 1 || answers[l].questionType == 7) {
							if(res.questions[i].userAns.length > 0)
								answers[l].answer = res.questions[i].userAns[0].answer;
						}
						else if(answers[l].questionType == 2 || answers[l].questionType == 3 || answers[l].questionType == 6) {
							if(res.questions[i].userAns.length > 0) {
								answers[l].answer = [];
								for(var j = 0; j < res.questions[i].userAns.length; j++)
									answers[l].answer.push(res.questions[i].userAns[j].answer);
							}
						}
						else if(answers[l].questionType == 5) {
							if(res.questions[i].childQuestions.length > 0) {
								answers[l].childs = [];
								for(var j = 0; j < res.questions[i].childQuestions.length; j++) {
									var temp = {};
									if(res.questions[i].childQuestions[j].questionType == 0 || res.questions[i].childQuestions[j].questionType == 1) {
										temp.answer = res.questions[i].childQuestions[j].userAns[0].answer;
									}
									else if(res.questions[i].childQuestions[j].questionType == 2 || res.questions[i].childQuestions[j].questionType == 6) {
										temp.answer = [];
										for(var k = 0; k < res.questions[i].childQuestions[j].userAns.length; k++) {
											temp.answer.push(res.questions[i].childQuestions[j].userAns[k].answer);
										}
									}
									temp.id = res.questions[i].childQuestions[j].id;
									temp.questionType = res.questions[i].childQuestions[j].questionType;
									temp.parentId = res.questions[i].childQuestions[j].parentId;
									answers[l].childs.push(temp);
								}
							}
						}
					}
				}
				//console.log(answers);
				var html = '<div class="section'+sectionCount+' section" data-id="'+sectionCount+'">';
				var buttonClass = '';
				for(var i=0; i<questions[sectionCount].length; i++) {
					if(questions[sectionCount][i].status ==  0)
						buttonClass = 'btn-white';
					else if(questions[sectionCount][i].status ==  1)
						buttonClass = 'btn-danger';
					else if(questions[sectionCount][i].status ==  2)
						buttonClass = 'btn-success';
					else if(questions[sectionCount][i].status ==  3)
						buttonClass = 'btn-info';
					else if(questions[sectionCount][i].status ==  4)
						buttonClass = 'btn-success btn-info';
					html += '<a class="btn question-button ' + buttonClass + '" data-id="' + questions[sectionCount][i].id + '">' + (i+1) + '</a>';
				}
				html += '</div>';
				$('#questionButtons').append(html);
				
				//creating event handlers for each question
				$('.question-button').on('click', function() {
					if(!eachQuestionFlag || examType == 0)
						saveAnswer();
					var qNum = parseInt($(this).text()) - 1;
					showNextQuestion(qNum);
					clearResponses();
					fillAnswer();
				});
				$('.section-switch:eq(' + sectionCount + ')').removeClass('btn-danger').addClass('disable-course');
				sectionCount++;
				$('.section-switch:eq(' + sectionCount + ')').removeClass('disable-course').addClass('btn-danger');
				if(sectionCount < sections.length && sectionCount >= 0) {
					fetchQuestionsForAssignmentInResume(sections[sectionCount].id);
				}
				else {
					sectionCount = 0;
					fetchAndSetTime();
				}
			}
		});
	}
	
	//function to fetch and set time
	function fetchAndSetTime() {
		var req = {};
		var res;
		req.action = 'get-time-and-section';
		req.attemptId = attemptId;
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				totalTime = Math.floor(parseInt(res.time)/60);
				secondTimer = res.time - (totalTime * 60);
				totalTime--;
				if(res.sectionCount != undefined)
					sectionCount = res.sectionCount;
				$('#container-for-questions').show();
				if(examType == 0)
					startExam();
				else
					startAssignment();
			}
		});
	}
	
	//function to show next question
	function showNextQuestion(number) {
		$('#saveAndNext').text('Save and next');
		stopQuestionTimer();
		$('#previous').show();
		$('.question-container, .options, .optionsMul, .true_false, .audio').find('label').css('font-size', '');
		if(number < 0) {
			//console.log("I came here");
			if(sectionCount - 1 < 0)
				return;
			else {
				if($('.section-switch:eq('+(sectionCount-1)+')').attr('disabled') == "disabled") {
					alert('You can\'t go back to the previous section.');
					return;
				}
				else {
					sectionCount--;
					number = questions[sectionCount].length - 1;
					selectedQuestion = number;
					$('.section').hide();
					$('.section'+sectionCount).show();
					$('#sectionName').text($('.section-switch:eq('+sectionCount+')').text());
				}
			}
		}
		else if(number > questions[sectionCount].length - 1) {
			number--;
			if(sectionCount + 1 >= sections.length) {
				$('#saveAndNext').text('Save');
				return;
				//$('#submitExam').click();
			}
			else {
				if($('.section-switch:eq('+(sectionCount+1)+')').attr('disabled') == "disabled") {
					number = 0;
				}
				else {
					$('.section-switch:eq(' + sectionCount + ')').removeClass('btn-danger').addClass('disable-course');
					sectionCount++;
					$('.section-switch:eq(' + sectionCount + ')').removeClass('disable-course').addClass('btn-danger');
					selectedQuestion = 0;
					number = 0;
					$('.section').hide();
					$('.section'+sectionCount).show();
					$('#sectionName').text($('.section-switch:eq('+sectionCount+')').text());
				}
			}
		}
		//now changing the text of saveAndNext button as per bug ARC-595
		if(number + 1 >= questions[sectionCount].length) {
			$('#saveAndNext').text('Save and next section');
			if(sectionCount + 1 >= sections.length)
				$('#saveAndNext').text('Save');
		}
		if(examType == 1 && eachQuestionFlag) {
			$('#saveAndNext').hide();
			$('#showAnswer').show();
		}
		$('#compro').hide();
		$('.section').hide();
		$('.section'+sectionCount).show();
		$('#answer').show();
		$('#question').show();
		$('#questions').show();
		$('.generated').remove();
		$('#questionNumber').text(number + 1);
		$('#marks').text('Marks +' + questions[sectionCount][number].correct + '/-' + questions[sectionCount][number].wrong);
		if($('.question-button[data-id="' + questions[sectionCount][number].id + '"]').hasClass('btn-white')) {
			$('.question-button[data-id="' + questions[sectionCount][number].id + '"]').removeClass('btn-white').addClass('btn-danger');
			answers[questions[sectionCount][number].id].status = 1;
		}
		$('#questionId').val(questions[sectionCount][number].id);
		$('#question').html(questions[sectionCount][number]['question']);
		MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
		$.each($('#question p'), function() {
			if($(this).html().indexOf('<img ') < 0 ) {
				if($(this).text().trim() == '')
					$(this).remove();
			}
		});
		selectedQuestion = number;
		//console.log(questions[sectionCount][number]['questionType']);
		//handling question according to its type
		$('#jsPlayer').html('');
		if(questions[sectionCount][number]['parentId'] == 0) {
			if(questions[sectionCount][number]['questionType'] == 0) {
				handleOptions();
			}
			else if(questions[sectionCount][number]['questionType'] == 1) {
				handleTrueFalse();
			}
			else if(questions[sectionCount][number]['questionType'] == 2) {
				handleFtb();
			}
			else if(questions[sectionCount][number]['questionType'] == 3) {
				handleMtf();
			}
			else if(questions[sectionCount][number]['questionType'] == 6) {
				handleOptionsMul();
			}
			else if(questions[sectionCount][number]['questionType'] == 5) {
				handleDpn();
			}
			else if(questions[sectionCount][number]['questionType'] == 7) {
				handleAudio();
			}
		}
		else
			handleCmp();
		
		//code to make selected option bold
		$('input[type="radio"]').off('change');
		$('input[type="radio"]').on('change', function() {
			$(this).parents('.question-container, .options, .optionsMul, .true_false').find('label').css('font-size', '');
			if($(this).prop('checked')) {
				$(this).parents('label').css('font-size', 'large')
			}
		});
		startQuestionTimer(questions[sectionCount][number].id);
		if(sectionCount == 0 && number == 0)
			$('#previous').hide();
		//code to save answer as soon as user sees a question
		sendAnswerToServer();
	}
	
	//function to start exam
	function startExam() {
		$('#loader').hide();
		examTimerStart();
		$('#timeNow').parents('.js-timer:eq(0)').show();
		for(var i=0; i<sections.length; i++) {
			$('.section'+i).hide();
		}
		$('.section'+sectionCount).show();
		if(resumeMode == 0)
			sendQuestions();
		else {
			if(sectionSwitch == 0) {
				var n = $('.question-button.btn-danger:eq(-1)').parents('.section').attr('data-id');
				if(n!= undefined)
					sectionCount = parseInt(n);
			}
			var n = $('.section[data-id="' + sectionCount + '"] .btn-danger.question-button:eq(-1)').text();
			if(n != '') {
				showNextQuestion(parseInt(n)-1);
				$('.section-switch:eq('+sectionCount+')').trigger("click"); // to solve the section select issue
				return;
			}
		}
		$('.generated .temp').remove();
		$('#questionNumber').text(1);
		$('.question-button[data-id="'+questions[sectionCount][0].id+'"]').removeClass('btn-white').addClass('btn-danger');
		if(!answers[questions[sectionCount][0].id].status)
			answers[questions[sectionCount][0].id].status = 1;
		$('#questionId').val(questions[sectionCount][0].id);
		stopQuestionTimer();
		startQuestionTimer(questions[sectionCount][0].id);
		$('#question').html(questions[sectionCount][0]['question']);
		selectedQuestion = 0;
		$('#sectionName').text($('.section-switch:eq('+sectionCount+')').text());
		$('#compro').hide();
		$('#answer').show();
		$('#question').show();
		
		//handling question according to its type
		if(questions[sectionCount][0]['parentId'] == 0) {
			if(questions[sectionCount][0]['questionType'] == 0) {
				handleOptions();
			}
			else if(questions[sectionCount][0]['questionType'] == 1) {
				handleTrueFalse();
			}
			else if(questions[sectionCount][0]['questionType'] == 2) {
				handleFtb();
			}
			else if(questions[sectionCount][0]['questionType'] == 3) {
				handleMtf();
			}
			else if(questions[sectionCount][0]['questionType'] == 5) {
				handleDpn();
			}
			else if(questions[sectionCount][0]['questionType'] == 6) {
				handleOptionsMul();
			}
			else if(questions[sectionCount][0]['questionType'] == 7) {
				handleAudio();
			}
		}
		else
			handleCmp();
		$('.section-switch:eq('+sectionCount+')').trigger("click");
	}
	
	//function to start assignment
	function startAssignment() {
		$('#loader').hide();
		assignmentTimerStart();
		$('#timeNow').parents('.js-timer:eq(0)').show();
		for(var i=0; i<sections.length; i++) {
			$('.section'+i).hide();
		}
		$('.section[data-id="' + sectionCount + '"]').show();
		if(resumeMode == 0)
			sendQuestions();
		else {
			if(sectionSwitch == 0) {
				var n = $('.question-button.btn-danger:eq(-1)').parents('.section').attr('data-id');
				if(n!= undefined)
					sectionCount = parseInt(n);
			}
			var n = $('.section[data-id="' + sectionCount + '"] .btn-danger.question-button:eq(-1)').text();
			if(n != '') {
				showNextQuestion(parseInt(n)-1);
				$('.section-switch:eq('+sectionCount+')').trigger("click"); // to solve the section select issue
				return;
			}
		}
		$('.generated .temp').remove();
		$('#questionNumber').text(1);
		//console.log(questions[sectionCount]);
		$('.question-button[data-id="'+questions[sectionCount][0].id+'"]').removeClass('btn-white').addClass('btn-danger');
		if(!answers[questions[sectionCount][0].id].status)
			answers[questions[sectionCount][0].id].status = 1;
		$('#questionId').val(questions[sectionCount][0].id);
		stopQuestionTimer();
		startQuestionTimer(questions[sectionCount][0].id);
		$('#question').html(questions[sectionCount][0]['question']);
		selectedQuestion = 0;
		$('#sectionName').text($('.section-switch:eq(0)').text());
		$('#compro').hide();
		$('#answer').show();
		$('#question').show();
		if(examType == 1 && eachQuestionFlag) {
			$('#saveAndNext').hide();
			$('#showAnswer').show();
		}
		//handling question according to its type
		if(questions[sectionCount][0]['parentId'] == 0) {
			if(questions[sectionCount][0]['questionType'] == 0) {
				handleOptions();
			}
			else if(questions[sectionCount][0]['questionType'] == 1) {
				handleTrueFalse();
			}
			else if(questions[sectionCount][0]['questionType'] == 2) {
				handleFtb();
			}
			else if(questions[sectionCount][0]['questionType'] == 3) {
				handleMtf();
			}
			else if(questions[sectionCount][0]['questionType'] == 5) {
				handleDpn();
			}
			else if(questions[sectionCount][0]['questionType'] == 6) {
				handleOptionsMul();
			}
			else if(questions[sectionCount][0]['questionType'] == 7) {
				handleAudio();
			}
		}
		else
			handleCmp();
		$('.section-switch:eq(0)').click();
	}
	
	//function to handle MCQ questions
	function handleOptions() {
		$('#answer .optionsMul, #answer .ftb, #answer .mtf, #answer .dpn, #answer .true_false, #answer .audio').hide();
		$('#answer .options').show();
		if(questions[sectionCount][selectedQuestion].answer.length > 2) {
			addOptions(questions[sectionCount][selectedQuestion].answer.length - 2);
		}
		for(var i=0; i<questions[sectionCount][selectedQuestion].answer.length; i++) {
			$('#answer .options input[type="radio"]:eq('+i+')').val(questions[sectionCount][selectedQuestion].answer[i].option);
			$('#answer .options .opt-ans:eq('+i+')').html(questions[sectionCount][selectedQuestion].answer[i].option);
		}
	}

	//function to handle MCQ questions
	function handleAudio() {
		$('#answer .options, #answer .optionsMul, #answer .ftb, #answer .mtf, #answer .dpn, #answer .true_false').hide();
		$('#answer .audio').show();
		//console.log(questions[sectionCount][selectedQuestion]);
		if(questions[sectionCount][selectedQuestion].answer.length > 2) {
			addAudioOptions(questions[sectionCount][selectedQuestion].answer.length - 2);
		}
		for(var i=0; i<questions[sectionCount][selectedQuestion].answer.length; i++) {
			$('#answer .audio input[type="radio"]:eq('+i+')').val(questions[sectionCount][selectedQuestion].answer[i].option);
			$('#answer .audio .opt-ans:eq('+i+')').html(questions[sectionCount][selectedQuestion].answer[i].option);
		}
		var mp3 = questions[sectionCount][selectedQuestion].audio;
		if (!!mp3) {
			$('#jsPlayer').html(
				'<audio controls="control" id="audioShowcasePlayer" preload="none" src="'+mp3+'" type="audio/mp3"></audio>'
			);
			$('#jsPlayer').attr('data-path', mp3);
			player = new MediaElementPlayer('#audioShowcasePlayer', {type: 'audio/mp3'});
			var sources = [
				{ src: mp3, type: 'audio/mp3' }
			];

			player.setSrc(sources);
			player.load();
		}
	}
	
	//function to handle MAQ questions
	function handleOptionsMul() {
		$('#answer .options, #answer .ftb, #answer .mtf, #answer .dpn, #answer .true_false, #answer .audio').hide();
		$('#answer .optionsMul').show();
		if(questions[sectionCount][selectedQuestion].answer.length > 2) {
			addOptionsMul(questions[sectionCount][selectedQuestion].answer.length - 2);
		}
		for(var i=0; i<questions[sectionCount][selectedQuestion].answer.length; i++) {
			$('#answer .optionsMul input[type="checkbox"]:eq('+i+')').val(questions[sectionCount][selectedQuestion].answer[i].option);
			$('#answer .optionsMul .optMul-ans:eq('+i+')').html(questions[sectionCount][selectedQuestion].answer[i].option);
		}
	}
	
	//function to handle T/F questions
	function handleTrueFalse() {
		$('#answer .optionsMul, #answer .ftb, #answer .mtf, #answer .dpn, #answer .options, #answer .audio').hide();
		$('#answer .true_false').show();
	}
	
	//function to handle MTF questions
	function handleMtf() {
		$('#answer .optionsMul, #answer .ftb, #answer .options, #answer .dpn, #answer .true_false, #answer .audio').hide();
		$('#answer .mtf').show();
		if(questions[sectionCount][selectedQuestion].answer.columnA.length > 2) {
			addOptionsMtf(questions[sectionCount][selectedQuestion].answer.columnA.length - 2);
		}
		$('#answer .mtf .columnA:eq(0)').html('(a.)&emsp;' + questions[sectionCount][selectedQuestion].answer.columnA[0].columnA);
		$('#answer .mtf .columnB:eq(0)').html('(1.)&emsp;' + questions[sectionCount][selectedQuestion].answer.columnB[0].columnB);
		$('#answer .mtf .columnA:eq(1)').html('(b.)&emsp;' + questions[sectionCount][selectedQuestion].answer.columnA[1].columnA);
		$('#answer .mtf .columnB:eq(1)').html('(2.)&emsp;' + questions[sectionCount][selectedQuestion].answer.columnB[1].columnB);
		$('#answer .mtf .matcher').find('.temp').remove();
		$('#answer .mtf .matcher').append('<option class="temp" value="' + questions[sectionCount][selectedQuestion].answer.columnB[0].columnB + '">1</option><option class="temp" value="' + questions[sectionCount][selectedQuestion].answer.columnB[1].columnB + '">2</option>');
		for(var i=2; i<questions[sectionCount][selectedQuestion].answer.columnA.length; i++) {
			$('#answer .mtf .columnA:eq('+i+')').html('('+letters[i]+'.)&emsp;' + questions[sectionCount][selectedQuestion].answer.columnA[i].columnA);
			$('#answer .mtf .columnB:eq('+i+')').html('(' + ( i + 1 ) + '.)&emsp;' + questions[sectionCount][selectedQuestion].answer.columnB[i].columnB);
			$('#answer .mtf .matcher').append('<option class="temp" value="' + questions[sectionCount][selectedQuestion].answer.columnB[i].columnB + '">' + ( i + 1 ) + '</option>');
		}
	}
	
	//function to handle FTB questions
	function handleFtb() {
		$('#answer .optionsMul, #answer .options, #answer .mtf, #answer .dpn, #answer .true_false, #answer .audio').hide();
		$('#answer .ftb').show();
		if(questions[sectionCount][selectedQuestion].answer.length > 1) {
			addBlanks(questions[sectionCount][selectedQuestion].answer.length - 1);
		}
	}
	
	//function to handle CMP questions
	function handleCmp() {
		$('#answer').hide();
		$('#compro').show();
		$('#question').hide();
		$('#parentQuestion').html(questions[sectionCount][selectedQuestion].parent);
		$('#compro .child-question').html(questions[sectionCount][selectedQuestion].question);
		if(questions[sectionCount][selectedQuestion].questionType == 0) {
			$('#compro .answer .optionsMul, #compro .answer .ftb, #compro .answer .true_false').hide();
			$('#compro .answer .options').show();
			$('#compro .answer .options').find('.generated').remove();
			if(questions[sectionCount][selectedQuestion].answer.length > 2)
				addChildOptions(questions[sectionCount][selectedQuestion].answer.length - 2);
			for(var i=0; i<questions[sectionCount][selectedQuestion].answer.length; i++) {
				$('#compro .answer .options input[type="radio"]:eq('+i+')').val(questions[sectionCount][selectedQuestion].answer[i].option);
				$('#compro .answer .options .opt-ans:eq('+i+')').html(questions[sectionCount][selectedQuestion].answer[i].option);
			}
		}
		else if(questions[sectionCount][selectedQuestion].questionType == 1) {
			$('#compro .answer .optionsMul, #compro .answer .ftb, #compro .answer .options').hide();
			$('#compro .answer .true_false').show();
		}
		else if(questions[sectionCount][selectedQuestion].questionType == 2) {
			$('#compro .answer .optionsMul, #compro .answer .options, #compro .answer .true_false').hide();
			$('#compro .answer .ftb').show();
			$('#compro .answer .ftb').find('.generated').remove();
			if(questions[sectionCount][selectedQuestion].answer.length > 1) {
				addChildBlanks(questions[sectionCount][selectedQuestion].answer.length - 1);
			}
		}
		else if(questions[sectionCount][selectedQuestion].questionType == 6) {
			$('#compro .answer .options, #compro .answer .ftb, #compro .answer .true_false').hide();
			$('#compro .answer .optionsMul').show();
			$('#compro .answer .optionsMul').find('.generated').remove();
			if(questions[sectionCount][selectedQuestion].answer.length > 2)
				addChildOptionsMul(questions[sectionCount][selectedQuestion].answer.length - 2);
			for(var i=0; i<questions[sectionCount][selectedQuestion].answer.length; i++) {
				$('#compro .answer .optionsMul input[type="checkbox"]:eq('+i+')').val(questions[sectionCount][selectedQuestion].answer[i].option);
				$('#compro .answer .optionsMul .optMul-ans:eq('+i+')').html(questions[sectionCount][selectedQuestion].answer[i].option);
			}
		}
	}
	
	//function to handle DPN questions
	function handleDpn() {
		$('#answer .optionsMul, #answer .options, #answer .mtf, #answer .ftb, #answer .true_false, #answer .audio').hide();
		$('#answer .dpn').show();
		$('#question').hide();
		$('#answer .dpn .row:eq(0)').find('.generated').remove();
		$('#answer .dpn .row:eq(0) .question').html(questions[sectionCount][selectedQuestion].question);
		for(var i=0; i<questions[sectionCount][selectedQuestion].childQuestions.length; i++) {
			html = '<div class="col-md-12 generated question-container">'
				+ '<div><strong style="width: 25px;float: left;">' + letters[i] + '.</strong>' + questions[sectionCount][selectedQuestion].childQuestions[i].question + '</div>'
				+ '<input type="hidden" class="childQuestionId" value="'+questions[sectionCount][selectedQuestion].childQuestions[i].id+'">'
				+ '<input type="hidden" class="childQuestionType" value="'+questions[sectionCount][selectedQuestion].childQuestions[i].questionType+'">';
			if(questions[sectionCount][selectedQuestion].childQuestions[i].questionType == 0) {
				for(var j=0; j<questions[sectionCount][selectedQuestion].childQuestions[i].answer.length; j++) {
					html += '<label class="radio-inline inline-exam">'
							+ '<input type="radio" name="options'+i+'" value="' + questions[sectionCount][selectedQuestion].childQuestions[i].answer[j].option + '"><div class="opt-ans">' + questions[sectionCount][selectedQuestion].childQuestions[i].answer[j].option + '</div>'
						+ '</label><br/>';
				}
				html += '</div>';
				$('#answer .dpn .row:eq(0) .custom-right-question').append(html);
			}
			else if(questions[sectionCount][selectedQuestion].childQuestions[i].questionType == 6) {
				for(var j=0; j<questions[sectionCount][selectedQuestion].childQuestions[i].answer.length; j++) {
					html += '<label class="checkbox-inline inline-exam">'
							+ '<input type="checkbox" name="options'+i+'" value="' + questions[sectionCount][selectedQuestion].childQuestions[i].answer[j].option + '"><div class="optMul-ans">' + questions[sectionCount][selectedQuestion].childQuestions[i].answer[j].option + '</div>'
						+ '</label><br/>';
				}
				html += '</div>';
				$('#answer .dpn .row:eq(0) .custom-right-question').append(html);
			}
			else if(questions[sectionCount][selectedQuestion].childQuestions[i].questionType == 1) {
				html += '<label class="radio-inline inline-exam">'
						+ '<input type="radio" name="dpn_true_false'+i+'" class="true">True'
					+ '</label> <br/>'
					+ '<label class="radio-inline inline-exam">'
						+ '<input type="radio" name="dpn_true_false'+i+'" class="false">False'
					+ '</label> <br/>';
				html += '</div>';
				$('#answer .dpn .row:eq(0) .custom-right-question').append(html);
			}
			else if(questions[sectionCount][selectedQuestion].childQuestions[i].questionType == 2) {
				for(var j=0; j<questions[sectionCount][selectedQuestion].childQuestions[i].answer.length; j++) {
					html += '<div class="col-lg-8">'
							+ '<input type="text" class="form-control" style="margin-top: 8px;">'
						+ '</div>';
				}
				html += '</div>';
				$('#answer .dpn .row:eq(0) .custom-right-question').append(html);
			}
		}
	}
	
	//function to add dynamic options
	function addOptions(amount) {
		var html = '';
		for(var i=0; i<amount; i++) {
			html += '<div class="generated">'
					+ '<label class="radio-inline inline-exam">'
						+ '<input type="radio" name="options" value=""><div class="opt-ans"></div>'
					+ '</label>'
					+ '<br/></div>';
		}
		$('#answer .options').append(html);
	}

	//function to add dynamic options
	function addAudioOptions(amount) {
		var html = '';
		for(var i=0; i<amount; i++) {
			html += '<div class="generated">'
					+ '<label class="radio-inline inline-exam">'
						+ '<input type="radio" name="options" value=""><div class="opt-ans"></div>'
					+ '</label>'
					+ '<br/></div>';
		}
		$('#answer .audio').append(html);
	}
	
	//function to add dynamic options in cmp questions
	function addChildOptions(amount) {
		var html = '';
		for(var i=0; i<amount; i++) {
			html += '<div class="generated">'
					+ '<label class="radio-inline inline-exam">'
						+ '<input type="radio" name="childOptions" value=""><div class="opt-ans"></div>'
					+ '</label>'
					+ '<br/></div>';
		}
		$('#compro .answer .options').append(html);
	}
	
	//function to add dynamic mtf columns
	function addOptionsMtf(amount) {
		var html = '';
		for(var i=0; i<amount; i++) {
			html += '<div class="row generated" style="margin-top: 10px">'
						+ '<div class="col-lg-2">'
							+ '<div class="columnA"></div>'
						+ '</div>'
						+ '<div class="col-lg-2">'
							+ '<select class="form-control matcher">'
								+ '<option value="0">Select Answer</option>'
							+ '</select>'
						+ '</div>'
						+ '<div class="col-lg-2">'
							+ '<div class="columnB"></div>'
						+ '</div>'
					+ '</div>';
		}
		$('#answer .mtf').append(html);
	}
	
	//function to add dynamic multiple select options
	function addOptionsMul(amount) {
		var html = '';
		for(var i=0; i<amount; i++) {
			html += '<div class="generated">'
					+ '<label class="checkbox-inline inline-exam">'
						+ '<input type="checkbox" name="options" value=""><div class="optMul-ans"></div>'
					+ '</label>'
					+ '<br/></div>';
		}
		$('#answer .optionsMul').append(html);
	}
	
	//function to add dynamic multiple select options in cmp questions
	function addChildOptionsMul(amount) {
		var html = '';
		for(var i=0; i<amount; i++) {
			html += '<div class="generated">'
					+ '<label class="checkbox-inline inline-exam">'
						+ '<input type="checkbox" name="options" value=""><div class="optMul-ans"></div>'
					+ '</label>'
					+ '<br/></div>';
		}
		$('#compro .answer .optionsMul').append(html);
	}
	
	//function to add dynamic blanks in ftb
	function addBlanks(amount) {
		var html = '';
		for(var i=0; i<amount; i++) {
			html += '<div class="row generated">'
						+ '<div class="col-lg-3">'
							+ '<input type="text" class="form-control" style="margin-top: 10px;">'
						+ '</div>'
					+ '</div>';
		}
		$('#answer .ftb').append(html);
	}
	
	//function to add dynamic blanks in ftb in cmp type questions
	function addChildBlanks(amount) {
		var html = '';
		for(var i=0; i<amount; i++) {
			html += '<div class="row generated">'
						+ '<div class="col-lg-6">'
							+ '<input type="text" class="form-control" style="margin-top: 10px;">'
						+ '</div>'
					+ '</div>';
		}
		$('#compro .answer .ftb').append(html);
	}
	
	//function to save answers given by student into local variables
	function saveAnswer() {
		var flagAnswer = false;
		var needle = questions[sectionCount][selectedQuestion].id;
		answers[needle].questionType = questions[sectionCount][selectedQuestion].questionType;
		answers[needle].questionId = questions[sectionCount][selectedQuestion].id;
		if(questions[sectionCount][selectedQuestion].parentId == 0) {
			if(questions[sectionCount][selectedQuestion].questionType == 0) {
				$('#answer .options').find('input[type="radio"]').each(function() {
					if($(this).prop('checked')) {
						answers[needle].answer = $(this).val();
						flagAnswer =true;
					}
				});
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 7) {
				$('#answer .audio').find('input[type="radio"]').each(function() {
					if($(this).prop('checked')) {
						answers[needle].answer = $(this).val();
						//console.log(answers[needle].answer);
						flagAnswer =true;
					}
				});
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 6) {
				answers[needle].answer = [];
				$('#answer .optionsMul').find('input[type="checkbox"]').each(function() {
					if($(this).prop('checked')) {
						answers[needle].answer.push($(this).val());
						flagAnswer = true;
					}
				});
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 1) {
				if($('#answer .true_false .true').prop('checked')) {
					answers[needle].answer = 1;
					flagAnswer =true;
				}
				else if($('#answer .true_false .false').prop('checked')) {
					answers[needle].answer = 0;
					flagAnswer = true;
				}
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 2) {
				answers[needle].answer = [];
				$('#answer .ftb').find('input[type="text"]').each(function() {
					if($(this).val() != '') {
						answers[needle].answer.push($(this).val());
						flagAnswer = true;
					}
					else
						answers[needle].answer.push('');
				});
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 3) {
				answers[needle].answer = [];
				$('#answer .mtf').find('select').each(function() {
					if($(this).val() != 0) {
						answers[needle].answer.push($(this).val());
						flagAnswer = true;
					}
					else
						answers[needle].answer.push(0);
				});
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 5) {
				var needle = $('#questionId').val();
				answers[needle].childs = [];
				$('#answer .dpn .custom-right-question .question-container').each(function() {
					var questionType = $(this).find('.childQuestionType').val();
					var tempobj = {};
					tempobj.id = $(this).find('.childQuestionId').val();
					tempobj.questionType = questionType;
					tempobj.parentId = needle;
					if(questionType == 0) {
						$(this).find('input[type="radio"]').each(function() {
							if($(this).prop('checked')) {
								tempobj.answer = $(this).val();
								flagAnswer =true;
							}
						});
					}
					else if(questionType == 1) {
						if($(this).find('.true').prop('checked')) {
							tempobj.answer = 1;
							flagAnswer =true;
						}
						else if($(this).find('.false').prop('checked')) {
							tempobj.answer = 0;
							flagAnswer = true;
						}
					}
					else if(questionType == 2) {
						tempobj.answer = [];
						$(this).find('input[type="text"]').each(function() {
							if($(this).val() != '') {
								tempobj.answer.push($(this).val());
								flagAnswer = true;
							}
							else
								tempobj.answer.push('');
						});
					}
					else if(questionType == 6) {
						tempobj.answer = [];
						$(this).find('input[type="checkbox"]').each(function() {
							if($(this).prop('checked')) {
								tempobj.answer.push($(this).val());
								flagAnswer = true;
							}
						});
					}
					answers[needle].childs.push(tempobj);
				});
			}
		}
		else {
			if(questions[sectionCount][selectedQuestion].questionType == 0) {
				$('#compro .answer .options').find('input[type="radio"]').each(function() {
					if($(this).prop('checked')) {
						answers[needle].answer = $(this).val();
						flagAnswer =true;
					}
				});
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 6) {
				answers[needle].answer = [];
				$('#compro .answer .optionsMul').find('input[type="checkbox"]').each(function() {
					if($(this).prop('checked')) {
						answers[needle].answer.push($(this).val());
						flagAnswer = true;
					}
				});
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 1) {
				if($('#compro .answer .true_false .true').prop('checked')) {
					answers[needle].answer = 1;
					flagAnswer = true;
				}
				else if($('#compro .answer .true_false .false').prop('checked')) {
					answers[needle].answer = 0;
					flagAnswer = true;
				}
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 2) {
				answers[needle].answer = [];
				$('#compro .answer .ftb').find('input[type="text"]').each(function() {
					if($(this).val() != '') {
						answers[needle].answer.push($(this).val());
						flagAnswer = true;
					}
					else
						answers[needle].answer.push('');
				});
			}
		}
		if(flagAnswer) {
			$('.question-button[data-id="'+questions[sectionCount][selectedQuestion].id+'"]').removeClass('btn-danger btn-white').addClass('btn-success');
			if($('.question-button[data-id="'+questions[sectionCount][selectedQuestion].id+'"]').hasClass('btn-info'))
				answers[questions[sectionCount][selectedQuestion].id].status = 4;
			else
				answers[questions[sectionCount][selectedQuestion].id].status = 2;
			if(examType == 1)
				$('.question-button[data-id="'+questions[sectionCount][selectedQuestion].id+'"]').removeClass('btn-info');
		}
		
		//send the data to server for auto save
		sendAnswerToServer();
	}
	
	//function to save one answer on server
	function sendAnswerToServer() {
		//console.log('saving...');
		autoSaveCounter = 60;
		needle = questions[sectionCount][selectedQuestion].id;
		var req = {};
		var res;
		req.action = 'update-user-answer';
		req.question = answers[needle];
		req.attemptId = attemptId;
		req.examId = getUrlParameter('examId');
		/* if answer is not defined then only time is needed to be set
		if(req.question.answer == undefined && req.question.questionType != 5)
			return;
		else if(req.question.childs == undefined && req.question.questionType == 5)
			return;
		*/
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			/*if(res.status == 0)
				console.log('failed');
			else
				console.log('success');*/
		});
	}
	
	//function to start exam time counter
	function examTimerStart() {
		formatTime(totalTime);
		//function to send answers to server in every two min if time has to be paused in exam
		s2MinTimer = setInterval(function() {
			if(autoSaveCounter < 0) {
				autoSaveCounter = 60;
				//send the current question to server so it can be saved
				sendAnswerToServer();
			}
			autoSaveCounter--;
		}, 1000);
		//decreasing the value of time per second
		timer = setInterval(function() {
			secondTimer--;
			if(secondTimer < 0) {
				totalTime--;
				formatTime(totalTime);
				secondTimer = 59;
			}
			if(totalTime < 0) {
				clearInterval(timer);
				alert('Time Up');
				if(!switchFlag) {
					if(sectionCount + 1 < sections.length) {
						$('.section-switch:eq(' + sectionCount + ')').removeClass('btn-danger').addClass('disable-course');
						sectionCount++;
						$('.section-switch:eq(' + sectionCount + ')').removeClass('disable-course').addClass('btn-danger');
						totalTime = sections[sectionCount].time - 1;
						formatTime(totalTime);
						//to freeze screen during the gap time
						$('#freeze').show();
						var tempGapTimer = gapTime * 60;
						//starting the gap timer
						$('#gapTimer').text('00:00:00');
						gapTimer = setInterval(function() {
							timeInFormat = '';
							tempGapTime = tempGapTimer;
							if(tempGapTime >= 3600) {
								timeInFormat = Math.floor(tempGapTime / 3600);
								tempGapTime = tempGapTime - (timeInFormat * 3600);
								if(timeInFormat <= 9)
									timeInFormat = '0' + timeInFormat;
								timeInFormat += ':';
							}
							else
								timeInFormat += '00:'
							if(tempGapTime >= 60) {
								mins = Math.floor(tempGapTime / 60);
								tempGapTime = tempGapTime - (mins * 60);
								if(mins <= 9)
									timeInFormat += '0' + mins + ':';
								else
									timeInFormat += mins + ':';
							}
							else
								timeInFormat += '00:';
							if(tempGapTime <= 9)
								timeInFormat += '0';
							timeInFormat += tempGapTime;
							$('#gapTimer').text(timeInFormat);
							tempGapTimer--;
						}, 1000);
						setTimeout(function() {
							examTimerStart();
							$('.section-switch:eq('+sectionCount+')').attr('disabled', false);
							$('.section-switch:eq('+(sectionCount-1)+')').attr('disabled', true);
							$('.section').hide();
							$('.section'+sectionCount).show();
							$('#sectionName').text($('.section-switch:eq('+sectionCount+')').text());
							$('#freeze').hide();
							clearInterval(gapTimer);
							showNextQuestion(0);
						}, (gapTime * 60 * 1000));
					}
					else
						submitExam();
				}
				else
					submitExam();
			}
			if(secondTimer < 10)
				$('#timeSecond').text('0' + secondTimer);
			else
				$('#timeSecond').text(secondTimer);
		},	1000);
	}
	
	//function to start assignment time counter
	function assignmentTimerStart() {
		formatTime(totalTime);
		//function to send answers to server in every two min if time has to be paused in exam
		s2MinTimer = setInterval(function() {
			if(autoSaveCounter < 0) {
				autoSaveCounter = 60;
				//send the current question to server so it can be saved
				sendAnswerToServer();
			}
			autoSaveCounter--;
		}, 1000);
		//decreasing the value of time per second
		timer = setInterval(function() {
			if(secondTimer < 10)
				$('#timeSecond').text('0' + secondTimer);
			else
				$('#timeSecond').text(secondTimer);
			secondTimer++;
			if(secondTimer > 59) {
				totalTime++;
				formatTime(totalTime);
				secondTimer = 0;
			}
		},	1000);
	}
	
	//function to clear all html answer fields
	function clearResponses() {
		$('input[type="radio"]').prop('checked', false);
		$('input[type="checkbox"]').prop('checked', false);
		$('input[type="text"]').val('');
		//now clearing any given answers (ticks and crosses) with explanation
		$('#answer .generated.explaination').remove();
		$('#answer .generated.fa').remove();
	}
	
	//function to fill answers already given by student
	function fillAnswer() {
		$('input, select').attr('disabled', false);
		$('#clearResponses').show();
		$('#markReview').show();
		var needle = $('#questionId').val();
		if(answers[needle].status == 2 || answers[needle].status == 4)
			$('#markReview').hide();
		clearResponses();
		if((!(answers[needle].answer == undefined || answers[needle].answer.length == 0)) && examType == 1 && eachQuestionFlag) {
			$('input, select').attr('disabled', true);
			$('#clearResponses').hide();
			$('#markReview').hide();
		}
		if(questions[sectionCount][selectedQuestion].parentId == 0) {
			if(answers[needle].questionType == 1) {
				if(answers[needle].answer == 0)
					$('#answer .true_false .false').prop('checked', true);
				else if(answers[needle].answer == 1)
					$('#answer .true_false .true').prop('checked', true);
			}
			else if(answers[needle].questionType == 0) {
				$('#answer .options input[type="radio"][value="'+answers[needle].answer+'"]').prop('checked', true);
			}
			else if(answers[needle].questionType == 7) {
				$('#answer .audio input[type="radio"][value="'+answers[needle].answer+'"]').prop('checked', true);
			}
			else if(answers[needle].questionType == 6) {
				if(answers[needle].answer != undefined) {
					for(var i=0; i< answers[needle].answer.length; i++) {
						$('#answer .optionsMul input[type="checkbox"][value="'+answers[needle].answer[i]+'"]').prop('checked', true);
					}
				}
			}
			else if(answers[needle].questionType == 2) {
				if(answers[needle].answer != undefined) {
					for(var i=0; i< answers[needle].answer.length; i++) {
						$('#answer .ftb input[type="text"]:eq('+i+')').val(answers[needle].answer[i]);
					}
				}
			}
			else if(answers[needle].questionType == 3) {
				if(answers[needle].answer != undefined) {
					for(var i=0; i< answers[needle].answer.length; i++) {
						$('#answer .mtf select:eq('+i+')').val(answers[needle].answer[i]);
					}
				}
			}
			else if(answers[needle].questionType == 5 && answers[needle].childs != undefined) {
				for(var i=0; i< answers[needle].childs.length; i++) {
					if(answers[needle].childs[i].questionType == 0) {
						$('#answer .dpn .question-container:eq('+i+')').find('input[type="radio"][value="'+answers[needle].childs[i].answer+'"]').prop('checked', true);
					}
					else if(answers[needle].childs[i].questionType == 1) {
						if(answers[needle].childs[i].answer == 1)
							$('#answer .dpn .question-container:eq('+i+')').find('.true').prop('checked', true);
						else if(answers[needle].childs[i].answer == 0)
							$('#answer .dpn .question-container:eq('+i+')').find('.false').prop('checked', true);
					}
					else if(answers[needle].childs[i].questionType == 2) {
						for(var j=0; j<answers[needle].childs[i].answer.length; j++) {
							$('#answer .dpn .question-container:eq('+i+')').find('input[type="text"]:eq('+j+')').val(answers[needle].childs[i].answer[j]);
						}
					}
					else if(answers[needle].childs[i].questionType == 6) {
						for(var j=0; j<answers[needle].childs[i].answer.length; j++) {
							$('#answer .dpn .question-container:eq('+i+')').find('input[type="checkbox"][value="'+answers[needle].childs[i].answer[j]+'"]').prop('checked', true);
						}
					}
				}
			}
		}
		else {
			if(answers[needle].questionType == 1) {
				if(answers[needle].answer == 0)
					$('#compro .answer .true_false .false').prop('checked', true);
				else if(answers[needle].answer == 1)
					$('#compro .answer .true_false .true').prop('checked', true);
			}
			else if(answers[needle].questionType == 0) {
				$('#compro .answer .options input[type="radio"][value="'+answers[needle].answer+'"]').prop('checked', true);
			}
			else if(answers[needle].questionType == 6) {
				if(answers[needle].answer != undefined) {
					for(var i=0; i< answers[needle].answer.length; i++) {
						$('#compro .answer .optionsMul input[type="checkbox"][value="'+answers[needle].answer[i]+'"]').prop('checked', true);
					}
				}
			}
			else if(answers[needle].questionType == 2) {
				if(answers[needle].answer != undefined) {
					for(var i=0; i< answers[needle].answer.length; i++) {
						$('#compro .answer .ftb input[type="text"]:eq('+i+')').val(answers[needle].answer[i]);
					}
				}
			}
		}
		if((answers[needle].status == 2 || answers[needle].status == 4) && eachQuestionFlag) {
			if(examType ==1 )
				$('#showAnswer').click();
		}
	}
	
	//function to show the answer of each question
	function showAnswerForQuestion() {
		var correct = ' <i class="fa fa-check-circle text-success generated"></i>';
		var wrong = ' <i class="fa fa-times-circle text-danger generated"></i>';
		needle = questions[sectionCount][selectedQuestion].id;
		if(questions[sectionCount][selectedQuestion].parentId == 0) {
			if(questions[sectionCount][selectedQuestion].questionType == 0) {
				for(var i=0; i<questions[sectionCount][selectedQuestion].answer.length; i++) {
					if(questions[sectionCount][selectedQuestion].answer[i].correct == 1) {
						$('#answer .options label.radio-inline:eq('+i+')').after(correct);
					}
					else if($('#answer .options input[type="radio"]:eq('+i+')').prop('checked')) {
						$('#answer .options label.radio-inline:eq('+i+')').after(wrong);
					}
				}
				$('#answer .options').append('<div class="generated explaination"><strong>Explantion</strong> :'+questions[sectionCount][selectedQuestion].description+'</div>');
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 7) {
				for(var i=0; i<questions[sectionCount][selectedQuestion].answer.length; i++) {
					if(questions[sectionCount][selectedQuestion].answer[i].correct == 1) {
						$('#answer .audio label.radio-inline:eq('+i+')').after(correct);
					}
					else if($('#answer .audio input[type="radio"]:eq('+i+')').prop('checked')) {
						$('#answer .audio label.radio-inline:eq('+i+')').after(wrong);
					}
				}
				$('#answer .audio').append('<div class="generated explaination"><strong>Explantion</strong> :'+questions[sectionCount][selectedQuestion].description+'</div>');
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 6) {
				for(var i=0; i<questions[sectionCount][selectedQuestion].answer.length; i++) {
					if(questions[sectionCount][selectedQuestion].answer[i].correct == 1) {
						$('#answer .optionsMul label.checkbox-inline:eq('+i+')').after(correct);
					}
					else if($('#answer .optionsMul input[type="checkbox"]:eq('+i+')').prop('checked')) {
						$('#answer .optionsMul label.checkbox-inline:eq('+i+')').after(wrong);
					}
				}
				$('#answer .optionsMul').append('<div class="generated explaination"><strong>Explantion</strong> :'+questions[sectionCount][selectedQuestion].description+'</div>');
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 1) {
				if(questions[sectionCount][selectedQuestion].answer[0].answer == 1) {
					$('#answer .true_false label.radio-inline:eq(0)').after(correct);
					if($('#answer .true_false input[type="radio"]:eq(1)').prop('checked'))
						$('#answer .true_false label.radio-inline:eq(1)').after(wrong);
				}
				else {
					$('#answer .true_false label.radio-inline:eq(1)').after(correct);
					if($('#answer .true_false input[type="radio"]:eq(0)').prop('checked'))
						$('#answer .true_false label.radio-inline:eq(0)').after(wrong);
				}
				$('#answer .true_false').append('<div class="generated explaination"><strong>Explantion</strong> :'+questions[sectionCount][selectedQuestion].description+'</div>');
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 2) {
				for(var i=0; i<questions[sectionCount][selectedQuestion].answer.length; i++) {
					var userAns = answers[needle].answer[i].trim().toLowerCase().trim();
					var origAns = questions[sectionCount][selectedQuestion].answer[i].blanks.toLowerCase().trim();
					if(userAns == origAns)
						$('#answer .ftb input[type="text"]:eq('+i+')').after(correct);
					else
						$('#answer .ftb input[type="text"]:eq('+i+')').after(wrong+'<span class="generated">'+origAns+'</span>');
				}
				$('#answer .ftb').append('<div class="generated explaination"><strong>Explantion</strong> :'+questions[sectionCount][selectedQuestion].description+'</div>');
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 3) {
				for(var i=0; i<questions[sectionCount][selectedQuestion].answer.columnA.length; i++) {
					var userAns = answers[needle].answer[i];
					var origAns = questions[sectionCount][selectedQuestion].answer.columnA[i].columnB;
					if(userAns === origAns)
						$('#answer .mtf .row:eq('+i+')').append(correct);
					else
						$('#answer .mtf .row:eq('+i+')').append(wrong+'<span class="generated">'+origAns+'</span>');
				}
				$('#answer .mtf').append('<div class="generated explaination"><strong>Explantion</strong> :'+questions[sectionCount][selectedQuestion].description+'</div>');
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 5) {
				var needle = $('#questionId').val();
				for(var i=0; i<$('#answer .dpn .custom-right-question .question-container').length; i++) {
					var questionType = $('#answer .dpn .custom-right-question .question-container:eq('+i+')').find('.childQuestionType').val();
					if(questionType == 2) {
						for(var j=0; j<questions[sectionCount][selectedQuestion].childQuestions[i].answer.length; j++) {
							var userAns = answers[needle].childs[i].answer[j].trim().toLowerCase();
							var origAns = questions[sectionCount][selectedQuestion].childQuestions[i].answer[j].blanks;
							if(userAns == origAns)
								$('#answer .dpn .custom-right-question .question-container:eq('+i+')').find('input[type="text"]:eq('+j+')').after(correct);
							else
								$('#answer .dpn .custom-right-question .question-container:eq('+i+')').find('input[type="text"]:eq('+j+')').after(wrong+'<span class="generated">'+origAns+'</span>');
						}
						$('#answer .dpn .custom-right-question .question-container:eq('+i+')').append('<div class="generated explaination"><strong>Explantion</strong> :'+questions[sectionCount][selectedQuestion].childQuestions[i].description+'</div>');
					}
					else if(questionType == 0) {
						for(var j=0; j<questions[sectionCount][selectedQuestion].childQuestions[i].answer.length; j++) {
							if(questions[sectionCount][selectedQuestion].childQuestions[i].answer[j].correct == 1)
								$('#answer .dpn .custom-right-question .question-container:eq('+i+')').find('label.radio-inline:eq('+j+')').after(correct);
							else if($('#answer .dpn .custom-right-question .question-container:eq('+i+')').find('input[type="radio"]:eq('+j+')').prop('checked'))
								$('#answer .dpn .custom-right-question .question-container:eq('+i+')').find('label.radio-inline:eq('+j+')').after(wrong);
						}
						$('#answer .dpn .custom-right-question .question-container:eq('+i+')').append('<div class="generated explaination"><strong>Explantion</strong> :'+questions[sectionCount][selectedQuestion].childQuestions[i].description+'</div>');
					}
					else if(questionType == 6) {
						for(var j=0; j<questions[sectionCount][selectedQuestion].childQuestions[i].answer.length; j++) {
							if(questions[sectionCount][selectedQuestion].childQuestions[i].answer[j].correct == 1)
								$('#answer .dpn .custom-right-question .question-container:eq('+i+')').find('label.checkbox-inline:eq('+j+')').after(correct);
							else if($('#answer .dpn .custom-right-question .question-container:eq('+i+')').find('input[type="checkbox"]:eq('+j+')').prop('checked'))
								$('#answer .dpn .custom-right-question .question-container:eq('+i+')').find('label.checkbox-inline:eq('+j+')').after(wrong);
						}
						$('#answer .dpn .custom-right-question .question-container:eq('+i+')').append('<div class="generated explaination"><strong>Explantion</strong> :'+questions[sectionCount][selectedQuestion].childQuestions[i].description+'</div>');
					}
					else if(questionType == 1) {
						if(questions[sectionCount][selectedQuestion].childQuestions[i].answer[0].answer == 1) {
							$('#answer .dpn .custom-right-question .question-container:eq('+i+')').find('label.radio-inline:eq(0)').after(correct);
							 if($('#answer .dpn .custom-right-question .question-container:eq('+i+')').find('input[type="radio"]:eq(1)').prop('checked'))
								$('#answer .dpn .custom-right-question .question-container:eq('+i+')').find('label.radio-inline:eq(1)').after(wrong);
						}
						else {
							$('#answer .dpn .custom-right-question .question-container:eq('+i+')').find('label.radio-inline:eq(1)').after(correct);
							if($('#answer .dpn .custom-right-question .question-container:eq('+i+')').find('input[type="radio"]:eq(0)').prop('checked'))
								$('#answer .dpn .custom-right-question .question-container:eq('+i+')').find('label.radio-inline:eq(0)').after(wrong);
						}
						$('#answer .dpn .custom-right-question .question-container:eq('+i+')').append('<div class="generated explaination"><strong>Explantion</strong> :'+questions[sectionCount][selectedQuestion].childQuestions[i].description+'</div>');
					}
				}
			}
		}
		else {
			if(questions[sectionCount][selectedQuestion].questionType == 0) {
				for(var i=0; i<questions[sectionCount][selectedQuestion].answer.length; i++) {
					if(questions[sectionCount][selectedQuestion].answer[i].correct == 1) {
						$('#compro .answer .options label.radio-inline:eq('+i+')').after(correct);
					}
					else if($('#compro .answer .options input[type="radio"]:eq('+i+')').prop('checked')) {
						$('#compro .answer .options label.radio-inline:eq('+i+')').after(wrong);
					}
				}
				$('#compro .answer .options').append('<div class="generated explaination"><strong>Explantion</strong> :'+questions[sectionCount][selectedQuestion].description+'</div>');
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 6) {
				for(var i=0; i<questions[sectionCount][selectedQuestion].answer.length; i++) {
					if(questions[sectionCount][selectedQuestion].answer[i].correct == 1) {
						$('#compro .answer .optionsMul label.checkbox-inline:eq('+i+')').after(correct);
					}
					else if($('#compro .answer .optionsMul input[type="checkbox"]:eq('+i+')').prop('checked')) {
						$('#compro .answer .optionsMul label.checkbox-inline:eq('+i+')').after(wrong);
					}
				}
				$('#compro .answer .optionsMul').append('<div class="generated explaination"><strong>Explantion</strong> :'+questions[sectionCount][selectedQuestion].description+'</div>');
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 1) {
				if(questions[sectionCount][selectedQuestion].answer[0].answer == 1) {
					$('#compro .answer .true_false label.radio-inline:eq(0)').after(correct);
					if($('#compro .answer .true_false input[type="radio"]:eq(1)').prop('checked'))
						$('#compro .answer .true_false label.radio-inline:eq(1)').after(wrong);
				}
				else {
					$('#compro .answer .true_false label.radio-inline:eq(1)').after(correct);
					if($('#compro .answer .true_false input[type="radio"]:eq(0)').prop('checked'))
						$('#compro .answer .true_false label.radio-inline:eq(0)').after(wrong);
				}
				$('#compro .answer .true_false').append('<div class="generated explaination"><strong>Explantion</strong> :'+questions[sectionCount][selectedQuestion].description+'</div>');
			}
			else if(questions[sectionCount][selectedQuestion].questionType == 2) {
				for(var i=0; i<questions[sectionCount][selectedQuestion].answer.length; i++) {
					var userAns = answers[needle].answer[i].trim().toLowerCase().trim();
					var origAns = questions[sectionCount][selectedQuestion].answer[i].blanks.trim().toLowerCase();
					if(userAns == origAns)
						$('#compro .answer .ftb input[type="text"]:eq('+i+')').after(correct);
					else
						$('#compro .answer .ftb input[type="text"]:eq('+i+')').after(wrong+'<span class="generated">'+origAns+'</span>');
				}
				$('#compro .answer .ftb').append('<div class="generated explaination"><strong>Explantion</strong> :'+questions[sectionCount][selectedQuestion].description+'</div>');
			}
		}
		if((!(answers[needle].answer == undefined || answers[needle].answer.length == 0) || !(answers[needle].childs == undefined || answers[needle].childs.length == 0)) && examType == 1 && eachQuestionFlag) {
			$('input, select').attr('disabled', true);
			$('#clearResponses').hide();
			$('#markReview').hide();
		}
	}
	
	//function to show all questions and answers at the end of assignment if selected sort
	function showAllAnswers() {
		//window.location = 'studentResult.php?examId=' + getUrlParameter('examId');
		return;
		$('#questions').hide();
		$('#allAnswers').show();
		//creating all questions with answer
		var correct = '&emsp;<i class="fa fa-check-circle text-success generated"></i>';
		var wrong = '&emsp;<i class="fa fa-times-circle text-danger generated"></i>';
		var html = '';
		var buttons = '';
		for(var i = 0; i < sections.length; i++) {
			buttons = '<div class="col-md-2 col-sm-3">'
                                + '<a class="btn btn-md btn-info section-answer" data-number="'+i+'" data-id="' + sections[i].id + '">' + sections[i].name + '</a>'
                            + '</div>';
			$('#buttons').append(buttons);
			for(var j = 0; j < questions[i].length; j++) {
				needle = questions[i][j].id;
				html = '';
				if(questions[i][j].questionType == 0) {
					html += '<div class="all-container" data-number="'+i+'">'
							+ '<div class="question">Q ' + ( j + 1 ) + '. ' + questions[i][j].question + '</div>'
							+ '<div class="answer-container">';
					for(var k = 0; k < questions[i][j].answer.length; k++) {
						html += '<label class="radio-inline inline-exam">'
								+ '<input type="radio" name="opt' + i + '' + j + '" value="' + questions[i][j].answer[k].option + '"> ' + questions[i][j].answer[k].option
							+ '</label><br/>';
					}
					html += '</div>'
						+ '<div><strong>Explantion</strong> :'+questions[i][j].description+'</div><hr></div>';
					$('#all').append(html);
					if(answers[needle].answer != undefined)
						$('#all .all-container:eq(-1)').find('input[type="radio"][value="'+answers[needle].answer+'"]').prop('checked', true);
					for(var k = 0; k < questions[i][j].answer.length; k++) {
						if(questions[i][j].answer[k].correct == 1)
							$('#all .all-container:eq(-1) label.inline-exam:eq('+k+')').after(correct);
						else if($('#all .all-container:eq(-1) input[type="radio"]:eq('+k+')').prop('checked'))
							$('#all .all-container:eq(-1) label.inline-exam:eq('+k+')').after(wrong);
					}
				}
				else if(questions[i][j].questionType == 7) {
					html += '<div class="all-container" data-number="'+i+'">'
							+ '<div class="question">Q ' + ( j + 1 ) + '. ' + questions[i][j].question + '</div>'
							+ '<div class="answer-container">';
					for(var k = 0; k < questions[i][j].answer.length; k++) {
						html += '<label class="radio-inline inline-exam">'
								+ '<input type="radio" name="opt' + i + '' + j + '" value="' + questions[i][j].answer[k].option + '"> ' + questions[i][j].answer[k].option
							+ '</label><br/>';
					}
					html += '</div>'
						+ '<div><strong>Explantion</strong> :'+questions[i][j].description+'</div><hr></div>';
					$('#all').append(html);
					if(answers[needle].answer != undefined)
						$('#all .all-container:eq(-1)').find('input[type="radio"][value="'+answers[needle].answer+'"]').prop('checked', true);
					for(var k = 0; k < questions[i][j].answer.length; k++) {
						if(questions[i][j].answer[k].correct == 1)
							$('#all .all-container:eq(-1) label.inline-exam:eq('+k+')').after(correct);
						else if($('#all .all-container:eq(-1) input[type="radio"]:eq('+k+')').prop('checked'))
							$('#all .all-container:eq(-1) label.inline-exam:eq('+k+')').after(wrong);
					}
				}
				else if(questions[i][j].questionType == 6) {
					html += '<div class="all-container" data-number="'+i+'">'
							+ '<div class="question">Q ' + ( j + 1 ) + '. ' + questions[i][j].question + '</div>'
							+ '<div class="answer-container">';
					for(var k = 0; k < questions[i][j].answer.length; k++) {
						html += '<label class="checkbox-inline inline-exam">'
								+ '<input type="checkbox" name="opt' + i + '' + j + '" value="' + questions[i][j].answer[k].option + '"> ' + questions[i][j].answer[k].option
							+ '</label><br/>';
					}
					html += '</div>'
						+ '<div><strong>Explantion</strong> :'+questions[i][j].description+'</div><hr></div>';
					$('#all').append(html);
					if(answers[needle].answer != undefined) {
						for(var k = 0; k<answers[needle].answer.length; k++) {
							$('#all .all-container:eq(-1)').find('input[type="checkbox"][value="'+answers[needle].answer[k]+'"]').prop('checked', true);
						}
					}
					for(var k = 0; k < questions[i][j].answer.length; k++) {
						if(questions[i][j].answer[k].correct == 1)
							$('#all .all-container:eq(-1) label.inline-exam:eq('+k+')').after(correct);
						else if($('#all .all-container:eq(-1) input[type="checkbox"]:eq('+k+')').prop('checked'))
							$('#all .all-container:eq(-1) label.inline-exam:eq('+k+')').after(wrong);
					}
				}
				else if(questions[i][j].questionType == 1) {
					html += '<div class="all-container" data-number="'+i+'">'
							+ '<div class="question">Q ' + ( j + 1 ) + '. ' + questions[i][j].question + '</div>'
							+ '<div class="answer-container">';
					html += '<label class="radio-inline inline-exam">'
							+ '<input type="radio" name="opt' + i + '' + j + '" class="true"> True'
						+ '</label><br/>'
						+ '<label class="radio-inline inline-exam">'
							+ '<input type="radio" name="opt' + i + '' + j + '" class="false"> False'
						+ '</label><br/>';
					html += '</div>'
						+ '<div><strong>Explantion</strong> :'+questions[i][j].description+'</div><hr></div>';
					$('#all').append(html);
					if(answers[needle].answer == 1) {
						$('#all .all-container:eq(-1)').find('input[type="radio"]:eq(0)').prop('checked', true);
					}
					else if(answers[needle].answer == 0) {
						$('#all .all-container:eq(-1)').find('input[type="radio"]:eq(1)').prop('checked', true);
					}
					if(questions[i][j].answer[0].answer == 1) {
						$('#all .all-container:eq(-1) label.inline-exam:eq(0)').after(correct);
						if($('#all .all-container:eq(-1) input[type="radio"]:eq(1)').prop('checked'))
							$('#all .all-container:eq(-1) label.inline-exam:eq(1)').after(wrong);
					}
					else if(questions[i][j].answer[0].answer == 0) {
						$('#all .all-container:eq(-1) label.inline-exam:eq(1)').after(correct);
						if($('#all .all-container:eq(-1) input[type="radio"]:eq(0)').prop('checked'))
							$('#all .all-container:eq(-1) label.inline-exam:eq(0)').after(wrong);
					}
				}
				else if(questions[i][j].questionType == 2) {
					html += '<div class="all-container" data-number="'+i+'">'
							+ '<div class="question">Q ' + ( j + 1 ) + '. ' + questions[i][j].question + '</div>'
							+ '<div class="answer-container">';
					for(var k = 0; k < questions[i][j].answer.length; k++) {
						html += '<div class="row" style="margin: 10px"><div class="col-md-3">'
								+ '<input type="text" class="form-control">'
							+ '</div></div>';
					}
					html += '</div>'
						+ '<div><strong>Explantion</strong> :'+questions[i][j].description+'</div><hr></div>';
					$('#all').append(html);
					if(answers[needle].answer != undefined) {
						for(var k = 0; k<answers[needle].answer.length; k++) {
							$('#all .all-container:eq(-1)').find('input[type="text"]:eq('+k+')').val(answers[needle].answer[k]);
						}
					}
					for(var k = 0; k < questions[i][j].answer.length; k++) {
						if(answers[needle].answer != undefined)
							var userAns = answers[needle].answer[k].trim().toLowerCase();
						else
							var userAns = '';
						var origAns = questions[i][j].answer[k].blanks;
						if(origAns == userAns)
							$('#all .all-container:eq(-1) input[type="text"]:eq('+k+')').after(correct);
						else
							$('#all .all-container:eq(-1) input[type="text"]:eq('+k+')').after(wrong + origAns);
					}
				}
				else if(questions[i][j].questionType == 3) {
					html += '<div class="all-container" data-number="'+i+'">'
							+ '<div class="question">Q ' + ( j + 1 ) + '. ' + questions[i][j].question + '</div>'
							+ '<div class="answer-container">';
					for(var k = 0; k < questions[i][j].answer.columnA.length; k++) {
						html += '<div class="row" style="margin: 10px">'
								+ '<div class="col-lg-2">'
									+ '<div class="columnA">(' + letters[k] + '.)&emsp;' + questions[i][j].answer.columnA[k].columnA + '</div>'
								+ '</div>'
								+ '<div class="col-lg-2">'
									+ '<select class="form-control matcher">'
										+ '<option value="0">Select Answer</option>';
						for(var l = 0; l < questions[i][j].answer.columnB.length; l++) {
							html += '<option class="temp" value="' + questions[i][j].answer.columnB[l].columnB + '">' + (l + 1) + '</option>';
						}
									html += '</select>'
								+ '</div>'
								+ '<div class="col-lg-2">'
									+ '<div class="columnB">(' + ( k + 1 ) + '.)&emsp;' + questions[i][j].answer.columnB[k].columnB + '</div>'
							+ '</div>'
						+ '</div>';
					}
					html += '</div>'
						+ '<div><strong>Explantion</strong> :'+questions[i][j].description+'</div><hr></div>';
					$('#all').append(html);
					if(answers[needle].answer != undefined) {
						for(var k = 0; k<answers[needle].answer.length; k++) {
							$('#all .all-container:eq(-1)').find('.matcher:eq('+k+')').val(answers[needle].answer[k]);
						}
					}
					for(var k = 0; k < questions[i][j].answer.columnA.length; k++) {
						if(answers[needle].answer != undefined)
							var userAns = answers[needle].answer[k];
						else
							var userAns = '';
						var origAns = questions[i][j].answer.columnA[k].columnB;
						if(origAns == userAns)
							$('#all .all-container:eq(-1) .row:eq('+k+')').append(correct);
						else
							$('#all .all-container:eq(-1) .row:eq('+k+')').append(wrong + origAns);
					}
				}
				else if(questions[i][j].questionType == 5) {
					html = '<div class="all-container" data-number="' + i + '">'
							+ '<div class="question">Q ' + ( j + 1 ) + '. ' + questions[i][j].question + '</div>'
						+ '</div>';
					$('#all').append(html);
					var innerHTML = '';
					for(var k = 0; k < questions[i][j].childQuestions.length; k++) {
						innerHTML = '<div class="child-question-container">'
									+ '<div class="child-question">' + questions[i][j].childQuestions[k].question + '</div>';
						if(questions[i][j].childQuestions[k].questionType == 0) {
							for(var l = 0; l < questions[i][j].childQuestions[k].answer.length; l++) {
								innerHTML += '<label class="radio-inline inline-exam">'
											+ '<input type="radio" name="opt' + i + '' + j + '' + k + '" value="' + questions[i][j].childQuestions[k].answer[l].option + '"> ' + questions[i][j].childQuestions[k].answer[l].option
										+ '</label><br/>';
							}
							innerHTML += '<div><strong>Explantion</strong> :' + questions[i][j].childQuestions[k].description + '</div></div>';
							$('#all .all-container:eq(-1)').append(innerHTML);
							//filling answer given by student for dpn question and child mcq questions
							if(answers[needle].childs != undefined && answers[needle].childs[k].answer != undefined)
								$('#all .all-container:eq(-1) .child-question-container:eq(-1)').find('input[type="radio"][value="'+answers[needle].childs[k].answer+'"]').prop('checked', true);
							//checking the filled question
							for(var l = 0; l < questions[i][j].childQuestions[k].answer.length; l++) {
								if(questions[i][j].childQuestions[k].answer[l].correct == 1)
									$('#all .all-container:eq(-1) .child-question-container:eq(-1) label.inline-exam:eq('+l+')').after(correct);
								else if($('#all .all-container:eq(-1) .child-question-container:eq(-1) input[type="radio"]:eq('+l+')').prop('checked'))
									$('#all .all-container:eq(-1) .child-question-container:eq(-1) label.inline-exam:eq('+l+')').after(wrong);
							}
						}
						else if(questions[i][j].childQuestions[k].questionType == 6) {
							for(var l = 0; l < questions[i][j].childQuestions[k].answer.length; l++) {
								innerHTML += '<label class="checkbox-inline inline-exam">'
											+ '<input type="checkbox" name="opt' + i + '' + j + '' + k + '" value="' + questions[i][j].childQuestions[k].answer[l].option + '"> ' + questions[i][j].childQuestions[k].answer[l].option
										+ '</label><br/>';
							}
							innerHTML += '<div><strong>Explantion</strong> :' + questions[i][j].childQuestions[k].description + '</div></div>';
							$('#all .all-container:eq(-1)').append(innerHTML);
							//filling answer given by student for dpn question and child mcq questions
							if(answers[needle].childs != undefined && answers[needle].childs[k].answer != undefined) {
								for(var l = 0; l < questions[i][j].childQuestions[k].answer.length; l++) {
									$('#all .all-container:eq(-1) .child-question-container:eq(-1)').find('input[type="checkbox"][value="'+answers[needle].childs[k].answer[l]+'"]').prop('checked', true);
								}
							}
							//checking the filled question
							for(var l = 0; l < questions[i][j].childQuestions[k].answer.length; l++) {
								if(questions[i][j].childQuestions[k].answer[l].correct == 1)
									$('#all .all-container:eq(-1) .child-question-container:eq(-1) label.inline-exam:eq('+l+')').after(correct);
								else if($('#all .all-container:eq(-1) .child-question-container:eq(-1) input[type="checkbox"]:eq('+l+')').prop('checked'))
									$('#all .all-container:eq(-1) .child-question-container:eq(-1) label.inline-exam:eq('+l+')').after(wrong);
							}
						}
						else if(questions[i][j].childQuestions[k].questionType == 2) {
							for(var l = 0; l < questions[i][j].childQuestions[k].answer.length; l++) {
								innerHTML += '<div class="row" style="margin: 10px"><div class="col-md-3">'
											+ '<input type="text" class="form-control">'
										+ '</div></div>';
							}
							innerHTML += '<div><strong>Explantion</strong> :' + questions[i][j].childQuestions[k].description + '</div></div>';
							$('#all .all-container:eq(-1)').append(innerHTML);
							//filling answer given by student for dpn question and child ftb questions
							if(answers[needle].childs != undefined && answers[needle].childs[k].answer != undefined) {
								for(var l = 0; l < questions[i][j].childQuestions[k].answer.length; l++) {
									$('#all .all-container:eq(-1) .child-question-container:eq(-1)').find('input[type="text"]:eq('+l+')').val(answers[needle].childs[k].answer[l]);
								}
							}
							//checking the filled answers
							for(var l = 0; l < questions[i][j].childQuestions[k].answer.length; l++) {
								if(answers[needle].childs != undefined && answers[needle].childs[k].answer[l] != undefined)
									var userAns = answers[needle].childs[k].answer[l].trim().toLowerCase();
								else
									var userAns = '';
								var origAns = questions[i][j].childQuestions[k].answer[l].blanks;
								if(userAns == origAns)
									$('#all .all-container:eq(-1) .child-question-container:eq(-1) input[type="text"]:eq('+l+')').after(correct);
								else
									$('#all .all-container:eq(-1) .child-question-container:eq(-1) input[type="text"]:eq('+l+')').after(wrong + origAns);
							}
						}
						else if(questions[i][j].childQuestions[k].questionType == 1) {
							innerHTML += '<label class="radio-inline inline-exam">'
											+ '<input type="radio" name="opt' + i + '' + j + '' + k + '" value="true"> True'
										+ '</label><br/>'
										+ '<label class="radio-inline inline-exam">'
											+ '<input type="radio" name="opt' + i + '' + j + '' + k + '" value="false"> False'
										+ '</label><br/>';
							innerHTML += '<div><strong>Explantion</strong> :' + questions[i][j].childQuestions[k].description + '</div></div>';
							$('#all .all-container:eq(-1)').append(innerHTML);
							//filling answer given by student for dpn question and child mcq questions
							if(answers[needle].childs != undefined && answers[needle].childs[k].answer != undefined) {
								if(answers[needle].childs[k].answer == 1)
									$('#all .all-container:eq(-1) .child-question-container:eq(-1)').find('input[type="radio"]:eq(0)').prop('checked', true);
								else if(answers[needle].childs[k].answer == 1)
									$('#all .all-container:eq(-1) .child-question-container:eq(-1)').find('input[type="radio"]:eq(1)').prop('checked', true);
							}
							//checking the filled question
							if(questions[i][j].childQuestions[k].answer[0].answer == 1) {
								$('#all .all-container:eq(-1) .child-question-container:eq(-1) label.inline-exam:eq(0)').after(correct);
								if($('#all .all-container:eq(-1) .child-question-container:eq(-1) input[type="radio"]:eq(1)').prop('checked'))
									$('#all .all-container:eq(-1) .child-question-container:eq(-1) label.inline-exam:eq(1)').after(wrong);
							}
							else if(questions[i][j].childQuestions[k].answer[0].answer == 0) {
								$('#all .all-container:eq(-1) .child-question-container:eq(-1) label.inline-exam:eq(1)').after(correct);
								if($('#all .all-container:eq(-1) .child-question-container:eq(-1) input[type="radio"]:eq(0)').prop('checked'))
									$('#all .all-container:eq(-1) .child-question-container:eq(-1) label.inline-exam:eq(0)').after(wrong);
							}
						}
					}
					$('#all .all-container:eq(-1)').append('<hr>');
				}
			}
		}
		//event handler for buttons to switch between sections
		$('#buttons .section-answer').on('click', function() {
			$('#all .all-container').hide();
			number = $(this).attr('data-number');
			$('.section-answer').removeClass('btn-success').addClass('btn-info');
			$(this).addClass('btn-success').removeClass('btn-info');
			$('#all .all-container[data-number="'+number+'"]').show();
		});
		$('#buttons .section-answer:eq(0)').click();
	}
	
	//function to submit exam
	function submitExam() {
		saveAnswer();
		clearInterval(timer);
		clearInterval(questionTimer);
		clearInterval(s2MinTimer);
		autoSaveCounter = 60;
		needle = questions[sectionCount][selectedQuestion].id;
		var req = {};
		var res;
		req.action = 'update-user-answer';
		req.question = answers[needle];
		req.attemptId = attemptId;
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			var req = {};
			var res;
			req.action = 'check-and-save';
			req.examId = getUrlParameter('examId');
			req.attemptId = attemptId;
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(showResult == 1)
				{
					//window.location = 'studentResult.php?examId=' + getUrlParameter('examId');
					if(examType == 0) {
						//window.location = 'examSuccess.php';
						//window.location = 'studentResult.php?examId=' + getUrlParameter('examId');
						window.location = sitepathStudent+'exams/' + getUrlParameter('examId') + '/result/';
					/*else if(eachQuestionFlag) {
						alert("You have successfully completed your assignment. Thanks!!!");
						window.location = 'examSuccess.php';
					}*/
					}
					else {
						//window.location = 'studentResult.php?examId=' + getUrlParameter('examId');
						window.location = sitepathStudent+'exams/' + getUrlParameter('examId') + '/result/';
					}
				
				}else{
					if(showTill==2 && showFrom==2)
					alert("Submitted sucessfully.Result will be shown only from "+''+showdateFrom+'To '+showdatetill);
					else if(showTill==2 && showFrom==1)
					alert("Submitted sucessfully.Result will be shown till "+' '+showdatetill);
					else
					alert("Submitted sucessfully.Result will be shown after "+' '+showdateFrom);
					//window.location = 'MyExam.php?courseId='+courseId;
					window.location = sitepathStudent+'subjects/' + res.subjectId;
				}
			});
		});
	}
	
	//function to send all questions to server for auto save
	function sendQuestions() {
		var req = {};
		var res;
		req.action = 'save-all-questions';
		req.questions = answers;
		req.examId = getUrlParameter('examId');
		req.attemptId = attemptId;
		req.completed = 0;
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
		});
	}
	
	//function to start timer for each question
	function startQuestionTimer(needle) {
		questionTimer = setInterval(function() {
			answers[needle].timer += 1;
			//for temporary time display of each question
			//$('#tempQuestionTimer').text(answers[needle].timer);
		},	1000);
	}
	
	//function to stop timer for each question
	function stopQuestionTimer() {
		clearInterval(questionTimer);
	}
	
	//function to fetch data from url
	function getUrlParameter(sParam) {
		sParam = sParam.toLowerCase();
		var sPageURL = window.location.search.substring(1).toLowerCase();
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++) 
		{
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam) 
			{
				return sParameterName[1];
			}
		}
	}

});