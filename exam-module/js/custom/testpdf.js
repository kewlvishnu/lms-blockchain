var ApiEndPoint = '../api/index.php';
var contentIds = [];
var chapterId = getUrlParameter('chapterId');
var courseId=getUrlParameter('courseId');
var subjectId=getUrlParameter('subjectId');
$(function() {
	fetchBreadCrumb();
	fetchHeadings();
	
	//event handler for next button
	$('#next').on('click', function() {
		/*if($(this).attr('data-id') == 0) {
			console.log($('.accordion-toggle[data-heading='+$(this).attr('data-heading')+']').text());
			$('#contentHeading').attr('data-id', '0');
			$('#contentHeading').attr('data-heading', $(this).attr('data-heading'));
			var nextContent = $('.accordion-toggle[data-heading='+$(this).attr('data-heading')+']').parents('.panel-default').find('.content-view:eq(0)').attr('data-id');
			$('#next').attr('data-id', nextContent);
		}
		else*/
			$('.content-view[data-id="'+$(this).attr('data-id')+'"]').click();
	});
	
	//event handler for previous button
	$('#previous').on('click', function() {
		$('.content-view[data-id="'+$(this).attr('data-id')+'"]').click();
	});
	
	//function to fetch and fill breadcrumbs
	function fetchBreadCrumb() {
		var req = {};
		var res;
		req.action = 'get-breadcrumb-for-chapter';
		req.chapterId = chapterId;
               	$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				$('#breadcrumb .btc:eq(0)').html('<a href="MyExam.php?courseId='+courseId+'" class="custom-btc-link">'+res.breadcrumb.courseName+'</a>');
				$('#breadcrumb .btc:eq(1)').html('<a href="chapter.php?courseId='+courseId+'&subjectId='+subjectId+'" class="custom-btc-link">'+res.breadcrumb.subjectName+'</a>');
				$('#breadcrumb .btc:eq(2)').html('<a href="#" class="custom-btc-link">'+res.breadcrumb.chapterName+'</a>');
			}
		});
	}
	
	//function to fetch headings of the chapter
	function fetchHeadings() {
		var req = {};
		var res;
		req.action = 'get-headings';
		req.chapterId = getUrlParameter('chapterId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
				fillHeadings(res);
		});
	}
	
	//function to fill headings and generate side view
	function fillHeadings(data) {
		var html = '';
		var chapterNum = parseInt(data.chapterNum);
		for(var i = 0; i < data.headings.length; i++) {
			html += '<div class="panel panel-default">'
					+ '<div class="panel-heading">'
						+ '<h3 class="panel-title">'
							+ '<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse' + i + '" data-heading="' + data.headings[i].id + '">Heading ' + (chapterNum + 1) + '.' + (i+1) + ' - ' + data.headings[i].title+'</a>'
						+ '</h3>'
					+ '</div>'
					+ '<div id="collapse'+i+'" class="panel-collapse collapse in" style="height: auto;">'
						+ '<div class="panel-body">'
							+ '<div class="timeline-messages">';
			//contentIds.push('0');
			for(var j = 0; j < data.headings[i].content.length; j++) {
				contentIds.push(data.headings[i].content[j].id);
				html += '<div class="msg-time-chat">'
							+ '<a href="#" class="message-img">';
				if(data.headings[i].content[j].contentType == "doc")
					html += '<img class="avatar" src="img/pdf.jpg" alt="">';
				else if(data.headings[i].content[j].contentType == "ppt")
					html += '<img class="avatar" src="img/pdf.png" alt="">';
				else if(data.headings[i].content[j].contentType == "text")
					html += '<img class="avatar" src="img/lec.png" alt="">';
				else if(data.headings[i].content[j].contentType == "video")
					html += '<img class="avatar" src="img/vid.jpg" alt="">';
				else if(data.headings[i].content[j].contentType == "dwn")
					html += '<img class="avatar" src="img/download.png" alt="">';
				else if(data.headings[i].content[j].contentType == "Youtube")
					html += '<img class="avatar" src="img/youtube.png" alt="">';
				else if(data.headings[i].content[j].contentType == "Vimeo")
					html += '<img class="avatar" src="img/vimeo.png" alt="">';
						html += '</a>'
							+ '<div class="message-body msg-in">'
								+ '<span class="arrow"></span>'
								+ '<div class="text">'
									+ '<p class="attribution">'
										+ '<a data-id="'+data.headings[i].content[j].id+'" class="content-view">Sub Heading ' + (chapterNum + 1) + '.' + (i+1) + '.' + (j+1) + ' : ' + data.headings[i].content[j].title + '</a>'
									+ '</p>'
									+ '<p>' + data.headings[i].content[j].contentType;
				if(data.headings[i].content[j].downloadable == 1)
					html += '<a href="downloadStuff?contentId='+data.headings[i].content[j].id+'" target="_blank"><i class="fa fa-download"></i></a></p>';
				else
					html += '</p>';
				html +=			 '</div>'
							+ '</div>'
						+ '</div>';
			}
			html +=		'</div>'                                       
					+ '</div>'
				+ '</div>'
			+ '</div>';
		}
		$('#accordion').append(html);
		
		//event listener for content views
		$('.content-view').on('click', function() {
			$('#contentHeading').html($(this).text());
			$('#contentHeading').attr('data-id', $(this).attr('data-id'));
			var req = {};
			var res;
			req.action = 'get-content-stuff';
			req.contentId = $(this).attr('data-id');
			$.ajax({
					'type'	:	'post',
					'url'	:	ApiEndPoint,
					'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else
					fillContentDetails(res);
			});
		});
		$('.content-view:eq(0)').click();
	}
	
	//function to fill content details
	function fillContentDetails(data) {
		//filling next button
		$('#next').show();
		if(contentIds[contentIds.indexOf($('#contentHeading').attr('data-id'))+1] != undefined) {
			var index = $('#contentHeading').attr('data-id');
			/*if(index == 0) {
				console.log('aaaaa');
			}
			else {*/
				$('#next').attr('data-id', contentIds[contentIds.indexOf(index)+1]);
				/*if(contentIds[contentIds.indexOf($('#contentHeading').attr('data-id'))+1] == 0) {
					var headingId = $('.accordion-toggle:eq(' + (contentIds.indexOf($('#contentHeading').attr('data-id')) - 1) + ')').attr('data-heading');
					$('#next').attr('data-heading', headingId);
				}
			}*/
		}
		else
			$('#next').hide();
			
		//filling previous button
		$('#previous').show();
		if(contentIds[contentIds.indexOf($('#contentHeading').attr('data-id'))-1] != undefined)
			$('#previous').attr('data-id', contentIds[contentIds.indexOf($('#contentHeading').attr('data-id'))-1]);
		else
			$('#previous').hide();
		
		$('#block-everything').hide();
		if(data.content.type == "text") {
			$('#content-view .custom-content-part').html(data.content.stuff);
			$('#block-everything').show();
		}
		else if(data.content.type == "dwn") {
			var html = '<iframe src="downloader.php?filename='+data.content.stuff+'" heigth="0" width="0" style="display: none;visibility: hidden; border: 0px;"></iframe>Your file will be downloaded soon.';
			$('#content-view .custom-content-part').html(html);
		}
		else if(data.content.type == "Youtube") {
			var videoId = getStringParameter('v', data.content.stuff);
			var html = '<object type="application/x-shockwave-flash" id="youtubeFlash" data="https://www.youtube.com/v/' +
			 videoId + '" width="880" height="400"><param name="allowScriptAccess" value="always"><param name="bgcolor" value="#cccccc"></object>';
			$('#content-view .custom-content-part').html(html);
		}
		else if(data.content.type == "Vimeo") {
			var videoId = getVimeoId(data.content.stuff);
			var html = '<object width="880" height="400" id="vimeoFlash">'
					+ '<param name="allowfullscreen" value="true" />'
					+ '<param name="allowscriptaccess" value="always" />'
					+ '<param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id='+videoId+'&amp;server=vimeo.com&amp;color=00adef&amp;fullscreen=1" />'
					+ '<embed src="http://vimeo.com/moogaloop.swf?clip_id='+videoId+'&amp;server=vimeo.com&amp;color=00adef&amp;fullscreen=1" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="880" height="400"></embed>'
				+ '</object>';
			$('#content-view .custom-content-part').html(html);
		}
		else if(data.content.type == "ppt" || data.content.type == "doc" || data.content.type == "pdf" ) {
//			var html = '<object data="' + data.content.stuff + '" type="application/pdf" height="400" width="880">'
//						+ '<embed src="' + data.content.stuff + '" type="application/pdf">&nbsp; </embed>'
//						+ '</object>';
                       
                       // var html='<iframe style="float:right;" id="viewer" src = "assets/ViewerJS/#'+data.content.stuff+'" width=880 height=400  allowfullscreen webkitallowfullscreen ></iframe>'
                       var html='<iframe style="float:right;" id="viewer" src = "assets/ViewerJS/#../../user-data/files/demo.pdf" width=880 height=400  allowfullscreen webkitallowfullscreen ></iframe>'
                        console.log(html);
                         $('#content-view .custom-content-part').html(html);
		}
		else if(data.content.type == "video") {
			//code for using media elemnt
			var html = '<video src="'+data.content.stuff+'" height="400" width="880" controls="controls"></video>';
			$('#content-view .custom-content-part').html(html);
			$('#content-view .custom-content-part video').mediaelementplayer();
			/* code for using flow player
			var html = '<div class="flowplayer" style="width: 880px;height: 400px;">'
						+ '<video src="'+data.content.stuff+'"></video>'
					+ '</div>';
			$('#content-view .custom-content-part').html(html);
			$('.flowplayer').flowplayer();*/
		}
	}
	
	$('.fa-plus').click(function (e) {
		if ($('#sidebar > ul').is(":visible") === true) {
			$('#sidebar').css({
				'margin-left': '-410px'
			});
			$('.custom-course').css({
				'margin-left': '0px'
			});
			$('#sidebar > ul').hide();
			$("#container").addClass("sidebar-closed");
		} else {
			$('.custom-course').css({
				'margin-left': '17%'
			});
			$('#sidebar > ul').show();
			$('#sidebar').css({
				'margin-left': '0px'
			});
			$("#container").removeClass("sidebar-closed");
		}
    });
	
	//function to fetch data from url string
	function getStringParameter(sParam, url) {
		var sPageURL = url.split('?')[1];
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++) 
		{
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam) 
			{
				return sParameterName[1];
			}
		}
	}
	
	//function to fetch video id from vimeo link
	function getVimeoId(url) {
		var sURLVariables = url.split('/');
		return sURLVariables[sURLVariables.length - 1];
	}
	
});