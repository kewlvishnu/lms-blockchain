var ApiEndPoint = '../api/index.php';
var details;

$(function() {
	
	fetchUserDetails();
	
	//event handler to change currency
	$('.changeCurrency').on('click', function() {
		if($(this).hasClass('btn-white')) {
			var currency = $(this).attr('currency');
			var req = {};
			var res;
			req.action = 'SetCurrencySetting';
			req.currency = currency;
			$.ajax({
					'type'	:	'post',
					'url'	:	ApiEndPoint,
					'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else {
					details.details.currency = req.currency;
					fillUserDetails(details);
				}
			});
		}
	});
	
	//event handler to change password
	$('#changePwd').on('click', function() {
		var oldPwd = $('#oldPwd').val();
		var newPwd = $('#newPwd').val();
		//validation required
		unsetError($('#oldPwd'));
		if(oldPwd.length < 6) {
			setError($('#oldPwd'), 'Please provide a password greater than 6 characters.');
			return;
		}
		unsetError($('#newPwd'));
		if(newPwd.length < 6) {
			setError($('#newPwd'), 'Please provide a password greater than 6 characters.');
			return;
		}
		unsetError($('#repeatPwd'));
		if(newPwd != $('#repeatPwd').val()) {
			setError($('#repeatPwd'), 'Your password does not match.');
			return;
		}
		var req = {};
		var res;
		req.action = 'changePassword';
		req.oldPwd = oldPwd;
		req.newPwd = newPwd;
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			$('#oldPwd, #newPwd, #repeatPwd').val('');
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				alertMsg('Password changed successfully.');
			}
		});
	});
	
	//function to fetch user details for change password page
	function fetchUserDetails() {
		var req = {};
		var res;
		req.action = 'get-student-details-for-changePassword';
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				details = res;
				fillUserDetails(res);
			}
		});
	}
	
	//function to fill user details
	function fillUserDetails(data) {
		$('.notify.badge').text(data.notiCount);
		$('.fullName').text(data.details.firstName + ' ' + data.details.lastName);
		$('.username').text(data.details.username);
		$('.user-image').attr('src', data.details.profilePic);
		$('#btn_rupee, #btn_dollar').removeClass('btn-white btn-info');
		if(data.details.currency == 1) {
			$('#btn_dollar').addClass('btn-info');
			$('#btn_rupee').addClass('btn-white');
		}
		else if(data.details.currency == 2) {
			$('#btn_dollar').addClass('btn-white');
			$('#btn_rupee').addClass('btn-info');
		}
	}
	
	//function to unset error
	function unsetError(where) {
		if(where.parents('div:eq(0)').hasClass('has-error')) {
			where.parents('div:eq(0)').removeClass('has-error');
			where.parents('div:eq(0)').find('.help-block').remove();
		}
	}
	
	//function to set error
	function setError(where, what) {
		where.parents('div:eq(0)').addClass('has-error');
		where.parents('div:eq(0)').append('<span class="help-block">'+what+'</span>');
	}
	
});