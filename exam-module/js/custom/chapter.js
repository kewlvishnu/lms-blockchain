var ApiEndPoint = '../api/index.php';
var details;
var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var courseId = getUrlParameter('courseId');
var subjectId = getUrlParameter('subjectId');
var rating;
var certificate;
var check=1;
var showdatetill='';
var showdateFrom='';
var showTill=1;
var showFrom=1;
var subjectiveExams = [];
var manualExams = [];
$(function () {

	checkCourseAcess();
	function checkCourseAcess() {
		var req = {};
		var res;
		req.action = 'checkCourseAcess';
		req.courseId = courseId;
		req.subjectId = subjectId;
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			//console.log(res);
			if (res.status == 0)
				alert('Not Allowed to view this chapter');
			else {
				fetchUserDetails();
				getStudentExams();
				fetchUserRating();
				getSubjectProfessors();
			}
		});
	}
	// fetchCourseDetails();
	//function to fetch user details for change password page
	function fetchUserDetails() {
		var req = {};
		var res;
		req.action = 'get-student-details-for-changePassword';
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			if (res.status == 0)
				alert(res.message);
			else {
				details = res;
				fillUserDetails(res);
			}
		});
	}

	//function to fill user details
	function fillUserDetails(data) {
		$('.notify.badge').text(data.notiCount);
		$('.fullName').text(data.details.firstName + ' ' + data.details.lastName);
		$('.username').text(data.details.username);
		$('.user-image').attr('src', data.details.profilePic);
		$('#btn_rupee, #btn_dollar').removeClass('btn-white btn-info');
		if (data.details.currency == 1) {
			$('#btn_dollar').addClass('btn-info');
			$('#btn_rupee').addClass('btn-white');
		}
		else if (data.details.currency == 2) {
			$('#btn_dollar').addClass('btn-white');
			$('#btn_rupee').addClass('btn-info');
		}
	}
	function getSubjectProfessors() {
		var req = {};
		var res;
		req.action = 'get-subject-professors-chat';
		req.courseId = courseId;
		req.subjectId = subjectId;
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			//console.log(res);
			if (res.status == 0)
				console.log(res.message);
			else {
				fillSubjectProfessors(res.professors);
			}
		});

	}
	function fillSubjectProfessors (data) {
		console.log(data);
		if (data.length>0) {
			for (var i = 0; i < data.length; i++) {
				/*$('#dropdownChat ul').append('<li><a href="../messenger/'+data[i].username+'" class="chat-initiate">'+data[i].firstName+' '+data[i].lastName+'</a></li>');*/
				$('#dropdownChat ul').append('<li><a href="javascript:void(0)" data-professor="'+data[i].professorId+'" class="chat-initiate">'+data[i].firstName+' '+data[i].lastName+'</a></li>');
			};
			$('#dropdownChat ul').append('<li><a href="javascript:void(0)" data-professor="0" class="chat-initiate">All Professors</a></li>');
			$('#dropdownChat').removeClass("hide");
		} else {
			$('#dropdownChat').remove();
		}
	}
	$('#dropdownChat').on("click", "a", function(){
		var req = {};
		var res;
		req.action = 'get-student-chat-link';
		req.courseId = courseId;
		req.subjectId = subjectId;
		req.professorId = $(this).attr("data-professor");
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			//console.log(res);
			if (res.status == 0)
				console.log(res.message);
			else {
				//window.location = res.roomId;
				window.open('../messenger/'+res.roomId);
			}
		});
	});
	function getStudentExams() {
		var req = {};
		var res;
		req.action = 'exam-attempts-chapterwise';
		req.courseId = courseId;
		req.subjectId = subjectId;
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			//console.log(res);
			if (res.status == 0)
				console.log(res.message);
			else {
				fillStudentExams(res);
			}
		});

	}
	function fillStudentExams(data) {
		//console.log(data);
		var examHtml = '', AssignmentHtml = '';
		var divHtml = '', tr = '', divIntial = '', divSubIndHtml = '', divManualHtml = '', divClose = '';
		var contentLink = '';
		var count = 1;
		var disabled = '', startnow = '';
		var syllabus= data.syllabus;
		if(syllabus !='' && syllabus != undefined)
		{	var urlAbsolute='assets/ViewerJS/#'+syllabus;
			$('#viewSyllabus #viewerSyllabus').attr("src", urlAbsolute);
			//$('#viewSyllabusButton1').show();
			$('#viewSyllabusButton1').removeClass("hide");
			
		}
		$('#course-name').html('<a href="MyExam.php?courseId='+courseId+'">'+data.course+'</a>');
		$('.subject-name').html('<strong><a href="#">'+data.subject+'</strong></a>');
		$('#subjectName').removeClass('hide');
		$.each(data.chapters, function (i, chapter) {
		
			contentLink ='chapterContent.php?courseId='+courseId+'&subjectId='+subjectId+'&chapterId=' + chapter.id;
			divIntial += '<div class="panel all-container">'
					+ '<div class="panel-heading">'
					+ '<div class="row">'
					+ '<div class="col-lg-4"><h4>Section ' + (i + 1) + ': <strong>' + chapter.name + '</strong></h4></div>'
					+ '<div class="col-lg-2 col-md-3 col-sm-6 text-right">';
			if(chapter.content) {
				//console.log(chapter.content);
				if (chapter.exams.length > 0) {
					divIntial += ' <a class="btn btn-md btn-success btn-custom viewExam" data-hidden="1">Hide Assignment/Exam</a>';
				}
				divIntial += '</div><div class="col-lg-2 col-md-3 col-sm-6 text-right">';
				if (chapter.subjectiveExams.length > 0) {
					divIntial += '<a class="btn btn-md btn-success btn-custom viewSubjectiveExam" data-hidden="1">Hide Subjective Exam</a>';
				}
				divIntial += '</div><div class="col-lg-2 col-md-3 col-sm-6 text-right"><a href="' + contentLink + '" class="btn btn-md btn-success btn-custom">View Content</a>';
				var coursePercentatge=Math.round((chapter.seen/chapter.content.length) *100);
				divIntial += '</div><div class="col-lg-2 col-md-3 col-sm-6 course-percent">  '+coursePercentatge+' % Completed';
				divIntial += '</div></div></div>';
				divIntial += '<div class="unpublish"><table class="table tbl_exams">'
								+'<thead><tr><th class="tbl-header-row">Title</th></tr></thead><tbody>';
				$.each(chapter.content, function (j, content) {
					divIntial += '<tr><td>'+content.title+((content.published==0)?' <span class="text-danger">(Not published)</span>':'')+'</td></tr>';
				});
				divIntial += '</tbody></table></div>';
			}
			else {
				if (chapter.exams.length > 0) {
					divIntial += '<a class="btn btn-md btn-success btn-custom viewExam" data-hidden="1"">Hide Assignment/Exam</a>';
				}
				divIntial += '</div><div class="col-lg-2 text-right">';
				if (chapter.subjectiveExams.length > 0) {
					divIntial += '<a class="btn btn-md btn-success btn-custom viewSubjectiveExam" data-hidden="1">Hide Subjective Exam</a>';
				}
				divIntial += '</div><div class="col-lg-1 text-right">';
				divIntial += '</div></div></div>';
				divIntial += '<p>No content uploaded</p>';
			}

			if (chapter.exams.length > 0) {
				divIntial += '<div class="panel-heading ch-exams">'
						+ '<h5>Assignment Exams</h5>'
						+ '</div>'
						+ '<div class="div-exams">'
						+ '<table class="table tbl_exams"> '
						+ '<thead>'
						+ '<tr>'
						+ '<th width="30%" class="tbl-header-row">Exam<br></th>'
						+ '<th width="10%" class="tbl-header-row">Type<br></th>'
						+ '<th width="10%" class="tbl-header-row">Start Date</th>'
						+ '<th width="10%" class="tbl-header-row">End Date</th>'
						+ '<th width="10%" class="tbl-header-row text-center">Attempts Given</th>'
						+ '<th width="10%" class="tbl-header-row text-center">Attempts Left</th>'
						+ '<th width="10%" class="tbl-header-row text-center">Results</th>'
						+ '<th width="10%" class="tbl-header-row"></th>'
						+ '</tr>'
						+ '</thead>'
						+ '<tbody>';

				tr = '';
				$.each(chapter.exams, function (i, exam) {
					check=1;
					showdatetill='';
					showdateFrom='';
					showTill=1;
					showFrom=1;
					var today= $.now(); // added later by Jackie
					if(exam.showResult=='immediately' || exam.endDate=='')
					{
						check=1;
					}
					else{
						if(today> exam.endDate)
						{
							check=1;
						}			
						else{
							check=2;
							showFrom=2;
							var tillDate = new Date(parseInt(exam.endDate));
							if(tillDate!='' || tillDate!=null)
							{
								var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
								if(tillDate.getHours() < 10)
									tsd += '0' + tillDate.getHours();
								else
									tsd += tillDate.getHours();
								tsd += ':';
								if(tillDate.getMinutes() < 10)
									tsd += '0' + tillDate.getMinutes();
								else
									tsd += tillDate.getMinutes();
							}
							showdateFrom=tsd;
						}
					}
					if((exam.showResultTill == 2 && today< exam.showResultTillDate)|| exam.showResultTill == 1)
					{	
						check=1;
								
					}			
					else{
						check=2;
						showTill=2;
						showdatetill=exam.showResultTillDate;
						var tillDate = new Date(parseInt(showdatetill));
						if(tillDate!='' || tillDate!=null)
						{
							var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
							if(tillDate.getHours() < 10)
								tsd += '0' + tillDate.getHours();
							else
								tsd += tillDate.getHours();
							tsd += ':';
							if(tillDate.getMinutes() < 10)
								tsd += '0' + tillDate.getMinutes();
							else
								tsd += tillDate.getMinutes();
						}											
						showdatetill=tsd;
					}																			
			
					var show='';
					var startdate = new Date(parseInt(exam.startDate));
					var display_chapter;
					var enddate, display_enddate;
					if (exam.endDate == null || exam.endDate == '') {
						display_enddate = '--';
					} else {
						enddate = new Date(parseInt(exam.endDate));
						display_enddate = enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + '<br/>' + enddate.getHours() + ':' + enddate.getMinutes();
					}
					if (exam.examgiven == 0) {
						disabled = 'disabled="disabled"';
					} else {
						disabled = '';
					}
					var attempsLeft = exam.attempts - exam.examgiven;
					var buttonText = '';
					if(exam.paused == 0)
						buttonText = 'Start Test';
					else
						buttonText = 'Resume';
					tr += '<tr>'
						+ '<td>' + exam.exam + '</td>'
						+ '<td>' + exam.type + '</td>'
						+ '<td>' + startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + '<br>' + startdate.getHours() + ':' + startdate.getMinutes() + '</td>'
						+ '<td>' + display_enddate + '</td>'
						+ '<td class="text-center">' + exam.examgiven + '</td>'
						+ '<td class="text-center">' + (exam.attempts - exam.examgiven) + '</td>';
					if(check == 1)
					{
					tr+= '<td class="text-center"><a style="text-decoration: underline;" ' + disabled + ' href="studentResult.php?examId=' + exam.id + '"  class="btn btn-md myHover">Show</a></td>';
					}
					else{
						if(showTill==2 && showFrom==2)
						{	
							show= 'Result will be shown only from'+showdateFrom+ 'to' +showdatetill;
							tr+= '<td class="text-center">'+show+'</td>';
							
						}
						//("Submitted sucessfully.Result will be shown only from "+''+showdateFrom+'To '+showdatetill);
						else if(showTill==2 && showFrom==1)
						{
							show= 'Result was shown till '+showdatetill;
							tr+= '<td class="text-center">'+show+'</td>';
						}
						//alert("Submitted sucessfully.Result will be shown till "+' '+showdatetill);
						else
						{
							show= 'Result will be shown after '+showdateFrom;
							tr+= '<td class="text-center">'+show+'</td>';
					
						}
						//alert("Submitted sucessfully.Result will be shown after "+' '+showdateFrom);
					}																					
					
					//+ '<td class="text-center border-none"><a ' + disabled + ' href="studentResult.php?examId=' + exam.id + '" class="btn td-show-btn btn-xs ">Show</a></td>'
					tr+= '<td class="text-center"><a  href="examModule.php?examId=' + exam.id + '" class="btn btn-success btn-sm start-exam btn-custom" data-startdate="' + exam.startDate + '" data-endDate=' + exam.endDate + ' data-attempts=' + attempsLeft + ' DISABLED>' + buttonText + '</a></td>'
					+ '</tr>';

				});
				divIntial = divIntial + tr + '</tbody>'
						+ '</table>';
			}
			divIntial += ' </div>';
			if (chapter.subjectiveExams.length > 0) {
				divIntial += '<div class="div-subjective-exams">'
						+ '<strong>Subjective Exams</strong>'
						+ '<table class="table tbl_exams"> '
						+ ' <thead>'
						+ ' <tr>'
						+ '<th width="30%" class="tbl-header-row">Exam<br></th>'
						+ '<th width="10%" class="tbl-header-row">Type<br></th>'
						+ '<th width="10%" class="tbl-header-row">Start Date</th>'
						+ '<th width="10%" class="tbl-header-row">End Date</th>'
						+ '<th width="10%" class="tbl-header-row text-center">Attempts Given</th>'
						+ '<th width="10%" class="tbl-header-row text-center">Attempts Left</th>'
						+ '<th width="10%" class="tbl-header-row text-center">Results</th>'
						+ '<th width="10%" class="tbl-header-row"></th>'
						+ '</tr>'
						+ '</thead>'
						+ '<tbody>';

				tr = '';
				$.each(chapter.subjectiveExams, function (i, exam) {
					subjectiveExams.push(exam);
					check=1;
					showdatetill='';
					showdateFrom='';
					showTill=1;
					showFrom=1;
					var today= $.now();

					check=2;
					showFrom=2;
					var tillDate = new Date(parseInt(exam.endDate));
					if(tillDate!='' || tillDate!=null)
					{
						var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
						if(tillDate.getHours() < 10)
							tsd += '0' + tillDate.getHours();
						else
							tsd += tillDate.getHours();
						tsd += ':';
						if(tillDate.getMinutes() < 10)
							tsd += '0' + tillDate.getMinutes();
						else
							tsd += tillDate.getMinutes();
					}
					showdateFrom=tsd;																			
			
					var show='';
					var startdate = new Date(parseInt(exam.startDate));
					var display_chapter;
					var enddate, display_enddate;
					if (exam.endDate == null || exam.endDate == '') {
						display_enddate = '--';
					} else {
						enddate = new Date(parseInt(exam.endDate));
						display_enddate = enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + '<br/>' + enddate.getHours() + ':' + enddate.getMinutes();
					}
					if (exam.examgiven == 0) {
						disabled = 'disabled="disabled"';
					} else {
						disabled = '';
					}
					var attempsLeft = exam.attempts - exam.examgiven;
					disabledStartTest = '';
					if(attempsLeft<=0)
					{
						attempsLeft			=	0;
						disabledStartTest	= 'disabled="disabled"';
					}
					disabledStartTest	= 'disabled="disabled"';
					var buttonText = '';
					if(exam.paused == 0)
						buttonText = 'Start Test';
					else
						buttonText = 'Resume';
					tr += '<tr>'
						+ '<td class="border-none ">' + exam.exam + '</td>'
						+ '<td class="border-none">' + exam.type + '</td>'
						+ '<td class="border-none">' + startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + '<br>' + startdate.getHours() + ':' + startdate.getMinutes() + '</td>'
						+ '<td class="border-none">' + display_enddate + '</td>'
						+ '<td class="text-center border-none">' + exam.examgiven + '</td>'
						+ '<td class="text-center border-none">' + (exam.attempts - exam.examgiven) + '</td>';
					tr+= '<td class="text-center border-none"><a style="text-decoration: underline;"' + disabled + ' href="javascript:void(0)"  class="btn btn-md js-check-attempts" data-examid="' + exam.id + '">Show</a></td>';					
					tr+= '<td class="text-center border-none"><a '+disabledStartTest+'href="subjectiveExamModule.php?courseId=' + getUrlParameter('courseId') + '&examId=' + exam.id + '"  class="btn btn-success btn-sm start-exam" data-startdate="' + exam.startDate + '" data-endDate="' + exam.endDate + '" data-attempts=' + attempsLeft + ' disabled>' + buttonText + '</a></td>'
						+ '</tr>';

				});
				divIntial = divIntial + tr + '</tbody>'
						+ '</table>';
			}
			divIntial += ' </div>'
					+ '</div>';
		});

		if (data.indepdentExams.length > 0) {
			divHtml = '<div class="panel all-container">'
					+ '<div class="panel-heading">'
					+ '<div class="row">'
					+ '<div class="col-md-6"><h4><strong>Independent Assignments/Exams</strong></h4></div>'
					+ '<div class="col-md-4 text-right"> <a class="btn btn-md btn-custom btn-success" id="viewIndependent">Hide Assignment/Exam</a></div><div class="col-md-2">'
					+ /*' <a class="btn btn-md btn-custom btn-success" id="hideIndependent">Cancel</a>'*/'</div>'
					+ '</div>'
					+ '</div>'
					+ '<div class="div-exams">'
					+ '<table class="table tbl_exams"> '
					+ '<thead>'
					+ '<tr>'
					+ '<th width="30%" class="tbl-header-row">Exam<br></th>'
					+ '<th width="10%" class="tbl-header-row">Type<br></th>'
					+ '<th width="10%" class="tbl-header-row">Start Date</th>'
					+ '<th width="10%" class="tbl-header-row">End Date</th>'
					+ '<th width="10%" class="tbl-header-row text-center">Attempts Given</th>'
					+ '<th width="10%" class="tbl-header-row text-center">Attempts Left</th>'
					+ '<th width="10%" class="tbl-header-row text-center">Results</th>'
					+ '<th width="10%" class="tbl-header-row"></th>'
					+ '</tr>'
					+ '</thead>'
					+ '<tbody>';
			tr = '';
			$.each(data.indepdentExams, function (i, exam) {
				check=1;
				showdatetill='';
				showdateFrom='';
				showTill=1;
				showFrom=1;
				var today= $.now();

				if(exam.showResult=='immediately' || exam.endDate=='')
				{
				check=1;
			}		
				else{
					if(today> exam.endDate)
					{
						check=1;
					}			
					else{
					check=2;
					showFrom=2;
					var tillDate = new Date(parseInt(exam.endDate));
					if(tillDate!='' || tillDate!=null)
					{
						var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
						if(tillDate.getHours() < 10)
							tsd += '0' + tillDate.getHours();
						else
							tsd += tillDate.getHours();
						tsd += ':';
						if(tillDate.getMinutes() < 10)
							tsd += '0' + tillDate.getMinutes();
						else
							tsd += tillDate.getMinutes();
					}
					showdateFrom=tsd;
				}																		
				}																								
				if((exam.showResultTill == 2 && today< exam.showResultTillDate)|| exam.showResultTill == 1)
				{	
				check=1;		
				}						
				else{
					check=2;
					showTill=2;
					showdatetill=exam.showResultTillDate;
					var tillDate = new Date(parseInt(showdatetill));
					if(tillDate!='' || tillDate!=null)
					{
						var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
						if(tillDate.getHours() < 10)
							tsd += '0' + tillDate.getHours();
						else
							tsd += tillDate.getHours();
						tsd += ':';
						if(tillDate.getMinutes() < 10)
							tsd += '0' + tillDate.getMinutes();
						else
							tsd += tillDate.getMinutes();
					}											
					showdatetill=tsd;
				}																																						
			
				var show='';

				var startdate = new Date(parseInt(exam.startDate));
				var display_chapter;
				var enddate, display_enddate;
				if (exam.endDate == null || exam.endDate == '') {
					display_enddate = '--';
				} else {
					enddate = new Date(parseInt(exam.endDate));
					display_enddate = enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + '<br/>' + enddate.getHours() + ':' + enddate.getMinutes();
				}
				if (exam.examgiven == 0) {
					disabled = 'disabled="disabled"';
				} else {
					disabled = '';
				}
				var attempsLeft = exam.attempts - exam.examgiven;
				if(exam.paused == 0)
						buttonText = 'Start Test';
					else
						buttonText = 'Resume';
				tr += '<tr>'
						+ '<td>' + exam.exam + '</td>'
						+ '<td>' + exam.type + '</td>'
						+ '<td>' + startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + '</br>' + startdate.getHours() + ':' + startdate.getMinutes() + '</td>'
						+ '<td>' + display_enddate + '</td>'
						+ '<td class="text-center">' + exam.examgiven + '</td>'
						+ '<td class="text-center">' + (exam.attempts - exam.examgiven) + '</td>';
						if(check == 1)
						{
							tr+= '<td class="text-center border-none"><a style="text-decoration: underline;" ' + disabled + ' href="studentResult.php?examId=' + exam.id + '"  class="btn btn-md myHover">Show</a></td>';
						}
						else{
								if(showTill==2 && showFrom==2)
								{	
									show= 'Result will be shown only from'+showdateFrom+ 'to' +showdatetill;
									tr+= '<td class="text-center border-none">'+show+'</td>';
									
								}
								//("Submitted sucessfully.Result will be shown only from "+''+showdateFrom+'To '+showdatetill);
								else if(showTill==2 && showFrom==1)
								{
									show= 'Result was shown till '+showdatetill;
									tr+= '<td class="text-center border-none">'+show+'</td>';
								}
								//alert("Submitted sucessfully.Result will be shown till "+' '+showdatetill);
								else
								{
									show= 'Result will be shown after '+showdateFrom;
									tr+= '<td class="text-center border-none">'+show+'</td>';
							
								}
						//alert("Submitted sucessfully.Result will be shown after "+' '+showdateFrom);
						}
						//+ '<td class="text-center border-none"><a ' + disabled + ' href="studentResult.php?examId=' + exam.id + '" class="btn td-show-btn btn-xs ">Show</a></td>'
						tr+= '<td class="text-center"><a  href="examModule.php?examId=' + exam.id + '" class="btn btn-success btn-sm btn-custom start-exam" data-startdate="' + exam.startDate + '" data-endDate=' + exam.endDate + ' data-attempts=' + attempsLeft + ' DISABLED>' + buttonText + '</a></td>'
						+ '</tr>';
			});
			divHtml = divHtml + tr + '</tbody>'
					+ '</table></div>'
					+ '</div><br/><br/>';
		}

		if (data.independentSubjectiveExams.length > 0) {
			divSubIndHtml = '<div class="panel all-container">'
					+ '<div class="panel-heading">'
					+ '<div class="row">'
					+ '<div class="col-md-6"><h4><strong>Independent Subjective Exams</strong></h4></div>'
					+ '<div class="col-md-4 text-right"> <a  class="btn btn-md btn-custom btn-success" id="viewSubjectiveIndependent">Hide Subjective Exams</a></div><div class="col-md-2">'
					+ /*' <a class="btn btn-md btn-custom btn-success" id="hideSubjectiveIndependent">Cancel</a>'*/'</div>'
					+ '</div>'
					+ '</div>'
					+ '<div class="div-exams">'
					+ '<table class="table tbl_exams"> '
					+ '<thead>'
					+ '<tr>'
					+ '<th width="30%" class="tbl-header-row">Exam<br></th>'
					+ '<th width="10%" class="tbl-header-row">Type<br></th>'
					+ '<th width="10%" class="tbl-header-row">Start Date</th>'
					+ '<th width="10%" class="tbl-header-row">End Date</th>'
					+ '<th width="10%" class="tbl-header-row text-center">Attempts Given</th>'
					+ '<th width="10%" class="tbl-header-row text-center">Attempts Left</th>'
					+ '<th width="10%" class="tbl-header-row text-center">Results</th>'
					+ '<th width="10%" class="tbl-header-row"></th>'
					+ '</tr>'
					+ '</thead>'
					+ '<tbody>';
			tr = '';
			$.each(data.independentSubjectiveExams, function (i, exam) {
				subjectiveExams.push(exam);
				check=1;
				showdatetill='';
				showdateFrom='';
				showTill=1;
				showFrom=1;
				var today= $.now();

				check=2;
				showFrom=2;
				var tillDate = new Date(parseInt(exam.endDate));
				if(tillDate!='' || tillDate!=null)
				{
					var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
					if(tillDate.getHours() < 10)
						tsd += '0' + tillDate.getHours();
					else
						tsd += tillDate.getHours();
					tsd += ':';
					if(tillDate.getMinutes() < 10)
						tsd += '0' + tillDate.getMinutes();
					else
						tsd += tillDate.getMinutes();
				}
				showdateFrom=tsd;																																		
			
				var show='';

				var startdate = new Date(parseInt(exam.startDate));
				var display_chapter;
				var enddate, display_enddate;
				if (exam.endDate == null || exam.endDate == '') {
					display_enddate = '--';
				} else {
					enddate = new Date(parseInt(exam.endDate));
					display_enddate = enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + '<br/>' + enddate.getHours() + ':' + enddate.getMinutes();
				}
				if (exam.examgiven == 0) {
					disabled = 'disabled="disabled"';
				} else {
					disabled = '';
				}
				var attempsLeft = exam.attempts - exam.examgiven;
				disabledStartTest = '';
				if(attempsLeft<=0)
				{
					attempsLeft			=	0;
					disabledStartTest	= 'disabled="disabled"';
				}
				disabledStartTest	= 'disabled="disabled"';
				if(exam.paused == 0)
						buttonText = 'Start Test';
					else
						buttonText = 'Resume';
				tr += '<tr>'
						+ '<td>' + exam.exam + '</td>'
						+ '<td>' + exam.type + '</td>'
						+ '<td>' + startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + '</br>' + startdate.getHours() + ':' + startdate.getMinutes() + '</td>'
						+ '<td>' + display_enddate + '</td>'
						+ '<td class="text-center">' + exam.examgiven + '</td>'
						+ '<td class="text-center">' + (exam.attempts - exam.examgiven) + '</td>';
				tr+= '<td class="text-center"><a style="text-decoration: underline;"' + disabled + ' href="javascript:void(0)"  class="btn btn-md js-check-attempts" data-examid="' + exam.id + '">Show</a></td>';
				tr+= '<td class="text-center"><a '+disabledStartTest+'href="subjectiveExamModule.php?courseId=' + getUrlParameter('courseId') + '&examId=' + exam.id + '"  class="btn btn-success btn-sm start-exam" data-startdate="' + exam.startDate + '" data-endDate="' + exam.endDate + '" data-attempts=' + attempsLeft + ' disabled>' + buttonText + '</a></td>'
						+ '</tr>';
			});
			divSubIndHtml = divSubIndHtml + tr + '</tbody>'
					+ '</table></div>'
					+ '</div>';
		}

		if (data.independentManualExams.length > 0) {
			divManualHtml = '<div class="panel all-container">'
					+ '<div class="panel-heading">'
					+ '<div class="row">'
					+ '<div class="col-md-6"><h4><strong>Independent Manual Exams</strong></h4></div>'
					+ '<div class="col-md-4 text-right"> <a  class="btn btn-md btn-custom btn-success" id="viewManualIndependent">Hide Manual Exams</a></div><div class="col-md-2">'
					+ /*' <a class="btn btn-md btn-custom btn-success" id="hideSubjectiveIndependent">Cancel</a>'*/'</div>'
					+ '</div>'
					+ '</div>'
					+ '<div class="div-exams">'
					+ '<table class="table tbl_exams"> '
					+ '<thead>'
					+ '<tr>'
					+ '<th width="30%" class="tbl-header-row">Exam<br></th>'
					+ '<th width="40%" class="tbl-header-row">Type<br></th>'
					+ '<th width="30%" class="tbl-header-row text-center">Results</th>'
					+ '</tr>'
					+ '</thead>'
					+ '<tbody>';
			tr = '';
			$.each(data.independentManualExams, function (i, exam) {
				manualExams.push(exam);
				var today= $.now();

				var show='';

				tr += '<tr>'
						+ '<td>' + exam.name + '</td>'
						+ '<td>' + exam.type + ' Analytics</td>'
				tr+= '<td class="text-center"><a style="text-decoration: underline;" href="studentManualExamResult.php?courseId='+courseId+'&examId='+exam.id+'"  class="btn btn-md js-check-attempts" data-examid="' + exam.id + '">Show</a></td>' + '</tr>';
			});
			divManualHtml = divManualHtml + tr + '</tbody>'
					+ '</table></div>'
					+ '</div>';
		}
		$('#div_exam').append(divIntial + divHtml + divSubIndHtml + divManualHtml);
		//$('.div-exams,.div-subjective-exams').hide();
		var currentDate = data.currentDate;
		setInterval(function () {
			var todayTime = new Date(currentDate * 1000);
			$('.start-exam').each(function () {
				if ($(this).attr('data-attempts') != 0)
				{
					var startdate = new Date(parseInt($(this).attr('data-startdate')));
					if (startdate.valueOf() <= todayTime.valueOf())
					{
						$(this).attr('disabled', false);
					}
					var endDate = $(this).attr('data-endDate');
					if (endDate != '') {
						var examendDate = new Date(parseInt(endDate));
						if (examendDate.valueOf() <= todayTime.valueOf())
						{
							$(this).attr('disabled', 'disabled');
						}
				  }
				}
			});
			currentDate++;
		}, 1000);

		$('.viewExam').on('click', function () {
			if($(this).attr('data-hidden') == 0) {
				$(this).parents('.all-container').find('.div-exams').slideDown('slow');
				$(this).attr('data-hidden', 1);
				$(this).text('Hide Assignment/Exam');
			}
			else {
				$(this).parents('.all-container').find('.div-exams').slideUp('slow');
				$(this).attr('data-hidden', 0);
				$(this).text('View Assignment/Exam');
			}
		});
		$('.viewSubjectiveExam').on('click', function () {
			if($(this).attr('data-hidden') == 0) {
				$(this).parents('.all-container').find('.div-subjective-exams').slideDown('slow');
				$(this).attr('data-hidden', 1);
				$(this).text('Hide Subjective Exam');
			}
			else {
				$(this).parents('.all-container').find('.div-subjective-exams').slideUp('slow');
				$(this).attr('data-hidden', 0);
				$(this).text('View Subjective Exam');
			}
		});
		/*$('#hideIndependent').on('click', function () {
			$(this).parents('.all-container').find('.div-exams').slideUp('slow');
			$('#hideIndependent').hide();
			//$('#viewIndependent').css('margin-right', '25%');
		});*/
		$('#viewIndependent').on('click', function () {
			if($(this).attr('data-hidden') == 0) {
				$(this).parents('.all-container').find('.div-exams').slideDown('slow');
				$(this).attr('data-hidden', 1);
				$(this).text('Hide Assignment/Exam');
			} else {
				$(this).parents('.all-container').find('.div-exams').slideUp('slow');
				$(this).attr('data-hidden', 0);
				$(this).text('View Assignment/Exam');
			}
			//$('#hideIndependent').show();
			//$('#viewIndependent').css('margin-right', '2%');
		});
		/*$('#hideSubjectiveIndependent').on('click', function () {
			$(this).parents('.all-container').find('.div-exams').slideUp('slow');
			$('#hideSubjectiveIndependent').hide();
			//$('#viewSubjectiveIndependent').css('margin-right', '25%');
		});*/
		$('#viewSubjectiveIndependent').on('click', function () {
			if($(this).attr('data-hidden') == 0) {
				$(this).parents('.all-container').find('.div-exams').slideDown('slow');
				$(this).attr('data-hidden', 1);
				$(this).text('Hide Subjective Exams');
			} else {
				$(this).parents('.all-container').find('.div-exams').slideUp('slow');
				$(this).attr('data-hidden', 0);
				$(this).text('View Subjective Exams');
			}
			//$('#hideSubjectiveIndependent').show();
			//$('#viewSubjectiveIndependent').css('margin-right', '2%');
		});
		$('#viewManualIndependent').on('click', function () {
			if($(this).attr('data-hidden') == 0) {
				$(this).parents('.all-container').find('.div-exams').slideDown('slow');
				$(this).attr('data-hidden', 1);
				$(this).text('Hide Manual Exams');
			} else {
				$(this).parents('.all-container').find('.div-exams').slideUp('slow');
				$(this).attr('data-hidden', 0);
				$(this).text('View Manual Exams');
			}
			//$('#hideSubjectiveIndependent').show();
			//$('#viewSubjectiveIndependent').css('margin-right', '2%');
		});
	}
	$('#showExam').on('click', function () {
		$('#tbl_exams tbody tr').show('slow');

	});
	$('#showAssignment').on('click', function () {
		$('#tbl_assignment tbody tr').show('slow');

	});

	function fetchCourseDetails() {
		var req = {};
		var res;
		req.action = 'courseDetails-for-student';
		req.courseId = courseId;
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			if (res.status == 0)
				console.log(res.message);
			else {
				// details = res;
				fillCourseSubjectDetails(res);
			}
		});
	}
	function fillCourseSubjectDetails(data) {
		var tr = '';
		$('#course-name').text('Course : ' + data.course[0].course);
		$('#instiute-name').text(data.course[0].Institute);
		$.each(data.subjects, function (i, subject) {
			tr += '<td class="text-center"><img alt="" src="' + subject.image + '"><br/><a href="#"> <b>' + subject.name + '</b></a></td>';
		  });
		$('#tbl-subjects tbody').append(tr);
	}
	$('#div_exam').on("click", ".js-check-attempts", function(){
		var examId = $(this).attr("data-examid");
		var courseId = getUrlParameter('courseId');
		$.each(subjectiveExams, function (i, exam) {
			//console.log(exam);
			if (examId==exam.id) {
				if (exam.listAttempts.length>0) {
					$('#tblSubjectiveChecked').html('<tr><th>Attempts</th><th>Start Date</th><th>End Date</th><th>Checked</th></tr>');
					$.each(exam.listAttempts, function (j, attempt) {
						//console.log(attempt);
						$('#tblSubjectiveChecked').append(
							'<tr>'+
								'<td>Attempt '+(j+1)+'</td>'+
								'<td>'+formatTime(attempt.startDate)+'</td>'+
								'<td>'+formatTime(attempt.endDate)+'</td>'+
								'<td>'+((attempt.checked==1)?('<a href="showSubjectiveResult.php?courseId='+courseId+'&examId='+examId+'&attemptId='+attempt.attemptId+'">See Result</a>'):('Not Checked'))+'</td>'+
							'</tr>');
					});
					$('#subjectiveCheckedModal').modal("show");
				}
			};
		});
	});
});

/**
* function to fetch that user has submitted his review or not
*
* @author Rupesh Pandey
* @date 22/05/2015
*/
function fetchUserRating() {
	var req = {};
	var res;
	req.action = 'fetch-user-rating';
	req.subjectId = getUrlParameter('subjectId');
	req.courseId = getUrlParameter('courseId');
	$.ajax({
		type: "post",
		url: ApiEndPoint,
		data: JSON.stringify(req) 
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			alertMsg(res.message);
		else {
			rating = res;
			initRatingSystem(res);
		}
	});
}

/**
* function to fetch that user has submitted his review or not
*
* @author Rupesh Pandey
* @date 22/05/2015
* @param JSON with required details
*/
function initRatingSystem(data) {

	//rating initilisation
	$('#rating').rating({
		extendSymbol: function (rate) {
			title = '';
			switch(rate) {
				case 1:
					title = 'Very Bad';
					break;
				case 2:
					title = 'Bad';
					break;
				case 3:
					title = 'OK';
					break;
				case 4:
					title = 'Good';
					break;
				case 5:
					title = 'Very Good';
					break;
			}
			$(this).tooltip({
				placement: 'bottom',
				title: title
			});
		}
	});

	//event listener for submit review button
	$('#submitReview').on('click', function() {
		unsetError($('#titleReview'));
		if($('#titleReview').val() != '' && $('#titleReview').val().length > 200) {
			setError($('#titleReview'), 'Please input a smaller title');
			return;
		}
		unsetError($('#rating'));
		if($('#rating').val() <= 0) {
			setError($('#rating'), 'Please select a rate');
			return;
		}

		//now sending ajax to save rating
		var req = {};
		var res;
		req.action = 'save-rating';
		req.rating = $('#rating').val();
		req.title = $('#titleReview').val();
		req.review = $('#review').val();
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		req.reviewId = $('#reviewId').val();
		$.ajax({
			type: "post",
			url: ApiEndPoint,
			data: JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			$('#reviewModal').modal('hide');
			if(res.status == 1)
				location.reload();
			else
				alertMsg(res.message);
		})
	});

	//modal close event listener
	$('#reviewModal').on('hidden.bs.modal', function() {
		fillRating(rating);
	})

	//remove review event listener
	$('#removeReview').on('click', function() {
		if(confirm('Are you sure you want to remove your Rating and Review?')) {
			var req = {};
			var res;
			req.action = 'remove-review';
			req.reviewId = $('#reviewId').val();
			$.ajax({
				type: "post",
				url: ApiEndPoint,
				data: JSON.stringify(req)   
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alertMsg(res.message);
				else
					location.reload();
			});
		}
	});

	fillRating(data);
}

function fillRating(data) {
	if(data.rating == 0) {
		$('#reviewButton').text('Write Review').removeClass('hide');
		$('#rating').rating('rate', 0);
		$('#titleReview').val('');
		$('#review').val('');
		$('#reviewId').val(0);
		$('#removeReview').hide();
	}
	else {
		$('#reviewButton').text('Edit Review').removeClass('hide');
		$('#rating').rating('rate', data.rating.rating);
		$('#titleReview').val(data.rating.title);
		$('#review').val(data.rating.review);
		$('#reviewId').val(data.rating.id);
		$('#removeReview').show();
	}
}
//function to format time
function formatTime(time) {
	var a = new Date(time * 1000);
	var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var hour = a.getHours();
	var min = a.getMinutes();
	var sec = a.getSeconds();
	var formattedTime = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
	return formattedTime;
}