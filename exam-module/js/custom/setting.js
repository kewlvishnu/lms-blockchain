var ApiEndPoint = '../api/index.php';
var details;

$(function() {
	
	fetchUserDetails();
	
	$(".mobile, .landline, .landlinePrefix").keypress(function (e) {
		//if the letter is not digit then don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	});
	
	//function to fetch user details for setting page
	function fetchUserDetails() {
		var req = {};
		var res;
		req.action = 'get-student-details-for-setting';
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				details = res;
				fillUserDetails(res);
			}
		});
	}
	
	//function to fill user details
	function fillUserDetails(data) {
		$('.notify.badge').text(data.notiCount);
		$('#details .view-section .studentId').text(data.details.id);
		$('#details .view-section .firstName').text(data.details.firstName);
		$('#details .view-section .lastName').text(data.details.lastName);
		$('#details .edit-section .firstName').val(data.details.firstName);
		$('#details .edit-section .lastName').val(data.details.lastName);
		$('#details .view-section .email').text(data.details.email);
		if(data.details.dob == '')
			dobString = 'Not Set';
		else {
			dob = new Date(parseInt(data.details.dob));
        	var dobString = dob.getDate()+ ' ' +MONTH[dob.getMonth()]+ ' ' + dob.getFullYear();
			$('#details .edit-section .dob').val(dobString);
		}
		$('#details .view-section .dob').text(dobString);
		$('.fullName').text(data.details.firstName + ' ' + data.details.lastName);
		$('.username').text(data.details.username);
		$('.user-image').attr('src', data.details.profilePic);
		$('#details .view-section .mobile').text(data.details.contactMobilePrefix + '-' + data.details.contactMobile);
		$('#details .view-section .landline').text(data.details.contactLandlinePrefix + '-' + data.details.contactLandline);
		$('#details .edit-section .mobilePrefix').val(data.details.contactMobilePrefix);
		$('#details .edit-section .mobile').val(data.details.contactMobile);
		$('#details .edit-section .landlinePrefix').val(data.details.contactLandlinePrefix);
		$('#details .edit-section .landline').val(data.details.contactLandline);
		$('#details .view-section .country').text(data.country.countryName);
		$('#details .edit-section .country').val(data.country.countryId);
	}
	
	//event handler for edit button click
	$('.edit-button').on('click', function() {
		if($(this).find('i').hasClass('fa-edit')) {
			$(this).find('i').removeClass('fa-edit').addClass('fa-times');
			$(this).parents('section:eq(0)').find('.view-section').hide();
			$(this).parents('section:eq(0)').find('.edit-section').show();
		}
		else {
			$(this).find('i').removeClass('fa-times').addClass('fa-edit');
			$(this).parents('section:eq(0)').find('.edit-section').hide();
			$(this).parents('section:eq(0)').find('.view-section').show();
			fillUserDetails(details);
		}
	});
	
	//event handler for save button
	$('#details .save-button').on('click', function() {
		var req = {};
		var res;
		req.action = 'save-student-setting';
		req.firstName = $('#details .edit-section .firstName').val();
		req.lastName = $('#details .edit-section .lastName').val();
		dob = new Date($('#details .edit-section .dob').val());
		req.dob = dob.getTime();
		req.country = $('#details .edit-section .country').val();
		req.mobilePrefix = $('#details .edit-section .mobilePrefix').val();
		req.mobile = $('#details .edit-section .mobile').val();
		req.landlinePrefix = $('#details .edit-section .landlinePrefix').val();
		req.landline = $('#details .edit-section .landline').val();
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				details.details.firstName = req.firstName;
				details.details.lastName = req.lastName;
				details.details.dob = req.dob;
				details.details.contactMobilePrefix = req.mobilePrefix;
				details.details.contactMobile = req.mobile;
				details.details.contactLandlinePrefix = req.landlinePrefix;
				details.details.contactLandline = req.landline;
				details.country.countryId = req.country;
				details.country.countryName = $('option[value="'+req.country+'"]').text();
				$('#details .edit-section').hide();
				$('#details .view-section').show();
				$('#details .edit-button').find('i').removeClass('fa-times').addClass('fa-edit');
				fillUserDetails(details);
			}
		});
	});
	
	//event handler for cancel button
	$('#details .cancel-button').on('click', function() {
		$('#details .edit-section').hide();
		$('#details .view-section').show();
		$('#details .edit-button').find('i').removeClass('fa-times').addClass('fa-edit');
		fillUserDetails(details);
	});
	
	//adding datepicker for dob
	$('input.dob').datepicker({
		format : 'dd MM yyyy'
	});
});