var ApiEndPoint = '../api/index.php';
var MONTH = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
//variables to store exam details
var letters = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
var timer;
var totalTime = 0;
var totalExamTime = 0;
var examType = 0;
var endBeforeTime = 1;
var eachQuestionFlag = 0;
var gapTime = 0;
var gapTimer;
var sections = [];
var questions = [];
var answers = {};
var selectedQuestion = 0;
var questionTimer;
var attemptId;
var order = 0;
var resumeMode = 0;
var secondTimer = 0;
var autoSaveCounter = 60;
var timePauseFlag = false;
var showResult=1;
var showdatetill='';
var showdateFrom='';
var courseId=0;
var s2MinTimer;
var qtimer;
var lastQuestionId;
var lastQuestionIndex=0;
var lastSubQuestionIndex=0;
var submission;
var totalQuestions=0;
var totalMarks=0;
var totalAttempts=0;
/*questions.push({id:1,qno:1,questionType:'question',question:'Capital of Maharashtra?',upload:'',answer:'',subQuestions:{}});
questions.push({id:2,qno:2,questionType:'question',question:'Capital of Karnataka?',upload:'',answer:'',subQuestions:{}});
questions.push({id:3,qno:3,questionType:'questionGroup',question:'Rapid Fire on Eastern State Capitals',upload:'',answer:'',listType:'one',subQuestions:[{id:1,question:'Capital of Odisha?',answer:'',upload:''},{id:2,question:'Capital of Arunachal Pradesh?',answer:'',upload:''}]});
questions.push({id:4,questionType:'questionGroup',question:'Rapid Fire on Southern State Capitals',upload:'',answer:'',listType:'multi',subQuestions:[{id:1,qno:4,question:'Capital of Kerala?',answer:'',upload:''},{id:2,qno:5,question:'Capital of Tamil Nadu?',answer:'',upload:''}]});
console.log(JSON.stringify(questions));*/
$(function() {
	/*
     * this swallows backspace keys on any non-input element.
     * stops backspace -> back
     */
    /*var rx = /INPUT|SELECT|TEXTAREA/i;

    $(document).bind("keydown keypress", function(e){
        if( e.which == 8 ){ // 8 == backspace
            if(!rx.test(e.target.tagName) || e.target.disabled || e.target.readOnly ){
                e.preventDefault();
            }
        }
    });*/

	history.pushState(null, null, document.URL);
		window.addEventListener('popstate', function () {
		history.pushState(null, null, document.URL);
	});

	fetchResumeStatus();
	$('#btnStartExam').click(function(){
		$('#instructions').addClass('hide');
		$('#questions').removeClass('hide');
		startExam();
	});

	$('.questions-list').on('click', '.btn', function(){
		var thiz = $(this);
		var questionIndex = $(this).attr('data-question');
		var subQuestionIndex = $(this).attr('data-subquestion');
		if (questionIndex != "") {
			thiz.attr('disabled', true);
			$('#btnQuestionSave').trigger('click');
			setTimeout(function() {
				thiz.attr('disabled', false);
				displayQuestion(questionIndex,subQuestionIndex);
			}, 2000);
		};
	});

	$('#questions').on('click', '.btn-upload', function(){
		$(this).next().click();
	});
	$('#questions').on('change', '.js-uploader', function(){
		var file = $(this).val();
		if (!!file) {
			$(this).closest('form').submit();
		};
	});

	$('#tblQuestion').attr('data-question');

	$('.js-btn-qnav').click(function(){
		var thiz = $(this);
		var questionIndex = thiz.attr('data-question');
		var subQuestionIndex = thiz.attr('data-subquestion');
		var answerChanged = $('#btnQuestionSave').attr('changed');
		//alert(answerChanged);
		if (answerChanged>0) {
			thiz.attr('disabled', true);
			$('#btnQuestionSave').trigger('click');
			setTimeout(function() {
				thiz.attr('disabled', false);
				if (questionIndex != "") {
					displayQuestion(questionIndex,subQuestionIndex);
				};
			}, 2000);
		} else {
			if (questionIndex != "") {
				displayQuestion(questionIndex,subQuestionIndex);
			};
		}
	});
	$('#btnQuestionSave').click(function(){
		$('#btnQuestionSave').attr('changed', 0);

		var questionIndex = $(this).attr('data-question');
		if (questions[questionIndex].questionType == 'question') {
			//console.log($('.js-question [name="answer"]').val());
			//console.log(CKEDITOR.instances.answer.getData());
			//console.log(questions[questionIndex].answer);
			if (CKEDITOR.instances["answer"]) {
				if (CKEDITOR.instances.answer.getData() != questions[questionIndex].answer) {
					questions[questionIndex].answer = CKEDITOR.instances.answer.getData();
				}
			}
			questions[questionIndex].upload = [];
			$('.js-question .js-file-path li').each(function( index ) {
				questions[questionIndex]["upload"][index] = $(this).find('a:first-child').attr('href');
			});
			saveAnswer(questions[questionIndex]);
			var questionPalette = $('.questions-list .btn[data-question='+questionIndex+']');
		} else if (questions[questionIndex].listType == "one") {
			$('.js-questiongroup .sub-cover').each( function(k,obj) {
				var subQuestionIndex = $(obj).attr('data-subquestion');
				//console.log(CKEDITOR.instances["answer"+subQuestionIndex].getData());
				if (CKEDITOR.instances["answer"+subQuestionIndex]) {
					if (CKEDITOR.instances["answer"+subQuestionIndex].getData() != questions[questionIndex]["subQuestions"][subQuestionIndex]["answer"]) {
						questions[questionIndex]["subQuestions"][subQuestionIndex]["answer"] = CKEDITOR.instances["answer"+subQuestionIndex].getData();
					}
				}
				questions[questionIndex]["subQuestions"][subQuestionIndex]["upload"] = [];//$(obj).find('.js-file-path a').attr('href');
				$(obj).find('.js-file-path li').each(function( index ) {
					questions[questionIndex]["subQuestions"][subQuestionIndex]["upload"][index] = $(this).find('a:first-child').attr('href');
				});
			});
			saveAnswer(questions[questionIndex]);
			var questionPalette = $('.questions-list .btn[data-question='+questionIndex+']');
		} else {
			var subQuestionIndex = $(this).attr('data-subquestion');
			if (CKEDITOR.instances["answer"]) {
				if (CKEDITOR.instances.answer.getData() != questions[questionIndex]['subQuestions'][subQuestionIndex].answer) {
					questions[questionIndex]['subQuestions'][subQuestionIndex].answer = CKEDITOR.instances.answer.getData();
				}
			}
			questions[questionIndex]['subQuestions'][subQuestionIndex].upload = [];//$('.js-subQuestions .js-file-path a').attr('href');
			$('.js-subQuestions .js-file-path li').each(function( index ) {
				questions[questionIndex]['subQuestions'][subQuestionIndex]["upload"][index] = $(this).find('a:first-child').attr('href');
			});
			//saveAnswer(questions[questionIndex]['subQuestions'][subQuestionIndex]); edited by Jackie on 25/03/2017
			saveAnswer(questions[questionIndex]);
			var questionPalette = $('.questions-list .btn[data-question='+questionIndex+'][data-subquestion='+subQuestionIndex+']');
		}
		if (questionPalette.hasClass('btn-unanswered')) {
			questionPalette.removeClass('btn-unanswered').addClass('btn-answered');
		};
	});
	$('#questions').on('click', '.js-upload-remove', function(){
		var container = $(this).closest('li');
		var filepath  = container.find('a:first-child').attr('href');
		$('#btnQuestionSave').trigger('click');
		container.remove();
	});
	/*$('#questions').on('change', '.js-answer-field', function(){
		$('#btnQuestionSave').attr('changed', 1);
	});*/
	$('#btnSubmitExam').click(function(){
		var con = confirm("Are you sure you want to submit this exam?");
		if(con) {
			$('#timeUp .bigpad .lead').html('Exam ended');
			$('#mainExam').addClass('hide').next().removeClass('hide');
			submitExam();
		}
	});
});
function checkEditorChange() {
	for (var i in CKEDITOR.instances) {
        CKEDITOR.instances[i].on('change', function() {
        	$('#btnQuestionSave').attr('changed', 1);
        });
    }
}
function saveAnswer(question) {
	//console.log('saving...');
	autoSaveCounter = 60;
	var req = {};
	var res;
	req.action = 'update-subjective-Answer';
	req.subjectiveExamId = attemptId;
	req.examId = getUrlParameter('examId');
	req.questions = question;
	req.qtimer = qtimer;

	//console.log(question);
	stopQuestionTimer();
	$('#btnQuestionSave').attr('disabled', true);
	if ($('.answer-loader').length>0) {
		$('.answer-loader').html('<img src="img/ajax-loader.gif" alt="loader" />');
	} else {
		$('.answer-btns').after('<div class="answer-loader"><img src="img/ajax-loader.gif" alt="loader" /></div>');
	}
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		$('#btnQuestionSave').attr('disabled', false);
		res = $.parseJSON(res);
		$('.answer-loader').html('Last Saved at '+res.time);
		qtimer = {};
		qtimer = res.qtimer;
		startQuestionTimer(question.id);
		if(res.status == 0)
			console.log('failed');
		else
			console.log('success');
	});
}
//function to fetch resume status of the exam or assignment
function fetchResumeStatus() {
	var req = {};
	var res;
	req.action = 'get-subjective-exam-resume-status';
	req.examId = getUrlParameter('examId');
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		//console.log(res);
		if(res.status == 0)
			alert(res.message);
		else {
			if(res.data) {
				eachQuestionFlag = parseInt(res.data.eachQuestionFlag);
				attemptId = res.data.id;
				lastQuestionId = res.data.lastQuestionId;
			}
			resumeMode = res.resumeMode;
			fetchExamDetails();
		}
	});
}
//getting exam details to initiate view.
function fetchExamDetails() {
	var req = {};
	var res;
	req.action = 'get-subjective-exam-for-student';
	req.examId = getUrlParameter('examId');
	req.resumeMode = resumeMode;
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function (res) {
		res = $.parseJSON(res);
		//console.log(res);
		if(res.status == 0)
			alert(res.message);
		else {
			if(res.exam.type == "Exam")
				handlerExam(res);
		}
	});
}
//function to handle exams
function handlerExam(data) {
	//console.log(data);
	var today= $.now();
	increaseAttempt();
	
	//$('#startExam').show();
	examType = 0;
	$('#examName').text(data.exam.name);
	totalTime = parseInt(data.exam.totalTime);
	
	if(totalTime != 0)
		formatTime(totalTime);
	totalExamTime = formatTheTime(data.exam.totalTime);
	totalAttempts = data.exam.attempts;
	$('#timeText').attr('data-original-title', 'Time Left');
	
	if(resumeMode == 1) {
		$('#btnStartExam').text("Continue Subjective Exam");
	}
}
//function to increase number of attempt in database
function increaseAttempt() {
	var req = {};
	var res;
	req.action = "increase-subjectiveExam-attempt";
	req.examId = getUrlParameter('examId');
	req.resumeMode = resumeMode;
	req.attemptId = attemptId;
	req.eachQuestionFlag = eachQuestionFlag;
	//$('#loader').show();
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		//console.log(res);
		if(res.status == 0)
			alert(res.message);
		else {
			attemptId = res.attemptId;
			if(examType == 0)
				if(resumeMode == 0)
					fetchQuestions(0);
				else
					fetchQuestionsInResume(0);
		}
	});
}
//function to fetch questions and categories for exam
function fetchQuestions(sectionId) {
	var randomQuestions = false;
	//randomQuestions = true;
	var req = {};
	var res;
	req.action = 'get-questions-for-subjectiveExam';
	req.examId = getUrlParameter('examId');
	req.attemptId	= attemptId;
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		//console.log(res);
		if(res.status == 0)
			alert(res.message)
		else {
			submission	= res.submission;
			questions	= res.questions;
			qtimer		= res.qtimer;
			totalQuestions = res.totalQuestions;
			totalMarks	= res.totalMarks;
			fillQuestionsPallette();
			//startExam();
			preExam();
		}
	});
}

//function to fetch questions in resume mode
function fetchQuestionsInResume(sectionId) {
	var req = {};
	var res;
	req.action = 'get-questions-for-subjectiveExam-in-resume';
	req.examId = getUrlParameter('examId');
	req.resumeMode	= 1;
	req.attemptId	= attemptId;
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		//console.log(res);
		if(res.status == 0)
			alert(res.message)
		else {
			submission	= res.submission;
			questions = res.questions;
			qtimer	  = res.qtimer;
			answers	  = res.alreadyAnswered;
			totalQuestions = res.totalQuestions;
			totalMarks	= res.totalMarks;
			getLastQuestionIndex();
			fillQuestionsPallette();
			fillAnswers();
			fetchAndSetTime();
		}
	});
}
//function to fetch and set time
function fetchAndSetTime() {
	var req = {};
	var res;
	req.action = 'get-subjective-time';
	req.attemptId = attemptId;
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			alert(res.message);
		else {
			totalTime = Math.floor(parseInt(res.time)/60);
			secondTimer = res.time - (totalTime * 60);
			totalTime--;
			//startExam();
			preExam();
		}
	});
}
function fillAnswers() {
	for (var i = 0; i < answers.length; i++) {
		for (var j = 0; j < questions.length; j++) {
			if (answers[i].questionId == questions[j].id) {
				if (!!answers[i].answer) {
					questions[j].answer = answers[i].answer;
				};
				if (answers[i].upload) {
					questions[j].upload = answers[i].upload;
				}
				var questionPalette = $('.questions-list .btn[data-question='+j+']');
				questionPalette.removeClass('btn-unvisited').removeClass('btn-unanswered').addClass('btn-answered');
			};
			/*if (lastQuestionId == questions[j].id) {
				lastQuestionIndex = j;
			};*/
			if (questions[j]["subQuestions"].length>0) {
				for (var k = 0; k < questions[j]["subQuestions"].length; k++) {
					if (answers[i].questionId == questions[j]["subQuestions"][k].id) {
						if (!!answers[i].answer) {
							questions[j]["subQuestions"][k].answer = answers[i].answer;
						};
						if (answers[i].upload) {
							questions[j]["subQuestions"][k].upload = answers[i].upload;
						}
						if (questions[j].listType == "one") {
							var questionPalette = $('.questions-list .btn[data-question='+j+']');
							questionPalette.removeClass('btn-unvisited').removeClass('btn-unanswered').addClass('btn-answered');
						} else {
							var questionPalette = $('.questions-list .btn[data-question='+j+'][data-subquestion='+k+']');
							questionPalette.removeClass('btn-unvisited').removeClass('btn-unanswered').addClass('btn-answered');
						}
					};
					/*if (lastQuestionId == questions[j]["subQuestions"][k].id) {
						lastSubQuestionIndex = k;
					};*/
				};
			};
		};
	};
}
function getLastQuestionIndex() {
	//console.log(questions);
	for (var j = 0; j < questions.length; j++) {
		if (lastQuestionId == questions[j].id) {
			lastQuestionIndex = j;
		};
		if (questions[j]["subQuestions"].length>0) {
			for (var k = 0; k < questions[j]["subQuestions"].length; k++) {
				if (lastQuestionId == questions[j]["subQuestions"][k].id) {
					lastQuestionIndex = j;
					lastSubQuestionIndex = k;
				};
			};
		};
	};
}
function preExam() {
	$('#totalQuestions').html(totalQuestions+" Questions");
	$('#totalMarks').html(totalMarks+" Marks");
	$('#totalExamTime').html(totalExamTime);
	$('#totalAttempts').html(totalAttempts);
	$('#loader').addClass('hide');
	$('#btnStartExam').attr('disabled', false);
}
//function to start exam
function startExam() {
	//$('#loader').hide();
	//window.onbeforeunload = function() { return "You work will be lost."; };
	displayQuestion(lastQuestionIndex,lastSubQuestionIndex);
	examTimerStart();
	$('#timeNow').parents('.js-timer:eq(0)').show();
}

//function to start exam time counter
function examTimerStart() {
	formatTime(totalTime);
	//function to send answers to server in every two min if time has to be paused in exam
	s2MinTimer = setInterval(function() {
		if(autoSaveCounter < 0) {
			autoSaveCounter = 120;
			//send the current question to server so it can be saved
			$('#btnQuestionSave').trigger('click');
		}
		autoSaveCounter--;
	}, 1000);
	//decreasing the value of time per second
	timer = setInterval(function() {
		secondTimer--;
		if(secondTimer < 0) {
			totalTime--;
			formatTime(totalTime);
			secondTimer = 59;
		}
		if(totalTime < 0) {
			clearInterval(timer);
			//alert('Time Up');
			$('#mainExam').addClass('hide').next().removeClass('hide');
			submitExam();
		}
		if(secondTimer < 10)
			$('#timeSecond').text('0' + secondTimer);
		else
			$('#timeSecond').text(secondTimer);
	},	1000);
}
function fillQuestionsPallette() {
	for (var i = 0; i < questions.length; i++) {
		if (questions[i].questionType == 'question' || questions[i].listType == 'one') {
			$('.questions-list').append('<li><button class="btn btn-default btn-unvisited" data-question="'+i+'">'+questions[i].qno+'</button></li>');
		} else {
			for (var j = 0; j < questions[i]['subQuestions'].length; j++) {
				$('.questions-list').append('<li><button class="btn btn-default btn-unvisited" data-question="'+i+'" data-subquestion="'+j+'">'+questions[i]['subQuestions'][j].qno+'</button></li>');
			}
		}
	};
}
function displayQuestion(questionIndex,subQuestionIndex) {
	//alert(questionIndex);
	stopQuestionTimer();
	$('#btnQuestionSave').attr('data-question',questionIndex);
	$('#btnQuestionSave').attr('changed', 0);
	$('.questions-list .btn').removeClass('active');
	var question = questions[questionIndex];
	$('.js-question').find('.js-form-con').html('');
	$('.js-questiongroup td').html('');
	$('.js-subQuestions').find('.js-form-con').html('');
	if (question.questionType == 'question') {
		startQuestionTimer(question.id);
		$('#jsQNo').html('Question No '+question.qno+' [Marks: '+question.marks+']');
		$('#txtQuestion').html(question.question);
		$('.js-qtype').addClass('hide');
		$('.js-question').removeClass('hide');
		if (submission == 1) {
			$('.js-question').find('.js-form-con').html(
								'<div class="form-group">'+
									'<label for="">Answer :</label>'+
									'<textarea name="answer" id="answer" cols="30" rows="10" class="form-control js-answer-field"></textarea>'+
								'</div>');
			ckeditorOn('answer');
			CKEDITOR.instances["answer"].setData(question.answer);
			checkEditorChange();
			//$('.js-question [name="answer"]').val(question.answer);
		} else if (submission == 2) {
			$('.js-question').find('.js-form-con').html(
								'<div class="form-group">'+
									'<label for="">Answer :</label>'+
								'</div>'+
								'<form class="frm-upload" action="../api/files1.php" method="post">'+
									'<button type="button" class="btn btn-warning btn-upload">Upload Answer (PDF/JPG/PNG)</button>'+
									'<input type="file" name="subjective-upload" class="js-uploader hide">'+
									'<ul class="list-inline list-files js-file-path"></ul>'+
								'</form>');
			if (question["upload"].length>0) {
				for (var i = 0; i < question["upload"].length; i++) {
					$('.js-question .js-file-path').append('<li><a href="'+question["upload"][i]+'" target="_blank">Click here to check the upload</a><a href="javascript:void(0)" class="js-upload-remove"><i class="fa fa-times"></i></a></li>');
				};
			};
		} else {
			$('.js-question').find('.js-form-con').html(
								'<div class="form-group">'+
									'<label for="">Answer :</label>'+
									'<textarea name="answer" id="answer" cols="30" rows="10" class="form-control js-answer-field"></textarea>'+
								'</div>'+
								'<div class="form-group">OR</div>'+
								'<form class="frm-upload" action="../api/files1.php" method="post">'+
									'<button type="button" class="btn btn-warning btn-upload">Upload Answer (PDF/JPG/PNG)</button>'+
									'<input type="file" name="subjective-upload" class="js-uploader hide">'+
									'<ul class="list-inline list-files js-file-path"></ul>'+
								'</form>');
			ckeditorOn('answer');
			CKEDITOR.instances["answer"].setData(question.answer);
			checkEditorChange();
			//$('.js-question [name="answer"]').val(question.answer);
			if (question["upload"].length>0) {
				for (var i = 0; i < question["upload"].length; i++) {
					$('.js-question .js-file-path').append('<li><a href="'+question["upload"][i]+'" target="_blank">Click here to check the upload</a><a href="javascript:void(0)" class="js-upload-remove"><i class="fa fa-times"></i></a></li>');
				};
			};
		}
		var questionPalette = $('.questions-list .btn[data-question='+questionIndex+']');
		questionPalette.addClass('active');
		if (questionPalette.hasClass('btn-unvisited')) {
			questionPalette.removeClass('btn-unvisited').addClass('btn-unanswered');
		};
		if (questionPalette.hasClass('btn-unanswered') && (!!question.answer || question["upload"].length>0)) {
			questionPalette.removeClass('btn-unanswered').addClass('btn-answered');
		};
	} else if (question.listType == "one") {
		startQuestionTimer(question.id);
		$('#jsQNo').html('Question No '+question.qno+' [Marks: '+question.marks+']');
		$('#txtQuestion').html(question.question);
		$('.js-qtype').addClass('hide');
		if (question.subQuestions.length>0) {
			var subQuestions = question.subQuestions;
			$('.js-questiongroup td').html('<div class="qgroup-cover"></div>');
			for (var i = 0; i < subQuestions.length; i++) {
				$('.js-questiongroup .qgroup-cover').append(
							'<div class="sub-cover" data-subquestion="'+i+'">'+
								'<div class="sub-question">'+
									'<p>'+subQuestions[i].question+'</p>'+
								'</div>'+
								'<div class="sub-answer js-form-con"></div>'+
							'</div>');
				if (submission == 1) {
					$('.js-questiongroup .qgroup-cover .sub-cover:last-child').find('.js-form-con').html(
										'<div class="form-group">'+
											'<label for="">Answer :</label>'+
											'<textarea name="answer" id="answer'+i+'" cols="30" rows="5" class="form-control js-answer-field"></textarea>'+
										'</div>');
					ckeditorOn('answer'+i);
					CKEDITOR.instances["answer"+i].setData(subQuestions[i].answer);
					checkEditorChange();
					//$('.js-questiongroup .sub-cover:last-child [name="answer"]').val(subQuestions[i].answer);
				} else if (submission == 2) {
					$('.js-questiongroup .qgroup-cover .sub-cover:last-child').find('.js-form-con').html(
										'<div class="form-group">'+
											'<label for="">Answer :</label>'+
										'</div>'+
										'<form class="frm-upload" action="../api/files1.php" method="post">'+
											'<button type="button" class="btn btn-warning btn-upload">Upload Answer (PDF/JPG/PNG)</button>'+
											'<input type="file" name="subjective-upload" class="js-uploader hide">'+
											'<ul class="list-inline list-files js-file-path"></ul>'+
										'</form>');
					if (subQuestions[i]["upload"].length>0) {
						for (var j = 0; j < subQuestions[i]["upload"].length; j++) {
							$('.js-questiongroup .sub-cover:last-child .js-file-path').append('<li><a href="'+subQuestions[i]["upload"][j]+'" target="_blank">Click here to check the upload</a><a href="javascript:void(0)" class="js-upload-remove"><i class="fa fa-times"></i></a></li>');
						};
					};
				} else {
					$('.js-questiongroup .qgroup-cover .sub-cover:last-child').find('.js-form-con').html(
										'<div class="form-group">'+
											'<label for="">Answer :</label>'+
											'<textarea name="answer" id="answer'+i+'" cols="30" rows="5" class="form-control js-answer-field"></textarea>'+
										'</div>'+
										'<div class="form-group">OR</div>'+
										'<form class="frm-upload" action="../api/files1.php" method="post">'+
											'<button type="button" class="btn btn-warning btn-upload">Upload Answer (PDF/JPG/PNG)</button>'+
											'<input type="file" name="subjective-upload" class="js-uploader hide">'+
											'<ul class="list-inline list-files js-file-path"></ul>'+
										'</form>');
					ckeditorOn('answer'+i);
					CKEDITOR.instances["answer"+i].setData(subQuestions[i].answer);
					checkEditorChange();
					//$('.js-questiongroup .sub-cover:last-child [name="answer"]').val(subQuestions[i].answer);
					if (subQuestions[i]["upload"].length>0) {
						for (var j = 0; j < subQuestions[i]["upload"].length; j++) {
							$('.js-questiongroup .sub-cover:last-child .js-file-path').append('<li><a href="'+subQuestions[i]["upload"][j]+'" target="_blank">Click here to check the upload</a><a href="javascript:void(0)" class="js-upload-remove"><i class="fa fa-times"></i></a></li>');
						};
					};
				}
			};
		};
		$('.js-questiongroup').removeClass('hide');
		var questionPalette = $('.questions-list .btn[data-question='+questionIndex+']');
		questionPalette.addClass('active');
		if (questionPalette.hasClass('btn-unvisited')) {
			questionPalette.removeClass('btn-unvisited').addClass('btn-unanswered');
		};
		if (questionPalette.hasClass('btn-unanswered') && (!!question.answer || question["upload"].length>0)) {
			questionPalette.removeClass('btn-unanswered').addClass('btn-answered');
		};
	} else {
		var subquestion = questions[questionIndex]["subQuestions"][subQuestionIndex];
		startQuestionTimer(subquestion.id);
		$('#btnQuestionSave').attr('data-subquestion',subQuestionIndex);
		$('#jsQNo').html('Question No '+subquestion.qno+' [Marks: '+subquestion.marks+']');
		$('#txtQuestion').html(subquestion.question);
		$('.js-qtype').addClass('hide');
		$('.js-subQuestions').removeClass('hide');
		if (submission == 1) {
			$('.js-subQuestions').find('.js-form-con').html(
								'<div class="form-group">'+
									'<label for="">Answer :</label>'+
									'<textarea name="answer" id="answer" cols="30" rows="10" class="form-control js-answer-field"></textarea>'+
								'</div>');
			ckeditorOn('answer');
			CKEDITOR.instances["answer"].setData(subquestion.answer);
			checkEditorChange();
			//$('.js-subQuestions [name="answer"]').val(subquestion.answer);
		} else if (submission == 2) {
			$('.js-subQuestions').find('.js-form-con').html(
								'<div class="form-group">'+
									'<label for="">Answer :</label>'+
								'</div>'+
								'<form class="frm-upload" action="../api/files1.php" method="post">'+
									'<button type="button" class="btn btn-warning btn-upload">Upload Answer (PDF/JPG/PNG)</button>'+
									'<input type="file" name="subjective-upload" class="js-uploader hide">'+
									'<ul class="list-inline list-files js-file-path"></ul>'+
								'</form>');
			if (subquestion["upload"].length>0) {
				for (var i = 0; i < subquestion["upload"].length; i++) {
					$('.js-subQuestions .js-file-path').append('<li><a href="'+subquestion["upload"][i]+'" target="_blank">Click here to check the upload</a><a href="javascript:void(0)" class="js-upload-remove"><i class="fa fa-times"></i></a></li>');
				};
			};
		} else {
			$('.js-subQuestions').find('.js-form-con').html(
								'<div class="form-group">'+
									'<label for="">Answer :</label>'+
									'<textarea name="answer" id="answer" cols="30" rows="10" class="form-control js-answer-field"></textarea>'+
								'</div>'+
								'<div class="form-group">OR</div>'+
								'<form class="frm-upload" action="../api/files1.php" method="post">'+
									'<button type="button" class="btn btn-warning btn-upload">Upload Answer (PDF/JPG/PNG)</button>'+
									'<input type="file" name="subjective-upload" class="js-uploader hide">'+
									'<ul class="list-inline list-files js-file-path"></ul>'+
								'</form>');
			ckeditorOn('answer');
			CKEDITOR.instances["answer"].setData(subquestion.answer);
			checkEditorChange();
			//$('.js-subQuestions [name="answer"]').val(subquestion.answer);
			if (subquestion["upload"].length>0) {
				for (var i = 0; i < subquestion["upload"].length; i++) {
					$('.js-subQuestions .js-file-path').append('<li><a href="'+subquestion["upload"][i]+'" target="_blank">Click here to check the upload</a><a href="javascript:void(0)" class="js-upload-remove"><i class="fa fa-times"></i></a></li>');
				};
			};
		}
		var questionPalette = $('.questions-list .btn[data-question='+questionIndex+'][data-subquestion='+subQuestionIndex+']');
		questionPalette.addClass('active');
		if (questionPalette.hasClass('btn-unvisited')) {
			questionPalette.removeClass('btn-unvisited').addClass('btn-unanswered');
		};
		if (questionPalette.hasClass('btn-unanswered') && (!!subquestion.answer || subquestion["upload"].length>0)) {
			questionPalette.removeClass('btn-unanswered').addClass('btn-answered');
		};
	}
	if (((question.questionType == 'question' || question.listType == "one")
			|| (question.questionType == 'questionGroup' && question.listType == "multi" && subQuestionIndex == 0))
			&& (questionIndex == 0)) {
		$('#btnQuestionNavPrev').addClass('disabled');
		$('#btnQuestionNavPrev').attr('data-question','');
		$('#btnQuestionNavPrev').attr('data-subquestion','');
	} else {
		$('#btnQuestionNavPrev').removeClass('disabled');
		$('#btnQuestionNavPrev').attr('data-subquestion','');
		$('#btnQuestionNavPrev').attr('data-question',(parseInt(questionIndex)-1));
		var questionPrev = questions[questionIndex-1];
		if ((question.questionType == 'question' || question.listType == "one") || (question.questionType == 'questionGroup' && question.listType == "multi" && subQuestionIndex == 0)) {
			//alert(questions[questionIndex-1]["subQuestions"].length);
			if (questionPrev.questionType == 'questionGroup' && questionPrev.listType == "multi") {
				if(questions[questionIndex-1]["subQuestions"].length) {
					$('#btnQuestionNavPrev').attr('data-subquestion',(parseInt(questions[questionIndex-1]["subQuestions"].length)-1));
				}
			}
		} else {
			if (question.questionType == 'questionGroup' && question.listType == "multi") {
				if(questions[questionIndex]["subQuestions"].length) {
					$('#btnQuestionNavPrev').attr('data-question',(parseInt(questionIndex)));
					$('#btnQuestionNavPrev').attr('data-subquestion',(parseInt(subQuestionIndex)-1));
				}
			}
		}
	}
	//alert(questionIndex + ' ' + questions.length);
	if (((question.questionType == 'question' || question.listType == "one")
			|| (question.questionType == 'questionGroup' && question.listType == "multi" && subQuestionIndex == parseInt(questions[questionIndex]["subQuestions"].length)-1))
			&& (questionIndex == (questions.length-1))) {
		$('#btnQuestionNavNext').addClass('disabled');
		$('#btnQuestionNavNext').attr('data-question','');
		$('#btnQuestionNavNext').attr('data-subquestion','');
	} else {
		$('#btnQuestionNavNext').removeClass('disabled');
		$('#btnQuestionNavNext').attr('data-subquestion','');
		$('#btnQuestionNavNext').attr('data-question',(parseInt(questionIndex)+1));
		var questionNext = questions[parseInt(questionIndex)+1];
		
		if ((question.questionType == 'question' || question.listType == "one") || (question.questionType == 'questionGroup' && question.listType == "multi" && subQuestionIndex == parseInt(questions[questionIndex]["subQuestions"].length)-1)) {
			//alert(JSON.stringify(questions[questionIndex]));
			if (questionNext.questionType == 'questionGroup' && questionNext.listType == "multi") {
				if(questionNext["subQuestions"].length) {
					$('#btnQuestionNavNext').attr('data-subquestion',0);
				}
			}
		} else {
			if (question.questionType == 'questionGroup' && question.listType == "multi") {
				if(questions[questionIndex]["subQuestions"].length) {
					$('#btnQuestionNavNext').attr('data-question',(parseInt(questionIndex)));
					$('#btnQuestionNavNext').attr('data-subquestion',(parseInt(subQuestionIndex)+1));
				}
			}
		}
	}
}
//function to start timer for each question
function startQuestionTimer(needle) {
	questionTimer = setInterval(function() {

		qtimer[needle] = parseInt(qtimer[needle]) + 1;
		//console.log(needle);

	},	1000);
}

//function to stop timer for each question
function stopQuestionTimer() {
	clearInterval(questionTimer);
}

//function to submit exam
function submitExam() {
	clearInterval(timer);
	clearInterval(questionTimer);
	clearInterval(s2MinTimer);
	autoSaveCounter = 60;

	$('#btnQuestionSave').trigger('click');
	setTimeout(function() {
		var req = {};
		var res;
		req.action = 'save-subjective-exam';
		req.examId = getUrlParameter('examId');
		req.attemptId = attemptId;
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			//console.log(res);
			if (res.status == 1) {
				$('#timeUp .bigpad').html('<p>Submitted sucessfully. Result will be shown later.</p><p><a href="../student/" class="btn btn-warning">Goto Dashboard</a></p>');
			} else {
				$('#timeUp .bigpad').html('<p>There was a problem, please contact instructor.</p>');
			}
		});
	}, 2000);
}
// prepare the form when the DOM is ready 
$(document).ready(function() { 
 
    // bind to the form's submit event 
	$('#questions').on('submit', '.frm-upload', function(){
    	var thiz = $(this);
    	$('.btn-upload').attr('disabled', true);
    	$('.btn-upload').after('<span class="upload-loader"><img src="img/ajax-loader.gif" alt="loader" /></span>');
	    var options = { 
	        //target:        '#output2',   // target element(s) to be updated with server response 
	        data: { examId: getUrlParameter('examId') },
	        dataType: 'json',
	        beforeSubmit:  showRequest,  // pre-submit callback 
	        success: function(msg) {
	        	$('.js-uploader').val('');
	        	$('.upload-loader').remove();
    			$('.btn-upload').attr('disabled', false);
	        	if (msg.status == 0) {
	        		alert(msg.message);
	        	} else {
	        		thiz.closest('.js-form-con').find('.js-file-path').append('<li><a href="'+msg.fileName+'" target="_blank">Click here to check the upload</a><a href="javascript:void(0)" class="js-upload-remove"><i class="fa fa-times"></i></a></li>');
					$('#btnQuestionSave').trigger('click');
	        	}
	        }  // post-submit callback 
	 
	    }; 
        // inside event callbacks 'this' is the DOM element so we first 
        // wrap it in a jQuery object and then invoke ajaxSubmit 
        $(this).ajaxSubmit(options); 
 
        // !!! Important !!! 
        // always return false to prevent standard browser submit and page navigation 
        return false;
    }); 
}); 
 
// pre-submit callback 
function showRequest(formData, jqForm, options) { 
    // formData is an array; here we use $.param to convert it to a string to display it 
    // but the form plugin does this for you automatically when it submits the data 
    var queryString = $.param(formData); 
 
    // jqForm is a jQuery object encapsulating the form element.  To access the 
    // DOM element for the form do this: 
    // var formElement = jqForm[0]; 
 
    //alert('About to submit: \n\n' + queryString); 
 
    // here we could return false to prevent the form from being submitted; 
    // returning anything other than false will allow the form submit to continue 
    return true; 
}
//function to format time
function formatTheTime (time) {
	if(time < 0)
                return;
	var hours = 0;
	var minutes = 0;
	if(time >= 60) {
                hours = Math.floor( time / 60 );
                minutes = time - ( hours * 60 )
	}
	else
		minutes = time;
	var formatHour = (hours > 9) ? hours + '' : '0' + hours;
	var formatMinute = (minutes > 9) ? minutes + '' : '0' + minutes;
	return formatHour+' hours '+formatMinute+' min';
}
function formatTime(time) {
	if(time < 0)
                return;
	var hours = 0;
	var minutes = 0;
	if(time >= 60) {
                hours = Math.floor( time / 60 );
                minutes = time - ( hours * 60 )
	}
	else
		minutes = time;
	var formatHour = (hours > 9) ? hours + '' : '0' + hours;
	var formatMinute = (minutes > 9) ? minutes + '' : '0' + minutes;
	$('#timeHour').text(formatHour);
	$('#timeMinute').text(formatMinute);
	$('#timeSecond').text('00');
}
//function to fetch data from url
function getUrlParameter(sParam) {
	sParam = sParam.toLowerCase();
	var sPageURL = window.location.search.substring(1).toLowerCase();
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) 
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) 
		{
			return sParameterName[1];
		}
	}
}
function ckeditorOn(element) {
	CKEDITOR.inline( element, {
					toolbar: [
							{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ]},
							'/',
							{ name: 'insert', items: [ 'Image', 'Mathjax' ]},
							{ name: 'styles', items: [ 'FontSize' ] },
							{ name: 'colors', items: [ 'TextColor' ] }],
					right: 0,
					filebrowserBrowseUrl: '../api/browse.php',
					filebrowserUploadUrl: '../api/uploader.php',
					filebrowserWindowWidth: '640',
					filebrowserWindowHeight: '480',
					extraPlugins: 'confighelper'
				});
	//for inline ckeditor
	//CKEDITOR.instances["question"].setData('<div contenteditable="true">asxkgsydfb</div>');
	CKEDITOR.instances[element].on("instanceReady", function() {

		//set keypress event
		/*CKEDITOR.instances[element].on('key', function(e) {
			if(e.data.keyCode == 8 || e.data.keyCode == 46)
				return true;
		});*/

		//set keypress event
		this.document.on("keypress", function(e) {
			if(e.data.keyCode == 8 || e.data.keyCode == 46) {
				return true;
			}
		});

		//set keyup event
		this.document.on("keyup", function() {
			$("#" + element).val(CKEDITOR.instances[element].getData());
		});
		
		//and paste event
		this.document.on("paste", function() {
			$("#" + element).val(CKEDITOR.instances[element].getData());
		});
		CKEDITOR.instances[element].on('blur', function(e) {
			$("#" + element).val(CKEDITOR.instances[element].getData());
			$("#" + element).blur();
			if($("#question").val() == '') {
				if($('#hiddenQuestionType').val() == 4 || $('#hiddenQuestionType').val() == 5) {
					$('div[aria-describedby="cke_43"]').html('Please enter the passage here. A Passage could be a few paragraphs or a problem statement.');
				}
			}
		});
	});
}