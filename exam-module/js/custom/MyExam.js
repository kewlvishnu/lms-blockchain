var ApiEndPoint = '../api/index.php';
var details;
var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var courseId = getUrlParameter('courseId');
var check=1;
var showdatetill='';
var showdateFrom='';
var showTill=1;
var showFrom=1;
var subjectiveExams;

$(function () {

	fetchUserDetails();
	fetchCourseDetails();
	getStudentExams();
	getStudentSubjectiveExams();
	getStudentManualExams();
	
	//function to fetch user details for change password page
	function fetchUserDetails() {
		var req = {};
		var res;
		req.action = 'get-student-details-for-changePassword';
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			if (res.status == 0)
				alert(res.message);
			else {
				details = res;
				fillUserDetails(res);
			}
		});
	}

	//function to fill user details
	function fillUserDetails(data) {
		$('.notify.badge').text(data.notiCount);
		$('.fullName').text(data.details.firstName + ' ' + data.details.lastName);
		$('.username').text(data.details.username);
		$('.user-image').attr('src', data.details.profilePic);
		$('#btn_rupee, #btn_dollar').removeClass('btn-white btn-info');
		if (data.details.currency == 1) {
			$('#btn_dollar').addClass('btn-info');
			$('#btn_rupee').addClass('btn-white');
		}
		else if (data.details.currency == 2) {
			$('#btn_dollar').addClass('btn-white');
			$('#btn_rupee').addClass('btn-info');
		}
	}
	function getStudentExams() {
		var req = {};
		var res;
		req.action = 'student-exam-attempts';
		req.courseId = courseId;
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			if (res.status == 0){
				//console.log(res.message);
				$('#tbl_exams').hide();
				$('.tbl_header_exam').hide();
				$('#tbl_assignment').hide();
				$('.tbl_header_ass').hide();
				$('#tbl_assignment').after('<h4> No Assignment / Exam Found </h4>');
			}
			else {
				fillStudentExams(res);
			}
		});
	}
	function fillStudentExams(data) {
		var examHtml = '', AssignmentHtml = '';
		var examRow = '', AssignmentRow = '';
		var examRowafterClick = '';
		var examAssignmentRowClick = '';
		var countexamrow = 0;
		var countrowAssignment = 0;
		var disabled = '', startnow = '';
		var startButton = 'disabled=disabled';
		var examMore=false;
		var assignmentMore=false;
		var examCount=0;
		var AssignmentCount=0;
		
		$.each(data.exams, function (i, exam) {
			var startdate = new Date(parseInt(exam.startDate));
			var today= $.now();
			//alert(startdate);
			// var today = new Date();
			if(exam.showResult=='immediately' || exam.endDate=='')
			{
				check=1;
			}
			else{
				if(today>exam.endDate)
				{
					check=1;
				}
				else{
					check=2;
					showFrom=2;
					var tillDate = new Date(parseInt(exam.endDate));
					if(tillDate!='' || tillDate!=null)
					{
						var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
						if(tillDate.getHours() < 10)
							tsd += '0' + tillDate.getHours();
						else
							tsd += tillDate.getHours();
						tsd += ':';
						if(tillDate.getMinutes() < 10)
							tsd += '0' + tillDate.getMinutes();
						else
							tsd += tillDate.getMinutes();
					}
					showdateFrom=tsd;
				}
			}
			if((exam.showResultTill == 2 && today< exam.showResultTillDate) || exam.showResultTill == 1)
			{
				check=1;
			}
			else{
				check=2;
				showTill=2;
				showdatetill=exam.showResultTillDate;
				var tillDate = new Date(parseInt(showdatetill));
				if(tillDate!='' || tillDate!=null)
				{
					var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
					if(tillDate.getHours() < 10)
						tsd += '0' + tillDate.getHours();
					else
						tsd += tillDate.getHours();
					tsd += ':';
					if(tillDate.getMinutes() < 10)
						tsd += '0' + tillDate.getMinutes();
					else
						tsd += tillDate.getMinutes();
				}
				showdatetill=tsd;
			}

			var show='';

			var diaplay_chapter;
			var enddate, display_enddate;

			if (exam.endDate == null || exam.endDate == '') {
				display_enddate = '--';
			} else {
				enddate = new Date(parseInt(exam.endDate));
				display_enddate = enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + '<br/>' + enddate.getHours() + ':' + enddate.getMinutes();
			}
			if (exam.chapter == null) {
				diaplay_chapter = '';
			} else {
				diaplay_chapter = ' &gt; ' + exam.chapter;
			}
			var attempsLeft = exam.attempts - exam.examgiven;
			disabledStartTest = '';
			if(attempsLeft<=0)
			{
				attempsLeft			=	0;
				disabledStartTest	= 'disabled="disabled"';
			}
			disabledStartTest	= 'disabled="disabled"';
			if (exam.examgiven == 0) {
				disabled = 'disabled="disabled"';
			} else {
				disabled = '';
			}
			if(exam.paused == 0)
				buttonText = 'Start Test';
			else
				buttonText = 'Resume';
			var tr = '<tr ><td class="border-none" width="30%"><h5><strong>' + exam.exam + ' </strong><br/><span style="font-size:12px;margin-top:5px;">' + exam.subject+ diaplay_chapter + '</span></h5></td>'
					+ '<td class="border-none">' + startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + '<br/>' + startdate.getHours() + ':' + startdate.getMinutes() + '</td>'
					+ '<td class="border-none">' + display_enddate + '</td>'
					+ '<td class="text-center border-none">' + exam.examgiven + '</td>'
					+ '<td class="text-center border-none">' + attempsLeft + '</td>';
			if(check == 1)
			{
				tr+= '<td class="text-center border-none"><a style="text-decoration: underline;" ' + disabled + ' href="studentResult.php?examId=' + exam.id + '"  class="btn btn-md myHover">Show</a></td>';
			}
			else{
				if(showTill==2 && showFrom==2)
				{	
					show= 'Result will be shown only from'+showdateFrom+ 'to' +showdatetill;
					tr+= '<td class="text-center border-none">'+show+'</td>';
					
				}
				//("Submitted sucessfully.Result will be shown only from "+''+showdateFrom+'To '+showdatetill);
				else if(showTill==2 && showFrom==1)
				{
					show= 'Result was shown till '+showdatetill;
					tr+= '<td class="text-center border-none">'+show+'</td>';
				}
				//alert("Submitted sucessfully.Result will be shown till "+' '+showdatetill);
				else
				{
					show= 'Result will be shown after '+showdateFrom;
					tr+= '<td class="text-center border-none">'+show+'</td>';
			
				}
			//alert("Submitted sucessfully.Result will be shown after "+' '+showdateFrom);
			}
			tr+= '<td class="text-center border-none"><a '+disabledStartTest+'href="examModule.php?examId=' + exam.id + '"  class="btn btn-success btn-sm start-exam" data-startdate="' + exam.startDate + '" data-endDate="' + exam.endDate + '" data-attempts=' + attempsLeft + ' disabled>' + buttonText + '</a></td>'
			
			+ '</tr>';
			if (exam.type == 'Exam') {
				examRow += tr;
				examCount++;
			} else {
				AssignmentRow += tr;
				AssignmentCount++;
			}
		});
		if(AssignmentRow==''){
			$('#tbl_assignment').hide();
			$('.tbl_header_ass').text('No Active Assignment Now');
		}
		if(examRow==''){
			$('#tbl_exams').hide();
			$('.tbl_header_exam').text('No Active Exams Now');
		}

		$('#tbl_exams tbody').html(examRow);
		$('#tbl_exams tbody tr').hide();
		$('#tbl_exams tbody tr:eq(0)').show();
		$('#tbl_exams tbody tr:eq(1)').show();
		$('#tbl_exams tbody tr:eq(2)').show();

		$('#tbl_assignment tbody').html(AssignmentRow);
		$('#tbl_assignment tbody tr').hide();
		$('#tbl_assignment tbody tr:eq(0)').show();
		$('#tbl_assignment tbody tr:eq(1)').show();
		$('#tbl_assignment tbody tr:eq(2)').show();
		if( examCount>3){
			$('#showExam').show();
		}
		if( AssignmentRow>3){
			$('#showAssignment').show();
		}
		var currentDate = data.currentDate;
		setInterval(function(){
			var todayTime = new Date(currentDate * 1000);
			$('.start-exam').each(function () {
				if ($(this).attr('data-attempts') > 0)
				{	
					var startdate = new Date(parseInt($(this).attr('data-startdate')));
					if (startdate.valueOf() <= todayTime.valueOf() )
					{	
						$(this).attr('disabled', false);
					}
					var endDate = $(this).attr('data-endDate');
					if (endDate != '') {
						var examendDate = new Date(parseInt(endDate));
						if (examendDate.valueOf() <= todayTime.valueOf())
						{
							$(this).attr('disabled', 'disabled');
						}
					}
				}
				
				
				
			});
			currentDate++;
		}, 1000);
		
	}
	$('#showExam').on('click', function () {
		if($(this).attr('show')=='More'){
			$('#tbl_exams tbody tr').show('slow');
			$(this).text('Less..');
			$(this).attr('show','Less');
		}else{
			$('#tbl_exams tbody tr').hide();
			$('#tbl_exams tbody tr:eq(0)').show();
			$('#tbl_exams tbody tr:eq(1)').show();
			$('#tbl_exams tbody tr:eq(2)').show();
			$(this).attr('show','More');
			$(this).text('More..');
		}
	});
	$('#showAssignment').on('click', function () {
		if($(this).attr('show')=='More'){
			$('#tbl_assignment tbody tr').show('slow');
			$(this).text('Less..');
			$(this).attr('show','Less');
		}else{
			$('#tbl_assignment tbody tr').hide();
			$('#tbl_assignment tbody tr:eq(0)').show();
			$('#tbl_assignment tbody tr:eq(1)').show();
			$('#tbl_assignment tbody tr:eq(2)').show();
			$(this).attr('show', 'More');
			$(this).text('More..');
		}
	});


	//for subjective Exams
	function getStudentSubjectiveExams() {
		var req = {};
		var res;
		req.action = 'student-exam-subjective-attempts';
		req.courseId = courseId;
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			//console.log(res);
			if (res.status == 0){
				//console.log(res.message);
				$('#tbl_subjective_exams').hide();
				$('.tbl_header_subjective_exam').hide();
				//$('#tbl_assignment').hide();
				//$('.tbl_header_ass').hide();
				$('#tbl_subjective_exams').after('<h4> No Subjective Exam Found </h4>');
			}
			else {
				fillStudentSubjectiveExams(res);
				subjectiveExams = res.exams;
			}
		});
	}
	
	function fillStudentSubjectiveExams(data) {
		//console.log(data);
		var examHtml = '', AssignmentHtml = '';
		var examRow = '', AssignmentRow = '';
		var examRowafterClick = '';
		var examAssignmentRowClick = '';
		var countexamrow = 0;
		var countrowAssignment = 0;
		var disabled = '', startnow = '';
		var startButton = 'disabled=disabled';
		var examMore=false;
		var assignmentMore=false;
		var examCount=0;
		var AssignmentCount=0;
		$.each(data.exams, function (i, exam) {
			var startdate = new Date(parseInt(exam.startDate));
			var today= $.now();
			//alert(startdate);
			// var today = new Date();
			showFrom=2;
			var endDate	 = parseInt(exam.endDate);
			var tillDate = new Date(endDate);
			if (today>endDate) {
				var tr = '';
			} else {
				//console.log(today);
				//console.log(parseInt(exam.endDate));
				if(tillDate!='' || tillDate!=null)
				{
					var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
					if(tillDate.getHours() < 10)
						tsd += '0' + tillDate.getHours();
					else
						tsd += tillDate.getHours();
					tsd += ':';
					if(tillDate.getMinutes() < 10)
						tsd += '0' + tillDate.getMinutes();
					else
						tsd += tillDate.getMinutes();
				}
				showdateFrom=tsd;																		
				
				var show='';		
				
				var diaplay_chapter;
				var enddate, display_enddate;

				if (exam.endDate == null || exam.endDate == '') {
					display_enddate = '--';
				} else {
					enddate = new Date(parseInt(exam.endDate));
					display_enddate = enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + '<br/>' + enddate.getHours() + ':' + enddate.getMinutes();
				}
				if (exam.chapter == null) {
					diaplay_chapter = '';
				} else {
					diaplay_chapter = ' &gt; ' + exam.chapter;
				}
				var attempsLeft = exam.attempts - exam.examgiven;
				disabledStartTest = '';
				if(attempsLeft<=0)
				{
					attempsLeft			=	0;
					disabledStartTest	= 'disabled="disabled"';
				}
				disabledStartTest	= 'disabled="disabled"';
				if (exam.examgiven == 0) {
					disabled = 'disabled="disabled"';
				} else {
					disabled = '';
				}
				if(exam.paused == 0)
					buttonText = 'Start Test';
				else
					buttonText = 'Resume';
				var tr = '<tr ><td class="border-none" width="30%"><h5><strong>' + exam.exam + ' </strong><br/><span style="font-size:12px;margin-top:5px;">' + exam.subject+ diaplay_chapter + '</span></h5></td>'
						+ '<td class="border-none">' + startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + '<br/>' + startdate.getHours() + ':' + startdate.getMinutes() + '</td>'
						+ '<td class="border-none">' + display_enddate + '</td>'
						+ '<td class="text-center border-none">' + exam.examgiven + '</td>'
						+ '<td class="text-center border-none">' + attempsLeft + '</td>';
					tr+= '<td class="text-center border-none"><a style="text-decoration: underline;"' + disabled + ' href="javascript:void(0)"  class="btn btn-md js-check-attempts" data-examid="' + exam.id + '">Show</a></td>';
					tr+= '<td class="text-center border-none"><a '+disabledStartTest+'href="subjectiveExamModule.php?courseId=' + getUrlParameter('courseId') + '&examId=' + exam.id + '"  class="btn btn-success btn-sm start-exam" data-startdate="' + exam.startDate + '" data-endDate="' + exam.endDate + '" data-attempts=' + attempsLeft + ' disabled>' + buttonText + '</a></td>'
						
						+ '</tr>';
			}
			if (exam.type == 'Exam') {
				examRow += tr;
				examCount++;
			}
		});
		//console.log(examRow);
		if(examRow==''){
			$('#tbl_subjective_exams').hide();
			$('.tbl_header_subjective_exam').text('No Active Subjective Exams Now');
		}

		$('#tbl_subjective_exams tbody').html(examRow);
		$('#tbl_subjective_exams tbody tr').hide();
		$('#tbl_subjective_exams tbody tr:eq(0)').show();
		$('#tbl_subjective_exams tbody tr:eq(1)').show();
		$('#tbl_subjective_exams tbody tr:eq(2)').show();

		if( examCount>3){
			$('#showSubjectiveExam').show();
		}
		var currentDate = data.currentDate;
		setInterval(function(){
			var todayTime = new Date(currentDate * 1000);
			$('.start-exam').each(function () {
				if ($(this).attr('data-attempts') > 0)
				{	
					var startdate = new Date(parseInt($(this).attr('data-startdate')));
					if (startdate.valueOf() <= todayTime.valueOf() )
					{	
						$(this).attr('disabled', false);
					}
					var endDate = $(this).attr('data-endDate');
					if (endDate != '') {
						var examendDate = new Date(parseInt(endDate));
						if (examendDate.valueOf() <= todayTime.valueOf())
						{
							$(this).attr('disabled', 'disabled');
						}
					}
				}
			});
			currentDate++;
		}, 1000);
		
	}
	
	$('#showSubjectiveExam').click(function(){
		//console.log('clicked');
		$('#tbl_subjective_exams tr').each(function(index){
			$(this).css('display', 'table-row');
		});
	});
	
	//for subjective Exams
	function getStudentManualExams() {
		var req = {};
		var res;
		req.action = 'student-exam-manual-exams';
		req.courseId = courseId;
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			//console.log(res);
			if (res.status == 0){
				//console.log(res.message);
				$('#tbl_manual_exams').hide();
				$('.tbl_header_manual_exam').hide();
				$('#tbl_manual_exams').after('<h4> No Manual Exam Found </h4>');
			}
			else {
				fillStudentManualExams(res);
			}
		});
	}
	
	function fillStudentManualExams(data) {
		//console.log(data);
		var examHtml = '';
		var examCount = 0;
		$.each(data.exams, function (i, exam) {
			//console.log(exam);
			examHtml+= '<tr>'+
							'<td width="30%" class="border-none">'+exam.name+'</td>'+
							'<td class="border-none">'+exam.type+' Analytics</td>'+
							'<td class="text-center border-none"><a style="text-decoration: underline;" href="studentManualExamResult.php?courseId='+courseId+'&examId='+exam.id+'" class="btn btn-md myHover">Show</a></td>'+
						'</tr>';
			examCount++;
		});
		if(examCount==0){
			$('#tbl_manual_exams').hide();
			$('.tbl_header_manual_exam').text('No Active Manual Exams Now');
		} else {
			$('#tbl_manual_exams').append(examHtml);
		}
	}
	
	function fetchCourseDetails() {
		var req = {};
		var res;
		req.action = 'courseDetails-for-student';
		req.courseId = courseId;
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			if (res.status == 0)
				console.log(res.message);
			else {
				// details = res;
				fillCourseSubjectDetails(res);
			}
		});
	}
	function fillCourseSubjectDetails(data) {
		//console.log(data);
		var html = '';
		var courseDoc= data.coursesDoc;
		if(courseDoc !='' && courseDoc != undefined)
		{	var urlAbsolute='assets/ViewerJS/#'+courseDoc;
			$('#viewCourseDoc #viewerCourseDocument').attr("src", urlAbsolute);
			$('#viewcoursedoc1').show();
		}
		
		if(data.certificationStatus ==1 && data.certification!=null)
		{
			certificate=data.certification;
			$('#certification-download').attr("href", data.certification);
			$('#certification-download').show();
			$('#coursecertification').html('Congratulations, You have Sucessfully completed the course');
		}
		$('#course-name').text(data.course[0].course);
		$('#instiute-name').text(data.course[0].Institute);
		$.each(data.subjects, function (i, subject) {
			html += '<div class="text-center"> <a href="chapter.php?courseId='+courseId+'&subjectId='+subject.id+'"><img  alt="" src="' + subject.image + '" style="width: 150px;margin: auto;"><br/><strong> <i style="font-size:16px;color:orange;" class=" fa fa-angle-right" ></i><br/>' + subject.name + '</strong></a></div>';
		});
		$('#tbl-subjects').append(html);
		$('#tbl-subjects').slick({
			infinite: false,
			slidesToShow: 4,
			slidesToScroll: 4
		});
		$('ul.breadcrumb li:eq(1)').html('<strong>' + data.course[0].course + '</strong>');
	}

	$('#tbl_subjective_exams').on("click", ".js-check-attempts", function(){
		var examId = $(this).attr("data-examid");
		var courseId = getUrlParameter('courseId');
		$.each(subjectiveExams, function (i, exam) {
			if (examId==exam.id) {
				if (exam.listAttempts.length>0) {
					$('#tblSubjectiveChecked').html('<tr><th>Attempts</th><th>Start Date</th><th>End Date</th><th>Checked</th></tr>');
					$.each(exam.listAttempts, function (j, attempt) {
						//console.log(attempt);
						$('#tblSubjectiveChecked').append(
							'<tr>'+
								'<td>Attempt '+(j+1)+'</td>'+
								'<td>'+formatTime(attempt.startDate)+'</td>'+
								'<td>'+formatTime(attempt.endDate)+'</td>'+
								'<td>'+((attempt.checked==1)?('<a href="showSubjectiveResult.php?courseId='+courseId+'&examId='+examId+'&attemptId='+attempt.attemptId+'">See Result</a>'):('Not Checked'))+'</td>'+
							'</tr>');
					});
					$('#subjectiveCheckedModal').modal("show");
				}
			};
		});
	});
	//function to format time
	function formatTime(time) {
		var a = new Date(time * 1000);
		var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		var year = a.getFullYear();
		var month = months[a.getMonth()];
		var date = a.getDate();
		var hour = a.getHours();
		var min = a.getMinutes();
		var sec = a.getSeconds();
		var formattedTime = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
		return formattedTime;
	}
});