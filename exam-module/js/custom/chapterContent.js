var ApiEndPoint = '../api/index.php';
var chapterId = getUrlParameter('chapterId');
var courseId=getUrlParameter('courseId');
var subjectId=getUrlParameter('subjectId');
var contentIds = [];
var StuffType=0;
var presentContentId=0;
var idleCheckTimer;
var idleCheckTime=0;
var idleTimer;
var idleTime=0;
var startTime=0;
var videoDuration=0;
var videoPlaying=0;

$(function() {

	checkCourseAcess();

	function checkCourseAcess() {
		var req = {};
		var res;
		req.action = 'checkCourseAcess';
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			//console.log(res);
			if (res.status == 0)
				alert('Not Allowed to view this chapter');
			else {
				fetchUserDetails();
				fetchBreadCrumb();
				fetchHeadings();
			}
		});
	}

	//function to fetch headings of the chapter
	function fetchHeadings() {
		var req = {};
		var res;
		req.action = 'get-headings';
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		req.demo = getUrlParameter('demo');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status == 0)
				alert(res.message);
			else
				fillHeadings(res);
		});
	}

	//function to fill headings and generate side view
	function fillHeadings(data) {
		var html = '';
		var chapterNum = parseInt(data.chapterNum);
		for(var i = 0; i < data.headings.length; i++) {
			html += '<div class="panel panel-default custom-panel">'
					+ '<div class="panel-heading">'
						+ '<h3 class="custom-panel-title">'
							//+ '<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse' + i + '" data-heading="' + data.headings[i].id + '"><strong>Heading ' + (chapterNum + 1) + '.' + (i+1) + ':</strong> ' + data.headings[i].title+'</a>'
							+ '<span data-heading="' + data.headings[i].id + '"><strong>Section ' + (i+1) + ':</strong> ' + data.headings[i].title+'</span>'
						+ '</h3>'
					+ '</div>'
					+ '<div id="collapse'+i+'" class="panel-collapse collapse in" style="height: auto;">'
						+ '<div class="custom-panel-body">'
							+ '<div class="timeline-messages">';
			//contentIds.push('0');
			for(var j = 0; j < data.headings[i].content.length; j++) {
				contentIds.push(data.headings[i].content[j].id);
					//html += '<a data-id="' + data.headings[i].content[j].id + '" class="content-view"><div class="content-container"  style="background-color:#2ecc71;">';
					html += '<a data-id="' + data.headings[i].content[j].id + '" class="content-view"><div class="content-container">';

				if(data.headings[i].content[j].contentType == "doc")
					html += '<img class="avatar" src="img/pdf.jpg" alt="">';
				else if(data.headings[i].content[j].contentType == "ppt")
					html += '<img class="avatar" src="img/pdf.png" alt="">';
				else if(data.headings[i].content[j].contentType == "text")
					html += '<img class="avatar" src="img/lec.png" alt="">';
				else if(data.headings[i].content[j].contentType == "video")
					html += '<img class="avatar" src="img/vid.jpg" alt="">';
				else if(data.headings[i].content[j].contentType == "dwn")
					html += '<img class="avatar" src="img/download.png" alt="">';
				else if(data.headings[i].content[j].contentType == "Youtube")
					html += '<img class="avatar" src="img/youtube.png" alt="">';
				else if(data.headings[i].content[j].contentType == "Vimeo")
					html += '<img class="avatar" src="img/vimeo.png" alt="">';
					html += '<div class="content-text"><span class="init">Lecture ' + (i+1) + '.' + (j+1) + ' </span>: <span class="cont">' + data.headings[i].content[j].title + '</span>';
					
				if(data.headings[i].content[j].downloadable == 1)
					html += '<br><a data-contentId="'+data.headings[i].content[j].id+'" href="#" class="contentDownload btn btn-success btn-xs btn-custom">Download</a>';
				/*else
					html += '</p>';*/
				html +=		'</div>'
						+ '</div></a>';
			}
			html +=		'</div>'
					+ '</div>'
				+ '</div>'
			+ '</div>';
		}
		$('#accordion').append(html);
		
		//event listener for content views
		$('.content-view').on('click', function() {
			//console.log($(this).find('.init').text());
			var dataID 	 = $(this).attr('data-id');
			var dataElem = $("#sidebar").find('[data-id='+dataID+']');
			//console.log($("#sidebar").find('[data-id='+dataID+']').text());
			$('#loaderContent').show();
			$('#content-view .custom-content-part').html("").removeClass("pad-clr");
			// Text type content
			if(StuffType == 1) {
				update_Timer();
			}
			// Downloadable type content
			else if(StuffType == 2) {
				update_Timer();
			}
			// Youtube type content
			else if(StuffType == 3 && videoPlaying==1) {
				update_Timer();
			}
			// Vimeo type content
			else if(StuffType == 4 && videoPlaying==1) {
				update_Timer();
			}
			// Documents type content (PDF)
			else if(StuffType == 5) {
				update_Timer();
			}
			// Video type content (Flowplayer)
			else if(StuffType == 6 && videoPlaying==1) {
				update_Timer();
			} else {
				$('#loaderContent').hide();
			}
			$('#contentHeading .init').html(dataElem.find('.init').text());
			$('#contentHeading .cont').html(dataElem.find('.cont').text());
			$('#contentHeading').attr('data-id', dataID);
			var req = {};
			var res;
			req.action = 'get-content-stuff';
			req.contentId = dataID;
			$.ajax({
					'type'	:	'post',
					'url'	:	ApiEndPoint,
					'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				//console.log(res);
				if(res.status == 0)
					alert(res.message);
				else
					fillContentDetails(res);
			});
		});
		//clicking on chapters content
		if($('[data-heading="' + getUrlParameter('chapterId') + '"]').parents('div.panel').find('.content-view').length != 0) 
			$('[data-heading="' + getUrlParameter('chapterId') + '"]').parents('div.panel').find('.content-view:eq(0)').click();
		else
			$('.content-view:eq(0)').click();
		$('.contentDownload').on('click', function() {
			var contentId = $(this).attr('data-contentId');
			//send a request to server to fetch the link and generate signature also check if the file is really downloadable.
			var req = {};
			var res;
			req.action = 'download-content';
			req.contentId = contentId;
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				window.open(res.link.contentLink, '_blank');
				//var html = '<iframe src="downloader.php?filename='+res.link.contentLink+'" heigth="0" width="0" style="display: none;visibility: hidden; border: 0px;"></iframe>';
				//$('body').append(html);
			});
		});
	}

	//function to fill content details
	function fillContentDetails(data) {
		//console.log(data);
		var i=0;
		$('.description').hide();
		//filling next button
		$('#next').show();
		if(contentIds[contentIds.indexOf($('#contentHeading').attr('data-id'))+1] != undefined) {
			var index = $('#contentHeading').attr('data-id');
			$('#next').attr('data-id', contentIds[contentIds.indexOf(index)+1]);
		}
		else
			$('#next').hide();
			
		//filling previous button
		$('#previous').show();
		if(contentIds[contentIds.indexOf($('#contentHeading').attr('data-id'))-1] != undefined)
			$('#previous').attr('data-id', contentIds[contentIds.indexOf($('#contentHeading').attr('data-id'))-1]);
		else
			$('#previous').hide();
		if(data.content.description != '') {
			//if description is more than 226 characters hide the remaining
			if(data.content.description.length > 226) {
				var first = data.content.description.substring(0, 226);
				var second = '<span id="moreDesc" style="display: none;">' + data.content.description.substring(226, data.content.description.length) + '</span>';
				var html = first + second + '<a data-hidden="0" href="#" id="viewMoreDesc">... View More</a>';
				$('#desc').html(html).parents('.description').show();
				$('#viewMoreDesc').off('click');
				$('#viewMoreDesc').on('click', function(e) {
					e.preventDefault();
					if($(this).attr('data-hidden') == 0) {
						$(this).text(' Hide');
						$(this).attr('data-hidden', "1")
						$('#moreDesc').slideDown();
					}
					else {
						$(this).text('... View More');
						$(this).attr('data-hidden', "0")
						$('#moreDesc').slideUp();
					}
				});
			}
			else
				$('#desc').html(data.content.description).parents('.description').show();
		}
		else
			$('#desc').html('No description found.');
		if(data.supplementary.length > 0) {
			var html = '';
			for(i = 0; i < data.supplementary.length; i++) {
				html += '<a href="' + data.supplementary[i].signedURL + '" target="_blank"><img src="../admin/img/att.png" style="width: 32px;height: 32px;"><br>' + data.supplementary[i].fileRealName + '</a><br>';
			}
			$('#supp').html(html).parents('.description').show();
		}
		else
			$('#supp').html('No supplementary materials found.');
		$('#block-everything').hide();

		contentType = data.content.type;
		
		idleTime = 0;
		idleCheckTime = 0;
		videoPlaying = 0;
		$('#activityModal').modal("hide");
		stopIdleCheckTimer();
		stopIdleTimer();
		startIdleCheckTimer();

		if(data.content.type == "text") {
			StuffType		= 1;
			presentContentId= (data.content.contentId);
			startTime = $.now();

			var html = '<div id="ckeditor">' + data.content.stuff + '</div>';
			$('#content-view .custom-content-part').html(html);
			$('#block-everything').show();
			CKEDITOR.inline('ckeditor', {toolbar: [
								{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ]},
								'/',
								{ name: 'insert', items: [ 'Image', 'Mathjax' ]},
								{ name: 'styles', items: [ 'FontSize' ] },
								{ name: 'colors', items: [ 'TextColor' ] }]
							});

			var contentCheck=document.getElementById("ckeditor");
			if(contentCheck!=null || contentCheck!='' )
			{
				update_Timer();
			}
		}
		else if(data.content.type == "dwn") {
			StuffType=2;
			presentContentId = (data.content.contentId);
			startTime = $.now();
			update_Timer();
			var html = '<div class="download-pad"><button class="btn btn-lg btn-primary">Download</button></div>';
			$('#content-view .custom-content-part').html(html).addClass("pad-clr");
			//window.open(data.content.stuff, '_blank');
			$('.custom-content-part').on("click", ".download-pad .btn", function(){
				window.open(data.content.stuff, '_blank');
			});
		}
		else if(data.content.type == "Youtube") {
			//console.log('youtube');
			StuffType		 = 3;
			presentContentId = (data.content.contentId);

			//console.log(percent_done);
			var videoId = getStringParameter('v', data.content.stuff);
			/*var html 	= '<iframe id="ik_player_iframe" frameborder="0"  src="https://www.youtube.com/embed/' +
							videoId +'?enablejsapi=1&version=3" width="100%" height="420"></iframe>';*/
			var html = '<div id="ik_player"></div>';
			$('#content-view .custom-content-part').html(html).addClass("pad-clr");		
			/*var tag = document.createElement('script');
			tag.src = "https://www.youtube.com/player_api";
			var firstScriptTag = document.getElementsByTagName('script')[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);*/
			// create youtube player
			var player;
			function onYouTubePlayerAPIReady() {
				player = new YT.Player('ik_player', {
					height: '420',
					width: '100%',
					videoId: videoId,
					events: {
						'onReady': onPlayerReady,
						'onStateChange': onPlayerStateChange
					}
				});
			}

			// autoplay video
			function onPlayerReady(event) {
			    //event.target.playVideo();
			}

			// when video ends
			function onPlayerStateChange(event) {
				videoDuration =(event.target.getDuration());     
			    if(event.data === 0) {
			    	videoPlaying = 0;        
			        callPause();
			    }
			    if (event.data == 1) {
			    	videoPlaying = 1;
					callPlay();
			    }
				else if (event.data == 2) {
			    	videoPlaying = 0;
					callPause();
				}
			}
			function callPlay () {
				startTime = $.now();
				//update_Timer();
			}
			function callPause () {
				//startTime = $.now();
				update_Timer();
			}
			onYouTubePlayerAPIReady();
		}
		else if(data.content.type == "Vimeo") {
			StuffType=4;
			presentContentId=(data.content.contentId);
			var videoId = getVimeoId(data.content.stuff);
				
			var html= '<iframe id="player1" src="https://player.vimeo.com/video/'+videoId+'?api=1&player_id=player1" id="player1" width="100%" height="420" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
			$('#content-view .custom-content-part').html(html).addClass("pad-clr");
			
			var iframe = $('#player1')[0];
			var player = $f(iframe);

			// When the player is ready, add listeners for pause, finish, and playProgress
			player.addEvent('ready', function() {
				//console.log('ready');
				player.addEvent('play', onPlay);
				player.addEvent('pause', onPause);
				player.addEvent('finish', onFinish);
				//player.addEvent('playProgress', onPlayProgress);
			});

			// Call the API when a button is pressed
			$('.custom-content-part button').bind('click', function() {
				//console.log($(this));
				player.api($(this).text().toLowerCase());
			});

			function onPause() {
			    videoPlaying = 0;
				update_Timer();
			}

			function onFinish() {
			    videoPlaying = 0;
				update_Timer();
			}

			function onPlay(data) {
			    videoPlaying = 1;
				//console.log(data);
				videoDuration =(data.duration);
				startTime = $.now();
			}
		}
		else if(data.content.type == "ppt" || data.content.type == "doc" || data.content.type == "pdf" ) {
			StuffType			=	5;

			presentContentId	=	(data.content.contentId);

			var html='<iframe id="viewer" src = "assets/ViewerJS/#'+data.content.stuff+'" width="100%" height="420"  allowfullscreen webkitallowfullscreen ></iframe>';

			$('#content-view .custom-content-part').html(html);

			$("#viewer").on("load", function () {
				update_Timer();
			});
		}
		else if(data.content.type == "video") {
			//console.log('video');
			//http://d2wqzjmvp96657.cloudfront.net/user-data/files/content-2270.mp4
			StuffType=6;
			presentContentId=(data.content.contentId);

			var html = '<video id="videoPlayer" width="100%" height="420"></video>';
			$('#content-view .custom-content-part').html(html).addClass("pad-clr");

			var src = data.content.stuff;
			player = new MediaElementPlayer('#videoPlayer', {
				type: 'video/mp4',
				videoWidth:"100%",
				videoHeight: "420px",
				success: function (mediaElement, domObject) {
					mediaElement.addEventListener('pause', function () {
						//console.log("paused");
						videoPlaying = 0;
						update_Timer();
					}, false);
					mediaElement.addEventListener('play', function () {
						//console.log("play");
						videoPlaying = 1;
						videoDuration = mediaElement.duration;
						startTime = $.now();
					}, false);
					mediaElement.addEventListener('ended', function () {
						//console.log("ended");
						videoPlaying = 0;
						update_Timer();
					}, false);
				}
			});
			var sources = [
				{ src: src, type: 'video/mp4' }
			];

			player.setSrc(sources);
			player.load();
			//player.play();
			/*var html = '<div class="flowplayer color-light no-background" id="player" style="width: 100%;height: 400px;">'+
						'<video>'+
							'<source type="video/mp4" src="'+data.content.stuff+'">'+
						'</video>'+
					'</div>';*/
			/*var html = '<div class="flowplayer color-light no-background" id="player" style="width: 100%;height: 400px;"></div>';
			$('#content-view .custom-content-part').html(html);
			if (isMobile) {
  				var videoAutoPlay = true;
			} else {
				var videoAutoPlay = false;
			}
			flowplayer("#player", {
  				autoplay: videoAutoPlay,
				clip: {
					sources: [
						{
							type: "video/mp4",
							src:  data.content.stuff
						}
					]
				}
			});

			$('.flowplayer').flowplayer().on('pause', function(){
			    videoPlaying = 0;
				update_Timer();
			});
			$('.flowplayer').flowplayer().on('resume', function(){
			    videoPlaying = 1;
				//console.log('pauseStart'+pauseStart);
				videoDuration = flowplayer($("#player")).video.duration;
				startTime = $.now();
			});
			$('.flowplayer').flowplayer().on('finish', function() {
			    videoPlaying = 0;
				update_Timer();
			});*/
		}
	}

	function update_Timer() {
		var endTime=$.now();
		//console.log(endTime+'|'+startTime);
		if (endTime>startTime) {
			var	reqContentId=presentContentId;
			var req = {};
			var res;
			req.stuffType=StuffType;
			req.contentId=presentContentId;
			req.chapterId=chapterId;
			req.subjectId=subjectId;
			req.startTime=startTime;
			req.endTime  =endTime;
			req.idleTime =idleTime;
			req.videoDuration=videoDuration;
			req.action = 'set-content-watched';
			$.ajax({
				'type': 'post',
				'url': ApiEndPoint,
				'data': JSON.stringify(req)
			}).done(function (res) {
				$('#loaderContent').hide();
				//console.log(res);
				res = $.parseJSON(res);
				//console.log(res);
				if(res.status ==1  )
				{	
					$('[data-id="'+reqContentId+'"]').children('.content-container').css('background-color',"#2ecc71")
				} else {
					alert(res.message);
				}
			});
		};
	}

	function fetchUserDetails() {
		var req = {};
		var res;
		req.action = 'get-student-details-for-changePassword';
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			//console.log(res);
			if (res.status == 0)
				alert(res.message);
			else {
				details = res;
				fillUserDetails(res);
			}
		});
	}

	//function to fetch and fill breadcrumbs
	function fetchBreadCrumb() {
		var req = {};
		var res;
		req.action = 'get-breadcrumb-for-chapter';
		req.subjectId = getUrlParameter('subjectId');
		req.courseId = getUrlParameter('courseId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				$('#breadcrumb .btc:eq(0)').html('<a href="MyExam.php?courseId='+courseId+'" class="custom-btc-link">'+res.breadcrumb.courseName+'</a>');
				$('#breadcrumb .btc:eq(1)').html('<a href="chapter.php?courseId='+courseId+'&subjectId='+subjectId+'" class="custom-btc-link">'+res.breadcrumb.subjectName+'</a>');
				$('#breadcrumb .btc:eq(2)').html('<strong>Content</strong>');
				if(getUrlParameter('demo') == '1')
					$('#backSubject').attr('href', '../courseDetails.php?courseId=' + courseId).text('Back');
				else
					$('#backSubject').attr('href', 'chapter.php?courseId=' + courseId + '&subjectId=' + subjectId);
			}
		});
	}

	//function to fill user details
	function fillUserDetails(data) {
		$('.notify.badge').text(data.notiCount);
		$('.fullName').text(data.details.firstName + ' ' + data.details.lastName);
		$('.username').text(data.details.username);
		$('.user-image').attr('src', data.details.profilePic);
		$('#btn_rupee, #btn_dollar').removeClass('btn-white btn-info');
		if (data.details.currency == 1) {
			$('#btn_dollar').addClass('btn-info');
			$('#btn_rupee').addClass('btn-white');
		}
		else if (data.details.currency == 2) {
			$('#btn_dollar').addClass('btn-white');
			$('#btn_rupee').addClass('btn-info');
		}
	}

	//function to fetch data from url string
	function getStringParameter(sParam, url) {
		var sPageURL = url.split('?')[1];
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++) 
		{
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam) 
			{
				return sParameterName[1];
			}
		}
	}

	//function to fetch video id from vimeo link
	function getVimeoId(url) {
		var sURLVariables = url.split('/');
		return sURLVariables[sURLVariables.length - 1];
	}
	
	$(document).mousemove(function () {
		idleCheckTime = 0;
		//console.log("mousemove");
		$('#activityModal').modal("hide");
		stopIdleCheckTimer();
		stopIdleTimer();
		startIdleCheckTimer();
	});
	$(document).click(function () {
		idleCheckTime = 0;
		//console.log("click");
		$('#activityModal').modal("hide");
		stopIdleCheckTimer();
		stopIdleTimer();
		startIdleCheckTimer();
	});
	$(document).keypress(function (e) {
		idleCheckTime = 0;
		//console.log("keypress");
		$('#activityModal').modal("hide");
		stopIdleCheckTimer();
		stopIdleTimer();
		startIdleCheckTimer();
	});

	//function to start timer for each question
	function startIdleCheckTimer() {
		idleCheckTimer = setInterval(function() {
			idleCheckTime += 1;
			//console.log("idleCheckTime :"+idleCheckTime);
			if (idleCheckTime==1200) {
				$('#activityModal').modal("show");
				stopIdleCheckTimer();
				startIdleTimer();
			};
			//for temporary time display of each question
			//$('#tempQuestionTimer').text(answers[needle].timer);
		},	1000);
	}
	
	//function to stop timer for each question
	function stopIdleCheckTimer() {
		clearInterval(idleCheckTimer);
	}

	//function to start timer for each question
	function startIdleTimer() {
		idleTimer = setInterval(function() {
			idleTime += 1;
			//console.log("idleTime : "+idleTime);
			//for temporary time display of each question
			//$('#tempQuestionTimer').text(answers[needle].timer);
		},	1000);
	}
	
	//function to stop timer for each question
	function stopIdleTimer() {
		clearInterval(idleTimer);
	}
});