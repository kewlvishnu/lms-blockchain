var ApiEndPoint = '../api/index.php';
var details;
var currency=2;
var price=0.00;
$(function() {
	
	//fetchUserDetails();
        checkCurrency();
		
	//function to fetch user details for change password page
	function fetchUserDetails() {
		var req = {};
		var res;
		req.action = 'get-student-details-for-changePassword';
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				details = res;
				fillUserDetails(res);
			}
		});
	}
	
	//function to fill user details
	function fillUserDetails(data) {
		$('.notify.badge').text(data.notiCount);
		$('.fullName').text(data.details.firstName + ' ' + data.details.lastName);
		$('.username').text(data.details.username);
		$('.user-image').attr('src', data.details.profilePic);
		$('#btn_rupee, #btn_dollar').removeClass('btn-white btn-info');
		if(data.details.currency == 1) {
			$('#btn_dollar').addClass('btn-info');
			$('#btn_rupee').addClass('btn-white');
		}
		else if(data.details.currency == 2) {
			$('#btn_dollar').addClass('btn-white');
			$('#btn_rupee').addClass('btn-info');
		}
	}
	
	//function to fetch notifications
	function fetchPurchasingDetails() {
		var req = {};
		var res;
		req.action = 'get-course-details-studentEnd';
                req.courseId = getUrlParameter('courseId');
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alertMsg(res.message);
			else {
				details = res;
				fillPurchasingDetails(res);
			}
		});
	}
	
	//function to fill notifications table
		function fillPurchasingDetails(data) {
		var html = '';
                 var displayCurrency='<i class="fa fa-dollar"></i>';
                  if(currency==2){
                     displayCurrency='<i class="fa fa-rupee"></i>';
                     data.courseDetails.studentPrice=data.courseDetails.studentPriceINR;
                     
                }
                price= data.courseDetails.studentPrice;
               $('#notify-list').find('li').remove();
                   html += '<li data-id="'+data.courseDetails.id+'">'
					+ '<div class="task-title">'
						+ '<span class="task-title-sp">The Course  <b> '+data.courseDetails.name+' of price   '+displayCurrency+data.courseDetails.studentPrice+'</b> </span>'
						+ '<div class="pull-right hidden-phone">'
							+ '<a id="purchase-button" class="btn btn-success btn-xs mark-notification"><i class=" fa fa-shopping-cart"></i>&nbsp;Purchase</a>&emsp;\n\
                                            <a id="purchased-course-btn" style="display:none;" class="btn btn-warning btn-xs"><i class=" fa fa-check"></i>&nbsp;Purchased</a>&emsp;'
                                        	+ '</div>'
					+ '</div>'
				+ '</li>';
		if(html == '')
		html = 'No new invitations found.';
		$('#notify-list').append(html);

		//$('#notify-list').append(html);
		
		//adding event handlers for accept button
		$('#purchase-button').on('click', function() {
			elem = $(this);
			elem.attr('disabled', true);
			var id = $(this).parents('li').attr('data-id');
			var req = {};
			var res;
			req.action = 'link-student-course';
			req.courseId = id;
                        req.price=price;
			$.ajax({
					'type'	:	'post',
					'url'	:	ApiEndPoint,
					'data'	:	JSON.stringify(req)
			}).done(function(res) {
				elem.attr('disabled', false);
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else {
                                     $('#purchase-button').remove();
                                     $('#purchased-course-btn').show();
					//alertMsg("Invitation Accepted successfully.");
					//fetchNotifications();
				}
			});
		});
		
		//adding event handlers for reject button
		$('.reject-button').on('click', function() {
			var id = $(this).parents('li').attr('data-id');
			var req = {};
			var res;
			req.action = 'rejected_student_invitation';
			req.link_id = id;
			$.ajax({
					'type'	:	'post',
					'url'	:	ApiEndPoint,
					'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else {
					alertMsg("Invitation rejected successfully.");
					fetchNotifications();
				}
			});
		});
	}
          function checkCurrency(){
                 var req = {};
                 var res;
        	req.action = 'get-currency';
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status==1){
                           currency=res.currency[0].currency;//'<i class="fa fa-rupee"></i>';
                       } 
                      fetchPurchasingDetails();
		});
  
}
	
});