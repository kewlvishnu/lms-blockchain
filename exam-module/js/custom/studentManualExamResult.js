var courseId = getUrlParameter('courseId');
var subjectId;
var examId = getUrlParameter('examId');
var student = [];
var data;
$(function(){
	fetchBreadcrumb();
	function fetchManualExam() {
		var req = {};
		var res;
		req.action = 'get-manual-exam-student';
		req.courseId = courseId;
		req.subjectId = subjectId;
		req.examId = examId;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				data = res;
				//console.log(res);
				setManualExam();
				fillManualExam();
			}
		});
	}
	function setManualExam() {
		var exam1 = data.exam;
		var rank = 1;
		if (exam1.type == "quick") {
			student['maxMarks']	= parseInt(exam1.questions['marks']);
		} else {
			student['maxMarks']	= parseInt(exam1.maxMarks);
		}
		
		student['studentId']	= exam1.students[0];
		student['studentName']	= exam1.students[1];
		if (exam1.type == "quick") {
			student['marks']		= parseInt(exam1.students[2]['marks']);
			student['questionId']	= parseInt(exam1.students[2]['question_id']);
		} else {
			student['marks']		= parseInt(exam1.students[2]);
		}
		student['percent']		= parseFloat(((student['marks']/student['maxMarks'])*100).toFixed(2));
		student['rank']		= 1;
		if (exam1.type == "quick") {
			student['qMarks']	= exam1.students[2];
		} else {
			student['qMarks']	= exam1.students[3];
		}
		student['rank'] = exam1.students['rank'];
		student['percentile'] = exam1.students['percentile'];

		//console.log(student);
	}
	function fillManualExam() {
		//console.log(data);
		var exam = data.exam;
		var questionsCount = data.questionsCount;
		var questions = exam.questions;
		var filler = '';
		$('#examTitle').html('<h2>Exam : '+exam.name+' <button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button></h2>');
		$('#exam').html('<table class="table table-hover table-bordered '+((exam.type == "deep")?'table-deep':'')+'">'+
							'<thead>'+
								'<tr><th class="text-center" colspan="2">Exam Marks Table ['+((exam.type=='quick')?'Quick':'Deep')+' Analytics]</th></tr>'+
								'<tr>'+
									'<th width="50%">Student Id</th>'+
									'<td class="text-center">'+student['studentId']+'</td>'+
								'</tr>'+
								'<tr class="table-row">'+
									'<th>Student Name</th>'+
									'<td class="text-center">'+student['studentName']+'</td>'+
								'</tr>'+
								'<tr class="table-row">'+
									'<th width="50%">Total Marks (out of '+student.maxMarks+')</th>'+
									'<td class="text-center">'+student['marks']+'</td>'+
								'</tr>'+
								'<tr class="table-row">'+
									'<th width="50%">Percent</th>'+
									'<td class="text-center">'+student['percent']+'</td>'+
								'</tr>'+
								'<tr class="table-row">'+
									'<th width="50%">Rank</th>'+
									'<td class="text-center">'+student['rank']+'</td>'+
								'</tr>'+
								'<tr class="table-row">'+
									'<th width="50%">Percentile</th>'+
									'<td class="text-center">'+student['percentile']+'</td>'+
								'</tr>'+
							'</thead>'+
							'<tbody></tbody>'+
						'</table>');
		var marks = student['qMarks'];
		//console.log(students[studentIndex]);
		//console.log(marks);
		if (exam.type == "deep") {
			$('#questionMarks').html('<table class="table table-bordered">'+
										'<thead>'+
											'<tr>'+
												'<th width="50%" colspan="2">Question Details</th>'+
												'<th width="50%">Pie Chart</th>'+
											'</tr>'+
										'</thead>'+
										'<tbody></tbody></table>');
			var qclass = '';
			for (var i = 0; i < marks.length; i++) {
				qclass = ((i%2==0)?'q-even':'q-odd');
				$('#questionMarks tbody').append('<tr class="marks-dis">'+
													'<th width="25%" class="'+qclass+'">Question</th>'+
													'<td width="25%" class="text-center '+qclass+'">'+'Q'+(i+1)+'</td>'+
													'<td rowspan="3"><div id="pieChart'+i+'" class="ht-250"></div></td>'+
												'</tr>'+
												'<tr class="marks-dis">'+
													'<th class="'+qclass+'">Marks</th>'+
													'<td class="text-center '+qclass+'">'+marks[i]['marks']+'</td>'+
												'</tr>'+
												'<tr class="marks-dis '+qclass+'">'+
													'<th class="'+qclass+'">Maximum Marks</th>'+
													'<td class="text-center '+qclass+'">'+data.exam.questions[i]['marks']+'</td>'+
												'</tr>');
			};
		}
		drawManualExam();
	}
	function drawManualExam () {
		var marksDis = data.exam.marksDis;
		var dataGraph = [];
		var i = 0;
		$.each( marksDis, function( key, value ) {
			dataGraph[i] = [key+' marks',parseInt(value)];
			i++;
		});
		$('#pieGraph').highcharts({
			chart: {
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false
			},
			title: {
				text: 'Total number of students who took this exam are ' + parseInt(data.exam.studentsCount)
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						format: '<b>{point.name}</b>: {point.percentage:.1f} %',
						style: {
							color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
						}
					}
				}
			},
			series: [{
				type: 'pie',
				name: 'Number',
				data: dataGraph
			}]
		});
		drawManualExamQuestions();
	}
	function drawManualExamQuestions () {
		var qMarksDis = data.exam.qMarksDis;
		$.each( qMarksDis, function( key, value ) {
			var dataGraph = [];
			var i = 0;
			$.each( value, function( key1, value1 ) {
				dataGraph[i] = [key1+' marks',parseInt(value1)];
				i++;
			});
			$('#pieChart'+key).highcharts({
				chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false
				},
				title: {
					text: 'Total number of students who took this exam are ' + data.exam.questions[key]['studentCount']
				},
				tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				},
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
							enabled: true,
							format: '<b>{point.name}</b>: {point.percentage:.1f} %',
							style: {
								color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
							}
						}
					}
				},
				series: [{
					type: 'pie',
					name: 'Number',
					data: dataGraph
				}]
			});
		});
		drawPercentGraph();
	}
	function drawPercentGraph() {

		//console.log(data);
		var totalScore=parseFloat(data.exam.maxMarks);
		var differnece=totalScore-parseFloat(data.exam.highest);
		var blocksDiff=(parseFloat(totalScore)-parseFloat(differnece+parseFloat(data.exam.lowest)))/10;
		//var marksDis=[];
		var students=[];
		var markPer=[];
		var newlowest=(parseFloat(data.exam.lowest)+parseFloat(differnece)).toFixed(1);
		
		for(var i=1;i<11;i++)
		{	//marksDis[i-1]=(parseFloat(newlowest)+parseFloat(blocksDiff*i)).toFixed(1);
			students[i-1]=0;
			//checking
			var temp=(parseFloat(newlowest)+parseFloat(blocksDiff*i)).toFixed(1);
			markPer[i-1]=Math.round((temp/totalScore)*100)+'%';
			//markPer[i-1]=markPer[i-1]+'%';
		}
		for(var i=markPer.length-1;i>0;i--)
		{
			markPer[i]= markPer[i-1]+' - '+markPer[i];
		}
		
		markPer[0]=Math.round(newlowest)+'% - '+markPer[0];

		$.each( data.exam.perStudents, function( key, value ) {
			var newscore=(parseFloat(value['score'])+parseFloat(differnece)).toFixed(1);
			var index=Math.round(((newscore-newlowest)/blocksDiff));
			if(index>0)
			{	index=index-1;
			}else{
				index=0;
			}
			students[index]=students[index]+1;
			
			
		});
		//console.log(students);
		$('#percentDistGraph').highcharts({
			chart: {
				zoomType: 'xy'
			},
			title: {
				text: 'Percentage distribution of all students'
			},
			subtitle: {
				text: 'This graph shows percentage w.r.t to highest scorer'
			},
			xAxis: [
			{
				title: {
					text: 'Percentage Score',
					style: {
						color: Highcharts.getOptions().colors[1]
					}
				},
				categories: markPer,
				crosshair: true
			}],
			yAxis: [{ // Primary yAxis
				labels: {
					format: '{value}',
					style: {
						color: Highcharts.getOptions().colors[1]
					}
				},
				title: {
					text: 'No. of Students',
					style: {
						color: Highcharts.getOptions().colors[1]
					}
				}
			}, { // Secondary yAxis
				title: {
					text: 'Students',
					style: {
						color: Highcharts.getOptions().colors[0]
					}
				},
				labels: {
					format: '{value}',
					style: {
						color: Highcharts.getOptions().colors[0]
					}
				},
				opposite: true
			}],
			tooltip: {
				shared: true
			},
			legend: {
				layout: 'vertical',
				align: 'left',
				x: 120,
				verticalAlign: 'top',
				y: 100,
				floating: true,
				backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
			},
			series: [{
				name: 'Students',
				type: 'column',
				yAxis: 1,
				data: students,
				tooltip: {
					valueSuffix: ''
				}

			}, {
				name: 'No. of Students',
				type: 'spline',
				yAxis: 1,
				data: students,
				tooltip: {
					valueSuffix: ''
				}
			}]
		});
		$('.percAllStudents').show();
	}
	function fetchBreadcrumb() {
		var req = {};
		var res;
		req.action = 'get-breadcrumb-for-manual-exam';
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				//console.log(res);
				subjectId = res.subject.subjectId;
				fillBreadcrumb(res);
				fetchManualExam();
			}
		});
	}
	function fillBreadcrumb(data) {
		$('ul.breadcrumb li:eq(1)').find('a').attr('href', 'MyExam.php?courseId=' + data.course.courseId).text(data.course.courseName);
		$('ul.breadcrumb li:eq(2)').find('a').attr('href', 'chapter.php?courseId=' + data.course.courseId + '&subjectId=' + data.subject.subjectId).text(data.subject.subjectName);
		$('ul.breadcrumb li:eq(3)').find('a').attr('href', 'studentManualExamResult.php?courseId=' + data.course.courseId + '&subjectId=' + data.subject.subjectId + '&examId=' + data.exam.examId).text(data.exam.examName);
	}

});