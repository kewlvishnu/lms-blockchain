var ApiEndPoint = '../api/index.php';
var details;
var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

$(function () {
    fetchUserDetails();
    getStudentCourses();
	getStudentSubsCourses();
    //function to fetch user details for change password page
    function fetchUserDetails() {
        var req = {};
        var res;
        req.action = 'get-student-details-for-changePassword';
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 0)
                alert(res.message);
            else {
                details = res;
                fillUserDetails(res);
            }
        });
    }

    //function to fill user details
    function fillUserDetails(data) {
	$('.notify.badge').text(data.notiCount);
        $('.fullName').text(data.details.firstName + ' ' + data.details.lastName);
        $('.username').text(data.details.username);
        $('.user-image').attr('src', data.details.profilePic);
        $('#btn_rupee, #btn_dollar').removeClass('btn-white btn-info');
        if (data.details.currency == 1) {
            $('#btn_dollar').addClass('btn-info');
            $('#btn_rupee').addClass('btn-white');
        }
        else if (data.details.currency == 2) {
            $('#btn_dollar').addClass('btn-white');
            $('#btn_rupee').addClass('btn-info');
        }
    }
    function getStudentCourses() {
        var req = {};
        var res;
        req.portfolioSlug = $('#slug').val();
        req.pageUserId = $('#inputUserId').val();
        req.pageUserRole = $('#inputUserRole').val();
        req.action = 'student-joined-courses';
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
		
            res = $.parseJSON(res);
            if (res.status == 0)
                alert(res.message);
            else {
				
                fillStudentCourses(res);
            }
        });
    }
    function fillStudentCourses(data) {
        var subjectHtml = '', html = '';
		var courseStartDate = '';
        if (data.courses.length>0) {
            $.each(data.courses, function (i, course) {
    			//variable to detect payment can be made or not
    			var paymentStatus = 1;
                var startdate = new Date(course.startDate);
                var today = new Date();
                /*var enddate = new Date(startdate);
                enddate.setFullYear(enddate.getFullYear() + 1);*/
                if (course.endDate != null && course.endDate != '') {
                    //var enddatedb = new Date(parseInt(course.endDate));
                    var enddate = new Date(parseInt(course.endDate));
    				//var origStart = new Date(parseInt(course.origStartDate));
                    /*if (enddatedb.valueOf() < enddate.valueOf()) {
                        //enddate = new Date(enddatedb);
                        enddate = enddatedb;
    					//disabling self payment
    					paymentStatus = 0;
                    }*/
    				//this will show the course starting date
    				//courseStartDate = '<br><i class="fa fa-clock-o"></i> <strong>Start Date</strong> ' + origStart.getDate() + ' ' + month[origStart.getMonth()] + ' ' + origStart.getFullYear();
                } else {
                    var enddate = "";
                }
                var percentage = 0;
                var percent = 0;
                var display = '0%';
                var expired="";
                if(enddate!= "" && (enddate.valueOf()<today.valueOf())){
                    expired="disable-mycourse";
                    if(enddate!= "") {
                        percentage = (today.valueOf() - startdate.valueOf()) / (enddate.valueOf() - startdate.valueOf()) * 100;
                        percent = parseFloat(percentage);
                        display = percent.toFixed(2) + '%';
                        expired="";
                    }
                }
                // alert(display);
                html += '<div class="col-md-12 col-sm-12">'
                        + '<div class="' + expired + '" data-paymentStatus="' + paymentStatus + '">'
                        + '<div class="row">'
                        + '<div class="col-md-3"><a  href="MyExam.php?courseId=' + course.course_id + '" ><img style="width: 100%;height: 120px;" onerror="this.src=\'user-data/images/course.png\'"  class="img-responsive" src="' + course.coverPic + '" alt=""></a>'
                        + '</div>'
                        + '<div class="col-md-9">'
                        + '<div class="col-md-12"><div style="float:left"> <h3 class=""> <a href="MyExam.php?courseId=' + course.course_id + '" > ' + course.name + '<span style="font-size:12px;font-weight:600;"> (ID: C00' + course.course_id + ')</span> </a></h3><a href="../' + course.inviter_role + '/' + course.userId + '" target="_blank" class="institute" data-instituteId="' + course.userId + '"><strong>By</strong>: ' + course.inviter + '</a>'
    					+ courseStartDate
                        + '</div>'
                        + '<div style="float:right;">'
                        + '<div class="text-right">'
                        + '<a href="MyExam.php?courseId='+course.course_id+'" class="btn btn-md btn-success course" data-courseId="' + course.course_id + '">Start Learning</a>'
                        + '</div>'
                        + '</div>'
                        + '</div>'
                        + '<div class="col-sm-4" style="padding-top:10px;">'
                        + '<div class="col-xs-2"><i style="font-size:35px; color:orange;margin:5px 10px 0px 0px;" class="fa fa-clock-o"></i></div>'
                        + '<div class="col-xs-10">'                  
                        + '<span><strong>Joining Date</strong>&nbsp;' + startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + '</span>'+((enddate!="")?('<br/><span><strong>End Date</strong>&nbsp;&nbsp;&nbsp;' + enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + '</span>'):(''))
                        + '<div style="margin-top: 6px;" class="progress progress-xs">'
                        + '<div style="width: ' + display + '" aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar progress-bar-danger">'
                        + '<span class="sr-only">' + display + ' Complete</span>'
                        + '</div>'
                        + '</div>'
                        + '</div>'
                        + '</div>'
                        + '<div class="col-md-8" style="padding-top:10px;">'
                        + '<div class="col-xs-1"><i style="font-size:30px; color:orange;margin:5px 15px 0px 0px;" class="fa fa-th-list"></i></div>'
                        + '<div class="col-xs-11">'  
                        + '<strong>Subjects </strong><br/>'
                       subjectHtml='';
                $.each(course.subjects, function (j, subject) {
                    subjectHtml += '<a href="chapter.php?courseId='+course.course_id+'&subjectId='+subject.subjectId+'">' + subject.subjectName + ', </a>';
                });
    			subjectHtml = subjectHtml.substring(0, subjectHtml.length - 6) + '</a>';
                 html = html + subjectHtml + '</div>'
                        + '</div>'
                        +'</div>'
                        + '</div>'
                        +'<hr style="color: #CBC8C8;"/>'
                        + '</div>'

                        + '</div>';
            });
        } else {
            html = '<div class="col-md-12 col-sm-12"><div class="text-center"><p class="text-center">Oops you are not enrolled to any courses.</p><a href="../courses" class="btn btn-warning btn-lg">Explore Courses</a></div></div>';
        }
        $('#div-student-course').append(html);
		//event listener for handling expired courses
		$('.disable-mycourse a:not(.institute)').on('click', function(e) {
			e.preventDefault();
			var instituteId = $(this).parents('.disable-mycourse').find('.institute').attr('data-instituteId');
			var courseId = $(this).parents('.disable-mycourse').find('.course').attr('data-courseId');
			$('#instituteId').val(instituteId);
			$('#courseId').val(courseId);
			$('#payYourself').attr('href', 'paymentDetails.php?courseId=' + courseId);
			if($(this).parents('.disable-mycourse').attr('data-paymentStatus') == 0) {
				//now removing self pay button and changing the text of other button
				$('#payYourself').hide();
				$('#showContact').text('Contact Institute/Professor for extending the course');
				$('#payment .message').text('This course has expired. Contact respective Institute/Professor to extend this course.');
			}
			$('#payment').modal('show');
		});
    }
	function getStudentSubsCourses() {
        var req = {};
        var res;
        req.pageUserId = $('#inputUserId').val();
        req.pageUserRole = $('#inputUserRole').val();
        req.action = 'student-subs-courses';
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 0)
                alert(res.message);
            else {
			
               fillStudentSubsCourses(res);
            }
        });
    }
    function fillStudentSubsCourses(data) {		
        var subjectHtml = '', html = '';
		var courseStartDate = '';
        if (data.courses.length>0) {
            $.each(data.courses, function (i, course) {
    			//variable to detect payment can be made or not
    			var paymentStatus = 1;
                var startdate = new Date(course.startDate);
                var today = new Date();
                /*var enddate = new Date(startdate);
    			//var newenddate= new Date(course.endDate);
    			//console.log(newenddate);
                enddate.setFullYear(enddate.getFullYear() + 1);*/
                if (course.endDate != null && course.endDate != '') {
                    /*var enddatedb = new Date((course.endDate));
    				//var origStart = new Date(parseInt(course.origStartDate));
    				enddate=enddatedb;
    				console.log(enddate);*/
    				//this will show the course starting date
    				//courseStartDate = '<br><i class="fa fa-clock-o"></i> <strong>Start Date</strong> ' + origStart.getDate() + ' ' + month[origStart.getMonth()] + ' ' + origStart.getFullYear();
                    var enddate = new Date((course.endDate));
                } else {
                    var enddate = "";
                }
                var percentage = 0;
                var percent = 0;
                var display = '0%';
                var expired="";
                if(enddate!= "" && (enddate.valueOf()<today.valueOf())) {
                    expired="disable-mycourse";
                    if(enddate!= "") {
                        percentage = (today.valueOf() - startdate.valueOf()) / (enddate.valueOf() - startdate.valueOf()) * 100;
                        percent = parseFloat(percentage);
                        display = percent.toFixed(2) + '%';
                        expired="";
                    }
                }
                // alert(display);
                html += '<div class="col-md-12 col-sm-12">'
                        + '<div class="' + expired + '" data-paymentStatus="' + paymentStatus + '">'
                        + '<div class="row">'
                        + '<div class="col-md-3"><a  href="MyExam.php?courseId=' + course.course_id + '" ><img style="width: 100%;height: 120px;" onerror="this.src=\'user-data/images/course.png\'"  class="img-responsive" src="' + course.coverPic + '" alt=""></a>'
                        + '</div>'
                        + '<div class="col-md-9">'
                        + '<div class="col-md-12"><div style="float:left"> <h3 class=""> <a href="MyExam.php?courseId=' + course.course_id + '" > ' + course.name + '<span style="font-size:12px;font-weight:600;"> (ID: C00' + course.course_id + ')</span> </a></h3><a href="../' + course.inviter_role + '/' + course.userId + '" target="_blank" class="institute" data-instituteId="' + course.userId + '"><strong>By</strong>: ' + course.inviter + '</a>'
    					+ courseStartDate
                        + '</div>'
                        + '<div style="float:right;">'
                        + '<div class="text-right">'
                        + '<a href="MyExam.php?courseId='+course.course_id+'" class="btn btn-md btn-success course" data-courseId="' + course.course_id + '">Start Learning</a>'
                        + '</div>'
                        + '</div>'
                        + '</div>'
                        + '<div class="col-sm-4" style="padding-top:10px;">'
                        + '<div class="col-xs-2"><i style="font-size:35px; color:orange;margin:5px 10px 0px 0px;" class="fa fa-clock-o"></i></div>'
                        + '<div class="col-xs-10">'                  
                        + '<span><strong>Joining Date</strong>&nbsp;' + startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + '</span>'+((enddate!="")?('<br/><span><strong>End Date</strong>&nbsp;&nbsp;&nbsp;' + enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + '</span>'):(''))
                        + '<div style="margin-top: 6px;" class="progress progress-xs">'
                        + '<div style="width: ' + display + '" aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar progress-bar-danger">'
                        + '<span class="sr-only">' + display + ' Complete</span>'
                        + '</div>'
                        + '</div>'
                        + '</div>'                   
                        + '</div>'
                       
                        +'<div class="col-md-8" style="padding-top:10px;">'
                        + '<div class="col-xs-1"><i style="font-size:30px; color:orange;margin:5px 15px 0px 0px;" class="fa fa-th-list"></i></div>'
                        +'<div class="col-xs-11">'  
                        + '<strong>Subjects </strong><br/>'
                       subjectHtml='';
                $.each(course.subjects, function (j, subject) {
                    subjectHtml += '<a href="chapter.php?courseId='+course.course_id+'&subjectId='+subject.subjectId+'">' + subject.subjectName + ', </a>';
                });
    			subjectHtml = subjectHtml.substring(0, subjectHtml.length - 6) + '</a>';
                 html = html + subjectHtml + '</div>'
                        + '</div>'
                        +'</div>'
                        + '</div>'
                 +'<hr style="color: #CBC8C8;"/>'
                        + '</div>'
             
                        + '</div>';
            });
        } else {
            html = '<div class="col-md-12 col-sm-12"><div class="text-center"><p class="text-center">Oops you are not subscribed to any packages.</p><a href="../packages" class="btn btn-warning btn-lg">Explore Packages</a></div></div>';
        }
        $('#div-student-subscibed-course').append(html);
		//event listener for handling expired courses
		$('.disable-mycourse a:not(.institute)').on('click', function(e) {
			e.preventDefault();
			var instituteId = $(this).parents('.disable-mycourse').find('.institute').attr('data-instituteId');
			var courseId = $(this).parents('.disable-mycourse').find('.course').attr('data-courseId');
			$('#instituteId').val(instituteId);
			$('#courseId').val(courseId);
			$('#payYourself').attr('href', 'paymentDetails.php?courseId=' + courseId);
			if($(this).parents('.disable-mycourse').attr('data-paymentStatus') == 0) {
				//now removing self pay button and changing the text of other button
				$('#payYourself').hide();
				$('#showContact').text('Contact Institute/Professor for extending the course');
				$('#payment .message').text('This course has expired. Contact respective Institute/Professor to extend this course.');
			}
			$('#payment').modal('show');
		});
    }
	
	//event listener for payment modal
	$('#showContact').on('click', function(){
		$('#contactContainer').show();
	}/*, function() {
		$('#contactContainer').hide();
	}*/);
	
	$('#sendContact').on('click', function() {
		unsetError($('#contact'));
		if($('#contact').val() == '') {
			setError($('#contact'), 'Please fill the message box.');
			return;
		}
		
		var req = {};
		var res;
		req.action = 'send-renew request';
		req.courseId = $('#courseId').val();
		req.instituteId = $('#instituteId').val();
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			$('#payment').modal('hide');
			if(res.status == 0) {
				alert(res.message);
			}
			else {
				alertMsg("Your request send to the respective Institute/Professor.");
			}
		});
	});
	
	$('#payment').on('hidden.bs.modal', function() {
		$('#contact').val('Type your message here.');
		$('#contactContainer').hide();
		//reverting buttons to original on modal hidden
		$('#payYourself').show();
		$('#showContact').text('Contact Institute/Professor for key access.');
		$('#payment .message').text('Your access to this course has expired. Please renew your subscription by selecting any one payment method.');
	});
});