var ApiEndPoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var imageRoot = '/';
var details;

$(function() {
	
	fetchUserDetails();
	
	//event handlers to switch between edit and view sections
	$('.edit-button').on('click', function() {
		if($(this).find('i:eq(0)').hasClass('fa-pencil')) {
			$(this).find('.fa-pencil').removeClass('fa-pencil').addClass('fa-times');
			$(this).parents('section:eq(0)').find('.view-section').hide();
			$(this).parents('section:eq(0)').find('.edit-section').show();
		}
		else if($(this).find('i:eq(0)').hasClass('fa-times')) {
			$(this).find('.fa-times').removeClass('fa-times').addClass('fa-pencil');
			$(this).parents('section:eq(0)').find('.edit-section').hide();
			$(this).parents('section:eq(0)').find('.view-section').show();
			fillUserDetails(details);
		}
	});
	
	//event handlers to save profile details
	$('.save-button').on('click', function() {
		$(this).attr('disabled', true);
		var name = $(this).attr('data-name');
		if(name == 'bio')
			suc = saveBio();
		else if(name == 'achievement')
			suc = saveAchievement();
		else if(name == 'interest')
			suc = saveInterest();
	});
	
	//event handler for cancel buttons
	$('.cancel-button').on('click', function() {
		$(this).parents('section:eq(0)').find('.edit-section').hide();
		$(this).parents('section:eq(0)').find('.view-section').show();
		$(this).parents('section:eq(0)').find('.fa-times').removeClass('fa-times').addClass('fa-pencil');
	});
	
	//event handler to show change picture button on hover for cover pic
	$('.cover-photo').hover(function() {
		$(this).find('.upload-cover-image').show();
	},function(){
		$(this).find('.upload-cover-image').hide();
	});
	$("#cover-image-form #cover-image-uploader").change(function() {
        $('#cover-image-form input[type="submit"]').show();
        readURL1(this);
    });
	
	$('#cover-image-form').attr('action', fileApiEndpoint);
	$('#cover-image-form').on('submit', function(e) {
		e.preventDefault();
		c = jcrop_api.tellSelect();
		crop = [c.x, c.y, c.w, c.h];
		imageC = document.getElementById('uploaded-image1');
		var sources = {
            'cover-image': {
                image: imageC,
                type: 'image/jpeg',
                crop: crop,
                size: [634, 150],
                quality: 2.0
            }
        }
        //settings for $.ajax function
        var settings = {
			url: fileApiEndpoint,
			data: {'text': 'This is the text!'}, //three fields (medium, small, text) to upload
			beforeSend: function()
			{
				$("#cover-image-form .progress").show();
			},
			complete: function (resp) {
				$("#cover-image-form .progress").hide();
				//$("#cover-image-form .msg").html("<font color='green'>"+resp.responseText+"</font>").show();
				$('#cover-image-form .jcrop-holder').hide();
				$('#cover-image-form input[type="file"]').val('');
				$('#cover-image-form input[type="submit"]').hide();
				fetchUserDetails();
				$('#upload-cover-image').modal('hide');
			}
        }
        cropUploadAPI.cropUpload(sources, settings);
	});
	
	//event handler to show change picture button on hover for profile pic
	$('.profile-photo').hover(function() {
		$(this).find('.upload-profile-image').show();
	},function(){
		$(this).find('.upload-profile-image').hide();
	});
	$("#profile-image-form #profile-image-uploader").change(function() {
        $('#profile-image-form input[type="submit"]').show();
        readURL2(this);
    });
	
	$('#profile-image-form').attr('action', fileApiEndpoint);
	//uploading profile image
	$('#profile-image-form').on('submit', function(e) {
		e.preventDefault();
		c = jcrop_api.tellSelect();
		crop = [c.x, c.y, c.w, c.h];
		imageP = document.getElementById('uploaded-image2');
		var sources = {
            'profile-image': {
                image: imageP,
                type: 'image/jpeg',
                crop: crop,
                size: [200, 200],
                quality: 2.0
            }
        }
        //settings for $.ajax function
        var settings = {
			url: fileApiEndpoint,
			data: {'text': 'This is the text!'}, //three fields (medium, small, text) to upload
			beforeSend: function()
			{
				$("#profile-image-form .progress").show();
			},
			complete: function (resp) {
				$("#profile-image-form .progress").hide();
				//$("#profile-image-form .msg").html("<font color='green'>"+resp.responseText+"</font>").show();
				res = $.parseJSON(resp.responseText);
				if(res.status == 0)
					alertMsg('Something went wrong. Please try again');
				$('#profile-image-form .jcrop-holder').hide();
				$('#profile-image-form input[type="file"]').val('');
				$('#profile-image-form input[type="submit"]').hide();
				$('#upload-profile-image').modal('hide');
				fetchUserDetails();
			}
        }
        cropUploadAPI.cropUpload(sources, settings);
	});
	
	//function to fetch user details
	function fetchUserDetails() {
		var req = {};
		var res;
		req.action = 'get-student-details';
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				details = res;
				fillUserDetails(res);
			}
		});
	}
	
	//function to fill user details
	function fillUserDetails(data) {
		$('.notify.badge').text(data.notiCount);
		$('.username').text(data.details.username);
		$('.fullName').text(data.details.name);
		$('#profile-name').text(data.details.name);
		$('#user-type').text('Student');
		if(data.details.coverPic != '')
			$('#cover-pic').attr('src', data.details.coverPic);
		if(data.details.profilePic != '') {
			$('#profile-pic').attr('src', data.details.profilePic);
			$('.user-image').attr('src', data.details.profilePic);
		}
		if(data.details.background == '')
			$('#user-bio .view-section .text').text('A short note about you.');
		else {
			 var showdata=data.details.background.replace(new RegExp('\r?\n','g'), '<br />');
                         $('#user-bio .view-section .text').html(showdata);
                    	$('#user-bio .edit-section textarea').val(data.details.background);
		}
		if(data.details.achievements == '')
			$('#user-achievements .view-section').text('Your Achievements.');
		else {
                      
			$('#user-achievements .view-section').html(data.details.achievements.replace(new RegExp('\r?\n','g'), '<br />'));
			$('#user-achievements .edit-section textarea').val(data.details.achievements);
		}
		if(data.details.interests == '')
			$('#student-interests .view-section').text('Your Interests.');
		else {
			$('#student-interests .view-section').html(data.details.interests.replace(new RegExp('\r?\n','g'), '<br />'));
			$('#student-interests .edit-section textarea').val(data.details.interests);
		}
		var html = '';
		if(data.courses.length != 0) {
			for(var i = 0; i < data.courses.length; i++) {
				html += '<p><a href="MyExam.php?courseId=' + data.courses[i].id + '">'+data.courses[i].name+'</a></p>';
			}
		}
		else
			html = '<p>You are not enrolled in any of the courses.</p>';
		$('#student-courses .view-section').html(html);
	}
	
	//function to save background information
	function saveBio() {
		var req = {};
		var res;
		req.action = 'save-student-bio';
		req.background = $('#user-bio .edit-section textarea').val();
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			$('.save-button').attr('disabled', false);
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				$('#user-bio').find('.edit-section').hide();
				$('#user-bio').find('.view-section').show();
				$('#user-bio').find('.fa-times').removeClass('fa-times').addClass('fa-pencil');
				details.details.background = req.background;
				$('#user-bio').find('.view-section .text').html(details.details.background.replace(new RegExp('\r?\n','g'), '<br />'));
			}
		});
	}
	
	//function to save student's achievements
	function saveAchievement() {
		var req = {};
		var res;
		req.action = 'save-student-achievement';
		req.achievement = $('#user-achievements .edit-section textarea').val();
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			$('.save-button').attr('disabled', false);
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				$('#user-achievements').find('.edit-section').hide();
				$('#user-achievements').find('.view-section').show();
				$('#user-achievements').find('.fa-times').removeClass('fa-times').addClass('fa-pencil');
				details.details.achievement = req.achievement;
				$('#user-achievements').find('.view-section').html(details.details.achievement.replace(new RegExp('\r?\n','g'), '<br />'));
			}
		});
	}
	
	//function to save student interests
	function saveInterest() {
		var req = {};
		var res;
		req.action = 'save-student-interest';
		req.interest = $('#student-interests .edit-section textarea').val();
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			$('.save-button').attr('disabled', false);
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				$('#student-interests').find('.edit-section').hide();
				$('#student-interests').find('.view-section').show();
				$('#student-interests').find('.fa-times').removeClass('fa-times').addClass('fa-pencil');
				details.details.interests = req.interest;
				$('#student-interests').find('.view-section').html(details.details.interests.replace(new RegExp('\r?\n','g'), '<br />'));
			}
		});
	}
	
	
	//function to handle img upload
	function readURL1(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
		//console.log(input.files[0]['type']);
			if(!(input.files[0]['type'] == 'image/jpeg' || input.files[0]['type'] == 'image/png' || input.files[0]['type'] == 'image/gif')) {
				$('#cover-image-form input[type=submit]').prop('disabled',true);
				$('#cover-image-form .error').remove();
				$('#cover-image-form').append('<span style="color:red;" class="error">Please select a proper file type.</span>');
				return;
			}
			else if(input.files[0]['size']>819200) {
				$('#cover-image-form input[type=submit]').prop('disabled',true);
				$('#cover-image-form .error').remove();
				$('#cover-image-form').append('<span style="color:red;" class="error">Files larger than 800kb are not allowed.</span>');
				return;
			}
			else {
				$('#cover-image-form .error').remove();
				$('#cover-image-form input[type=submit]').prop('disabled',false);
			}
			if (typeof jcrop_api != 'undefined' && jcrop_api != null) {
				jcrop_api.destroy();
				jcrop_api = null;
				var pImage = $('.crop');
				pImage.css('height', 'auto');
				pImage.css('width', 'auto');
				var height = pImage.height();
				var width = pImage.width();
				
				$('.jcrop').width(width);
				$('.jcrop').height(height);
			}
			reader.onload = function (e) {
				//$('#cover-image-form #uploaded-image1').remove();
				//$('<img id="cover-image-form" class="crop" src="" />').insertAfter($('#cover-image-form #cover-image-uploader'));
				$('#cover-image-form #uploaded-image1').attr('src', e.target.result).show();
				$('#cover-image-form .crop').Jcrop({
					onSelect: updateCoords1,
					bgOpacity:   .4,
					boxWidth: 830,
					minSize: [280, 200],
					setSelect:   [0, 0, 280, 200],
					aspectRatio: 1109/258
				}, function() {
					jcrop_api = this;
					jcrop_api.setSelect([0, 0, 280, 200]);
				});
			}
			reader.readAsDataURL(input.files[0]);
		}
	}

	function updateCoords1(c) {
		$('#x1').val(c.x);
		$('#y1').val(c.y);
		$('#w1').val(c.w);
		$('#h1').val(c.h);
		var crop = [c.x, c.y, c.w, c.h];
	}

	function readURL2(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			if(!(input.files[0]['type'] == 'image/jpeg' || input.files[0]['type'] == 'image/png'|| input.files[0]['type'] == 'image/gif')) {
				$('#profile-image-form input[type=submit]').prop('disabled',true);
				$('#profile-image-form input[type=submit]').next().remove();
				$('#profile-image-form').append('<span style="color:red;" class="error">Please select a proper file type.</span>');
				return;
			}
			else if(input.files[0]['size']>819200) {
				$('#profile-image-form input[type=submit]').prop('disabled',true);
				$('#profile-image-form input[type=submit]').next().remove();
				$('#profile-image-form').append('<span style="color:red;" class="error">Files larger than 800kb are not allowed.</span>');
				return;
			}
			else {
				$('#profile-image-form .error').remove();
				$('#profile-image-form input[type=submit]').prop('disabled',false);
			}
			if (typeof jcrop_api != 'undefined' && jcrop_api != null) {
				jcrop_api.destroy();
				jcrop_api = null;
				var pImage = $('.crop');
				pImage.css('height', 'auto');
				pImage.css('width', 'auto');
				var height = pImage.height();
				var width = pImage.width();
				
				$('.jcrop').width(width);
				$('.jcrop').height(height);
			}
			reader.onload = function (e) {
				$('#profile-image-form #uploaded-image2').attr('src', e.target.result).show();
				$('#profile-image-form .crop').Jcrop({
					onSelect: updateCoords2,
					bgOpacity:   .4,
					boxWidth: 830,
					setSelect:   [ 0, 0, 200, 200],
					aspectRatio: 1
				}, function() {
					jcrop_api = this;
					jcrop_api.setSelect([0, 0, 200, 200]);
				});
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	function updateCoords2(c) {
		$('#x2').val(c.x);
		$('#y2').val(c.y);
		$('#w2').val(c.w);
		$('#h2').val(c.h);
		var crop = [c.x, c.y, c.w, c.h];
	}
});