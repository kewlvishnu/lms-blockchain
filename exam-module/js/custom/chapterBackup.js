var ApiEndPoint = '../api/index.php';
var details;
var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var courseId = getUrlParameter('courseId');
var subjectId = getUrlParameter('subjectId');

$(function () {

    //fetchUserDetails();
    getStudentExams();
    // fetchCourseDetails();
    //function to fetch user details for change password page
    function fetchUserDetails() {
        var req = {};
        var res;
        req.action = 'get-student-details-for-changePassword';

        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 0)
                alert(res.message);
            else {
                details = res;
                fillUserDetails(res);
            }
        });
    }

    //function to fill user details
    function fillUserDetails(data) {
        $('.fullName').text(data.details.firstName + ' ' + data.details.lastName);
        $('.username').text(data.details.username);
        $('.user-image').attr('src', data.details.profilePic);
        $('#btn_rupee, #btn_dollar').removeClass('btn-white btn-info');
        if (data.details.currency == 1) {
            $('#btn_dollar').addClass('btn-info');
            $('#btn_rupee').addClass('btn-white');
        }
        else if (data.details.currency == 2) {
            $('#btn_dollar').addClass('btn-white');
            $('#btn_rupee').addClass('btn-info');
        }
    }
    function getStudentExams() {
        var req = {};
        var res;
        req.action = 'exam-attempts-chapterwise';
        req.courseId = courseId;
        req.subjectId = subjectId;
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 0)
                console.log(res.message);
            else {
                fillStudentExams(res);
            }
        });

    }
    function fillStudentExams(data) {
        var examHtml = '', AssignmentHtml = '';
        var divHtml = '', tr = '', divIntial = '', divClose = '';
        var contentLink = '';
        var count = 1;
        var disabled = '', startnow = '';
        $.each(data.exams, function (i, exam) {
            var startdate = new Date(parseInt(exam.startDate));
            var display_chapter;
            var enddate, display_enddate;

            if (exam.endDate == null || exam.endDate == '') {
                display_enddate = '--';
            } else {
                enddate = new Date(parseInt(exam.endDate));
               display_enddate = enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + ' ' + enddate.getHours() + ':' + enddate.getMinutes();
            }
            if (exam.chapter == null) {
                display_chapter = 'Independent Exams/Assignments';
            } else {
                display_chapter = ' ' + exam.chapter;
            }
            if (exam.chapterId == 0) {
                contentLink = '#';
            } else {
                contentLink = 'chapterContent.php?chapterId=' + exam.chapterId;
            }
            divIntial = '<div class="custom-bg all-container ">'
                    + ' <div>'
                    + '<div class="col-md-6">' + count + ') &nbsp;<strong>Chapter</strong>: ' + display_chapter + ' </div>'
                    + ' <div style="border: medium none;" class="col-md-6 text-right"><a href="' + contentLink + '" class="btn btn-xs btn-danger">View Content</a> <a  class="btn btn-xs btn-danger viewExam">View Assignment</a></div>'
                    + '</div>'
                    + ' <br/>'
                    + '<div class="panel-body div-exams">'
                    + '<strong>Assignment Exams</strong>'
                    + '<table class="table tbl_exams"> '
                    + ' <thead>'
                    + ' <tr>'
                    + '<th class="border-none">Exam<br></th>'
                    + '<th class="border-none">Type<br></th>'
                    + ' <th class="text-center border-none">Start Date</th>'
                    + '<th class="text-center border-none">End Date</th>'
                    + '<th class="text-center border-none">Attempts Given</th>'
                    + '<th class="text-center border-none">Attempts Left</th>'
                    + '<th class="text-center border-none">Results</th>'
                    + '<th class="border-none"></th>'
                    + '</tr>'
                    + '</thead>'
                    + '<tbody>';
            if (exam.examgiven == 0) {
                disabled = 'disabled="disabled"';
            } else {
                disabled = '';
            }
            var attempsLeft = exam.attempts - exam.examgiven;

            tr = '<tr>'
                    + ' <td class="border-none text-danger">' + exam.exam + '</td>'
                    + '<td class="text-center border-none">' + exam.type + '</td>'
                   + '<td class="text-center border-none">' + startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + ' ' + startdate.getHours() + ':' + startdate.getMinutes() + '</td>'
                    + '<td class="text-center border-none">' + display_enddate + '</td>'
                    + '<td class="text-center border-none">' + exam.examgiven + '</td>'
                    + '<td class="text-center border-none">' + (exam.attempts - exam.examgiven) + '</td>'
                    + '<td class="text-center border-none"><a ' + disabled + ' href="studentResult.php?examId=' + exam.id + '" class="btn btn-success btn-xs ">Show</a></td>'
                    + '<td class="text-center border-none"><a  href="examModule.php?examId=' + exam.id + '" class="btn btn-danger btn-xs start-exam" data-startdate="' + exam.startDate + '" data-endDate=' + exam.endDate + ' data-attempts=' + attempsLeft + ' DISABLED>Start Now</a></td>'
                    + '</tr>';

            divClose = '</tbody>'
                    + '</table></div>'
                    + '</div><br/><br/>';

            if (i == 0) {
                divHtml += divIntial;
                count++;
            } else {
                if (data.exams[i].chapterId != data.exams[i - 1].chapterId) {
                    divHtml += divIntial;
                    count++;
                }
            }
            divHtml += tr;
            if (i + 1 == data.exams.length) {
                divHtml += divClose;
            } else {
                if (data.exams[i].chapterId != data.exams[i + 1].chapterId) {
                    divHtml += divClose;
                }
            }
        });

        $('#div_exam ').append(divHtml);
        $('.div-exams').hide();
       var currentDate = data.currentDate;
        setInterval(function(){ 
            var todayTime = new Date(currentDate * 1000);
            $('.start-exam').each(function () {
           if ($(this).attr('data-attempts') != 0)
                {
                    var startdate = new Date(parseInt($(this).attr('data-startdate')));
                    if (startdate.valueOf() <= todayTime.valueOf())
                    {
                        $(this).attr('disabled', false);
                    }
                    var endDate = $(this).attr('data-endDate');
                    if (endDate != '') {
                        var examendDate = new Date(parseInt(endDate));
                        if (examendDate.valueOf() <= todayTime.valueOf())
                        {
                            $(this).attr('disabled', 'disabled');
                        }

                    }
                }

            });
 currentDate++;
        }, 1000);
        
        $('.viewExam').on('click', function () {
            $(this).parents('.all-container').find('.div-exams').slideDown('slow');

        });


    }
    $('#showExam').on('click', function () {
        $('#tbl_exams tbody tr').show('slow');

    });
    $('#showAssignment').on('click', function () {
        $('#tbl_assignment tbody tr').show('slow');

    });

    function fetchCourseDetails() {
        var req = {};
        var res;
        req.action = 'courseDetails-for-student';
        req.courseId = courseId;
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 0)
                console.log(res.message);
            else {
                // details = res;
                fillCourseSubjectDetails(res);
            }
        });

    }
    function fillCourseSubjectDetails(data) {
        var tr = '';
        $('#course-name').text('Course : ' + data.course[0].course);
        $('#instiute-name').text(data.course[0].Institute);
        $.each(data.subjects, function (i, subject) {
            tr += '<td class="text-center"><img alt="" src="' + subject.image + '"><br/><a href="#"> <b>' + subject.name + '</b></a></td>';

        });
        $('#tbl-subjects tbody').append(tr);
    }

});