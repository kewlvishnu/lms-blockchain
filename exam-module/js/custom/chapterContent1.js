var ApiEndPoint = '../api/index.php';
var contentIds = [];
var chapterId = getUrlParameter('chapterId');
var courseId=getUrlParameter('courseId');
var subjectId=getUrlParameter('subjectId');
var watchedVideo=[];
var progress=[];
var pauseTotal=0;
var percent_done=0;	
var starttime=0;
var pauseStart=0;
var pauseCount=0;
var presentContentId=0;
var CHECKVIDEO=70;
var StuffType=0;
var youTubeduration=1; // for youtube player total duration done
var vimeoPercent_done=0;//for vimeo player percentage done
var pauseStartVideo;
var idleTime = 0;
var idleTimePoint = 0;
var noOftime=0;
var watchnumber=0;
var contentType="";
/*
window.onYouTubeIframeAPIReady = function() {
  	var ik_player;
	ik_player = new YT.Player('ik_player_iframe');
	//subscribe to eventsif (player.addEventListener)
	if(ik_player.addEventListener){
	 	ik_player.addEventListener("onStateChange", "onYouTubePlayerStateChange");
	}	
}
*/	
/*
functions to be checking focus of winows
$(window).focusout(function() {
			//    alert('focusout'); 
			 console.log('out');   
			});	
$(window).focusout(function() {
	 pauseStart=$.now();
	// updateVideoWatch();  
});	
window.onfocus = function () { 
  pauseTotal=($.now()-pauseStart);
  console.log('in');
}; 
*/
/*
		onYouTubeIframeAPIReady - when youtube Api gets ready it binds play pause resume and finish events 
*/	
function onYouTubeIframeAPIReady() {
	//creates the player object
	var ik_player;
	ik_player = new YT.Player('ik_player_iframe');
	//subscribe to eventsif (player.addEventListener)
	if(ik_player.addEventListener){
	 	ik_player.addEventListener("onStateChange", "onYouTubePlayerStateChange");
	}	
	
}	
// calling update fuctions or pause based on state 										  
function onYouTubePlayerStateChange(event) {
	youTubeduration =(event.target.getDuration());
	var state = event.data;
	if(state === 2){
		// the video is pause, do something here.
		pauseStart=$.now();
		updateYouTubeWatch();
		
	}
	if(state === 1){
		// the video is play/resume, do something here.
		if(pauseCount==0)
		{	//console.log('1sttimeplaye');
			pauseCount=1;
			pauseTotal=0;
		} else{
			pauseTotal=($.now()-pauseStart);
			//alert(pauseTotal);
		}
	}
	//finish
	if(state === 0){
		// the video is end, do something here.
		updateYouTubeWatch();
	}
}
// this function is used to track timings of pdf doc ppts for which it is active.
function timerIncrement() {
	var endTime=$.now();
	idleTime = idleTime + 1;
	if (idleTime < 5) {
		var index=watchedVideo.indexOf(presentContentId);
		percent_done=0;
		if(index!= -1)
		{	percent_done=progress[index];							 				
		}	
		var	reqContentId=presentContentId;
		var req = {};
		var res;
		req.contentId= presentContentId;
		req.chapterId=chapterId;
		req.subjectId=subjectId;
		req.watchedVideo=watchedVideo;
		req.watchedPercentage=parseInt(percent_done)+parseInt(1);
		req.totalTime=((endTime-starttime)/60000).toFixed(2);
		req.action = 'set-content-watched';
		$.ajax({
				'type': 'post',
				'url': ApiEndPoint,
				'data': JSON.stringify(req)
			}).done(function (res) {
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status ==1  )
			{	
				starttime=$.now();
				percent_done=parseInt(percent_done)+parseInt(1);
				$('[data-id="'+reqContentId+'"]').children('.content-container').css('background-color',"#2ecc71");
				
			}
		});
	}
}

$(function() {

	checkCourseAcess();

	function checkCourseAcess() {
		var req = {};
		var res;
		req.action = 'checkCourseAcess';
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			//console.log(res);
			if (res.status == 0)
				alert('Not Allowed to view this chapter');
			else {
				fetchUserDetails();
				fetchBreadCrumb();
				fetchHeadings();
			}
		});
	}

	function fetchUserDetails() {
		var req = {};
		var res;
		req.action = 'get-student-details-for-changePassword';
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			//console.log(res);
			if (res.status == 0)
				alert(res.message);
			else {
				details = res;
				fillUserDetails(res);
			}
		});
	}

	//function to fill user details
	function fillUserDetails(data) {
		$('.notify.badge').text(data.notiCount);
		$('.fullName').text(data.details.firstName + ' ' + data.details.lastName);
		$('.username').text(data.details.username);
		$('.user-image').attr('src', data.details.profilePic);
		$('#btn_rupee, #btn_dollar').removeClass('btn-white btn-info');
		if (data.details.currency == 1) {
			$('#btn_dollar').addClass('btn-info');
			$('#btn_rupee').addClass('btn-white');
		}
		else if (data.details.currency == 2) {
			$('#btn_dollar').addClass('btn-white');
			$('#btn_rupee').addClass('btn-info');
		}
	}

	//event handler for next button 
	//based on content type corresponding update is called
	$('#next').on('click', function() {

		$('.content-view[data-id="'+$(this).attr('data-id')+'"]').click();

	});
	
	//event handler for previous button
	$('#previous').on('click', function() {

		$('.content-view[data-id="'+$(this).attr('data-id')+'"]').click();

	});

	//function to fetch and fill breadcrumbs
	function fetchBreadCrumb() {
		var req = {};
		var res;
		req.action = 'get-breadcrumb-for-chapter';
		req.subjectId = getUrlParameter('subjectId');
		req.courseId = getUrlParameter('courseId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				$('#breadcrumb .btc:eq(0)').html('<a href="MyExam.php?courseId='+courseId+'" class="custom-btc-link">'+res.breadcrumb.courseName+'</a>');
				$('#breadcrumb .btc:eq(1)').html('<a href="chapter.php?courseId='+courseId+'&subjectId='+subjectId+'" class="custom-btc-link">'+res.breadcrumb.subjectName+'</a>');
				$('#breadcrumb .btc:eq(2)').html('<strong>Content</strong>');
				if(getUrlParameter('demo') == '1')
					$('#backSubject').attr('href', '../courseDetails.php?courseId=' + courseId).text('Back');
				else
					$('#backSubject').attr('href', 'chapter.php?courseId=' + courseId + '&subjectId=' + subjectId);
			}
		});
	}

	//function to fetch headings of the chapter
	function fetchHeadings() {
		var req = {};
		var res;
		req.action = 'get-headings';
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		req.demo = getUrlParameter('demo');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status == 0)
				alert(res.message);
			else
				fillHeadings(res);
		});
	}

	//function to fill headings and generate side view
	function fillHeadings(data) {
		//console.log(data);
		var html = '';
		var chapterNum = parseInt(data.chapterNum);
		for(var i = 0; i < data.headings.length; i++) {
			html += '<div class="panel panel-default custom-panel">'
					+ '<div class="panel-heading">'
						+ '<h3 class="custom-panel-title">'
							//+ '<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse' + i + '" data-heading="' + data.headings[i].id + '"><strong>Heading ' + (chapterNum + 1) + '.' + (i+1) + ':</strong> ' + data.headings[i].title+'</a>'
							+ '<span data-heading="' + data.headings[i].id + '"><strong>Section ' + (i+1) + ':</strong> ' + data.headings[i].title+'</span>'
						+ '</h3>'
					+ '</div>'
					+ '<div id="collapse'+i+'" class="panel-collapse collapse in" style="height: auto;">'
						+ '<div class="custom-panel-body">'
							+ '<div class="timeline-messages">';
			//contentIds.push('0');
			for(var j = 0; j < data.headings[i].content.length; j++) {
				var watched=data.headings[i].content[j].watched;
				var progressPer=data.headings[i].content[j].progress;				
				if(watched==null )
				watched=0;
				contentIds.push(data.headings[i].content[j].id);
				if(watched >0 && 
					(
						(
							data.headings[i].content[j].contentType == "doc"
							|| data.headings[i].content[j].contentType == "ppt"
							|| data.headings[i].content[j].contentType == "pdf"
							|| data.headings[i].content[j].contentType == "dwn"
						) || (
							data.headings[i].content[j].contentType == "text"
							|| data.headings[i].content[j].contentType == "video"
							|| data.headings[i].content[j].contentType == "Youtube"
							|| data.headings[i].content[j].contentType == "Vimeo"
							&& progressPer >CHECKVIDEO
						)
					)
				)
				{
					watchedVideo.push(data.headings[i].content[j].id);
					progress.push(data.headings[i].content[j].progress);
					//views.push(NoOfViews);
					html += '<a data-id="' + data.headings[i].content[j].id + '" class="content-view"><div class="content-container"  style="background-color:#2ecc71;">';
				}else{
					//console.log(progressPer+' '+CHECKVIDEO);
					if(watched >0)
					{
						watchedVideo.push(data.headings[i].content[j].id);
						progress.push(data.headings[i].content[j].progress);
					}
					html += '<a data-id="' + data.headings[i].content[j].id + '" class="content-view"><div class="content-container">';	
			
				}

				if(data.headings[i].content[j].contentType == "doc")
					html += '<img class="avatar" src="img/pdf.jpg" alt="">';
				else if(data.headings[i].content[j].contentType == "ppt")
					html += '<img class="avatar" src="img/pdf.png" alt="">';
				else if(data.headings[i].content[j].contentType == "text")
					html += '<img class="avatar" src="img/lec.png" alt="">';
				else if(data.headings[i].content[j].contentType == "video")
					html += '<img class="avatar" src="img/vid.jpg" alt="">';
				else if(data.headings[i].content[j].contentType == "dwn")
					html += '<img class="avatar" src="img/download.png" alt="">';
				else if(data.headings[i].content[j].contentType == "Youtube")
					html += '<img class="avatar" src="img/youtube.png" alt="">';
				else if(data.headings[i].content[j].contentType == "Vimeo")
					html += '<img class="avatar" src="img/vimeo.png" alt="">';
					html += '<div class="content-text"><span class="init">Lecture ' + (i+1) + '.' + (j+1) + ' </span>: <span class="cont">' + data.headings[i].content[j].title + '</span>';
					
				if(data.headings[i].content[j].downloadable == 1)
					html += '<br><a data-contentId="'+data.headings[i].content[j].id+'" href="#" class="contentDownload btn btn-success btn-xs btn-custom">Download</a>';
				/*else
					html += '</p>';*/
				html +=		'</div>'
						+ '</div></a>';
			}
			html +=		'</div>'
					+ '</div>'
				+ '</div>'
			+ '</div>';
		}
		$('#accordion').append(html);
		
		//event listener for content views
		$('.content-view').on('click', function() {
			
			//console.log(StuffType);
			if(StuffType == 6){
				updateVideoWatch();
			}
			else if(StuffType == 3)
			{
				updateYouTubeWatch();
			}
			else if(StuffType == 4)
			{
				updateVimeoWatch();
			}
			else if(StuffType == 5)
			{
				updateDoc_watch();
			}
			else if(StuffType == 1)
			{
				updateTxt_DwnWatch();
			}
			else{
				//no need to update
			}

			$('#contentHeading .init').html($(this).find('.init').text());
			$('#contentHeading .cont').html($(this).find('.cont').text());
			$('#contentHeading').attr('data-id', $(this).attr('data-id'));
			var req = {};
			var res;
			req.action = 'get-content-stuff';
			req.contentId = $(this).attr('data-id');
			$.ajax({
					'type'	:	'post',
					'url'	:	ApiEndPoint,
					'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				console.log(res);
				if(res.status == 0)
					alert(res.message);
				else
					fillContentDetails(res);
			});
		});
		//clicking on chapters content
		if($('[data-heading="' + getUrlParameter('chapterId') + '"]').parents('div.panel').find('.content-view').length != 0) 
			$('[data-heading="' + getUrlParameter('chapterId') + '"]').parents('div.panel').find('.content-view:eq(0)').click();
		else
			$('.content-view:eq(0)').click();
		$('.contentDownload').on('click', function() {
			var contentId = $(this).attr('data-contentId');
			//send a request to server to fetch the link and generate signature also check if the file is really downloadable.
			var req = {};
			var res;
			req.action = 'download-content';
			req.contentId = contentId;
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				window.open(res.link.contentLink, '_blank');
				//var html = '<iframe src="downloader.php?filename='+res.link.contentLink+'" heigth="0" width="0" style="display: none;visibility: hidden; border: 0px;"></iframe>';
				//$('body').append(html);
			});
		});
	}

	//function to fill content details
	function fillContentDetails(data) {
		//console.log(data);
		starttime=$.now();	
		var i=0;
		$('.description').hide();
		//filling next button
		$('#next').show();
		if(contentIds[contentIds.indexOf($('#contentHeading').attr('data-id'))+1] != undefined) {
			var index = $('#contentHeading').attr('data-id');
			/*if(index == 0) {
				console.log('aaaaa');
			}
			else {*/
				$('#next').attr('data-id', contentIds[contentIds.indexOf(index)+1]);
				/*if(contentIds[contentIds.indexOf($('#contentHeading').attr('data-id'))+1] == 0) {
					var headingId = $('.accordion-toggle:eq(' + (contentIds.indexOf($('#contentHeading').attr('data-id')) - 1) + ')').attr('data-heading');
					$('#next').attr('data-heading', headingId);
				}
			}*/
		}
		else
			$('#next').hide();
			
		//filling previous button
		$('#previous').show();
		if(contentIds[contentIds.indexOf($('#contentHeading').attr('data-id'))-1] != undefined)
			$('#previous').attr('data-id', contentIds[contentIds.indexOf($('#contentHeading').attr('data-id'))-1]);
		else
			$('#previous').hide();
		if(data.content.description != '') {
			//if description is more than 226 characters hide the remaining
			if(data.content.description.length > 226) {
				var first = data.content.description.substring(0, 226);
				var second = '<span id="moreDesc" style="display: none;">' + data.content.description.substring(226, data.content.description.length) + '</span>';
				var html = first + second + '<a data-hidden="0" href="#" id="viewMoreDesc">... View More</a>';
				$('#desc').html(html).parents('.description').show();
				$('#viewMoreDesc').off('click');
				$('#viewMoreDesc').on('click', function(e) {
					e.preventDefault();
					if($(this).attr('data-hidden') == 0) {
						$(this).text(' Hide');
						$(this).attr('data-hidden', "1")
						$('#moreDesc').slideDown();
					}
					else {
						$(this).text('... View More');
						$(this).attr('data-hidden', "0")
						$('#moreDesc').slideUp();
					}
				});
			}
			else
				$('#desc').html(data.content.description).parents('.description').show();
		}
		else
			$('#desc').html('No description found.');
		if(data.supplementary.length > 0) {
			var html = '';
			for(i = 0; i < data.supplementary.length; i++) {
				html += '<a href="' + data.supplementary[i].signedURL + '" target="_blank"><img src="../admin/img/att.png" style="width: 32px;height: 32px;"><br>' + data.supplementary[i].fileRealName + '</a><br>';
			}
			$('#supp').html(html).parents('.description').show();
		}
		else
			$('#supp').html('No supplementary materials found.');
		$('#block-everything').hide();

		contentType = data.content.type;
		
		if(data.content.type == "text") {
			StuffType=1;
			starttime=$.now();	
			pauseTotal=0;
			presentContentId=(data.content.contentId);
			watchnumber=0;
			var html = '<div id="ckeditor">' + data.content.stuff + '</div>';
			$('#content-view .custom-content-part').html(html);
			$('#block-everything').show();
			CKEDITOR.inline('ckeditor', {toolbar: [
								{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ]},
								'/',
								{ name: 'insert', items: [ 'Image', 'Mathjax' ]},
								{ name: 'styles', items: [ 'FontSize' ] },
								{ name: 'colors', items: [ 'TextColor' ] }]
							});
								
								
								
			percent_done=100;			
			var contentCheck=document.getElementById("ckeditor");
			if(contentCheck!=null || contentCheck!='' )
			{
				updateTxt_DwnWatch();
										
			}
		}
		else if(data.content.type == "dwn") {
			StuffType=2;
			window.open(data.content.stuff, '_blank');
			percent_done=100;
			starttime		=$.now();	
			pauseTotal=0;
			watchnumber=0;
			updateTxt_DwnWatch();
			/*var html = '<iframe src="downloader.php?filename='+data.content.stuff+'" heigth="0" width="0" style="display: none;visibility: hidden; border: 0px;"></iframe>Your file will be downloaded soon.';
			$('#content-view .custom-content-part').html(html);*/
		}
		else if(data.content.type == "Youtube") {
			//console.log('youtube');
			StuffType		 = 3;
			presentContentId = (data.content.contentId);
			var index		 = watchedVideo.indexOf(presentContentId);
			percent_done	 = 0;
			pauseStart		 = 0;
			starttime		=$.now();	
			pauseTotal=0;
			pauseCount=0;
			idleTime = 0;
			idleTimePoint=$.now();
			watchnumber=0;
			//console.log(pauseTotal+'pauseTotal1');
			if(index!= -1)
			{
				percent_done=progress[index];
			}
			$(this).mousemove(function () {
				idleTime = 0;
				idleTimePoint=$.now();
			});
			$(this).click(function () {
				idleTime = 0;
				idleTimePoint=$.now();
			});
			$(this).keypress(function (e) {
				idleTime = 0;
				idleTimePoint=$.now();
			});
			//console.log(percent_done);
			var videoId = getStringParameter('v', data.content.stuff);
			var html 	= '<iframe id="ik_player_iframe" frameborder="0"  src="http://www.youtube.com/embed/' +
							videoId +'?enablejsapi=1&version=3" width="100%" height="420"></iframe>';
			$('#content-view .custom-content-part').html(html);		
			var tag = document.createElement('script');
			tag.src = "https://www.youtube.com/player_api";
			var firstScriptTag = document.getElementsByTagName('script')[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
			//	onYouTubeIframeAPIReady();			
			onYouTubeIframeAPIReady();		
			console.log(percent_done);
		}
		else if(data.content.type == "Vimeo") {
			StuffType=4;
			presentContentId=(data.content.contentId);
			var videoId = getVimeoId(data.content.stuff);
			/*var html = '<object width="100%" height="420" id="vimeoFlash">'
					+ '<param name="allowfullscreen" value="true" />'
					+ '<param name="allowscriptaccess" value="always" />'
					+'<param name="flashvars" value="api=1" />'
					+ '<param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id='+videoId+'&amp;server=vimeo.com&amp;color=00adef&amp;fullscreen=1&amp;player_id=vimeoFlash" />'
					+ '<embed src="http://vimeo.com/moogaloop.swf?clip_id='+videoId+'&amp;server=vimeo.com&amp;color=00adef&amp;fullscreen=1&amp;player_id=vimeoFlash" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="880" height="400" ></embed>'
				+ '</object>';*/
				
			var html= '<iframe id="player1" src="https://player.vimeo.com/video/'+videoId+'?api=1&player_id=player1" id="player1" width="100%" height="400" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
			$('#content-view .custom-content-part').html(html);
			var index=watchedVideo.indexOf(presentContentId);
			percent_done=0;
			starttime=$.now();	
			pauseTotal=0;
			pauseCount=0;
			watchnumber=0;
			if(index!= -1)
			{	 percent_done=progress[index];	
				
			}	
			var iframe = $('#player1')[0];
			var player = $f(iframe);
			var status = $('.status');
			// When the player is ready, add listeners for pause, finish, and playProgressupdateVimeoWatch
			
			player.addEvent('ready', function() {
				player.addEvent('finish', onFinish);
				player.addEvent('pause', onPause);
				player.addEvent('playProgress', onPlayProgress);
			});
			function onFinish(id) {
				updateVimeoWatch();
			}
			function onPause(id) {
				updateVimeoWatch();
				if(pauseCount==0)
				{	//console.log('1sttimeplaye');
					pauseCount=1;
					pauseTotal=0;
				} else{
					pauseTotal=($.now()-pauseStart);
					//alert(pauseTotal);
				}
			}
			function onPlayProgress(data, id) {	
				vimeoPercent_done=data.percent*100; 			 
			}
		}
		else if(data.content.type == "ppt" || data.content.type == "doc" || data.content.type == "pdf" ) {
			noOftime			=	noOftime+1;
			watchnumber			=	0;
			StuffType			=	5;
			var idleInterval20 	= 	setInterval("timerIncrement()", 10000);
			setTimeout(function () { clearInterval(idleInterval20); }, 30000);
			var idleInterval 	= 	setInterval("timerIncrement()", 60000);
			//var idleInterval 	= 	setInterval("timerIncrement()", 60000);
			presentContentId	=	(data.content.contentId);
			//	var html = '<object data="' + data.content.stuff + '" type="application/pdf" height="400" width="880">'
			//				+ '<embed src="' + data.content.stuff + '" type="application/pdf">&nbsp; </embed>'
			//				+ '</object>';
			var html='<iframe id="viewer" src = "assets/ViewerJS/#'+data.content.stuff+'" width="100%" height="420"  allowfullscreen webkitallowfullscreen ></iframe>';
			//var html='<iframe id="viewer" src = "assets/ViewerJS/#jd.pdf" width="100%" height="420"  allowfullscreen webkitallowfullscreen ></iframe>';
			$('#content-view .custom-content-part').html(html);
			//percent_done=100;
			$(this).mousemove(function () {
				idleTime = 0;
			});
			$(this).click(function () {
				idleTime = 0;
			});
			$(this).keypress(function (e) {
				idleTime = 0;
			});
			$("#viewer").on("load", function () {
				var index=watchedVideo.indexOf(presentContentId);
				percent_done=0;
				//NoOfViews=0;
				if(index!= -1)
				{	 percent_done=progress[index];	
					// NoOfViews=views[index]+1;				
				}	
				var	reqContentId=presentContentId;
				var req = {};
				var res;
				var endTime=$.now();
				req.contentId= presentContentId;
				req.chapterId=chapterId;
				req.subjectId=subjectId;
				req.watchedVideo=watchedVideo;
				req.watchedPercentage=percent_done;
				req.totalTime=((endTime-starttime)/60000).toFixed(2);
				req.NoOfViews1=1;
				req.action = 'set-content-watched';
				$.ajax({
					'type': 'post',
					'url': ApiEndPoint,
					'data': JSON.stringify(req)
				}).done(function (res) {
					//console.log(res);
					res = $.parseJSON(res);
					//console.log(res);
					if(res.status ==1  )
					{	
						$('[data-id="'+reqContentId+'"]').children('.content-container').css('background-color',"#2ecc71")
					}
				});
			});
		}
		else if(data.content.type == "video") {
			//console.log('video');
			StuffType=6;
			pauseStart=0;
			starttime=$.now();	
			pauseTotal=0;
			pauseCount=0;
			watchnumber=0;
			idleTimePoint=$.now();
			presentContentId=(data.content.contentId);
			var html = '<div class="flowplayer color-light no-background" id="player" style="width: 100%;height: 400px;">'
						+ '<video autoplay>'
						//+ '<source type="video/mp4" src="kambakt.mp4" /></video>' //for local testing
						+ '<source type="video/mp4" src="'+data.content.stuff+'"></video>'
					+ '</div>';
			$('#content-view .custom-content-part').html(html);
			var index=watchedVideo.indexOf(presentContentId);
			percent_done=0;
			//NoOfViews=0;
			if(index!= -1)
			{
				percent_done=progress[index];
				// NoOfViews=views[index]+1;
			}
			$(this).mousemove(function () {
				idleTime = 0;
				idleTimePoint=$.now();
			});
			$(this).click(function () {
				idleTime = 0;
				idleTimePoint=$.now();
			});
			$(this).keypress(function (e) {
				idleTime = 0;
				idleTimePoint=$.now();
			});
			
			$('.flowplayer').flowplayer().on('pause', function(){
				pauseStart=$.now();
				//alert('hi'+idleTimePoint);
				updateVideoWatch();
			});
			$('.flowplayer').flowplayer().on('resume', function(){
				//console.log('pauseStart'+pauseStart);
				if(pauseCount==0 )
				{
					pauseCount=1;
					pauseTotal=0;
				}
				else{
					if(pauseStart!=0)
					{
						pauseTotal=($.now()-pauseStart);
					}
					
				}
			});
			$('.flowplayer').flowplayer().on('finish', function() {
				updateVideoWatch();
				if($('#next').css('display') != 'none')
					$('#next').click();
			});
		}
	}

	// video update content 
	function updateVideoWatch() {
		var endTime=$.now();
		//console.log((endTime-idleTimePoint))
		/*
		this condition is used to check the if student just played the video and active in page or just played the video
		*/
		if((endTime-idleTimePoint)/60000<5)
		{
			//alert((endTime-idleTimePoint)/6000);
			var	reqContentId=presentContentId;
			percent_done=Math.abs(percent_done);
			var reqpercent_done=percent_done;
			var watchedVideoTime=(endTime-starttime-pauseTotal)/1000;
			//console.log(endTime+'---'+starttime+'--'+pauseTotal);
			var total_time=flowplayer($("#player")).video.duration;
			//console.log(percent_done+'---'+watchedVideoTime+'---'+total_time+'---'+(watchedVideoTime / total_time)+'-----'+parseInt(Math.ceil((watchedVideoTime / total_time) * 100)));
			percent_done = Math.abs(parseFloat(percent_done)+parseFloat(((watchedVideoTime / total_time) * 100)));
			//console.log(percent_done);
			if(percent_done>100)
				percent_done=100;			
			//updating the if user had watched the video
			//if(percent_done > 10){
				reqpercent_done=percent_done;
				var req = {};
				var res;
				req.contentId= presentContentId;
				req.chapterId=chapterId;
				req.subjectId=subjectId;
				req.watchedVideo=watchedVideo;
				req.watchedPercentage=percent_done.toFixed(2);
				if(watchnumber==0)
				{
					watchnumber=1;
					req.NoOfViews1=1;
				}else{
					req.NoOfViews1=0;
				}
				req.totalTime=((endTime-starttime-pauseTotal)/60000).toFixed(2);
				req.action = 'set-content-watched';
				//console.log(req);
				$.ajax({
					'type': 'post',
					'url': ApiEndPoint,
					'data': JSON.stringify(req)
				}).done(function (res) {
					//console.log(res);
					res = $.parseJSON(res);
					//console.log(reqpercent_done);
					if(res.status ==1)
					{	starttime=pauseStart;
						//console.log(starttime+"starttime");
						if(reqpercent_done > CHECKVIDEO){
							$('[data-id="'+reqContentId+'"]').children('.content-container').css('background-color',"#2ecc71");
						}
						
					}
				});
				
		}
		else{
			//console.log('hi');
		}
		//}	
	}
	//vimeo updtae
	function updateVimeoWatch() {
		var	reqContentId=presentContentId;	
		var reqpercent_done=Math.abs(percent_done);
		var endTime=$.now();
		var watchedVideoTime=(endTime-starttime-pauseTotal)/1000;
		//var total_time=flowplayer($("#player")).video.duration;
		//percent_done = percent_done+Math.floor((watchedVideoTime / total_time) * 100);
		percent_done = Math.abs(parseFloat(percent_done)+parseFloat(vimeoPercent_done));
		if(percent_done>100)
			percent_done=100;
		reqpercent_done=percent_done;
		var req = {};
		
		var res;
		req.contentId= presentContentId;
		req.chapterId=chapterId;
		req.subjectId=subjectId;
		req.watchedVideo=watchedVideo;
		req.watchedPercentage=percent_done.toFixed(2);
		if(watchnumber==0)
		{
			watchnumber=1;
			req.NoOfViews1=1;
		}else{
			req.NoOfViews1=0;
		}
		req.totalTime=((endTime-starttime)/60000).toFixed(2);
		//console.log(req);
		req.action = 'set-content-watched';
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
		//console.log(res);
			res = $.parseJSON(res);
			//console.log(reqpercent_done);
			if(res.status ==1)
			{	starttime=$.now();
				if(reqpercent_done > CHECKVIDEO){
					$('[data-id="'+reqContentId+'"]').children('.content-container').css('background-color',"#2ecc71");
				}
				
			}
		});
		//}
	}

	function updateTxt_DwnWatch() {
		var endTime=$.now();
		var	reqContentId=presentContentId;
		var req = {};
		var res;
		req.contentId= presentContentId;
		req.chapterId=chapterId;
		req.subjectId=subjectId;
		req.watchedVideo=watchedVideo;
		req.watchedPercentage=Math.abs(parseInt(percent_done));
		req.NoOfViews1=1;
		req.totalTime=((endTime-starttime)/60000).toFixed(2);
		req.action = 'set-content-watched';
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			//console.log(res);
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status ==1  )
			{	
				$('[data-id="'+reqContentId+'"]').children('.content-container').css('background-color',"#2ecc71")
			}
		});
	}

	function updateDoc_watch(){
		var index=watchedVideo.indexOf(presentContentId);
		var endTime=$.now();
		percent_done=0;
		//NoOfViews=0;
		if(index!= -1)
		{
			percent_done=progress[index];
			// NoOfViews=views[index]+1;
		}
		var	reqContentId=presentContentId;
		var req = {};
		var res;
		req.contentId= presentContentId;
		req.chapterId=chapterId;
		req.subjectId=subjectId;
		req.watchedVideo=watchedVideo;
		req.watchedPercentage=percent_done;
		req.NoOfViews1=1;
		req.totalTime=((endTime-starttime)/60000).toFixed(2);
		req.action = 'set-content-watched';
		//console.log(req);
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			//console.log(res);
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status ==1  )
			{	
				$('[data-id="'+reqContentId+'"]').children('.content-container').css('background-color',"#2ecc71")
			}
		});
	}

	//function to fetch data from url string
	function getStringParameter(sParam, url) {
		var sPageURL = url.split('?')[1];
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++) 
		{
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam) 
			{
				return sParameterName[1];
			}
		}
	}

	//function to fetch video id from vimeo link
	function getVimeoId(url) {
		var sURLVariables = url.split('/');
		return sURLVariables[sURLVariables.length - 1];
	}
});
/*
	function to update the youtube content after checking if the video is played actively and idle for less then 5 mins
*/
function updateYouTubeWatch()
{	
	//console.log("video api call");
	//alert(percent_done);
	var endTime=$.now();
	//alert('9999--'+idleTimePoint);
	var partialTime = ((endTime-idleTimePoint)/60000).toFixed(2);
	console.log(partialTime);
	if(partialTime<5){
		
		//console.log('youtube API function called');
		//console.log(pauseTotal+'pauseTotal2')
		var	reqContentId=presentContentId;	
		var reqpercent_done=Math.abs(percent_done);
		var endTime=$.now();
		var watchedVideoTime=(endTime-starttime-pauseTotal)/1000;
		//console.log(starttime+'#widjw#'+endTime+'widjw'+pauseTotal);
		//var player =document.getElementById("youtubeFlash");
		var total_time=youTubeduration;
		//console.log(total_time);
		//console.log(total_time+"dd"+youTubeduration);
		//console.log(percent_done+'###'+watchedVideoTime+'###'+total_time+'###'+(watchedVideoTime / total_time)+'###'+parseFloat(((watchedVideoTime / total_time) * 100)));
		percent_done = Math.abs(parseFloat(percent_done)+parseFloat((watchedVideoTime / total_time) * 100));
		//console.log(percent_done);
		if(percent_done>100)
			percent_done=100;			
		//updating the if user had watched the video
		//if(percent_done > 10){
		reqpercent_done=percent_done;
		//console.log(watchedVideoTime+"skdek"+youTubeduration+"wmw"+percent_done );
		var req = {};
		var res;
		req.contentId= presentContentId;
		req.chapterId=chapterId;
		req.subjectId=subjectId;
		req.watchedVideo=watchedVideo;
		req.watchedPercentage=percent_done.toFixed(2);
		if(watchnumber==0)
		{
			watchnumber=1;
			req.NoOfViews1=1;
		}else{
			req.NoOfViews1=0;
		}
		req.totalTime=((endTime-starttime-pauseTotal)/60000).toFixed(2);
		if (req.totalTime>20000) {
			alert("endTime : "+endTime+"; starttime : "+starttime+"; pauseTotal : "+pauseTotal);
		} else {
			req.action = 'set-content-watched';
			console.log(req);
			$.ajax({
				'type': 'post',
				'url': ApiEndPoint,
				'data': JSON.stringify(req)
			}).done(function (res) {
				console.log(res);
				res = $.parseJSON(res);
				//console.log(reqpercent_done);
				if(res.status ==1 )
				{	
				
					starttime=pauseStart;
					//console.log(starttime);
					if(reqpercent_done > CHECKVIDEO ){
						$('[data-id="'+reqContentId+'"]').children('.content-container').css('background-color',"#2ecc71");
					}
					
				}
			});
		}
	} else {
		//console.log('hiiii');
	}
}