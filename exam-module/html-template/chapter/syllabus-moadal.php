<div aria-hidden="true" aria-labelledby="upload-image-modal" role="dialog" tabindex="-1"
	id="viewSyllabus" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h4 class="modal-title">
					 Syllabus
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<iframe id="viewerSyllabus" width="100%" height="420"  allowfullscreen webkitallowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>