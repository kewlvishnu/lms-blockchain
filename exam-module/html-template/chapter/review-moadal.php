<div id="reviewModal" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h3 class="modal-title">Your Ratings and Review</h3>
</div>
<div class="modal-body">
	<div class="login-form clearfix">
		<form role="form">
			<div class="form-group">
				<span>Choose Your Rating: </span><input type="hidden" id="rating" data-filled="fa fa-star fa-2x gold" data-empty="fa fa-star-o fa-2x gold" value="0">
				<a class="pull-right" id="removeReview" style="display: none;">Remove Rating & Review</a>
			</div>
			<div>
				<!-- <h4 style="color: rgb(255, 83, 42);">Please describe why this course is very good?</h4> -->
				<ul style="padding-left: 20px;">
					<li style="list-style: inherit;">Did the course help you achieve what you set out to learn</li>
					<li style="list-style: inherit;">How engaging and prepared was the instructor</li>
					<li style="list-style: inherit;">What was the production quality like?</li>
				</ul>
			</div>
			<div class="form-group">
				<input type="text" class="form-control" id="titleReview" placeholder="Please enter your review title here"/>
			</div>
			<div class="form-group">
				<textarea class="form-control" id="review" placeholder="Please enter your review here"></textarea>
			</div>
			<div class="pull-right">
				<input type="hidden" value="0" id="reviewId">
				<a class="btn btn-success" id="submitReview">Save</a>
				<a class="btn btn-info" data-dismiss="modal">Cancel</a>
			</div>
		</form>
	</div>
</div>
</div>
</div>
</div>