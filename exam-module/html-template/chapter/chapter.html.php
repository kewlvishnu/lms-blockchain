<section id="main-content" class="panel">
	<div id="div_exam" class="panel-body">
		<!--<h3><span id="course-name"> Course </span> / <span id="subject-name"> Subject </span></h3>-->
		<ul class="breadcrumb">
			<li><a href="MyCourse.php">My Courses</a></li>
			<li id="course-name"></li>
			<li class="subject-name" id="subject-name"></li>
		</ul>
		<div class="row">
			<div class="col-lg-6">
				<h3>
					<span class="subject-name"> </span>
				</h3>
			</div>
			<div class="col-lg-2">
				<a href="#viewSyllabus" class="btn btn-md btn-custom btn-success hide" data-toggle="modal" id="viewSyllabusButton1">View Syllabus</a>
			</div>
			<div class="col-lg-2">
				<div class="dropdown hide" id="dropdownChat">
					<button class="btn btn-md btn-custom btn-primary btn-block dropdown-toggle" type="button" id="dropdownProfessorMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						Chat with professor
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						<!-- <li><a href="#">Action</a></li>
						<li><a href="#">Another action</a></li>
						<li><a href="#">Something else here</a></li> -->
					</ul>
				</div>
			</div>
			<div class="col-lg-2">
				<a href="#reviewModal" class="btn btn-md btn-custom btn-primary btn-block hide" data-toggle="modal" id="reviewButton">Write Review</a>
			</div>
		</div>
		<p id="subjectName" class="hide">Following are the list of sections, assignment, exams of subject <span class="subject-name"> </span></p>
	</div>
</section>