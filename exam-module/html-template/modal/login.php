<div id="login-modal" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h3 class="modal-title">Please Login and start learning!</h3>
</div>
<div class="modal-body">
	<div class="login-form clearfix">
		<form role="form">
			<div class="form-group">
				<select class="form-control" id="userRole">
					<option value="1">I represent an institute</option>
					<option value="2">I am a professor</option>
					<option value="3">I am a publisher</option>
					<option value="4">I am a student</option>
				</select>
			</div>
			<div class="form-group">
				<input type="text" class="form-control" id="user" placeholder="Enter username or email"/>
				<div class="error-msg" id="userError"></div>
			</div>
			<div class="form-group">
				<input type="password" class="form-control" id="pwd" placeholder="Password">
				<div class="error-msg" id="pwdError"></div>
			</div>
			<div class="checkbox pull-right">
				<label>
					<input type="checkbox" id="remember"> Remember me on this computer
				</label>
			</div>
			<div class="pull-left">
				<div class="error-msg" id="signInError"></div>
				<a class="btn btn-success" id="signIn">Sign In</a>
			</div>
		</form>
	</div>
</div>
<div class="modal-footer">
	<div class="pull-left">
		<a href="#forget-password-modal" data-toggle="modal" data-dismiss="modal">Forgot Password?</a>
	</div>
	<div  class="pull-right">
		<span>Don't have an account?</span>
		<a href="signup-student.php#student" class="btn btn-primary" style="margin-top: -7px;">Sign Up</a>
	</div>
</div>
</div>
</div>
</div>