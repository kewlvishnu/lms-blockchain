<div id="forget-password-modal" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
	<button type="button" class="close"  data-dismiss="modal">&times;</button>
	<h3 class="modal-title">Forgot Password?</h3>
</div>
<div class="modal-body">
	<div class="login-form clearfix">
		<form role="form">
			<div class="form-group">
				<select class="form-control" id="fuserRole">
					<option value="1">I represent an institute</option>
					<option value="2">I am a professor</option>
					<option value="3">I am a publisher</option>
					<option value="4">I am a student</option>
				</select>
			</div>
			<div class="form-group">
				<input type="text" class="form-control" id="fuser" placeholder="Enter Email or Username">
				<div class="error-msg" id="fuserError"></div>
			</div>
			<div class="pull-left">
				<div class="error-msg" id="forgetError"></div>
				<a class="btn btn-success" id="resetPassword">Reset</a>
			</div>
		</form>
	</div>
</div>
<div class="modal-footer">
	<div  class="pull-right">
		<span>Don't have an account?</span>
		<a href="signup-student.php#student" class="btn btn-primary">Sign Up</a>
	</div>
</div>
</div>
</div>
</div>