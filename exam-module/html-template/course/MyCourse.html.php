<section  id ='main-content'>
	<article class="panel panel-body">
		<h3>My Courses</h3>
		<p>Following are the list of courses in which you are enrolled</p>
		<hr style="color: #eeeeee;" />
		<div id="div-student-course" class="row"></div>
	</article>
	<article class="panel panel-body">
		<h3>Subscribed Courses</h3>
		<p>Following are the list of courses in which you are Subscribed</p>
		<hr style="color: #eeeeee;" />
		<div id="div-student-subscibed-course" class="row"></div>
	</article>
</section>