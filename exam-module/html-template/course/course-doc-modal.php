<div aria-hidden="true" aria-labelledby="upload-image-modal" role="dialog" tabindex="-1"
	id="viewCourseDoc" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h4 class="modal-title">
					 Course Document
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<iframe id="viewerCourseDocument" width="100%" height="420"  allowfullscreen webkitallowfullscreen ></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>