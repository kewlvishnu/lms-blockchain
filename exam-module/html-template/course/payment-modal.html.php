<div id="payment" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
	<button type="button" class="close"  data-dismiss="modal">&times;</button>
	<h3 class="modal-title">Your Access Period has Expired.</h3>
</div>
<div class="modal-body">
	<div class="row">
		<span class="message">Your access to this course has expired. Please renew your subscription by selecting any one payment method.</span>
		<div class="col-lg-12 text-center">
			<a class="btn btn-md btn-info" href="#" id="payYourself">Pay yourself</a>
			<a class="btn btn-md btn-info" id="showContact">Contact Institute/Professor for key access.</a>
		</div>
		<div class="col-lg-12 form-group" id="contactContainer" style="display: none;">
			<input type="hidden" id="instituteId" value="0" />
			<input type="hidden" id="courseId" value="0" />
			<textarea id="contact" class="form-control" rows="8">Type your message here.</textarea>
			<br>
			<button class="btn btn-md btn-info pull-right" id="sendContact">Send</button>
		</div>
	</div>
</div>
</div>
</div>
</div>