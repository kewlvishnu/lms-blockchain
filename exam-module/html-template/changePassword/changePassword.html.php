<section id="main-content">
	<section>
		<div class="panel panel-primary">
			<div class="panel-heading"> Set New Password </div>
			<div class="panel-body">
				<form role="form" class="form-horizontal">
					<div class="form-group">
						<label class="col-lg-2 control-label">Current Password</label>
						<div class="col-lg-6">
							<input id='oldPwd' type="password" placeholder=" " id="c-pwd" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">New Password</label>
						<div class="col-lg-6">
							<input id='newPwd' type="password" placeholder=" " id="n-pwd" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Re-type New Password</label>
						<div class="col-lg-6">
							<input id='repeatPwd' type="password" placeholder=" " id="rt-pwd" class="form-control">
						</div>
					</div>
					<div class="col-lg-offset-2 col-lg-10">
						<a id="changePwd" class="btn btn-info" type="button" >Save</a>
						<a  href='profile.php' class="btn btn-default" type="button">Cancel</a>
					</div>
	
				</form>
			</div>
		</div>
	</section>
	<section>
		<div class="panel panel-primary">
			<div class="panel-heading"> Change Currency</div>
			<div class="panel-body">
				<form role="form" class="form-horizontal">
				<div class="col-lg-offset-2 col-lg-10">
						<a currency='1' id='btn_dollar' class="btn btn-space changeCurrency" >
							<i class="fa fa-dollar"></i>
						</a>
						<a currency='2' id='btn_rupee' class="btn btn-space changeCurrency " >
							<i class="fa fa-rupee"></i>
						</a>
	
					</div>
				</form>
			</div>
	
		</div>
	
	</section>
</section>