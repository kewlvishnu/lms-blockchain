    <!--[footer start]-->
	<footer class="midnight-blue" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    &copy; 2014 <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">Arcanemind</a>. All Rights Reserved.
               
                </div>
                <div class="col-sm-8">
                    <ul class="pull-right">
                        <li><a href="<?php echo $sitepathMarket; ?>">Home</a></li>
                        <li><a href="<?php echo $sitepathAbout; ?>">About Us</a></li>
                        <li><a href="<?php echo $sitepathAbout; ?>">FAQ</a></li>
                        <li><a href="<?php echo $sitepathContact; ?>">Contact Us</a></li>
            						<li><a href="<?php echo $sitepathPolicyTerms; ?>">Terms of Use</a></li>
            						<li><a href="<?php echo $sitepathPolicyPrivacy; ?>">Privacy Policy</a></li>
            						<li><a href="<?php echo $sitepathMarket; ?>signup">Become a Member</a></li>
            						<li><a href="#login-modal" data-toggle="modal">Login</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>  
<!--[footer End]-->	
    <!--[if lt IE 9]-->
    <script src="assets/es5-shim/es5-shim.js"></script>
    <script src="assets/json3/lib/json3.min.js"></script>
    <!--[endif]-->                                                                                    
    <!-- build:js scripts/vendor.js -->                                                             
    <!-- bower:js -->                                                                               
	<!-- endbower -->                                                                           
	<script src="assets/jquery/dist/jquery.js"></script>
	<script src="assets/bootstrap/dist/js/bootstrap.js"></script>
	<!-- endbuild -->                                                                               

		<!--common script for all pages-->
		<!-- <script src="vendor-scripts/common-scripts.js"></script> -->
		<script src="vendor-scripts/jquery.easing.min.js"></script>

		
		
		<!--sunny codes-->
		<script src="assets/jquery-multi-select/js/jquery.multi-select.js"></script>
		<script src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="scripts/login.js"></script>
		
		
		
		<script type="text/javascript" src="assets/bxslider/jquery.bxslider.js"></script>


		
		<script src="js/jquery.js"></script>
		<script src="js/jquery-1.8.3.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/hover-dropdown.js"></script>
		<script defer src="js/jquery.flexslider.js"></script>

		<script type="text/javascript" src="js/jquery.parallax-1.1.3.js"></script>

		<script src="js/jquery.easing.min.js"></script>
		<script src="js/link-hover.js"></script>

		<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>

		<script type="text/javascript" src="assets/revolution_slider/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
		<script type="text/javascript" src="assets/revolution_slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!--common script for all pages-->

		<script src="js/revulation-slide.js"></script>
  <script>

      RevSlide.initRevolutionSlider();

      $(window).load(function() {
          $('[data-zlname = reverse-effect]').mateHover({
              position: 'y-reverse',
              overlayStyle: 'rolling',
              overlayBg: '#fff',
              overlayOpacity: 0.7,
              overlayEasing: 'easeOutCirc',
              rollingPosition: 'top',
              popupEasing: 'easeOutBack',
              popup2Easing: 'easeOutBack'
          });
      });

      $(window).load(function() {
          $('.flexslider').flexslider({
              animation: "slide",
              start: function(slider) {
                  $('body').removeClass('loading');
              }
          });
      });

      //    fancybox
      jQuery(".fancybox").fancybox();



  </script>

