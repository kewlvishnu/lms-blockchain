<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <html lang="en">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
		<title>Arcanemind</title>
    <link rel="shortcut icon" href="img/favicon.ico">
		<!-- Place bootstrap theme and reset css in bower_components manually -->
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!-- build:css styles/vendor.css -->
    <!-- bower:css -->
			<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css" />
			<link rel="stylesheet" href="bower_components/bootstrap/dist/css/theme.css" />
			<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.css" />
			<link rel="stylesheet" href="assets/jquery-multi-select/css/multi-select.css" />
			<link rel="stylesheet" href="assets/bootstrap-datepicker/css/datepicker.css" />
		<!-- endbower -->
    <!-- endbuild -->
    
		<!-- build:css({.tmp,app}) styles/main.css -->
			<link rel="stylesheet" href="styles/style-responsive.css"/>
			<link rel="stylesheet" href="styles/custom.css"/>
			
			<!--sunny codes start-->
			<link rel="stylesheet" href="css/flexslider.css"/>
			<link href="assets/bxslider/jquery.bxslider.css" rel="stylesheet" />
			<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
			<link rel="stylesheet" href="assets/revolution_slider/css/rs-style.css" media="screen">
			<link rel="stylesheet" href="assets/revolution_slider/rs-plugin/css/settings.css" media="screen">
			<link rel="stylesheet" href="assets/addresspicker/demos/themes/base/jquery.ui.all.css">			
			<link rel="stylesheet" href="styles/style.css" />
			<link rel="stylesheet" href="styles/main.css" />
			<link rel="stylesheet" href="styles/responsive.css" />
			<!--sunny codes end-->
			
			<link rel="stylesheet" href="assets/addresspicker/demos/themes/base/jquery.ui.all.css">
    <!-- endbuild -->
  </head>
  <body>
    <!--[if lt IE 7]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
	
		<!--header start-->
    <header class="header-frontend">
	<?php
	include 'modal/login.php';
	include 'modal/forgotpassword.php';
	include 'modal/map-view.php';
	?>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="fa fa-bars"></span>
                    </button>
                    <a class="navbar-brand" href=".">
                        <img src="img/arcanemind-logo.png" alt="Arcanemind" />
										</a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li><a href="signup-student.php#student">Register</a></li>
						<li><a href="promotion.php">For Institute/ Professor/ Publisher</a></li>
                        <li><a href="#login-modal" data-toggle="modal">Login</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!--header end-->