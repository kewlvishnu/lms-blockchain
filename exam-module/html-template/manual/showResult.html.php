<section  id ='main-content' class="panel panel-default">
	<div class="panel-body">
		<ul class="breadcrumb">
			<li><a href="MyCourse.php">My Course</a></li>
			<li><a href=""></a></li>
			<li><a href=""></a></li>
			<li><strong><a href=""></a></strong></li>
		</ul>
		<h3 id="examName">Attempt Details</h3>
		<hr>
		<?php
			if (isset($_GET['courseId']) && !empty($_GET['courseId'])) {
				echo '<a href="MyExam.php?courseId='.$_GET['courseId'].'" class="btn btn-info">Back</a>';
			}
		?>
		<div id="percentDistGraph"></div>
		<div class="row">
			<div class="col-md-6">
				<div id="scoreGraph"></div>
			</div>
			<div class="col-md-6">
				<div id="percentGraph"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div id="pieGraph"></div>
			</div>
			<div class="col-md-6">
				<div id="exam"></div>
			</div>
		</div>
		<div id="questionMarks"></div>
	</div>
</section>