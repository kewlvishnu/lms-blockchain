<section  id ='main-content' class="panel panel-default">
	<div class="panel-heading">
		<h3>Attempt Details</h3>
	</div>
	<div class="panel-body">
		<?php
			if (isset($_GET['courseId']) && !empty($_GET['courseId'])) {
				echo '<a href="MyExam.php?courseId='.$_GET['courseId'].'" class="btn btn-info">Back</a>';
			}
		?>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-12">
				<button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#attemptDetailsGraph" aria-expanded="true" aria-controls="attemptDetailsGraph">
					Attempt Details Analytics
				</button>
				<div class="collapse in" id="attemptDetailsGraph">
					<div class="well">
						<div id="graph12"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-12">
				<button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#topperGraph" aria-expanded="true" aria-controls="topperGraph">
					Topper Comparison Analytics
				</button>
				<div class="collapse in" id="topperGraph">
					<div class="well">
						<div id="hero-bar1"></div>
						<div id="hero-bar2"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-12">
				<button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#percentGraph" aria-expanded="true" aria-controls="percentGraph">
					Percentage distribution Analytics
				</button>
				<div class="collapse in" id="percentGraph">
					<div class="well">
						<div id="hero-bar3"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="nav nav-btns">
		<div class="col-sm-6 text-center">
			<a href="#" class="btn btn-warning btn-lg btn-prev" id="btnPrevAttempt"><i class="fa fa-arrow-circle-left"></i> Prev</a>
		</div>
		<div class="col-sm-6 text-center">
			<a href="#" class="btn btn-warning btn-lg btn-next" id="btnNextAttempt">Next <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<hr>
	<div class="panel-body">
		<h3>Questions analytics :</h3>
		<div id="questionsContainer"></div>
	</div>
</section>

<!-- Modal -->
<div class="modal fade" id="timeModal" tabindex="-1" role="dialog" aria-labelledby="timeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="timeModalLabel">Time spent on a question statistics</h4>
      </div>
      <div class="modal-body">
        <div id="timeGraph"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="answerModal" tabindex="-1" role="dialog" aria-labelledby="answerModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="answerModalLabel">Answer in detail</h4>
      </div>
      <div class="modal-body">
        <div id="showAnswerDetail"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- <div id="hidden-time-graph">
	<div id="time-container">
		<span class="pull-right" style="margin-right: -27px;font-size: 1.8em;margin-top: -35px;" id="timeClose"><i class="fa fa-times-circle-o text-danger"></i></span>
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						Time spent on a question statistics
					</header>
					<div class="panel-body to-be-added">
					</div>
				</section>
			</div>
		</div>
	</div>
</div> -->