<div class="row" id="main-content">
	<aside class="profile-info col-lg-12">
		<section class="panel" id="details">
			<div class="bio-graph-heading">
				My Profile Setting
				<a class="pull-right btn btn-danger btn-xs edit-button"><i class="fa fa-edit"></i></a>
			</div>
			<div class="panel-body bio-graph-info view-section">
				<h1>User Details</h1>
				<div class="row">
					<div class="bio-row">
						<p><span1 >Username </span1>: <span class="username"></span></p>
					</div>
					<div class="bio-row">
						<p><span1>Student ID </span1>: <span class="studentId"></span></p>
					</div>
					<div class="bio-row">
						<p><span1>First Name </span1>: <span class="firstName"></span></p>
					</div>
					<div class="bio-row">
						<p><span1>Last Name </span1>: <span class="lastName"></span></p>
					</div>
					<div class="bio-row">
						<p><span1>Country </span1>: <span class="country"></span></p>
					</div>
					<div class="bio-row">
						<p><span1>Birthday</span1>: <span class="dob"></span></p>
					</div>
					<div class="bio-row">
						<p><span1>Email </span1>: <span class="email small-txt"></span></p>
					</div>
					<div class="bio-row">
						<p><span1>Mobile </span1>: <span class="mobile"></span></p>
					</div>
					<div class="bio-row">
						<p><span1>Phone </span1>: <span class="landline"></span></p>
					</div>
				</div>
			</div>
			<div class="panel-body bio-graph-info edit-section">
				<div class="row">
					<div class="form-group">
						<div class="col-lg-6">
							<label for="firstName">First Name</label>
							<input type="text" class="form-control firstName txt-captial"/>
						</div>
						<div class="col-lg-6">
							<label for="lastName">Last Name</label>
							<input type="text" class="form-control lastName txt-captial">
						</div>
						<div class="col-lg-6">
							<label for="country">Country</label>
							<select class="form-control country">
								<option value='0'>Select Country</option>
								<option value="3">Afghanistan</option>
								<option value="4">Albania</option>
								<option value="5">Algeria</option>
								<option value="6">American Samoa</option>
								<option value="7">Andorra</option>
								<option value="8">Angola</option>
								<option value="9">Anguilla</option>
								<option value="10">Antarctica</option>
								<option value="11">Antigua and Barbuda</option>
								<option value="12">Argentina</option>
								<option value="13">Armenia</option>
								<option value="14">Aruba</option>
								<option value="15">Australia</option>
								<option value="16">Austria</option>
								<option value="17">Azerbaijan</option>
								<option value="18">Bahamas</option>
								<option value="19">Bahrain</option>
								<option value="20">Bangladesh</option>
								<option value="21">Barbados</option>
								<option value="22">Belarus</option>
								<option value="23">Belgium</option>
								<option value="24">Belize</option>
								<option value="25">Benin</option>
								<option value="26">Bermuda</option>
								<option value="27">Bhutan</option>
								<option value="28">Bolivia</option>
								<option value="29">Bosnia and Herzegovina</option>
								<option value="30">Botswana</option>
								<option value="31">Brazil</option>
								<option value="32">Brunei Darussalam</option>
								<option value="33">Bulgaria</option>
								<option value="34">Burkina Faso</option>
								<option value="35">Burundi</option>
								<option value="36">Cambodia</option>
								<option value="37">Cameroon</option>
								<option value="38">Canada</option>
								<option value="39">Cape Verde</option>
								<option value="40">Cayman Islands</option>
								<option value="41">Central African Republic</option>
								<option value="42">Chad</option>
								<option value="43">Chile</option>
								<option value="44">China</option>
								<option value="45">Christmas Island</option>
								<option value="46">Cocos (Keeling) Islands</option>
								<option value="47">Colombia</option>
								<option value="48">Comoros</option>
								<option value="49">Democratic Republic of the Congo (Kinshasa)</option>
								<option value="50">Congo, Republic of(Brazzaville)</option>
								<option value="51">Cook Islands</option>
								<option value="52">Costa Rica</option>
								<option value="53">Ivory Coast</option>
								<option value="54">Croatia</option>
								<option value="55">Cuba</option>
								<option value="56">Cyprus</option>
								<option value="57">Czech Republic</option>
								<option value="58">English Name</option>
								<option value="59">Denmark</option>
								<option value="60">Djibouti</option>
								<option value="61">Dominica</option>
								<option value="62">Dominican Republic</option>
								<option value="63">East Timor (Timor-Leste)</option>
								<option value="64">Ecuador</option>
								<option value="65">Egypt</option>
								<option value="66">El Salvador</option>
								<option value="67">Equatorial Guinea</option>
								<option value="68">Eritrea</option>
								<option value="69">Estonia</option>
								<option value="70">Ethiopia</option>
								<option value="71">Falkland Islands</option>
								<option value="72">Faroe Islands</option>
								<option value="73">Fiji</option>
								<option value="74">Finland</option>
								<option value="75">France</option>
								<option value="76">French Guiana</option>
								<option value="77">French Polynesia</option>
								<option value="78">French Southern Territories</option>
								<option value="79">Gabon</option>
								<option value="80">Gambia</option>
								<option value="81">Georgia</option>
								<option value="82">Germany</option>
								<option value="83">Ghana</option>
								<option value="84">Gibraltar</option>
								<option value="85">Great Britain</option>
								<option value="86">Greece</option>
								<option value="87">Greenland</option>
								<option value="88">Grenada</option>
								<option value="89">Guadeloupe</option>
								<option value="90">Guam</option>
								<option value="91">Guatemala</option>
								<option value="92">Guinea</option>
								<option value="93">Guinea-Bissau</option>
								<option value="94">Guyana</option>
								<option value="95">Haiti</option>
								<option value="96">Holy See</option>
								<option value="97">Honduras</option>
								<option value="98">Hong Kong</option>
								<option value="99">Hungary</option>
								<option value="100">Iceland</option>
								<option value="1">India</option>
								<option value="101">Indonesia</option>
								<option value="102">Iran (Islamic Republic of)</option>
								<option value="103">Iraq</option>
								<option value="104">Ireland</option>
								<option value="105">Israel</option>
								<option value="106">Italy</option>
								<option value="107">Jamaica</option>
								<option value="108">Japan</option>
								<option value="109">Jordan</option>
								<option value="110">Kazakhstan</option>
								<option value="111">Kenya</option>
								<option value="112">Kiribati</option>
								<option value="113">Korea, Democratic People's Rep. (North Korea)</option>
								<option value="114">Korea, Republic of (South Korea)</option>
								<option value="115">Kosovo</option>
								<option value="116">Kuwait</option>
								<option value="117">Kyrgyzstan</option>
								<option value="118">Lao, People's Democratic Republic</option>
								<option value="119">Latvia</option>
								<option value="120">Lebanon</option>
								<option value="121">Lesotho</option>
								<option value="122">Liberia</option>
								<option value="123">Libya</option>
								<option value="124">Liechtenstein</option>
								<option value="125">Lithuania</option>
								<option value="126">Luxembourg</option>
								<option value="127">Macau</option>
								<option value="128">Macedonia, Rep. of</option>
								<option value="129">Madagascar</option>
								<option value="130">Malawi</option>
								<option value="131">Malaysia</option>
								<option value="132">Maldives</option>
								<option value="133">Mali</option>
								<option value="134">Malta</option>
								<option value="135">Marshall Islands</option>
								<option value="136">Martinique</option>
								<option value="137">Mauritania</option>
								<option value="138">Mauritius</option>
								<option value="139">Mayotte</option>
								<option value="140">Mexico</option>
								<option value="141">Micronesia, Federal States of</option>
								<option value="142">Moldova, Republic of</option>
								<option value="143">Monaco</option>
								<option value="144">Mongolia</option>
								<option value="145">Montenegro</option>
								<option value="146">Montserrat</option>
								<option value="147">Morocco</option>
								<option value="148">Mozambique</option>
								<option value="149">Myanmar, Burma</option>
								<option value="150">Namibie</option>
								<option value="151">Nauru</option>
								<option value="152">Nepal</option>
								<option value="153">Netherlands</option>
								<option value="154">Netherlands Antilles</option>
								<option value="155">New Caledonia</option>
								<option value="156">New Zealand</option>
								<option value="157">Nicaragua</option>
								<option value="158">Niger</option>
								<option value="159">Nigeria</option>
								<option value="160">Niue</option>
								<option value="161">Northern Mariana Islands</option>
								<option value="162">Norway</option>
								<option value="163">Oman</option>
								<option value="164">Pakistan</option>
								<option value="165">Palau</option>
								<option value="166">Palestinian territories</option>
								<option value="167">Panama</option>
								<option value="168">Papua New Guinea</option>
								<option value="169">Paraguay</option>
								<option value="170">Peru</option>
								<option value="171">Philippines</option>
								<option value="172">Pitcairn Island</option>
								<option value="173">Poland</option>
								<option value="174">Portugal</option>
								<option value="175">Puerto Rico</option>
								<option value="176">Qatar</option>
								<option value="177">Reunion Island</option>
								<option value="178">Romania</option>
								<option value="179">Russian Federation</option>
								<option value="180">Rwanda</option>
								<option value="181">Saint Kitts and Nevis</option>
								<option value="182">Saint Lucia</option>
								<option value="183">Saint Vincent and the Grenadines</option>
								<option value="184">Samoa</option>
								<option value="185">San Marino</option>
								<option value="186">Sao Tome and Principe</option>
								<option value="187">Saudi Arabia</option>
								<option value="188">Senegal</option>
								<option value="189">Serbia</option>
								<option value="190">Seychelles</option>
								<option value="191">Sierra Leone</option>
								<option value="192">Singapore</option>
								<option value="193">Slovakia (Slovak Republic)</option>
								<option value="194">Slovenia</option>
								<option value="195">Solomon Islands</option>
								<option value="196">Somalia</option>
								<option value="197">South Africa</option>
								<option value="198">South Sudan</option>
								<option value="199">Spain</option>
								<option value="200">Sri Lanka</option>
								<option value="201">Sudan</option>
								<option value="202">Suriname</option>
								<option value="203">Swaziland</option>
								<option value="204">Sweden</option>
								<option value="205">Switzerland</option>
								<option value="206">Syria, Syrian Arab Republic</option>
								<option value="207">Taiwan (Republic of China)</option>
								<option value="208">Tajikistan</option>
								<option value="209">Tanzania; officially the United Republic of Tanzania</option>
								<option value="210">Thailand</option>
								<option value="211">Tibet</option>
								<option value="212">Timor-Leste (East Timor)</option>
								<option value="213">Togo</option>
								<option value="214">Tokelau</option>
								<option value="215">Tonga</option>
								<option value="216">Trinidad and Tobago</option>
								<option value="217">Tunisia</option>
								<option value="218">Turkey</option>
								<option value="219">Turkmenistan</option>
								<option value="220">Turks and Caicos Islands</option>
								<option value="221">Tuvalu</option>
								<option value="222">Uganda</option>
								<option value="223">Ukraine</option>
								<option value="224">United Arab Emirates</option>
								<option value="225">United Kingdom</option>
								<option value="2">United States of America</option>
								<option value="226">Uruguay</option>
								<option value="227">Uzbekistan</option>
								<option value="228">Vanuatu</option>
								<option value="229">Vatican City State (Holy See)</option>
								<option value="230">Venezuela</option>
								<option value="231">Vietnam</option>
								<option value="232">Virgin Islands (British)</option>
								<option value="233">Virgin Islands (U.S.)</option>
								<option value="234">Wallis and Futuna Islands</option>
								<option value="235">Western Sahara</option>
								<option value="236">Yemen</option>
								<option value="237">Zambia</option>
								<option value="238">Zimbabwe</option>
							</select>
						</div>
						<div class="col-lg-6">
							<label for="dob">Birthday</label>
							<input type="text" class="form-control dob">
						</div>
						<div class="col-lg-6">
							<label for="mobile">Mobile</label>
							<div class="row">
								<div class="col-lg-4">
									<input type="text" class="form-control mobilePrefix">
								</div>
								<div class="col-lg-8">
									<input type="text" class="form-control mobile">
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<label for="landline">Landline</label>
							<div class="row">
								<div class="col-lg-4">
									<input type="text" class="form-control landlinePrefix">
								</div>
								<div class="col-lg-8">
									<input type="text" class="form-control landline">
								</div>
							</div>
						</div>
					</div>
				</div><br/>
				<p class="text-right">
					<button class="btn btn-info btn-md save-button">Save</button>
					<button class="btn btn-info btn-md cancel-button">Cancel</button>
				</p>
			</div>
		</section>
	</aside>
</div>