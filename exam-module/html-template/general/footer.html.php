<!--footer start-->
<?php include 'loader.html.php'; ?>
<footer class="site-footer">
  <div class="text-center">
	  2016 &copy; Copyright by <a href="#" style="color:#fff;">Arcanemind.</a>
	  <a href="#" class="go-top">
		  <i class="fa fa-angle-up"></i>
	  </a>
  </div>
</footer>
<!--footer end-->