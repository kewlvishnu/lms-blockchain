<!-- js placed at the end of the document so the pages load faster -->
<script src="../admin/js/jquery.js"></script>
<script src="../admin/js/bootstrap.js"></script>
<script class="include" type="text/javascript" src="../admin/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="../admin/js/jquery.scrollTo.min.js"></script>
<script src="../admin/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="../admin/js/respond.min.js" ></script>
<script src="../admin/assets/jcrop/js/jquery.Jcrop.js"></script>
<script src="../admin/assets/crop_upload/crop_upload/js/crop_upload.min.js"></script>
<script src="../admin/assets/jquery-form/jquery.form.js"></script>
<script src="../admin/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="../admin/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../admin/js/easy-pie-chart.js"></script>
<!--common script for all pages-->	
<script src="../admin/js/common-scripts.js"></script>
<script src="../admin/assets/fuelux/js/tree.min.js"></script>
<script src="js/custom/treeLoader.js"></script>
<!-- script for custom scroll -->
<script src="../admin/js/custom/customscroll.js"></script>
<!--script for carousel -->
<script type="text/javascript" src="../admin/assets/slick/slick.min.js"></script>
<!-- script for charts and graphs -->
<script src="../admin/assets/chart-master/Chart.js"></script>
<script src="../admin/js/jquery.sparkline.js" type="text/javascript"></script>