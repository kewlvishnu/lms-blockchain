<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Mosaddek">
<meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
<link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">

<!-- Bootstrap core CSS -->
<link href="../admin/css/bootstrap.min.css" rel="stylesheet">
<link href="../admin/css/bootstrap-reset.css" rel="stylesheet">
<!--external css-->
<link href="../admin/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="../admin/assets/fuelux/css/tree-style.css" type="text/css" rel="stylesheet"/>
<!-- Custom styles for this template -->
<link href="../admin/css/style.css" rel="stylesheet">
<link href="../admin/css/style-responsive.css" rel="stylesheet" />
<link rel="stylesheet" href="../admin/assets/jcrop/css/jquery.Jcrop.css" type="text/css" />
<link href="../admin/assets/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="../admin/assets/slick/slick.css"/>
<link href="../admin/css/custom.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->

<!-- analytics code -->
<?php @include_once "../../analytics.php"; ?>