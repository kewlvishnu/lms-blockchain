<!--sidebar start-->
<aside>
  <div class="nav-collapse collapse" id="sidebar" tabindex="5000">
	  <!-- sidebar menu start-->
		<ul id="nav-accordion" class="sidebar-menu">
			<li>
				<a href="MyCourse.php">
				<i class="fa fa-copy" class="course"></i>
				<span class="course">My Courses</span></a>
				<ul class="sub" style="display: none;">
					<li style="padding-left: 5px;">
						<div class="custom-side-tree">
							<div id="Div1" class="tree tree-plus-minus tree-solid-line tree-unselectable">
								<div class="tree-folder" style="display: none;">
									<div class="tree-folder-header">
										<i class="fa fa-folder"></i>
										<div class="tree-folder-name"></div>
									</div>
									<div class="tree-folder-content"></div>
									<div class="tree-loader" style="display: none;"></div>
								</div>
								<div class="tree-item" style="display: none;">
									<div class="tree-item-name"></div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</li>
			<li>
			  <a href="subscription.php">
				  <i class="fa fa-file"></i>
				  <span>Subscriptions</span>
			  </a>
			</li>
			<li>
			  <a href="profile.php">
				  <i class="fa fa-picture-o"></i>
				  <span>My Profile</span>
			  </a>
			</li>
			<li>
			  <a href="setting.php">
				  <i class="fa fa-cog"></i>
				  <span>Settings</span>
			  </a>
			</li>
			<li>
			  <a href="notification.php">
				  <i class="fa fa-bell-o"></i>
				  <span>Notifications</span>
			  </a>
			</li>
			<li>
			  <a href="changePassword.php">
				  <i class="fa fa-money"></i>
				  <span>Password/Currency</span>
			  </a>
			</li>
		</ul>
	  <!-- sidebar menu end-->
  </div>
</aside>
<!--sidebar end-->