<?php
	@session_start();
	// setting link for logo
	$logolink = '../student/';
	if (isset($_SESSION["userId"]) && !empty($_SESSION["userId"])) {
		if(!isset($logoPath) || empty($logoPath)) {
			$logolink = '../student/';
		}
	}
	if(isset($logoPath) && !empty($logoPath)) {
		$logoBlock = '<a href="'.$logolink.'" class="logo" >
						<h1 class="logo-profile">
							<img src="'.$logoPath.'" alt="'.$portfolioName.'" class="img-logo" />
							<span>'.$portfolioName.'</span>
						</h1>
					</a>';
	} else {
		$logoBlock = '<a href="'.$logolink.'" class="logo" ><img src="../img/Arcane_Logo.png" alt="Arcanemind" class="img-logo" /></a>';
	}
	/*require_once('/../../../api/Vendor/ArcaneMind/Api.php');
	$u = new Api();
	$data = $u->trackUser();
	print_r('hello1');
	
	*/
?>
<!--header start-->
<header class="header white-bg">
	<div class="sidebar-toggle">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
		<!-- <div data-original-title="Toggle Navigation" data-placement="right" class="fa fa-bars tooltips"></div> -->
	</div>
	<!--logo start-->
	<?php echo $logoBlock; ?>
	<!--logo end-->          
	<!-- <div class="top-nav ">
		<ul class="nav pull-right top-menu">
			<li id="header_notification_bar" class="dropdown">
				<a class="dropdown-toggle" href="notification.php">
					<i class="fa fa-bell-o"></i>
					<span class="notify badge bg-warning"></span>
				</a>
			</li>
			user login dropdown start
			<li class="dropdown">
				<a data-toggle="dropdown" class="dropdown-toggle" href="#" id="user-profile-card">
					<?php if(isset($_SESSION['username']))echo $_SESSION['username'];else echo 'Guest'; ?>
					<b class="caret"></b>
				</a>
				<ul class="dropdown-menu extended logout">
					<div class="log-arrow-up"></div>
					<li><a href="profile.php"><i class=" fa fa-suitcase"></i>Profile</a></li>
					<li><a href="setting.php"><i class="fa fa-cog"></i> Settings</a></li>
					<li><a href="notification.php"><i class="fa fa-bell-o"></i> Notification</a></li>
					<li><a href="../logout.php"><i class="fa fa-key"></i> Log Out</a></li>
				</ul>
			</li>
			user login dropdown end
		</ul>
	</div> -->
</header>
<!--header end-->