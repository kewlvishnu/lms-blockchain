<section class="panel" id="user-achievements">
	<div class="bio-graph-heading">
		Achievements
<!--		<button class="btn btn-primary btn-xs edit-button" style="float:right; border-color:#fff; background-color:#0A3151;"><i class="fa fa-pencil"></i></button>-->
	</div>
	<div class="panel-body bio-graph-info view-section">
	</div> 
	 <!-- edit section -->
	<div class="contact-form panel-body edit-section">
		<textarea class="form-control " style="width:100%;height:100px;" placeholder="Your Achievements..."></textarea><br/>
		<p class="text-right">
		<button class="btn btn-primary btn-xs cancel-button"><i class="fa fa-times"></i> Cancel</button>
		<button class="btn btn-primary btn-xs save-button" data-name="achievement"><i class="fa fa-save"></i> Save</button>
		</p>
	</div>
</section>