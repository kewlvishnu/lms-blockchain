<section class="panel" id="user-profile-cover">
	  <div class="cover-photo">
		  <div class="fb-timeline-img">
			  <img src="" alt="" id="cover-pic" />
		  </div>
<!--		  <button type="button"  href="#upload-cover-image" data-toggle="modal" class="btn btn-info upload-cover-image"><i class="fa fa-pencil"></i> Change cover</button>-->
	  </div>
	  <div class="panel-body">
		  <div class="profile-thumb profile-photo">
			  <img src="" alt="" id="profile-pic"/>
			  <button type="button"  href="#upload-profile-image" data-toggle="modal" class="btn btn-info btn-xs upload-profile-image"><i class="fa fa-pencil"></i></button>
		  </div>
		  <a href="#" class="fb-user-mail" id="profile-name"></a><br />
		  <span class="fb-user-mail" id="user-type"></span>
	  </div>
  </section>