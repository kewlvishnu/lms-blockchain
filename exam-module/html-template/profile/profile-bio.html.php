<section class="panel" id="user-bio">
  <div class="bio-graph-heading">
  Background                              
	  <button class="btn btn-danger btn-xs edit-button pull-right" style="border-color:#fff;"><i class="fa fa-pencil"></i></button>
  </div>
  <div class="panel-body bio-graph-info view-section">
	 <p class="text">
	 </p>
  </div>
  <div class="panel-body bio-graph-info edit-section">
	<textarea class="form-control " style="width:100%;height:100px;" placeholder="Some info"></textarea><br/>
	<p class="text-right">
	<button class="btn btn-primary btn-xs cancel-button"><i class="fa fa-times"></i> Cancel</button>
	<button class="btn btn-primary btn-xs save-button" data-name="bio"><i class="fa fa-save"></i> Save</button>
	</p>
  </div>
</section>