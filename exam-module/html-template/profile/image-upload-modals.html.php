<!--Modal for image upload-->
	<div aria-hidden="true" aria-labelledby="upload-image-modal" role="dialog" tabindex="-1"
        id="upload-cover-image" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                    <h4 class="modal-title">
                        Upload Cover Image
					</h4>
                </div>
                <div class="modal-body">
                <div class="row">
					<div class="col-lg-12">
						<form id="cover-image-form"  method="post" enctype="multipart/form-data">
							<input type='file' id="cover-image-uploader" name="cover-image" accept="image/*"/>
							<img id="uploaded-image1" class="crop" src="#" alt="Your image"  style="display: none;"/>
							<input type="hidden" id="x1" name="x" />
							<input type="hidden" id="y1" name="y" />
							<input type="hidden" id="w1" name="w" />
							<input type="hidden" id="h1" name="h" />
							<div class="progress" style="display:none">Saving....</div>
							<div class="msg"></div>
							<input type="submit" value="Save!" style="display:none" disabled=true/>
						</form>						
					</div>
                </div>
                </div>
            </div>
        </div>
    </div>
	
	<div aria-hidden="true" aria-labelledby="upload-image-modal" role="dialog" tabindex="-1"
        id="upload-profile-image" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                    <h4 class="modal-title">
                        Upload Profile Image
					</h4>
                </div>
                <div class="modal-body">
                <div class="row">
					<div class="col-lg-12">
						<form id="profile-image-form"  method="post" enctype="multipart/form-data">
							<input type='file' id="profile-image-uploader" name="profile-image" accept="image/*"/>
							<img id="uploaded-image2" class="crop" src="#" alt="Your image"  style="display: none;"/>
							<input type="hidden" id="x2" name="x" />
							<input type="hidden" id="y2" name="y" />
							<input type="hidden" id="w2" name="w" />
							<input type="hidden" id="h2" name="h" />
							<div class="progress" style="display:none">Saving....</div>
							<div class="msg"></div>
							<input type="submit" value="Save!" style="display:none"/>
						</form>						
					</div>
                </div>
                </div>
            </div>
        </div>
    </div>