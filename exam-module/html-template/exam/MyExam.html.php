<section id="main-content">
	<div class="panel">
		<div class="panel-body">
			<ul class="breadcrumb">
				<li><a href="MyCourse.php">My Course</a></li>
				<li></li>
			</ul>
			<div class="row">
				<div class="col-lg-12">
				<h3>
					<span  id='course-name'> </span>
					<a href="#viewCourseDoc" class="btn btn-md btn-custom btn-success pull-right dispNone" data-toggle="modal" id="viewcoursedoc1">View Course Document</a>
					</h3>
				</div>
			<h4>
				<span class="course-certificaation" id="coursecertification"></span>
				<a href="#" download  class="btn btn-md btn-custom btn-success pull-right dispNone"  id="certification-download">Download certificate</a>
			</h4>
			</div>
			Following are the list of subjects, exams and assignments available in this course created by: <strong><span id='instiute-name'></span> </strong>
			<hr/>
			<h4><strong>Subjects</strong></h4>
			<div id='tbl-subjects'></div>
			<br />
			<h4 class="tbl_header_exam">Active <strong>Exams</strong></h4>
			<table id='tbl_exams' class="table panel-body"> 
				<thead>
					<tr>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="">Exam<br/></th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class=" ">Start Date</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class=" ">End Date</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="text-center ">Attempts Given</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="text-center ">Attempts Left</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="text-center ">Results</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="text-center ">
							<!--<a id='showExam'  class='btn btn-info btn-xs' > Show</a>-->
						</th>
					</tr>
				</thead>
				<tbody>
					<!--<tr>
						<td colspan="7" >No Active Exams Now</td>
					</tr>-->
				</tbody>
			</table>
			<a show="More" class="btn btn-info btn-xs pull-right" id="showExam" style="display: none;margin-right: 3%;">More..</a>
			<br />
			<h4 class="tbl_header_subjective_exam">Active <strong>Subjective Exams</strong></h4>
			<table id='tbl_subjective_exams' class="table panel-body"> 
				<thead>
					<tr>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="">Exam<br/></th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class=" ">Start Date</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class=" ">End Date</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="text-center ">Attempts Given</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="text-center ">Attempts Left</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="text-center ">Results</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="text-center ">
							<!--<a id='showExam'  class='btn btn-info btn-xs' > Show</a>-->
						</th>
					</tr>
				</thead>
				<tbody>
					<!--<tr>
						<td colspan="7" >No Active Exams Now</td>
					</tr>-->
				</tbody>
			</table>
			<a show="More" class="btn btn-info btn-xs pull-right" id="showSubjectiveExam" style="display: none;margin-right: 3%;">More..</a>
			<br />
			<h4 class="tbl_header_ass">Active <strong>Assignments</strong></h4>
			<table id='tbl_assignment' class="table panel-body"> 
				<thead>
					<tr>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class=""> Assignment</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class=" ">Start Date</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class=" ">End Date</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="text-center ">Attempts Given</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="text-center">Attempts Left</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="text-center ">Results</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="text-center ">
						<!-- <a id='showAssignment'  class='btn btn-info btn-xs' > Show</a>-->
					</th>
					</tr>	
				</thead>
				<tbody>
				<!--<tr>
					<td colspan="7" >No Active Assignment Now</td>
				</tr>-->
				</tbody>
			</table>
			<a show="More" class="btn btn-info btn-xs pull-right" id="showAssignment" style="display: none;margin-right: 3%;">More..</a>
			<br />
			<h4 class="tbl_header_manual_exam">Active <strong>Manual Exams</strong></h4>
			<table id='tbl_manual_exams' class="table panel-body"> 
				<thead>
					<tr>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="">Exam</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="">Type</th>
						<th style="border:none;color:rgba(8, 107, 255, 0.96);" class="text-center">Results</th>
						<!-- <a id='showManualExam'  class='btn btn-info btn-xs' > Show</a>-->
					</th>
					</tr>	
				</thead>
				<tbody>
				<!--<tr>
					<td colspan="7" >No Active Assignment Now</td>
				</tr>-->
				</tbody>
			</table>
			<a show="More" class="btn btn-info btn-xs pull-right" id="showManualExam" style="display: none;margin-right: 3%;">More..</a>
		</div>
	</div>
</section>