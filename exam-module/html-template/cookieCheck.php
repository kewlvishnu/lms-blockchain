<?php
    if(isset($_COOKIE["mtwebLogin"]) && !empty($_COOKIE["mtwebLogin"])) {
        $libPath = "../api/Vendor";
        include_once $libPath . '/Zend/Loader/AutoloaderFactory.php';
        require_once $libPath . "/ArcaneMind/Api.php";
        Zend\Loader\AutoloaderFactory::factory(array(
                'Zend\Loader\StandardAutoloader' => array(
                        'autoregister_zf' => true,
                        'db' => 'Zend\Db\Sql'
                )
        ));
        require_once '../api/Vendor/ArcaneMind/User.php';
        $cookie = $_COOKIE["mtwebLogin"];
        $res = Api::checkUserRemembered($cookie);
        @session_start();
        $_SESSION["userId"] = $res->user['id'];
        $_SESSION["userRole"] = $res->user['roleId'];
        $_SESSION["username"] = $res->user['email'];
    }
?>