<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
	 id="purchaseNowModal" class="modal fade a-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" id="modelclose" data-dismiss="modal" aria-hidden="true">
					×</button>
				<h4 class="modal-title">
					Confirm Purchase
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-4">
						<strong><span class="course-name"></span></strong>
					</div>
					<div class="col-lg-4">
					</div>
					<div class="col-lg-4">
						<?php /*	<span class="amount">$150</span>*/?>
					</div>
					
					<div class="col-lg-4">
						<strong>You Pay</strong>
					</div>
					<div class="col-lg-4">
					</div>
					<div class="col-lg-4">
					<strong><span class="amount">$150</span></strong>
					</div>
				</div>
				<hr>
					<button class="btn btn-info btn-block" id="payUMoneyButton">Pay Now</button><br>
					<span style="text-align: justified;">
						You will be redirected to payment page and then sent back once you complete your purchase.<br>
						<br>By clicking the 'Pay Now' button you agree these <a href="#">Terms and Conditions</a>.
					</span>
				</div>
			</div>
		</div>
	</div>
</div>