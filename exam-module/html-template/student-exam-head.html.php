<meta charset="utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="Mosaddek" name="author" />
<meta content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina" name="keyword" />
<link href="img/favicon.png" rel="shortcut icon" />
<!-- Bootstrap core CSS   -->
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/bootstrap-reset.css" />
<link type="text/css" rel="stylesheet" href="css/custom.css" />
<!--external css-->
<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.css" />
<link rel="stylesheet" href="assets/dropzone/css/dropzone.css" />
<!-- Custom styles for this template -->
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/style-responsive.css" />
<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->