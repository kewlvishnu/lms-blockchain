<?php
	$actual_link  = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$temp_link    = explode("/", $actual_link);
	$sub_link = explode(".", $temp_link[2]);
	$final_link = str_replace($sub_link[0], 'www', $actual_link);

	$subdomain = FALSE;
	if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
		$u = new User();
		if(isset($_GET['pageDetailId']) && !empty($_GET['pageDetailId']) && !in_array($_GET['pageDetailId'], $u->urlDomains) ) {
			
			$actual_link  = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$temp_link    = explode("/", $actual_link);
			$sub_link = explode(".", $temp_link[2]);
			//$final_link = str_replace($sub_link[0], 'www', $actual_link);
			$domainPath = "//www.".$sub_link[end(array_keys($sub_link))-1].".".$sub_link[end(array_keys($sub_link))]."/";
			
			if ($domainPath == $u->sitePath) {
				//$final_link = str_replace($sub_link[0], 'www', $actual_link);
				$subdomain = TRUE;
			}
			$final_link = $u->sitePath;
			$slug = trim($_GET['pageDetailId']);
			if ($branch != "production") {
				if (strpos($slug, ".")) {
					$slug = explode(".", $slug);
					$slug = $slug[0];
				}
			}
			$res = Api::getPortfolioBySlug($slug);
			if($res->valid) {
				$portfolioDetails = $res;
				$portfolioName 	  = $portfolioDetails->portfolio['name'];
				$logoPath = $res->portfolio['img'];
				if(!empty($res->portfolio['favicon'])) {
					$favicon  = $res->portfolio['favicon'];
				}
			} else {
				header('location:'.$final_link);
			}
		}
	}
	$trackData = Api::trackUser();
	if (isset($_SESSION["temp"]) && $_SESSION["temp"]==1) {
		header('location:../init-student/');
	}
?>