﻿<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	require_once "html-template/cookieCheck.php";
	//code to update any paused exam or assignment
	// Loading Zend
    require_once "../api/Vendor/ArcaneMind/Api.php";
	include '../api/Vendor/Zend/Loader/AutoloaderFactory.php';
	Zend\Loader\AutoloaderFactory::factory(array(
			'Zend\Loader\StandardAutoloader' => array(
					'autoregister_zf' => true,
					'db' => 'Zend\Db\Sql'
			)
	));
	require_once '../api/Vendor/ArcaneMind/StudentExam.php';
	$se = new StudentExam();
	$se->completeAllExams();
	require_once "html-template/pageCheck.php";
	require_once "../config/url.functions.php";
	if (!isset($_GET["examId"]) || empty($_GET["examId"])) {
		header("location:".$sitepathStudent);
	}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		
		<title>Exam Module</title>
		<?php include "html-template/general/admin-header.html.php"; ?>
		<?php include 'html-template/student-exam-head.html.php'; ?>
		<link rel="stylesheet" href="../css/mediaelementplayer.min.css" />
        <meta charset="utf-8">
        <script language="javascript">
        	var sitepathStudent = '<?php echo $sitepathStudent; ?>';
			//script to disable f12 key
			/*document.onkeypress = function (event) {
				event = (event || window.event);
				if (event.keyCode == 123 || event.keyCode == 18) {
				//alert('No F-12');
					return false;
				}
			}
			document.onmousedown = function (event) {
				event = (event || window.event);
				if (event.keyCode == 123 || event.keyCode == 18) {
					//alert('No F-keys');
					return false;
				}
			}
			document.onkeydown = function (event) {
				event = (event || window.event);
				if (event.keyCode == 123 || event.keyCode == 18) {
					//alert('No F-keys');
					return false;
				}
			}

			//script to disable right mouse click key
			function clickIE4() {
				if (event.button == 2) {
					return false;
				}
			}

			function clickNS4(e) {
				if (document.layers || document.getElementById&&!document.all) {
					if (e.which==2||e.which==3) {
						return false;
					}
				}
			}

			if (document.layers){
				document.captureEvents(Event.MOUSEDOWN);
				document.onmousedown = clickNS4;
			}
			else if (document.all && !document.getElementById){
				document.onmousedown = clickIE4;
			}
			document.oncontextmenu = new Function("return false;");

			//disabling ctrl keys
			var isCtrl = false;
			document.onkeyup=function(e) {
				if(e.which == 17)
					isCtrl = false;
			}

			document.onkeydown=function(e) {
				if(e.which == 17)
					isCtrl = true;
				if(((e.which == 85) || (e.which == 117) || (e.which == 65) || (e.which == 97) || (e.which == 67) || (e.which == 99)) && isCtrl == true) {
				//alert('Keyboard shortcuts are cool!');
					return false;
				}
			}
*/		</script>
	</head>
	<body>
		<?php include "html-template/general/header.html.php"; ?>
<div id="loader" style="background: rgba(0, 0, 0, 0.31);
width: 100%;
height: 100%;
position: fixed;
top: 0;
left: 0;
z-index: 10000;
display: none;">
	<img src="img/loader.gif" style="position: fixed;
top: 50%;
left: 50%;
border: 5px solid white;
border-radius: 3px;">
</div>
		<section class="wrapper" id="container">
			<section class="panel-body">
					<div id="questions" style="display: none;">
						<div class="row">
							<div class="col-md-9 col-sm-8 custom-font">
								<div id="sectionButtons">
								</div>
								<div id="container-for-questions" style="display: none;">
									<div class="row">
										<div class="col-xs-12">
											<strong>Question No. <span id="questionNumber"></span></strong>
											&emsp;<span style="color: rgb(0, 114, 197);">(<span id="marks"></span>)</span>
										</div><br/>
										<div class="col-md-12">
											<div id="question">
											</div>
											<input type="hidden" id="questionId" value="0">
										</div>
									</div>
									<div class="row" id="answer">
										<div class="col-md-12 options">
											<label class="radio-inline inline-exam">
												<input type="radio" name="options" value="Option 1"><div class="opt-ans"></div>
											</label>
											<br/>
											<label class="radio-inline inline-exam">
												<input type="radio" name="options" value="Option 2"><div class="opt-ans"></div>
											</label>
											<br/>
										</div>
										<div class="col-md-12 optionsMul">
											<label class="checkbox-inline inline-exam">
												<input type="checkbox" value="Option 1"><div class="optMul-ans"></div>
											</label>
											<br/>
											<label class="checkbox-inline inline-exam">
												<input type="checkbox" value="Option 2"><div class="optMul-ans"></div>
											</label>
											<br/>
										</div>
										<div class="col-md-12 true_false">
											<label class="radio-inline inline-exam">
												<input type="radio" name="true_false" class="true">True
											</label> <br/>
											<label class="radio-inline inline-exam">
												<input type="radio" name="true_false" class="false">False
											</label> <br/>
										</div>
										<div class="col-md-12 ftb">
											<div class="row">
												<div class="col-lg-3">
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
										<div class="col-md-12 mtf">
											<div class="row" style="margin-top: 10px">
												<div class="col-lg-2">
													<div class="columnA"></div>
												</div>
												<div class="col-lg-2">
													<select class="form-control matcher">
														<option value="0">Select Answer</option>
													</select>
												</div>
												<div class="col-lg-2">
													<div class="columnB"></div>
												</div>
											</div>
											<div class="row" style="margin-top: 10px">
												<div class="col-lg-2">
													<div class="columnA"></div>
												</div>
												<div class="col-lg-2">
													<select class="form-control matcher">
														<option value="0">Select Answer</option>
													</select>
												</div>
												<div class="col-lg-2">
													<div class="columnB"></div>
												</div>
											</div>
										</div>
										<div class="col-md-12 dpn">
											<div class="row">
												<div class="col-md-6 question">
												</div>
												<div class="col-md-6 custom-right-question">
												</div>
											</div>
										</div>
										<div class="col-md-12 audio">
											<div class="form-group" id="jsPlayer"></div>
											<div>
												<label class="radio-inline inline-exam">
													<input type="radio" name="options" value="Option 1"><div class="opt-ans"></div>
												</label>
												<br/>
												<label class="radio-inline inline-exam">
													<input type="radio" name="options" value="Option 2"><div class="opt-ans"></div>
												</label>
												<br/>
											</div>
										</div>
									</div>
									<div id="compro" style="display: none;">
										<div class="row">
											<div class="col-md-6" id="parentQuestion">
											</div>
											<div class="col-md-6 custom-right-question">
												<div class="col-md-12 child-question">
												</div>
												<div class="col-md-12 answer">
													<div class="col-md-12 options">
														<label class="radio-inline inline-exam">
															<input type="radio" name="childOptions" value="Option 1"><div class="opt-ans"></div>
														</label>
														<br/>
														<label class="radio-inline inline-exam">
															<input type="radio" name="childOptions" value="Option 2"><div class="opt-ans"></div>
														</label>
														<br/>
													</div>
													<div class="col-md-12 optionsMul">
														<label class="checkbox-inline inline-exam">
															<input type="checkbox" value="Option 1"><div class="optMul-ans"></div>
														</label>
														<br/>
														<label class="checkbox-inline inline-exam">
															<input type="checkbox" value="Option 2"><div class="optMul-ans"></div>
														</label>
														<br/>
													</div>
													<div class="col-md-12 true_false">
														<label class="radio-inline inline-exam">
															<input type="radio" name="child_true_false" class="true">True
														</label> <br/>
														<label class="radio-inline inline-exam">
															<input type="radio" name="child_true_false" class="false">False
														</label> <br/>
													</div>
													<div class="col-md-12 ftb">
														<div class="row">
															<div class="col-lg-6">
																<input type="text" class="form-control">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3 col-sm-4">
								<h2 id="name" class="exam-name"></h2>
								<span class="js-timer tooltips" style="display: none;" data-original-title="Time Left" id="timeText" data-placement="left">
									<h3 style="color: rgb(0, 114, 197);"><i class="fa fa-clock-o"></i>&nbsp;<span id="timeNow"></span><span id="timeSecond"></span></h3>
								</span>
								<div class="row">
									<div class="col-lg-12">
										<div class="custom-bg">
											<h4 style="font-weight:600; text-transform: capitalize;" id="sectionName"></h4>
											Question Palate:
											<div id="questionButtons" class="custom-mod-button" style="max-height: 150px;"></div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="custom-bg dib">
											<strong>Legend</strong><br/>
											<span><a class="btn btn-white btn-xs" style="line-height: 1;">&nbsp;</a> Not Visited</span>
											<span> <a class="btn btn-success btn-xs" style="line-height: 1;">&nbsp;</a> Answered</span>
											<span><a class="btn btn-info btn-xs" style="line-height: 1;">&nbsp;</a> <span id="legend">Marked</span></span>
											<span class="toBeRemoved"><a class="btn btn-success btn-info btn-xs" style="line-height: 1;">&nbsp;</a> Answered &amp; Marked</span>
											<span><a class="btn btn-danger btn-xs" style="line-height: 1;">&nbsp;</a> Not Answered</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<br/>
						<div class="row" style="position: fixed;width: 100%;margin: 0px;bottom: 0px;background: white; border-radius: 5px;padding-top: 5px;padding-bottom: 5px;">
							<div class="col-lg-10 col-sm-8">
								<div class="pull-left">
									<a href="#" class="btn btn-info btn-md" id="clearResponses">Clear Response</a>
									<a href="#" class="btn btn-info btn-md" id="markReview">Mark for review &amp; Next</a>
								<!-- </div>
								<div class="pull-right" style="padding-right: 2em;"> -->
									<a href="#" class="btn btn-info btn-md" id="previous"> Previous</a>
									<a href="#" class="btn btn-success btn-md" id="saveAndNext"> Save &amp; Next</a>
									<a href="#" class="btn btn-success btn-md" id="showAnswer" style="display: none;">Save and view answer</a>
								</div>
							</div>
							<div class="col-lg-2 col-sm-4 pull-left">
									<a class="btn btn-danger btn-md" id="submitExam">Submit Exam</a>
									<a id="showInstructions"><h4 style="text-transform: capitalize;margin-top: 2px;"><i class="fa fa-file-text-o"></i> Instructions</h4></a>
								<!-- <a  id="qppr"><h4 style="text-transform: capitalize;"><i class="fa fa-info-circle"></i> Question Paper</h4></a> -->
							</div>
						</div>
					</div>
					<div id="instructions">
						<div class="row">
							<div class="col-lg-12">
								<div id="inst">
								</div>
							</div>
						</div>

						<hr>

						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="checkbox-inline">
										<input type="checkbox" id="optionInstructions" name="optionInstructions"> I agree to the instructions
									</label>
								</div>
							</div>
						</div>

						<hr>
						
						<div class="row">
							<div class="col-md-4 col-sm-6" style="display: none;">
								<div id="askAnswer">
									<div>Do you want to display answers after each question.</div>
									<label class="radio-inline">
										<input type="radio" id="yes" name="askAnswer">Yes
									</label>
									<label class="radio-inline">
										<input type="radio" id="no" name="askAnswer">No
									</label>
								</div>
							</div>
							<div class="col-md-2 col-sm-4">
								<a class="btn btn-info btn-md" id="startExam" style="display: none;">Start Exam</a>
								<a class="btn btn-info btn-md" id="backToExam" style="display: none;">Back to Exam</a>
								<a class="btn btn-info btn-md" id="startAssignment" style="display: none;">Start Assignment</a>
							</div>
						</div>
					</div>
					<div id="questionPaper" style="display: none;">
						<div class="row">
							<div class="col-lg-12">
								<div class="questionPaper">
								</div>
							</div>
							<button class="btn btn-info btn-md pull-right question-paper-back" style="margin-right: 5%;">Back</button>
						</div>
					</div>
					<div id="allAnswers" style="display: none;">
						Go to your <a href="<?php echo $sitepathStudentProfile; ?>">profile</a> or <a href="<?php echo $sitepathStudentCourses; ?>">My Courses</a>.
						<div class="row">
							<div class="col-lg-12">
								<div id="buttons" class="row" style="margin: 1%;">
								</div>
								<div id="all">
								</div>
							</div>
						</div>
					</div>
			</section>
		</section>
		<div id="freeze">
			<div class="page-middle">
				Gap time remaining
				<div id="gapTimer">00:00:00</div>
			</div>
		</div>
		<?php include 'html-template/student-exam-footer.html.php'; ?>
        <?php include "html-template/pageCheckFooter.php"; ?>
		<script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
		<script src="../js/mediaelement-and-player.min.js"></script>
		<script src="js/custom/examModule.js" type="text/javascript"></script>
	</body>
</html>
