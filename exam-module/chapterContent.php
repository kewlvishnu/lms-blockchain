<?php
	require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
	require_once "../api/Vendor/ArcaneMind/AccessApi.php";
	$access = AccessApi::checkAccess();
	require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>View Chapter</title>
		<?php include 'html-template/general/admin-header.html.php'; ?>
		<!--<link rel="stylesheet" type="text/css" href="flowplayer/skin/minimalist.css" />
		<link rel="stylesheet" href="mediaelement/build/mediaelementplayer.min.css" />-->
		<link rel="stylesheet" href="../css/mediaelementplayer.min.css" />
		<script language="JavaScript">
			//script to disable f12 key
			document.onkeypress = function (event) {
				event = (event || window.event);
				if (event.keyCode == 123 || event.keyCode == 18) {
			 		//alert('No F-12');
					return false;
				}
			}
			document.onmousedown = function (event) {
				event = (event || window.event);
				if (event.keyCode == 123 || event.keyCode == 18) {
					//alert('No F-keys');
					return false;
				}
			}
			document.onkeydown = function (event) {
					event = (event || window.event);
					if (event.keyCode == 123 || event.keyCode == 18) {
							//alert('No F-keys');
							return false;
					}
			}
			//script to disable right mouse click key
			function clickIE4() {
					if (event.button == 2) {
							return false;
					}
			}

			function clickNS4(e) {
					if (document.layers || document.getElementById && !document.all) {
							if (e.which == 2 || e.which == 3) {
									return false;
							}
					}
			}

			if (document.layers) {
					document.captureEvents(Event.MOUSEDOWN);
					document.onmousedown = clickNS4;
			}
			else if (document.all && !document.getElementById) {
				document.onmousedown = clickIE4;
			}
			//document.oncontextmenu = new Function("return false;");

			//disabling ctrl keys
			var isCtrl = false;
			document.onkeyup = function (e) {
					if (e.which == 17)
							isCtrl = false;
			}

			document.onkeydown = function (e) {
					if (e.which == 17)
							isCtrl = true;
					if (((e.which == 85) || (e.which == 117) || (e.which == 65) || (e.which == 97) || (e.which == 67) || (e.which == 99)) && isCtrl == true) {
							//alert('Keyboard shortcuts are cool!');
							return false;
					}
			}
		</script>
	</head>

	<body class="custom-container">

	<section id="container">
	<!--header start-->
		<?php include "html-template/general/header.html.php"; ?>
		<!--sidebar start-->
		<aside>
			<div id="sidebar" class="nav-collapse collapse custom-sidebar">		  
				<!-- sidebar menu start-->
				<ul class="sidebar-menu custom-sidebar-menu" id="nav-accordion">
					<div class="panel-group m-bot20 nav-accordion" id="accordion">
					</div>
				</ul>
				<!-- sidebar menu end-->
			</div>
		</aside>
		<!--sidebar end-->
		<!--main content start-->
		<div class="panel-body custom-course">
			<section class="custom-wrapper">
				<div class="panel-body custom-brad" id="breadcrumb" style="color: #BBBBBB;padding-left: 0%;">
					<span><a href="MyCourse.php" class="custom-btc-link">My Courses</a></span> /
					<span class="btc"></span> /
					<span class="btc"></span> /
					<span class="btc"></span>
				</div>
				<section class="panel" id="content-view">
					<div class="row">
						<div class="col-md-12">
							<header id="contentHeading">
								<span class="custom-panel-heading init"></span>
								<span class="arrow"></span>
								<span class="custom-panel-content cont"></span>
								<a class="pull-right btn btn-success btn-custom btn-md" id="backSubject" href="#">Back</a>
							</header>
						</div>
					</div>
					<section id="block-everything"></section>
					<section id="progress">
						<div class="pcontainer">Loading document please wait<br>
							<div class="progress progress-striped active progress-sm">
									<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
									</div>
							</div>
						</div>
					</section>
					<div class="panel-body custom-content-part">
						<div class="flowplayer color-light no-background" id="player" style="width: 100%;height: 400px;"></div>
					</div>
				</section>
				<div class="pull-right">
					<button type="button" class="btn btn-success btn-sm btn-custom content-view" id="previous">Previous Lecture</button>
					<button type="button" class="btn btn-success btn-sm btn-custom content-view" id="next">Next Lecture</button>
				</div>
				<div class="description" style="display: none;">
					<div class="heading">Description</div>
					<div id="desc"></div>
				</div><br>
				<div class="description"  style="display: none;">
					<div class="heading">Supplementary Files</div>
					<div id="supp"></div>
				</div>
				<br><br>
			</section>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="activityModal" tabindex="-1" role="dialog" aria-labelledby="activityModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="activityModalLabel">No activity</h4>
					</div>
					<div class="modal-body">
						<p>Are you here?</p>
					</div>
					<div class="modal-footer text-center">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Yes</button>
					</div>
				</div>
			</div>
		</div>
		<?php include "html-template/pageCheckFooter.php"; ?>
	</section>
	<?php include 'html-template/course/loader.html.php'; ?>
	<?php include 'html-template/general/admin-footer.html.php'; ?>
	<script src="https://www.youtube.com/player_api"></script>
	<!--<script type="text/javascript" src="flowplayer/flowplayer.min.js"></script>-->
	<script src="../js/mediaelement-and-player.min.js"></script>
	<script src="assets/ViewerJS/viewer.js"></script>
	<script src="../admin/assets/ckeditor/ckeditor.js"></script>
	<script src="js/custom/chapterContent.js" type="text/javascript"></script>
	<script src="http://a.vimeocdn.com/js/froogaloop2.min.js"></script>
	</body>
</html>
