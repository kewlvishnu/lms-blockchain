<?php
require_once "../api/Vendor/ArcaneMind/Api.php";
require_once "../api/Vendor/ArcaneMind/AccessApi.php";
$access = AccessApi::checkAccess();
require_once "html-template/pageCheck.php";
require_once "../../config/url.functions.php";
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Mosaddek">
        <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
        <link rel="shortcut icon" href="img/favicon.png">
		<link rel="stylesheet" href="../styles/custom.css">
		<link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
   		<link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
		<link rel="stylesheet" href="../admin/assets/bootstrap-rating/bootstrap-rating.css">
 
        <title>My Subscriptions</title>
        <?php include "html-template/general/admin-header.html.php"; ?>
   </head>
    <body>
        <section id="container" class="">
            <?php include "html-template/general/header.html.php"; ?>
			<?php include 'html-template/general/sidebar.html.php'; ?>
             <!--main content start-->
            <section>
                <section class="wrapper site-min-height">
                    <!-- page start-->
                    <div class="row">    
                        <div class="col-md-12">
                        <?php include "html-template/subscription/MySubscription.html.php"; ?>
                        </div>	
                    </div>
                    <!-- page end-->
                </section>
            </section>
            <!--main content end-->
            <!--footer start-->
            <?php include "html-template/general/footer.html.php"; ?>
            <?php include "html-template/pageCheckFooter.php"; ?>
            <!--footer end-->
        </section>
        <?php include "html-template/subscription/pacSubs.html.php"; ?>
		<?php include "html-template/subscription/pacSubs1.html.php"; ?>
        <?php include "html-template/subscription/refundModal.html.php"; ?>
        <?php include "html-template/general/admin-footer.html.php"; ?>
       
    <script src="js/custom/MySubscription.js" type="text/javascript"></script>
	<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
	<?php include "html-template/subscription/packCourseDetails.html.php"; ?>
	<?php include 'html-template/purchasepackageModal.html.php'; ?>
	<?php include 'html-template/purchaseCourseModal.html.php'; ?>
    </body>
</html>
