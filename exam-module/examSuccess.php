<?php
	require_once "html-template/cookieCheck.php";
	require_once "../api/Vendor/ArcaneMind/Api.php";
	require_once "../api/Vendor/ArcaneMind/AccessApi.php";
	$access = AccessApi::checkAccess();
	require_once "html-template/pageCheck.php";
	require_once "../../config/url.functions.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Exam Success</title>
	<?php include "html-template/general/admin-header.html.php"; ?>
  </head>
   <body>
	<section id="container" class="">
		<?php include "html-template/general/header.html.php"; ?>
		<!--main content start--><br/>
		<section class="wrapper">
		<div style="position: absolute; right: 0px; left: 0px; top: 45%;">
			<h1  style="text-transform: unset;" class="text-center">Your exam/assignment have been successfully submitted.<h1/>
			<h1 class="text-center">Go To <a class="text-danger" href="<?php echo $sitepathStudentProfile; ?>">Profile</a> or <a class="text-danger" href="<?php echo $sitepathStudentCourses; ?>">My courses</a>.</h1>		</div>
		</section>
		<!--main content end-->
	</section>
  </body>
</html>