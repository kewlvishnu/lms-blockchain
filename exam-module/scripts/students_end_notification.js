var ApiEndpoint = '../api/index.php';

$(function() {
   var key_rate="";
   var display_button='';
  var invitation_link=getUrlParameter("invitation_link");
  
  invitation_details();
	function invitation_details(){
	    var req = {};
		var res; 
		req.action = "load_student_notification";
		req.link_id = invitation_link;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			data =  $.parseJSON(res);
			//return false;
			var html="";
			if(data.invitations != undefined) {
				for(var i=0; i<data.invitations.length; i++)
				{
				var invitation_id=data.invitations[i].id;
				var course_id=data.invitations[i].course_id;
				var email=data.invitations[i].email_id;
				var contact_no=data.invitations[i].contact_no;
				var enrollment_type=data.invitations[i].enrollment_type;
				var status=data.invitations[i].status;
				var course=data.invitations[i].courseName;
				var studentName=data.invitations[i].studentName;
				var timestamp=data.invitations[i].timestamp;
				}
				$('#lbl_course').text(course);
				$('#div_link_id').attr('link_id',invitation_id);
				//$('#tbl_student_deatils').append(html);
			}
			
			
		$('.accept_invitation').on('click',function() {
			   var link_id=$('#div_link_id').attr('link_id');
			   alert(link_id)
			    var req = {};
				var res; 
				req.action = "accepted_student_invitation";
				req.link_id =link_id;
	 	      	$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
				}).done(function (res) {
					data =  $.parseJSON(res);
					window.location=self.location;
		  });
	
	  });
		 $('.reject_invitation').on('click',function() {
			  var link_id=$('#div_link_id').attr('link_id');
			    var req = {};
				var res; 
				req.action = "rejected_student_invitation";
				req.link_id =link_id;
	 	   	$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
				}).done(function (res) {
					data =  $.parseJSON(res);
					return false;
		
		   });
	
		});   
	});
}
	function getUrlParameter(sParam) {
		sParam = sParam.toLowerCase();
		var sPageURL = window.location.search.substring(1).toLowerCase();
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++) 
		{
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam) 
			{
				return sParameterName[1];
			}
		}
	}

});