$(document).ready(function() {
var invitation_link = getUrlParameter("invitation_link");
	$("#signIn").click(function() {
		$('.error-msg').hide();
		var user=$('#user').val();
		var pwd=$('#pwd').val();
		if(user.length<3)
			$('#userError').html("A valid username is required.").show();
		if(pwd.length<8)
			$('#pwdError').html("A valid password is required.").show();
		if(user.length>=3 && pwd.length>=8) {
			req = {
					'action' : 'login',
					'username' : user,
					'password' : pwd,
					'userRole' : $('#userRole').val() 
				};
			req=JSON.stringify(req);
			$.post('api/', req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.valid == false)
					$('#signInError').html(res.reason).show();
				if(res.valid == true && res.userRole == 4) {
					if(typeof(invitation_link) === "undefined"){
								 					window.location = "student/studentProfile.php";
							}else{
							 window.location = "student/invite_course_student.php?invitation_link="+invitation_link;
						
							}
				}
				else if(res.valid == true)
					window.location = "admin/profile.php";
			});
		}
	});
	$('#resetPassword').click(function() {
		$('.error-msg').hide();
		var fuser=$('#fuser').val();
		if(fuser.length<3)
			$('#fuserError').html("A valid username is required.").show();
		if(fuser.length>=3) {
			req = {
					'action' : 'send-reset-password-link',
					'username' : fuser,
					'userRole' : $('#fuserRole').val() 
				};
			req=JSON.stringify(req);
			$.post('api/', req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				$('#forgetError').html(res.message).show();
			});
		}
	});
	
	function getUrlParameter(sParam)
	{
		sParam = sParam.toLowerCase();
		var sPageURL = window.location.search.substring(1).toLowerCase();
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++) 
		{
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam) 
			{
				return sParameterName[1];
			}
		}
	}
});