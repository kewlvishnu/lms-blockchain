<?php
	require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
	require_once "../api/Vendor/ArcaneMind/AccessApi.php";
	AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Settings</title>
		<?php include 'html-template/general/admin-header.html.php'; ?>
	</head>
	
	<body>
	
	<section id="container" class="">
		<!--header start-->
		<?php include 'html-template/general/header.html.php'; ?>
		<?php include 'html-template/general/sidebar.html.php'; ?>
		<!--header end-->
		<!--main content start-->
		<section>
			<section class="wrapper site-min-height">
				<!-- page start-->
				<?php include 'html-template/other/information.html.php'; ?>
				<!-- page end-->
			</section>
		</section>
		<!--main content end-->
		<!--footer start-->
		<?php include 'html-template/general/footer.html.php'; ?>
        <?php include "html-template/pageCheckFooter.php"; ?>
		<!--footer end-->
	</section>
	<?php include 'html-template/general/admin-footer.html.php'; ?>
	<script src="js/custom/setting.js" type="text/javascript"></script>
	</body>
</html>
