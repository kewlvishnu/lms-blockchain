<?php
	require_once "html-template/cookieCheck.php";
	require_once "../api/Vendor/ArcaneMind/Api.php";
	require_once "../api/Vendor/ArcaneMind/AccessApi.php";
	$access = AccessApi::checkAccess();
	require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Profile</title>
	<?php include "html-template/general/admin-header.html.php"; ?>
  </head>
  <body>
	<section id="container" class="">
		<?php include "html-template/general/header.html.php"; ?>
		<?php include 'html-template/general/sidebar.html.php'; ?>
		<!--main content start-->
		<section>
			<section class="wrapper site-min-height">
				<!-- page start-->
				<div class="row">
					<div class="col-md-12">
						<section id="main-content">
							<?php include "html-template/profile/student-profile-cover.html.php"; ?>
							<?php include "html-template/profile/student-profile-bio.html.php"; ?>
							<?php include "html-template/profile/student-profile-achievements.html.php"; ?>
							<?php include "html-template/profile/student-profile-courses.html.php"; ?>
							<?php include "html-template/profile/student-profile-interests.html.php"; ?>
						</section>
					</div>
				</div>
				<!-- page end-->
			</section>
      </section>
      <!--main content end-->
	  <?php include "html-template/general/footer.html.php"; ?>
	</section>
	<?php include "html-template/general/admin-footer.html.php"; ?>
	<?php include "html-template/profile/image-upload-modals.html.php"; ?>
	<script src="../admin/js/custom/scripts.js" type="text/javascript"></script>
	<script src="js/custom/profile.js" type="text/javascript"></script>
  </body>
</html>