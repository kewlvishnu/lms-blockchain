FROM kewlvishnu/deployment-container-php:1.0

COPY . /var/www/html/

RUN rm -rf /var/www/html/api/Vendor/ArcaneMind/Conf.php

RUN  cp -r /var/www/html/api/Vendor/ArcaneMind/conf/develop-dev/Conf.php  /var/www/html/api/Vendor/ArcaneMind/ 

RUN  rm -rf /var/www/html/api/Vendor/ArcaneMind/global.php

RUN  cp -r /var/www/html/api/Vendor/ArcaneMind/conf/develop-dev/global.php    /var/www/html/api/Vendor/ArcaneMind/

RUN  cp -r /var/www/html/api/Vendor/ArcaneMind/conf/develop-dev/cloudFront.php  /var/www/html/api/Vendor/ArcaneMind/


RUN  rm -rf /var/www/html/api/index.php

RUN  cp -r /var/www/html/api/Vendor/ArcaneMind/conf/develop-dev/index.php    /var/www/html/api/


RUN cp -r /var/www/html/api/Vendor/ArcaneMind/conf/develop-dev/confD.php  /var/www/html/api/Vendor/ArcaneMind/


EXPOSE 56000 80
