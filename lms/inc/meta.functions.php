<?php
	$title = 'Integro | Online Courses | Entrance Exam Preparation';
	$description = 'Integro is a platform & marketplace for online courses & entrance exam preparation. We have the best instructors and institutes in the world, we ensure that the content and questions are of the highest quality.';
	$image = 'https://dhzkq6drovqw5.cloudfront.net/fb-arcanemind.jpg';
	switch ($page) {
		case 'signup':
			if ($pageArr[1] == 'course') {
				$title	= "Course in $slug | Integro | Online Courses";
				$courseTitle = "";
				require_once 'api/Vendor/ArcaneMind/Course.php';
				$c = new Course();
				$input = new stdClass();
				$input->courseId = $slug;
				$result = $c->getCourseForMeta($input);
				if($result->status == 1) {
					$courseTitle = $result->details['name'];
					$title = "Course in ".$result->details['name']." | Integro";
					if (isset($result->details['description']) && !empty($result->details['description'])) {
						$description = $result->details['description'];
					}
					$image = $result->details['ogimage'];
				}
			}
			break;
		
		default:
			$title = 'Integro | Online Courses | Entrance Exam Preparation';
			$description = "Integro is a platform & marketplace for online courses & entrance exam preparation.";
			break;
	}
	$fbTitle = $title;
?>