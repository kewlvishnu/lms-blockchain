<?php
	require_once '../config/url.functions.php';
	$global = new stdClass();
	//loading zend framework
	$libPath = "../api/Vendor";
	include_once $libPath . "/Zend/Loader/AutoloaderFactory.php";
	require_once $libPath . "/ArcaneMind/Api.php";
	Zend\Loader\AutoloaderFactory::factory(array(
			'Zend\Loader\StandardAutoloader' => array(
					'autoregister_zf' => true,
					'db' => 'Zend\Db\Sql'
			)
	));
	require_once '../api/Vendor/ArcaneMind/Course.php';
	$c = new Course();
	$res = $c->getCourseCategories();
	if ($res->status == 1) {
		$global->courseCategories = $res->courseCategories;
	}
	$page = "home";
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>LMS | Learning Management System | Arcanemind.com</title>
		<meta name="description" content="Arcanemind Learning Management System (LMS) solutions. We provide innovative online learning solutions for colleges, institutes, coaching classes.">
		<!-- Bootstrap -->
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="shortcut icon" type="image/ico" href="assets/img/favicon.ico" />
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<header>
			<nav class="navbar navbar-dark">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><img src="assets/img/logo.png" alt="Arcanemind Logo"></a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li><a href="http://blog.arcanemind.com" target="_blank">Blog</a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li><a href="<?php echo $sitepath; ?>">Marketplace</a></li>
							<?php
								@session_start();
								if(!(isset($_SESSION['userId']) && !empty($_SESSION['userId']))) {
									?>
									<li><a href="#loginModal" data-toggle="modal">Sign In</a></li>
									<li><a href="<?php echo $sitepath; ?>signup">Sign Up</a></li>
									<?php
								}
							?>
							<?php
								if(isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
									if($_SESSION['userRole'] == 4) {
										?>
										<li><a href="<?php echo $sitepathStudent; ?>">Dashboard</a></li>
										<li><a href="<?php echo $sitepath; ?>signout">Logout</a></li>
										<?php
									}
									else if($_SESSION['userRole'] == 1){
										?>
										<li><a href="<?php echo $sitepathManage; ?>">Dashboard</a></li>
										<li><a href="<?php echo $sitepathLogout; ?>">Logout</a></li>
										<?php
									}
									else if($_SESSION['userRole'] == 2){
										?>
										<li><a href="admin/dashboard.php">Dashboard</a></li>
										<li><a href="<?php echo $sitepath; ?>signout">Logout</a></li>
										<?php
									}
									else if($_SESSION['userRole'] == 3){
										?>
										<li><a href="admin/dashboard.php">Dashboard</a></li>
										<li><a href="<?php echo $sitepath; ?>signout">Logout</a></li>
										<?php
									}
									else if($_SESSION['userRole'] == 5){
										?>
										<li><a href="admin/approvals.php">Dashboard</a></li>
										<li><a href="<?php echo $sitepath; ?>signout">Logout</a></li>
										<?php
									}
									else if($_SESSION['userRole'] == 6){
										?>
										<li><a href="parent/">Dashboard</a></li>
										<li><a href="<?php echo $sitepath; ?>signout">Logout</a></li>
										<?php
									}
								}
							?>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
		</header>
		<section class="feature-image feature-lg feature-image-section-1">
			<div class="container">
				<div class="content text-center">
					<h1 class="hide">Arcanemind LMS solutions</h1>
					<h2>Revolutionize your institute into an innovative learning hub</h2>
					<p class="lead">Adopt the intelligent standards of education</p>
				</div>
				<div class="content text-center">
					<h3>Innovate education. Save time. Expedite learning.</h3>
					<p class="lead">We empower institutes and instructors with tools that Monitor learning and engagement by providing a platform for students and content management.</p>
					<br>
					<p class="lead">Arcanemind's Learning Management System is powered with</p>
					<h3>Artificial Intelligence</h3>
				</div>
			</div>
		</section>
		<section class="feature-image feature-lg feature-image-section-3">
			<div class="container">
				<div class="content text-center">
					<h3>Focus on Building Great Educational Content and Student Services</h3>
					<h3>We will keep improving your LMS <i class="fa fa-smile-o" aria-hidden="true"></i></h3>
				</div>
			</div>
		</section>
		<section class="feature-image feature-sm feature-image-section-4">
			<div class="container">
				<div class="content">
					<div class="row">
						<div class="col-sm-4 col-sm-offset-8">
							<h2>Course Content</h2>
							<p class="lead">Arcanemind provides out of the box content for many courses. We also invite third party content providers to create content that your organization demands <i class="fa fa-smile-o" aria-hidden="true"></i></p>
							<p class="lead">If you are a content provider - please contact us</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="feature-image feature-sm feature-image-section-5">
			<div class="container">
				<div class="content">
					<div class="row">
						<div class="col-sm-4">
							<h2>Test series</h2>
							<p class="lead">We provide quality test material and questions ready to import into your course for private use. And yes, your students can also directly enroll to practice the national and international mock exams.</p>
							<p class="lead">You can also start your very own test series :)</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="feature-image feature-sm feature-slider">
			<div class="content">
				<div class="container">
					<div id="sync2" class="owl-carousel">
						<div class="item">
							<a href="javascript:;">
								<div class="icon-round">
									<i class="fa fa-diamond" aria-hidden="true"></i>
								</div>
								<span class="product-label">Course creation</span>
							</a>
						</div>
						<div class="item">
							<span class="connector"></span>
							<a href="javascript:;">
								<div class="icon-round">
									<i class="fa fa-tachometer" aria-hidden="true"></i>
								</div>
								<span class="product-label">Onboarding</span>
							</a>
						</div>
						<div class="item">
							<span class="connector"></span>
							<a href="javascript:;">
								<div class="icon-round">
									<i class="fa fa-arrow-down" aria-hidden="true"></i>
								</div>
								<span class="product-label">Content import</span>
							</a>
						</div>
						<div class="item">
							<span class="connector"></span>
							<a href="javascript:;">
								<div class="icon-round">
									<i class="fa fa-users" aria-hidden="true"></i>
								</div>
								<span class="product-label">Students connect</span>
							</a>
						</div>
						<div class="item">
							<span class="connector"></span>
							<a href="javascript:;">
								<div class="icon-round">
									<i class="fa fa-commenting-o" aria-hidden="true"></i>
								</div>
								<span class="product-label">Messenger</span>
							</a>
						</div>
						<div class="item">
							<span class="connector"></span>
							<a href="javascript:;">
								<div class="icon-round">
									<i class="fa fa-sitemap" aria-hidden="true"></i>
								</div>
								<span class="product-label">Content category</span>
							</a>
						</div>
						<div class="item">
							<span class="connector"></span>
							<a href="javascript:;">
								<div class="icon-round">
									<i class="fa fa-line-chart" aria-hidden="true"></i>
								</div>
								<span class="product-label">Analytics</span>
							</a>
						</div>
						<div class="item">
							<span class="connector"></span>
							<a href="javascript:;">
								<div class="icon-round">
									<i class="fa fa-bar-chart" aria-hidden="true"></i>
								</div>
								<span class="product-label">Monitor Students</span>
							</a>
						</div>
						<div class="item">
							<span class="connector"></span>
							<a href="javascript:;">
								<div class="icon-round">
									<i class="fa fa-calendar-plus-o" aria-hidden="true"></i>
								</div>
								<span class="product-label">New Semester</span>
							</a>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div id="sync1" class="owl-carousel">
						<div class="item">
							<div class="row">
								<div class="col-md-6">
									<div class="item-content">
										<h3>Course creation</h3>
										<p class="desc">A courses can be created for each program run by your institute or organization. Each course can comprise of subjects, which are subsets of the given program.</p>
										<p class="desc">Students are enrolled into the course so that they have access to study material for each subjects. However Arcanemind LMS provides the flexibility to allocate specific subjects to certain student – when there are electives or optional subjects. </p>
									</div>
								</div>
								<div class="col-md-6">
									<img src="assets/img/slider/course-creation.png" alt="Course creation" class="btn-block">
								</div>
							</div>
						</div>
						<div class="item">
							<div class="row">
								<div class="col-md-6">
									<div class="item-content">
										<h3>Onboarding</h3>
										<p class="desc">Each subject is administered by an instructor. There can be one or multiple instructors assigned to a given subject. Instructors are first invited to the institute and then their allocations to the subjects can be managed by the admin.</p>
									</div>
								</div>
								<div class="col-md-6">
									<img src="assets/img/slider/on-boarding.png" alt="Onboarding" class="btn-block">
								</div>
							</div>
						</div>
						<div class="item">
							<div class="row">
								<div class="col-md-6">
									<div class="item-content">
										<h3>Content import</h3>
										<p class="desc">Once the instructors are assigned to the subjects, they can create a fully customized course or even import from our vast database of popular course material. We also invite third party content providers, who offer amazing content to institutions.</p>
									</div>
								</div>
								<div class="col-md-6">
									<img src="assets/img/slider/content-import.png" alt="Content import" class="btn-block">
								</div>
							</div>
						</div>
						<div class="item">
							<div class="row">
								<div class="col-md-6">
									<div class="item-content">
										<h3>Students connect</h3>
										<p class="desc">Once the content is ready to use students can be invited to the course. The process of student enrollment is extremely streamlined and can be done in less than an hour. Once the students are registered to the course, the administrator can make specific subject allocations.</p>
										<p class="desc">Of course content can be added and modified even when students are consuming the content.</p>
									</div>
								</div>
								<div class="col-md-6">
									<img src="assets/img/slider/students-connect.png" alt="Students connect" class="btn-block">
								</div>
							</div>
						</div>
						<div class="item">
							<div class="row">
								<div class="col-md-6">
									<div class="item-content">
										<h3>Messenger</h3>
										<p class="desc">When students have any questions during their study, they can connect with the professor immediately wherever they are with the help of our Messenger service. Subject based conversations keep the context clear.</p>
									</div>
								</div>
								<div class="col-md-6">
									<img src="assets/img/slider/messenger.png" alt="Messenger" class="btn-block">
								</div>
							</div>
						</div>
						<div class="item">
							<div class="row">
								<div class="col-md-6">
									<div class="item-content">
										<h3>Content category</h3>
										<p class="desc">Instructors can add video files, pdf files, presentation files, text content and downloadable content among others.</p>
										<p class="desc">Exams and Assignments can also be created - these can be objective exams, theoretical exams, paper based exams and OMR based objective exams. Instructors can even manage the scores manually for the exams.</p>
										<p class="desc">Students can complete the exams and assignments from the institute or home - there are some cool features that keep copying/cheating at bay. </p>
										<p class="desc">Have you heard of Artificial Intelligence? ;)</p>
									</div>
								</div>
								<div class="col-md-6">
									<img src="assets/img/slider/content-category.png" alt="Content category" class="btn-block">
								</div>
							</div>
						</div>
						<div class="item">
							<div class="row">
								<div class="col-md-6">
									<div class="item-content">
										<h3>Analytics</h3>
										<p class="desc">Students can monitor their performance with the help of top notch detailed analytics, as well as high level analytics. This will help them know their realtime class performance and also boost motivation to reattempt and improve their grade performance. - We tell them where they should focus.</p>
										<p class="desc">Although we encourage “Learning by making mistakes” - encourage multiple attempts, Instructors can control the number of attempts on exams and assignments. :)</p>
									</div>
								</div>
								<div class="col-md-6">
									<img src="assets/img/slider/analytics.png" alt="Analytics" class="btn-block">
								</div>
							</div>
						</div>
						<div class="item">
							<div class="row">
								<div class="col-md-6">
									<div class="item-content">
										<h3>Monitor Students</h3>
										<p class="desc">Instructors and Parents/Guardians can monitor the students’ performance, and also be able to know the weak spots in their learning. They can get a graphical overview of the performance with respect to the class and provide detailed consultation to the students on how they can improve in their studies.</p>
										<p class="desc">You can never be in more control!</p>
									</div>
								</div>
								<div class="col-md-6">
									<img src="assets/img/slider/monitor-students.png" alt="Monitor Students" class="btn-block">
								</div>
							</div>
						</div>
						<div class="item">
							<div class="row">
								<div class="col-md-6">
									<div class="item-content">
										<h3>New Semester</h3>
										<p class="desc">Once you create the content you have it forever in your archives. You are just a click away to import your content to the new semester program. You can also maintain your students’ performance details history.</p>
										<p class="desc">It's unbelievable how many hours you will save while preparing for your next program. :)</p>
									</div>
								</div>
								<div class="col-md-6">
									<img src="assets/img/slider/new-semester.png" alt="New Semester" class="btn-block">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<footer class="footer-dark text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-6">
						<aside class="footer-nav-menu">
							<h4 class="text-uppercase">About</h4>
							<div>
								<ul class="list-unstyled">
									<li><a href="<?php echo $sitepath; ?>about">About Us</a></li>
									<li><a href="<?php echo $sitepath; ?>contact">Contact Us</a></li>
								</ul>
							</div>
						</aside>
					</div>
					<div class="col-xs-6">
						<aside class="footer-nav-menu">
							<h4 class="text-uppercase">Terms</h4>
							<div>
								<ul class="list-unstyled">
									<li><a href="<?php echo $sitepath; ?>policies/terms">Terms Of Use</a></li>
									<li><a href="<?php echo $sitepath; ?>policies/privacy">Privacy Policy</a></li>
								</ul>
							</div>
						</aside>
					</div>
				</div>
				<nav class="navbar">
					<div class="container text-center">
						<p>&copy; Copyright 2016 | Arcanemind Technologies Pvt. Ltd. | All Rights Reserved</p>
					</div><!-- /.container-fluid -->
				</nav>
			</div>
		</footer>
		<!-- Modal -->
		<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="loginModalLabel">Login Form</h4>
					</div>
					<div class="modal-body">
						<div class="login-form clearfix">
							<form role="form">
								<div class="form-group">
									<select id="userRole" class="form-control">
										<option value="4">I am a student</option>
										<option value="2">I am a professor/tutor</option>
										<option value="1">I represent an institute</option>
									</select>
								</div>
								<div class="form-group">
									<input type="text" placeholder="Enter username or email" id="user" class="form-control">
									<div id="userError" class="error-msg"></div>
								</div>
								<div class="form-group">
									<input type="password" placeholder="Password" id="pwd" class="form-control">
									<div id="pwdError" class="error-msg"></div>
								</div>
								<div class="checkbox pull-right">
									<label>
										<input type="checkbox" id="remember"> Remember me on this computer
									</label>
								</div>
								<div class="pull-left">
									<div id="signInError" class="error-msg"></div>
									<button id="signIn" class="btn btn-primary" type="submit">Log In</button>
								</div>
							</form>
						</div>
					</div>
					<div class="modal-footer">
						<div class="pull-left">
							<a data-dismiss="modal" data-toggle="modal" href="#forgetPasswordModal">Forgot Password?</a>
						</div>
						<div class="pull-right">
							<span>Don't have an account?</span>
							<a class="btn btn-danger" href="<?php echo $sitepath; ?>signup">Register</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="forgetPasswordModal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close"  data-dismiss="modal">&times;</button>
						<h3 class="modal-title">Forgot Password?</h3>
					</div>
					<div class="modal-body">
						<div class="login-form clearfix">
							<form role="form">
								<input type="hidden" id="fuserRole" value="4" />
								<div class="form-group">
									<input type="text" class="form-control" id="fuser" placeholder="Enter Email or Username">
									<div class="error-msg" id="fuserError"></div>
								</div>
								<div class="pull-left">
									<div class="error-msg" id="forgetError"></div>
									<a class="btn btn-success" id="resetPassword">Reset</a>
								</div>
							</form>
						</div>
					</div>
					<div class="modal-footer">
						<div class="pull-left">
							<a data-dismiss="modal" data-toggle="modal" href="#loginModal">Login</a>
						</div>
						<div  class="pull-right">
							<span>Don't have an account?</span>
							<a href="signup-student.php#student" class="btn btn-primary">Register</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php @include_once('../analytics.php'); ?>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
		<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<script src="assets/js/modernizr.js"></script>
		<script src="assets/js/owl.carousel.min.js"></script>
		<script src="assets/js/main.js"></script>
		<?php require_once('jsphp/global.js.php'); ?>
		<?php require_once('jsphp/login.js.php'); ?>
	</body>
</html>