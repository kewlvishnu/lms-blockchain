<script type="text/javascript">
var roleuser=0;
$(document).ready(function() {
	$("#btnInstituteSignin").click(function(e) {
		e.preventDefault();
		var disError 				= $("#instituteSignin").find('.help-block');
		var frmError 				= disError.closest('.form-group');
		var email=$('#inputSEmailAddress');
		var pwd=$('#inputSPassword');
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!email.val()) {
			disError.html("Email is compulsory!");
			frmError.removeClass('has-success').addClass('has-error');
			email.focus();
		/*} else if(!regex.test(email.val())) {
			disError.html("Please enter a valid email!");
			frmError.removeClass('has-success').addClass('has-error');
			email.focus();*/
		} else if(!pwd.val()) {
			disError.html("Password is compulsory!");
			frmError.removeClass('has-success').addClass('has-error');
			pwd.focus();
		} else if(pwd.val().length<6 || pwd.val().length>20) {
			disError.html("Password needs to be between 6-20 characters.");
			frmError.removeClass('has-success').addClass('has-error');
			pwd.focus();
		} else {
			var req = {
					'action'	: 'login',
					'username'	: email.val(),
					'password'	: pwd.val(),
					'userRole'	: $('#userRole').val(),
					'remember' : (($('#optionSRemember').prop("checked"))?1:0)
				};
			if (slug) {
				req.courseId = slug;
			}
			req=JSON.stringify(req);
			$.post(sitePath+'api/', req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				//console.log(res);
				if(res.valid == false) {
					if(res.reset == 1) {
						toastr.error(res.reason + '<a href="#" class="resend-link">Resend email</a>');
						addResetListener(res.userId);
					}
					else
						toastr.error(res.reason);
				}
				if(res.valid == true && res.userRole == 4) {
					//console.log(sitePath+"student/");
					window.location = sitePath+"student/";
				}
				else if(res.valid == true && (res.userRole == 1 || res.userRole == 2))
				{
					//console.log(sitePath+"manage/");
					window.location = sitePath+"manage/";
				}
				else if(res.valid == true)
				{
					//console.log(sitePath+"admin/dashboard.php");
					window.location = sitePath+"admin/dashboard.php";
				}
			});
		}
	});
	$('#firesetPassword').click(function() {
		$('.error-msg').addClass("hide");
		var fuser=$('#fiuser').val();
		if(fuser.length < 6)
			$('#fiuserError').html("A valid username is required.").removeClass("hide");
		if(fuser.length >= 6) {
			var req = {};
			req.action = 'send-reset-password-link';
			req.username = fuser;
			req.userRole = $('#fiuserRole').val();
			req = JSON.stringify(req);
			$.post(sitePath+'api/', req).success(function(resp) {
				res = jQuery.parseJSON(resp);
				$('#fiforgetError').html(res.message).removeClass("hide");
			});
		}
	});
	
	function addResetListener(userId) {
		$('.resend-link').off('click');
		//resend verification link
		$('.resend-link').on('click', function() {
			var req = {};
			var res;
			req.id = userId;
			if(req.id == undefined)
				toastr.error("Some error occurred contact admin.");
			req.action = 'resend-verification-link';
			$.ajax({
				'type'	:	'post',
				'url'	:	sitePath+'api/',
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else
					toastr.success("Verification mail sent. Please check your mailbox.");
			});
		});
	}
	
});
</script>