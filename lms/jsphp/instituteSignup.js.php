<script type="text/javascript">
$(document).ready(function(){
    $('#userRole').change(function(){
        var userType = $(this).val();
        //console.log(userType);
        if(userType == "1") {
            $('.js-professor').addClass('hide');
            $('.js-institute').removeClass('hide');
        } else if(userType == "2") {
            $('.js-institute').addClass('hide');
            $('.js-professor').removeClass('hide');
        }
    });
	$(".form-3d .form-control").keyup(function(e){
		if(e.which!=9) {
			var disError = $(this).closest("form").find(".help-block");
			disError.html('').closest('.form-group').removeClass('has-success').removeClass('has-error');
		}
	});
    var formProfessor = {
        'type' : "Professor",
        'userInstituteTypes' : [ ],
        'stucturedInsTypes' : { },
        'addressCountryId' : 1,
        'userSchoolBoard' : 0,
        'userSchoolClasses' : 0,
        'agreeOn1' : 0,
        'agreeOn2' : 0,
        'action' : 'register-short',
        'userType' : 'professor',
        'userRole' : 2,
        'gender' : 0
    };
    var formInstitute = {
        'type' : "Institute",
        'userInstituteTypes' : [ ],
        'stucturedInsTypes' : { },
        'addressCountryId' : 1,
        'userSchoolBoard' : 0,
        'userSchoolClasses' : 0,
        'agreeOn1' : 0,
        'agreeOn2' : 0,
        'action' : 'register-short',
        'userType' : 'institute',
        'userRole' : 1
    };
	function emailValidation(element,user) {
		var frmGroup=element.closest('.form-group');
		var disError=element.closest('form').find('.help-block');
		var frmError=disError.closest('.form-group');
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(regex.test(element.val())) {
			frmGroup.removeClass('has-error');
			var req = {};
			req.action = 'validate-email';
			req.email = element.val();
			req.role = user.userRole;
			req=JSON.stringify(req);
			$.post(ApiEndpoint, req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.valid == false) {
					frmGroup.removeClass('has-success').addClass('has-error');
					disError.html("Email address already registered. <a href='"+sitepathLogin+"'>Click here</a> to login.");
					frmError.removeClass('has-success').addClass('has-error');
					//element.focus();
				}
				else {
					frmGroup.removeClass('has-error').addClass('has-success');
					disError.html("");
					frmError.removeClass('has-success').removeClass('has-error');
					return true;
				}
			});
		}
		else {
			frmGroup.removeClass('has-success').addClass('has-error');
			disError.html("Doesn't seem to be a valid email address.");
			frmError.removeClass('has-success').addClass('has-error');
			//element.focus();
		}
		return false;
	}
	$("#inputEmailAddress").blur(function(){
		if ($('#userRole').val() == "1") {
            emailValidation($(this),formInstitute);
        } else if ($('#userRole').val() == "2") {
            emailValidation($(this),formProfessor);
        };
	});
	$("#btnInstituteSignup").click(function(){
		var disError 				= $("#instituteSignup").find('.help-block');
		var frmError 				= disError.closest('.form-group');
        var userType                = $('#userRole').val();
        if (userType == "1") {
            var inputInstituteName  = $("#inputInstituteName");
        } else {
            var inputFirstName      = $("#inputFirstName");
            var inputLastName       = $("#inputLastName");
        }
		var inputEmailAddress		= $("#inputEmailAddress");
		var inputPassword			= $("#inputPassword");
		if((userType == "2") && inputFirstName.val().length<3) {
            disError.html("First Name is invalid.");
            frmError.removeClass('has-success').addClass('has-error');
            inputFirstName.focus();
        } else if((userType == "2") && inputLastName.val().length<3) {
            disError.html("Last Name is invalid.");
            frmError.removeClass('has-success').addClass('has-error');
            inputLastName.focus();
        } else if(userType == "1" && inputInstituteName.val().length<3) {
            disError.html("Institute Name is invalid.");
            frmError.removeClass('has-success').addClass('has-error');
            inputInstituteName.focus();
        } else if($(inputEmailAddress).closest('.form-group').hasClass('has-error') ||
                    (!$(inputEmailAddress).closest('.form-group').hasClass('has-error') && !$(inputEmailAddress).closest('.form-group').hasClass('has-success'))
                ) {
            if (userType == "1") {
                emailValidation(inputEmailAddress,formInstitute);
            } else {
                emailValidation(inputEmailAddress,formProfessor);
            }
        } else if(inputPassword.val().length<6 || inputPassword.val().length>20) {
			disError.html("Password needs to be between 6-20 characters.");
			frmError.removeClass('has-success').addClass('has-error');
			inputPassword.focus();
		} else {
            $("#btnInstituteSignup").attr('disabled', true);
			if (userType == "2") {
                formProfessor.firstName       = inputFirstName.val();
                formProfessor.lastName        = inputLastName.val();
                formProfessor.email           = inputEmailAddress.val();
                formProfessor.password        = inputPassword.val();
                formProfessor                 = JSON.stringify(formProfessor);
                $.post(ApiEndpoint, formProfessor).success(function(resp) {
                    $("#btnInstituteSignup").attr('disabled', false);
                    formProfessor = JSON.parse(formProfessor);
                    res=jQuery.parseJSON(resp);
                    if(res.status == 1) {
                        window.location=sitePath+"signup/verify/" + res.userId;
                    }
                    if(res.status == 0)
                        toastr.error(res.message);
                });
            } else {
                formInstitute.instituteName   = inputInstituteName.val();
                formInstitute.email           = inputEmailAddress.val();
                formInstitute.password        = inputPassword.val();
                formInstitute                 = JSON.stringify(formInstitute);
                $.post(ApiEndpoint, formInstitute).success(function(resp) {
                    $("#btnInstituteSignup").attr('disabled', false);
                    formInstitute = JSON.parse(formInstitute);
                    res=jQuery.parseJSON(resp);
                    if(res.status == 1) {
                        window.location=sitePath+"institute-instructor-signup/verify/" + res.userId;
                    }
                    if(res.status == 0)
                        toastr.error(res.message);
                });
            }
		}
		return false;
	});
});
</script>