<script type="text/javascript">
var userId = slug;
function getVerifiedStatus() {
	var req = {};
	var res;
	req.userId = userId;
	req.action = 'get-verified-status';
	$.ajax({
		'type'	:	'post',
		'url'	:	sitePath+'api/',
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1){
			if(res.validated == 1){
				window.location = sitePath;
			} else {
				if(res.userRole == 4){
					$('.js-student').removeClass('hide');
				} else {
					$('.js-institute').removeClass('hide');
				}
			}
		}
		else {
			toastr.error(res.message);
		}
	});
}
$(document).ready(function(){
	<?php if (isset($slug) && !empty($slug)) { ?>
		addResetFListener(<?php echo $slug; ?>);
	<?php } ?>

	function disableBtn(fewSeconds) {
		$(".button-2min").removeClass('hide');
		setTimeout(function(){
			$(".button-2min").addClass('hide');
			$('.resend-link1').attr('disabled', false);
		}, fewSeconds*1000);
	}
	
	function addResetFListener(userId) {
		$('.resend-link1').off('click');
		//resend verification link
		$('.resend-link1').on('click', function() {
			var disabled = $(this).attr('disabled');
			if (!disabled) {
				$('.resend-link1').attr('disabled', true);
				var req = {};
				var res;
				req.id = userId;
				if(req.id == undefined)
					toastr.error("Some error occurred contact admin.");
				req.action = 'resend-verification-link';
				$.ajax({
					'type'	:	'post',
					'url'	:	sitePath+'api/',
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 0){
						toastr.error(res.message);
						$('.resend-link1').attr('disabled', false);
					}
					else {
						toastr.success("Verification mail sent. Please check your mailbox.");
						disableBtn(120);
					}
				});
			}
		});
	}
});
</script>