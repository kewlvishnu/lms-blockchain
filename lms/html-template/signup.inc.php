<section class="main-body bg-trans-bg">
	<article class="container">
		<div class="col-md-6 col-md-offset-3">
			<form class="form-3d signup" id="studentSignup">
				<div class="text-center">
					<h3 class="form-title">Sign Up</h3>
				</div>
				<div class="form-group">
					<div class="help-block text-center"></div>
				</div>
				<div class="form-group">
					<label class="text-uppercase" for="inputSFirstName">First Name</label>
					<input type="text" class="form-control trans-control" id="inputSFirstName" name="inputSFirstName" placeholder="First Name">
				</div>
				<div class="form-group">
					<label class="text-uppercase" for="inputSLastName">Last Name</label>
					<input type="text" class="form-control trans-control" id="inputSLastName" name="inputSLastName" placeholder="Last Name">
				</div>
				<div class="form-group">
					<label class="text-uppercase" for="inputSEmailAddress">Email address</label>
					<input type="email" class="form-control trans-control" id="inputSEmailAddress" name="inputSEmailAddress" placeholder="Email">
				</div>
				<div class="form-group">
					<label class="text-uppercase" for="inputSPassword">Password</label>
					<input type="password" class="form-control trans-control" id="inputSPassword" name="inputSPassword" placeholder="Password">
				</div>
				<button type="submit" class="btn btn-color2 btn-block text-uppercase" id="btnStudentSignup">Sign Up</button>
				<div class="text-center">
					<small>By signing up, you agree to our <a href="<?php echo $sitepathPolicyTerms; ?>">Terms of Use</a> and <a href="<?php echo $sitepathPolicyPrivacy; ?>">Privacy Policy</a></small>
				</div>
				<hr class="style-two">
				<div class="text-center">
					Already have an account? <a href="<?php echo $sitepathLogin; ?>">Login</a>
				</div>
			</form>
		</div>
	</article>
</section>