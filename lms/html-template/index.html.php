<?php
	require_once('includes/header.php');

	switch ($page) {
		
		case 'home':
			require_once('home.inc.php');
			break;

		case 'policiesTerms':
			require_once('policiesTerms.inc.php');
			break;

		case 'policiesPrivacy':
			require_once('policiesPrivacy.inc.php');
			break;

		case 'contact':
			require_once('contact.inc.php');
			break;

		case 'about':
			require_once('about.inc.php');
			break;

		case 'integroAssets':
			require_once('integroAssets.inc.php');
			break;

		case 'logout':
			require_once('logout.inc.php');
			break;

		case 'verify':
			require_once('verify.inc.php');
			break;

		case 'signup':
			require_once('signup.inc.php');
			break;

		case 'signupVerify':
			require_once('signupVerify.inc.php');
			break;

		case 'signupComplete':
			require_once('signupComplete.inc.php');
			break;

		case 'signin':
			require_once('signin.inc.php');
			break;

		case 'instituteSignup':
			require_once('instituteSignup.inc.php');
			break;

		case 'instituteSignin':
			require_once('instituteSignin.inc.php');
			break;

		case 'signinVerify':
			require_once('signinVerify.inc.php');
			break;

		default:
			echo "There was some error";
			break;
	}

	require_once('includes/footer.php');
?>