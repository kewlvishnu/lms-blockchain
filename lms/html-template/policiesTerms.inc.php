<section class="feature-image feature-lg feature-image-section-contact">
	<div class="container">
		<h1>Terms and Conditions</h1>
		<hr class="style-six">
		<h4><strong>1. Definitions</strong></h4>
		<div class="well">
			<p><strong>Account:</strong> Individual user's designated authorized access to the Site which is accessible through a user designated Integro.io and password. The Account will identify registered Courses and payment information</p>
			<p><strong>Company:</strong> Integro.io and its affiliates, officers, directors, and employees</p>
			<p><strong>Content:</strong> Content may be Company Content or User Content. It is the substantive information,  educational materials, documents,  audio, video, and all other materials posted to the Site, including  all software, technology, designs, materials, information, communications, text, graphics, links, electronic art, animations, illustrations, artwork, audio clips, video clips, photos, images, reviews, ideas, and other data or copyrightable materials or content, including the selection and arrangements there of </p>
			<p><strong>Course(s):</strong> Our and our user's proprietary online classes, products and services which include facilitating and hosting live and recorded online audio and video conferences</p>
			<p><strong>Other Sites:</strong> Third-party websites that may be linked on Our Site</p>
			<p><strong>Students:</strong> Individual Users who are participating in Our Courses.</p>
			<p><strong>Instructors:</strong> Individual Users who are independently contracted Instructors, coaches, mentors and instructors who provide live and recorded teaching, instruction, tutoring, mentoring and/or coaching services through Our proprietary online classes platform</p>
			<p><strong>Institutes:</strong> Organizational users who are independently contracted Institutes, coaches, mentors and instructors who provide live and recorded teaching, instruction, tutoring, mentoring and/or coaching services through Our proprietary online classes platform.</p>
			<p><strong>Terms:</strong> These Terms and Conditions under which You are authorized to use Our Site</p>
			<p><strong>Trademarks:</strong> The trademarks, service marks, and logos displayed on the site, whether registered or unregistered.</p>
			<p><strong>US, Our, We:</strong> The Company.</p>
			<p><strong>Use of Content:</strong> Your permission to Us allowing US a license to use, reproduce, distribute, publicly perform, offer, market and otherwise use and exploit Your Content on the Site and sub-license it to Instructors and Students for these purposes directly or through third parties</p>
			<p><strong>You, Your:</strong> Individual Users, whether Students, or Instructors, as well as any company, or organization (if any) through which the individual user is accessing the Site</p>
		</div>
		<h4><strong>2. Introduction</strong></h4>
		<div class="well">
			<p>By clicking on the accept button below, and/or by using any of the products or services offered through Our Site, You agree to be legally bound to the Terms described herein</p>
			<p>If You do not agree to all the terms and conditions of this agreement, click the "cancel" button and do not use Our Site.  Permission for YOU to access and/or use Our Site is expressly conditioned upon Your agreement to all the terms and conditions herein. No alterations or amendments will be accepted</p>
		</div>
		<h4><strong>3. Privacy</strong></h4>
		<div class="well">
			<p>Any personal information submitted in connection with Your use of Our Site is subject to Our Privacy Policy which is viewable in Privacy Policy document</p>
		</div>
		<h4><strong>4. General</strong></h4>
		<div class="well">
			<p>Our products and services enable users to connect with Institutes and Instructors who provide live and recorded teaching, instruction, tutoring, mentoring and/or coaching services through Our proprietary online Courses. Our products and services include facilitating and hosting Courses, providing supporting materials, and promoting feedback from Instructors and Students who participate in Our proprietary platform and process</p>
			<p>We reserve the right to revise these Terms in Our sole discretion at any time by posting the changes on the Site. Changes are effective thirty (30) days after posting. Your continued use of Our Site after the changes become effective means that You accept those changes.  It is Your obligation to stay aware of the latest version of the Terms by visiting this Site</p>
			<p>We may modify Our products and services or discontinue them at any time</p>
			<p>You are solely responsible for all connection fees, service fees, telephone costs, data charges and/or any other fees and/or costs associated with Your access to and use of Our Site. You are responsible for obtaining and maintaining any and all telephone, computer hardware, and other equipment required for such access and use</p>
			<p>If You choose to access or use any product or service requiring payment of a fee, then You agree to pay and will be responsible for that fee and all taxes associated with such access or use.  You hereby represent and warrant that You are authorized to supply credit card information and hereby authorize charges to Your credit card on an ongoing regular basis as necessary to pay the fees as they are due.  All fees are quoted in Indian Rupees and U.S. Dollars, as applicable. If Your payment method fails or Your account is past due, then We may collect fees owed using other collection mechanisms. This may include charging other payment methods on file with us and/or retaining collection agencies and legal counsel. You further agree to reimburse Us for any costs or expenses that We entail, including attorney's fees, filing costs, service costs and collection costs</p>
			<p>Additionally, We may block Your access to Our Site and any or all of Our products and services pending payment of any amounts due by You to Us</p>
			<p>You warrant that use, access and other activities relating to the Site is in compliance with all applicable laws and regulations, including, without limitations, laws relating to copyright and other intellectual property use, and to privacy and personal identity. In connection with Your use of the Site and the products and services, You warrant that you will not provide inaccurate or knowingly false information; copy, distribute, modify, reverse engineer, deface, tarnish, mutilate, hack, or interfere with the products or services or the operation of the Site; frame or embed the Site or products or services on it; impersonate another person or gain unauthorized access to another person's account; introduce any virus, worm, spy-ware or any other computer code, file or program that may or is intended to damage or hijack the operation of any hardware, software or telecommunications equipment, or any other aspect of the products or services or operation of the Site; scrape, spider, use a robot or other automated means of any kind to access the Products</p>
		</div>
		<h4><strong>5. General Disclaimer</strong></h4>
		<div class="well">
			<p>You expressly understand and agree that the Site is a portal for Institutes, Instructors and Students to connect with one another. Institutes and Instructors are not Our employees. We are not responsible or liable for any interactions involved between the Instructors and Students. We are not responsible for disputes, claims, losses, injuries, or damage of any kind that might arise out of or relate to conduct of Instructors or Students, including, but not limited to, any user's reliance upon any information provided by a Institute and Instructor</p>
			<p>We do not control any of the Content posted on the Site and, as such, do not guarantee in any manner the reliability, validity, accuracy or truthfulness of such Content. You understand that by using the products and services that You may be exposed to Content that You consider offensive, indecent, or objectionable. We have no responsibility to keep such content from You and no liability for Your access or use of any Content</p>
			<p>Through the use and access of this Site You may have further access to links to third-party websites ("Other Sites"), either directly or through Courses or Institutes or Instructors. We do not endorse any of these Other Sites and do not control them in any manner. We do not assume any liability associated with Other Sites. You need to take appropriate steps to determine whether accessing an Other Site is appropriate. You are responsible for protecting Your personal information and privacy on any Other Site</p>
		</div>
		<h4><strong>6. Registration</strong></h4>
		<div class="well">
			<p>You are required to register with Us and obtain an Account, user name and password.  We may use Your information to assist you with finding Content related to your interests. We may also use your information so that customer service may better help you with questions that you may have and also that We may better manage Our network.  It is Your obligation and responsibility to safeguard Your Account information. You agree to accurately maintain Your Account information. Notify us immediately if you suspect unauthorized use of Your Account or any other breach of security. You must log out of Your Account at the end your session. We are not responsible for any loss or damage due to Your failure to comply with the foregoing or as a result the unauthorized use of Your Account prior to You notifying us of such unauthorized access</p>
			<p>You may not give access to Your Account to any other person and You may not use another person's Account.  You are responsible for the online conduct of anyone to whom you allow access to Your Account, including minors</p>
		</div>
		<h4><strong>7. Your Obligations</strong></h4>
		<div class="well">
			<p>You may only access Our Site for lawful purposes. You are responsible for knowing and adhering to any and all laws, rules, and regulations pertaining to Your use of the products and services. You agree not to recruit, solicit, or contact in any form Instructors or Students outside of this Site without their written permission, which may be withheld in their sole discretion and OUR understood agreement. You assume any and all risks from any meetings or contact between You and any Institute or Instructor or Student or other user of the Site</p>
		</div>
		<h4><strong>8. Obligations of Students</strong></h4>
		<div class="well">
			<p>If You are a Student accessing this Site and/or engaging a Instructor on this Site, You represent, warrant and covenant that:</p>
			<p>You have read, understood, and agree to be bound by the pricing information (see the Pricing section below) before using the Site or registering for a Course;</p>
			<p>You will not upload, post or otherwise transmit any unsolicited or unauthorized advertising, promotional materials, junk mail, spam, chain letters, pyramid schemes or any other form of solicitation (commercial or otherwise) through the Site;</p>
			<p>You will not post any defamatory, inappropriate, false, racist, sexist, pornographic, hateful, misleading, infringing, offensive or libelous content;</p>
			<p>You will not publicly perform, distribute, publicly display, reproduce, communicate to the public, create derivative works from or otherwise use and exploit any of Our Content or the Content of Instructors or other Students except as permitted by these Terms or the relevant Instructor or other Student as applicable; You agree to hold Us harmless and indemnify Us against any claims of a third party to Your Content, or Your use of other person's Content.</p>
			<p>You will not disclose any personal information to a Instructor and otherwise will assume responsibility for controlling how Your personal information is disclosed or used, including, without limitation, taking appropriate steps to protect such information; and</p>
			<p>You will not solicit personal information from any Instructor or Other Student</p>
			<p>You agree to hold us harmless and indemnify Us against any claims of a third party to resulting from Your violations of these Terms</p>
			<p>You are 18 Years of Age or older, or if You are under the age of 18, You have obtained parental or legal guardian consent before using the Site, contacting a Instructor, or registering for a Course </p>
		</div>
		<h4><strong>9. Obligations of Institute and Instructors</strong></h4>
		<div class="well">
			<p>If You are providing any teaching, instruction, tutoring, mentoring and/or coaching services through Our Site you are a Instructor and the following additional terms and conditions apply to you. You represent, warrant and covenant that:</p>
			<p>You are subject to Our approval which We may grant or deny in Our sole discretion;</p>
			<p>You need to visit registration page and complete the Institute/Instructor enrollment form.  You must read and agree to the Pricing terms if you are charging fees to users</p>
			<p>You are responsible for any and all Content that you post.  You own or have licenses, rights, consents, and permissions, and You have the authority to authorize Us to reproduce, distribute, publicly perform (including by means of a digital audio transmission), publicly display, communicate to the public, promote, market and otherwise use and exploit any of Your Content on and through Our site.  Your Content does not infringe or misappropriate any other person's property or rights to Your Content. You agree to hold Us harmless and indemnify Us against any claims of a third party to Your Content</p>
			<p>You have the necessary credentials and expertise, education, training, experience, knowledge, and skill to teach and offer the services You offer on and through the Site;</p>
			<p>You will not post any inappropriate, offensive, racist, hateful, sexist, pornographic, false, misleading, infringing, defamatory or libelous content;</p>
			<p>You will not upload, post or otherwise transmit any unsolicited or unauthorized advertising, promotional materials, junk mail, spam, chain letters, pyramid schemes or any other form of solicitation (commercial or otherwise) through the Products or to users;</p>
			<p>You will not use any of Our Content or the Content of other Institute/Instructors for any business other than for providing tutoring, teaching and instructional services to Students on the Site;</p>
			<p>You will not copy, modify or distribute Our Content or the Content of other Institutes/Instructors without written permission;</p>
			<p>You will not interfere with or otherwise prevent other Institute/Instructors from providing their services; nor will you demean or undermine other Institutes/Instructors;</p>
			<p>You will accurately maintain Your account information including enrollment information; </p>
			<p>You will promptly engage with Students seeking Your services and ensure a quality of service commensurate with generally accepted instructional standards; </p>
			<p>You agree to hold Us harmless and indemnify US against any claims of a third party to resulting from Your violations of these Terms</p>
			<p>You are over the age of 18 or if not a third party parent or legal guardian has agreed to the terms of this Instructor Agreement and will assume responsibility and liability for Your performance and compliance hereunder</p>
		</div>
		<h4><strong>10. Content, Licenses &amp; Permissions</strong></h4>
		<div class="well">
			<p>You hereby grant Us a non-exclusive Use of Content license.  However, You have the right to remove Your Content, in whole or in part, from the Site at any time. If You remove Your Content the foregoing Use of Content license and rights will cease sixty (60) days after such removal, provided, however, that any rights given to Instructors or Students prior to that time will continue</p>
			<p>We grant You a limited, non-exclusive, non-transferable license to access and use the Site and the Site Content for which You have paid all required fees. This license is solely for Your personal, non-commercial, educational use.  This license may only be used in accordance with these Terms and any conditions or restrictions placed on Content by Instructors in regard to their Courses. All other uses are expressly prohibited unless otherwise agreed to in writing. Reproduction, redistribution, transmission, assignment, transference, broadcasting, or any other use of the Content or derivative works thereof, is strictly prohibited unless We give You express written permission. Your use of any Content is by license. You have no ownership rights of any kind in any Content other than your own. Any statement or suggestion otherwise from anyone other than a official of the Company is without authority and a violation of these Terms</p>
			<p>We may record all or any part of any Course including your participation in the Course. We may use these recordings for quality control, marketing, promoting, demonstrating or operating the Site and the Products. You agree that We may use of Your name, image, voice or likeness in connection for the above purposes, without limitation. You waive any and all rights of publicity, privacy, or any other rights of a similar nature</p>
			<p>We adhere to all copyright, privacy, defamation and other laws relating to content and information and will not tolerate any violation of such laws. Notwithstanding the foregoing, the company does not screen Content posted by Instructors or Students. You acknowledge that access and use is at your own risk and the company shall have no liability for such use. In particular, no review or posting or appearance of the submitted content on the site or through the products is intended to act as an endorsement or representation that any content is free of violation of any copyright, privacy or other laws or will suit a particular purpose or be accurate or useful. If You believe that Content violates any law or regulation or is inaccurate or poses any risk whatsoever to a third party it is Your responsibility to take such steps You deem necessary to correct the situation. If You believe that Content of a third party or any Content violates any laws or regulations, including, without limitation, any copyright laws, You should report it to the Us by emailing to copyright@integro.io</p>
			<p>All rights not expressly granted in these Terms are retained by the Content owners and these Terms do not grant any implied licenses</p>
		</div>
		<h4><strong>11. Pricing for Courses</strong></h4>
		<div class="well">
			<p>Instructors are solely responsible for setting the price for any charges associated with their Courses. If You are a Student, You agree to pay the charges assessed for Courses that You take. You authorize Us to charge Your credit card for these amounts</p>
			<p>You authorize Us to charge Your credit card monthly for any amounts owed. If Your credit card is declined, You agree to pay Us the fees within thirty (30) days of Our notifying You that the charge failed and pay Us (at Our discretion) a late payment of 1.5% per month, or the maximum permitted by law, whichever is greater</p>
		</div>
		<h4><strong>12. Refunds</strong></h4>
		<div class="well">
			<p>We provide You a fifteen (15)-day, no-questions-asked money back guarantee on Courses. If you, as a Student, are unhappy with a Course, You must request a refund within fifteen (15) days of the date that you paid for a Course. We will refund the full amount you paid. To request a refund, contact us via email on refund@integro.io</p>
			<p>If We believe that you are abusing Our refund policy, we reserve the right to suspend or terminate your account in our sole discretion. We may also refuse or restrict any and all current or future use of the Company Products. You agree that We have no liability to you for imposing any such restriction. In addition, please note that notwithstanding anything to the contrary in these Terms</p>
			<p>You, as a Instructor, agree to Our Refund Policy and accept that Students have the right to receive a refund as described herein. Instructors and/or the Company will not receive any payments, fees or commissions relating to transactions for which a refund has been made. If a Student requests a refund for a Course after we have made payment to a Instructor, We may deduct the amount of such refund from the next payment to be sent to that Institute/Instructor. If the Institute/Instructor is entitled to no additional payments or such payments are insufficient to cover the amount refunded You the Institute/Instructor agree to refund to Us any amounts We refunded to the Student</p>
		</div>
		<h4><strong>13. Warranty Disclaimer</strong></h4>
		<div class="well">
			<p>All materials made available on or through the Site are provided "as is," without any warranties of any kind. We disclaim all such warranties, express or implied, including, but not limited to, warranties of merchantability, fitness for a particular purpose, non-infringement, accuracy, freedom from errors, suitability of content, or availability</p>
		</div>
		<h4><strong>14. Trademarks</strong></h4>
		<div class="well">
			<p>Trademarks used and displayed on the Site or in any Content are either Ours or Our suppliers' or third parties'.  Trademarks are protected pursuant to Indian, U.S. and international trademark laws. All rights are reserved and You may not alter or obscure the Trademarks, or link to them without Our prior approval</p>
		</div>
		<h4><strong>15. Indemnification</strong></h4>
		<div class="well">
			<p>You hereby indemnify, defend and hold Us and Our affiliates, officers, directors, agents, partners, employees, licensors, representatives and third party providers harmless from and against all losses, expenses, damages, costs, claims and demands, including reasonable attorneys' fees and related costs and expenses, due to or arising out of Your breach of any representation or warranty hereunder. We reserve the right, at Our own expense, to assume the exclusive defense and control of any matter otherwise subject to indemnification by You, and in such case, You agree to fully cooperate with such defense and in asserting any available defenses</p>
		</div>
		<h4><strong>16. Limitation of Liability</strong></h4>
		<div class="well">
			<p>We assume no liability unde0r any theory, including, without limitation, contract, tort or negligence, for any indirect, special, incidental or consequential damages or lost profits. Our total liability hereunder shall be limited to the amounts paid in connection with the courses or products under which such liability arose</p>
		</div>
		<h4><strong>17. Termination</strong></h4>
		<div class="well">
			<p>We may terminate Your use of the Site without notice for any breach by You of these Terms or any of Our applicable policies, as may be posted on the Site from time to time. We may discontinue offering any Content without notice and at any time.  If such discontinuance affects Your Ability to access a Course or provide a Course as an Institute/Instructor, we have no liability other than refunding any fees paid for the course. You agree that You, either as a Student or a Instructor, have no other recourse for such cancellation. You may terminate your use of the Site or the Products at any time, either by ceasing to access them, or by contacting us at privacy@integro.io</p>
			<p>If You are a Instructor You authorize US to allow Students enrolled in Your Courses continued access to Your Content for the duration of the Course. We have no obligation to retain any of Your Account or Content for any period of time beyond what may be required by applicable law. Upon termination, You must cease all use of the Site and/or Content</p>
			<p>Any accrued rights to payment and Sections 5,6,7,8,9 and 11-18 and all representations and warranties shall survive termination</p>
		</div>
		<h4><strong>18. Miscellaneous</strong></h4>
		<div class="well">
			<p>These Terms and any policies applicable to You posted on the Site constitute the entire agreement between the parties with respect to the subject matter hereof, and supersede all previous written or oral agreements between the parties with respect to such subject matter</p>
			<p>A provision of these Terms may be waived only by a written instrument executed by the party entitled to the benefit of such provision. The failure of Company to exercise or enforce any right or provision of these Terms will not constitute a waiver of such right or provision</p>
			<p>If any provision of these Terms is found to be illegal, void or unenforceable, then that provision shall be deemed severable from these Terms and shall not affect the validity and enforceability of any remaining provisions of these Terms</p>
			<p>No Agency. Nothing in these Terms shall be construed as making either party the partner, joint venture, agent, legal representative, employer, contractor or employee of the other. Neither the Company nor any other party to this Agreement shall have, or hold itself out to any third party as having, any authority to make any statements, representations or commitments of any kind, or to take any action that shall be binding on the other except as provided for herein or authorized in writing by the party to be bound</p>
			<p>Any notice or other communication to be given hereunder will be in writing and given by facsimile, postpaid registered or certified mail return receipt requested, or electronic mail</p>
			<p>These Terms and Your use of the Site and the Products shall be governed by the substantive laws of the India without reference to its choice or conflicts of law principles. The place of jurisdiction shall be exclusively in Mumbai.</p>
		</div>
	</div>
</section>