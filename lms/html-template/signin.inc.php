<section class="main-body bg-trans-bg">
	<article class="container">
		<div class="col-md-6 col-md-offset-3">
			<form class="form-3d signup" id="studentSignin">
				<div class="text-center">
					<h3 class="form-title">Student Sign In</h3>
					<p>Not a student? <a href="<?php echo $sitepathInstituteLogin; ?>" data-toggle="modal">Click here</a></p>
				</div>
				<div class="form-group">
					<div class="help-block text-center"></div>
				</div>
				<div class="form-group">
					<label class="text-uppercase" for="inputSEmailAddress">Username OR Email address</label>
					<input type="email" class="form-control trans-control" id="inputSEmailAddress" name="inputSEmailAddress" placeholder="Email">
				</div>
				<div class="form-group">
					<label class="text-uppercase" for="inputSPassword">Password</label>
					<input type="password" class="form-control trans-control" id="inputSPassword" name="inputSPassword" placeholder="Password">
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<div class="checkbox">
								<label>
									<input type="checkbox" id="optionSRemember"> Remember me
								</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="checkbox pull-right">
								<a href="#forgetPasswordModal" data-toggle="modal">Forgot your password?</a>
							</div>
						</div>
					</div>
				</div>
				<button type="button" class="btn btn-color2 btn-block text-uppercase" id="btnStudentSignin">Sign In</button>
				<hr class="style-two">
				<div class="text-center">
					New user? <a href="<?php echo $sitepathSignup; ?>" data-toggle="modal">Sign Up</a>
				</div>
			</form>
		</div>
	</article>
</section>