<section class="feature-image feature-lg feature-image-section-1">
	<div class="container">
		<div class="content shadow-content text-center">
			<h1 class="hide">Integro.io LMS solutions</h1>
			<h2>Breaking the Boundaries of Intellectual Capital</h2>
			<br><br>
			<p class="lead">Owned by All | Managed by All | Open-Protocol | Open-Source</p>
		</div>
		<div class="content shadow-content text-center">
			<h3>Innovate Education. Expedite Learning.</h3>
			<p class="lead">Empower Institutes and Organizations. <br> Motivate Instructors, Managers and Content Creators. <br>Incentivize Students and Employees.</p>
		</div>
	</div>
</section>
<section class="feature-image feature-lg feature-image-section-3">
	<div class="container">
		<div class="content text-center">
			<h3>Focused on Building  Great Ecosystem of <br> Content, Employee, and Student Services</h3>
			<h3>Bring Down Cost to <br> Institutes &amp; Organizations <br> and Enhance Performance.</h3>
		</div>
	</div>
</section>
<section class="feature-image feature-sm clearfix">
	<div class="col-2 min-height-400 feature-image-section-4"></div>
	<div class="col-2 min-height-400 feature-content-section-4">
		<div class="content">
			<h2>Performance Analysis</h2>
			<p class="lead">Integro.io's LMS is powered with</p>
			<h3>Artificial Intelligence</h3>
		</div>
	</div>
	<!-- <div class="container">
		<div class="content">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-8">
					<h2>Performance Analysis</h2>
					<p class="lead">Integro.io's LMS is powered with <br><big><strong>Artificial Intelligence</strong></big></p>
				</div>
			</div>
		</div>
	</div> -->
</section>
<section class="feature-image feature-sm clearfix">
	<div class="col-2 min-height-400 feature-content-section-5">
		<div class="content">
			<h2>Cryptocurrency</h2>
			<p class="lead">Cryptocurrency in the hands of Common Man</p>
			<p class="lead">Participate in our decentralized network and get incentivized with IGRO tokens</p>
		</div>
	</div>
	<div class="col-2 min-height-400 feature-image-section-5"></div>
	<!-- <div class="container">
		<div class="content">
			<div class="row">
				<div class="col-sm-4">
					<h2>Cryptocurrency</h2>
					<p class="lead">Cryptocurrency in the hands of Common Man</p>
					<p class="lead">Participate in the decentralized network to get incentivized with tokens</p>
				</div>
			</div>
		</div>
	</div> -->
</section>
<section class="feature-image feature-sm feature-slider">
	<div class="content">
		<div class="container">
			<div id="sync2" class="owl-carousel">
				<div class="item">
					<a href="javascript:;">
						<div class="icon-round">
							<i class="fa fa-diamond" aria-hidden="true"></i>
						</div>
						<span class="product-label">Course creation</span>
					</a>
				</div>
				<div class="item">
					<span class="connector"></span>
					<a href="javascript:;">
						<div class="icon-round">
							<i class="fa fa-tachometer" aria-hidden="true"></i>
						</div>
						<span class="product-label">Onboarding</span>
					</a>
				</div>
				<div class="item">
					<span class="connector"></span>
					<a href="javascript:;">
						<div class="icon-round">
							<i class="fa fa-arrow-down" aria-hidden="true"></i>
						</div>
						<span class="product-label">Content import</span>
					</a>
				</div>
				<div class="item">
					<span class="connector"></span>
					<a href="javascript:;">
						<div class="icon-round">
							<i class="fa fa-users" aria-hidden="true"></i>
						</div>
						<span class="product-label">Students connect</span>
					</a>
				</div>
				<div class="item">
					<span class="connector"></span>
					<a href="javascript:;">
						<div class="icon-round">
							<i class="fa fa-commenting-o" aria-hidden="true"></i>
						</div>
						<span class="product-label">Messenger</span>
					</a>
				</div>
				<div class="item">
					<span class="connector"></span>
					<a href="javascript:;">
						<div class="icon-round">
							<i class="fa fa-sitemap" aria-hidden="true"></i>
						</div>
						<span class="product-label">Content category</span>
					</a>
				</div>
				<div class="item">
					<span class="connector"></span>
					<a href="javascript:;">
						<div class="icon-round">
							<i class="fa fa-line-chart" aria-hidden="true"></i>
						</div>
						<span class="product-label">Analytics</span>
					</a>
				</div>
				<div class="item">
					<span class="connector"></span>
					<a href="javascript:;">
						<div class="icon-round">
							<i class="fa fa-bar-chart" aria-hidden="true"></i>
						</div>
						<span class="product-label">Monitor Students</span>
					</a>
				</div>
				<div class="item">
					<span class="connector"></span>
					<a href="javascript:;">
						<div class="icon-round">
							<i class="fa fa-calendar-plus-o" aria-hidden="true"></i>
						</div>
						<span class="product-label">New Semester</span>
					</a>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div id="sync1" class="owl-carousel">
				<div class="item">
					<div class="row">
						<div class="col-md-6">
							<div class="item-content">
								<h3>Course creation</h3>
								<p class="desc">A courses can be created for each program run by your institute or organization. Each course can comprise of subjects, which are subsets of the given program.</p>
								<p class="desc">Students are enrolled into the course so that they have access to study material for each subjects. However Integro.io LMS provides the flexibility to allocate specific subjects to certain student – when there are electives or optional subjects. </p>
							</div>
						</div>
						<div class="col-md-6">
							<img src="lms/assets/img/slider/course-creation.png" alt="Course creation" class="btn-block">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-md-6">
							<div class="item-content">
								<h3>Onboarding</h3>
								<p class="desc">Each subject is administered by an instructor. There can be one or multiple instructors assigned to a given subject. Instructors are first invited to the institute and then their allocations to the subjects can be managed by the admin.</p>
							</div>
						</div>
						<div class="col-md-6">
							<img src="lms/assets/img/slider/on-boarding.png" alt="Onboarding" class="btn-block">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-md-6">
							<div class="item-content">
								<h3>Content import</h3>
								<p class="desc">Once the instructors are assigned to the subjects, they can create a fully customized course or even import from our vast database of popular course material. We also invite third party content providers, who offer amazing content to institutions.</p>
							</div>
						</div>
						<div class="col-md-6">
							<img src="lms/assets/img/slider/content-import.png" alt="Content import" class="btn-block">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-md-6">
							<div class="item-content">
								<h3>Students connect</h3>
								<p class="desc">Once the content is ready to use students can be invited to the course. The process of student enrollment is extremely streamlined and can be done in less than an hour. Once the students are registered to the course, the administrator can make specific subject allocations.</p>
								<p class="desc">Of course content can be added and modified even when students are consuming the content.</p>
							</div>
						</div>
						<div class="col-md-6">
							<img src="lms/assets/img/slider/students-connect.png" alt="Students connect" class="btn-block">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-md-6">
							<div class="item-content">
								<h3>Messenger</h3>
								<p class="desc">When students have any questions during their study, they can connect with the instructor immediately wherever they are with the help of our Messenger service. Subject based conversations keep the context clear.</p>
							</div>
						</div>
						<div class="col-md-6">
							<img src="lms/assets/img/slider/messenger.png" alt="Messenger" class="btn-block">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-md-6">
							<div class="item-content">
								<h3>Content category</h3>
								<p class="desc">Instructors can add video files, pdf files, presentation files, text content and downloadable content among others.</p>
								<p class="desc">Exams and Assignments can also be created - these can be objective exams, theoretical exams, paper based exams and OMR based objective exams. Instructors can even manage the scores manually for the exams.</p>
								<p class="desc">Students can complete the exams and assignments from the institute or home - there are some cool features that keep copying/cheating at bay. </p>
								<p class="desc">Have you heard of Artificial Intelligence? ;)</p>
							</div>
						</div>
						<div class="col-md-6">
							<img src="lms/assets/img/slider/content-category.png" alt="Content category" class="btn-block mt-75">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-md-6">
							<div class="item-content">
								<h3>Analytics</h3>
								<p class="desc">Students can monitor their performance with the help of top notch detailed analytics, as well as high level analytics. This will help them know their realtime class performance and also boost motivation to reattempt and improve their grade performance. - We tell them where they should focus.</p>
								<p class="desc">Although we encourage “Learning by making mistakes” - encourage multiple attempts, Instructors can control the number of attempts on exams and assignments.</p>
							</div>
						</div>
						<div class="col-md-6">
							<img src="lms/assets/img/slider/analytics.png" alt="Analytics" class="btn-block">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-md-6">
							<div class="item-content">
								<h3>Monitor Students</h3>
								<p class="desc">Instructors and Parents/Guardians can monitor the students’ performance, and also be able to know the weak spots in their learning. They can get a graphical overview of the performance with respect to the class and provide detailed consultation to the students on how they can improve in their studies.</p>
								<p class="desc">You can never be in more control!</p>
							</div>
						</div>
						<div class="col-md-6">
							<img src="lms/assets/img/slider/monitor-students.png" alt="Monitor Students" class="btn-block">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row">
						<div class="col-md-6">
							<div class="item-content">
								<h3>New Semester</h3>
								<p class="desc">Once you create the content you have it forever in your archives. You are just a click away to import your content to the new semester program. You can also maintain your students’ performance details history.</p>
								<p class="desc">It's unbelievable how many hours you will save while preparing for your next program.</p>
							</div>
						</div>
						<div class="col-md-6">
							<img src="lms/assets/img/slider/new-semester.png" alt="New Semester" class="btn-block">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>