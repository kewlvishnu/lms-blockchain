<section class="feature-image feature-lg feature-image-section-3">
	<div class="container">
		<div class="nav-text-color text-center pad-content">
			<h3>Integro.io Assets</h3>
			<div class="pad20">
				<p>Below you can find Integro.io's assets</p>
			</div>
		</div>
	</div>
</section>
<section class="feature-image feature-sm">
	<div class="container min-height-400">
		<div class="pad20"></div>
		<div class="pad20"></div>
		<div class="pad20 text-center">
			<a href="<?php echo $sitepath; ?>img/logo-mini.png" target="_blank">
				<img src="<?php echo $sitepath; ?>img/logo-mini.png" alt="" class="">
			</a>
			<p class="lead">64x64</p>
		</div>
		<div class="pad20 text-center">
			<a href="<?php echo $sitepath; ?>img/integro-logo.png" target="_blank">
				<img src="<?php echo $sitepath; ?>img/integro-logo.png" alt="" class="">
			</a>
			<p class="lead">200x41</p>
		</div>
	</div>
</section>