<section class="feature-image feature-lg feature-image-section-general">
	<h1 class="text-center">Contact Us</h1>
</section>
<section class="feature-image feature-lg feature-image-section-contact">
	<div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3773.3774526382667!2d72.81563331539593!3d18.95893066056744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7ce12211bb693%3A0xee7a2200047d2a4b!2sTrance+Computers!5e0!3m2!1sen!2sin!4v1444789874280" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
</section>
<section class="feature-image feature-lg feature-image-section-contact">
	<div class="container">
		<form action="" class="frm-trans" id="frmContact">
			<div class="pad20">
				<p class="lead color-grey1">Please fill in the requisite details in the following form.</p>
				<div class="form-group">
					<div class="help-block text-center"></div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" class="form-control" name="inputName" id="inputName" placeholder="Name/Institute Name">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="email" class="form-control" name="inputEmailAddress" id="inputEmailAddress" placeholder="Email">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" class="form-control" name="inputPhone" id="inputPhone" placeholder="Phone">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" class="form-control" name="inputSubject" id="inputSubject" placeholder="Subject">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<textarea class="form-control" name="inputMessage" id="inputMessage" rows="10" placeholder="Message"></textarea>
						</div>
					</div>
					<div class="col-md-12">
						<button class="btn btn-color1" id="btnContact">Submit Message</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>