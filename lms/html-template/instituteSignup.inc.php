<section class="main-body bg-trans-bg">
	<article class="container">
		<div class="col-md-6 col-md-offset-3">
			<form class="form-3d signup" id="instituteSignup">
				<div class="text-center">
					<h3 class="form-title">Sign Up</h3>
				</div>
				<div class="form-group">
					<div class="help-block text-center"></div>
				</div>
				<div class="form-group">
					<select id="userRole" class="form-control">
						<option value="1">I represent an institute</option>
						<option value="2">I am an instructor</option>
					</select>
				</div>
				<div class="js-institute">
					<div class="form-group">
						<label class="text-uppercase" for="inputInstituteName">Institute Name</label>
						<input type="text" class="form-control trans-control" id="inputInstituteName" name="inputInstituteName" placeholder="Institute Name">
					</div>
				</div>
				<div class="js-professor hide">
					<div class="form-group">
						<label class="text-uppercase" for="inputFirstName">First Name</label>
						<input type="text" class="form-control trans-control" id="inputFirstName" name="inputFirstName" placeholder="First Name">
					</div>
					<div class="form-group">
						<label class="text-uppercase" for="inputLastName">Last Name</label>
						<input type="text" class="form-control trans-control" id="inputLastName" name="inputLastName" placeholder="Last Name">
					</div>
				</div>
				<div class="form-group">
					<label class="text-uppercase" for="inputEmailAddress">Email address</label>
					<input type="email" class="form-control trans-control" id="inputEmailAddress" name="inputEmailAddress" placeholder="Email">
				</div>
				<div class="form-group">
					<label class="text-uppercase" for="inputPassword">Password</label>
					<input type="password" class="form-control trans-control" id="inputPassword" name="inputPassword" placeholder="Password">
				</div>
				<button type="submit" class="btn btn-color2 btn-block text-uppercase" id="btnInstituteSignup">Sign Up</button>
				<div class="text-center">
					<small>By signing up, you agree to our <a href="<?php echo $sitepathPolicyTerms; ?>">Terms of Use</a> and <a href="<?php echo $sitepathPolicyPrivacy; ?>">Privacy Policy</a></small>
				</div>
				<hr class="style-two">
				<div class="text-center">
					Already have an account? <a href="<?php echo $sitepathInstituteLogin; ?>">Login</a>
				</div>
			</form>
		</div>
	</article>
</section>