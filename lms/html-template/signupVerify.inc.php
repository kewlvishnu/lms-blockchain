<section class="main-body bg-trans-bg">
	<article class="container">
		<div class="col-md-6 col-md-offset-3">
			<form class="form-3d signup" id="studentSignup">
				<div class="text-center">
					<h3 class="form-title">Confirm your email</h3>
					<h4 class="mb20">Great!! Just one last step</h4>
					<h4 class="mb20">We have sent you a Verification email</h4>
					<div class="text-danger mb20 hide button-2min">Button will enable after 2 minutes.</div>
					<div class="mb20">
						<a class="btn btn-primary btn-lg mb10 btn-block resend-link1" href="javascript:void(0)">Resend Verification Link</a>
					</div>
					<hr class="style-two">
					<div class="text-center hide js-student">
						Already have an account? <a href="<?php echo $sitepathLogin; ?>">Login</a>
					</div>
					<div class="text-center hide js-institute">
						Already have an account? <a href="<?php echo $sitepathInstituteLogin; ?>">Login</a>
					</div>
				</div>
			</form>
		</div>
	</article>
</section>