<section class="main-body bg-trans-bg">
	<article class="container">
		<div class="col-md-6 col-md-offset-3">
			<form class="form-3d signup" id="studentSignup">
				<div class="text-center">
					<h3 class="form-title">Signup complete</h3>
					<h4 class="mb20">Great! You have been successfully registered!</h4>
					<h4 class="mb20">Please click on login button to login and start your course.</h4>
					<div class="mb20">
						<a class="btn btn-primary btn-lg mb10 btn-block resend-link1" href="#loginModal" data-toggle="modal">Login</a>
					</div>
				</div>
			</form>
		</div>
	</article>
</section>