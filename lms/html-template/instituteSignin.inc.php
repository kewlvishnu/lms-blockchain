<section class="main-body bg-trans-bg">
	<article class="container">
		<div class="col-md-6 col-md-offset-3">
			<form class="form-3d signup" id="instituteSignin">
				<div class="text-center">
					<h3 class="form-title">Institute/Instructor <br> Sign In</h3>
					<p>Are you a student? <a href="<?php echo $sitepathLogin; ?>">Click here</a></p>
				</div>
				<div class="form-group">
					<div class="help-block text-center"></div>
				</div>
				<div class="form-group">
					<select id="userRole" class="form-control">
						<option value="1">I represent an institute</option>
						<option value="2">I am an instructor</option>
					</select>
				</div>
				<div class="form-group">
					<label class="text-uppercase" for="inputSEmailAddress">Username OR Email address</label>
					<input type="email" class="form-control trans-control" id="inputSEmailAddress" name="inputSEmailAddress" placeholder="Email">
				</div>
				<div class="form-group">
					<label class="text-uppercase" for="inputSPassword">Password</label>
					<input type="password" class="form-control trans-control" id="inputSPassword" name="inputSPassword" placeholder="Password">
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<div class="checkbox">
								<label>
									<input type="checkbox" id="optionSRemember"> Remember me
								</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="checkbox pull-right">
								<a href="#forgetPasswordInstituteModal" data-toggle="modal">Forgot your password?</a>
							</div>
						</div>
					</div>
				</div>
				<button type="button" class="btn btn-color2 btn-block text-uppercase" id="btnInstituteSignin">Sign In</button>
				<hr class="style-two">
				<div class="text-center">
					New user? <a href="<?php echo $sitepathInstituteSignup; ?>" data-toggle="modal">Sign Up</a>
				</div>
			</form>
		</div>
	</article>
</section>