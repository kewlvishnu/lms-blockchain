		<!-- Modal --><?php /*
		<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="loginModalLabel">Login Form</h4>
					</div>
					<div class="modal-body">
						<div class="login-form clearfix">
							<form role="form">
								<div class="form-group">
									<select id="userRole" class="form-control">
										<option value="1">I represent an institute</option>
										<option value="2">I am an instructor</option>
										<option value="4">I am a student</option>
									</select>
								</div>
								<div class="form-group">
									<input type="text" placeholder="Enter username or email" id="user" class="form-control">
									<div id="userError" class="text-danger hide error-msg"></div>
								</div>
								<div class="form-group">
									<input type="password" placeholder="Password" id="pwd" class="form-control">
									<div id="pwdError" class="text-danger hide error-msg"></div>
								</div>
								<div class="checkbox pull-right">
									<label>
										<input type="checkbox" id="remember"> Remember me on this computer
									</label>
								</div>
								<div class="pull-left">
									<div id="signInError" class="text-danger hide error-msg"></div>
									<button id="signIn" class="btn btn-primary" type="submit">Log In</button>
								</div>
							</form>
						</div>
					</div>
					<div class="modal-footer">
						<div class="pull-left">
							<a data-dismiss="modal" data-toggle="modal" href="#forgetPasswordModal">Forgot Password?</a>
						</div>
						<div class="pull-right">
							<span>Don't have an account?</span>
							<a class="btn btn-danger" data-dismiss="modal" data-toggle="modal" href="#registerModal">Register</a>
						</div>
					</div>
				</div>
			</div>
		</div>*/ ?>
		<div id="forgetPasswordModal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close"  data-dismiss="modal">&times;</button>
						<h3 class="modal-title">Forgot Password?</h3>
					</div>
					<div class="modal-body">
						<div class="login-form clearfix">
							<form role="form">
								<input type="hidden" id="fuserRole" value="4" />
								<div class="form-group">
									<input type="text" class="form-control" id="fuser" placeholder="Enter Email or Username">
									<div class="text-danger hide error-msg" id="fuserError"></div>
								</div>
								<div class="form-group clearfix">
									<div class="pull-right">
										<div class="text-danger hide error-msg" id="forgetError"></div>
									</div>
								</div>
								<div class="form-group clearfix">
									<div class="pull-right">
										<a class="btn btn-success" id="resetPassword">Reset</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="forgetPasswordInstituteModal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close"  data-dismiss="modal">&times;</button>
						<h3 class="modal-title">Forgot Password?</h3>
					</div>
					<div class="modal-body">
						<div class="login-form clearfix">
							<form role="form">
								<div class="form-group">
									<select id="fiuserRole" class="form-control">
										<option value="1">I represent an institute</option>
										<option value="2">I am an instructor</option>
									</select>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" id="fiuser" placeholder="Enter Email or Username">
									<div class="text-danger hide error-msg" id="fiuserError"></div>
								</div>
								<div class="form-group clearfix">
									<div class="pull-right">
										<div class="text-danger hide error-msg" id="fiforgetError"></div>
									</div>
								</div>
								<div class="form-group clearfix">
									<div class="pull-right">
										<a class="btn btn-success" id="firesetPassword">Reset</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div><?php /*
		<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="loginModalLabel">Signup Form</h4>
					</div>
					<div class="modal-body">
						<div class="signup-form clearfix">
							<form class="form-3d" method="post" id="teachSignup">
								<div class="form-group">
									<div class="help-block text-center"></div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-4 text-center">
											<div class="radio">
												<label>
													<input type="radio" name="optionUserType" class="js-user-type" data-type="institute" id="optionInstituteUser" checked=""> Institute
												</label>
											</div>
										</div>
										<div class="col-sm-4 text-center">
											<div class="radio">
												<label>
													<input type="radio" name="optionUserType" class="js-user-type" data-type="professor" id="optionInstructorUser"> Instructor
												</label>
											</div>
										</div>
										<div class="col-sm-4 text-center">
											<div class="radio">
												<label>
													<input type="radio" name="optionUserType" class="js-user-type" data-type="student" id="optionStudentUser"> Student
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group js-institute">
									<input type="text" class="form-control" id="inputInstituteName" name="inputInstituteName" placeholder="Institute Name">
								</div>
								<div class="form-group hide js-professor">
									<div class="row">
										<div class="col-sm-6">
											<input type="text" class="form-control" id="inputFirstName" name="inputFirstName" placeholder="First Name">
										</div>
										<div class="col-sm-6">
											<input type="text" class="form-control" id="inputLastName" name="inputLastName" placeholder="Last Name">
										</div>
									</div>
								</div>
								<div class="form-group">
									<input type="email" class="form-control" id="inputEmailAddress" name="inputEmailAddress" placeholder="Email">
								</div>
								<div class="form-group">
									<input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Password">
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-4">
											<select class="form-control" name="selectCountry" id="selectCountry">
												<option value="0">Select Country</option>
												<option value="3">Afghanistan</option>
												<option value="4">Albania</option>
												<option value="5">Algeria</option>
												<option value="6">American Samoa</option>
												<option value="7">Andorra</option>
												<option value="8">Angola</option>
												<option value="9">Anguilla</option>
												<option value="10">Antarctica</option>
												<option value="11">Antigua and Barbuda</option>
												<option value="12">Argentina</option>
												<option value="13">Armenia</option>
												<option value="14">Aruba</option>
												<option value="15">Australia</option>
												<option value="16">Austria</option>
												<option value="17">Azerbaijan</option>
												<option value="18">Bahamas</option>
												<option value="19">Bahrain</option>
												<option value="20">Bangladesh</option>
												<option value="21">Barbados</option>
												<option value="22">Belarus</option>
												<option value="23">Belgium</option>
												<option value="24">Belize</option>
												<option value="25">Benin</option>
												<option value="26">Bermuda</option>
												<option value="27">Bhutan</option>
												<option value="28">Bolivia</option>
												<option value="29">Bosnia and Herzegovina</option>
												<option value="30">Botswana</option>
												<option value="31">Brazil</option>
												<option value="32">Brunei Darussalam</option>
												<option value="33">Bulgaria</option>
												<option value="34">Burkina Faso</option>
												<option value="35">Burundi</option>
												<option value="36">Cambodia</option>
												<option value="37">Cameroon</option>
												<option value="38">Canada</option>
												<option value="39">Cape Verde</option>
												<option value="40">Cayman Islands</option>
												<option value="41">Central African Republic</option>
												<option value="42">Chad</option>
												<option value="43">Chile</option>
												<option value="44">China</option>
												<option value="45">Christmas Island</option>
												<option value="46">Cocos (Keeling) Islands</option>
												<option value="47">Colombia</option>
												<option value="48">Comoros</option>
												<option value="49">Democratic Republic of the Congo (Kinshasa)</option>
												<option value="50">Congo, Republic of(Brazzaville)</option>
												<option value="51">Cook Islands</option>
												<option value="52">Costa Rica</option>
												<option value="53">Ivory Coast</option>
												<option value="54">Croatia</option>
												<option value="55">Cuba</option>
												<option value="56">Cyprus</option>
												<option value="57">Czech Republic</option>
												<option value="58">English Name</option>
												<option value="59">Denmark</option>
												<option value="60">Djibouti</option>
												<option value="61">Dominica</option>
												<option value="62">Dominican Republic</option>
												<option value="63">East Timor (Timor-Leste)</option>
												<option value="64">Ecuador</option>
												<option value="65">Egypt</option>
												<option value="66">El Salvador</option>
												<option value="67">Equatorial Guinea</option>
												<option value="68">Eritrea</option>
												<option value="69">Estonia</option>
												<option value="70">Ethiopia</option>
												<option value="71">Falkland Islands</option>
												<option value="72">Faroe Islands</option>
												<option value="73">Fiji</option>
												<option value="74">Finland</option>
												<option value="75">France</option>
												<option value="76">French Guiana</option>
												<option value="77">French Polynesia</option>
												<option value="78">French Southern Territories</option>
												<option value="79">Gabon</option>
												<option value="80">Gambia</option>
												<option value="81">Georgia</option>
												<option value="82">Germany</option>
												<option value="83">Ghana</option>
												<option value="84">Gibraltar</option>
												<option value="85">Great Britain</option>
												<option value="86">Greece</option>
												<option value="87">Greenland</option>
												<option value="88">Grenada</option>
												<option value="89">Guadeloupe</option>
												<option value="90">Guam</option>
												<option value="91">Guatemala</option>
												<option value="92">Guinea</option>
												<option value="93">Guinea-Bissau</option>
												<option value="94">Guyana</option>
												<option value="95">Haiti</option>
												<option value="96">Holy See</option>
												<option value="97">Honduras</option>
												<option value="98">Hong Kong</option>
												<option value="99">Hungary</option>
												<option value="100">Iceland</option>
												<option value="1" selected="">India</option>
												<option value="101">Indonesia</option>
												<option value="102">Iran (Islamic Republic of)</option>
												<option value="103">Iraq</option>
												<option value="104">Ireland</option>
												<option value="105">Israel</option>
												<option value="106">Italy</option>
												<option value="107">Jamaica</option>
												<option value="108">Japan</option>
												<option value="109">Jordan</option>
												<option value="110">Kazakhstan</option>
												<option value="111">Kenya</option>
												<option value="112">Kiribati</option>
												<option value="113">Korea, Democratic People's Rep. (North Korea)</option>
												<option value="114">Korea, Republic of (South Korea)</option>
												<option value="115">Kosovo</option>
												<option value="116">Kuwait</option>
												<option value="117">Kyrgyzstan</option>
												<option value="118">Lao, People's Democratic Republic</option>
												<option value="119">Latvia</option>
												<option value="120">Lebanon</option>
												<option value="121">Lesotho</option>
												<option value="122">Liberia</option>
												<option value="123">Libya</option>
												<option value="124">Liechtenstein</option>
												<option value="125">Lithuania</option>
												<option value="126">Luxembourg</option>
												<option value="127">Macau</option>
												<option value="128">Macedonia, Rep. of</option>
												<option value="129">Madagascar</option>
												<option value="130">Malawi</option>
												<option value="131">Malaysia</option>
												<option value="132">Maldives</option>
												<option value="133">Mali</option>
												<option value="134">Malta</option>
												<option value="135">Marshall Islands</option>
												<option value="136">Martinique</option>
												<option value="137">Mauritania</option>
												<option value="138">Mauritius</option>
												<option value="139">Mayotte</option>
												<option value="140">Mexico</option>
												<option value="141">Micronesia, Federal States of</option>
												<option value="142">Moldova, Republic of</option>
												<option value="143">Monaco</option>
												<option value="144">Mongolia</option>
												<option value="145">Montenegro</option>
												<option value="146">Montserrat</option>
												<option value="147">Morocco</option>
												<option value="148">Mozambique</option>
												<option value="149">Myanmar, Burma</option>
												<option value="150">Namibie</option>
												<option value="151">Nauru</option>
												<option value="152">Nepal</option>
												<option value="153">Netherlands</option>
												<option value="154">Netherlands Antilles</option>
												<option value="155">New Caledonia</option>
												<option value="156">New Zealand</option>
												<option value="157">Nicaragua</option>
												<option value="158">Niger</option>
												<option value="159">Nigeria</option>
												<option value="160">Niue</option>
												<option value="161">Northern Mariana Islands</option>
												<option value="162">Norway</option>
												<option value="163">Oman</option>
												<option value="164">Pakistan</option>
												<option value="165">Palau</option>
												<option value="166">Palestinian territories</option>
												<option value="167">Panama</option>
												<option value="168">Papua New Guinea</option>
												<option value="169">Paraguay</option>
												<option value="170">Peru</option>
												<option value="171">Philippines</option>
												<option value="172">Pitcairn Island</option>
												<option value="173">Poland</option>
												<option value="174">Portugal</option>
												<option value="175">Puerto Rico</option>
												<option value="176">Qatar</option>
												<option value="177">Reunion Island</option>
												<option value="178">Romania</option>
												<option value="179">Russian Federation</option>
												<option value="180">Rwanda</option>
												<option value="181">Saint Kitts and Nevis</option>
												<option value="182">Saint Lucia</option>
												<option value="183">Saint Vincent and the Grenadines</option>
												<option value="184">Samoa</option>
												<option value="185">San Marino</option>
												<option value="186">Sao Tome and Principe</option>
												<option value="187">Saudi Arabia</option>
												<option value="188">Senegal</option>
												<option value="189">Serbia</option>
												<option value="190">Seychelles</option>
												<option value="191">Sierra Leone</option>
												<option value="192">Singapore</option>
												<option value="193">Slovakia (Slovak Republic)</option>
												<option value="194">Slovenia</option>
												<option value="195">Solomon Islands</option>
												<option value="196">Somalia</option>
												<option value="197">South Africa</option>
												<option value="198">South Sudan</option>
												<option value="199">Spain</option>
												<option value="200">Sri Lanka</option>
												<option value="201">Sudan</option>
												<option value="202">Suriname</option>
												<option value="203">Swaziland</option>
												<option value="204">Sweden</option>
												<option value="205">Switzerland</option>
												<option value="206">Syria, Syrian Arab Republic</option>
												<option value="207">Taiwan (Republic of China)</option>
												<option value="208">Tajikistan</option>
												<option value="209">Tanzania; officially the United Republic of Tanzania</option>
												<option value="210">Thailand</option>
												<option value="211">Tibet</option>
												<option value="212">Timor-Leste (East Timor)</option>
												<option value="213">Togo</option>
												<option value="214">Tokelau</option>
												<option value="215">Tonga</option>
												<option value="216">Trinidad and Tobago</option>
												<option value="217">Tunisia</option>
												<option value="218">Turkey</option>
												<option value="219">Turkmenistan</option>
												<option value="220">Turks and Caicos Islands</option>
												<option value="221">Tuvalu</option>
												<option value="222">Uganda</option>
												<option value="223">Ukraine</option>
												<option value="224">United Arab Emirates</option>
												<option value="225">United Kingdom</option>
												<option value="2">United States of America</option>
												<option value="226">Uruguay</option>
												<option value="227">Uzbekistan</option>
												<option value="228">Vanuatu</option>
												<option value="229">Vatican City State (Holy See)</option>
												<option value="230">Venezuela</option>
												<option value="231">Vietnam</option>
												<option value="232">Virgin Islands (British)</option>
												<option value="233">Virgin Islands (U.S.)</option>
												<option value="234">Wallis and Futuna Islands</option>
												<option value="235">Western Sahara</option>
												<option value="236">Yemen</option>
												<option value="237">Zambia</option>
												<option value="238">Zimbabwe</option>
											</select>
										</div>
										<div class="col-sm-8">
											<div class="row">
												<div class="col-sm-4">
													<input type="text" class="form-control small" id="inputPrefixContactMobile" placeholder="Country Code" value="+91">
												</div>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="inputContactMobile" placeholder="Phone Number">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="clearfix">
										<div class="pull-left">											
											<div class="text-center">
												<small>By signing up, you agree to our <a href="<?php echo $sitepathPolicyTerms; ?>">Terms of Use</a> and <a href="<?php echo $sitepathPolicyPrivacy; ?>">Privacy Policy</a></small>
											</div>
										</div>
										<div class="pull-right">
											<button type="submit" class="btn btn-primary text-uppercase" id="btnTeachSignup">Sign Up</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="modal-footer">
						<div class="pull-left">
							<a data-dismiss="modal" data-toggle="modal" href="#forgetPasswordModal">Forgot Password?</a>
						</div>
						<div class="pull-right">
							<span>Already have an account?</span>
							<a class="btn btn-danger" data-dismiss="modal" data-toggle="modal" href="#loginModal">Login</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="registerSuccessModal" tabindex="-1" role="dialog" aria-labelledby="registerSuccessModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="loginModalLabel">Signup Complete</h4>
					</div>
					<div class="modal-body">
						<div class="text-center">
							<h3 class="form-title">Confirm your email</h3>
							<h4 class="form-group">Great!! Just one last step</h4>
							<h4 class="form-group">We have sent you a Verification email</h4>
							<div class="form-group text-center">
								<a class="btn btn-primary resend-link" href="javascript:void(0)">Resend Verification Link</a>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="pull-left">
							<a href="javascript:void(0)" data-dismiss="modal" class="btn btn-danger">Close</a>
						</div>
						<div class="pull-right">
							Already have an account? <a href="#loginModal" data-toggle="modal" data-dismiss="modal" class="btn btn-success">Login</a>
						</div>
					</div>
				</div>
			</div>
		</div>*/ ?>
		<?php
			if(!(isset($_SESSION['userId']) && !empty($_SESSION['userId']))) { ?>
		<div class="modal fade" id="exitCTAModal" tabindex="-1" role="dialog" aria-labelledby="exitCTAModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="exitCTAModalLabel">Please let us help you</h4>
					</div>
					<div class="modal-body">
						<div class="login-form clearfix">
							<form role="form">
								<div class="help-block"></div>
								<div class="form-group">
									<input type="text" placeholder="Enter email" id="exitEmail" class="form-control">
								</div>
								<div class="form-group">
									<input type="text" placeholder="Enter Phone Number" id="exitNumber" class="form-control">
								</div>
							</form>
						</div>
					</div>
					<div class="modal-footer">
						<div class="pull-right">
							<a href="javascript:void(0)" data-dismiss="modal" class="btn btn-danger">Close</a>
							<button type="button" class="btn btn-success" id="exitSubmit">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</div><?php
		} ?>
		<footer class="footer-dark text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-6">
						<aside class="footer-nav-menu">
							<h4 class="text-uppercase">About</h4>
							<div>
								<ul class="list-unstyled">
									<li><a href="<?php echo $sitepath; ?>about">About Us</a></li>
									<li><a href="<?php echo $sitepath; ?>contact">Contact Us</a></li>
								</ul>
							</div>
						</aside>
					</div>
					<div class="col-xs-6">
						<aside class="footer-nav-menu">
							<h4 class="text-uppercase">Terms</h4>
							<div>
								<ul class="list-unstyled">
									<li><a href="<?php echo $sitepath; ?>policies/terms">Terms Of Use</a></li>
									<li><a href="<?php echo $sitepath; ?>policies/privacy">Privacy Policy</a></li>
								</ul>
							</div>
						</aside>
					</div>
				</div>
				<nav class="navbar">
					<div class="container text-center">
						<p>&copy; Copyright 2016 | Integro.io, Inc. | All Rights Reserved</p>
					</div><!-- /.container-fluid -->
				</nav>
			</div>
		</footer>
		<?php @include_once('analytics.php'); ?>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
		<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" crossorigin="anonymous"></script>
		<script src="lms/assets/js/modernizr.js"></script>
		<script src="lms/assets/js/owl.carousel.min.js"></script>
		<script src="lms/assets/js/main.js"></script>
        <script src="https://apis.google.com/js/platform.js" async defer></script>
		<?php require_once('lms/jsphp/global.js.php'); ?>
		<?php
			if (!isset($_SESSION["username"]) || empty($_SESSION["username"])) {
				require_once('lms/jsphp/login.js.php');
			}
			switch($page) {
				case "contact":
					require_once('lms/jsphp/contact.js.php');
					break;
				case "logout":
					require_once('lms/jsphp/logout.js.php');
					break;
				case "verify":
					require_once('lms/jsphp/verify.js.php');
					break;
				case "signup":
					require_once('lms/jsphp/signup.js.php');
					break;
				case "instituteSignup":
					require_once('lms/jsphp/instituteSignup.js.php');
					break;
				case "signupVerify":
					require_once('lms/jsphp/signupVerify.js.php');
					break;
				case "signin":
					require_once('lms/jsphp/signin.js.php');
					break;
				case "instituteSignin":
					require_once('lms/jsphp/instituteSignin.js.php');
					break;
				case "signinVerify":
					require_once('lms/jsphp/signinVerify.js.php');
					break;
			}
		?>
<?php
    if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){ ?>
		<!--Start of Tawk.to Script-->
		<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/58f86bb530ab263079b60876/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
		</script>
		<!--End of Tawk.to Script--><?php
    } ?>
	</body>
</html>