<?php
	require_once 'config/url.functions.php';
	$global = new stdClass();
	//loading zend framework
	$libPath = "api/Vendor";
	include_once $libPath . "/Zend/Loader/AutoloaderFactory.php";
	require_once $libPath . "/ArcaneMind/Api.php";
	Zend\Loader\AutoloaderFactory::factory(array(
			'Zend\Loader\StandardAutoloader' => array(
					'autoregister_zf' => true,
					'db' => 'Zend\Db\Sql'
			)
	));
	require_once 'api/Vendor/ArcaneMind/Course.php';
	$c = new Course();
	$res = $c->getCourseCategories();
	if ($res->status == 1) {
		$global->courseCategories = $res->courseCategories;
	}
	$bodyClass = "";
	if ($page == "signup" || $page == "signupVerify" || $page == "signupComplete" || $page == "verify") {
		$bodyClass = "bg-nav-color";
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="google-signin-client_id" content="636517984343-grgpvjcac86l8ms5j10ulkga9q4oemio.apps.googleusercontent.com">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>LMS | Learning Management System | Integro.io</title>
		<meta name="description" content="Integro.io Learning Management System (LMS) solutions. We provide innovative online learning solutions for colleges, institutes, coaching classes.">

		<!-- facebook graph share properties -->
		<meta property="og:type" content="website" />
	    <meta property="og:title" content="<?php echo $fbTitle; ?>">
		<meta property="og:url" content="<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
		<meta property="og:site_name" content="Integro - Study online Courses"/>
		<meta property="og:description" content="<?php echo $description; ?>">
		<meta property="og:image" content="<?php echo $image; ?>">

		<!-- Bootstrap -->
		<base href="<?php echo $sitepath; ?>">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<link rel="stylesheet" href="lms/assets/css/style.css">
		<link rel="shortcut icon" type="image/ico" href="lms/assets/img/favicon.ico" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
	    <!-- Google Tag Manager -->
	    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	    })(window,document,'script','dataLayer','GTM-KFVMH5X');</script>
	    <!-- End Google Tag Manager -->
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="<?php echo $bodyClass; ?>">
	    <!-- Google Tag Manager (noscript) -->
	    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KFVMH5X"
	    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	    <!-- End Google Tag Manager (noscript) -->
		<header>
			<nav class="navbar navbar-dark">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><img src="lms/assets/img/logo.png" alt="Integro.io Logo"></a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<!-- <ul class="nav navbar-nav">
							<li><a href="https://blog.integro.io" target="_blank">Blog</a></li>
						</ul> -->
						<ul class="nav navbar-nav navbar-right"><?php /*
							<li><a href="<?php echo $sitepathMarket; ?>">Marketplace</a></li>*/ ?>
							<li><a href="https://medium.com/integro" target="_blank">Blog</a></li>
							<li><a href="https://t.me/integro" target="_blank">Telegram</a></li>
							<?php
								@session_start();
								if(!(isset($_SESSION['userId']) && !empty($_SESSION['userId']))) {
									?>
									<li><a href="<?php echo $sitepathLogin; ?>" data-toggle="modal" class="js-login-link">Sign In</a></li>
									<li><a href="<?php echo $sitepathSignup; ?>" data-toggle="modal">Sign Up</a></li>
									<?php
								}
							?>
							<?php
								if(isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
									if($_SESSION['userRole'] == 4) {
										?>
										<li><a href="<?php echo $sitepathStudent; ?>">Dashboard</a></li>
										<li><a href="javascript:;" class="btn-logout">Logout</a></li>
										<?php
									}
									else if($_SESSION['userRole'] == 1){
										?>
										<li><a href="<?php echo $sitepathManage; ?>">Dashboard</a></li>
										<li><a href="javascript:;" class="btn-logout">Logout</a></li>
										<?php
									}
									else if($_SESSION['userRole'] == 2){
										?>
										<li><a href="<?php echo $sitepathManage; ?>">Dashboard</a></li>
										<li><a href="javascript:;" class="btn-logout">Logout</a></li>
										<?php
									}
									else if($_SESSION['userRole'] == 3){
										?>
										<li><a href="admin/dashboard.php">Dashboard</a></li>
										<li><a href="javascript:;" class="btn-logout">Logout</a></li>
										<?php
									}
									else if($_SESSION['userRole'] == 5){
										?>
										<li><a href="admin/approvals.php">Dashboard</a></li>
										<li><a href="javascript:;" class="btn-logout">Logout</a></li>
										<?php
									}
									else if($_SESSION['userRole'] == 6){
										?>
										<li><a href="parent/">Dashboard</a></li>
										<li><a href="javascript:;" class="btn-logout">Logout</a></li>
										<?php
									}
								}
							?>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
		</header>