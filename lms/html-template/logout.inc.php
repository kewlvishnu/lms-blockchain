<section class="feature-image feature-lg feature-image-section-contact">
	<div class="container">
		<h1>Logout</h1>
		<hr class="style-six">
		<p class="min-height-500">You have been logged out...</p>
	</div>
</section>