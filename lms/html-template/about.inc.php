<section class="feature-image feature-lg feature-image-section-3">
	<div class="container">
		<div class="nav-text-color text-center pad-content">
			<h3>Welcome To Integro.io</h3>
			<div class="pad20">
				<p>Integro.io is a Cloud Based Learning platform &amp; Market place, where Coaching institutes, Trainers, Schools and Colleges host education content &amp; conduct online tests with detailed analytics.</p>
			</div>
			<div class="pad20">
				<p>The founders are a bunch of Mckinsey &amp; Harvard Alumni, who are really passionate about e-learning and how it can help Students/anyone with a zest for learning perform better in their career.</p>
			</div>
		</div>
	</div>
</section>
<section class="feature-image feature-sm">
	<div class="container">
		<h2 class="text-center">What do we offer?</h2>
		<hr class="style-six">
		<div class="pad20 text-center">
			<div class="row">
				<div class="col-md-6 form-group">
					<div class="row">
						<div class="col-sm-4">
							<div class="pad10 icon-round bg-nav-color nav-text-color">
								<i class="fa fa-book"></i>
							</div>
						</div>
						<div class="col-sm-8 text-left">
							<p class="lead">Study Courses</p>
							<p><em>Study courses which consist of online Tests and Education Content like Pre recorded Videos, documents and Presentations on our cloud based platform.</em></p>
						</div>
					</div>
				</div>
				<div class="col-md-6 form-group">
					<div class="row">
						<div class="col-sm-4">
							<div class="pad10 icon-round bg-nav-color nav-text-color">
								<i class="fa fa-file-text-o"></i>
							</div>
						</div>
						<div class="col-sm-8 text-left">
							<p class="lead">See detailed Assessment</p>
							<p><em>Get detailed assessment of your performance across examinations.</em></p>
						</div>
					</div>
				</div>
			</div>
			<div class="row mb20">
				<div class="col-md-6 form-group">
					<div class="row">
						<div class="col-sm-4">
							<div class="pad10 icon-round bg-nav-color nav-text-color">
								<i class="fa fa-graduation-cap"></i>
							</div>
						</div>
						<div class="col-sm-8 text-left">
							<p class="lead">Improve Performance</p>
							<p><em>Significantly improve your performance by practicing hard and learning by mistakes.</em></p>
						</div>
					</div>
				</div>
				<div class="col-md-6 form-group">
					<div class="row">
						<div class="col-sm-4">
							<div class="pad10 icon-round bg-nav-color nav-text-color">
								<i class="fa fa-check"></i>
							</div>
						</div>
						<div class="col-sm-8 text-left">
							<p class="lead">Learn and Study as per Convenience</p>
							<p><em>Study and learn as per your own convenient timing and place.</em></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>