<?php
include 'html-template/header.php';
?> 
<br/>
    <!--container start-->
	<div class="container">
        <div class="row margin_top_90">
            <!--blog start-->
            <div class="col-md-9 ">
                    <div class="row">
						<div class="col-lg-12 col-sm-12 contact-info"> 
						<h2>Advanced Java Programming </h2>  
						</div>                       
                        <div class="col-lg-7 col-sm-7">
                            <div class="blog-img">
                                <img src="img/blog/img1.jpg" alt="">
                        </div>
                        </div>                   
                        <div class="col-lg-5 col-sm-5">                            
                            <h4>Course Subtitle</h4>
								<p>Introduction to Java Spring 3.2 Framework. Dreams meet reality.</p>
								<h4 class="margin_top_15">Targeted Audience</h4>
								<p>12 to 16 years Age, Group, who want to learn and improve the skills on java</p>
							  <a href="#" class="btn btn-danger">
                                  Free Demo
                              </a>
							  <a class="pull-right" href="#">
                                  <i class="fa fa-user"></i> 55 followers
							  </a>
                        </div>
                    </div>
					<br/>
					<div class="row">            
					<div class="col-lg-12 about">
						<h4>Course Description</h4>
					<p>
						Welcome To Avada
						Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore dolore magnm aliquam quaerat voluptatem.
					</p>
					<p>
						Aenean nibh ante, lacinia non tincidunt nec, lobortis ut tellus. Sed in porta diam. Suspendisse potenti. Donec luctus ullamcorper nulla. Duis nec velit odio.
					</p>
					</div>
					</div>
					<div class="row">					
					<div class="col-md-12 about">
					<h4>Subject</h4>
					</div>
					<div class="col-md-12 about">
					<div class="custom-bg">
					<h5 class="pull-left">Subject Name</h5>
						<a class="btn btn-xs btn-danger pull-right" href="#" style="margin: 6px 0px 0px;">View All Chapter</a>
					</div>
					</div>
					<div class="col-md-2 about">
						<img class="img-responsive" alt="" src="img/blog/subject.jpg">
					</div>
					<div class="col-md-10 custom-font about">
					<div id="accordion" class="panel-group mbot20">
                          <div class="panel panel-default ">
                              <div class="panel-heading">
                                    <ul class="custom-list">
										    <li>
										    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
											<i class="fa fa-plus"></i> Chapter 1 Introduction to Java </a>
											</li>
											<li>
											<a href="#">Exam 1</a></li>
											<li><a href="#">Assignment 1</a></li>
											<li><a class="btn btn-xs btn-info">Preview</a></li>
									</ul>
                              </div>
                              <div class="panel-collapse collapse in" id="collapseOne" style="height: auto;">
                                  <div class="panel-body" style="padding-top: 0px; border: medium none;">
									<table class="table">
                              
                              <tbody>
                              <tr>
                                  <td>1. Introduction </td>
								  
                              </tr>
                              <tr>
                                  <td>2. Why Are Startups Hard? </td>
								  
                              </tr>
                              <tr>
                                  <td>3. What Is Running Lean </td>
								  
                              </tr>
                              </tbody>
                          </table>
                                  </div>
                              </div>
                          </div>
                          <div class="panel panel-default ">
                              <div class="panel-heading">                                      
									   <ul class="custom-list">
										    <li>
										    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
											<i class="fa fa-plus"></i> Chapter 2 Basic of Java </a>
											</li>
											<li>
											<a href="#">Exam 1</a></li>
											<li><a href="#">Assignment 1</a></li>
											<li><a class="btn btn-xs btn-info">Preview</a></li>
											
										</ul>
                              </div>
                              <div class="panel-collapse collapse" id="collapseTwo" style="height: 0px;">
                                  <div class="panel-body" style="padding-top: 0px; border: medium none;">
									<table class="table">
                              
                               <tbody>
                              <tr>
                                  <td>1. Introduction </td>
								  
                              </tr>
                              <tr>
                                  <td>2. Why Are Startups Hard? </td>
								  
                              </tr>
                              <tr>
                                  <td>3. What Is Running Lean </td>								  
                              </tr>
                              </tbody>
                          </table>
                                  </div>
                              </div>
                          </div>
                          <div class="panel panel-default">
                              <div class="panel-heading">
                                      <ul class="custom-list">
										    <li>
										    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
											<i class="fa fa-plus"></i> Independent Exam/Assignment </a>
											</li>
											<li>
											<a href="#">Exam 1</a></li>
											<li><a href="#">Assignment 1</a></li>
											<li><a class="btn btn-xs btn-info">Preview</a></li>
									  </ul>
                              </div>
                              <div class="panel-collapse collapse" id="collapseThree" style="height: 0px;">
							  </div>
                          </div>
                      </div>
					</div>					
					</div>
					<div class="row">					
					
					<div class="col-md-12 about">
					<div class="custom-bg">
					<h5 class="pull-left">Subject Name</h5>
						<a class="btn btn-xs btn-danger pull-right" href="#" style="margin: 6px 0px 0px;">View All Chapter</a>
					</div>
					</div>
					<div class="col-md-2 about">
						<img class="img-responsive" alt="" src="img/blog/subject.jpg">
					</div>
					<div class="col-md-10 custom-font about">
					<div id="accordion" class="panel-group mbot20">
                          <div class="panel panel-default ">
                              <div class="panel-heading">
                                    <ul class="custom-list">
										    <li>
										    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
											<i class="fa fa-plus"></i> Chapter 1 Introduction to Java </a>
											</li>
											<li>
											<a href="#">Exam 1</a></li>
											<li><a href="#">Assignment 1</a></li>
											<li><a class="btn btn-xs btn-info">Preview</a></li>
									</ul>
                              </div>
                              <div class="panel-collapse collapse" id="collapseOne" style="height: 0px;">
                                  <div class="panel-body" style="padding-top: 0px; border: medium none;">
									<table class="table">
                              
                              <tbody>
                              <tr>
                                  <td>1. Introduction </td>
								  
                              </tr>
                              <tr>
                                  <td>2. Why Are Startups Hard? </td>
								  
                              </tr>
                              <tr>
                                  <td>3. What Is Running Lean </td>
								  
                              </tr>
                              </tbody>
                          </table>
                                  </div>
                              </div>
                          </div>
                          <div class="panel panel-default ">
                              <div class="panel-heading">                                      
									   <ul class="custom-list">
										    <li>
										    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
											<i class="fa fa-plus"></i> Chapter 2 Basic of Java </a>
											</li>
											<li>
											<a href="#">Exam 1</a></li>
											<li><a href="#">Assignment 1</a></li>
											<li><a class="btn btn-xs btn-info">Preview</a></li>
										</ul>
                              </div>
                              <div class="panel-collapse collapse" id="collapseTwo" style="height: 0px;">
                                  <div class="panel-body" style="padding-top: 0px; border: medium none;">
									<table class="table">
                              
                               <tbody>
                              <tr>
                                  <td>1. Introduction </td>
								  
                              </tr>
                              <tr>
                                  <td>2. Why Are Startups Hard? </td>
								  
                              </tr>
                              <tr>
                                  <td>3. What Is Running Lean </td>
								  
                              </tr>
                              </tbody>
                          </table>
                                  </div>
                              </div>
                          </div>
                          <div class="panel panel-default">
                              <div class="panel-heading">
                                      <ul class="custom-list">
										    <li>
										    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
											<i class="fa fa-plus"></i> Independent Exam/Assignment </a>
											</li>
											<li>
											<a href="#">Exam 1</a></li>
											<li><a href="#">Assignment 1</a></li>
											<li><a class="btn btn-xs btn-info">Preview</a></li>
									  </ul>
                              </div>
                              <div class="panel-collapse collapse in" id="collapseThree" style="height: auto;">
							  </div>
                          </div>
                      </div>
					</div>					
					</div>
					</div>
			<div class="col-md-3 right-side">
                <div class="profile-nav course-info">
				<ul class="nav nav-pills nav-stacked">
					<li><a><i class="fa fa-book text-danger"></i>Course Id <span class="label label-primary pull-right r-activity" id="course-id">C1234</span></a></li>
			</ul>
			</div>
<div class="Value text-center">                    
  					<h4>License Type</h4>
                </div>
			<div class="profile-nav course-info">
				<ul class="nav nav-pills nav-stacked">
					
					<li><a><i class="fa fa-calendar text-danger"></i>One Time Transfer $ 1000</a></li>
					<li><a><i class="fa fa-calendar text-danger"></i>One Year Licensing $ 300</a></li>
					<li><a><i class="fa fa-calendar text-danger"></i>Commission Based $ 5/User</a></li>
				</ul>
			</div>
			<br>
			<div class="margin-t-p">
			<strong>Category :</strong> <a href="#"> Advance Progrmming </a>
            </div>
			<br>
			<div class="testimonial margin-t-p">
                        <h4>Taught by</h4>
                        <div class="media testimonial-inner">
                            <div class="pull-left">
                                <img src="img/testimonials1.png" class="img-responsive img-circle">
                            </div>
                            <div class="media-body">
                                <p>Arcanmind ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
                                <span><strong>-kaushik/</strong>  Arcanemind</span>
                            </div>
								<p>"Life's Too Short to Build Something Nobody Wants".</p>
								<a class="btn-xs btn pull-right btn-danger">Read More..</a>
                        </div>
            </div>
					<div>
                        <h4>Institute Background</h4>
							<p style="text-align:justify;">Aenean nibh ante, lacinia non tincidunt nec, lobortis ut tellus. Sed in porta diam. Suspendisse potenti. Donec luctus ullamcorper nulla. Duis nec velit odio.</p>
                    </div>
					<br>
        </div>
    </div>
	
	<br/><br/><br/></div>
   <?php
include 'html-template/footer.php';
?>
		
</body>
</html>