var Script = function () {


//    tool tips

    $('.tooltips').tooltip();

//    popovers

    $('.popovers').popover();

//    bxslider

    $('.bxslider').show();
    $('.bxslider').bxSlider({
        minSlides: 4,
        maxSlides: 4,
        slideWidth: 276,
        slideMargin: 20
    });
};
function alertMsg(msg, time){
	time = time || 5000;
	$('.bubble-msg').remove();
	var top = '10px';
	if($('.bubble-msg').length > 0)
	  top = parseInt($('.bubble-msg').last().css('top').replace(/px/, '')) + $('.bubble-msg').last().outerHeight() + 10 + 'px';
	var elem = $('<div />').html(msg).attr('title', 'Click to dismiss.').addClass('bubble-msg animate-300')
		.appendTo($('body')).animate({'top': top}, 300);
	if(time != -1) {
		elem.removeAfter(time);
		$('.bubble-msg').on('click', function(){
			$(this).removeAfter(0);
		});
	}	
}