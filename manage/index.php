<?php
	//get the last-modified-date of this very file
	$lastModified=filemtime(__FILE__);
	//get a unique hash of this file (etag)
	$etagFile = md5_file(__FILE__);
	//get the HTTP_IF_MODIFIED_SINCE header if set
	$ifModifiedSince=(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
	//get the HTTP_IF_NONE_MATCH header if set (etag: unique file hash)
	$etagHeader=(isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

	//set last-modified header
	header("Last-Modified: ".gmdate("D, d M Y H:i:s", $lastModified)." GMT");
	//set etag-header
	header("Etag: $etagFile");
	//make sure caching is turned on
	header('Cache-Control: max-age=290304000, public');

	//check if page has changed. If not, send 304 and exit
	if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])==$lastModified || $etagHeader == $etagFile)
	{
		header("HTTP/1.1 304 Not Modified");
		exit;
	}
	
	@session_start();
	//  url functions and routes
	require_once('../config/url.functions.php');
	$pageArr = explode('/', $pageUrl);

	//$page = "student";
	/*
	**  flagInvalid : true = valid urls, false = invalid urls
	*/
	$flagInvalid = false;
	/*
	**  flagSession :
	**  0 = neutral URLs which opens regardless of user login status
	**  1 = these urls open only for non logged in users
	**  2 = these urls open only for logged in users
	*/
	$flagSession = 2;
	$pagesInstitute  = array('','index.php', 'courses', 'subjects', 'instructors', 'students', 'coursekeys', 'profile', 'notifications', 'settings', 'portfolios', 'portfolio', 'exams', 'subjective-exams', 'manual-exams', 'submissions', 'trash');
	$pagesInstructor = array('','index.php', 'courses', 'subjects',	'invitations', 'students', 'coursekeys', 'profile', 'notifications', 'settings', 'portfolios', 'portfolio', 'exams', 'subjective-exams', 'manual-exams', 'submissions', 'subjects-assigned', 'trash');
	$pageType = "";
	$pageGlobal = false;
	if ($pageArr[0] == 'manage') {
		if (isset($pageArr[1]) && !empty($pageArr[1])) {
			if ($pageArr[1] == "institute") {
				$pageType = "institute";
				$page	  = "home";
				if (in_array($pageArr[2], $pagesInstitute)) {
					if ($pageArr[2] == "courses") {
						$pageGlobal = true;
						$page = "courses";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							$pageGlobal = false;
							$page = "courseDetail";
							$courseId = $pageArr[3];
							if (isset($pageArr[4]) && !empty($pageArr[4])) {
								if ($pageArr[4] == "assign") {
									$pageGlobal = true;
									$page = "courseAssignSubjects";
								}
							}
						}
					} else if ($pageArr[2] == "subjects") {
						$page = "subjects";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							$page = "subjectDetail";
							$subjectId = $pageArr[3];
							if (isset($pageArr[4]) && !empty($pageArr[4])) {
								if ($pageArr[4] == "sections") {
									$pageGlobal = true;
									$page = "subjectSections";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										$chapterId = $pageArr[5];
									} else {
										$chapterId = 0;
									}
								} else if ($pageArr[4] == "students") {
									$pageGlobal = true;
									$page = "subjectStudents";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										if ($pageArr[5] == "groups") {
											$page = "subjectStudentGroups";
										}
									}
								} else if ($pageArr[4] == "analytics") {
									$pageGlobal = true;
									$page = "subjectAnalytics";
								} else if ($pageArr[4] == "grading") {
									$pageGlobal = true;
									$page = "subjectGrading";
								} else if ($pageArr[4] == "rewards") {
									$pageGlobal = true;
									$page = "subjectRewards";
								} else if ($pageArr[4] == "performance") {
									$pageGlobal = true;
									$page = "subjectPerformance";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										$pageGlobal = true;
										$page = "studentPerformance";
										$studentId = $pageArr[5];
									}
								} else if ($pageArr[4] == "exams") {
									//$page = "exams";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										if ($pageArr[5] == "create") {
											$pageGlobal = true;
											$page = "examCreate";
										}
									}
								} else if ($pageArr[4] == "subjective-exams") {
									//$page = "exams";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										if ($pageArr[5] == "create") {
											$pageGlobal = true;
											$page = "subjectiveExamCreate";
										}
									}
								} else if ($pageArr[4] == "manual-exams") {
									//$page = "exams";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										if ($pageArr[5] == "create") {
											$pageGlobal = true;
											$page = "manualExamCreate";
										} elseif ($pageArr[5] == "import") {
											$pageGlobal = true;
											$page = "manualExamImport";
										} elseif ($pageArr[5] == "generate") {
											$page = "manualExamGenerate";
											$pageType = "blank";
											if (isset($pageArr[6]) && !empty($pageArr[6])) {
												$genType = $pageArr[6];
												if ($genType == 1) {
													$genQuestions = 0;
												} elseif ($genType == 2) {
													if (isset($pageArr[7]) && !empty($pageArr[7])) {
														$genQuestions = $pageArr[7];
													} else {
														$flagInvalid = true;
													}
												} else {
													$flagInvalid = true;
												}
											} else {
												$flagInvalid = true;
											}
										}
									}
								} else if ($pageArr[4] == "submissions") {
									//$page = "exams";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										if ($pageArr[5] == "create") {
											$pageGlobal = true;
											$page = "submissionCreate";
										}
									}
								} else if ($pageArr[4] == "questions") {
									$pageGlobal = true;
									$page = "subjectQuestions";
								} else if ($pageArr[4] == "subjective-questions") {
									$pageGlobal = true;
									$page = "subjectSubjectiveQuestions";
								} else if ($pageArr[4] == "submission-questions") {
									$pageGlobal = true;
									$page = "subjectSubmissionQuestions";
								}
							}
						}
					} else if ($pageArr[2] == "exams") {
						//$page = "exams";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							$pageGlobal = true;
							$page = "examDetail";
							$examId = $pageArr[3];
							$subjectId = 1;
							$courseId = 1;
							if (isset($pageArr[4]) && !empty($pageArr[4])) {
								if ($pageArr[4] == "settings") {
									$page = "examSettings";
								} elseif ($pageArr[4] == "objective-comments") {
									$page = "examObjComments";
								} elseif ($pageArr[4] == "result") {
									$page = "examResult";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										if ($pageArr[5] == "student") {
											if (isset($pageArr[6]) && !empty($pageArr[6])) {
												$page = "examResultStudent";
												$studentId = $pageArr[6];
												$attemptId = 0;
												if ((isset($pageArr[7]) && !empty($pageArr[7])) && ($pageArr[7] == "attempt") && (isset($pageArr[8]) && !empty($pageArr[8]))) {
													$attemptId = $pageArr[8];
												}
											}
										}
									}
								}
							}
						}
					} else if ($pageArr[2] == "subjective-exams") {
						//$page = "exams";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							$pageGlobal = true;
							$page = "subjectiveExamQuestions";
							$examId = $pageArr[3];
							$subjectId = 1;
							$courseId = 1;
							if (isset($pageArr[4]) && !empty($pageArr[4])) {
								if ($pageArr[4] == "settings") {
									$page = "subjectiveExamSettings";
								} elseif ($pageArr[4] == "objective-comments") {
									$page = "subjectiveExamObjComments";
								} elseif ($pageArr[4] == "check") {
									$page = "subjectiveExamCheck";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										$page = "subjectiveExamCheckAttempt";
										$attemptId = $pageArr[5];
									}
								} elseif ($pageArr[4] == "result") {
									$page = "subjectiveExamResult";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										if ($pageArr[5] == "student") {
											if (isset($pageArr[6]) && !empty($pageArr[6])) {
												$page = "subjectiveExamResultStudent";
												$studentId = $pageArr[6];
												if ((isset($pageArr[7]) && !empty($pageArr[7])) && ($pageArr[7] == "attempt") && (isset($pageArr[8]) && !empty($pageArr[8]))) {
													$attemptId = $pageArr[8];
												} else {
													$flagInvalid = true;
												}
											}
										}
									}
								}
							}
						}
					} else if ($pageArr[2] == "manual-exams") {
						//$page = "exams";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							$pageGlobal = true;
							$page = "manualExamResult";
							$examId = $pageArr[3];
							$subjectId = 1;
							$courseId = 1;
							if (isset($pageArr[4]) && !empty($pageArr[4])) {
								if ($pageArr[4] == "settings") {
									$page = "manualExamSettings";
								} elseif ($pageArr[4] == "objective-comments") {
									$page = "manualExamObjComments";
								}
							}
						}
					} else if ($pageArr[2] == "submissions") {
						//$page = "exams";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							$pageGlobal = true;
							$page = "submissionQuestions";
							$examId = $pageArr[3];
							$subjectId = 1;
							$courseId = 1;
							if (isset($pageArr[4]) && !empty($pageArr[4])) {
								if ($pageArr[4] == "settings") {
									$page = "submissionSettings";
								} elseif ($pageArr[4] == "objective-comments") {
									$page = "submissionObjComments";
								} elseif ($pageArr[4] == "check") {
									$page = "submissionCheck";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										$page = "submissionCheckAttempt";
										$attemptId = $pageArr[5];
									}
								} elseif ($pageArr[4] == "result") {
									$page = "submissionResult";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										if ($pageArr[5] == "student") {
											if (isset($pageArr[6]) && !empty($pageArr[6])) {
												$page = "submissionResultStudent";
												$studentId = $pageArr[6];
												if ((isset($pageArr[7]) && !empty($pageArr[7])) && ($pageArr[7] == "attempt") && (isset($pageArr[8]) && !empty($pageArr[8]))) {
													$attemptId = $pageArr[8];
												} else {
													$flagInvalid = true;
												}
											}
										}
									}
								}
							}
						}
					} else if ($pageArr[2] == "instructors") {
						$page = "instructors";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							if ($pageArr[3] == "invite") {
								$page = "instructorsInvite";
							} else if ($pageArr[3] == "invitations") {
								$page = "instructorsInvitations";
							}
						}
					} else if ($pageArr[2] == "students") {
						$pageGlobal = true;
						$page = "students";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							if ($pageArr[3] == "invites") {
								$page = "studentsInvites";
							} else if ($pageArr[3] == "transfer") {
								$page = "studentsTransfer";
							}
						}
					} else if ($pageArr[2] == "coursekeys") {
						$pageGlobal = true;
						$page = "courseKeys";
					} else if ($pageArr[2] == "profile") {
						$page = "profile";
						$subPage = "profileMe";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							if ($pageArr[3] == "edit") {
								$subPage = "profileEdit";
							}
						}
					} else if ($pageArr[2] == "notifications") {
						if ($pageArr[3] == "activity") {
							$page = "notificationsActivity";
						} else if ($pageArr[3] == "chat") {
							$page = "notificationsChat";
						}
					} else if ($pageArr[2] == "settings") {
						$pageGlobal = true;
						$page = "settings";
					} else if ($pageArr[2] == "portfolios") {
						$pageGlobal = true;
						$page = "portfolios";
					} else if ($pageArr[2] == "portfolio") {
						$pageGlobal = true;
						$page = "portfolio";
						$subPage = "portfolioEdit";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							if ($pageArr[3] == "pages") {
								$subPage = "portfolioPages";
							} else if ($pageArr[3] == "menu") {
								$subPage = "portfolioMenu";
							}
						}
					} else if ($pageArr[2] == "trash") {
						$pageGlobal = true;
						$page = "trash";
 					}
				}
			} else if ($pageArr[1] == "instructor") {
				$pageType = "instructor";
				$page	  = "home";
				if (in_array($pageArr[2], $pagesInstructor)) {
					if ($pageArr[2] == "courses") {
						$pageGlobal = true;
						$page = "courses";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							$pageGlobal = false;
							$page = "courseDetail";
							$courseId = $pageArr[3];
							if (isset($pageArr[4]) && !empty($pageArr[4])) {
								if ($pageArr[4] == "assign") {
									$pageGlobal = true;
									$page = "courseAssignSubjects";
								}
							}
						}
					} else if ($pageArr[2] == "subjects") {
						$page = "subjects";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							$page = "subjectDetail";
							$subjectId = $pageArr[3];
							if (isset($pageArr[4]) && !empty($pageArr[4])) {
								if ($pageArr[4] == "sections") {
									$pageGlobal = true;
									$page = "subjectSections";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										$chapterId = $pageArr[5];
									} else {
										$chapterId = 0;
									}
								} else if ($pageArr[4] == "students") {
									$pageGlobal = true;
									$page = "subjectStudents";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										if ($pageArr[5] == "groups") {
											$page = "subjectStudentGroups";
										}
									}
								} else if ($pageArr[4] == "analytics") {
									$pageGlobal = true;
									$page = "subjectAnalytics";
								} else if ($pageArr[4] == "grading") {
									$pageGlobal = true;
									$page = "subjectGrading";
								} else if ($pageArr[4] == "performance") {
									$pageGlobal = true;
									$page = "subjectPerformance";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										$pageGlobal = true;
										$page = "studentPerformance";
										$studentId = $pageArr[5];
									}
								} else if ($pageArr[4] == "chat") {
									$page = "subjectChat";
								} else if ($pageArr[4] == "parents") {
									$page = "subjectParents";
								} else if ($pageArr[4] == "exams") {
									//$page = "exams";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										if ($pageArr[5] == "create") {
											$pageGlobal = true;
											$page = "examCreate";
										}
									}
								} else if ($pageArr[4] == "subjective-exams") {
									//$page = "exams";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										if ($pageArr[5] == "create") {
											$pageGlobal = true;
											$page = "subjectiveExamCreate";
										}
									}
								} else if ($pageArr[4] == "manual-exams") {
									//$page = "exams";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										if ($pageArr[5] == "create") {
											$pageGlobal = true;
											$page = "manualExamCreate";
										} elseif ($pageArr[5] == "import") {
											$pageGlobal = true;
											$page = "manualExamImport";
										} elseif ($pageArr[5] == "generate") {
											$page = "manualExamGenerate";
											$pageType = "blank";
											if (isset($pageArr[6]) && !empty($pageArr[6])) {
												$genType = $pageArr[6];
												if ($genType == 1) {
													$genQuestions = 0;
												} elseif ($genType == 2) {
													if (isset($pageArr[7]) && !empty($pageArr[7])) {
														$genQuestions = $pageArr[7];
													} else {
														$flagInvalid = true;
													}
												} else {
													$flagInvalid = true;
												}
											} else {
												$flagInvalid = true;
											}
										}
									}
								} else if ($pageArr[4] == "submissions") {
									//$page = "exams";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										if ($pageArr[5] == "create") {
											$pageGlobal = true;
											$page = "submissionCreate";
										}
									}
								} else if ($pageArr[4] == "questions") {
									$pageGlobal = true;
									$page = "subjectQuestions";
								} else if ($pageArr[4] == "subjective-questions") {
									$pageGlobal = true;
									$page = "subjectSubjectiveQuestions";
								} else if ($pageArr[4] == "submission-questions") {
									$pageGlobal = true;
									$page = "subjectSubmissionQuestions";
								}
							}
						}
					} else if ($pageArr[2] == "exams") {
						//$page = "exams";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							$pageGlobal = true;
							$page = "examDetail";
							$examId = $pageArr[3];
							$subjectId = 1;
							$courseId = 1;
							if (isset($pageArr[4]) && !empty($pageArr[4])) {
								if ($pageArr[4] == "settings") {
									$page = "examSettings";
								} elseif ($pageArr[4] == "objective-comments") {
									$page = "examObjComments";
								} elseif ($pageArr[4] == "result") {
									$page = "examResult";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										if ($pageArr[5] == "student") {
											if (isset($pageArr[6]) && !empty($pageArr[6])) {
												$page = "examResultStudent";
												$studentId = $pageArr[6];
												$attemptId = 0;
												if ((isset($pageArr[7]) && !empty($pageArr[7])) && ($pageArr[7] == "attempt") && (isset($pageArr[8]) && !empty($pageArr[8]))) {
													$attemptId = $pageArr[8];
												}
											}
										}
									}
								}
							}
						}
					} else if ($pageArr[2] == "subjective-exams") {
						//$page = "exams";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							$pageGlobal = true;
							$page = "subjectiveExamQuestions";
							$examId = $pageArr[3];
							$subjectId = 1;
							$courseId = 1;
							if (isset($pageArr[4]) && !empty($pageArr[4])) {
								if ($pageArr[4] == "settings") {
									$page = "subjectiveExamSettings";
								} elseif ($pageArr[4] == "objective-comments") {
									$page = "subjectiveExamObjComments";
								} elseif ($pageArr[4] == "check") {
									$page = "subjectiveExamCheck";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										$page = "subjectiveExamCheckAttempt";
										$attemptId = $pageArr[5];
									}
								} elseif ($pageArr[4] == "result") {
									$page = "subjectiveExamResult";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										if ($pageArr[5] == "student") {
											if (isset($pageArr[6]) && !empty($pageArr[6])) {
												$page = "subjectiveExamResultStudent";
												$studentId = $pageArr[6];
												if ((isset($pageArr[7]) && !empty($pageArr[7])) && ($pageArr[7] == "attempt") && (isset($pageArr[8]) && !empty($pageArr[8]))) {
													$attemptId = $pageArr[8];
												} else {
													$flagInvalid = true;
												}
											}
										}
									}
								}
							}
						}
					} else if ($pageArr[2] == "manual-exams") {
						//$page = "exams";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							$pageGlobal = true;
							$page = "manualExamResult";
							$examId = $pageArr[3];
							$subjectId = 1;
							$courseId = 1;
							if (isset($pageArr[4]) && !empty($pageArr[4])) {
								if ($pageArr[4] == "settings") {
									$page = "manualExamSettings";
								} elseif ($pageArr[4] == "objective-comments") {
									$page = "manualExamObjComments";
								}
							}
						}
					} else if ($pageArr[2] == "submissions") {
						//$page = "exams";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							$pageGlobal = true;
							$page = "submissionQuestions";
							$examId = $pageArr[3];
							$subjectId = 1;
							$courseId = 1;
							if (isset($pageArr[4]) && !empty($pageArr[4])) {
								if ($pageArr[4] == "settings") {
									$page = "submissionSettings";
								} elseif ($pageArr[4] == "objective-comments") {
									$page = "submissionObjComments";
								} elseif ($pageArr[4] == "check") {
									$page = "submissionCheck";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										$page = "submissionCheckAttempt";
										$attemptId = $pageArr[5];
									}
								} elseif ($pageArr[4] == "result") {
									$page = "submissionResult";
									if (isset($pageArr[5]) && !empty($pageArr[5])) {
										if ($pageArr[5] == "student") {
											if (isset($pageArr[6]) && !empty($pageArr[6])) {
												$page = "submissionResultStudent";
												$studentId = $pageArr[6];
												if ((isset($pageArr[7]) && !empty($pageArr[7])) && ($pageArr[7] == "attempt") && (isset($pageArr[8]) && !empty($pageArr[8]))) {
													$attemptId = $pageArr[8];
												} else {
													$flagInvalid = true;
												}
											}
										}
									}
								}
							}
						}
					} else if ($pageArr[2] == "students") {
						$pageGlobal = true;
						$page = "students";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							if ($pageArr[3] == "invites") {
								$page = "studentsInvites";
							} else if ($pageArr[3] == "transfer") {
								$page = "studentsTransfer";
							}
						}
					} else if ($pageArr[2] == "coursekeys") {
						$pageGlobal = true;
						$page = "courseKeys";
					} else if ($pageArr[2] == "profile") {
						$page = "profile";
						$subPage = "profileMe";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							if ($pageArr[3] == "edit") {
								$subPage = "profileEdit";
							}
						}
					} else if ($pageArr[2] == "notifications") {
						if ($pageArr[3] == "activity") {
							$page = "notificationsActivity";
						} else if ($pageArr[3] == "chat") {
							$page = "notificationsChat";
						}
					} else if ($pageArr[2] == "settings") {
						$pageGlobal = true;
						$page = "settings";
					} else if ($pageArr[2] == "portfolios") {
						$pageGlobal = true;
						$page = "portfolios";
					} else if ($pageArr[2] == "portfolio") {
						$pageGlobal = true;
						$page = "portfolio";
						$subPage = "portfolioEdit";
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							if ($pageArr[3] == "pages") {
								$subPage = "portfolioPages";
							} else if ($pageArr[3] == "menu") {
								$subPage = "portfolioMenu";
							}
						}
					} else if ($pageArr[2] == "invitations") {
						$page = "instituteInvitations";
					} else if ($pageArr[2] == "subjects-assigned") {
						$page = "subjectsAssigned";
					} else if ($pageArr[2] == "trash") {
						$pageGlobal = true;
						$page = "trash";
 					}
				}
			} else if ($pageArr[1] == "content") {
				$pageType = "content";
				if ((isset($pageArr[2]) && !empty($pageArr[2])) && (isset($pageArr[3]) && !empty($pageArr[3]))) {
					if ($pageArr[2] == "subjects") {
						$page = "content";
						$subjectId = $pageArr[3];
						if (isset($pageArr[4]) && !empty($pageArr[4])) {
							if ( ($pageArr[4] == 'chapters') && (isset($pageArr[5]) && !empty($pageArr[5])) ) {
								$chapterId = $pageArr[5];
								if (isset($pageArr[6]) && !empty($pageArr[6])) {
									if ( ($pageArr[6] == 'content') && (isset($pageArr[7]) && !empty($pageArr[7])) ) {
										$contentId = $pageArr[7];
									} else {
										$contentId = 0;
									}
								} else {
									$contentId = 0;
								}
							} else {
								$chapterId = 0;
							}
						} else {
							$chapterId = 0;
						}
					} else {
						$flagInvalid = true;
					}
				} else {
					$flagInvalid = true;
				}
			}
		} else {
			$pageType = "";
			$page	  = "home";
		}
	} else {
		$flagInvalid = true;
	}
	if ($flagInvalid) {
		//var_dump($_SESSION);
		//header('location:'.$sitepath404);
	} else {
		if (!$flagInvalid && ($flagSession == 2) && (!isset($_SESSION['userId']) || empty($_SESSION['userId']) || (($_SESSION['userRole'] != 1) && ($_SESSION['userRole'] != 2)))) {
			
			header('location:'.$sitepath);

		} else if (empty($pageType)) {
			if ($_SESSION['userRole'] == 1) {
				header("location:".$sitepathManageInstitute);
			} else {
				header("location:".$sitepathManageInstructor);
			}
		} else if ($pageType == "institute" && $_SESSION['userRole'] == 2) {
			header("location:".$sitepathManageInstructor);
		} else if ($pageType == "instructor" && $_SESSION['userRole'] == 1) {
			header("location:".$sitepathManageInstitute);
		} else if ($pageType == "content") {
			require_once('inc/header.functions.php');
			// content page
			require_once('content/includes/header.php');
			require_once('content/includes/content.php');
			require_once('content/includes/footer.php');
			require_once('../config/tracking.functions.php');
		} else {
			/**
			* header checks for metas, page specific validation and portfolio subdomain validations
			**/
			require_once('inc/header.functions.php');

			if ($pageType != "blank") {
				if ($pageGlobal == true) {
					require_once('html-template/global/index.html.php');
				} else {
					require_once('html-template/'.$pageType.'/index.html.php');
				}
			}

			require_once('../config/tracking.functions.php');
		}
	}
?>