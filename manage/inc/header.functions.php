<?php
	$global = new stdClass();
	//loading zend framework
	$libPath = "../api/Vendor";
	include_once $libPath . "/Zend/Loader/AutoloaderFactory.php";
	require_once $libPath . "/ArcaneMind/Api.php";
	Zend\Loader\AutoloaderFactory::factory(array(
			'Zend\Loader\StandardAutoloader' => array(
					'autoregister_zf' => true,
					'db' => 'Zend\Db\Sql'
			)
	));
	if ($page == 'logout') {
		unset($_SESSION["userId"]);
		unset($_SESSION["userRole"]);
		unset($_SESSION['username']);
		unset($_SESSION['userEmail']);
		unset($_SESSION['temp']);
		session_destroy();
		setcookie('mtwebLogin', "", time() - 3600, '/');
		unset($_COOKIE['mtwebLogin']);
	}
	if(isset($_COOKIE["mtwebLogin"]) && !empty($_COOKIE["mtwebLogin"])) {
		require_once '../api/Vendor/ArcaneMind/User.php';
		$cookie = $_COOKIE["mtwebLogin"];
		$res = Api::checkUserRemembered($cookie);
		$_SESSION["userId"] = $res->user['id'];
		$_SESSION["userRole"] = $res->user['roleId'];
		$_SESSION["username"] = $res->user['email'];
		$_SESSION["userEmail"] = $res->user['email'];
		$_SESSION["temp"] = $res->user['temp'];
		$_SESSION["details"] = $res->user['details'];
	}
	if($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='::1') {
		//$_GET['pageDetailId'] = "institute-demo";
		//$_GET['pageDetailId'] = "harvard";
	}
	if(isset($_GET['pageDetailId']) && !empty($_GET['pageDetailId'])) {
		$actual_link  = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$temp_link    = explode("/", $actual_link);
		$sub_link = explode(".", $temp_link[2]);
		$final_link = str_replace($sub_link[0], 'www', $actual_link);
		$subdomain = trim($_GET['pageDetailId']);
		$_SESSION["subdomain"] = $subdomain;
		$res = Api::getPortfolioBySlug($subdomain);
		if($res->valid) {
			$portfolioDetails = $res;
			$portfolioName 	  = $portfolioDetails->portfolio['name'];
			$logoPath = $res->portfolio['img'];
			$favicon = "http://institute-demo.arcanemind.org/img/favicon.ico";
			if(!empty($res->portfolio['favicon'])) {
				$favicon  = $res->portfolio['favicon'];
			}
			if($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
				$sitepathManageIncludes = str_replace($sub_link[0], 'www', $sitepathManageIncludes);
			}
		} else {
			header('location:'.$final_link);
		}
	}
	$title = 'Integro | Instructors | Institutes | Online Courses | Entrance Exam Preparation';
	//$description = 'Arcanemind is a platform & marketplace for selling online courses & entrance exam preparation material.';
	$description = 'Arcanemind is a platform & marketplace for online courses & entrance exam preparation. We have the best instructors and institutes in the world, we ensure that the content and questions are of the highest quality.';
	$image = 'http://dhzkq6drovqw5.cloudfront.net/fb-arcanemind.jpg';

	$logolink = $sitepathManage;

	if(isset($logoPath) && !empty($logoPath)) {
		$logoBlock = '<a href="'.$logolink.'">
                        <img src="'.$logoPath.'" alt="'.$portfolioName.'" alt="logo" class="logo-default" />
                    </a>';
	} else {
		$logoBlock = '<a href="'.$logolink.'">
                        <img src="'.$sitepathStudent.'assets/global/img/logo-default.png" alt="Arcanemind" alt="logo" class="logo-default" />
                    </a>';
	}
	require_once '../api/Vendor/ArcaneMind/User.php';
	$u = new User();
	$data = new stdClass();
	$data->userId = $_SESSION['userId'];
	$data->userRole = $_SESSION['userRole'];
	$res = $u->getAccountTerminology($data);
	if ($res->status == 1) {
		$global->terminology = $res->terminology;
	}
	switch ($page) {
		case 'profile':
			$gCountries = Api::getCountriesList();
			$gCountries = $gCountries->countries;
			$global->countries = $gCountries;
			/*echo '<pre>';
			print_r($global->countries);
			die();*/
			break;
		case 'content':
			$data = new stdClass();
			$data->subjectId = $subjectId;
			$data->userId = $_SESSION['userId'];
			$data->preview = 1;
			$content = Api::getHeadings($data);
			if ($content->status == 0) {
				header('location:'.$sitepath404);
			}
			$subjectName= $content->subjectName;
			$content	= $content->headings;
			if ($chapterId == 0) {
				$chapterId = $content[0]['id'];
				$contentId = 0;
				if(count($content[0]['content'])) {
					$contentId = $content[0]['content'][0]['id'];
				}
			} else {
				if ($contentId == 0) {
					$key = array_search($chapterId, array_column($content, 'id'));
					$contentId = $content[$key]['content'][0]['id'];
				}
			}
			break;
		case 'portfolios':
			if (isset($subdomain) && !empty($subdomain)) {
				header('location:'.$sitepathManagePortfolio);
			}
			break;
		case 'portfolio':
			if (!isset($subdomain) || empty($subdomain)) {
				header('location:'.$sitepathManagePortfolios);
			}
			break;
		case 'courseDetail':
		case 'courseAssignSubjects':
			$data = new stdClass();
			$data->courseId = $courseId;
			$courseDetails = Api::getCourseDetails($data);
			if ($courseDetails->status == 0) {
				header('location:'.$sitepath404);
			}
			break;
		case 'subjectDetail':
		case 'subjectStudents':
		case 'subjectStudentGroups':
		case 'subjectSections':
		case 'subjectAnalytics':
		case 'subjectGrading':
		case 'subjectRewards':
		case 'subjectPerformance':
		case 'subjectChat':
        case 'subjectParents':
		case 'examCreate':
		case 'subjectiveExamCreate':
		case 'subjectQuestions':
		case 'subjectSubjectiveQuestions':
		case 'subjectSubmissionQuestions':
		case 'manualExamCreate':
		case 'manualExamImport':
		case 'submissionCreate':
		case 'studentPerformance':
			$data = new stdClass();
			$data->subjectId = $subjectId;
			$data->userId = $_SESSION['userId'];
			$data->userRole = $_SESSION['userRole'];
			$subjectDetails = Api::getSubjectDetails($data);
			if ($subjectDetails->status == 0) {
				header('location:'.$sitepath404);
			} else {
				$subjectDetails = $subjectDetails->subjectDetails;
				$subjectId		= $subjectDetails['id'];
				$courseId		= $subjectDetails['courseId'];
				$courseName		= $subjectDetails['courseName'];
				$data = new stdClass();
				$data->courseId = $courseId;
				$courseDetails	= Api::getCourseDetails($data);
				if ($courseDetails->status == 0) {
					header('location:'.$sitepath404);
				} else {
					$courseDetails = $courseDetails->courseDetails;
				}
			}
			break;
		case 'examSettings':
		case 'examDetail':
		case 'examQuestions':
		case 'examResult':
			$data = new stdClass();
			$data->examId = $examId;
			$data->userId = $_SESSION['userId'];
			$data->userRole = $_SESSION['userRole'];
			$examDetails = Api::getExamDetailAdmin($data);
			if ($examDetails->status == 0) {
				header('location:'.$sitepath404);
			} else {
				$examDetails = $examDetails->exam;
				$data = new stdClass();
				$data->subjectId = $examDetails['subjectId'];
				$data->userId = $_SESSION['userId'];
				$data->userRole = $_SESSION['userRole'];
				$subjectDetails = Api::getSubjectDetails($data);
				if ($subjectDetails->status == 0) {
					header('location:'.$sitepath404);
				} else {
					$subjectDetails = $subjectDetails->subjectDetails;
					$subjectId		= $subjectDetails['id'];
					$courseId		= $subjectDetails['courseId'];
					$courseName		= $subjectDetails['courseName'];
					$data = new stdClass();
					$data->courseId = $courseId;
					$courseDetails	= Api::getCourseDetails($data);
					if ($courseDetails->status == 0) {
						header('location:'.$sitepath404);
					} else {
						$courseDetails = $courseDetails->courseDetails;
					}
				}
			}
			break;
		case 'examObjComments':
			$data = new stdClass();
			$data->examId = $examId;
			$data->userId = $_SESSION['userId'];
			$data->userRole = $_SESSION['userRole'];
			//$examDetails = Api::getExamOnlyAdmin($data);
			$examDetails = Api::getExamDetail($data);
			if ($examDetails->status == 0) {
				header('location:'.$sitepath404);
			} else {
				$examDetails = $examDetails->exam;
				$data = new stdClass();
				$data->subjectId = $examDetails['subjectId'];
				$data->userId = $_SESSION['userId'];
				$data->userRole = $_SESSION['userRole'];
				$subjectDetails = Api::getSubjectDetails($data);
				if ($subjectDetails->status == 0) {
					header('location:'.$sitepath404);
				} else {
					$subjectDetails = $subjectDetails->subjectDetails;
					$subjectId		= $subjectDetails['id'];
					$courseId		= $subjectDetails['courseId'];
					$courseName		= $subjectDetails['courseName'];
					$data = new stdClass();
					$data->courseId = $courseId;
					$courseDetails	= Api::getCourseDetails($data);
					if ($courseDetails->status == 0) {
						header('location:'.$sitepath404);
					} else {
						$courseDetails = $courseDetails->courseDetails;
					}
				}
			}
			break;
		case 'examResultStudent':
			$data = new stdClass();
			$data->userId = $studentId;
			$studentDetails = Api::getStudentDetailsNew($data);
			if ($studentDetails->status == 0) {
				header('location:'.$sitepath404);
			} else {
				$studentDetails = $studentDetails->details;
				$data = new stdClass();
				$data->examId = $examId;
				$data->userId = $_SESSION['userId'];
				$data->userRole = $_SESSION['userRole'];
				$examDetails = Api::getExamDetail($data);
				if ($examDetails->status == 0) {
					header('location:'.$sitepath404);
				} else {
					$examDetails = $examDetails->exam;
					$data = new stdClass();
					$data->subjectId = $examDetails['subjectId'];
					$data->userId = $_SESSION['userId'];
					$data->userRole = $_SESSION['userRole'];
					$subjectDetails = Api::getSubjectDetails($data);
					if ($subjectDetails->status == 0) {
						header('location:'.$sitepath404);
					} else {
						$subjectDetails = $subjectDetails->subjectDetails;
						$subjectId		= $subjectDetails['id'];
						$courseId		= $subjectDetails['courseId'];
						$courseName		= $subjectDetails['courseName'];
						$data = new stdClass();
						$data->courseId = $courseId;
						$courseDetails	= Api::getCourseDetails($data);
						if ($courseDetails->status == 0) {
							header('location:'.$sitepath404);
						} else {
							$courseDetails = $courseDetails->courseDetails;
						}
					}
				}
			}
			break;
		case 'subjectiveExamSettings':
		case 'subjectiveExamObjComments':
		case 'subjectiveExamQuestions':
		case 'subjectiveExamCheck':
		case 'subjectiveExamCheckAttempt':
		case 'subjectiveExamResult':
			$data = new stdClass();
			$data->examId = $examId;
			$data->userId = $_SESSION['userId'];
			$data->userRole = $_SESSION['userRole'];
			$examDetails = Api::getSubjectiveExamDetail($data);
			if ($examDetails->status == 0) {
				header('location:'.$sitepath404);
			} else {
				$examDetails = $examDetails->exam;
				$data = new stdClass();
				$data->subjectId = $examDetails['subjectId'];
				$data->userId = $_SESSION['userId'];
				$data->userRole = $_SESSION['userRole'];
				$subjectDetails = Api::getSubjectDetails($data);
				if ($subjectDetails->status == 0) {
					header('location:'.$sitepath404);
				} else {
					$subjectDetails = $subjectDetails->subjectDetails;
					$subjectId		= $subjectDetails['id'];
					$courseId		= $subjectDetails['courseId'];
					$courseName		= $subjectDetails['courseName'];
					$data = new stdClass();
					$data->courseId = $courseId;
					$courseDetails	= Api::getCourseDetails($data);
					if ($courseDetails->status == 0) {
						header('location:'.$sitepath404);
					} else {
						$courseDetails = $courseDetails->courseDetails;
					}
				}
			}
			break;
		case 'subjectiveExamResultStudent':
			$data = new stdClass();
			$data->userId = $studentId;
			$studentDetails = Api::getStudentDetailsNew($data);
			if ($studentDetails->status == 0) {
				header('location:'.$sitepath404);
			} else {
				$studentDetails = $studentDetails->details;
				$data = new stdClass();
				$data->examId = $examId;
				$data->userId = $_SESSION['userId'];
				$data->userRole = $_SESSION['userRole'];
				$examDetails = Api::getSubjectiveExamDetail($data);
				if ($examDetails->status == 0) {
					header('location:'.$sitepath404);
				} else {
					$examDetails = $examDetails->exam;
					$data = new stdClass();
					$data->subjectId = $examDetails['subjectId'];
					$data->userId = $_SESSION['userId'];
					$data->userRole = $_SESSION['userRole'];
					$subjectDetails = Api::getSubjectDetails($data);
					if ($subjectDetails->status == 0) {
						header('location:'.$sitepath404);
					} else {
						$subjectDetails = $subjectDetails->subjectDetails;
						$subjectId		= $subjectDetails['id'];
						$courseId		= $subjectDetails['courseId'];
						$courseName		= $subjectDetails['courseName'];
						$data = new stdClass();
						$data->courseId = $courseId;
						$courseDetails	= Api::getCourseDetails($data);
						if ($courseDetails->status == 0) {
							header('location:'.$sitepath404);
						} else {
							$courseDetails = $courseDetails->courseDetails;
						}
					}
				}
			}
			break;
		case 'manualExamGenerate':
			$data = new stdClass();
			$data->subjectId = $subjectId;
			$data->userId = $_SESSION['userId'];
			$data->userRole = $_SESSION['userRole'];
			$subjectDetails = Api::getSubjectDetails($data);
			if ($subjectDetails->status == 0) {
				header('location:'.$sitepath404);
			} else {
				$subjectDetails = $subjectDetails->subjectDetails;
				$subjectId		= $subjectDetails['id'];
				$courseId		= $subjectDetails['courseId'];
				$courseName		= $subjectDetails['courseName'];
				$data = new stdClass();
				$data->courseId = $courseId;
				$courseDetails	= Api::getCourseDetails($data);
				if ($courseDetails->status == 0) {
					header('location:'.$sitepath404);
				} else {
					$courseDetails = $courseDetails->courseDetails;
					//require_once '../api/Vendor/ArcaneMind/Exam.php';
					$data = new stdClass();
					$data->type			= Api::sanitize($genType);
					$data->questions	= Api::sanitize($genQuestions);
					$data->courseId	= $courseId;
					$data->subjectId= $subjectId;
					//$ex = new Exam();
					$result = Api::generateManualExamCSV($data);
					//var_dump($result);
				}
			}
			break;
		case 'manualExamSettings':
		case 'manualExamObjComments':
		case 'manualExamResult':
			$data = new stdClass();
			$data->examId = $examId;
			$data->userId = $_SESSION['userId'];
			$data->userRole = $_SESSION['userRole'];
			$examDetails = Api::getManualExamDetails($data);
			if ($examDetails->status == 0) {
				header('location:'.$sitepath404);
			} else {
				$examDetails = $examDetails->exam;
				$data = new stdClass();
				$data->subjectId = $examDetails['subjectId'];
				$data->userId = $_SESSION['userId'];
				$data->userRole = $_SESSION['userRole'];
				$subjectDetails = Api::getSubjectDetails($data);
				if ($subjectDetails->status == 0) {
					header('location:'.$sitepath404);
				} else {
					$subjectDetails = $subjectDetails->subjectDetails;
					$subjectId		= $subjectDetails['id'];
					$courseId		= $subjectDetails['courseId'];
					$courseName		= $subjectDetails['courseName'];
					$data = new stdClass();
					$data->courseId = $courseId;
					$courseDetails	= Api::getCourseDetails($data);
					if ($courseDetails->status == 0) {
						header('location:'.$sitepath404);
					} else {
						$courseDetails = $courseDetails->courseDetails;
					}
				}
			}
			break;
		case 'submissionSettings':
		case 'submissionObjComments':
		case 'submissionQuestions':
		case 'submissionCheck':
		case 'submissionCheckAttempt':
		case 'submissionResult':
			$data = new stdClass();
			$data->examId = $examId;
			$data->userId = $_SESSION['userId'];
			$data->userRole = $_SESSION['userRole'];
			$examDetails = Api::getSubmissionDetail($data);
			if ($examDetails->status == 0) {
				header('location:'.$sitepath404);
			} else {
				$examDetails = $examDetails->exam;
				$data = new stdClass();
				$data->subjectId = $examDetails['subjectId'];
				$data->userId = $_SESSION['userId'];
				$data->userRole = $_SESSION['userRole'];
				$subjectDetails = Api::getSubjectDetails($data);
				if ($subjectDetails->status == 0) {
					header('location:'.$sitepath404);
				} else {
					$subjectDetails = $subjectDetails->subjectDetails;
					$subjectId		= $subjectDetails['id'];
					$courseId		= $subjectDetails['courseId'];
					$courseName		= $subjectDetails['courseName'];
					$data = new stdClass();
					$data->courseId = $courseId;
					$courseDetails	= Api::getCourseDetails($data);
					if ($courseDetails->status == 0) {
						header('location:'.$sitepath404);
					} else {
						$courseDetails = $courseDetails->courseDetails;
					}
				}
			}
			break;
		case 'submissionResultStudent':
			$data = new stdClass();
			$data->userId = $studentId;
			$studentDetails = Api::getStudentDetailsNew($data);
			if ($studentDetails->status == 0) {
				header('location:'.$sitepath404);
			} else {
				$studentDetails = $studentDetails->details;
				$data = new stdClass();
				$data->examId = $examId;
				$data->userId = $_SESSION['userId'];
				$data->userRole = $_SESSION['userRole'];
				$examDetails = Api::getSubmissionDetail($data);
				if ($examDetails->status == 0) {
					header('location:'.$sitepath404);
				} else {
					$examDetails = $examDetails->exam;
					$data = new stdClass();
					$data->subjectId = $examDetails['subjectId'];
					$data->userId = $_SESSION['userId'];
					$data->userRole = $_SESSION['userRole'];
					$subjectDetails = Api::getSubjectDetails($data);
					if ($subjectDetails->status == 0) {
						header('location:'.$sitepath404);
					} else {
						$subjectDetails = $subjectDetails->subjectDetails;
						$subjectId		= $subjectDetails['id'];
						$courseId		= $subjectDetails['courseId'];
						$courseName		= $subjectDetails['courseName'];
						$data = new stdClass();
						$data->courseId = $courseId;
						$courseDetails	= Api::getCourseDetails($data);
						if ($courseDetails->status == 0) {
							header('location:'.$sitepath404);
						} else {
							$courseDetails = $courseDetails->courseDetails;
						}
					}
				}
			}
			break;
		default:
			# code...
			break;
	}
	/*if ($page == "profile") {
		$gCountries = Api::getCountriesList();
		$gCountries = $gCountries->countries;
	} else if ($page == "content") {
		$data = new stdClass();
		$data->subjectId = $subjectId;
		$data->userId = $_SESSION['userId'];
		$content = Api::getHeadings($data);
		if ($content->status == 0) {
			header('location:'.$sitepath404);
		}
		$subjectName= $content->subjectName;
		$content	= $content->headings;
		if ($chapterId == 0) {
			$chapterId = $content[0]['id'];
			$contentId = $content[0]['content'][0]['id'];
		} else {
			if ($contentId == 0) {
				$key = array_search($chapterId, array_column($content, 'id'));
				$contentId = $content[$key]['content'][0]['id'];
			}
		}
	} else if ($page == "courseDetail") {
		$data = new stdClass();
		$data->courseId = $courseId;
		$courseDetails = Api::getCourseDetails($data);
		if ($courseDetails->status == 0) {
			header('location:'.$sitepath404);
		}
	} else if ($page == "subjectDetail") {
		$data = new stdClass();
		$data->subjectId = $subjectId;
		$data->userId = $_SESSION['userId'];
		$data->userRole = $_SESSION['userRole'];
		$subjectDetails = Api::getSubjectDetails($data);
		if ($subjectDetails->status == 0) {
			header('location:'.$sitepath404);
		} else {
			$subjectDetails = $subjectDetails->subjectDetails;
			$subjectId		= $subjectDetails['id'];
			$courseId		= $subjectDetails['courseId'];
			$courseName		= $subjectDetails['courseName'];
			$data = new stdClass();
			$data->courseId = $courseId;
			$courseDetails	= Api::getCourseDetails($data);
			if ($courseDetails->status == 0) {
				header('location:'.$sitepath404);
			}
		}
	} else if ($page == "examResult") {
		$data = new stdClass();
		$data->examId = $examId;
		$data->userId = $_SESSION['userId'];
		$examDetails = Api::getExamDetail($data);
		if ($examDetails->status == 0) {
			header('location:'.$sitepath404);
		}
		$examDetails = $examDetails->exam;
	} else if ($page == "subjectiveExamResult") {
		$data = new stdClass();
		$data->examId = $examId;
		$data->userId = $_SESSION['userId'];
		$examDetails = Api::getSubjectiveExamDetail($data);
		if ($examDetails->status == 0) {
			header('location:'.$sitepath404);
		}
		$examDetails = $examDetails->exam;
	} else if ($page == "manualExamResult") {
		$data = new stdClass();
		$data->examId = $examId;
		$data->userId = $_SESSION['userId'];
		$examDetails = Api::getManualExamDetail($data);
		if ($examDetails->status == 0) {
			header('location:'.$sitepath404);
		}
		$examDetails = $examDetails->exam;
		$courseId = $examDetails['courseId'];
		$subjectId = $examDetails['subjectId'];
	} else if ($page == "content") {
		$data = new stdClass();
		$data->subjectId = $subjectId;
		$data->userId = $_SESSION['userId'];
		$content = Api::getHeadings($data);
		if ($content->status == 0) {
			header('location:'.$sitepath404);
		}
		$subjectName= $content->subjectName;
		$content	= $content->headings;
		if ($chapterId == 0) {
			$chapterId = $content[0]['id'];
			$contentId = $content[0]['content'][0]['id'];
		} else {
			if ($contentId == 0) {
				$key = array_search($chapterId, array_column($content, 'id'));
				$contentId = $content[$key]['content'][0]['id'];
			}
		}
	}*/
	require_once '../api/Vendor/ArcaneMind/Course.php';
	$c = new Course();
	$res = $c->getCourseCategories();
	if ($res->status == 1) {
		$global->courseCategories = $res->courseCategories;
	}
	require_once '../api/Vendor/ArcaneMind/CryptoWallet.php';
	$w = new CryptoWallet();
	$data = new stdClass();
	$data->userId = $_SESSION['userId'];
	$data->userRole = $_SESSION['userRole'];
	$res = $w->getWalletAddress($data);
	$global->walletAddress = '';
	$global->walletApp = '';
	if ($res->status == 1) {
		if (isset($res->wallet) && !empty($res->wallet)) {
			$global->walletAddress = $res->wallet;
			$global->walletApp = (($res->app=='metamask')?'m':'p');
		}
	}
	$res = $w->getIGROContractData($data);
	if ($res->status == 1) {
		$global->toAddress = $res->ownerAddress;
		$global->contractAddress = $res->contractAddress;
		$global->contractCode = $res->contractCode;
		$global->decimalPrecision = $res->decimalPrecision;
		$global->balanceDivisor = $res->balanceDivisor;
		$global->displayCurrency = $res->displayCurrency;
	}

	//var_dump($_SESSION);
?>