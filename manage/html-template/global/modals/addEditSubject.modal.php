<div id="modalAddEditSubject" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><span class="js-subject-action">New</span> <?php echo $global->terminology["subject_single"]; ?></h4>
            </div>
            <div class="modal-body">
                <form role="form" id="formAddEditSubject">
                    <div class="form-body">
                        <div class="form-group">
                            <label><?php echo $global->terminology["subject_single"]; ?> Name</label>
                            <input type="text" id="inputSubjectName" name="inputSubjectName" class="form-control" placeholder="<?php echo $global->terminology["subject_single"]; ?> Name">
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label><?php echo $global->terminology["subject_single"]; ?> Description</label>
                            <textarea name="inputSubjectDescription" id="inputSubjectDescription" cols="30" rows="10" class="form-control" placeholder="<?php echo $global->terminology["subject_single"]; ?> Description"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button type="submit" class="btn blue btn-outline" id="btnAddEditSubject"><span class="js-subject-action">New</span> <?php echo $global->terminology["subject_single"]; ?></button>
            </div>
        </div>
    </div>
</div>