<div id="modalGenerateStudents" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add New <?php echo $global->terminology["student_plural"]; ?></h4>
            </div>
            <div class="modal-body">
                <form role="form" id="frmGenStudents">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Available Enrollment Keys: <span class="badge badge-warning js-available-keys">50</span></label>
                            <p class="text-info">NOTE: <?php echo $global->terminology["student_plural"]; ?> generation will use the available <?php echo strtolower($global->terminology["course_single"]); ?> keys.</p>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label>Number of <?php echo $global->terminology["student_plural"]; ?> to generate :</label>
                            <input type="number" id="inputGenStudents" name="inputGenStudents" class="form-control" placeholder="Enter number">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button type="submit" class="btn blue btn-outline" id="btnGenStudents">Generate <?php echo $global->terminology["student_plural"]; ?></button>
            </div>
        </div>
    </div>
</div>