<div id="modalAddEditCourse" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><span class="js-course-action">Add</span> <?php echo $global->terminology["course_single"]; ?></h4>
            </div>
            <div class="modal-body">
                <!-- BEGIN FORM-->
                <form role="form" id="formAddEditCourse" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> <?php echo $global->terminology["course_single"]; ?> <span class="js-course-action">update</span> is successful! </div>
                        <div class="form-group margin-top-20">
                            <label><?php echo $global->terminology["course_single"]; ?> Name
                                <span class="required"> * </span>
                            </label>
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" class="form-control" id="inputCourseName" name="inputCourseName" /> </div>
                        </div>
                        <div class="form-group">
                            <label><?php echo $global->terminology["course_single"]; ?> Subtitle
                                <span class="required"> * </span>
                            </label>
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" class="form-control" id="inputCourseSubtitle" name="inputCourseSubtitle" /> </div>
                        </div>
                        <div class="form-group">
                            <label><?php echo $global->terminology["course_single"]; ?> Description
                                <span class="required"> * </span>
                            </label>
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <textarea class="form-control" cols="30" rows="10" id="inputCourseDescription" name="inputCourseDescription" /> </textarea></div>
                        </div>
                        <div class="form-group">
                            <label>Social Media Share Image
                                <span class="required"> * </span>
                            </label>
                            <div class="btn-block">
                                <button type="button" class="btn green btn-social-upload">Select Image</button>
                                <img src="" alt="" style="width: 160px" id="jsSocialImage">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Target Audience
                                <span class="required"> * </span>
                            </label>
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" class="form-control" id="inputTargetAudience" name="inputTargetAudience" /> </div>
                        </div>
                        <div class="form-group">
                            <label>Tags
                            </label>
                            <div class="btn-block">
                                <div id="tagssuggest"></div>
                            </div>
                            <!-- <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" class="form-control" id="inputTags" name="inputTags" /> </div> -->
                        </div>
                        <div class="form-group">
                            <label><?php echo $global->terminology["course_single"]; ?> Start Date
                                <span class="required"> * </span>
                            </label>
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input id="inputCourseStartDate" name="inputCourseStartDate" class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="" /> </div>
                        </div>
                        <div class="form-group">
                            <div class="mt-checkbox-list" data-error-container="#optionCourseEndDate_error">
                                <label class="mt-checkbox">
                                    <input type="checkbox" id="optionCourseEndDate" name="optionCourseEndDate" value="1" checked=""> Set End Date
                                    <span></span>
                                </label>
                            </div>
                            <div id="optionCourseEndDate_error"> </div>
                        </div>
                        <div class="form-group js-course-end-date">
                            <label><?php echo $global->terminology["course_single"]; ?> End Date
                                <span class="required"> * </span>
                            </label>
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input id="inputCourseEndDate" name="inputCourseEndDate" class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="" /> </div>
                        </div>
                        <div class="form-group">
                            <label><?php echo $global->terminology["course_single"]; ?> Category</label>
                            <div class="input-icon right">
                                <i class="fa"></i><?php
                                if(count(($global->courseCategories))) { ?>
                                <select multiple id="selectCourseCategory" name="selectCourseCategory" class="form-control"><?php
                                    foreach ($global->courseCategories as $key => $category) {
                                        echo "<option value='{$category['id']}'>{$category['category']}</option>";
                                    }?>
                                </select><?php
                                } ?>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
                <form action="file1.php" id="formSocialImage">
                    <input type="file" id="fileSocialImage" name="fileSocialImage">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button type="submit" class="btn blue btn-outline" id="btnAddEditCourse">Save</button>
            </div>
        </div>
    </div>
</div>