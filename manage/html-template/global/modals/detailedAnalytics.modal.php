<!--Modal for contacts-->
<div aria-hidden="true" aria-labelledby="detailedAnalyticsModal" role="dialog" tabindex="-1" id="detailedAnalyticsModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h4 class="modal-title">
					Summary of <?php echo $global->terminology["student_plural"]; ?> <?php echo strtolower($global->terminology["subject_single"]); ?> progress
				</h4>
			</div>
			<div class="modal-body">
				<table class="display table table-bordered table-striped" id="tableStudentProgress">
					<thead>
						<tr>
							<th>ID</th>
							<th><?php echo $global->terminology["student_single"]; ?> Name</th>
							<th class="text-center">Content:<span id="content-name"></span><br> Watched (%)  </th>    
							<th class="text-center">Total time spent by <?php echo $global->terminology["student_single"]; ?><br> (hh:mm:ss)</th>                    
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>