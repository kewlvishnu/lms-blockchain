<!-- Modal -->
<div class="modal fade" id="exportExamModal" tabindex="-1" role="dialog" aria-labelledby="exportExamModalLabel">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exportExamModalLabel">Export Exam</h4>
			</div>
			<div class="modal-body">
				<table id="exportExamDetail" class="table table-bordered">
					<thead>
						<tr>
							<th rowspan="2">Section/Categories</th>
							<th rowspan="2" width="75px" class="text-center">Questions</th>
							<th rowspan="2" width="75px" class="text-center">Marks</th>
							<th colspan="2" class="text-center">Export</th>
						</tr>
						<tr>
							<th width="75px">Questions</th>
							<th width="75px">Marks</th>
						</tr>
					</thead>
					<tbody></tbody>
					<tfoot>
						<tr>
							<td></td>
							<td colspan="2" class="text-right">Total</td>
							<td class="text-center total-export-questions">0</td>
							<td class="text-center total-export-marks">0</td>
						</tr>
					</tfoot>
				</table>
				<div id="exportedExamDetail" class="hide export-exam-block"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn green" id="btnExportExam">Export</button>
				<button type="button" class="btn green-jungle hide" id="btnPrintExam">Print</button>
			</div>
		</div>
	</div>
</div>