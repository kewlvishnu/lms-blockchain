<div id="modalUploadDemoVideo" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Upload Demo Video</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="note note-success">Please use pdf files only. The maximum file size can be 1 GB</div>
                        <div class="well">
                            <div class="form-group clearfix">
                                <label class="col-md-4">Select file to upload :</label>
                                <div class="col-md-4">
                                    <button type="button" class="btn blue btn-block btn-upload-video">Select Video</button>
                                    <input type="file" name="demo-course-video" class="hide upload-file" />
                                </div>
                                <?php if(!empty($courseDetails->courseDetails["demoVideo"])) { ?>
                                <div class="col-md-4">
                                    <button type="button" class="btn blue btn-block delete-file">Delete Video</button>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="progress">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                    <span class="sr-only"> 0% Complete </span>
                                </div>
                            </div>
                        </div>
                        <?php if(!empty($courseDetails->courseDetails["demoVideo"])) { ?>
                        <div class="well">
                            <video width="100%" height="350" id="player1" src="<?php echo $courseDetails->courseDetails["demoVideo"]; ?>" type="video/mp4" controls="controls"></video>
                        </div>
                        <?php } ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>