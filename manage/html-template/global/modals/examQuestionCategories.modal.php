<div id="examQuestionCategoriesModal" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Exam Categories</h4>
            </div>
            <div class="modal-body no-space">
                <div class="category-table clearfix">
                    <div class="table-reponsive">
                        <table class="table table-bordered no-margin">
                            <thead>
                                <tr class="bg-green-turquoise font-white">
                                    <th colspan="4">Section A: </th>
                                </tr>
                                <tr class="bg-green font-white">
                                    <th width="24%">No of questions</th>
                                    <th width="56%">Question Type</th>
                                    <th width="10%">Correct</th>
                                    <th width="10%">Wrong</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>5</td>
                                    <td>Multiple Choice questions</td>
                                    <td>+2.00</td>
                                    <td>-1.00</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>True/False</td>
                                    <td>+2.00</td>
                                    <td>-1.00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>