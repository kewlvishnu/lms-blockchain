<div id="modalPurchaseCourseKeys" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Invite <?php echo $global->terminology["student_plural"]; ?></h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Enter No of <?php echo $global->terminology["course_single"]; ?> Enrollment Key :</label>
                            <input type="text" id="inputTotalKeys" name="inputTotalKeys" class="form-control" placeholder="Enter number of keys to buy" />
                        </div>
                        <div class="form-group">
                            <div class="text-info">Price: <span id="keyrate">0</span>/Enrollment Key</div>
                        </div>
                        <div class="form-group">
                            <div class="text-warning">Total Amount: <span id="totalkeyrate">0</span></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button type="submit" class="btn blue btn-outline" id="purchaseNow">Buy Now</button>
                <button type="button" class="btn green btn-outline" id="keysPurchase">Get</button>
            </div>
        </div>
    </div>
</div>