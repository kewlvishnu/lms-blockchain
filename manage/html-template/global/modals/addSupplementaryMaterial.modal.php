<div id="modalAddSupplementaryMaterial" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Supplementary Material</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="formSupplementary" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="well">
                            <div class="form-group clearfix">
                                <label class="col-md-6">Select file to upload :</label>
                                <div class="col-md-6">
                                    <button type="button" class="btn blue btn-block btn-upload-material">Select Material</button>
                                    <input type="file" class="hide upload-file" name="uploaded-supplementary-stuff" />
                                    <input type="hidden" class="hide file-name" name="fileName" />
                                    <input type="hidden" id="suppContentId" name="suppContentId">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="progress">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                    <span class="sr-only"> 0% Complete (warning) </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="note note-success">
                                <p> Use any file no larger than 1.0 GiB. </p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>