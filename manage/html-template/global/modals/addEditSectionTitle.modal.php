<div id="modalAddEditSection" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><span class="js-section-action">Add</span> Section</h4>
            </div>
            <div class="modal-body">
                <!-- BEGIN FORM-->
                <form role="form" id="formSectionTitle">
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> Section title is updated! </div>
                        <div class="form-group margin-top-20">
                            <label for="sectionName">Section Name
                                <span class="required"> * </span>
                            </label>
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" class="form-control" name="sectionName" id="sectionName" /> </div>
                        </div>
                        <input type="hidden" id="sectionHeadingId" name="sectionHeadingId">
                        <div class="note note-success">
                            <p>Note after clicking on <strong>Save</strong>, please click on <strong>Add Lecture</strong>. You can add multiple lectures.</p>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
            <div class="modal-footer">
                <div class="pull-left">
                    <button id="btnDeleteSection" type="button" class="btn red btn-outline btn-delete"><i class="fa fa-trash"></i> Delete</button>
                </div>
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button id="btnSaveSectionTitle" type="submit" class="btn blue btn-outline"><i class="fa fa-save"></i> Save</button>
            </div>
        </div>
    </div>
</div>