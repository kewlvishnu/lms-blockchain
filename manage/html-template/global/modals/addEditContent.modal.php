<div id="modalAddEditContent" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><span class="js-content-title-action">Add</span> Content Title</h4>
            </div>
            <div class="modal-body">
                <!-- BEGIN FORM-->
                <div id="formContent">
                    <div class="form-body">
                        <div class="alert alert-danger hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                        <div class="alert alert-success hide">
                            <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                        <div id="selectContentType" class="text-center">
                            <div class="btn btn-sm btn-outline green btn-content-type" data-action="youtube"><i class="fa fa-youtube"></i> Youtube</div>
                            <div class="btn btn-sm btn-outline green btn-content-type" data-action="vimeo"><i class="fa fa-vimeo"></i> Vimeo</div>
                            <div class="btn btn-sm btn-outline green btn-content-type" data-action="video"><i class="fa fa-play-circle-o"></i> Video</div>
                            <div class="btn btn-sm btn-outline green btn-content-type" data-action="text"><i class="fa fa-book"></i> Text</div>
                            <div class="btn btn-sm btn-outline green btn-content-type" data-action="presentation"><i class="fa fa-file-powerpoint-o"></i> Presentation</div>
                            <div class="btn btn-sm btn-outline green btn-content-type" data-action="document"><i class="fa fa-file-word-o"></i> Document</div>
                            <div class="btn btn-sm btn-outline green btn-content-type" data-action="downloadable"><i class="fa fa-download"></i> Downloadable</div>
                            <div class="btn btn-sm btn-outline green btn-content-type" data-action="quiz"><i class="fa fa-question"></i> Quiz</div>
                            <div class="btn btn-sm btn-outline green btn-content-type" data-action="survey"><i class="fa fa-book"></i> Feedback Survey</div>
                        </div>
                        <div class="hide js-content-type" id="youtubeContent">
                            <form role="form" id="formYoutube">
                                <div class="form-group margin-top-20">
                                    <label>Youtube URL
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="url" class="form-control" name="inputYoutube" id="inputYoutube" /> </div>
                                </div>
                                <div class="note note-success">
                                    <p>Enter your youtube url link not the embed link.</p>
                                </div>
                            </form>
                        </div>
                        <div class="hide js-content-type" id="vimeoContent">
                            <form role="form" id="formVimeo">
                                <div class="form-group margin-top-20">
                                    <label>Vimeo URL
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="text" class="form-control" name="inputVimeo" id="inputVimeo" /> </div>
                                </div>
                                <div class="note note-success">
                                    <p>Enter your vimeo video url.</p>
                                </div>
                            </form>
                        </div>
                        <div class="hide js-content-type" id="videoContent">
                            <form role="form" id="formVideo" method="post" enctype="multipart/form-data">
                                <div class="form-group margin-top-20">
                                    <label>Video
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="input-icon right">
                                        <button type="button" class="btn blue btn-block btn-upload-video">Select Video</button>
                                        <input type="file" class="hide upload-file" name="uploaded-content-stuff" />
                                        <input type="hidden" class="hide file-name" name="fileName" /> </div>
                                </div>
                                <div class="form-group">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            <span class="sr-only"> 0% Complete (warning) </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="note note-success">
                                    <p>Please upload your video (only mp4 format supported).</p>
                                </div>
                            </form>
                        </div>
                        <div class="hide js-content-type" id="textContent">
                            <form role="form" id="formText">
                                <div class="form-group margin-top-20">
                                    <label>Text
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <textarea class="ckeditor form-control" name="textEditor" id="textEditor" cols="30" rows="10"></textarea> </div>
                                </div>
                                <div class="note note-success">
                                    <p>Enter your text above.</p>
                                </div>
                            </form>
                        </div>
                        <div class="hide js-content-type" id="presentationContent">
                            <form role="form" id="formPresentation" method="post" enctype="multipart/form-data">
                                <div class="form-group margin-top-20">
                                    <label>Presentation
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="input-icon right">
                                        <button type="button" class="btn blue btn-block btn-upload-presentation">Select Presentation</button>
                                        <input type="file" class="hide upload-file" name="uploaded-content-stuff" />
                                        <input type="hidden" class="hide file-name" name="fileName" /> </div>
                                </div>
                                <div class="form-group">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            <span class="sr-only"> 0% Complete (warning) </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="note note-success">
                                    <p>Please upload your presentation in PDF format.</p>
                                </div>
                            </form>
                        </div>
                        <div class="hide js-content-type" id="documentContent">
                            <form role="form" id="formDocument" method="post" enctype="multipart/form-data">
                                <div class="form-group margin-top-20">
                                    <label>Document
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="input-icon right">
                                        <button type="button" class="btn blue btn-block btn-upload-document">Select Document</button>
                                        <input type="file" class="hide upload-file" name="uploaded-content-stuff" />
                                        <input type="hidden" class="hide file-name" name="fileName" /> </div>
                                </div>
                                <div class="form-group">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            <span class="sr-only"> 0% Complete (warning) </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="note note-success">
                                    <p>Please upload your document in PDF format.</p>
                                </div>
                            </form>
                        </div>
                        <div class="hide js-content-type" id="downloadableContent">
                            <form role="form" id="formDownloadable" method="post" enctype="multipart/form-data">
                                <div class="form-group margin-top-20">
                                    <label>Downloadable
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="input-icon right">
                                        <button type="button" class="btn blue btn-block btn-upload-download">Select Downloadable</button>
                                        <input type="file" class="hide upload-file" name="uploaded-content-stuff" />
                                        <input type="hidden" class="hide file-name" name="fileName" /> </div>
                                </div>
                                <div class="form-group">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            <span class="sr-only"> 0% Complete (warning) </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="note note-success">
                                    <p>Please upload your downloadable file. There is not restriction on file type.</p>
                                </div>
                            </form>
                        </div>
                        <div class="hide js-content-type" id="quizContent">
                            <form role="form" class="margin-top-20" id="formQuiz" method="post" enctype="multipart/form-data">
                                <div class="alert alert-danger" style="display: none">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success hide" style="display: none"></div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="quizTitle" name="quizTitle" placeholder="Quiz Title">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="3" id="quizDescription" name="quizDescription" placeholder="Quiz Description"></textarea>
                                </div>
                                <!-- <div class="form-group">
                                    <button type="submit" class="btn green" id="addQuizQ"><i class="fa fa-save"></i> Save</button>
                                </div> -->
                            </form>
                            <hr>
                            <div class="js-quiz-q"></div>
                            <form role="form" id="formQuizQuestion" method="post" enctype="multipart/form-data">
                                <div class="form-body">
                                    <div class="alert alert-danger" style="display: none">
                                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                    <div class="alert alert-success hide" style="display: none"></div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="3" id="quizQuestion" name="quizQuestion" placeholder="Question"></textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" class="quiz-answer" id
                                                        ="quizAnswer1" name="quizAnswer[]" value="a">
                                                    </span>
                                                    <input type="text" class="form-control" id="quizOption1" name="quizOption1" placeholder="Option A">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" class="quiz-answer" id
                                                        ="quizAnswer2" name="quizAnswer[]" value="b">
                                                    </span>
                                                    <input type="text" class="form-control" id="quizOption2" name="quizOption2" placeholder="Option B">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" class="quiz-answer" id
                                                        ="quizAnswer3" name="quizAnswer[]" value="c">
                                                    </span>
                                                    <input type="text" class="form-control" id="quizOption3" name="quizOption3" placeholder="Option C">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" class="quiz-answer" id
                                                        ="quizAnswer4" name="quizAnswer[]" value="d">
                                                    </span>
                                                    <input type="text" class="form-control" id="quizOption4" name="quizOption4" placeholder="Option D">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group js-add-quiz">
                                        <button type="submit" class="btn green" id="addQuizQ"><i class="fa fa-plus"></i> Add Question</button>
                                    </div>
                                    <div class="form-group js-update-quiz hide">
                                        <button type="submit" class="btn green" id="updateQuizQ"><i class="fa fa-save"></i> Update Question</button>
                                        <button type="button" class="btn red" id="cancelQuizQ">Cancel</button>
                                    </div>
                                </div>
                            </form>
                            <div class="note note-success">
                                <p>Please click on add question, to add questions to this quiz. Select the right answers using checkbox.</p>
                            </div>
                        </div>
                        <div class="hide js-content-type" id="surveyContent">
                            <form role="form" id="formText">
                                <div class="form-group margin-top-20">
                                    <label>Survey Type
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="mt-radio-inline">
                                        <label class="mt-radio">
                                            <input type="radio" name="optionSurvey" value="google" class="option-survey" checked=""> Google Forms
                                            <span></span>
                                        </label>
                                        <label class="mt-radio">
                                            <input type="radio" name="optionSurvey" value="surveymonkey" class="option-survey"> Survey Monkey
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="js-gf-id">
                                        <label>Google ID <small>(bold part shown in below example)</small>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="note note-info">https://docs.google.com/forms/d/e/<strong>1FAIpQLSd55uhEWVAjGAGJWgGzKRsWZq4bLuhrVE7jEyTtLlnvl3uOSQ</strong>/viewform</div>
                                    </div>
                                    <div class="js-sm-id hide">
                                        <label>Survey Monkey ID <small>(bold part shown in below example)</small>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="note note-info">https://www.surveymonkey.com/r/<strong>22XZM8K</strong></div>
                                    </div>
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="text" class="form-control" name="surveyId" id="surveyId" />
                                    </div>
                                </div>
                            </form>
                        </div>
                        <input type="hidden" id="contentContentId" name="contentContentId">
                    </div>
                </div>
                <!-- END FORM-->
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button id="btnSaveContent" type="submit" class="btn blue btn-outline">Save</button>
            </div>
        </div>
    </div>
</div>