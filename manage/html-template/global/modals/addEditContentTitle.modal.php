<div id="modalAddEditContentTitle" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><span class="js-content-title-action">Add</span> Content Title</h4>
            </div>
            <div class="modal-body">
                <!-- BEGIN FORM-->
                <form role="form" id="formContentTitle">
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> Content is updated! </div>
                        <div class="form-group margin-top-20">
                            <label>Lecture Name
                                <span class="required"> * </span>
                            </label>
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" class="form-control" name="lectureName" id="lectureName" /> </div>
                        </div>
                        <div class="form-group">
                            <div class="mt-checkbox-list" data-error-container="#free_demo_error">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="optionFreeDemo" /> Free Demo
                                    <span></span>
                                </label>
                            </div>
                            <div id="free_demo_error"> </div>
                        </div>
                        <input type="hidden" id="headingId" name="headingId">
                        <input type="hidden" id="contentId" name="contentId">
                        <div class="note note-success">
                            <p>You can choose the content type after clicking <strong>Save</strong></p>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
            <div class="modal-footer">
                <div class="pull-left">
                    <button id="btnDeleteHeading" type="button" class="btn red btn-outline btn-delete"><i class="fa fa-trash"></i> Delete</button>
                </div>
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button id="btnSaveHeading" type="submit" class="btn blue btn-outline">Save</button>
            </div>
        </div>
    </div>
</div>