<div id="modalTransferStudents" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Transfer <?php echo $global->terminology["student_plural"]; ?></h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-body">
                        <div class="form-group">
                            <label for="">Select <?php echo $global->terminology["course_single"]; ?> to transfer</label>
                            <select name="transferDropdown" id="transferDropdown" class="form-control"></select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button type="submit" class="btn blue btn-outline" id="btnTransfer">Transfer <?php echo $global->terminology["student_plural"]; ?></button>
            </div>
        </div>
    </div>
</div>