<div id="modalAnswerFile" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Answer File</h4>
            </div>
            <div class="modal-body">
                <div id="jsAnswer">
                    <img src="http://dmsd6g597imqb.cloudfront.net/user-data/files/subjective-upload-1-1458505164.png" class="img-responsive" alt="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>