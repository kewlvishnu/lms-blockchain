<div id="modalViewReviews" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Reviews</h4>
            </div>
            <div class="modal-body">
                <div class="rate-reviews">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <h4>Average Ratings</h4>
                            <div class="text-warning avg-rate">0</div>
                            <div class="avg-rating" title="">
                                <input type="number" min="0" max="5" step="0.1" value="4" data-size="md">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <h4>Details</h4>
                            <ul class="list-unstyled list-rates">
                                <li>
                                    <div class="item-title">5 Stars</div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning progress5" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            <span class="sr-only"> 0% Complete (warning) </span>
                                        </div>
                                    </div>
                                    <div class="item-count">(<span class="count5">0</span>)</div>
                                </li>
                                <li>
                                    <div class="item-title">4 Stars</div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning progress4" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            <span class="sr-only"> 0% Complete (warning) </span>
                                        </div>
                                    </div>
                                    <div class="item-count">(<span class="count4">0</span>)</div>
                                </li>
                                <li>
                                    <div class="item-title">3 Stars</div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning progress3" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            <span class="sr-only"> 0% Complete (warning) </span>
                                        </div>
                                    </div>
                                    <div class="item-count">(<span class="count3">0</span>)</div>
                                </li>
                                <li>
                                    <div class="item-title">2 Stars</div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning progress2" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            <span class="sr-only"> 0% Complete (warning) </span>
                                        </div>
                                    </div>
                                    <div class="item-count">(<span class="count2">0</span>)</div>
                                </li>
                                <li>
                                    <div class="item-title">1 Star</div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning progress1" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            <span class="sr-only"> 0% Complete (warning) </span>
                                        </div>
                                    </div>
                                    <div class="item-count">(<span class="count1">0</span>)</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="comments" id="reviews">
                    <h3>Reviews :</h3>
                    <ul class="list-unstyled list-comments">
                        <!-- <li class="comment-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="user-block">
                                        <div class="user-avatar">
                                            <img src="assets/layouts/layout2/img/avatar3_small.jpg" class="img-circle" alt="">
                                        </div>
                                        <span class="user-name">Student Eight</span>
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="rating">
                                        <input class="star-rating" type="number" class="rating" min="0" max="5" step="0.1" value="4" data-size="xs">
                                        <span class="comment-time"> 10 May 2016 01:51</span>
                                        <span class="subject"> (Subject: Power Electronics)</span>
                                    </div>
                                    <h4>really very motivating. </h4>
                                    <div class="comment">
                                        <p>i love it here</p>
                                    </div>
                                </div>
                            </div>
                        </li> -->
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>