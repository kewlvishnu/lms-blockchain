<!--Modal for academic positions-->
<div aria-hidden="true" aria-labelledby="importSubjectQuestionsModal" role="dialog" tabindex="-1" id="importSubjectQuestionsModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h4 class="modal-title">
					Import Exam from Question Bank
				</h4>
			</div>
			<div class="modal-body">
				<div class="text text-info"><small>Please select below options to view the list of questions.</small></div>
				<form action="" id="formImportExam">
					<div class="form-group">
						<p class="help-block text-danger"></p>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
								<select class="form-control input-sm courseSelect" disabled>
									<option value="0" selected>Select <?php echo $global->terminology["course_single"]; ?></option>
								</select>
							</div>
							<div class="col-md-6">
								<select class="form-control input-sm subjectSelect" disabled>
									<option value="0" selected>Select <?php echo $global->terminology["subject_single"]; ?></option>
								</select>
							</div>
						</div>
					</div>
					<div class="">
						<div class="form-group">
							<div class="row">
								<div class="col-md-4">
									<label><small>Select Exam</small></label>
									<select class="form-control input-sm assignmentSelect" disabled>
										<option value="0" selected>Select Exam</option>
									</select>
								</div>
								<div class="col-md-4">
									<label><small>Select Difficulty</small></label>
									<select class="form-control input-sm" id="filterDifficulty" name="filterDifficulty" disabled>
										<option value="0">All Difficulties</option>
                                        <option value="1">Very Easy</option>
                                        <option value="2">Easy</option>
                                        <option value="3">Fair</option>
                                        <option value="4">Difficult</option>
                                        <option value="5">Very Difficult</option>
									</select>
								</div>
								<div class="col-md-4">
									<label><small>Select Learning Objectives</small></label>
                                    <div id="tagsfilter"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="table-scrollable" id="listSubjectQuestions">
						<table class="table table-hover table-striped hide">
							<thead>
								<tr>
									<th><input type="checkbox" class="checkbox-inline" id="selectSQAll"></th>
									<th></th>
									<th colspan="3">Question</th>
									<th>Type</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</form>
			</div>
            <div class="modal-footer">
				<button type="button" class="btn btn-primary" id="modalImportSubjectQuestions">Submit</button>
                <button type="button" data-dismiss="modal" class="btn dark btn-outline cancel-button">Close</button>
            </div>
		</div>
	</div>
</div>