<div id="modalInviteStudentsToCourse" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Invite <?php echo $global->terminology["student_plural"]; ?></h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-body">
                        <div class="form-group">
                            <label for="">Select <?php echo $global->terminology["course_single"]; ?> :</label>
                            <select name="courses" id="courses" class="form-control"></select>
                        </div>
                        <div class="form-group">
                            <label>Pending Enrollments: <span class="badge badge-warning js-pending">0</span></label>
                            <label>Invitations Remaining: <span class="badge badge-warning js-remaining">0</span></label>
                            <p class="text-info">NOTE: To send multiple invitations, please separate email-addresses by using Comma(,).</p>
                        </div>
                        <div class="form-group">
                            <label>Enter email address :</label>
                            <textarea name="textEmailAddresses" id="textEmailAddresses" cols="30" rows="10" class="form-control" placeholder="Enter email address here"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Email Template :</label>
                            <div class="note note-info"><code>[course_invite_block]</code> - This contains the course invite block.</div>
                            <textarea name="textEmailNote" id="textEmailNote" cols="30" rows="10" class="form-control ckeditor" placeholder="Enter custom note for email"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button type="submit" class="btn blue btn-outline" id="btnInviteStudent">Invite <?php echo $global->terminology["student_plural"]; ?></button>
            </div>
        </div>
    </div>
</div>