<div id="modalCopyExam" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Copy Exam/Assignment</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="frmNewPortfolio">
                    <div class="form-body">
                        <div class="form-group">
                            <label for="copyExamTitle">New Exam Name</label>
                            <input type="text" id="copyExamTitle" name="copyExamTitle" class="form-control">
                            <span class="help-block"></span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button type="submit" class="btn blue btn-outline" id="btnCopyExam">Submit</button>
            </div>
        </div>
    </div>
</div>