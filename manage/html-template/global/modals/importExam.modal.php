<!--Modal for academic positions-->
<div aria-hidden="true" aria-labelledby="importExamModal" role="dialog" tabindex="-1" id="importExamModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h4 class="modal-title">
					Import Exam from Question Bank
				</h4>
			</div>
			<div class="modal-body">
				<form action="" id="formImportExam">
					<div class="form-group">
						<p class="help-block text-danger"></p>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-4">
								<select class="form-control input-sm courseSelect" disabled>
									<option value="0" selected>Select <?php echo $global->terminology["course_single"]; ?></option>
								</select>
							</div>
							<div class="col-md-4">
								<select class="form-control input-sm subjectSelect" disabled>
									<option value="0" selected>Select <?php echo $global->terminology["subject_single"]; ?></option>
								</select>
							</div>
							<div class="col-md-4">
								<select class="form-control input-sm chapterSelect" disabled>
									<option value="-1" selected>Select Chapter</option>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-4">
								<select class="form-control input-sm assignmentSelect" disabled>
									<option value="0" selected>Select Assignment</option>
								</select>
							</div>
							<div class="col-md-8">
								<select class="form-control input-sm importCategorySelect" disabled>
									<option value="0" selected>Select Import Category</option>
								</select>
							</div>
						</div>
					</div>
					<div id="initialView" class="note note-info">Please select above options to view the list of questions.</div>
					<div class="import-question table-scrollable">
						<table class="table table-hover table-striped hide">
							<thead>
								<tr>
									<th><input type="checkbox" class="checkbox-inline" id="selectAll"></th>
									<th></th>
									<th colspan="3">Question</th>
									<th>Type</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</form>
			</div>
            <div class="modal-footer">
				<button type="button" class="btn btn-primary" id="modalImportQuestion">Submit</button>
                <button type="button" data-dismiss="modal" class="btn dark btn-outline cancel-button">Close</button>
            </div>
		</div>
	</div>
</div>