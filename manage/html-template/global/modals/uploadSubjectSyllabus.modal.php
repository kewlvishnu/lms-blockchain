<div id="modalUploadSubjectSyllabus" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Upload <?php echo $global->terminology["subject_single"]; ?> <?php echo $global->terminology["syllabus_single"]; ?></h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="well">
                            <div class="form-group clearfix">
                                <label class="col-md-6">Select file to upload :</label>
                                <div class="col-md-6">
                                    <button type="buton" class="btn blue btn-block btn-upload-syllabus">Select Document</button>
                                    <input type="file" name="course-syllabus" class="hide upload-file" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="progress">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                    <span class="sr-only"> 0% Complete (warning) </span>
                                </div>
                            </div>
                        </div>
                        <?php if(!empty($subjectDetails["syllabus"])) { ?>
                        <div class="well">
                            <iframe id="viewer" src="<?php echo $sitepathManageIncludes; ?>assets/custom/js/plugins/pdfjs/web/viewer.html?‌​file=<?php echo $subjectDetails["syllabus"]; ?>" class="btn-block min-height-700" frameborder="0"></iframe>
                        </div>
                        <?php } ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>