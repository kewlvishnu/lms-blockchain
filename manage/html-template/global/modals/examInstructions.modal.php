<!--Modal for academic positions-->
<div aria-hidden="true" aria-labelledby="examInstructionsModal" role="dialog" tabindex="-1" id="examInstructionsModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h4 class="modal-title">
					Instructions
				</h4>
			</div>
			<div class="modal-body">
				<div class="ins-container"></div>
				<div class="edit-ins-container row">
					<div class="col-md-12">
						<label for="instructions">Your additional Instructions</label>
						<textarea rows="8" class="form-control"></textarea>
					</div>
				</div>
			</div>
            <div class="modal-footer">
				<button type="button" class="btn btn-primary" id="makeExamLive">Submit</button>
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
		</div>
	</div>
</div>