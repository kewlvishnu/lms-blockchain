<div id="modalUploadSubjectImage" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Change <?php echo $global->terminology["subject_single"]; ?> Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                        <p> Please select an image for your <?php echo strtolower($global->terminology["subject_single"]); ?> picture. </p>
                        <form action="#" role="form">
                            <!-- <div class="form-group">
                                <input type="file" />
                                <img class="crop" style="display:none" />
                                <button type="submit" style="display:none">Upload</button>
                            </div> -->
                            <div class="form-group">
                                <div class="profile-pic">
                                    <div class="thumbnail">
                                        <img id="subjectPic" src="<?php echo $subjectDetails["image"]; ?>" alt="" /> </div>
                                    <div>
                                        <span class="btn default btn-file">
                                            <span> Change image </span>
                                            <input id="fileSubjectPic" type="file" name="..."> </span>
                                    </div>
                                </div>
                            </div>
                            <div class="margin-top-10 hide js-upload">
                                <a href="javascript:;" class="btn green" id="btnUpload">Upload</a>
                                <a href="javascript:;" class="btn default"> Cancel </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>