<div id="modalImportSubject" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Import <?php echo $global->terminology["subject_single"]; ?></h4>
            </div>
            <div class="modal-body no-space">
                <table class="table table-bordered" id="tableImportCourses">
                    <thead>
                        <tr class="success">
                            <th colspan="4" class="text-center">Existing <?php echo $global->terminology["course_plural"]; ?></th>
                        </tr>
                        <tr class="success">
                            <th colspan="2" class="text-center"><?php echo $global->terminology["course_single"]; ?></th>
                            <th width="20%">Expiry Date</th>
                            <th width="15%">Licensed</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- <tr>
                            <td>
                                <label class="mt-checkbox mt-checkbox-outline">
                                    <input type="checkbox"> Course 1
                                    <span></span>
                                </label>
                            </td>
                            <td>No Expiry</td>
                            <td>Licensed</td>
                        </tr> -->
                    </tbody>
                </table>
                <table class="table table-bordered no-margin" id="tableImportPurchasedCourses">
                    <thead>
                        <tr class="warning">
                            <th colspan="4" class="text-center">Purchased <?php echo $global->terminology["course_plural"]; ?></th>
                        </tr>
                        <tr class="warning">
                            <th colspan="2" class="text-center"><?php echo $global->terminology["course_single"]; ?></th>
                            <th width="20%">Expiry Date</th>
                            <th width="15%">Licensed</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- <tr>
                            <td>
                                <label class="mt-checkbox mt-checkbox-outline">
                                    <input type="checkbox"> Course 1
                                    <span></span>
                                </label>
                            </td>
                            <td>No Expiry</td>
                            <td>Licensed</td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button type="button" class="btn blue btn-outline" id="btnImportSubjects">Import <?php echo $global->terminology["subject_single"]; ?></button>
            </div>
        </div>
    </div>
</div>