<div id="modalMarketplaceSettings" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo $global->terminology["student_single"]; ?> Marketplace Settings</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-body">
                        <div class="license-box">
                            <div class="license-container">
                                <div class="form-group">
                                    <label class="mt-checkbox mt-checkbox-outline"> Show <?php echo strtolower($global->terminology["course_single"]); ?> on the <?php echo $global->terminology["student_single"]; ?> market place
                                        <input type="checkbox" id="availStudentMarket" <?php echo (($courseDetails->courseDetails["availStudentMarket"]==0)?'':'checked'); ?> />
                                        <span></span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>Price in <i class="fa fa-rupee"></i> :</label>
                                    <input type="number" class="form-control price-inr" id="inputStudentPriceINR" value="<?php echo $courseDetails->courseDetails["studentPriceINR"]; ?>" placeholder="Enter number">
                                </div>
                                <div class="form-group">
                                    <label>Price in <i class="fa fa-dollar"></i> :</label>
                                    <input type="number" class="form-control price-usd" id="inputStudentPriceUSD" value="<?php echo $courseDetails->courseDetails["studentPrice"]; ?>" placeholder="Enter number">
                                </div>
                                <div class="form-group clearfix price-breakdown hide">
                                    <label>Your Amount <span class="your-price">100 INR, 10 $</span></label>
                                    <div class="pull-right">
                                        <a href="javascript:;" data-details-shown="0" class="btn green show-price-breakdown">View Details</a>
                                    </div>
                                </div>
                                <div class="price-breakdown-details hide">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="panel panel-success">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Price INR</h3>
                                                </div>
                                                <div class="panel-body in inr-breakdown">
                                                    <div><span>Arcane mind : </span><span>0.00 INR</span></div>
                                                    <div><span>SC : </span><span>0.00 INR</span></div>
                                                    <div><span>VAT : </span><span>0.00 INR</span></div>
                                                    <div><span>TC : </span><span>0.00 INR</span></div>
                                                    <div><span>entertain : </span><span>0.00 INR</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="panel panel-success">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Price USD</h3>
                                                </div>
                                                <div class="panel-body in usd-breakdown">
                                                    <div><span>Arcane mind : </span><span>0.00$</span></div>
                                                    <div><span>SC : </span><span>0.00$</span></div>
                                                    <div><span>VAT : </span><span>0.00$</span></div>
                                                    <div><span>TC : </span><span>0.00$</span></div>
                                                    <div><span>entertain : </span><span>0.00$</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button type="button" class="btn blue btn-outline" id="btnSaveRateStudent">Save Rates</button>
                <button type="button" class="btn green btn-outline" id="live-course-student" <?php echo (($courseDetails->courseDetails["availStudentMarket"]==0)?'disabled':''); ?>>Go Live</button>
                <button type="button" class="btn green btn-outline hide" id="unlive-course-student">Put <?php echo $global->terminology["course_single"]; ?> Offline</button>
            </div>
        </div>
    </div>
</div>