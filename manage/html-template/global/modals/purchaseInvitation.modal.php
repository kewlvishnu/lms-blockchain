<div id="modalPurchaseInvitation" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Customer Support</h4>
            </div>
            <div class="modal-body">
                <div class="note note-info">
                    <p>You will be soon contacted by our Sales Team at your registered Mobile No &amp; Email Id.</p>
                    <p>You can also get in touch with us at <a href="mailto:sales@integro.io">sales@integro.io.</a></p>
                    <p>If you would like to be contacted at some other mobile number or email id, please enter in the below box.</p>
                </div>
                <form role="form">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Email/Phone no :</label>
                            <input type="text" id="inputInviteEmail" name="inputInviteEmail" class="form-control" placeholder="Enter Email/Phone no" />
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button type="submit" class="btn blue btn-outline" id="keys-purchaseInvitation">Send</button>
            </div>
        </div>
    </div>
</div>