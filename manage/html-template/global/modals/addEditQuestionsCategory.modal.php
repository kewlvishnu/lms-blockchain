<div id="modalAddEditQuestionsCategory" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Category</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-body">
                        <div class="form-group">
                            <label for=""><?php echo $global->terminology["student_single"]; ?> will get random questions? <input type="checkbox" id="randomQuestions" class="" data-on-text="Yes" data-off-text="No"></label>
                        </div>
                        <div class="form-group note-order-drag">
                            <p class="note note-info">Click and drag the table rows to set the order of categories.</p>
                        </div>
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th colspan="3">Question Type</th>
                                    <th>Correct Answer</th>
                                    <th>Wrong Answer</th>
                                    <th>No of Question</th>
                                    <th>Total</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr data-id="0">
                                    <td colspan="3">
                                        <select class="form-control input-sm questionTypeSelect">
                                            <option value="-1" selected="">Select Category</option>
                                            <option value="0">Multiple Choice Question</option>
                                            <option value="1">True or False</option>
                                            <option value="2">Fill in the blanks</option>
                                            <option value="3">Match the following</option>
                                            <option value="4">Comprehensive Type Question</option>
                                            <option value="5">Dependent Type Question</option>
                                            <option value="6">Multiple Answer Question</option>
                                            <option value="7">Audio question</option>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-plus"></i>
                                            </span>
                                            <input type="text" class="form-control input-sm plus" placeholder="0">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-minus"></i>
                                            </span>
                                            <input type="text" class="form-control input-sm minus" placeholder="0">
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control input-sm number" placeholder="0">
                                    </td>
                                    <td class="text-right">
                                        <strong><span class="total">0</span></strong>
                                    </td>
                                </tr><?php /*
                                <tr data-id="223">
                                    <td colspan="3">
                                        <select class="form-control input-sm questionTypeSelect" disabled="disabled">
                                            <option value="-1" selected="">Select Category</option>
                                            <option value="0">Multiple Choice Question</option>
                                            <option value="1">True or False</option>
                                            <option value="2">Fill in the blanks</option>
                                            <option value="3">Match the following</option>
                                            <option value="4">Comprehensive Type Question</option>
                                            <option value="5">Dependent Type Question</option>
                                            <option value="6">Multiple Answer Question</option>
                                            <option value="7">Audio question</option>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-plus"></i>
                                            </span>
                                            <input type="text" class="form-control input-sm plus" placeholder="0">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-minus"></i>
                                            </span>
                                            <input type="text" class="form-control input-sm minus" placeholder="0">
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control input-sm number" placeholder="5">
                                    </td>
                                    <td class="text-right">
                                        <strong><span class="total">10</span></strong>
                                    </td>
                                </tr>
                                <tr class="generated" data-id="666">
                                    <td colspan="3">
                                        <select class="form-control input-sm questionTypeSelect" disabled="disabled">
                                            <option value="-1">Select Category</option>
                                            <option value="0">Multiple Choice Question</option>
                                            <option value="1">True or False</option>
                                            <option value="2">Fill in the blanks</option>
                                            <option value="3">Match the following</option>
                                            <option value="4">Comprehension Type Question</option>
                                            <option value="5">Dependent Type Question</option>
                                            <option value="6">Multiple Answer Question</option>
                                            <option value="7">Audio Question</option>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-plus"></i>
                                            </span>
                                            <input type="text" class="form-control input-sm plus" placeholder="0">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-minus"></i>
                                            </span>
                                            <input type="text" class="form-control input-sm minus" placeholder="0">
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control input-sm number" placeholder="5">
                                    </td>
                                    <td class="text-right">
                                        <strong><span class="total">20</span></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="text-right">Total</td>
                                    <td><strong>30</strong></td>
                                </tr> */ ?>
                            </tbody>
                            <tfoot class="tfoot-sm">
                                <tr class="text-right">
                                    <td colspan="6">Total</td>
                                    <td><strong id="grandTotal">0</strong></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="pull-left">
                    <button type="button" class="btn green" id="modalAddCategory"><i class="fa fa-plus-circle"></i> Category</button>
                </div>
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button type="submit" class="btn blue btn-outline" id="modalSaveCategory">Save</button>
            </div>
        </div>
    </div>
</div>