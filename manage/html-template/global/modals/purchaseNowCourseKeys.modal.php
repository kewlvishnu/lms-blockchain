<div id="modalPurchaseNowCourseKeys" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirm Purchase</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="3"><?php echo $global->terminology["course_single"]; ?> Enrollment Key</th>
                        </tr>
                        <tr>
                            <th>Unit price</th>
                            <th>Units</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><span class="price-mul"></span></td>
                            <td><span class="units"></span></td>
                            <td><span class="amount"></span></td>
                        </tr>
                        <tr>
                            <td colspan="2"><strong>You pay</strong></td>
                            <td><strong class="you-pay"></strong></td>
                        </tr>
                    </tbody>
                </table>
                <hr><?php /*
                <div class="pad20 text-center">
                    <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-primary active">
                        <input type="radio" name="optionPayVia" id="option1" value="p" autocomplete="off" checked> Pay via blockchain (Portis)
                      </label>
                      <label class="btn btn-primary">
                        <input type="radio" name="optionPayVia" id="option2" value="m" autocomplete="off"> Pay via metamask
                      </label>
                    </div>
                </div>
                <hr>*/ ?>
                <button class="btn green btn-block" id="payUMoneyButton">Pay Now</button><br>
                <div class="text-justified">
                    <p>You will be redirected to payment page and then sent back once you complete your purchase.</p>
                    <p>By clicking the 'Pay Now' button you agree these <a href="<?php echo $sitepath; ?>policies/terms" target="_blank">Terms and Conditions</a>.</p>
                </div> 
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>