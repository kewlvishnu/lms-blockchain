<div id="modalExamCommentsEdit" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit <span class="range-type">score</span> range</h4>
            </div>
            <div class="modal-body">
                <form class="well well-sm" method="post" id="formScorePercentBracketEdit">
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> Your data is successfully updated! </div>
                        <div class="form-group">
                            <label for="">Percent Range</label>
                            <div class="row">
                                <div class="col-xs-5">
                                    <input type="text" name="percentLow" id="percentLowEdit" class="form-control percent-low">
                                </div>
                                <div class="col-xs-2 text-center"> - </div>
                                <div class="col-xs-5">
                                    <input type="text" name="percentHigh" id="percentHighEdit" class="form-control percent-high">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Rating Comment</label>
                            <input type="text" name="comment" id="commentEdit" class="form-control comment" />
                        </div>
                        <div class="form-group">
                            <label for="">Select a Smiley</label>
                            <div class="btn-group list-smileys list-smileys-3" data-toggle="buttons"></div>
                            <div id="smileyError3"></div>
                        </div>
                        <input type="hidden" name="rangeId" id="rangeId">
                        <input type="hidden" name="rangeType" id="rangeType">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button type="submit" class="btn blue btn-outline" id="rangePercentBracketEdit">Edit <span class="range-type">score</span> range</button>
            </div>
        </div>
    </div>
</div>