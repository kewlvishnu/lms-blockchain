<div id="modalAddEditReward" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><span class="js-reward-action">Add</span> Reward</h4>
            </div>
            <div class="modal-body">
                <!-- BEGIN FORM-->
                <form role="form" id="formAddEditCourse" class="form-horizontal">
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> Reward <span class="js-reward-action">add</span> is successful! </div>
                        <div class="form-group margin-top-20">
                            <label class="col-md-3 col-md-offset-2 control-label">Percentage block <span class="required"> * </span></label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="inputRewardPercentage" id="inputRewardPercentage" placeholder="Enter Percentage">
                                            <span class="input-group-addon">
                                                <i class="fa fa-percent"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-md-offset-2 control-label">Reward Amount <span class="required"> * </span></label>
                            <div class="col-md-7">
                                <div class="js-rewards">
                                    <div class="row js-reward">
                                        <div class="col-md-4">
                                            <input type="text" name="inputRewardAmount" class="form-control reward-amount" value="">
                                        </div>
                                        <div class="col-md-4">
                                            <select name="selectRewardCurrency" class="form-control reward-currency">
                                                <option value="IGRO">IGRO</option>
                                                <option value="ETH">ETH</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn green btn-reward"><i class="fa fa-plus-circle"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button type="submit" class="btn blue btn-outline" id="btnAddEditReward">Save</button>
            </div>
        </div>
    </div>
</div>