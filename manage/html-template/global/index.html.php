<?php
	require_once('html-template/'.$userRole.'/includes/header.php');
	require_once('html-template/'.$userRole.'/includes/sidebar.php');
	
	switch ($page) {
		case 'courses':
			require_once('pages/courses.inc.php');
			break;
		case 'students':
			require_once('pages/students.inc.php');
			break;
		case 'studentGroups':
			require_once('pages/studentGroups.inc.php');
			break;
		case 'studentsInvites':
			require_once('pages/studentsInvites.inc.php');
			break;
		case 'studentsTransfer':
			require_once('pages/studentsTransfer.inc.php');
			break;
		case 'courseKeys':
			require_once('pages/courseKeys.inc.php');
			break;
		case 'courseAssignSubjects':
			require_once('pages/courseAssignSubjects.inc.php');
			break;
		case 'subjectSections':
			require_once('pages/subjectSections.inc.php');
			break;
		case 'subjectStudents':
			require_once('pages/subjectStudents.inc.php');
			break;
		case 'subjectStudentGroups':
			require_once('pages/subjectStudentGroups.inc.php');
			break;
		case 'subjectAnalytics':
			require_once('pages/subjectAnalytics.inc.php');
			break;
		case 'subjectGrading':
			require_once('pages/subjectGrading.inc.php');
			break;
		case 'subjectRewards':
			require_once('pages/subjectRewards.inc.php');
			break;
		case 'subjectPerformance':
			require_once('pages/subjectPerformance.inc.php');
			break;
		case 'studentPerformance':
			require_once('pages/studentPerformance.inc.php');
			break;
		case 'examCreate':
			require_once('pages/examCreate.inc.php');
			break;
		case 'examSettings':
			require_once('pages/examSettings.inc.php');
			break;
		case 'examObjComments':
		case 'subjectiveExamObjComments':
		case 'submissionObjComments':
			require_once('pages/examObjComments.inc.php');
			break;
		case 'manualExamObjComments':
			require_once('pages/manualExamObjComments.inc.php');
			break;
		case 'examDetail':
			require_once('pages/examDetail.inc.php');
			break;
		case 'examResult':
			require_once('pages/examResult.inc.php');
			break;
		case 'examResultStudent':
			require_once('pages/examResultStudent.inc.php');
			break;
		case 'subjectiveExamCreate':
			require_once('pages/subjectiveExamCreate.inc.php');
			break;
		case 'subjectiveExamSettings':
			require_once('pages/subjectiveExamSettings.inc.php');
			break;
		case 'subjectiveExamQuestions':
			require_once('pages/subjectiveExamQuestions.inc.php');
			break;
		case 'subjectiveExamCheck':
			require_once('pages/subjectiveExamCheck.inc.php');
			break;
		case 'subjectiveExamCheckAttempt':
			require_once('pages/subjectiveExamCheckAttempt.inc.php');
			break;
		case 'subjectiveExamResult':
			require_once('pages/subjectiveExamResult.inc.php');
			break;
		case 'subjectiveExamResultStudent':
			require_once('pages/subjectiveExamResultStudent.inc.php');
			break;
		case 'manualExamCreate':
			require_once('pages/manualExamCreate.inc.php');
			break;
		case 'manualExamImport':
			require_once('pages/manualExamImport.inc.php');
			break;
		case 'manualExamSettings':
			require_once('pages/manualExamSettings.inc.php');
			break;
		case 'manualExamResult':
			require_once('pages/manualExamResult.inc.php');
			break;
		case 'submissionCreate':
			require_once('pages/submissionCreate.inc.php');
			break;
		case 'submissionSettings':
			require_once('pages/submissionSettings.inc.php');
			break;
		case 'submissionQuestions':
			require_once('pages/submissionQuestions.inc.php');
			break;
		case 'submissionCheck':
			require_once('pages/submissionCheck.inc.php');
			break;
		case 'submissionCheckAttempt':
			require_once('pages/submissionCheckAttempt.inc.php');
			break;
		case 'submissionResult':
			require_once('pages/submissionResult.inc.php');
			break;
		case 'submissionResultStudent':
			require_once('pages/submissionResultStudent.inc.php');
			break;
		case 'subjectQuestions':
			require_once('pages/subjectQuestions.inc.php');
			break;
		case 'subjectSubjectiveQuestions':
			require_once('pages/subjectSubjectiveQuestions.inc.php');
			break;
		case 'subjectSubmissionQuestions':
			require_once('pages/subjectSubmissionQuestions.inc.php');
			break;
		case 'settings':
			require_once('pages/settings.inc.php');
			break;
		case 'portfolios':
			require_once('pages/portfolios.inc.php');
			break;
		case 'portfolio':
			require_once('pages/portfolio.inc.php');
			break;
		case 'trash':
			require_once('pages/trash.inc.php');
			break;
		default:
			echo "There was some error";
			break;
	}
	require_once('html-template/'.$userRole.'/includes/footer.php');
?>