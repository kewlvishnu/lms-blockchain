<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<?php
			require_once('html-template/'.$userRole.'/includes/header-subject.php');
		?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light ">
					<div class="portlet-title tabbable-line">
						<div class="caption caption-md">
							<i class="icon-globe theme-font hide"></i>
							<span class="caption-subject font-blue-madison bold uppercase js-student"><?php echo $global->terminology["student_single"]; ?> Performance <small></small></span>
						</div>
					</div>
					<div class="portlet-body">
                        <div class="note note-success">
                            <h4 class="block no-margin">Average <?php echo $global->terminology["subject_single"]; ?> Score : <strong id="subjectScore"></strong></h4>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover order-column">
                                <thead>
                                    <tr>
                                        <th width="20%"> Title </th>
                                        <th width="10%"> Type </th>
                                        <th width="11%"> Section </th>
                                        <th class="text-center" width="12%"> Start Date </th>
                                        <th class="text-center" width="12%"> End Date </th>
                                        <th class="text-center" width="5%"> Your / Total Attempts </th>
                                        <th width="10%"> Grade Type </th>
                                        <th width="10%"> Avg of last __ attempts </th>
                                        <th width="10%"> Weight </th>
                                        <th width="10%"> Performance </th>
                                    </tr>
                                </thead>
                                <tbody id="listExams"></tbody>
                            </table>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->