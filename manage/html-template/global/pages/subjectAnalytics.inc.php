<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<?php
			require_once('html-template/'.$userRole.'/includes/header-subject.php');
		?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<?php
					require_once('html-template/'.$userRole.'/includes/sidebar-subject.php');
				?>
				<!-- BEGIN TICKET LIST CONTENT -->
				<div class="profile-content">
					<div class="portlet light ">
						<div class="portlet-title tabbable-line">
							<div class="caption caption-md">
								<i class="icon-globe theme-font hide"></i>
								<span class="caption-subject font-blue-madison bold uppercase">Content Analytics</span>
							</div>
						</div>
						<div class="portlet-body">
							<div id="subjectAnalytics" style="height:500px;"></div>
						</div>
					</div>
				</div>
				<!-- END PROFILE CONTENT -->
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->