<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<?php
			require_once('html-template/'.$userRole.'/includes/header-subject.php');
		?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light " id="listSubjectStudents">
					<div class="portlet-title tabbable-line">
						<div class="caption caption-md">
							<i class="icon-globe theme-font hide"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo $global->terminology["subject_single"]; ?> <?php echo $global->terminology["student_plural"]; ?></span>
						</div>
						<div class="actions">
							<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>/students/groups" class="btn btn-info btn-sm"> <?php echo $global->terminology["student_single"]; ?> Groups </a>
							<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>" class="btn btn-primary btn-sm">
								<i class="fa fa-arrow-left"></i> Back </a>
							<!-- <a href="javascript:;" class="btn btn-primary btn-sm js-chat" data-student="0">
								<i class="fa fa-comment"></i> Public Chat </a> -->
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-10">
									<div class="btn-group margin-bottom-10">
										<button href="#modalGenerateStudents" data-toggle="modal" class="btn sbold green"> Add New
											<i class="fa fa-plus"></i>
										</button>
										<button href="#modalInviteStudents" data-toggle="modal" class="btn sbold blue"> Invite <?php echo $global->terminology["student_plural"]; ?>
											<i class="fa fa-plus"></i>
										</button>
									</div>
									<div class="btn-group margin-bottom-10">
										<button id="btnRemoveStudents" class="btn sbold red"> Remove
											<i class="fa fa-trash"></i>
										</button>
										<button id="btnParentCredentials" class="btn sbold yellow"> <?php echo $global->terminology["parent_single"]; ?>
											<i class="fa fa-user"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
						<table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblSubjectStudents">
							<thead>
								<tr>
									<th>
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="group-checkable" data-set="#tblSubjectStudents .checkboxes" />
											<span></span>
										</label>
									</th>
									<th> ID # </th>
									<th> Username </th>
									<th> Password </th>
									<th> Name </th>
									<th> Email </th>
									<th> <?php echo $global->terminology["parent_single"]; ?> <br><small>(username)</small> </th>
									<th> <?php echo $global->terminology["parent_single"]; ?> <br><small>(password)</small> </th>
									<!-- <th width="35%"> Actions </th> -->
								</tr>
							</thead>
							<tbody>
								<!-- <tr class="odd gradeX">
									<td>
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="checkboxes" value="1" />
											<span></span>
										</label>
									</td>
									<td>420</td>
									<td>student1</td>
									<td><div class="text-success">REGISTERED</div></td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>40356</td>
									<td>55315</td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green js-chat"><i class="fa fa-comment"></i> Chat</a>
									</td>
								</tr> -->
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->