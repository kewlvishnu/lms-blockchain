<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
        <?php require_once('html-template/'.$userRole.'/includes/header-exam.php'); ?>
		<!-- END PAGE HEADER-->
		<div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"> <?php
                            if ($page == 'examObjComments') {
                                echo "Objective Exam";
                            } else if ($page == 'subjectiveExamObjComments') {
                                echo "Subjective Exam";
                            } else if ($page == 'manualExamObjComments') {
                                echo "Manual Exam";
                            } else if ($page == 'submissionObjComments') {
                                echo "Submission";
                            } else {
                                echo "Error";
                            }
                        ?> Comments </div>
                    </div>
                    <div class="portlet-body">
                        <div class="note note-info">
                            <strong>Maximum score: <?php echo $examDetails['totalScore']; ?></strong> |
                            <strong>Total questions: <span id="totalQuestions"><?php echo $examDetails['questions']; ?></span></strong>
                        </div>
                        <div class="text-center table-responsive">
                            <table class="table table-striped table-bordered" id="tableTagAnalysis">
                                <thead>
                                    <tr>
                                        <th class="text-center" rowspan="2">No</th>
                                        <th class="text-center" rowspan="2">Tag</th>
                                        <th class="text-center" colspan="2">Appear</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center" width="25%">Count</th>
                                        <th class="text-center" width="25%">Percentage</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-bordered" id="tableScoreRanges">
                                    <thead>
                                        <tr>
                                            <th class="text-center" colspan="5">Score Ranges</th>
                                        </tr>
                                        <tr>
                                            <th>No</th>
                                            <th>Range</th>
                                            <th>Comment</th>
                                            <th class="text-center">Smiley</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center" colspan="5">No comment ranges found!</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-bordered" id="tableTagRanges">
                                    <thead>
                                        <tr>
                                            <th class="text-center" colspan="5">Tag Ranges</th>
                                        </tr>
                                        <tr>
                                            <th>No</th>
                                            <th>Range</th>
                                            <th>Comment</th>
                                            <th class="text-center">Smiley</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center" colspan="5">No comment ranges found!</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="note note-info">
                            <p>Create a percentage bracket range for exam scores</p>
                            <p>For e.g.: 0-20%: Failed, 20-40%: Work harder, 40-60%: Not bad can be better, 60-80%: Good can be better, 80-100%: Excellent!</p>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <form class="well well-sm" method="post" id="formScorePercentBracket">
                                    <h4 class="mt-clear">Exam Score Bracket</h4>
                                    <hr>
                                    <div class="form-body">
                                        <div class="alert alert-danger display-hide">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                        <div class="alert alert-success display-hide">
                                            <button class="close" data-close="alert"></button> Percent bracket successfully created! </div>
                                        <div class="form-group">
                                            <label for="">Percent Range</label>
                                            <div class="row">
                                                <div class="col-xs-5">
                                                    <input type="text" name="percentLow" id="percentLow1" class="form-control percent-low">
                                                </div>
                                                <div class="col-xs-2 text-center"> - </div>
                                                <div class="col-xs-5">
                                                    <input type="text" name="percentHigh" class="form-control percent-high">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Rating Comment</label>
                                            <input type="text" name="comment" class="form-control comment" />
                                        </div>
                                        <div class="form-group">
                                            <label for="">Select a Smiley</label>
                                            <div class="btn-group list-smileys list-smileys-1" data-toggle="buttons"></div>
                                            <div id="smileyError1"></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Action</label>
                                            <button class="btn green btn-block" id="scorePercentBracket">Create a percent bracket</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-6">
                                <form class="well well-sm" id="formTagPercentBracket">
                                    <h4 class="mt-clear">Learning Objectives Bracket</h4>
                                    <hr>
                                    <div class="form-body">
                                        <div class="alert alert-danger display-hide">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                        <div class="alert alert-success display-hide">
                                            <button class="close" data-close="alert"></button> Your data is successfully updated! </div>
                                        <div class="form-group">
                                            <label for="">Percent Range</label>
                                            <div class="row">
                                                <div class="col-xs-5">
                                                    <input type="text" name="percentLow" id="percentLow2" class="form-control percent-low">
                                                </div>
                                                <div class="col-xs-2 text-center"> - </div>
                                                <div class="col-xs-5">
                                                    <input type="text" name="percentHigh" class="form-control percent-high">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Rating Comment</label>
                                            <input type="text" name="comment" class="form-control comment" />
                                        </div>
                                        <div class="form-group">
                                            <label for="">Select a Smiley</label>
                                            <div class="btn-group list-smileys list-smileys-2" data-toggle="buttons"></div>
                                            <div id="smileyError2"></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Action</label>
                                            <button class="btn green btn-block" id="scorePercentBracket">Create a percent bracket</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->