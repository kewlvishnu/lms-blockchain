<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
        <?php require_once('html-template/'.$userRole.'/includes/header-questionbank.php'); ?>
		<!-- END PAGE HEADER-->
		<div class="row">
            <div class="col-md-4">
                <div class="portlet light portlet-fit no-space">
                    <div class="portlet-body no-space">
                        <div class="mt-element-list questions-sidebar-list">
                            <div class="mt-list-head list-simple ext-1 font-white bg-grey-dark">
                                <div class="list-head-title-container">
                                    <h3 class="list-title">Subjective Question Bank</h3>
                                </div>
                            </div>
                            <div class="mt-list-head list-default ext-1 green-jungle">
                                <div class="row">
                                    <div class="col-xs-8">
                                        <div class="list-head-title-container">
                                            <h3 class="list-title uppercase sbold no-margin">Exam Questions</h3>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <!-- Single button -->
                                        <div class="btn-group">
                                          <button type="button" class="btn btn-xs dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-plus"></i> <!-- <span class="caret"></span> -->
                                          </button>
                                          <ul class="dropdown-menu">
                                            <li><a href="javascript:void(0);" class="new-question" data-type="question">Question</a></li>
                                            <li><a href="javascript:void(0);" class="new-question" data-type="question-group">Question Group</a></li>
                                          </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-list-container list-default ext-1 no-padding">
                                <ul id="listQuestions">
                                    <!-- <li class="mt-list-item done">
                                        <div class="list-icon-container">
                                            <a href="javascript:;">
                                                <i class="icon-check"></i>
                                            </a>
                                        </div>
                                        <div class="list-datetime">
                                            <a href="javascript:void(0)" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:void(0)" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                                        </div>
                                        <div class="list-item-content">
                                            <h3 class="uppercase">
                                                <a href="javascript:;">MCQ</a>
                                            </h3>
                                            <p class="text-overflow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A eaque, quasi itaque soluta voluptatum, totam minus accusamus, facere nesciunt illo ea velit odio accusantium beatae quae mollitia voluptatibus inventore aperiam.</p>
                                        </div>
                                    </li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .col-md-4 -->
            <div class="col-md-8">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Add Question </div>
                    </div>
                    <div class="portlet-body form">
                        <form action="#" id="form-username">
                            <div class="form-body">
                                <div class="row text-center chooseQuestionType">
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="clearfix">
                                                    <label class="mt-radio">
                                                        <input type="radio" class="js-question-type" name="questionType" id="optionQuestion" value="question" checked> Question
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="clearfix">
                                                    <label class="mt-radio">
                                                        <input type="radio" class="js-question-type" name="questionType" id="optionQuestionGroup" value="questionGroup"> Question Group
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="questionBlock" class="js-question-block">
                                    <div class="form-group">
                                        <label class="control-label">Question :</label>
                                        <textarea class="ckeditor form-control" name="inputQuestion" rows="6"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Answer :</label>
                                        <textarea class="ckeditor form-control" name="inputAnswer" rows="6"></textarea>
                                    </div>
                                    <!-- <div class="form-group marks">
                                        <label class="control-label">Enter Maximum Marks :</label>
                                        <input type="number" class="form-control" name="inputMarks" id="inputMarks" />
                                    </div> -->
                                </div>
                                <div class="js-question-block hide" id="questionGroupBlock">
                                    <div class="form-group">
                                        <label class="control-label">Questions to show on each page :</label>
                                        <div class="mt-radio-inline">
                                            <label class="mt-radio">
                                                <input type="radio" name="questionsOnpage" id="allquestion" class="js-sub-question-type" value="one" checked> In one page
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="questionsOnpage" id="onequestion" class="js-sub-question-type" value="multiple"> One question per page
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group" id="descriptionBlock">
                                        <label class="control-label">Description :</label>
                                        <textarea class="ckeditor form-control" name="inputQuestionDescription" rows="6"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Number of Questions to show :</label>
                                        <input type="number" class="form-control" name="inputNoOfQuestions" id="inputNoOfQuestions" />
                                    </div>
                                    <!-- <div class="form-group">
                                        <label class="control-label">Marks of each subquestions :</label>
                                        <input type="number" class="form-control" name="subquestionsMrks" id="subquestionsMrks" />
                                    </div> -->
                                </div>
                                <div class="row js-tagging">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Difficulty Level :</label>
                                            <select name="selectDifficulty" id="selectDifficulty" class="form-control">
                                                <option value="0">Select Difficulty</option>
                                                <option value="1">Very Easy</option>
                                                <option value="2">Easy</option>
                                                <option value="3">Fair</option>
                                                <option value="4">Difficult</option>
                                                <option value="5">Very Difficult</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Learning Objectives :</label>
                                            <div class="btn-block">
                                                <input type="text" id="magicsuggest" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions right">
                                <a href="javascript:;" class="btn dark" id="btnAddQuestion" data-question="" data-parent="">
                                    <i class="fa fa-check"></i> Submit</a>
                                <a href="javascript:;" class="btn btn-outline grey-salsa">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->