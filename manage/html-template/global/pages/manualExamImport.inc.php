<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
        <?php require_once('html-template/'.$userRole.'/includes/header-exam.php'); ?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Manual Exam Creation </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form portlet-form">
                        <!-- BEGIN FORM-->
                        <div class="horizontal-form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="well">
                                            <form class="frm-upload" action="<?php echo $sitepath; ?>api/files1.php" method="post">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <label class="control-label">Select Section <i class="tooltips fa fa-info-circle" data-original-title="Please select the section name under which the Manual Exam would be listed. You can make the Manual Exam as Independent, if you do not want it to be part of any section" data-placement="right"></i></label>
                                                            <select id="chapterSelect" class="form-control" data-placeholder="Choose a Section" tabindex="1">
                                                                <option value="">Select Section</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Select file to upload :</label>
                                                    <div class="btn-block">
                                                        <button type="button" class="btn btn-warning btn-upload">Upload Excel Sheet (CSV)</button>
                                                        <input type="file" name="manual-exam-upload" class="js-uploader hide">
                                                        <ul class="list-inline list-files js-file-path"></ul>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-center">
                                            <a href="<?php echo $sitepathManageSubjects.$subjectId; ?>/manual-exams/create" class="btn btn-primary btn-lg">Generate CSV</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="jsForm" class="oy-auto" data-analytics="" data-questions=""></div>
                                        <!-- <div id="jsFormQuick" class="oy-auto" data-analytics="quick" data-questions="">
                                            <div class="form-group">
                                                <label for="inputExamName">Exam Name:</label>
                                                <input type="text" id="inputExamName" class="form-control" placeholder="Exam Name">
                                            </div>
                                            <div class="form-group">
                                                <label for="inputTotalMarks">Total Marks for exam:</label>
                                                <input type="text" id="inputTotalMarks" class="form-control" placeholder="Exam Total Marks">
                                            </div>
                                            <table class="table table-bordered table-marks">
                                                <tbody>
                                                    <tr class="active">
                                                        <th>Student ID</th>
                                                        <th>Student Name</th>
                                                        <th>Marks</th>
                                                    </tr>
                                                    <tr data-sid="134">
                                                        <td>134</td>
                                                        <td>Jackie Christ</td>
                                                        <td><input type="text" class="form-control input-marks"></td>
                                                    </tr>
                                                    <tr data-sid="379">
                                                        <td>379</td>
                                                        <td>student one</td>
                                                        <td><input type="text" class="form-control input-marks"></td>
                                                    </tr>
                                                    <tr data-sid="380">
                                                        <td>380</td>
                                                        <td>student two</td>
                                                        <td><input type="text" class="form-control input-marks"></td>
                                                    </tr>
                                                    <tr data-sid="381">
                                                        <td>381</td>
                                                        <td>student three</td>
                                                        <td><input type="text" class="form-control input-marks"></td>
                                                    </tr>
                                                    <tr data-sid="382">
                                                        <td>382</td>
                                                        <td>Student four</td>
                                                        <td><input type="text" class="form-control input-marks"></td>
                                                    </tr>
                                                    <tr data-sid="384">
                                                        <td>384</td>
                                                        <td>Student Six</td>
                                                        <td><input type="text" class="form-control input-marks"></td>
                                                    </tr>
                                                    <tr data-sid="386">
                                                        <td>386</td>
                                                        <td>Student Eight</td>
                                                        <td><input type="text" class="form-control input-marks"></td>
                                                    </tr>
                                                    <tr data-sid="387">
                                                        <td>387</td>
                                                        <td>Student Nine</td>
                                                        <td><input type="text" class="form-control input-marks"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div>
                                                <button class="btn btn-primary" id="btnSubmit">SUBMIT</button>
                                            </div>
                                        </div>
                                        <div id="jsFormDeep" class="oy-auto" data-analytics="deep" data-questions="5">
                                            <div class="form-group">
                                                <label for="inputExamName">Exam Name:</label>
                                                <input type="text" id="inputExamName" class="form-control" placeholder="Exam Name">
                                            </div>
                                            <table class="table table-bordered table-total">
                                                <tbody>
                                                    <tr class="active">
                                                        <th colspan="5" class="text-center">Total Marks of questions</th>
                                                    </tr>
                                                    <tr class="active">
                                                        <th>Q1</th>
                                                        <th>Q2</th>
                                                        <th>Q3</th>
                                                        <th>Q4</th>
                                                        <th>Q5</th>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="text" class="form-control input-total" data-q="0"></td>
                                                        <td><input type="text" class="form-control input-total" data-q="1"></td>
                                                        <td><input type="text" class="form-control input-total" data-q="2"></td>
                                                        <td><input type="text" class="form-control input-total" data-q="3"></td>
                                                        <td><input type="text" class="form-control input-total" data-q="4"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="table table-bordered table-marks">
                                                <tbody>
                                                    <tr class="active">
                                                        <th rowspan="2">Student ID</th>
                                                        <th rowspan="2">Student Name</th>
                                                        <th colspan="5" class="text-center">Marks</th>
                                                    </tr>
                                                    <tr class="active">
                                                        <th>Q1</th>
                                                        <th>Q2</th>
                                                        <th>Q3</th>
                                                        <th>Q4</th>
                                                        <th>Q5</th>
                                                    </tr>
                                                    <tr data-sid="134">
                                                        <td>134</td>
                                                        <td>Jackie Christ</td>
                                                        <td><input type="text" class="form-control input-marks" data-q="0"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="1"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="2"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="3"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="4"></td>
                                                    </tr>
                                                    <tr data-sid="379">
                                                        <td>379</td>
                                                        <td>student one</td>
                                                        <td><input type="text" class="form-control input-marks" data-q="0"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="1"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="2"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="3"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="4"></td>
                                                    </tr>
                                                    <tr data-sid="380">
                                                        <td>380</td>
                                                        <td>student two</td>
                                                        <td><input type="text" class="form-control input-marks" data-q="0"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="1"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="2"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="3"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="4"></td>
                                                    </tr>
                                                    <tr data-sid="381">
                                                        <td>381</td>
                                                        <td>student three</td>
                                                        <td><input type="text" class="form-control input-marks" data-q="0"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="1"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="2"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="3"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="4"></td>
                                                    </tr>
                                                    <tr data-sid="382">
                                                        <td>382</td><td>Student four</td>
                                                        <td><input type="text" class="form-control input-marks" data-q="0"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="1"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="2"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="3"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="4"></td>
                                                    </tr>
                                                    <tr data-sid="384">
                                                        <td>384</td>
                                                        <td>Student Six</td>
                                                        <td><input type="text" class="form-control input-marks" data-q="0"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="1"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="2"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="3"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="4"></td>
                                                    </tr>
                                                    <tr data-sid="386">
                                                        <td>386</td>
                                                        <td>Student Eight</td>
                                                        <td><input type="text" class="form-control input-marks" data-q="0"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="1"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="2"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="3"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="4"></td>
                                                    </tr>
                                                    <tr data-sid="387">
                                                        <td>387</td>
                                                        <td>Student Nine</td>
                                                        <td><input type="text" class="form-control input-marks" data-q="0"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="1"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="2"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="3"></td>
                                                        <td><input type="text" class="form-control input-marks" data-q="4"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div>
                                                <button class="btn btn-primary" id="btnSubmit">SUBMIT</button>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="form-actions right">
                                <button type="button" class="btn default">Cancel</button>
                                <button type="submit" class="btn blue">
                                    <i class="fa fa-check"></i> Save</button>
                            </div> -->
                        </div>
                        <!-- END FORM-->
                    </div>
                </div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->