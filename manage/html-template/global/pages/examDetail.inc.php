<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
        <?php require_once('html-template/'.$userRole.'/includes/header-exam.php'); ?>
		<!-- END PAGE HEADER-->
		<div class="row">
            <div class="col-md-4">
                <div class="portlet light portlet-fit no-space margin-bottom-20" id="sections">
                    <div class="portlet-body no-space">
                        <div class="mt-element-list questions-sidebar-list">
                            <div class="mt-list-head list-simple ext-1 font-white bg-grey-dark">
                                <div class="list-head-title-container">
                                    <h3 class="list-title">Exam <div class="pull-right"> <small class="font-white"><span id="examTotalMarks"></span> Marks</small></div></h3>
                                </div>
                            </div>
                            <!-- <div class="mt-list-head list-simple ext-1 font-white bg-blue-chambray">
                                <div class="list-head-title-container">
                                    <h3 class="list-title">Exam Sections</h3>
                                </div>
                            </div> -->
                            <div class="mt-list-container list-simple ext-1 group">
                                <a class="list-toggle-container" data-toggle="collapse" href="#completed-simple" aria-expanded="false">
                                    <div class="list-toggle done uppercase"> Completed Exam Sections
                                        <span class="badge badge-default pull-right bg-white font-green bold js-completed-count">0</span>
                                    </div>
                                </a>
                                <div class="panel-collapse collapse in js-sections" id="completed-simple">
                                    <ul><?php /*
                                        <li class="mt-list-item done">
                                            <div class="list-icon-container">
                                                <i class="icon-check"></i>
                                            </div>
                                            <div class="list-datetime"> 20 Marks </div>
                                            <div class="list-item-content">
                                                <h3 class="uppercase">
                                                    <a href="javascript:;">Section 1: Section A</a>
                                                </h3>
                                            </div>
                                        </li>
                                        <li class="mt-list-item done">
                                            <div class="list-icon-container">
                                                <i class="icon-check"></i>
                                            </div>
                                            <div class="list-datetime"> 20 Marks </div>
                                            <div class="list-item-content">
                                                <h3 class="uppercase">
                                                    <a href="javascript:;">Section 1: Section C</a>
                                                </h3>
                                            </div>
                                        </li>*/ ?></ul>
                                </div>
                                <a class="list-toggle-container" data-toggle="collapse" href="#pending-simple" aria-expanded="false">
                                    <div class="list-toggle uppercase"> Pending Exam Sections
                                        <span class="badge badge-default pull-right bg-white font-dark bold js-pending-count">0</span>
                                    </div>
                                </a>
                                <div class="panel-collapse collapse in js-sections" id="pending-simple">
                                    <ul><?php /*
                                        <li class="mt-list-item">
                                            <div class="list-icon-container">
                                                <i class="icon-close"></i>
                                            </div>
                                            <div class="list-datetime"> 20 Marks </div>
                                            <div class="list-item-content">
                                                <h3 class="uppercase">
                                                    <a href="javascript:;">Section 1: Section B</a>
                                                </h3>
                                            </div>
                                        </li>
                                        <li class="mt-list-item">
                                            <div class="list-icon-container">
                                                <i class="icon-close"></i>
                                            </div>
                                            <div class="list-datetime"> 20 Marks </div>
                                            <div class="list-item-content">
                                                <h3 class="uppercase">
                                                    <a href="javascript:;">Section 1: Section D</a>
                                                </h3>
                                            </div>
                                        </li>
                                        <li class="mt-list-item">
                                            <div class="list-icon-container">
                                                <i class="icon-close"></i>
                                            </div>
                                            <div class="list-datetime"> 20 Marks </div>
                                            <div class="list-item-content">
                                                <h3 class="uppercase">
                                                    <a href="javascript:;">Section 1: Section E</a>
                                                </h3>
                                            </div>
                                        </li>*/ ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sectionQuestions"><?php /*
                    <div class="portlet light portlet-fit no-space">
                        <div class="portlet-body no-space">
                            <div class="mt-element-list">
                                <div class="mt-list-head list-default ext-1 green-jungle">
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="list-head-title-container">
                                                <h3 class="list-title uppercase sbold">Multiple Choice Question</h3>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="list-head-summary-container">
                                                <div class="list-pending">
                                                    <div class="list-count badge badge-default ">+3</div>
                                                    <div class="list-label">Correct Ans</div>
                                                </div>
                                                <div class="list-done">
                                                    <div class="list-count badge badge-default last">-2</div>
                                                    <div class="list-label">Wrong Ans</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-list-container list-default ext-1">
                                    <div class="mt-list-title uppercase">List of questions
                                        <span class="badge badge-default pull-right bg-hover-green-jungle">
                                            <a class="font-white" href="javascript:;">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </span>
                                    </div>
                                    <ul>
                                        <li class="mt-list-item done">
                                            <div class="list-icon-container">
                                                <a href="javascript:;">
                                                    <i class="icon-check"></i>
                                                </a>
                                            </div>
                                            <div class="list-datetime"> <a href="javascript:void(0)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> </div>
                                            <div class="list-item-content">
                                                <h3 class="uppercase">
                                                    <a href="javascript:;">MCQ</a>
                                                </h3>
                                                <p class="text-overflow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A eaque, quasi itaque soluta voluptatum, totam minus accusamus, facere nesciunt illo ea velit odio accusantium beatae quae mollitia voluptatibus inventore aperiam.</p>
                                            </div>
                                        </li>
                                        <li class="mt-list-item">
                                            <div class="list-icon-container">
                                                <a href="javascript:;">
                                                    <i class="icon-close"></i>
                                                </a>
                                            </div>
                                            <div class="list-datetime"> <a href="javascript:void(0)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> </div>
                                            <div class="list-item-content">
                                                <h3 class="uppercase">
                                                    <a href="javascript:;">MCQ</a>
                                                </h3>
                                                <p class="text-overflow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis eligendi quia non, doloremque aspernatur iusto est esse molestiae dolores neque illum consequuntur sit, quas officia provident sequi facere vitae optio!</p>
                                            </div>
                                        </li>
                                        <li class="mt-list-item">
                                            <div class="list-icon-container">
                                                <a href="javascript:;">
                                                    <i class="icon-close"></i>
                                                </a>
                                            </div>
                                            <div class="list-datetime"> <a href="javascript:void(0)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> </div>
                                            <div class="list-item-content">
                                                <h3 class="uppercase">
                                                    <a href="javascript:;">MCQ</a>
                                                </h3>
                                                <p class="text-overflow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus tempora nemo sit consequuntur facilis molestiae, eveniet quas animi, inventore blanditiis culpa, repellat qui, aliquid officiis veniam nesciunt voluptas similique soluta.</p>
                                            </div>
                                        </li>
                                        <li class="mt-list-item done">
                                            <div class="list-icon-container">
                                                <a href="javascript:;">
                                                    <i class="icon-check"></i>
                                                </a>
                                            </div>
                                            <div class="list-datetime"> <a href="javascript:void(0)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> </div>
                                            <div class="list-item-content">
                                                <h3 class="uppercase">
                                                    <a href="javascript:;">MCQ</a>
                                                </h3>
                                                <p class="text-overflow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut explicabo eligendi nulla magni qui aspernatur! Minus in eum, nam quidem totam voluptatem animi ipsam mollitia quo! Aliquid a culpa repellat.</p>
                                            </div>
                                        </li>
                                        <li class="mt-list-item">
                                            <div class="list-icon-container">
                                                <a href="javascript:;">
                                                    <i class="icon-close"></i>
                                                </a>
                                            </div>
                                            <div class="list-datetime"> <a href="javascript:void(0)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> </div>
                                            <div class="list-item-content">
                                                <h3 class="uppercase">
                                                    <a href="javascript:;">MCQ</a>
                                                </h3>
                                                <p class="text-overflow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem odio repellendus, hic expedita earum iste nemo ullam odit, cumque eligendi possimus. Explicabo illum nihil dolorum aliquid voluptatibus laborum assumenda minus.</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>*/ ?>
                </div>
            </div><!-- .col-md-4 -->
            <div class="col-md-8">
                <div class="portlet light bordered" id="sectionDetail">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-speech font-green-sharp"></i>
                            <span class="caption-subject bold uppercase" id="sectionName"> </span>
                            <span class="badge bg-blue-hoki">Draft</span>
                        </div>
                        <div class="actions">
                            <a data-toggle="modal" href="#modalAddEditQuestionsCategory" class="btn btn-circle btn-default btn-sm">
                                <i class="fa fa-plus"></i> Add </a>
                            <a href="javascript:;" class="btn btn-circle btn-default btn-sm delete-section">
                                <i class="fa fa-trash"></i> Delete </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-hover table-striped hide" id="sectionTable">
                            <thead>
                                <tr>
                                    <th>Category <button data-toggle="modal" href="#modalAddEditQuestionsCategory" type="button" class="btn blue-hoki btn-xs"><i class="fa fa-pencil"></i> Edit</button></th>
                                    <th class="text-center">Required</th>
                                    <th class="text-center">Entered</th>
                                    <th class="text-center">Marks</th>
                                    <th class="text-center">Remaining</th>
                                    <th>Refresh</th>
                                    <th>Import</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody><?php /*
                                <tr data-id="223">
                                    <td>Multiple Choice Question  +2.00 &amp; -1.00</td>
                                    <td class="required">5</td>
                                    <td class="entered">4</td>
                                    <td><strong>8</strong></td>
                                    <td>1</td>
                                    <td class="text"><button class="btn btn-danger btn-xs remove-category" title="" data-placement="top" type="button"><i class="fa fa-trash-o "></i></button></td>
                                </tr>
                                <tr class="lastCol">
                                    <td></td>
                                    <td></td>
                                    <td><strong>Total</strong></td>
                                    <td><strong>32</strong></td>
                                    <td></td>
                                    <td></td>
                                </tr>*/ ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="sectionNewQuestion" class="hide">
                    <input type="hidden" id="questionId" value="0">
                    <input type="hidden" id="hiddenQuestionType" value="0">
                    <div class="portlet box blue-hoki">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>Add Question </div>
                            <div class="actions">
                                <button type="button" class="btn btn-sm btn-default" id="newQuestion" disabled=""> New Question </button>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form action="#" id="formQuestion">
                                <div class="form-body">
                                    <input type="hidden" id="hiddenQuestionType" value="0">
                                    <div class="form-group">
                                        <label class="control-label">Category :</label>
                                        <select class="form-control" id="categorySelect"></select>
                                        <div class="help-block">
                                            <i class="fa fa-info-circle tooltips" data-placement="right" data-original-title="Multiple Choice Questions are the  most common type of questions used across exams<br>They only have one answer option as the correct answer" id="questionInfo"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Question :</label>
                                        <textarea class="ckeditor form-control" name="question" rows="6"></textarea>
                                    </div>
                                    <div class="form-group" id="options">
                                        <label class="control-label">Only one answer option is correct :</label>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <input type="radio" name="options">
                                                                    <span></span>
                                                                </span>
                                                                <input type="text" class="form-control option" id="option0" placeholder="Answer Option">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <input type="radio" name="options">
                                                                    <span></span>
                                                                </span>
                                                                <input type="text" class="form-control option" id="option1" placeholder="Answer Option">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row add-div"><?php /*
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <input type="radio" name="options">
                                                                    <span></span>
                                                                </span>
                                                                <input type="text" class="form-control option" placeholder="Answer Option">
                                                                <span class="input-group-btn">
                                                                    <button class="btn red" type="button"><i class="fa fa-trash"></i></button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <input type="radio" name="options">
                                                                    <span></span>
                                                                </span>
                                                                <input type="text" class="form-control option" placeholder="Answer Option">
                                                                <span class="input-group-btn">
                                                                    <button class="btn red" type="button"><i class="fa fa-trash"></i></button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>*/ ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <button class="btn btn-info add-option"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="trueFalse">
                                        <label class="control-label">Only one answer option is correct :</label>
                                        <div class="mt-radio-inline">
                                            <label class="mt-radio">
                                                <input type="radio" name="trueFalse" id="true"> True
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="trueFalse" id="false"> False
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group" id="ftb">
                                        <label class="control-label">Please write the correct answer :</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" placeholder="Please write the answer" class="form-control ftb" id="ftbOption0">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="match">
                                        <label class="control-label">Enter the data for match the following :</label>
                                        <p class="note note-info"><?php echo $global->terminology["student_single"]; ?> will see column B in jumbled order</p>
                                        <div class="row text-center">
                                            <div class="col-xs-5">
                                                <strong>A</strong>
                                            </div>
                                            <div class="col-xs-5">
                                                <strong>B</strong>
                                            </div>
                                        </div>
                                        <div class="custom-match">
                                            <div class="row match margin-bottom-10">
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control a" placeholder="Enter option A here" id="mtfOptionA0">
                                                </div>
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control b" placeholder="Correct answer for column A" id="mtfOptionB0">
                                                </div>
                                                <div class="col-md-2">
                                                    <button type="button" class="btn btn-info add-option"><i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="row match margin-bottom-10">
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control a" placeholder="Enter option A here" id="mtfOptionA1">
                                                </div>
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control b" placeholder="Correct answer for column A" id="mtfOptionB1">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="optionsMul">
                                        <label class="control-label">More than one answer option can be correct :</label>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <input type="checkbox">
                                                                    <span></span>
                                                                </span>
                                                                <input type="text" class="form-control optionMul" id="optionMul0" placeholder="Answer Option">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <input type="checkbox">
                                                                    <span></span>
                                                                </span>
                                                                <input type="text" class="form-control optionMul" id="optionMul1" placeholder="Answer Option">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row add-div"></div>
                                            </div>
                                            <div class="col-md-2">
                                                <button class="btn btn-info add-option"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="audio">
                                        <label class="control-label">Select audio clip to upload :</label>
                                        <div class="form-group">
                                            <button type="button" class="btn btn-warning btn-audio-upload">Upload Audio Clip (MP3)</button>
                                        </div>
                                        <div class="form-group">
                                            <div class="js-file-path" data-path=""></div>
                                        </div>
                                        <label class="control-label">Only one answer option is correct :</label>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <input type="radio">
                                                                    <span></span>
                                                                </span>
                                                                <input type="text" class="form-control optionAud" id="optionAud0" placeholder="Answer Option">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <input type="radio">
                                                                    <span></span>
                                                                </span>
                                                                <input type="text" class="form-control optionAud" id="optionAud1" placeholder="Answer Option">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row add-div"></div>
                                            </div>
                                            <div class="col-md-2">
                                                <button class="btn btn-info add-option"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="compro" class="form-group hide">
                                        <div class="well well-sm sectionNewQuestion">
                                            <input type="hidden" class="questionId" value="0">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="control-label">Category :</label>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select class="form-control questionTypeSelect">
                                                                <option value="0">Multiple Choice Question</option>
                                                                <option value="1">True or False</option>
                                                                <option value="2">Fill in the blanks</option>
                                                                <option value="6">Multiple Answer Questions</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="marks text-info"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Question :</label>
                                                    <textarea placeholder="Please enter the question text here. The passage is already written above." rows="2" class="form-control ckeditor question" id="childQuestion0"></textarea>
                                                </div>
                                                <div class="form-group options">
                                                    <label class="control-label">Only one answer option is correct :</label>
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">
                                                                                <input type="radio" name="childOptions0">
                                                                                <span></span>
                                                                            </span>
                                                                            <input type="text" class="form-control option" id="childOption00" placeholder="Answer Option">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">
                                                                                <input type="radio" name="childOptions0">
                                                                                <span></span>
                                                                            </span>
                                                                            <input type="text" class="form-control option" id="childOption01" placeholder="Answer Option">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row add-div"></div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button class="btn btn-info add-option"><i class="fa fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group trueFalse">
                                                    <label class="control-label">Only one answer option is correct :</label>
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" class="true" name="trueFalse0"> True
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" class="false" name="trueFalse0"> False
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group ftbs">
                                                    <label class="control-label">Please write the correct answer :</label>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input type="text" placeholder="Please write the answer" class="form-control ftb" id="childftb00">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group optionsMul">
                                                    <label class="control-label">More than one answer option can be correct :</label>
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">
                                                                                <input type="checkbox">
                                                                                <span></span>
                                                                            </span>
                                                                            <input type="text" class="form-control optionMul" id="childOptionMul00" placeholder="Answer Option">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">
                                                                                <input type="checkbox">
                                                                                <span></span>
                                                                            </span>
                                                                            <input type="text" class="form-control optionMul" id="childOptionMul01" placeholder="Answer Option">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row add-div"></div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button class="btn btn-info add-option"><i class="fa fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Explanation :</label>
                                                    <textarea class="form-control ckeditor desc" rows="2" placeholder="Please enter the explanation of the question" id="childDesc0"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions lastCol">
                                            <button type="button" class="btn green add-question"><i class="fa fa-plus-circle"> Question</i></button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Difficulty Level :</label>
                                                <select name="selectDifficulty" id="selectDifficulty" class="form-control">
                                                    <option value="0">Select Difficulty</option>
                                                    <option value="1">Very Easy</option>
                                                    <option value="2">Easy</option>
                                                    <option value="3">Fair</option>
                                                    <option value="4">Difficult</option>
                                                    <option value="5">Very Difficult</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Learning Objectives :</label>
                                                <div class="btn-block">
                                                    <div id="magicsuggest"></div>
                                                    <!-- <input type="text" value="Amsterdam,Washington,Sydney,Beijing,Cairo" data-role="tagsinput" id="tags-input"> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group desc-block">
                                        <label class="control-label">Explanation :</label>
                                        <textarea class="ckeditor form-control" name="desc" id="desc" rows="6"></textarea>
                                    </div>
                                </div>
                                <div class="form-actions right">
                                    <a href="javascript:;" class="btn dark save-button">
                                        <i class="fa fa-check"></i> Submit</a>
                                    <a href="javascript:;" class="btn btn-outline grey-salsa cancel-button">Cancel</a>
                                </div>
                            </form>
                            <form id="frmAudioUpload" class="frm-upload" action="<?php echo $sitepath; ?>api/files1.php" method="post">
                                <input type="file" name="audio-upload" class="js-uploader hide">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->