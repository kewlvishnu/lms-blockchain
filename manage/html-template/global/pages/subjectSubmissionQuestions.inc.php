<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
        <?php require_once('html-template/'.$userRole.'/includes/header-questionbank.php'); ?>
		<!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-4">
                <div id="sectionQuestions"></div>
            </div><!-- .col-md-4 -->
            <div class="col-md-8">
                <div id="sectionNewQuestion" class="">
                    <input type="hidden" id="questionId" value="0">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>Add Question </div>
                            <div class="actions">
                                <button type="button" class="btn btn-sm btn-default" id="newQuestion"> New Question </button>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form action="#" id="formQuestion">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label">Question :</label>
                                        <textarea class="ckeditor form-control" name="question" id="question" rows="6"></textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Difficulty Level :</label>
                                                <select name="selectDifficulty" id="selectDifficulty" class="form-control">
                                                    <option value="0">Select Difficulty</option>
                                                    <option value="1">Very Easy</option>
                                                    <option value="2">Easy</option>
                                                    <option value="3">Fair</option>
                                                    <option value="4">Difficult</option>
                                                    <option value="5">Very Difficult</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Learning Objectives :</label>
                                                <div class="btn-block">
                                                    <div id="magicsuggest"></div>
                                                    <!-- <input type="text" value="Amsterdam,Washington,Sydney,Beijing,Cairo" data-role="tagsinput" id="tags-input"> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group desc-block">
                                        <label class="control-label">Explanation :</label>
                                        <textarea class="ckeditor form-control" name="desc" id="desc" rows="6"></textarea>
                                    </div>
                                </div>
                                <div class="form-actions right">
                                    <a href="javascript:;" class="btn dark save-button">
                                        <i class="fa fa-check"></i> Submit</a>
                                    <a href="javascript:;" class="btn btn-outline grey-salsa cancel-button">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->