<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<?php
			require_once('html-template/'.$userRole.'/includes/header-subject.php');
		?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light ">
					<div class="portlet-title tabbable-line">
						<div class="caption caption-md">
							<i class="icon-globe theme-font hide"></i>
							<span class="caption-subject font-blue-madison bold uppercase">Grading (Overall)</span>
						</div>
						<div class="tools">
							<button class="btn green" id="btnSaveGrading"><i class="fa fa-save"></i> Save</button>
							<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>/rewards" class="btn blue" style="height:auto"><i class="fa fa-gift"></i> Rewards</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover order-column">
								<thead>
									<tr>
										<th width="30%"> Title </th>
										<th width="10%"> Type </th>
										<th width="11%"> Section </th>
										<th class="text-center" width="12%"> Start Date </th>
										<th class="text-center" width="12%"> End Date </th>
										<th class="text-center" width="5%"> Attempts </th>
										<th width="10%"> Grade Type </th>
										<th width="10%"> Avg of last __ attempts </th>
										<th width="10%"> Weight </th>
									</tr>
								</thead>
								<tbody id="listExams"></tbody>
							</table>
						</div>
						<div class="form-group">
							<label for="">Notes :</label>
							<textarea name="notes" id="notes" cols="30" rows="10" class="form-control ckeditor"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->