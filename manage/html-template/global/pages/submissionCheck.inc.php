<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
        <h1 class="page-title"> Submission Attempts to Check </h1>
        <?php require_once('html-template/'.$userRole.'/includes/header-exam.php'); ?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
                <div class="panel-group" id="studentSubmissions" role="tablist" aria-multiselectable="true">
                    <!-- <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="heading379">
                            <h4 class="panel-title">
                                <a class="" role="button" data-toggle="collapse" data-parent="#" href="#collapse379" aria-expanded="true" aria-controls="collapse379">Student one</a>
                            </h4>
                        </div>
                        <div id="collapse379" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading379">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>Attempt</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Checked</th>
                                    </tr>
                                    <tr>
                                        <td><a href="<?php echo $sitepathInstituteSubjectiveExams; ?>1/check/1">Attempt 1</a></td>
                                        <td>21 Mar 2016 1:48:2</td>
                                        <td>21 Mar 2016 1:55:27</td>
                                        <td><span class="badge">Yes</span></td>
                                    </tr>
                                    <tr>
                                        <td><a href="<?php echo $sitepathInstituteSubjectiveExams; ?>1/check/1">Attempt 2</a></td>
                                        <td>13 Apr 2016 11:45:57</td>
                                        <td>13 Apr 2016 12:46:5</td>
                                        <td><span class="badge">Yes</span></td>
                                    </tr>
                                    <tr>
                                        <td><a href="<?php echo $sitepathInstituteSubjectiveExams; ?>1/check/1">Attempt 3</a></td>
                                        <td>9 May 2016 20:7:35</td>
                                        <td>9 May 2016 20:13:0</td>
                                        <td><span class="badge">No</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="heading380">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#" href="#collapse380" aria-expanded="false" aria-controls="collapse380">Student two</a>
                            </h4>
                        </div>
                        <div id="collapse380" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading380">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>Attempt</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Checked</th>
                                    </tr>
                                    <tr>
                                        <td><a href="<?php echo $sitepathInstituteSubjectiveExams; ?>1/check/1">Attempt 1</a></td>
                                        <td>10 May 2016 1:25:21</td>
                                        <td>10 May 2016 1:30:20</td>
                                        <td><span class="badge">Yes</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="heading383">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#" href="#collapse383" aria-expanded="false" aria-controls="collapse383">Student five</a>
                            </h4>
                        </div>
                        <div id="collapse383" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading383">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>Attempt</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Checked</th>
                                    </tr>
                                    <tr>
                                        <td><a href="<?php echo $sitepathInstituteSubjectiveExams; ?>1/check/1">Attempt 1</a></td>
                                        <td>10 May 2016 1:32:34</td>
                                        <td>10 May 2016 1:39:31</td>
                                        <td><span class="badge">Yes</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="heading385">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#" href="#collapse385" aria-expanded="false" aria-controls="collapse385">Student Seven</a>
                            </h4>
                        </div>
                        <div id="collapse385" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading385">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>Attempt</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Checked</th>
                                    </tr>
                                    <tr>
                                        <td><a href="<?php echo $sitepathInstituteSubjectiveExams; ?>1/check/1">Attempt 1</a></td>
                                        <td>14 Apr 2016 10:36:35</td>
                                        <td>14 Apr 2016 10:43:34</td>
                                        <td><span class="badge">Yes</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> -->
                </div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->