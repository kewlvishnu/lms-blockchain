<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h1 class="page-title"> Portfolios
            <small>manage portfolios</small>
        </h1>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo $sitepathManage; ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Portfolios</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Manage Portfolios</span>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_1" data-toggle="tab">Portfolios</a>
                            </li>
                            <li>
                                <a href="#tab_1_2" data-toggle="tab">Add New Portfolio</a>
                            </li>
                        </ul>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <div class="tab-pane active" id="tab_1_1">
                                <h3>Portfolio <?php echo $global->terminology["course_plural"]; ?> <small>select <?php echo strtolower($global->terminology["course_plural"]); ?> to be displayed in the portfolios</small></h3>
                                <div class="row select2-dip js-portfolios">
                                    <!-- <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">rknec-test.integro.io  [Status : Published]</h3>
                                            </div>
                                            <div class="panel-body">
                                                <form class="form-horizontal form-row-seperated">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <div class="col-md-12 text-center">
                                                                <select multiple="multiple" class="multi-select" id="my_multi_select1" name="my_multi_select1[]">
                                                                    <option value="1">Naval Engineering - 2016</option>
                                                                    <option value="2" selected="">Naval Acad -2017</option>
                                                                    <option value="3" selected="">Engineering Demo</option>
                                                                    <option value="4">Demostration</option>
                                                                    <option value="5">Demonstartion</option>
                                                                    <option value="6" selected="">Loads of Crap</option>
                                                                    <option value="7">Its all crap</option>
                                                                    <option value="8">Testing crap</option>
                                                                    <option value="9">Course Pack 1</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12 text-center">
                                                                <button type="submit" class="btn green">
                                                                    <i class="fa fa-check"></i> Submit</button>
                                                                <button type="button" class="btn grey-salsa btn-outline">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                END FORM
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <!-- END PERSONAL INFO TAB -->
                            <!-- CHANGE AVATAR TAB -->
                            <div class="tab-pane" id="tab_1_2">
                                <div class="row">
                                    <div class="col-md-3">
                                        <p> Please select an image for your portfolio. </p>
                                        <form action="#" role="form">
                                            <!-- <div class="form-group">
                                                <input type="file" />
                                                <img class="crop" style="display:none" />
                                                <button type="submit" style="display:none">Upload</button>
                                            </div> -->
                                            <div class="form-group">
                                                <div class="profile-pic">
                                                    <div class="thumbnail">
                                                        <img id="userProfilePic" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span> Select image </span>
                                                            <input id="fileProfilePic" type="file" name="..."> </span>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="portfolioPicPath" name="portfolioPicPath" value="">
                                            </div>
                                            <div class="margin-top-10 hide js-upload">
                                                <a href="javascript:;" class="btn green" id="btnUpload">Upload</a>
                                                <a href="javascript:;" class="btn default" id="btnUploadCancel"> Cancel </a>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-9">
                                        <form role="form" action="#" id="formNewPortfolio">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your profile is successfully updated! </div>
                                            <div class="form-group">
                                                <label class="control-label">Portfolio Name <span class="required" aria-required="true"> * </span></label>
                                                <input type="text" id="inputNewPortfolioName" name="inputNewPortfolioName" placeholder="e.g. Harvard University" class="form-control" /> </div>
                                            <div class="form-group">
                                                <label class="control-label">Portfolio Description <span class="required" aria-required="true"> * </span></label>
                                                <textarea name="inputNewPortfolioDesc" id="inputNewPortfolioDesc" cols="30" rows="10" class="form-control" placeholder="e.g. We want portfolio for our Mechanical Department of the same institute."></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Subdomain <span class="required" aria-required="true"> * </span></label>
                                                <input type="text" id="inputNewSubdomain" name="inputNewSubdomain" class="form-control input-inline input-medium" placeholder="e.g. harvard">
                                                <span class="help-inline"> .integro.io </span>
                                            </div>
                                            <div class="margin-top-10">
                                                <button type="submit" id="btnNewPortfolio" name="btnNewPortfolio" class="btn green">Submit</button>
                                                <a href="javascript:;" class="btn default"> Cancel </a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- END CHANGE AVATAR TAB -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->