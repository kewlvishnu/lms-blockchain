<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<?php
			require_once('html-template/'.$userRole.'/includes/header-subject.php');
		?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light ">
					<div class="portlet-title tabbable-line">
						<div class="caption caption-md">
							<i class="icon-globe theme-font hide"></i>
							<span class="caption-subject font-blue-madison bold uppercase">Rewards</span>
						</div>
						<div class="tools">
							<?php /*<button class="btn green" id="btnSaveGrading"><i class="fa fa-save"></i> Save</button>*/ ?>
							<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>/grading" class="btn blue" style="height:auto">Grading</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover order-column">
								<thead>
									<tr>
										<th width="25%"> Title </th>
										<th width="10%"> Type </th>
										<th width="10%"> Section </th>
										<th>Percentage Block 1</th>
										<th>Percentage Block 2</th>
										<th>Percentage Block 3</th>
										<th>Percentage Block 4</th>
										<th>Percentage Block 5</th>
									</tr>
								</thead>
								<tbody id="listExams"></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->