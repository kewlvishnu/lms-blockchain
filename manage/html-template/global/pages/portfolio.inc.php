<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h1 class="page-title"> Manage Portfolio
            <small>manage this portfolio</small>
        </h1>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo $sitepathManage; ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Manage Portfolio</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <?php
                    require_once('html-template/'.$userRole.'/includes/sidebar-portfolio.php');
                    switch ($subPage) {
                        case 'portfolioEdit':
                            require_once('html-template/'.$userRole.'/pages/portfolio/portfolioEdit.html.php');
                            break;
                        case 'portfolioMenu':
                            require_once('html-template/'.$userRole.'/pages/portfolio/portfolioMenu.html.php');
                            break;
                        case 'portfolioPages':
                            require_once('html-template/'.$userRole.'/pages/portfolio/portfolioPages.html.php');
                            break;
                        
                        default:
                            echo "There was some error!";
                            break;
                    }
                ?>
                
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->