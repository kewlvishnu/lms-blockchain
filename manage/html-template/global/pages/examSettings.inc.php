<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <?php require_once('html-template/'.$userRole.'/includes/header-exam.php'); ?>
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Exam Settings </div>
                        <div class="actions">
                            <button class="btn btn-default btn-xs" type="button" id="examStatus">Draft</button>
                            <button class="btn btn-default btn-xs" id="examDelete"><i class="fa fa-trash-o"></i></button>
                            <!-- <a href="javascript:;" class="btn btn-default btn-sm">
                                <i class="fa fa-pencil"></i> Edit </a>
                            <a href="javascript:;" class="btn btn-default btn-sm">
                                <i class="fa fa-plus"></i> Add </a> -->
                        </div>
                    </div>
                    <div class="portlet-body form portlet-form">
                        <!-- BEGIN FORM-->
                        <form id="formCreateExam" class="horizontal-form">
                            <div class="form-body">
                                <h3 class="form-section">Basic Information</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Title of the test</label>
                                            <input type="text" id="titleName" class="form-control" placeholder="Title">
                                            <span class="help-block"> Enter title of the test. </span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Section <i class="tooltips fa fa-info-circle" data-original-title="Please select the section name under which the Assignment/ Exam would be listed. You can make the Assignment/ Exam as Independent, if you do not want it to be part of any section" data-placement="right"></i></label>
                                            <select id="chapterSelect" class="form-control" data-placeholder="Choose a Section" tabindex="1">
                                                <option value="0">Select Section</option>
                                            </select>
                                            <span class="help-block"> Select section. </span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <!-- <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Test type</label>
                                            <input type="text" value="Exam" class="form-control" readonly="">
                                        </div>
                                    </div> -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Select test type</label>
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionExamType" id="assignmentCat" class="js-exam-type" value="assignment" disabled> Assignment
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionExamType" id="examCat" class="js-exam-type" value="exam" disabled> Exam
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">No of attempts</label>
                                            <input type="text" id="noOfAttempts" class="form-control" placeholder="0">
                                            <span class="help-block"> Enter no of attempts a <?php echo $global->terminology["student_single"]; ?> is allowed. </span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <h3 class="form-section">Exam Settings</h3>
                                <div class="js-exam-settings">
                                    <div class="row">
                                        <!--/span-->
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div id="sections">
                                                    <label class="control-label">Exam Sections</label>
                                                    <?php /*
                                                    <!-- <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" value="Section 1" readonly>
                                                            <span class="input-group-btn">
                                                                <button class="btn green" type="button"><i class="fa fa-edit"></i></button>
                                                                <button class="btn red" type="button"><i class="fa fa-trash"></i></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" value="Section 1" readonly>
                                                            <span class="input-group-btn">
                                                                <button class="btn green" type="button"><i class="fa fa-edit"></i></button>
                                                                <button class="btn red" type="button"><i class="fa fa-trash"></i></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" value="Section 1" readonly>
                                                            <span class="input-group-btn">
                                                                <button class="btn green" type="button"><i class="fa fa-edit"></i></button>
                                                                <button class="btn red" type="button"><i class="fa fa-trash"></i></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" value="Section 1" readonly>
                                                            <span class="input-group-btn">
                                                                <button class="btn green" type="button"><i class="fa fa-edit"></i></button>
                                                                <button class="btn red" type="button"><i class="fa fa-trash"></i></button>
                                                            </span>
                                                        </div>
                                                    </div> --> */ ?>
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group" id="tempSection">
                                                        <input type="text" class="form-control">
                                                        <span class="input-group-btn">
                                                            <button class="btn blue" type="button" id="addSection"><i class="fa fa-plus"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Start date of test</label>
                                                <div id="startDate" class="input-group date">
                                                    <input type="text" size="16" readonly class="form-control">
                                                    <span class="input-group-btn">
                                                        <button class="btn default date-set" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">End date of test</label>
                                                <div id="endDate" class="input-group date">
                                                    <input type="text" size="16" readonly class="form-control">
                                                    <span class="input-group-btn">
                                                        <button class="btn default date-set" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row" id="result-show">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label"><?php echo $global->terminology["student_plural"]; ?> can see result after</label>
                                                <div class="mt-radio-inline">
                                                    <label class="mt-radio">
                                                        <input type="radio" name="optionResultAfter" id="immediately" value="option1" checked> Immediately after finishing the exam
                                                        <span></span>
                                                    </label>
                                                    <label class="mt-radio">
                                                        <input type="radio" name="optionResultAfter" id="after-examend" value="option2"> After the exam end date
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Time by which <?php echo $global->terminology["student_single"]; ?> can see result</label>
                                                <div class="mt-radio-inline">
                                                    <label class="mt-radio">
                                                        <input type="radio" name="optionResultTime" id="course-end" class="js-set-time-options" value="0" checked> Till <?php echo strtolower($global->terminology["course_single"]); ?> end date
                                                        <span></span>
                                                    </label>
                                                    <label class="mt-radio">
                                                        <input type="radio" name="optionResultTime" id="custom-time" class="js-set-time-options" value="1"> Set time
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group js-set-time hide">
                                                <div id="settime" class="input-group date form_datetime">
                                                    <input type="text" size="16" readonly class="form-control">
                                                    <span class="input-group-btn">
                                                        <button class="btn default date-set" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="js-section-timings hide">
                                        <h4>Section timings</h4>
                                        <div class="row">
                                            <div class="col-md-6" id="switchSections">
                                                <div class="form-group">
                                                    <label class="control-label">Can <?php echo $global->terminology["student_plural"]; ?> Switch Between Sections?</label>
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" name="optionSwitchSections" id="switchYes" value="option1" checked> Yes
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="optionSwitchSections" id="switchNo" value="option2"> No
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6" id="totalTime">
                                                <div class="form-group">
                                                    <label class="control-label">Total time of test</label>
                                                    <div class="clearfix">
                                                        <div class="dib">
                                                            <div class="input-group input-small">
                                                                <input type="number" id="totalTimeHour" class="form-control" placeholder="Hours" value="0" aria-describedby="sizing-addon1">
                                                                <span class="input-group-addon">Hours</span>
                                                            </div>
                                                        </div>
                                                        <div class="dib">
                                                            <div class="input-group input-small">
                                                                <input type="number" id="totalTimeMinute" class="form-control" placeholder="Minutes" value="0" aria-describedby="sizing-addon1">
                                                                <span class="input-group-addon">Minutes</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php /*
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Total Time for Section 1</label>
                                                    <div class="clearfix">
                                                        <div class="dib">
                                                            <div class="input-group input-small">
                                                                <input type="number" class="form-control time hour" placeholder="Hours" aria-describedby="sizing-addon1">
                                                                <span class="input-group-addon" id="sizing-addon1">Hours</span>
                                                            </div>
                                                        </div>
                                                        <div class="dib">
                                                            <div class="input-group input-small">
                                                                <input type="number" class="form-control time minute" placeholder="Minutes" aria-describedby="sizing-addon1">
                                                                <span class="input-group-addon" id="sizing-addon1">Minutes</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Total Time for Section 2</label>
                                                    <div class="clearfix">
                                                        <div class="dib">
                                                            <div class="input-group input-small">
                                                                <input type="number" class="form-control time hour" placeholder="Hours" aria-describedby="sizing-addon1">
                                                                <span class="input-group-addon" id="sizing-addon1">Hours</span>
                                                            </div>
                                                        </div>
                                                        <div class="dib">
                                                            <div class="input-group input-small">
                                                                <input type="number" class="form-control time minute" placeholder="Minutes" aria-describedby="sizing-addon1">
                                                                <span class="input-group-addon" id="sizing-addon1">Minutes</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span--> */ ?>
                                        </div>
                                        <div id="sectionTimes" class="hide">
                                            <div class="row"></div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Gap between Sections</label>
                                                        <div class="clearfix">
                                                            <div class="dib">
                                                                <div class="input-group input-small">
                                                                    <input type="number" class="form-control" value="0" placeholder="Minutes" aria-describedby="sizing-addon1" id="gapTime">
                                                                    <span class="input-group-addon" id="sizing-addon1">Minutes</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Can <?php echo $global->terminology["student_plural"]; ?> end test before time?</label>
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" name="optionEndTest" id="endYes" value="option1" checked> Yes
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="optionEndTest" id="endNo" value="option2"> No
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">If power goes off/on browser closes (answers are not lost)</label>
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" name="optionPower" id="timeLost" value="option1" checked> Timer continues
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="optionPower" id="noTimeLost" value="option2"> Exam Ends
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="js-sections-manage hide">
                                        <h4>Sections management</h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Section ordering</label>
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" name="optionSectionOrdering" id="orderStart" value="option1" checked> Fixed
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="optionSectionOrdering" id="orderRandom" value="option2"> Random
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-12">
                                                <div class="dd" id="nestable_list_1">
                                                    <ol class="dd-list hide" id="sortOrder"><?php /*
                                                        <!-- <li class="dd-item" data-id="1">
                                                            <div class="dd-handle"> Item 1 </div>
                                                        </li>
                                                        <li class="dd-item" data-id="2">
                                                            <div class="dd-handle"> Item 2 </div>
                                                        </li>
                                                        <li class="dd-item" data-id="3">
                                                            <div class="dd-handle"> Item 3 </div>
                                                        </li>
                                                        <li class="dd-item" data-id="4">
                                                            <div class="dd-handle"> Item 4 </div>
                                                        </li> --> */ ?>
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                                <input type="hidden" id="courseStartDate">
                                <input type="hidden" id="courseEndDate">
                            </div>
                            <div class="form-actions right">
                                <a href="<?php echo $sitepathManageSubjects.$subjectId; ?>" class="btn default">Cancel</a>
                                <button type="submit" class="btn blue save-button">
                                    <i class="fa fa-check"></i> Save</button>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->