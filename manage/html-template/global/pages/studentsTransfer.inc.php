<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<h1 class="page-title"> Transfer <?php echo $global->terminology["student_plural"]; ?> </h1>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo $sitepathManage; ?>">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<i class="icon-list"></i>
					<a href="<?php echo $sitepathManageStudents; ?>"><?php echo $global->terminology["student_plural"]; ?></a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<span>Transfer <?php echo $global->terminology["student_plural"]; ?></span>
				</li>
			</ul>
			<div class="page-toolbar">
				<div class="btn-group pull-right">
					<button type="button" data-toggle="modal" data-target="#modalInviteStudentsToCourse" class="btn btn-fit-height blue"> Invite </button>
					<a href="<?php echo $sitepathManageStudents; ?>invites" class="btn btn-fit-height green"> <?php echo $global->terminology["student_single"]; ?> Invites </a>
					<a href="<?php echo $sitepathManageStudents; ?>" class="btn btn-fit-height green-jungle"> <?php echo $global->terminology["student_plural"]; ?> List </a>
				</div>
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light ">
					<div class="portlet-title tabbable-line">
						<div class="caption caption-md">
							<i class="icon-globe theme-font hide"></i>
							<span class="caption-subject font-blue-madison bold uppercase">Transfer <?php echo $global->terminology["student_plural"]; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
							<thead>
								<tr>
									<th> # </th>
									<th> Username </th>
									<th> Password </th>
									<th> <?php echo $global->terminology["course_plural"]; ?> </th>
									<th> Actions </th>
								</tr>
							</thead>
							<tbody>
								<tr class="odd gradeX">
									<td colspan="5">No <?php echo $global->terminology["student_single"]; ?> has joined yet</td>
								</tr>
								<!-- <tr class="odd gradeX">
									<td>
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="checkboxes" value="1" />
											<span></span>
										</label>
									</td>
									<td>420</td>
									<td>student1</td>
									<td><div class="text-success">REGISTERED</div></td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>9876543210</td>
									<td>Naval Engineering 2016, Bio Engineering 2017</td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-comment"></i> Chat</a>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Remove</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="checkboxes" value="1" />
											<span></span>
										</label>
									</td>
									<td>420</td>
									<td>student1</td>
									<td><div class="text-success">REGISTERED</div></td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>9876543210</td>
									<td>Naval Engineering 2016, Bio Engineering 2017</td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-comment"></i> Chat</a>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Remove</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="checkboxes" value="1" />
											<span></span>
										</label>
									</td>
									<td>420</td>
									<td>student1</td>
									<td><div class="text-success">REGISTERED</div></td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>9876543210</td>
									<td>Naval Engineering 2016, Bio Engineering 2017</td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-comment"></i> Chat</a>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Remove</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="checkboxes" value="1" />
											<span></span>
										</label>
									</td>
									<td>420</td>
									<td>student1</td>
									<td><div class="text-success">REGISTERED</div></td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>9876543210</td>
									<td>Naval Engineering 2016, Bio Engineering 2017</td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-comment"></i> Chat</a>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Remove</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="checkboxes" value="1" />
											<span></span>
										</label>
									</td>
									<td>420</td>
									<td>student1</td>
									<td><div class="text-success">REGISTERED</div></td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>9876543210</td>
									<td>Naval Engineering 2016, Bio Engineering 2017</td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-comment"></i> Chat</a>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Remove</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="checkboxes" value="1" />
											<span></span>
										</label>
									</td>
									<td>420</td>
									<td>student1</td>
									<td><div class="text-success">REGISTERED</div></td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>9876543210</td>
									<td>Naval Engineering 2016, Bio Engineering 2017</td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-comment"></i> Chat</a>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Remove</a>
									</td>
								</tr> -->
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->