<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
        <?php require_once('html-template/'.$userRole.'/includes/header-exam.php'); ?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Subjective Exam Settings </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form portlet-form">
                        <!-- BEGIN FORM-->
                        <form id="formCreateExam" class="horizontal-form">
                            <div class="form-body">
                                <h3 class="form-section">Basic Information</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Title of the test</label>
                                            <input type="text" id="titleName" class="form-control" placeholder="Title">
                                            <span class="help-block"> Enter title of the test. </span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Section <i class="tooltips fa fa-info-circle" data-original-title="Please select the section name under which the Subjective Exam would be listed. You can make the Subjective Exam as Independent, if you do not want it to be part of any section" data-placement="right"></i></label>
                                            <select id="chapterSelect" class="form-control" data-placeholder="Choose a Section" tabindex="1">
                                                <option value="0">Select Section</option>
                                            </select>
                                            <span class="help-block"> Select section. </span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Shuffle questions?</label>
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionExamType" id="switchyes" value="option1" checked> Yes
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionExamType" id="switchno" value="option2"> No
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">No of attempts</label>
                                            <input type="text" id="noOfAttempts" class="form-control" placeholder="0">
                                            <span class="help-block"> Enter no of attempts a <?php echo $global->terminology["student_single"]; ?> is allowed. </span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <h3 class="form-section">Exam Settings</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Start date of test</label>
                                            <div id="startDate" class="input-group date">
                                                <input type="text" size="16" readonly class="form-control">
                                                <span class="input-group-btn">
                                                    <button class="btn default date-set" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">End date of test (optional)</label>
                                            <div id="endDate" class="input-group date">
                                                <input type="text" size="16" readonly class="form-control">
                                                <span class="input-group-btn">
                                                    <button class="btn default date-set" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Total time of test</label>
                                            <div class="clearfix">
                                                <div class="dib">
                                                    <div class="input-group input-small">
                                                        <input type="number" class="form-control" id="hrsTest" value="0" placeholder="Hours" aria-describedby="sizing-addon1">
                                                        <span class="input-group-addon" id="sizing-addon1">Hours</span>
                                                    </div>
                                                </div>
                                                <div class="dib">
                                                    <div class="input-group input-small">
                                                        <input type="number" class="form-control" id="mnsTest" value="0" placeholder="Minutes" aria-describedby="sizing-addon1">
                                                        <span class="input-group-addon" id="sizing-addon1">Minutes</span>
                                                    </div>
                                                </div>
		                                    </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Submission of answers</label>
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios" id="optionText" class="js-answer-type" value="option1"> Text Only
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios" id="optionFile" class="js-answer-type" value="option2"> File Upload Only
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios" id="optionBoth" class="js-answer-type" value="option3"> Text and File Upload both
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                                <input type="hidden" id="courseStartDate">
                                <input type="hidden" id="courseEndDate">
                            </div>
                            <div class="form-actions right">
                                <a href="<?php echo $sitepathManageSubjects.$subjectId; ?>" class="btn default">Cancel</a>
                                <button type="submit" class="btn blue">
                                    <i class="fa fa-save"></i> Save</button>
                                <!-- <button type="button" class="btn green makelive">
                                    <i class="fa fa-check"></i> Go Live</button> -->
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->