<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
        <?php require_once('html-template/'.$userRole.'/includes/header-exam.php'); ?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<div class="portlet box green">
					<div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-white"></i>
                            <span class="caption-subject bold font-white uppercase">Percentage distribution of all <?php echo $global->terminology["student_plural"]; ?></span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
					<div class="portlet-body">
						<div id="percentDistGraph" class="min-height-500"></div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class=" icon-layers font-white"></i>
							<span class="caption-subject bold font-white uppercase"><?php echo $global->terminology["student_plural"]; ?> Attempted vs Unattempted</span>
						</div>
					</div>
					<div class="portlet-body">
						<div id="attemptsPieGraph" class="min-height-500"></div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class=" icon-layers font-white"></i>
							<span class="caption-subject bold font-white uppercase">Scores</span>
						</div>
					</div>
					<div class="portlet-body">
						<div id="scoresBarGraph" class="min-height-500"></div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class=" icon-layers font-white"></i>
							<span class="caption-subject bold font-white uppercase">Percent</span>
						</div>
					</div>
					<div class="portlet-body">
						<div id="percentBarGraph" class="min-height-500"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
                            <i class=" icon-layers font-white"></i>
                            <span class="caption-subject bold font-white uppercase">Exam <?php echo $global->terminology["student_plural"]; ?></span>
                        </div>
					</div>
					<div class="portlet-body no-space" id="exam">
						<table class="table table-hover table-bordered no-margin" id="studentList">
							<thead>
								<tr>
									<th class="text-center" width="10%"><?php echo $global->terminology["student_single"]; ?> Id</th>
									<th>Name</th>
									<th class="text-center" width="12%">Email</th>
									<th class="text-center" width="12%">Attempts</th>
									<th class="text-center" width="12%">Last Attempt Score</th>
									<th class="text-center" width="12%">Highest Score</th>
									<th class="text-center" width="12%">Average Score</th>
								</tr>
							</thead>
							<tbody>
								<?php /*<tr class="table-row" data-student="0" data-max="100">
									<td class="text-center">384</td>
									<td><a href="<?php echo $sitepathManageExams; ?>1/result/student/1">Student Six</a></td>
									<td class="text-center"><a href="mailto:abc@gmail.com">abc@gmail.com</a></td>
									<td class="text-center">5</td>
									<td class="text-center">65</td>
									<td class="text-center">78</td>
									<td class="text-center">54.51</td>
								</tr>
								<tr class="table-row" data-student="1" data-max="100">
									<td class="text-center">387</td>
									<td><a href="<?php echo $sitepathManageExams; ?>1/result/student/1">Student Nine</a></td>
									<td class="text-center"><a href="mailto:abc@gmail.com">abc@gmail.com</a></td>
									<td class="text-center">5</td>
									<td class="text-center">57</td>
									<td class="text-center">85</td>
									<td class="text-center">54.51</td>
								</tr>
								<tr class="table-row" data-student="2" data-max="100">
									<td class="text-center">380</td>
									<td><a href="<?php echo $sitepathManageExams; ?>1/result/student/1">student two</a></td>
									<td class="text-center"><a href="mailto:abc@gmail.com">abc@gmail.com</a></td>
									<td class="text-center">4</td>
									<td class="text-center">92</td>
									<td class="text-center">71</td>
									<td class="text-center">54.51</td>
								</tr>
								<tr class="table-row" data-student="3" data-max="100">
									<td class="text-center">382</td>
									<td><a href="<?php echo $sitepathManageExams; ?>1/result/student/1">Student four</a></td>
									<td class="text-center"><a href="mailto:abc@gmail.com">abc@gmail.com</a></td>
									<td class="text-center">4</td>
									<td class="text-center">75</td>
									<td class="text-center">57</td>
									<td class="text-center">54.51</td>
								</tr>
								<tr class="table-row" data-student="4" data-max="100">
									<td class="text-center">381</td>
									<td><a href="<?php echo $sitepathManageExams; ?>1/result/student/1">student three</a></td>
									<td class="text-center"><a href="mailto:abc@gmail.com">abc@gmail.com</a></td>
									<td class="text-center">6</td>
									<td class="text-center">85</td>
									<td class="text-center">42</td>
									<td class="text-center">54.51</td>
								</tr>
								<tr class="table-row" data-student="5" data-max="100">
									<td class="text-center">386</td>
									<td><a href="<?php echo $sitepathManageExams; ?>1/result/student/1">Student Eight</a></td>
									<td class="text-center"><a href="mailto:abc@gmail.com">abc@gmail.com</a></td>
									<td class="text-center">6</td>
									<td class="text-center">95</td>
									<td class="text-center">96</td>
									<td class="text-center">54.51</td>
								</tr>
								<tr class="table-row" data-student="6" data-max="100">
									<td class="text-center">379</td>
									<td><a href="<?php echo $sitepathManageExams; ?>1/result/student/1">student one</a></td>
									<td class="text-center"><a href="mailto:abc@gmail.com">abc@gmail.com</a></td>
									<td class="text-center">7</td>
									<td class="text-center">44</td>
									<td class="text-center">46</td>
									<td class="text-center">54.51</td>
								</tr>*/ ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->