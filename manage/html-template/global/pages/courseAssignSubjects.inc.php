<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<?php
			require_once('html-template/'.$userRole.'/includes/header-course.php');
		?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<?php
					require_once('html-template/'.$userRole.'/includes/sidebar-course.php');
				?>
				<!-- BEGIN TICKET LIST CONTENT -->
				<div class="app-ticket app-ticket-list">
					<div class="row">
						<div class="col-md-12">
							<!-- BEGIN EXAMPLE TABLE PORTLET-->
							<div class="portlet light ">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-globe"></i>Assign <?php echo strtolower($global->terminology["subject_plural"]); ?> to <?php echo $global->terminology["student_plural"]; ?> </div>
									<div class="actions">
										<a href="<?php echo $sitepathInstituteCourses.$courseId; ?>" class="btn btn-default">
											<i class="fa fa-arrow-left"></i> Back </a>
									</div>
								</div>
								<div class="portlet-body">
									<div class="table-toolbar hide">
										<div class="row">
											<div class="col-md-12">
												<div class="btn-group margin-bottom-10">
													<button class="btn sbold green" id="btnSaveAssigns"> Save Assigns
														<i class="fa fa-save"></i>
													</button>
												</div>
											</div>
										</div>
									</div>
									<table class="table table-striped table-bordered table-hover order-column" id="tableStudentsSubjects">
										<thead>
											<tr>
												<th><?php echo $global->terminology["student_plural"]; ?></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>No <?php echo strtolower($global->terminology["student_plural"]); ?> enrolled in this <?php echo strtolower($global->terminology["course_single"]); ?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>
				</div>
				<!-- END PROFILE CONTENT -->
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<?php
    /*require_once 'html-template/global/modals/importSubject.modal.php';
    require_once 'html-template/global/modals/generateStudents.modal.php';
    require_once 'html-template/global/modals/inviteStudents.modal.php';
    require_once 'html-template/global/modals/purchaseCourseKeys.modal.php';
    require_once 'html-template/global/modals/uploadDemoVideo.modal.php';
    require_once 'html-template/global/modals/uploadCourseDocument.modal.php';
    require_once 'html-template/global/modals/marketplaceSettings.modal.php';*/
?>