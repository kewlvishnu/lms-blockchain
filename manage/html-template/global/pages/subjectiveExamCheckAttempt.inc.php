<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<h1 class="page-title"> Subjective Exam Check - <span id="studentName"></span> - <span>Attempt No <span id="attemptNo">1</span></span> </h1>
		<?php require_once('html-template/'.$userRole.'/includes/header-exam.php'); ?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
                <div id="questionsContainer">
					<!-- <div class="panel panel-primary panel-questions">
						<div class="panel-heading" role="tab" id="heading1">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
								Question #1
								</a>
							</h4>
						</div>
						<div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
							<div class="panel-body no-space">
								<div class="alert alert-default no-margin">What is voltage?</div>
								<div class="alert alert-success no-margin">
									<label>Correct Answer :</label>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis rerum, ratione mollitia tenetur magni praesentium recusandae aut quasi alias maiores commodi, quis asperiores sint nisi ipsum quisquam quia quaerat laudantium.</p>
								</div>
								<div class="alert alert-info no-margin">
									<div class="row">
										<div class="col-md-6">
											<label>Student's Answer :</label>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis rerum, ratione mollitia tenetur magni praesentium recusandae aut quasi alias maiores commodi, quis asperiores sint nisi ipsum quisquam quia quaerat laudantium.</p>
										</div>
										<div class="col-md-6">
											<label>Reviews/Comments :</label>
											<textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
										</div>
									</div>
								</div>
								<div class="alert alert-info no-margin">
									<div class="row">
										<div class="col-md-6">
											<label>Student's Answer :</label>
											<button class="btn green" data-target="#modalAnswerFile" data-toggle="modal">Checkout the answer (Image format)</button>
										</div>
										<div class="col-md-6">
											<label>Reviews/Comments :</label>
											<textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
										</div>
									</div>
								</div>
								<div class="alert alert-warning no-margin">
									<div class="row">
										<div class="col-md-4">
											<label>Time :</label>
											<ul class="list-unstyled">
												<li><i class="fa fa-clock-o"></i>Time Taken: 00:00:58</li>
												<li><i class="fa fa-clock-o"></i>Topper Time: 00:01:11</li>
												<li><i class="fa fa-clock-o"></i>Average Time: 00:00:00</li>
												<li><i class="fa fa-clock-o"></i>Shortest Time: 00:00:57</li>
											</ul>
										</div>
										<div class="col-md-4">
											<label>Marks :</label>
											<ul class="list-unstyled">
												<li><i class="fa fa-check-square-o"></i>Maximum Marks: 5.00</li>
												<li><i class="fa fa-check-square-o"></i>Topper Marks: 3.00</li>
												<li><i class="fa fa-check-square-o"></i>Average Marks: 3</li>
											</ul>
										</div>
										<div class="col-md-4">
											<label>Assign Marks :</label>
											<div class="input-group">
												<input type="text" class="form-control" value="4.00"><span class="input-group-btn"><button class="btn btn-primary js-assign-marks" data-question="1" data-max="5.00">Set</button></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-primary panel-questions">
						<div class="panel-heading" role="tab" id="heading1">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
								Question #2
								</a>
							</h4>
						</div>
						<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
							<div class="panel-body no-space">
								<div class="alert alert-default no-margin">What is voltage?</div>
								<div class="alert alert-success no-margin">
									<label>Correct Answer :</label>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis rerum, ratione mollitia tenetur magni praesentium recusandae aut quasi alias maiores commodi, quis asperiores sint nisi ipsum quisquam quia quaerat laudantium.</p>
								</div>
								<div class="alert alert-info no-margin">
									<div class="row">
										<div class="col-md-6">
											<label>Student's Answer :</label>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis rerum, ratione mollitia tenetur magni praesentium recusandae aut quasi alias maiores commodi, quis asperiores sint nisi ipsum quisquam quia quaerat laudantium.</p>
										</div>
										<div class="col-md-6">
											<label>Reviews/Comments :</label>
											<textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
										</div>
									</div>
								</div>
								<div class="alert alert-info no-margin">
									<div class="row">
										<div class="col-md-6">
											<label>Student's Answer :</label>
											<button class="btn green">Checkout the answer (Image format)</button>
										</div>
										<div class="col-md-6">
											<label>Reviews/Comments :</label>
											<textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
										</div>
									</div>
								</div>
								<div class="alert alert-warning no-margin">
									<div class="row">
										<div class="col-md-4">
											<label>Time :</label>
											<ul class="list-unstyled">
												<li><i class="fa fa-clock-o"></i>Time Taken: 00:00:58</li>
												<li><i class="fa fa-clock-o"></i>Topper Time: 00:01:11</li>
												<li><i class="fa fa-clock-o"></i>Average Time: 00:00:00</li>
												<li><i class="fa fa-clock-o"></i>Shortest Time: 00:00:57</li>
											</ul>
										</div>
										<div class="col-md-4">
											<label>Marks :</label>
											<ul class="list-unstyled">
												<li><i class="fa fa-check-square-o"></i>Maximum Marks: 5.00</li>
												<li><i class="fa fa-check-square-o"></i>Topper Marks: 3.00</li>
												<li><i class="fa fa-check-square-o"></i>Average Marks: 3</li>
											</ul>
										</div>
										<div class="col-md-4">
											<label>Assign Marks :</label>
											<div class="input-group">
												<input type="text" class="form-control" value="4.00"><span class="input-group-btn"><button class="btn btn-primary js-assign-marks" data-question="1" data-max="5.00">Set</button></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-primary panel-questions">
						<div class="panel-heading" role="tab" id="heading3">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
								Question #3
								</a>
							</h4>
						</div>
						<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
							<div class="panel-body no-space">
								<div class="alert alert-default no-margin">What is voltage?</div>
								<div class="alert alert-success no-margin">
									<label>Correct Answer :</label>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis rerum, ratione mollitia tenetur magni praesentium recusandae aut quasi alias maiores commodi, quis asperiores sint nisi ipsum quisquam quia quaerat laudantium.</p>
								</div>
								<div class="alert alert-info no-margin">
									<div class="row">
										<div class="col-md-6">
											<label>Student's Answer :</label>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis rerum, ratione mollitia tenetur magni praesentium recusandae aut quasi alias maiores commodi, quis asperiores sint nisi ipsum quisquam quia quaerat laudantium.</p>
										</div>
										<div class="col-md-6">
											<label>Reviews/Comments :</label>
											<textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
										</div>
									</div>
								</div>
								<div class="alert alert-info no-margin">
									<div class="row">
										<div class="col-md-6">
											<label>Student's Answer :</label>
											<button class="btn green">Checkout the answer (Image format)</button>
										</div>
										<div class="col-md-6">
											<label>Reviews/Comments :</label>
											<textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
										</div>
									</div>
								</div>
								<div class="alert alert-warning no-margin">
									<div class="row">
										<div class="col-md-4">
											<label>Time :</label>
											<ul class="list-unstyled">
												<li><i class="fa fa-clock-o"></i>Time Taken: 00:00:58</li>
												<li><i class="fa fa-clock-o"></i>Topper Time: 00:01:11</li>
												<li><i class="fa fa-clock-o"></i>Average Time: 00:00:00</li>
												<li><i class="fa fa-clock-o"></i>Shortest Time: 00:00:57</li>
											</ul>
										</div>
										<div class="col-md-4">
											<label>Marks :</label>
											<ul class="list-unstyled">
												<li><i class="fa fa-check-square-o"></i>Maximum Marks: 5.00</li>
												<li><i class="fa fa-check-square-o"></i>Topper Marks: 3.00</li>
												<li><i class="fa fa-check-square-o"></i>Average Marks: 3</li>
											</ul>
										</div>
										<div class="col-md-4">
											<label>Assign Marks :</label>
											<div class="input-group">
												<input type="text" class="form-control" value="4.00"><span class="input-group-btn"><button class="btn btn-primary js-assign-marks" data-question="1" data-max="5.00">Set</button></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->