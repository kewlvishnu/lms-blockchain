<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<?php
			require_once('html-template/'.$userRole.'/includes/header-subject.php');
		?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light ">
					<div class="portlet-title tabbable-line">
						<div class="caption caption-md">
							<i class="icon-globe theme-font hide"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo $global->terminology["student_plural"]; ?> Performance <small>(Click on performance to see performance of any <?php echo $global->terminology["student_single"]; ?>.)</small></span>
						</div>
					</div>
					<div class="portlet-body">
                        <div class="table-responsive">
                            <table id="tblSubjectStudents" class="table table-striped table-bordered table-hover order-column">
                            	<thead>
									<th> ID # </th>
									<th> Username </th>
									<th> Name </th>
									<th> Email </th>
									<th> Actions </th>
                            	</thead>
                            	<tbody></tbody>
                            </table>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->