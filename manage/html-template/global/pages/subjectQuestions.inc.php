<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
        <?php require_once('html-template/'.$userRole.'/includes/header-questionbank.php'); ?>
		<!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-4">
                <div id="sectionQuestions"></div>
            </div><!-- .col-md-4 -->
            <div class="col-md-8">
                <div id="sectionNewQuestion" class="hide">
                    <input type="hidden" id="questionId" value="0">
                    <!-- <input type="hidden" id="hiddenQuestionType" value="0"> -->
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>Add Question </div>
                            <div class="actions">
                                <button type="button" class="btn btn-sm btn-default" id="newQuestion"> New Question </button>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form action="#" id="formQuestion">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label">Question Type :</label>
                                        <select class="form-control input-sm" id="hiddenQuestionType">
                                            <option value="-1" selected="">Select one</option>
                                            <option value="0">Multiple Choice Question</option>
                                            <option value="1">True or False</option>
                                            <option value="2">Fill in the blanks</option>
                                            <option value="3">Match the following</option>
                                            <option value="4">Comprehensive Type Question</option>
                                            <option value="5">Dependent Type Question</option>
                                            <option value="6">Multiple Answer Question</option>
                                            <option value="7">Audio question</option>
                                        </select>
                                        <div class="help-block">
                                            <i class="fa fa-info-circle tooltips" data-placement="right" data-original-title="Multiple Choice Questions are the  most common type of questions used across exams<br>They only have one answer option as the correct answer" id="questionInfo"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Question :</label>
                                        <textarea class="ckeditor form-control" name="question" rows="6"></textarea>
                                    </div>
                                    <div class="form-group" id="options">
                                        <label class="control-label">Only one answer option is correct :</label>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <input type="radio" name="options">
                                                                    <span></span>
                                                                </span>
                                                                <input type="text" class="form-control option" id="option0" placeholder="Answer Option">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <input type="radio" name="options">
                                                                    <span></span>
                                                                </span>
                                                                <input type="text" class="form-control option" id="option1" placeholder="Answer Option">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row add-div"><?php /*
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <input type="radio" name="options">
                                                                    <span></span>
                                                                </span>
                                                                <input type="text" class="form-control option" placeholder="Answer Option">
                                                                <span class="input-group-btn">
                                                                    <button class="btn red" type="button"><i class="fa fa-trash"></i></button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <input type="radio" name="options">
                                                                    <span></span>
                                                                </span>
                                                                <input type="text" class="form-control option" placeholder="Answer Option">
                                                                <span class="input-group-btn">
                                                                    <button class="btn red" type="button"><i class="fa fa-trash"></i></button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>*/ ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <button class="btn btn-info add-option"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="trueFalse">
                                        <label class="control-label">Only one answer option is correct :</label>
                                        <div class="mt-radio-inline">
                                            <label class="mt-radio">
                                                <input type="radio" name="trueFalse" id="true"> True
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="trueFalse" id="false"> False
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group" id="ftb">
                                        <label class="control-label">Please write the correct answer :</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" placeholder="Please write the answer" class="form-control ftb" id="ftbOption0">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="match">
                                        <label class="control-label">Enter the data for match the following :</label>
                                        <p class="note note-info"><?php echo $global->terminology["student_single"]; ?> will see column B in jumbled order</p>
                                        <div class="row text-center">
                                            <div class="col-xs-5">
                                                <strong>A</strong>
                                            </div>
                                            <div class="col-xs-5">
                                                <strong>B</strong>
                                            </div>
                                        </div>
                                        <div class="custom-match">
                                            <div class="row match margin-bottom-10">
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control a" placeholder="Enter option A here" id="mtfOptionA0">
                                                </div>
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control b" placeholder="Correct answer for column A" id="mtfOptionB0">
                                                </div>
                                                <div class="col-md-2">
                                                    <button type="button" class="btn btn-info add-option"><i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="row match margin-bottom-10">
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control a" placeholder="Enter option A here" id="mtfOptionA1">
                                                </div>
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control b" placeholder="Correct answer for column A" id="mtfOptionB1">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="optionsMul">
                                        <label class="control-label">More than one answer option can be correct :</label>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <input type="checkbox">
                                                                    <span></span>
                                                                </span>
                                                                <input type="text" class="form-control optionMul" id="optionMul0" placeholder="Answer Option">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <input type="checkbox">
                                                                    <span></span>
                                                                </span>
                                                                <input type="text" class="form-control optionMul" id="optionMul1" placeholder="Answer Option">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row add-div"></div>
                                            </div>
                                            <div class="col-md-2">
                                                <button class="btn btn-info add-option"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="audio">
                                        <label class="control-label">Select audio clip to upload :</label>
                                        <div class="form-group">
                                            <button type="button" class="btn btn-warning btn-audio-upload">Upload Audio Clip (MP3)</button>
                                        </div>
                                        <div class="form-group">
                                            <div class="js-file-path" data-path=""></div>
                                        </div>
                                        <label class="control-label">Only one answer option is correct :</label>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <input type="radio">
                                                                    <span></span>
                                                                </span>
                                                                <input type="text" class="form-control optionAud" id="optionAud0" placeholder="Answer Option">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <input type="radio">
                                                                    <span></span>
                                                                </span>
                                                                <input type="text" class="form-control optionAud" id="optionAud1" placeholder="Answer Option">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row add-div"></div>
                                            </div>
                                            <div class="col-md-2">
                                                <button class="btn btn-info add-option"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="compro" class="form-group hide">
                                        <div class="well well-sm sectionNewQuestion">
                                            <input type="hidden" class="questionId" value="0">
                                            <div class="form-body">
                                                <input type="hidden" id="hiddenQuestionType" value="0">
                                                <div class="form-group">
                                                    <label class="control-label">Category :</label>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <select class="form-control questionTypeSelect">
                                                                <option value="0">Multiple Choice Question</option>
                                                                <option value="1">True or False</option>
                                                                <option value="2">Fill in the blanks</option>
                                                                <option value="6">Multiple Answer Questions</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="marks text-info"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Question :</label>
                                                    <textarea placeholder="Please enter the question text here. The passage is already written above." rows="2" class="form-control ckeditor question" id="childQuestion0"></textarea>
                                                </div>
                                                <div class="form-group options">
                                                    <label class="control-label">Only one answer option is correct :</label>
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">
                                                                                <input type="radio" name="childOptions0">
                                                                                <span></span>
                                                                            </span>
                                                                            <input type="text" class="form-control option" id="childOption00" placeholder="Answer Option">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">
                                                                                <input type="radio" name="childOptions0">
                                                                                <span></span>
                                                                            </span>
                                                                            <input type="text" class="form-control option" id="childOption01" placeholder="Answer Option">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row add-div"></div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button class="btn btn-info add-option"><i class="fa fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group trueFalse">
                                                    <label class="control-label">Only one answer option is correct :</label>
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" class="true" name="trueFalse0"> True
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" class="false" name="trueFalse0"> False
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group ftbs">
                                                    <label class="control-label">Please write the correct answer :</label>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input type="text" placeholder="Please write the answer" class="form-control ftb" id="childftb00">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group optionsMul">
                                                    <label class="control-label">More than one answer option can be correct :</label>
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">
                                                                                <input type="checkbox">
                                                                                <span></span>
                                                                            </span>
                                                                            <input type="text" class="form-control optionMul" id="childOptionMul00" placeholder="Answer Option">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">
                                                                                <input type="checkbox">
                                                                                <span></span>
                                                                            </span>
                                                                            <input type="text" class="form-control optionMul" id="childOptionMul01" placeholder="Answer Option">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row add-div"></div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button class="btn btn-info add-option"><i class="fa fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Explanation :</label>
                                                    <textarea class="form-control ckeditor desc" rows="2" placeholder="Please enter the explanation of the question" id="childDesc0"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions lastCol">
                                            <button type="button" class="btn green add-question"><i class="fa fa-plus-circle"> Question</i></button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Difficulty Level :</label>
                                                <select name="selectDifficulty" id="selectDifficulty" class="form-control">
                                                    <option value="0">Select Difficulty</option>
                                                    <option value="1">Very Easy</option>
                                                    <option value="2">Easy</option>
                                                    <option value="3">Fair</option>
                                                    <option value="4">Difficult</option>
                                                    <option value="5">Very Difficult</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Learning Objectives :</label>
                                                <div class="btn-block">
                                                    <div id="magicsuggest"></div>
                                                    <!-- <input type="text" value="Amsterdam,Washington,Sydney,Beijing,Cairo" data-role="tagsinput" id="tags-input"> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group desc-block">
                                        <label class="control-label">Explanation :</label>
                                        <textarea class="ckeditor form-control" name="desc" id="desc" rows="6"></textarea>
                                    </div>
                                </div>
                                <div class="form-actions right">
                                    <a href="javascript:;" class="btn dark save-button">
                                        <i class="fa fa-check"></i> Submit</a>
                                    <a href="javascript:;" class="btn btn-outline grey-salsa cancel-button">Cancel</a>
                                </div>
                            </form>
                            <form id="frmAudioUpload" class="frm-upload" action="<?php echo $sitepath; ?>api/files1.php" method="post">
                                <input type="file" name="audio-upload" class="js-uploader hide">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->