<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
        <?php require_once('html-template/'.$userRole.'/includes/header-exam.php'); ?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green">
					<div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-white"></i>
                            <span class="caption-subject bold font-white uppercase">Percentage distribution of all <?php echo $global->terminology["student_plural"]; ?></span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
					<div class="portlet-body">
						<div id="percentDistGraph" class="min-height-500"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
                            <i class=" icon-layers font-white"></i>
                            <span class="caption-subject bold font-white uppercase">Scores</span>
                        </div>
					</div>
					<div class="portlet-body">
						<div id="scoresBarGraph" class="min-height-500"></div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
                            <i class=" icon-layers font-white"></i>
                            <span class="caption-subject bold font-white uppercase">Percent</span>
                        </div>
					</div>
					<div class="portlet-body">
						<div id="percentBarGraph" class="min-height-500"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" id="exam">
				<!-- <div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
				                            <i class=" icon-layers font-white"></i>
				                            <span class="caption-subject bold font-white uppercase">Exam Marks Table [Quick Analytics]</span>
				                        </div>
					</div>
					<div class="portlet-body no-space">
						<table class="table table-hover table-bordered no-margin">
							<thead>
								<tr>
									<th class="text-center" width="10%">Student Id</th>
									<th>Student Name</th>
									<th class="text-center" width="15%">Total Marks (out of 100)</th>
									<th class="text-center" width="15%">Percent</th>
									<th class="text-center" width="15%">Rank</th>
									<th class="text-center" width="15%">Percentile</th>
								</tr>
							</thead>
							<tbody>
								<tr class="table-row" data-student="0" data-max="100">
									<td class="text-center">384</td>
									<td>Student Six</td>
									<td class="text-center">76 <button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button></td>
									<td class="text-center">76</td>
									<td class="text-center">1</td>
									<td class="text-center">100</td>
								</tr>
								<tr class="table-row" data-student="1" data-max="100">
									<td class="text-center">387</td>
									<td>Student Nine</td>
									<td class="text-center">65 <button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button></td>
									<td class="text-center">65</td>
									<td class="text-center">2</td>
									<td class="text-center">85.71</td>
								</tr>
								<tr class="table-row" data-student="2" data-max="100">
									<td class="text-center">380</td>
									<td>student two</td>
									<td class="text-center">56 <button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button></td>
									<td class="text-center">56</td>
									<td class="text-center">3</td>
									<td class="text-center">71.43</td>
								</tr>
								<tr class="table-row" data-student="3" data-max="100">
									<td class="text-center">382</td>
									<td>Student four</td>
									<td class="text-center">54 <button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button></td>
									<td class="text-center">54</td>
									<td class="text-center">4</td>
									<td class="text-center">57.14</td>
								</tr>
								<tr class="table-row" data-student="4" data-max="100">
									<td class="text-center">381</td>
									<td>student three</td>
									<td class="text-center">23 <button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button></td>
									<td class="text-center">23</td>
									<td class="text-center">5</td>
									<td class="text-center">42.86</td>
								</tr>
								<tr class="table-row" data-student="5" data-max="100">
									<td class="text-center">386</td>
									<td>Student Eight</td>
									<td class="text-center">23 <button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button></td>
									<td class="text-center">23</td>
									<td class="text-center">5</td>
									<td class="text-center">42.86</td>
								</tr>
								<tr class="table-row" data-student="6" data-max="100">
									<td class="text-center">379</td>
									<td>student one</td>
									<td class="text-center">23 <button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button></td>
									<td class="text-center">23</td>
									<td class="text-center">5</td>
									<td class="text-center">42.86</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
				                            <i class=" icon-layers font-white"></i>
				                            <span class="caption-subject bold font-white uppercase">Exam Marks Table [Deep Analytics]</span>
				                            <small>(Click on any row to get marks by question)</small>
				                        </div>
					</div>
					<div class="portlet-body no-space">
						<table class="table table-hover table-bordered table-deep no-margin">
							<thead>
								<tr>
									<th class="text-center" width="10%">Student Id</th>
									<th>Student Name</th>
									<th class="text-center" width="15%">Total Marks (out of 1000)</th>
									<th class="text-center" width="15%">Percent</th>
									<th class="text-center" width="15%">Rank</th>
									<th class="text-center" width="15%">Percentile</th>
								</tr>
							</thead>
							<tbody>
								<tr class="table-row" data-student="0" data-max="1000">
									<td class="text-center">134</td>
									<td>Jackie Christ</td>
									<td class="text-center">638</td>
									<td class="text-center">63.8</td>
									<td class="text-center">1</td>
									<td class="text-center">100</td>
								</tr>
								<tr class="table-row" data-student="1" data-max="1000">
									<td class="text-center">380</td>
									<td>student two</td>
									<td class="text-center">623</td>
									<td class="text-center">62.3</td>
									<td class="text-center">2</td>
									<td class="text-center">87.5</td>
								</tr>
								<tr class="table-row" data-student="2" data-max="1000">
									<td class="text-center">379</td>
									<td>student one</td>
									<td class="text-center">591</td>
									<td class="text-center">59.1</td>
									<td class="text-center">3</td>
									<td class="text-center">75</td>
								</tr>
								<tr class="table-row" data-student="3" data-max="1000">
									<td class="text-center">387</td>
									<td>Student Nine</td>
									<td class="text-center">519</td>
									<td class="text-center">51.9</td>
									<td class="text-center">4</td>
									<td class="text-center">62.5</td>
								</tr>
								<tr class="table-row" data-student="4" data-max="1000">
									<td class="text-center">382</td>
									<td>Student four</td>
									<td class="text-center">506</td>
									<td class="text-center">50.6</td>
									<td class="text-center">5</td>
									<td class="text-center">50</td>
								</tr>
								<tr class="table-row" data-student="5" data-max="1000">
									<td class="text-center">381</td>
									<td>student three</td>
									<td class="text-center">496</td>
									<td class="text-center">49.6</td>
									<td class="text-center">6</td>
									<td class="text-center">37.5</td>
								</tr>
								<tr class="table-row" data-student="6" data-max="1000">
									<td class="text-center">386</td>
									<td>Student Eight</td>
									<td class="text-center">467</td>
									<td class="text-center">46.7</td>
									<td class="text-center">7</td>
									<td class="text-center">25</td>
								</tr>
								<tr class="table-row" data-student="7" data-max="1000">
									<td class="text-center">384</td>
									<td>Student Six</td>
									<td class="text-center">304</td>
									<td class="text-center">30.4</td>
									<td class="text-center">8</td>
									<td class="text-center">12.5</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div> -->
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->