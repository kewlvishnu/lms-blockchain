<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<h1 class="page-title"> <?php echo $global->terminology["course_single"]; ?> Enrollment Keys
			<small>your <?php echo strtolower($global->terminology["course_single"]); ?> enrollment keys consumption summary</small>
		</h1>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo $sitepathManageInstitute; ?>">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<span><?php echo $global->terminology["course_single"]; ?> Enrollment Keys</span>
				</li>
			</ul>
			<div class="page-toolbar">
				<div class="btn-group pull-right">
					<?php if (isset($global->walletAddress) && !empty($global->walletAddress)) { ?>
					<button class="btn green btn-fit-height btn-purchase-keys" data-wallet="<?php echo $global->walletAddress; ?>"> Purchase Enrollment Keys (<?php echo $global->displayCurrency; ?>)</button>
					<?php } else { ?>
					<button class="btn green btn-fit-height btn-purchase-keys"> Purchase Enrollment Keys (<?php echo $global->displayCurrency; ?>)</button>
					<?php } ?>
				</div>
			</div>
		</div>
		<!-- END PAGE HEADER-->
        <!-- BEGIN DASHBOARD STATS 1-->
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <span class="dashboard-stat dashboard-stat-v2 blue">
                    <div class="visual">
                        <i class="fa fa-eye"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="1349" id="active_key">0</span>
                        </div>
                        <div class="desc"> Enrollment Keys Used </div>
                    </div>
                </span>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <span class="dashboard-stat dashboard-stat-v2 red">
                    <div class="visual">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="125" id="remaining_key">0</span> </div>
                        <div class="desc"> Enrollment Keys Remaining </div>
                    </div>
                </span>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <span class="dashboard-stat dashboard-stat-v2 green">
                    <div class="visual">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="549" id="total_keys">0</span>
                        </div>
                        <div class="desc"> Total Enrollment Keys </div>
                    </div>
                </span>
            </div>
        </div>
        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="portlet light">
                    <div class="portlet-title tabbable-line">
                        <div class="caption">
                            <span class="caption-subject bold font-dark uppercase"> Enrollment Keys </span>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#portlet_tab1" data-toggle="tab"> History </a>
                            </li>
                            <li>
                                <a href="#portlet_tab2" data-toggle="tab"> <?php echo $global->terminology["course_single"]; ?>wise </a>
                            </li>
                            <li>
                                <a href="#portlet_tab3" data-toggle="tab"> Purchased <?php echo $global->terminology["course_plural"]; ?> </a>
                            </li>
                        </ul>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="portlet_tab1">
                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tableTotalKeys">
									<thead>
										<tr>
											<th>#</th>
											<th>Purchased Date</th>
											<th>Purchased Time</th>
											<th>Enrollment Key Purchased</th>
											<th>Rate</th>
											<th>Total Amount</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
                            </div>
                            <div class="tab-pane" id="portlet_tab2">
                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tableCourseWise">
									<thead>
										<tr>
											<th>#</th>
											<th><?php echo $global->terminology["course_single"]; ?> Id</th>
											<th><?php echo $global->terminology["course_single"]; ?> Name</th>
											<th><?php echo $global->terminology["student_single"]; ?> (<?php echo $global->terminology["course_single"]; ?> key)</th>
											<th><?php echo $global->terminology["student_single"]; ?> (Stand alone)</th>
										</tr>
									</thead>
									<tbody>
										<!-- <tr course_id="99">
											<td>99 </td>
											<td>Naval Engineering - 2016</td>
											<td>
												<a class="showcourse" data-toggle="modal" href="#modal_student">11</a>
											</td>
											<td>
												<a href="#modal_student" data-toggle="modal" class="shownonkey">0 </a>
											</td>
										</tr>
										<tr course_id="118">
											<td>118 </td>
											<td>Engineering Demo</td>
											<td>
												<a class="showcourse" data-toggle="modal" href="#modal_student">7</a>
											</td>
											<td>
												<a href="#modal_student" data-toggle="modal" class="shownonkey">0 </a>
											</td>
										</tr> -->
									</tbody>
								</table>
                            </div>
                            <div class="tab-pane" id="portlet_tab3">
                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tablePurchasedCourses">
									<thead>
										<tr>
											<th>#</th>
											<th><?php echo $global->terminology["course_single"]; ?> Id</th>
											<th><?php echo $global->terminology["course_single"]; ?> Name</th>
											<th>Date</th>
											<th>Institute</th>
											<th>Price</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->