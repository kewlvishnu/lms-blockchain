<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
        <?php require_once('html-template/'.$userRole.'/includes/header-exam.php'); ?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Manual Exam Settings </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form portlet-form">
                        <!-- BEGIN FORM-->
                        <form id="formManualExam" class="horizontal-form">
                            <div class="form-body">
                                <h3 class="form-section">Basic Information</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Title of the test</label>
                                            <input type="text" id="titleName" class="form-control" placeholder="Title">
                                            <span class="help-block"> Enter title of the test. </span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Section <i class="tooltips fa fa-info-circle" data-original-title="Please select the section name under which the Subjective Exam would be listed. You can make the Subjective Exam as Independent, if you do not want it to be part of any section" data-placement="right"></i></label>
                                            <select id="chapterSelect" class="form-control" data-placeholder="Choose a Section" tabindex="1">
                                                <option value="">Select Section</option>
                                            </select>
                                            <span class="help-block"> Select section. </span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                            <div class="form-actions right">
                                <a href="<?php echo $sitepathManageSubjects.$subjectId; ?>" class="btn default">Cancel</a>
                                <button type="submit" class="btn blue">
                                    <i class="fa fa-save"></i> Save</button>
                                <!-- <button type="button" class="btn green makelive">
                                    <i class="fa fa-check"></i> Go Live</button> -->
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->