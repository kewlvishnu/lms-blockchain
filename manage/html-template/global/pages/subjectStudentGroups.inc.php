<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<?php
			require_once('html-template/'.$userRole.'/includes/header-subject.php');
		?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light " id="listSubjectStudents">
					<div class="portlet-title tabbable-line">
						<div class="caption caption-md">
							<i class="icon-globe theme-font hide"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo $global->terminology["subject_single"]; ?> <?php echo $global->terminology["student_single"]; ?> Groups</span>
						</div>
						<div class="actions">
							<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>/students" class="btn btn-info btn-sm"> <?php echo $global->terminology["student_plural"]; ?> </a>
							<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>" class="btn btn-primary btn-sm">
								<i class="fa fa-arrow-left"></i> Back </a>
							<!-- <a href="javascript:;" class="btn btn-primary btn-sm js-chat" data-student="0">
								<i class="fa fa-comment"></i> Public Chat </a> -->
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-toolbar">
							<div class="row">
								<div class="col-md-10">
									<div class="btn-group margin-bottom-10">
										<button id="btnAddStudentGroup" class="btn sbold green"> Add New
											<i class="fa fa-plus"></i>
										</button>
									</div>
									<div class="btn-group margin-bottom-10">
										<button id="btnRemoveStudents" class="btn sbold red"> Remove
											<i class="fa fa-trash"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
						<div class="js-students row"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->