<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <?php require_once('html-template/'.$userRole.'/includes/header-exam.php'); ?>
        <!-- END PAGE HEADER-->
        <div class="row margin-bottom-20 attempt-block">
            <div class="col-md-6">
                <a href="javascript:;" class="btn green btn-block previous-attempt" disabled><i class="fa fa-arrow-left"></i> Previous Attempt</a>
            </div>
            <div class="col-md-6">
                <a href="javascript:;" class="btn green btn-block next-attempt" disabled><i class="fa fa-arrow-right"></i> Next Attempt</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat dashboard-stat-v2 blue">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="rank">0/0</span>
                        </div>
                        <div class="desc"> Rank </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat dashboard-stat-v2 red">
                    <div class="visual">
                        <i class="fa fa-percent"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="percentage">0</span>% </div>
                        <div class="desc"> Percentage </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat dashboard-stat-v2 green">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="score">0/0</span>
                        </div>
                        <div class="desc"> Score </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat stat-sm dashboard-stat-v2 purple">
                    <div class="visual">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="startDate">11-May-2016 23:17</span> </div>
                        <div class="desc"> Started On </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat stat-sm dashboard-stat-v2 blue">
                    <div class="visual">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="endDate">11-May-2016 23:20</span>
                        </div>
                        <div class="desc"> Completed On </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat stat-sm dashboard-stat-v2 red">
                    <div class="visual">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="timeTaken">2 min 38 sec</span> </div>
                        <div class="desc"> Time taken </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BEGIN : HIGHCHARTS -->
        <div class="row">
            <div class="col-md-6">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-white"></i>
                            <span class="caption-subject bold font-white uppercase">TimeLine Graph</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="highchart_1" style="height:500px;"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-white"></i>
                            <span class="caption-subject bold font-white uppercase">Percentage Distribution</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="hero-bar3" style="height:500px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-microphone font-white hide"></i>
                            <span class="caption-subject bold font-white uppercase"> Attempt Summary</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body no-space">
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-md-6">
                                    <span>
                                        <strong>Legends :</strong>
                                        <span class="label bg-green-jungle"> Correctly answered </span>
                                        <span class="label bg-yellow-casablanca"> Wrongly answered </span>
                                        <span class="label bg-dark"> Unattempted </span>
                                    </span>
                                </div>
                                <div class="col-md-3">
                                    <a class="text-success" href="#examQuestionCategoriesModal" data-toggle="modal"><i class="fa fa-bars"></i> See Question Categories</a>
                                </div>
                                <div class="col-md-3">
                                    <a class="text-success" href="#topperCompare" data-scroll><i class="fa fa-search"></i> Compare with toppers</a>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table no-margin" id="sectionTable">
                                <thead>
                                    <tr>
                                        <th>Section</th>
                                        <th class="text-center">Total Questions</th>
                                        <th class="text-center">Your Score</th>
                                        <th>Time Taken</th>
                                        <th>Question Response</th>
                                    </tr>
                                </thead>
                                <tbody><?php /*
                                    <tr>
                                        <td>Section A</td>
                                        <td class="text-center">10</td>
                                        <td class="text-center"></td>
                                        <td>2 min 38 sec</td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar bg-green-jungle" style="width: 60%;"><span>6</span><span class="sr-only">6</span></div><div class="progress-bar bg-yellow-casablanca" style="width: 30%;"><span>3</span><span class="sr-only">3</span></div><div class="progress-bar bg-dark" style="width: 10%"><span>1</span><span class="sr-only">1</span></div>
                                            </div>
                                        </td>
                                    </tr>*/
                                 ?></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="topperCompare">
            <div class="col-md-6">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-white"></i>
                            <span class="caption-subject bold font-white uppercase">Topper Comparison by Score</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chart_1" class="chart" style="height: 500px;"> </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-white"></i>
                            <span class="caption-subject bold font-white uppercase">Topper Comparison by Percentage</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chart_2" class="chart" style="height: 500px;"> </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div id="sectionDetails">
                    <!-- <section class="panel panel-success panel-result no-space">
                        <header class="panel-heading">
                            <a href="javascript:void(0)" class="hide-section text-uppercase">
                                <strong><i class="fa fa-minus-square"></i> Section A</strong>
                            </a>
                        </header>
                        <div class="panel-body no-space">
                            <section class="panel">
                                <table class="table no-margin">
                                    <tbody>
                                        <tr data-id="6744">
                                            <td width="5%">Q.1<div class="answer-status"><i class="fa fa-check-circle-o text-success"></i></div></td>
                                            <td colspan="2">
                                                <div class="question">
                                                    <p>Theory of Mercantilism propagates&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                    <p> <strong> ( Multiple Choice Questions) </strong></p>
                                                </div>
                                                <div class="form-group">
                                                    <button class="btn btn-info btn-sm view-solution" data-hidden="0" data-text="View Solution">View Solution</button>
                                                </div>
                                                <div class="answer well well-sm" style="display: none;">
                                                    <ul class="list-unstyled">
                                                        <li>a) Encourage exports and imports</li>
                                                        <li>b) Encourage exports and discourage imports<i class="fa fa-check-circle-o text-success"></i></li>
                                                        <li>c) Discourage exports and imports</li>
                                                        <li>d) Discourage exports and encourage imports</li>
                                                    </ul>
                                                    <p><strong>Explanation</strong></p>
                                                </div>
                                            </td>
                                            <td class="text-right" width="12%">
                                                <strong>Marks</strong> +2.00 / -1.00<br><strong>Time</strong> 39s<br><a class="time-compare"><strong>Compare</strong></a>
                                            </td>
                                        </tr>
                                        <tr data-id="6745">
                                            <td width="5%">Q.2<div class="answer-status"><i class="fa fa-check-circle-o text-success"></i></div>
                                        </td>
                                        <td colspan="2">
                                            <div class="question">
                                                <p>Firm that operate internationally are able to&nbsp;</p>
                                                <p>&nbsp;</p>
                                                <p> <strong> ( Multiple Choice Questions) </strong></p>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-info btn-sm view-solution" data-hidden="0" data-text="View Solution">View Solution</button>
                                            </div>
                                                <div class="answer well well-sm" style="display: none;">
                                                    <ul class="list-unstyled">
                                                        <li>a) Earn a greater return from their skills and core competencies</li>
                                                        <li>b) Realize location economies where they can be performed most efficiently</li>
                                                        <li>c) Realize greater experience curve economies, which reduces the cost of production</li>
                                                        <li>d) All of the above<i class="fa fa-check-circle-o text-success"></i></li>
                                                    </ul>
                                                    <p><strong>Explanation</strong></p>
                                                </div>
                                            </td>
                                            <td class="text-right" width="12%">
                                                <strong>Marks</strong> +2.00 / -1.00<br><strong>Time</strong> 17s<br><a class="time-compare"><strong>Compare</strong></a>
                                            </td>
                                        </tr>
                                        <tr data-id="6741">
                                            <td width="5%">Q.3<div class="answer-status"><i class="fa fa-times-circle-o text-danger"></i></div>
                                            </td>
                                            <td colspan="2">
                                                <div class="question">
                                                    <p>Quantitative restrictions refer to limit set by countries to curb?</p>
                                                    <p> <strong> ( Multiple Choice Questions) </strong></p>
                                                </div>
                                                <div class="form-group">
                                                    <button class="btn btn-info btn-sm view-solution" data-hidden="0" data-text="View Solution">View Solution</button>
                                                </div>
                                                <div class="answer well well-sm" style="display: none;">
                                                    <ul class="list-unstyled">
                                                        <li>a) Imports<i class="fa fa-times-circle-o text-danger"></i></li>
                                                        <li>b)  Exports</li>
                                                        <li>c) Imports &amp; exports<i class="fa fa-check-circle-o text-success"></i></li>
                                                        <li>d) None of the above</li>
                                                    </ul>
                                                    <p><strong>Explanation</strong></p>
                                                </div>
                                            </td>
                                            <td class="text-right" width="12%">
                                                <strong>Marks</strong> +2.00 / -1.00<br><strong>Time</strong> 15s<br><a class="time-compare"><strong>Compare</strong></a>
                                            </td>
                                        </tr>
                                        <tr data-id="6742">
                                            <td width="5%">Q.4<div class="answer-status"><i class="fa fa-dot-circle-o text-warning"></i></div></td>
                                            <td colspan="2">
                                                <div class="question"><p>Power distance Index (PDI) of 77 compared to a world average of 56.5 for India indicates?</p><p>&nbsp;</p>
                                                <p> <strong> ( Multiple Choice Questions) </strong></p></div>
                                                <div class="form-group">
                                                    <button class="btn btn-info btn-sm view-solution" data-hidden="0" data-text="View Solution">View Solution</button>
                                                </div>
                                                <div class="answer well well-sm" style="display: none;">
                                                    <ul class="list-unstyled">
                                                        <li>a) High level of inequality of power and wealth within the society<i class="fa fa-check-circle-o text-success"></i></li>
                                                        <li>b) Low level of inequality of power and wealth within the society</li>
                                                        <li>c) High level of Political corruption</li>
                                                        <li>d) Low level of Human development Index </li>
                                                    </ul>
                                                    <p><strong>Explanation</strong></p>
                                                </div>
                                            </td>
                                            <td class="text-right" width="12%">
                                                <strong>Marks</strong> +2.00 / -1.00<br><strong>Time</strong> 11s<br><a class="time-compare"><strong>Compare</strong></a>
                                            </td>
                                        </tr>
                                        <tr data-id="6743">
                                            <td width="5%">Q.5<div class="answer-status"><i class="fa fa-check-circle-o text-success"></i></div></td>
                                            <td colspan="2">
                                                <div class="question"><p>An attribute that doesn’t contribute to Porters Diamond model is</p>
                                                <p><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</strong></p>
                                                <p> <strong> ( Multiple Choice Questions) </strong></p></div>
                                                <div class="form-group">
                                                    <button class="btn btn-info btn-sm view-solution" data-hidden="0" data-text="View Solution">View Solution</button>
                                                </div>
                                                <div class="answer well well-sm" style="display: none;">
                                                    <ul class="list-unstyled">
                                                        <li>a) Government</li>
                                                        <li>b) Factor Conditions</li>
                                                        <li>c) Organised Trade Union<i class="fa fa-check-circle-o text-success"></i></li>
                                                        <li>d) Demand Conditions</li>
                                                    </ul>
                                                    <p><strong>Explanation</strong></p>
                                                </div>
                                            </td>
                                            <td class="text-right" width="12%">
                                                <strong>Marks</strong> +2.00 / -1.00<br><strong>Time</strong> 61s<br><a class="time-compare"><strong>Compare</strong></a>
                                            </td>
                                        </tr>
                                        <tr data-id="6749">
                                            <td width="5%">Q.6<div class="answer-status"><i class="fa fa-times-circle-o text-danger"></i></div>
                                            </td>
                                            <td colspan="2">
                                                <div class="question"><p>Geocentric staffing policy ensures best qualified people at suitable positions irrespective of their national and cultural differences.&nbsp;</p>
                                                <p> <strong> ( True and False ) </strong></p></div>
                                                <div class="form-group">
                                                    <button class="btn btn-info btn-sm view-solution" data-hidden="0" data-text="View Solution">View Solution</button>
                                                </div>
                                                <div class="answer well well-sm" style="display: none;">True <i class="fa fa-check-circle-o text-success"></i><br>False <i class="fa fa-times-circle-o text-danger"></i><p><strong>Explanation</strong></p></div>
                                            </td>
                                            <td class="text-right" width="12%">
                                                <strong>Marks</strong> +2.00 / -1.00<br><strong>Time</strong> 6s<br><a class="time-compare"><strong>Compare</strong></a>
                                            </td>
                                        </tr>
                                        <tr data-id="6746">
                                            <td width="5%">Q.7<div class="answer-status"><i class="fa fa-check-circle-o text-success"></i></div>
                                            </td>
                                            <td colspan="2">
                                                <div class="question">
                                                    <p>An international acquisition is a cross boarder investment in which a foreign investor acquires an established local firm and makes the acquired local firm a subsidiary business within its global portfolio.&nbsp;</p>
                                                    <p> <strong> ( True and False ) </strong></p>
                                                </div>
                                                <div class="form-group">
                                                    <button class="btn btn-info btn-sm view-solution" data-hidden="0" data-text="View Solution">View Solution</button>
                                                </div>
                                                <div class="answer well well-sm" style="display: none;">True <i class="fa fa-check-circle-o text-success"></i><br>False<p><strong>Explanation</strong></p></div>
                                            </td>
                                            <td class="text-right" width="12%">
                                                <strong>Marks</strong> +2.00 / -1.00<br><strong>Time</strong> 2s<br><a class="time-compare"><strong>Compare</strong></a>
                                            </td>
                                        </tr>
                                        <tr data-id="6747">
                                            <td width="5%">Q.8<div class="answer-status"><i class="fa fa-times-circle-o text-danger"></i></div>
                                            </td>
                                            <td colspan="2">
                                                <div class="question">
                                                    <p>Theory of Comparative advantage demonstrates that mutually advantageous trade cannot occur when one trading partner has an absolute advantage</p>
                                                    <p> <strong> ( True and False ) </strong></p>
                                                </div>
                                                <div class="form-group">
                                                    <button class="btn btn-info btn-sm view-solution" data-hidden="0" data-text="View Solution">View Solution</button>
                                                </div>
                                                <div class="answer well well-sm" style="display: none;">True <i class="fa fa-times-circle-o text-danger"></i><br>False <i class="fa fa-check-circle-o text-success"></i><p><strong>Explanation</strong></p></div>
                                            </td>
                                            <td class="text-right" width="12%">
                                                <strong>Marks</strong> +2.00 / -1.00<br><strong>Time</strong> 2s<br><a class="time-compare"><strong>Compare</strong></a>
                                            </td>
                                        </tr>
                                        <tr data-id="6748">
                                            <td width="5%">Q.9<div class="answer-status"><i class="fa fa-check-circle-o text-success"></i></div>
                                            </td>
                                            <td colspan="2">
                                                <div class="question">
                                                    <p>Product life cycle theory states that, an exporter country of a particular product can become an importer country during the product cycle of a product.&nbsp;</p>
                                                    <p> <strong> ( True and False ) </strong></p></div>
                                                <div class="form-group">
                                                    <button class="btn btn-info btn-sm view-solution" data-hidden="0" data-text="View Solution">View Solution</button>
                                                </div>
                                                <div class="answer well well-sm" style="display: none;">True <i class="fa fa-check-circle-o text-success"></i><br>False<p><strong>Explanation</strong></p></div>
                                            </td>
                                            <td class="text-right" width="12%">
                                                <strong>Marks</strong> +2.00 / -1.00<br><strong>Time</strong> 1s<br><a class="time-compare"><strong>Compare</strong></a>
                                            </td>
                                        </tr>
                                        <tr data-id="6750">
                                            <td width="5%">Q.10<div class="answer-status"><i class="fa fa-check-circle-o text-success"></i></div>
                                            </td>
                                            <td colspan="2">
                                                <div class="question">
                                                    <p>Risk of direct exporting are relatively lower since the investments are also lesser.&nbsp;</p>
                                                    <p> <strong> ( True and False ) </strong></p>
                                                </div>
                                                <div class="form-group">
                                                    <button class="btn btn-info btn-sm view-solution" data-hidden="0" data-text="View Solution">View Solution</button>
                                                </div>
                                                <div class="answer well well-sm" style="display: none;">True<br>False <i class="fa fa-check-circle-o text-success"></i><p><strong>Explanation</strong></p></div>
                                            </td>
                                            <td class="text-right" width="12%"><strong>Marks</strong> +2.00 / -1.00<br><strong>Time</strong> 4s<br><a class="time-compare"><strong>Compare</strong></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </section>
                        </div>
                    </section> -->
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->