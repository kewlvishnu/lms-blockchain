<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h1 class="page-title"> Account Settings
            <small>modify your account settings</small>
        </h1>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo $sitepathManage; ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Account Settings</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet light ">
                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Account Settings</span>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_1" data-toggle="tab">CryptoWallet</a>
                            </li>
                            <li>
                                <a href="#tab_1_2" data-toggle="tab">Social Settings</a>
                            </li>
                            <li>
                                <a href="#tab_1_3" data-toggle="tab">Account Type</a>
                            </li>
                            <li>
                                <a href="#tab_1_4" data-toggle="tab">Personal Data</a>
                            </li>
                        </ul>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <div class="tab-pane active" id="tab_1_1">
                                <div class="note note-info">
                                    <h4 class="block">Set your address</h4>
                                    <p> The address below is your physical address that will be used for currency transactions</p>
                                </div>
                                <form role="form" action="" id="formCryptoWallet">
                                    <div class="alert alert-danger display-hide">
                                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                    <div class="alert alert-success display-hide">
                                        <button class="close" data-close="alert"></button> Your address is successfully updated! </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Physical Address</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" value="<?php echo $global->walletAddress; ?>" placeholder="Your Wallet Address" readonly>
                                        </div>
                                    </div>
                                    <div class="clearfix form-group margin-top-10">
                                        <button type="button" class="btn green" id="btnSetAddress" <?php echo (!empty($global->walletAddress)?'disabled':''); ?>>Set My Address</button>
                                    </div>
                                </form>
                            </div>
                            <!-- END PERSONAL INFO TAB -->
                            <!-- PRIVACY SETTINGS TAB -->
                            <div class="tab-pane" id="tab_1_2">
                                <div class="note note-info">
                                    <h4 class="block">Social Connections</h4>
                                    <p> Here you can link your social media websites to this account. Currently we support just facebook, twitter and google+ are coming soon. </p>
                                </div>
                                <div class="well">
                                    <form role="form" action="#" id="formSocial">
                                        <div class="alert alert-danger display-hide">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                        <div class="alert alert-success display-hide">
                                            <button class="close" data-close="alert"></button> Your profile is successfully updated! </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="control-label">Facebook Connect</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <button type="button" class="btn btn-success btn-facebook">Connect</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div id="profile_facebook"></div>
                                </div>
                            </div>
                            <!-- END PRIVACY SETTINGS TAB -->
                            <!-- PRIVACY SETTINGS TAB -->
                            <div class="tab-pane" id="tab_1_3">
                                <div class="note note-info">
                                    <h4 class="block">Account Type</h4>
                                    <p> Please select your type of organisation. </p>
                                </div>
                                <form role="form" action="" id="formAccountType">
                                    <div class="alert alert-danger display-hide">
                                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                    <div class="alert alert-success display-hide">
                                        <button class="close" data-close="alert"></button> Your profile is successfully updated! </div>
                                    <div class="form-group clearfix">
                                        <label class="col-md-3 control-label">Select Account Type</label>
                                        <div class="col-md-5">
                                            <select name="selectAccountType" id="selectAccountType" class="form-control account-type">
                                                <option value="institute">Institute</option>
                                                <option value="organization">Organization</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="well">
                                        <div class="form-group clearfix">
                                            <h4 class="col-md-3 control-label">Institute Terminology</h4>
                                        </div>
                                        <div class="clearfix">
                                            <label class="col-md-3 control-label">Singular</label>
                                            <div class="col-md-3">
                                                <input type="text" id="institute_single" class="form-control" value="Institute" placeholder="Institute Terminology Single">
                                            </div>
                                            <label class="col-md-3 control-label">Plural</label>
                                            <div class="col-md-3">
                                                <input type="text" id="institute_plural" class="form-control" value="Institutes" placeholder="Institute Terminology Plural">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="well">
                                        <div class="form-group clearfix">
                                            <h4 class="col-md-3 control-label">Instructor Terminology</h4>
                                        </div>
                                        <div class="clearfix">
                                            <label class="col-md-3 control-label">Singular</label>
                                            <div class="col-md-3">
                                                <input type="text" id="instructor_single" class="form-control" value="Instructor" placeholder="Instructor Terminology Single">
                                            </div>
                                            <label class="col-md-3 control-label">Plural</label>
                                            <div class="col-md-3">
                                                <input type="text" id="instructor_plural" class="form-control" value="Instructors" placeholder="Instructor Terminology Plural">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="well">
                                        <div class="form-group clearfix">
                                            <h4 class="col-md-3 control-label">Student Terminology</h4>
                                        </div>
                                        <div class="clearfix">
                                            <label class="col-md-3 control-label">Singular</label>
                                            <div class="col-md-3">
                                                <input type="text" id="student_single" class="form-control" value="Student" placeholder="Student Terminology Single">
                                            </div>
                                            <label class="col-md-3 control-label">Plural</label>
                                            <div class="col-md-3">
                                                <input type="text" id="student_plural" class="form-control" value="Students" placeholder="Student Terminology Plural">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="well">
                                        <div class="form-group clearfix">
                                            <h4 class="col-md-3 control-label">Parent Terminology</h4>
                                        </div>
                                        <div class="clearfix">
                                            <label class="col-md-3 control-label">Singular</label>
                                            <div class="col-md-3">
                                                <input type="text" id="parent_single" class="form-control" value="Parent" placeholder="Parent Terminology Single">
                                            </div>
                                            <label class="col-md-3 control-label">Plural</label>
                                            <div class="col-md-3">
                                                <input type="text" id="parent_plural" class="form-control" value="Parents" placeholder="Parent Terminology Plural">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="well">
                                        <div class="form-group clearfix">
                                            <h4 class="col-md-3 control-label">Course Terminology</h4>
                                        </div>
                                        <div class="clearfix">
                                            <label class="col-md-3 control-label">Singular</label>
                                            <div class="col-md-3">
                                                <input type="text" id="course_single" class="form-control" value="Course" placeholder="Course Terminology Single">
                                            </div>
                                            <label class="col-md-3 control-label">Plural</label>
                                            <div class="col-md-3">
                                                <input type="text" id="course_plural" class="form-control" value="Courses" placeholder="Course Terminology Plural">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="well">
                                        <div class="form-group clearfix">
                                            <h4 class="col-md-3 control-label">Subject Terminology</h4>
                                        </div>
                                        <div class="clearfix">
                                            <label class="col-md-3 control-label">Singular</label>
                                            <div class="col-md-3">
                                                <input type="text" id="subject_single" class="form-control" value="Subject" placeholder="Subject Terminology Single">
                                            </div>
                                            <label class="col-md-3 control-label">Plural</label>
                                            <div class="col-md-3">
                                                <input type="text" id="subject_plural" class="form-control" value="Subjects" placeholder="Subject Terminology Plural">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="well">
                                        <div class="form-group clearfix">
                                            <h4 class="col-md-3 control-label">Syllabus Terminology</h4>
                                        </div>
                                        <div class="clearfix">
                                            <label class="col-md-3 control-label">Singular</label>
                                            <div class="col-md-3">
                                                <input type="text" id="syllabus_single" class="form-control" value="Syllabus" placeholder="Syllabus Terminology Single">
                                            </div>
                                            <label class="col-md-3 control-label">Plural</label>
                                            <div class="col-md-3">
                                                <input type="text" id="syllabus_plural" class="form-control" value="Syllabus" placeholder="Syllabus Terminology Plural">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="margin-top-10">
                                        <button type="submit" class="btn green" id="btnAccountType">Save</button>
                                    </div>
                                </form>
                            </div>
                            <!-- END PRIVACY SETTINGS TAB -->
                            <!-- PERSONAL DATA SETTINGS TAB -->
                            <div class="tab-pane" id="tab_1_4">
                                <div class="note note-info">
                                    <h4 class="block">Personal Data</h4>
                                    <p> Delete Personal Data. </p>
                                </div>
                                <div class="well">
                                    <a href="javascript:;" id="btnDeletePersonalData" class="btn red">Delete my personal data</a>
                                    <a href="javascript:;" id="btnDeleteAccount" class="btn red">Delete my account</a>
                                </div>
                            </div>
                            <!-- END PERSONAL DATA SETTINGS TAB -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->