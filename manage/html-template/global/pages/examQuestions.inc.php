<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
        <?php require_once('html-template/'.$userRole.'/includes/header-exam.php'); ?>
		<!-- END PAGE HEADER-->
		<div class="row">
            <div class="col-md-4">
                <div class="portlet light portlet-fit no-space">
                    <div class="portlet-body no-space">
                        <div class="mt-element-list">
                            <div class="mt-list-head list-default ext-1 green-jungle">
                                <div class="row">
                                    <div class="col-xs-8">
                                        <div class="list-head-title-container">
                                            <h3 class="list-title uppercase sbold">Multiple Choice Question</h3>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="list-head-summary-container">
                                            <div class="list-pending">
                                                <div class="list-count badge badge-default ">+3</div>
                                                <div class="list-label">Correct Ans</div>
                                            </div>
                                            <div class="list-done">
                                                <div class="list-count badge badge-default last">-2</div>
                                                <div class="list-label">Wrong Ans</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-list-container list-default ext-1">
                                <div class="mt-list-title uppercase">List of questions
                                    <span class="badge badge-default pull-right bg-hover-green-jungle">
                                        <a class="font-white" href="javascript:;">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </span>
                                </div>
                                <ul>
                                    <li class="mt-list-item done">
                                        <div class="list-icon-container">
                                            <a href="javascript:;">
                                                <i class="icon-check"></i>
                                            </a>
                                        </div>
                                        <div class="list-datetime"> <a href="javascript:void(0)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> </div>
                                        <div class="list-item-content">
                                            <h3 class="uppercase">
                                                <a href="javascript:;">MCQ</a>
                                            </h3>
                                            <p class="text-overflow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A eaque, quasi itaque soluta voluptatum, totam minus accusamus, facere nesciunt illo ea velit odio accusantium beatae quae mollitia voluptatibus inventore aperiam.</p>
                                        </div>
                                    </li>
                                    <li class="mt-list-item">
                                        <div class="list-icon-container">
                                            <a href="javascript:;">
                                                <i class="icon-close"></i>
                                            </a>
                                        </div>
                                        <div class="list-datetime"> <a href="javascript:void(0)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> </div>
                                        <div class="list-item-content">
                                            <h3 class="uppercase">
                                                <a href="javascript:;">MCQ</a>
                                            </h3>
                                            <p class="text-overflow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis eligendi quia non, doloremque aspernatur iusto est esse molestiae dolores neque illum consequuntur sit, quas officia provident sequi facere vitae optio!</p>
                                        </div>
                                    </li>
                                    <li class="mt-list-item">
                                        <div class="list-icon-container">
                                            <a href="javascript:;">
                                                <i class="icon-close"></i>
                                            </a>
                                        </div>
                                        <div class="list-datetime"> <a href="javascript:void(0)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> </div>
                                        <div class="list-item-content">
                                            <h3 class="uppercase">
                                                <a href="javascript:;">MCQ</a>
                                            </h3>
                                            <p class="text-overflow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus tempora nemo sit consequuntur facilis molestiae, eveniet quas animi, inventore blanditiis culpa, repellat qui, aliquid officiis veniam nesciunt voluptas similique soluta.</p>
                                        </div>
                                    </li>
                                    <li class="mt-list-item done">
                                        <div class="list-icon-container">
                                            <a href="javascript:;">
                                                <i class="icon-check"></i>
                                            </a>
                                        </div>
                                        <div class="list-datetime"> <a href="javascript:void(0)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> </div>
                                        <div class="list-item-content">
                                            <h3 class="uppercase">
                                                <a href="javascript:;">MCQ</a>
                                            </h3>
                                            <p class="text-overflow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut explicabo eligendi nulla magni qui aspernatur! Minus in eum, nam quidem totam voluptatem animi ipsam mollitia quo! Aliquid a culpa repellat.</p>
                                        </div>
                                    </li>
                                    <li class="mt-list-item">
                                        <div class="list-icon-container">
                                            <a href="javascript:;">
                                                <i class="icon-close"></i>
                                            </a>
                                        </div>
                                        <div class="list-datetime"> <a href="javascript:void(0)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> </div>
                                        <div class="list-item-content">
                                            <h3 class="uppercase">
                                                <a href="javascript:;">MCQ</a>
                                            </h3>
                                            <p class="text-overflow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem odio repellendus, hic expedita earum iste nemo ullam odit, cumque eligendi possimus. Explicabo illum nihil dolorum aliquid voluptatibus laborum assumenda minus.</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .col-md-4 -->
            <div class="col-md-8">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Add Question </div>
                    </div>
                    <div class="portlet-body form">
                        <form action="#" id="form-username">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label">Question :</label>
                                    <textarea class="ckeditor form-control" name="editor1" rows="6"></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Only one answer option is correct :</label>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <input type="radio">
                                                                <span></span>
                                                            </span>
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <input type="radio">
                                                                <span></span>
                                                            </span>
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <input type="radio">
                                                                <span></span>
                                                            </span>
                                                            <input type="text" class="form-control">
                                                            <span class="input-group-btn">
                                                                <button class="btn red" type="button"><i class="fa fa-trash"></i></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <input type="radio">
                                                                <span></span>
                                                            </span>
                                                            <input type="text" class="form-control">
                                                            <span class="input-group-btn">
                                                                <button class="btn red" type="button"><i class="fa fa-trash"></i></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <button class="btn btn-info"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Difficulty Level :</label>
                                            <select name="" id="" class="form-control">
                                                <option value="1">Very Easy</option>
                                                <option value="2">Easy</option>
                                                <option value="3">Fair</option>
                                                <option value="4">Difficult</option>
                                                <option value="5">Very Difficult</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Learning Objectives :</label>
                                            <div class="btn-block">
                                                <input type="text" value="Amsterdam,Washington,Sydney,Beijing,Cairo" data-role="tagsinput">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Explanation :</label>
                                    <textarea class="form-control autosizeme" name="editor2" rows="6"></textarea>
                                </div>
                            </div>
                            <div class="form-actions right">
                                <a href="javascript:;" class="btn dark">
                                    <i class="fa fa-check"></i> Submit</a>
                                <a href="javascript:;" class="btn btn-outline grey-salsa">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->