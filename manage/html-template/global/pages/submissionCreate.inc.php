<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
        <?php require_once('html-template/'.$userRole.'/includes/header-exam.php'); ?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>New Submission </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form portlet-form">
                        <!-- BEGIN FORM-->
                        <form id="formCreateExam" class="horizontal-form">
                            <div class="form-body">
                                <h3 class="form-section">Basic Information</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Title of the test</label>
                                            <input type="text" id="titleName" class="form-control" placeholder="Title">
                                            <span class="help-block"> Enter title of the test. </span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Section <i class="tooltips fa fa-info-circle" data-original-title="Please select the section name under which the Submission would be listed. You can make the Submission as Independent, if you do not want it to be part of any section" data-placement="right"></i></label>
                                            <select id="chapterSelect" class="form-control" data-placeholder="Choose a Section" tabindex="1">
                                                <option value="">Select Section</option>
                                            </select>
                                            <span class="help-block"> Select section. </span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">No of attempts</label>
                                            <input type="text" id="noOfAttempts" class="form-control" placeholder="0">
                                            <span class="help-block"> Enter no of attempts a <?php echo $global->terminology["student_single"]; ?> is allowed. </span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <h3 class="form-section">Submission Settings</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Start date of test</label>
                                            <div id="startDate" class="input-group date">
                                                <input type="text" size="16" readonly class="form-control">
                                                <span class="input-group-btn">
                                                    <button class="btn default date-set" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">End date of test (optional)</label>
                                            <div id="endDate" class="input-group date">
                                                <input type="text" size="16" readonly class="form-control">
                                                <span class="input-group-btn">
                                                    <button class="btn default date-set" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <h3 class="form-section">Submission Access</h3>
                                <div class="form-group">
                                    <div class="mt-radio-inline">
                                        <label class="mt-radio mt-radio-line">
                                            <input type="radio" name="optionAccess" id="optionAccess1" value="public" class="js-access" checked=""> Public
                                            <span></span>
                                        </label>
                                        <label class="mt-radio mt-radio-line">
                                            <input type="radio" name="optionAccess" id="optionAccess2" value="private" class="js-access"> Private
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="js-private-block hide">
                                    <div class="note note-info">
                                        <p>Select <?php echo $global->terminology["student_single"]; ?> Groups who can access the submission. Only groups can be selected not individual <?php echo $global->terminology["student_plural"]; ?>.</p>
                                    </div>
                                    <div class="js-students"></div>
                                </div>
                                <input type="hidden" id="switchFlag" value="0">
                                <input type="hidden" id="courseStartDate">
                                <input type="hidden" id="courseEndDate">
                            </div>
                            <div class="form-actions right">
                                <button type="button" class="btn default">Cancel</button>
                                <button type="submit" class="btn blue">
                                    <i class="fa fa-check"></i> Save</button>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->