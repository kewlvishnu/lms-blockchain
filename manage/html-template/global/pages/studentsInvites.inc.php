<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<h1 class="page-title"> <?php echo $global->terminology["student_plural"]; ?> </h1>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo $sitepathManage; ?>">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<i class="icon-list"></i>
					<a href="<?php echo $sitepathManageStudents; ?>"><?php echo $global->terminology["student_plural"]; ?></a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<span><?php echo $global->terminology["student_single"]; ?> Invites</span>
				</li>
			</ul>
			<div class="page-toolbar">
				<div class="btn-group pull-right">
					<button type="button" data-toggle="modal" data-target="#modalInviteStudentsToCourse" class="btn btn-fit-height blue"> Invite </button>
					<a href="<?php echo $sitepathManageStudents; ?>" class="btn btn-fit-height green-jungle"> <?php echo $global->terminology["student_plural"]; ?> List </a>
					<a href="<?php echo $sitepathManageStudents; ?>transfer" class="btn btn-fit-height purple"> Transfer <?php echo $global->terminology["student_plural"]; ?> </a>
				</div>
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light ">
					<div class="portlet-title tabbable-line">
						<div class="caption caption-md">
							<i class="icon-globe theme-font hide"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo $global->terminology["student_single"]; ?> Invites</span>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
							<thead>
								<tr>
									<th> # </th>
									<?php /*<th> echo $global->terminology["course_single"]; ID </th>*/ ?>
									<th> <?php echo $global->terminology["course_single"]; ?> </th>
									<th> Name </th>
									<th> Email </th>
									<th> Contact No </th>
									<th> Enrollment Type </th>
									<th> Invited </th>
									<th> Registered </th>
									<th> Status </th>
									<th> Actions </th>
								</tr>
							</thead>
							<tbody>
								<!-- <tr class="odd gradeX">
									<td>
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="checkboxes" value="1" />
											<span></span>
										</label>
									</td>
									<td>1</td>
									<td>C0003</td>
									<td>Naval Engineering 2016</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>9876543210</td>
									<td>Course Key</td>
									<td><span class="label label-warning"><i class="fa fa-info-circle"></i> Pending</span></td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"> Resend</a>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm red"> Reject</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>
										<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
											<input type="checkbox" class="checkboxes" value="1" />
											<span></span>
										</label>
									</td>
									<td>1</td>
									<td>C0003</td>
									<td>Naval Engineering 2016</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>9876543210</td>
									<td>Course Key</td>
									<td><span class="label bg-green-dark"><i class="fa fa-check-circle"></i> Accepted</span></td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"> Resend</a>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm red"> Reject</a>
									</td>
								</tr> -->
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->