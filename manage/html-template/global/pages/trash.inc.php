<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h1 class="page-title"> Trash
            <small>all your deleted subjects, content and exams go here</small>
        </h1>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo $sitepathManageInstitute; ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Trash</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-sm-12" id="listSections">
                <div class="portlet box portlet-course blue-soft">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-book"></i> Trashed Items </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                            <a href="" class="fullscreen" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body no-space js-list">
                        <table class="table table-striped table-bordered table-hover order-column no-margin">
                            <thead>
                                <tr>
                                    <th width="60%"> Title </th>
                                    <th width="20%"> Type </th>
                                    <th width="20%"> Restore </th>
                                </tr>
                            </thead>
                            <tbody class="js-trash-list"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- <div class="col-sm-12" id="deletedExams">
                <div class="portlet box portlet-course blue-soft">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-book"></i> Deleted Exams </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                            <a href="" class="fullscreen" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body no-space js-list"></div>
                </div>
            </div>
            <div class="col-sm-12" id="deletedAssignments">
                <div class="portlet box portlet-course blue-soft">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-book"></i> Deleted Assignments </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                            <a href="" class="fullscreen" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body no-space js-list"></div>
                </div>
            </div>
            <div class="col-sm-12" id="deletedSubjectiveExams">
                <div class="portlet box portlet-course blue-soft">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-book"></i> Deleted Subjective Exams </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                            <a href="" class="fullscreen" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body no-space js-list"></div>
                </div>
            </div>
            <div class="col-sm-12" id="deletedManualExams">
                <div class="portlet box portlet-course blue-soft">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-book"></i> Deleted Manual Exams </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                            <a href="" class="fullscreen" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body no-space js-list"></div>
                </div>
            </div>
            <div class="col-sm-12" id="deletedSubmissions">
                <div class="portlet box portlet-course blue-soft">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-book"></i> Deleted Submissions </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                            <a href="" class="fullscreen" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body no-space js-list"></div>
                </div>
            </div> -->
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->