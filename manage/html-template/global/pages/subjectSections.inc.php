<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<?php
			require_once('html-template/'.$userRole.'/includes/header-subject.php');
		?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light ">
					<div class="portlet-title tabbable-line">
						<div class="caption caption-md">
							<i class="icon-globe theme-font hide"></i>
							<span class="caption-subject font-blue-madison bold uppercase">Sections</span>
						</div>
                        <div class="actions">
                            <a href="<?php echo $sitepathManageSubjects.$subjectId; ?>" class="btn btn-fit-height green btn-course"> Back</a>
                        </div>
					</div>
					<div class="portlet-body">
                        <div class="dd list-sections" id="content">
                            <ol class="dd-list">
                                <li class="dd-item section-heading" data-id="9">
                                    <div class="dd-handle"> Add Section <button class="btn green btn-sm btn-section-title no-drag" data-toggle="modal" data-action="new"><i class="fa fa-plus"></i></button></div>
                                </li>
                                <?php /*
                                <li class="dd-item section-heading" data-id="1">
                                    <div class="dd-handle bg-green bg-font-green"> Section 1: Lecture 1 <a href="javascript:;" class="btn-section-title bg-font-green-jungle no-drag" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i></a> <button class="btn btn-info btn-sm btn-content-title no-drag" data-toggle="modal" data-action="new"><i class="fa fa-plus"></i> Add Lecture</button></div>
                                </li>
                                <li class="dd-item section-heading" data-id="2">
                                    <div class="dd-handle bg-green bg-font-green"> Section 2: Lecture 2 <a href="javascript:;" class="btn-section-title bg-font-green-jungle no-drag" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i></a> <button class="btn btn-info btn-sm btn-content-title no-drag" data-toggle="modal" data-action="new"><i class="fa fa-plus"></i> Add Lecture</button></div>
                                    <ol class="dd-list">
                                        <li class="dd-item section-content" data-id="3">
                                            <div class="dd-handle bg-green-jungle bg-font-green-jungle"> 2.1 <span class="content-title">Electronics Fragmentation</span> <a href="javascript:;" class="btn-content-title bg-font-green-jungle no-drag" data-toggle="modal"><i class="fa fa-edit"></i></a> <span class="pull-right"><a class="collapsible bg-font-green-jungle collapsed no-drag" role="button" data-toggle="collapse" href="#ddItem21" aria-expanded="false" aria-controls="ddItem21"><i class="fa fa-toggle-down"></i></span></a></div>
											<div class="collapse" id="ddItem21">
												<div class="well">
													<div class="row">
														<div class="col-md-9">
															<h3 class="font-yellow"><i class="fa fa-youtube"></i> Youtube Link</h3>
															<p class="text-overflow">https://www.youtube.com/watch?v=Qd32ERYKUM4</p>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm blue"><i class="fa fa-eye"></i> Preview</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm green-jungle"><i class="fa fa-cloud-upload"></i> Unpublish</button>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm green btn-content" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i> Edit</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</button>
																	</div>
																</div>
															</div>
															<div class="clearfix">
																<h4 class="downloadable pull-right">
																	Downloadable:</b> <input type="checkbox" class="d-checkbox">
																</h4>
															</div>
														</div>
													</div>
													<div class="list-additional">
														<h4>Content Description :</h4>
                                                		<a href="javascript:;" id="description" data-type="textarea" data-pk="1" data-placeholder="Your description here..." data-original-title="Enter description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo aspernatur, perferendis, facere placeat distinctio nam repellendus eos voluptates similique a incidunt quo hic obcaecati ipsum vitae error cupiditate harum in!</a>
														<div class="js-download-text cursor-pointer">
															<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo aspernatur, perferendis, facere placeat distinctio nam repellendus eos voluptates similique a incidunt quo hic obcaecati ipsum vitae error cupiditate harum in!</p>
														</div>
														<div class="js-download-update">
															<div class="form-group">
																<textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
															</div>
															<div class="form-group">
																<button class="btn green">Update</button>
																<button class="btn red">Cancel</button>
															</div>
														</div>
													</div>
													<div class="list-additional">
														<h4>Supplementary files :</h4>
														<ul class="list-group">
															<li class="list-group-item">
																<div class="row">
																	<div class="col-md-8 text-overflow">
																		_1427891948Inventory_management_under_uncertainty__safety_inventory.pdf
																	</div>
																	<div class="col-md-4 text-right">
																		<a href="javascript:;" class="btn btn-info btn-sm"><i class="fa fa-download"></i></a> <a href="javascript:;" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
																	</div>
																</div>
															</li>
															<li class="list-group-item">
																<div class="row">
																	<div class="col-md-8 text-overflow">
																		_1427891948Inventory_management_under_uncertainty__safety_inventory.pdf
																	</div>
																	<div class="col-md-4 text-right">
																		<a href="javascript:;" class="btn btn-info btn-sm"><i class="fa fa-download"></i></a> <a href="javascript:;" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
																	</div>
																</div>
															</li>
															<li class="list-group-item">
																<div class="row">
																	<div class="col-md-8 text-overflow">
																		_1427891948Inventory_management_under_uncertainty__safety_inventory.pdf
																	</div>
																	<div class="col-md-4 text-right">
																		<a href="javascript:;" class="btn btn-info btn-sm"><i class="fa fa-download"></i></a> <a href="javascript:;" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
																	</div>
																</div>
															</li>
															<li class="list-group-item">
																<div class="row">
																	<div class="col-md-8 text-overflow">
																		_1427891948Inventory_management_under_uncertainty__safety_inventory.pdf
																	</div>
																	<div class="col-md-4 text-right">
																		<a href="javascript:;" class="btn btn-info btn-sm"><i class="fa fa-download"></i></a> <a href="javascript:;" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
																	</div>
																</div>
															</li>
														</ul>
													</div>
													<hr>
													<div class="row">
														<div class="col-md-12">
															<button class="btn btn-success" data-target="#modalAddSupplementaryMaterial" data-toggle="modal">Add Supplementary Material</button>
															<button class="btn btn-success">Add Description</button>
														</div>
													</div>
												</div>
											</div>
										</li>
                                        <li class="dd-item section-content" data-id="4">
                                            <div class="dd-handle bg-green-jungle bg-font-green-jungle"> 2.2 <span class="content-title">Electronics Fragmentation</span> <a href="javascript:;" class="btn-content-title bg-font-green-jungle no-drag" data-toggle="modal"><i class="fa fa-edit"></i></a> <span class="pull-right"><a class="collapsible bg-font-green-jungle collapsed no-drag" role="button" data-toggle="collapse" href="#ddItem22" aria-expanded="false" aria-controls="ddItem22"><i class="fa fa-toggle-down"></i></span></a></div>
                                        	<div class="collapse" id="ddItem22">
                                            	<div class="well">
													<div class="row">
														<div class="col-md-9">
															<h3 class="font-yellow"><i class="fa fa-vimeo"></i> Vimeo Link</h3>
															<p class="text-overflow">https://vimeo.com/190666965</p>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm blue"><i class="fa fa-eye"></i> Preview</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-warning"><i class="fa fa-cloud-upload"></i> Publish</button>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm green btn-content" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i> Edit</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</button>
																	</div>
																</div>
															</div>
															<div class="clearfix">
																<h4 class="downloadable pull-right">
																	Downloadable:</b> <input type="checkbox" class="d-checkbox">
																</h4>
															</div>
														</div>
													</div>
													<div class="list-additional">
														<h4>Content Description :</h4>
														<div class="js-download-update">
															<div class="form-group">
																<textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
															</div>
															<div class="form-group">
																<button class="btn green">Update</button>
																<button class="btn red">Cancel</button>
															</div>
														</div>
													</div>
													<hr>
													<div class="row">
														<div class="col-md-12">
															<button class="btn btn-success">Add Supplementary Material</button>
															<button class="btn btn-success">Add Description</button>
														</div>
													</div>
												</div>
											</div>
                                        </li>
                                        <li class="dd-item section-content" data-id="5">
                                            <div class="dd-handle bg-green-jungle bg-font-green-jungle"> 2.3 <span class="content-title">Electronics Fragmentation</span> <a href="javascript:;" class="btn-content-title  bg-font-green-jungle no-drag" data-toggle="modal"><i class="fa fa-edit"></i></a> <span class="pull-right"><a class="collapsible bg-font-green-jungle collapsed no-drag" role="button" data-toggle="collapse" href="#ddItem23" aria-expanded="false" aria-controls="ddItem23"><i class="fa fa-toggle-down"></i></span></a></div>
                                        	<div class="collapse" id="ddItem23">
                                            	<div class="well">
													<div class="row">
														<div class="col-md-9">
															<h3 class="font-yellow"><i class="fa fa-play-circle-o"></i> Video Link</h3>
															<p class="text-overflow">20150829_190130.mp4</p>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm blue"><i class="fa fa-eye"></i> Preview</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-warning"><i class="fa fa-cloud-upload"></i> Publish</button>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm green btn-content" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i> Edit</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</button>
																	</div>
																</div>
															</div>
															<div class="clearfix">
																<h4 class="downloadable pull-right">
																	Downloadable:</b> <input type="checkbox" class="d-checkbox">
																</h4>
															</div>
														</div>
													</div>
													<hr>
													<div class="row">
														<div class="col-md-12">
															<button class="btn btn-success">Add Supplementary Material</button>
															<button class="btn btn-success">Add Description</button>
														</div>
													</div>
												</div>
											</div>
                                        </li>
                                        <li class="dd-item section-content" data-id="6">
                                            <div class="dd-handle bg-green-jungle bg-font-green-jungle"> 2.4 <span class="content-title">Electronics Fragmentation</span> <a href="javascript:;" class="btn-content-title  bg-font-green-jungle no-drag" data-toggle="modal"><i class="fa fa-edit"></i></a> <span class="pull-right"><a class="collapsible bg-font-green-jungle collapsed no-drag" role="button" data-toggle="collapse" href="#ddItem24" aria-expanded="false" aria-controls="ddItem24"><i class="fa fa-toggle-down"></i></span></a></div>
                                        	<div class="collapse" id="ddItem24">
                                            	<div class="well">
													<div class="row">
														<div class="col-md-9">
															<h3 class="font-yellow"><i class="fa fa-book"></i> Text</h3>
															<p class="text-overflow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et aliquam tempora earum dolore a mollitia blanditiis, iusto repellendus nostrum rem laborum. Magni, necessitatibus, rerum. Neque, odit, in! Hic, beatae, enim.</p>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm blue"><i class="fa fa-eye"></i> Preview</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-warning"><i class="fa fa-cloud-upload"></i> Publish</button>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm green btn-content" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i> Edit</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</button>
																	</div>
																</div>
															</div>
															<div class="clearfix">
																<h4 class="downloadable pull-right">
																	Downloadable:</b> <input type="checkbox" class="d-checkbox">
																</h4>
															</div>
														</div>
													</div>
													<hr>
													<div class="row">
														<div class="col-md-12">
															<button class="btn btn-success">Add Supplementary Material</button>
															<button class="btn btn-success">Add Description</button>
														</div>
													</div>
												</div>
											</div>
                                        </li>
                                        <li class="dd-item section-content" data-id="7">
                                            <div class="dd-handle bg-green-jungle bg-font-green-jungle"> 2.5 <span class="content-title">Electronics Fragmentation</span> <a href="javascript:;" class="btn-content-title  bg-font-green-jungle no-drag" data-toggle="modal"><i class="fa fa-edit"></i></a> <span class="pull-right"><a class="collapsible bg-font-green-jungle collapsed no-drag" role="button" data-toggle="collapse" href="#ddItem25" aria-expanded="false" aria-controls="ddItem25"><i class="fa fa-toggle-down"></i></span></a></div>
                                        	<div class="collapse" id="ddItem25">
                                            	<div class="well">
													<div class="row">
														<div class="col-md-9">
															<h3 class="font-yellow"><i class="fa fa-download"></i> Downloadable</h3>
															<p class="text-overflow">2016-08-12.png</p>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm blue"><i class="fa fa-eye"></i> Preview</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-warning"><i class="fa fa-cloud-upload"></i> Publish</button>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm green btn-content" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i> Edit</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</button>
																	</div>
																</div>
															</div>
															<div class="clearfix">
																<h4 class="downloadable pull-right">
																	Downloadable:</b> <input type="checkbox" class="d-checkbox" checked="">
																</h4>
															</div>
														</div>
													</div>
													<hr>
													<div class="row">
														<div class="col-md-12">
															<button class="btn btn-success">Add Supplementary Material</button>
															<button class="btn btn-success">Add Description</button>
														</div>
													</div>
												</div>
											</div>
                                        </li>
                                        <li class="dd-item section-content" data-id="8">
                                            <div class="dd-handle bg-green-jungle bg-font-green-jungle"> 2.6 <span class="content-title">Electronics Fragmentation</span> <a href="javascript:;" class="btn-content-title  bg-font-green-jungle no-drag" data-toggle="modal"><i class="fa fa-edit"></i></a> <span class="pull-right"><a class="collapsible bg-font-green-jungle collapsed no-drag" role="button" data-toggle="collapse" href="#ddItem26" aria-expanded="false" aria-controls="ddItem26"><i class="fa fa-toggle-down"></i></span></a></div>
                                        	<div class="collapse" id="ddItem26">
                                            	<div class="well">
													<div class="row">
														<div class="col-md-9">
															<h3 class="font-yellow"><i class="fa fa-file-pdf-o"></i> PDF/Presentation</h3>
															<p class="text-overflow">Prof._Protano_s_BEST_Marketing_Tools.pdf-_subject_uid=26887840&w=AABZQK4IRXsugQKxLh55Src1m2ntENWRDRuB4jfN_O23HA&get_preview=1&rams_ui=1 (1).pdf</p>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm blue"><i class="fa fa-eye"></i> Preview</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-warning"><i class="fa fa-cloud-upload"></i> Publish</button>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm green btn-content" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i> Edit</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</button>
																	</div>
																</div>
															</div>
															<div class="clearfix">
																<h4 class="downloadable pull-right">
																	Downloadable:</b> <input type="checkbox" class="d-checkbox">
																</h4>
															</div>
														</div>
													</div>
													<hr>
													<div class="row">
														<div class="col-md-12">
															<button class="btn btn-success">Add Supplementary Material</button>
															<button class="btn btn-success">Add Description</button>
														</div>
													</div>
												</div>
											</div>
                                        </li>
                                        <li class="dd-item section-content" data-id="9">
                                            <div class="dd-handle bg-yellow-lemon bg-font-yellow-lemon"> 2.7 <span class="content-title">Electronics Fragmentation</span>  <span class="pull-right"><button class="btn btn-warning btn-xs btn-content no-drag" data-toggle="modal" data-action="new"><i class="fa fa-plus"></i> Add Content</button></span></div>
                                        </li>
                                    </ol>
                                </li>
                                <li class="dd-item section-heading" data-id="9">
                                    <div class="dd-handle bg-green bg-font-green"> Section 3: Lecture 3 <a href="javascript:;" class="btn-section-title bg-font-green-jungle no-drag" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i></a> <button class="btn btn-info btn-sm btn-content-title no-drag" data-toggle="modal" data-action="new"><i class="fa fa-plus"></i> Add Lecture</button></div>
                                </li>
                                <li class="dd-item section-heading" data-id="10">
                                    <div class="dd-handle bg-green bg-font-green"> Section 4: Lecture 4 <a href="javascript:;" class="btn-section-title bg-font-green-jungle no-drag" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i></a> <button class="btn btn-info btn-sm btn-content-title no-drag" data-toggle="modal" data-action="new"><i class="fa fa-plus"></i> Add Lecture</button></div>
                                    <ol class="dd-list">
                                        <li class="dd-item section-content" data-id="11">
                                            <div class="dd-handle bg-green-jungle bg-font-green-jungle"> 4.1 Electromagnetic Induction <a href="javascript:;" class="btn-content-title bg-font-green-jungle no-drag" data-toggle="modal"><i class="fa fa-edit"></i></a> <span class="pull-right"><a class="collapsible bg-font-green-jungle collapsed no-drag" role="button" data-toggle="collapse" href="#ddItem41" aria-expanded="false" aria-controls="ddItem41"><i class="fa fa-toggle-down"></i></span></a></div>
                                            <div class="collapse" id="ddItem41">
                                            	<div class="well">
													<div class="row">
														<div class="col-md-9">
															<h3 class="font-yellow"><i class="fa fa-youtube"></i> Youtube Link</h3>
															<p class="text-overflow">https://www.youtube.com/watch?v=Qd32ERYKUM4</p>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm blue"><i class="fa fa-eye"></i> Preview</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm green-jungle"><i class="fa fa-cloud-upload"></i> Unpublish</button>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm green btn-content" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i> Edit</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</button>
																	</div>
																</div>
															</div>
															<div class="clearfix">
																<h4 class="downloadable pull-right">
																	Downloadable:</b> <input type="checkbox" class="d-checkbox">
																</h4>
															</div>
														</div>
													</div>
													<hr>
													<div class="row">
														<div class="col-md-12">
															<button class="btn btn-success">Add Supplementary Material</button>
															<button class="btn btn-success">Add Description</button>
														</div>
													</div>
												</div>
											</div>
                                        </li>
                                        <li class="dd-item section-content" data-id="12">
                                            <div class="dd-handle bg-green-jungle bg-font-green-jungle"> 4.2 Electromagnetic Induction <a href="javascript:;" class="btn-content-title bg-font-green-jungle no-drag" data-toggle="modal"><i class="fa fa-edit"></i></a> <span class="pull-right"><a class="collapsible bg-font-green-jungle collapsed no-drag" role="button" data-toggle="collapse" href="#ddItem42" aria-expanded="false" aria-controls="ddItem42"><i class="fa fa-toggle-down"></i></span></a></div>
                                        	<div class="collapse" id="ddItem42">
                                            	<div class="well">
													<div class="row">
														<div class="col-md-9">
															<h3 class="font-yellow"><i class="fa fa-vimeo"></i> Vimeo Link</h3>
															<p class="text-overflow">https://vimeo.com/190666965</p>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm blue"><i class="fa fa-eye"></i> Preview</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-warning"><i class="fa fa-cloud-upload"></i> Publish</button>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm green btn-content" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i> Edit</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</button>
																	</div>
																</div>
															</div>
															<div class="clearfix">
																<h4 class="downloadable pull-right">
																	Downloadable:</b> <input type="checkbox" class="d-checkbox">
																</h4>
															</div>
														</div>
													</div>
													<hr>
													<div class="row">
														<div class="col-md-12">
															<button class="btn btn-success">Add Supplementary Material</button>
															<button class="btn btn-success">Add Description</button>
														</div>
													</div>
												</div>
											</div>
                                        </li>
                                        <li class="dd-item section-content" data-id="13">
                                            <div class="dd-handle bg-green-jungle bg-font-green-jungle"> 4.3 Electromagnetic Induction <a href="javascript:;" class="btn-content-title bg-font-green-jungle no-drag" data-toggle="modal"><i class="fa fa-edit"></i></a> <span class="pull-right"><a class="collapsible bg-font-green-jungle collapsed no-drag" role="button" data-toggle="collapse" href="#ddItem43" aria-expanded="false" aria-controls="ddItem43"><i class="fa fa-toggle-down"></i></span></a></div>
                                        	<div class="collapse" id="ddItem43">
                                            	<div class="well">
													<div class="row">
														<div class="col-md-9">
															<h3 class="font-yellow"><i class="fa fa-play-circle-o"></i> Video Link</h3>
															<p class="text-overflow">20150829_190130.mp4</p>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm blue"><i class="fa fa-eye"></i> Preview</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-warning"><i class="fa fa-cloud-upload"></i> Publish</button>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm green btn-content" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i> Edit</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</button>
																	</div>
																</div>
															</div>
															<div class="clearfix">
																<h4 class="downloadable pull-right">
																	Downloadable:</b> <input type="checkbox" class="d-checkbox">
																</h4>
															</div>
														</div>
													</div>
													<hr>
													<div class="row">
														<div class="col-md-12">
															<button class="btn btn-success">Add Supplementary Material</button>
															<button class="btn btn-success">Add Description</button>
														</div>
													</div>
												</div>
											</div>
                                        </li>
                                        <li class="dd-item section-content" data-id="14">
                                            <div class="dd-handle bg-green-jungle bg-font-green-jungle"> 4.4 Electromagnetic Induction <a href="javascript:;" class="btn-content-title bg-font-green-jungle no-drag" data-toggle="modal"><i class="fa fa-edit"></i></a> <span class="pull-right"><a class="collapsible bg-font-green-jungle collapsed no-drag" role="button" data-toggle="collapse" href="#ddItem44" aria-expanded="false" aria-controls="ddItem44"><i class="fa fa-toggle-down"></i></span></a></div>
                                        	<div class="collapse" id="ddItem44">
                                            	<div class="well">
													<div class="row">
														<div class="col-md-9">
															<h3 class="font-yellow"><i class="fa fa-book"></i> Text</h3>
															<p class="text-overflow">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et aliquam tempora earum dolore a mollitia blanditiis, iusto repellendus nostrum rem laborum. Magni, necessitatibus, rerum. Neque, odit, in! Hic, beatae, enim.</p>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm blue"><i class="fa fa-eye"></i> Preview</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-warning"><i class="fa fa-cloud-upload"></i> Publish</button>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm green btn-content" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i> Edit</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</button>
																	</div>
																</div>
															</div>
															<div class="clearfix">
																<h4 class="downloadable pull-right">
																	Downloadable:</b> <input type="checkbox" class="d-checkbox">
																</h4>
															</div>
														</div>
													</div>
													<hr>
													<div class="row">
														<div class="col-md-12">
															<button class="btn btn-success">Add Supplementary Material</button>
															<button class="btn btn-success">Add Description</button>
														</div>
													</div>
												</div>
											</div>
                                        </li>
                                        <li class="dd-item section-content" data-id="15">
                                            <div class="dd-handle bg-green-jungle bg-font-green-jungle"> 4.5 Electromagnetic Induction <a href="javascript:;" class="btn-content-title bg-font-green-jungle no-drag" data-toggle="modal"><i class="fa fa-edit"></i></a> <span class="pull-right"><a class="collapsible bg-font-green-jungle collapsed no-drag" role="button" data-toggle="collapse" href="#ddItem45" aria-expanded="false" aria-controls="ddItem45"><i class="fa fa-toggle-down"></i></span></a></div>
                                        	<div class="collapse" id="ddItem45">
                                            	<div class="well">
													<div class="row">
														<div class="col-md-9">
															<h3 class="font-yellow"><i class="fa fa-download"></i> Downloadable</h3>
															<p class="text-overflow">2016-08-12.png</p>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm blue"><i class="fa fa-eye"></i> Preview</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-warning"><i class="fa fa-cloud-upload"></i> Publish</button>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm green btn-content" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i> Edit</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</button>
																	</div>
																</div>
															</div>
															<div class="clearfix">
																<h4 class="downloadable pull-right">
																	Downloadable:</b> <input type="checkbox" class="d-checkbox" checked="">
																</h4>
															</div>
														</div>
													</div>
													<hr>
													<div class="row">
														<div class="col-md-12">
															<button class="btn btn-success">Add Supplementary Material</button>
															<button class="btn btn-success">Add Description</button>
														</div>
													</div>
												</div>
											</div>
                                        </li>
                                        <li class="dd-item section-content" data-id="16">
                                            <div class="dd-handle bg-green-jungle bg-font-green-jungle"> 4.6 Electromagnetic Induction <a href="javascript:;" class="btn-content-title bg-font-green-jungle no-drag" data-toggle="modal"><i class="fa fa-edit"></i></a> <span class="pull-right"><a class="collapsible bg-font-green-jungle collapsed no-drag" role="button" data-toggle="collapse" href="#ddItem46" aria-expanded="false" aria-controls="ddItem46"><i class="fa fa-toggle-down"></i></span></a></div>
                                        	<div class="collapse" id="ddItem46">
                                            	<div class="well">
													<div class="row">
														<div class="col-md-9">
															<h3 class="font-yellow"><i class="fa fa-file-pdf-o"></i> PDF/Presentation</h3>
															<p class="text-overflow">Prof._Protano_s_BEST_Marketing_Tools.pdf-_subject_uid=26887840&w=AABZQK4IRXsugQKxLh55Src1m2ntENWRDRuB4jfN_O23HA&get_preview=1&rams_ui=1 (1).pdf</p>
														</div>
														<div class="col-md-3">
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm blue"><i class="fa fa-eye"></i> Preview</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-warning"><i class="fa fa-cloud-upload"></i> Publish</button>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<div class="btn-group btn-group-justified" role="group">
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm green btn-content" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i> Edit</button>
																	</div>
																	<div class="btn-group" role="group">
																		<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</button>
																	</div>
																</div>
															</div>
															<div class="clearfix">
																<h4 class="downloadable pull-right">
																	Downloadable:</b> <input type="checkbox" class="d-checkbox">
																</h4>
															</div>
														</div>
													</div>
													<hr>
													<div class="row">
														<div class="col-md-12">
															<button class="btn btn-success">Add Supplementary Material</button>
															<button class="btn btn-success">Add Description</button>
														</div>
													</div>
												</div>
											</div>
                                        </li>
                                    </ol>
                                </li>
                                <li class="dd-item section-heading" data-id="9">
                                    <div class="dd-handle"> Add Section <button class="btn green btn-sm btn-section-title no-drag" data-toggle="modal" data-action="new"><i class="fa fa-plus"></i></button></div>
                                </li> */ ?>
                            </ol>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->