<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<h1 class="page-title"> Submission Check - <span id="studentName"></span> - <span>Attempt No <span id="attemptNo">1</span></span> </h1>
		<?php require_once('html-template/'.$userRole.'/includes/header-exam.php'); ?>
		<!-- END PAGE HEADER-->
        <div class="bg-white">
            <div class="note">
                <div class="row">
                    <div class="col-md-8">
                        <p>Please read the answers submitted by <?php echo $global->terminology["student_plural"]; ?> and review/comment them with appropriate marks. After reviewing and marks assigning submission will be automatically marked as checked, if it doesn't click on the <strong>Mark submission as checked</strong> button to mark the submission as checked.</p>
                    </div>
                    <div class="col-md-4">
                        <button class="btn green btn-fit-height btn-block" id="btnAttemptChecked" disabled="">Mark submission as checked</button>
                    </div>
                </div>
            </div>
        </div>
		<div class="row">
			<div class="col-md-12">
                <div id="questionsContainer"></div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->