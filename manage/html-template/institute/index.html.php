<?php
	require_once('includes/header.php');
	require_once('includes/sidebar.php');
	
	switch ($page) {
		case 'home':
			require_once('pages/home.inc.php');
			break;
		/*case 'courses':
			require_once('pages/courses.inc.php');
			break;*/
		case 'courseDetail':
			require_once('pages/courseDetail.inc.php');
			break;
		/*case 'courseAssignSubjects':
			require_once('pages/courseAssignSubjects.inc.php');
			break;*/
		case 'subjectDetail':
			require_once('pages/subjectDetail.inc.php');
			break;
		/*case 'subjectSections':
			require_once('pages/subjectSections.inc.php');
			break;*/
		/*case 'subjectStudents':
			require_once('pages/subjectStudents.inc.php');
			break;*/
		/*case 'subjectAnalytics':
			require_once('pages/subjectAnalytics.inc.php');
			break;*/
		/*case 'examCreate':
			require_once('pages/examCreate.inc.php');
			break;*/
		/*case 'examSettings':
			require_once('pages/examSettings.inc.php');
			break;
		case 'examDetail':
			require_once('pages/examDetail.inc.php');
			break;
		case 'examResult':
			require_once('pages/examResult.inc.php');
			break;
		case 'examResultStudent':
			require_once('pages/examResultStudent.inc.php');
			break;*/
		/*case 'subjectiveExamCreate':
			require_once('pages/subjectiveExamCreate.inc.php');
			break;
		case 'subjectiveExamSettings':
			require_once('pages/subjectiveExamSettings.inc.php');
			break;
		case 'subjectiveExamQuestions':
			require_once('pages/subjectiveExamQuestions.inc.php');
			break;
		case 'subjectiveExamCheck':
			require_once('pages/subjectiveExamCheck.inc.php');
			break;
		case 'subjectiveExamCheckAttempt':
			require_once('pages/subjectiveExamCheckAttempt.inc.php');
			break;
		case 'subjectiveExamResult':
			require_once('pages/subjectiveExamResult.inc.php');
			break;
		case 'subjectiveExamResultStudent':
			require_once('pages/subjectiveExamResultStudent.inc.php');
			break;
		case 'manualExamCreate':
			require_once('pages/manualExamCreate.inc.php');
			break;
		case 'manualExamImport':
			require_once('pages/manualExamImport.inc.php');
			break;
		case 'manualExamResult':
			require_once('pages/manualExamResult.inc.php');
			break;
		case 'subjectQuestions':
			require_once('pages/subjectQuestions.inc.php');
			break;
		case 'subjectSubjectiveQuestions':
			require_once('pages/subjectSubjectiveQuestions.inc.php');
			break;*/
		case 'instructors':
			require_once('pages/instructors.inc.php');
			break;
		case 'instructorsInvite':
			require_once('pages/instructorsInvite.inc.php');
			break;
		case 'instructorsInvitations':
			require_once('pages/instructorsInvitations.inc.php');
			break;
		/*case 'students':
			require_once('pages/students.inc.php');
			break;
		case 'studentsInvites':
			require_once('pages/studentsInvites.inc.php');
			break;
		case 'studentsTransfer':
			require_once('pages/studentsTransfer.inc.php');
			break;*/
		/*case 'courseKeys':
			require_once('pages/courseKeys.inc.php');
			break;*/
		case 'profile':
			require_once('pages/profile.inc.php');
			break;
		case 'notificationsActivity':
			require_once('pages/notificationsActivity.inc.php');
			break;
		case 'notificationsChat':
			require_once('pages/notificationsChat.inc.php');
			break;
		/*case 'settings':
			require_once('pages/settings.inc.php');
			break;*/
		/*case 'portfolios':
			require_once('pages/portfolios.inc.php');
			break;
		case 'portfolio':
			require_once('pages/portfolio.inc.php');
			break;*/
		default:
			echo "There was some error";
			break;
	}
	require_once('includes/footer.php');
?>