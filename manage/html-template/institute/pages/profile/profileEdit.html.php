<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                        </li>
                        <li>
                            <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                        </li>
                        <li>
                            <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <!-- PERSONAL INFO TAB -->
                        <div class="tab-pane active" id="tab_1_1">
                            <form role="form" action="#" id="formProfile">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your profile is successfully updated! </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group validator-group">
                                            <label class="control-label">Institute Name <span class="required" aria-required="true"> * </span></label>
                                            <input type="text" id="inputInstituteName" name="inputInstituteName" placeholder="e.g. Harvard University" class="form-control" /> </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group validator-group">
                                            <label class="control-label">Mission/Tagline/Motto <span class="required" aria-required="true"> * </span></label>
                                            <input type="text" id="inputInstituteMission" name="inputInstituteMission" placeholder="e.g. Lux et veritas" class="form-control" /> </div>
                                    </div>
                                </div>
                                <div class="form-group validator-group">
                                    <label class="control-label">Owner/Trust Name <span class="required" aria-required="true"> * </span></label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" id="inputOwnerFirstName" name="inputFirstName" placeholder="e.g. John" class="form-control" />
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" id="inputOwnerLastName" name="inputLastName" placeholder="e.g. Doe" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group validator-group">
                                    <label class="control-label">Contact Person Name <span class="required" aria-required="true"> * </span></label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" id="inputContactFirstName" name="inputFirstName" placeholder="e.g. John" class="form-control" />
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" id="inputContactLastName" name="inputLastName" placeholder="e.g. Doe" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group validator-group">
                                            <label class="control-label">No. of <?php echo $global->terminology["student_plural"]; ?> <span class="required" aria-required="true"> * </span></label>
                                            <select id="selectStudentCount" name="selectStudentCount" class="form-control">
                                                <option value="">Not Specified</option>
                                                <option value="100-500">100-500</option>
                                                <option value="500-1000">500-1000</option>
                                                <option value="1000-2000">1000-2000</option>
                                            </select> </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group validator-group">
                                            <label class="control-label">Year Of Inception <span class="required" aria-required="true"> * </span></label>
                                            <select class="form-control m-bot15" id="selectYearInception" name="selectYearInception">
                                                <option value="0">Not Specified</option>
                                                <?php
                                                for ($i=1600; $i <= date("Y", time()); $i++) {
                                                    echo "<option value='$i'>$i</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Mobile Number (CC + 10 digit number)</label>
                                            <div class="input-group">
                                                <span class="validator-group">
                                                    <input type="text" id="inputMobilePrefix" name="inputMobilePrefix" class="form-control w-60 input-xs" data-error-container="#mobileError">
                                                </span>
                                                <span class="input-group-btn"></span>
                                                <span class="validator-group">
                                                    <input type="text" id="inputMobile" name="inputMobile" placeholder="e.g. 9876543210" class="form-control input-medium" data-error-container="#mobileError" />
                                                </span>
                                            </div>
                                            <div id="mobileError" class="has-error"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Landline Number</label>
                                            <div class="input-group">
                                                <span class="validator-group">
                                                    <input type="text" id="inputLandlinePrefix" name="inputLandlinePrefix" placeholder="022" class="form-control w-60 input-xs">
                                                </span>
                                                <span class="input-group-btn"></span>
                                                <span class="validator-group">
                                                    <input type="text" id="inputLandline" name="inputLandline" placeholder="e.g. 2654987" class="form-control input-medium" />
                                                </span>
                                            </div>
                                            <div id="landlineError" class="has-error"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group validator-group">
                                            <label class="control-label">Address </label>
                                            <input type="text" id="inputAddress" name="inputAddress" placeholder="e.g. 123, rth Cross Street" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group validator-group">
                                            <label class="control-label">City </label>
                                            <input type="text" id="inputCity" name="inputCity" placeholder="e.g. Bangalore" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group validator-group">
                                            <label class="control-label">State </label>
                                            <select id="selectState" name="selectState" class="form-control">
                                                <option value="">Select State</option>
                                                <optgroup label="States">
                                                    <option>Andhra Pradesh</option>
                                                    <option>Arunachal Pradesh</option>
                                                    <option>Assam</option>
                                                    <option>Bihar</option>
                                                    <option>Chhattisgarh</option>
                                                    <option>Goa</option>
                                                    <option>Gujarat</option>
                                                    <option>Haryana</option>
                                                    <option>Himachal Pradesh</option>
                                                    <option>Jammu and Kashmir</option>
                                                    <option>Jharkhand</option>
                                                    <option>Karnataka</option>
                                                    <option>Kerala</option>
                                                    <option>Madhya Pradesh</option>
                                                    <option>Maharashtra</option>
                                                    <option>Manipur</option>
                                                    <option>Meghalaya</option>
                                                    <option>Mizoram</option>
                                                    <option>Nagaland</option>
                                                    <option>Odisha</option>
                                                    <option>Punjab</option>
                                                    <option>Rajasthan</option>
                                                    <option>Sikkim</option>
                                                    <option>Tamil Nadu</option>
                                                    <option>Telangana</option>
                                                    <option>Tripura</option>
                                                    <option>Uttar Pradesh</option>
                                                    <option>Uttarakhand</option>
                                                    <option>West Bengal</option>
                                                </optgroup>
                                                <optgroup label="Union territories">
                                                    <option>Andaman and Nicobar Islands</option>
                                                    <option>Chandigarh</option>
                                                    <option>Dadra and Nagar Haveli</option>
                                                    <option>Daman and Diu</option>
                                                    <option>Lakshadweep</option>
                                                    <option>Delhi</option>
                                                    <option>Puducherry</option>
                                                </optgroup>
                                                <optgroup label="Other">
                                                    <option>Other</option>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group validator-group">
                                            <label class="control-label">Country </label>
                                            <select name="selectCountry" id="selectCountry" class="form-control"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group validator-group">
                                            <label class="control-label">PIN/ZIP Number </label>
                                            <input type="text" id="inputPin" name="inputPin" placeholder="e.g. 400004" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="margin-top-10">
                                    <button type="submit" class="btn green" id="btnProfileSubmit">Submit</button>
                                </div>
                            </form>
                        </div>
                        <!-- END PERSONAL INFO TAB -->
                        <!-- CHANGE AVATAR TAB -->
                        <div class="tab-pane" id="tab_1_2">
                            <p> Please select an image for your profile picture. </p>
                            <form action="#" role="form">
                                <!-- <div class="form-group">
                                    <input type="file" />
                                    <img class="crop" style="display:none" />
                                    <button type="submit" style="display:none">Upload</button>
                                </div> -->
                                <div class="form-group">
                                    <div class="profile-pic">
                                        <div class="thumbnail">
                                            <img id="userProfilePic" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span> Change image </span>
                                                <input id="fileProfilePic" type="file" name="..."> </span>
                                            <a href="javascript:;" class="btn default js-remove"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="margin-top-10 hide js-upload">
                                    <a href="javascript:;" class="btn green" id="btnUpload">Upload</a>
                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                </div>
                            </form>
                        </div>
                        <!-- END CHANGE AVATAR TAB -->
                        <!-- CHANGE PASSWORD TAB -->
                        <div class="tab-pane" id="tab_1_3">
                            <form role="form" action="#" id="formPassword">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your password is successfully changed! </div>
                                <div class="form-group">
                                    <label class="control-label">Current Password</label>
                                    <input type="password" class="form-control" id="inputOldPassword" name="inputOldPassword" /> </div>
                                <div class="form-group">
                                    <label class="control-label">New Password</label>
                                    <input type="password" class="form-control" id="inputNewPassword" name="inputNewPassword" /> </div>
                                <div class="form-group">
                                    <label class="control-label">Re-type New Password</label>
                                    <input type="password" class="form-control" id="inputNewRePassword" name="inputNewRePassword" /> </div>
                                <div class="margin-top-10">
                                    <button type="submit" class="btn green">Change Password</button>
                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                </div>
                            </form>
                        </div>
                        <!-- END CHANGE PASSWORD TAB -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PROFILE CONTENT -->