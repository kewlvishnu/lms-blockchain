<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<h1 class="page-title"> <?php echo $global->terminology["instructor_plural"]; ?> </h1>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo $sitepathManage; ?>">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<i class="icon-list"></i>
					<a href="<?php echo $sitepathManageInstructors; ?>"><?php echo $global->terminology["instructor_plural"]; ?></a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<span>Invite <?php echo $global->terminology["instructor_plural"]; ?></span>
				</li>
			</ul>
			<div class="page-toolbar">
				<div class="btn-group pull-right">
					<a href="<?php echo $sitepathManageInstructors; ?>invite" class="btn btn-fit-height blue"> Invite <?php echo $global->terminology["instructor_plural"]; ?></a>
					<a href="<?php echo $sitepathManageInstructors; ?>invitations" class="btn btn-fit-height green"> Invitations</a>
				</div>
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light ">
					<div class="portlet-title tabbable-line">
						<div class="caption caption-md">
							<i class="icon-globe theme-font hide"></i>
							<span class="caption-subject font-blue-madison bold uppercase">Invite <?php echo $global->terminology["instructor_plural"]; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-lg-6 col-lg-offset-3 col-sm-8 col-sm-offset-2">
								<div class="input-group input-group-3">
									<input type="text" class="form-control" id="inputSearchText" name="inputSearchText" placeholder="Search for professors"/>
									<select class="form-control" id="selectSearchFilter" name="selectSearchFilter">
										<option value="1">Name</option>
										<option value="2">Email</option>
										<option value="3">Country</option>
									</select> 
									<span class="input-group-btn">
										<button type="button" class="btn green" id="btnSearchProfessors"><i class="fa fa-search"></i> Search</button>
									</span>
								</div>
							</div>
						</div>
						<div class="margin-bottom-20"></div>
						<div class="row" id="siteProfessors">
							<!-- <div class="col-lg-3 col-md-4 col-sm-6">
								begin: widget 1-1
								<div class="mt-widget-1 ar-widget-2">
									<div class="mt-img">
										<img src="assets/pages/media/users/avatar80_8.jpg"> </div>
									<div class="mt-body">
										<h3 class="mt-username">Diana Ellison</h3>
										<p class="mt-user-title">
											<ul class="list-unstyled">
												<li>E-Mail: professor1@yopmail.com</li>
												<li>Country: India</li>
											</ul> </p>
										<div class="mt-stats">
											<div class="btn-group btn-group btn-group-justified">
												<a href="javascript:;" class="btn font-red"> Profile </a>
												<a href="javascript:;" class="btn font-green"> Invited </a>
											</div>
										</div>
									</div>
								</div>
								end: widget 1-1
							</div>
							<div class="col-lg-3 col-md-4 col-sm-6">
								begin: widget 1-2
								<div class="mt-widget-1 ar-widget-2">
									<div class="mt-img">
										<img src="assets/pages/media/users/avatar80_7.jpg"> </div>
									<div class="mt-body">
										<h3 class="mt-username">Jason Baker</h3>
										<p class="mt-user-title">
											<ul class="list-unstyled">
												<li>E-Mail: professor1@yopmail.com</li>
												<li>Country: India</li>
											</ul> </p>
										<div class="mt-stats">
											<div class="btn-group btn-group btn-group-justified">
												<a href="javascript:;" class="btn font-red"> Profile </a>
												<a href="javascript:;" class="btn font-green"> Invited </a>
											</div>
										</div>
									</div>
								</div>
								end: widget 1-2
							</div>
							<div class="col-lg-3 col-md-4 col-sm-6">
								begin: widget 1-3
								<div class="mt-widget-1 ar-widget-2">
									<div class="mt-img">
										<img src="assets/pages/media/users/avatar80_6.jpg"> </div>
									<div class="mt-body">
										<h3 class="mt-username">Julia Berry</h3>
										<p class="mt-user-title">
											<ul class="list-unstyled">
												<li>E-Mail: professor1@yopmail.com</li>
												<li>Country: India</li>
											</ul> </p>
										<div class="mt-stats">
											<div class="btn-group btn-group btn-group-justified">
												<a href="javascript:;" class="btn font-red"> Profile </a>
												<a href="javascript:;" class="btn font-green"> Invited </a>
											</div>
										</div>
									</div>
								</div>
								end: widget 1-3
							</div>
							<div class="col-lg-3 col-md-4 col-sm-6">
								begin: widget 1-1
								<div class="mt-widget-1 ar-widget-2">
									<div class="mt-img">
										<img src="assets/pages/media/users/avatar80_8.jpg"> </div>
									<div class="mt-body">
										<h3 class="mt-username">Diana Ellison</h3>
										<p class="mt-user-title">
											<ul class="list-unstyled">
												<li>E-Mail: professor1@yopmail.com</li>
												<li>Country: India</li>
											</ul> </p>
										<div class="mt-stats">
											<div class="btn-group btn-group btn-group-justified">
												<a href="javascript:;" class="btn font-red"> Profile </a>
												<a href="javascript:;" class="btn font-green"> Invited </a>
											</div>
										</div>
									</div>
								</div>
								end: widget 1-1
							</div>
							<div class="col-lg-3 col-md-4 col-sm-6">
								begin: widget 1-2
								<div class="mt-widget-1 ar-widget-2">
									<div class="mt-img">
										<img src="assets/pages/media/users/avatar80_7.jpg"> </div>
									<div class="mt-body">
										<h3 class="mt-username">Jason Baker</h3>
										<p class="mt-user-title">
											<ul class="list-unstyled">
												<li>E-Mail: professor1@yopmail.com</li>
												<li>Country: India</li>
											</ul> </p>
										<div class="mt-stats">
											<div class="btn-group btn-group btn-group-justified">
												<a href="javascript:;" class="btn font-red"> Profile </a>
												<a href="javascript:;" class="btn font-green"> Invited </a>
											</div>
										</div>
									</div>
								</div>
								end: widget 1-2
							</div>
							<div class="col-lg-3 col-md-4 col-sm-6">
								begin: widget 1-3
								<div class="mt-widget-1 ar-widget-2">
									<div class="mt-img">
										<img src="assets/pages/media/users/avatar80_6.jpg"> </div>
									<div class="mt-body">
										<h3 class="mt-username">Julia Berry</h3>
										<p class="mt-user-title">
											<ul class="list-unstyled">
												<li>E-Mail: professor1@yopmail.com</li>
												<li>Country: India</li>
											</ul> </p>
										<div class="mt-stats">
											<div class="btn-group btn-group btn-group-justified">
												<a href="javascript:;" class="btn font-red"> Profile </a>
												<a href="javascript:;" class="btn font-green"> Invited </a>
											</div>
										</div>
									</div>
								</div>
								end: widget 1-3
							</div> -->
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->