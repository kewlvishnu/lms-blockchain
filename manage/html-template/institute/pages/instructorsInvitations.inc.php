<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<h1 class="page-title"> <?php echo $global->terminology["instructor_plural"]; ?> </h1>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo $sitepathManage; ?>">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<i class="icon-list"></i>
					<a href="<?php echo $sitepathManageInstructors; ?>"><?php echo $global->terminology["instructor_plural"]; ?></a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<span>Invite <?php echo $global->terminology["instructor_plural"]; ?></span>
				</li>
			</ul>
			<div class="page-toolbar">
				<div class="btn-group pull-right">
					<a href="<?php echo $sitepathManageInstructors; ?>invite" class="btn btn-fit-height blue"> Invite <?php echo $global->terminology["instructor_plural"]; ?></a>
					<a href="<?php echo $sitepathManageInstructors; ?>invitations" class="btn btn-fit-height green"> Invitations</a>
				</div>
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light ">
					<div class="portlet-title tabbable-line">
						<div class="caption caption-md">
							<i class="icon-globe theme-font hide"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo $global->terminology["instructor_plural"]; ?> Invitations</span>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
							<thead>
								<tr>
									<th> # </th>
									<th> Name </th>
									<th> Email </th>
									<th> Phone </th>
									<th> Status </th>
									<th> Actions </th>
								</tr>
							</thead>
							<tbody>
								<tr class="odd gradeX">
									<td colspan="6">No invitations found</td>
								</tr>
								<!-- <tr class="odd gradeX">
									<td>420</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>+91-9876543210</td>
									<td><span class="label label-warning"><i class="fa fa-info-circle"></i> Pending</span></td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"> Resend</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>420</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>+91-9876543210</td>
									<td><span class="label bg-green-dark"><i class="fa fa-check-circle"></i> Accepted</span></td>
									<td></td>
								</tr>
								<tr class="odd gradeX">
									<td>420</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>+91-9876543210</td>
									<td><span class="label bg-green-dark"><i class="fa fa-check-circle"></i> Accepted</span></td>
									<td></td>
								</tr>
								<tr class="odd gradeX">
									<td>420</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>+91-9876543210</td>
									<td><span class="label bg-green-dark"><i class="fa fa-check-circle"></i> Accepted</span></td>
									<td></td>
								</tr>
								<tr class="odd gradeX">
									<td>420</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>+91-9876543210</td>
									<td><span class="label label-warning"><i class="fa fa-info-circle"></i> Pending</span></td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"> Resend</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>420</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>+91-9876543210</td>
									<td><span class="label label-warning"><i class="fa fa-info-circle"></i> Pending</span></td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"> Resend</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>420</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>+91-9876543210</td>
									<td><span class="label label-warning"><i class="fa fa-info-circle"></i> Pending</span></td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"> Resend</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>420</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>+91-9876543210</td>
									<td><span class="label label-warning"><i class="fa fa-info-circle"></i> Pending</span></td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"> Resend</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>420</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>+91-9876543210</td>
									<td><span class="label label-warning"><i class="fa fa-info-circle"></i> Pending</span></td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"> Resend</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>420</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>+91-9876543210</td>
									<td><span class="label label-warning"><i class="fa fa-info-circle"></i> Pending</span></td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"> Resend</a>
									</td>
								</tr> -->
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->