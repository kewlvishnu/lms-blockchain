<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<h1 class="page-title"> <?php echo $global->terminology["instructor_plural"]; ?> </h1>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo $sitepathManage; ?>">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<span><?php echo $global->terminology["instructor_plural"]; ?></span>
				</li>
			</ul>
			<div class="page-toolbar">
				<div class="btn-group pull-right">
					<a href="<?php echo $sitepathManageInstructors; ?>invite" class="btn btn-fit-height blue"> Invite <?php echo $global->terminology["instructor_plural"]; ?></a>
					<a href="<?php echo $sitepathManageInstructors; ?>invitations" class="btn btn-fit-height green"> Invitations</a>
				</div>
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light ">
					<div class="portlet-title tabbable-line">
						<div class="caption caption-md">
							<i class="icon-globe theme-font hide"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo $global->terminology["instructor_plural"]; ?></span>
							<span class="caption-helper"><?php echo $global->terminology["instructor_plural"]; ?> affiliated to your institute</span>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table table-striped table-bordered table-hover table-checkable order-column table-instructors" id="sample_1">
							<thead>
								<tr>
									<th> ID # </th>
									<th> Picture </th>
									<th> Name </th>
									<th> Email </th>
									<th> Contact </th>
									<th> <?php echo $global->terminology["subject_plural"]; ?> </th>
									<th> Actions </th>
								</tr>
							</thead>
							<tbody>
								<tr class="odd gradeX">
									<td colspan="7">There are no <?php echo strtolower($global->terminology["instructor_plural"]); ?> currently associated with you</td>
								</tr>
								<?php /*<tr class="odd gradeX">
									<td>420</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>+919876543210</td>
									<td>
										<ul>
											<li>Power Electronics</li>
											<li>International Finance</li>
										</ul>
									</td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-comment"></i> Chat</a>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Remove</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>420</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>+919876543210</td>
									<td>
										<ul>
											<li>Power Electronics</li>
											<li>International Finance</li>
										</ul>
									</td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-comment"></i> Chat</a>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Remove</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>420</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>+919876543210</td>
									<td>
										<ul>
											<li>Power Electronics</li>
											<li>International Finance</li>
										</ul>
									</td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-comment"></i> Chat</a>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Remove</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>420</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>+919876543210</td>
									<td>
										<ul>
											<li>Power Electronics</li>
											<li>International Finance</li>
										</ul>
									</td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-comment"></i> Chat</a>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Remove</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>420</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>+919876543210</td>
									<td>
										<ul>
											<li>Power Electronics</li>
											<li>International Finance</li>
										</ul>
									</td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-comment"></i> Chat</a>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Remove</a>
									</td>
								</tr>
								<tr class="odd gradeX">
									<td>420</td>
									<td> Jane </td>
									<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
									<td>+919876543210</td>
									<td>
										<ul>
											<li>Power Electronics</li>
											<li>International Finance</li>
										</ul>
									</td>
									<td>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-comment"></i> Chat</a>
										<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Remove</a>
									</td>
								</tr> */ ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->