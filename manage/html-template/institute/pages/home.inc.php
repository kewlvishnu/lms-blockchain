    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <h1 class="page-title"> Dashboard
                <small>dashboard &amp; statistics</small>
            </h1>
            <!-- END PAGE HEADER-->
            <div class="row widget-row widget-row-sm">
                <div class="col-md-3">
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <div class="widget-thumb-wrap">
                            <a href="<?php echo $sitepathInstituteCourses; ?>"><i class="widget-thumb-icon bg-green icon-layers"></i></a>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle"><?php echo $global->terminology["course_plural"]; ?></span>
                                <span class="widget-thumb-body-stat courseCount">0</span>
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
                </div>
                <div class="col-md-3">
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <div class="widget-thumb-wrap">
                            <a href="<?php echo $sitepathInstituteStudents; ?>"><i class="widget-thumb-icon bg-red fa fa-users"></i></a>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle"><?php echo $global->terminology["student_plural"]; ?></span>
                                <span class="widget-thumb-body-stat studentCount">0</span>
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
                </div>
                <div class="col-md-3">
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <div class="widget-thumb-wrap">
                            <a href="<?php echo $sitepathInstituteInstructors; ?>"><i class="widget-thumb-icon bg-purple icon-screen-desktop"></i></a>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle"><?php echo $global->terminology["instructor_plural"]; ?></span>
                                <span class="widget-thumb-body-stat professorCount">0</span>
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
                </div>
                <div class="col-md-3">
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <div class="widget-thumb-wrap">
                            <a href="<?php echo $sitepathInstituteKeys; ?>"><i class="widget-thumb-icon bg-blue fa fa-bars"></i></a>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle">Enrollment Keys</span>
                                <span class="widget-thumb-body-stat courseKeysCount">0</span>
                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12" id="blockCourses">
                    <div class="portlet light portlet-fit">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-microphone font-dark hide"></i>
                                <span class="caption-subject bold font-dark uppercase"> <?php echo $global->terminology["course_plural"]; ?></span>
                                <span class="caption-helper">Your <?php echo strtolower($global->terminology["course_plural"]); ?></span>
                            </div>
                            <div class="actions">
                                <a href="javascript:void(0)" data-action="new" data-toggle="modal" class="btn btn-fit-height green btn-course"> Create <?php echo strtolower($global->terminology["course_single"]); ?></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row" id="listCourses"></div>
                        </div>
                    </div>
                </div><?php /*
                <div class="col-sm-6" id="blockNotifications">
                    <div class="portlet light portlet-invitations">
                        <div class="portlet-title tabbable-line">
                            <div class="caption">
                                <i class=" icon-social-twitter font-dark hide"></i>
                                <span class="caption-subject font-dark bold uppercase">Notifications</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="scroller" style="height: 250px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                <!-- BEGIN: Actions -->
                                <ul class="feeds" id="siteNotifications"></ul>
                                <!-- END: Actions -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6" id="blockChat">
                    <div class="portlet light portlet-chats">
                        <div class="portlet-title tabbable-line">
                            <div class="caption">
                                <i class="icon-bubbles font-dark hide"></i>
                                <span class="caption-subject font-dark bold uppercase">Chat Updates</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="scroller" style="height: 250px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                <!-- BEGIN: Comments -->
                                <div class="mt-comments"></div>
                                <!-- END: Comments -->
                            </div>
                        </div>
                    </div>
                </div>*/ ?>
                <div class="col-sm-6">
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-microphone font-dark hide"></i>
                                <span class="caption-subject bold font-dark uppercase"> <?php echo $global->terminology["course_single"]; ?> <?php echo $global->terminology["student_plural"]; ?></span>
                                <span class="caption-helper">Overall <?php echo $global->terminology["student_plural"]; ?> per <?php echo $global->terminology["course_single"]; ?></span>
                            </div>
                            <div class="actions">
                                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group">
                                <div id="dashboard_amchart_1" class="min-height-500"></div>
                            </div>
                            <p class="text-center">No of <?php echo $global->terminology["student_plural"]; ?> per <?php echo $global->terminology["course_single"]; ?> created by you</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-microphone font-dark hide"></i>
                                <span class="caption-subject bold font-dark uppercase"><?php echo $global->terminology["course_single"]; ?> Views</span>
                                <span class="caption-helper">Unique views in last week</span>
                            </div>
                            <div class="actions actions-graph">
                                <select name="" id="courseSelector" class="form-control"></select>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-group">
                                <div id="dashboard_amchart_3" class="min-height-500"></div>
                            </div>
                            <p class="text-center"><a href="javascript:;">Click here to see detailed graph</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->