<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<?php
			require_once('html-template/institute/includes/header-subject.php');
		?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<?php
					require_once('html-template/institute/includes/sidebar-subject.php');
				?>
				<!-- BEGIN TICKET LIST CONTENT -->
				<div class="profile-content">
					<!-- BEGIN DASHBOARD STATS 1-->
					<div class="row">
						<!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<a class="dashboard-stat dashboard-stat-v2 dashboard-stat-sm blue" data-toggle="modal" href="#modalSubjectProfessors">
								<div class="visual">
									<i class="fa fa-eye"></i>
								</div>
								<div class="details">
									<div class="number">
										<span data-counter="counterup" data-value="<?php //echo $subjectDetails['instructors']; ?>"><?php //echo $subjectDetails['instructors']; ?></span>
									</div>
									<div class="desc"> Professors </div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<a class="dashboard-stat dashboard-stat-v2 dashboard-stat-sm red" href="<?php //echo $sitepathInstituteSubjects.$subjectId; ?>/students">
								<div class="visual">
									<i class="fa fa-users"></i>
								</div>
								<div class="details">
									<div class="number">
										<span data-counter="counterup" data-value="<?php //echo $subjectDetails['students']; ?>"><?php //echo $subjectDetails['students']; ?></span> </div>
									<div class="desc"> Students </div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<a class="dashboard-stat dashboard-stat-v2 dashboard-stat-sm purple" href="<?php //echo (($subjectDetails['chapters']>0)?($sitepathManageContent.'subjects/'.$subjectId):('javascript:;')); ?>">
								<div class="visual">
									<i class="fa fa-users"></i>
								</div>
								<div class="details">
									<div class="number">
										<span data-counter="counterup" data-value="<?php //echo $subjectDetails['chapters']; ?>"><?php //echo $subjectDetails['chapters']; ?></span>
									</div>
									<div class="desc"> Lectures </div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<a class="dashboard-stat dashboard-stat-v2 dashboard-stat-sm green" data-scroll href="#listSections">
								<div class="visual">
									<i class="fa fa-dollar"></i>
								</div>
								<div class="details">
									<div class="number">
										<span data-counter="counterup" data-value="<?php //echo $subjectDetails['exams']; ?>"><?php //echo $subjectDetails['exams']; ?></span>
									</div>
									<div class="desc"> Exams </div>
								</div>
							</a>
						</div> -->
						<!-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<a class="dashboard-stat dashboard-stat-v2 dashboard-stat-sm green-jungle" href="<?php //echo $sitepathInstituteSubjects.$subjectId; ?>/questions">
								<div class="visual">
									<i class="fa fa-dollar"></i>
								</div>
								<div class="details">
									<div class="number">
										<span data-counter="counterup" data-value="<?php //echo $subjectDetails['questions']; ?>"><?php //echo $subjectDetails['questions']; ?></span>
									</div>
									<div class="desc"> Questions </div>
								</div>
							</a>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<a class="dashboard-stat dashboard-stat-v2 dashboard-stat-sm green-soft" href="<?php //echo $sitepathInstituteSubjects.$subjectId; ?>/subjective-questions">
								<div class="visual">
									<i class="fa fa-dollar"></i>
								</div>
								<div class="details">
									<div class="number">
										<span data-counter="counterup" data-value="<?php //echo $subjectDetails['subjective_questions']; ?>"><?php //echo $subjectDetails['subjective_questions']; ?></span>
									</div>
									<div class="desc"> Subjective Questions </div>
								</div>
							</a>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<a class="dashboard-stat dashboard-stat-v2 dashboard-stat-sm blue-oleo" href="<?php //echo $sitepathInstituteSubjects.$subjectId; ?>/submission-questions">
								<div class="visual">
									<i class="fa fa-dollar"></i>
								</div>
								<div class="details">
									<div class="number">
										<span data-counter="counterup" data-value="<?php //echo $subjectDetails['submission_questions']; ?>"><?php //echo $subjectDetails['submission_questions']; ?></span>
									</div>
									<div class="desc"> Submission Questions </div>
								</div>
							</a>
						</div> -->
					</div>
					<div class="clearfix"></div>
					<!-- END DASHBOARD STATS 1-->
					<div class="row">
						<div class="col-sm-12" id="listSections">
							<!-- <div class="portlet box portlet-course blue-hoki">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-book"></i> Section 1: Lecture 1 </div>
									<div class="tools">
										<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
										<a href="" class="fullscreen" data-original-title="" title=""> </a>
									</div>
									<div class="actions">
										<a href="<?php //echo $sitepathManageContent.'subjects/'.$subjectId; ?>" class="btn btn-default btn-sm">
											<i class="fa fa-eye"></i> See Content </a>
										<a href="<?php //echo $sitepathInstituteSubjects.$subjectId; ?>/sections" data-action="edit" data-toggle="modal" class="btn btn-default btn-sm btn-course">
											<i class="fa fa-pencil"></i> Edit Section </a>
										<a href="javascript:;" class="btn btn-default btn-sm">
											<i class="fa fa-trash"></i> Delete </a>
									</div>
								</div>
								<div class="portlet-body no-space">
									<table class="table table-striped table-bordered table-hover order-column no-margin">
										<thead>
											<tr>
												<th class="text-center" colspan="7">Exams/Assignments</th>
											</tr>
											<tr>
												<th width="25%"> Title </th>
												<th class="text-center" width="10%"> Type </th>
												<th class="text-center" width="10%"> Start Date </th>
												<th class="text-center" width="5%"> Attempts </th>
												<th class="text-center" width="5%"> Status </th>
												<th class="text-center" width="10%"> End Date </th>
												<th width="35%"> Actions </th>
											</tr>
										</thead>
										<tbody>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteExams; ?>1">Objective Exam 1</a> </td>
												<td class="text-center"> Objective Exam </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="<?php //echo $sitepathInstituteExams; ?>1" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="<?php //echo $sitepathInstituteExams; ?>1/settings" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="<?php //echo $sitepathInstituteExams; ?>1/result" class="btn btn-circle btn-sm green"><i class="fa fa-book"></i> Result</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteExams; ?>1">Objective Exam 1</a> </td>
												<td class="text-center"> Objective Exam </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="<?php //echo $sitepathInstituteExams; ?>1" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="<?php //echo $sitepathInstituteExams; ?>1/settings" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="<?php //echo $sitepathInstituteExams; ?>1/result" class="btn btn-circle btn-sm green"><i class="fa fa-book"></i> Result</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteExams; ?>1">Objective Assignment 1</a> </td>
												<td class="text-center"> Assignment </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="<?php //echo $sitepathInstituteExams; ?>1" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="<?php //echo $sitepathInstituteExams; ?>1/settings" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="<?php //echo $sitepathInstituteExams; ?>1/result" class="btn btn-circle btn-sm green"><i class="fa fa-book"></i> Result</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteExams; ?>1">Objective Exam 1</a> </td>
												<td class="text-center"> Assignment </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="<?php //echo $sitepathInstituteExams; ?>1" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="<?php //echo $sitepathInstituteExams; ?>1/settings" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="<?php //echo $sitepathInstituteExams; ?>1/result" class="btn btn-circle btn-sm green"><i class="fa fa-book"></i> Result</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjectiveExams; ?>1">Subjective Exam 1</a> </td>
												<td class="text-center"> Subjective Exam </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="<?php //echo $sitepathInstituteSubjectiveExams; ?>1" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="<?php //echo $sitepathInstituteSubjectiveExams; ?>1/settings" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="<?php //echo $sitepathInstituteSubjectiveExams; ?>1/result" class="btn btn-circle btn-sm green"><i class="fa fa-book"></i> Result</a>
													<a href="<?php //echo $sitepathInstituteSubjectiveExams; ?>1/check" class="btn btn-circle btn-sm green"><i class="fa fa-check"></i> Check</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjectiveExams; ?>1">Subjective Exam 1</a> </td>
												<td class="text-center"> Subjective Exam </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="<?php //echo $sitepathInstituteSubjectiveExams; ?>1" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="<?php //echo $sitepathInstituteSubjectiveExams; ?>1/settings" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="<?php //echo $sitepathInstituteSubjectiveExams; ?>1/result" class="btn btn-circle btn-sm green"><i class="fa fa-book"></i> Result</a>
													<a href="<?php //echo $sitepathInstituteSubjectiveExams; ?>1/check" class="btn btn-circle btn-sm green"><i class="fa fa-check"></i> Check</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteManualExams; ?>1">Manual Exam 1</a> </td>
												<td class="text-center"> Manual Exam </td>
												<td class="text-center" colspan="4"> Quick Analysis </td>
												<td>
													<a href="<?php //echo $sitepathInstituteManualExams; ?>1" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Result</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteManualExams; ?>1">Manual Exam 1</a> </td>
												<td class="text-center"> Manual Exam </td>
												<td class="text-center" colspan="4"> Deep Analysis </td>
												<td>
													<a href="<?php //echo $sitepathInstituteManualExams; ?>1" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Result</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="portlet box portlet-course blue-hoki">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-book"></i> Section 2: Lecture 2 </div>
									<div class="tools">
										<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
										<a href="" class="fullscreen" data-original-title="" title=""> </a>
									</div>
									<div class="actions">
										<a href="<?php //echo $sitepathInstituteCourses; ?>1" class="btn btn-default btn-sm">
											<i class="fa fa-eye"></i> See Content </a>
										<a href="javascript:;" data-action="edit" data-toggle="modal" class="btn btn-default btn-sm btn-course">
											<i class="fa fa-pencil"></i> Edit Section </a>
										<a href="javascript:;" class="btn btn-default btn-sm">
											<i class="fa fa-trash"></i> Delete </a>
									</div>
								</div>
								<div class="portlet-body no-space">
									<table class="table table-striped table-bordered table-hover order-column no-margin">
										<thead>
											<tr>
												<th class="text-center" colspan="7">Exams/Assignments</th>
											</tr>
											<tr>
												<th width="25%"> Title </th>
												<th class="text-center" width="10%"> Type </th>
												<th class="text-center" width="10%"> Start Date </th>
												<th class="text-center" width="5%"> Attempts </th>
												<th class="text-center" width="5%"> Status </th>
												<th class="text-center" width="10%"> End Date </th>
												<th width="35%"> Actions </th>
											</tr>
										</thead>
										<tbody>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Objective Exam 1</a> </td>
												<td class="text-center"> Objective Exam </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Objective Exam 1</a> </td>
												<td class="text-center"> Objective Exam </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Objective Assignment 1</a> </td>
												<td class="text-center"> Assignment </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Objective Exam 1</a> </td>
												<td class="text-center"> Assignment </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Subjective Exam 1</a> </td>
												<td class="text-center"> Subjective Exam </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Subjective Exam 1</a> </td>
												<td class="text-center"> Subjective Exam </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Manual Exam 1</a> </td>
												<td class="text-center"> Manual Exam </td>
												<td class="text-center" colspan="4"> Quick Analysis </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Manual Exam 1</a> </td>
												<td class="text-center"> Manual Exam </td>
												<td class="text-center" colspan="4"> Deep Analysis </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="portlet box portlet-course blue-hoki">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-book"></i> Section 3: Lecture 3 </div>
									<div class="tools">
										<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
										<a href="" class="fullscreen" data-original-title="" title=""> </a>
									</div>
									<div class="actions">
										<a href="<?php //echo $sitepathInstituteCourses; ?>1" class="btn btn-default btn-sm">
											<i class="fa fa-eye"></i> See Content </a>
										<a href="javascript:;" data-action="edit" data-toggle="modal" class="btn btn-default btn-sm btn-course">
											<i class="fa fa-pencil"></i> Edit Section </a>
										<a href="javascript:;" class="btn btn-default btn-sm">
											<i class="fa fa-trash"></i> Delete </a>
									</div>
								</div>
								<div class="portlet-body no-space">
									<table class="table table-striped table-bordered table-hover order-column no-margin">
										<thead>
											<tr>
												<th class="text-center" colspan="7">Exams/Assignments</th>
											</tr>
											<tr>
												<th width="25%"> Title </th>
												<th class="text-center" width="10%"> Type </th>
												<th class="text-center" width="10%"> Start Date </th>
												<th class="text-center" width="5%"> Attempts </th>
												<th class="text-center" width="5%"> Status </th>
												<th class="text-center" width="10%"> End Date </th>
												<th width="35%"> Actions </th>
											</tr>
										</thead>
										<tbody>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Objective Exam 1</a> </td>
												<td class="text-center"> Objective Exam </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Objective Exam 1</a> </td>
												<td class="text-center"> Objective Exam </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Objective Assignment 1</a> </td>
												<td class="text-center"> Assignment </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Objective Exam 1</a> </td>
												<td class="text-center"> Assignment </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Subjective Exam 1</a> </td>
												<td class="text-center"> Subjective Exam </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Subjective Exam 1</a> </td>
												<td class="text-center"> Subjective Exam </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Manual Exam 1</a> </td>
												<td class="text-center"> Manual Exam </td>
												<td class="text-center" colspan="4"> Quick Analysis </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Manual Exam 1</a> </td>
												<td class="text-center"> Manual Exam </td>
												<td class="text-center" colspan="4"> Deep Analysis </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="portlet box portlet-course blue-soft">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-book"></i> Independent Exams/Assignments </div>
									<div class="tools">
										<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
										<a href="" class="fullscreen" data-original-title="" title=""> </a>
									</div>
								</div>
								<div class="portlet-body no-space">
									<table class="table table-striped table-bordered table-hover order-column no-margin">
										<thead>
											<tr>
												<th class="text-center" colspan="7">Independent Exams/Assignments</th>
											</tr>
											<tr>
												<th width="25%"> Title </th>
												<th class="text-center" width="10%"> Type </th>
												<th class="text-center" width="10%"> Start Date </th>
												<th class="text-center" width="5%"> Attempts </th>
												<th class="text-center" width="5%"> Status </th>
												<th class="text-center" width="10%"> End Date </th>
												<th width="35%"> Actions </th>
											</tr>
										</thead>
										<tbody>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Objective Exam 1</a> </td>
												<td class="text-center"> Objective Exam </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Objective Exam 1</a> </td>
												<td class="text-center"> Objective Exam </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Objective Assignment 1</a> </td>
												<td class="text-center"> Assignment </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Objective Exam 1</a> </td>
												<td class="text-center"> Assignment </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Subjective Exam 1</a> </td>
												<td class="text-center"> Subjective Exam </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Subjective Exam 1</a> </td>
												<td class="text-center"> Subjective Exam </td>
												<td class="text-center"> 14-Dec-2015 </td>
												<td class="text-center"> 10 </td>
												<td class="text-center"> Draft </td>
												<td class="text-center"> 14-Dec-2016 </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Manual Exam 1</a> </td>
												<td class="text-center"> Manual Exam </td>
												<td class="text-center" colspan="4"> Quick Analysis </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
											<tr class="odd gradeX">
												<td> <a href="<?php //echo $sitepathInstituteSubjects; ?>1">Manual Exam 1</a> </td>
												<td class="text-center"> Manual Exam </td>
												<td class="text-center" colspan="4"> Deep Analysis </td>
												<td>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i> Questions</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-gear"></i> Settings</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-copy"></i> Copy</a>
													<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div> -->
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12" id="listIndependent">
						</div>
					</div>
				</div>
				<!-- END PROFILE CONTENT -->
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->