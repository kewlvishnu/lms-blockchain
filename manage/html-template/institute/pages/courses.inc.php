<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<h1 class="page-title"> <?php echo $global->terminology["course_plural"]; ?>
			<small>your <?php echo strtolower($global->terminology["course_plural"]); ?></small>
		</h1>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo $sitepathManageInstitute; ?>">Home</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<span><?php echo $global->terminology["course_plural"]; ?></span>
				</li>
			</ul>
			<div class="page-toolbar">
				<div class="btn-group pull-right">
					<a href="javascript:void(0)" data-action="new" data-toggle="modal" class="btn btn-fit-height green btn-course"> Create <?php echo strtolower($global->terminology["course_single"]); ?></a>
				</div>
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<div class="row" id="listCourses">
			<?php /*
			<div class="col-md-6 col-sm-12">
				<div class="portlet box portlet-course blue-hoki">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Naval Engineering - 2016 <small>(ID - C0099)</small></div>
						<div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
							<a href="" class="fullscreen" data-original-title="" title=""> </a>
						</div>
						<div class="actions">
							<a href="<?php echo $sitepathInstituteCourses; ?>1" class="btn btn-default btn-sm">
								<i class="fa fa-eye"></i> Open </a>
							<a href="javascript:;" data-action="edit" data-toggle="modal" class="btn btn-default btn-sm btn-course">
								<i class="fa fa-pencil"></i> Edit </a>
							<a href="javascript:;" class="btn btn-default btn-sm">
								<i class="fa fa-trash"></i> Delete </a>
						</div>
					</div>
					<div class="portlet-body portlet-body-sm">
						<div class="row">
							<div class="col-lg-4 col-md-6">
								<div class="mt-widget-4">
									<div class="mt-img-container">
										<img src="assets/pages/img/background/34.jpg" /> </div>
									<div class="mt-container bg-dark-opacity">
										<div class="mt-head-title"> Power Electronics </div>
										<div class="mt-body-icons">
											<a href="<?php echo $sitepathManageInstitute; ?>subjects/1" title="Lectures">
												<i class="icon-notebook"></i><span class="count"> 5 </span>
											</a>
											<a href="<?php echo $sitepathManageInstitute; ?>subjects/1" title="Exams">
												<i class="icon-note"></i><span class="count"> 10 </span>
											</a>
										</div>
										<a href="<?php echo $sitepathManageInstitute; ?>subjects/1">
											<div class="mt-footer-button">
												<button type="button" class="btn btn-circle btn-danger btn-sm">Start Learning</button>
											</div>
										</a>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6">
								<a href="javascript:void(0)" data-action="new" data-toggle="modal" class="mt-widget-4 bg-blue-opacity btn-subject">
									<span class="image bg-green bg-font-green new">
										<h4><i class="fa fa-plus-circle"></i><span>Subject</span></h4>
									</span>
									<span class="mask">
										<span class="actions text-center">
											<span class="fa fa-plus"></span>
										</span>
									</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-12">
				<div class="portlet box portlet-course red">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Naval Engineering - 2016 <small>(ID - C0099)</small></div>
						<div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
							<a href="" class="fullscreen" data-original-title="" title=""> </a>
						</div>
						<div class="actions">
							<a href="<?php echo $sitepathInstituteCourses; ?>1" class="btn btn-default btn-sm">
								<i class="fa fa-eye"></i> Open </a>
							<a href="javascript:;" data-action="edit" data-toggle="modal" class="btn btn-default btn-sm btn-course">
								<i class="fa fa-pencil"></i> Edit </a>
							<a href="javascript:;" class="btn btn-default btn-sm">
								<i class="fa fa-trash"></i> Delete </a>
						</div>
					</div>
					<div class="portlet-body portlet-body-sm">
						<div class="row">
							<div class="col-lg-4 col-md-6">
								<div class="mt-widget-4">
									<div class="mt-img-container">
										<img src="assets/pages/img/background/34.jpg" /> </div>
									<div class="mt-container bg-dark-opacity">
										<div class="mt-head-title"> Power Electronics </div>
										<div class="mt-body-icons">
											<a href="<?php echo $sitepathManageInstitute; ?>subjects/1" title="Lectures">
												<i class="icon-notebook"></i><span class="count"> 5 </span>
											</a>
											<a href="<?php echo $sitepathManageInstitute; ?>subjects/1" title="Exams">
												<i class="icon-note"></i><span class="count"> 10 </span>
											</a>
										</div>
										<a href="<?php echo $sitepathManageInstitute; ?>subjects/1">
											<div class="mt-footer-button">
												<button type="button" class="btn btn-circle btn-danger btn-sm">Start Learning</button>
											</div>
										</a>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6">
								<div class="mt-widget-4">
									<div class="mt-img-container">
										<img src="assets/pages/img/background/34.jpg" /> </div>
									<div class="mt-container bg-dark-opacity">
										<div class="mt-head-title"> Power Electronics </div>
										<div class="mt-body-icons">
											<a href="<?php echo $sitepathManageInstitute; ?>subjects/1" title="Lectures">
												<i class="icon-notebook"></i><span class="count"> 5 </span>
											</a>
											<a href="<?php echo $sitepathManageInstitute; ?>subjects/1" title="Exams">
												<i class="icon-note"></i><span class="count"> 10 </span>
											</a>
										</div>
										<a href="<?php echo $sitepathManageInstitute; ?>subjects/1">
											<div class="mt-footer-button">
												<button type="button" class="btn btn-circle btn-danger btn-sm">Start Learning</button>
											</div>
										</a>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6">
								<div class="mt-widget-4">
									<div class="mt-img-container">
										<img src="assets/pages/img/background/34.jpg" /> </div>
									<div class="mt-container bg-dark-opacity">
										<div class="mt-head-title"> Power Electronics </div>
										<div class="mt-body-icons">
											<a href="<?php echo $sitepathManageInstitute; ?>subjects/1" title="Lectures">
												<i class="icon-notebook"></i><span class="count"> 5 </span>
											</a>
											<a href="<?php echo $sitepathManageInstitute; ?>subjects/1" title="Exams">
												<i class="icon-note"></i><span class="count"> 10 </span>
											</a>
										</div>
										<a href="<?php echo $sitepathManageInstitute; ?>subjects/1">
											<div class="mt-footer-button">
												<button type="button" class="btn btn-circle btn-danger btn-sm">Start Learning</button>
											</div>
										</a>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6">
								<a href="javascript:void(0)" data-action="new" data-toggle="modal" class="mt-widget-4 bg-blue-opacity btn-subject">
									<span class="image bg-green bg-font-green new">
										<h4><i class="fa fa-plus-circle"></i><span>Subject</span></h4>
									</span>
									<span class="mask">
										<span class="actions text-center">
											<span class="fa fa-plus"></span>
										</span>
									</span>
								</a>
							</div>
						</div>
						<!-- <div class="row">
							<div class="col-md-4">
								<div class="sparkline-chart ar-widget-1">
									<span class="preview">
										<span class="image">
											<img src="assets/pages/img/background/34.jpg" alt="">
										</span>
										<span class="mask">
											<span class="actions">
												<a href="javascript:;" class="btn btn-info"><span class="fa fa-book"></span></a> |
												<a href="javascript:;" class="btn btn-warning"><span class="fa fa-edit"></span></a>
											</span>
										</span>
									</span>
									<a class="title" href="javascript:;"> Power Electronics
										<i class="icon-arrow-right"></i>
									</a>
								</div>
							</div>
							<div class="margin-bottom-10 visible-sm"> </div>
							<div class="col-md-4">
								<div class="sparkline-chart ar-widget-1">
									<span class="preview">
										<span class="image">
											<img src="assets/pages/img/background/34.jpg" alt="">
										</span>
										<span class="mask">
											<span class="actions">
												<a href="javascript:;" class="btn btn-info"><span class="fa fa-book"></span></a> |
												<a href="javascript:;" class="btn btn-warning"><span class="fa fa-edit"></span></a>
											</span>
										</span>
									</span>
									<a class="title" href="javascript:;"> Power Electronics
										<i class="icon-arrow-right"></i>
									</a>
								</div>
							</div>
							<div class="margin-bottom-10 visible-sm"> </div>
							<div class="col-md-4">
								<div class="sparkline-chart ar-widget-1">
									<span class="preview">
										<span class="image">
											<img src="assets/pages/img/background/34.jpg" alt="">
										</span>
										<span class="mask">
											<span class="actions">
												<a href="javascript:;" class="btn btn-info"><span class="fa fa-book"></span></a> |
												<a href="javascript:;" class="btn btn-warning"><span class="fa fa-edit"></span></a>
											</span>
										</span>
									</span>
									<a class="title" href="javascript:;"> Power Electronics
										<i class="icon-arrow-right"></i>
									</a>
								</div>
							</div>
							<div class="margin-bottom-10 visible-sm"> </div>
							<div class="col-md-4">
								<div class="sparkline-chart ar-widget-1">
									<a class="title" href="javascript:;">
										<span class="preview">
											<span class="image bg-green bg-font-green new">
												<h4><i class="fa fa-plus-circle"></i><span>Subject</span></h4>
											</span>
											<span class="mask">
												<span class="actions">
													<span class="fa fa-plus"></span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</div>
						</div> -->
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-12">
				<div class="sparkline-chart ar-widget-1">
					<a class="title btn-course" href="javascript:void(0)" data-action="new" data-toggle="modal">
						<span class="preview">
							<span class="image bg-grey-cararra bg-font-grey-cararra">
								<h4><i class="fa fa-plus-circle"></i><span>Course</span></h4>
							</span>
							<span class="mask">
								<span class="actions">
									<span class="fa fa-plus"></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div> */ ?>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->