<div id="modalEditPortfolio" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Portfolio</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="frmNewPortfolio">
                    <div class="form-body">
                        <div class="form-group">
                            <label for="">Portfolio Name</label>
                            <input type="text" id="inputPortfolioName" name="inputPortfolioName" class="form-control">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label for="">Portfolio Description</label>
                            <input type="text" id="inputPortfolioDescription" name="inputPortfolioDescription" class="form-control">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label for="">Portfolio Subdomain</label>
                            <input type="text" id="inputSubdomain" name="inputSubdomain" class="form-control">
                            <span class="help-block"></span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button type="submit" class="btn blue btn-outline" id="btnEditPortfolio">Submit</button>
            </div>
        </div>
    </div>
</div>