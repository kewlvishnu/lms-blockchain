<!--Modal for academic positions-->
<div aria-hidden="true" aria-labelledby="academicPositionsModal" role="dialog" tabindex="-1" id="academicPositionsModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h4 class="modal-title">
					Add Academic position
				</h4>
			</div>
			<div class="modal-body">
				<form action="">
					<div class="form-group">
						<p class="help-block text-danger"></p>
					</div>
					<div class="form-group">
						<div class="container-fluid">
							<div class="col-sm-6">
								<label for="">From Year</label>
								<input type="text" class="form-control dpYears" id="inputAPFromYear" />
							</div>
							<div class="col-sm-6">
								<label for="">To Year<small>(Leave blank for present)</small></label>
								<input type="text" class="form-control dpYears" id="inputAPToYear" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="">Position</label>
						<input type="text" class="form-control" id="inputAPPosition" />
					</div>
					<div class="form-group">
						<label for="">University</label>
						<input type="text" class="form-control" id="inputAPUniversity" />
					</div>
					<div class="form-group">
						<label for="">Department</label>
						<input type="text" class="form-control" id="inputAPDepartment" />
					</div>
					<div class="form-group">
						<button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnAcademicPosition">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--Modal for education and training-->
<div aria-hidden="true" aria-labelledby="educationModal" role="dialog" tabindex="-1" id="educationModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h4 class="modal-title">
					Add Education &amp; Training Information
				</h4>
			</div>
			<div class="modal-body">
				<form action="">
					<div class="form-group">
						<p class="help-block text-danger"></p>
					</div>
					<div class="form-group">
						<label for="">Degree</label>
						<input type="text" class="form-control" id="inputETDegree" />
					</div>
					<div class="form-group">
						<label for="">Year</label>
						<input type="text" class="form-control dpYears" id="inputETYear" />
					</div>
					<div class="form-group">
						<label for="">Title</label>
						<input type="text" class="form-control" id="inputETTitle" />
					</div>
					<div class="form-group">
						<label for="">University</label>
						<input type="text" class="form-control" id="inputETUniversity" />
					</div>
					<div class="form-group">
						<button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnEducation">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--Modal for Our Honors-->
<div aria-hidden="true" aria-labelledby="honorsModal" role="dialog" tabindex="-1" id="honorsModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h4 class="modal-title">
					Add Our Honors Information
				</h4>
			</div>
			<div class="modal-body">
				<form action="">
					<div class="form-group">
						<p class="help-block text-danger"></p>
					</div>
					<div class="form-group">
                        <div class="profile-pic">
                            <div class="thumbnail">
                                <img id="imgOHImage" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span> Select image </span>
                                    <input id="fileOHImage" type="file" name="fileOHImage"> </span>
                            </div>
                        </div>
                        <input type="hidden" id="inputOHImage" name="inputOHImage" value="">
                    </div>
					<div class="form-group">
						<label for="">Year</label>
						<input type="text" class="form-control dpYears" id="inputOHYear" />
					</div>
					<div class="form-group">
						<label for="">Title</label>
						<input type="text" class="form-control" id="inputOHTitle" />
					</div>
					<div class="form-group">
						<label for="">Description</label>
						<input type="text" class="form-control" id="inputOHDescription" />
					</div>
					<div class="form-group">
						<button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnHonors">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--Modal for Faculty-->
<div aria-hidden="true" aria-labelledby="facultyMembersModal" role="dialog" tabindex="-1" id="facultyMembersModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h4 class="modal-title">
					Add Faculty Member
				</h4>
			</div>
			<div class="modal-body">
				<form action="">
					<div class="form-group">
						<p class="help-block text-danger"></p>
					</div>
					<div class="form-group">
                        <div class="profile-pic">
                            <div class="thumbnail">
                                <img id="imgFMImage" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span> Select image </span>
                                    <input id="fileFMImage" type="file" name="fileFMImage"> </span>
                            </div>
                        </div>
                        <input type="hidden" id="inputFMImage" name="inputFMImage" value="">
                    </div>
					<div class="form-group">
						<label for="">Name</label>
						<input type="text" class="form-control" id="inputFMName" />
					</div>
					<div class="form-group">
						<label for="">University</label>
						<input type="text" class="form-control" id="inputFMUniversity" />
					</div>
					<div class="form-group">
						<label for="">Department</label>
						<input type="text" class="form-control" id="inputFMDepartment" />
					</div>
					<div class="form-group">
						<button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnFacultyMember">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--Modal for Publications-->
<div aria-hidden="true" aria-labelledby="publicationsModal" role="dialog" tabindex="-1" id="publicationsModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h4 class="modal-title">
					Add Publication
				</h4>
			</div>
			<div class="modal-body">
				<form action="">
					<div class="form-group">
						<p class="help-block text-danger"></p>
					</div>
					<div class="form-group">
						<label for="selectPBType">Publication Type</label>
						<select name="selectPBType" id="selectPBType" class="form-control">
							<option value="1">Published Papers</option>
							<option value="2">Research Papers</option>
							<option value="3">Books</option>
						</select>
					</div>
					<div class="form-group">
						<label for="">Name</label>
						<input type="text" class="form-control" id="inputPBName" />
					</div>
					<div class="form-group">
						<label for="">Description</label>
						<input type="text" class="form-control" id="inputPBDescription" />
					</div>
					<div class="form-group">
						<label for="">Year</label>
						<input type="text" class="form-control dpYears" id="inputPBYear" />
					</div>
					<div class="form-group">
						<label for="">ISBN</label>
						<input type="text" class="form-control" id="inputPBISBN" />
					</div>
					<div class="form-group">
						<button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnPublication">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--Modal for Teachings-->
<div aria-hidden="true" aria-labelledby="teachingModal" role="dialog" tabindex="-1" id="teachingModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h4 class="modal-title">
					Add Teaching
				</h4>
			</div>
			<div class="modal-body">
				<form action="">
					<div class="form-group">
						<p class="help-block text-danger"></p>
					</div>
					<div class="form-group">
						<label for="">Title</label>
						<input type="text" class="form-control" id="inputTCTitle" />
					</div>
					<div class="form-group">
						<label for="">Description</label>
						<input type="text" class="form-control" id="inputTCDescription" />
					</div>
					<div class="form-group">
						<label for="">From Year</label>
						<input type="text" class="form-control dpYears" id="inputTCFrom" />
					</div>
					<div class="form-group">
						<label for="">To Year<small>(Leave blank for present)</small></label>
						<input type="text" class="form-control dpYears" id="inputTCTo" />
					</div>
					<div class="form-group">
						<button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnTeaching">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--Modal for Gallery-->
<div aria-hidden="true" aria-labelledby="galleryModal" role="dialog" tabindex="-1" id="galleryModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h4 class="modal-title">
					Add Gallery Image
				</h4>
			</div>
			<div class="modal-body">
				<form action="">
					<div class="form-group">
						<p class="help-block text-danger"></p>
					</div>
					<div class="form-group">
                        <div class="profile-pic">
                            <div class="thumbnail">
                                <img id="imgGIImage" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span> Select image </span>
                                    <input id="fileGIImage" type="file" name="fileGIImage"> </span>
                            </div>
                        </div>
                        <input type="hidden" id="inputGIImage" name="inputGIImage" value="">
                    </div>
					<div class="form-group">
						<label for="">Title</label>
						<input type="text" class="form-control" id="inputGITitle" />
					</div>
					<div class="form-group">
						<label for="">Description</label>
						<input type="text" class="form-control" id="inputGIDescription" />
					</div>
					<div class="form-group">
						<button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnGalleryImage">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--Modal for contacts-->
<div aria-hidden="true" aria-labelledby="contactModal" role="dialog" tabindex="-1" id="contactModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h4 class="modal-title">
					Add Contact
				</h4>
			</div>
			<div class="modal-body">
				<form action="">
					<div class="form-group">
						<p class="help-block text-danger"></p>
					</div>
					<div class="form-group">
						<select class="form-control" id="selectCIType">
							<option value="phone">Phone</option>
							<option value="email">Email</option>
							<option value="skype">Skype</option>
							<option value="facebook">Facebook</option>
							<option value="twitter">Twitter</option>
							<option value="linkedin">Linkedin</option>
						</select>
					</div>
					<div class="form-group">
						<label for="">Title</label>
						<input type="text" class="form-control" id="inputCITitle" />
					</div>
					<div class="form-group">
						<label for="">Value</label>
						<input type="text" class="form-control" id="inputCIValue" />
					</div>
					<div class="form-group">
						<button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnContact">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>