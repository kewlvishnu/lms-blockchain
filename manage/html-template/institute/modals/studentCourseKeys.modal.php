<div id="modalStudentCourseKeys" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Consumption Detail</h4>
            </div>
            <div class="modal-body">
                <table id="tableCourseDetails" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th><?php echo $global->terminology["student_single"]; ?> Name</th>
                            <th>Email Id</th>
                            <th>Contact No.</th>
                            <th>Enrollment Key</th>
                            <th>Enrollment Date</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>