<div id="modalSubjectProfessors" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo $global->terminology["subject_single"]; ?> <?php echo $global->terminology["instructor_plural"]; ?></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form-row-seperated">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-xs-6 text-right">
                                <h4>Available <?php echo $global->terminology["instructor_plural"]; ?></h4>
                            </div>
                            <div class="col-xs-6 text-left">
                                <h4>Selected <?php echo $global->terminology["instructor_plural"]; ?></h4>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group select2-dip text-center" id="listProfessors">
                            <!-- <select multiple="multiple" class="multi-select" id="my_multi_select1" name="my_multi_select1[]">
                                <option value="1">Naval Engineering - 2016</option>
                                <option value="2" selected="">Naval Acad -2017</option>
                                <option value="3" selected="">Engineering Demo</option>
                                <option value="4">Demostration</option>
                                <option value="5">Demonstartion</option>
                                <option value="6" selected="">Loads of Crap</option>
                                <option value="7">Its all crap</option>
                                <option value="8">Testing crap</option>
                                <option value="9">Course Pack 1</option>
                            </select> -->
                        </div>
                    </div>
                </form>
                <!-- <table class="table table-striped table-bordered table-hover order-column no-margin" id="tableInstructors">
                    <tbody>
                        <tr class="odd gradeX">
                            <td>
                                <a href="<?php //echo $sitepathInstituteSubjects; ?>1" target="_blank">Professor 1</a> 
                                <div class="pull-right">
                                    <a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i> Delete</a>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table> -->
            </div>
            <div class="modal-footer">
                <a href="<?php echo $sitepathInstituteInstructors; ?>invite" class="btn blue btn-outline">Invite Professors</a>
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>