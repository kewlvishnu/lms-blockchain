<?php
	$rreClass = $ralHtml = $mplClass = '';
	if ($courseDetails->courseDetails["approved"] == -1) {
		$rreClass = 'hide';
		$ralHtml = '<button type="button" class="btn btn-fit-height btn-success request-approval-link"> Send Request</button>';
        $mplClass = 'hide';
    } else if ($courseDetails->courseDetails["approved"] == 0) {
		$rreClass = 'hide';
		$ralHtml = '<button type="button" class="btn btn-fit-height btn-warning" disabled> <i class="fa fa-user"></i> Approval Pending</button>';
        $mplClass = 'hide';
    } else if ($courseDetails->courseDetails["approved"] == 1) {
		$rreClass = 'hide';
		$ralHtml = '<button type="button" class="btn btn-fit-height btn-success request-approval-link hide"> Send Request</button>';
        //$('#avail-student, #avail-professor').show();
    } else if ($courseDetails->courseDetails["approved"] == 2) {
		$ralHtml = '<button type="button" class="btn btn-fit-height btn-danger request-approval-link"> <i class="fa fa-user"></i> Resend Approval</button>';
        $mplClass = 'hide';
    }
?>
<h1 class="page-title"> <?php echo $courseDetails->courseDetails["name"]; ?>
	<small><?php echo $courseDetails->courseDetails["subtitle"]; ?></small>
</h1>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?php echo $sitepathManage; ?>">Home</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<i class="icon-list"></i>
			<a href="<?php echo $sitepathManageCourses; ?>"><?php echo $global->terminology["course_plural"]; ?></a>
			<i class="fa fa-angle-right"></i>
		</li>
		<?php
			if ($page == 'courseDetail') { ?>
		<li>
			<span><?php echo $courseDetails->courseDetails["name"]; ?> <small>(ID - C00<?php echo $courseDetails->courseDetails["id"]; ?>)</small></span>
		</li>
		<?php
			} else { ?>
		<li>
			<a href="<?php echo $sitepathManageCourses.$courseId; ?>"><?php echo $courseDetails->courseDetails["name"]; ?> <small>(ID - C00<?php echo $courseId; ?>)</small></a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<span> Assign <?php echo $global->terminology["subject_plural"]; ?> </span>
		</li>
		<?php
			} ?>
	</ul>
	<div class="page-toolbar">
		<div class="btn-group pull-right">
			<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"> Actions
				<i class="fa fa-angle-down"></i>
			</button>
			<ul class="dropdown-menu pull-right" role="menu">
				<li class="<?php echo $mplClass; ?>">
					<a href="#" data-toggle="modal" data-target="#modalMarketplaceSettings" class="green market-place-link "> Marketplace Settings</a>
				</li>
				<li>
					<a href="#" data-toggle="modal" data-target="#modalUploadDemoVideo"> Upload Demo Video</a>
				</li>
				<li>
					<a href="#" data-toggle="modal" data-target="#modalUploadCourseDocument"> Upload <?php echo $global->terminology["course_single"]; ?> Document</a>
				</li>
			</ul>
		</div>
		<div class="btn-group pull-right">
			<button type="button" id="resultResend" class="btn btn-fit-height red <?php echo $rreClass; ?>" disabled> <?php echo $global->terminology["course_single"]; ?> Rejected</button>
			<?php echo $ralHtml; ?>
			<!-- <button type="button" data-toggle="modal" data-target="#modalMarketplaceSettings" class="btn btn-fit-height green market-place-link <?php //echo $mplClass; ?>"> Marketplace Settings</button> -->
		</div>
		<!-- <div class="btn-group pull-right">
			<button type="button" data-toggle="modal" data-target="#modalUploadDemoVideo" class="btn btn-fit-height blue"> Upload Demo Video</button>
			<button type="button" data-toggle="modal" data-target="#modalUploadCourseDocument" class="btn btn-fit-height green-jungle"> Upload Course Document</button>
		</div> -->
	</div>
</div>