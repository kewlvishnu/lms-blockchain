<!-- BEGIN PROFILE SIDEBAR -->
<div class="profile-sidebar portfolio-sidebar">
    <!-- PORTLET MAIN -->
    <div class="portlet light profile-sidebar-portlet ">
        <!-- SIDEBAR USERPIC -->
        <div class="profile-userpic">
            <img src="<?php echo $portfolioDetails->portfolio['img']; ?>" class="img-responsive" alt=""> </div>
        <!-- END SIDEBAR USERPIC -->
        <!-- SIDEBAR USER TITLE -->
        <div class="profile-usertitle">
            <div class="profile-usertitle-name js-institute"> <?php echo $portfolioDetails->portfolio['name']; ?> </div>
            <div class="profile-usertitle-job"> Portfolio </div>
        </div>
        <!-- END SIDEBAR USER TITLE -->
        <?php /*
        <!-- SIDEBAR BUTTONS -->
        <div class="profile-userbuttons">
            <button type="button" class="btn btn-circle blue-hoki btn-sm">Follow</button>
            <button type="button" class="btn btn-circle red btn-sm">Message</button>
        </div> */ ?>
        <!-- END SIDEBAR BUTTONS -->
        <!-- SIDEBAR MENU -->
        <div class="profile-usermenu">
            <ul class="nav">
                <li class="<?php echo (($page == 'portfolio' && $subPage == 'portfolioEdit')?'active':'') ; ?>">
                    <a href="<?php echo $sitepathManagePortfolio; ?>">
                        <i class="fa fa-edit"></i> Edit Portfolio </a>
                </li>
                <li class="<?php echo (($page == 'portfolio' && $subPage == 'portfolioPages')?'active':'') ; ?>">
                    <a href="<?php echo $sitepathManagePortfolio; ?>pages">
                        <i class="fa fa-indent"></i> Portfolio Pages </a>
                </li>
                <li class="<?php echo (($page == 'portfolio' && $subPage == 'portfolioMenu')?'active':'') ; ?>">
                    <a href="<?php echo $sitepathManagePortfolio; ?>menu">
                        <i class="fa fa-bars"></i> Portfolio Menus </a>
                </li>
            </ul>
        </div>
        <!-- END MENU -->
    </div>
    <!-- END PORTLET MAIN -->
    <!-- PORTLET MAIN -->
    <div class="portlet light ">
        <!-- STAT -->
        <div class="row list-separated profile-stat">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="uppercase profile-stat-title js-menus"> <?php echo $portfolioDetails->portfolio['menus']; ?> </div>
                <div class="uppercase profile-stat-text"> Menus </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="uppercase profile-stat-title js-pages"> <?php echo $portfolioDetails->portfolio['pages']; ?> </div>
                <div class="uppercase profile-stat-text"> Pages </div>
            </div>
        </div>
        <!-- END STAT -->
        <div>
            <h4 class="profile-desc-title">Portfolio Description</h4>
            <span class="profile-desc-text" id="description"> <?php echo $portfolioDetails->portfolio['description']; ?> </span>
            <?php if (isset($portfolioDetails->portfolio['facebook']) && !empty($portfolioDetails->portfolio['facebook'])) {
                $socialName = explode("/", $portfolioDetails->portfolio['facebook']);
                if (!empty(end($socialName))) {
                    $socialName = end($socialName);
                } else {
                    $socialName = $socialName[count($socialName)-2];
                }
                ?>
            <div class="margin-top-20 profile-desc-link js-facebook">
                <i class="fa fa-facebook">
                <a href="<?php echo $portfolioDetails->portfolio['facebook']; ?>"></i> <?php echo $socialName; ?></a>
            </div>
            <?php } ?>
            <?php if (isset($portfolioDetails->portfolio['twitter']) && !empty($portfolioDetails->portfolio['twitter'])) {
                $socialName = explode("/", $portfolioDetails->portfolio['twitter']);
                if (!empty(end($socialName))) {
                    $socialName = end($socialName);
                } else {
                    $socialName = $socialName[count($socialName)-2];
                }
                ?>
            <div class="margin-top-20 profile-desc-link js-twitter">
                <i class="fa fa-twitter"></i>
                <a href="<?php echo $portfolioDetails->portfolio['twitter']; ?>">@<?php echo $socialName; ?></a>
            </div>
            <?php } ?>
            <?php if (isset($portfolioDetails->portfolio['linkedin']) && !empty($portfolioDetails->portfolio['linkedin'])) {
                $socialName = explode("/", $portfolioDetails->portfolio['linkedin']);
                if (!empty(end($socialName))) {
                    $socialName = end($socialName);
                } else {
                    $socialName = $socialName[count($socialName)-2];
                }
                ?>
            <div class="margin-top-20 profile-desc-link js-linkedin">
                <i class="fa fa-linkedin"></i>
                <a href="<?php echo $portfolioDetails->portfolio['linkedin']; ?>"><?php echo $socialName; ?></a>
            </div>
            <?php } ?>
        </div>
    </div>
    <!-- END PORTLET MAIN -->
</div>
<!-- END BEGIN PROFILE SIDEBAR -->