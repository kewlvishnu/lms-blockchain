<h1 class="page-title"> <?php echo $subjectDetails["name"]; ?> </h1>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?php echo $sitepathManageInstitute; ?>">Home</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<i class="icon-list"></i>
			<a href="<?php echo $sitepathInstituteCourses; ?>"><?php echo $global->terminology["course_plural"]; ?></a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="<?php echo $sitepathInstituteCourses.$courseId; ?>"><?php echo $courseDetails["name"]; ?></a>
			<i class="fa fa-angle-right"></i>
		</li>
		<?php
			if ($page == 'subjectDetail') { ?>
		<li>
			<span><?php echo $subjectDetails["name"]; ?> <small>(ID - S0<?php echo $subjectDetails["id"]; ?>)</small></span>
		</li>
		<?php
			} else { ?>
		<li>
			<a href="<?php echo $sitepathInstituteSubjects.$subjectId; ?>"><?php echo $subjectDetails["name"]; ?> </a>
			<i class="fa fa-angle-right"></i>
		</li>
		<?php
			if ($page == 'subjectStudents') { ?>
		<li>
			<span> <?php echo $global->terminology["student_plural"]; ?> </span>
		</li>
		<?php
			} else if ($page == 'subjectSections') { ?>
		<li>
			<span> Sections </span>
		</li>
		<?php
			} else if ($page == 'subjectAnalytics') { ?>
		<li>
			<span> Content Analytics </span>
		</li>
		<?php
			} else if ($page == 'subjectGrading') { ?>
		<li>
			<span> <?php echo $global->terminology["subject_single"]; ?> Grading </span>
		</li>
		<?php
			} else if ($page == 'subjectPerformance') { ?>
		<li>
			<span> <?php echo $global->terminology["subject_single"]; ?> Performance </span>
		</li>
		<?php
			} else if ($page == 'studentPerformance') { ?>
		<li>
			<span> <?php echo $global->terminology["student_single"]; ?> Performance </span>
		</li>
		<?php
			} else { ?>
		<li>
			<span> Exams </span>
		</li>
		<?php
			} ?>
		<?php
			} ?>
	</ul>
    <div class="page-toolbar">
		<div class="btn-group pull-right">
			<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"> <i class="fa fa-plus"></i> Add New
				<i class="fa fa-angle-down"></i>
			</button>
			<ul class="dropdown-menu pull-right" role="menu">
				<li>
					<a href="<?php echo $sitepathInstituteSubjects.$subjectId; ?>/sections">
						<i class="icon-social-youtube"></i> Section</a>
				</li>
				<li>
					<a href="<?php echo $sitepathInstituteSubjects.$subjectId; ?>/exams/create">
						<i class="icon-note"></i> Exam/Assignment</a>
				</li>
				<li>
					<a href="<?php echo $sitepathInstituteSubjects.$subjectId; ?>/subjective-exams/create">
						<i class="icon-note"></i> Subjective Exam</a>
				</li>
				<li>
					<a href="<?php echo $sitepathInstituteSubjects.$subjectId; ?>/manual-exams/create">
						<i class="icon-note"></i> Manual Exam</a>
				</li>
				<li>
					<a href="<?php echo $sitepathInstituteSubjects.$subjectId; ?>/submissions/create">
						<i class="icon-note"></i> Submission</a>
				</li>
			</ul>
		</div>
		<div class="btn-group pull-right">
			<button type="button" class="btn btn-fit-height grey-salsa dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"> <i class="fa fa-gavel"></i> Manage
				<i class="fa fa-angle-down"></i>
			</button>
			<ul class="dropdown-menu pull-right" role="menu">
				<li>
					<a data-toggle="modal" href="#modalSubjectProfessors"> <i class="fa fa-users"></i> <?php echo $global->terminology["subject_single"]; ?> <?php echo $global->terminology["instructor_plural"]; ?></a>
				</li>
				<li>
					<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>/students"> <i class="fa fa-users"></i> <?php echo $global->terminology["subject_single"]; ?> <?php echo $global->terminology["student_plural"]; ?></a>
				</li>
				<li>
					<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>/students/groups"> <i class="fa fa-users"></i> <?php echo $global->terminology["student_single"]; ?> Groups</a>
				</li>
				<li>
					<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>/analytics"> <i class="fa fa-bar-chart"></i> Content Analytics</a>
				</li>
				<li>
					<a data-toggle="modal" href="#modalUploadSubjectSyllabus"> <i class="fa fa-file-pdf-o"></i> Upload <?php echo $global->terminology["syllabus_single"]; ?></a>
				</li>
				<li>
					<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>/grading"> <i class="fa fa-star"></i> Grading (Overall)</a>
				</li>
				<li>
					<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>/rewards"> <i class="fa fa-star"></i> Rewards</a>
				</li>
				<li>
					<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>/performance"> <i class="fa fa-star"></i> <?php echo $global->terminology["student_plural"]; ?> Performance</a>
				</li>
			</ul>
		</div>
		<div class="btn-group pull-right">
			<button type="button" class="btn btn-fit-height grey-cascade dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"> <i class="fa fa-question"></i> Question Banks
				<i class="fa fa-angle-down"></i>
			</button>
			<ul class="dropdown-menu pull-right" role="menu">
				<li>
					<a href="<?php echo $sitepathInstituteSubjects.$subjectId; ?>/questions"> Objective Questions</a>
				</li>
				<li>
					<a href="<?php echo $sitepathInstituteSubjects.$subjectId; ?>/subjective-questions"> Subjective Questions</a>
				</li>
				<li>
					<a href="<?php echo $sitepathInstituteSubjects.$subjectId; ?>/submission-questions"> Submission Questions</a>
				</li>
			</ul>
		</div>
	</div>
</div>