<h1 class="page-title"> <?php echo $subjectDetails["name"]; ?> </h1>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?php echo $sitepathManage; ?>">Home</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<i class="icon-list"></i>
			<a href="<?php echo $sitepathManageCourses; ?>"><?php echo $global->terminology["course_plural"]; ?></a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="<?php echo $sitepathManageCourses.$courseId; ?>"><?php echo $courseDetails["name"]; ?></a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>"><?php echo $subjectDetails["name"]; ?> </a>
			<i class="fa fa-angle-right"></i>
		</li>
		<?php
			if ($page == 'subjectQuestions') { ?>
		<li>
			<span> Question Bank </span>
		</li>
		<?php
			} else if ($page == 'subjectSubjectiveQuestions') { ?>
		<li>
			<span> Subjective Questions Bank </span>
		</li>
		<?php
			} else if ($page == 'subjectSubmissionQuestions') { ?>
		<li>
			<span> Submission Questions Bank </span>
		</li>
		<?php
			} ?>
	</ul>
    <div class="page-toolbar">
		<div class="btn-group pull-right">
			<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>" class="btn btn-fit-height blue"> Back to <?php echo $global->terminology["subject_single"]; ?></a>
		</div>
	</div>
</div>