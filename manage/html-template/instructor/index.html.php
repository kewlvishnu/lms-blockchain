<?php
	require_once('includes/header.php');
	require_once('includes/sidebar.php');
	
	switch ($page) {
		case 'home':
			require_once('pages/home.inc.php');
			break;
		/*case 'courses':
			require_once('pages/courses.inc.php');
			break;*/
		case 'courseDetail':
			require_once('pages/courseDetail.inc.php');
			break;
		case 'subjectDetail':
			require_once('pages/subjectDetail.inc.php');
			break;
		/*case 'subjectSections':
			require_once('pages/subjectSections.inc.php');
			break;*/
		/*case 'subjectStudents':
			require_once('pages/subjectStudents.inc.php');
			break;*/
		/*case 'subjectAnalytics':
			require_once('pages/subjectAnalytics.inc.php');
			break;*/
		case 'profile':
			require_once('pages/profile.inc.php');
			break;
		case 'notificationsActivity':
			require_once('pages/notificationsActivity.inc.php');
			break;
		case 'notificationsChat':
			require_once('pages/notificationsChat.inc.php');
			break;
		case 'instituteInvitations':
			require_once('pages/instituteInvitations.inc.php');
			break;
		case 'subjectsAssigned':
			require_once('pages/subjectsAssigned.inc.php');
			break;
		case 'subjectChat':
			require_once('pages/subjectChat.inc.php');
			break;
		case 'subjectParents':
			require_once('pages/subjectParents.inc.php');
			break;
		default:
			echo "There was some error";
			break;
	}
	require_once('includes/footer.php');
?>