<div id="modalNewParentNotification" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><span class="js-section-action">New</span> Notification</h4>
            </div>
            <div class="modal-body">
                <!-- BEGIN FORM-->
                <form role="form" id="formParentNotification">
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> Section title is updated! </div>
                        <div class="form-group margin-top-20">
                            <label for="notification">Notification
                                <span class="required"> * </span>
                            </label>
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <textarea class="form-control" name="notification" id="notification" rows="5"> </textarea>
                            </div>
                        </div>
                        <input type="hidden" id="parentId" name="parentId">
                        <div class="note note-success">
                            <p>Please enter notification to send to the <?php echo $global->terminology["parent_plural"]; ?>.</p>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                <button id="btnSaveNotification" type="submit" class="btn blue btn-outline"><i class="fa fa-save"></i> Save</button>
            </div>
        </div>
    </div>
</div>