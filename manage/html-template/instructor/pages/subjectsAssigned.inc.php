<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
        <h1 class="page-title"> <?php echo $global->terminology["subject_plural"]; ?> Assigned </h1>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo $sitepathManage; ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span><?php echo $global->terminology["subject_plural"]; ?> Assigned</span>
                </li>
            </ul>
            <div class="page-toolbar"> </div>
        </div>
        <!-- END PAGE HEADER-->
		<div class="row" id="listCourses"> </div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->