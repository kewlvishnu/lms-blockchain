<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<?php
			require_once('html-template/instructor/includes/header-course.php');
		?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<?php
					require_once('html-template/instructor/includes/sidebar-course.php');
				?>
				<!-- BEGIN TICKET LIST CONTENT -->
				<div class="app-ticket app-ticket-list">
					<!-- <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 blue" href="javascript:;">
                                <div class="visual">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo $courseDetails->subjects; ?>"><?php echo $courseDetails->subjects; ?></span>
                                    </div>
                                    <div class="desc"> Subjects </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 red" href="<?php echo $sitepathInstructorStudents; ?>">
                                <div class="visual">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" id="jsCourseStudents" data-value="0">0</span> </div>
                                    <div class="desc"> Students </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 green" href="<?php echo $sitepathInstructorKeys; ?>">
                                <div class="visual">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" id="jsPending" data-value="0">0</span>
                                    </div>
                                    <div class="desc"> Pending enrolls </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 purple" href="<?php echo $sitepathInstructorKeys; ?>">
                                <div class="visual">
                                    <i class="fa fa-dollar"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" id="jsRemaining" data-value="0">0</span>
                                    </div>
                                    <div class="desc"> Remaining keys </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="clearfix"></div> -->
					<div class="row">
						<div class="col-md-12">
							<div class="portlet light ">
								<div class="portlet-title tabbable-line">
									<div class="caption caption-md">
										<i class="icon-globe theme-font hide"></i>
										<span class="caption-subject font-blue-madison bold uppercase"><?php echo $global->terminology["course_single"]; ?> <?php echo $global->terminology["subject_plural"]; ?></span>
									</div>
									<div class="actions">
										<a href="javascript:void(0)" data-action="new" data-toggle="modal" class="btn btn-primary btn-sm btn-subject">
											<i class="fa fa-plus"></i> Add New <?php echo $global->terminology["subject_single"]; ?> </a>
										<a href="#modalImportSubject" data-toggle="modal" class="btn btn-primary btn-sm">
											<i class="fa fa-download"></i> Import <?php echo $global->terminology["subject_single"]; ?> </a>
									</div>
								</div>
								<div class="portlet-body">
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover table-checkable order-column" id="jsSubjects">
											<thead>
												<tr>
													<th> ID # </th>
													<th> <?php echo $global->terminology["subject_single"]; ?> </th>
													<th> Lectures </th>
													<th> Exams </th>
													<th> <?php echo $global->terminology["student_plural"]; ?> </th>
													<th colspan="2"> Actions </th>
												</tr>
											</thead>
											<tbody>
												<!-- <tr class="odd gradeX">
													<td>420</td>
													<td> <a href="<?php //echo $sitepathManageSubjects; ?>1">Power Electronics</a> </td>
													<td> 42 </td>
													<td> 64 </td>
													<td> 60 </td>
													<td>
														<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-edit"></i></a> 
														<a href="javascript:void(0)" class="btn btn-circle btn-sm red"><i class="fa fa-trash"></i></a>
													</td>
												</tr> -->
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="portlet light ">
								<div class="portlet-title tabbable-line">
									<div class="caption caption-md">
										<i class="icon-globe theme-font hide"></i>
										<span class="caption-subject font-blue-madison bold uppercase"><?php echo $global->terminology["course_single"]; ?> <?php echo $global->terminology["student_plural"]; ?></span>
									</div>
									<div class="actions">
										<a href="<?php echo $sitepathManageCourses.$courseId; ?>/assign/" class="btn btn-primary btn-sm" id="btnAssignSubjects">
											<i class="fa fa-bars"></i> Assign <?php echo strtolower($global->terminology["subject_plural"]); ?> </a>
										<?php if (isset($global->walletAddress) && !empty($global->walletAddress)) { ?>
										<button class="btn btn-primary btn-purchase-keys" data-wallet="<?php echo $global->walletAddress; ?>"> Enrollment Keys (<?php echo $global->displayCurrency; ?>)</button>
										<?php } else { ?>
										<button class="btn btn-primary btn-purchase-keys"> Enrollment Keys (<?php echo $global->displayCurrency; ?>)</button>
										<?php } ?>
									</div>
								</div>
								<div class="portlet-body">
									<div class="table-toolbar">
										<div class="row">
											<div class="col-md-10">
												<div class="btn-group clearfix">
												    <a class="btn green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
												        <i class="fa fa-angle-down"></i>
												    </a>
												    <ul class="dropdown-menu">
												        <li>
												            <a href="#modalGenerateStudents" data-toggle="modal"> Add New
																<i class="fa fa-plus"></i>
															</a>
												        </li>
												        <li>
												            <a href="#modalInviteStudents" data-toggle="modal"> Invite <?php echo $global->terminology["student_plural"]; ?> <i class="fa fa-plus"></i>
															</a>
												        </li>
												        <li class="divider"> </li>
												        <li>
												            <a href="javascript:;" id="btnRemoveStudents"> Remove
																<i class="fa fa-trash"></i>
															</a>
												        </li>
												        <li>
												            <a href="javascript:;" id="btnParentCredentials"> <?php echo $global->terminology["parent_single"]; ?>
																<i class="fa fa-user"></i>
															</a>
												        </li>
												    </ul>
												</div>
												<!-- <div class="btn-group margin-bottom-10">
													<button href="#modalGenerateStudents" data-toggle="modal" class="btn sbold green"> Add New
														<i class="fa fa-plus"></i>
													</button>
													<button href="#modalInviteStudents" data-toggle="modal" class="btn sbold blue"> Invite Students
														<i class="fa fa-plus"></i>
													</button>
												</div>
												<div class="btn-group margin-bottom-10">
													<button id="btnRemoveStudents" class="btn sbold red"> Remove
														<i class="fa fa-trash"></i>
													</button>
													<button id="btnParentCredentials" class="btn sbold yellow"> Parent
														<i class="fa fa-user"></i>
													</button>
												</div> -->
											</div>
										</div>
									</div>
									<table class="table table-striped table-bordered table-hover table-checkable order-column" id="tblCourseStudents">
										<thead>
											<tr>
												<th>
													<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
														<input type="checkbox" class="group-checkable" data-set="#tblCourseStudents .checkboxes" />
														<span></span>
													</label>
												</th>
												<th> ID # </th>
												<th> Username </th>
												<th> Password </th>
												<th> Name </th>
												<th> Email </th>
												<th> <?php echo $global->terminology["parent_single"]; ?> <br><small>(username)</small> </th>
												<th> <?php echo $global->terminology["parent_single"]; ?> <br><small>(password)</small> </th>
											</tr>
										</thead>
										<tbody>
											<!-- <tr class="odd gradeX">
												<td>
													<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
														<input type="checkbox" class="checkboxes" value="1" />
														<span></span>
													</label>
												</td>
												<td>420</td>
												<td>student1</td>
												<td><div class="text-success">REGISTERED</div></td>
												<td> Jane </td>
												<td><a href="mailto:customer@gmail.com"> customer@gmail.com </a></td>
												<td>40356</td>
												<td>55315</td>
											</tr> -->
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END PROFILE CONTENT -->
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->