<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Manage Portfolio Menu</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <form role="form" action="#" id="frmPortfolioMenu">
                        <div class="form-body">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                            <div class="alert alert-success display-hide">
                                <button class="close" data-close="alert"></button> Your profile is successfully updated! </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <h3>Menu Information</h3>
                                    <hr>
                                    <div class="form-group">
                                        <label for="selectMenuType">Menu Type</label>
                                        <select class="form-control" id="selectMenuType" name="selectMenuType">
                                            <option value="page">Page</option>
                                            <option value="url">Custom URL</option>
                                        </select>
                                        <input type="hidden" id="menuId" name="menuId" />
                                        <input type="hidden" id="menuType" name="menuType" />
                                    </div>
                                    <div id="pageBlock" class="menu-block">
                                        <div class="form-group">
                                            <label for="selectPortfolioPage">Pages</label>
                                            <select class="form-control" id="selectPortfolioPage" name="selectPortfolioPage"></select>
                                        </div>
                                    </div>
                                    <div id="urlBlock" class="menu-block hide">
                                        <div class="form-group">
                                            <label for="inputMenuUrl">URL</label>
                                            <input type="text" class="form-control" id="inputMenuUrl" name="inputMenuUrl" placeholder="Enter Menu URL">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputMenuName">Name</label>
                                        <input type="text" class="form-control" id="inputMenuName" name="inputMenuName" placeholder="Enter Menu Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputMenuDescription">Description</label>
                                        <textarea class="form-control" id="inputMenuDescription" name="inputMenuDescription" rows="5" placeholder="Enter Menu Description"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputMenuOrder">Menu Order</label>
                                        <input type="text" class="form-control" id="inputMenuOrder" name="inputMenuOrder" placeholder="Enter Menu Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="selectMenuStatus">Status</label>
                                        <select class="form-control" id="selectMenuStatus" name="selectMenuStatus">
                                            <option value="active">Active</option>
                                            <option value="inactive">Inactive</option>
                                        </select>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" id="btnSavePortfolioMenu" name="btnSavePortfolioMenu" class="btn green">Submit</button>
                                        <a href="javascript:;" class="btn default"> Cancel </a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h3>List of Menus</h3>
                                    <hr>
                                    <div class="js-portfolio-menus">
                                        <ul class="nav nav-pills nav-stacked list-pf-menu">
                                            <li><a href="javascript:void" class="js-menu selected"><span class="checkbox"><input type="radio" name="optionMenu" value="new" checked /> New</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>