<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Manage Portfolio Pages</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-8">
                            <h3>Page Information</h3>
                            <hr>
                            <form role="form" action="#" id="frmPortfolioPages">
                                <div class="form-body">
                                    <div class="alert alert-danger display-hide">
                                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                    <div class="alert alert-success display-hide">
                                        <button class="close" data-close="alert"></button> Your profile is successfully updated! </div>
                                    <div class="form-group">
                                        <label for="inputProfileName">Page Type :</label>
                                        <select class="form-control" id="selectPageType" name="selectPageType"></select>
                                        <input type="hidden" id="pageId" name="pageId" />
                                        <input type="hidden" id="pageType" name="pageType" />
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPageTitle">Page Title :</label>
                                        <input type="text" class="form-control" id="inputPageTitle" name="inputPageTitle" placeholder="Enter Page Title">
                                    </div>
                                    <div id="coursesBlock" class="page-block hide">
                                        <div class="form-group">
                                            <label for="inputCourses">Select <?php echo $global->terminology["course_plural"]; ?> :</label>
                                            <!-- <input type="text" class="form-control" id="inputCourses" name="inputCourses" placeholder="Select Courses" /> -->
                                            <div class="js-courses"></div>
                                        </div>
                                    </div>
                                    <div id="aboutBlock" class="page-block hide">
                                        <div class="form-group">
                                            <label for="inputAboutDescription">About Description</label>
                                            <textarea class="form-control" id="inputAboutDescription" rows="5" placeholder="Enter About Description"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <p class="lead">Academic Position</p>
                                            <hr>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputAcademicTitle">Title</label>
                                            <input type="text" class="form-control" id="inputAcademicTitle" name="inputAcademicTitle" value="Academic Position" placeholder="Academic Position Title" />
                                            <button class="btn btn-warning btn-modal" data-target="#academicPositionsModal">Add Academic Position</button>
                                            <div class="js-academic"></div>
                                        </div>
                                        <div class="form-group">
                                            <p class="lead">Education &amp; Training</p>
                                            <hr>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEducationTitle">Title</label>
                                            <input type="text" class="form-control" id="inputEducationTitle" name="inputEducationTitle" value="Education &amp; Training" placeholder="Education &amp; Training Title" />
                                            <button class="btn btn-warning btn-modal" data-target="#educationModal">Add Education &amp; Training</button>
                                            <div class="js-education"></div>
                                        </div>
                                        <div class="form-group">
                                            <p class="lead">Our Honors</p>
                                            <hr>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputHonorsTitle">Title</label>
                                            <input type="text" class="form-control" id="inputHonorsTitle" name="inputHonorsTitle" value="Our Honors" placeholder="Honors Section Title" />
                                            <button class="btn btn-warning btn-modal" data-target="#honorsModal">Add Our Honors Information</button>
                                            <div class="js-honors"></div>
                                        </div>
                                    </div>
                                    <div id="facultyBlock" class="page-block hide">
                                        <div class="form-group">
                                            <label for="inputFacultyDescription">Faculty Description</label>
                                            <textarea class="form-control" id="inputFacultyDescription" rows="5" placeholder="Enter Faculty Description"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-warning btn-modal" data-target="#facultyMembersModal">Add Faculty Member</button>
                                            <div class="js-faculty"></div>
                                        </div>
                                    </div>
                                    <div id="galleryBlock" class="page-block hide">
                                        <div class="form-group">
                                            <label for="inputGalleryDescription">Gallery Description</label>
                                            <textarea class="form-control" id="inputGalleryDescription" rows="5" placeholder="Enter Gallery Description"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-warning btn-modal" data-target="#galleryModal">Add Gallery Image</button>
                                            <div class="js-gallery"></div>
                                        </div>
                                    </div>
                                    <div id="contactBlock" class="page-block hide">
                                        <div class="form-group">
                                            <button class="btn btn-warning btn-modal" data-target="#contactModal">Add Contact</button>
                                            <div class="js-contact"></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputContactDescription">Contact Description</label>
                                            <textarea class="form-control" id="inputContactDescription" rows="5" placeholder="Enter Contact Description"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputOfficeDescription">Office Description</label>
                                            <textarea class="form-control" id="inputOfficeDescription" rows="5" placeholder="Enter Office Description"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputWorkDescription">Work Description</label>
                                            <textarea class="form-control" id="inputWorkDescription" rows="5" placeholder="Enter Work Description"></textarea>
                                        </div>
                                    </div>
                                    <div id="customBlock" class="page-block hide">
                                        <div class="form-group">
                                            <label for="inputPageContent">Page Content</label>
                                            <textarea class="form-control ckeditor" id="inputPageContent" name="inputPageContent" rows="10" placeholder="Enter Page Content"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" id="btnSavePortfolioPage" name="btnSavePortfolioPage" class="btn green">Save Page</button>
                                    <!-- <a href="javascript:;" class="btn default"> Reset </a> -->
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <h3>List of Pages</h3>
                            <hr>
                            <div class="js-portfolio-menus">
                                <div class="js-portfolio-pages">
                                    <ul class="nav nav-pills nav-stacked list-pf-menu">
                                        <li><a href="javascript:void(0)" class="js-page selected"><span class="checkbox"><input type="radio" name="optionPage" value="new" checked /> New</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>