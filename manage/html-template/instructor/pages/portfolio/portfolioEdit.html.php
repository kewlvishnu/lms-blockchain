<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Manage Portfolio</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1_1" data-toggle="tab">Portfolio Info</a>
                        </li>
                        <li>
                            <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <!-- PERSONAL INFO TAB -->
                        <div class="tab-pane active" id="tab_1_1">
                            <form role="form" action="#" id="formEditPortfolio">
                                <div class="form-body">
                                    <div class="alert alert-danger display-hide">
                                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                    <div class="alert alert-success display-hide">
                                        <button class="close" data-close="alert"></button> Your profile is successfully updated! </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h3>Basic Information</h3>
                                            <hr>
                                            <div class="form-group">
                                                <label class="control-label">Portfolio Name <span class="required" aria-required="true"> * </span></label>
                                                <input type="text" id="portfolioName" name="portfolioName" placeholder="e.g. Harvard University" class="form-control" value="<?php echo $portfolioDetails->portfolio['name']; ?>" /> </div>
                                            <div class="form-group">
                                                <label class="control-label">Portfolio Description <span class="required" aria-required="true"> * </span></label>
                                                <textarea name="portfolioDescription" id="portfolioDescription" cols="30" rows="10" class="form-control" placeholder="e.g. We want portfolio for our Mechanical Department of the same institute."><?php echo $portfolioDetails->portfolio['description']; ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Affiliation <span class="required" aria-required="true"> * </span></label>
                                                <input type="text" id="affiliation" name="affiliation" placeholder="e.g. Harvard University" class="form-control" value="<?php echo $portfolioDetails->portfolio['affiliation']; ?>" /> </div>
                                            <div class="form-group">
                                                <label class="control-label">Subdomain <span class="required" aria-required="true"> * </span></label>
                                                <input type="text" id="inputSubdomain" name="inputSubdomain" class="form-control input-inline input-medium" value="<?php echo $subdomain; ?>.integro.io" disabled />
                                                <span class="help-inline"> </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h3>Social Information</h3>
                                            <hr>
                                            <div class="form-group">
                                                <label class="control-label">Facebook</label>
                                                <input type="text" id="inputFacebook" name="inputFacebook" placeholder="e.g. Harvard University" class="form-control" value="<?php echo $portfolioDetails->portfolio['facebook']; ?>" /> </div>
                                            <div class="form-group">
                                                <label class="control-label">Twitter</label>
                                                <input type="text" name="inputTwitter" id="inputTwitter" cols="30" rows="10" class="form-control" placeholder="e.g. We want portfolio for our Mechanical Department of the same institute." value="<?php echo $portfolioDetails->portfolio['twitter']; ?>" /> </div>
                                            <div class="form-group">
                                                <label class="control-label">Linkedin</label>
                                                <input type="text" name="inputLinkedin" id="inputLinkedin" cols="30" rows="10" class="form-control" placeholder="e.g. We want portfolio for our Mechanical Department of the same institute." value="<?php echo $portfolioDetails->portfolio['linkedin']; ?>" /> </div>
                                            <div class="form-group">
                                                    <label for="">Status :</label>
                                                    <select id="selectStatus" name="selectStatus" class="form-control">
                                                        <option value="Draft">Draft</option>
                                                        <option value="Published" selected="">Published</option>
                                                    </select> </div>
                                            <div class="form-group">
                                                <p> Please select an image for your portfolio. </p>
                                                <form action="#" role="form">
                                                    <div class="form-group">
                                                        <div class="profile-pic">
                                                            <div class="thumbnail">
                                                                <img id="portfolioFavicon" src="<?php echo $favicon; ?>" alt="" /> </div>
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span> Select image </span>
                                                                    <input id="fileFavicon" type="file" name="fileFavicon"> </span>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" id="faviconPath" name="faviconPath" value="<?php echo $favicon; ?>">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" id="btnUpdatePortfolio" name="btnUpdatePortfolio" class="btn green">Submit</button>
                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                </div>
                            </form>
                        </div>
                        <!-- END PERSONAL INFO TAB -->
                        <!-- CHANGE AVATAR TAB -->
                        <div class="tab-pane" id="tab_1_2">
                            <p> Please select an image for your profile picture. </p>
                            <form action="#" role="form">
                                <!-- <div class="form-group">
                                    <input type="file" />
                                    <img class="crop" style="display:none" />
                                    <button type="submit" style="display:none">Upload</button>
                                </div> -->
                                <div class="form-group">
                                    <div class="profile-pic">
                                        <div class="thumbnail">
                                            <img id="userProfilePic" src="<?php echo $portfolioDetails->portfolio['img']; ?>" alt="" /> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span> Change image </span>
                                                <input id="fileProfilePic" type="file" name="..."> </span>
                                            <a href="javascript:;" class="btn default js-remove"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="margin-top-10 hide js-upload">
                                    <a href="javascript:;" class="btn green" id="btnUpload">Upload</a>
                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                </div>
                            </form>
                        </div>
                        <!-- END CHANGE AVATAR TAB -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>