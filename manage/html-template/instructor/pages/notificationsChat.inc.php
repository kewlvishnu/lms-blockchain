<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h1 class="page-title"> Chat Notifications
            <small>your messenger invitations</small>
        </h1>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo $sitepathManageInstitute; ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Chat Notifications</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet light portlet-fit ">
                    <div class="portlet-title" id="pagerChats">
                        <div class="caption">
                            <i class="icon-microphone font-dark hide"></i>
                            <span class="caption-subject bold font-dark uppercase"> Chat Notifications</span>
                            <span class="caption-helper">Your all chat invitations</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="pagerChatsContent" class="mt-comments">
                            <!-- <div class="mt-comment chat-parent-all">
                                                        <div class="mt-comment-img">
                                                            <img src="assets/pages/media/users/avatar1.jpg" /> </div>
                                                        <div class="mt-comment-body">
                                                            <div class="mt-comment-info">
                                                                <span class="mt-comment-author">Michael Baker</span>
                                                                <span class="mt-comment-date">26 Feb, 10:30AM</span>
                                                            </div>
                                                            <div class="mt-comment-text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. </div>
                                                            <div class="mt-comment-details">
                                                                <span class="mt-comment-status mt-comment-status-pending">
                                                                    <a href="javascript:void(0)" class="chat-button" data-id="12" data-chat="1" target="_blank">Chat</a>
                                                                </span>
                                                                <ul class="mt-comment-actions">
                                                                    <li>
                                                                        <a href="javascript:void(0)" class="chat-button" data-id="12" data-chat="0">Mark as read</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-comment chat-parent-all disabled">
                                                        <div class="mt-comment-img">
                                                            <img src="assets/pages/media/users/avatar1.jpg" /> </div>
                                                        <div class="mt-comment-body">
                                                            <div class="mt-comment-info">
                                                                <span class="mt-comment-author">Michael Baker</span>
                                                                <span class="mt-comment-date">26 Feb, 10:30AM</span>
                                                            </div>
                                                            <div class="mt-comment-text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. </div>
                                                            <div class="mt-comment-details">
                                                                <span class="mt-comment-status mt-comment-status-pending">
                                                                    <a href="javascript:;" class="chat-button" target="_blank">Chat</a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-comment chat-parent-all">
                                                        <div class="mt-comment-img">
                                                            <img src="assets/pages/media/users/avatar1.jpg" /> </div>
                                                        <div class="mt-comment-body">
                                                            <div class="mt-comment-info">
                                                                <span class="mt-comment-author">Michael Baker</span>
                                                                <span class="mt-comment-date">26 Feb, 10:30AM</span>
                                                            </div>
                                                            <div class="mt-comment-text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. </div>
                                                            <div class="mt-comment-details">
                                                                <span class="mt-comment-status mt-comment-status-pending">
                                                                    <a href="javascript:void(0)" class="chat-button" data-id="12" data-chat="1" target="_blank">Chat</a>
                                                                </span>
                                                                <ul class="mt-comment-actions">
                                                                    <li>
                                                                        <a href="javascript:void(0)" class="chat-button" data-id="12" data-chat="0">Mark as read</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-comment chat-parent-all">
                                                        <div class="mt-comment-img">
                                                            <img src="assets/pages/media/users/avatar1.jpg" /> </div>
                                                        <div class="mt-comment-body">
                                                            <div class="mt-comment-info">
                                                                <span class="mt-comment-author">Michael Baker</span>
                                                                <span class="mt-comment-date">26 Feb, 10:30AM</span>
                                                            </div>
                                                            <div class="mt-comment-text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. </div>
                                                            <div class="mt-comment-details">
                                                                <span class="mt-comment-status mt-comment-status-pending">
                                                                    <a href="javascript:void(0)" class="chat-button" data-id="12" data-chat="1" target="_blank">Chat</a>
                                                                </span>
                                                                <ul class="mt-comment-actions">
                                                                    <li>
                                                                        <a href="javascript:void(0)" class="chat-button" data-id="12" data-chat="0">Mark as read</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     -->                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->