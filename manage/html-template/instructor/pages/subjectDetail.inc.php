<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<?php
			require_once('html-template/instructor/includes/header-subject.php');
		?>
		<!-- END PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<?php
					require_once('html-template/instructor/includes/sidebar-subject.php');
				?>
				<!-- BEGIN TICKET LIST CONTENT -->
				<div class="profile-content dashboard-metrics">
					<!-- BEGIN DASHBOARD STATS 1-->
					<!-- <div class="row">
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
							<a class="dashboard-stat dashboard-stat-v2 red" href="<?php //echo $sitepathManageSubjects.$subjectId; ?>/students">
								<div class="visual">
									<i class="fa fa-users"></i>
								</div>
								<div class="details">
									<div class="number">
										<span data-counter="counterup" data-value="<?php //echo $subjectDetails['students']; ?>"><?php //echo $subjectDetails['students']; ?></span> </div>
									<div class="desc"> Students </div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
							<a class="dashboard-stat dashboard-stat-v2 purple" href="<?php //echo (($subjectDetails['chapters']>0)?($sitepathManageContent.'subjects/'.$subjectId):('javascript:;')); ?>">
								<div class="visual">
									<i class="fa fa-users"></i>
								</div>
								<div class="details">
									<div class="number">
										<span data-counter="counterup" data-value="<?php //echo $subjectDetails['chapters']; ?>"><?php //echo $subjectDetails['chapters']; ?></span>
									</div>
									<div class="desc"> Lectures </div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
							<a class="dashboard-stat dashboard-stat-v2 green" data-scroll href="#listSections">
								<div class="visual">
									<i class="fa fa-dollar"></i>
								</div>
								<div class="details">
									<div class="number">
										<span data-counter="counterup" data-value="<?php //echo $subjectDetails['exams']; ?>"><?php //echo $subjectDetails['exams']; ?></span>
									</div>
									<div class="desc"> Exams </div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
							<a class="dashboard-stat dashboard-stat-v2 green-jungle" href="<?php //echo $sitepathManageSubjects.$subjectId; ?>/questions">
								<div class="visual">
									<i class="fa fa-dollar"></i>
								</div>
								<div class="details">
									<div class="number">
										<span data-counter="counterup" data-value="<?php //echo $subjectDetails['questions']; ?>"><?php //echo $subjectDetails['questions']; ?></span>
									</div>
									<div class="desc"> Questions </div>
								</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
							<a class="dashboard-stat dashboard-stat-v2 green-soft" href="<?php //echo $sitepathManageSubjects.$subjectId; ?>/subjective-questions">
								<div class="visual">
									<i class="fa fa-dollar"></i>
								</div>
								<div class="details">
									<div class="number">
										<span data-counter="counterup" data-value="<?php //echo $subjectDetails['subjective_questions']; ?>"><?php //echo $subjectDetails['subjective_questions']; ?></span>
									</div>
									<div class="desc"> Subjective <br> Questions </div>
								</div>
							</a>
						</div>
					</div>
					<div class="clearfix"></div> -->
					<!-- END DASHBOARD STATS 1-->
					<div class="row">
						<div class="col-sm-12" id="listSections"></div>
					</div>
					<div class="row">
						<div class="col-sm-12" id="listIndependent"></div>
					</div>
				</div>
				<!-- END PROFILE CONTENT -->
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->