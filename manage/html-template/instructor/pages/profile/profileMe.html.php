<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET -->
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Integro Experience</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-advance table-hover" id="tableArcaneExp">
                        <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-book"></i> <?php echo $global->terminology["course_single"]; ?> </th>
                                <th class="hidden-xs">
                                    <i class="fa fa-bars"></i> <?php echo $global->terminology["subject_plural"]; ?> </th>
                                <th>
                                    <i class="fa fa-users"></i> <?php echo $global->terminology["student_plural"]; ?> </th>
                                <th> Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- <tr>
                                <td>
                                    <a href="javascript:;"> Pixel Ltd </a>
                                </td>
                                <td class="hidden-xs"> <a href="javascript:void(0)">Power Electronics</a>, <a href="javascript:void(0)">Information Technology</a>, <a href="javascript:void(0)">Nano Technology</a> </td>
                                <td> 100 </td>
                                <td>
                                    <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                </td>
                            </tr> -->
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END PORTLET -->
        </div>
        <div class="col-md-12">
            <!-- BEGIN PORTLET -->
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">General Information</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="portlet sale-summary">
                                <div class="portlet-body">
                                    <ul class="list-unstyled">
                                        <li>
                                            <span class="sale-info"> <?php echo $global->terminology["instructor_single"]; ?> Name
                                                <i class="fa fa-img-up"></i>
                                            </span>
                                            <span class="sale-num js-instructor"> </span>
                                        </li>
                                        <li>
                                            <span class="sale-info"> Owner/Trust Name
                                                <i class="fa fa-img-down"></i>
                                            </span>
                                            <span class="sale-num js-owner"> Not Specified </span>
                                        </li>
                                        <li>
                                            <span class="sale-info"> Year Of Inception
                                                <i class="fa fa-img-down"></i>
                                            </span>
                                            <span class="sale-num js-year"> Not set </span>
                                        </li>
                                        <li>
                                            <span class="sale-info"> No. of <?php echo $global->terminology["course_plural"]; ?> </span>
                                            <span class="sale-num js-courses"> 0 </span>
                                        </li>
                                        <li>
                                            <span class="sale-info"> No. of <?php echo $global->terminology["student_plural"]; ?> </span>
                                            <span class="sale-num js-students"> Not Specified </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portlet sale-summary">
                                <div class="portlet-body">
                                    <ul class="list-unstyled">
                                        <li>
                                            <span class="sale-info"> Contact Person Name </span>
                                            <span class="sale-num js-contact-name"> Not Specified </span>
                                        </li>
                                        <li>
                                            <span class="sale-info"> Email
                                                <i class="fa fa-img-up"></i>
                                            </span>
                                            <span class="sale-num js-email"> </span>
                                        </li>
                                        <li>
                                            <span class="sale-info"> Contact No
                                                <i class="fa fa-img-up"></i>
                                            </span>
                                            <span class="sale-num js-contact"> </span>
                                        </li>
                                        <li>
                                            <span class="sale-info"> Country
                                                <i class="fa fa-img-down"></i>
                                            </span>
                                            <span class="sale-num js-country"> </span>
                                        </li>
                                        <li>
                                            <span class="sale-info"> Address </span>
                                            <span class="sale-num js-address"> </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PORTLET -->
        </div>
    </div>
</div>
<!-- END PROFILE CONTENT -->