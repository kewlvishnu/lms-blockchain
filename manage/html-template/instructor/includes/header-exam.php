<?php
	$page_title = '';
	if ($page == 'examCreate') {
		$page_title = 'New Objective Exam/Assignment';
	} else if ($page == 'examSettings') {
		$page_title = 'Edit '.$examDetails['name'].' Settings';
	} else if ($page == 'examDetail') {
		$page_title = $examDetails['type'].': '.$examDetails['name'].' Questions';
	} else if ($page == 'examQuestions') {
		$page_title = $examDetails['type'].': '.$examDetails['name'].' Questions';
	} else if ($page == 'examResult') {
        $page_title = $examDetails['type'].': '.$examDetails['name'];
    } else if ($page == 'examResultStudent') {
        $page_title = $examDetails['type'].': '.$examDetails['name'].' - '.$studentDetails['name'];
    } else if ($page == 'examObjComments') {
        $page_title = $examDetails['type'].': '.$examDetails['name'].' Comments';
    } else if ($page == 'subjectiveExamCreate') {
		$page_title = 'New Subjective Exam';
	} else if ($page == 'subjectiveExamSettings') {
		$page_title = 'Edit '.$examDetails['examName'].' Settings';
	} else if ($page == 'subjectiveExamQuestions') {
		$page_title = 'Subjective Exam: '.$examDetails['examName'].' Questions';
	} else if ($page == 'subjectiveExamResult') {
        $page_title = 'Subjective Exam: '.$examDetails['examName'];
    } else if ($page == 'subjectiveExamResultStudent') {
        $page_title = 'Subjective Exam: '.$examDetails['examName'].' - '.$studentDetails['name'];
    } else if ($page == 'subjectiveExamObjComments') {
        $page_title = 'Subjective Exam: '.$examDetails['examName'].' Comments';
    } else if ($page == 'manualExamCreate') {
		$page_title = 'New Manual Exam';
	} else if ($page == 'manualExamImport') {
		$page_title = 'Import Manual Exam';
	} else if ($page == 'manualExamSettings') {
		$page_title = 'Manual Exam: '.$examDetails['name'];
	} else if ($page == 'manualExamResult') {
		$page_title = 'Manual Exam: '.$examDetails['name'];
	} else if ($page == 'manualExamObjComments') {
		$page_title = 'Manual Exam: '.$examDetails['name'].' Comments';
	} else if ($page == 'submissionCreate') {
		$page_title = 'New Submission';
	} else if ($page == 'submissionSettings') {
		$page_title = 'Submission: '.$examDetails['name'];
	} else if ($page == 'submissionQuestions') {
		$page_title = 'Submission: '.$examDetails['name'];
	} else if ($page == 'submissionResult') {
        $page_title = 'Submission: '.$examDetails['name'];
    } else if ($page == 'submissionResultStudent') {
        $page_title = 'Submission: '.$examDetails['name'].' - '.$studentDetails['name'];
    } else if ($page == 'submissionObjComments') {
        $page_title = 'Submission: '.$examDetails['name'].' Comments';
    }
	/*var_dump($examDetails);
	die();*/
    /*var_dump($studentDetails);
    die();*/
?>
<h1 class="page-title"> <?php echo $page_title; ?> </h1>
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="<?php echo $sitepathManage; ?>">Home</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<i class="icon-list"></i>
				<a href="<?php echo $sitepathManageCourses; ?>"><?php echo $global->terminology["course_plural"]; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<i class="icon-list"></i>
				<a href="<?php echo $sitepathManageCourses.$courseId; ?>"><?php echo $courseDetails["name"]; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>"><?php echo $subjectDetails["name"]; ?> </a>
				<i class="fa fa-angle-right"></i>
			</li>
			<?php
				if ($page == 'examCreate') { ?>
			<li>
				<span>New Exam/Assignment</span>
			</li>
			<?php
				} else if ($page == 'examSettings') { ?>
			<li>
				<span><?php echo $examDetails['name']; ?> Settings</span>
			</li>
			<?php
				} else if ($page == 'examObjComments') { ?>
			<li>
				<a href="<?php echo $sitepathManageExams.$examId; ?>"><?php echo $examDetails['name']; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Objective Comments</span>
			</li>
			<?php
				} else if ($page == 'examDetail' || $page == 'examQuestions') { ?>
			<li>
				<span id="examType"><?php echo $examDetails['type']; ?></span><span>: </span><span id="examName"><?php echo $examDetails['name']; ?></span><span> Questions</span>
			</li>
			<?php
				} else if ($page == 'examResult') { ?>
			<li>
				<a href="<?php echo $sitepathManageExams.$examId; ?>"><?php echo $examDetails['name']; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Result</span>
			</li>
			<?php
				} else if ($page == 'examResultStudent') { ?>
			<li>
				<a href="<?php echo $sitepathManageExams.$examId; ?>"><?php echo $examDetails['name']; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a href="<?php echo $sitepathManageExams.$examId.'/result'; ?>">Result</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span><?php echo $studentDetails['name']; ?></span>
			</li>
			<?php
				} else if ($page == 'subjectiveExamCreate') { ?>
			<li>
				<span>New Subjective Exam</span>
			</li>
			<?php
				} else if ($page == 'subjectiveExamSettings') { ?>
			<li>
				<span><?php echo $examDetails['examName']; ?> Settings</span>
			</li>
			<?php
				} else if ($page == 'subjectiveExamObjComments') { ?>
			<li>
				<a href="<?php echo $sitepathManageSubjectiveExams.$examId; ?>"><?php echo $examDetails['examName']; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Subjective Comments</span>
			</li>
			<?php
				} else if ($page == 'subjectiveExamQuestions') { ?>
			<li>
				<span><?php echo $examDetails['examName']; ?> Questions</span>
			</li>
			<?php
				} else if ($page == 'subjectiveExamCheck') { ?>
			<li>
				<a href="<?php echo $sitepathManageSubjectiveExams.$examId; ?>"><?php echo $examDetails['examName']; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Attempts</span>
			</li>
			<?php
				} else if ($page == 'subjectiveExamCheckAttempt') { ?>
			<li>
				<a href="<?php echo $sitepathManageSubjectiveExams.$examId; ?>"><?php echo $examDetails['examName']; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Check Attempt</span>
			</li>
			<?php
				} else if ($page == 'subjectiveExamResult') { ?>
			<li>
				<a href="<?php echo $sitepathManageSubjectiveExams.$examId; ?>"><?php echo $examDetails['examName']; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Result</span>
			</li>
			<?php
				} else if ($page == 'subjectiveExamResultStudent') { ?>
			<li>
				<a href="<?php echo $sitepathManageSubjectiveExams.$examId; ?>"><?php echo $examDetails['examName']; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a href="<?php echo $sitepathManageSubjectiveExams.$examId.'/result'; ?>">Result</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span><?php echo $studentDetails['name']; ?></span>
			</li>
			<?php
				} else if ($page == 'manualExamCreate' || $page == 'manualExamImport') { ?>
			<li>
				<span><?php echo $page_title; ?></span>
			</li>
			<?php
				} else if ($page == 'manualExamSettings') { ?>
			<li>
				<a href="<?php echo $sitepathManageManualExams.$examId; ?>"><?php echo $examDetails['name']; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Settings</span>
			</li>
			<?php
				} else if ($page == 'manualExamObjComments') { ?>
			<li>
				<a href="<?php echo $sitepathManageManualExams.$examId; ?>"><?php echo $examDetails['name']; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Manual Comments</span>
			</li>
			<?php
				} else if ($page == 'manualExamResult') { ?>
			<li>
				<a href="<?php echo $sitepathManageManualExams.$examId; ?>" class="exam-name"><?php echo $examDetails['name']; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Result</span>
			</li>
			<?php
				} else if ($page == 'submissionCreate') { ?>
			<li>
				<span><?php echo $page_title; ?></span>
			</li>
			<?php
				} else if ($page == 'submissionSettings' || $page == 'submissionQuestions') { ?>
			<li>
				<span><?php echo $examDetails['name']; ?> Settings</span>
			</li>
			<?php
				} else if ($page == 'submissionCheck') { ?>
			<li>
				<a href="<?php echo $sitepathManageSubmissions.$examId; ?>"><?php echo $examDetails['name']; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Attempts</span>
			</li>
			<?php
				} else if ($page == 'submissionCheckAttempt') { ?>
			<li>
				<a href="<?php echo $sitepathManageSubmissions.$examId; ?>"><?php echo $examDetails['name']; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Check Attempt</span>
			</li>
			<?php
				} else if ($page == 'submissionObjComments') { ?>
			<li>
				<a href="<?php echo $sitepathManageSubmissions.$examId; ?>"><?php echo $examDetails['name']; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Submission Comments</span>
			</li>
			<?php
				} else if ($page == 'submissionResult') { ?>
			<li>
				<a href="<?php echo $sitepathManageSubmissions.$examId; ?>"><?php echo $examDetails['name']; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span>Result</span>
			</li>
			<?php
				} else if ($page == 'submissionResultStudent') { ?>
			<li>
				<a href="<?php echo $sitepathManageSubmissions.$examId; ?>"><?php echo $examDetails['name']; ?></a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a href="<?php echo $sitepathManageSubmissions.$examId.'/result'; ?>">Result</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<span><?php echo $studentDetails['name']; ?></span>
			</li>
			<?php
				} ?>
		</ul>
		<div class="page-toolbar">
			<div class="btn-group pull-right">
				<?php if ($page == 'subjectiveExamQuestions') { ?>
				<button type="button" class="btn btn-fit-height green-jungle makelive" disabled=""> Go Live </button>
				<button type="button" class="btn btn-fit-height red makeNotlive hide"> Go Offline </button>
				<button type="button" class="btn btn-fit-height grey-cascade" data-target="#importSubjectQuestionsModal" data-toggle="modal" id="importSubjectiveQuestions"> Import Questions </button>
				<a href="<?php echo $sitepathManageSubjectiveExams.$examId.'/settings'; ?>" class="btn btn-fit-height grey-salsa"> Exam Settings</a>
				<?php
					} ?>
				<?php if ($page == 'examDetail') { ?>
				<!-- <button type="button" class="btn btn-fit-height purple" id="importQuestions" data-target="#importExamModal" data-toggle="modal" disabled=""> Import Exam </button> -->
				<div class="btn-group"><button class="btn btn-fit-height" id="examStatus" disabled></button></div>
				<div class="btn-group">
					<button type="button" class="btn btn-fit-height grey-cascade dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"> <i class="fa fa-gear"></i> Settings
						<i class="fa fa-angle-down"></i>
					</button>
					<ul class="dropdown-menu pull-right" role="menu">
						<li>
							<a href="javascript:;" id="btnExportSettings"> Export Settings</a>
						</li>
						<li>
							<a href="<?php echo $sitepathManageExams.$examId.'/settings'; ?>"> Exam Settings</a>
						</li>
					</ul>
				</div>
				<!-- <a href="javascript:;" id="btnExportSettings" class="btn btn-fit-height grey-cascade"> Export Settings</a>
				<a href="<?php echo $sitepathManageExams.$examId.'/settings'; ?>" class="btn btn-fit-height grey-salsa"> Exam  -->Settings</a>
				<?php
					} ?>
				<?php if ($page == 'subjectiveExamCheckAttempt') { ?>
					<a href="javascript:void(0)" class="btn btn-fit-height btn-success" id="btnAttemptChecked">Mark exam as checked</a>
				<?php } ?>
				<?php if ($page == 'examResultStudent') { ?>
					<a href="<?php echo $sitepathManageExams.$examId.'/result'; ?>" class="btn btn-fit-height btn-success">Back to Result</a>
				<?php } ?>
				<?php if ($page == 'subjectiveExamResultStudent') { ?>
					<a href="<?php echo $sitepathManageSubjectiveExams.$examId.'/result'; ?>" class="btn btn-fit-height btn-success">Back to Result</a>
				<?php } ?>
				<?php if ($page == 'manualExamResult') { ?>
					<a href="<?php echo $sitepathManageManualExams.$examId.'/settings'; ?>" class="btn btn-fit-height grey-salsa"> Exam Settings</a>
				<?php } ?>
				<?php if ($page == 'submissionQuestions') { ?>
				<button type="button" class="btn btn-fit-height grey-cascade" data-target="#importSubjectQuestionsModal" data-toggle="modal" id="importSubjectiveQuestions"> Import Questions </button>
				<button type="button" class="btn btn-fit-height green-jungle makelive" disabled=""> Go Live </button>
				<button type="button" class="btn btn-fit-height red makeNotlive hide"> Go Offline </button>
				<a href="<?php echo $sitepathManageSubmissions.$examId.'/settings'; ?>" class="btn btn-fit-height grey-salsa"> Exam Settings</a>
				<?php
					} ?>
				<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>" class="btn btn-fit-height blue"> Back to <?php echo $global->terminology["subject_single"]; ?></a>
			</div>
		</div>
	</div>