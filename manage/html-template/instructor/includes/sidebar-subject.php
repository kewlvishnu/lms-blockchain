<!-- BEGIN PROFILE SIDEBAR -->
<div class="profile-sidebar course-sidebar">
	<!-- PORTLET MAIN -->
	<div class="portlet light course-sidebar-portlet ">
		<!-- SIDEBAR USERPIC -->
		<div class="coursepic">
			<button type="button" class="btn blue-hoki" data-target="#modalUploadSubjectImage" data-toggle="modal">Change</button>
			<img src="<?php echo $subjectDetails["image"]; ?>" class="btn-block" alt=""> </div>
		<!-- END SIDEBAR USERPIC -->
		<!-- SIDEBAR USER TITLE -->
		<div class="profile-usertitle">
			<div class="profile-usertitle-name"> <?php echo $subjectDetails["name"]; ?> </div>
		</div>
		<!-- END SIDEBAR USER TITLE -->
		<!-- SIDEBAR BUTTONS -->
		<div class="profile-userbuttons">
			<button type="button" class="btn blue-hoki btn-sm edit-subject"><i class="fa fa-edit"></i> Edit</button>
		</div>
		<!-- END SIDEBAR BUTTONS -->
		<!-- SIDEBAR MENU -->
		<div class="profile-usermenu">
			<ul class="nav">
				<li>
					<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>">
						<i class="icon-book-open"></i> <?php echo $global->terminology["subject_single"]; ?> Id
						<span class="pull-right"><small>S0<?php echo $subjectDetails["id"]; ?></small></span>
					</a>
				</li>
				<li>
					<a href="<?php echo $sitepathManageCourses.$courseId; ?>">
						<i class="icon-notebook"></i> <?php echo $global->terminology["course_single"]; ?> Id
						<span class="pull-right"><small>C00<?php echo $courseDetails["id"]; ?></small></span>
					</a>
				</li>
				<li>
					<a href="<?php echo $sitepathManageSubjects.$subjectId; ?>/students">
						<i class="fa fa-users"></i> <?php echo $global->terminology["student_plural"]; ?>
						<span class="pull-right"><small><?php echo $subjectDetails['students']; ?></small></span>
					</a>
				</li>
				<li>
					<a href="<?php echo (($subjectDetails['chapters']>0)?($sitepathManageContent.'subjects/'.$subjectId):('javascript:;')); ?>">
						<i class="icon-notebook"></i> Lectures
						<span class="pull-right"><small><?php echo $subjectDetails['chapters']; ?></small></span>
					</a>
				</li>
				<li>
					<a data-scroll href="#listSections">
						<i class="icon-notebook"></i> Exams
						<span class="pull-right"><small><?php echo $subjectDetails['exams']; ?></small></span>
					</a>
				</li>
				<li>
					<a data-scroll href="<?php echo $sitepathManageSubjects.$subjectId; ?>/questions">
						<i class="icon-notebook"></i> Objective Questions
						<span class="pull-right"><small><?php echo $subjectDetails['questions']; ?></small></span>
					</a>
				</li>
				<li>
					<a data-scroll href="<?php echo $sitepathManageSubjects.$subjectId; ?>/subjective-questions">
						<i class="icon-notebook"></i> Subjective Questions
						<span class="pull-right"><small><?php echo $subjectDetails['subjective_questions']; ?></small></span>
					</a>
				</li>
				<li>
					<a data-scroll href="<?php echo $sitepathManageSubjects.$subjectId; ?>/submission-questions">
						<i class="icon-notebook"></i> Submission Questions
						<span class="pull-right"><small><?php echo $subjectDetails['submission_questions']; ?></small></span>
					</a>
				</li>
			</ul>
		</div>
		<!-- END MENU -->
	</div>
	<!-- END PORTLET MAIN -->
	<!-- PORTLET MAIN -->
	<div class="portlet light ">
		<!-- STAT -->
		<div class="list-separated profile-stat text-center">
			<div title="<?php echo round($subjectDetails["rating"]->rating,1); ?>">
				<input class="star-rating" type="number" class="rating" min="0" max="5" step="0.1" value="<?php echo $subjectDetails["rating"]->rating; ?>" data-size="xs">
			</div>
			<div class="clearfix">
				<div class="pull-left">(<?php echo $subjectDetails["rating"]->total; ?> Ratings)</div>
				<div class="pull-right">
					<small><a href="#modalViewReviews" data-toggle="modal" class="btn btn-info btn-xs">See reviews</a></small>
				</div>
			</div>
		</div>
		<!-- <div class="row list-separated profile-stat">
			<div class="col-md-4 col-sm-4 col-xs-6">
				<div class="uppercase profile-stat-title"> 37 </div>
				<div class="uppercase profile-stat-text"> Lectures </div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6">
				<div class="uppercase profile-stat-title"> 51 </div>
				<div class="uppercase profile-stat-text"> Exams </div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6">
				<div class="uppercase profile-stat-title"> 61 </div>
				<div class="uppercase profile-stat-text"> Assignments </div>
			</div>
		</div> -->
		<!-- END STAT -->
		<div>
			<h4 class="profile-desc-title"><?php echo $global->terminology["subject_single"]; ?> Name</h4>
			<span class="profile-desc-text"> <?php echo $subjectDetails["name"]; ?></span>
			<h4 class="profile-desc-title"><?php echo $global->terminology["subject_single"]; ?> Description</h4>
			<span class="profile-desc-text"> <?php echo $subjectDetails["description"]; ?> </span>
		</div>
	</div>
	<!-- END PORTLET MAIN -->
</div>
<!-- END BEGIN PROFILE SIDEBAR -->
<?php
	require_once 'html-template/global/modals/viewReviews.modal.php';
?>