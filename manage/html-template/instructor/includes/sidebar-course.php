<!-- BEGIN PROFILE SIDEBAR -->
<div class="profile-sidebar course-sidebar">
	<!-- PORTLET MAIN -->
	<div class="portlet light course-sidebar-portlet ">
		<!-- SIDEBAR USERPIC -->
		<div class="coursepic">
			<button type="button" class="btn blue-hoki" data-target="#modalUploadCourseImage" data-toggle="modal">Change</button>
			<img src="<?php echo $courseDetails->courseDetails["image"]; ?>" class="btn-block" alt=""> </div>
		<!-- END SIDEBAR USERPIC -->
		<!-- SIDEBAR USER TITLE -->
		<div class="profile-usertitle">
			<div class="profile-usertitle-name"> <?php echo $courseDetails->courseDetails["name"]; ?> </div>
			<div class="profile-usertitle-job"> <?php echo $courseDetails->courseDetails["subtitle"]; ?> </div>
		</div>
		<!-- END SIDEBAR USER TITLE -->
		<!-- SIDEBAR BUTTONS -->
		<div class="profile-userbuttons">
			<button type="button" class="btn blue-hoki btn-sm btn-course edit-course"><i class="fa fa-edit"></i> Edit</button>
		</div>
		<!-- END SIDEBAR BUTTONS -->
		<!-- SIDEBAR MENU -->
		<div class="profile-usermenu">
			<ul class="nav">
				<li>
					<a href="<?php echo $sitepathInstituteCourses.$courseId; ?>">
						<i class="icon-notebook"></i> <?php echo $global->terminology["course_single"]; ?> Id
						<span class="pull-right"><small>C00<?php echo $courseDetails->courseDetails["id"]; ?></small></span>
					</a>
				</li>
				<li>
					<a href="javascript:;">
						<i class="icon-calendar"></i> Live Date
						<span class="pull-right"><small><?php echo date("d M Y",($courseDetails->courseDetails["liveDate"]/1000)); ?></small></span>
					</a>
				</li>
				<li>
					<a href="javascript:;">
						<i class="icon-calendar"></i> End Date
						<span class="pull-right"><small><?php echo ((empty($courseDetails->courseDetails["endDate"]))?'-':date("d M Y",($courseDetails->courseDetails["endDate"]/1000))); ?></small></span>
					</a>
				</li>
				<li>
					<a href="javascript:;">
						<i class="fa fa-users"></i> <?php echo $global->terminology["subject_plural"]; ?>
						<span class="pull-right"><small><?php echo $courseDetails->subjects; ?></small></span>
					</a>
				</li>
				<li>
					<a href="<?php echo $sitepathInstituteStudents; ?>">
						<i class="fa fa-users"></i> <?php echo $global->terminology["student_plural"]; ?>
						<span class="pull-right"><small><span id="jsCourseStudents"></span></small></span>
					</a>
				</li>
				<li>
					<a href="<?php echo $sitepathInstituteKeys; ?>">
						<i class="fa fa-users"></i> Pending enrolls
						<span class="pull-right"><small><span id="jsPending"></span></small></span>
					</a>
				</li>
				<li>
					<a href="<?php echo $sitepathInstituteKeys; ?>">
						<i class="fa fa-dollar"></i> Remaining keys
						<span class="pull-right"><small><span id="jsRemaining"></span></small></span>
					</a>
				</li>
			</ul>
		</div>
		<!-- END MENU -->
	</div>
	<!-- END PORTLET MAIN -->
	<!-- PORTLET MAIN -->
	<div class="portlet light ">
		<!-- STAT -->
		<div class="list-separated profile-stat text-center">
			<div title="<?php echo round($courseDetails->courseDetails["rating"]->rating,1); ?>">
				<input class="star-rating" type="number" class="rating" min="0" max="5" step="0.1" value="<?php echo $courseDetails->courseDetails["rating"]->rating; ?>" data-size="xs">
			</div>
			<div class="clearfix">
				<div class="pull-left">(<?php echo $courseDetails->courseDetails["rating"]->total; ?> Ratings)</div>
				<div class="pull-right">
					<small><a href="#modalViewReviews" data-toggle="modal" class="btn btn-info btn-xs">See reviews</a></small>
				</div>
			</div>
		</div>
		<!-- <div class="row list-separated profile-stat">
			<div class="col-md-4 col-sm-4 col-xs-6">
				<div class="uppercase profile-stat-title"> 37 </div>
				<div class="uppercase profile-stat-text"> Subjects </div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6">
				<div class="uppercase profile-stat-title"> 51 </div>
				<div class="uppercase profile-stat-text"> Exams </div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6">
				<div class="uppercase profile-stat-title"> 61 </div>
				<div class="uppercase profile-stat-text"> Students </div>
			</div>
		</div> -->
		<!-- END STAT -->
		<div>
			<h4 class="profile-desc-title"><?php echo $global->terminology["course_single"]; ?> Category</h4>
			<span class="profile-desc-text"> <?php
				$listCategories = "";
				foreach ($courseDetails->courseCategories as $key => $courseCategory) {
					$listCategories.= $courseCategory["category"].", ";
				}
				echo substr($listCategories, 0, strlen($listCategories)-2);
			?></span>
			<h4 class="profile-desc-title">Target Audience</h4>
			<span class="profile-desc-text"> <?php echo $courseDetails->courseDetails["targetAudience"]; ?> </span>
			<h4 class="profile-desc-title"><?php echo $global->terminology["course_single"]; ?> Introduction</h4>
			<span class="profile-desc-text"> <?php echo $courseDetails->courseDetails["description"]; ?> </span>
		</div>
	</div>
	<!-- END PORTLET MAIN -->
</div>
<!-- END BEGIN PROFILE SIDEBAR -->
<?php
	require_once 'html-template/global/modals/viewReviews.modal.php';
?>