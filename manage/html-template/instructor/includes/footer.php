        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer text-center">
            <div class="page-footer-inner clearfix"> 2016 &copy; Copyrights. All Rights Reserved.
                <a target="_blank" href="<?php echo $sitepath; ?>">Integro.io, Inc</a>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
        </div>
        <!-- END FOOTER -->
        <!-- DATA HOLDERS START -->
        <input type="hidden" id="contractCode" value="<?php echo $global->contractCode; ?>">
        <input type="hidden" id="contractAddress" value="<?php echo $global->contractAddress; ?>">
        <input type="hidden" id="ownerAddress" value="<?php echo $global->toAddress; ?>">
        <!-- DATA HOLDERS END -->
        <!--[if lt IE 9]>
        <script src="assets/global/plugins/respond.min.js"></script>
        <script src="assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->
<?php
    // Modals inclusion
    require_once 'html-template/global/modals/metamaskNotInstalled.modal.php';
    require_once 'html-template/global/modals/metamaskAssociateAddress.modal.php';
    require_once 'html-template/global/modals/metamaskChangeAddress.modal.php';
    require_once 'html-template/global/modals/portisAssociateAddress.modal.php';
    require_once 'html-template/global/modals/setAddress.modal.php';
    if (isset($page) && !empty($page)) {
        switch ($page) {
            case 'home':
            case 'courses':
                require_once 'html-template/global/modals/addEditCourse.modal.php';
                require_once 'html-template/global/modals/addEditSubject.modal.php';
                break;
            case 'courseDetail':
            case 'courseAssignSubjects':
                require_once 'html-template/global/modals/addEditCourse.modal.php';
                require_once 'html-template/global/modals/addEditSubject.modal.php';
                require_once 'html-template/global/modals/importSubject.modal.php';
                require_once 'html-template/global/modals/generateStudents.modal.php';
                require_once 'html-template/global/modals/inviteStudents.modal.php';
                require_once 'html-template/global/modals/purchaseCourseKeys.modal.php';
                require_once 'html-template/global/modals/purchaseInvitation.modal.php';
                require_once 'html-template/global/modals/purchaseNowCourseKeys.modal.php';
                require_once 'html-template/global/modals/uploadDemoVideo.modal.php';
                require_once 'html-template/global/modals/uploadCourseDocument.modal.php';
                require_once 'html-template/global/modals/marketplaceSettings.modal.php';
                require_once 'html-template/global/modals/uploadCourseImage.modal.php';
                break;
            case 'subjectDetail':
            case 'subjectStudents':
            case 'subjectStudentGroups':
            case 'subjectChat':
                require_once 'html-template/global/modals/addEditSubject.modal.php';
                require_once 'html-template/global/modals/uploadSubjectSyllabus.modal.php';
                require_once 'html-template/global/modals/generateStudents.modal.php';
                require_once 'html-template/global/modals/inviteStudents.modal.php';
                require_once 'html-template/global/modals/copyExam.modal.php';
                require_once 'html-template/global/modals/uploadSubjectImage.modal.php';
                break;
            case 'subjectParents':
                require_once 'html-template/global/modals/addEditSubject.modal.php';
                require_once 'html-template/global/modals/uploadSubjectSyllabus.modal.php';
                require_once 'html-template/global/modals/generateStudents.modal.php';
                require_once 'html-template/global/modals/inviteStudents.modal.php';
                require_once 'html-template/global/modals/copyExam.modal.php';
                require_once 'html-template/global/modals/uploadSubjectImage.modal.php';
                require_once 'html-template/instructor/modals/newParentNotification.modal.php';
                break;
            case 'subjectAnalytics':
            case 'subjectGrading':
            case 'subjectRewards':
            case 'subjectPerformance':
            case 'studentPerformance':
                require_once 'html-template/global/modals/addEditSubject.modal.php';
                require_once 'html-template/global/modals/uploadSubjectSyllabus.modal.php';
                require_once 'html-template/global/modals/generateStudents.modal.php';
                require_once 'html-template/global/modals/inviteStudents.modal.php';
                require_once 'html-template/global/modals/copyExam.modal.php';
                require_once 'html-template/global/modals/uploadSubjectImage.modal.php';
                require_once 'html-template/global/modals/detailedAnalytics.modal.php';
                break;
            case 'subjectSections':
                require_once 'html-template/global/modals/addEditSubject.modal.php';
                //require_once 'html-template/global/modals/subjectProfessors.modal.php';
                require_once 'html-template/global/modals/uploadSubjectSyllabus.modal.php';
                require_once 'html-template/global/modals/generateStudents.modal.php';
                require_once 'html-template/global/modals/inviteStudents.modal.php';
                require_once 'html-template/global/modals/copyExam.modal.php';
                require_once 'html-template/global/modals/uploadSubjectImage.modal.php';
                require_once 'html-template/global/modals/addEditContentTitle.modal.php';
                require_once 'html-template/global/modals/addEditContent.modal.php';
                require_once 'html-template/global/modals/addSupplementaryMaterial.modal.php';
                require_once 'html-template/global/modals/addEditSectionTitle.modal.php';
                break;
            case 'students':
            case 'studentsInvites':
                require_once 'html-template/global/modals/inviteStudentsToCourse.modal.php';
                break;
            case 'studentsTransfer':
                require_once 'html-template/global/modals/transferStudents.modal.php';
                break;
            case 'courseKeys':
                //require_once 'html-template/global/modals/studentCourseKeys.modal.php';
                require_once 'html-template/global/modals/purchaseCourseKeys.modal.php';
                require_once 'html-template/global/modals/purchaseNowCourseKeys.modal.php';
                require_once 'html-template/global/modals/purchaseInvitation.modal.php';
                break;
            case 'examDetail':
                require_once 'html-template/global/modals/addEditQuestionsCategory.modal.php';
                require_once 'html-template/global/modals/importExam.modal.php';
                require_once 'html-template/global/modals/examInstructions.modal.php';
                require_once 'html-template/global/modals/importSubjectQuestions.modal.php';
                require_once 'html-template/global/modals/exportExam.modal.php';
                break;
            case 'examResultStudent':
                require_once("html-template/global/modals/timeCompare.modal.php");
                require_once("html-template/global/modals/examQuestionCategories.modal.php");
                break;
            case 'subjectiveExamQuestions':
            case 'submissionQuestions':
                require_once 'html-template/global/modals/importSubjectQuestions.modal.php';
                break;
            case 'subjectiveExamCheckAttempt':
            case 'submissionCheckAttempt':
                require_once 'html-template/global/modals/answerFile.modal.php';
                break;
            case 'subjectiveExamResultStudent':
                require_once 'html-template/global/modals/answerFile.modal.php';
                require_once("html-template/global/modals/timeCompare.modal.php");
                break;
            case 'manualExamResult':
                require_once 'html-template/global/modals/manualExamResultMarks.modal.php';
                break;
            case 'examObjComments':
            case 'subjectiveExamObjComments':
                require_once 'html-template/global/modals/examCommentsEdit.modal.php';
                break;
            default:
                break;
        }
    }
?>
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-bootpag/jquery.bootpag.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/holder.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <!-- <script src="assets/global/plugins/web3/web3.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/web3/ethjs-util.min.js" type="text/javascript"></script> -->
        <script src="https://cdn.jsdelivr.net/gh/ethereum/web3.js/dist/web3.min.js"></script>
        <!-- <script src="https://cdn.jsdelivr.net/npm/portis/dist/bundle.min.js"></script> -->
        <script src="assets/global/plugins/magicsuggest/magicsuggest-min.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML"></script>
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <?php
            if (isset($page) && !empty($page)) {
                $customIncludes = '';
                switch ($page) {
                    case 'home':
                        $customIncludes.=
                        '<script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/horizontal-timeline/horozontal-timeline.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/highcharts/js/highcharts.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/highcharts/js/highcharts-3d.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/highcharts/js/highcharts-more.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>';
                        break;
                    case 'courses':
                        $customIncludes.=
                        '<script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>';
                        break;
                    case 'courseDetail':
                    case 'courseAssignSubjects':
                        $customIncludes.=
                        '<script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery.mockjax.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-star-rating-master/js/star-rating.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-form/jquery.form.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/mediaelement/mediaelement-and-player.min.js"></script>
                        <script src="assets/global/plugins/jcrop/js/jquery.color.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jcrop/js/jquery.Jcrop.min.js" type="text/javascript"></script>
                        <!--<script src="assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js" type="text/javascript"></script>-->';
                        break;
                    case 'subjectDetail':
                    case 'subjectStudents':
                    case 'subjectStudentGroups':
                    case 'subjectChat':
                    case 'subjectParents':
                        $customIncludes.=
                        '<script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-star-rating-master/js/star-rating.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-form/jquery.form.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jcrop/js/jquery.color.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jcrop/js/jquery.Jcrop.min.js" type="text/javascript"></script>';
                        break;
                    case 'subjectSections':
                        $customIncludes.=
                        '<script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-star-rating-master/js/star-rating.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-form/jquery.form.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jcrop/js/jquery.color.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jcrop/js/jquery.Jcrop.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-nestable/jquery.nestable.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery.mockjax.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-editable/inputs-ext/address/address.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-editable/inputs-ext/wysihtml5/wysihtml5.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>';
                        break;
                    case 'subjectAnalytics':
                    case 'subjectGrading':
                    case 'subjectRewards':
                        $customIncludes.=
                        '<script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-star-rating-master/js/star-rating.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-form/jquery.form.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jcrop/js/jquery.color.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jcrop/js/jquery.Jcrop.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/highcharts/js/highcharts.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/highcharts/js/highcharts-3d.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/highcharts/js/highcharts-more.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-star-rating-master/js/star-rating.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>';
                        break;
                    case 'examCreate':
                    case 'examSettings':
                    case 'subjectiveExamCreate':
                    case 'subjectiveExamSettings':
                    case 'submissionCreate':
                    case 'submissionSettings':
                        $customIncludes.=
                        '<script src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-nestable/jquery.nestable.js" type="text/javascript"></script>';
                        break;
                    case 'examDetail':
                    case 'examQuestions':
                    case 'subjectiveExamQuestions':
                    case 'submissionQuestions':
                    case 'subjectQuestions':
                    case 'subjectSubjectiveQuestions':
                    case 'subjectSubmissionQuestions':
                        $customIncludes.=
                        '<script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
                        <!--<script src="assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>-->
                        <script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-form/jquery.form.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/mediaelement/mediaelement-and-player.min.js"></script>';
                        break;
                    case 'students':
                    case 'studentsInvites':
                    case 'studentsTransfer':
                        $customIncludes.=
                        '<script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery.mockjax.js" type="text/javascript"></script>';
                        break;
                    case 'courseKeys':
                        $customIncludes.=
                        '<script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>';
                        break;
                    case 'portfolios':
                        $customIncludes.=
                        '<script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jcrop/js/jquery.color.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jcrop/js/jquery.Jcrop.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>';
                        break;
                    case 'manualExamImport':
                        $customIncludes.=
                        '<script src="assets/global/plugins/jquery-form/jquery.form.min.js" type="text/javascript"></script>';
                        break;
                    case 'examResult':
                    case 'examResultStudent':
                    case 'subjectiveExamResult':
                    case 'subjectiveExamResultStudent':
                    case 'manualExamResult':
                        $customIncludes.=
                        '<script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
                        <script src="https://code.highcharts.com/stock/highstock.js"></script>
                        <script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
                        <script src="https://code.highcharts.com/stock/modules/heatmap.js"></script>
                        <script src="https://code.highcharts.com/stock/modules/data.js"></script>
                        <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript"></script>';
                        break;
                    case 'profile':
                        $customIncludes.=
                        '<script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-editable/inputs-ext/address/address.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-editable/inputs-ext/wysihtml5/wysihtml5.js" type="text/javascript"></script>';
                        if ($subPage == 'profileEdit') {
                            $customIncludes.=
                            '<script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/jcrop/js/jquery.color.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/jcrop/js/jquery.Jcrop.min.js" type="text/javascript"></script>';
                        }
                        break;
                    case 'portfolio':
                        $customIncludes.=
                        '<script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-editable/inputs-ext/address/address.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-editable/inputs-ext/wysihtml5/wysihtml5.js" type="text/javascript"></script>';
                        if (($subPage == 'portfolioEdit') || ($subPage == 'portfolioPages')) {
                            $customIncludes.=
                            '<script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/jcrop/js/jquery.color.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/jcrop/js/jquery.Jcrop.min.js" type="text/javascript"></script>';
                        }
                        break;
                    default:break;
                }
                echo $customIncludes;
            }
        ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <script src="assets/global/scripts/functions.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <!-- <script src="assets/pages/scripts/ui-general.min.js" type="text/javascript"></script> -->
        <!-- <script src="assets/pages/scripts/ui-toastr.min.js" type="text/javascript"></script> -->
        <!-- <script src="assets/pages/scripts/dashboard.min.js" type="text/javascript"></script> -->
        <!-- END PAGE LEVEL SCRIPTS -->
        <script src="assets/layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
        <!-- <script src="assets/layouts/layout2/scripts/demo.min.js" type="text/javascript"></script> -->
        <?php
            require_once('jsphp/instructor/global.js.php');
            require_once('jsphp/wallet/web3ops.js.php');
            if (isset($page) && !empty($page)) {
                $customIncludes = '';
                if (file_exists('jsphp/'.$page.'.js.php')) {
                    require_once('jsphp/'.$page.'.js.php');
                }
                if (isset($subPage) && !empty($subPage)) {
                    if (file_exists('jsphp/'.$subPage.'.js.php')) {
                        require_once('jsphp/'.$subPage.'.js.php');
                    }
                }
                if ($_SESSION['userRole'] == 1) {
                    if (file_exists('jsphp/institute/'.$page.'.js.php')) {
                        require_once('jsphp/institute/'.$page.'.js.php');
                    }
                    if (isset($subPage) && !empty($subPage)) {
                        if (file_exists('jsphp/institute/'.$subPage.'.js.php')) {
                            require_once('jsphp/institute/'.$subPage.'.js.php');
                        }
                    }
                } else {
                    if (file_exists('jsphp/instructor/'.$page.'.js.php')) {
                        require_once('jsphp/instructor/'.$page.'.js.php');
                    }
                    if (isset($subPage) && !empty($subPage)) {
                        if (file_exists('jsphp/instructor/'.$subPage.'.js.php')) {
                            require_once('jsphp/instructor/'.$subPage.'.js.php');
                        }
                    }
                }
                switch ($page) {
                    case 'home':
                    case 'courses':
                        require_once('jsphp/courseSubjectOps.js.php');
                        break;
                    case 'courseDetail':
                    case 'courseAssignSubjects':
                        require_once('jsphp/courseSubjectOps.js.php');
                        require_once('jsphp/courseOps.js.php');
                        require_once('jsphp/license.js.php');
                        break;
                    case 'subjectDetail':
                    case 'subjectStudents':
                    case 'subjectAnalytics':
                    case 'subjectGrading':
                    case 'subjectRewards':
                    case 'subjectChat':
                    case 'subjectParents':
                    case 'subjectSections':
                        require_once('jsphp/courseSubjectOps.js.php');
                        require_once('jsphp/subjectOps.js.php');
                        break;
                    case 'examCreate':
                        require_once('jsphp/examCommon.js.php');
                        break;
                    case 'examSettings':
                        require_once('jsphp/examCommon.js.php');
                        break;
                    /*case 'subjectiveExamCreate':
                        require_once('jsphp/examCommon.js.php');
                        break;
                    case 'subjectiveExamSettings':
                        require_once('jsphp/examCommon.js.php');
                        break;*/
                    default:break;
                }
                echo $customIncludes;
            }
            if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
        ?>
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/58f86bb530ab263079b60876/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
        <!--End of Tawk.to Script--><?php
                } ?>
    </body>

</html>