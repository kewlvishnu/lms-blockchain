<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- END SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start <?php echo (($page=='home')?'active open':''); ?>">
                <a href="<?php echo $sitepathManage; ?>" class="nav-link">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
            </li>
            <li class="nav-item <?php echo (($page=='courses')?'active open':''); ?>" id="navCourses">
                <a href="<?php echo $sitepathInstructorCourses; ?>" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">My <?php echo $global->terminology["course_plural"]; ?></span>
                    <span class="arrow"></span>
                </a>
            </li>
            <li class="nav-item <?php echo (($page=='instituteInvitations')?'active open':''); ?>">
                <a href="<?php echo $sitepathInstructorInvitations; ?>" class="nav-link">
                    <i class="icon-diamond"></i>
                    <span class="title">Invitations</span>
                    <span class="arrow"></span>
                </a>
            </li>
            <li class="nav-item <?php echo (($page=='subjectsAssigned')?'active open':''); ?>">
                <a href="<?php echo $sitepathInstructorSubjectsAssigned; ?>" class="nav-link">
                    <i class="icon-diamond"></i>
                    <span class="title"><?php echo $global->terminology["subject_plural"]; ?> Assigned</span>
                    <span class="arrow"></span>
                </a>
            </li>
            <?php /*
            <li class="nav-item <?php echo (($page=='allExams')?'active open':''); ?>">
                <a href="<?php echo $sitepathInstructorAllExams; ?>" class="nav-link">
                    <i class="icon-diamond"></i>
                    <span class="title">Exams</span>
                    <span class="arrow"></span>
                </a>
            </li>
            <li class="nav-item <?php echo (($page=='allContent')?'active open':''); ?>">
                <a href="<?php echo $sitepathInstructorAllContent; ?>" class="nav-link">
                    <i class="icon-user"></i>
                    <span class="title">Content</span>
                    <span class="arrow"></span>
                </a>
            </li>
            <li class="nav-item <?php echo (($page=='packages')?'active open':''); ?>">
                <a href="<?php echo $sitepathInstructorPackages; ?>" class="nav-link">
                    <i class="icon-user"></i>
                    <span class="title">Packages</span>
                    <span class="arrow"></span>
                </a>
            </li> */ ?>
            <li class="nav-item <?php echo (($page=='students')?'active open':''); ?>">
                <a href="<?php echo $sitepathInstructorStudents; ?>" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title"><?php echo $global->terminology["student_plural"]; ?></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php echo (($page == 'students')?'active open':'') ; ?>">
                        <a href="<?php echo $sitepathInstructorStudents; ?>" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title"><?php echo $global->terminology["student_plural"]; ?> List</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo (($page == 'studentsInvites')?'active open':'') ; ?>">
                        <a href="<?php echo $sitepathInstructorStudents; ?>invitations" class="nav-link ">
                            <i class="icon-pencil"></i>
                            <span class="title">Invitations</span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo (($page == 'studentsTransfer')?'active open':'') ; ?>">
                        <a href="<?php echo $sitepathInstructorStudents; ?>transfer" class="nav-link ">
                            <i class="icon-pencil"></i>
                            <span class="title">Transfer</span>
                        </a>
                    </li>
                </ul>
            </li>
            <?php /*
            <li class="nav-item <?php echo (($page=='coupons')?'active open':''); ?>">
                <a href="<?php echo $sitepathInstructorCoupons; ?>" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">Coupons</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php echo (($page == 'students' && $subPage == 'list')?'active open':'') ; ?>">
                        <a href="<?php echo $sitepathInstructorCoupons; ?>" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Coupons List</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo (($page == 'students' && $subPage == 'invitations')?'active open':'') ; ?>">
                        <a href="<?php echo $sitepathInstructorCoupons; ?>archive" class="nav-link ">
                            <i class="icon-pencil"></i>
                            <span class="title">Coupons Archive</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item <?php echo (($page=='discounts')?'active open':''); ?>">
                <a href="<?php echo $sitepathInstructorDiscounts; ?>" class="nav-link">
                    <i class="icon-user"></i>
                    <span class="title">Discounts</span>
                    <span class="arrow"></span>
                </a>
            </li> */ ?>
            <li class="nav-item <?php echo (($page == 'courseKeys')?'active open':'') ; ?>">
                <a href="<?php echo $sitepathInstructorKeys; ?>" class="nav-link">
                    <i class="icon-key"></i>
                    <span class="title">Enrollment Keys</span>
                    <span class="arrow"></span>
                </a>
            </li><?php /*
            <li class="nav-item <?php echo (($page == 'stats')?'active open':'') ; ?>">
                <a href="<?php echo $sitepathInstructorStats; ?>settings" class="nav-link">
                    <i class="icon-settings"></i>
                    <span class="title">Statistics</span>
                    <span class="arrow"></span>
                </a>
            </li>*/ ?>
            <li class="nav-item <?php echo (($page=='profile')?'active open':''); ?>">
                <a href="<?php echo $sitepathInstructorProfile; ?>" class="nav-link">
                    <i class="icon-user"></i>
                    <span class="title">My Profile</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php echo (($page == 'profile' && $subPage == 'profileMe')?'active open':'') ; ?>">
                        <a href="<?php echo $sitepathInstructorProfile; ?>" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Overview</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo (($page == 'profile' && $subPage == 'profileEdit')?'active open':'') ; ?>">
                        <a href="<?php echo $sitepathInstructorProfile; ?>edit" class="nav-link ">
                            <i class="icon-pencil"></i>
                            <span class="title">Edit Profile</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item <?php echo (($page == 'settings')?'active open':'') ; ?>">
                <a href="<?php echo $sitepathInstructorSettings; ?>" class="nav-link">
                    <i class="icon-settings"></i>
                    <span class="title">Settings</span>
                    <span class="arrow"></span>
                </a>
            </li>
            <li class="nav-item <?php echo (($page == 'portfolios')?'active open':'') ; ?>">
                <a href="<?php echo $sitepathInstructorPortfolios; ?>" class="nav-link">
                    <i class="icon-settings"></i>
                    <span class="title">Portfolios</span>
                    <span class="arrow"></span>
                </a>
            </li>
            <li class="nav-item <?php echo (($page == 'trash')?'active open':'') ; ?>">
                <a href="<?php echo $sitepathManageTrash; ?>" class="nav-link">
                    <i class="icon-trash"></i>
                    <span class="title">Trash</span>
                    <span class="arrow"></span>
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->