<script type="text/javascript">
var correctMarks = [];
var incorrectMarks = [];
var categoryQuestionType = [];
var letters = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
var flagCategory = true;
var oldCategory = 0;
var sortOrder = [];
var tempQuestion = 0;
var categoryModalDetail;
var player;
var videoDuration;
//var tags = {};
var reqTag = {};
reqTag.action = 'tags';
var ms = $('#magicsuggest').magicSuggest({
	valueField: 'name',
	data: TypeEndPoint,
	dataUrlParams: reqTag
});
$(ms).on('keydown', keydownAlphaNumeric);
var msfilter = $('#tagsfilter').magicSuggest({
	valueField: 'name',
	data: TypeEndPoint,
	dataUrlParams: reqTag
});
$(msfilter).on('keydown', keydownAlphaNumeric);
function fetchExamName() {
	var req = {};
	var res;
	req.action = 'get-exam-name';
	req.examId = examId;
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		fillExamName(res);
	});
}

function fillExamName(data) {
	$('#examType').text(data.type);
	$('#examName').text(data.name);
	//console.log(data.status);
	if(data.status == 0) {
		$('#examStatus').removeClass('hide');
		$('#examLive').addClass('hide');
		$('#examStop').addClass('hide');
	}
	else if(data.status == 1) {
		$('#examStatus').attr('disabled', false);
		$('#examStatus').removeClass('btn-primary');
		$('#examStatus').addClass('green-jungle');
		$('#examStatus').text('Go Live');
	}
	else if(data.status == 2) {
		$('#examStatus').attr('disabled', false);
		$('#examStatus').text('Live');
		$('#examStatus').removeClass('btn-primary');
		$('#examStatus').addClass('green-jungle');
		$('#examStatus').attr('data-assoc', data.association);
		$('#examStatus').addClass('hide');
		var html = '<button class="btn btn-fit-height red" type="button" id="examStop">Go Offline</button>';
		html+= '<button class="btn btn-fit-height red" type="button" id="examEnd">End Exam</button>';
		$('#examStatus').closest('.btn-group').append(html);
		$('#examStop').on('click', function() {
			var req = {};
			req.action = 'make-unlive';
			req.examId = examId;
			$.ajax({
				'type'	: 'post',
				'url'	: ApiEndPoint,
				'data'	: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					$('#examEnd').remove();
					$('#examStop').remove();
					$('#examStatus').text('Go Live').removeClass('hide');
				}
			});
		});
		$('#examEnd').on('click', function() {
			var req = {};
			req.action = 'make-exam-end';
			req.examId = examId;
			$.ajax({
				'type'	: 'post',
				'url'	: ApiEndPoint,
				'data'	: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					$('#examEnd').attr("disabled","disabled");
				}
			});
		});
	}
	/*if(data.status == 0) {
		$('#examStatus').removeClass('green-jungle');
		$('#examStatus').addClass('btn-primary');
		$('#examStatus').attr('disabled', true);
		$('#examStatus').text('Draft');
	}
	else if(data.status == 1) {
		$('#examStatus').attr('disabled', false);
		$('#examStatus').removeClass('btn-primary');
		$('#examStatus').addClass('green-jungle');
		$('#examStatus').text('Go Live');
	}
	else if(data.status == 2) {
		$('#examStatus').attr('disabled', false);
		$('#examStatus').text('Live');
		$('#examStatus').removeClass('btn-primary');
		$('#examStatus').addClass('green-jungle');
		$('#examStatus').attr('data-assoc', data.association);
		var html = '<button class="btn btn-fit-height red" type="button" id="examStop">Go Offline</button>';
		$('#examStatus').closest('.btn-group').append(html);
		$('#examStop').on('click', function() {
			var req = {};
			req.action = 'make-unlive';
			req.examId = examId;
			$.ajax({
				'type'	: 'post',
				'url'	: ApiEndPoint,
				'data'	: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					$('#examStop').remove();
					$('#examStatus').text('Go Live');
				}
			});
		});
	}*/
	//$('#exam').show();
}

function fetchExamSections() {
	var req = {};
	var res;
	req.action = 'get-exam-sections';
	req.examId = examId;
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		createSectionView(res);
	});
}

function createSectionView(data) {
	var li = "";
	var pending=completed=0;
	$('.js-sections').find('li').remove();
	for(var i=0; i<data.sections.length; i++) {
		li = '<li data-id="'+data.sections[i]['id']+'" class="mt-list-item section-list">'+
                '<div class="list-icon-container">'+
                    '<i class="icon-check"></i>'+
                '</div>'+
                '<div class="list-datetime sectionTotalMarks" title="Marks"> '+ data.sections[i].totalMarks +' </div>'+
                '<div class="list-item-content">'+
                    '<h3 class="uppercase">'+
                        '<a href="javascript:;">Section ' + ( i + 1 ) + ' : '+data.sections[i]['name']+'</a>'+
                    '</h3>'+
                '</div>'+
            '</li>';
        var container;
        if(data.sections[i]['status'] == 1) {
        	container = $('#completed-simple ul');
        	completed++;
			$(li).find('.section-list').addClass('done');
        } else {
        	container = $('#pending-simple ul');
        	pending++;
        }
		container.append(li);
		if(data.sections[i].delete == 1) {
			$('#sections li.mt-list-item:eq(-1)').addClass('deleted-item');
			var restore = '<button class="btn btn-success btn-xs restore-section" style="margin-left: 5px"><i class="fa fa-refresh"></i></button>';
			$('#sections li.mt-list-item:eq(-1)').find('.label-mini').removeClass('label-primary').addClass('label-danger').text('Deleted').after(restore);
		}
	}

	$('.js-completed-count').html(completed);
	$('.js-pending-count').html(pending);
	
	$('body').on('click', '.section-list', function() {
		elem = $(this);
		$('.section-list').removeClass('highlight');
		elem.addClass('highlight');
		fetchSectionDetails(elem.attr('data-id'));
	});
	
	$('#sections .section-list .restore-section').on('click', function(e) {
		elem = $(this);
		e.stopPropagation();
		var req = {};
		var res;
		req.action = 'restore-section';
		req.sectionId = elem.parents('a.section-list').attr('data-id');
		req.examId = examId;
		$.ajax({
			'type'	: 'post',
			'url'	: ApiEndPoint,
			'data'	: JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				toastr.error(res.message);
			else {
				elem.parents('li:eq(0)').removeClass('deleted-item');
				elem.parent().find('.label').removeClass('label-danger').addClass('label-primary').text('Draft');
				elem.remove();
				fetchSectionStatus();
			}
		});
	});
	elem = $('#sections li.mt-list-item:eq(0)')
	$('.section-list:eq(0)').addClass('highlight');
	if(data.sections.length>0) {
		fetchSectionDetails(data.sections[0]['id']);
	}
}

function fetchSectionDetails(sectionId) {
	//$('#sectionNewQuestion').hide();
	var req = {};
	var res;
	req.action = 'get-section-details';
	req.sectionId = sectionId;
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		$('#sectionName').text($('.js-sections [data-id="'+sectionId+'"] a').text());
		$('#sectionName').attr('data-id', req.sectionId);
		//if section is already deleted then no need of delete button
		if(elem.parent().hasClass('deleted-item'))
			$('#sectionDetail').find('.delete-section').addClass('hide');
		else
			$('#sectionDetail').find('.delete-section').removeClass('hide');
		fillSectionDetails(res);
		fetchSectionStatus(sectionId);
	});
}

function fillSectionDetails(data) {
	var html = '';
	var total = 0;
	var queshtml = '';
	var importCategory = '<option value="0">Select import category</option>';
	var catSelect = '<option value=0>Select Category</option>';
	$('#sectionTable tbody').find('tr').remove();
	$('#modalAddEditQuestionsCategory table.table tbody tr.generated').remove();
	$('#modalAddEditQuestionsCategory table.table tbody tr').find('.questionTypeSelect').attr('disabled', false);
	$('#modalAddEditQuestionsCategory table.table tbody tr').attr('data-id', 0);
	$('#modalAddEditQuestionsCategory table.table tbody tr').find('.questionTypeSelect').val(-1);
	$('#modalAddEditQuestionsCategory table.table tbody tr').find('input[type="text"]').val('');
	////$('#modalAddEditQuestionsCategory table.table tbody tr').find('span').text('0');
	$('#modalAddEditQuestionsCategory table.table tbody tr:eq(-1) td:eq(-1) .total').text('0');
	//console.log(data.categories);
	if(data.categories.length > 0) {
		addNewCategory(data.categories.length - 1);
	}
	categoryModalDetail = data;
	fillModalCategory(data);
	for(var i=0; i<data.categories.length; i++) {
		var questionDisplay = '';
		var greenInfo = '';
		var redInfo = '';
		if(data.categories[i].questionType == 0) {
			questionDisplay = 'Multiple Choice Question';
			greenInfo = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="Green symbol represents that the  question  is complete. The question would be counted in the no of entered questions"></i>';
			redInfo = '<i class="icon-close text-danger tooltips" data-placement="right" data-original-title="The Red symbol denotes that the  question is incomplete. The question would not be counted in the number  of entered questions<br><br>Following conditions have to met, for a question to appear green(complete)<br><br>a)  The question  text should be entered<br>b)  There should be at least 2 answer options<br>c)  One of them should be selected as correct"></i>';
		}
		else if(data.categories[i].questionType == 1) {
			questionDisplay = 'True or False';
			greenInfo = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="Green symbol represents that the  question  is complete. The question would be counted in the no of entered questions"></i>';
			redInfo = '<i class="icon-close text-danger tooltips" data-placement="right" data-original-title="The Red symbol denotes that the  question is incomplete. The question would not be counted in the number  of entered questions<br><br>Following conditions have to met, for a question to appear green(complete)<br><br>a)  The question text should be entered<br>b)  The answer should be marked as either  true or false"></i>';
		}
		else if(data.categories[i].questionType == 2) {
			questionDisplay = 'Fill in the blanks';
			greenInfo = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="Green symbol represents that the  question  is complete. The question would be counted in the no of entered questions"></i>';
			redInfo = '<i class="icon-close text-danger tooltips" data-placement="right" data-original-title="The Red symbol denotes that the  question is incomplete. The question would not be counted in the number  of entered questions<br><br>Following conditions have to met, for a question to appear green(complete)<br><br>a)  The question text  should be entered<br>b) The answer should be entered in the blank/box"></i>';
		}
		else if(data.categories[i].questionType == 3) {
			questionDisplay = 'Match the following';
			greenInfo = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="Green symbol represents that the  question  is complete. The question would be counted in the no of entered questions"></i>';
			redInfo = '<i class="icon-close text-danger tooltips" data-placement="right" data-original-title="The Red symbol denotes that the question is incomplete. The question would not be counted in the number of entered questions<br><br>Following conditions have to met, for a question to appear green(complete)<br><br>a) The question text should be entered<br>b) There should be  corresponding entry in column B for every entry in Column A"></i>';
		}
		else if(data.categories[i].questionType == 4) {
			questionDisplay = 'Comprehension Type Question';
			greenInfo = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="All Questions are Complete"></i>';
			redInfo = '<i class="icon-close text-danger tooltips" data-placement="right" data-original-title="Red represents that some part is complete and certain part is incomplete<br><br>Red symbol would appear here, if any of the conditions are not met:<br>a) The passage should be entered<br>b) There should be at least one complete question<br>c) All the questions entered should be complete"></i>';
		}
		else if(data.categories[i].questionType == 5) {
			questionDisplay = 'Dependent Type Question';
			greenInfo = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="All Sub-parts are Complete"></i>';
			redInfo = '<i class="icon-close text-danger tooltips" data-placement="right" data-original-title="Red represents that some part is complete and certain part is incomplete<br>Red symbol would appear here, if any of the conditions are not met:<br><br>a)The passage should be entered<br>b)There should be minimum one  complete sub part (multiple choice/multiple answer/true or false/fill in the  blanks)<br>c)All the sub parts entered should be complete"></i>';
		}
		else if(data.categories[i].questionType == 6) {
			questionDisplay = 'Multiple Answer Question';
			greenInfo = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="Green symbol represents that the  question  is complete. The question would be counted in the no of entered questions"></i>';
			redInfo = '<i class="icon-close text-danger tooltips" data-placement="right" data-original-title="The Red symbol denotes that the  question is incomplete. The question would not be counted in the number  of entered questions<br><br>Following conditions have to met, for a question to appear green(complete)<br><br>a)  The question text  should be entered<br>b)  There should be at least 2 answer options<br>c)  Minimum 1 of the options should be selected as correct"></i>';
		}
		else if(data.categories[i].questionType == 7) {
			questionDisplay = 'Audio Question';
			greenInfo = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="Green symbol represents that the  question  is complete. The question would be counted in the no of entered questions"></i>';
			redInfo = '<i class="icon-close text-danger tooltips" data-placement="right" data-original-title="The Red symbol denotes that the  question is incomplete. The question would not be counted in the number  of entered questions<br><br>Following conditions have to met, for a question to appear green(complete)<br><br>a)  The question  text should be entered<br>b)  There should be at least 2 answer options<br>c)  One of them should be selected as correct"></i>';
		}
		var remaining = data.categories[i]['required'] - data.categories[i]['entered'];
		if(remaining<=0) {
			rem = '--';
			marks = data.categories[i]['required'] * data.categories[i]['correct'];
		}
		else {
			rem = remaining;
			marks = data.categories[i]['entered'] * data.categories[i]['correct'];
		}
		total += marks;
		html += '<tr data-id="'+data.categories[i]['id']+'" data-section="'+data.categories[i]['sectionId']+'">'+
	                '<td>' + questionDisplay + '&emsp; +'+data.categories[i]['correct']+' &amp; -'+data.categories[i]['wrong']+'</td>'+
	                '<td class="text-center required">'+data.categories[i]['required']+'</td>'+
	                '<td class="text-center entered">'+data.categories[i]['entered']+'</td>'+
	                '<td class="text-center"><strong>'+marks+'</strong></td>'+
	                '<td class="text-center">'+rem+'</td>'+
	                '<td class="text-center"><button class="btn bg-blue-hoki font-white btn-xs refresh-status" title="Refresh Status"><i class="fa fa-refresh"></i></button></td>'+
	                '<td class="text-center"><button class="btn bg-blue-hoki font-white btn-xs import-questions" title="Import Questions"><i class="fa fa-download"></i></button></td>'+
	                '<td class="text-center"><button class="btn btn-danger btn-xs remove-category" title="" data-placement="top" type="button"><i class="fa fa-trash-o "></i></button></td>'+
	            '</tr>';
		catSelect += '<option value=' + data.categories[i]['id'] + ' data-questionType="' + data.categories[i]['questionType'] + '">' + questionDisplay + '&emsp;+' + data.categories[i]['correct'] + ' &amp; -' + data.categories[i]['wrong'] + '</option>';
		importCategory += '<option value=' + data.categories[i]['id'] + ' data-questionType="' + data.categories[i]['questionType'] + '">' + questionDisplay + '&emsp;+' + data.categories[i]['correct'] + ' &amp; -' + data.categories[i]['wrong'] + '</option>';
		id = data.categories[i].id;
		categoryQuestionType[id] = data.categories[i].questionType;
		correctMarks[id] = data.categories[i].correct;
		incorrectMarks[id] = data.categories[i].wrong;
		queshtml+= '<div class="portlet light portlet-fit no-space">'+
						'<div class="portlet-body no-space">'+
							'<div class="mt-element-list questions-sidebar-list">'+
								'<div class="mt-list-head list-default ext-1 blue-hoki">'+
									'<div class="row">'+
										'<div class="col-xs-12">'+
											'<div class="list-head-title-container">'+
												'<h3 class="list-title uppercase sbold">'+questionDisplay+'</h3>'+
											'</div>'+
										'</div>'+
										'<div class="col-xs-6">'+
											'<div class="list-head-summary-container">'+
												'<div class="list-done">'+
													'<div class="list-count badge badge-default ">+'+data.categories[i]['correct']+'</div> '+
													'<div class="list-label">Correct Ans</div>'+
												'</div>'+
											'</div>'+
										'</div>'+
										'<div class="col-xs-6">'+
											'<div class="list-head-summary-container">'+
												'<div class="list-done">'+
													'<div class="list-count badge badge-default last">-'+data.categories[i]['wrong']+'</div> '+
													'<div class="list-label">Wrong Ans</div>'+
												'</div>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>'+
								'<div class="mt-list-container list-default ext-1">'+
									'<div class="mt-list-title uppercase">List of questions'+
										'<span class="badge badge-default pull-right bg-hover-green-jungle">'+
											'<a class="font-white new-question" href="javascript:;">'+
												'<i class="fa fa-plus"></i>'+
											'</a>'+
										'</span>'+
									'</div>'+
									'<ul class="questions">';
		var questions = '';
		if(data.categories[i].questions.length > 0) {
			$('#modalAddEditQuestionsCategory tr[data-id="'+data.categories[i]['id']+'"]').find('.questionTypeSelect').attr('disabled', true);
			var questionNumber = 0;
			for(var j=0; j<data.categories[i].questions.length; j++) {
				if(data.categories[i].questions[j]['parentId'] == 0) {
					var dispQuestionNumber = (questionNumber + 1);
					if(data.categories[i].questions[j]['questionType'] == 4) {
						for(var k=0;k<data.categories[i].questions.length;k++) {
							if(data.categories[i].questions[k]['parentId'] == data.categories[i].questions[j]['id'])
								questionNumber++;
						}
					}
					else {
						questionNumber++;
					}

					var status = '<i class="fa fa-question-circle text-warning tooltips" data-original-title="The yellow symbol represents that the question has been imported, but has not been saved.<br>Please save the question so that the question is actually considered." data-placement="right"></i>';
					if(data.categories[i].questions[j]['status'] == 1)
						status = greenInfo;
					else if(data.categories[i].questions[j]['status'] == 0)
						status = redInfo;
					else
						status = '<i class="fa fa-question-circle text-warning tooltips" data-original-title="The yellow symbol represents that the question has been imported, but has not been saved.<br>Please save the question so that the question is actually considered." data-placement="right"></i>';
					var questionText = $(data.categories[i].questions[j]['question']).text();
					if(questionText.length > 35)
						questionText = questionText.substring(0, 35) + '...';
					if(data.categories[i].questions[j]['questionType'] == 0)
						questionType = 'MCQ';
					else if(data.categories[i].questions[j]['questionType'] == 1)
						questionType = 'T/F';
					else if(data.categories[i].questions[j]['questionType'] == 2)
						questionType = 'FTB';
					else if(data.categories[i].questions[j]['questionType'] == 3)
						questionType = 'MTF';
					else if(data.categories[i].questions[j]['questionType'] == 6)
						questionType = 'MAQ';
					else if(data.categories[i].questions[j]['questionType'] == 7)
						questionType = 'AUD';
					else if(data.categories[i].questions[j]['questionType'] == 4) {
						var subParts = 0;
						var parentId = data.categories[i].questions[j]['id'];
						var subParts = data.categories[i].questions[j]['subQuestions'].length;
						questionType = '<span class="pull-right">(<span class="totalQuestions">' + subParts + '</span> Questions)</span>CMP';
					}
					else if(data.categories[i].questions[j]['questionType'] == 5) {
						var subParts = 0;
						var parentId = data.categories[i].questions[j]['id'];
						var subParts = data.categories[i].questions[j]['subQuestions'].length;
						questionType = '<span class="pull-right">(<span class="totalQuestions">' + subParts + '</span> Sub-parts)</span>DPN';
					}
					questions+= '<li class="mt-list-item done" data-id="'+data.categories[i].questions[j]['id']+'" data-status="'+data.categories[i].questions[j]['status']+'" data-category="'+data.categories[i]['id']+'" data-questionType="' + data.categories[i].questions[j]['questionType'] + '">'+
							'<div class="list-icon-container">'+
								'<a href="javascript:;">'+
									status+
								'</a>'+
							'</div>'+
							'<div class="list-datetime"> <a href="javascript:void(0)" class="btn btn-danger btn-sm remove-question"><i class="fa fa-trash"></i></a> </div>'+
							'<div class="list-item-content">'+
								'<h3 class="uppercase">'+
									'<a href="javascript:;">'+questionType+'</a>'+
								'</h3>'+
								'<p class="text-overflow">'+ questionText +'</p>'+
							'</div>'+
						'</li>';
				}
			}
		}
		else {
			questions += '<li class="mt-list-item">No questions found</li>';
			$('#modalAddEditQuestionsCategory tr[data-id="'+data.categories[i]['id']+'"]').find('.questionTypeSelect').attr('disabled', false);
		}
		queshtml += questions;
		queshtml +=				'</ul>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>';
	}
	$('#categorySelect').find('option').remove();
	$('#categorySelect').append(catSelect);
	$('#categorySelect').val(tempQuestion);
	changeCategory(tempQuestion);
	tempQuestion = 0;
	$('#importExamModal .importCategorySelect').find('option').remove();
	$('#importExamModal .importCategorySelect').append(importCategory);
	$('#sectionQuestions').find('*').remove();
	$('#sectionQuestions').append(queshtml);
	$('.tooltips').tooltip({
		html: true
	});
	html += '<tr class="lastCol">'+
				'<td></td>'+
				'<td></td>'+
				'<td class="text-center"><strong>Total</strong></td>'+
				'<td class="text-center"><strong>'+total+'</strong></td>'+
				'<td></td>'+
				'<td></td>'+
				'<td></td>'+
				'<td></td>'+
			+ '</tr>';
	$('#sectionTable tbody').append(html);
	if(data.categories.length == 0) {
		$('#sectionTable').addClass('hide');
		$('#newQuestion').attr('disabled', true);
		$('#importQuestions').attr('disabled', true);
	}
	else {
		$('#sectionTable').removeClass('hide');
		$('#newQuestion').attr('disabled', false);
		$('#importQuestions').attr('disabled', false);
	}
	$('#sectionAdd').show();
	$('#sectionQuestions').show();
	$('#sectionDetail').show();
	$('.refresh-status').on('click', function() {
		var categoryId = $(this).parents('tr').attr('data-id');
		var sectionId = $(this).parents('tr').attr('data-section');
		checkCategoryStatus(categoryId,sectionId);
	});
	$('.remove-category').on('click', function() {
		if(parseInt($('#examStatus').attr('data-assoc')) > 0) {
			var category = $(this).parents('tr').attr('data-category');
			if(!($('#sectionTable tbody').find('tr[data-id="'+category+'"] .required').text() < $('#sectionTable tbody').find('tr[data-id="'+category+'"] .entered').text())) {
				toastr.error('You can\'t delete this category as there are '+terminologies["student_plural"].toLowerCase()+' associated with this exam. Please remove them in order to delete any other question.')
				return;
			}
		}
		if($(this).parents('tr').find('.entered').text() != 0)
			toastr.error("There are questions in this category. Please delete them first.");
		else {
			if($('#sectionTable tbody tr').length <= 2) {
				toastr.error("You can\'t delete this category as at least one category is required.");
				return;
			}
			if($('#examStop').length == 0)
				var con = confirm("Are you sure you want to delete this category.");
			else
				var con = confirm("Are you sure you want to delete this category. It will change your Assignment/Exam state to Draft/Not Live.");
			if(con) {
				var req = {};
				req.action = 'delete-category';
				req.categoryId = $(this).parents('tr').attr('data-id');
				req.sectionId = $('#sectionName').attr('data-id');
				var temp = $('#modalAddEditQuestionsCategory tbody tr.lastCol').find('td:eq(-1)').text();
				var tempId = $(this).parents('tr').attr('data-id');
				var minus = $('#modalAddEditQuestionsCategory tbody tr[data-id="'+tempId+'"]').find('.plus').val() * $('#modalAddEditQuestionsCategory tbody tr[data-id="'+tempId+'"]').find('.number').val();
				req.totalMarks = temp - minus;
				req.examId = examId;
				$.ajax({
					'type'	: 'post',
					'url'	: ApiEndPoint,
					'data'	: JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 0)
						toastr.error(res.message);
					else if(res.status == 1) {
						$('#examStop').click();
						fetchSectionDetails($('#sectionName').attr('data-id'));
					}
				});
			}
		}
	});
	$('#sectionQuestions .remove-question').on('click', function(e) {
		e.stopPropagation();
		if(parseInt($('#examStatus').attr('data-assoc')) > 0) {
			var category = $(this).parents('.mt-list-item').attr('data-category');
			if(parseInt($('#sectionTable tbody').find('tr[data-id="'+category+'"] .required').text()) == parseInt($('#sectionTable tbody').find('tr[data-id="'+category+'"] .entered').text())) {
				toastr.error('You can\'t cross the minimum required question limit as there are '+terminologies["student_plural"].toLowerCase()+' associated with this exam. Please remove them in order to delete any other question.')
				return;
			}
		}
		var con = false;
		if($('#examStop').length != 0) {
			var category = $(this).parents('.mt-list-item').attr('data-category');
			if(parseInt($('#sectionTable tbody').find('tr[data-id="'+category+'"] .required').text()) == parseInt($('#sectionTable tbody').find('tr[data-id="'+category+'"] .entered').text())) {
				con = confirm("State of your Assignment/Exam will be changed to draft as it crosses minimum specified criteria. Are you sure you want to delete this question?");
			}
			else
				con = confirm("Are you sure to delete this question");
		}
		else
			con = confirm("Are you sure to delete this question");
		if(con) {
			$('#sectionNewQuetion .cancel-button').click();
			var req = {};
			req.action = 'delete-question-exam';
			req.questionId = $(this).parents('.mt-list-item').attr('data-id');
			req.status = $(this).parents('.mt-list-item').attr('data-status');
			req.questionType = $(this).parents('.mt-list-item').attr('data-questionType');
			if(req.questionType == 4)
				req.amount = $(this).parents('.mt-list-item').find('.totalQuestions').text();
			req.categoryId = $(this).parents('.mt-list-item').attr('data-category');
			req.sectionId = $('#sectionName').attr('data-id');
			req.examId = examId;
			req.subjectId = subjectId;
			$('#sectionNewQuestion .cancel-button').click();
			$.ajax({
				'type'	: 'post',
				'url'	: ApiEndPoint,
				'data'	: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else if(res.status == 1)
					fetchSectionDetails($('#sectionName').attr('data-id'));
			});
		}
	});
	$('#sectionQuestions .questions .mt-list-item').each(function() {
		if($(this).attr('data-id') != undefined) {
			$(this).on('click', function() {
				$('.mt-list-item.done.active').removeClass('active');
				$('.marks').hide();
				$(this).addClass('active');
				fetchQuestionDetails($(this).attr('data-id'));
				scrollToElem("#formQuestion",1000);
			});
		}
	});
	if($("#sectionTable tbody tr").length>1) {
		$('#newQuestion').trigger("click");
	}
}

function checkCategoryStatus(categoryId,sectionId) {
	var req = {};
	req.action = 'check-category-status';
	req.categoryId = categoryId;
	req.sectionId = sectionId;
	req.examId = examId;
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else if(res.status == 1) {
			fetchExamSections();
		}
	});
}

function fetchSectionStatus(sectionId) {
	var req = {};
	var res;
	req.action = 'get-section-status';
	req.examId = examId;
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		res.sectionId = sectionId;
		fillSectionStatus(res);
	});
}

function fillSectionStatus(data) {
	var c=0;
	for(var i=0; i<data.length; i++) {
		var status = 'Draft';
		if(data[i]['status'] == 1) {
			status = 'Completed';
			c++;
		}
		else
			status = 'Draft';
		var t = $('#sectionName').closest('div.caption').find('.badge:eq(0)');
		if(status == 'Draft')
			t.text(status).removeClass('bg-green-jungle').addClass('bg-blue-loki');
		else if(status == 'Completed')
			t.text(status).removeClass('bg-blue-loki').addClass('bg-green-jungle');
		if(!$('#sections').find('[data-id="'+data[i]['id']+'"]').hasClass('deleted-item')) {
			var t = $('#sections').find('[data-id="'+data[i]['id']+'"]').get(0).outerHTML;
			$('#sections').find('[data-id="'+data[i]['id']+'"]').remove();
			if(status == 'Draft') {
				$('#pending-simple ul').append(t);
				//t.text(status).removeClass('bg-green-jungle').addClass('bg-blue-loki');
			}
			else if(status == 'Completed') {
				$('#completed-simple ul').append(t);
				//t.text(status).removeClass('bg-blue-loki').addClass('bg-green-jungle');
			}
			$('.js-completed-count').text($('#completed-simple ul li').length);
			$('.js-pending-count').text($('#pending-simple ul li').length);
		}
		if(data[i].id == data.sectionId && data[i].randomQuestions == 0) {
			$('#modalAddEditQuestionsCategory #randomQuestions').prop('checked', true);
			$("#randomQuestions").prop("checked", false);
			$("#randomQuestions").bootstrapSwitch();
			$('#modalAddEditQuestionsCategory table.table tbody').sortable({revert : true,
				placeholder : 'sort-placeholder',
				cursor : 'move',
				forcePlaceholderSize : true,
				tolerance : 'pointer',
				opacity : 0.6,
				update : function() {
					for(var i=0; i< $('#modalAddEditQuestionsCategory table.table tbody').find('tr').length; i++) {
						sortOrder[i] = $('#modalAddEditQuestionsCategory table.table tbody tr:eq('+i+')').attr('data-id');
					}
				},
				start: function (event, ui) {
					// Build a placeholder cell that spans all the cells in the row
					var cellCount = 0;
					$('td, th', ui.helper).each(function () {
						// For each TD or TH try and get it's colspan attribute, and add that or 1 to the total
						var colspan = 1;
						var colspanAttr = $(this).attr('colspan');
						if (colspanAttr > 1) {
							colspan = colspanAttr;
						}
						cellCount += colspan;
					});
					// Add the placeholder UI - note that this is the item's content, so TD rather than TR
					ui.placeholder.html('<td colspan="' + cellCount + '">&nbsp;</td>');
				}
			});
			$('.note-order-drag').removeClass('hide');
		}
		else if(data[i].id == data.sectionId){
			$('#modalAddEditQuestionsCategory #randomQuestions').prop('checked', false);
			$("#randomQuestions").prop("checked", true);
			$("#randomQuestions").bootstrapSwitch();
			if($('#modalAddEditQuestionsCategory table.table tbody').hasClass('ui-sortable'))
				$('#modalAddEditQuestionsCategory table.table tbody').sortable('destroy');
			$('.note-order-drag').addClass('hide');
		}
	}
	if(c == $('#sections li.mt-list-item:not(.deleted-item)').length) {
		$('#examStatus').attr('disabled', false);
		$('#examStatus').removeClass('btn-primary');
		$('#examStatus').addClass('green-jungle');
		if($('#examStop').length != 0)
			$('#examStatus').text('Live');
		else
			$('#examStatus').text('Go Live');
	}
	else {
		$('#examStop').remove();
		$('#examStatus').text('Draft');
		$('#examStatus').removeClass('green-jungle');
		$('#examStatus').addClass('btn-primary');
		$('#examStatus').attr('disabled', true);
	}
}

function fillModalCategory(data) {
	$('#modalAddEditQuestionsCategory .has-error').each(function(){
		$(this).removeClass('has-error');
	});
	$('#modalAddEditQuestionsCategory .help-block').remove();
	for(var i=0; i<data.categories.length; i++) {
		$('#modalAddEditQuestionsCategory table.table tbody tr:eq('+i+')').attr('data-id', data.categories[i].id);
		$('#modalAddEditQuestionsCategory table.table tbody tr:eq('+i+') .questionTypeSelect').val(data.categories[i].questionType);
		$('#modalAddEditQuestionsCategory table.table tbody tr:eq('+i+') .plus').val(data.categories[i].correct);
		$('#modalAddEditQuestionsCategory table.table tbody tr:eq('+i+') .minus').val(data.categories[i].wrong);
		$('#modalAddEditQuestionsCategory table.table tbody tr:eq('+i+') .number').val(data.categories[i].required);
		$('#modalAddEditQuestionsCategory table.table tbody tr:eq('+i+') .number').trigger("blur");
	}
}

function changeCategory(value) {
	//destroying the ckeditor of question
	$('#qq').text('Question');
	//$('div[aria-describedby="cke_43"]').html('Please enter the question text here');
	$('.add-option').attr('disabled', false);
	$('#questionInfo').addClass('hide');
	$('.subQuestionNumbers').hide();
	$('.temp').remove();
	numbering();
	flagCategory = true;
	calculateMarks();
	$('#hiddenQuestionType').val(categoryQuestionType[value]);
	$('.marks').hide();
	$('#eq').show();
	var type = $('#hiddenQuestionType').val();
	if($('#categorySelect option[value="0"]').css('display') == 'none')
		flagCategory = false;
	if(type === '0') {
		$('.desc-block').removeClass('hide');
		$('#trueFalse').addClass('hide');
		$('#ftb').addClass('hide');
		$('#match').addClass('hide');
		$('#compro').addClass('hide');
		$('#optionsMul').addClass('hide');
		$('#audio').addClass('hide');
		$('#options').removeClass('hide');
		$('#questionInfo').attr('data-original-title', 'Multiple Choice Questions are the  most common type of questions used across exams<br>They only have one answer option as the correct answer').removeClass('hide');
	}
	else if(type == 1) {
		$('.desc-block').removeClass('hide');
		$('#options').addClass('hide');
		$('#ftb').addClass('hide');
		$('#compro').addClass('hide');
		$('#optionsMul').addClass('hide');
		$('#match').addClass('hide');
		$('#audio').addClass('hide');
		$('#trueFalse').removeClass('hide');
		$('#questionInfo').attr('data-original-title', 'True or False type of Questions  generally have a statement which is either true or false. please use the radio button to select the correct answer').removeClass('hide');
	}
	else if(type == 2) {
		$('.desc-block').removeClass('hide');
		$('#options').addClass('hide');
		$('#trueFalse').addClass('hide');
		$('#match').addClass('hide');
		$('#compro').addClass('hide');
		$('#optionsMul').addClass('hide');
		$('#audio').addClass('hide');
		$('#ftb').removeClass('hide');
		$('#questionInfo').attr('data-original-title', 'Fill in the blanks have few words/word, which are missing. Please write the exact/words in the box provided<br>'+terminologies["student_plural"]+' also will  have to write down the exact word/words.').removeClass('hide');
	}
	else if(type == 3) {
		$('.desc-block').removeClass('hide');
		$('#options').addClass('hide');
		$('#trueFalse').addClass('hide');
		$('#ftb').addClass('hide');
		$('#compro').addClass('hide');
		$('#optionsMul').addClass('hide');
		$('#audio').addClass('hide');
		$('#match').removeClass('hide');
		$('#questionInfo').attr('data-original-title', 'Match the Following basically consists of 2 columns where '+terminologies["student_plural"].toLowerCase()+' have to match the statements on both the sides<br>We have 2 columns A and B, such that every entry on A side has its corresponding entry on the B side. '+terminologies["student_plural"]+' will see the entries on the B side in jumbled order').removeClass('hide');
	}
	else if(type == 4 || type == 5) {
		$('div[aria-describedby="cke_43"]').html('Please enter the passage here. A Passage could be a few paragraphs or a problem statement.');
		if(type == 5) {
			$('#sectionNewQuestion .add-question').html('<i class="fa fa-plus-circle"> Sub-parts</i>');
			calculateMarks();
			$('#questionInfo').attr('data-original-title', 'This question type has multiple sub parts.The marks allotted for this question type will be equally divided among all the sub parts. So, if  there is a dependent question of total  5 marks, with 2 sub parts, then each sub part will have 2.5 marks. The total score of the student will be a sum  of all the  sub parts of the question(dependent on all the sub parts)').removeClass('hide');
		}
		else {
			$('#sectionNewQuestion .add-question').html('<i class="fa fa-plus-circle"> Question</i>');
			$('#questionInfo').attr('data-original-title', 'This question type consists of a  common passage/ paragraphs, followed by a couple of independent questions. This passage/paragraphs will  appear every time a question appears to the student').removeClass('hide');
		}
		$('#qq').text('Passage/Paragraph');
		$('#eq').hide();
		$('.desc-block').addClass('hide');
		$('#options').addClass('hide');
		$('#optionsMul').addClass('hide');
		$('#trueFalse').addClass('hide');
		$('#ftb').addClass('hide');
		$('#match').addClass('hide');
		$('#audio').addClass('hide');
		$('#compro .ftbs').addClass('hide');
		$('#compro .trueFalse').addClass('hide');
		$('#compro .optionsMul').addClass('hide');
		$('#compro').removeClass('hide');
	}
	else if(type == 6) {
		$('.desc-block').removeClass('hide');
		$('#options').addClass('hide');
		$('#trueFalse').addClass('hide');
		$('#ftb').addClass('hide');
		$('#compro').addClass('hide');
		$('#match').addClass('hide');
		$('#audio').addClass('hide');
		$('#optionsMul').removeClass('hide');
		$('#questionInfo').attr('data-original-title', 'These type of questions can have more than one answer option as the correct answer. Please use the check boxes to tick more than one correct option').removeClass('hide');
	}
	else if(type == 7) {
		$('.desc-block').removeClass('hide');
		$('#trueFalse').addClass('hide');
		$('#ftb').addClass('hide');
		$('#match').addClass('hide');
		$('#compro').addClass('hide');
		$('#optionsMul').addClass('hide');
		$('#options').addClass('hide');
		$('#audio').removeClass('hide');
		$('#questionInfo').attr('data-original-title', 'Audio Questions have an audio clip in the question which you have to hear and answer').removeClass('hide');
	}
}

function allowDecimal(event, obj) {
	if ((event.which != 46 || obj.val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) && event.which != 8) {
		return false;
	}
	var text = obj.val();
	if ((text.indexOf('.') != -1) && (text.substring(text.indexOf('.')).length > 2)) {
		if (event.which != 8)
			return false;
	}
	return true;
}

function fetchQuestionDetails(id) {
	$('#sectionNewQuestion .cancel-button').click();
	var req = {};
	req.action = 'get-question-details';
	req.examId = examId;
	req.questionId = id;
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			//console.log(res);
			if(res.question[0].questionType == 0)
				fillQuestionEditMcq(res);
			else if(res.question[0].questionType == 1)
				fillQuestionEditTrueFalse(res);
			else if(res.question[0].questionType == 2)
				fillQuestionEditFtb(res);
			else if(res.question[0].questionType == 3)
				fillQuestionEditMtf(res);
			else if(res.question[0].questionType == 4 || res.question[0].questionType == 5)
				fillQuestionEditCmp(res);
			else if(res.question[0].questionType == 6)
				fillQuestionEditMaq(res);
			else if(res.question[0].questionType == 7)
				fillQuestionEditAud(res);
		}
		else
			toastr.error(res.exception);
	});
}

function fillQuestionEditMcq(data) {
	if(data.question[0].status == 2) {
		var warningSpan = '<div class="text-danger" style="margin-left: 15px;margin-right: 15px;">Duplicate : Question copied from same Assignment/Exam. Edit question to save and increase question count in category.</div>';
		$(warningSpan).insertBefore('#questionId');
	}
	else
		$('#sectionNewQuestion').find('div.text-danger').remove();
	$('#sectionNewQuestion').removeClass('hide');
	$('#trueFalse').addClass('hide');
	$('#ftb').addClass('hide');
	$('.desc-block').removeClass('hide');
	$('#match').addClass('hide');
	$('#compro').addClass('hide');
	$('#optionsMul').addClass('hide');
	$('#audio').addClass('hide');
	$('#audio .js-file-path').html('');
	$('#audio .js-file-path').attr('data-path','');
	$('#options').removeClass('hide');
	$('#questionId').val(data.question[0]['id']);
	$('#hiddenQuestionType').val(data.question[0]['questionType']);
	$('#categorySelect option').hide();
	$('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').show();
	flagCategory = true;
	$('#categorySelect').val($('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').val());
	oldCategory = data.question[0]['categoryId'];
	CKEDITOR.instances['question'].setData(data.question[0]['question']);
	$('#selectDifficulty').val(data.question[0]['difficulty']);
	ms.setSelection(data.tags);
	CKEDITOR.instances['desc'].setData(data.question[0]['description']);
	if(data.question[0]['questionType'] == 0) {
		if(data.options.length > 2) {
			addOptions(data.options.length - 2);
			if($('#options input[type="text"]').length >= 6)
				$('#options .add-option').attr('disabled', true);
			else
				$('#options .add-option').attr('disabled', false);
		}
		for(var i=0; i<data.options.length; i++) {
			$('#options').find('input[type="text"]:eq('+i+')').val(data.options[i]['option']);
			var editor = $('#options').find('[type="text"]:eq('+i+')').attr('id');
			if(data.options[i]['correct'] == 1)
				prop = true;
			else
				prop = false;
			$('#options').find('input[type="radio"]:eq('+i+')').prop('checked', prop);
		}
	}
}

function fillQuestionEditMaq(data) {
	if(data.question[0].status == 2) {
		toastr.error('Duplicate : Question copied from same Assignment/Exam. Edit question to save and increse question count in category.');
	}
	$('#sectionNewQuestion').removeClass('hide');
	$('#trueFalse').addClass('hide');
	$('#ftb').addClass('hide');
	$('#match').addClass('hide');
	$('.desc-block').removeClass('hide');
	$('#compro').addClass('hide');
	$('#options').addClass('hide');
	$('#audio').addClass('hide');
	$('#audio .js-file-path').html('');
	$('#audio .js-file-path').attr('data-path', '');
	$('#optionsMul').removeClass('hide');
	$('#questionId').val(data.question[0]['id']);
	$('#hiddenQuestionType').val(data.question[0]['questionType']);
	$('#categorySelect option').hide();
	$('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').show();
	flagCategory = true;
	$('#categorySelect').val($('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').val());
	oldCategory = data.question[0]['categoryId'];
	CKEDITOR.instances['question'].setData(data.question[0]['question']);
	$('#selectDifficulty').val(data.question[0]['difficulty']);
	ms.setSelection(data.tags);
	CKEDITOR.instances['desc'].setData(data.question[0]['description']);
	if(data.question[0]['questionType'] == 6) {
		if(data.options.length > 2) {
			addOptionsMul(data.options.length - 2);
			if($('#optionsMul input[type="text"]').length >=6)
				$('#optionsMul .add-option').attr('disabled', true);
			else
				$('#optionsMul .add-option').attr('disabled', false);
		}
		for(var i=0; i<data.options.length; i++) {
			$('#optionsMul').find('input[type="text"]:eq('+i+')').val(data.options[i]['option']);
			var editor = $('#optionsMul').find('input[type="text"]:eq('+i+')').attr('id');
			if(data.options[i]['correct'] == 1)
				prop = true;
			else
				prop = false;
			$('#optionsMul').find('input[type="checkbox"]:eq('+i+')').prop('checked', prop);
		}
	}
}

function fillQuestionEditTrueFalse(data) {
	if(data.question[0].status == 2) {
		var warningSpan = '<div class="text-danger" style="margin-left: 15px;margin-right: 15px;">Duplicate : Question copied from same Assignment/Exam. Edit question to save and increse question count in category.</div>';
		$(warningSpan).insertBefore('#questionId');
	}
	else
		$('#sectionNewQuestion').find('div.text-danger').remove();
	$('#sectionNewQuestion').removeClass('hide');
	$('#trueFalse').removeClass('hide');
	$('#ftb').addClass('hide');
	$('#compro').addClass('hide');
	$('#optionsMul').addClass('hide');
	$('#match').addClass('hide');
	$('.desc-block').removeClass('hide');
	$('#options').addClass('hide');
	$('#audio').addClass('hide');
	$('#audio .js-file-path').html('');
	$('#audio .js-file-path').attr('data-path', '');
	$('#questionId').val(data.question[0]['id']);
	$('#hiddenQuestionType').val(data.question[0]['questionType']);
	$('#categorySelect option').hide();
	$('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').show();
	flagCategory = true;
	$('#categorySelect').val($('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').val());
	oldCategory = data.question[0]['categoryId'];
	CKEDITOR.instances['question'].setData(data.question[0]['question']);
	$('#selectDifficulty').val(data.question[0]['difficulty']);
	ms.setSelection(data.tags);
	CKEDITOR.instances['desc'].setData(data.question[0]['description']);
	if(data.answer.length > 0) {
		if(data.answer[0].answer == 1)
			$('#true').prop('checked', true);
		else if(data.answer[0].answer == 0)
			$('#false').prop('checked', true);
	}
}

function fillQuestionEditFtb(data) {
	if(data.question[0].status == 2) {
		var warningSpan = '<div class="text-danger" style="margin-left: 15px;margin-right: 15px;">Duplicate : Question copied from same Assignment/Exam. Edit question to save and increse question count in category.</div>';
		$(warningSpan).insertBefore('#questionId');
	}
	else
		$('#sectionNewQuestion').find('div.text-danger').remove();
	$('#sectionNewQuestion').removeClass('hide');
	$('#trueFalse').addClass('hide');
	$('#match').addClass('hide');
	$('#compro').addClass('hide');
	$('.desc-block').removeClass('hide');
	$('#optionsMul').addClass('hide');
	$('#options').addClass('hide');
	$('#audio').addClass('hide');
	$('#audio .js-file-path').html('');
	$('#audio .js-file-path').attr('data-path', '');
	$('#ftb').removeClass('hide');
	$('#questionId').val(data.question[0]['id']);
	$('#hiddenQuestionType').val(data.question[0]['questionType']);
	$('#categorySelect option').hide();
	$('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').show();
	flagCategory = true;
	$('#categorySelect').val($('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').val());
	oldCategory = data.question[0]['categoryId'];
	CKEDITOR.instances['question'].setData(data.question[0]['question']);
	$('#selectDifficulty').val(data.question[0]['difficulty']);
	ms.setSelection(data.tags);
	CKEDITOR.instances['desc'].setData(data.question[0]['description']);
	$('#question').blur();
	for(var i=0; i<data.blanks.length; i++) {
		$('#ftb').find('input[type="text"]:eq('+i+')').val(data.blanks[i]['blanks']);
		var editor = $('#ftb').find('input[type="text"]:eq('+i+')').attr('id');
	}
}

function fillQuestionEditMtf(data) {
	if(data.question[0].status == 2) {
		var warningSpan = '<div class="text-danger" style="margin-left: 15px;margin-right: 15px;">Duplicate : Question copied from same Assignment/Exam. Edit question to save and increse question count in category.</div>';
		$(warningSpan).insertBefore('#questionId');
	}
	else
		$('#sectionNewQuestion').find('div.text-danger').remove();
	$('#sectionNewQuestion').removeClass('hide');
	$('#trueFalse').addClass('hide');
	$('#ftb').addClass('hide');
	$('#options').addClass('hide');
	$('#audio').addClass('hide');
	$('#audio .js-file-path').html('');
	$('#audio .js-file-path').attr('data-path', '');
	$('#match').removeClass('hide');
	$('#questionId').val(data.question[0]['id']);
	$('#hiddenQuestionType').val(data.question[0]['questionType']);
	$('#categorySelect option').hide();
	$('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').show();
	flagCategory = true;
	$('#categorySelect').val($('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').val());
	oldCategory = data.question[0]['categoryId'];
	CKEDITOR.instances['question'].setData(data.question[0]['question']);
	$('#selectDifficulty').val(data.question[0]['difficulty']);
	ms.setSelection(data.tags);
	CKEDITOR.instances['desc'].setData(data.question[0]['description']);
	if(data.matches.length > 2) {
		$('#match .row.generated').remove();
		addOptionMatch(data.matches.length - 2);
		if($('#match input[type="text"].a').length >= 10)
			$('#match .add-option').attr('disabled', true);
		else
			$('#match .add-option').attr('disabled', false);
	}
	for(var i=0; i<data.matches.length; i++) {
		$('#match .match:eq('+i+')').find('input[type="text"].a').val(data.matches[i]['columnA']);
		var editor = $('#match .match:eq('+i+')').find('input[type="text"].a').attr('id');
		$('#match .match:eq('+i+')').find('input[type="text"].b').val(data.matches[i]['columnB']);
		editor = $('#match .match:eq('+i+')').find('input[type="text"].b').attr('id');
	}
}

function fillQuestionEditCmp(data) {
	if(data.question[0].status == 2) {
		var warningSpan = '<div class="text-danger" style="margin-left: 15px;margin-right: 15px;">Duplicate : Question copied from same Assignment/Exam. Edit question to save and increse question count in category.</div>';
		$(warningSpan).insertBefore('#questionId');
	}
	else
	$('#sectionNewQuestion').find('div.text-danger').remove();
	$('#sectionNewQuestion').removeClass('hide');
	$('#trueFalse').addClass('hide');
	$('#ftb').addClass('hide');
	if(data.question[0].questionType == 5)
		$('#sectionNewQuestion .add-question').html('<i class="fa fa-plus-circle"> Sub-parts</i>');
	else
		$('#sectionNewQuestion .add-question').html('<i class="fa fa-plus-circle"> Question</i>');
	$('#match').addClass('hide');
	$('#options').addClass('hide');
	$('#audio').addClass('hide');
	$('#audio .js-file-path').html('');
	$('#audio .js-file-path').attr('data-path', '');
	$('#compro .trueFalse').addClass('hide');
	$('#compro .ftbs').addClass('hide');
	$('#compro .optionsMul').addClass('hide');
	$('#compro').removeClass('hide');
	$('.desc-block').addClass('hide');
	$('#questionId').val(data.question[0]['id']);
	$('#hiddenQuestionType').val(data.question[0]['questionType']);
	$('#categorySelect option').hide();
	$('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').show();
	flagCategory = true;
	$('#categorySelect').val($('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').val());
	oldCategory = data.question[0]['categoryId'];
	CKEDITOR.instances['question'].setData(data.question[0]['question']);
	$('#selectDifficulty').val(data.question[0]['difficulty']);
	ms.setSelection(data.tags);
	CKEDITOR.instances['desc'].setData(data.question[0]['description']);
	$('#compro .sectionNewQuestion.generated').remove();
	$('#compro .questionTypeSelect').val(data.childQuestions[0].question.questionType);
	$('#compro .questionId').val(data.childQuestions[0].question.id);
	$('#compro .question').val(data.childQuestions[0].question.question);
	
	var editor = $('#compro .question').attr('id');
	CKEDITOR.instances[editor].setData(data.childQuestions[0].question.question);
	$('#compro .desc').val(data.childQuestions[0].question.description);
	editor = $('#compro .desc').attr('id');
	CKEDITOR.instances[editor].setData(data.childQuestions[0].question.description);
	if(data.childQuestions[0].question.questionType == 0) {
		$('#compro .ftbs').addClass('hide');
		$('#compro .trueFalse').addClass('hide');
		$('#compro .optionsMul').addClass('hide');
		$('#compro .options').removeClass('hide');
		if(data.childQuestions[0].answer.length > 2) {
			addChildOptions(data.childQuestions[0].answer.length - 2, $('#compro .options .add-div'), 0);
			if($('#compro .sectionNewQuestion:eq(0) .options').find('input[type="text"]').length >= 6)
				$('#compro .sectionNewQuestion:eq(0) .options .add-option').attr('disabled', true);
			else
				$('#compro .sectionNewQuestion:eq(0) .options .add-option').attr('disabled', false);
		}
		var options = $('#compro input[type="text"].option');
		var check = $('#compro input[type="radio"]');
		for(var j=0; j<data.childQuestions[0].answer.length; j++) {
			options[j]['value'] = data.childQuestions[0].answer[j].option;
			editor = options[j]['id'];
			if(data.childQuestions[0].answer[j].correct == 1)
				check[j]['checked'] = true;
		}
	}
	else if(data.childQuestions[0].question.questionType == 1) {
		$('#compro .ftbs').addClass('hide');
		$('#compro .options').addClass('hide');
		$('#compro .optionsMul').addClass('hide');
		$('#compro .trueFalse').removeClass('hide');
		if(data.childQuestions[0].answer.length > 0) {
			if(data.childQuestions[0].answer[0].answer == 1)
				$('#compro .trueFalse .true').prop('checked', true);
			if(data.childQuestions[0].answer[0].answer == 0)
				$('#compro .trueFalse .false').prop('checked', true);
		}
	}
	else if(data.childQuestions[0].question.questionType == 2) {
		$('#compro .options').addClass('hide');
		$('#compro .trueFalse').addClass('hide');
		$('#compro .optionsMul').addClass('hide');
		$('#compro .ftbs').removeClass('hide');
		$('#compro .question').blur();
		var blanks = $('#compro .ftbs input[type="text"].ftb');
		for(var j=0; j<data.childQuestions[0].answer.length; j++) {
			blanks[j]['value'] = data.childQuestions[0].answer[j].blanks;
			editor = blanks[j]['id'];
		}
	}
	else if(data.childQuestions[0].question.questionType == 6) {
		$('#compro .ftbs').addClass('hide');
		$('#compro .options').addClass('hide');
		$('#compro .trueFalse').addClass('hide');
		$('#compro .optionsMul').removeClass('hide');
		if(data.childQuestions[0].answer.length > 2)
			addChildOptionsMul(data.childQuestions[0].answer.length - 2, $('#compro .optionsMul .add-div'));
			if($('#compro .sectionNewQuestion:eq(0) .optionsMul').find('input[type="text"]').length >= 6)
				$('#compro .sectionNewQuestion:eq(0) .optionsMul .add-option').attr('disabled', true);
			else
				$('#compro .sectionNewQuestion:eq(0) .optionsMul .add-option').attr('disabled', false);
		var options = $('#compro input[type="text"].optionMul');
		var check = $('#compro input[type="checkbox"]');
		for(var j=0; j<data.childQuestions[0].answer.length; j++) {
			options[j]['value'] = data.childQuestions[0].answer[j].option;
			editor = options[j]['id'];
			if(data.childQuestions[0].answer[j].correct == 1)
				check[j]['checked'] = true;
		}
	}
	if(data.childQuestions.length > 1) {
		addChildQuestion(data.childQuestions.length - 1, 0);
		if(data.question[0]['questionType'] == 5) {
			$('.marks').show();
			calculateMarks();
		}
		for(var i=1; i<data.childQuestions.length; i++) {
			$('#compro .sectionNewQuestion:eq('+i+')').find('.questionTypeSelect').val(data.childQuestions[i].question.questionType);
			$('#compro .sectionNewQuestion:eq('+i+')').find('.questionId').val(data.childQuestions[i].question.id);
			$('#compro .sectionNewQuestion:eq('+i+')').find('.question').val(data.childQuestions[i].question.question);
			var editor = $('#compro .sectionNewQuestion:eq('+i+')').find('.question').attr('id');
			CKEDITOR.instances[editor].setData(data.childQuestions[i].question.question);
			$('#compro .sectionNewQuestion:eq('+i+')').find('.desc').val(data.childQuestions[i].question.description);
			editor = $('#compro .sectionNewQuestion:eq('+i+')').find('.desc').attr('id');
			CKEDITOR.instances[editor].setData(data.childQuestions[i].question.description);
			if(data.childQuestions[i].question.questionType == 0) {
				$('#compro .sectionNewQuestion:eq('+i+')').find('.ftbs').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.optionsMul').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.trueFalse').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.options').removeClass('hide');
				if(data.childQuestions[i].answer.length > 2) {
					addChildOptions(data.childQuestions[i].answer.length - 2, $('#compro .sectionNewQuestion:eq('+i+')').find('.options .add-div'), i);
					if($('#compro .sectionNewQuestion:eq('+i+') .options').find('input[type="text"]').length >=6)
						$('#compro .sectionNewQuestion:eq('+i+') .options .add-option').attr('disabled', true);
					else
						$('#compro .sectionNewQuestion:eq(0) .options .add-option').attr('disabled', false);
				}
				var options = $('#compro .sectionNewQuestion:eq('+i+')').find('input[type="text"].option');
				var check = $('#compro .sectionNewQuestion:eq('+i+')').find('input[type="radio"]');
				for(var j=0; j<data.childQuestions[i].answer.length; j++) {
					options[j]['value'] = data.childQuestions[i].answer[j].option;
					editor = options[j]['id'];
					if(data.childQuestions[i].answer[j].correct == 1)
						check[j]['checked'] = true;
				}
			}
			else if(data.childQuestions[i].question.questionType == 1) {
				$('#compro .sectionNewQuestion:eq('+i+')').find('.ftbs').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.options').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.optionsMul').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.trueFalse').removeClass('hide');
				if(data.childQuestions[i].answer.length > 0) {
					if(data.childQuestions[i].answer[0].answer != 'undefined') {
						if(data.childQuestions[i].answer[0].answer == 1)
							$('#compro .sectionNewQuestion:eq('+i+')').find('.trueFalse .true').prop('checked', true);
						if(data.childQuestions[i].answer[0].answer == 0)
							$('#compro .sectionNewQuestion:eq('+i+')').find('.trueFalse .false').prop('checked', true);
					}
				}
			}
			else if(data.childQuestions[i].question.questionType == 2) {
				$('#compro .sectionNewQuestion:eq('+i+')').find('.options').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.trueFalse').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.optionsMul').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.ftbs').removeClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.question').blur();
				var blanks = $('#compro .sectionNewQuestion:eq('+i+')').find('.ftbs input[type="text"].ftb');
				for(var j=0; j<data.childQuestions[i].answer.length; j++) {
					blanks[j]['value'] = data.childQuestions[i].answer[j].blanks;
					editor = blanks[j]['id'];
				}
			}
			else if(data.childQuestions[i].question.questionType == 6) {
				$('#compro .sectionNewQuestion:eq('+i+')').find('.ftbs').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.options').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.trueFalse').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.optionsMul').removeClass('hide');
				if(data.childQuestions[i].answer.length > 2)
					addChildOptionsMul(data.childQuestions[i].answer.length - 2, $('#compro .sectionNewQuestion:eq('+i+')').find('.optionsMul .add-div'));
					if($('#compro .sectionNewQuestion:eq('+i+') .optionsMul').find('input[type="text"]').length >=6)
						$('#compro .sectionNewQuestion:eq('+i+') .optionsMul .add-option').attr('disabled', true);
					else
						$('#compro .sectionNewQuestion:eq(0) .optionsMul .add-option').attr('disabled', false);
				var options = $('#compro .sectionNewQuestion:eq('+i+')').find('input[type="text"].optionMul');
				var check = $('#compro .sectionNewQuestion:eq('+i+')').find('input[type="checkbox"]');
				for(var j=0; j<data.childQuestions[i].answer.length; j++) {
					options[j]['value'] = data.childQuestions[i].answer[j].option;
					editor = options[j]['id'];
					if(data.childQuestions[i].answer[j].correct == 1)
						check[j]['checked'] = true;
				}
			}
		}
	}
	numbering();
}

function fillQuestionEditAud(data) {
	if(data.question[0].status == 2) {
		var warningSpan = '<div class="text-danger" style="margin-left: 15px;margin-right: 15px;">Duplicate : Question copied from same Assignment/Exam. Edit question to save and increase question count in category.</div>';
		$(warningSpan).insertBefore('#questionId');
	}
	else
		$('#sectionNewQuestion').find('div.text-danger').remove();
	$('#sectionNewQuestion').removeClass('hide');
	$('#trueFalse').addClass('hide');
	$('#ftb').addClass('hide');
	$('.desc-block').removeClass('hide');
	$('#match').addClass('hide');
	$('#compro').addClass('hide');
	$('#optionsMul').addClass('hide');
	$('#options').addClass('hide');
	$('#audio').removeClass('hide');
	$('#audio .js-file-path').html('');
	$('#audio .js-file-path').attr('data-path', '');
	$('#questionId').val(data.question[0]['id']);
	$('#hiddenQuestionType').val(data.question[0]['questionType']);
	$('#categorySelect option').hide();
	$('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').show();
	flagCategory = true;
	$('#categorySelect').val($('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').val());
	oldCategory = data.question[0]['categoryId'];
	CKEDITOR.instances['question'].setData(data.question[0]['question']);
	$('#selectDifficulty').val(data.question[0]['difficulty']);
	ms.setSelection(data.tags);
	CKEDITOR.instances['desc'].setData(data.question[0]['description']);
	if(data.question[0]['questionType'] == 7) {
		if(data.options.length > 2) {
			addAudioOptions(data.options.length - 2);
			if($('#audio input[type="text"]').length >= 6)
				$('#audio .add-option').attr('disabled', true);
			else
				$('#audio .add-option').attr('disabled', false);
		}
		for(var i=0; i<data.options.length; i++) {
			$('#audio').find('input[type="text"]:eq('+i+')').val(data.options[i]['option']);
			var editor = $('#audio').find('[type="text"]:eq('+i+')').attr('id');
			if(data.options[i]['correct'] == 1)
				prop = true;
			else
				prop = false;
			$('#audio').find('input[type="radio"]:eq('+i+')').prop('checked', prop);
		}
	}
	if (!!data.audio) {
		var mp3 = data.audio;
		$('.js-file-path').html(
			'<audio controls="control" id="audioShowcasePlayer" preload="none" src="'+mp3+'" type="audio/mp3"></audio>'
		);
		$('.js-file-path').attr('data-path', mp3);
		player = new MediaElementPlayer('#audioShowcasePlayer', {type: 'audio/mp3'});
		var sources = [
			{ src: mp3, type: 'audio/mp3' }
		];

		player.setSrc(sources);
		player.load();
	}
}

function fetchImportCourses() {
	var req = {};
	var res;
	req.action = 'get-courses-for-exam';
	req.import = true;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 1)
			fillImportCourseSelect(res);
	});
}

function fillImportCourseSelect(data) {
	$('.courseSelect').find('option').remove();
	var html = '<option value="0">Select '+terminologies["course_single"]+'</option>';
	for(var i=0; i<data.courses.length; i++) {
		html += '<option value="' + data.courses[i].id + '">' + data.courses[i].name + '</option>';
	}
	$('.courseSelect').append(html);
	$('.courseSelect').prop('disabled', false);
}

function fetchImportSubjectsQuestions() {
	var req = {};
	var res;
	req.action = 'get-subjects-for-exam';
	req.import = true;
	req.courseId = $('#importSubjectQuestionsModal .courseSelect').val();
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 1)
			fillImportSubjectQuestions(res);
	});
}

function fillImportSubjectQuestions(data) {
	$('#importSubjectQuestionsModal .subjectSelect').find('option').remove();
	var html = '<option value="0">Select '+terminologies["subject_single"]+'</option>';
	for(var i=0; i<data.subjects.length; i++) {
		html += '<option value="' + data.subjects[i].id + '">' + data.subjects[i].name + '</option>';
	}
	$('#importSubjectQuestionsModal .subjectSelect').append(html);
	$('#importSubjectQuestionsModal .subjectSelect').attr('disabled', false);
}

function fetchImportSubjectExams() {
	var req = {};
	var res;
	req.action = 'get-exams-of-subject';
	req.import = true;
	req.subjectId = $('#importSubjectQuestionsModal .subjectSelect').val();
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 1)
			fillImportSubjectExams(res);
	});
}

function fillImportSubjectExams(data) {
	$('#importSubjectQuestionsModal .assignmentSelect').find('option').remove();
	var html = '<option value="0">All Exams</option>';
	for(var i=0; i<data.exams.length; i++) {
		html += '<option value="' + data.exams[i].id + '">' + data.exams[i].name + '</option>';
	}
	$('#importSubjectQuestionsModal .assignmentSelect').append(html);
	$('#importSubjectQuestionsModal .assignmentSelect').attr('disabled', false);
	$('#filterDifficulty').attr('disabled', false);
}

function fetchSubjectQuestions() {
	var req = {};
	var res;
	req.action = 'get-subject-questions-category';
	req.subjectId = $('#importSubjectQuestionsModal .subjectSelect').val();
	req.examId = $('#importSubjectQuestionsModal .assignmentSelect').val();
	req.difficulty = $('#filterDifficulty').val();
	req.categoryId = $('#modalImportSubjectQuestions').attr('data-category');
	req.tags = msfilter.getValue();
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 0) {
			toastr.error(res.message);
		} else {
			fillSubjectQuestions(res);
		}
	});
}

function fillSubjectQuestions(data) {
	//console.log(data);
	$('#listSubjectQuestions tbody').html('');
	var html = '';
	if(data.questions.length == 0)
		html += '<tr><td></td><td></td><td colspan="3">No Questions Found</td><td></td></tr>';
	for(var i=0; i<data.questions.length; i++) {
		html += '<tr data-id="' + data.questions[i].id + '" data-questionType="' + data.questions[i].questionType + '">'
					+ '<td><input type="checkbox" class="checkbox-inline selectbox"></td>'
					+ '<td>';
		if(data.questions[i].status == 0)
			html += '<i class="fa fa-minus-circle text-danger"></i></td>';
		else
			html += '<i class="fa fa-check-circle text-success"></i></td>';
		var shortQuestion = $(data.questions[i].question).text();
		if(shortQuestion.length > 35)
			shortQuestion = shortQuestion.substring(0, 33) + '...';
		html += '<td colspan="3">' + shortQuestion + '</td>';
		var type= '';
		if(data.questions[i].questionType == 0)
			type = 'MCQ';
		else if(data.questions[i].questionType == 1)
			type = 'T/F';
		else if(data.questions[i].questionType == 2)
			type = 'FTB';
		else if(data.questions[i].questionType == 3)
			type = 'MTF';
		else if(data.questions[i].questionType == 4)
			type = 'CMP';
		else if(data.questions[i].questionType == 5)
			type = 'DPN';
		else if(data.questions[i].questionType == 6)
			type = 'MAQ';
		else if(data.questions[i].questionType == 7)
			type = 'AUD';
		html += '<td>' + type + '</td>'
				+ '</tr>';
	}
	$('#listSubjectQuestions table tbody').append(html);
	$('#listSubjectQuestions table').removeClass('hide');
}

function fetchImportSubjects() {
	var req = {};
	var res;
	req.action = 'get-subjects-for-exam';
	req.import = true;
	req.courseId = $('#importExamModal .courseSelect').val();
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 1)
			fillImportSubjectSelect(res);
	});
}

function fillImportSubjectSelect(data) {
	$('#importExamModal .subjectSelect').find('option').remove();
	var html = '<option value="0">Select '+terminologies["subject_single"]+'</option>';
	for(var i=0; i<data.subjects.length; i++) {
		html += '<option value="' + data.subjects[i].id + '">' + data.subjects[i].name + '</option>';
	}
	$('#importExamModal .subjectSelect').append(html);
	$('#importExamModal .subjectSelect').attr('disabled', false);
}

function fetchImportChapters() {
	var req = {};
	var res;
	req.action = 'get-chapters-for-exam';
	req.import = true;
	req.subjectId = $('.subjectSelect').val();
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 1)
			fillImportChapterSelect(res);
	});
}

function fillImportChapterSelect(data) {
	$('#importExamModal .chapterSelect').find('option').remove();
	var html = '<option value="-1">Select Chapter</option>';
	for(var i=0; i<data.chapters.length; i++) {
		html += '<option value="' + data.chapters[i].id + '">' + data.chapters[i].name + '</option>';
	}
	html += '<option value="0">Independent</option>';
	$('#importExamModal .chapterSelect').append(html);
	$('#importExamModal .chapterSelect').attr('disabled', false);
}

function fetchAssignments() {
	var req = {};
	var res;
	req.action = 'get-assignments-for-exam';
	req.import = true;
	req.chapterId = $('#importExamModal .chapterSelect').val();
	req.subjectId = $('#importExamModal .subjectSelect').val();
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 1)
			fillAssignmentSelect(res);
	});
}

function fillAssignmentSelect(data) {
	$('#importExamModal .assignmentSelect').find('option').remove();
	var html = '<option value="0">Select Assignment</option>';
	for(var i=0; i<data.exams.length; i++) {
		html += '<option value="' + data.exams[i].id + '">' + data.exams[i].name + '</option>';
	}
	$('#importExamModal .assignmentSelect').append(html);
	$('#importExamModal .assignmentSelect').attr('disabled', false);
	$('#importExamModal .importCategorySelect').attr('disabled', false);
}

function fetchQuestions() {
	var req = {};
	var res;
	req.action = 'get-questions-for-import';
	req.import = true;
	req.examId = $('#importExamModal .assignmentSelect').val();
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 1)
			fillQuestions(res);
	});
}

function fillQuestions(data) {
	var html = '';
	$('#importExamModal table tbody').find('tr').remove();
	if(data.questions.length == 0)
		html += '<tr><td></td><td></td><td colspan="3">No Questions Found</td><td></td></tr>';
	for(var i=0; i<data.questions.length; i++) {
		html += '<tr data-id="' + data.questions[i].id + '" data-questionType="' + data.questions[i].questionType + '">'
					+ '<td><input type="checkbox" class="checkbox-inline selectbox"></td>'
					+ '<td>';
		if(data.questions[i].status == 0)
			html += '<i class="fa fa-minus-circle text-danger"></i></td>';
		else
			html += '<i class="fa fa-check-circle text-success"></i></td>';
		var shortQuestion = $(data.questions[i].question).text();
		if(shortQuestion.length > 35)
			shortQuestion = shortQuestion.substring(0, 33) + '...';
		html += '<td colspan="3">' + shortQuestion + '</td>';
		var type= '';
		if(data.questions[i].questionType == 0)
			type = 'MCQ';
		else if(data.questions[i].questionType == 1)
			type = 'T/F';
		else if(data.questions[i].questionType == 2)
			type = 'FTB';
		else if(data.questions[i].questionType == 3)
			type = 'MTF';
		else if(data.questions[i].questionType == 4)
			type = 'CMP';
		else if(data.questions[i].questionType == 5)
			type = 'DPN';
		else if(data.questions[i].questionType == 6)
			type = 'MAQ';
		else if(data.questions[i].questionType == 7)
			type = 'AUD';
		html += '<td>' + type + '</td>'
				+ '</tr>';
	}
	$('#importExamModal table tbody').append(html);
	if($('#importExamModal .importCategorySelect').val() != 0)
		refineQuestions();
}

function refineQuestions(data) {
	var selected = $('#importExamModal .importCategorySelect').val();
	var selectedType = $('#importExamModal .importCategorySelect').find('option[value="' + selected + '"]').attr('data-questionType');
	$('#importExamModal table tbody tr').each(function() {
		if($(this).attr('data-questionType') != selectedType) {
			$(this).find('input[type=checkbox]').prop('checked', false);
			$(this).hide();
		}
		else
			$(this).show();
	});
}

function addOptions(amount) {
	var html = '';
	var newIdNumber = $('#options input[type="text"]:eq(-1)').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	for(var i=0; i<amount; i++) {
		html = '<div class="col-md-6 temp">'+
                    '<div class="form-group">'+
                        '<div class="input-group">'+
                            '<span class="input-group-addon">'+
                                '<input type="radio" name="options">'+
                                '<span></span>'+
                            '</span>'+
                            '<input type="text" class="form-control option" placeholder="Answer Option" id="option' + newIdNumber + '">'+
                            '<span class="input-group-btn">'+
                                '<button class="btn red delete-option" type="button"><i class="fa fa-trash"></i></button>'+
                            '</span>'+
                        '</div>'+
                    '</div>'+
                '</div>';
		$('#options .add-div').append(html);
		newIdNumber++;
	}
	$('.delete-option').on('click', function(e) {
		$(this).parents('.temp').remove();
		if($('#options input[type="text"]').length >=6)
			$('#options .add-option').attr('disabled', true);
		else
			$('#options .add-option').attr('disabled', false);
		return false;
	});
}

function addOptionsMul(amount) {
	var html = '';
	var newIdNumber = $('#optionsMul input[type="text"]:eq(-1)').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	for(var i=0; i<amount; i++) {
		html = '<div class="col-md-6 temp">'+
                    '<div class="form-group">'+
                        '<div class="input-group">'+
                            '<span class="input-group-addon">'+
                                '<input type="checkbox">'+
                                '<span></span>'+
                            '</span>'+
                            '<input type="text" class="form-control optionMul" placeholder="Answer Option" id="optionMul' + newIdNumber + '">'+
                            '<span class="input-group-btn">'+
                                '<button class="btn red delete-option" type="button"><i class="fa fa-trash"></i></button>'+
                            '</span>'+
                        '</div>'+
                    '</div>'+
                '</div>';
		$('#optionsMul .add-div').append(html);
		newIdNumber++;
	}
	$('.delete-option').on('click', function(e) {
		$(this).parents('.temp').remove();
		if($('#optionsMul input[type="text"]').length >=6)
			$('#optionsMul .add-option').attr('disabled', true);
		else
			$('#optionsMul .add-option').attr('disabled', false);
		return false;
	});
}

function addAudioOptions(amount) {
	var html = '';
	var newIdNumber = $('#audio input[type="text"]:eq(-1)').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	for(var i=0; i<amount; i++) {
		html = '<div class="col-md-6 temp">'+
                    '<div class="form-group">'+
                        '<div class="input-group">'+
                            '<span class="input-group-addon">'+
                                '<input type="radio">'+
                                '<span></span>'+
                            '</span>'+
                            '<input type="text" class="form-control optionAud" placeholder="Answer Option" id="optionAud' + newIdNumber + '">'+
                            '<span class="input-group-btn">'+
                                '<button class="btn red delete-option" type="button"><i class="fa fa-trash"></i></button>'+
                            '</span>'+
                        '</div>'+
                    '</div>'+
                '</div>';
		$('#audio .add-div').append(html);
		newIdNumber++;
	}
	$('.delete-option').on('click', function(e) {
		$(this).parents('.temp').remove();
		if($('#audio input[type="text"]').length >=6)
			$('#audio .add-option').attr('disabled', true);
		else
			$('#audio .add-option').attr('disabled', false);
	});
}

function addChildOptions(amount, which, number) {
	var html = '';
	var flag;
	var newIdNumber = which.parents('.options').find('input[type="text"]:eq(-1)').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	for(var i=0; i<amount; i++) {
		html = '<div class="col-md-6 temp">'+
                    '<div class="form-group">'+
                        '<div class="input-group">'+
                            '<span class="input-group-addon">'+
                                '<input type="radio" name="childOptions' + number + '">'+
                                '<span></span>'+
                            '</span>'+
                            '<input type="text" class="form-control option" placeholder="Answer Option" id="childOption' + number + '' + newIdNumber + '">'+
                            '<span class="input-group-btn">'+
                                '<button class="btn red delete-option" type="button"><i class="fa fa-trash"></i></button>'+
                            '</span>'+
                        '</div>'+
                    '</div>'+
                '</div>';
		$(which).append(html);
		newIdNumber++;
	}
	$(which).closest('.options').find('.delete-option').on('click', function(e) {
		if($(this).parents('.options input[type="text"]').length-1 >= 6)
			$(this).parents('.options').find('.add-option').attr('disabled', true);
		else
			$(this).parents('.options').find('.add-option').attr('disabled', false);
		$(this).parents('.temp').remove();
		return false;
	});
}

function addChildOptionsMul(amount, which) {
	var html = '';
	var flag;
	var number = which.parents('.optionsMul').find('input[type="text"]:eq(-1)').attr('id');
	number = parseInt(number.substring(number.length-2, number.length-1)) + 1;
	var newIdNumber = which.parents('.optionsMul').find('input[type="text"]:eq(-1)').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	for(var i=0; i<amount; i++) {
		html = '<div class="col-md-6 temp">'+
                    '<div class="form-group">'+
                        '<div class="input-group">'+
                            '<span class="input-group-addon">'+
                                '<input type="checkbox">'+
                                '<span></span>'+
                            '</span>'+
                            '<input type="text" class="form-control optionMul" placeholder="Answer Option" id="childOptionMul' + number + '' + newIdNumber + '">'+
                            '<span class="input-group-btn">'+
                                '<button class="btn red delete-option" type="button"><i class="fa fa-trash"></i></button>'+
                            '</span>'+
                        '</div>'+
                    '</div>'+
                '</div>';
		$(which).append(html);
		newIdNumber++;
	}
	$(which).closest('.optionsMul').find('.delete-option').on('click', function(e) {
		if($(this).parents('.optionsMul input[type="text"]').length-1 >= 6)
			$(this).parents('.optionsMul').find('.add-option').attr('disabled', true);
		else
			$(this).parents('.optionsMul').find('.add-option').attr('disabled', false);
		$(this).parents('.temp').remove();
		return false;
	});
}

function addChildQuestion(amount, number) {
	var html = '';
	var flag;
	var newIdNumber = $('#compro .sectionNewQuestion:eq(-1)').find('.question').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	if(number == 0)
		flag = true;
	else
		flag = false;
	for(var i=0; i<amount; i++) {
		if(flag)
			number = i + 1;

		html = '<div class="well well-sm sectionNewQuestion generated">'+
                    '<input type="hidden" class="questionId" value="0">'+
                    '<div class="form-body">'+
                        '<input type="hidden" id="hiddenQuestionType" value="0">'+
                        '<div class="form-group">'+
                            '<label class="control-label">Category :</label>'+
                            '<div class="row">'+
                                '<div class="col-md-6">'+
                                    '<select class="form-control questionTypeSelect">'+
                                        '<option value="0">Multiple Choice Question</option>'+
                                        '<option value="1">True or False</option>'+
                                        '<option value="2">Fill in the blanks</option>'+
                                        '<option value="6">Multiple Answer Questions</option>'+
                                    '</select>'+
                                '</div>'+
                                '<div class="col-md-6">'+
                                    '<div class="marks text-info"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="form-group">'+
                            '<label class="control-label">Question :</label>'+
                            '<textarea placeholder="Please enter the question text here. The passage is already written above." rows="2" class="form-control ckeditor question" id="childQuestion' + newIdNumber + '"></textarea>'+
                        '</div>'+
                        '<div class="form-group options">'+
                            '<label class="control-label">Only one answer option is correct :</label>'+
                            '<div class="row">'+
                                '<div class="col-md-10">'+
                                    '<div class="row">'+
                                        '<div class="col-md-6">'+
                                            '<div class="form-group">'+
                                                '<div class="input-group">'+
                                                    '<span class="input-group-addon">'+
                                                        '<input type="radio" name="childOptions' + (number) + '">'+
                                                        '<span></span>'+
                                                    '</span>'+
                                                    '<input type="text" class="form-control option" id="childOption'+number+'0" placeholder="Answer Option">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6">'+
                                            '<div class="form-group">'+
                                                '<div class="input-group">'+
                                                    '<span class="input-group-addon">'+
                                                        '<input type="radio" name="childOptions' + (number) + '">'+
                                                        '<span></span>'+
                                                    '</span>'+
                                                    '<input type="text" class="form-control option" id="childOption'+number+'1" placeholder="Answer Option">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="row add-div"></div>'+
                                '</div>'+
                                '<div class="col-md-2">'+
                                    '<button class="btn btn-info add-option"><i class="fa fa-plus"></i></button>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="form-group trueFalse">'+
                            '<label class="control-label">Only one answer option is correct :</label>'+
                            '<div class="mt-radio-inline">'+
                                '<label class="mt-radio">'+
                                    '<input type="radio" class="true" name="trueFalse'+number+'"> True'+
                                    '<span></span>'+
                                '</label>'+
                                '<label class="mt-radio">'+
                                    '<input type="radio" class="false" name="trueFalse'+number+'"> False'+
                                    '<span></span>'+
                                '</label>'+
                            '</div>'+
                        '</div>'+
                        '<div class="form-group ftbs">'+
                            '<label class="control-label">Please write the correct answer :</label>'+
                            '<div class="row">'+
                                '<div class="col-md-6">'+
                                    '<input type="text" placeholder="Please write the answer" class="form-control ftb" id="childftb'+number+'0">'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="form-group optionsMul">'+
                            '<label class="control-label">More than one answer option can be correct :</label>'+
                            '<div class="row">'+
                                '<div class="col-md-10">'+
                                    '<div class="row">'+
                                        '<div class="col-md-6">'+
                                            '<div class="form-group">'+
                                                '<div class="input-group">'+
                                                    '<span class="input-group-addon">'+
                                                        '<input type="checkbox">'+
                                                        '<span></span>'+
                                                    '</span>'+
                                                    '<input type="text" class="form-control optionMul" id="childOptionMul'+number+'0" placeholder="Answer Option">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6">'+
                                            '<div class="form-group">'+
                                                '<div class="input-group">'+
                                                    '<span class="input-group-addon">'+
                                                        '<input type="checkbox">'+
                                                        '<span></span>'+
                                                    '</span>'+
                                                    '<input type="text" class="form-control optionMul" id="childOptionMul'+number+'1" placeholder="Answer Option">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="row add-div"></div>'+
                                '</div>'+
                                '<div class="col-md-2">'+
                                    '<button class="btn btn-info add-option"><i class="fa fa-plus"></i></button>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="form-group">'+
                            '<label class="control-label">Explanation :</label>'+
                            '<textarea class="form-control ckeditor desc" rows="2" placeholder="Please enter the explanation of the question" id="childDesc' + newIdNumber + '"></textarea>'+
                        '</div>'+
                    '</div>'+
                '</div>';
		$(html).insertBefore('#compro .lastCol');
		addCKEditor('childQuestion' + newIdNumber);
		addCKEditor('childDesc' + newIdNumber);
		newIdNumber++;
	}
	$('#compro .sectionNewQuestion:eq(-1)').find('.ftbs, .trueFalse, .optionsMul').addClass('hide');
	$('#compro .questionTypeSelect').on('change', function() {
		elem = $(this);
		if($(this).val() == 0) {
			elem.parents('.sectionNewQuestion').find('.ftbs').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.trueFalse').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.optionsMul').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.options').removeClass('hide');
		}
		else if($(this).val() == 1) {
			elem.parents('.sectionNewQuestion').find('.ftbs').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.options').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.optionsMul').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.trueFalse').removeClass('hide');
		}
		else if($(this).val() == 2) {
			elem.parents('.sectionNewQuestion').find('.trueFalse').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.options').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.optionsMul').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.ftbs').removeClass('hide');
		}
		else if($(this).val() == 6) {
			elem.parents('.sectionNewQuestion').find('.trueFalse').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.options').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.ftbs').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.optionsMul').removeClass('hide');
		}
	});
	$('#compro .question').off('blur');
	$('#compro .question').on('blur', function() {
		if($(this).parents('.sectionNewQuestion').find('.questionTypeSelect').val() == 2)
			addChildBlanks(occurrences($(this).parents('.sectionNewQuestion').find('.question').val(), '____'), $(this));
	});
	$('#compro .add-option').off('click');
	$('#compro .options .add-option').on('click', function() {
		which = $(this).parents('.sectionNewQuestion').find('.options .add-div');
		var temp = $(this).parents('.options').find('input[type="radio"]:eq(0)').attr('name');
		var number = temp.substring(temp.length-1);
		addChildOptions(1, which, number);
		if($(this).parents('.sectionNewQuestion .options').find('input[type="text"]').length >= 6)
			$(this).parents('.sectionNewQuestion').find('.options .add-option').attr('disabled', true);
		else
			$(this).parents('.sectionNewQuestion').find('.options .add-option').attr('disabled', false);
		return false;
	});
	$('#compro .optionsMul .add-option').on('click', function() {
		which = $(this).parents('.sectionNewQuestion').find('.optionsMul .add-div');
		addChildOptionsMul(1, which);
		if($(this).parents('.sectionNewQuestion .optionsMul').find('input[type="text"]').length >= 6)
			$(this).parents('.sectionNewQuestion').find('.optionsMul .add-option').attr('disabled', true);
		else
			$(this).parents('.sectionNewQuestion').find('.optionsMul .add-option').attr('disabled', false);
		return false;
	});
	$('#compro .remove-question').off('click');
	$('#compro .remove-question').on('click', function() {
		$(this).parents('.sectionNewQuestion').remove();
		numbering();
		if($('#hiddenQuestionType').val() == 5)
			calculateMarks();
		return false;
	});
}

function addCKEditor(elem) {
	CKEDITOR.replace( elem );
}

function addChildBlanks(amount, where) {
	var html = '';
	where.parents('.sectionNewQuestion').find('.temp').remove();
	var number = where.parents('.sectionNewQuestion').find('.ftbs input[type="text"]:eq(-1)').attr('id');
	number = parseInt(number.substring(number.length-2, number.length-1)) + 1;
	var newIdNumber = where.parents('.sectionNewQuestion').find('.ftbs input[type="text"]:eq(-1)').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	for(var i=0; i<(amount-1); i++) {
		html = '<div class="col-lg-5 temp">'
					+ '<input type="text" type="text" placeholder="Answer" class="form-control ftb" id="childftb'+number+''+newIdNumber+'">'
				+ '</div>';
		where.parents('.sectionNewQuestion').find('.ftbs').append(html);
		newIdNumber++;
	}
}

function addOptionMatch(amount) {
	var html = '';
	var newIdNumber = $('#match input[type="text"].a:eq(-1)').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	for(var i=0; i<amount; i++) {
		html = '<div class="match row generated margin-bottom-10">'+
					'<div class="col-xs-5 temp">'+
						'<input type="text" class="form-control a" placeholder="Enter option A here" id="mtfOptionA' + newIdNumber + '">'+
					'</div>'+
					'<div class="col-xs-5 temp">'+
						'<input type="text" class="form-control b" placeholder="Correct answer for column A" id="mtfOptionB' + newIdNumber + '">'+
					'</div>'+
					'<div class="col-xs-2" temp">'+
						'<button class="btn btn-danger delete-mtf"><i class="fa fa-times-circle"></i></button>'+
					'</div>'+
				'</div>';
		$('#match .custom-match').append(html);
		newIdNumber++;
	}
	$('#match .delete-mtf').on('click', function() {
		$(this).parents('.generated:eq(0)').remove();
		if($('#match input[type="text"].a').length >= 10)
			$('#match .add-option').attr('disabled', true);
		else
			$('#match .add-option').attr('disabled', false);
		return false;
	});
}

function addBlanks(amount) {
	var html = '';
	$('#ftb').find('.temp').remove();
	var newIdNumber = $('#ftb input[type="text"]:eq(-1)').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	for(var i=0; i<(amount-1); i++) {
		html = '<div class="col-lg-5 temp">'
					+ '<input type="text" placeholder="Please write the answer" class="form-control ftb" id="ftbOption' + newIdNumber + '">'
				+ '</div>';
		$('#ftb').append(html);
		newIdNumber++;
	}
}

function occurrences(string, subString) {
	string+=""; subString+="";
	if(subString.length<=0) return string.length+1;

	var n=0, pos=0;
	var step=subString.length;

	while(true) {
		pos=string.indexOf(subString,pos);
		if(pos>=0) {
			n++;
			pos+=step;
		}
		else
			break;
	}
	return(n);
}

function numbering() {
	var val = $('#categorySelect').val();
	var questionType = $('#categorySelect').find('option[value="'+val+'"]').attr('data-questionType');
	if(questionType == 4 || questionType == 5) {
		for(var i=0; i<$('.sectionNewQuestion').length; i++) {
			if(questionType == 4)
				$('.subQuestionNumbers:eq('+i+')').text((i+1) + '. ');
			else
				$('.subQuestionNumbers:eq('+i+')').text(letters[i] + '. ');
		}
		$('.subQuestionNumbers').show();
	}
}

function checkDuplicateCategory() {
	for(var j = 0; j < $('#modalAddEditQuestionsCategory table.table tbody tr').length - 1; j++) {
		$('#catError').html('');
		$('#modalAddEditQuestionsCategory tr:eq(-1)').removeClass('has-error');
		if($('#modalAddEditQuestionsCategory table.table tbody tr:eq('+j+')').find('.questionTypeSelect').val() == $('#modalAddEditQuestionsCategory tr:eq(-1)').find('.questionTypeSelect').val()
		&& $('#modalAddEditQuestionsCategory table.table tbody tr:eq('+j+')').find('.plus').val() == $('#modalAddEditQuestionsCategory tr:eq(-1)').find('.plus').val()
		&& $('#modalAddEditQuestionsCategory table.table tbody tr:eq('+j+')').find('.minus').val() == $('#modalAddEditQuestionsCategory tr:eq(-1)').find('.minus').val()) {
			$('#modalAddEditQuestionsCategory tr:eq(-1)').addClass('has-error');
			$('#catError').html('Two categories cannot be same. Please change something.');
			break;;
		}
	}
}

function validateAmount(element) {
	element = element.parents('tr');
	if(element.hasClass('has-error'))
		return;
	element.removeClass('has-error');
	$('#catError').html('');
	if(element.find('.plus').val() != '') {
		if(element.find('.plus').val() < 1 || element.find('.plus').val() > 100) {
			element.addClass('has-error');
			$('#catError').html('Please enter a correct answer between 1 to 100.');
		}
	}
	if(element.find('.minus').val() != '') {
		if(element.find('.minus').val() < 0 || element.find('.minus').val() > 100) {
			element.addClass('has-error');
			$('#catError').html('Please enter a wrong answer between 1 to 100.');
		}
	}
	if(element.find('.number').val() != '') {
		if(element.find('.number').val() < 1 || element.find('.number').val() > 1000) {
			element.addClass('has-error');
			$('#catError').html('Please enter number of question between 1 to 1000.');
		}
	}
}

function calculateMarks() {
	$('.marks').show();
	questions = $('#compro .sectionNewQuestion').length;
	if($('#categorySelect').val() == 0) {
		inmarks = 0;
		marks = 0;
	}
	else {
		marks = correctMarks[$('#categorySelect').val()];
		inmarks = incorrectMarks[$('#categorySelect').val()];
	}
	eachQuestionC = +(marks / questions).toFixed(2);
	eachQuestionI = +(inmarks / questions).toFixed(2);
	$('.marks').text('+' + eachQuestionC + ' & -'+ eachQuestionI + ' Marks');
}

function addNewCategory(amount) {
	html = '';
	for(i=0; i<amount; i++) {
		html += '<tr class="generated" data-id="0">'+
                    '<td colspan="3">'+
                        '<select class="form-control input-sm questionTypeSelect">'+
                            '<option value="-1" selected="">Select Category</option>'+
                            '<option value="0">Multiple Choice Question</option>'+
                            '<option value="1">True or False</option>'+
                            '<option value="2">Fill in the blanks</option>'+
                            '<option value="3">Match the following</option>'+
                            '<option value="4">Comprehensive Type Question</option>'+
                            '<option value="5">Dependent Type Question</option>'+
                            '<option value="6">Multiple Answer Question</option>'+
                            '<option value="7">Audio question</option>'+
                        '</select>'+
                    '</td>'+
                    '<td>'+
                    	'<div class="input-group">'+
                            '<span class="input-group-addon">'+
                                '<i class="fa fa-plus"></i>'+
                            '</span>'+
	                        '<input type="text" class="form-control input-sm plus" placeholder="0">'+
                        '</div>'+
                    '</td>'+
                    '<td>'+
                    	'<div class="input-group">'+
                            '<span class="input-group-addon">'+
                                '<i class="fa fa-minus"></i>'+
                            '</span>'+
                        	'<input type="text" class="form-control input-sm minus" placeholder="0">'+
                        '</div>'+
                    '</td>'+
                    '<td>'+
                        '<input type="text" class="form-control input-sm number" placeholder="0">'+
                    '</td>'+
                    '<td class="text-right">'+
                        '<strong><span class="total">0</span></strong>'+
                    '</td>'+
                '</tr>';
	}
	$('#modalAddEditQuestionsCategory table.table tbody').append(html);
	
	$('#modalAddEditQuestionsCategory .number').keypress(function (e) {
		//if the letter is not digit then don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	});
	
	//for allowing decimal number
	$('#modalAddEditQuestionsCategory .plus, #modalAddEditQuestionsCategory .minus').keypress(function (event) {
		return allowDecimal(event, $(this));
	});
	
	$('.number').on('blur', function() {
		checkDuplicateCategory();
		$('#modalAddEditQuestionsCategory tr').removeClass('has-error');
		var total = $(this).val() * $(this).parents('tr').find('.plus').val();
		$(this).parents('tr').find('strong .total').text(total);
		var grandTotal = 0;
		$('#modalAddEditQuestionsCategory .total').each(function() {
			grandTotal += parseFloat($(this).text());
		});
		$('#modalAddEditQuestionsCategory #grandTotal').text(grandTotal);
		var id = $('#sectionName').attr('data-id');
		$('#sections li.mt-list-item[data-id="' + id + '"]').find('.sectionTotalMarks').text(grandTotal.toFixed(2));
		var count = 0;
		for(var i=0;i<$('.sectionTotalMarks').length; i++) {
			count += parseFloat($('.sectionTotalMarks:eq('+i+')').text());
		}
		$('#examTotalMarks').text(count);
		validateAmount($(this));
	});
	
	//for duplicate category prevention
	$('.plus, .minus').on('blur', function() {
		checkDuplicateCategory();
		validateAmount($(this));
	});
	
	$('.questionTypeSelect').on('change', function() {
		checkDuplicateCategory();
		$(this).parents('tr').find('.number').blur();
	});
}

function reformatCategoryDialog() {
	fillModalCategory(categoryModalDetail);
	//if($('#modalAddEditQuestionsCategory table.table tbody tr').length > categoryModalDetail.categories.length) {
		if($('#modalAddEditQuestionsCategory table.table tbody tr:eq(-1)').attr('data-id') == 0 && $('#modalAddEditQuestionsCategory table.table tbody tr').length > 1)
			$('#modalAddEditQuestionsCategory table.table tbody tr:eq(-1)').remove();
	//}
}
function fetchExamSectionsForExport() {
	var req = {};
	var res;
	req.action = 'get-exam-sections-for-export';
	req.examId = examId;
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		//console.log(res);
		fillExamSectionsForExport(res);
	});
}
function fillExamSectionsForExport(data) {
	var sections = data.sections;
	$('#exportExamDetail tbody').html('');
	if (sections.length>0) {
		for(var i=0; i<sections.length; i++) {
			$('#exportExamDetail tbody').append(
				'<tr data-section="'+sections[i]['id']+'" class="parent">'+
					'<td><strong>'+(i+1)+') Section : '+sections[i]['name']+'</strong></td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
					'<td></td>'+
				'</tr>'
				);
			if (sections[i].categories.length>0) {
				for(var j=0; j<data.sections[i].categories.length; j++) {
					var marks = data.sections[i]['categories'][j]['required'] * data.sections[i]['categories'][j]['correct'];
					$('#exportExamDetail tbody').append(
						'<tr data-section="'+sections[i]['id']+'" data-category="'+data.sections[i]['categories'][j]['id']+'">'+
							'<td class="padding-left-20">'+(j+1)+') '+data.sections[i]['categories'][j]['name']+'</td>'+
							'<td class="text-center">'+data.sections[i]['categories'][j]['required']+'</td>'+
							'<td class="text-center">'+marks+'</td>'+
							'<td><input type="text" class="form-control text-center input-sm js-questions-export" data-marks="'+data.sections[i]['categories'][j]['correct']+'" value="0" /></td>'+
							'<td class="text-center js-marks-export">0</td>'+
						'</tr>'
						);
				}
			}
		}
	} else {
		toastr.error('Please complete at least 1 category questions.');
	}
	$('#exportExamDetail').on('keyup', '.js-questions-export', function(){
		var questions = $(this).val();
		var correct = $(this).attr('data-marks');
		//console.log(questions+' '+correct);
		$(this).closest('tr').find('.js-marks-export').html(questions*correct);
		var totalQuestions = 0;
		$('.js-questions-export').each(function(obj) {
			totalQuestions+=parseInt($(this).val());
		});
		$('.total-export-questions').html(totalQuestions);
		var totalMarks = 0;
		$('.js-marks-export').each(function(obj) {
			totalMarks+=parseFloat($(this).html());
		});
		$('.total-export-marks').html(totalMarks);
	});
	$('#exportExamModal').modal("show");
}
function fetchExportedExam(exportQuestions) {
	//console.log(exportQuestions);
	var req = {};
	var res;
	req.action = 'export-exam';
	req.examId = examId;
	req.exportQuestions = exportQuestions;
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 0) {
			toastr.error(message);
		} else {
			fillExportedExam(res.exportData);
		}
	});
}
function fillExportedExam(data) {
	$('#exportExamDetail').addClass('hide');
	$('#exportedExamDetail').html();
	$('#exportedExamDetail').removeClass('hide');
	$('#btnExportExam').addClass('hide');
	$('#btnPrintExam').removeClass('hide');
	$('#exportedExamDetail').html(
		'<h1>'+$('.page-title').html()+'</h1>'+
		'<div class="sections"></div>');
	for(var i=0; i<data.length;i++) {
		$('#exportedExamDetail .sections').append(
			'<div class="section">'+
				'<h2>Section: '+data[i].name+'</h2>'+
				'<div class="categories"></div>'+
			'</div>');
		for(var j=0;j<data[i].categories.length;j++) {
			var marks = round2(parseFloat(data[i]['categories'][j].correct*data[i]['categories'][j]['questions'].length));
			$('#exportedExamDetail .section:last-child .categories').append(
				'<div class="category">'+
					'<h3>Category: '+data[i]['categories'][j].name+'<small class="pull-right">'+marks+' Marks</small></h3>'+
					'<hr />'+
					'<div class="questions"></div>'+
				'</div>');
			var questions = data[i]['categories'][j]['questions'];
			var divQuestions = $('#exportedExamDetail .section:last-child .categories .category:last-child .questions');
			for(var k=0;k<questions.length;k++) {
				divQuestions.append(
					'<div class="question">'+
						'<h4>Q'+(k+1)+')'+questions[k].question+'</h4>'+
						'<div class="options"></div>'+
					'</div>');
				if(data[i]['categories'][j]['questionType'] == 0 || data[i]['categories'][j]['questionType'] == 6) {
					divQuestions.find('.question:last-child .options').html('<ul class="list-unstyled"></ul>');
					for(var l=0; l<questions[k]['options'].length; l++) {
						divQuestions.find('.question:last-child .options ul').append(
							'<li>'+letters[l]+') '+questions[k]['options'][l].option+'</li>'
							);
					}
				} else if(data[i]['categories'][j]['questionType'] == 1) {
					divQuestions.find('.question:last-child .options').html(
						'<ul class="list-unstyled">'+
							'<li>A) True</li>'+
							'<li>B) False</li>'+
						'</ul>');
				} else if(data[i]['categories'][j]['questionType'] == 2) {
					divQuestions.find('.question:last-child .options').html('<ul class="list-unstyled"></ul>');
					for(var l=0; l<questions[k]['blanks'].length; l++) {
						divQuestions.find('.question:last-child .options ul').append(
							'<li>'+letters[l]+') '+questions[k]['blanks'][l].blanks+'</li>'
							);
					}
				} else if(data[i]['categories'][j]['questionType'] == 3) {
					divQuestions.find('.question:last-child .options').html('<table class="table table-bordered"><thead><tr><th></th><th>Column A</th><th>Column B</th></tr></thead><tbody></tbody></table>');
					for(var l=0; l<questions[k]['matches'].length; l++) {
						divQuestions.find('.question:last-child .options .table>tbody').append(
							'<tr><td>'+letters[l]+')</td><td>'+questions[k]['matches'][l]['columnA']+'</td><td>'+questions[k]['matches'][l]['columnB']+'</td></tr>'
							);
					}
				} else if(data[i]['categories'][j]['questionType'] == 4 || data[i]['categories'][j]['questionType'] == 5) {
					for(var l=0; l<questions[k]['childQuestions'].length; l++) {
						divQuestions.find('.question:last-child .options').append(
						'<div class="child-question">'+
							'<h5>CQ'+(l+1)+')'+questions[k]['childQuestions'][l]['question'].question+'</h5>'+
							'<div class="child-options"></div>'+
						'</div>');
						//console.log(questions[k]['childQuestions'][l]);
						if(questions[k]['childQuestions'][l]['question']['questionType'] == 0 || questions[k]['childQuestions'][l]['question']['questionType'] == 6) {
							divQuestions.find('.question:last-child .options .child-question:last-child .child-options').html('<ul class="list-unstyled"></ul>');
							for(var m=0; m<questions[k]['childQuestions'][l]['answer'].length; m++) {
								divQuestions.find('.question:last-child .options .child-question:last-child .child-options ul').append(
									'<li>'+letters[m]+') '+questions[k]['childQuestions'][l]['answer'][m].option+'</li>'
									);
							}
						} else if(questions[k]['childQuestions'][l]['question']['questionType'] == 1) {
							divQuestions.find('.question:last-child .options .child-question:last-child .child-options').html(
								'<ul class="list-unstyled">'+
									'<li>A) True</li>'+
									'<li>B) False</li>'+
								'</ul>');
						} else if(questions[k]['childQuestions'][l]['question']['questionType'] == 6) {
							/*divQuestions.find('.question:last-child .options .child-question:last-child .child-options').html('<ul class="list-unstyled"></ul>');
							for(var m=0; m<questions[k]['childQuestions'][l]['answer'].length; m++) {
								divQuestions.find('.question:last-child .options .child-question:last-child .child-options ul').append(
									'<li>'+letters[m]+') '+questions[k]['childQuestions'][l]['answer'][m].blanks+'</li>'
									);
							}*/
						}
					}
				}
			}
		}
	}
}
$(document).ready(function() {
	$(".d-checkbox").bootstrapSwitch();

	$('#btnPrintExam').click(function(){
		PrintElem('#exportedExamDetail');
	});

	$('#btnExportSettings').click(function(){
		fetchExamSectionsForExport();		
	});

	$('#exportExamModal').on('hidden.bs.modal', function () {
		$('#exportExamDetail tbody').html('').removeClass('hide');
		$('#exportedExamDetail').html('').addClass('hide');
		$('#btnPrintExam').addClass('hide');
		$('#btnExportExam').removeClass('hide');
	})

	$('#btnExportExam').click(function(){
		var exportQuestions = [];
		if ($('.js-questions-export').length>0) {
			var i = 0;
			var j = 0;
			$('tr[data-section].parent').each(function(obj) {
				exportQuestions[i] = {};
				exportQuestions[i]['sectionId'] = $(this).closest('tr').attr('data-section');
				exportQuestions[i]['category'] = {};
				j = 0;
				$('tr[data-section='+exportQuestions[i]['sectionId']+'][data-category]').each(function(obj1) {
					//console.log($(this).attr('data-category'));
					exportQuestions[i]['category'][j] = {};
					exportQuestions[i]['category'][j]['categoryId'] = $(this).attr('data-category');
					exportQuestions[i]['category'][j]['questions'] = parseInt($(this).find('.js-questions-export').val());
					j++;
				});
				i++;
			});
			fetchExportedExam(exportQuestions);
		} else {
			$('#exportExamModal').modal('hide');
		}
	});

	$('#randomQuestions').on('switchChange.bootstrapSwitch', function (e, data) {
		if(!$('#randomQuestions').prop('checked')) {
			$('#modalAddEditQuestionsCategory table.table tbody').sortable({revert : true,
					placeholder : 'sort-placeholder',
					cursor : 'move',
					forcePlaceholderSize : true,
					tolerance : 'pointer',
					opacity : 0.6,
					update : function() {
						for(var i=0; i< $('#modalAddEditQuestionsCategory table.table tbody').find('tr').length; i++) {
							sortOrder[i] = $('#modalAddEditQuestionsCategory table.table tbody tr:eq('+i+')').attr('data-id');
						}
					},
					start: function (event, ui) {
						// Build a placeholder cell that spans all the cells in the row
						var cellCount = 0;
						$('td, th', ui.helper).each(function () {
							// For each TD or TH try and get it's colspan attribute, and add that or 1 to the total
							var colspan = 1;
							var colspanAttr = $(this).attr('colspan');
							if (colspanAttr > 1) {
								colspan = colspanAttr;
							}
							cellCount += colspan;
						});
						// Add the placeholder UI - note that this is the item's content, so TD rather than TR
						ui.placeholder.html('<td colspan="' + cellCount + '">&nbsp;</td>');
						//$(this).attr('data-previndex', ui.item.index());
					}
			});
			$('.note-order-drag').removeClass('hide');
		}
		else {
			$('#modalAddEditQuestionsCategory table.table tbody').sortable('destroy');
			$('.note-order-drag').addClass('hide');
		}
	});
	
	$('#modalAddEditQuestionsCategory').find('.plus, .minus').on('blur', function() {
		$('#modalAddEditQuestionsCategory tr').removeClass('has-error');
		validateAmount($(this));
	});
	
	$('#modalAddEditQuestionsCategory .questionTypeSelect').on('change', function() {
		$('#modalAddEditQuestionsCategory tr').removeClass('has-error');
	});

	$('.number').on('blur', function() {
		$('#modalAddEditQuestionsCategory tr').removeClass('has-error');
		total = $(this).val() * $(this).parents('tr').find('.plus').val();
		$(this).parents('tr').find('strong .total').text(total);
		var grandTotal = 0;
		$('#modalAddEditQuestionsCategory .total').each(function() {
			grandTotal += parseFloat($(this).text());
		});
		$('#modalAddEditQuestionsCategory #grandTotal').text(grandTotal);
		validateAmount($(this));
	});

	$('#sectionTable').on('click', '.import-questions', function(){
		var categoryId = $(this).closest('tr').attr('data-id');
		//console.log(categoryId);
		$('#modalImportSubjectQuestions').attr('data-category', categoryId);
		$('#importSubjectQuestionsModal').modal('show');
	});

	$('#importSubjectQuestionsModal .courseSelect').on('change', function() {
		$('#importSubjectQuestionsModal .subjectSelect').attr('disabled', true);
		$('#importSubjectQuestionsModal .subjectSelect').val(0);
		$('#importSubjectQuestionsModal .assignmentSelect').attr('disabled', true);
		$('#importSubjectQuestionsModal .assignmentSelect').val(0);
		$('#filterDifficulty').attr('disabled', true);
		$('#filterDifficulty').val(0);
		$('#importSubjectQuestionsModal table').addClass('hide');
		if($(this).val() != 0)
			fetchImportSubjectsQuestions();
	});
	
	$('#importSubjectQuestionsModal .subjectSelect').on('change', function() {
		$('#importSubjectQuestionsModal .assignmentSelect').attr('disabled', true);
		$('#importSubjectQuestionsModal .assignmentSelect').val(0);
		$('#filterDifficulty').attr('disabled', true);
		$('#filterDifficulty').val(0);
		$('#importSubjectQuestionsModal table').addClass('hide');
		if($(this).val() != 0) {
			fetchImportSubjectExams();
			fetchSubjectQuestions();
		}
	});
	
	$('#importSubjectQuestionsModal .assignmentSelect').on('change', function() {
		$('#importSubjectQuestionsModal table').addClass('hide');
		fetchSubjectQuestions();
	});
	
	$('#filterDifficulty').on('change', function() {
		$('#importSubjectQuestionsModal table').addClass('hide');
		fetchSubjectQuestions();
	});

	$(msfilter).on('selectionchange', function(e,m){
		//console.log("values: " + JSON.stringify(this.getValue()));
		fetchSubjectQuestions();
	});
	
	$('#importSubjectQuestionsModal').on('hidden.bs.modal', function() {
		$('#importSubjectQuestionsModal .cancel-button').click();
	});
	
	//For cancel button modal reset
	$('#importSubjectQuestionsModal .cancel-button').on('click', function() {
		$('#importSubjectQuestionsModal .subjectSelect').attr('disabled', true);
		$('#importSubjectQuestionsModal .subjectSelect').val(0);
		$('#importSubjectQuestionsModal .assignmentSelect').attr('disabled', true);
		$('#importSubjectQuestionsModal .assignmentSelect').val(0);
		$('#importSubjectQuestionsModal .courseSelect').val(0);
		$('#filterDifficulty').attr('disabled', true);
		$('#filterDifficulty').val(0);
		$('#importSubjectQuestionsModal table').addClass('hide');
	});
	
	$('#importExamModal .courseSelect').on('change', function() {
		$('#importExamModal .subjectSelect').attr('disabled', true);
		$('#importExamModal .subjectSelect').val(0);
		$('#importExamModal .chapterSelect').attr('disabled', true);
		$('#importExamModal .chapterSelect').val(-1);
		$('#importExamModal .assignmentSelect').attr('disabled', true);
		$('#importExamModal .assignmentSelect').val(0);
		$('#importExamModal .importCategorySelect').attr('disabled', true);
		$('#importExamModal .importCategorySelect').val(0);
		$('#importExamModal table').addClass('hide');
		$('#initialView').show();
		if($(this).val() != 0)
			fetchImportSubjects();
	});
	
	$('#importExamModal .subjectSelect').on('change', function() {
		$('#importExamModal .chapterSelect').attr('disabled', true);
		$('#importExamModal .chapterSelect').val(-1);
		$('#importExamModal .assignmentSelect').attr('disabled', true);
		$('#importExamModal .assignmentSelect').val(0);
		$('#importExamModal .importCategorySelect').attr('disabled', true);
		$('#importExamModal .importCategorySelect').val(0);
		$('#importExamModal table').addClass('hide');
		$('#initialView').show();
		if($(this).val() != 0)
			fetchImportChapters();
	});
	
	$('#importExamModal .chapterSelect').on('change', function() {
		$('#importExamModal .assignmentSelect').attr('disabled', true);
		$('#importExamModal .assignmentSelect').val(0);
		$('#importExamModal .importCategorySelect').attr('disabled', true);
		$('#importExamModal .importCategorySelect').val(0);
		$('#importExamModal table').addClass('hide');
		$('#initialView').show();
		if($(this).val() != -1)
			fetchAssignments();
	});
	
	$('#importExamModal .assignmentSelect').on('change', function() {
		$('#initialView').hide();
		$('#importExamModal table').removeClass('hide');
		$('#selectAll').prop('checked', false);
		if($(this).val() != 0)
			fetchQuestions();
	});
	
	$('#importExamModal .importCategorySelect').on('change', function() {
		if($(this).val() != 0)
			refineQuestions();
	});
	
	//events to reformat category dialog
	$('#modalAddEditQuestionsCategory').on('hidden.bs.modal', function() {
		reformatCategoryDialog();
	});
	
	
	//For cancel button modal reset
	$('#importExamModal .cancel-button').on('click', function() {
		$('#importExamModal .subjectSelect').attr('disabled', true);
		$('#importExamModal .subjectSelect').val(0);
		$('#importExamModal .chapterSelect').attr('disabled', true);
		$('#importExamModal .chapterSelect').val(-1);
		$('#importExamModal .assignmentSelect').attr('disabled', true);
		$('#importExamModal .assignmentSelect').val(0);
		$('#importExamModal .importCategorySelect').attr('disabled', true);
		$('#importExamModal .importCategorySelect').val(0);
		$('#importExamModal .courseSelect').val(0);
		$('#importExamModal table').addClass('hide');
		$('#initialView').show();
	});
	$('#importExamModal').on('hidden.bs.modal', function() {
		$('#importExamModal .cancel-button').click();
	});
	
	//to import selected questions
	$('#modalImportQuestion').on('click', function() {
		if($('#importExamModal .importCategorySelect').val() != 0) {
			var flag = false;
			$('input[type="checkbox"]').each(function() {
				if($(this).prop('checked')) {
					flag = true;
					return;
				}
			});
			if(flag) {
				$(this).attr('disabled', true);
				var req = {};
				var res;
				req.action = 'import-questions';
				req.questions = [];
				$('#importExamModal table tr').each(function() {
					var obj = {};
					if($(this).find('input[type="checkbox"]:not(#selectAll)').prop('checked')) {
						obj.questionId = $(this).attr('data-id');
						obj.questionType = $(this).attr('data-questionType');
						obj.categoryId = $('#importExamModal .importCategorySelect').val();
						obj.parentId = 0;
						obj.sectionId = $('#sectionName').attr('data-id');
						obj.examId = examId;
						obj.sameExam = ($('#importExamModal .assignmentSelect').val() == examId)?true:false;
						req.questions.push(obj);
					}
				});
				$.ajax({
					'type'  : 'post',
					'url'   : ApiEndPoint,
					'data' 	: JSON.stringify(req)
				}).done(function (res) {
					$('#modalImportQuestion').attr('disabled', false);
					res =  $.parseJSON(res);
					$("#importExamModal").modal('hide');
					if(res.status == 1)
						fetchSectionDetails($('#sectionName').attr('data-id'));
					else
						toastr.error(res.message);
				});
			}
			else
				toastr.error('Please select at least one question.');
		}
		else
			toastr.error("Please select a import category first.");
	});

	//to import selected questions from question bank
	$('#modalImportSubjectQuestions').on('click', function() {
		if($('#importSubjectQuestionsModal .importCategorySelect').val() != 0) {
			var flag = false;
			$('input[type="checkbox"]').each(function() {
				if($(this).prop('checked')) {
					flag = true;
					return;
				}
			});
			if(flag) {
				$(this).attr('disabled', true);
				var req = {};
				var res;
				req.action = 'import-subject-questions';
				req.questions = [];
				$('#importSubjectQuestionsModal table tbody tr').each(function() {
					var obj = {};
					if($(this).find('input[type="checkbox"]:not(#selectAll)').prop('checked')) {
						obj.questionId = $(this).attr('data-id');
						obj.questionType = $(this).attr('data-questionType');
						obj.categoryId = $('#modalImportSubjectQuestions').attr('data-category');
						obj.parentId = 0;
						obj.examId = examId;
						req.questions.push(obj);
					}
				});
				$.ajax({
					'type'  : 'post',
					'url'   : ApiEndPoint,
					'data' 	: JSON.stringify(req)
				}).done(function (res) {
					$('#modalImportSubjectQuestions').attr('disabled', false);
					res =  $.parseJSON(res);
					$("#importSubjectQuestionsModal").modal('hide');
					if(res.status == 1)
						fetchSectionDetails($('#sectionName').attr('data-id'));
					else
						toastr.error(res.message);
				});
			}
			else
				toastr.error('Please select at least one question.');
		}
		else
			toastr.error("Please select a import category first.");
	});
	
	$('#examStatus').on('click', function() {
		if($(this).text() == 'Go Live') {
			var req = {};
			req.action = 'create-instructions';
			req.examId = examId;
			$.ajax({
				'type'	: 'post',
				'url'	: ApiEndPoint,
				'data'	: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					if (res.dateCrossed == 1) {
						bootbox.dialog({
						    message: "This exam has expired. Please extend date in settings page or proceed to ignore.",
						    title: "Exam expired",
						    buttons: {
						      danger: {
						        label: "Proceed anyway",
						        className: "green",
						        callback: function() {
									$('#examInstructionsModal').modal('show');
									$('#examInstructionsModal .ins-container').html(res.instruction);
									$('#examInstructionsModal .edit-ins-container textarea').val('');
						        }
						      },
						      success: {
						        label: "Goto Settings",
						        className: "red",
						        callback: function() {
						          window.location = sitepathManageExams+examId+'/settings';
						        }
						      }
						    }
						});
					} else {
						$('#examInstructionsModal').modal('show');
						$('#examInstructionsModal .ins-container').html(res.instruction);
						$('#examInstructionsModal .edit-ins-container textarea').val('');
					}
				}
			});
		}
	});
	
	//event handler for edit button
	$('#editInstruction').on('click', function() {
		$('#examInstructionsModal .ins-container').hide();
		$('#examInstructionsModal .edit-ins-container').show();
	});
	
	//event handler for exam instructions ok
	$('#makeExamLive').on('click', function() {
		var req = {};
		req.action = 'make-live';
		var customIns = $('#examInstructionsModal .edit-ins-container textarea').val();
		customIns = '<br><br>' + customIns.replace(new RegExp('\r?\n','g'), '<br>');
		req.instructions = $('#examInstructionsModal .ins-container').html() + customIns;
		req.examId = examId;
		$.ajax({
			'type'	: 'post',
			'url'	: ApiEndPoint,
			'data'	: JSON.stringify(req)
		}).done(function(res) {
			$('#examInstructionsModal').modal('hide');
			res = $.parseJSON(res);
			if(res.status == 0)
				toastr.error(res.message);
			else {
				$('#examStatus').text('Live').addClass('hide');
				$('#examStop').remove();
				var html = '<button class="btn btn-fit-height red" type="button" id="examStop">Go Offline</button>';
				html+= '<button class="btn btn-fit-height red" type="button" id="examEnd">End Exam</button>';
				$('#examStatus').closest('.btn-group').append(html);
				$('#examStop').on('click', function() {
					var req = {};
					req.action = 'make-unlive';
					req.examId = examId;
					$.ajax({
						'type'	: 'post',
						'url'	: ApiEndPoint,
						'data'	: JSON.stringify(req)
					}).done(function(res) {
						res = $.parseJSON(res);
						if(res.status == 0)
							toastr.error(res.message);
						else {
							$('#examEnd').remove();
							$('#examStop').remove();
							$('#examStatus').text('Go Live').removeClass('hide');
						}
					});
				});
				$('#examEnd').on('click', function() {
					var req = {};
					req.action = 'make-exam-end';
					req.examId = examId;
					$.ajax({
						'type'	: 'post',
						'url'	: ApiEndPoint,
						'data'	: JSON.stringify(req)
					}).done(function(res) {
						res = $.parseJSON(res);
						if(res.status == 0)
							toastr.error(res.message);
						else {
							$('#examEnd').attr("disabled","disabled");
						}
					});
				});
			}
		});
	});
	
	$('#sectionDetail .delete-section').on('click', function() {
		var sectionsCount=$('#sections li.mt-list-item').length;
		if(sectionsCount<=1) {
			toastr.error('You can\'t delete the last section in your Exam/Assignment.');
			return;
		}
		if(parseInt($('#examStatus').attr('data-assoc')) > 0) {
			var category = $(this).parents('tr').attr('data-category');
			if(!($('#sectionTable tbody').find('tr[data-id="'+category+'"] .required').text() < $('#sectionTable tbody').find('tr[data-id="'+category+'"] .entered').text())) {
				toastr.error('You can\'t cross the minimum required question limit as there are '+terminologies["student_plural"].toLowerCase()+' associated with this exam. Please remove them in order to delete any other question.')
				return;
			}
		}
		if($('#examStop').length == 0)
			var con = confirm("Are you sure you want to delete this section.");
		else
			var con = confirm("Are you sure you want to delete this section. It will change your Assignment/Exam state to Draft/Not Live.");
		if(con) {
			var req = {};
			req.action = 'delete-section';
			req.sectionId = $('#sectionName').attr('data-id');
			req.examId = examId;
			$.ajax({
				'type'	: 'post',
				'url'	: ApiEndPoint,
				'data'	: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					$('#examStop').click();
					fetchExamSections();
				}
			});
		}
	});
	
	$('#categorySelect').on('change', function() {
		changeCategory($(this).val());
	});

	$('#compro .question').on('blur', function() {
		if($(this).parents('.sectionNewQuestion').find('.questionTypeSelect').val() == 2)
			addChildBlanks(occurrences($(this).parents('.sectionNewQuestion').find('.question').val(), '____'), $(this));
	});
	
	$('#options .add-option').on('click', function() {
		addOptions(1);
		if($('#options input[type="text"]').length >= 6)
			$('#options .add-option').attr('disabled', true);
		else
			$('#options .add-option').attr('disabled', false);
		return false;
	});
	
	$('#match .add-option').on('click', function() {
		addOptionMatch(1);
		if($('#match input[type="text"].a').length >= 10)
			$('#match .add-option').attr('disabled', true);
		else
			$('#match .add-option').attr('disabled', false);
		return false;
	});
	
	$('#optionsMul .add-option').on('click', function() {
		addOptionsMul(1);
		if($('#optionsMul input[type="text"]').length >= 6)
			$('#optionsMul .add-option').attr('disabled', true);
		else
			$('#optionsMul .add-option').attr('disabled', false);
		return false;
	});

	$('#audio .add-option').on('click', function() {
		addAudioOptions(1);
		if($('#audio input[type="text"]').length >= 6)
			$('#audio .add-option').attr('disabled', true);
		else
			$('#audio .add-option').attr('disabled', false);
		return false;
	});
	
	$('#compro .options .add-option').on('click', function() {
		which = $(this).parents('.sectionNewQuestion').find('.options .add-div');
		addChildOptions(1, which, 0);
		if($(this).parents('.sectionNewQuestion .options').find('input[type="text"]').length >= 6)
			$(this).parents('.sectionNewQuestion').find('.options .add-option').attr('disabled', true);
		else
			$(this).parents('.sectionNewQuestion').find('.options .add-option').attr('disabled', false);
		return false;
	});
	
	$('#compro .optionsMul .add-option').on('click', function() {
		which = $(this).parents('.sectionNewQuestion').find('.optionsMul .add-div');
		addChildOptionsMul(1, which);
		if($(this).parents('.sectionNewQuestion .optionsMul').find('input[type="text"]').length >= 6)
			$(this).parents('.sectionNewQuestion').find('.optionsMul .add-option').attr('disabled', true);
		else
			$(this).parents('.sectionNewQuestion').find('.optionsMul .add-option').attr('disabled', false);
		return false;
	});
	
	$('#compro .add-question').on('click', function() {
		//taking the previous question number
		var temp = $(this).parents('#compro').find('.sectionNewQuestion:eq(-1) .subQuestionNumbers').text();
		var number = temp.substring(0, 1);
		//converting letter into number for DPN questions
		if($('#hiddenQuestionType').val() == 5)
			number = letters.indexOf(number) + 1;
		number = parseInt(number);
		addChildQuestion(1, number);
		numbering();
		if($('#hiddenQuestionType').val() == 5)
			calculateMarks();
		return false;
	});
	
	$('#sectionNewQuestion .cancel-button').on('click', function() {
		/*setTimeout(function() {
			if(CKEDITOR.instances['question'])
				CKEDITOR.instances['question'].destroy(true);
			$('#question').attr('placeholder', 'Please enter the question text here');
			ckeditorOn('question');
		}, 1500);*/
		$('div[aria-describedby="cke_43"]').html('Please enter the question text here');
		$('.add-option').attr('disabled', false);
		$('.temp').remove();
		$('.help-block').remove();
		$('#sectionNewQuestion').find('div.text-danger').remove();

		if ($('.sectionNewQuestion.generated .question').length>0) {
			$('.sectionNewQuestion.generated .question').each(function(){
				var childQuestionId = $(this).attr('id');
				CKEDITOR.instances[childQuestionId].destroy();
			});
		}
		if ($('.sectionNewQuestion.generated .desc').length>0) {
			$('.sectionNewQuestion.generated .desc').each(function(){
				var childDescId = $(this).attr('id');
				CKEDITOR.instances[childDescId].destroy();
			});
		}

		$('#sectionNewQuestion .generated').remove();
		$('#trueFalse').addClass('hide');
		if($('#questionId').val() == 0)
			tempQuestion = $('#categorySelect').val();
		$('#questionId').val(0);
		$('#ftb').addClass('hide');
		$('#question').val('');
		$('#selectDifficulty').val(0);
		ms.clear();
		$('#match').addClass('hide');
		$('#compro').addClass('hide');
		$('#optionsMul').addClass('hide');
		$('#audio').addClass('hide');
		$('#compro .ftbs').addClass('hide');
		$('#compro .trueFalse').addClass('hide');
		$('#compro .questionTypeSelect').val(0);
		$('#hiddenQuestionType').val(0);
		$('#compro .question').val('');
		////$('#desc').val('');
		$('#categorySelect').find('option').show();
		$('#compro .desc').val('');
		$('#categorySelect').val(0);
		flagCategory = true;
		$('.true, .false, #true, #false').prop('checked', false);
		$('#compro .options').show();
		$('#options').removeClass('hide');
		$('#sectionNewQuestion').find('input[type="text"]').val('');
		$('#sectionNewQuestion').find('input[type="radio"]').prop('checked', false);
		$('#sectionNewQuestion').find('input[type="checkbox"]').prop('checked', false);
		//removing text from ckeditor textboxes
		for(instance in CKEDITOR.instances) {
			CKEDITOR.instances[instance].setData('');
			CKEDITOR.instances[instance].fire('blur');
		}
	});
	
	$('#newQuestion').on('click', function() {
		$('.mt-list-item.done.active').removeClass('active');
		$('#sectionNewQuestion').removeClass('hide');
		$('#categorySelect option').removeClass('hide');
		$('#sectionNewQuestion .cancel-button').click();
		$('#questionId').val(0);
		$('.js-file-path').html('').attr('data-path', '');
	});

	$('body').on('click', '.new-question', function() {
		$('#newQuestion').trigger('click');
	});
		
	$('#compro .questionTypeSelect').on('change', function() {
		elem = $(this);
		if($(this).val() == 0) {
			elem.parents('.sectionNewQuestion').find('.ftbs').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.trueFalse').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.optionsMul').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.options').removeClass('hide');
		}
		else if($(this).val() == 1) {
			elem.parents('.sectionNewQuestion').find('.ftbs').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.options').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.optionsMul').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.trueFalse').removeClass('hide');
		}
		else if($(this).val() == 2) {
			elem.parents('.sectionNewQuestion').find('.trueFalse').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.options').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.optionsMul').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.ftbs').removeClass('hide');
		}
		else if($(this).val() == 6) {
			elem.parents('.sectionNewQuestion').find('.trueFalse').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.options').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.ftbs').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.optionsMul').removeClass('hide');
		}
	});
	
	$('#question').on('blur', function() {
		if($('#hiddenQuestionType').val() == 2)
			addBlanks(occurrences($('#question').val(), '____'));
	});
	
	$('#sectionNewQuestion .save-button').on('click', function() {
		if(!flagCategory) {
			var con = confirm("You have changed the category. Do you want to save like this.");
			if(!con)
				return;
		}
		if($('#categorySelect').val() != 0) {
			if($(CKEDITOR.instances['question'].getData()).text().length > 3) {
				var req = {};
				if($('#questionId').val() == 0)
					req.action = 'save-question-exam';
				else {
					req.action = 'edit-question-exam';
					req.questionId = $('#questionId').val();
				}
				req.parentId = 0;
				req.desc = CKEDITOR.instances['desc'].getData();
				req.categoryId = $('#categorySelect').val();
				req.sectionId = $('#sectionName').attr('data-id');
				req.examId = examId;
				req.subjectId = subjectId;
				//req.question = $('#question').val();
				req.question = CKEDITOR.instances['question'].getData();
				req.difficulty = $('#selectDifficulty').val();
				req.questionType = $('#hiddenQuestionType').val();
				req.tags = ms.getValue();
				req.categorySwitch = false;
				if(!flagCategory) {
					req.categorySwitch = true;
					req.oldCategory = oldCategory;
				}
				if(req.questionType == 0) {
					req.options = [];
					$('#options input[type="text"].option').each(function() {
						if($(this).val() != '') {
							var obj = {};
							obj.opt = '';
							obj.opt = $(this).val();
							obj.cState = 0;
							if($(this).parent().find('input[type="radio"]').prop('checked'))
								obj.cState = 1;
							req.options.push(obj);
						}
					});
					req.status = 0;
					if(req.options.length < 2)
						req.status = 0;
					else {
						for(var i=0; i<req.options.length; i++) {
							if(req.options[i]['cState'] == 1)
								req.status = 1;
						}
					}
				}
				else if(req.questionType == 1) {
					req.answer = '';
					if($('#true').prop('checked'))
						req.answer = 1;
					else if($('#false').prop('checked'))
						req.answer = 0;
					req.status = 0;
					if($('#true').prop('checked') || $('#false').prop('checked'))
						req.status = 1;
				}
				else if(req.questionType == 2) {
					req.blanks = [];
					var count = 0;
					$('#ftb input[type="text"].ftb').each(function() {
						if($(this).val() != '') {
							var obj = {};
							obj.ftb = '';
							obj.ftb = $(this).val();
							req.blanks.push(obj);
						}
						count++;
					});
					req.status = 0;
					if(req.blanks.length != count)
						req.status = 0;
					else
						req.status = 1;
				}
				else if(req.questionType == 3) {
					req.matches = [];
					$('#match .match').each(function() {
						if($(this).find('.a').val() != '' || $(this).find('.b').val() != '') {
							var obj = {};
							obj.optA = '';
							obj.optB = '';
							obj.optA = $(this).find('.a').val();
							obj.optB = $(this).find('.b').val();
							req.matches.push(obj);
						}
					});
					req.status = 0;
					if(req.matches.length < 2)
						req.status = 0;
					else {
						req.status = 1;
						//for(var i=0; i< req.matches.length; i++) {
						for(var i=0; i< 2; i++) {
							if(req.matches[i]['optA'] == '' || req.matches[i]['optB'] == '')
								req.status = 0;
						}
						for(var i=0; i< req.matches.length; i++) {
							if(req.matches[i]['optA'] == '') {
								req.matches[i]['optA'] = 'none';
							}
							if(req.matches[i]['optB'] == '') {
								req.matches[i]['optB'] = 'none';
							}
						}
					}
				}
				else if(req.questionType == 4 || req.questionType == 5) {
					req.childQuestions = [];
					$('#compro .sectionNewQuestion').each(function() {
						elem = $(this);
						newobj = {};
						newobj.desc = '';
						newobj.question = '';
						newobj.status = 0;
						var childQuestionId = elem.find('.question').attr('id');
						var childDescId = elem.find('.desc').attr('id');
						newobj.question = CKEDITOR.instances[childQuestionId].getData();
						newobj.desc = CKEDITOR.instances[childDescId].getData();
						/*if(elem.find('.desc').val().length != '')
							newobj.desc = elem.find('.desc').val();
						if(elem.find('.question').val().length != '') {
							var childQuestionId = elem.find('.question').attr('id');
							newobj.question = CKEDITOR.instances[childQuestionId].getData();
							//newobj.question = elem.find('.question').val();
						}*/ // Edited by Jitu
						newobj.questionType = elem.find('.questionTypeSelect').val();
						if(elem.find('.questionId').val() != 0)
							newobj.questionId = elem.find('.questionId').val();
						else
							newobj.questionId = 0;
						if(newobj.questionType == 0) {
							newobj.options = [];
							elem.find('.options input[type="text"].option').each(function() {
								if($(this).val() != '') {
									var obj = {};
									obj.opt = '';
									obj.opt = $(this).val();
									obj.cState = 0;
									if($(this).parent().find('input[type="radio"]').prop('checked'))
										obj.cState = 1;
									newobj.options.push(obj);
								}
							});
							newobj.status = 0;
							if(newobj.options.length < 2)
								newobj.status = 0;
							else {
								for(var i=0; i<newobj.options.length; i++) {
									if(newobj.options[i]['cState'] == 1)
										newobj.status = 1;
								}
							}
						}
						else if(newobj.questionType == 1) {
							newobj.answer = '';
							if(elem.find('.true').prop('checked'))
								newobj.answer = 1;
							else if(elem.find('.false').prop('checked'))
								newobj.answer = 0;
							newobj.status = 0;
							if(elem.find('.true').prop('checked') || elem.find('.false').prop('checked'))
								newobj.status = 1;
						}
						else if(newobj.questionType == 2) {
							newobj.blanks = [];
							var count = 0;
							elem.find('.ftbs input[type="text"].ftb').each(function() {
								if($(this).val() != '') {
									var obj = {};
									obj.ftb = '';
									obj.ftb = $(this).val();
									newobj.blanks.push(obj);
								}
								count++;
							});
							newobj.status = 0;
							if(newobj.blanks.length != count)
								newobj.status = 0;
							else
								newobj.status = 1;
						}
						else if(newobj.questionType == 6) {
							newobj.options = [];
							elem.find('.optionsMul input[type="text"].optionMul').each(function() {
								if($(this).val() != '') {
									var obj = {};
									obj.opt = '';
									obj.opt = $(this).val();
									obj.cState = 0;
									if($(this).parent().find('input[type="checkbox"]').prop('checked'))
										obj.cState = 1;
									newobj.options.push(obj);
								}
							});
							newobj.status = 0;
							if(newobj.options.length < 2)
								newobj.status = 0;
							else {
								for(var i=0; i<newobj.options.length; i++) {
									if(newobj.options[i]['cState'] == 1)
										newobj.status = 1;
								}
							}
						}
						newobj.categoryId = $('#categorySelect').val();
						newobj.sectionId = $('#sectionName').attr('data-id');
						newobj.examId = examId;
						newobj.subjectId = subjectId;
						newobj.categorySwitch = false;
						//to check the length of question
						if(newobj.question.length < 3)
							newobj.status = 0;
						req.childQuestions.push(newobj);
					});
					req.status = 1;
					$.each(req.childQuestions, function(i, v) {
						if(v.status == 0)
							req.status = 0;
					});
				}
				else if(req.questionType == 6) {
					req.options = [];
					$('#optionsMul input[type="text"].optionMul').each(function() {
						if($(this).val() != '') {
							var obj = {};
							obj.opt = '';
							obj.opt = $(this).val();
							obj.cState = 0;
							if($(this).parent().find('input[type="checkbox"]').prop('checked'))
								obj.cState = 1;
							req.options.push(obj);
						}
					});
					req.status = 0;
					if(req.options.length < 2)
						req.status = 0;
					else {
						for(var i=0; i<req.options.length; i++) {
							if(req.options[i]['cState'] == 1)
								req.status = 1;
						}
					}
				}
				else if(req.questionType == 7) {
					req.options = [];
					$('#audio input[type="text"].optionAud').each(function() {
						if($(this).val() != '') {
							var obj = {};
							obj.opt = '';
							obj.opt = $(this).val();
							obj.cState = 0;
							if($(this).parent().find('input[type="radio"]').prop('checked'))
								obj.cState = 1;
							req.options.push(obj);
						}
					});
					req.audio = $('#audio .js-file-path').attr('data-path');
					req.status = 0;
					if(req.options.length < 2)
						req.status = 0;
					else {
						for(var i=0; i<req.options.length; i++) {
							if(req.options[i]['cState'] == 1)
								req.status = 1;
						}
					}
				}
				$.ajax({
					'type'	: 'post',
					'url'	: ApiEndPoint,
					'data'	: JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 0)
						toastr.error(res.message);
					else {
						/*setTimeout(function() {
							if(CKEDITOR.instances['question'])
								CKEDITOR.instances['question'].destroy(true);
							$('#question').attr('placeholder', 'Please enter the question text here');
							ckeditorOn('question');
						}, 1500);*/
						toastr.success("Question saved successfully!");
						$('div[aria-describedby="cke_43"]').html('Please enter the question text here');
						tempQuestion = $('#categorySelect').val();
						$('#sectionNewQuestion .cancel-button').click();
						fetchSectionDetails($('#sectionName').attr('data-id'));
					}
				});
			}
			else
				toastr.error("Please give your question text here which should be longer than 3 characters.");
		}
		else
			toastr.error("Please select a category");
	});
	
	$('#modalAddEditQuestionsCategory .number').keypress(function (e) {
		//if the letter is not digit then don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	});
	
	//for allowing decimal number
	$('#modalAddEditQuestionsCategory .plus, #modalAddEditQuestionsCategory .minus').keypress(function (event) {
		return allowDecimal(event, $(this));
	});

	$('#modalSaveCategory').on('click', function() {
		//for storing correct weights
		for(var i=0; i<$('#modalAddEditQuestionsCategory table.table tbody tr').length; i++) {
			sortOrder[i] = $('#modalAddEditQuestionsCategory table.table tbody tr:eq(' + i + ')').attr('data-id');
		}
		if($('#modalAddEditQuestionsCategory .has-error').length == 0) {
			var brk = true;
			$('#catError').html('');
			var lastRow = $('#modalAddEditQuestionsCategory table.table tbody').find('tr:eq(-1)');
			lastRow.removeClass('has-error');
			var lastRowFlag = false;
			if(lastRow.find('.questionTypeSelect').val() != -1 || lastRow.find('.plus').val() != '' || lastRow.find('.minus').val() != '' || lastRow.find('.number').val() != '') {
				$('#modalAddEditQuestionsCategory table.table tbody').find('tr input[type="text"]').each(function() {
					if($(this).val() == '') {
						$('#modalAddEditQuestionsCategory table.table tbody tr:eq(-1)').addClass('has-error');
						$('#catError').html('Please Specify the required fields to add new category.');
						brk = false;
						return;
					}
				});
				if($('#modalAddEditQuestionsCategory table.table tbody tr:eq(-1)').find('.questionTypeSelect').val() == -1) {
					brk = false;
					$('#modalAddEditQuestionsCategory table.table tbody tr:eq(-1)').addClass('has-error');
					$('#catError').html('Please select a question category.');
				}
			}
			else
				lastRowFlag = true;
			if(brk) {
				$(this).attr('disabled', true);
				var req = {};
				var res;
				req.action = 'save-categories';
				req.sectionId = $('#sectionName').attr('data-id');
				req.examId = examId;
				req.totalMarks = $('#modalAddEditQuestionsCategory #grandTotal').text();
				req.randomQuestions = 0;
				if($('#modalAddEditQuestionsCategory #randomQuestions').prop('checked'))
					req.randomQuestions = 1;
				req.categories = [];
				var categories = $('#modalAddEditQuestionsCategory table.table tbody').find('input[type="text"]');
				var limit = categories.length;
				if(lastRowFlag)
					limit = categories.length - 3;
				var count = 0;
				for(var i=0; i<limit; i = i+3) {
					var temp = {};
					temp.correct = categories[i]['value'];
					temp.questionType = $('#modalAddEditQuestionsCategory table.table tbody').find('tr:eq('+count+') .questionTypeSelect').val();
					temp.wrong = categories[i+1]['value'];
					temp.required = categories[i+2]['value'];
					temp.categoryId = $('#modalAddEditQuestionsCategory table.table tbody').find('tr:eq('+count+')').attr('data-id');
					temp.sectionId = $('#sectionName').attr('data-id');
					temp.examId = examId;
					temp.weight = sortOrder.indexOf($('#modalAddEditQuestionsCategory table.table tbody').find('tr:eq(' + count + ')').attr('data-id'));
					count++;
					req.categories.push(temp);
				}
				$.ajax({
					'type'  : 'post',
					'url'   : ApiEndPoint,
					'data' 	: JSON.stringify(req)
				}).done(function (res) {
					$('#modalSaveCategory').attr('disabled', false);
					res =  $.parseJSON(res);
					if(res.status == 1) {
						$("#modalAddEditQuestionsCategory").modal('hide');
						fetchSectionDetails($('#sectionName').attr('data-id'));
						$('.number').trigger('blur');
					}
					else
						toastr.error(res.message);
				});
			}
		}
	});
	
	$('#modalAddCategory').on('click', function() {
		if($('#modalAddEditQuestionsCategory .has-error').length == 0) {
			var brk = true;
			$('#catError').html('');
			$('#modalAddEditQuestionsCategory table.table tbody tr:eq(-1)').removeClass('has-error');
			$('#modalAddEditQuestionsCategory table.table tbody tr:eq(-1)').find('input[type="text"]').each(function() {
				if($(this).val() == '') {
					$('#modalAddEditQuestionsCategory table.table tbody tr:eq(-1)').addClass('has-error');
					$('#catError').html('Please Specify the required fields to add new category.');
					brk = false;
					return;
				}
			});
			if($('#modalAddEditQuestionsCategory table.table tbody tr:eq(-1)').find('.questionTypeSelect').val() == -1) {
				brk = false;
				$('#modalAddEditQuestionsCategory table.table tbody tr:eq(-1)').addClass('has-error');
				$('#catError').html('Please select a question category.');
			}
			if(brk) {
				addNewCategory(1);
			}
		}
	});

	//select all event listener
	$('#selectAll').on('change', function(){
		var fill = $(this).prop('checked');
		$('#importExamModal tbody tr').each(function() {
			if($(this).css('display') != 'none')
				$(this).find('input[type="checkbox"]').prop('checked', fill);
		});
	});

	//select all event listener
	$('#selectSQAll').on('change', function(){
		var fill = $(this).prop('checked');
		$('#importSubjectQuestionsModal tbody tr').each(function() {
			if($(this).css('display') != 'none')
				$(this).find('input[type="checkbox"]').prop('checked', fill);
		});
	});

	$('#audio').on('click', '.btn-audio-upload', function(){
		$('#frmAudioUpload input').click();
	});
	$('#frmAudioUpload .js-uploader').change(function(){
		var file = $(this).val();
		if (!!file) {
			$('#frmAudioUpload').submit();
		};
	});

	// bind to the form's submit event 
	$('#frmAudioUpload').submit(function(){
		var thiz = $(this);
		
		if (player != undefined) {
			player.pause();
		};

		$('.btn-audio-upload').attr('disabled', true);
		$('.btn-audio-upload').after('<span class="upload-loader"><img src="'+sitepathManageIncludes+'assets/custom/img/ajax-loader.gif" alt="loader" /></span>');
		
		var options = { 
			//target:        '#output2',   // target element(s) to be updated with server response 
			data: { subjectId: subjectId },
			dataType: 'json',
			beforeSubmit:  showRequest,  // pre-submit callback 
			success: function(msg) {
				$('.js-uploader').val('');
				$('.upload-loader').remove();
				$('.btn-audio-upload').attr('disabled', false);
				if (msg.status == 0) {
					toastr.error(msg.message);
				} else {
					var mp3 = msg.fileName;
					$('.js-file-path').html(
						'<audio controls="control" id="audioShowcasePlayer" preload="none" src="'+mp3+'" type="audio/mp3"></audio>'
					);
					$('.js-file-path').attr('data-path', mp3);
					player = new MediaElementPlayer('#audioShowcasePlayer', {
								type: 'audio/mp3',
								success: function (mediaElement, domObject) {
									mediaElement.addEventListener('pause', function () {
										//console.log("paused");
									}, false);
									mediaElement.addEventListener('play', function () {
										//console.log("play");
										videoDuration = mediaElement.duration;
									}, false);
									mediaElement.addEventListener('ended', function () {
										//console.log("ended");
									}, false);
								}
							});
					var sources = [
						{ src: mp3, type: 'audio/mp3' }
					];

					player.setSrc(sources);
					player.load();
					//player.play();
				}
			}  // post-submit callback 
		}; 
		// inside event callbacks 'this' is the DOM element so we first 
		// wrap it in a jQuery object and then invoke ajaxSubmit 
		$(this).ajaxSubmit(options);

		// !!! Important !!! 
		// always return false to prevent standard browser submit and page navigation 
		return false;
	});
	// pre-submit callback 
	function showRequest(formData, jqForm, options) { 
	    // formData is an array; here we use $.param to convert it to a string to display it 
	    // but the form plugin does this for you automatically when it submits the data 
	    var queryString = $.param(formData); 

	    // jqForm is a jQuery object encapsulating the form element.  To access the 
	    // DOM element for the form do this: 
	    // var formElement = jqForm[0]; 
	 
	    //toastr.error('About to submit: \n\n' + queryString); 
	 
	    // here we could return false to prevent the form from being submitted; 
	    // returning anything other than false will allow the form submit to continue 
	    return true; 
	}
});
</script>