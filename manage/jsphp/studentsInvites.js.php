<script type="text/javascript">
var TableDatatablesManaged = function () {

    var initTable1 = function () {

        var table = $('#sample_1');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // setup buttons extension: http://datatables.net/extensions/buttons/
            buttons: [
                { extend: 'print', className: 'btn dark btn-outline' },
                { extend: 'pdf', className: 'btn green btn-outline' },
                { extend: 'csv', className: 'btn purple btn-outline' }
            ],

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 15, 20, -1],
                [10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery('#sample_1_wrapper');

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).prop("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).prop("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
        }

    };

}();
var keys_available,keys_pending;
function bindCoursesDropdown() {
    var req = {};
    var res;
    req.action = "Courses_for_student";
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        if (data.courses.length>0) {
            for (var i = 0; i < data.courses.length; i++)
            {
                var id = data.courses[i].id;
                var name = data.courses[i].name;
                $("#courses").append("<option value=" + id + " >" + name + "</option>");
            }
        } else {
            $("#courses").closest('.modal-body').html('<div class="note note-info">Add '+terminologies["course_plural"].toLowerCase()+' first!</div>');
            $("#btnInviteStudent").remove();
        }
    });
}
$('#modalInviteStudentsToCourse').on('shown.bs.modal', function () {
    $('#textEmailAddresses').val('');
    CKEDITOR.instances['textEmailNote'].setData('Dear Student,<br /><br />Welcome to Integro! You have been invited to join the following course.<br /><br />[course_invite_block]<br /><br />Best Regards,<br />Integro Team<br />Integro.io');
    keys_available = get_keys_remaining();
    $('.js-remaining').html(keys_available);
    $('.js-pending').html(keys_pending);
})
function get_keys_remaining() {
    var req = {};
    req.action = 'keys_avilable';
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        if (data.status == 1) {
            keys_available = data.keys;
            keys_pending = data.invitation;
        }
        else {
            toastr.error(res.msg);
        }
    });
    return keys_available;
}
$('#btnInviteStudent').on('click', function () {
    elem = $(this);
    $('.help-block').remove();
    var invalid = '';
    var correct = true;
    var emails = $.trim($('#textEmailAddresses').val());
    var emailNote = CKEDITOR.instances['textEmailNote'].getData();
    var email = emails.split(',');
    if (emails == '') {
        toastr.error('Please enter email id');
        return false;
    }
    else if (checkDuplicates(email) == 'false') {
        toastr.error('Please remove duplicates');
        return false;
    }
    else if ((email.length > keys_available)) {
        toastr.error('You have only ' + keys_available + ' keys available');
        return false;
    }
    else if (emailNote.indexOf('[course_invite_block]') == -1) {
        toastr.error('Please add [course_invite_block] in the email note, it is necessary.');
        return false;
    }
    $.each(email, function (index, value) {
        if (validateEmail($.trim(value)) == false) {
            invalid += value + ',';
            correct = false;
        }
    });
    if (correct == false) {
        $('#textEmailAddresses').after('<span class="help-block text-danger "> Correct these email addresses:\n' + invalid+'</span>');
        return false;
    }
    var courseId = $('#courses').val();
    if (courseId == 0) {
        $('#courses').after('<span class="help-block text-danger "> Please select a '+terminologies["course_single"]+' ! </span>');
        return false;
    }
    else {
        var req = {};
        req.courseId = $('#courses').val();
        req.action = 'send_activation_link_student';
        req.email = email;
        req.emailNote = emailNote;
        req.enrollment_type = 0;
        elem.attr('disabled', true);
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            elem.attr('disabled', false);
            data = $.parseJSON(res);
            if (data.status == 1) {
                toastr.success(data.message);
            }
            else {
                if(data.msg)
                    toastr.error(data.msg);
                else
                    toastr.error(data.message);
            }
            $('#modalInviteStudentsToCourse').modal('hide');

        });
    }
});
var display_button='';
function invitation_details() {
    var req = {};
    var res;
    req.action = "student_invitation_details";
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        //return false;
        var html = "";
        if ( $.fn.DataTable.isDataTable( '#example' ) ) {
            $('#sample_1').DataTable().destroy();
        }
        if (data.invitations.length > 0) {
            for (var i = 0; i < data.invitations.length; i++)
            {
                var invitation_id = data.invitations[i].id;
                var course_id = "C" + String("000" + data.invitations[i].course_id).slice(-4);
                var email = data.invitations[i].email_id;
                if (data.invitations[i].contactMobile == undefined)
                    var contact_no = '--';
                else
                    var contact_no = data.invitations[i].contactMobile;
                if (data.invitations[i].enrollment_type == 0)
                    var enrollment_type = terminologies["course_single"]+' Key';
                else if (data.invitations[i].enrollment_type == 1)
                    var enrollment_type = 'Market Place';
                var status = data.invitations[i].status;
                var course = data.invitations[i].courseName;
                if (data.invitations[i].studentName == null)
                    var studentName = '--';
                else
                    var studentName = data.invitations[i].studentName;
                var timestamp = data.invitations[i].timestamp;
                //var signup_time = new Date(course.signup_time+'000');
                var signup_time = data.invitations[i].signup_time;
                signup_time = ((signup_time == null)?'-':signup_time);
                var disable = '';
                var actionButton = '';
                switch (status) {
                    case '0':
                        display_button = '<span class="label label-warning"><i class="fa fa-info-circle"></i> Pending</span>';
                        actionButton = '<a href="javascript:void(0)" class="btn btn-circle btn-sm green resend_pending"> Resend</a><a href="javascript:void(0)" class="btn btn-circle btn-sm red reject_link"> Reject</a>';
                        break;
                    case '1':
                        display_button = '<span class="label label-success"><i class="fa fa-check-circle"></i> Accepted</span>';
                        // disable = "disabled= disabled ";
                        actionButton = '<a href="javascript:void(0)" class="btn btn-circle btn-sm green resend_link"> Resend</a><a href="javascript:void(0)" class="btn btn-circle btn-sm red reject_link"> Reject</a>';
                        break;
                    case '2':
                        display_button = '<span class="label label-danger"><i class="fa fa-ban"></i> Rejected</span>';
                        actionButton = '<a href="javascript:void(0)" class="btn btn-circle btn-sm green resend_link"> Resend</a>';
                        break;
                    case '3':
                        display_button = '<span class="label label-info"><i class="fa fa-info-circle"></i> Resent</span>';
                        actionButton = '<a href="javascript:void(0)" class="btn btn-circle btn-sm green resend_link"> Resend</a><a href="javascript:void(0)" class="btn btn-circle btn-sm red reject_link"> Reject</a>';
                        break;
                }
                html += '<tr class="odd gradeX" data-invitation= ' + invitation_id + '>'+
                            '<td>' + (i + 1) + '</td>'+
                            //'<td>' + course_id + '</td>'+
                            '<td>' + course + ' (' + course_id + ')</td>'+
                            '<td>' + studentName + '</td>'+
                            '<td><a href="mailto:' + email + '"> ' + email + ' </a></td>'+
                            '<td>' + contact_no + '</td>'+
                            '<td>' + enrollment_type + '</td>'+
                            '<td>' + timestamp + '</td>'+
                            '<td>' + signup_time + '</td>'+
                            '<td>' + display_button + '</td>'+
                            '<td>' + actionButton + '</td>'+
                        '</tr>';
            }
            // $('#sample_1').find('tr.generated').remove();
            $('#sample_1 tbody').html(html);
            TableDatatablesManaged.init();
        } else {
            $('#sample_1 tbody').html('<tr><td colspan="9">No '+terminologies["student_plural"].toLowerCase()+' invited yet</td></tr>');
        }

        $('.resend_link').off('click');
        $('.resend_link').on('click', function () {
            var req = {};
            var link_id = $(this).parents('tr').attr('data-invitation');
            var res;
            req.action = 'keys_avilable';
            $.ajax({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                data = $.parseJSON(res);
                if (data.status == 1) {
                    if (data.keys > 0) {
                        req.action = "resend_invitation_link";
                        req.link_id = link_id;
                        $.ajax({
                            'type': 'post',
                            'url': ApiEndPoint,
                            'data': JSON.stringify(req)
                        }).done(function (res) {
                            data = $.parseJSON(res);
                            if (data.status == 1)
                                toastr.success('Invitation has been sent Successfully');
                            invitation_details();
                            //  tr.find('.status').html('<i class="fa fa-info-circle"></i>pending');

                        });
                    }
                    else {
                        toastr.error('You do not have '+terminologies["course_single"].toLowerCase()+' keys to send invitation.');
                    }
                }
                else {
                    toastr.error('You do not have '+terminologies["course_single"].toLowerCase()+' keys to send invitation.');
                }
            });
        });
        $('.reject_link').off('click');
        $('.reject_link').on('click', function () {
            var link_id = $(this).parents('tr').attr('data-invitation');
            var req = {};
            var res;
            req.action = "reject_link_by_admin";
            req.link_id = link_id;
            $.ajax({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                data = $.parseJSON(res);
                if (data.status == 1)
                    toastr.success("Invitation has been rejected Successfully");
                invitation_details();
            });
        });
        $('.resend_pending').off('click');
        $('.resend_pending').on('click', function() {
            var parent = $(this).parents('tr:eq(0)');
            var req = {};
            var res;
            req.action = 'resend-pending-invitation';
            req.invitationId = $(this).parents('tr').attr('data-invitation');
            $.ajax({
                'type'  :   'post',
                'url'   :   ApiEndPoint,
                'data'  :   JSON.stringify(req)
            }).done(function(res) {
                res = $.parseJSON(res);
                if(res.status == 1)
                    toastr.success('Mail sent successfully.');
                else
                    toastr.error(res.message);
            });
        });
    });
}
</script>