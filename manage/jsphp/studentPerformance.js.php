<script type="text/javascript">
function fetchStudentPerformance() {
	var req = {};
	var res;
	req.action = 'get-student-performance';
	req.courseId = courseId;
	req.subjectId = subjectId;
    req.studentId = studentId;
	ajaxRequest({
		'type': 'post',
		'url': ApiEndPoint,
		'data': JSON.stringify(req),
		success:function(res) {
			res = $.parseJSON(res);
			if (res.status == 0)
				toastr.error(res.message);
			else {
				fillStudentPerformance(res);
			}
		}
	});
}
function fillStudentPerformance(data) {
	//console.log(data);
	var html = '';
    if (data.student) {
        $('.js-student').html(data.student.studentDetails.name+"'s Performance");
    }
	$('#subjectScore').html(data.subjectScore+'%');
	if (data.notes) {
		$('#gradingNotes').html(
			'<div class="form-group">'+
				'<label for="">Notes :</label>'+
				'<div class="well">'+data.notes+'</div>'+
			'</div>'
			);
	}
    if (data.contents) {
        var contents = data.contents;
        for (var i = 0; i < contents.length; i++) {
            var section = contents[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            html+='<tr data-id="'+contents[i].id+'" data-type="exam">'+
                    '<td>'+contents[i].title+'</td>'+
                    '<td>Content</td>'+
                    '<td>'+section+'</td>'+
                    '<td>-</td>'+
                    '<td>-</td>'+
                    '<td>-</td>'+
                    '<td>-</td>'+
                    '<td>-</td>'+
                    '<td>'+contents[i].weight_grading+'</td>'+
                    '<td>'+contents[i].performance+'</td>'+
                '</tr>';
        }
    }
    if (data.exams) {
        var exams = data.exams;
        for (var i = 0; i < exams.length; i++) {
            var endDate = '-';
            if (exams[i].endDate) {
                endDate = formatDate(parseInt(exams[i].endDate), '-');
            }
            var section = exams[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            html+='<tr data-id="'+exams[i].id+'" data-type="exam">'+
                    '<td>'+exams[i].name+'</td>'+
                    '<td>'+exams[i].type+'</td>'+
                    '<td>'+section+'</td>'+
                    '<td>'+formatDate(parseInt(exams[i].startDate), '-')+'</td>'+
                    '<td>'+endDate+'</td>'+
                    '<td>'+exams[i].user_attempts+'/'+exams[i].attempts+'</td>'+
                    '<td>'+exams[i].grade_type+'</td>'+
                    '<td>'+exams[i].avg_attempts+'</td>'+
                    '<td>'+exams[i].weight+'</td>'+
                    '<td>'+exams[i].performance+'</td>'+
                '</tr>';
        }
    }
    if (data.subjective_exams) {
        var exams = data.subjective_exams;
        for (var i = 0; i < exams.length; i++) {
            var endDate = '-';
            if (exams[i].endDate) {
                endDate = formatDate(parseInt(exams[i].endDate), '-');
            }
            var section = exams[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            html+='<tr data-id="'+exams[i].id+'" data-type="subjective-exam">'+
                    '<td>'+exams[i].name+'</td>'+
                    '<td>Subjective Exam</td>'+
                    '<td>'+section+'</td>'+
                    '<td>'+formatDate(parseInt(exams[i].startDate), '-')+'</td>'+
                    '<td>'+endDate+'</td>'+
                    '<td>'+exams[i].user_attempts+'/'+exams[i].attempts+'</td>'+
                    '<td>'+exams[i].grade_type+'</td>'+
                    '<td>'+exams[i].avg_attempts+'</td>'+
                    '<td>'+exams[i].weight+'</td>'+
                    '<td>'+exams[i].performance+'</td>'+
                '</tr>';
        }
    }
    if (data.manual_exams) {
        var exams = data.manual_exams;
        for (var i = 0; i < exams.length; i++) {
            var section = exams[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            html+='<tr data-id="'+exams[i].id+'" data-type="manual-exam">'+
                    '<td>'+exams[i].name+'</td>'+
                    '<td>Manual Exam</td>'+
                    '<td>'+section+'</td>'+
                    '<td>NA</td>'+
                    '<td>NA</td>'+
                    '<td>'+exams[i].user_attempts+'/1</td>'+
                    '<td>'+exams[i].grade_type+'</td>'+
                    '<td><span class="badge">N.A.</span></td>'+
                    '<td>'+exams[i].weight+'</td>'+
                    '<td>'+exams[i].performance+'</td>'+
                '</tr>';
        }
    }
    if (data.submissions) {
        var exams = data.submissions;
        for (var i = 0; i < exams.length; i++) {
            var endDate = '-';
            if (exams[i].endDate) {
                endDate = formatDate(parseInt(exams[i].endDate), '-');
            }
            var section = exams[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            html+='<tr data-id="'+exams[i].id+'" data-type="submission">'+
                    '<td>'+exams[i].name+'</td>'+
                    '<td>Submission</td>'+
                    '<td>'+section+'</td>'+
                    '<td>'+formatDate(parseInt(exams[i].startDate), '-')+'</td>'+
                    '<td>'+endDate+'</td>'+
                    '<td>'+exams[i].user_attempts+'/'+exams[i].attempts+'</td>'+
                    '<td>'+exams[i].grade_type+'</td>'+
                    '<td>'+exams[i].avg_attempts+'</td>'+
                    '<td>'+exams[i].weight+'</td>'+
                    '<td>'+exams[i].performance+'</td>'+
                '</tr>';
        }
    }
    $('#listExams').html(html);
}
jQuery(document).ready(function() {
    
});
</script>