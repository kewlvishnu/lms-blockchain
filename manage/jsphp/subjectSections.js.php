<script type="text/javascript">
var headingCount = 0;
var chapterId = 1;
var reorderTimer = null;
var quizJSON = {
    "info": {
        "name":    "",
        "main":    "",
        "results": "Result",
        "level1":  "Level1",
        "level2":  "Level2",
        "level3":  "Level3",
        "level4":  "Level4",
        "level5":  "Level5" // no comma here
    },
    "questions": []
};
var UINestable = function () {

    var updateOutput = function (e) {
        //console.log(e);
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
        if(reorderTimer != null)
                clearTimeout(reorderTimer);
            reorderTimer = setTimeout(function(){reNumberEverything(true);}, 5000);
    };


    return {
        //main function to initiate the module
        init: function () {

            // activate Nestable for list 1
            $('#content').nestable({
                group: 1,
                noDragClass: 'no-drag'
            })
                .on('change', updateOutput);

            // output initial serialised data
            updateOutput($('#content').data('output', $('#content_output')));

            /*$('#nestable_list_menu').on('click', function (e) {
                var target = $(e.target),
                    action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });*/
            $(".d-checkbox").bootstrapSwitch();

        }

    };

}();

var FormEditable = function() {

    $.mockjaxSettings.responseTime = 500;

    var log = function(settings, response) {
        var s = [],
            str;
        s.push(settings.type.toUpperCase() + ' url = "' + settings.url + '"');
        for (var a in settings.data) {
            if (settings.data[a] && typeof settings.data[a] === 'object') {
                str = [];
                for (var j in settings.data[a]) {
                    str.push(j + ': "' + settings.data[a][j] + '"');
                }
                str = '{ ' + str.join(', ') + ' }';
            } else {
                str = '"' + settings.data[a] + '"';
            }
            s.push(a + ' = ' + str);
        }
        s.push('RESPONSE: status = ' + response.status);

        if (response.responseText) {
            if ($.isArray(response.responseText)) {
                s.push('[');
                $.each(response.responseText, function(i, v) {
                    s.push('{value: ' + v.value + ', text: "' + v.text + '"}');
                });
                s.push(']');
            } else {
                s.push($.trim(response.responseText));
            }
        }
        s.push('--------------------------------------\n');
        $('#console').val(s.join('\n') + $('#console').val());
    }

    var initAjaxMock = function() {
        //ajax mocks

        $.mockjax({
            url: '/post',
            response: function(settings) {
                log(settings, this);
            }
        });

        $.mockjax({
            url: '/error',
            status: 400,
            statusText: 'Bad Request',
            response: function(settings) {
                this.responseText = 'Please input correct value';
                log(settings, this);
            }
        });

        $.mockjax({
            url: '/status',
            status: 500,
            response: function(settings) {
                this.responseText = 'Internal Server Error';
                log(settings, this);
            }
        });

        $.mockjax({
            url: '/groups',
            response: function(settings) {
                this.responseText = [{
                    value: 0,
                    text: 'Guest'
                }, {
                    value: 1,
                    text: 'Service'
                }, {
                    value: 2,
                    text: 'Customer'
                }, {
                    value: 3,
                    text: 'Operator'
                }, {
                    value: 4,
                    text: 'Support'
                }, {
                    value: 5,
                    text: 'Admin'
                }];
                log(settings, this);
            }
        });

    }

    var initEditables = function() {

        //set editable mode based on URL parameter
        $.fn.editable.defaults.mode = 'inline';

        //global settings 
        $.fn.editable.defaults.inputclass = 'form-control';
        $.fn.editable.defaults.url = ApiEndPoint;

        $('.description').editable({
            showbuttons: 'bottom',
            params: function (params) {
                var req = {};
                req.descText = params.value;
                req.contentId = $(this).parents('.section-content').attr('data-conid');
                req.action = "save-stuff-desc";
                return JSON.stringify(req);
            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {

            // inii ajax simulation
            initAjaxMock();

            // init editable elements
            initEditables();
        }

    };

}();
function fetchContent() {
    var req = {};
    var res;
    req.courseId = courseId;
    req.subjectId = subjectId;
    req.chapterId = chapterId;
    //chapterId = chapterId;
    req.action = "get-chapter-content";
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1)
            initializePanel(res);
        else
            toastr.error(res.message);
    });
}
function initializePanel(data) {
    var flagHeading = false, flagContent = false;
    //console.log(data);
    if(data.status == 1) {
        $.each(data.headings, function(i, heading) {
            addNewHeading(heading);
            flagHeading = false;
            flagContent = false;
            $.each(heading.content, function(i, content) {
                addNewContent(content);
            });
        });
    }
    /*$('.section-heading').each( function() {
        headingEventHandlers($(this));
    });

    $('.section-content').each(function() {
        contentEventHandlers($(this));
    });*/

    UINestable.init();
    FormEditable.init();
}
function addNewHeading(heading) {
    headingCount ++;
    var headingNum = headingCount;
    headingNum = 'Section ' + headingNum + ':';
    if (typeof heading == "undefined") {
        var heading = {};
        heading.id = "0";
        heading.name = "";
    }
    var html = '<div class="section-heading sortable-item temp" data-hid="' + heading.id + '">'
                    + '<div class="view-pane hidden-l">'
                        + '<div class="panel-heading">'
                            + '<label class="heading-number">' + headingNum + ': </label> <span class="heading-title">' + heading.name + '</span>'
                            + '&nbsp;&nbsp;<a href="#" class="edit-heading"><i class="fa fa-pencil"></i></a>'
                            + '<button class="btn btn-xs btn-warning add-content pull-right" style="margin-top:0px;"><i class="fa fa-plus"></i> Add Lecture</button>'
                        + '</div>'
                    + '</div>'
                    + '<div class="edit-pane">'
                        + '<div class="row panel-body">'
                            + '<div class="form-group">'
                                + '<div class="col-lg-2">'
                                    + '<label class="heading-number">' + headingNum + ': </label>'
                                + '</div>'
                                + '<div class="col-lg-10">'
                                    + '<input type="text" class="form-control heading-title-input" placeholder="Title" value="' + heading.name + '" maxlength="60"><br>'
                                    + 'Note : After you click on <button class="btn btn-info" DISABLED>Save</button> please click on <button class="btn btn-xs btn-warning" DISABLED><i class="fa fa-plus"></i> Add Lecture</button>. You can add multiple Lecctures'
                                + '</div>'
                            + '</div>'
                        + '</div>'
                        + '<div class="row panel-body">'
                            + '<div class="col-md-6"><a class="btn btn-info save-title" href="#">Save</a> Or <a href="#" class="cancel-edit">Cancel</a></div>'
                            + '<div class="col-md-6" style="text-align: right;">'
                                + '<a href="#" class="delete-heading">'
                                    + '<i class="fa fa-trash-o"></i>'
                                + '</a>'
                            + '</div>'
                        + '</div>'
                    + '</div>'
                + '</div>';
    var html = '<li class="dd-item section-heading sortable-item" data-hid="' + heading.id + '">'+
                    '<div class="dd-handle bg-green bg-font-green"><span class="heading-number">' + headingNum + '</span> <span class="section-title">' + heading.name + '</span> <a href="javascript:;" class="btn-section-title bg-font-green-jungle no-drag" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i></a> <button class="btn btn-info btn-sm btn-content-title no-drag" data-toggle="modal" data-action="new"><i class="fa fa-plus"></i> Add Lecture</button></div>'+
                    '<ol class="dd-list"></ol>'+
                '</li>';
    var elem = $(html);
    $('#content>.dd-list').append(elem);
    
    reNumberEverything(false);
}
function getLastHeadingContentCount() {
    return 0;
    return $('.section-heading').last().find('.section-content').length;
}
function addNewContent(content, heading) {
    var headingNum = headingCount;
    var contentCount = getLastHeadingContentCount() + 1;
    
    var contentNum = 'Content ' + headingNum  + '.' + contentCount;
    //console.log(content);
    if (typeof content == "undefined" || content == null) {
        var content = {};
        content.id = "0";
        content.title = "";
        content.hideEdit = false;
    } else {
        content.hideEdit = true;
    }
    var checked = parseInt(content.demo) ? 'CHECKED' : '';
    var html = '<div class="section-content indented temp sortable-item" data-conid="' + content.id + '">'
                    + '<div class="view-pane hidden-l">'
                        + '<div class="panel-heading orange">'
                            + '<label class="content-number">' + contentNum + '</label> <span class="content-title">' + content.title + '</span>'
                            + '&nbsp;&nbsp;<a href="#" class="edit-content"><i class="fa fa-pencil"></i></a>'
                            + '<button class="btn btn-xs btn-warning add-stuff pull-right" style="margin-top:0px;"><i class="fa fa-plus"></i> Add Content</button>'
                        + '</div>'
                    + '</div>'
                    + '<div class="edit-pane">'
                        + '<div class="row panel-body">'
                            + '<div class="form-group">'
                                + '<div class="col-lg-2">'
                                    + '<label class="content-number">' + contentNum + '</label>'
                                + '</div>'
                                + '<div class="col-lg-10">'
                                    + '<input type="text" class="form-control content-title-input" placeholder="Lecture name" value="' + content.title + '" maxlength="60"/><br>'
                                    + '<label><input type="checkbox" class="demo" ' + checked + '> Free Demo</label><br>'
                                    + 'Note : You can choose the content type after clicking <button class="btn btn-info" DISABLED>Save</button>'
                                + '</div>'
                            + '</div>'
                        + '</div>'
                        + '<div class="row panel-body">'
                            + '<div class="col-md-6"><a class="btn btn-info save-title" href="#">Save</a> Or <a href="#" class="cancel-edit">Cancel</a></div>'
                            + '<div class="col-md-6" style="text-align: right;">'
                                + '<a href="#" class="delete-content">'
                                    + '<i class="fa fa-trash-o"></i>'
                                + '</a>'
                            + '</div>'
                        + '</div>'
                    + '</div>';
    html += '</div>';

    if(typeof content.stuff != "undefined" && content.stuff != null) {
        var html = '<li class="dd-item section-content sortable-item" data-conid="' + content.id + '">'+
                        '<div class="dd-handle bg-yellow bg-font-yellow"> <span class="content-number">' + contentNum + '</span> <span class="content-title">' + content.title + '</span> <a href="javascript:;" class="btn-content-title bg-font-green-jungle no-drag" data-toggle="modal"><i class="fa fa-edit"></i></a> <span class="pull-right"><a class="collapsible bg-font-green-jungle collapsed no-drag" role="button" data-toggle="collapse" href="#ddItem' + content.id + '" aria-expanded="false" aria-controls="ddItem' + content.id + '"><i class="fa fa-toggle-down"></i></span></a></div>'+
                        '<div class="collapse" id="ddItem' + content.id + '">'+
                        '</div>'+
                    '</li>';
        var elem = $(html);
        elem.attr('data-content-type', content.fileType);
        if (content.fileName == 'Link' || content.fileName == 'Text') {
            elem.attr('data-file-name', content.stuff);
        } else {
            elem.attr('data-file-name', content.fileName);
        }
        if (content.fileType == "quiz" || content.fileType == "survey") {
            elem.attr('data-stuff', content.stuff);
        }
        elem.attr('data-metadata', content.metadata);
        if(content.fileType == 'text')
            elem.append('<div class="hide text-stuff">' + content.stuff + '</div>');
        else if(content.fileType == 'Youtube' || content.fileType == 'Vimeo')
            elem.append('<div class="hide link-stuff">' + content.stuff + '</div>');
        else if(content.fileType == 'quiz')
            elem.append('<div class="hide link-stuff">' + content.stuff + '</div>');
        elem.addClass('has-file');
        
        var stuffDP = createStuffDP(elem);
        //console.log(stuffDP[0].innerHTML);
        if(content.published == '1') {
            stuffDP.find('.change-published').attr('data-published', '1').html('<i class="fa fa-cloud-upload"></i> UnPublish');
            stuffDP.find('.change-published').removeClass('btn-warning').addClass('green-jungle');
            elem.closest('.section-content').find('.dd-handle').removeClass('bg-yellow bg-font-yellow').addClass('bg-green-jungle bg-font-green-jungle');
            stuffDP.find('.downloadable-holder').removeClass('hide');
        }

        if(content.downloadable == '1' && content.fileType != 'text') {
            //stuffDP.find('.change-downloadable').attr('data-downloadable', '1').text('Remove Downloadable');
            stuffDP.find('.change-downloadable').attr('data-downloadable', '1').bootstrapSwitch('state', true, true);
        }
        
        elem.find('#ddItem' + content.id).append(stuffDP);
        if(content.descText.trim() != '') {
            var stuffDescriptionWidget = createStuffDescriptionWidget(elem);
            elem.find('.supp-section').before(stuffDescriptionWidget);
            stuffDP.find('.add-stuff-desc').remove();
        }
        if(typeof content.supplementaryFiles !== 'undefined' && content.supplementaryFiles !== null) {
            var supplementaryStuffWidget = createSupplementaryStuffWidget(elem, content.supplementaryFiles);
            elem.find('.supp-section').before(supplementaryStuffWidget);
        }
    } else {
        var html = '<li class="dd-item section-content sortable-item" data-conid="' + content.id + '">'+
                        '<div class="dd-handle bg-yellow-lemon bg-font-yellow-lemon"> <span class="content-number">' + contentNum + '</span> <span class="content-title">' + content.title + '</span> <a href="javascript:;" class="btn-content-title bg-font-green-jungle no-drag" data-toggle="modal"><i class="fa fa-edit"></i></a> <span class="pull-right"><button class="btn btn-warning btn-xs btn-content no-drag" data-toggle="modal" data-action="new"><i class="fa fa-plus"></i> Add Content</button></span></div>'+
                    '</li>';
        var elem = $(html);
    }
    
    if(typeof heading == 'undefined' || heading == null) {
        $('.section-heading:last-child .dd-list').append(elem);
    } else if(heading.nextAll('.section-heading:eq(0)').length == 0) {
        $('.section-heading:last-child .dd-list').append(elem);
    } else {
        heading.find('.dd-list').append(elem);
        //elem.insertBefore(heading.nextAll('.section-heading:eq(0)'));
    }
    
    if(content.descText != null && content.descText.trim() != '') {
        //stuffDescriptionWidget.find('textarea').val(content.descText.trim());
        stuffDescriptionWidget.find('.description').text(content.descText.trim());
        // CKEDITOR.replace( editorId, {
            // height: 100
        // });
        // CKEDITOR.instances[editorId].setData(content.descText.trim());
    }
    reNumberEverything(false);
}
function reNumberEverything(updateOnServer) {
    var headingNum = 1;
    var contentNum = 1;
    var ordering = {'headings' : {}};
    var formattedContentNum, formattedHeadingNum, increaseHeading, headingEncounter;
    headingEncounter = false;
    var  hid = 0;
    var conid = 0;
    //ordering['headings']['0'] = {};
    //ordering['headings']['0']['content'] = {};
    $('#content .dd-list > .sortable-item').each(function() {
        if($(this).hasClass('section-heading')) {
            headingEncounter = true;
            if(increaseHeading == true) {
                headingNum++;
                increaseHeading = false;
            }
            if(updateOnServer == true) {
                hid = $(this).attr('data-hid');
                if(hid != '0')
                    ordering['headings'][hid] = {'weight' : headingNum, 'content' : {}};
            }
            formattedHeadingNum = 'Section ' + headingNum + ':';
            $(this).find('.heading-number').html(formattedHeadingNum);
            increaseHeading = true;
            contentNum = 1;
        }
        
        if($(this).hasClass('section-content')) {
            formattedContentNum = 'Lecture '  + headingNum + '.' + contentNum;
            if(headingEncounter == false) {
                formattedContentNum = '';
            }
            if(updateOnServer == true) {
                conid = $(this).attr('data-conid');
                if(conid != '0')
                    ordering['headings'][hid]['content'][conid] = contentNum;
            }
            $(this).find('.content-number').html(formattedContentNum);
            contentNum++;
            // if(headingNum == 1)
                // increaseHeading = true;
        }
    });
    if(updateOnServer == true) {
        //console.log(ordering);
        
        updateContentOrderOnServer(ordering);
    }
    headingCount = headingNum - 1;
}
function createStuffDP(sectionContent) {
    //console.log(sectionContent.parents('.section-heading'));
    var contentType = sectionContent.attr('data-content-type');
    var fileName = sectionContent.attr('data-file-name');
    var metadata = sectionContent.attr('data-metadata');
    //console.log(contentType);
    var html = '<div class="well">'+
                    '<div class="row">'+
                        '<div class="col-md-9">'+
                            '<h3 class="font-yellow content-type-title">File Type</h3>'+
                            '<p class="text-overflow file-name">File Name</p>'+
                        '</div>'+
                        '<div class="col-md-3">'+
                            '<div class="form-group">'+
                                '<div class="btn-group btn-group-justified" role="group">'+
                                    '<div class="btn-group" role="group">'+
                                        '<a href="javascript:;" class="btn btn-sm blue btn-preview" target="_blank"><i class="fa fa-eye"></i> Preview</a>'+
                                    '</div>'+
                                    '<div class="btn-group" role="group">'+
                                        //'<button class="btn btn-sm green-jungle"><i class="fa fa-cloud-upload"></i> UnPublish</button>'+
                                        '<button class="btn btn-sm btn-warning change-published" data-published="0"><i class="fa fa-cloud-upload"></i> Publish</button>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<div class="btn-group btn-group-justified" role="group">'+
                                    '<div class="btn-group" role="group">'+
                                        '<button class="btn btn-sm green btn-content edit-stuff" data-toggle="modal" data-action="edit"><i class="fa fa-edit"></i> Edit</button>'+
                                    '</div>'+
                                    '<div class="btn-group" role="group">'+
                                        '<button class="btn btn-sm btn-danger delete-stuff"><i class="fa fa-trash"></i> Delete</button>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="clearfix downloadable-holder">'+
                                '<h4 class="downloadable pull-right">'+
                                    'Downloadable:</b> <input type="checkbox" class="d-checkbox change-downloadable" data-downloadable="0">'+
                                '</h4>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="row supp-section">'+
                        '<div class="col-md-12">'+
                            '<hr>'+
                            '<button class="btn btn-success btn-supp-material" data-toggle="modal">Add Supplementary Material</button>'+
                            '<i class="tooltips fa fa-info-circle" data-original-title="Supplementary materials is basically files that the '+terminologies["student_plural"].toLowerCase()+' can download. You can add more that one files. Each lecture can have its own supplemntary material." data-placement="right"></i>'+
                            '<span class="add-stuff-desc">'+
                                '<button class="btn btn-success">Add Description</button>'+
                                '<i class="tooltips fa fa-info-circle" data-original-title="Please add any information or details about the lecture for the '+terminologies["student_single"].toLowerCase()+'" data-placement="right"></i>'+
                            '</span>'+
                        '</div>'+
                    '</div>'+
                '</div>';
    var stuffDP = $(html);
    //event handler for preview button click
    stuffDP.find('.preview-button').off('click');
    stuffDP.find('.preview-button').on('click', function() {
        var iContentId = $(this).parents('div.section-content:eq(0)').attr('data-conId');
        var iChapterId = $(this).parents('div.section-content:eq(0)').prev().attr('data-hid');
        window.location = "chapterPreview.php?courseId=" + courseId + "&subjectId=" + subjectId + "&chapterId=" + iChapterId + '&contentId=' + iContentId;
    });
    stuffDP.find('.tooltips').tooltip();
    if(contentType == 'text' || contentType == 'Youtube' || contentType == 'Vimeo')
        stuffDP.find('.downloadable-holder').addClass('hide');

    stuffDP.find('.content-type-title').html(getStuffImageByType(contentType));
    stuffDP.find('.file-name').html(fileName).attr('title',fileName);

    if(contentType ==  'text') {
        var textStuff = sectionContent.find('.text-stuff');
        if(textStuff.length != 0){
            stuffDP.append('<div class="hide text-stuff">' + textStuff.html() + '</div>');
            textStuff.remove();
        }
    }
    if(contentType ==  'Youtube' || contentType == 'Vimeo') {
        var linkStuff = sectionContent.find('.link-stuff');
        if(linkStuff.length != 0){
            stuffDP.append('<div class="hide link-stuff">' + linkStuff.html() + '</div>');
            linkStuff.remove();
        }
    }
    if(contentType ==  'quiz') {
        var linkStuff = sectionContent.find('.link-stuff');
        if(linkStuff.length != 0){
            stuffDP.append('<div class="hide link-stuff">' + linkStuff.html() + '</div>');
            linkStuff.remove();
        }
    }
    stuffDPEventHandler(stuffDP);
    return stuffDP;
}
function getStuffImageByType(contentType) {
    if(contentType == 'doc')
        return '<i class="fa fa-file-text-o"></i> Word Document';
    else if(contentType == 'ppt')
        return '<i class="fa fa-desktop"></i> Presentation';
    else if(contentType == 'text')
        return '<i class="fa fa-book"></i> Text';
    else if(contentType == 'dwn')
        return '<i class="fa fa-download"></i> Downloadable';
    else if(contentType == 'video')
        return '<i class="fa fa-play-circle-o"></i> Video Link';
    else if(contentType == 'Youtube')
        return '<i class="fa fa-youtube"></i> Youtube Link';
    else if(contentType == 'Vimeo')
        return '<i class="fa fa-vimeo"></i> Vimeo Link';
    else if(contentType == 'quiz')
        return '<i class="fa fa-question-circle"></i> Quiz';
}
function stuffDPEventHandler(elem) {
    elem.find(".d-checkbox").bootstrapSwitch();
    elem.find('.edit-stuff').on('click',function(e) {
        e.preventDefault();
        var parent = $(this).parents('.section-content');
        var type = parent.attr('data-content-type');
        var fileName = parent.attr('data-file-name');
        var dataStuff = parent.attr('data-stuff');
        switch(type) {
            case 'text':
                $('#modalAddEditContent .btn-content-type[data-action="text"]').trigger("click");                
                CKEDITOR.instances['textEditor'].setData(fileName);
                break;
            case 'Youtube':
                $('#modalAddEditContent .btn-content-type[data-action="youtube"]').trigger("click");
                $('#inputYoutube').val(fileName);
                break;
            case 'Vimeo':
                $('#modalAddEditContent .btn-content-type[data-action="vimeo"]').trigger("click");
                $('#inputVimeo').val(fileName);
                break;
            case 'video':
                $('#modalAddEditContent .btn-content-type[data-action="video"]').trigger("click");
                break;
            case 'ppt':
                $('#modalAddEditContent .btn-content-type[data-action="presentation"]').trigger("click");
                break;
            case 'doc':
                $('#modalAddEditContent .btn-content-type[data-action="document"]').trigger("click");
                break;
            case 'dwn':
                $('#modalAddEditContent .btn-content-type[data-action="downloadable"]').trigger("click");
                break;
            case 'quiz':
                $('#modalAddEditContent .btn-content-type[data-action="quiz"]').trigger("click");
                quizJSON = $.parseJSON(dataStuff);
                arrangeQuizQuestions();
            case 'survey':
                $('#modalAddEditContent .btn-content-type[data-action="survey"]').trigger("click");
                var surveyStuff = $.parseJSON(dataStuff);
                arrangeSurveyStuff(surveyStuff);
                break;
        }
    });
    
    elem.find('.change-published').on('click',function() {
        var thiz = $(this);
        var req = {};
        req.subjectId = subjectId;
        req.courseId = courseId;
        if($(this).attr('data-published') == '0') {
            req.status = 1;
            bootbox.dialog({
                message: "Do you want to mail all students?",
                title: "Content Published Notification",
                buttons: {
                  success: {
                    label: "Yes",
                    className: "green",
                    callback: function() {
                        req.mail = 1;
                        updatePublishedStatus(req, thiz);
                    }
                  },
                  danger: {
                    label: "No",
                    className: "red",
                    callback: function() {
                        req.mail = 0;
                        updatePublishedStatus(req, thiz);
                    }
                  },
                  main: {
                    label: "Cancel",
                    className: "blue",
                    callback: function() {
                      
                    }
                  }
                }
            });
        }else{
            req.status = 0;
            updatePublishedStatus(req, thiz);
        }
    });
    
    elem.find('.change-downloadable').on('click',function() {
        var req = {};
        if($(this).attr('data-downloadable') == '0') {
            req.status = 1;
        } else {
            req.status = 0;
        }
        updateDownloadableStatus(req, $(this));
    });
    
    /*elem.find('.add-supp-stuff').on('click', function() {
        var parent = $(this).parents('.section-content');
        $(this).hide();
        var fileUploader = createFileUploader();
        fileUploader.addClass('supp-files');
        fileUploader.find('.content-id').val(parent.attr('data-conid'));
        fileUploader.find('.file-type').html('Supplementary');
        fileUploader.find('.stuff-msg').html('Use any file no larger than 1.0 GiB.');
        fileUploader.find('.stuff-desc').html('Tip: This is a supplementary material');
        fileUploader.find('.content-type').val('dwn');
        fileUploader.find('input[type="file"]').attr('name','uploaded-supplementary-stuff');
        parent.append(fileUploader);
    });*/
    
    elem.find('.add-stuff-desc .btn').on('click',function() {
        var stuffDescriptionWidget = createStuffDescriptionWidget(elem.parents('.section-content'));
        elem.find('.supp-section').before(stuffDescriptionWidget);
        FormEditable.init();
        elem.find('.description').trigger('click');
        // var editorId = stuffDescriptionWidget.find('textarea').attr('id');
        // CKEDITOR.replace( editorId, {
            // extraPlugins: 'image',
            // height: 100
        // });
        $(this).parents('.add-stuff-desc').remove();
    });
    
    //elem.find('.change-downloadable').bootstrapSwitch('size', 'mini');
    elem.find('.change-downloadable').on('switchChange.bootstrapSwitch', function(event, state) {
        // console.log(this); // DOM element
        // console.log(event); // jQuery event
        // console.log(state); // true | false
        var req = {};
        if(state == true) {
            req.status = 1;
        } else {
            req.status = 0;
        }
        updateDownloadableStatus(req, $(this));
    });

    elem.find('.delete-stuff').on('click', function(e) {
        e.preventDefault();
        var req = {};
        if(confirm("Are you sure to delete ? ")){
            deleteContentStuff(req, $(this));
        }
        
    });
}
function createStuffDescriptionWidget (elem) {
    var html = '<div class="list-additional">'+
                    '<h4>Content Description :</h4>'+
                    '<a href="javascript:;" class="description" data-type="textarea" data-pk="1" data-placeholder="Your description here..." data-original-title="Enter description"></a>'+
                    /*'<div class="js-download-text cursor-pointer">'+
                        '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo aspernatur, perferendis, facere placeat distinctio nam repellendus eos voluptates similique a incidunt quo hic obcaecati ipsum vitae error cupiditate harum in!</p>'+*/
                    /*'</div>'+*/
                    /*'<div class="js-download-update">'+
                        '<div class="form-group">'+
                            '<textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>'+
                        '</div>'+
                        '<div class="form-group">'+
                            '<button class="btn green save-stuff-desc">Update</button>'+
                            '<button class="btn red">Cancel</button>'+
                        '</div>'+
                    '</div>'+*/
                '</div>';
    var widget = $(html);
    var conid = elem.attr('data-conid');
    var id = 'desc-editor-' + conid;
    widget.find('.description').attr('id', id);
    //widget.find('textarea').attr('id', id);
    return widget;
}
function createStuffDescDisplay(html) {
    console.log(html);
    /*var html = '<div class="stuff-description-display">'
                + '<div><b>Stuff Description</b></div><br/>'
                + html
                + '</div>';
    var elem = $(html);
    return elem;*/
}
function updateContentOrderOnServer(order) {
    var req = {};
    req.action = 'update-content-order';
    var res;
    req.order = order;
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status != 1) {
            toastr.error('Some error occured! Whatever you did was not succesfull');
        }
    });
} 
function createSupplementaryStuffWidget(elem, suppFiles) {
    var html = '<div class="list-additional supplementary-stuff-widget">'+
                '<h4>Supplementary files :</h4>'+
                '<ul class="list-group">';
    $.each(suppFiles, function(i,v) {
        html += createSuppFileDP(v);
    });
    html = html + '</ul>'+
                '</div>';
    var widget = $(html);
    var conid = elem.attr('data-conid');
    
    supplementaryStuffWidgetEventHandlers(widget);
    return widget;
}
function createSuppFileDP(file) {
    var html = '<div class="supp-file clearfix" data-supp-file="' + file.id + '">'
                +'<span class="supp-file-name">' + file.fileRealName + '</span>'
                +'<a href="#" class="delete-supp-file pull-right"><i class="fa fa-trash-o"></i></a>'
                + '</div>';
    var html = '<li class="list-group-item supp-file" data-supp-file="' + file.id + '">'+
                    '<div class="row">'+
                        '<div class="col-md-8 text-overflow">' + file.fileRealName + '</div>'+
                        '<div class="col-md-4 text-right">'+
                            /*'<a href="javascript:;" class="btn btn-info btn-sm"><i class="fa fa-download"></i></a>'+*/
                            '<a href="javascript:;" class="btn btn-danger btn-sm delete-supp-file"><i class="fa fa-trash"></i></a>'+
                        '</div>'+
                    '</div>'+
                '</li>';
    return html;
}
function supplementaryStuffWidgetEventHandlers(elem) {
    elem.find('.delete-supp-file').on('click', function(e) {
        e.preventDefault();
        var req = {};
        req.suppFileId = $(this).parents('.supp-file').attr('data-supp-file');
        deleteSuppFileFromDB(req, $(this).parents('.supp-file'));
    });
}
function updatePublishedStatus(req, elem) {
    req.action = 'update-published-status';
    var res;
    var conid = elem.parents('.section-content').attr('data-conid');
    req.conid = conid;
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1){
            elem.attr('data-published', req.status);
            elem.html(req.status == 1 ? '<i class="fa fa-cloud-upload"></i> UnPublish' : '<i class="fa fa-cloud-upload"></i> Publish');
            if(req.status == 1) {
                elem.removeClass('btn-warning').addClass('green-jungle');
                elem.parents('.section-content').find('.dd-handle').removeClass('bg-yellow bg-font-yellow').addClass('bg-green-jungle bg-font-green-jungle');

                elem.parents('.stuff-dp').find('.downloadable-holder').removeClass('hide');
            }
            else {
                elem.removeClass('green-jungle').addClass('btn-warning');
                elem.parents('.section-content').find('.dd-handle').removeClass('bg-green-jungle bg-font-green-jungle').addClass('bg-yellow bg-font-yellow');

                elem.parents('.stuff-dp').find('.downloadable-holder').addClass('hide');
            }
        }else
            toastr.error(res.message);
    });
}    
function updateDownloadableStatus(req, elem) {
    req.action = 'update-downloadable-status';
    var res;
    var conid = elem.parents('.section-content').attr('data-conid');
    req.conid = conid;
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1) {
            elem.attr('data-downloadable', req.status);
            elem.text(req.status == 1 ? 'Remove Downloadable' : 'Make Downloadable');
        }else
            toastr.error(res.message);
    });
}
var sectionTitleValidation = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form3 = $('#formSectionTitle');
    var error3 = $('.alert-danger', form3);
    var success3 = $('.alert-success', form3);

    form3.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input
        rules: {
            sectionName: {
                minlength: 2,
                required: true
            }
        },

        messages: { // custom messages for radio buttons and checkboxes
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.parent(".input-group").size() > 0) {
                error.insertAfter(element.parent(".input-group"));
            } else if (element.attr("data-error-container")) { 
                error.appendTo(element.attr("data-error-container"));
            } else if (element.parents('.radio-list').size() > 0) { 
                error.appendTo(element.parents('.radio-list').attr("data-error-container"));
            } else if (element.parents('.radio-inline').size() > 0) { 
                error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
            } else if (element.parents('.checkbox-list').size() > 0) {
                error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
            } else if (element.parents('.checkbox-inline').size() > 0) { 
                error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success3.hide();
            error3.show();
            App.scrollTo(error3, -200);
        },

        highlight: function (element) { // hightlight error inputs
           $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function (form) {
            success3.show();
            error3.hide();
            //form[0].submit(); // submit the form
            var title = $('#sectionName').val();
            var req = {};
            req.subjectId = subjectId;
            req.title = title;
            req.headingId = $('#sectionHeadingId').val();
            req.demo = 0;
            saveHeadingToDB(req, $('[data-hid="'+req.headingId+'"].section-heading'));
        }

    });
}
var contentTitleValidation = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form3 = $('#formContentTitle');
    var error3 = $('.alert-danger', form3);
    var success3 = $('.alert-success', form3);

    form3.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input
        rules: {
            lectureName: {
                minlength: 2,
                required: true
            }
        },

        messages: { // custom messages for radio buttons and checkboxes
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.parent(".input-group").size() > 0) {
                error.insertAfter(element.parent(".input-group"));
            } else if (element.attr("data-error-container")) { 
                error.appendTo(element.attr("data-error-container"));
            } else if (element.parents('.radio-list').size() > 0) { 
                error.appendTo(element.parents('.radio-list').attr("data-error-container"));
            } else if (element.parents('.radio-inline').size() > 0) { 
                error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
            } else if (element.parents('.checkbox-list').size() > 0) {
                error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
            } else if (element.parents('.checkbox-inline').size() > 0) { 
                error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success3.hide();
            error3.show();
            App.scrollTo(error3, -200);
        },

        highlight: function (element) { // hightlight error inputs
           $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function (form) {
            success3.show();
            error3.hide();
            //form[0].submit(); // submit the form
            var title = $('#lectureName').val();
            var req = {};
            req.chapterId = chapterId;
            req.demo = (($('#optionFreeDemo').prop('checked')) ? 1 : 0);
            req.headingId = $('#headingId').val();
            req.title = title;
            req.contentId = $('#contentId').val();
            saveContentToDB(req, $('[data-conid="'+req.contentId+'"].section-content'));
        }

    });
}
var formQuizQuestionValidation = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form3 = $('#formQuizQuestion');
    var error3 = $('.alert-danger', form3);
    var success3 = $('.alert-success', form3);

    form3.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input
        rules: {
            quizQuestion: {
                minlength: 10,
                required: true
            },
            quizOption1: {
                required: true
            },
            quizOption2: {
                required: true
            },
            quizOption3: {
                required: true
            },
            quizOption4: {
                required: true
            }
        },

        messages: { // custom messages for radio buttons and checkboxes
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.parent(".input-group").size() > 0) {
                error.insertAfter(element.parent(".input-group"));
            } else if (element.attr("data-error-container")) { 
                error.appendTo(element.attr("data-error-container"));
            } else if (element.parents('.radio-list').size() > 0) { 
                error.appendTo(element.parents('.radio-list').attr("data-error-container"));
            } else if (element.parents('.radio-inline').size() > 0) { 
                error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
            } else if (element.parents('.checkbox-list').size() > 0) {
                error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
            } else if (element.parents('.checkbox-inline').size() > 0) { 
                error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success3.hide();
            error3.show();
            App.scrollTo(error3, -200);
        },

        highlight: function (element) { // hightlight error inputs
           $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function (form) {
            success3.show();
            error3.hide();
            saveQuizQuestion();
        }

    });
}
function saveHeadingToDB(req, elem) {
    var res;
    req.action = "create-chapter";
    if(req.headingId != 0) {
        req.action = 'update-chapter-details';
        req.chapterId = req.headingId;
        req.chapterName = req.title;
    }
    req.courseId = courseId;
    req.subjectId = subjectId;
    req.chapterName = req.title;
    //req.chapterId = getUrlParameter('chapterId');
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1){
            $('#modalAddEditSection').modal('hide');
            if(req.headingId == 0){
                var heading = {};
                heading.id=res.chapterId;
                heading.name=req.title;
                addNewHeading(heading);
                toastr.success("New section successfully added!");
                //$('[data-hid="'+res.chapterId+'"].section-heading>.dd-handle').find('.btn-content-title').trigger('click');
            } else {
                elem.attr('data-hid', res.chapterId);
                elem.find('.section-title').html(req.title);
            }
        }
        else
            toastr.error(res.message);
    });
}    
function saveContentToDB(req, elem) {
    var res;
    req.action = "save-content";
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1) {
            $('#modalAddEditContentTitle').modal('hide');
            if(req.contentId == 0){
                var content = {};
                content.id=res.contentId;
                content.title=req.title;
                content.demo=req.demo;
                addNewContent(content,$('[data-hid='+req.headingId+'].section-heading'));
                $('[data-conid="'+res.contentId+'"].section-content').find('.btn-content').trigger('click');
            } else {
                elem.find('.content-title').html(req.title);
            }
        }
        else
            toastr.error(res.message);
    });
}
function saveLinkToDB(req, elem) {
    var res;
    req.action = "save-link-stuff";
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1) {
            var sectionContent = elem;
            var contentType = req.contentType;
            if(!sectionContent.hasClass('has-file'))
                sectionContent.addClass('has-file');
            sectionContent.attr('data-content-type', contentType);
            sectionContent.attr('data-file-name', req.stuff);
            sectionContent.attr('data-metadata', '');
            sectionContent.find('.link-stuff').remove();
            sectionContent.find('.collapse').remove();
            sectionContent.append('<div class="hide link-stuff">' + req.stuff + '</div>');
            sectionContent.find('.dd-handle').removeClass('bg-yellow-lemon bg-font-yellow-lemon').addClass('bg-yellow bg-font-yellow');
            sectionContent.find('.pull-right').html('<a class="collapsible bg-font-green-jungle no-drag" role="button" data-toggle="collapse" href="#ddItem'+req.contentId+'" aria-expanded="false" aria-controls="ddItem'+req.contentId+'"><i class="fa fa-toggle-down"></i></a>');
            sectionContent.append('<div class="collapse in" id="ddItem' + req.contentId + '"></div>');
            var stuffDp = createStuffDP(sectionContent);
            sectionContent.find('.collapse').append(stuffDp);
        }else
            toastr.error(res.message);
    });
}
function saveTextStuffToDB(req, elem) {
    var res;
    req.action = "save-text-stuff";
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1) {
            var sectionContent = elem;
            var contentType = 'text';
            if(!sectionContent.hasClass('has-file'))
                sectionContent.addClass('has-file');
            sectionContent.attr('data-content-type', contentType);
            sectionContent.attr('data-file-name', req.stuff);
            sectionContent.attr('data-metadata', '');
            sectionContent.find('.text-stuff').remove();
            sectionContent.find('.collapse').remove();
            sectionContent.append('<div class="hide text-stuff">' + req.stuff + '</div>');
            sectionContent.find('.dd-handle').removeClass('bg-yellow-lemon bg-font-yellow-lemon').addClass('bg-yellow bg-font-yellow');
            sectionContent.find('.pull-right').html('<a class="collapsible bg-font-green-jungle no-drag" role="button" data-toggle="collapse" href="#ddItem'+req.contentId+'" aria-expanded="false" aria-controls="ddItem'+req.contentId+'"><i class="fa fa-toggle-down"></i></a>');
            sectionContent.append('<div class="collapse in" id="ddItem' + req.contentId + '"></div>');
            var stuffDp = createStuffDP(sectionContent);
            sectionContent.find('.collapse').append(stuffDp);
        }else
            toastr.error(res.message);
    });
}
function saveQuizStuffToDB(req, elem) {
    var res;
    req.action = "save-quiz-stuff";
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1) {
            var sectionContent = elem;
            var contentType = 'quiz';
            if(!sectionContent.hasClass('has-file'))
                sectionContent.addClass('has-file');
            sectionContent.attr('data-content-type', contentType);
            sectionContent.attr('data-file-name', req.title);
            sectionContent.attr('data-stuff', req.stuff);
            sectionContent.attr('data-metadata', '');
            sectionContent.find('.text-stuff').remove();
            sectionContent.find('.collapse').remove();
            sectionContent.append('<div class="hide text-stuff">' + req.description + '</div>');
            sectionContent.find('.dd-handle').removeClass('bg-yellow-lemon bg-font-yellow-lemon').addClass('bg-yellow bg-font-yellow');
            sectionContent.find('.pull-right').html('<a class="collapsible bg-font-green-jungle no-drag" role="button" data-toggle="collapse" href="#ddItem'+req.contentId+'" aria-expanded="false" aria-controls="ddItem'+req.contentId+'"><i class="fa fa-toggle-down"></i></a>');
            sectionContent.append('<div class="collapse in" id="ddItem' + req.contentId + '"></div>');
            var stuffDp = createStuffDP(sectionContent);
            sectionContent.find('.collapse').append(stuffDp);
        }else
            toastr.error(res.message);
    });
}
function saveSurveyStuffToDB(req, elem) {
    var res;
    req.action = "save-survey-stuff";
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1) {
            var sectionContent = elem;
            var contentType = 'survey';
            if(!sectionContent.hasClass('has-file'))
                sectionContent.addClass('has-file');
            sectionContent.attr('data-content-type', contentType);
            sectionContent.attr('data-file-name', req.title);
            sectionContent.attr('data-stuff', req.stuff);
            sectionContent.attr('data-metadata', '');
            sectionContent.find('.text-stuff').remove();
            sectionContent.find('.collapse').remove();
            sectionContent.append('<div class="hide text-stuff">' + req.description + '</div>');
            sectionContent.find('.dd-handle').removeClass('bg-yellow-lemon bg-font-yellow-lemon').addClass('bg-yellow bg-font-yellow');
            sectionContent.find('.pull-right').html('<a class="collapsible bg-font-green-jungle no-drag" role="button" data-toggle="collapse" href="#ddItem'+req.contentId+'" aria-expanded="false" aria-controls="ddItem'+req.contentId+'"><i class="fa fa-toggle-down"></i></a>');
            sectionContent.append('<div class="collapse in" id="ddItem' + req.contentId + '"></div>');
            var stuffDp = createStuffDP(sectionContent);
            sectionContent.find('.collapse').append(stuffDp);
        }else
            toastr.error(res.message);
    });
}
function deleteContentStuff(req, elem) {
    req.action = 'delete-content-stuff';
    var res;
    var conid = elem.parents('.section-content').attr('data-conid');
    req.contentId = conid;
    //elem.parents('.stuff-dp').addClass('deleted');
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1) {
            console.log(elem);
            var sectionContent = elem.parents('.section-content');
            sectionContent.removeClass('has-file');
            sectionContent.removeAttr('data-content-type');
            sectionContent.removeAttr('data-file-name');
            sectionContent.removeAttr('data-metadata');
            sectionContent.find('.link-stuff').remove();
            sectionContent.find('.collapse').remove();
            sectionContent.find('.dd-handle').removeClass('bg-yellow bg-font-yellow').addClass('bg-yellow-lemon bg-font-yellow-lemon');
            sectionContent.find('.pull-right').html('<button class="btn btn-warning btn-xs btn-content no-drag" data-toggle="modal" data-action="new"><i class="fa fa-plus"></i> Add Content</button>');

            reNumberEverything(false);
        }else
            toastr.error(res.message);
    });
    
    // Todo: Delete Files from structure
}
function deleteSuppFileFromDB(req, elem) {
    req.action = 'delete-supp-stuff';
    var res;
    var conid = elem.parents('.section-content').attr('data-conid');
    req.contentId = conid;
    //elem.parents('.stuff-dp').addClass('deleted');
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1) {
            var widget = elem.parents('.supplementary-stuff-widget');
            //console.log(widget.find('.sup'));
            if(widget.find('.supp-file').length == 1){
                widget.remove();
            }else{
                elem.remove();
            }
            reNumberEverything(false);
        }else
            toastr.error(res.message);
    });
    
    // Todo: Delete Files from structure
}
function deleteContentFromDB(content) {
    var conid = content.attr('data-conid');

    var req = {};
    var res;
    req.conid = conid;
    req.action = "delete-content";
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1) {
            content.remove();
            $('#modalAddEditContentTitle').modal('hide');
            reNumberEverything(false);
        }else
            toastr.error(res.message);
    });
}
function deleteHeadingFromDB(heading) {
    var hid = heading.attr('data-hid');

    var req = {};
    var res;
    req.hid = hid;
    req.chapterId = hid;
    req.action = "delete-chapter";
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1) {
            heading.remove();
            $('#modalAddEditSection').modal('hide');
            reNumberEverything(false);
        }
        else {
            toastr.error(res.message);
        }
    });
}
function saveQuizQuestion() {
    var question = {};
    question['q'] = $("#quizQuestion").val();
    var selectedAnswers = $(".quiz-answer:checked").length;
    if (selectedAnswers<1) {
        toastr.error("Please select at least one answer!");
    } else {
        question['a'] = {};
        question['a'][0] = {"option": $("#quizOption1").val(), "correct": $("#quizAnswer1").is(":checked")};
        question['a'][1] = {"option": $("#quizOption2").val(), "correct": $("#quizAnswer2").is(":checked")};
        question['a'][2] = {"option": $("#quizOption3").val(), "correct": $("#quizAnswer3").is(":checked")};
        question['a'][3] = {"option": $("#quizOption4").val(), "correct": $("#quizAnswer4").is(":checked")};
        question['select_any'] = ((selectedAnswers>1)?true:false);
        question['correct'] =  "<p>Your answer is correct.</p>";
        question['incorrect'] =  "<p>Your answer is incorrect.</p>";
        if ($(".js-update-quiz").hasClass('hide')) {
            quizJSON['questions'].push(question);
        } else {
            var accessKey = $("#updateQuizQ").attr('data-key');
            if (accessKey != '') {
                quizJSON['questions'][accessKey] = question;
            }
            resetQuizQuestion();
        }
        $("#formQuizQuestion")[0].reset();
        arrangeQuizQuestions();
        $(".js-quiz-q").focus();
        //console.log(quizJSON);
    }
}
function arrangeQuizQuestions() {
    //console.log(quizJSON);
    $("#quizTitle").val(quizJSON.info.name);
    $("#quizDescription").val(quizJSON.info.main);
    if (quizJSON.questions.length>0) {
        $(".js-quiz-q").html('<table class="table table-bordered table-condensed">'+
                                '<thead>'+
                                    '<tr>'+
                                        '<th width="75" class="text-center">Sr</th>'+
                                        '<th>Question</th>'+
                                        '<th width="75" class="text-center">Edit</th>'+
                                        '<th width="75" class="text-center">Delete</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody></tbody>'+
                            '</table>');
        var quizQuestions = quizJSON.questions;
        for(i=0;i<quizQuestions.length;i++) {
            $(".js-quiz-q tbody").append('<tr data-key="'+i+'">'+
                                            '<td class="text-center">'+(i+1)+'</td>'+
                                            '<td>'+quizQuestions[i].q+'</td>'+
                                            '<td class="text-center"><a href="javascript:;" class="btn btn-xs btn-edit"><i class="fa fa-edit"></i></a></td>'+
                                            '<td class="text-center"><a href="javascript:;" class="btn btn-xs btn-delete"><i class="fa fa-times"></i></a></td>'+
                                        '</tr>');
        }
    }
}
function resetQuizQuestion() {
    $("#cancelQuizQ").attr('data-key', '');
    $("#updateQuizQ").attr('data-key', '');
    $('.js-update-quiz').addClass('hide');
    $('.js-add-quiz').removeClass('hide');
    $('#quizQuestion').val("");
    $('#quizOption1').val("");
    $('#quizAnswer1').prop('checked', false);
    $('#quizOption2').val("");
    $('#quizAnswer2').prop('checked', false);
    $('#quizOption3').val("");
    $('#quizAnswer3').prop('checked', false);
    $('#quizOption4').val("");
    $('#quizAnswer4').prop('checked', false);
}
function arrangeSurveyStuff(surveyStuff) {
    //console.log(quizJSON);
    $("input[name=optionSurvey]").prop('checked', false);
    $("input[name=optionSurvey][value=" + surveyStuff.survey_type + "]").prop('checked', true);
    $("input[name=optionSurvey][value=" + surveyStuff.survey_type + "]").trigger('click');
    $("#surveyId").val(surveyStuff.survey_id);
    
}
$(document).ready(function() {    
    //UINestable.init();
    //FormEditable.init();
    sectionTitleValidation();
    contentTitleValidation();
    formQuizQuestionValidation();
    $('.option-survey').click(function(event) {
        /* Act on the event */
        if ($(this).val() == "google") {
            $(".js-gf-id").removeClass('hide');
            $(".js-sm-id").addClass('hide');
        } else {
            $(".js-gf-id").addClass('hide');
            $(".js-sm-id").removeClass('hide');
        }
    });
    $('.js-quiz-q').on('click', '.btn-edit', function(event) {
        event.preventDefault();
        /* Act on the event */
        var accessKey = $(this).closest('tr').attr('data-key');
        //console.log(quizJSON.questions[accessKey]);
        $('#quizQuestion').val(quizJSON.questions[accessKey].q);
        $('#quizOption1').val(quizJSON.questions[accessKey]['a'][0].option);
        $('#quizAnswer1').prop('checked', quizJSON.questions[accessKey]['a'][0].correct);
        $('#quizOption2').val(quizJSON.questions[accessKey]['a'][1].option);
        $('#quizAnswer2').prop('checked', quizJSON.questions[accessKey]['a'][1].correct);
        $('#quizOption3').val(quizJSON.questions[accessKey]['a'][2].option);
        $('#quizAnswer3').prop('checked', quizJSON.questions[accessKey]['a'][2].correct);
        $('#quizOption4').val(quizJSON.questions[accessKey]['a'][3].option);
        $('#quizAnswer4').prop('checked', quizJSON.questions[accessKey]['a'][3].correct);
        $('.js-add-quiz').addClass('hide');
        $('.js-update-quiz').removeClass('hide');
        $("#updateQuizQ").attr('data-key', accessKey);
        $("#cancelQuizQ").attr('data-key', accessKey);
    });
    $('.js-quiz-q').on('click', '.btn-delete', function(event) {
        event.preventDefault();
        /* Act on the event */
        var accessKey = $(this).closest('tr').attr('data-key');
        quizJSON.questions.splice(accessKey,1);
        console.log(quizJSON.questions);
    });
    $("#cancelQuizQ").click(function(event) {
        /* Act on the event */
        resetQuizQuestion();
    });
    $('#content').on('click', '.btn-preview', function(){
        var chapterId = $(this).parents('.section-heading').attr('data-hid');
        var contentId = $(this).parents('.section-content').attr('data-conid');
        window.location = sitepathManageContent+'subjects/'+subjectId+'/chapters/'+chapterId+'/content/'+contentId;
    });
    $('#btnSaveSectionTitle').on('click', function(e) {
        e.preventDefault();
        $('#formSectionTitle').submit();
    });
    $('#btnSaveHeading').on('click', function(e) {
        e.preventDefault();
        $('#formContentTitle').submit();
    });
    $('#btnSaveContent').on('click', function() {
        var contentType = $('.btn-content-type:not(.btn-outline)').attr('data-action');
        var errContainer = $('#formContent').find('.alert-danger');
        var sucContainer = $('#formContent').find('.alert-success');
        $('#formContent').find('.ajax-error').addClass('hide');
        $('#formContent').find('.alert').addClass('hide');
        var req = {};
        req.contentId = $('#contentContentId').val();
        if (contentType == 'youtube') {
            var inputYoutube = $('#inputYoutube').val();
            if (!inputYoutube) {
                errContainer.html('Please enter youtube URL').removeClass('hide');
                $('#inputYoutube').focus();
            } else if (!validateYoutubeUrl(inputYoutube)) {
                errContainer.html('Please enter valid youtube URL').removeClass('hide');
                $('#inputYoutube').focus();
            } else {
                req.stuff = inputYoutube;
                req.contentType = 'Youtube';
                saveLinkToDB(req, $('[data-conid="'+req.contentId+'"].section-content'));
                $('#modalAddEditContent').modal("hide");
            }
        } else if (contentType == 'vimeo') {
            var inputVimeo = $('#inputVimeo').val();
            if (!inputVimeo) {
                errContainer.html('Please enter vimeo URL').removeClass('hide');
                $('#inputVimeo').focus();
            } else if (!validateVimeoUrl(inputVimeo)) {
                errContainer.html('Please enter valid vimeo URL').removeClass('hide');
                $('#inputVimeo').focus();
            } else {
                req.stuff = inputVimeo;
                req.contentType = 'Vimeo';
                saveLinkToDB(req, $('[data-conid="'+req.contentId+'"].section-content'));
                $('#modalAddEditContent').modal("hide");
            }
        } else if (contentType == 'video') {

        } else if (contentType == 'text') {
            var textEditor = $.trim(CKEDITOR.instances['textEditor'].getData());
            if (!textEditor) {
                errContainer.html('Please enter some text').removeClass('hide');
            } else {
                req.stuff = textEditor;
                req.contentType = 'text';
                saveTextStuffToDB(req, $('[data-conid="'+req.contentId+'"].section-content'));
                $('#modalAddEditContent').modal("hide");
            }
        } else if (contentType == 'presentation') {

        } else if (contentType == 'document') {

        } else if (contentType == 'downloadable') {

        } else if (contentType == 'quiz') {
            var quizTitle = $('#quizTitle').val();
            var quizDescription = $('#quizDescription').val();
            if (!quizTitle) {
                errContainer.html('Please enter quiz title').removeClass('hide');
                $("#modalAddEditContent").scrollTop(0);
            } else if (!quizDescription) {
                errContainer.html('Please enter quiz description').removeClass('hide');
                $("#modalAddEditContent").scrollTop(0);
            } else if (quizJSON.questions.length<1) {
                errContainer.html('Please enter atleast one question').removeClass('hide');
                $("#modalAddEditContent").scrollTop(0);
            } else {
                quizJSON.info.name = quizTitle;
                quizJSON.info.main = quizDescription;
                //console.log(quizJSON);
                req.stuff = JSON.stringify(quizJSON);
                //console.log(req.stuff);
                req.title = quizTitle;
                req.contentType = 'quiz';
                saveQuizStuffToDB(req, $('[data-conid="'+req.contentId+'"].section-content'));
                $('#modalAddEditContent').modal("hide");
            }
        } else if (contentType == 'survey') {
            var optionSurvey = $('input[name=optionSurvey]:checked').val();
            var surveyId = $('#surveyId').val();
            if (!optionSurvey) {
                errContainer.html('Please select a survey type').removeClass('hide');
                $("#modalAddEditContent").scrollTop(0);
            } else if (optionSurvey == 'google' && !surveyId) {
                errContainer.html('Please enter Google form ID').removeClass('hide');
                $("#modalAddEditContent").scrollTop(0);
            } else if (optionSurvey == 'surveymonkey' && !surveyId) {
                errContainer.html('Please enter SurveyMonkey survey ID').removeClass('hide');
                $("#modalAddEditContent").scrollTop(0);
            } else {
                var surveyStuff = {};
                surveyStuff.survey_type = optionSurvey;
                surveyStuff.survey_id = surveyId;
                //console.log(surveyStuff);
                req.stuff = JSON.stringify(surveyStuff);
                //console.log(req.stuff);
                req.title = ((optionSurvey=='google')?'Google Form':'SurveyMonkey Survey');
                req.contentType = 'survey';
                saveSurveyStuffToDB(req, $('[data-conid="'+req.contentId+'"].section-content'));
                $('#modalAddEditContent').modal("hide");
            }
        } else {
            toastr.error("There was some error!");
        }
        return false;
        /*$('.help-block').remove();
        var req = {};
        req.stuff = elem.find('input[type="text"]').val();
        req.contentType = $(this).parents('.stuff-link-widget').find('.link-type').html();
        var matches='';
        if(req.contentType=='Youtube'){
           matches= req.stuff.match(/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11,})(?:\S+)?$/);
        }
        if (req.contentType == 'Vimeo') {
            matches = req.stuff.match(/\/\/(?:www\.)?vimeo.com\/([0-9a-z\-_]+)/i);
        }
        if (matches) {
            // toastr.error('valid');
            req.contentId = $(this).parents('.section-content').attr('data-conid');
            saveLinkToDB(req, $(this).parents('.section-content'));

        } else {
            // toastr.error('Invalid');
            $(this).parent().prev().find('input[type=text]').after('<span class="help-block text-danger">Enter a valid url </span>');
            return false;
        }*/
    });
    $('#btnDeleteSection').on('click', function(e) {
        e.preventDefault();
        var headingId = $('#sectionHeadingId').val();
        //console.log(headingId);
        if($('.section-heading[data-hid="'+headingId+'"]').find('.section-content').length > 0){
            toastr.error('The heading has content. Delete them first.');
            return;
        }
        var msg = "Are you sure you want to delete this?";
        if(!confirm(msg))
            return;
        deleteHeadingFromDB($('.section-heading[data-hid="'+headingId+'"]'));
        headingCount--;
        reNumberEverything(false);
        return false;
    });
    $('#btnDeleteHeading').on('click', function(e) {
        e.preventDefault();
        var contentId = $('#contentId').val();
        if($('.section-content[data-conid="'+contentId+'"]').hasClass('has-file')){
            toastr.error('A file is attached to this content. You will have to delete that first');
            return;
        }
        var msg = "Are you sure you want to delete this?";
        if(!confirm(msg))
            return;
        // Delete from DB
        deleteContentFromDB($('.section-content[data-conid="'+contentId+'"]'))
        //$(this).parents('.section-content').remove();
        reNumberEverything(false);
        return false;
    });
    $('#content').on('click','[data-toggle="modal"].btn-section-title',function(e){
        e.preventDefault();
        var thiz   = $(this);
        var action = thiz.attr("data-action");
        $('#sectionHeadingId').val('');
        var headingId = thiz.parents('.section-heading').attr('data-hid');
        if (!headingId) {
            headingId = 0;
        }
        $('#sectionHeadingId').val(headingId);
        if (action == "new") {
            $('.js-section-action').html("New");
            $('#modalAddEditSection .btn-delete').addClass('hide');
        } else {
            $('.js-section-action').html("Edit");
            var sectionName = thiz.closest(".dd-handle").find('.section-title').text();
            $("#sectionName").val(sectionName);
            $('#modalAddEditSection .btn-delete').removeClass('hide');
        }
        $('#modalAddEditSection').modal("show");
        return false;
    });
    $('#modalAddEditSection').on('hidden.bs.modal', function (e) {
        $("#sectionName").val("");
        $("#sectionHeadingId").val(0);
        $('#formSectionTitle .alert').attr('style','');
    });
    $('#content').on('click','[data-toggle="modal"].btn-content-title',function(e){
        e.preventDefault();
        var thiz   = $(this);
        var action = thiz.attr("data-action");
        $('#headingId').val('');
        var headingId = thiz.parents('.section-heading').attr('data-hid');
        var contentId = thiz.parents('.section-content').attr('data-conid');
        if (!contentId) {
            contentId = 0;
        }
        $('#headingId').val(headingId);
        $('#contentId').val(contentId);
        if (action == "new") {
            $('.js-content-title-action').html("New");
            $('#modalAddEditContentTitle .btn-delete').addClass('hide');
        } else {
            var lectureName = thiz.closest(".dd-handle").find('.content-title').text();
            $("#lectureName").val(lectureName);
            $('.js-content-title-action').html("Edit");
            $('#modalAddEditContentTitle .btn-delete').removeClass('hide');
        }
        $('#modalAddEditContentTitle').modal("show");
        return false;
    });
    $('#modalAddEditContentTitle').on('hidden.bs.modal', function (e) {
        $("#lectureName").val("");
        $("#headingId").val(0);
        $("#contentId").val(0);
        $('#optionFreeDemo').prop('checked', false);
        $('#formContentTitle .alert').attr('style','');
    });
    $('#content').on('click','[data-toggle="modal"].btn-content',function(e){
        e.preventDefault();
        var thiz   = $(this);
        var action = thiz.attr("data-action");
        $('#contentContentId').val('');
        var contentId = thiz.parents('.section-content').attr('data-conid');
        if (!contentId) {
            contentId = 0;
        }
        $('#contentContentId').val(contentId);
        if (action == "new") {
            $('.js-content-action').html("New");
        } else {
            $('.js-content-action').html("Edit");
        }
        $('#modalAddEditContent').modal("show");
        return false;
    });
    $('#modalAddEditContent').on('hidden.bs.modal', function (e) {
        $('#formContent form').trigger("reset");
        $("#contentContentId").val(0);
        $('#formContent .alert').attr('style','');
        CKEDITOR.instances['textEditor'].setData('');
        $('.btn-content-type').addClass("btn-outline");
        $('.js-content-type').addClass("hide");
        var progress = $('#formContent').find('.progress');
        progress.find('.progress-bar').width('0%'); //update progressbar percent complete
        progress.find('.sr-only').html('0%'); //update progressbar percent
        // reinitializing quiz
        quizJSON = {
            "info": {
                "name":    "",
                "main":    "",
                "results": "Result",
                "level1":  "Level1",
                "level2":  "Level2",
                "level3":  "Level3",
                "level4":  "Level4",
                "level5":  "Level5" // no comma here
            },
            "questions": []
        };
    });
    $('#content').on('click','[data-toggle="modal"].btn-supp-material',function(e){
        e.preventDefault();
        var thiz   = $(this);
        $('#suppContentId').val('');
        var contentId = thiz.parents('.section-content').attr('data-conid');
        if (!contentId) {
            contentId = 0;
        }
        $('#suppContentId').val(contentId);
        $('#modalAddSupplementaryMaterial').modal("show");
        return false;
    });
    $('#modalAddSupplementaryMaterial').on('hidden.bs.modal', function (e) {
        $('#formSupplementary').trigger("reset");
        $("#suppContentId").val(0);
        $('#formSupplementary .alert').attr('style','');
        var progress = $('#formSupplementary').find('.progress');
        progress.find('.progress-bar').width('0%'); //update progressbar percent complete
        progress.find('.sr-only').html('0%'); //update progressbar percent
    });
    $('#formVideo').attr('action', FileApiPoint).ajaxForm({
        data: {contentId:function(){return $('#contentContentId').val();},contentType:'video'},
        beforeSend: function(arr, $form, data) {
            //console.log('starting');
        },
        uploadProgress: function(event, position, total, percentComplete) {
            //Progress bar
            var progress = $('#formVideo').find('.progress')
            progress.find('.progress-bar').width(percentComplete + '%') //update progressbar percent complete
            progress.find('.sr-only').html(percentComplete + '%') //update progressbar percent complete
        },
        success: function(resp) {
            if((typeof(resp) === 'string') && resp.substr(0, 6) == '<br />') {
                toastr.error('There was some error, please refresh and try again!');
            }
        },
        complete: function(resp) {
            file = $.parseJSON(resp.responseText);
            if(file.status == 1) {
                //location.reload();
                //location.reload(true);
                //console.log(resp.responseText);
                var contentId = $('#formContent').find('#contentContentId').val();
                //console.log(contentId);
                var sectionContent = $('.section-content[data-conid="'+contentId+'"]');
                //file = 'h';
                var contentType = 'video';
                sectionContent.attr('data-content-type', contentType);
                sectionContent.attr('data-file-name', $('#formVideo .file-name').val());
                sectionContent.attr('data-metadata', '');
                sectionContent.find('.stuff-dp').remove();
                if(!sectionContent.hasClass('has-file'))
                    sectionContent.addClass('has-file');
                //sectionContent.attr('data-metadata', file.metadata);
                sectionContent.find('.collapse').remove();
                sectionContent.find('.dd-handle').removeClass('bg-yellow-lemon bg-font-yellow-lemon').addClass('bg-yellow bg-font-yellow');
                sectionContent.find('.pull-right').html('<a class="collapsible bg-font-green-jungle no-drag" role="button" data-toggle="collapse" href="#ddItem'+contentId+'" aria-expanded="false" aria-controls="ddItem'+contentId+'"><i class="fa fa-toggle-down"></i></a>');
                sectionContent.append('<div class="collapse in" id="ddItem' + contentId + '"></div>');
                var stuffDp = createStuffDP(sectionContent);
                sectionContent.find('.collapse').append(stuffDp);
                $('#modalAddEditContent').modal("hide");
            }
            else
                toastr.error('An error occured. Please refresh the page and try again. Error Details: ' + resp.message);
        },
        error: function() {
            console.log('Error');
        }
    });
    //events for demo video
    $('#formVideo .btn-upload-video').on('click', function() {
        $('#formVideo .upload-file').click();
        return false;
    });
    $('#formVideo .upload-file').on('change', function() {
        var file = this.files[0];
        var fileName = file.name;
        if(file.type =='video/mp4'){
            $('#formVideo .file-name').val(fileName);
            $('#formVideo').submit();
        }
        else{
            toastr.error("Please upload mp4 videos only");
        }
    });
    $('#formPresentation').attr('action', FileApiPoint).ajaxForm({
        data: {contentId:function(){return $('#contentContentId').val();},contentType:'ppt'},
        beforeSend: function(arr, $form, data) {
            //console.log('starting');
        },
        uploadProgress: function(event, position, total, percentComplete) {
            //Progress bar
            var progress = $('#formPresentation').find('.progress')
            progress.find('.progress-bar').width(percentComplete + '%') //update progressbar percent complete
            progress.find('.sr-only').html(percentComplete + '%') //update progressbar percent complete
        },
        success: function(resp) {
            if((typeof(resp) === 'string') && resp.substr(0, 6) == '<br />') {
                toastr.error('There was some error, please refresh and try again!');
            }
        },
        complete: function(resp) {
            file = $.parseJSON(resp.responseText);
            if(file.status == 1) {
                //location.reload();
                //location.reload(true);
                //console.log(resp.responseText);
                var contentId = $('#formContent').find('#contentContentId').val();
                //console.log(contentId);
                var sectionContent = $('.section-content[data-conid="'+contentId+'"]');
                //file = 'h';
                var contentType = 'ppt';
                sectionContent.attr('data-content-type', contentType);
                sectionContent.attr('data-file-name', $('#formPresentation .file-name').val());
                sectionContent.attr('data-metadata', '');
                sectionContent.find('.stuff-dp').remove();
                if(!sectionContent.hasClass('has-file'))
                    sectionContent.addClass('has-file');
                //sectionContent.attr('data-metadata', file.metadata);
                sectionContent.find('.collapse').remove();
                sectionContent.find('.dd-handle').removeClass('bg-yellow-lemon bg-font-yellow-lemon').addClass('bg-yellow bg-font-yellow');
                sectionContent.find('.pull-right').html('<a class="collapsible bg-font-green-jungle no-drag" role="button" data-toggle="collapse" href="#ddItem'+contentId+'" aria-expanded="false" aria-controls="ddItem'+contentId+'"><i class="fa fa-toggle-down"></i></a>');
                sectionContent.append('<div class="collapse in" id="ddItem' + contentId + '"></div>');
                var stuffDp = createStuffDP(sectionContent);
                sectionContent.find('.collapse').append(stuffDp);
                $('#modalAddEditContent').modal("hide");
            }
            else
                toastr.error('An error occured. Please refresh the page and try again. Error Details: ' + resp.message);
        },
        error: function() {
            console.log('Error');
        }
    });
    //events for demo video
    $('#formPresentation .btn-upload-presentation').on('click', function() {
        $('#formPresentation .upload-file').click();
        return false;
    });
    $('#formPresentation .upload-file').on('change', function() {
        var file = this.files[0];
        var fileName = file.name;
        if(file.type =='application/pdf'){
            $('#formPresentation .file-name').val(fileName);
            $('#formPresentation').submit();
        }
        else{
            toastr.error("Please upload PDF files only");
        }
    });
    $('#formDocument').attr('action', FileApiPoint).ajaxForm({
        data: {contentId:function(){return $('#contentContentId').val();},contentType:'doc'},
        beforeSend: function(arr, $form, data) {
            //console.log('starting');
        },
        uploadProgress: function(event, position, total, percentComplete) {
            //Progress bar
            var progress = $('#formDocument').find('.progress')
            progress.find('.progress-bar').width(percentComplete + '%') //update progressbar percent complete
            progress.find('.sr-only').html(percentComplete + '%') //update progressbar percent complete
        },
        success: function(resp) {
            if((typeof(resp) === 'string') && resp.substr(0, 6) == '<br />') {
                toastr.error('There was some error, please refresh and try again!');
            }
        },
        complete: function(resp) {
            file = $.parseJSON(resp.responseText);
            if(file.status == 1) {
                //location.reload();
                //location.reload(true);
                //console.log(resp.responseText);
                var contentId = $('#formContent').find('#contentContentId').val();
                //console.log(contentId);
                var sectionContent = $('.section-content[data-conid="'+contentId+'"]');
                //file = 'h';
                var contentType = 'doc';
                sectionContent.attr('data-content-type', contentType);
                sectionContent.attr('data-file-name', $('#documentContent .file-name').val());
                sectionContent.attr('data-metadata', '');
                sectionContent.find('.stuff-dp').remove();
                if(!sectionContent.hasClass('has-file'))
                    sectionContent.addClass('has-file');
                //sectionContent.attr('data-metadata', file.metadata);
                sectionContent.find('.collapse').remove();
                sectionContent.find('.dd-handle').removeClass('bg-yellow-lemon bg-font-yellow-lemon').addClass('bg-yellow bg-font-yellow');
                sectionContent.find('.pull-right').html('<a class="collapsible bg-font-green-jungle no-drag" role="button" data-toggle="collapse" href="#ddItem'+contentId+'" aria-expanded="false" aria-controls="ddItem'+contentId+'"><i class="fa fa-toggle-down"></i></a>');
                sectionContent.append('<div class="collapse in" id="ddItem' + contentId + '"></div>');
                var stuffDp = createStuffDP(sectionContent);
                sectionContent.find('.collapse').append(stuffDp);
                $('#modalAddEditContent').modal("hide");
            }
            else
                toastr.error('An error occured. Please refresh the page and try again. Error Details: ' + resp.message);
        },
        error: function() {
            console.log('Error');
        }
    });
    //events for demo video
    $('#formDocument .btn-upload-document').on('click', function() {
        $('#formDocument .upload-file').click();
        return false;
    });
    $('#formDocument .upload-file').on('change', function() {
        var file = this.files[0];
        var fileName = file.name;
        if(file.type =='application/pdf'){
            $('#formDocument .file-name').val(fileName);
            $('#formDocument').submit();
        }
        else{
            toastr.error("Please upload PDF files only");
        }
    });
    $('#formDownloadable').attr('action', FileApiPoint).ajaxForm({
        data: {contentId:function(){return $('#contentContentId').val();},contentType:'dwn'},
        beforeSend: function(arr, $form, data) {
            //console.log('starting');
        },
        uploadProgress: function(event, position, total, percentComplete) {
            //Progress bar
            var progress = $('#formDownloadable').find('.progress')
            progress.find('.progress-bar').width(percentComplete + '%') //update progressbar percent complete
            progress.find('.sr-only').html(percentComplete + '%') //update progressbar percent complete
        },
        success: function(resp) {
            if((typeof(resp) === 'string') && resp.substr(0, 6) == '<br />') {
                toastr.error('There was some error, please refresh and try again!');
            }
        },
        complete: function(resp) {
            file = $.parseJSON(resp.responseText);
            if(file.status == 1) {
                //location.reload();
                //location.reload(true);
                //console.log(resp.responseText);
                var contentId = $('#formContent').find('#contentContentId').val();
                //console.log(contentId);
                var sectionContent = $('.section-content[data-conid="'+contentId+'"]');
                //file = 'h';
                var contentType = 'dwn';
                sectionContent.attr('data-content-type', contentType);
                sectionContent.attr('data-file-name', $('#formDownloadable .file-name').val());
                sectionContent.attr('data-metadata', '');
                sectionContent.find('.stuff-dp').remove();
                if(!sectionContent.hasClass('has-file'))
                    sectionContent.addClass('has-file');
                //sectionContent.attr('data-metadata', file.metadata);
                sectionContent.find('.collapse').remove();
                sectionContent.find('.dd-handle').removeClass('bg-yellow-lemon bg-font-yellow-lemon').addClass('bg-yellow bg-font-yellow');
                sectionContent.find('.pull-right').html('<a class="collapsible bg-font-green-jungle no-drag" role="button" data-toggle="collapse" href="#ddItem'+contentId+'" aria-expanded="false" aria-controls="ddItem'+contentId+'"><i class="fa fa-toggle-down"></i></a>');
                sectionContent.append('<div class="collapse in" id="ddItem' + contentId + '"></div>');
                var stuffDp = createStuffDP(sectionContent);
                sectionContent.find('.collapse').append(stuffDp);
                $('#modalAddEditContent').modal("hide");
            }
            else
                toastr.error('An error occured. Please refresh the page and try again. Error Details: ' + resp.message);
        },
        error: function() {
            console.log('Error');
        }
    });
    //events for demo video
    $('#formDownloadable .btn-upload-download').on('click', function() {
        $('#formDownloadable .upload-file').click();
        return false;
    });
    $('#formDownloadable .upload-file').on('change', function() {
        var file = this.files[0];
        var fileName = file.name;
        $('#formDownloadable .file-name').val(fileName);
        $('#formDownloadable').submit();
    });
    $('#formSupplementary').attr('action', FileApiPoint).ajaxForm({
        data: {contentId:function(){return $('#suppContentId').val();},contentType:'dwn'},
        beforeSend: function(arr, $form, data) {
            //console.log('starting');
        },
        uploadProgress: function(event, position, total, percentComplete) {
            //Progress bar
            var progress = $('#formSupplementary').find('.progress')
            progress.find('.progress-bar').width(percentComplete + '%') //update progressbar percent complete
            progress.find('.sr-only').html(percentComplete + '%') //update progressbar percent complete
        },
        success: function(resp) {
            if((typeof(resp) === 'string') && resp.substr(0, 6) == '<br />') {
                toastr.error('There was some error, please refresh and try again!');
            }
        },
        complete: function(resp) {
            file = $.parseJSON(resp.responseText);
            if(file.status == 1) {
                //location.reload();
                //location.reload(true);
                console.log(resp.responseText);
                var contentId = $('#formSupplementary').find('#suppContentId').val();
                var sectionContent = $('.section-content[data-conid="'+contentId+'"]');
                if(sectionContent.find('.supplementary-stuff-widget').length == 0) {
                    var temp = [];
                    temp.push(file);
                    var supplementaryStuffWidget = createSupplementaryStuffWidget(sectionContent, temp);
                    sectionContent.find('.supp-section').before(supplementaryStuffWidget);
                } else {
                    var suppDp = $(createSuppFileDP(file));
                    suppDp.find('.delete-supp-file').on('click', function(e) {
                        e.preventDefault();
                        var req = {};
                        req.suppFileId = $(this).parents('.supp-file').attr('data-supp-file');
                        deleteSuppFileFromDB(req, $(this).parents('.supp-file'));
                    });
                    sectionContent.find('.supplementary-stuff-widget').find('.list-group').append(suppDp);
                }
                $('#modalAddSupplementaryMaterial').modal("hide");
                /*var contentId = $('#formContent').find('#contentContentId').val();
                console.log(contentId);
                var sectionContent = $('.section-content[data-conid="'+contentId+'"]');
                //file = 'h';
                var contentType = 'video';
                sectionContent.attr('data-content-type', contentType);
                sectionContent.attr('data-file-name', $('#formSupplementary .file-name').val());
                sectionContent.attr('data-metadata', '');
                sectionContent.find('.stuff-dp').remove();
                if(!sectionContent.hasClass('has-file'))
                    sectionContent.addClass('has-file');
                //sectionContent.attr('data-metadata', file.metadata);
                sectionContent.find('.collapse').remove();
                sectionContent.find('.dd-handle').removeClass('bg-yellow-lemon bg-font-yellow-lemon').addClass('bg-yellow bg-font-yellow');
                sectionContent.find('.pull-right').html('<a class="collapsible bg-font-green-jungle no-drag" role="button" data-toggle="collapse" href="#ddItem'+contentId+'" aria-expanded="false" aria-controls="ddItem'+contentId+'"><i class="fa fa-toggle-down"></i></a>');
                sectionContent.append('<div class="collapse in" id="ddItem' + contentId + '"></div>');
                var stuffDp = createStuffDP(sectionContent);
                sectionContent.find('.collapse').append(stuffDp);
                $('#modalAddSupplementaryMaterial').modal("hide");*/
            }
            else
                toastr.error('An error occured. Please refresh the page and try again. Error Details: ' + resp.message);
        },
        error: function() {
            console.log('Error');
        }
    });
    //events for demo material
    $('#formSupplementary .btn-upload-material').on('click', function() {
        $('#formSupplementary .upload-file').click();
        return false;
    });
    $('#formSupplementary .upload-file').on('change', function() {
        var file = this.files[0];
        var fileName = file.name;
        $('#formSupplementary .file-name').val(fileName);
        $('#formSupplementary').submit();
    });
    $('.btn-content-type').click(function(e){
        e.preventDefault();
        var thiz   = $(this);
        var action = thiz.attr("data-action");
        $('.btn-content-type').addClass("btn-outline");
        thiz.removeClass("btn-outline");
        if (!!action) {
            $('.js-content-type').addClass("hide");
            $('#'+action+'Content').removeClass("hide");
        };
        return false;
    });
    $("input[type=file]").on("click", function(e){
        e.stopPropagation();
    });
    //events for quiz
    /*$('#addQuizQ').on('click', function(event) {
        event.preventDefault();
    });*/
});
</script>