<script type="text/javascript">
var UserCurrency = 3;
var key_rate = 10;
var display_current_currency = '<?php echo $global->displayCurrency; ?>';
var flagForImport = false;
var importCourse;
var keys_available = 0;
var keys_pending = 0;
var TableDatatablesManaged = function () {

    var initTable1 = function () {

        var table = $('#tblCourseStudents');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // setup buttons extension: http://datatables.net/extensions/buttons/
            buttons: [
                { extend: 'print', className: 'btn dark btn-outline' },
                { extend: 'pdf', className: 'btn green btn-outline' },
                { extend: 'csv', className: 'btn purple btn-outline' }
            ],

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {  // set default column settings
                    'orderable': false,
                    'targets': [0]
                }, 
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery('#sample_1_wrapper');

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).prop("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).prop("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
        }

    };

}();
function fetchSubjects() {
    var req = {};
    var res;
    req.courseId = courseId;
    if (req.courseId) {
        req.action = "get-course-subjects";
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            //console.log(res);
            fillSubjects(res);
        });
    }
}
function fillSubjects(data) {
    var html = "";
    var editLink;
    var deletedClass = "";
    if(data.subjects.length > 0)
    {
        $.each(data.subjects, function (i, subject) {
            deletedClass = "";
            if (subject.deleted == 1) {
                deletedClass = "danger";
                deletedButton = '<a href="javascript:void(0)" class="btn btn-circle btn-sm red restore-subject"><i class="fa fa-refresh"></i></a>';
                editLink = 'javascript:;';
            } else {
                deletedButton = '<a href="javascript:void(0)" class="btn btn-circle btn-sm red delete-subject"><i class="fa fa-trash"></i></a>';
                editLink = sitepathManageSubjects+subject.id;
            }
            
            html += '<tr class="odd gradeX subject '+deletedClass+'" data-subject="'+subject.id+'">'+
                        '<td>S' + String("000" + subject.id).slice(-4) + '</td>'+
                        '<td> <a href="'+editLink+'">'+subject.name+'</a> </td>'+
                        '<td> '+subject.chapters+' </td>'+
                        '<td> '+subject.exams+' </td>'+
                        '<td> '+subject.students+' </td>'+
                        '<td>'+
                            /*'<a href="'+editLink+'" class="btn btn-circle btn-sm green-jungle"><i class="fa fa-eye"></i></a>'+
                            '<a href="javascript:;" class="btn btn-circle btn-sm green edit-subject"><i class="fa fa-edit"></i></a>'+*/deletedButton+
                        '</td>'+
                    '</tr>';
        });

        $('#jsSubjects tbody').html(html);
        addEventHandlersForSubjects();
        //deleteProfessorEventHandler();
        //makeSubjectsSortable('#jsSubjects tbody');
    } else {
        $('#jsSubjects tbody').html('<tr class="odd gradeX"><td colspan="6">No '+terminologies["subject_single"]+' added yet</td></tr>');
        $('#btnAssignSubjects').addClass("hide");
    }
}
//Event handlers
function addEventHandlersForSubjects() {
    $('#jsSubjects').on('click','.delete-subject', function (e) {
        e.preventDefault();
        var con = confirm("Are you sure you want to delete this subject?");
        if(con) {
            var subject = $(this).parents('tr');
            var sid = subject.attr('data-subject');
            var req = {};
            req.action = "delete-subject";
            req.subjectId = sid;
            $.ajax({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if (res.status == 1) {
                    toastr.success("Subject successfully deleted!");
                    subject.remove();
                    /*subject.find('.delete-subject').remove();
                    subject.find('.green').after('<a href="javascript:void(0)" class="btn btn-circle btn-sm red restore-subject"><i class="fa fa-refresh"></i></a>');
                    if ($('#jsSubjects tbody tr').length == 0) {
                        $('#jsSubjects tbody').html('<tr class="odd gradeX"><td> No '+terminologies["subject_single"]+' added yet </td></tr>');
                        $('.request-approval-link').removeClass('hide');
                        $('.market-place-link').addClass('hide');
                    }*/
                } else {
                    toastr.error(res.message);
                }
            });
        }
    });

    $('#jsSubjects').on('click','.restore-subject', function (e) {
        e.preventDefault();
        var con = confirm("Are you sure you want to restore this "+terminologies["subject_single"].toLowerCase()+"?");
        if(con) {
            var subject = $(this).parents('tr');
            var sid = subject.attr('data-subject');
            var req = {};
            req.action = "restore-subject";
            req.subjectId = sid;
            $.ajax({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if (res.status == 1) {
                    subject.find('.restore-subject').remove();
                    subject.find('.green').after('<a href="javascript:void(0)" class="btn btn-circle btn-sm red delete-subject"><i class="fa fa-trash"></i></a>');
                } else {
                    toastr.error(res.message);
                }
            });
        }
    });
}
function fetchCoursesForImport() {
    var req = {};
    var res;
    req.courseId = courseId;
    req.action = "get-courses-for-import";
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        res = $.parseJSON(res);
       // console.log(res);
       flagForImport = true;
       importCourse = res;
       fillCoursesForImport(importCourse);
    });
}
function fillCoursesForImport(data) {
    var html = "";
    var editCourseLink, editSubjectLink, addSubjectLink;
    var courseSubject = {};
    if (typeof data.courses == "undefined" ){
        $('#tableImportCourses tbody').html('<tr><td colspan="4" class="text-center">No '+terminologies["course_plural"].toLowerCase()+' to import from</td></tr>');
        return false;
    }
     if (data.courses.length == 0 ){
        $('#tableImportCourses tbody').html('<tr><td colspan="4" class="text-center">No '+terminologies["course_plural"].toLowerCase()+' to import from</td></tr>');
        return false;
    }
    $.each(data.courses, function (i, course) {
        courseSubject[course.id] = [];
        var expirydate='No Expiry';
        if(course.endDate!=""){
            expirydate=formatDate(parseInt(course.endDate));
        }
        html+= '<tr>'+
                    '<td colspan="2">'+
                        '<label class="mt-checkbox mt-checkbox-outline">'+
                            '<input type="checkbox" class="js-select-all" data-course="'+course.id+
                            '"> '+course.name+
                            '<span></span>'+
                        '</label>'+
                    '</td>'+
                    '<td>'+expirydate+'</td>'+
                    '<td>Licensed</td>'+
                '</tr>';
        $.each(course.subjects, function (i, subject) {
            courseSubject[course.id].push(subject.id);
            var importedClass = "";
            var attributes = "";
            if (subject.alreadyImported == 1) {
                importedClass = "import-tooltip";
                attributes = "checked disabled";
            }
            html+= '<tr>'+
                        '<td></td>'+
                        '<td>'+
                            '<label class="mt-checkbox mt-checkbox-outline '+importedClass+'">'+
                                '<input type="checkbox" class="course-'+course.id+
                            ' option-subject" '+attributes+' data-subject="'+subject.id+
                                '"> '+subject.name+
                                '<span></span>'+
                            '</label>'+
                        '</td>'+
                        '<td>'+expirydate+'</td>'+
                        '<td>Licensed</td>'+
                    '</tr>';
        });

    });
    //$("[data-toggle=tooltip]").tooltip({'placement': 'bottom'});
    $('body').tooltip({ selector: '.import-tooltip', placement: 'top', title: 'This '+terminologies["subject_single"].toLowerCase()+' has been already imported in this '+terminologies["course_single"].toLowerCase()+'.' });

    $('#tableImportCourses tbody').html(html);

    $('#tableImportCourses .js-select-all').on('click',function(){
        var dataCourse = $(this).attr('data-course');
        $('#tableImportCourses .course-'+dataCourse+':enabled').prop('checked', this.checked);
    });
}
function fetchPurchasedCoursesForImport() {
    var req = {};
    var res;
    req.courseId = courseId;
    req.action = "get-purchased-courses-for-import";
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        res = $.parseJSON(res);
       // console.log(res);
        fillPurchasedCoursesForImport(res);
    });
}
function fillPurchasedCoursesForImport(data) {
    
    var html = "";
    var editCourseLink, editSubjectLink, addSubjectLink;
    var courseSubject = {};
    if (typeof data.courses == "undefined" ){
        $('#tableImportPurchasedCourses tbody').html('<tr><td colspan="4" class="text-center">No '+terminologies["course_plural"].toLowerCase()+' to import from</td></tr>');
        return false;
    }
     if (data.courses.length == 0 ){
        $('#tableImportPurchasedCourses tbody').html('<tr><td colspan="4" class="text-center">No '+terminologies["course_plural"].toLowerCase()+' to import from</td></tr>');
        return false;
    }
    $.each(data.courses, function (i, course) {
        courseSubject[course.id] = [];
        var expirydate='No Expiry';
        if(course.endDate!=""){
            expirydate=formatDate(parseInt(course.endDate));
        }
        html+= '<tr>'+
                    '<td colspan="2">'+
                        '<label class="mt-checkbox mt-checkbox-outline">'+
                            '<input type="checkbox" class="js-select-all" data-course="'+course.id+
                            '"> '+course.name+
                            '<span></span>'+
                        '</label>'+
                    '</td>'+
                    '<td>'+expirydate+'</td>'+
                    '<td>Licensed</td>'+
                '</tr>';
        $.each(course.subjects, function (i, subject) {
            courseSubject[course.id].push(subject.id);
            var importedClass = "";
            var attributes = "";
            if (subject.alreadyImported == 1) {
                importedClass = "import-tooltip";
                attributes = "checked disabled";
            }
            html+= '<tr>'+
                        '<td></td>'+
                        '<td>'+
                            '<label class="mt-checkbox mt-checkbox-outline '+importedClass+'">'+
                                '<input type="checkbox" class="course-'+course.id+
                            ' option-subject" '+attributes+' data-subject="'+subject.id+
                                '"> '+subject.name+
                                '<span></span>'+
                            '</label>'+
                        '</td>'+
                        '<td>'+expirydate+'</td>'+
                        '<td>Licensed</td>'+
                    '</tr>';
        });

    });
    //$("[data-toggle=tooltip]").tooltip({'placement': 'bottom'});
    $('body').tooltip({ selector: '.import-tooltip', placement: 'top', title: 'This '+terminologies["subject_single"].toLowerCase()+' has been already imported in this '+terminologies["course_single"].toLowerCase()+'.' });

    $('#tableImportPurchasedCourses tbody').html(html);

    $('#tableImportPurchasedCourses .js-select-all').on('click',function(){
        var dataCourse = $(this).attr('data-course');
        $('#tableImportPurchasedCourses .course-'+dataCourse+':enabled').prop('checked', this.checked);
    });
}
function fetchCoursekeys() {
    var req = {};
    var res;
    req.action = "get-course-keys-details";
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        //console.log(data);
        key_rate = data.key_rate[0].key_rate;
        $('#keyrate').html(display_current_currency + ' ' + key_rate);
    });
}
function inviteStudent(req) {
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        if (data.status == 1) {
            toastr.success(data.message);
            $('#modalInviteStudents').modal('hide');
            get_keys_remaining();
        }
        else {
            toastr.error('<span class="help-block text-danger">' + data.message + ' </span>');
        }    
    });
}
function fetchLicensingStatus() {
    var req = {};
    var res;
    req.action = 'fetch-licensing-status';
    req.courseId = courseId;
    $.ajax({
        'type'  :   'post',
        'url'   :   ApiEndPoint,
        'data'  :   JSON.stringify(req)
    }).done(function(res) {
        res = $.parseJSON(res);
        if(res.status == 0)
            toastr.error(res.message);
        else {
            if(res.course.liveForStudent == 0) {
                $('#availStudentMarket').prop('checked', false);
            }
            else {
                $('#unlive-course-student').removeClass('hide');
                $('#live-course-student').addClass('hide');
            }
        }
    });
}
var _URL = window.URL || window.webkitURL;
var cropCoords,
file,
originalImageWidth, originalImageHeight,
uploadSize = 300,
previewSize = 500;

$("#fileCoursePic").on("change", function(){
    file = this.files[0];
    if (!!file) {
        var JcropAPI = $('#coursePic').data('Jcrop');
        if (JcropAPI != undefined) {
            JcropAPI.destroy();
        }
        $('.js-upload').removeClass('hide');
        var img;
        img = new Image();
        img.onload = function () {
            //console.log(this.width + " " + this.height);
            originalImageWidth = this.width;
            originalImageHeight = this.height;
            readFile(file, {
                width: originalImageWidth,
                height: originalImageHeight
            }).done(function(imgDataUrl, origImage) {
                //$("input, img, button").toggle();
                initJCrop(imgDataUrl);
            }).fail(function(msg) {
                toastr.error(msg);
            });
        };
        img.src = _URL.createObjectURL(file);
    } else {
        $('.js-upload').addClass('hide');
    }
});

$("#btnUpload").on("click", function(){
    var thiz = $(this);
    $(this).text("Uploading...").prop("disabled", true);

    readFile(file, {
        width: uploadSize,
        height: uploadSize,
        crop: cropCoords
    }).done(function(imgDataURI) {
        var data = new FormData();
        var blobFile = dataURItoBlob(imgDataURI);
        data.append('course-image', blobFile);
        data.append('courseId', courseId);
        
        ajaxRequest({
            url: FileApiPoint,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: 'json',
            success: function(res) {
                //console.log(res);
                if (res.status == 1) {
                    thiz.text('Upload');
                    var JcropAPI = $('#coursePic').data('Jcrop');
                    JcropAPI.destroy();
                    $('#coursePic').prop('style', '');
                    $('#coursePic').prop('src', res.image);
                    $('#coursePic').attr('old-image',res.image);
                    $('.coursepic img').prop('src', res.image);
                    $('.js-upload').addClass('hide');
                    toastr.success(res.message);
                } else {
                    toastr.error(res.message);
                }
            },
            error: function(xhr) {
                toastr.error(blobFile);
            }
        });
    });
});

/*****************************
show local image and init JCrop
*****************************/
var initJCrop = function(imgDataUrl){
    var img = $("img#coursePic").attr("src", imgDataUrl);
    
    var storeCoords = function(c) {
        //console.log(c);
        cropCoords = c;
    };
    
    var w = img.width();
    var h = img.height();
    var s = uploadSize;
    img.Jcrop({
        onChange: storeCoords,
        onSelect: storeCoords,
        aspectRatio: 1,
        setSelect: [(w - s) / 2, (h - s) / 2, (w - s) / 2 + s, (h - s) / 2 + s],
        trueSize: [originalImageWidth, originalImageHeight]
    });
};
$(document).ready(function() {
    $('.btn-purchase-keys').click(function(event) {
        /* Act on the event */
        if (walletApp == '') {
            $("#modalSetAddress").modal("show");
        } else {
            $('#modalPurchaseCourseKeys').modal('show');
        }
    });
    $('.request-approval-link').on('click', function () {
        var req = {};
        req.courseId = courseId;
        req.action = "request-course-approval";
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1) {
                toastr.success(res.message);
                $('.request-approval-link').removeClass('btn-success').removeClass('btn-danger').addClass('btn-warning').unbind('click').html('<i class="fa fa-user"></i> Pending');
                $('#resultResend').addClass('hide');
            } else {
                toastr.error(res.message);
            }
        });
    });
    $('#availStudentMarket').on('click', function() {
        if($(this).prop('checked')) {
            $('#live-course-student').attr('disabled', false);
        }
        else
            $('#live-course-student').attr('disabled', true);
    });
    //for allowing decimal number
    $('#inputStudentPriceUSD, #inputStudentPriceINR').keypress(function (event) {
        return allowDecimal(event, $(this));
    });
    if (demoVideo) {
        $('video').mediaelementplayer();
        $('#modalUploadDemoVideo .delete-file').on('click', function() {
            var req = {};
            var res;
            req.action = 'delete-demo-video';
            req.courseId = courseId;
            $.ajax({
                type: 'post',
                url: ApiEndPoint,
                data: JSON.stringify(req)
            }).done(function(res) {
                res = $.parseJSON(res);
                if(res.status == 1)
                    location.reload();
                else
                    toastr.error(res.message);
            });
        });
    }
    $('#modalUploadDemoVideo').find('form').attr('action', FileApiPoint).ajaxForm({
        data: {courseId:courseId,demoFlag:demoVideo},
        beforeSend: function(arr, $form, data) {
            //console.log('starting');
        },
        uploadProgress: function(event, position, total, percentComplete) {
            //Progress bar
            var progress = $('#modalUploadDemoVideo').find('.progress')
            progress.find('.progress-bar').width(percentComplete + '%') //update progressbar percent complete
            progress.find('.sr-only').html(percentComplete + '%') //update progressbar percent complete
        },
        success: function(resp) {
            if((typeof(resp) === 'string') && resp.substr(0, 6) == '<br />') {
                toastr.error('There was some error, please refresh and try again!');
            }
        },
        complete: function(resp) {
            res = $.parseJSON(resp.responseText);
            if(res.status == 1)
                location.reload();
            else
                toastr.error('An error occured. Please refresh the page and try again. Error Details: ' + res.message);
        },
        error: function() {
            console.log('Error');
        }
    });
    //events for demo video
    $('#modalUploadDemoVideo .btn-upload-video').on('click', function() {
        $('#modalUploadDemoVideo .upload-file').click();
        return false;
    });
    $('#modalUploadDemoVideo .upload-file').on('change', function() {
        $('#modalUploadDemoVideo').find('form').submit();
    });
    $('#modalUploadCourseDocument').find('form').attr('action', FileApiPoint).ajaxForm({
        data: {courseId:courseId,demoFlag:0},
        beforeSend: function(arr, $form, data) {
            //console.log('starting');
        },
        uploadProgress: function(event, position, total, percentComplete) {
            //Progress bar
            var progress = $('#modalUploadCourseDocument').find('.progress')
            progress.find('.progress-bar').width(percentComplete + '%') //update progressbar percent complete
            progress.find('.sr-only').html(percentComplete + '%') //update progressbar percent complete
        },
        success: function(resp) {
            if((typeof(resp) === 'string') && resp.substr(0, 6) == '<br />') {
                toastr.error('There was some error, please refresh and try again!');
            }
        },
        complete: function(resp) {
            res = $.parseJSON(resp.responseText);
            if(res.status == 1)
                location.reload();
            else
                toastr.error('An error occured. Please refresh the page and try again. Error Details: ' + res.message);
        },
        error: function() {
            console.log('Error');
        }
    });
    //events for demo document
    $('#modalUploadCourseDocument .btn-upload-document').on('click', function() {
        $('#modalUploadCourseDocument .upload-file').click();
        return false;
    });
    $('#modalUploadCourseDocument .upload-file').on('change', function() {
        var file = this.files[0];
        if(file.type =='application/pdf'){
            $('#modalUploadCourseDocument').find('form').submit();
        }
        else{
            toastr.error("Please upload PDF files only");
        }
    });
    $('#btnParentCredentials').click(function(){
        var students = "";
        $('.js-option-student').each(function(key,obj){
            if ($(obj).is(":checked")) {
                if ($(obj).closest('tr').attr("data-temp") == "0") {
                    students+= $(obj).closest('tr').find('td:nth-child(2)').text()+',';
                }
            }
        });
        students = students.substr(0,students.length-1);
        
        if (!students) {
            toastr.error("Please select at least 1 registered "+terminologies["student_single"]);
        } else {
            bootbox.confirm("Are you sure?", function(result) {
                if (result == true) {
                    var req = {};
                    var res;
                    req.course_id = courseId;
                    req.students  = students;
                    req.action    = "generate-parents";
                    $.ajax({
                        'type': 'post',
                        'url': ApiEndPoint,
                        'data': JSON.stringify(req)
                    }).done(function (res) {
                        data = $.parseJSON(res);
                        for (var i = 0; i < data.students.length; i++) {
                            $('[data-student="'+data.students[i].student_id+'"]').find('td:first-child').html('');
                            //$('[data-student="'+data.students[i].student_id+'"]').find('td:nth-child(6)').html(data.students[i].userid);
                            $('[data-student="'+data.students[i].student_id+'"]').find('td:nth-child(7)').html('<strong>'+data.students[i].userid+'</strong>');
                            $('[data-student="'+data.students[i].student_id+'"]').find('td:nth-child(8)').html('<strong>'+data.students[i].passwd+'</strong>');
                        };
                        console.log(data);
                    });
                }
            });
        };
    });
    $('#btnRemoveStudents').click(function(){
        var tempStudents = "";
        var permStudents = "";
        var noOfStudents = 0;
        $('.js-option-student').each(function(key,obj){
            if ($(obj).is(":checked")) {
                if ($(obj).closest('tr').attr("data-temp") == "1") {
                    tempStudents+= $(obj).closest('tr').find('td:nth-child(2)').text()+',';
                } else {
                    permStudents+= $(obj).closest('tr').find('td:nth-child(2)').text()+',';
                }
                noOfStudents++;
            }
        });
        tempStudents = ((tempStudents!="")?(tempStudents.substr(0,tempStudents.length-1)):(""));
        permStudents = ((permStudents!="")?(permStudents.substr(0,permStudents.length-1)):(""));
        
        if (noOfStudents==0) {
            toastr.error("Please select at least 1 "+terminologies["student_single"]);
        } else {
            bootbox.confirm("Are you sure?", function(result) {
                if (result == true) {
                    var req = {};
                    var res;
                    req.course_id = courseId;
                    req.tempStudents  = tempStudents;
                    req.permStudents  = permStudents;
                    req.action    = "delete-students";
                    
                    $.ajax({
                        'type': 'post',
                        'url': ApiEndPoint,
                        'data': JSON.stringify(req)
                    }).done(function (res) {
                        data = $.parseJSON(res);
                        if (data.status == 0) {
                            toastr.error(data.exception);
                        } else {
                            toastr.success("Successfully removed!");
                            for (var i = 0; i < data.students.length; i++) {
                                $('[data-student="'+data.students[i]+'"]').remove();
                                keys_available++;
                            };
                            $('#remaining_keys_total').html(keys_available);
                        }
                        //console.log(data);
                    });
                }
            });
        };
    });
    $('#btnImportSubjects').on('click', function () {
        var selectedSubjects = getSelectedFromExistingSubjectImportForm();
        if (selectedSubjects.length == 0) {
            toastr.error('Please select a '+terminologies["subject_single"].toLowerCase()+' to import.');
            return;
        }

        var req = {};
        req.courseId = courseId;
        req.action = 'import-subjects-to-course';
        req.subjects = selectedSubjects;
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1) {
                toastr.success(res.message);
                // $('#import-content-modal').modal('hide');
                // fetchSubjects();
                // fetchCoursesForImport();
                // toastr.error(res.msg);
                // location.reload();
            } else {
                toastr.error(res.msg);
            }
        });
    });
    $('#btnGenStudents').click(function(){
        var inputGenStudents = $('#inputGenStudents').val();
        if (!$.isNumeric(inputGenStudents)) {
            toastr.error("Please enter a number!");
        } else if (inputGenStudents<1) {
            toastr.error("Please enter a number greater than 0!");
        } else if (inputGenStudents>keys_available) {
            toastr.error("Please enter a number less than "+keys_available+"!");
        } else {
            var req = {};
            var res;
            req.action = 'generate-students-for-course';
            req.students = inputGenStudents;
            req.courseId = courseId;
            $.ajax({
                'type'  :   'post',
                'url'   :   ApiEndPoint,
                'data'  :   JSON.stringify(req)
            }).done(function(res) {
                res = $.parseJSON(res);
                if(res.status == 0)
                    toastr.error(res.message);
                else {
                    //console.log(res);
                    window.location = window.location;
                }
            });
        }
    });
    //purchaseNow button click event listener
    $('#purchaseNow').on('click', function() {
        //console.log($('#inputTotalKeys').val());
        if($('#inputTotalKeys').val() != '' && parseInt($('#inputTotalKeys').val()) > 0) {
            $('#modalPurchaseCourseKeys').modal('hide');
            var keys = $('#inputTotalKeys').val();
            var amount = keys * key_rate;
            //$('#inputTotalKeys').html('');
            $('#modalPurchaseNowCourseKeys').modal('show');
            $('#modalPurchaseNowCourseKeys .price-mul').html(key_rate + ' ' + display_current_currency);
            $('#modalPurchaseNowCourseKeys .units').html(keys);
            $('#modalPurchaseNowCourseKeys .amount').html(amount + ' ' + display_current_currency);
            $('#modalPurchaseNowCourseKeys .you-pay').html(amount + ' ' + display_current_currency);
        } else {
            toastr.error('Invalid entry');
        }
    });
    //event handler for PayU Money
    $('#payUMoneyButton').on('click', function() {
        var req = {};
        var res;
        req.action = 'purchaseCourseKeys';
        req.quantity = $('#inputTotalKeys').val();
        req.currency = UserCurrency;
        $.ajax({
            'type'  : 'post',
            'url'   : ApiEndPoint,
            'data'  : JSON.stringify(req), 
        }).done(function(res) {
            res = $.parseJSON(res);
            if(res.status == 1) {
                if(res.paymentSkip == 1)
                    location.reload();
                else {
                    transferIGRO(res.amount,res.IGROTxnID);
                    $("#modalPurchaseNowCourseKeys").modal("hide");
                }
                /*if(res.paymentSkip == 1)
                    location.reload();
                else if(res.method == 2){
                    var html = '';
                    html = '<form action="'+res.url+'" method="post" id="payUForm">'
                        + '<input type="hidden" name="txnid" value="' + res.txnid + '">'
                        + '<input type="hidden" name="key" value="'+res.key+'">'
                        + '<input type="hidden" name="amount" value="'+res.amount+'">'
                        + "<input type='hidden' name='productinfo' value='"+res.productinfo+"'>"
                        + '<input type="hidden" name="firstname" value="'+res.firstname+'">'
                        + '<input type="hidden" name="email" value="'+res.email+'">'
                        + '<input type="hidden" name="surl" value="'+res.surl+'">'
                        + '<input type="hidden" name="furl" value="'+res.furl+'">'
                        + '<input type="hidden" name="curl" value="'+res.curl+'">'
                        + '<input type="hidden" name="furl" value="'+res.furl+'">'
                        + '<input type="hidden" name="hash" value="'+res.hash+'">'
                        + '<input type="hidden" name="service_provider" value="'+res.service_provider+'">';
                    + '</form>'
                    $('body').append(html);
                    $('#payUForm').submit();
                }
                else if(res.method == 1) {
                    var html = '';
                    html += '<form action="' + res.url + '" method="post" id="paypalForm">'
                        + '<!-- Identify your business so that you can collect the payments. -->'
                        + '<input type="hidden" name="business" value="' + res.business + '">'
                        + '<!-- Specify a Buy Now button. -->'
                        + '<input type="hidden" name="cmd" value="_xclick">'
                        + '<!-- Specify details about the item that buyers purchase. -->'
                        + '<input type="hidden" name="item_name" value="Enrollment Keys">'
                        + '<input type="hidden" name="amount" value="' + res.rate + '">'
                        + '<input type="hidden" name="currency_code" value="USD">'
                        + '<input type="hidden" name="quantity" value="' + res.quantity + '">'
                        + '<input type="hidden" name="invoice" value="' + res.orderId + '">'
                        + '<input type="hidden" name="item_number" value="' + res.orderId + '">'
                        + '<input type="hidden" name="return" value="' + res.surl + '" />'
                        + '<input type="hidden" name="notify_url" value="' + res.nurl + '" />'
                        + '<input type="hidden" name="cancel_return" value="' + res.curl + '" />'
                        + '<input type="hidden" name="lc" value="US" />'
                        + '<!-- Display the payment button. -->'
                        + '<input type="image" name="submit" border="0" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online">'
                        + '<img alt="" border="0" width="1" height="1"  src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >'
                    + '</form>';
                    $('body').append(html);
                    $('#paypalForm').submit();
                }*/
            }else{
                toastr.error(res.message);
            }
        });
    });
    $('#inputTotalKeys').keyup(function () {
        var total = $(this).val() * key_rate;
        /*$('#totalkeyrate').text(total);
        $('#totalkeyrate').append(display_current_currency);*/
        $('#totalkeyrate').text(total + ' ' + display_current_currency);
    });
    $("#inputTotalKeys").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            toastr.error('Enter Only Numbers', terminologies["course_single"]+' Keys');
            //$('.help-block').fadeOut("slow");
            // ("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
    $('#keysPurchase').click(function () {
        total_puchased_keys = $.trim($('#inputTotalKeys').val());
        if (total_puchased_keys == '' || total_puchased_keys == 0) {
            toastr.error('Invalid entry', terminologies["course_single"]+' keys');
            return false;
        }
        $('#modalPurchaseCourseKeys').modal('hide');
        $('#modalPurchaseInvitation').modal('show');
    });
    $('#keys-purchaseInvitation').click(function () {
        var req = {};
        var res;
        var email = $.trim($('#inputInviteEmail').val());
        if (email != '' && validateEmail(email) == false && isNaN(email)) {
            toastr.error('Please enter Correct email address/ Phone no.');
        } else {
            if (validateMobile(email) && !(isNaN(email))) {
                toastr.error('Please enter Correct email address/ Phone no.');
            }
        }
        req.action = "keys-purchase_invitation";
        req.keys = total_puchased_keys;
        req.email = email;
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            toastr.success(data.message);
            $('#modalPurchaseCourseKeys').modal('hide');
            $('#modalPurchaseInvitation').modal('hide');
            // window.location.reload();
        });
    });
    function getSelectedFromExistingSubjectImportForm() {
        var selectedSubjects = [];
        $('#tableImportCourses .option-subject').each(function (i, subject) {
            var subjectId = $(this).attr('data-subject');
            if (($(this).prop('checked') == true) && (!$(this).prop('disabled')))
                selectedSubjects.push(subjectId);
        });
        //console.log(selectedSubjects);
        return selectedSubjects;
    }
    $('#modalImportSubject').on('show.bs.modal', function() {
        if(flagForImport)
            fillCoursesForImport(importCourse);
        else
            fetchCoursesForImport();
    });
    $(".star-rating").rating({displayOnly: true});
    $('body').on('click', '.edit-subject', function(e) {
        var subjectId = $(this).closest('tr').attr('data-subject');
        $(this).closest('tr').addClass('active');
        e.preventDefault();
        var req = {};
        var res;
        req.courseId = courseId;
        req.subjectId = subjectId;

        req.action = "get-subject-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillSubjectDetails(res);
            //details=res;
        });
    });
    function fillSubjectDetails(data) {
        $('.js-subject-action').html("Edit");
        // General Details
        if(data.subjectDetails.deleted == 1)
            toastr.error('This subject has been deleted!!', -1);
        $('#inputSubjectName').val(data.subjectDetails.name);
        $('#inputSubjectDescription').val(data.subjectDetails.description);
        $('#modalAddEditSubject').modal("show");
    }
    $('#modalInviteStudents').on('shown.bs.modal', function () {
        $('#inputEmailIds').val('');
        CKEDITOR.instances['textEmailNote'].setData('Dear Student,<br /><br />Welcome to Integro! You have been invited to join the following course.<br /><br />[course_invite_block]<br /><br />Best Regards,<br />Integro Team<br />Integro.io');
        keys_available = get_keys_remaining();
    });
    $('#btnInviteStudents').on('click', function () {
        var invalid = '';
        var correct = true
        var emails = $.trim($('#inputEmailIds').val());
        var emailNote = CKEDITOR.instances['textEmailNote'].getData();
        var email = emails.split(',');
        if (emails == '') {
            toastr.error('Please enter email address.');
            // toastr.error('Please enter email id ');
            return false;
        }
        if (checkDuplicates(email) == 'false') {
            toastr.error('Please remove duplicates.');
            //toastr.error('Please remove duplicates ');
            return false;
        }
        if ((email.length > keys_available)) {
            toastr.error('You have ' + keys_available + ' '+terminologies["course_single"].toLowerCase()+' keys.Please add '+terminologies["course_single"].toLowerCase()+' key in your account to assign '+terminologies["student_plural"].toLowerCase()+'.');
            // toastr.error('You have only ' + keys_available + ' available ');
            return false;
        }
        if (emailNote.indexOf('[course_invite_block]') == -1) {
            toastr.error('Please add [course_invite_block] in the email note, it is necessary.');
            return false;
        }
        $.each(email, function (index, value) {
            if (validateEmail($.trim(value)) == false) {
                invalid += value + ',';
                correct = false;
            }
        });
        if (correct == false) {
            toastr.error('Correct these email addresses:\n' + invalid + '');
            //toastr.error('Correct these email addresses:\n' + invalid);
            return false;
        }
        else {
            var req = {};
            req.courseId = courseId;
            req.action = 'send_activation_link_student';
            req.email = email;
            req.emailNote = emailNote;
            req.enrollment_type = 0;
            //req.ignoreWarning = false;
            inviteStudent(req);
        }
    });
});
</script>