<script type="text/javascript">
    var searchQuery,
    name = {};
    $(document).ready(function(){
        // on query entered in search bar 
        $('#inputSearchText').on("keypress", function (event) {
            if (event.which === 13) {
                searchQuery = getSearchQuery();
                if (searchQuery.text !== "") {
                    fetchProfessorDetails(searchQuery);
                }
            };
        });
        $('#btnSearchProfessors').on('click', function() {
            searchQuery = getSearchQuery();
            if (searchQuery.text !== "") {
                fetchProfessorDetails(searchQuery);
            }
        });
        $('#siteProfessors').on("click", ".send-invitation-btn", function () {
            var profId = $(this).attr("data-professor"),
            invited;
            var req = {},
            res     = {};

            // if professor already invited, return
            invited = $(this).hasClass('invited');
            if(invited) {
                return;
            }

            //send invitation request
            req.action = "invite-professor";
            req.profId   = profId;
            $.ajax({
                'type'  : 'post',
                'url'   : ApiEndPoint,
                'data'  : JSON.stringify(req)
            }).done(function (res) {
                res =  $.parseJSON(res);
                if (res.status === 1) {
                     $('[data-professor='+profId+']').text(res.message);
                     $('[data-professor='+profId+']').removeClass('send-invitation-btn font-purple').addClass('invited font-yellow').attr("disabled", true);
                    //$('.send-invitation-btn').addClass('invited');
                    //$('.send-invitation-btn').text('invited');
                }
                
            });
        });
    });
    var getSearchQuery = function () {
        var query = {};
        query.type = parseInt($('#selectSearchFilter').val());
        query.text = $('#inputSearchText').val().trim();
        return query;
    };
    var fetchProfessorDetails = function (query) {
        var req = {},
        res     = {};

        req.action = "fetch-professor-details";
        req.query   = query;
        $.ajax({
            'type'  : 'post',
            'url'   : ApiEndPoint,
            'data'  : JSON.stringify(req)
        }).done(function (res) {
            res =  $.parseJSON(res);
            userId = res.userId;
            //console.log(res);
            displayProfessorDetails(res);
        });
    };
    var displayProfessorDetails = function (professors) {
        var html = "";
        var professors = professors.userDetails;
        if (professors) {
            var invitedStatus;
            $.each(professors, function(i, professor) {
                console.log(professor);
                invitedStatus = (professor.invitedStatus !== null && professor.invitedStatus<2) ? true: false;
                html+= '<div class="col-lg-3 col-md-4 col-sm-6">'+
                            '<!--begin: widget 1-1-->'+
                            '<div class="mt-widget-1 ar-widget-2">'+
                                '<div class="mt-img">'+
                                    '<img src="' + professor.profilePic + '" alt="Profile Avatar"> </div>'+
                                '<div class="mt-body">'+
                                    '<h3 class="mt-username">' + professor.firstName + " " + professor.lastName + '</h3>'+
                                    '<p class="mt-user-title">'+
                                        '<ul class="list-unstyled">'+
                                            '<li>E-Mail: ' + professor.email + '</li>'+
                                            '<li>Country: ' + professor.addressCountry + '</li>'+
                                        '</ul> </p>'+
                                    '<div class="mt-stats">'+
                                        '<div class="btn-group btn-group btn-group-justified">'+
                                            '<a href="' + sitepathInstructor + professor.id + '" target="_blank" class="btn font-red"> Profile </a>'+
                                            ((!invitedStatus || professor.instituteId != userId)?'<a href="javascript:;" class="btn font-purple send-invitation-btn" data-professor="'+ professor.id +'"> Send Invitation </a>':'<a href="javascript:;" class="btn font-yellow invited" disabled> Invited </a>')+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<!--end: widget 1-1-->'+
                        '</div>';
            });
            $("#siteProfessors").html(html);
        };
        /*var html = "",
        flag = false,
        count = 0,
        userDetails,
        invitedStatus = false,
        notFound = "No such entries found!";

        userDetails = professors.userDetails;
        if (userDetails) {
            userDetails.forEach(function (professor) {
                invitedStatus = (professor.invitedStatus !== null) ? true: false;
                if(flag == false) {
                    flag = true;
                }
                if(professor)
                {
                    html += '<div class="row" style="margin-left:0px;margin-right:0px"><div class=" pull-left custom-img"><aside><div class="post-info"><span class="arrow-pro right"></span><div class="panel-body custom_prof"><h1><a href="../instructor/' + professor.id + '" target="_blank"><strong style="text-transform: none;">' + professor.firstName + " " + professor.lastName + '</strong></a>';
                    html += '</h1>E-Mail: ' + professor.email + '<br>Country: ' + professor.addressCountry + '<br><br>';
                    if (!invitedStatus || professor.instituteId != userId) {
                        html += '<div class="post-btn"><button onclick="window.location = \'../instructor/' + professor.id + '\'" class="btn btn-primary">Profile</button>&emsp;<button class="btn btn-info send-invitation-btn" type="button" id="' + professor.profId + '">Send Invitation</button></div>';
                    }
                    else {
                        html += '<div class="post-btn"><button onclick="window.location = \'../instructor/' + professor.id + '\'" class="btn btn-primary">Profile</button>&emsp;<button class="btn btn-info invited" type="button">Invited</button></div>';
                    }
                    html += '</div></div></aside></div>';
                    html += '<div class="pull-left custom-right-img"><aside class="post-highlight yellow v-align"><a href="../instructor/' + professor.id + '" target="_blank"><div class="panel-body text-center"><div class="pro-thumb"><img src="' + professor.profilePic + '" alt="Profile Avtar"></div></div></a></aside></div></div><hr>';
                }
            });
        }
        if(flag == true) {
            $('#invite-professor-table').html(html);
            if (typeof callback === 'function') {
                // call inviteProfessor
                callback();
            }
        }
        else {
            $('#invite-professor-table').html(notFound);
        }*/
    };
</script>