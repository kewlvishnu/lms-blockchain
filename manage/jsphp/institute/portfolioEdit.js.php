<script type="text/javascript">
$('#btnUpdatePortfolio').click(function(){
    var portfolioName = $.trim($('#portfolioName').val());
    var portfolioDescription = $.trim($('#portfolioDescription').val());
    var portfolioPic = $.trim($('#portfolioPic').val());
    var selectStatus = $.trim($('#selectStatus').val());
    if(!portfolioName) {
        $('#frmPortfolio .help-block').html('<p class="text-danger text-center">Portfolio Name can\'t be left empty</p>');
    } else if(!portfolioDescription) {
        $('#frmPortfolio .help-block').html('<p class="text-danger text-center">Portfolio Description can\'t be left empty</p>');
    } else if(!selectStatus) {
        $('#frmPortfolio .help-block').html('<p class="text-danger text-center">Status can\'t be left empty</p>');
    } else {
        updatePortfolio(portfolioName,portfolioDescription,portfolioPic);
    }
    return false;
});

function updatePortfolio(portfolioName,portfolioDescription,portfolioPic) {
    var req = {};
    req.slug = subdomain;
    req.portfolioName = portfolioName;
    req.portfolioDesc = portfolioDescription;
    req.portfolioPic  = portfolioPic;
    if ($('#affiliation').length>0) {
        req.affiliation  = $('#affiliation').val();
    };
    req.inputFavicon  = $('#faviconPath').val();
    req.inputFacebook = $('#inputFacebook').val();
    req.inputTwitter  = $('#inputTwitter').val();
    req.inputLinkedin = $('#inputLinkedin').val();
    req.selectStatus  = $('#selectStatus').val();
    var res;
    req.action = "update-portfolio";
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        res = $.parseJSON(res);
        if(res.status == 1) {
            //$('#frmPortfolio').html('<div class="jumbotron text-center"><h3>'+res.message+'</h3></div>');
            window.location = window.location;
        } else {
            $('#frmPortfolio .help-block').html('<p class="text-danger text-center">'+res.message+'</p>');
        }
        console.log(res);
    });
}
var _URL = window.URL || window.webkitURL;
var cropCoords,
file,
originalImageWidth, originalImageHeight,
uploadSize = 150,
previewSize = 500;

$("#fileProfilePic").on("change", function(){
    file = this.files[0];
    if (!!file) {
        $('.js-upload').removeClass('hide');
        var img;
        img = new Image();
        img.onload = function () {
            //console.log(this.width + " " + this.height);
            originalImageWidth = this.width;
            originalImageHeight = this.height;
            readFile(file, {
                width: originalImageWidth,
                height: originalImageHeight
            }).done(function(imgDataUrl, origImage) {
                //$("input, img, button").toggle();
                initJCrop(imgDataUrl);
            }).fail(function(msg) {
                toastr.error(msg);
            });
        };
        img.src = _URL.createObjectURL(file);
    } else {
        $('.js-upload').addClass('hide');
    }
});

$("#btnUpload").on("click", function(){
    var thiz = $(this);
    $(this).text("Uploading...").prop("disabled", true);

    readFile(file, {
        width: uploadSize,
        height: uploadSize,
        crop: cropCoords
    }).done(function(imgDataURI) {
        var data = new FormData();
        var blobFile = dataURItoBlob(imgDataURI);
        data.append('portfolio-image-update', blobFile);
        
        ajaxRequest({
            url: FileApiPoint,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: 'json',
            success: function(res) {
                console.log(res);
                if (res.status == 1) {
                    thiz.text('Upload');
                    var JcropAPI = $('#userProfilePic').data('Jcrop');
                    JcropAPI.destroy();
                    $('#userProfilePic').prop('style', '');
                    $('#userProfilePic').prop('src', res.image);
                    $('#userProfilePic').attr('old-image',res.image);
                    $('.js-upload').addClass('hide');
                    $('.profile-userpic img').attr('src', res.image);
                } else {
                    toastr.error(res.msg);
                }
            },
            error: function(xhr) {
                toastr.error(blobFile);
            }
        });
    });
});
$("#fileFavicon").on("change", function(){
    file = this.files[0];
    if (!!file) {
        var img;
        img = new Image();
        img.onload = function () {
            //console.log(this.width + " " + this.height);
            originalImageWidth = this.width;
            originalImageHeight = this.height;
            readFilePNG(file, {
                width: originalImageWidth,
                height: originalImageHeight
            }).done(function(imgDataUrl, origImage) {
                //$("input, img, button").toggle();
                //initJCrop(imgDataUrl);
                $("#portfolioFavicon").attr("src",imgDataUrl);
                console.log($(origImage).attr('src'));
                portfolioFavicon($(origImage).attr('src'));
            }).fail(function(msg) {
                toastr.error(msg);
            });
        };
        img.src = _URL.createObjectURL(file);
    }
});
function portfolioFavicon(imgDataURI) {
    var data = new FormData();
    var blobFile = dataURItoBlob(imgDataURI);
    data.append('fileFavicon', blobFile);
    
    ajaxRequest({
        url: FileApiPoint,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        dataType: 'json',
        success: function(res) {
            if (res.status == 1) {
                $('#portfolioFavicon').prop('src', res.imageName);
                $('#portfolioFavicon').attr('old-image',res.imageName);
                $('#faviconPath').val(res.imageName);
            } else {
                toastr.error(res);
            }
        },
        error: function(xhr) {
            toastr.error(blobFile);
        }
    });
}

/*****************************
show local image and init JCrop
*****************************/
var initJCrop = function(imgDataUrl){
    var img = $("img#userProfilePic").attr("src", imgDataUrl);
    
    var storeCoords = function(c) {
        //console.log(c);
        cropCoords = c;
    };
    
    var w = img.width();
    var h = img.height();
    var s = uploadSize;
    img.Jcrop({
        onChange: storeCoords,
        onSelect: storeCoords,
        aspectRatio: 1,
        setSelect: [(w - s) / 2, (h - s) / 2, (w - s) / 2 + s, (h - s) / 2 + s],
        trueSize: [originalImageWidth, originalImageHeight]
    });
};
</script>