<script type="text/javascript">
function getCountriesList() {
    var req = {};
    var res;
    req.action = 'get-countries-list';
    $.ajax({
        'type'  :   'post',
        'url'   :   ApiEndPoint,
        'data'  :   JSON.stringify(req)
    }).done(function(res) {
        res = $.parseJSON(res);
        //console.log(res);
        if(res.status == 0)
            toastr.error(res.message);
        else
            fillCountries(res.countries);
    });
}
function fillCountries(data) {
    for (var i = 0; i < data.length; i++) {
        $("#selectCountry").append('<option value="'+data[i].id+'">'+data[i].name+'</option>');
    }
    fillProfileEditForm();
}
function fillProfileEditForm() {
	//console.log(profileDetails);
	$('#inputInstituteName').val(profileDetails.profileDetails.name);
    $('#inputInstituteMission').val(profileDetails.profileDetails.tagline);
	$('#inputOwnerFirstName').val(profileDetails.profileDetails.ownerFirstName);
	$('#inputOwnerLastName').val(profileDetails.profileDetails.ownerLastName);
	$('#inputContactFirstName').val(profileDetails.profileDetails.contactFirstName);
	$('#inputContactLastName').val(profileDetails.profileDetails.contactLastName);
	$('#selectStudentCount').val(profileDetails.profileDetails.studentCount);
	$('#selectYearInception').val(profileDetails.profileDetails.foundingYear);

	$('#inputMobilePrefix').val(profileDetails.userDetails.contactMobilePrefix);
	$('#inputMobile').val(profileDetails.userDetails.contactMobile);
	$('#inputLandlinePrefix').val(profileDetails.userDetails.contactLandlinePrefix);
	$('#inputLandline').val(profileDetails.userDetails.contactLandline);

	$('#inputAddress').val(profileDetails.userDetails.addressStreet);
	$('#inputCity').val(profileDetails.userDetails.addressCity);
	$('#selectState').val(profileDetails.userDetails.addressState);
	$('#inputPin').val(profileDetails.userDetails.addressPin);
	$('#selectCountry').val(profileDetails.userDetails.countryId);
    if (profileDetails.userDetails.countryId == 1) {
        $("#selectState").attr("disabled", false);
    } else {
        $("#selectState").val("");1
        $("#selectState").attr("disabled", true);
    }
}
var handleValidation1 = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form1 = $('#formProfile');
    var error1 = $('.alert-danger', form1);
    var success1 = $('.alert-success', form1);

    //IMPORTANT: update CKEDITOR textarea with actual content before submit
    form1.on('submit', function() {
        for(var instanceName in CKEDITOR.instances) {
            CKEDITOR.instances[instanceName].updateElement();
        }
    })

    form1.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input
        rules: {
            inputInstituteName: {
                minlength: 2,
                required: true
            },
            inputInstituteMission: {
                minlength: 2,
                required: true
            },
            inputOwnerFirstName: {
                minlength: 2
            },
            inputOwnerLastName: {
                minlength: 2
            },
            inputContactFirstName: {
                minlength: 2
            },
            inputContactLastName: {
                minlength: 2
            },
            inputMobilePrefix: {
                minlength: 2,
                required: true
            },
            inputMobile: {
                minlength: 10,
                required: true
            },
            selectCountry: {
                required: true
            }
        },

        messages: { // custom messages for radio buttons and checkboxes
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.parent(".input-group").size() > 0) {
                error.insertAfter(element.parent(".input-group"));
            } else if (element.attr("data-error-container")) { 
                error.appendTo(element.attr("data-error-container"));
            } else if (element.parents('.radio-list').size() > 0) { 
                error.appendTo(element.parents('.radio-list').attr("data-error-container"));
            } else if (element.parents('.radio-inline').size() > 0) { 
                error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
            } else if (element.parents('.checkbox-list').size() > 0) {
                error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
            } else if (element.parents('.checkbox-inline').size() > 0) { 
                error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success1.hide();
            error1.show();
            App.scrollTo(error1, -200);
        },

        highlight: function (element) { // hightlight error inputs
           $(element)
                .closest('.validator-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.validator-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label
                .closest('.validator-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function (form) {
            success1.show();
            error1.hide();
            //form[0].submit(); // submit the form
            saveProfile();
            //toastr.success("Hurray!");
        }

    });
}
var handleValidation3 = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form3 = $('#formPassword');
    var error3 = $('.alert-danger', form3);
    var success3 = $('.alert-success', form3);

    //IMPORTANT: update CKEDITOR textarea with actual content before submit
    form3.on('submit', function() {
        for(var instanceName in CKEDITOR.instances) {
            CKEDITOR.instances[instanceName].updateElement();
        }
    })

    form3.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input
        rules: {
            inputOldPassword: {
                minlength: 2,
                required: true
            },
            inputNewPassword: {
                minlength: 2,
                required: true
            },
            inputNewRePassword: {
                minlength: 2,
                required: true,
                equalTo: "#inputNewPassword"
            }
        },

        messages: { // custom messages for radio buttons and checkboxes
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.parent(".input-group").size() > 0) {
                error.insertAfter(element.parent(".input-group"));
            } else if (element.attr("data-error-container")) { 
                error.appendTo(element.attr("data-error-container"));
            } else if (element.parents('.radio-list').size() > 0) { 
                error.appendTo(element.parents('.radio-list').attr("data-error-container"));
            } else if (element.parents('.radio-inline').size() > 0) { 
                error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
            } else if (element.parents('.checkbox-list').size() > 0) {
                error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
            } else if (element.parents('.checkbox-inline').size() > 0) { 
                error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success3.hide();
            error3.show();
            App.scrollTo(error3, -200);
        },

        highlight: function (element) { // hightlight error inputs
           $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function (form) {
            success3.show();
            error3.hide();
            //form[0].submit(); // submit the form
            changePassword();
            //toastr.success("Hurray!");
        }

    });
}
function saveProfile () {
	var req = {};
	req.instituteName	= $('#inputInstituteName').val();
    req.tagline         = $('#inputInstituteMission').val();
	req.ownerFirstName	= $('#inputOwnerFirstName').val();
	req.ownerLastName	= $('#inputOwnerLastName').val();
	req.contactFirstName= $('#inputContactFirstName').val();
	req.contactLastName	= $('#inputContactLastName').val();
	req.studentCount	= $('#selectStudentCount').val();
	req.foundedIn		= $('#selectYearInception').val();
	req.phnPrefix		= $('#inputMobilePrefix').val();
	req.phnNum			= $('#inputMobile').val();
	req.llPrefix		= $('#inputLandlinePrefix').val();
	req.llNum			= $('#inputLandline').val();
	req.street			= $('#inputAddress').val();
	req.city			= $('#inputCity').val();
	req.state			= $('#selectState').val();
    req.country         = $('#selectCountry').val();
	req.pin				= $('#inputPin').val();
	req.action			= 'update-profile-general';
	var res;
	ajaxRequest({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req),
		success:function(res){
			res = $.parseJSON(res);
			if(res.status == 0)
				toastr.error(res.message);
			else {
				$('.alert-success', '#formProfile').show();
				toastr.success("Successfully updated!", "Profile Update");
                $('.profile-usertitle-job').html(req.tagline);
				scrollToElem('.page-content', 100);
				//window.location = window.location;
			}
		}
	});
};
function changePassword() {
    var req = {};
    var res;
    req.oldPwd = $('#inputOldPassword').val();
    req.newPwd = $('#inputNewPassword').val();
    req.action = 'changePassword';

    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        if (data.status == 1) {
            toastr.success(data.message);
            // window.location = 'profile.php';
        }
        else {
            toastr.error(data.message);
        }
    });
}
$('.js-remove').click(function(){
    if ($('#userProfilePic').data('Jcrop')) {
        var JcropAPI = $('#userProfilePic').data('Jcrop');
        JcropAPI.destroy();
        var originalImage = $('#userProfilePic').attr('old-image');
        $('#userProfilePic').prop('src', originalImage);
    } else {
        var req = {};
        req.action      = 'remove-profile-image';
        var res;
        ajaxRequest({
            'type'  :   'post',
            'url'   :   ApiEndPoint,
            'data'  :   JSON.stringify(req),
            success:function(res){
                res = $.parseJSON(res);
                if(res.status == 0)
                    toastr.error(res.message);
                else {
                    $('#userProfilePic').prop('src', res.profilePic);
                    $('#userProfilePic').prop('old-image', res.profilePic);
                }
            }
        });
    }
});

var _URL = window.URL || window.webkitURL;
var cropCoords,
file,
originalImageWidth, originalImageHeight,
uploadSize = 300,
previewSize = 500;

$("#fileProfilePic").on("change", function(){
    file = this.files[0];
    if (!!file) {
        $('.js-upload').removeClass('hide');
        var img;
        img = new Image();
        img.onload = function () {
            //console.log(this.width + " " + this.height);
            originalImageWidth = this.width;
            originalImageHeight = this.height;
            readFile(file, {
                width: originalImageWidth,
                height: originalImageHeight
            }).done(function(imgDataUrl, origImage) {
                //$("input, img, button").toggle();
                initJCrop(imgDataUrl);
            }).fail(function(msg) {
                toastr.error(msg);
            });
        };
        img.src = _URL.createObjectURL(file);
    } else {
        $('.js-upload').addClass('hide');
    }
});

$("#btnUpload").on("click", function(){
    var thiz = $(this);
    $(this).text("Uploading...").prop("disabled", true);

    readFile(file, {
        width: uploadSize,
        height: uploadSize,
        crop: cropCoords
    }).done(function(imgDataURI) {
        var data = new FormData();
        var blobFile = dataURItoBlob(imgDataURI);
        data.append('profile-image', blobFile);
        
        ajaxRequest({
            url: FileApiPoint,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: 'json',
            success: function(res) {
                //console.log(res);
                if (res.status == 1) {
                    thiz.text('Upload');
                    var JcropAPI = $('#userProfilePic').data('Jcrop');
                    JcropAPI.destroy();
                    $('#userProfilePic').prop('style', '');
                    $('#userProfilePic').prop('src', res.image);
                    $('#userProfilePic').attr('old-image',res.image);
                    $('.js-upload').addClass('hide');
                    toastr.success(res.message);
                } else {
                    toastr.error(res.message);
                }
            },
            error: function(xhr) {
                toastr.error(blobFile);
            }
        });
    });
});

/*****************************
show local image and init JCrop
*****************************/
var initJCrop = function(imgDataUrl){
    var img = $("img#userProfilePic").attr("src", imgDataUrl);
    
    var storeCoords = function(c) {
        //console.log(c);
        cropCoords = c;
    };
    
    var w = img.width();
    var h = img.height();
    var s = uploadSize;
    img.Jcrop({
        onChange: storeCoords,
        onSelect: storeCoords,
        aspectRatio: 1,
        setSelect: [(w - s) / 2, (h - s) / 2, (w - s) / 2 + s, (h - s) / 2 + s],
        trueSize: [originalImageWidth, originalImageHeight]
    });
};
$(document).ready(function(){
	handleValidation1();
    handleValidation3();
    $("#selectCountry").change(function(e) {
        if($(this).val() == 1) {
            $("#selectState").attr("disabled", false);
        } else {
            $("#selectState").val("");
            $("#selectState").attr("disabled", true);
        }
    });
});
</script>