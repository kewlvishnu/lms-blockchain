<script type="text/javascript">
var portfolioMenus = [];
var portfolioPages = [];
$('#selectMenuType').change(function(){
    var menuType = $(this).val();
    $('.menu-block').addClass('hide');
    $('#'+menuType+'Block').removeClass('hide');
});
$('.js-portfolio-menus').on('click','.js-menu', function(e){
    e.preventDefault();
    $('.js-menu').removeClass('selected');
    $(this).addClass('selected');
    var pageChecked = $(this).find('[name=optionMenu]');
    $('.js-portfolio-menus [name=optionMenu]').removeAttr('checked');
    pageChecked.prop('checked', true);
    var menuId = pageChecked.val();
    menuProcess(menuId);
});
$('.js-portfolio-menus').on('click','[name=optionMenu]', function(){
    var thiz = $(this);
    $('.js-menu').removeClass('selected');
    thiz.closest('.js-menu').addClass('selected');
    var menuId = thiz.val();
    menuProcess(menuId);
    return true;
});
function menuProcess(menuId) {
    if(menuId == 'new') {
        $("#menuId").val('');
        $("#pageType").val('');
        resetForm();
        $('#selectMenuType').closest('.form-group').removeClass('hide');
        $('.menu-block').addClass('hide');
        var pageType = $('#selectMenuType option:selected').text().toLowerCase();
        $('#'+pageType+'Block').removeClass('hide');
        $('#inputPageContent').val('');

    } else {
        resetForm();
        $('#menuId').val(menuId);
        $('[name="optionMenu"][value="'+menuId+'"]').prop("checked", true);
        var menu = findObjByValue(portfolioMenus,'menuId',menuId);
        $('#selectMenuType').val(menu.type);
        $('.menu-block').addClass('hide');
        $('#'+menu.type+'Block').removeClass('hide');
        $('#inputMenuName').val(menu.name);
        $('#inputMenuDescription').val(menu.description);
        $('#inputMenuOrder').val(menu.order);
        $('#selectMenuStatus').val(menu.status);
        if(menu.type == 'page') {
            $('#selectPortfolioPage').append('<option value="'+menu.pageId+'" selected> '+menu.pageTitle+' [Type : '+menu.pageType+']</option>');
        } else {
            $('#inputMenuUrl').val(menu.url);
        }
    }
}
function resetForm() {
    $('#selectMenuType').val('page');
    $('#pageBlock').removeClass('hide');
    $('#urlBlock').addClass('hide');
    $('#inputMenuUrl').val('');
    $('#inputMenuName').val('');
    $('#inputMenuDescription').val('');
    $('#inputMenuOrder').val('');
    $('#selectMenuStatus').val('active');
    fillPortfolioPages(portfolioPages);
}
function fetchPortfolioMenus() {
    var req = {};
    req.slug = subdomain;
    req.menu = 1;
    var res;
    req.action = "get-portfolio-menus";
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        res = $.parseJSON(res);
        portfolioMenus = res.portfolioMenus;
        fillPortfolioMenus(res);
        //console.log(res);
    });
}
function fillPortfolioMenus(data) {
    // General Details
    if(data.portfolioMenus.length>0) {
        //$('.js-portfolio-menus').html('<ul class="nav nav-pills nav-stacked"></ul>');
        var portfolioMenutype = '';
        for (var i = 0; i < data.portfolioMenus.length; i++) {
            portfolioMenutype = ((data.portfolioMenus[i].type=='page')?'Page : '+data.portfolioMenus[i].pageTitle:'URL : '+data.portfolioMenus[i].url);
            $('.js-portfolio-menus>.nav-pills').append('<li><a href="javascript:void()" class="js-menu"><span class="checkbox"><input type="radio" name="optionMenu" value="'+data.portfolioMenus[i].menuId+'" /> '+data.portfolioMenus[i].name+' ['+portfolioMenutype+'] [Status : '+data.portfolioMenus[i].status+']</span></a></li>');
        };
    }
}
function fetchPortfolioPages() {
    var req = {};
    req.slug = subdomain;
    req.menu = 1;
    var res;
    req.action = "get-portfolio-pages";
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        res = $.parseJSON(res);
        portfolioPages = res.portfolioPages;
        fillPortfolioPages(portfolioPages);
    });
}
function fillPortfolioPages(portfolioPages) {
    $('#selectPortfolioPage').html('');
    //console.log(portfolioPages);
    // General Details
    if(portfolioPages.length>0) {
        //$('.js-portfolio-pages').html('<ul class="nav nav-pills nav-stacked"></ul>');
        //for (var i = 0; i < portfolioPages.length; i++) {
        $.each(portfolioPages, function(i,v){
            //console.log(i);
            if (portfolioPages[i]) {
                $('#selectPortfolioPage').append('<option value="'+portfolioPages[i].pageId+'"> '+portfolioPages[i].title+' [Type : '+portfolioPages[i].type+']</option>');
            }
        });
    } else {
        //$('#selectMenuType').find('option:contains(Page)').remove();
        $('#selectMenuType').val('url');
        $('#selectMenuType').change();
    }
}
$('#frmPortfolioMenu').submit(function(){
    //var menuId                  = $.trim($('#menuId').val());
    var menuId                  = $.trim($('[name="optionMenu"]:checked').val());
    var selectMenuType      = $.trim($('#selectMenuType').val());
    var inputMenuName          = $.trim($('#inputMenuName').val());
    var inputMenuDescription            = $.trim($('#inputMenuDescription').val());
    var inputMenuOrder   = $.trim($('#inputMenuOrder').val());
    var selectMenuStatus      = $.trim($('#selectMenuStatus').val());
    var selectPortfolioPage     = $.trim($('#selectPortfolioPage').val());
    var inputMenuUrl        = $.trim($('#inputMenuUrl').val());
    if(!selectMenuType) {
        $('#frmPortfolioMenu .help-block').html('<p class="text-danger text-center">Please select Menu Type</p>');
    } else if(!inputMenuName) {
        $('#frmPortfolioMenu .help-block').html('<p class="text-danger text-center">Menu Name can\'t be left empty</p>');
    } else if(!inputMenuDescription) {
        $('#frmPortfolioMenu .help-block').html('<p class="text-danger text-center">Menu Description can\'t be left empty</p>');
    } else if(!inputMenuOrder) {
        $('#frmPortfolioMenu .help-block').html('<p class="text-danger text-center">Menu Order can\'t be left empty</p>');
    } else if(!selectMenuStatus) {
        $('#frmPortfolioMenu .help-block').html('<p class="text-danger text-center">Menu Status can\'t be left empty</p>');
    } else if(selectMenuType == 'page' && !selectPortfolioPage) {
        $('#frmPortfolioMenu .help-block').html('<p class="text-danger text-center">Please select a page</p>');
    } else if(selectMenuType == 'url' && !inputMenuUrl) {
        $('#frmPortfolioMenu .help-block').html('<p class="text-danger text-center">Custom URL field can\'t be left empty</p>');
    } else {
        var req       = {};
        req.slug      = subdomain;
        req.menuId    = ((menuId=='new')?'':menuId);
        req.menuType  = selectMenuType;
        req.menuName  = inputMenuName;
        req.menuDescription = inputMenuDescription;
        req.menuOrder  = inputMenuOrder;
        req.menuStatus = selectMenuStatus;
        switch(selectMenuType) {
            case 'page' :  req.selectPortfolioPage = selectPortfolioPage;break;
            case 'url' :  req.inputMenuUrl = inputMenuUrl;break;
        }
        var res;
        req.action = "save-portfolio-menu";
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if(res.status == 1) {
                portfolioMenus = res.portfolioMenus;
                $('.js-portfolio-menus').html('<ul class="nav nav-pills nav-stacked list-pf-menu">'+
                                        '<li><a href="javascript:void" class="js-menu selected"><span class="checkbox"><input type="radio" name="optionMenu" value="new" checked /> New</span></a></li>'+
                                    '</ul>');
                fillPortfolioMenus(res);
                //$('#selectPortfolioPage option[value="'+selectPortfolioPage+'"]').remove();
                //$('#selectPortfolioPage').find('option:selected').remove();
                //console.log(portfolioPages);
                var pageIndex = findKeyByKeyValue(portfolioPages,'pageId',selectPortfolioPage);
                //delete portfolioPages[pageIndex];
                portfolioPages.splice(pageIndex,1);
                menuProcess(menuId);
                toastr.success('Menus Updated!');
            } else {
                toastr.error(res.message);
            }
        });
    }
    return false;
});
</script>