<script type="text/javascript">
var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
var userId = '<?php echo $_SESSION['userId']; ?>';
var userRole = '<?php echo $_SESSION['userRole']; ?>';
var userEmail = '<?php echo $_SESSION['userEmail']; ?>';
var sitePath	= '<?php echo $sitepath; ?>';
var sitePathCourses	= '<?php echo $sitepathCourses; ?>';
var sitePath404 = '<?php echo $sitepath404; ?>';
var sitepathManage = '<?php echo $sitepathManage; ?>';
var sitepathManageOld = '<?php echo $sitepathManageOld; ?>';
var sitepathManageIncludes = '<?php echo $sitepathManageIncludes; ?>';

var sitepathMarket = '<?php echo $sitepathMarket; ?>';
var sitepathPackages = '<?php echo $sitepathPackages; ?>';
var sitepathPackageDetail = '<?php echo $sitepathPackageDetail; ?>';
var sitepathCourses	 = '<?php echo $sitepathCourses	; ?>';
var sitepathCourseDetail = '<?php echo $sitepathCourseDetail; ?>';
var sitepathInstructor = '<?php echo $sitepathInstructor; ?>';
var sitepathInstitute = '<?php echo $sitepathInstitute; ?>';
var sitepathLogout = '<?php echo $sitepathLogout; ?>';
var sitepathPolicyTerms	= '<?php echo $sitepathPolicyTerms; ?>';
var sitepathPolicyPrivacy = '<?php echo $sitepathPolicyPrivacy; ?>';
var sitepathAbout = '<?php echo $sitepathAbout; ?>';
var sitepathContact	= '<?php echo $sitepathContact; ?>';

var sitepathManageInstitute = '<?php echo $sitepathManageInstitute; ?>';
var sitepathManageInstructor = '<?php echo $sitepathManageInstructor; ?>';
var sitepathManageContent = '<?php echo $sitepathManageContent; ?>';

var sitepathInstituteNotifications = '<?php echo $sitepathInstituteNotifications; ?>';
var sitepathInstituteCourses = '<?php echo $sitepathInstituteCourses; ?>';
var sitepathInstituteSubjects = '<?php echo $sitepathInstituteSubjects; ?>';
var sitepathInstituteAllExams = '<?php echo $sitepathInstituteAllExams; ?>';
var sitepathInstituteAllContent = '<?php echo $sitepathInstituteAllContent; ?>';
var sitepathInstitutePackages = '<?php echo $sitepathInstitutePackages; ?>';
var sitepathInstituteInstructors = '<?php echo $sitepathInstituteInstructors; ?>';
var sitepathInstituteStudents = '<?php echo $sitepathInstituteStudents; ?>';
var sitepathInstituteCoupons = '<?php echo $sitepathInstituteCoupons; ?>';
var sitepathInstituteDiscounts = '<?php echo $sitepathInstituteDiscounts; ?>';
var sitepathInstituteKeys = '<?php echo $sitepathInstituteKeys; ?>';
var sitepathInstituteProfile = '<?php echo $sitepathInstituteProfile; ?>';
var sitepathInstituteSettings = '<?php echo $sitepathInstituteSettings; ?>';
var sitepathInstitutePortfolios = '<?php echo $sitepathInstitutePortfolios; ?>';
var sitepathInstituteExams = '<?php echo $sitepathInstituteExams; ?>';
var sitepathInstituteSubjectiveExams = '<?php echo $sitepathInstituteSubjectiveExams; ?>';
var sitepathInstituteManualExams = '<?php echo $sitepathInstituteManualExams; ?>';

var sitepathManageNotifications		= '<?php echo $sitepathManageNotifications; ?>';
var sitepathManageCourses			= '<?php echo $sitepathManageCourses; ?>';
var sitepathManageSubjects			= '<?php echo $sitepathManageSubjects; ?>';
var sitepathManageInvitations		= '<?php echo $sitepathManageInvitations; ?>';
var sitepathManageSubjectsAssigned	= '<?php echo $sitepathManageSubjectsAssigned; ?>';
var sitepathManageAllExams			= '<?php echo $sitepathManageAllExams; ?>';
var sitepathManageAllContent		= '<?php echo $sitepathManageAllContent; ?>';
var sitepathManagePackages			= '<?php echo $sitepathManagePackages; ?>';
var sitepathManageInstructors		= '<?php echo $sitepathManageInstructors; ?>';
var sitepathManageStudents			= '<?php echo $sitepathManageStudents; ?>';
var sitepathManageCoupons			= '<?php echo $sitepathManageCoupons; ?>';
var sitepathManageDiscounts			= '<?php echo $sitepathManageDiscounts; ?>';
var sitepathManageKeys				= '<?php echo $sitepathManageKeys; ?>';
var sitepathManageStats				= '<?php echo $sitepathManageStats; ?>';
var sitepathManageProfile			= '<?php echo $sitepathManageProfile; ?>';
var sitepathManageSettings			= '<?php echo $sitepathManageSettings; ?>';
var sitepathManagePortfolios		= '<?php echo $sitepathManagePortfolios; ?>';
var sitepathManagePortfolio			= '<?php echo $sitepathManagePortfolio; ?>';
var sitepathManageExams				= '<?php echo $sitepathManageExams; ?>';
var sitepathManageSubjectiveExams	= '<?php echo $sitepathManageSubjectiveExams; ?>';
var sitepathManageManualExams		= '<?php echo $sitepathManageManualExams; ?>';
var sitepathManageSubmissions		= '<?php echo $sitepathManageSubmissions; ?>';
var terminologies					= $.parseJSON('<?php echo json_encode($global->terminology); ?>');

var page = '<?php echo $page; ?>';
<?php if (isset($subPage) && !empty(isset($subPage))) { ?>
var subPage = '<?php echo $subPage; ?>';
<?php }; ?>
var ApiEndPoint = ApiEndpoint = sitePath+'api/index.php';
var FileApiPoint = sitePath+'api/files1.php';
var TypeEndPoint = sitePath+'api/typeahead.php';
var currency = 2;
var slug = '<?php echo ((isset($slug))?($slug):('')); ?>';
var subdomain = '<?php echo ((isset($subdomain))?($subdomain):('')); ?>';
var courses = [];
var notifications;
var profileDetails;
var month = new Array();
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";
var MONTH=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var themeColors = ["white","default","dark","blue","blue-madison","blue-chambray","blue-ebonyclay","blue-hoki","blue-steel","blue-soft","blue-dark","blue-sharp","blue-oleo","green","green-meadow","green-seagreen","green-turquoise","green-haze","green-jungle","green-soft","green-dark","green-sharp","green-steel","grey","grey-steel","grey-cararra","grey-gallery","grey-cascade","grey-silver","grey-salsa","grey-salt","grey-mint","red","red-pink","red-sunglo","red-intense","red-thunderbird","red-flamingo","red-soft","red-haze","red-mint","yellow","yellow-gold","yellow-casablanca","yellow-crusta","yellow-lemon","yellow-saffron","yellow-soft","yellow-haze","yellow-mint","purple","purple-plum","purple-medium","purple-studio","purple-wisteria","purple-seance","purple-intense","purple-sharp","purple-soft"];
var darkThemeColors = ["dark","blue","blue-madison","blue-chambray","blue-ebonyclay","blue-hoki","blue-steel","blue-soft","blue-dark","blue-sharp","blue-oleo","green","green-meadow","green-seagreen","green-turquoise","green-haze","green-jungle","green-soft","green-dark","green-sharp","green-steel","red","red-pink","red-sunglo","red-intense","red-thunderbird","red-flamingo","red-soft","red-haze","red-mint","yellow","yellow-gold","yellow-casablanca","yellow-crusta","yellow-lemon","yellow-saffron","yellow-soft","yellow-haze","yellow-mint","purple","purple-plum","purple-medium","purple-studio","purple-wisteria","purple-seance","purple-intense","purple-sharp","purple-soft"];
var bootstrapColors = ["info", "warning", "danger", "success"]
toastr.options = {
	"closeButton": true,
	"debug": false,
	"positionClass": "toast-top-center",
	"onclick": null,
	"showDuration": "1000",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "1000",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
}
var summerShort = {
					height: 300,
					toolbar: [
						['style', ['bold', 'italic', 'underline', 'clear']]
					]
				};
<?php if (isset($courseId) && !empty($courseId)) { ?>
var courseId = "<?php echo $courseId; ?>";
<?php }; ?>
<?php if (isset($subjectId) && !empty($subjectId)) { ?>
var subjectId = "<?php echo $subjectId; ?>";
<?php }; ?>
<?php if (isset($subjectDetails) && !empty($subjectDetails)) { ?>
var subjectDetails = <?php echo json_encode($subjectDetails); ?>;
<?php }; ?>
<?php if (isset($examId) && !empty($examId)) { ?>
var examId = "<?php echo $examId; ?>";
<?php }; ?>
<?php if (isset($attemptId)) { ?>
var attemptId = "<?php echo $attemptId; ?>";
<?php }; ?>
<?php if (isset($chapterId)) { ?>
var chapterId = "<?php echo $chapterId; ?>";
<?php }; ?>
<?php if (isset($contentId)) { ?>
var contentId = "<?php echo $contentId; ?>";
<?php }; ?>
<?php if (isset($studentId)) { ?>
var studentId = "<?php echo $studentId; ?>";
<?php }; ?>
<?php if ($page == "courseDetail") { ?>
var demoVideo = "<?php echo $courseDetails->courseDetails["demoVideo"]; ?>";
<?php }; ?>
function signOut() {
	gapi.load('auth2', function() {
		gapi.auth2.init().then(() => {
			var auth2 = gapi.auth2.getAuthInstance();
			auth2.signOut().then(function () {
				console.log('User signed out.');
				window.location = sitepathLogout;
			});
		}).catch(function (err) {
			window.location = sitepathLogout;
		});
	});
}
$(document).ready(function(){

	initWeb3();

	fetchProfileDetails();
	fetchNotifications();
    
    $(".btn-logout").click(function(event) {
        /* Act on the event */
        signOut();
    });

	$('[data-scroll]').click(function(e){
		e.preventDefault();
		var scrollDiv = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(scrollDiv).offset().top - 100
		}, 1000);
	});

	$('body').tooltip({
		selector: '[data-toggle=tooltip]'
	});

	/*$('.btn-crypto-info').click(function(event) {
		getPortisInfo();
	});*/
    
    $('.btn-crypto-app').click(function(event) {
        /* Act on the event */
        $("#modalSetAddress").modal("show");
    });

	/*$('.btn-crypto-app').click(function(event) {
		checkCryptoApp();
	});*/

	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndPoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			//console.log(res);
			profileDetails = res;
			fillProfileDetails(res);
			fetchCourses();
			if (page == "home") {
				fetchCounters();
				fetchGraphs();
			} else if (page == "notificationsActivity") {
				fetchNotificationsActivity();
			} else if (page == "notificationsChat") {
				fetchNotificationsChat();
			} else if (page == "instructors") {
				fetchInstituteProfessors();
			} else if (page == "instructorsInvitations") {
				fetchinvitedprofessors();
			} else if (page == "students") {
				keys_available = get_keys_remaining();
				bindCoursesDropdown();
				StudentDetailsWithCourses();
			} else if (page == "studentsInvites") {
				keys_available = get_keys_remaining();
				bindCoursesDropdown();
				invitation_details();
			} else if (page == "studentsTransfer") {
				get_institute_students();
			} else if (page == "courseKeys") {
				fetchCoursekeys();
				/*get_students_of_course();
				fetchPurchasedCourses();*/
			} else if (page == "profile") {
				fillProfileInDetail();
			} else if (page == "settings") {
				checkSocialConnections();
				getAccountTerminology();
			} else if (page == "portfolios") {
				fetchPortfolios();
			} else if (page == "portfolio") {
				if (subPage == "portfolioPages") {
					fetchPortfolioCourses();
					fetchPageTypes();
				} else if (subPage == 'portfolioMenu') {
					fetchPortfolioMenus();
					fetchPortfolioPages();
				}
			} else if (page == "courseDetail") {
				//fetchCourseDetails();
				fetchSubjects();
				fetchPurchasedCoursesForImport();
				fetchCourseStudent_details();
				get_keys_remaining();
				fetchCoursekeys();
				fetchLicensingStatus();
				fetchCourseReviews();
			} else if (page == "courseAssignSubjects") {
				fetchCourseSubjectStudent();
				fetchCourseStudent_details();
				get_keys_remaining();
			} else if (page == "subjectDetail") {
				fetchIndependentExams();
				fetchSubjectChapters();
				fetchInstituteProfessors();
			} else if (page == "subjectStudents") {
				fetchSubjectStudents();
				get_keys_remaining();
			} else if (page == "subjectStudentGroups") {
				fetchSubjectStudents();
				get_keys_remaining();
			} else if (page == "subjectSections") {
				fetchContent();
			} else if (page == "subjectAnalytics") {
				fetchCourseStudentAnalytics();
			} else if (page == "subjectGrading") {
				fetchSubjectAllStuff();
			} else if (page == "subjectRewards") {
				fetchSubjectAllStuff();
			} else if (page == "subjectPerformance") {
				fetchSubjectStudents();
			} else if (page == "studentPerformance") {
				fetchStudentPerformance();
			} else if (page == "examCreate") {
				fetchChapters();
				fetchTemplates();
				fetchCourseDates();
			} else if (page == "examSettings") {
				fetchCourseDates();
				fetchChapters();
			} else if (page == "examDetail") {
				fetchExamName();
				fetchExamSections();
				fetchImportCourses();
			} else if (page == "examResult") {
				fetchStudentList();
			} else if (page == "examResultStudent") {
				fetchAttemptDetails();
			} else if (page == "examObjComments") {
				fetchTagAnalysis();
				getSmileys();
				getPercentRanges();
			} else if (page == "subjectiveExamCreate") {
				fetchCourseDates();
				fetchChapters();
			} else if (page == "subjectiveExamSettings") {
				fetchCourseDates();
				fetchChapters();
				fetchSubjectiveExam();
			} else if (page == "subjectiveExamQuestions") {
				fetchSubjectiveExam();
				fetchImportCourses();
			} else if (page == "subjectQuestions") {
				fetchSubjectQuestions();
				newQuestionOps();
			} else if (page == "subjectSubjectiveQuestions") {
				fetchSubjectiveQuestions();
				//newQuestionOps();
			} else if (page == "subjectSubmissionQuestions") {
				fetchSubmissionQuestions();
				newQuestionOps();
			} else if (page == "subjectiveExamCheck") {
				fetchSubjectiveExamAttempts();
			} else if (page == "subjectiveExamCheckAttempt") {
				fetchSubjectiveQuestions();
			} else if (page == "subjectiveExamResult") {
				fetchStudentList();
			} else if (page == "subjectiveExamResultStudent") {
				fetchSubjectiveQuestions();
			} else if (page == "subjectiveExamObjComments") {
				fetchTagAnalysis();
				getSmileys();
				getPercentRanges();
			} else if (page == "manualExamCreate") {
				fetchChapters();
			} else if (page == "manualExamImport") {
				fetchChapters();
			} else if (page == "manualExamSettings") {
				fetchChapters();
			} else if (page == "manualExamObjComments") {
				getSmileys();
				getPercentRanges();
			} else if (page == "manualExamResult") {
				fetchManualExam();
			} else if (page == "submissionCreate") {
				fetchCourseDates();
				fetchChapters();
				fetchSubjectStudentGroups();
			} else if (page == "submissionSettings") {
				fetchCourseDates();
				fetchChapters();
				fetchSubjectStudentGroups();
				fetchSubmission();
			} else if (page == "submissionQuestions") {
				fetchSubmission();
				fetchImportCourses();
			} else if (page == "submissionCheck") {
				fetchSubmissionAttempts();
			} else if (page == "submissionCheckAttempt") {
				fetchSubmissionQuestions();
			} else if (page == "submissionResult") {
				fetchStudentList();
			} else if (page == "submissionResultStudent") {
				fetchSubmissionQuestions();
			} else if (page == "submissionObjComments") {
				fetchTagAnalysis();
				getSmileys();
				getPercentRanges();
			} else if (page == "trash") {
				fetchDeletedStuff();
			}
		});
	}

	//function to fill user details
	function fillProfileDetails() {
		//console.log(data);
		if (profileDetails.profileDetails) {
			if (profileDetails.profileDetails.profilePic) {
				$('.dropdown-user .img-circle').attr("src",profileDetails.profileDetails.profilePic);
			};
			if (!profileDetails.profileDetails.name) {
				$('.dropdown-user .username').text(profileDetails.profileDetails.username);
			} else {
				$('.dropdown-user .username').text(profileDetails.profileDetails.name);
			}
		}
		$('.dropdown-user').removeClass('invisible');
	}

	//function to fetch notifications
	function fetchNotifications() {
		var req = {};
		var res;
		req.action = 'get-institute-notifications';
		ajaxRequest({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req),
			success:function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					notifications = res;
					fillNotifications(res);
				}
			}
		});
	}

	//function to fill notifications table
	function fillNotifications(data) {
		//console.log(data);
		$('#header_notification_bar .noti-count').html(data.notiCount);
		$('#header_inbox_bar .noti-count').html(data.chatCount);
		if (data.sitenotifications.length>0) {
			var html = '';
			for(var i = 0; i < data.sitenotifications.length; i++) {
				var d = new Date(data.sitenotifications[i].timestamp);
				d = timeSince(d.getTime()); //returns 1340220044000
				var notifLink = sitepathManageInstitute+'notifications/activity';
				html += '<li>'+
							'<a href="'+notifLink+'">'+
								'<span class="time">'+d+'</span>'+
								'<span class="details">'+
								'<span class="label label-sm label-icon label-'+((data.sitenotifications[i].status=="0")?"warning":"default")+'">'+
									'<i class="fa fa-bell-o"></i>'+
								'</span>'+
								'<span class="message" title="'+data.sitenotifications[i].message+'"> '+data.sitenotifications[i].message+' </span>'+
							'</a>'+
						'</li>';
			}
			$('#header_notification_bar .dropdown-menu-list').html(html);
		} else {
			$('#header_notification_bar .dropdown-menu-list').remove();
		}
		/*if (page == "home") {
			fillDashboardNotifications(data);
		};*/
		if (data.chatnotifications.length>0) {
			var html = '';
			for(var i = 0; i < data.chatnotifications.length; i++) {
				var message = "";
				var d = new Date(data.chatnotifications[i].timestamp);
				d = timeSince(d.getTime()); //returns 1340220044000
				switch(data.chatnotifications[i].type) {
					case '0':
						if (data.chatnotifications[i].room_type == "public") {
							message = data.chatnotifications[i].senderName+' just started conversation in \''+terminologies["subject_single"]+' : '+data.chatnotifications[i].subjectName+'\' of \''+terminologies["course_single"]+' : '+data.chatnotifications[i].courseName+'\'</span>';
						} else {
							message = data.chatnotifications[i].senderName+' just started conversation with '+data.chatnotifications[i].firstName+' '+data.chatnotifications[i].lastName+' in \''+terminologies["subject_single"]+' : '+data.chatnotifications[i].subjectName+'\' of \''+terminologies["course_single"]+' : '+data.chatnotifications[i].courseName+'\'</span>';
						}
						break;
					case '1':
						if (data.chatnotifications[i].room_type == "public") {
							message = data.chatnotifications[i].senderName+' just sent a message in \''+terminologies["subject_single"]+' : '+data.chatnotifications[i].subjectName+'\' of \''+terminologies["course_single"]+' : '+data.chatnotifications[i].courseName+'\'</span>';
						} else {
							message = data.chatnotifications[i].senderName+' just messaged '+data.chatnotifications[i].firstName+' '+data.chatnotifications[i].lastName+' in \''+terminologies["subject_single"]+' : '+data.chatnotifications[i].subjectName+'\' of \''+terminologies["course_single"]+' : '+data.chatnotifications[i].courseName+'\'</span>';
						}
						break;
				}
				html += '<li class="chat-parent">'+
							'<a href="'+sitepathManageNotifications+'chat" class="chat-button" data-id="'+data.chatnotifications[i].id+'" data-chat="1" target="_blank">'+
								'<span class="photo">'+
									'<img src="'+data.chatnotifications[i].profilePic+'" class="img-circle" alt=""> </span>'+
								'<span class="subject">'+
									'<span class="from"> '+data.chatnotifications[i].senderName+' </span>'+
									'<span class="time"> '+d+' </span>'+
								'</span>'+
								'<span class="message"> '+message+' </span>'+
							'</a>'+
						'</li>';
			}
			$('#header_inbox_bar .dropdown-menu-list').html(html);
		} else {
			$('#header_inbox_bar .dropdown-menu-list').remove();
		}
	}

	function fetchCourses() {
		var req = {};
		var res;
		req.action	= "get-all-courses";
		req.slug	= subdomain;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndPoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillCoursesMenu(res);
			if (page == "home" || page == "courses") {
				fillCourses(res);
			}
		});
	}

	function fillCoursesMenu (data) {
        if (data.courses.length>0) {
            $.each(data.courses, function (i, course) {
                if (i==0) {$('#navCourses .nav-link').after('<ul class="sub-menu"></ul>');}
                $('#navCourses .sub-menu').append(
                    '<li class="nav-item">'+
                        '<a href="'+sitepathManageCourses+course.id+'" class="nav-link">'+
                            '<span class="title">'+course.name+'</span>'+
                        '</a>'+
                    '</li>');
            });
        };
    }

});
</script>