<script type="text/javascript">
var TableDatatablesManaged = function () {

    var initTable1 = function () {

        var table = $('#sample_1');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // setup buttons extension: http://datatables.net/extensions/buttons/
            buttons: [
                { extend: 'print', className: 'btn dark btn-outline' },
                { extend: 'pdf', className: 'btn green btn-outline' },
                { extend: 'csv', className: 'btn purple btn-outline' }
            ],

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 15, 20, -1],
                [10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {  // set default column settings
                    'orderable': false,
                    'targets': [0]
                }, 
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery('#sample_1_wrapper');

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).prop("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).prop("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
        }

    };

}();
function fetchinvitedprofessors() {
    var req = {};
    var res;
    req.action = 'get-invited-professors-details';
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        fillinvitedprofessors(res);
        //console.log(res);
    
    });
}
function fillinvitedprofessors(data) {
    var html='';
    var name, uid, status, action;
    if ( $.fn.DataTable.isDataTable( '#sample_1' ) ) {
        $('#sample_1').DataTable().destroy();
    }
    //$('#sample_1').DataTable().destroy();
    if (data.check) {
        $.each(data.check, function(key, obj) {
            name = obj.firstName+' '+obj.lastName;
            uid = obj.userId;
            status = ((obj.status==0)?'<span class="label label-sm label-warning"><i class="fa fa-info-circle"></i> Pending</span>':((obj.status==1)?'<span class="label label-sm label-success"><i class="fa fa-check-circle"></i> Accepted</span>':'<span class="label label-sm label-danger"><i class="fa fa-ban"></i> Rejected</span>'));
            action = ((obj.status!=1)?'<a href="javascript:void(0)" class="btn btn-circle btn-xs green btn-resend"> Resend</a>':'');
            html+='<tr data-id="' + obj.id + '" class="odd gradeX">'+
                        '<td>' + (key+1) + '</td>'+
                        '<td>' + name + '</td>'+
                        '<td><a href="mailto:' + obj.email + '"> ' + obj.email + ' </a></td>'+
                        '<td>' + obj.contactMobilePrefix + '-' + obj.contactMobile + '</td>'+
                        '<td>' + status + '</td>'+
                        '<td>' + action + '</td>'+
                    '</tr>';
        });
        $('#sample_1 tbody').find('tr').remove();
        $('#sample_1 tbody').html(html);
        TableDatatablesManaged.init();
        addEventListeners();
    } else {
        html+='<tr class="odd gradeX">'+
                        '<td colspan="6">No invitations found</td>'+
                    '</tr>';
        $('#sample_1 tbody').find('tr').remove();
        $('#sample_1 tbody').html(html);
    }
}
function updateInvitation(elem) {
    var req = {};
    var res;
    req.action = "update-invitation";
    req.sno = elem.parents('tr').attr('data-id');
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'async': false,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        fetchinvitedprofessors();
    });
}
function addEventListeners() {
    $('#sample_1').on('click', '.btn-resend', function() {
        updateInvitation($(this));
    });
}
/*$(document).ready(function() {
    TableDatatablesManaged.init();
});*/
</script>