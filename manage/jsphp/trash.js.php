<script type="text/javascript">
function fetchDeletedStuff() {
	var req = {};
    var res;
    req.action ="get-deleted-stuff";
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1)
            fillDeletedStuff(res);
        else
            toastr.error(res.message);
    });
}
function fillDeletedStuff(data) {
	if (data.subjects.length>0) {
		var subjects = data.subjects;
		for (var i = 0; i < subjects.length; i++) {
			$(".js-trash-list").append('<tr data-subject="'+subjects[i].id+'">'+
					'<td>'+subjects[i]['name']+'</td>'+
					'<td>Subject</td>'+
					'<td><a href="javascript:void(0)" class="btn btn-circle btn-sm green restore-subject" data-toggle="tooltip" title="Restore"><i class="fa fa-recycle"></i></a></td>'+
				'</tr>');
		}
	}
	if (data.exams.length>0) {
		var exams = data.exams;
		for (var i = 0; i < exams.length; i++) {
			$(".js-trash-list").append('<tr data-exam="'+exams[i].id+'">'+
					'<td>'+exams[i]['name']+'</td>'+
					'<td>Exam</td>'+
					'<td><a href="javascript:void(0)" class="btn btn-circle btn-sm green restore-exam" data-toggle="tooltip" title="Restore"><i class="fa fa-recycle"></i></a></td>'+
				'</tr>');
		}
	}
	if (data.assignments.length>0) {
		var assignments = data.assignments;
		for (var i = 0; i < assignments.length; i++) {
			$(".js-trash-list").append('<tr data-exam="'+assignments[i].id+'">'+
					'<td>'+assignments[i]['name']+'</td>'+
					'<td>Assignments</td>'+
					'<td><a href="javascript:void(0)" class="btn btn-circle btn-sm green restore-exam" data-toggle="tooltip" title="Restore"><i class="fa fa-recycle"></i></a></td>'+
				'</tr>');
		}
	}
	if (data.subjectiveexams.length>0) {
		var subjectiveexams = data.subjectiveexams;
		for (var i = 0; i < subjectiveexams.length; i++) {
			$(".js-trash-list").append('<tr data-exam="'+subjectiveexams[i].id+'">'+
					'<td>'+subjectiveexams[i]['name']+'</td>'+
					'<td>Subjective Exams</td>'+
					'<td><a href="javascript:void(0)" class="btn btn-circle btn-sm green restore-subjective-exam" data-toggle="tooltip" title="Restore"><i class="fa fa-recycle"></i></a></td>'+
				'</tr>');
		}
	}
	if (data.manualexams.length>0) {
		var manualexams = data.manualexams;
		for (var i = 0; i < manualexams.length; i++) {
			$(".js-trash-list").append('<tr data-exam="'+manualexams[i].id+'">'+
					'<td>'+manualexams[i]['name']+'</td>'+
					'<td>Manual Exams</td>'+
					'<td><a href="javascript:void(0)" class="btn btn-circle btn-sm green restore-manual-exam" data-toggle="tooltip" title="Restore"><i class="fa fa-recycle"></i></a></td>'+
				'</tr>');
		}
	}
	if (data.submissions.length>0) {
		var submissions = data.submissions;
		for (var i = 0; i < submissions.length; i++) {
			$(".js-trash-list").append('<tr data-exam="'+submissions[i].id+'">'+
					'<td>'+submissions[i]['name']+'</td>'+
					'<td>Submissions</td>'+
					'<td><a href="javascript:void(0)" class="btn btn-circle btn-sm green restore-submission" data-toggle="tooltip" title="Restore"><i class="fa fa-recycle"></i></a></td>'+
				'</tr>');
		}
	}
}
$(document).ready(function() {
	$('#listSections').on('click','.restore-subject', function (e) {
        e.preventDefault();
        var con = confirm("Are you sure you want to restore this subject?");
        if(con) {
            var subject = $(this).parents('tr');
            var sid = subject.attr('data-subject');
            var req = {};
            req.action = "restore-subject";
            req.subjectId = sid;
            $.ajax({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if (res.status == 1) {
                    toastr.success('Subject restored successfully');
                    subject.remove();
                } else {
                    toastr.error(res.message);
                }
            });
        }
    });
	$('#listSections').on("click", ".restore-exam", function(){
        var con = confirm("Are you sure you want to restore this Exam/Assignment.");
        if (con) {
        	var tr = $(this).closest('tr');
            var req = {};
            req.action = 'restore-exam';
            req.examId = tr.attr('data-exam');
            $.ajax({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if (res.status == 0)
                    toastr.error(res.message);
                else {
                    toastr.success('Exam/Assignment restored successfully');
                    tr.remove();
                }
            });
        }
    });
    $('#listSections').on("click", ".restore-subjective-exam", function(){
        var thiz = $(this);
        var con = confirm("Are you sure you want to restore this Subjective Exam?");
        if(con) {
        	var tr = $(this).closest('tr');
            var examId = tr.attr('data-exam');
            var req = {};
            var res;
            req.action = 'restore-subjective-exam';
            req.examId = examId;
            $.ajax({
                'type'  : 'post',
                'url'   : ApiEndPoint,
                'data'  : JSON.stringify(req)
            }).done(function (res) {
                res =  $.parseJSON(res);
                if(res.status == 0) {
                    toastr.error(res.message);
                } else {
                    toastr.success('Subjective Exam restored successfully');
                    tr.remove();
                }
            });
        }
    });
    $('#listSections').on("click", ".restore-manual-exam", function(){
        var thiz = $(this);
        var con = confirm("Are you sure you want to restore this Manual Exam?");
        if(con) {
        	var tr = $(this).closest('tr');
            var examId = tr.attr('data-exam');
            var req = {};
            var res;
            req.action = 'restore-manual-exam';
            req.examId = examId;
            $.ajax({
                'type'  : 'post',
                'url'   : ApiEndPoint,
                'data'  : JSON.stringify(req)
            }).done(function (res) {
                res =  $.parseJSON(res);
                if(res.status == 0) {
                    toastr.error(res.message);
                } else {
                    toastr.success('Manual Exam restored successfully');
                    tr.remove();
                }
            });
        }
    });
    $('#listSections').on("click", ".restore-submission", function(){
        var thiz = $(this);
        var con = confirm("Are you sure you want to restore this Submission?");
        if(con) {
        	var tr = $(this).closest('tr');
            var examId = tr.attr('data-exam');
            var req = {};
            var res;
            req.action = 'restore-submission';
            req.examId = examId;
            $.ajax({
                'type'  : 'post',
                'url'   : ApiEndPoint,
                'data'  : JSON.stringify(req)
            }).done(function (res) {
                res =  $.parseJSON(res);
                if(res.status == 0) {
                    toastr.error(res.message);
                } else {
                    toastr.success('Submission restored successfully');
                    tr.remove();
                }
            });
        }
    });
});
</script>