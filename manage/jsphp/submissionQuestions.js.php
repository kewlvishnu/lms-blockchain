<script type="text/javascript">
var questions;
var index=-1;
var reqTag = {};
reqTag.action = 'tags';
var ms = $('#magicsuggest').magicSuggest({
	valueField: 'name',
	data: TypeEndPoint,
	dataUrlParams: reqTag
});
$(ms).on('keydown', keydownAlphaNumeric);
var msfilter = $('#tagsfilter').magicSuggest({
	valueField: 'name',
	data: TypeEndPoint,
	dataUrlParams: reqTag
});
$(msfilter).on('keydown', keydownAlphaNumeric);
function fetchSubmission() {
	var req = {};
	var res;
	req.action = 'get-submission-questions';
	req.subjectId = subjectId;
	req.courseId = courseId;
	req.examId =   examId;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
		{
			toastr.error(res.message);
			//window.location.href =sitePath+'404';
		}				
		else{
			if (res.questions) {
				if (res.questions.length>0) {
					$('.makelive').prop('disabled', false);
				}
			}
			fillSubmission(res);
		}
			
	});
}
function fillSubmission(data) {
	$('#listQuestions').html('');
	if(data.exam.status == 2)
	{
		$('.makeNotlive').removeClass('hide');
		$('.makelive').addClass('hide');
		
	}
	else{
		$('.makeNotlive').addClass('hide');
		$('.makelive').removeClass('hide');
	}
	//questions = data.questions;
	fillSubjectQuestions(data);
}

function fillSubjectQuestions(data) {
	$('#sectionQuestions').html('');
	//if (data.questions.length>0) {
		var queshtml = '';
		questions = data.questions;
		var status = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="Green symbol represents that the  question  is complete. The question would be counted in the no of entered questions"></i>';
		queshtml+= '<div class="portlet light portlet-fit no-space margin-bottom-10">'+
	                    '<div class="portlet-body no-space">'+
	                        '<div class="mt-element-list mt-element-list-sm questions-sidebar-list">'+
								'<div class="mt-list-head list-simple ext-1 font-white bg-grey-dark">'+
								    '<div class="list-head-title-container">'+
								        '<h3 class="list-title">Submission</h3>'+
								    '</div>'+
								'</div>'+
	                            '<div class="mt-list-head list-default ext-1 green-jungle">'+
	                                '<div class="row">'+
	                                    '<div class="col-xs-8">'+
	                                        '<div class="list-head-title-container">'+
	                                            '<h3 class="list-title uppercase sbold no-margin">SUBMISSION QUESTIONS</h3>'+
	                                        '</div>'+
	                                    '</div>'+
	                                    '<div class="col-xs-4 text-right">'+
	                                        '<a class="btn dark btn-xs new-question" href="javascript:;">'+
	                                            '<i class="fa fa-plus"></i>'+
	                                        '</a>'+
                                            /*'<a class="btn dark btn-sm" role="button" data-toggle="collapse" href="#collapse0" aria-expanded="true" aria-controls="collapse0"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></a>'+*/
                                        '</div>'+
	                                '</div>'+
	                            '</div>'+
	                            '<div class="" id="collapse0">'+
		                            '<div class="mt-list-container list-default ext-1 no-padding">'+
		                                /*'<div class="mt-list-title uppercase">List of questions'+
		                                    '<span class="badge badge-default pull-right bg-hover-green-jungle">'+
		                                        '<a class="font-white new-question" href="javascript:;">'+
		                                            '<i class="fa fa-plus"></i>'+
		                                        '</a>'+
		                                    '</span>'+
		                                '</div>'+*/
		                                '<ul class="questions">';
	    var questionHtml = '';
	    if(questions.length > 0) {
			var questionNumber = 0;
			for(var j=0; j<questions.length; j++) {
				var dispQuestionNumber = (questionNumber + 1);
				questionNumber++;
				var questionText = $(questions[j]['question']).text();
				if(questionText.length > 35)
					questionText = questionText.substring(0, 35) + '...';
				questionHtml+= '<li class="mt-list-item done" data-id="'+questions[j]['id']+'" data-status="'+questions[j]['status']+'" data-category="'+questions['id']+'">'+
                                '<div class="list-icon-container">'+
                                    '<a href="javascript:;">'+
                                    	status+
                                        //'<i class="icon-check"></i>'+
                                    '</a>'+
                                '</div>'+
                                '<div class="list-datetime"> <a href="javascript:void(0)" class="btn btn-danger btn-xs remove-question"><i class="fa fa-trash"></i></a> </div>'+
                                '<div class="list-item-content">'+
                                    '<h3 class="uppercase">'+
                                        '<a href="javascript:;">SUBMISSION TASK</a>'+
                                    '</h3>'+
                                    '<p class="text-overflow">'+ questionText +'</p>'+
                                '</div>'+
                            '</li>';
			}
		}
		else {
			questionHtml += '<li class="mt-list-item">No question found</li>';
		}
		queshtml += questionHtml;
		queshtml +=				'</ul>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>';
        //console.log(queshtml);
		$('#sectionQuestions').append(queshtml);
	//}
}
function fetchQuestionDetails(id) {
	$('#sectionNewQuestion .cancel-button').click();
	var req = {};
	req.action = 'get-submission-question-detail';
	req.examId = examId;
	req.questionId = id;
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillQuestionEditQuestion(res);
		}
		else
			toastr.error(res.exception);
	});
}
function fillQuestionEditQuestion(data) {
	if(data.question[0].status == 2) {
		var warningSpan = '<div class="text-danger" style="margin-left: 15px;margin-right: 15px;">Duplicate : Question copied from same Assignment/Exam. Edit question to save and increase question count in category.</div>';
		$(warningSpan).insertBefore('#questionId');
	}
	else
		$('#sectionNewQuestion').find('div.text-danger').remove();
	$('#sectionNewQuestion').removeClass('hide');
	$('#questionId').val(data.question[0]['id']);
	CKEDITOR.instances['question'].setData(data.question[0]['question']);
	$('#marks').val(data.question[0]['marks']);
	$('#selectDifficulty').val(data.question[0]['difficulty']);
	ms.setSelection(data.tags);
	CKEDITOR.instances['desc'].setData(data.question[0]['description']);
}
function newQuestionOps() {
	$('#sectionNewQuestion').removeClass('hide');
	$('#sectionNewQuestion .cancel-button').click();
	$('#questionId').val(0);
	$('#marks').val(0);
	$('.js-file-path').html('').attr('data-path', '');
}
function fetchImportCourses() {
	var req = {};
	var res;
	req.action = 'get-courses-for-exam';
	req.import = true;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 1)
			fillImportCourseSelect(res);
	});
}

function fillImportCourseSelect(data) {
	$('.courseSelect').find('option').remove();
	var html = '<option value="0">Select '+terminologies["course_single"]+'</option>';
	for(var i=0; i<data.courses.length; i++) {
		html += '<option value="' + data.courses[i].id + '">' + data.courses[i].name + '</option>';
	}
	$('.courseSelect').append(html);
	$('.courseSelect').prop('disabled', false);
}

function fetchImportSubjectsQuestions() {
	var req = {};
	var res;
	req.action = 'get-subjects-for-exam';
	req.import = true;
	req.courseId = $('#importSubjectQuestionsModal .courseSelect').val();
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 1)
			fillImportSubjectQuestions(res);
	});
}

function fillImportSubjectQuestions(data) {
	$('#importSubjectQuestionsModal .subjectSelect').find('option').remove();
	var html = '<option value="0">Select '+terminologies["subject_single"]+'</option>';
	for(var i=0; i<data.subjects.length; i++) {
		html += '<option value="' + data.subjects[i].id + '">' + data.subjects[i].name + '</option>';
	}
	$('#importSubjectQuestionsModal .subjectSelect').append(html);
	$('#importSubjectQuestionsModal .subjectSelect').attr('disabled', false);
}

function fetchImportSubjectExams() {
	var req = {};
	var res;
	req.action = 'get-submissions-of-subject';
	req.import = true;
	req.subjectId = $('#importSubjectQuestionsModal .subjectSelect').val();
	req.examId = examId;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 1)
			fillImportSubjectExams(res);
	});
}

function fillImportSubjectExams(data) {
	$('#importSubjectQuestionsModal .assignmentSelect').find('option').remove();
	var html = '<option value="0">All Exams</option>';
	for(var i=0; i<data.exams.length; i++) {
		html += '<option value="' + data.exams[i].id + '">' + data.exams[i].name + '</option>';
	}
	$('#importSubjectQuestionsModal .assignmentSelect').append(html);
	$('#importSubjectQuestionsModal .assignmentSelect').attr('disabled', false);
	$('#filterDifficulty').attr('disabled', false);
}

function fetchSubjectQuestions() {
	var req = {};
	var res;
	req.action = 'get-submission-questions-import';
	req.subjectId = $('#importSubjectQuestionsModal .subjectSelect').val();
	req.currExamId = examId;
	req.examId = $('#importSubjectQuestionsModal .assignmentSelect').val();
	req.difficulty = $('#filterDifficulty').val();
	req.categoryId = $('#modalImportSubjectQuestions').attr('data-category');
	req.tags = msfilter.getValue();
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 0) {
			toastr.error(res.message);
		} else {
			fillImportedSubjectQuestions(res);
		}
	});
}

function fillImportedSubjectQuestions(data) {
	console.log(data);
	$('#listSubjectQuestions tbody').html('');
	var html = '';
	if(data.questions.length == 0)
		html += '<tr><td></td><td></td><td colspan="3">No Questions Found</td><td></td></tr>';
	for(var i=0; i<data.questions.length; i++) {
		html += '<tr data-id="' + data.questions[i].id + '" data-questionType="' + data.questions[i].questionType + '">'
					+ '<td><input type="checkbox" class="checkbox-inline selectbox"></td>'
					+ '<td>';
		if(data.questions[i].status == 0)
			html += '<i class="fa fa-minus-circle text-danger"></i></td>';
		else
			html += '<i class="fa fa-check-circle text-success"></i></td>';
		var type= '';
		var shortQuestion = $(data.questions[i].question).text();
		type = 'Question';
		if(shortQuestion.length > 35)
			shortQuestion = shortQuestion.substring(0, 33) + '...';
		html += '<td colspan="3">' + shortQuestion + '</td>';
		html += '<td>' + type + '</td>'
				+ '</tr>';
	}
	$('#listSubjectQuestions table tbody').append(html);
	$('#listSubjectQuestions table').removeClass('hide');
}

$(document).ready(function(){
	//select all event listener
	$('#selectSQAll').on('change', function(){
		var fill = $(this).prop('checked');
		$('#importSubjectQuestionsModal tbody tr').each(function() {
			if($(this).css('display') != 'none')
				$(this).find('input[type="checkbox"]').prop('checked', fill);
		});
	});
	$('#sectionQuestions').on('click', '.mt-list-item', function() {
		if($(this).attr('data-id') != undefined) {
			fetchQuestionDetails($(this).attr('data-id'));
		}
	});
	$('#sectionQuestions').on('click', '.remove-question', function(e) {
		e.stopPropagation();
		var con = false;
		con = confirm("Are you sure to delete this question");
		if(con) {
			$('#sectionNewQuetion .cancel-button').click();
			var req = {};
			req.action = 'delete-submission-question';
			req.examId = examId;
			req.questionId = $(this).parents('.mt-list-item').attr('data-id');
			req.status = $(this).parents('.mt-list-item').attr('data-status');
			req.questionType = $(this).parents('.mt-list-item').attr('data-questionType');
			if(req.questionType == 4)
				req.amount = $(this).parents('.mt-list-item').find('.totalQuestions').text();
			//req.categoryId = $(this).parents('.mt-list-item').attr('data-category');
			req.sectionId = $('#sectionName').attr('data-id');
			req.subjectId = subjectId;
			$('#sectionNewQuestion .cancel-button').click();
			$.ajax({
				'type'	: 'post',
				'url'	: ApiEndPoint,
				'data'	: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else if(res.status == 1) {
					fetchSubmission();
					newQuestionOps();
				}
			});
		}
	});
	
	$('#sectionNewQuestion .cancel-button').on('click', function() {
		$('.help-block').remove();
		$('#sectionNewQuestion').find('div.text-danger').remove();

		$('#sectionNewQuestion .generated').remove();
		$('#questionId').val(0);
		$('#marks').val(0);
		$('#question').val('');
		$('#selectDifficulty').val(0);
		ms.clear();
		$('#sectionNewQuestion').find('input[type="text"]').val('');
		$('#sectionNewQuestion').find('input[type="radio"]').prop('checked', false);
		$('#sectionNewQuestion').find('input[type="checkbox"]').prop('checked', false);
		//removing text from ckeditor textboxes
		for(instance in CKEDITOR.instances) {
			CKEDITOR.instances[instance].setData('');
			CKEDITOR.instances[instance].fire('blur');
		}
	});
	
	$('#newQuestion').on('click', function() {
		newQuestionOps();
	});

	$('body').on('click', '.new-question', function() {
		$('#newQuestion').trigger('click');
	});
	
	$('#sectionNewQuestion .save-button').on('click', function() {
		var marks = $("#marks").val();
		if($(CKEDITOR.instances['question'].getData()).text().length <= 3) {
			toastr.error("Please give your question text here which should be longer than 3 characters.");
		} else if (marks<1) {
			toastr.error("Please enter some marks");
		} else {
			var req = {};
			if($('#questionId').val() == 0)
				req.action = 'save-submission-question';
			else {
				req.action = 'edit-submission-question';
				req.questionId = $('#questionId').val();
			}
			req.examId = examId;
			req.question = CKEDITOR.instances['question'].getData();
			req.marks = marks;
			req.difficulty = $("#selectDifficulty").val();
			req.desc = CKEDITOR.instances['desc'].getData();
			req.subjectId = subjectId;
			req.courseId=courseId;
			req.tags = ms.getValue();
			$.ajax({
				'type'	: 'post',
				'url'	: ApiEndPoint,
				'data'	: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					fetchSubmission();
					newQuestionOps();
					scrollToElem("#sectionQuestions", 1000);
				}
			});
		}
	});
	$('#importSubjectQuestionsModal .courseSelect').on('change', function() {
		$('#importSubjectQuestionsModal .subjectSelect').attr('disabled', true);
		$('#importSubjectQuestionsModal .subjectSelect').val(0);
		$('#importSubjectQuestionsModal .assignmentSelect').attr('disabled', true);
		$('#importSubjectQuestionsModal .assignmentSelect').val(0);
		$('#filterDifficulty').attr('disabled', true);
		$('#filterDifficulty').val(0);
		$('#importSubjectQuestionsModal table').addClass('hide');
		if($(this).val() != 0)
			fetchImportSubjectsQuestions();
	});	
	$('#importSubjectQuestionsModal .subjectSelect').on('change', function() {
		$('#importSubjectQuestionsModal .assignmentSelect').attr('disabled', true);
		$('#importSubjectQuestionsModal .assignmentSelect').val(0);
		$('#filterDifficulty').attr('disabled', true);
		$('#filterDifficulty').val(0);
		$('#importSubjectQuestionsModal table').addClass('hide');
		if($(this).val() != 0) {
			fetchImportSubjectExams();
			fetchSubjectQuestions();
		}
	});	
	$('#importSubjectQuestionsModal .assignmentSelect').on('change', function() {
		$('#importSubjectQuestionsModal table').addClass('hide');
		fetchSubjectQuestions();
	});	
	$('#filterDifficulty').on('change', function() {
		$('#importSubjectQuestionsModal table').addClass('hide');
		fetchSubjectQuestions();
	});
	$(msfilter).on('selectionchange', function(e,m){
		//console.log("values: " + JSON.stringify(this.getValue()));
		fetchSubjectQuestions();
	});	
	$('#importSubjectQuestionsModal').on('hidden.bs.modal', function() {
		$('#importSubjectQuestionsModal .cancel-button').click();
	});	
	//For cancel button modal reset
	$('#importSubjectQuestionsModal .cancel-button').on('click', function() {
		$('#importSubjectQuestionsModal .subjectSelect').attr('disabled', true);
		$('#importSubjectQuestionsModal .subjectSelect').val(0);
		$('#importSubjectQuestionsModal .assignmentSelect').attr('disabled', true);
		$('#importSubjectQuestionsModal .assignmentSelect').val(0);
		$('#importSubjectQuestionsModal .courseSelect').val(0);
		$('#filterDifficulty').attr('disabled', true);
		$('#filterDifficulty').val(0);
		$('#importSubjectQuestionsModal table').addClass('hide');
	});
	//to import selected questions from question bank
	$('#modalImportSubjectQuestions').on('click', function() {
		if($('#importSubjectQuestionsModal .importCategorySelect').val() != 0) {
			var flag = false;
			$('input[type="checkbox"]').each(function() {
				if($(this).prop('checked')) {
					flag = true;
					return;
				}
			});
			if(flag) {
				$(this).attr('disabled', true);
				var req = {};
				var res;
				req.action = 'import-submission-questions';
				req.questions = [];
				$('#importSubjectQuestionsModal table tbody tr').each(function() {
					var obj = {};
					if($(this).find('input[type="checkbox"]:not(#selectAll)').prop('checked')) {
						obj.questionId = $(this).attr('data-id');
						obj.questionType = $(this).attr('data-questionType');
						obj.categoryId = $('#modalImportSubjectQuestions').attr('data-category');
						obj.parentId = 0;
						obj.examId = examId;
						req.questions.push(obj);
					}
				});
				$.ajax({
					'type'  : 'post',
					'url'   : ApiEndPoint,
					'data' 	: JSON.stringify(req)
				}).done(function (res) {
					$('#modalImportSubjectQuestions').attr('disabled', false);
					res =  $.parseJSON(res);
					$("#importSubjectQuestionsModal").modal('hide');
					if(res.status == 1)
						fetchSubmission();
					else
						toastr.error(res.message);
				});
			}
			else
				toastr.error('Please select at least one question.');
		}
		else
			toastr.error("Please select a import category first.");
	});
	$('.makelive').click(function(){
		if(questions.length>0){
			var req={};
			req.submissionId=examId;
			req.action = 'live-submission';
			//console.log(submissionId);
			$.ajax({
			'type'  : 'post',
			'url'   : ApiEndPoint,
			'data' 	: JSON.stringify(req)
			}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				toastr.error(res.message);
			else
			{	
				toastr.success("Submission is live now");
				location.reload();
			}
	    	});
		}else{
			toastr.error("Unable to make submission live. Please add questions.");
		}
	});	
	$('.makeNotlive').click(function(){
		var req={};
		req.submissionId=examId;
		req.action = 'unlive-submission';
		//console.log(submissionId);
		$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				toastr.error(res.message);
			else
			{	
				toastr.success("Submission is offline now");
				location.reload();
			}
		});
	});
});
</script>