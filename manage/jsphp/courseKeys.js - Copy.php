<script type="text/javascript">
var TableDatatablesManaged = function () {

    var initTable1 = function () {

        var table = $('#tableTotalKeys');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // setup buttons extension: http://datatables.net/extensions/buttons/
            buttons: [],

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });
    }

    var initTable2 = function () {

        var table = $('#tableCourseWise');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // setup buttons extension: http://datatables.net/extensions/buttons/
            buttons: [],

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });
    }

    var initTable3 = function () {

        var table = $('#tablePurchasedCourses');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // setup buttons extension: http://datatables.net/extensions/buttons/
            buttons: [],

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });
    }

    return {

        //main function to initiate the module
        init1: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
        },

        init2: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable2();
        },

        init3: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable3();
        }

    };

}();
var total_puchased_keys = '';
var UserCurrency = 2;
var key_rate = 0;
var display_current_currency = '$';
$(document).ready(function(){

    //purchaseNow button click event listener
    $('#purchaseNow').on('click', function() {
        console.log($('#inputTotalKeys').val());
        if($('#inputTotalKeys').val() != '' && parseInt($('#inputTotalKeys').val()) > 0) {
            $('#modalPurchaseCourseKeys').modal('hide');
            var keys = $('#inputTotalKeys').val();
            var amount = keys * key_rate;
            //$('#inputTotalKeys').html('');
            $('#modalPurchaseNowCourseKeys').modal('show');
            $('#modalPurchaseNowCourseKeys .price-mul').html(display_current_currency + key_rate);
            $('#modalPurchaseNowCourseKeys .units').html(keys);
            $('#modalPurchaseNowCourseKeys .amount').html(display_current_currency + amount);
            $('#modalPurchaseNowCourseKeys .you-pay').html(display_current_currency + amount);
        } else {
            toastr.error('Invalid entry');
        }
    });

    //event handler for PayU Money
    $('#payUMoneyButton').on('click', function() {
        var req = {};
        var res;
        req.action = 'purchaseCourseKeys';
        req.quantity = $('#inputTotalKeys').val();
        req.currency = UserCurrency;
        $.ajax({
            'type'  : 'post',
            'url'   : ApiEndPoint,
            'data'  : JSON.stringify(req), 
        }).done(function(res) {
            res = $.parseJSON(res);
            if(res.status == 1) {
                if(res.paymentSkip == 1)
                    location.reload();
                else if(res.method == 2){
                    var html = '';
                    html = '<form action="'+res.url+'" method="post" id="payUForm">'
                        + '<input type="hidden" name="txnid" value="' + res.txnid + '">'
                        + '<input type="hidden" name="key" value="'+res.key+'">'
                        + '<input type="hidden" name="amount" value="'+res.amount+'">'
                        + "<input type='hidden' name='productinfo' value='"+res.productinfo+"'>"
                        + '<input type="hidden" name="firstname" value="'+res.firstname+'">'
                        + '<input type="hidden" name="email" value="'+res.email+'">'
                        + '<input type="hidden" name="surl" value="'+res.surl+'">'
                        + '<input type="hidden" name="furl" value="'+res.furl+'">'
                        + '<input type="hidden" name="curl" value="'+res.curl+'">'
                        + '<input type="hidden" name="furl" value="'+res.furl+'">'
                        + '<input type="hidden" name="hash" value="'+res.hash+'">'
                        + '<input type="hidden" name="service_provider" value="'+res.service_provider+'">';
                    + '</form>'
                    $('body').append(html);
                    $('#payUForm').submit();
                }
                else if(res.method == 1) {
                    var html = '';
                    html += '<form action="' + res.url + '" method="post" id="paypalForm">'
                        + '<!-- Identify your business so that you can collect the payments. -->'
                        + '<input type="hidden" name="business" value="' + res.business + '">'
                        + '<!-- Specify a Buy Now button. -->'
                        + '<input type="hidden" name="cmd" value="_xclick">'
                        + '<!-- Specify details about the item that buyers purchase. -->'
                        + '<input type="hidden" name="item_name" value="Enrollment Keys">'
                        + '<input type="hidden" name="amount" value="' + res.rate + '">'
                        + '<input type="hidden" name="currency_code" value="USD">'
                        + '<input type="hidden" name="quantity" value="' + res.quantity + '">'
                        + '<input type="hidden" name="invoice" value="' + res.orderId + '">'
                        + '<input type="hidden" name="item_number" value="' + res.orderId + '">'
                        + '<input type="hidden" name="return" value="' + res.surl + '" />'
                        + '<input type="hidden" name="notify_url" value="' + res.nurl + '" />'
                        + '<input type="hidden" name="cancel_return" value="' + res.curl + '" />'
                        + '<input type="hidden" name="lc" value="US" />'
                        + '<!-- Display the payment button. -->'
                        + '<input type="image" name="submit" border="0" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online">'
                        + '<img alt="" border="0" width="1" height="1"  src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >'
                    + '</form>';
                    $('body').append(html);
                    $('#paypalForm').submit();
                }
            }else{
                toastr.error(res.message);
            }
        });
    });
    $('#inputTotalKeys').keyup(function () {
        var total = $(this).val() * key_rate;
        $('#totalkeyrate').text(total);
        $('#totalkeyrate').append(display_current_currency);
    });
    $("#inputTotalKeys").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            toastr.error('Enter Only Numbers', terminologies["course_single"]+' Keys');
            //$('.help-block').fadeOut("slow");
            // ("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
    $('#keysPurchase').click(function () {
        total_puchased_keys = $.trim($('#inputTotalKeys').val());
        if (total_puchased_keys == '' || total_puchased_keys == 0) {
            toastr.error('Invalid entry', terminologies["course_single"]+' keys');
            return false;
        }
        $('#modalPurchaseCourseKeys').modal('hide');
        $('#modalPurchaseInvitation').modal('show');
    });
    $('#keys-purchaseInvitation').click(function () {
        var req = {};
        var res;
        var email = $.trim($('#inputInviteEmail').val());
        if (email != '' && validateEmail(email) == false && isNaN(email)) {
            toastr.error('Please enter Correct email address/ Phone no.');
        } else {
            if (validateMobile(email) && !(isNaN(email))) {
                toastr.error('Please enter Correct email address/ Phone no.');
            }
        }
        req.action = "keys-purchase_invitation";
        req.keys = total_puchased_keys;
        req.email = email;
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            toastr.success(data.message);
            $('#modalPurchaseCourseKeys').modal('hide');
            $('#modalPurchaseInvitation').modal('hide');
            // window.location.reload();
        });
    });
});
function fetchCoursekeys() {
    var req = {};
    var res;
    req.action = "get-course-keys-details";
    if (typeof (instituteId) === "undefined") {
    } else {
        req.instituteId = instituteId;
        $('#btn_Purchase_More').remove();
    }
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        var html = "";
        for (var i = 0; i < data.courses.length; i++)
        {
            var keys = data.courses[i].keys;
            var rate = data.courses[i].key_rate;
            var amount = keys * rate;
            var date = data.courses[i].date;
            var time = data.courses[i].time;
            var currency = data.courses[i].currency;
            if (currency == 2)
            {
                display_currency = '<i class="fa fa-inr"></i>';
            }
            else {
                display_currency = '$';
            }
            /*html += '<tr><td>' + data.courses[i].date + '&nbsp;&nbsp;' + time + ' GMT </td><td>' + keys + '</td><td>' + rate + ' ' + display_currency + '</td><td>' + amount + ' ' + display_currency + '</td></tr>';*/
            html += '<tr>'+
                        '<td>' + (i+1) + '</td>'+
                        '<td>' + data.courses[i].date + '</td>'+
                        '<td>' + time + ' GMT</td>'+
                        '<td>' + keys + '</td>'+
                        '<td>' + rate + ' ' + display_currency + '</td>'+
                        '<td>' + amount + ' ' + display_currency + '</td>'+
                    '</tr>';

        }
        $('#tableTotalKeys tbody').html(html);
        if ($('#tableTotalKeys tbody tr').length == 0) {
            $('#tableTotalKeys tbody').html('<tr><td colspan="6">No any purchase done yet </td></tr>');
        } else {
            TableDatatablesManaged.init1();
        }
        var current_currency = data.currency[0].currency;
        UserCurrency=data.currency[0].currency;
        key_rate = data.key_rate[0].key_rate;
        if (current_currency == 2) {
            display_current_currency = '<i class="fa fa-inr"></i>';
            key_rate = data.key_rate[0].key_rate_inr;
        }
        //key_rate=data.key_rate[0].key_rate;
        $('#keyrate').text(key_rate);
        $('#keyrate').append(display_current_currency);
        var active_key = data.key_consume[0].active_key;
        var total_keys = data.key_consume[0].total_keys;
        var remaining_keys = total_keys - active_key;
        $('#active_key').text(active_key);
        $('#total_keys').text(total_keys);
        $('#remaining_key').text(remaining_keys);
        $('#totalkeyrate').append(display_current_currency);
        //TableDatatablesManaged.init1();
    });
}
function get_students_of_course() {
    var req = {};
    var res;
    req.action = "get_students_of_course";
    if (typeof (instituteId) === "undefined") {
    } else {
        req.instituteId = instituteId;

    }
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        var html = "";
        if (data.students.length>0) {
            for (var i = 0; i < data.students.length; i++)
            {
                var key_student = data.students[i].key_student;
                var course_id = data.students[i].course_id;
                var name = data.students[i].name;
                var stand_alone = data.students[i].stand_student;
                /*html += '<tr course_id=' + course_id + '><td>' + course_id + ' </td><td>' + name + '</td><td><a class="showcourse" data-toggle=modal href="#modal_student">' + key_student + '</a></td><td> <a href="#modal_student" data-toggle="modal" class="shownonkey">' + stand_alone + ' </a></td></tr>';*/
                html += '<tr course_id="' + course_id + '">'+
                            '<td>' + (i+1) + '</td>'+
                            '<td>' + course_id + '</td>'+
                            '<td>' + name + '</td>'+
                            '<td>'+
                                '<a href="#modalStudentCourseKeys" data-toggle="modal" class="showcourse">' + key_student + '</a>'+
                            '</td>'+
                            '<td>'+
                                '<a href="#modalStudentCourseKeys" data-toggle="modal" class="shownonkey">' + stand_alone + '</a>'+
                            '</td>'+
                        '</tr>';
            }
            $('#tableCourseWise tbody').append(html);
            TableDatatablesManaged.init2();
        } else {
            $('#tableCourseWise tbody').append('<tr><td colspan="5">No token data available</td></tr>');
        }

        $('.showcourse').click(function () {
            $("#tableCourseDetails tbody").html("");
            var course_id = $(this).parents('tr').attr('course_id');
            var req = {};
            var res;
            req.action = "get_course_student_details";
            req.keyId = true;
            if (typeof (instituteId) !== "undefined") {
                req.instituteId = instituteId;
                $('#btn_Purchase_More').remove();
            }
            req.course_id = course_id;
            $.ajax({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                data = $.parseJSON(res);
                var html = "";
                if (data.students.length>0) {
                    for (var i = 0; i < data.students.length; i++)
                    {
                        var name = data.students[i].name;
                        var contactMobile = data.students[i].contactMobile;
                        var course_key = 'CK00' + data.students[i].key_id;
                        var email = data.students[i].email;
                        var date = data.students[i].date;
                        html += '<tr>'+
                                    '<td>' + name + '</td>'+
                                    '<td>' + email + '</td>'+
                                    '<td>' + contactMobile + '</td>'+
                                    '<td>' + course_key + '</td>'+
                                    '<td>' + date + '</td>'+
                                '</tr>';
                    }
                } else {
                    html += '<tr>'+
                                '<td colspan="5">No information</td>'+
                            '</tr>';
                }
                $('#tableCourseDetails tbody').append(html);
            });
        });
        $('.shownonkey').click(function () {
            $("#tableCourseDetails > tbody").html("");
            var course_id = $(this).parents('tr').attr('course_id');
            var req = {};
            var res;
            req.action = "get_course_student_details";
            req.keyId = false;
            if (typeof (instituteId) !== "undefined") {
                req.instituteId = instituteId;
                $('#btn_Purchase_More').remove();
            }
            req.course_id = course_id;
            $.ajax({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                data = $.parseJSON(res);
                var html = "";
                if (data.students.length>0) {
                    for (var i = 0; i < data.students.length; i++)
                    {
                        var name = data.students[i].name;
                        var contactMobile = data.students[i].contactMobile;
                        var course_key = 'CK00' + data.students[i].key_id;
                        var email = data.students[i].email;
                        var date = data.students[i].date;
                        html += '<tr>'+
                                    '<td>' + name + '</td>'+
                                    '<td>' + email + '</td>'+
                                    '<td>' + contactMobile + '</td>'+
                                    '<td>' + course_key + '</td>'+
                                    '<td>' + date + '</td>'+
                                '</tr>';
                    }
                } else {
                    html += '<tr>'+
                                '<td colspan="5" class="text-center">No information</td>'+
                            '</tr>';
                }
                $('#tableCourseDetails').append(html);
            });

        });
    
    });
}
function fetchPurchasedCourses() {
    var req = {};
    var res;
    req.action = "fetch-purchased-courses-details";
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        var html = "";
        if(data.purchasedCourses.length > 0) {
            $.each(data.purchasedCourses, function (c, pcourse) {
                var coursePrice="";
                /*html += '<tr courseId=' + pcourse.parentCourseId + '><td>' + pcourse.parentCourseId + ' </td><td>' + pcourse.name + '</td><td>' + pcourse.purchasedAt + '</td><td>' + pcourse.ownerName + ' </td><td>' + pcourse.priceUSD + '</td> </tr>';*/
                html += '<tr courseId="' + pcourse.parentCourseId + '">'+
                            '<td>' + (i+1) + '</td>'+
                            '<td>' + pcourse.parentCourseId + '</td>'+
                            '<td>' + pcourse.name + '</td>'+
                            '<td>' + pcourse.purchasedAt + '</td>'+
                            '<td>' + pcourse.ownerName + '</td>'+
                            '<td>' + pcourse.priceUSD + '</td>'+
                        '</tr>';
            });
            $('#tablePurchasedCourses').append(html);
            TableDatatablesManaged.init3();
        }
        else {
            $('#tablePurchasedCourses tbody').html('<tr><td colspan="6">No '+terminologies["course_plural"].toLowerCase()+' purchased</td></tr>');
        }
    });
}
</script>