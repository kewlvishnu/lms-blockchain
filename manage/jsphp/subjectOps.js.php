<script type="text/javascript">
var subjectProfessors = [];
var _URL = window.URL || window.webkitURL;
var cropCoords,
file,
originalImageWidth, originalImageHeight,
uploadSize = 300,
previewSize = 500;

$("#fileSubjectPic").on("change", function(){
    file = this.files[0];
    if (!!file) {
        var JcropAPI = $('#subjectPic').data('Jcrop');
        if (JcropAPI != undefined) {
            JcropAPI.destroy();
        }
        $('.js-upload').removeClass('hide');
        var img;
        img = new Image();
        img.onload = function () {
            //console.log(this.width + " " + this.height);
            originalImageWidth = this.width;
            originalImageHeight = this.height;
            readFile(file, {
                width: originalImageWidth,
                height: originalImageHeight
            }).done(function(imgDataUrl, origImage) {
                //$("input, img, button").toggle();
                initJCrop(imgDataUrl);
            }).fail(function(msg) {
                toastr.error(msg);
            });
        };
        img.src = _URL.createObjectURL(file);
    } else {
        $('.js-upload').addClass('hide');
    }
});

$("#btnUpload").on("click", function(){
    var thiz = $(this);
    $(this).text("Uploading...").prop("disabled", true);

    readFile(file, {
        width: uploadSize,
        height: uploadSize,
        crop: cropCoords
    }).done(function(imgDataURI) {
        var data = new FormData();
        var blobFile = dataURItoBlob(imgDataURI);
        data.append('subject-image', blobFile);
        data.append('subjectId', subjectId);
        
        ajaxRequest({
            url: FileApiPoint,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: 'json',
            success: function(res) {
                //console.log(res);
                if (res.status == 1) {
                    thiz.text('Upload');
                    var JcropAPI = $('#subjectPic').data('Jcrop');
                    JcropAPI.destroy();
                    $('#subjectPic').prop('style', '');
                    $('#subjectPic').prop('src', res.image);
                    $('#subjectPic').attr('old-image',res.image);
                    $('.coursepic img').prop('src', res.image);
                    $('.js-upload').addClass('hide');
                    toastr.success(res.message);
                } else {
                    toastr.error(res.message);
                }
            },
            error: function(xhr) {
                toastr.error(blobFile);
            }
        });
    });
});

/*****************************
show local image and init JCrop
*****************************/
var initJCrop = function(imgDataUrl){
    var img = $("img#subjectPic").attr("src", imgDataUrl);
    
    var storeCoords = function(c) {
        //console.log(c);
        cropCoords = c;
    };
    
    var w = img.width();
    var h = img.height();
    var s = uploadSize;
    img.Jcrop({
        onChange: storeCoords,
        onSelect: storeCoords,
        aspectRatio: 1,
        setSelect: [(w - s) / 2, (h - s) / 2, (w - s) / 2 + s, (h - s) / 2 + s],
        trueSize: [originalImageWidth, originalImageHeight]
    });
};
function fetchSubjectReviews() {
    var req = {};
    var res;
    req.action = 'get-all-reviews-for-subject';
    req.courseId = courseId;
    req.subjectId = subjectId;
    $.ajax({
        type: "post",
        url: ApiEndPoint,
        data: JSON.stringify(req) 
    }).done(function(res) {
        res = $.parseJSON(res);
        //console.log(res);
        $('#modalViewReviews .modal-title').text(terminologies["subject_single"]+' Reviews');
        var html = '';
        var c1=c2=c3=c4=c5=0;
        var rateTotal = 0;
        $.each(res.reviews, function(i, review) {
            //counting reviews
            switch(review.rating) {
                case '1.00':
                    c1++;
                    break;
                case '2.00':
                    c2++;
                    break;
                case '3.00':
                    c3++;
                    break;
                case '4.00':
                    c4++;
                    break;
                case '5.00':
                    c5++;
                    break;
            }
            rateTotal += parseFloat(review.rating);
            var date = '';
            date = formatTime(review.time);
            
            html += '<li class="comment-item">'+
                        '<div class="row">'+
                            '<div class="col-sm-3">'+
                                '<div class="user-block">'+
                                    '<div class="user-avatar">'+
                                        '<img src="' + review.studentImage + '" class="img-circle" alt="">'+
                                    '</div>'+
                                    '<span class="user-name">' + review.studentName + '</span>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-sm-9">'+
                                '<div class="rating">'+
                                    '<span title="' + review.rating + '">'+
                                        '<input class="star-rating" type="number" class="rating" min="0" max="5" step="0.1" value="' + review.rating + '" data-size="xs">'+
                                    '</span>'+
                                    '<span class="comment-time"> ' + date + '</span>'+
                                    '<span class="subject"> ('+terminologies["subject_single"]+': ' + review.subjectName + ')</span>'+
                                '</div>'+
                                '<h4>' + review.title + '</h4>'+
                                '<div class="comment">'+
                                    '<p>' + review.review + '</p>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</li>';
        });
        $('#reviews ul.list-comments').append(html);
        var total = c1 + c2 + c3 + c4 + c5;
        $('.ratingTotal').text(total + ' Ratings');
        if(total == 0) {
            $('#reviews ul').html('<li class="comment-item"><div class="user-block"><span style="margin-left: 0px;">No Reviews Found</span></div></li>');
            avg = 0;
        }
        else
            avg = (rateTotal / total).toFixed(1);
        $('.avg-rate').text(avg);
        $('.avg-rating').attr('title',avg);
        $('.avg-rating input').val(avg);
        $(".star-rating").rating({displayOnly: true});
        $(".avg-rating input").rating({displayOnly: true});
        $('.progress1').css('width', percentage(c1, total));
        $('.progress2').css('width', percentage(c2, total));
        $('.progress3').css('width', percentage(c3, total));
        $('.progress4').css('width', percentage(c4, total));
        $('.progress5').css('width', percentage(c5, total));
        $('.count1').text(c1);
        $('.count2').text(c2);
        $('.count3').text(c3);
        $('.count4').text(c4);
        $('.count5').text(c5);
    });
}

function fetchInstituteProfessors() {
    var req = {};
    var res;
    req.courseId = courseId;
    req.subjectId = subjectId;
    req.action = "get-institute-professors";
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        //console.log(res);
        res =  $.parseJSON(res);
        if(res.status == 1) {
            fillInstituteProfessors(res);
            //$('#manageProfessorSection').show();
        }
        else if(res.status == 0)
            toastr.error(res.message);
    });
}
function fillInstituteProfessors(data) {
    //console.log(data);
    var html = '';
    var option = '';
    if (data.instituteProfessors.length>0) {
        $('#listProfessors').html('<select multiple="multiple" class="multi-select" id="selectProfessors" name="selectProfessors"></select>');
        $.each(data.instituteProfessors, function(i,v) {
            var pName = v.firstName + ' ' + v.lastName;
            html+= '<tr class="odd gradeX" data-professor="' + v.professorId + '">'+
                        '<td>'+
                            '<a href="'+sitepathInstituteSubjects+subjectId+'" target="_blank">'+pName+'</a>'+
                            '<div class="pull-right">'+
                                '<a href="javascript:void(0)" class="btn btn-circle btn-sm red delete-professor"><i class="fa fa-trash"></i> Delete</a>'+
                            '</div>'+
                        '</td>'+
                    '</tr>';
            var pName = v.firstName + ' ' + v.lastName;
            option = "<option value='" + v.professorId + "'>" + pName + "</option>";
            $('#selectProfessors').append(option);
        });
        $.each(data.subjectProfessors, function(i,v) {
            subjectProfessors[i] = v.professorId;
        });
        $('#selectProfessors').val(subjectProfessors);
        $('#tableInstructors tbody').html(html);
        ComponentsDropdowns.init();
    }
}
var ComponentsDropdowns = function () {
    
    function updateSubjectProfessors(){
        var req = {};
        req.courseId = courseId;
        req.subjectId = subjectId;
        req.subjectProfessors = subjectProfessors;
        
        req.action = 'update-subject-professors';
        
        console.log(req);
        $.ajax({
            'type'  : 'post',
            'url'   : ApiEndPoint,
            'data'  : JSON.stringify(req)
        }).done( function (res) {
            res =  $.parseJSON(res);
            if(res.status == 1) {
                toastr.success(res.message);
                //$('#subject-professor-modal').modal('hide');
                //fetchInstituteProfessors();
            } else {
                toastr.error(res.msg);
            }
        });
    };

    var handleMultiSelect = function () {
        $('.multi-select').multiSelect({
          afterSelect: function(values){
            var index = subjectProfessors.indexOf(values[0]);
            if(index === -1)
                subjectProfessors.push(values[0]);
            updateSubjectProfessors();
          },
          afterDeselect: function(values){
            var index = subjectProfessors.indexOf(values[0]);
            subjectProfessors.splice(index, 1);
            updateSubjectProfessors();
          }
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleMultiSelect();
        }
    };

}();
$(document).ready(function() {
    fetchSubjectReviews();
    if (userRole == 1) {
        fetchInstituteProfessors();
    }
    $('body').on('click', '.edit-subject', function(e) {
        e.preventDefault();
        var req = {};
        var res;
        req.courseId = courseId;
        req.subjectId = subjectId;

        req.action = "get-subject-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillSubjectDetails(res);
            //details=res;
        });
    });
    function fillSubjectDetails(data) {
        $('.js-subject-action').html("Edit");
        // General Details
        if(data.subjectDetails.deleted == 1)
            toastr.error('This subject has been deleted!');
        $('#inputSubjectName').val(data.subjectDetails.name);
        $('#inputSubjectDescription').val(data.subjectDetails.description);
        $('#modalAddEditSubject').modal("show");
    }
    $('#modalUploadSubjectSyllabus').find('form').attr('action', FileApiPoint).ajaxForm({
        data: {subjectId:subjectId,demoFlag:0},
        beforeSend: function(arr, $form, data) {
            //console.log('starting');
        },
        uploadProgress: function(event, position, total, percentComplete) {
            //Progress bar
            var progress = $('#modalUploadSubjectSyllabus').find('.progress')
            progress.find('.progress-bar').width(percentComplete + '%') //update progressbar percent complete
            progress.find('.sr-only').html(percentComplete + '%') //update progressbar percent complete
        },
        success: function(resp) {
            if((typeof(resp) === 'string') && resp.substr(0, 6) == '<br />') {
                toastr.error('There was some error, please refresh and try again!');
            }
        },
        complete: function(resp) {
            res = $.parseJSON(resp.responseText);
            if(res.status == 1)
                location.reload();
            else
                toastr.error('An error occured. Please refresh the page and try again. Error Details: ' + res.message);
        },
        error: function() {
            console.log('Error');
        }
    });
    //events for demo syllabus
    $('#modalUploadSubjectSyllabus .btn-upload-syllabus').on('click', function() {
        $('#modalUploadSubjectSyllabus .upload-file').click();
        return false;
    });
    $('#modalUploadSubjectSyllabus .upload-file').on('change', function() {
        var file = this.files[0];
        if(file.type =='application/pdf'){
            $('#modalUploadSubjectSyllabus').find('form').submit();
        }
        else{
            toastr.error("Please upload PDF files only");
        }
    });
});
</script>