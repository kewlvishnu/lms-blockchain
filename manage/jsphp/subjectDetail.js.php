<script type="text/javascript">
function fetchSubjectChapters() {
    var req = {};
    var res;
    req.courseId = courseId;
    req.subjectId = subjectId;
    //req.action ="get-subject-progress-details"; 
    req.action ="get-subject-chapters-new";
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1)
            fillSubjectChapters(res);
        else
            toastr.error(res.message);
    });
}
function fillSubjectChapters(data) {
    var html = "";
    var flag = false;
    var editLink;
    var ChapterNum = 1;
    var examhtml = '';
    var assignmenthtml = '';
    var subjectiveExamshtml = '';
    var manualExamshtml = '';
    var submissionshtml = '';
    //console.log(data);
    if (data.chapters.length>0) {
        $.each(data.chapters, function(i, chapter) {
            contenthtml = '';
            examhtml = '';
            assignmenthtml = '';
            subjectiveExamshtml = '';
            manualExamshtml = '';
            submissionshtml = '';
            html+='<div class="portlet box portlet-course js-chapter blue-hoki" data-chapter="'+ chapter.name+'" data-ch-id="' + chapter.id  + '">'+
                    '<div class="portlet-title">'+
                        '<div class="caption">'+
                            '<i class="fa fa-book"></i> Section '+ ChapterNum + ': ' + chapter.name + ' </div>'+
                        '<div class="tools">'+
                            '<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
                            '<a href="" class="fullscreen" data-original-title="" title=""> </a>'+
                        '</div>'+
                    '</div>'+
                    '<div class="portlet-body">'+
                        '<div class="btn-group">'+
                            '<a href="'+sitepathManageContent+'subjects/'+subjectId+'" class="btn blue-hoki btn-default">'+
                                '<i class="fa fa-eye"></i> Watch lecture videos, access content </a>'+
                            '<a href="'+sitepathManageSubjects+subjectId+'/sections" data-action="edit" data-toggle="modal" class="btn blue-dark btn-default btn-course">'+
                                '<i class="fa fa-pencil"></i> Edit Section </a>'+
                            '<a href="javascript:;" class="btn red btn-default delete-chapter">'+
                                '<i class="fa fa-trash"></i> Delete Section </a>'+
                        '</div>'+
                    '</div>'+
                    '<div class="portlet-body no-space">';
            if (chapter.content.length>0 || chapter.exams.length>0 || chapter.assignments.length>0 || chapter.subjectiveExams.length>0 || chapter.manualExams.length>0 || chapter.submissions.length>0) {
                if (chapter.content.length>0) {
                    contenthtml = setContentHtml(chapter.content, 'Content');
                }
                if (chapter.exams.length>0) {
                    examhtml = setExamHtml(chapter.exams, 'Exams');
                }
                if (chapter.assignments.length>0) {
                    assignmenthtml = setExamHtml(chapter.assignments, 'Assignments');
                }
                if (chapter.subjectiveExams.length>0) {
                    subjectiveExamshtml = setSubjectiveExamHtml(chapter.subjectiveExams, 'Subjective Exams');
                }
                if (chapter.manualExams.length>0) {
                    manualExamshtml = setManualExamHtml(chapter.manualExams, 'Manual Exams');
                }
                if (chapter.submissions.length>0) {
                    //console.log(chapter.submissions);
                    submissionshtml = setSubmissionHtml(chapter.submissions, 'Submissions');
                }
            } else {
                //html+='<p class="text-center pad-20 no-margin">No exams in this section!</p>';
            }
            html += contenthtml + examhtml + assignmenthtml + subjectiveExamshtml + manualExamshtml + submissionshtml;
            html+='</div>'+
                '</div>';
            ChapterNum++;
        });
        $('#listSections').html(html);
        $('#listSections .delete-content').off('click');
        $('#listSections .delete-content').on('click', function(e) {
            e.preventDefault();
            var req = {};
            if(confirm("Are you sure to delete ? ")){
                deleteContentFromDB($(this).closest('tr'));
            }
            
        });
        $('#listSections .delete-exam').off('click');
        $('#listSections .delete-exam').on('click', function () {
            var con = confirm("Are you sure you want to delete this Exam/Assignment.");
            if (con) {
                var req = {};
                req.action = 'delete-exam';
                req.examId = $(this).closest('tr').attr('data-exam');
                $.ajax({
                    'type': 'post',
                    'url': ApiEndPoint,
                    'data': JSON.stringify(req)
                }).done(function (res) {
                    res = $.parseJSON(res);
                    if (res.status == 0)
                        toastr.error(res.message);
                    else {
                        //fetchExams($('#subjectSelect').val());
                        fetchSubjectChapters();
                        fetchIndependentExams();
                    }
                });
            }
        });
        $('#listSections').on("click", ".delete-subjective-exam", function(){
            var thiz = $(this);
            var con = confirm("Are you sure you want to delete this Subjective Exam?");
            if(con) {
                var examId = thiz.closest('tr').attr('data-exam');
                var req = {};
                var res;
                req.action = 'delete-subjective-exam';
                req.examId = examId;
                $.ajax({
                    'type'  : 'post',
                    'url'   : ApiEndPoint,
                    'data'  : JSON.stringify(req)
                }).done(function (res) {
                    res =  $.parseJSON(res);
                    if(res.status == 0) {
                        toastr.error(res.message);
                    } else {
                        toastr.success('Subjective Exam deleted successfully');
                        fetchSubjectChapters();
                        fetchIndependentExams();
                    }
                });
            }
        });
        $('#listSections').on("click", ".delete-manual-exam", function(){
            var thiz = $(this);
            var con = confirm("Are you sure you want to delete this Manual Exam?");
            if(con) {
                var examId = thiz.closest('tr').attr('data-exam');
                var req = {};
                var res;
                req.action = 'delete-manual-exam';
                req.examId = examId;
                $.ajax({
                    'type'  : 'post',
                    'url'   : ApiEndPoint,
                    'data'  : JSON.stringify(req)
                }).done(function (res) {
                    res =  $.parseJSON(res);
                    if(res.status == 0) {
                        toastr.error(res.message);
                    } else {
                        toastr.success('Manual Exam deleted successfully');
                        fetchSubjectChapters();
                        fetchIndependentExams();
                    }
                });
            }
        });
        $('#listSections').on("click", ".delete-submission", function(){
            var thiz = $(this);
            var con = confirm("Are you sure you want to delete this Submission?");
            if(con) {
                var examId = thiz.closest('tr').attr('data-exam');
                var req = {};
                var res;
                req.action = 'delete-submission';
                req.examId = examId;
                $.ajax({
                    'type'  : 'post',
                    'url'   : ApiEndPoint,
                    'data'  : JSON.stringify(req)
                }).done(function (res) {
                    res =  $.parseJSON(res);
                    if(res.status == 0) {
                        toastr.error(res.message);
                    } else {
                        toastr.success('Submission deleted successfully');
                        fetchSubjectChapters();
                        fetchIndependentExams();
                    }
                });
            }
        });
        $('#listSections .restore-exam').off('click');
        $('#listSections .restore-exam').on('click', function () {
            var con = confirm("Are you sure you want to restore this Exam/Assignment.");
            if (con) {
                var req = {};
                req.action = 'restore-exam';
                req.examId = $(this).closest('tr').attr('data-exam');
                $.ajax({
                    'type': 'post',
                    'url': ApiEndPoint,
                    'data': JSON.stringify(req)
                }).done(function (res) {
                    res = $.parseJSON(res);
                    if (res.status == 0)
                        toastr.error(res.message);
                    else {
                        toastr.success('Exam/Assignment restored successfully');
                        fetchSubjectChapters();
                        fetchIndependentExams();
                    }
                });
            }
        });
        $('#listSections').on("click", ".restore-subjective-exam", function(){
            var thiz = $(this);
            var con = confirm("Are you sure you want to restore this Subjective Exam?");
            if(con) {
                var examId = thiz.closest('tr').attr('data-exam');
                var req = {};
                var res;
                req.action = 'restore-subjective-exam';
                req.examId = examId;
                $.ajax({
                    'type'  : 'post',
                    'url'   : ApiEndPoint,
                    'data'  : JSON.stringify(req)
                }).done(function (res) {
                    res =  $.parseJSON(res);
                    if(res.status == 0) {
                        toastr.error(res.message);
                    } else {
                        toastr.success('Subjective Exam restored successfully');
                        fetchSubjectChapters();
                        fetchIndependentExams();
                    }
                });
            }
        });
        $('#listSections').on("click", ".restore-manual-exam", function(){
            var thiz = $(this);
            var con = confirm("Are you sure you want to restore this Manual Exam?");
            if(con) {
                var examId = thiz.closest('tr').attr('data-exam');
                var req = {};
                var res;
                req.action = 'restore-manual-exam';
                req.examId = examId;
                $.ajax({
                    'type'  : 'post',
                    'url'   : ApiEndPoint,
                    'data'  : JSON.stringify(req)
                }).done(function (res) {
                    res =  $.parseJSON(res);
                    if(res.status == 0) {
                        toastr.error(res.message);
                    } else {
                        toastr.success('Manual Exam restored successfully');
                        fetchSubjectChapters();
                        fetchIndependentExams();
                    }
                });
            }
        });
        $('#listSections').on("click", ".restore-submission", function(){
            var thiz = $(this);
            var con = confirm("Are you sure you want to restore this Submission?");
            if(con) {
                var examId = thiz.closest('tr').attr('data-exam');
                var req = {};
                var res;
                req.action = 'restore-submission';
                req.examId = examId;
                $.ajax({
                    'type'  : 'post',
                    'url'   : ApiEndPoint,
                    'data'  : JSON.stringify(req)
                }).done(function (res) {
                    res =  $.parseJSON(res);
                    if(res.status == 0) {
                        toastr.error(res.message);
                    } else {
                        toastr.success('Submission restored successfully');
                        fetchSubjectChapters();
                        fetchIndependentExams();
                    }
                });
            }
        });
        $('#listSections .copy-exam').off('click');
        $('#listSections .copy-exam').on('click', function () {
            var examId    = $(this).closest('tr').attr('data-exam');
            var examTitle = 'Copy of '+$(this).closest('tr').find('.exam-title').text();
            $('#copyExamTitle').val(examTitle);
            $('#btnCopyExam').attr('data-exam', examId);
            $('#btnCopyExam').attr('data-exam-type', 'exam');
            $('#modalCopyExam').modal('show');
        });
        $('#listSections .copy-subjective-exam').off('click');
        $('#listSections .copy-subjective-exam').on('click', function () {
            var examId    = $(this).closest('tr').attr('data-exam');
            var examTitle = 'Copy of '+$(this).closest('tr').find('.exam-title').text();
            $('#copyExamTitle').val(examTitle);
            $('#btnCopyExam').attr('data-exam', examId);
            $('#btnCopyExam').attr('data-exam-type', 'subjective-exam');
            $('#modalCopyExam').modal('show');
        });
        $('#listSections .copy-submission').off('click');
        $('#listSections .copy-submission').on('click', function () {
            var examId    = $(this).closest('tr').attr('data-exam');
            var examTitle = 'Copy of '+$(this).closest('tr').find('.exam-title').text();
            $('#copyExamTitle').val(examTitle);
            $('#btnCopyExam').attr('data-exam', examId);
            $('#btnCopyExam').attr('data-exam-type', 'submission');
            $('#modalCopyExam').modal('show');
        });
    } else {
        $('#listSections').html('<div class="note note-info">No sections added yet!</div>');
    }

    //makeChaptersSortable('#existing-chapters tbody');
    
    //event handlers for chapter delete button click
    $('.delete-chapter').off('click');
    $('.delete-chapter').on('click', function() {
        var con = confirm("Are you sure to delete this Section? All associated exams and assignments will become independent.");
        if(con) {
            var chapterId = $(this).closest('.js-chapter').attr('data-ch-id');
            var req = {};
            var res;
            req.action = 'delete-chapter';
            req.chapterId = chapterId;
            $.ajax({
                'type'  : 'post',
                'url'   : ApiEndPoint,
                'data'  : JSON.stringify(req)
            }).done(function (res) {
                res =  $.parseJSON(res);
                if(res.status == 0)
                    toastr.error(res.message);
                else {
                    toastr.success('Section deleted successfully');
                    fetchSubjectChapters();
                    fetchIndependentExams();
                }
            });
        }
    });
}
//function to fetch all independent exams and assignments
function fetchIndependentExams() {
    var req = {};
    var res;
    req.subjectId = subjectId;
    req.action = "get-independent-exams-new";
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1) {
            //console.log(res);
            fillIndependentExams(res);
        }
        else
            toastr.error(res.message);
    });
}
//function to fill independent exams table
function fillIndependentExams(data) {
    var ehtml = [];
    var ahtml = [];
    var html='';
    var ne = 0;
    var na = 0;
    var ihtml = '';
    var examhtml = '';
    var assignmenthtml = '';
    var subjectiveExamshtml = '';
    var manualExamshtml = '';
    var submissionshtml = '';
    //console.log(data);
    if (data.exams.length>0 || data.assignments.length>0 || data.subjectiveExams.length>0 || data.manualExams.length>0 || data.submissions.length>0) {
        ihtml+='<div class="portlet box portlet-course blue-soft">'+
                '<div class="portlet-title">'+
                    '<div class="caption">'+
                        '<i class="fa fa-book"></i> Independent Exams/Assignments </div>'+
                    '<div class="tools">'+
                        '<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
                        '<a href="" class="fullscreen" data-original-title="" title=""> </a>'+
                    '</div>'+
                '</div>'+
                '<div class="portlet-body no-space">';
    
        if (data.exams.length>0) {
            examhtml = setExamHtml(data.exams, 'Independent Exams');
        }
        if (data.assignments.length>0) {
            assignmenthtml = setExamHtml(data.assignments, 'Independent Assignments');
        }
        if (data.subjectiveExams.length>0) {
            subjectiveExamshtml = setSubjectiveExamHtml(data.subjectiveExams, 'Independent Subjective Exams');
        }
        if (data.manualExams.length>0) {
            manualExamshtml = setManualExamHtml(data.manualExams, 'Independent Manual Exams');
        }
        if (data.submissions.length>0) {
            submissionshtml = setSubmissionHtml(data.submissions, 'Independent Submissions');
        }
        ihtml += examhtml + assignmenthtml + subjectiveExamshtml + manualExamshtml + submissionshtml;
        ihtml+='</div>'+
            '</div>';
    }
    //console.log(ihtml);
    $('#listIndependent').html(ihtml);
    $('#listIndependent .delete-exam').off('click');
    $('#listIndependent .delete-exam').on('click', function () {
        var con = confirm("Are you sure you want to delete this Exam/Assignment.");
        if (con) {
            var req = {};
            req.action = 'delete-exam';
            req.examId = $(this).closest('tr').attr('data-exam');
            $.ajax({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if (res.status == 0)
                    toastr.error(res.message);
                else {
                    //fetchExams($('#subjectSelect').val());
                    fetchSubjectChapters();
                    fetchIndependentExams();
                }
            });
        }
    });
    $('#listIndependent').on("click", ".delete-subjective-exam", function(){
        var thiz = $(this);
        var con = confirm("Are you sure you want to delete this Subjective Exam?");
        if(con) {
            var examId = thiz.closest('tr').attr('data-exam');
            var req = {};
            var res;
            req.action = 'delete-subjective-exam';
            req.examId = examId;
            $.ajax({
                'type'  : 'post',
                'url'   : ApiEndPoint,
                'data'  : JSON.stringify(req)
            }).done(function (res) {
                res =  $.parseJSON(res);
                if(res.status == 0) {
                    toastr.error(res.message);
                } else {
                    toastr.success('Subjective Exam deleted successfully');
                    fetchSubjectChapters();
                    fetchIndependentExams();
                }
            });
        }
    });
    $('#listIndependent').on("click", ".delete-manual-exam", function(){
        var thiz = $(this);
        var con = confirm("Are you sure you want to delete this Manual Exam?");
        if(con) {
            var examId = thiz.closest('tr').attr('data-exam');
            var req = {};
            var res;
            req.action = 'delete-manual-exam';
            req.examId = examId;
            $.ajax({
                'type'  : 'post',
                'url'   : ApiEndPoint,
                'data'  : JSON.stringify(req)
            }).done(function (res) {
                res =  $.parseJSON(res);
                if(res.status == 0) {
                    toastr.error(res.message);
                } else {
                    toastr.success('Manual Exam deleted successfully');
                    fetchSubjectChapters();
                    fetchIndependentExams();
                }
            });
        }
    });
    $('#listIndependent').on("click", ".delete-submission", function(){
        var thiz = $(this);
        var con = confirm("Are you sure you want to delete this Submission?");
        if(con) {
            var examId = thiz.closest('tr').attr('data-exam');
            var req = {};
            var res;
            req.action = 'delete-submission';
            req.examId = examId;
            $.ajax({
                'type'  : 'post',
                'url'   : ApiEndPoint,
                'data'  : JSON.stringify(req)
            }).done(function (res) {
                res =  $.parseJSON(res);
                if(res.status == 0) {
                    toastr.error(res.message);
                } else {
                    toastr.success('Submission deleted successfully');
                    fetchSubjectChapters();
                    fetchIndependentExams();
                }
            });
        }
    });
    $('#listIndependent .restore-exam').off('click');
    $('#listIndependent .restore-exam').on('click', function () {
        var con = confirm("Are you sure you want to restore this Exam/Assignment.");
        if (con) {
            var req = {};
            req.action = 'restore-exam';
            req.examId = $(this).closest('tr').attr('data-exam');
            $.ajax({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if (res.status == 0)
                    toastr.error(res.message);
                else {
                    toastr.success('Exam/Assignment restored successfully');
                    fetchSubjectChapters();
                    fetchIndependentExams();
                }
            });
        }
    });
    $('#listIndependent').on("click", ".restore-subjective-exam", function(){
        var thiz = $(this);
        var con = confirm("Are you sure you want to restore this Subjective Exam?");
        if(con) {
            var examId = thiz.closest('tr').attr('data-exam');
            var req = {};
            var res;
            req.action = 'restore-subjective-exam';
            req.examId = examId;
            $.ajax({
                'type'  : 'post',
                'url'   : ApiEndPoint,
                'data'  : JSON.stringify(req)
            }).done(function (res) {
                res =  $.parseJSON(res);
                if(res.status == 0) {
                    toastr.error(res.message);
                } else {
                    toastr.success('Subjective Exam restored successfully');
                    fetchSubjectChapters();
                    fetchIndependentExams();
                }
            });
        }
    });
    $('#listIndependent').on("click", ".restore-manual-exam", function(){
        var thiz = $(this);
        var con = confirm("Are you sure you want to restore this Manual Exam?");
        if(con) {
            var examId = thiz.closest('tr').attr('data-exam');
            var req = {};
            var res;
            req.action = 'restore-manual-exam';
            req.examId = examId;
            $.ajax({
                'type'  : 'post',
                'url'   : ApiEndPoint,
                'data'  : JSON.stringify(req)
            }).done(function (res) {
                res =  $.parseJSON(res);
                if(res.status == 0) {
                    toastr.error(res.message);
                } else {
                    toastr.success('Manual Exam restored successfully');
                    fetchSubjectChapters();
                    fetchIndependentExams();
                }
            });
        }
    });
    $('#listIndependent').on("click", ".restore-submission", function(){
        var thiz = $(this);
        var con = confirm("Are you sure you want to restore this Submission?");
        if(con) {
            var examId = thiz.closest('tr').attr('data-exam');
            var req = {};
            var res;
            req.action = 'restore-submission';
            req.examId = examId;
            $.ajax({
                'type'  : 'post',
                'url'   : ApiEndPoint,
                'data'  : JSON.stringify(req)
            }).done(function (res) {
                res =  $.parseJSON(res);
                if(res.status == 0) {
                    toastr.error(res.message);
                } else {
                    toastr.success('Submission restored successfully');
                    fetchSubjectChapters();
                    fetchIndependentExams();
                }
            });
        }
    });
    $('#listIndependent .copy-exam').off('click');
    $('#listIndependent .copy-exam').on('click', function () {
        var examId    = $(this).closest('tr').attr('data-exam');
        var examTitle = 'Copy of '+$(this).closest('tr').find('.exam-title').text();
        $('#copyExamTitle').val(examTitle);
        $('#btnCopyExam').attr('data-exam', examId);
        $('#btnCopyExam').attr('data-exam-type', 'exam');
        $('#modalCopyExam').modal('show');
    });
    $('#listIndependent .copy-subjective-exam').off('click');
    $('#listIndependent .copy-subjective-exam').on('click', function () {
        var examId    = $(this).closest('tr').attr('data-exam');
        var examTitle = 'Copy of '+$(this).closest('tr').find('.exam-title').text();
        $('#copyExamTitle').val(examTitle);
        $('#btnCopyExam').attr('data-exam', examId);
        $('#btnCopyExam').attr('data-exam-type', 'subjective-exam');
        $('#modalCopyExam').modal('show');
    });
    $('#listIndependent .copy-submission').off('click');
    $('#listIndependent .copy-submission').on('click', function () {
        var examId    = $(this).closest('tr').attr('data-exam');
        var examTitle = 'Copy of '+$(this).closest('tr').find('.exam-title').text();
        $('#copyExamTitle').val(examTitle);
        $('#btnCopyExam').attr('data-exam', examId);
        $('#btnCopyExam').attr('data-exam-type', 'submission');
        $('#modalCopyExam').modal('show');
    });
}
function setContentHtml(content, type) {
    var contenthtml = '<table class="table table-striped table-bordered table-hover order-column no-margin">'+
                '<thead>'+
                    '<tr>'+
                        '<th class="text-center" colspan="6">'+type+'</th>'+
                    '</tr>'+
                    '<tr>'+
                        '<th width="25%"> Title </th>'+
                        '<th class="text-center" width="14.5%"> File Type </th>'+
                        '<th class="text-center" width="6%"> Published </th>'+
                        '<th class="text-center" width="5%"> Demo </th>'+
                        '<th class="text-center" width="14.5%"> Downloadable </th>'+
                        '<th width="35%"> Actions </th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody>';
    for(var j = 0; j < content.length; j++) {
        contenthtml += '<tr class="odd gradeX" data-content="'+content[j].id+'">'+
                        '<td class="exam-title"><a href="'+sitepathManageSubjects+subjectId+'/sections">' + content[j].title + '</a></td>'+
                        '<td class="text-center"> '+checkFileType(content[j].fileType)+' </td>'+
                        '<td class="text-center"> '+intToStatus(content[j].published)+' </td>'+
                        '<td class="text-center"> '+intToStatus(content[j].demo)+' </td>'+
                        '<td class="text-center"> '+intToStatus(content[j].downloadable)+' </td>'+
                        '<td>'+
                            '<a href="'+sitepathManageSubjects+subjectId+'/sections" class="btn btn-circle btn-sm blue-hoki" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>'+
                            '<a href="javascript:void(0)" class="btn btn-circle btn-sm red delete-content" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>'+
                        '</td>'+
                    '</tr>';
    }
    contenthtml += '</tbody>'+
                '</table>';
    return contenthtml;
}
function setExamHtml(exams, type) {
    var examhtml = '<table class="table table-striped table-bordered table-hover order-column no-margin">'+
                '<thead>'+
                    '<tr>'+
                        '<th class="text-center" colspan="6">'+type+'</th>'+
                    '</tr>'+
                    '<tr>'+
                        '<th width="25%"> Title </th>'+
                        '<th class="text-center" width="15%"> Start Date </th>'+
                        '<th class="text-center" width="5%"> Attempts </th>'+
                        '<th class="text-center" width="5%"> Status </th>'+
                        '<th class="text-center" width="15%"> End Date </th>'+
                        '<th width="35%"> Actions </th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody>';
    for(var j = 0; j < exams.length; j++) {
        var endDate = '-';
        if (exams[j].endDate) {
            endDate = formatDate(parseInt(exams[j].endDate), '-');
        }
        var displayStatus = getExamStatus(exams[j].status);
        var resultDisabled = '';
        var examLink = '';
        var deleterestoreHtml = '';
        if (exams[j].status==0) {
            resultDisabled = 'disabled';
            examLink = 'javascript:;';
        }
        if (exams[j].status>0 && exams[j].result==0) {
            resultDisabled = 'disabled';
        }
        if (exams[j].delete==0) {
            deleterestoreHtml = '<a href="javascript:void(0)" class="btn btn-circle btn-sm red delete-exam" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>';
        } else {
            deleterestoreHtml = '<a href="javascript:void(0)" class="btn btn-circle btn-sm red restore-exam" data-toggle="tooltip" title="Restore"><i class="fa fa-recycle"></i></a>';
        }
        /*examhtml += '<a examId='+exams[j].id+' independent=1 type=Exam data-startDate="'+exams[j].startDate+'" data-endDate="'+exams[j].endDate+'" data-status='+exams[j].status+' data-attempts='+exams[j].attempts+'   class="examPopup" >' + exams[j].name + ' </a><br />';*/
        examhtml += '<tr class="odd gradeX" data-exam="'+exams[j].id+'">'+
                        '<td class="exam-title"><a href="'+sitepathManageExams+exams[j].id+'">' + exams[j].name + '</a></td>'+
                        '<td class="text-center"> '+formatDate(parseInt(exams[j].startDate), '-')+' </td>'+
                        '<td class="text-center"> '+exams[j].attempts+' </td>'+
                        '<td class="text-center"> '+displayStatus+' </td>'+
                        '<td class="text-center"> '+endDate+' </td>'+
                        '<td>'+
                            '<a href="'+sitepathManageExams+exams[j].id+'" class="btn btn-circle btn-sm blue-hoki" data-toggle="tooltip" title="Questions"><i class="fa fa-edit"></i></a>'+
                            '<a href="'+sitepathManageExams+exams[j].id+'/settings" class="btn btn-circle btn-sm blue-hoki" data-toggle="tooltip" title="Settings"><i class="fa fa-gear"></i></a>'+
                            '<a href="'+sitepathManageExams+exams[j].id+'/objective-comments" class="btn btn-circle btn-sm blue-hoki" data-toggle="tooltip" title="Objective Comments"><i class="fa fa-comment"></i></a>'+
                            '<a href="'+((examLink!='')?examLink:sitepathManageExams+exams[j].id+'/result')+'" class="btn btn-circle btn-sm blue-hoki" '+resultDisabled+' data-toggle="tooltip" title="Result"><i class="fa fa-book"></i></a>'+
                            '<a href="javascript:void(0)" class="btn btn-circle btn-sm blue-hoki copy-exam" data-toggle="tooltip" title="Copy"><i class="fa fa-copy"></i></a>'+
                            deleterestoreHtml+
                        '</td>'+
                    '</tr>';
    }
    examhtml += '</tbody>'+
                '</table>';
    return examhtml;
}
function setSubjectiveExamHtml(exams, type) {
    subjectiveExamshtml = '<table class="table table-striped table-bordered table-hover order-column no-margin">'+
                            '<thead>'+
                                '<tr>'+
                                    '<th class="text-center" colspan="6">'+type+'</th>'+
                                '</tr>'+
                                '<tr>'+
                                    '<th width="25%"> Title </th>'+
                                    '<th class="text-center" width="15%"> Start Date </th>'+
                                    '<th class="text-center" width="5%"> Attempts </th>'+
                                    '<th class="text-center" width="5%"> Status </th>'+
                                    '<th class="text-center" width="15%"> End Date </th>'+
                                    '<th width="35%"> Actions </th>'+
                                '</tr>'+
                            '</thead>'+
                            '<tbody>';
    //console.log(exams);
    for(var j = 0; j < exams.length; j++) {
        var endDate = '-';
        if (exams[j].endDate) {
            endDate = formatDate(parseInt(exams[j].endDate), '-');
        }
        var displayStatus = getSubjectiveExamStatus(exams[j].status);
        var resultDisabled = '';
        var resultLink = '';
        var deleterestoreHtml = '';
        if (exams[j].status<2 || exams[j].result==0) {
            //resultDisabled = 'disabled';
            resultLink = 'javascript:;';
        }
        if (exams[j].result==0) {
            resultDisabled = 'disabled';
        }
        if (exams[j].Active==1) {
            deleterestoreHtml = '<a href="javascript:void(0)" class="btn btn-circle btn-sm red delete-subjective-exam" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>';
        } else {
            deleterestoreHtml = '<a href="javascript:void(0)" class="btn btn-circle btn-sm red restore-subjective-exam" data-toggle="tooltip" title="Restore"><i class="fa fa-recycle"></i></a>';
        }
        subjectiveExamshtml += '<tr class="odd gradeX" data-exam="'+exams[j].id+'">'+
                        '<td class="exam-title"> <a href="'+sitepathManageSubjectiveExams+exams[j].id+'">' + exams[j].name + '</a> </td>'+
                        '<td class="text-center"> '+formatDate(parseInt(exams[j].startDate), '-')+' </td>'+
                        '<td class="text-center"> '+exams[j].attempts+' </td>'+
                        '<td class="text-center"> '+displayStatus+' </td>'+
                        '<td class="text-center"> '+endDate+' </td>'+
                        '<td>'+
                            '<a href="'+sitepathManageSubjectiveExams+exams[j].id+'" class="btn btn-circle btn-sm blue-hoki" data-toggle="tooltip" title="Questions"><i class="fa fa-question"></i></a>'+
                            '<a href="'+sitepathManageSubjectiveExams+exams[j].id+'/settings" class="btn btn-circle btn-sm blue-hoki" data-toggle="tooltip" title="Settings"><i class="fa fa-gear"></i></a>'+
                            '<a href="'+sitepathManageSubjectiveExams+exams[j].id+'/objective-comments" class="btn btn-circle btn-sm blue-hoki" data-toggle="tooltip" title="Objective Comments"><i class="fa fa-comment"></i></a>'+
                            '<a href="'+((resultLink!='')?resultLink:sitepathManageSubjectiveExams+exams[j].id+'/result')+'" class="btn btn-circle btn-sm blue-hoki" '+resultDisabled+' data-toggle="tooltip" title="Result"><i class="fa fa-book"></i></a>'+
                            '<a href="javascript:void(0)" class="btn btn-circle btn-sm blue-hoki copy-subjective-exam" data-toggle="tooltip" title="Copy"><i class="fa fa-copy"></i></a>'+
                            '<a href="'+sitepathManageSubjectiveExams+exams[j].id+'/check" class="btn btn-circle btn-sm blue-hoki" data-toggle="tooltip" title="Check"><i class="fa fa-check"></i></a>'+
                            deleterestoreHtml+
                        '</td>'+
                    '</tr>';
    }
    subjectiveExamshtml += '</tbody>'+
                '</table>';
    return subjectiveExamshtml;
}
function setManualExamHtml(exams, type) {
    manualExamshtml = '<table class="table table-striped table-bordered table-hover order-column no-margin">'+
                        '<thead>'+
                            '<tr>'+
                                '<th class="text-center" colspan="3">'+type+'</th>'+
                            '</tr>'+
                            '<tr>'+
                                '<th width="37.5%"> Title </th>'+
                                '<th class="text-center" width="30%"> Analysis Type </th>'+
                                '<th width="32.5%"> Actions </th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody>';
    for(var j = 0; j < exams.length; j++) {
        var deleterestoreHtml = '';
        if (exams[j].status==1) {
            deleterestoreHtml = '<a href="javascript:void(0)" class="btn btn-circle btn-sm red delete-manual-exam" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>';
        } else {
            deleterestoreHtml = '<a href="javascript:void(0)" class="btn btn-circle btn-sm red restore-manual-exam" data-toggle="tooltip" title="Restore"><i class="fa fa-recycle"></i></a>';
        }
        manualExamshtml += '<tr class="odd gradeX" data-exam="'+exams[j].id+'">'+
                                '<td> <a href="'+sitepathManageManualExams+exams[j].id+'">' + exams[j].name + '</a> </td>'+
                                '<td class="text-center"> ' + capitalizeFirstLetter(exams[j].type) + ' Analysis </td>'+
                                '<td>'+
                                    '<a href="'+sitepathManageManualExams+exams[j].id+'/settings" class="btn btn-circle btn-sm blue-hoki" data-toggle="tooltip" title="Settings"><i class="fa fa-gear"></i></a>'+
                                    '<a href="'+sitepathManageManualExams+exams[j].id+'/objective-comments" class="btn btn-circle btn-sm blue-hoki" data-toggle="tooltip" title="Objective Comments"><i class="fa fa-comment"></i></a>'+
                                    '<a href="'+sitepathManageManualExams+exams[j].id+'" class="btn btn-circle btn-sm blue-hoki" data-toggle="tooltip" title="Result"><i class="fa fa-edit"></i></a>'+
                                    deleterestoreHtml+
                                '</td>'+
                            '</tr>';
    }
    manualExamshtml += '</tbody>'+
                '</table>';
    return manualExamshtml;
}
function setSubmissionHtml(exams, type) {
    submissionshtml = '<table class="table table-striped table-bordered table-hover order-column no-margin">'+
                            '<thead>'+
                                '<tr>'+
                                    '<th class="text-center" colspan="6">'+type+'</th>'+
                                '</tr>'+
                                '<tr>'+
                                    '<th width="25%"> Title </th>'+
                                    '<th class="text-center" width="15%"> Start Date </th>'+
                                    '<th class="text-center" width="5%"> Attempts </th>'+
                                    '<th class="text-center" width="5%"> Status </th>'+
                                    '<th class="text-center" width="15%"> End Date </th>'+
                                    '<th width="35%"> Actions </th>'+
                                '</tr>'+
                            '</thead>'+
                            '<tbody>';
    //console.log(exams);
    for(var j = 0; j < exams.length; j++) {
        var endDate = '-';
        if (exams[j].endDate) {
            endDate = formatDate(parseInt(exams[j].endDate), '-');
        }
        var displayStatus = getSubjectiveExamStatus(exams[j].status);
        var resultDisabled = '';
        var resultLink = '';
        var deleterestoreHtml = '';
        if (exams[j].status<2 || exams[j].result==0) {
            //resultDisabled = 'disabled';
            resultLink = 'javascript:;';
        }
        if (exams[j].result==0) {
            resultDisabled = 'disabled';
        }
        if (exams[j].deleted==0) {
            deleterestoreHtml = '<a href="javascript:void(0)" class="btn btn-circle btn-sm red delete-submission" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>';
        } else {
            deleterestoreHtml = '<a href="javascript:void(0)" class="btn btn-circle btn-sm red restore-submission" data-toggle="tooltip" title="Restore"><i class="fa fa-recycle"></i></a>';
        }
        submissionshtml += '<tr class="odd gradeX" data-exam="'+exams[j].id+'">'+
                        '<td class="exam-title"> <a href="'+sitepathManageSubmissions+exams[j].id+'">' + exams[j].name + '</a> </td>'+
                        '<td class="text-center"> '+formatDate(parseInt(exams[j].startDate), '-')+' </td>'+
                        '<td class="text-center"> '+exams[j].attempts+' </td>'+
                        '<td class="text-center"> '+displayStatus+' </td>'+
                        '<td class="text-center"> '+endDate+' </td>'+
                        '<td>'+
                            '<a href="'+sitepathManageSubmissions+exams[j].id+'" class="btn btn-circle btn-sm blue-hoki" data-toggle="tooltip" title="Questions"><i class="fa fa-question"></i></a>'+
                            '<a href="'+sitepathManageSubmissions+exams[j].id+'/settings" class="btn btn-circle btn-sm blue-hoki" data-toggle="tooltip" title="Settings"><i class="fa fa-gear"></i></a>'+
                            '<a href="'+sitepathManageSubmissions+exams[j].id+'/objective-comments" class="btn btn-circle btn-sm blue-hoki" data-toggle="tooltip" title="Objective Comments"><i class="fa fa-comment"></i></a>'+
                            '<a href="'+((resultLink!='')?resultLink:sitepathManageSubmissions+exams[j].id+'/result')+'" class="btn btn-circle btn-sm blue-hoki" '+resultDisabled+' data-toggle="tooltip" title="Result"><i class="fa fa-book"></i></a>'+
                            '<a href="javascript:void(0)" class="btn btn-circle btn-sm blue-hoki copy-submission" data-toggle="tooltip" title="Copy"><i class="fa fa-copy"></i></a>'+
                            '<a href="'+sitepathManageSubmissions+exams[j].id+'/check" class="btn btn-circle btn-sm blue-hoki" data-toggle="tooltip" title="Check"><i class="fa fa-check"></i></a>'+
                            deleterestoreHtml+
                        '</td>'+
                    '</tr>';
    }
    submissionshtml += '</tbody>'+
                '</table>';
    return submissionshtml;
}
function deleteContentFromDB(content) {
    var conid = content.attr('data-content');

    var req = {};
    var res;
    req.conid = conid;
    req.action = "delete-content";
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1) {
            content.remove();
        }else
            toastr.error(res.message);
    });
}
$(document).ready(function() {
    $(".star-rating").rating({displayOnly: true});
    $('#btnCopyExam').on('click', function () {
        $(this).attr('disabled', true);
        var newExamname = $('#copyExamTitle').val();
        var examType = $(this).attr('data-exam-type');
        if (newExamname != null) {
            var req = {};
            req.action = (examType=='exam'?'copy-exam':(examType=='subjective-exam'?'copy-subjective-exam':'copy-submission'));
            req.iexamId = $(this).attr('data-exam');
            req.iname = newExamname;
            $.ajax({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                $('#btnCopyExam').attr('disabled', false);
                res = $.parseJSON(res);
                if (res.status == 0)
                    toastr.error(res.message);
                else {
                    //fetchExams($('#subjectSelect').val());
                    fetchSubjectChapters();
                    fetchIndependentExams();
                    $('#modalCopyExam').modal('hide');
                }
            });
        }
    });
});
</script>