<script type="text/javascript">
var data;
function fetchChapters() {
    var req = {};
    req.action = "get-chapters-for-exam";
    req.subjectId = subjectId;
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        fillChaptersSelect(res);
    });
}
function fillChaptersSelect(data) {
    if(data.status == 0) {
        toastr.error(data.message);
        return;
    }
    var opts = '';
    for(i=0; i<data.chapters.length; i++) {
        opts += '<option value="'+data.chapters[i]['id']+'">'+data.chapters[i]['name']+'</option>';
    }
    opts += '<option value="0">Independent</option>';
    $('#chapterSelect').append(opts);
    if (typeof chapterId !== 'undefined') {
        $('#chapterSelect').val(xx);
    }
    fetchManualExam();
}
function fetchManualExam() {
	var req = {};
	var res;
	req.action = 'get-manual-exam-details';
	req.examId = examId;
	req.subjectId = subjectId;
	req.courseId = courseId;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else {
			data = res;
			//console.log(data);
			fillManualExam();
		}
	});
}
function fillManualExam() {
	//console.log(data);
	var exam = data.exam;
	var questionsCount = data.questionsCount;
	var questions = exam.questions;
	//console.log(exam);
	$('#titleName').val(exam.name);
	$('#chapterSelect').val(exam.chapterId);
}
$(document).ready(function(){
	$('#formManualExam').submit(function(){
		var examName = $('#titleName').val();
		var chapter = $('#chapterSelect').val();
		if (!examName) {
			toastr.error('Title is compulsory');
		} else if (!chapter) {
			toastr.error('Chapter is compulsory');
		} else {
			var req = {};
			var res;
			req.action = 'save-manual-exam';
			req.examId = examId;
			req.examName = examName;
			req.chapterId = chapter;
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndPoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0) {
					toastr.error(res.message);
				}
				else {
					toastr.success(res.message);
				}
			});
		}
		return false;
	});
});
</script>