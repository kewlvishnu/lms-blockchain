<script type="text/javascript">
var questions;
var index=-1;
var reqTag = {};
reqTag.action = 'tags';
var ms = $('#magicsuggest').magicSuggest({
	valueField: 'name',
	data: TypeEndPoint,
	dataUrlParams: reqTag
});
$(ms).on('keydown', keydownAlphaNumeric);
var msfilter = $('#tagsfilter').magicSuggest({
	valueField: 'name',
	data: TypeEndPoint,
	dataUrlParams: reqTag
});
$(msfilter).on('keydown', keydownAlphaNumeric);
function fetchSubjectiveExam() {
	var req = {};
	var res;
	req.action = 'get-subjectiveExam';
	req.subjectId = subjectId;
	req.courseId = courseId;
	req.examId =   examId;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
		{
			toastr.error(res.message);
			//window.location.href =sitePath+'404';
		}				
		else{
			if (res.questions) {
				if (res.questions.length>0) {
					$('.makelive').prop('disabled', false);
				}
			}
			fillSubjectiveExam(res);
		}
			
	});
}
function fillSubjectiveExam(data) {
	$("#examTotalMarks").html(data.totalMarks);
	$('#listQuestions').html('');
	if(data.exams[0].status == 2)
	{
		$('.makeNotlive').removeClass('hide');
		$('.makelive').addClass('hide');
		
	}
	else{
		$('.makeNotlive').addClass('hide');
		$('.makelive').removeClass('hide');
	}
	questions = data.questions;
	resetQuestionModal();
}
function resetQuestionModal() {
	$('.js-question-type').attr('disabled', false);
	$('#listQuestions').html('');
	CKEDITOR.instances["inputQuestion"].setData('');
	CKEDITOR.instances["inputAnswer"].setData('');
	$('#inputMarks').val('');
	$('#selectDifficulty').val(0);
	ms.clear();
	$('.marks').removeClass('hide');
	CKEDITOR.instances["inputQuestionDescription"].setData('');
	$('#inputNoOfQuestions').val('');
	$('#subquestionsMrks').val('');
	$('.chooseQuestionType').removeClass('hide');
	$('#optionQuestion').trigger('click');
	$('#allquestion').trigger('click');
	$('.js-tagging').removeClass('hide');
	$('#btnAddQuestion').attr('data-question','');
	$('#btnAddQuestion').attr('data-parent','');
	//fillSubjectiveExam();
	if (questions.length>0) {
		for (var i = 0; i < questions.length; i++) {
			if(questions[i].questionType == "question") {
				$('#listQuestions').append('<li class="mt-list-item done js-question-edit" data-question="'+i+'">'+
	                '<div class="list-icon-container">'+
	                    '<a href="javascript:;">'+
	                        (i+1)+
	                    '</a>'+
	                '</div>'+
	                '<div class="list-datetime">'+
	                	'<a href="javascript:void(0)" class="btn btn-danger btn-xs js-question-delete"><i class="fa fa-trash"></i></a>'+
	                '</div>'+
	                '<div class="list-item-content">'+
	                    '<h3 class="uppercase">'+
	                        '<a href="javascript:;">'+questions[i].questionBrief+'</a>'+
	                    '</h3>'+
	                    '<p class="text-overflow">'+questions[i].answerBrief+'</p>'+
	                    '<p class="font-green">Marks: '+questions[i].marks+'</p>'+
	                '</div>'+
	            '</li>');
			} else {
				$('#listQuestions').append('<li class="mt-list-item done js-question-edit" data-question="'+i+'">'+
	                '<div class="list-icon-container">'+
	                    '<a href="javascript:;">'+
	                        (i+1)+
	                    '</a>'+
	                '</div>'+
	                '<div class="list-datetime">'+
	                	'<a href="javascript:void(0)" class="btn green btn-xs js-subQuestions"><i class="fa fa-plus"></i></a>'+
	                	((questions[i].subQuestions.length==0)?'<a href="javascript:void(0)" class="btn btn-danger btn-xs js-question-delete"><i class="fa fa-trash"></i></a>':'')+
	                '</div>'+
	                '<div class="list-item-content">'+
	                    '<h3 class="uppercase">'+
	                        '<a href="javascript:;">'+((questions[i].questionsonPage==1)?(questions[i].questionBrief):'Question Group')+'</a>'+
	                    '</h3>'+
	                    '<p><small>'+((questions[i].questionsonPage==1)?('(QuestionGroup : In one page)'):('(QuestionGroup : One question per page)'))+'</small></p>'+
	                    '<p class="font-green">Marks per question: '+((questions[i].questionsonPage==1)?(questions[i].subquestionMrks):(questions[i].subquestionMrks))+'</p>'+
	                '</div>'+
	            '</li>');
	            var subQuestions = questions[i].subQuestions;
				if (subQuestions.length>0) {
					for (var j = 0; j < subQuestions.length; j++) {
						if(subQuestions[j].questionType == "question") {
							$('#listQuestions').append('<li class="mt-list-item done sub-question js-subquestion-edit" data-question="'+i+'" data-subquestion="'+j+'">'+
				                '<div class="list-icon-container">'+
				                    '<a href="javascript:;">'+
				                        (j+1)+
				                    '</a>'+
				                '</div>'+
				                '<div class="list-datetime">'+
				                	'<a href="javascript:void(0)" class="btn btn-danger btn-xs js-subquestion-delete"><i class="fa fa-trash"></i></a>'+
				                '</div>'+
				                '<div class="list-item-content">'+
				                    '<h3 class="uppercase">'+
				                        '<a href="javascript:;">'+subQuestions[j].questionBrief+'</a>'+
				                    '</h3>'+
				                    '<p class="text-overflow">'+subQuestions[j].answerBrief+'</p>'+
	                    			'<p class="font-green">Marks: '+questions[i].subquestionMrks+'</p>'+
				                '</div>'+
				            '</li>');
						}
					};
				};
			}
		}
	}
}
function resetQuestionShort() {
	$('.js-question-type').attr('disabled', false);
	$('#inputMarks').val('');
	$('#selectDifficulty').val(0);
	ms.clear();
	$('.marks').removeClass('hide');
	$('#inputNoOfQuestions').val('');
	$('#subquestionsMrks').val('');
	$('.chooseQuestionType').removeClass('hide');
	$('#optionQuestion').trigger('click');
	$('#allquestion').trigger('click');
	$('.js-tagging').removeClass('hide');
	$('#btnAddQuestion').attr('data-question','');
	$('#btnAddQuestion').attr('data-parent','');
}
function fetchImportCourses() {
	var req = {};
	var res;
	req.action = 'get-courses-for-exam';
	req.import = true;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 1)
			fillImportCourseSelect(res);
	});
}

function fillImportCourseSelect(data) {
	$('.courseSelect').find('option').remove();
	var html = '<option value="0">Select '+terminologies["course_single"]+'</option>';
	for(var i=0; i<data.courses.length; i++) {
		html += '<option value="' + data.courses[i].id + '">' + data.courses[i].name + '</option>';
	}
	$('.courseSelect').append(html);
	$('.courseSelect').prop('disabled', false);
}

function fetchImportSubjectsQuestions() {
	var req = {};
	var res;
	req.action = 'get-subjects-for-exam';
	req.import = true;
	req.courseId = $('#importSubjectQuestionsModal .courseSelect').val();
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 1)
			fillImportSubjectQuestions(res);
	});
}

function fillImportSubjectQuestions(data) {
	$('#importSubjectQuestionsModal .subjectSelect').find('option').remove();
	var html = '<option value="0">Select '+terminologies["subject_single"]+'</option>';
	for(var i=0; i<data.subjects.length; i++) {
		html += '<option value="' + data.subjects[i].id + '">' + data.subjects[i].name + '</option>';
	}
	$('#importSubjectQuestionsModal .subjectSelect').append(html);
	$('#importSubjectQuestionsModal .subjectSelect').attr('disabled', false);
}

function fetchImportSubjectExams() {
	var req = {};
	var res;
	req.action = 'get-subjective-exams-of-subject';
	req.import = true;
	req.subjectId = $('#importSubjectQuestionsModal .subjectSelect').val();
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 1)
			fillImportSubjectExams(res);
	});
}

function fillImportSubjectExams(data) {
	$('#importSubjectQuestionsModal .assignmentSelect').find('option').remove();
	var html = '<option value="0">All Exams</option>';
	for(var i=0; i<data.exams.length; i++) {
		html += '<option value="' + data.exams[i].id + '">' + data.exams[i].name + '</option>';
	}
	$('#importSubjectQuestionsModal .assignmentSelect').append(html);
	$('#importSubjectQuestionsModal .assignmentSelect').attr('disabled', false);
	$('#filterDifficulty').attr('disabled', false);
}

function fetchSubjectQuestions() {
	var req = {};
	var res;
	req.action = 'get-subjective-questions-import';
	req.subjectId = $('#importSubjectQuestionsModal .subjectSelect').val();
	req.currExamId = examId;
	req.examId = $('#importSubjectQuestionsModal .assignmentSelect').val();
	req.difficulty = $('#filterDifficulty').val();
	req.categoryId = $('#modalImportSubjectQuestions').attr('data-category');
	req.tags = msfilter.getValue();
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 0) {
			toastr.error(res.message);
		} else {
			fillSubjectQuestions(res);
		}
	});
}

function fillSubjectQuestions(data) {
	console.log(data);
	$('#listSubjectQuestions tbody').html('');
	var html = '';
	if(data.questions.length == 0)
		html += '<tr><td></td><td></td><td colspan="3">No Questions Found</td><td></td></tr>';
	for(var i=0; i<data.questions.length; i++) {
		html += '<tr data-id="' + data.questions[i].id + '" data-questionType="' + data.questions[i].questionType + '">'
					+ '<td><input type="checkbox" class="checkbox-inline selectbox"></td>'
					+ '<td>';
		if(data.questions[i].status == 0)
			html += '<i class="fa fa-minus-circle text-danger"></i></td>';
		else
			html += '<i class="fa fa-check-circle text-success"></i></td>';
		var type= '';
		if (data.questions[i].questionType == 'question') {
			var shortQuestion = $(data.questions[i].question).text();
			type = 'Question';
			if(shortQuestion.length > 35)
				shortQuestion = shortQuestion.substring(0, 33) + '...';
		} else {
			if (data.questions[i].questionsonPage==1) {
				type = 'Question Group - One Page';
				var shortQuestion = $(data.questions[i].question).text();
				if(shortQuestion.length > 35)
					shortQuestion = shortQuestion.substring(0, 33) + '...';
			} else {
				console.log(data.questions[i].subQuestions);
				type = 'Question Group - Multi Page';
				var shortQuestion = '';
				//var shortQuestion = '(QuestionGroup : One question per page)';
				if (data.questions[i].subQuestions.length>0) {
					var limitSubQ = 4;
					if (data.questions[i].subQuestions.length<4) {
						limitSubQ = data.questions[i].subQuestions.length;
					}
					for (var j = 0; j < limitSubQ; j++) {
						console.log(data.questions[i].subQuestions);
						shortQuestion+=data.questions[i].subQuestions[j].questionBrief.substring(0, 10)+','
					}
					shortQuestion = shortQuestion.substring(0,shortQuestion.length-1)+'...';
				} else {
					shortQuestion = '-- No questions feeded yet --';
				}
			}
		}
		html += '<td colspan="3">' + shortQuestion + '</td>';
		html += '<td>' + type + '</td>'
				+ '</tr>';
	}
	$('#listSubjectQuestions table tbody').append(html);
	$('#listSubjectQuestions table').removeClass('hide');
}

$(document).ready(function(){
	//to import selected questions from question bank
	$('#modalImportSubjectQuestions').on('click', function() {
		if($('#importSubjectQuestionsModal .importCategorySelect').val() != 0) {
			var flag = false;
			$('input[type="checkbox"]').each(function() {
				if($(this).prop('checked')) {
					flag = true;
					return;
				}
			});
			if(flag) {
				$(this).attr('disabled', true);
				var req = {};
				var res;
				req.action = 'import-subjective-questions';
				req.questions = [];
				$('#importSubjectQuestionsModal table tbody tr').each(function() {
					var obj = {};
					if($(this).find('input[type="checkbox"]:not(#selectAll)').prop('checked')) {
						obj.questionId = $(this).attr('data-id');
						obj.questionType = $(this).attr('data-questionType');
						obj.categoryId = $('#modalImportSubjectQuestions').attr('data-category');
						obj.parentId = 0;
						obj.examId = examId;
						req.questions.push(obj);
					}
				});
				$.ajax({
					'type'  : 'post',
					'url'   : ApiEndPoint,
					'data' 	: JSON.stringify(req)
				}).done(function (res) {
					$('#modalImportSubjectQuestions').attr('disabled', false);
					res =  $.parseJSON(res);
					$("#importSubjectQuestionsModal").modal('hide');
					if(res.status == 1)
						fetchSubjectiveExam();
					else
						toastr.error(res.message);
				});
			}
			else
				toastr.error('Please select at least one question.');
		}
		else
			toastr.error("Please select a import category first.");
	});
	//select all event listener
	$('#selectSQAll').on('change', function(){
		var fill = $(this).prop('checked');
		$('#importSubjectQuestionsModal tbody tr').each(function() {
			if($(this).css('display') != 'none')
				$(this).find('input[type="checkbox"]').prop('checked', fill);
		});
	});
	$("#formAddQuestion").on("change", ".js-question-type", function(){
		var questionType = $(this).val();
		$(".js-question-block").addClass("hide");
		$("#"+questionType+"Block").removeClass("hide");
	});
	$("#formAddQuestion").on("change", ".js-sub-question-type", function(){
		var subQuestionType = $(this).val();
		if (subQuestionType == "one") {
			$("#descriptionBlock").removeClass("hide");
		} else {
			$("#descriptionBlock").addClass("hide");
		}
	});
	$('.new-question').click(function(){
		var questionType = $(this).attr('data-type');
		resetQuestionModal();
		if(questionType == 'question') {
			$('#optionQuestion').trigger('click');
		} else {
			$('#optionQuestionGroup').trigger('click');
		}
	});
	$('#btnAddQuestion').click(function(){
		var req={};
		var questionType = $('.js-question-type:checked').val();
		var questionjson=[];
		var parentid='';
		var marks='';
		var difficulty = $('#selectDifficulty').val();
		var tags = ms.getValue();
		//console.log(ms.getSelection());
		var tagsJson = ms.getSelection();
		if (questionType == "question") {
			var question = CKEDITOR.instances['inputQuestion'].getData();
			var answer 	 = CKEDITOR.instances['inputAnswer'].getData();
			var marks	 = $('#inputMarks').val();
			
			//console.log($('#inputMarks').val());
			if($('#inputMarks').val()=='' || $('#inputMarks').val()==undefined)
			{
				marks=0;
			}
			if (!question || !answer) {
				toastr.error('Question and Answer both are compulsory!');
			} else {
				var questionEdit = $(this).attr('data-question');
				var parent = $(this).attr('data-parent');
				if (questionEdit == '' || questionEdit == undefined) {
					if (!parent) {
						var qno = questions.length+1;
						questions.push({id:'n'+qno,questionType:questionType,question:question,answer:answer,marks:marks,difficulty:difficulty,/*tags:tagsJson,*/subQuestions:[]});
						questionjson= ({id:'n'+qno,questionType:questionType,question:question,answer:answer,marks:marks,difficulty:difficulty,/*tags:tagsJson,*/subQuestions:[]});
						/*questions.push({id:qno,questionType:questionType,question:question,answer:answer,marks:marks,subQuestions:[]});
						questionjson= ({id:qno,questionType:questionType,question:question,answer:answer,marks:marks,subQuestions:[]});*/
						parentid='';
					}
					else {
						var qno = questions[parent]["subQuestions"].length+1;
						//console.log(questions[parent]);
						parentid=questions[parent].id;
						marks=questions[parent].subquestionMrks;
						//questions[parent]["subQuestions"].push({id:'s'+qno,questionType:questionType,question:question,answer:answer,marks:marks});
						questions[parent]["subQuestions"].push({id:qno,questionType:questionType,question:question,answer:answer,marks:marks});
						//console.log(marks);
						questionjson={id:'s'+qno,questionType:questionType,question:question,answer:answer,marks:marks};
						//questionjson={id:qno,questionType:questionType,question:question,answer:answer,marks:marks};
						//console.log({id:'s'+qno,questionType:questionType,question:question,answer:answer});
					}
					//saving question in db
					//console.log(tagsJson);
					req.questions=JSON.stringify(questionjson);
					req.parent=parentid;
					req.examId=examId;
					req.marks=marks;
					req.difficulty=difficulty;
					req.tags=tags;
					req.subjectId=subjectId;
					req.courseId=courseId;
					req.action = 'insert-subjective-questions-exam';
					//console.log(req);
					$.ajax({
						'type'	: 'post',
						'url'	: ApiEndPoint,
						'data'	: JSON.stringify(req)
					}).done(function (res) {
						res =  $.parseJSON(res);
						if(res.status == 0)
							toastr.error(res.message);
						else
						{
							if (!parent) {
								questions[questions.length-1].id			= res.questionId;
								questions[questions.length-1].questionBrief	= res.questionBrief;
								questions[questions.length-1].answerBrief	= res.answerBrief;
								questions[questions.length-1].tags			= res.tags;
							} else {
								questions[parent]["subQuestions"][questions[parent]["subQuestions"].length-1].id = res.questionId;
								questions[parent]["subQuestions"][questions[parent]["subQuestions"].length-1].questionBrief = res.questionBrief;
								questions[parent]["subQuestions"][questions[parent]["subQuestions"].length-1].answerBrief = res.answerBrief;
							}
							$('.makelive').prop('disabled', false);
							resetQuestionModal();
						}
					});
				} else {
					if (!parent) {
						questions[questionEdit].questionType = questionType;
						questions[questionEdit].question	 = question;
						questions[questionEdit].answer		 = answer;
						questions[questionEdit].marks		 = marks;
						questions[questionEdit].difficulty	 = difficulty;
						//questions[questionEdit].tags	 	 = tagsJson;
						questions[questionEdit].subQuestions = [];
						//console.log(questions[questionEdit]);
						questionjson=questions[questionEdit];
						//
					} else {
						questions[parent]["subQuestions"][questionEdit].questionType = questionType;
						questions[parent]["subQuestions"][questionEdit].question	 = question;
						questions[parent]["subQuestions"][questionEdit].answer		 = answer;
						questions[parent]["subQuestions"][questionEdit].marks		 = marks;
						//console.log(questions[parent]["subQuestions"][questionEdit]);
						//console.log(questions[parent].id);
						questionjson=questions[parent]["subQuestions"][questionEdit];
						parentid=questions[parent].id;
						marks=questions[parent].subquestionMrks;
						
					}
					req.questions=JSON.stringify(questionjson);
					req.parent=parentid;
					req.subjectiveExamId=examId;
					req.marks=marks;
					req.difficulty=difficulty;
					req.tags=tags;
					req.subjectId=subjectId;
					req.courseId=courseId;
					req.action = 'edit-subjective-questions-exam';
					//console.log(tagsJson);
					$.ajax({
						'type'	: 'post',
						'url'	: ApiEndPoint,
						'data'	: JSON.stringify(req)
					}).done(function (res) {
						res =  $.parseJSON(res);
						if(res.status == 0)
							toastr.error(res.message);
						else
						{
							if (!parent) {
								questions[questionEdit].questionBrief	= res.questionBrief;
								questions[questionEdit].answerBrief	= res.answerBrief;
								questions[questionEdit].tags		= res.tags;
							} else {
								questions[parent]["subQuestions"][questionEdit].questionBrief = res.questionBrief;
								questions[parent]["subQuestions"][questionEdit].answerBrief = res.answerBrief;
							}
							//toastr.error("Question paper saved sucessfully. Please make exam Live");
							$('.makelive').prop('disabled', false);
							resetQuestionModal();
						}
					});
				}
			}
		} 
		else {
			var question = CKEDITOR.instances['inputQuestionDescription'].getData();
			var noOfQuestions = $('#inputNoOfQuestions').val();		
			var subquestionMrks=$('#subquestionsMrks').val();
			var questionsonPage=1;
			var subquestions=[];
			if(!(index<0))
			{
				subquestions=questions[index].subQuestions;
			}
			if($('#onequestion').prop('checked'))
			{
				questionsonPage=2;
			}
			if (!noOfQuestions || noOfQuestions < 0 || subquestionMrks < 0) {
				$('#questionGroupBlock .help-block').html('<p class="text-danger">Question Group Description and No of Questions both are compulsory and  No of Questions and subquestions marks should be greater then 0!</p>');
			} else {
				var questionEdit = $(this).attr('data-question');
				if (!questionEdit) {
					var qno = questions.length+1;
					questions.push({id:'n'+qno,questionType:questionType,question:question,answer:noOfQuestions,subquestionMrks:subquestionMrks,questionsonPage:questionsonPage,difficulty:difficulty,/*tags:tagsJson,*/subQuestions:[]});
					questionjson		= ({id:'n'+qno,questionType:questionType,question:question,answer:noOfQuestions,subquestionMrks:subquestionMrks,questionsonPage:questionsonPage,difficulty:difficulty,/*tags:tagsJson,*/subQuestions:[]});
					/*questions.push({id:qno,questionType:questionType,question:question,answer:noOfQuestions,subquestionMrks:subquestionMrks,questionsonPage:questionsonPage,subQuestions:[]});
					questionjson		= ({id:qno,questionType:questionType,question:question,answer:noOfQuestions,subquestionMrks:subquestionMrks,questionsonPage:questionsonPage,subQuestions:[]});*/
					//console.log(questionjson);
					req.questions		= JSON.stringify(questionjson);
					req.tags			= tags;
					req.parent			= parentid;
					req.examId			= examId;
					req.subjectId		= subjectId;
					req.courseId		= courseId;
					req.action			= 'insert-subjective-questions-exam';
					//console.log(tagsJson);
					$.ajax({
						'type'	: 'post',
						'url'	: ApiEndPoint,
						'data'	: JSON.stringify(req)
					}).done(function (res) {
						res =  $.parseJSON(res);
						if(res.status == 0)
							toastr.error(res.message);
						else
						{
							questions[questions.length-1].id			= res.questionId;
							questions[questions.length-1].questionBrief	= res.questionBrief;
							questions[questions.length-1].answerBrief	= res.answerBrief;
							questions[questions.length-1].tags			= res.tags;
							//toastr.error("Question paper saved sucessfully. Please make exam Live");
							$('.makelive').prop('disabled', false);
							resetQuestionModal();
						}
					});
				} else {
					questions[questionEdit].questionType = questionType;
					questions[questionEdit].question	 = question;
					questions[questionEdit].answer		 = noOfQuestions;
					questions[questionEdit].subquestionMrks	 = subquestionMrks;
					questions[questionEdit].questionsonPage	 = questionsonPage;
					questions[questionEdit].difficulty	 = difficulty;
					//questions[questionEdit].tags	 	 = tagsJson;
					questions[questionEdit].subQuestions = subquestions;
					questionjson=questions[questionEdit];
					
					req.questions=JSON.stringify(questionjson);
					req.tags			= tags;
					req.parent=parentid;
					req.subjectiveExamId=examId;
					req.marks=marks;
					req.subjectId=subjectId;
					req.courseId=courseId;
					req.action = 'edit-subjective-questions';
					//console.log(tagsJson);
					$.ajax({
						'type'	: 'post',
						'url'	: ApiEndPoint,
						'data'	: JSON.stringify(req)
					}).done(function (res) {
						res =  $.parseJSON(res);
						if(res.status == 0)
							toastr.error(res.message);
						else
						{
							questions[questionEdit].questionBrief	= res.questionBrief;
							questions[questionEdit].answerBrief		= res.answerBrief;
							questions[questionEdit].tags		= res.tags;
							//toastr.error("Question paper saved sucessfully. Please make exam Live");
							$('.makelive').prop('disabled', false);
							resetQuestionModal();
						}
					});
				}
				//console.log(JSON.stringify(questions));
			}
		}
	});
	$('#listQuestions').on('click', '.js-subQuestions', function(){
		resetQuestionModal();
		var question = $(this).closest('.mt-list-item').attr('data-question');
		$('#btnAddQuestion').attr('data-parent',question);
		$('#optionQuestion').trigger('click');
		$('.chooseQuestionType').addClass('hide');
		$('.marks').addClass('hide');
		$('.js-question-type').attr('disabled', true);
		return false;
	});
	$('#listQuestions').on('click', '.js-question-edit', function(e){
		if(e.target.className=="btn green btn-xs js-subQuestions" || e.target.className=="fa fa-plus") {
			return;
		}
		//console.log("I came here");
		resetQuestionShort();
		var questionEdit = $(this).attr('data-question');
		index=questionEdit;
		console.log(questions[questionEdit]);
		var question	= questions[questionEdit].question;
		var answer		= questions[questionEdit].answer;
		var marks		= questions[questionEdit].marks;
		var difficulty	= questions[questionEdit].difficulty;
		var tags		= questions[questionEdit].tags;
		//console.log(difficulty);
		if(questions[questionEdit].questionType == "question") {
			//$('#optionQuestion').trigger('click');
			CKEDITOR.instances["inputQuestion"].setData(question);
			CKEDITOR.instances["inputAnswer"].setData(answer);
			$('#inputMarks').val(marks);
		} else {
			//console.log(questions[questionEdit].subquestionMrks);
			var subquestionMrks=questions[questionEdit].subquestionMrks;
			var questionsonPage=questions[questionEdit].questionsonPage;
			$('#optionQuestionGroup').trigger('click');
			CKEDITOR.instances["inputQuestionDescription"].setData(question);
			//$('#inputQuestionDescription').val(question);
			$('#inputNoOfQuestions').val(answer);
			$('#subquestionsMrks').val(subquestionMrks);
			if(questionsonPage==2)
			{
				//questionsonPage=2;
				$('#onequestion').trigger('click')
			}else{
				$('#allquestion').trigger('click')
			}
		}
		$('#selectDifficulty').val(difficulty);
		//console.log(tags);
		//ms.setData(tags);
		ms.setSelection(tags);
		$('.js-tagging').removeClass('hide');
		$('#btnAddQuestion').attr('data-question',questionEdit);
		$('.js-question-type').attr('disabled', true);
	});
	$('#listQuestions').on('click', '.js-subquestion-edit', function(e){
		resetQuestionShort();
		var questionEdit = $(this).attr('data-question');
		var subQuestionEdit = $(this).attr('data-subquestion');
		var question = questions[questionEdit]["subQuestions"][subQuestionEdit].question;
		var answer = questions[questionEdit]["subQuestions"][subQuestionEdit].answer;
		//var marks=  questions[questionEdit]["subQuestions"][subQuestionEdit].marks;
		var marks=  questions[questionEdit].subquestionMrks;
		if(questions[questionEdit]["subQuestions"][subQuestionEdit].questionType == "question") {
			$('#optionQuestion').trigger('click');
			CKEDITOR.instances["inputQuestion"].setData(question);
			CKEDITOR.instances["inputAnswer"].setData(answer);
			//$('#inputQuestion').val(question);
			//$('#inputAnswer').val(answer);
			$('#inputMarks').val(marks);
		}
		$('#btnAddQuestion').attr('data-question',subQuestionEdit);
		$('#btnAddQuestion').attr('data-parent',questionEdit);
		$('.chooseQuestionType').addClass('hide');
		$('.marks').addClass('hide');
		$('.js-tagging').addClass('hide');
		$('.js-question-type').attr('disabled', true);
		//$('#optionQuestionGroup').prop('disabled', true);
		//$('#questionModal').modal('show');
	});
	$('#listQuestions').on('click', '.js-question-delete', function(){
		var questionEdit = $(this).closest('.mt-list-item').attr('data-question');
		//console.log(JSON.stringify(questions[questionEdit]));
		var subQuestionsCheck=questions[questionEdit]["subQuestions"];
		if(subQuestionsCheck.length>0)
		{
			toastr.error("Please delete Subquestions before deleting Main questions");
		}
		else{
			var parentid='';
			var req={};
			req.questions=JSON.stringify(questions[questionEdit]);
			req.parent=parentid;
			req.subjectiveExamId=examId;
			req.subjectId=subjectId;
			req.courseId=courseId;
			req.action = 'delete-subjective-questions';
			//console.log(req);
			$.ajax({
			'type'  : 'post',
			'url'   : ApiEndPoint,
			'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else
				{
					questions.splice(questionEdit,1);
					resetQuestionModal();
					//toastr.error("Question paper saved sucessfully. Please make exam Live");
					//$('.makelive').prop('disabled', false);
				}
			});
		}
	});
	$('#listQuestions').on('click', '.js-subquestion-delete', function(){
		var questionDiv = $(this).closest('.mt-list-item');
		var questionEdit = questionDiv.attr('data-question');
		var subQuestionEdit = questionDiv.attr('data-subquestion');
		var parentid='';
		var req={};
		req.questions=JSON.stringify(questions[questionEdit]["subQuestions"][subQuestionEdit]);
		req.parent=questions[questionEdit].id;
		req.subjectiveExamId=examId;
		req.subjectId=subjectId;
		req.courseId=courseId;
		req.action = 'delete-subjective-questions';
		//console.log(req);
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndPoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				toastr.error(res.message);
			else
			{
				questions[questionEdit]["subQuestions"].splice(subQuestionEdit,1);
				resetQuestionModal();
				/*console.log(questions[questionEdit]["subQuestions"].length);
				if (questions[questionEdit]["subQuestions"].length==0) {
					questionDiv.find('.list-datetime').append('<a href="javascript:void(0)" class="btn btn-danger btn-xs js-question-delete"><i class="fa fa-trash"></i></a>');
				}*/
				//toastr.error("Question paper saved sucessfully. Please make exam Live");
				//$('.makelive').prop('disabled', false);
			}
		});
	});
	$('#importSubjectQuestionsModal .courseSelect').on('change', function() {
		$('#importSubjectQuestionsModal .subjectSelect').attr('disabled', true);
		$('#importSubjectQuestionsModal .subjectSelect').val(0);
		$('#importSubjectQuestionsModal .assignmentSelect').attr('disabled', true);
		$('#importSubjectQuestionsModal .assignmentSelect').val(0);
		$('#filterDifficulty').attr('disabled', true);
		$('#filterDifficulty').val(0);
		$('#importSubjectQuestionsModal table').addClass('hide');
		if($(this).val() != 0)
			fetchImportSubjectsQuestions();
	});	
	$('#importSubjectQuestionsModal .subjectSelect').on('change', function() {
		$('#importSubjectQuestionsModal .assignmentSelect').attr('disabled', true);
		$('#importSubjectQuestionsModal .assignmentSelect').val(0);
		$('#filterDifficulty').attr('disabled', true);
		$('#filterDifficulty').val(0);
		$('#importSubjectQuestionsModal table').addClass('hide');
		if($(this).val() != 0) {
			fetchImportSubjectExams();
			fetchSubjectQuestions();
		}
	});	
	$('#importSubjectQuestionsModal .assignmentSelect').on('change', function() {
		$('#importSubjectQuestionsModal table').addClass('hide');
		fetchSubjectQuestions();
	});	
	$('#filterDifficulty').on('change', function() {
		$('#importSubjectQuestionsModal table').addClass('hide');
		fetchSubjectQuestions();
	});
	$(msfilter).on('selectionchange', function(e,m){
		//console.log("values: " + JSON.stringify(this.getValue()));
		fetchSubjectQuestions();
	});	
	$('#importSubjectQuestionsModal').on('hidden.bs.modal', function() {
		$('#importSubjectQuestionsModal .cancel-button').click();
	});	
	//For cancel button modal reset
	$('#importSubjectQuestionsModal .cancel-button').on('click', function() {
		$('#importSubjectQuestionsModal .subjectSelect').attr('disabled', true);
		$('#importSubjectQuestionsModal .subjectSelect').val(0);
		$('#importSubjectQuestionsModal .assignmentSelect').attr('disabled', true);
		$('#importSubjectQuestionsModal .assignmentSelect').val(0);
		$('#importSubjectQuestionsModal .courseSelect').val(0);
		$('#filterDifficulty').attr('disabled', true);
		$('#filterDifficulty').val(0);
		$('#importSubjectQuestionsModal table').addClass('hide');
	});
	$('.makelive').click(function(){
		if(questions.length>0){
			var req={};
			req.subjectiveExamId=examId;
			req.action = 'live-subjective-questions';
			//console.log(subjectiveExamId);
			$.ajax({
			'type'  : 'post',
			'url'   : ApiEndPoint,
			'data' 	: JSON.stringify(req)
			}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				toastr.error(res.message);
			else
			{	
				toastr.success("Exam is Live now");
				location.reload();
			}
	    	});
		}else{
			toastr.error("Unable to make exam live.Please Add questions.");
		}
	});	
	$('.makeNotlive').click(function(){
		var req={};
		req.subjectiveExamId=examId;
		req.action = 'unlive-subjective-questions';
		//console.log(subjectiveExamId);
		$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				toastr.error(res.message);
			else
			{	
				toastr.success("Exam is Not Live now");
				location.reload();
			}
		});
	});
});
</script>