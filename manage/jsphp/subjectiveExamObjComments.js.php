<script type="text/javascript">
var handleValidation1 = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form1 = $('#formScorePercentBracket');
    var error1 = $('.alert-danger', form1);
    var success1 = $('.alert-success', form1);

    form1.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input
        rules: {
            percentLow: {
                required: true,
                digits: true
            },
            percentHigh: {
                required: true,
                digits: true,
                greaterThan: ["#percentLow1","Percent Low"]
            },
            comment: {
                minlength: 2,
                required: true
            },
            optionSmiley: {
                required: true
            }
        },

        messages: { // custom messages for radio buttons and checkboxes
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.parent(".input-group").size() > 0) {
                error.insertAfter(element.parent(".input-group"));
            } else if (element.attr("data-error-container")) { 
                error.appendTo(element.attr("data-error-container"));
            } else if (element.parents('.radio-list').size() > 0) { 
                error.appendTo(element.parents('.radio-list').attr("data-error-container"));
            } else if (element.parents('.radio-inline').size() > 0) { 
                error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
            } else if (element.parents('.checkbox-list').size() > 0) {
                error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
            } else if (element.parents('.checkbox-inline').size() > 0) { 
                error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success1.hide();
            error1.show();
            App.scrollTo(error1, -200);
        },

        highlight: function (element) { // hightlight error inputs
           $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function (form) {
            //success1.show();
            error1.hide();
            //form[0].submit(); // submit the form
            savePercentBracket(form,success1);
            //toastr.success("Hurray!");
        }

    });
    $.validator.addMethod("greaterThan", function(value, element, params) {
        return (Number(value) > Number($(params[0]).val())); 
    },'Must be greater than {1}.');
}
var handleValidation2 = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form2 = $('#formTagPercentBracket');
    var error2 = $('.alert-danger', form2);
    var success2 = $('.alert-success', form2);

    form2.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input
        rules: {
            percentLow: {
                required: true,
                digits: true
            },
            percentHigh: {
                required: true,
                digits: true,
                greaterThan: ["#percentLow2","Percent Low"]
            },
            comment: {
                minlength: 2,
                required: true
            },
            optionSmiley: {
                required: true
            }
        },

        messages: { // custom messages for radio buttons and checkboxes
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.parent(".input-group").size() > 0) {
                error.insertAfter(element.parent(".input-group"));
            } else if (element.attr("data-error-container")) { 
                error.appendTo(element.attr("data-error-container"));
            } else if (element.parents('.radio-list').size() > 0) { 
                error.appendTo(element.parents('.radio-list').attr("data-error-container"));
            } else if (element.parents('.radio-inline').size() > 0) { 
                error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
            } else if (element.parents('.checkbox-list').size() > 0) {
                error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
            } else if (element.parents('.checkbox-inline').size() > 0) { 
                error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success2.hide();
            error2.show();
            App.scrollTo(error2, -200);
        },

        highlight: function (element) { // hightlight error inputs
           $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function (form) {
            //success2.show();
            error2.hide();
            //form[0].submit(); // submit the form
            savePercentBracket(form,success2);
            //toastr.success("Hurray!");
        }

    });
    $.validator.addMethod("greaterThan", function(value, element, params) {
        return (Number(value) > Number($(params[0]).val())); 
    },'Must be greater than {1}.');
}
var handleValidation3 = function() {
    // for more info visit the official plugin documentation: 
    // http://docs.jquery.com/Plugins/Validation

    var form3 = $('#formScorePercentBracketEdit');
    var error3 = $('.alert-danger', form3);
    var success3 = $('.alert-success', form3);

    form3.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "", // validate all fields including form hidden input
        rules: {
            percentLow: {
                required: true,
                digits: true
            },
            percentHigh: {
                required: true,
                digits: true,
                greaterThan: ["#percentLowEdit","Percent Low"]
            },
            comment: {
                minlength: 2,
                required: true
            },
            optionSmiley: {
                required: true
            }
        },

        messages: { // custom messages for radio buttons and checkboxes
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.parent(".input-group").size() > 0) {
                error.insertAfter(element.parent(".input-group"));
            } else if (element.attr("data-error-container")) { 
                error.appendTo(element.attr("data-error-container"));
            } else if (element.parents('.radio-list').size() > 0) { 
                error.appendTo(element.parents('.radio-list').attr("data-error-container"));
            } else if (element.parents('.radio-inline').size() > 0) { 
                error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
            } else if (element.parents('.checkbox-list').size() > 0) {
                error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
            } else if (element.parents('.checkbox-inline').size() > 0) { 
                error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success3.hide();
            error3.show();
            App.scrollTo(error3, -200);
        },

        highlight: function (element) { // hightlight error inputs
           $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function (form) {
            //success3.show();
            error3.hide();
            //form[0].submit(); // submit the form
            savePercentBracketEdit(form,success3);
            //toastr.success("Hurray!");
        }

    });
    $.validator.addMethod("greaterThan", function(value, element, params) {
        return (Number(value) > Number($(params[0]).val())); 
    },'Must be greater than {1}.');
}
function savePercentBracket(form,success) {
    var req = {};
    var res;
    req.action = 'save-percent-bracket';
    req.subjectId = subjectId;
    req.examId = examId;
    req.examType = 'subjective-exam';
    req.percentLow = $(form).find('[name=percentLow]').val();
    req.percentHigh = $(form).find('[name=percentHigh]').val();
    req.comment = $(form).find('[name=comment]').val();
    req.smiley = $(form).find('[name=optionSmiley]:checked').val();
    if($(form).attr('id') == 'formScorePercentBracket') {
        req.bracketType = 'score';
    } else {
        req.bracketType = 'tag';
    }
    ajaxRequest({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req),
        success:function(res) {
            res = $.parseJSON(res);
            
            if (res.status == 0)
                toastr.error(res.message);
            else {
                //console.log(res);
                success.show();
                success.focus();
                window.location = window.location;
            }
        }
    });
}
function savePercentBracketEdit(form,success) {
    var req = {};
    var res;
    req.action = 'save-percent-bracket-edit';
    req.subjectId = subjectId;
    req.examId = examId;
    req.examType = 'subjective-exam';
    req.percentLow = $(form).find('[name=percentLow]').val();
    req.percentHigh = $(form).find('[name=percentHigh]').val();
    req.comment = $(form).find('[name=comment]').val();
    req.smiley = $(form).find('[name=optionSmiley]:checked').val();
    req.bracketId = $(form).find('[name=rangeId]').val();
    req.bracketType = $(form).find('[name=rangeType]').val();
    ajaxRequest({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req),
        success:function(res) {
            res = $.parseJSON(res);
            
            if (res.status == 0)
                toastr.error(res.message);
            else {
                //console.log(res);
                success.show();
                success.focus();
                window.location = window.location;
            }
        }
    });
}
function fetchTagAnalysis() {
	var req = {};
	var res;
	req.action = 'get-exam-tags';
	req.subjectId = subjectId;
	req.examId = examId;
	req.examType = 'subjective-exam';
	ajaxRequest({
		'type': 'post',
		'url': ApiEndPoint,
		'data': JSON.stringify(req),
		success:function(res) {
			res = $.parseJSON(res);
			
			if (res.status == 0)
				toastr.error(res.message);
			else {
				//console.log(res);
				$('#tableTagAnalysis tbody').html('');
                var totalQuestions = $('#totalQuestions').text();
                console.log(totalQuestions);
                if (totalQuestions) {
    				if (res.tagData.length>0) {
    					var tagData = res.tagData;
    					//console.log(totalQuestions);
    					for (var i = 0; i < tagData.length; i++) {
    						$('#tableTagAnalysis tbody').append(
    							'<tr>'+
    								'<td>'+(i+1)+'</td>'+
    								'<td>'+tagData[i].tag+'</td>'+
    								'<td>'+tagData[i].appear_count+'</td>'+
    								'<td>'+round2(parseFloat((tagData[i].appear_count/totalQuestions)*100))+'</td>'+
    							'</tr>');
    					}
    				} else {
    					$('#tableTagAnalysis tbody').append(
    							'<tr>'+
    								'<td colspan="4">No data found</td>'+
    							'</tr>');
    				}
                } else {
                    $('#tableTagAnalysis').closest(".table-responsive").addClass('hide');
                }
			}
		}
	});
}
function getSmileys() {
	var req = {};
	var res;
	req.action = 'get-smileys';
	ajaxRequest({
		'type': 'post',
		'url': ApiEndPoint,
		'data': JSON.stringify(req),
		success:function(res) {
			res = $.parseJSON(res);
			//console.log(res);
			
			if (res.status == 0)
				toastr.error(res.message);
			else {
				//console.log(res);
				$('.list-smileys').html('');
				if (res.smileys.length>0) {
					var smileys = res.smileys;
					for (var i = 0; i < smileys.length; i++) {
						$('.list-smileys-1').append(
							'<label class="btn btn-default">'+
								'<input type="radio" name="optionSmiley" value="'+smileys[i].id+'" data-error-container="#smileyError1" class="toggle option-smiley">'+
								'<img src="'+sitepathManageIncludes+'assets/custom/img/smileys/'+smileys[i].file+'" class="img-responsive" alt="'+smileys[i].title+'">'+
							'</label>');
                        $('.list-smileys-2').append(
                            '<label class="btn btn-default">'+
                                '<input type="radio" name="optionSmiley" value="'+smileys[i].id+'" data-error-container="#smileyError2" class="toggle option-smiley">'+
                                '<img src="'+sitepathManageIncludes+'assets/custom/img/smileys/'+smileys[i].file+'" class="img-responsive" alt="'+smileys[i].title+'">'+
                            '</label>');
                        $('.list-smileys-3').append(
                            '<label class="btn btn-default">'+
                                '<input type="radio" name="optionSmiley" value="'+smileys[i].id+'" data-error-container="#smileyError3" class="toggle option-smiley">'+
                                '<img src="'+sitepathManageIncludes+'assets/custom/img/smileys/'+smileys[i].file+'" class="img-responsive" alt="'+smileys[i].title+'">'+
                            '</label>');
					}
				}
                fillRange();
                handleValidation1();
                handleValidation2();
			}
		}
	});
}
function getPercentRanges() {
    var req = {};
    var res;
    req.action = 'get-percent-ranges';
    req.examId = examId;
    req.examType = 'subjective-exam';
    ajaxRequest({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req),
        success:function(res) {
            res = $.parseJSON(res);
            //console.log(res);
            
            if (res.status == 0)
                toastr.error(res.message);
            else {
                var scoreRanges = res.score;
                if (scoreRanges.length>0) {
                    $('#tableScoreRanges tbody').html('');
                    for (var i = 0; i < scoreRanges.length; i++) {
                        $('#tableScoreRanges tbody').append(
                            '<tr data-id="'+scoreRanges[i].id+'" data-type="score">'+
                                '<td>'+(i+1)+'</td>'+
                                '<td><span class="js-percent-low">'+scoreRanges[i].percentLow+'</span> - <span class="js-percent-high">'+scoreRanges[i].percentHigh+'</span></td>'+
                                '<td class="js-comment">'+scoreRanges[i].comment+'</td>'+
                                '<td class="text-center js-smiley" data-smiley="'+scoreRanges[i].smiley+'"><img src="'+sitepathManageIncludes+'assets/custom/img/smileys/'+scoreRanges[i].file+'" class="img-responsive smiley" /></td>'+
                                '<td><button class="btn btn-xs green js-edit-range"><i class="fa fa-edit"></i></button><button class="btn btn-xs red js-delete-range"><i class="fa fa-remove"></i></button></td>'+
                            '</tr>'
                            );
                    }
                };
                var tagRanges = res.tag;
                if (tagRanges.length>0) {
                    $('#tableTagRanges tbody').html('');
                    for (var i = 0; i < tagRanges.length; i++) {
                        $('#tableTagRanges tbody').append(
                            '<tr data-id="'+tagRanges[i].id+'" data-type="tag">'+
                                '<td>'+(i+1)+'</td>'+
                                '<td><span class="js-percent-low">'+tagRanges[i].percentLow+'</span> - <span class="js-percent-high">'+tagRanges[i].percentHigh+'</span></td>'+
                                '<td class="js-comment">'+tagRanges[i].comment+'</td>'+
                                '<td class="text-center js-smiley" data-smiley="'+tagRanges[i].smiley+'"><img src="'+sitepathManageIncludes+'assets/custom/img/smileys/'+tagRanges[i].file+'" class="img-responsive smiley" /></td>'+
                                '<td><button class="btn btn-xs green js-edit-range"><i class="fa fa-edit"></i></button><button class="btn btn-xs red js-delete-range"><i class="fa fa-remove"></i></button></td>'+
                            '</tr>'
                            );
                    }
                }
                
            }
        }
    });
}
$(document).ready(function(){
    $('#rangePercentBracketEdit').click(function(){
        $('#formScorePercentBracketEdit').trigger("submit");
    });
    $('#modalExamCommentsEdit').on('hidden.bs.modal', function (e) {
        $('#percentLowEdit').val(0);
        $('#percentHighEdit').val(0);
        $('#commentEdit').val('');
        $('#formScorePercentBracketEdit .list-smileys').find('.option-smiley').prop('checked', false);
        $('#formScorePercentBracketEdit .list-smileys').find('.btn').removeClass('active');
        $('#rangeId').val(0);
        $('#rangeType').val('');
        $('.range-type').html('');
    });
});
function fillRange(){
    $('.table').on('click','.js-edit-range',function(){
        var obj = $(this).closest("tr");
        var rangeId = obj.attr('data-id');
        var rangeType = obj.attr('data-type');
        var percentLow = obj.find('.js-percent-low').text();
        var percentHigh = obj.find('.js-percent-high').text();
        var comment = obj.find('.js-comment').text();
        var smiley = obj.find('.js-smiley').attr('data-smiley');
        $('#percentLowEdit').val(percentLow);
        $('#percentHighEdit').val(percentHigh);
        $('#commentEdit').val(comment);
        $('#formScorePercentBracketEdit .list-smileys').find('.option-smiley[value='+smiley+']').trigger('click');
        $('#rangeId').val(rangeId);
        $('#rangeType').val(rangeType);
        $('.range-type').html(rangeType);
        $('#modalExamCommentsEdit').modal('show');
    });
    $('.table').on('click','.js-delete-range',function(){
        var obj = $(this).closest("tr");
        var rangeId = obj.attr('data-id');
        var rangeType = obj.attr('data-type');
        var req = {};
        var res;
        req.action = 'delete-percent-bracket';
        req.subjectId = subjectId;
        req.examId = examId;
        req.examType = 'subjective-exam';
        req.bracketId = rangeId;
        req.bracketType = rangeType;
        ajaxRequest({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req),
            success:function(res) {
                res = $.parseJSON(res);
                
                if (res.status == 0)
                    toastr.error(res.message);
                else {
                    //console.log(res);
                    toastr.success(res.message);
                    //window.location = window.location;
                    $('tr[data-id="'+rangeId+'"][data-type="'+rangeType+'"]').remove();
                }
            }
        });
    });
    handleValidation3();
}
</script>