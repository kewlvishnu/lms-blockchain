<script type="text/javascript">
function fillCourses(data) {
	//console.log(data.courses);
    $('#listCourses').html('');
	if (data.courses.length>0) {
        $.each(data.courses, function (i, course) {
            var startDate = new Date(course.liveDate);
            startDate = timeSince(startDate.getTime()); //returns 1340220044000
            $('#listCourses').append('<div class="col-md-6 col-sm-12 js-course-list" data-course="'+course.id+'">'+
                '<div class="portlet box portlet-course blue-hoki">'+
                    '<div class="portlet-title">'+
                        '<div class="caption">'+
                            '<i class="fa fa-gift"></i>'+course.name+'</div>'+
                        '<div class="tools">'+
                            '<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
                            '<a href="" class="fullscreen" data-original-title="" title=""> </a>'+
                        '</div>'+
                        '<div class="actions">'+
                            '<a href="'+sitepathManageCourses+course.id+'" class="btn btn-default btn-sm">'+
                                '<i class="fa fa-eye"></i> Open </a>'+<?php /*
                            '<a href="javascript:;" class="btn btn-default btn-sm btn-course edit-course">'+
                                '<i class="fa fa-pencil"></i> Edit </a>'+*/ ?>
                           '<a href="javascript:;" class="btn btn-default btn-sm delete-course">'+
                                '<i class="fa fa-trash"></i> Delete </a>'+
                        '</div>'+
                    '</div>'+
                    '<div class="portlet-body portlet-body-sm">'+
                        '<div class="row js-subjects-list"></div>'+
                    '</div>'+
                '</div>'+
            '</div>');
            if (course.subjects.length>0) {
                $.each(course.subjects, function (i, subject) {
                    $('#listCourses .js-course-list:last-child').find('.js-subjects-list').append(
                        '<div class="col-lg-4 col-md-6">'+
                            '<div class="mt-widget-4 margin-bottom-20">'+
                                '<div class="mt-img-container">'+
                                    '<img src="' + subject.image + '" /> </div>'+
                                '<div class="mt-container bg-blue-hoki-opacity">'+
                                    '<div class="mt-head-title"> ' + subject.name + ' </div>'+
                                    '<div class="mt-body-icons">'+
                                        '<a href="'+sitepathManageSubjects+subject.id+'" title="Lectures">'+
                                            '<i class="icon-notebook"></i><span class="count">' + subject.chapters + '</span>'+
                                        '</a>'+
                                        '<a href="'+sitepathManageSubjects+subject.id+'" title="Exams">'+
                                            '<i class="icon-note"></i><span class="count">' + subject.exams + '</span>'+
                                        '</a>'+
                                    '</div>'+
                                    '<a href="'+sitepathManageSubjects+subject.id+'">'+
                                        '<div class="mt-footer-button">'+
                                            '<button type="button" class="btn btn-circle btn-danger btn-sm"><i class="fa fa-edit"></i> Edit</button>'+
                                        '</div>'+
                                    '</a>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>');
                });
            }
            $('#listCourses .js-course-list:last-child').find('.js-subjects-list').append(
                '<div class="col-lg-4 col-md-6">'+
                    '<a href="javascript:void(0)" data-action="new" data-toggle="modal" class="mt-widget-4 bg-blue-opacity margin-bottom-20 btn-subject">'+
                        '<span class="image bg-green bg-font-green new">'+
                            '<h4><i class="fa fa-plus-circle"></i><span>'+terminologies["subject_single"]+'</span></h4>'+
                        '</span>'+
                        '<span class="mask">'+
                            '<span class="actions text-center">'+
                                '<span class="fa fa-plus"></span>'+
                            '</span>'+
                        '</span>'+
                    '</a>'+
                '</div>');
        });
    }
    $('#listCourses').append(
            '<div class="col-md-2 col-sm-12">'+
                '<div class="sparkline-chart ar-widget-1">'+
                    '<a class="title btn-course" href="javascript:void(0)" data-action="new" data-toggle="modal">'+
                        '<span class="preview">'+
                            '<span class="image bg-grey-cararra bg-font-grey-cararra">'+
                                '<h4><i class="fa fa-plus-circle"></i><span>'+terminologies["course_single"]+'</span></h4>'+
                            '</span>'+
                            '<span class="mask">'+
                                '<span class="actions">'+
                                    '<span class="fa fa-plus"></span>'+
                                '</span>'+
                            '</span>'+
                        '</span>'+
                    '</a>'+
                '</div>'+
            '</div>');
    $('#listCourses .delete-course').on('click', function(e) {
        e.preventDefault();
        var con = confirm("Are you sure to delete this "+terminologies["course_single"].toLowerCase()+".");
        if(con) {
            var course = $(this).closest('.js-course-list');
            var cid = course.attr('data-course');
            var req = {};
            req.action = "delete-course";
            req.courseId = cid;
            $.ajax({
                'type'  : 'post',
                'url'   : ApiEndPoint,
                'data'  : JSON.stringify(req)
            }).done(function (res) {
                res =  $.parseJSON(res);
                if(res.status == 1) {
                    course.remove();
                    toastr.success(terminologies["course_single"]+" Deleted. Please contact admin to restore this "+terminologies["course_single"].toLowerCase()+".", ""+terminologies["course_single"]+" Delete");
                } else {
                    toastr.error(res.message);
                }
            });
        }
    });
    $('#listCourses .edit-course').on('click', function(e) {
        e.preventDefault();
        $(this).closest('.js-course-list').addClass('active');
        var course = $(this).closest('.js-course-list');
        var cid = course.attr('data-course');
        var req = {};
        var res;
        req.courseId = cid;

        if (req.courseId == "" || req.courseId == null) {
            window.location = window.location;
            return;
        }
        req.action = "get-course-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillCourseDetails(res);
            //details=res;
        });
    });
    function fillCourseDetails(data) {
        $('.js-course-action').html("Edit");
        $('#inputCourseStartDate').prop("disabled", true);
        $('.date-picker').datepicker({
            format: 'dd M yyyy',
            startDate: getToday(),
            orientation: "left",
            autoclose: true
        });
        // General Details
        if (data.courseDetails.deleted == 1)
            toastr.error('This '+terminologies["course_single"].toLowerCase()+' has been deleted!');
        var catIds = [], catNames = [];
        var catName=[];
        $('#inputCourseName').val(data.courseDetails.name);

        if (data.courseDetails.subtitle != '') {
            $('#inputCourseSubtitle').val(data.courseDetails.subtitle);
        }

        if (data.courseDetails.targetAudience != '') {
            $('#inputTargetAudience').val(data.courseDetails.targetAudience);
        }
        $('#inputTags').val(data.courseDetails.tags);
        if (data.courseDetails.description != '') {
            $('#inputCourseDescription').val(data.courseDetails.description);
        }
        startDate = formatDate(parseInt(data.courseDetails.liveDate));
        $('#inputCourseStartDate').val(startDate);

        if (data.courseDetails.endDate != '') {
            endDate = formatDate(parseInt(data.courseDetails.endDate));
            $('#inputCourseEndDate').val(endDate);
            $('#optionCourseEndDate').val(1);
            $('.js-course-end-date').removeClass('hide');
        } else {
            $('#optionCourseEndDate').prop('checked', false);
            $('.js-course-end-date').addClass('hide');
        }
        $('#modalAddEditCourse').modal("show");
 
        $.each(data.courseCategories, function (k, cat) {
            //catNames.push(cat.catName);
            catIds.push(cat.categoryId);
        });
        $('#selectCourseCategory').val(catIds);
    }
}
</script>