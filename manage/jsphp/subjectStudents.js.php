<script type="text/javascript">
var TableDatatablesManaged = function () {

    var initTable1 = function () {

        var table = $('#tblSubjectStudents');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // setup buttons extension: http://datatables.net/extensions/buttons/
            buttons: [
                { extend: 'print', className: 'btn dark btn-outline' },
                { extend: 'pdf', className: 'btn green btn-outline' },
                { extend: 'csv', className: 'btn purple btn-outline' }
            ],

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {  // set default column settings
                    'orderable': false,
                    'targets': [0]
                }, 
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery('#sample_1_wrapper');

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).prop("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).prop("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
        }

    };

}();
var keys_available = 0;
var keys_pending = 0;
//function to get the students and chapters for the subject
function fetchSubjectStudents() {
    var req = {};
    var res;
    req.courseId = courseId;
    req.subjectId = subjectId;
    req.action ="get-subject-students";
    $.ajax({
        'type'  :   'post',
        'url'   :   ApiEndPoint,
        'data'  :   JSON.stringify(req)
    }).done(function(res) {
        data = $.parseJSON(res);
        console.log(data);
        if(data.status == 0)
            toastr.error(data.message);
        else {
            //console.log(data);
            var html = "";
            if(data.students.length > 0) {
                for (var i = 0; i < data.students.length; i++)
                {
                    var student_id = data.students[i].id;
                    var student_username = data.students[i].username;
                    var name = data.students[i].name;
                    var password = ((data.students[i].password == "")?'<small class="text-success">REGISTERED</small>':data.students[i].password);
                    // var contactMobile = data.students[i].contactMobile;
                    // var course_key = data.students[i].key_id;
                    var email = data.students[i].email;
                    var date = data.students[i].date;
                    var checkbox = ((!data.students[i].parentid)?'<input type="checkbox" class="js-option-student" />':'');
                    var parent = ((!data.students[i].parentid)?'<td></td>':'<td><strong>'+data.students[i].userid+'</strong> / <strong>'+data.students[i].passwd+'</strong></td>');
                    html+= '<tr class="odd gradeX" data-student="'+student_id+'" data-temp="'+data.students[i].temp+'">'+
                                '<td>'+
                                    '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">'+
                                        '<input type="checkbox" class="checkboxes js-option-student" value="1" />'+
                                        '<span></span>'+
                                    '</label>'+
                                '</td>'+
                                '<td>' + student_id + '</td>'+
                                '<td>' + student_username + '</td>'+
                                '<td>' + password + '</td>'+
                                '<td>' + name + '</td>'+
                                '<td><a href="mailto:' + email + '">' + email + '</a></td>'+
                                '<td>'+data.students[i].userid+'</td>'+
                                '<td>'+data.students[i].passwd+'</td>'+
                                /*'<td>'+
                                    '<a href="javascript:void(0)" class="btn btn-circle btn-sm green js-chat" data-student="'+student_id+'"><i class="fa fa-comment"></i> Chat</a>'+
                                '</td>'+*/
                            '</tr>';
                }
            } else {
                html+= '<tr class="odd gradeX">'+
                            '<td colspan="7">No '+terminologies["subject_single"].toLowerCase()+' '+terminologies["student_plural"].toLowerCase()+' yet!</td>'+
                        '</tr>';
            }
            $('#tblSubjectStudents tbody').append(html);
            TableDatatablesManaged.init();
        }
    });
}
function get_keys_remaining() {
    var req = {};
    req.action = 'keys_avilable';
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        //console.log(data);
        if (data.status == 1) {
            keys_available = data.keys;
            keys_pending = data.invitation;
            $('#jsRemaining').attr('data-value',keys_available);
            $('#jsPending').attr('data-value',keys_pending);
            $('#jsRemaining,#jsPending').counterUp({
                delay: 10,
                time: 2000
            });
            $('.js-pending-keys').html(keys_pending);
            $('.js-available-keys').html(keys_available);
        }
        else {
            toastr.error(res.msg);
        }
    });
}
function inviteStudent(req) {
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        if (data.status == 1) {
            toastr.error(data.message);
            $('#modalInviteStudents').modal('hide');
            get_keys_remaining();
        }
        else {
            toastr.error('<span class="help-block text-danger">' + data.message + ' </span>');
        }    
    });
}
$(document).ready(function() {
    //TableDatatablesManaged.init();
    $(".star-rating").rating({displayOnly: true});
    $('#btnGenStudents').click(function(){
        var inputGenStudents = $('#inputGenStudents').val();
        if (!$.isNumeric(inputGenStudents)) {
            toastr.error("Please enter a number!");
        } else if (inputGenStudents<1) {
            toastr.error("Please enter a number greater than 0!");
        } else if (inputGenStudents>keys_available) {
            toastr.error("Please enter a number less than "+keys_available+"!");
        } else {
            var req = {};
            var res;
            req.action = 'generate-students-for-course';
            req.students = inputGenStudents;
            req.courseId = courseId;
            $.ajax({
                'type'  :   'post',
                'url'   :   ApiEndPoint,
                'data'  :   JSON.stringify(req)
            }).done(function(res) {
                res = $.parseJSON(res);
                if(res.status == 0)
                    toastr.error(res.message);
                else {
                    console.log(res);
                    toastr.success(res.message);
                    window.location = window.location;
                }
            });
        }
    });
    $('#btnInviteStudents').on('click', function () {
        var invalid = '';
        var correct = true
        var emails = $.trim($('#inputEmailIds').val());
        var email = emails.split(',');
        if (emails == '') {
            toastr.error('Please enter email address.');
            // toastr.error('Please enter email id ');
            return false;
        }
        if (checkDuplicates(email) == 'false') {
            toastr.error('Please remove duplicates.');
            //toastr.error('Please remove duplicates ');
            return false;
        }
        if ((email.length > keys_available)) {
            toastr.error('You have ' + keys_available + ' tokens. Please add token in your account to assign '+terminologies["student_plural"].toLowerCase()+'.');
            // toastr.error('You have only ' + keys_available + ' available ');
            return false;
        }
        $.each(email, function (index, value) {
            if (validateEmail($.trim(value)) == false) {
                invalid += value + ',';
                correct = false;
            }
        });
        if (correct == false) {
            toastr.error('Correct these email addresses:\n' + invalid + '');
            //toastr.error('Correct these email addresses:\n' + invalid);
            return false;
        }
        else {
            var req = {};
            req.courseId = courseId;
            req.action = 'send_activation_link_student';
            req.email = email;
            req.enrollment_type = 0;
            //req.ignoreWarning = false;
            inviteStudent(req);
        }
    });
    $('#btnRemoveStudents').click(function(){
        var tempStudents = "";
        var permStudents = "";
        var noOfStudents = 0;
        $('.js-option-student').each(function(key,obj){
            if ($(obj).is(":checked")) {
                if ($(obj).closest('tr').attr("data-temp") == "1") {
                    tempStudents+= $(obj).closest('tr').find('td:nth-child(2)').text()+',';
                } else {
                    permStudents+= $(obj).closest('tr').find('td:nth-child(2)').text()+',';
                }
                noOfStudents++;
            }
        });
        tempStudents = ((tempStudents!="")?(tempStudents.substr(0,tempStudents.length-1)):(""));
        permStudents = ((permStudents!="")?(permStudents.substr(0,permStudents.length-1)):(""));
        
        if (noOfStudents==0) {
            toastr.error("Please select at least 1 "+terminologies["student_single"].toLowerCase());
        } else {
            bootbox.confirm("Are you sure?", function(result) {
                if (result == true) {
                    var req = {};
                    var res;
                    req.course_id = courseId;
                    req.tempStudents  = tempStudents;
                    req.permStudents  = permStudents;
                    req.action    = "delete-students";
                    
                    $.ajax({
                        'type': 'post',
                        'url': ApiEndPoint,
                        'data': JSON.stringify(req)
                    }).done(function (res) {
                        data = $.parseJSON(res);
                        if (data.status == 0) {
                            toastr.error(data.exception);
                        } else {
                            toastr.success("Successfully removed!");
                            for (var i = 0; i < data.students.length; i++) {
                                $('[data-student="'+data.students[i]+'"]').remove();
                                keys_available++;
                            };
                            $('#remaining_keys_total').html(keys_available);
                        }
                        //console.log(data);
                    });
                }
            });
        };
    });
    $('#btnParentCredentials').click(function(){
        var students = "";
        $('.js-option-student').each(function(key,obj){
            if ($(obj).is(":checked")) {
                if ($(obj).closest('tr').attr("data-temp") == "0") {
                    students+= $(obj).closest('tr').find('td:nth-child(2)').text()+',';
                }
            }
        });
        students = students.substr(0,students.length-1);
        
        if (!students) {
            toastr.error("Please select at least 1 registered "+terminologies["student_single"].toLowerCase());
        } else {
            bootbox.confirm("Are you sure?", function(result) {
                if (result == true) {
                    var req = {};
                    var res;
                    req.course_id = courseId;
                    req.students  = students;
                    req.action    = "generate-parents";
                    $.ajax({
                        'type': 'post',
                        'url': ApiEndPoint,
                        'data': JSON.stringify(req)
                    }).done(function (res) {
                        data = $.parseJSON(res);
                        for (var i = 0; i < data.students.length; i++) {
                            $('[data-student="'+data.students[i].student_id+'"]').find('td:first-child').html('');
                            //$('[data-student="'+data.students[i].student_id+'"]').find('td:nth-child(6)').html(data.students[i].userid);
                            $('[data-student="'+data.students[i].student_id+'"]').find('td:nth-child(7)').html('<strong>'+data.students[i].userid+'</strong>');
                            $('[data-student="'+data.students[i].student_id+'"]').find('td:nth-child(8)').html('<strong>'+data.students[i].passwd+'</strong>');
                        };
                        console.log(data);
                    });
                }
            });
        };
    });
    /*$('#listSubjectStudents').on("click", ".js-chat", function(){
        var req = {};
        var res;
        req.action = 'get-professor-chat-link';
        req.courseId = courseId;
        req.subjectId = subjectId;
        req.studentId = $(this).attr("data-student");
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            //console.log(res);
            if (res.status == 0)
                toastr.error(res.message);
            else {
                //console.log(res.roomId);
                window.open(sitePath+'messenger/'+res.roomId);
            }
        });
    });*/
});
</script>