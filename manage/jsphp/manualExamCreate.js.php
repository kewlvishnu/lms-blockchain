<script type="text/javascript">
var chapterId;
function fetchChapters() {
    var req = {};
    req.action = "get-chapters-for-exam";
    req.subjectId = subjectId;
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        fillChaptersSelect(res);
    });
}
function fillChaptersSelect(data) {
    if(data.status == 0) {
        toastr.error(data.message);
        return;
    }
    var opts = '';
    for(i=0; i<data.chapters.length; i++) {
        opts += '<option value="'+data.chapters[i]['id']+'">'+data.chapters[i]['name']+'</option>';
    }
    opts += '<option value="0">Independent</option>';
    $('#chapterSelect').append(opts);
    if (typeof chapterId !== 'undefined') {
        $('#chapterSelect').val(xx);
    }
}
$(document).ready(function(){
	$('input[name=optionAnalytics]').click(function(){
		var objId = $(this).attr('id');
		$(".oy-auto").addClass("hide");
		if (objId == "optionQuickAnalytics") {
			$(".js-quick").addClass("hide");
			//$("#jsFormQuick").removeClass("hide");
		} else {
			$(".js-quick").removeClass("hide");
			//$("#jsFormDeep").removeClass("hide");
		}
	});
	$('#btnEnterManually').click(function(){
		var inputQuestions = $('#inputQuestions').val();
		var optionAnalytics = $('input[name=optionAnalytics]:checked').val();
		chapterId=$('#chapterSelect').val();
		if ((optionAnalytics == "deep") && (!inputQuestions || inputQuestions<=0)) {
			$('#inputQuestions').after('<span class="help-block alert alert-danger">Please enter no of questions.</span>');
			//toastr.error("Please enter no of questions.");
		} else if (!chapterId) {
			toastr.error("Please select a section!");
		} else {
			var req = {};
			var res;
			req.action		= 'generate-manual-exam-form';
			req.subjectId	= subjectId;
			req.courseId	= courseId;
			req.type		= ((optionAnalytics=="quick")?1:2);
			req.questions	= inputQuestions;
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndPoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					//console.log(res);
					fillManualExamForm(res.data,optionAnalytics,inputQuestions);
				}
			});
		}
		return false;
	});
	function fillManualExamForm (data,analytics,questions) {
		//console.log(data);
		if (data.length>0) {
			$('#jsForm').attr('data-analytics', analytics);
			$('#jsForm').attr('data-questions', questions);
			$('#jsForm').html('<div class="form-group"><label for="inputExamName">Exam Name:</label><input type="text" id="inputExamName" class="form-control" placeholder="Exam Name" /></div>');
			if (analytics == "deep") {
				var htmlTotal = '';
				var filler1 = '';
				var filler2 = '';
				for (var i = 0; i < questions; i++) {
					//htmlTotal+='<tr><th class="active">Q'+(i+1)+'</th><td><input type="text" class="form-control input-total" /></td></tr>';
					filler1+='<th>Q'+(i+1)+'</th>';
					filler2+= '<td><input type="text" class="form-control input-marks" data-q="'+i+'" /></td>';
					htmlTotal+= '<td><input type="text" class="form-control input-total" data-q="'+i+'" /></td>';
				};
				$('#jsForm').append('<table class="table table-bordered table-total">'+
									'<tr class="active">'+
										'<th colspan="'+questions+'" class="text-center">Total Marks of questions</th>'+
									'</tr>'+
									'<tr class="active">'+filler1+'</tr>'+
									'<tr>'+htmlTotal+'</tr>'+
									'</table>');
				$('#jsForm').append('<table class="table table-bordered table-marks">'+
									'<tr class="active">'+
										'<th rowspan="2">'+terminologies["student_single"]+' ID</th>'+
										'<th rowspan="2">'+terminologies["student_single"]+' Name</th>'+
										'<th colspan="'+questions+'" class="text-center">Marks</th>'+
									'</tr>'+
									'<tr class="active"></tr>'+
									'</table>');
				$('#jsForm .table-marks tr:last-child').append(filler1);
				for (var i = 0; i < data.length; i++) {
					$('#jsForm .table-marks').append('<tr data-sid="'+data[i][0]+'"><td>'+data[i][0]+'</td><td>'+data[i][1]+'</td>'+filler2+'</tr>');
				};
			} else {
				$('#jsForm').append('<div class="form-group"><label for="inputTotalMarks">Total Marks for exam:</label><input type="text" id="inputTotalMarks" class="form-control" placeholder="Exam Total Marks" /></div>');
				$('#jsForm').append('<table class="table table-bordered table-marks">'+
									'<tr class="active">'+
										'<th>'+terminologies["student_single"]+' ID</th>'+
										'<th>'+terminologies["student_single"]+' Name</th>'+
										'<th>Marks</th>'+
									'</tr>'+
									'</table>');
				for (var i = 0; i < data.length; i++) {
					$('#jsForm .table-marks').append('<tr data-sid="'+data[i][0]+'"><td>'+data[i][0]+'</td><td>'+data[i][1]+'</td><td><input type="text" class="form-control input-marks" /></td></tr>');
				};
			}
			$('#jsForm').append('<div><button class="btn btn-primary" id="btnSubmit">SUBMIT</button></div>');
			$(".oy-auto").removeClass("hide");
		};
	}
	$('#jsForm').on("click", "#btnSubmit", function(){
		$('.help-block').remove();
		$('#jsForm td').removeClass("danger");
		var flagTotal = false;
		var flagMarks = false;
		var flagValidMarks = false;
		var value = "";
		var qkey  = 0;
		var i = 0;
		var optionAnalytics = $('#jsForm').attr('data-analytics');
		var inputQuestions = $('#jsForm').attr('data-questions');
		chapterId=$('#chapterSelect').val();
		if (optionAnalytics == "deep") {
			var formTotal = [];
			$('.input-total').each(function(key,obj){
				value = $(obj).val();
				if(value == '') {
					flagTotal = true;
				}
				formTotal[i] = $(obj).val();
				i++;
			});
			$('.input-marks').each(function(key,obj){
				value = $(obj).val();
				if(value == '') {
					flagMarks = true;
				}
				qkey = $(obj).attr('data-q');
				if (parseFloat(value)>parseFloat(formTotal[qkey])) {
					$(this).closest("td").addClass("danger");
					flagValidMarks = true;
				}
			});
		} else {
			var formTotal = $("#inputTotalMarks").val();
			if (!formTotal) {
				flagTotal = true;
			};
			$('.input-marks').each(function(key,obj){
				value = $(obj).val();
				if(value == '') {
					flagMarks = true;
				}
				if (parseFloat(value)>parseFloat(formTotal)) {
					$(this).closest("td").addClass("danger");
					flagValidMarks = true;
				}
			});
		}
		var inputExamName = $("#inputExamName").val();
		//console.log(flagMarks);
		if (!inputExamName) {
			$('#inputExamName').focus();
			$('#inputExamName').closest('.form-group').after("<p class='help-block alert alert-danger'>Please enter exam name</p>");
		} else if (flagTotal) {
			if (optionAnalytics == "deep") {
				$('#inputExamName').focus();
				$('#jsForm .table-total').before("<p class='help-block alert alert-danger'>Please enter total marks in all input boxes</p>");
			} else {
				$('#inputExamName').focus();
				$('#inputTotalMarks').closest('.form-group').after("<p class='help-block alert alert-danger'>Please enter valid marks</p>");
			}
		} else if (flagMarks) {
			$('#inputExamName').focus();
			$('#jsForm .table-marks').before("<p class='help-block alert alert-danger'>Please enter marks in all input boxes</p>");
		} else if (flagValidMarks) {
			$('#inputExamName').focus();
			$('#jsForm .table-marks').before("<p class='help-block alert alert-danger'>Please enter valid marks in all input boxes</p>");
		} else if (!chapterId) {
			toastr.error("Please select a section!");
		} else {
			var formMarks = [];
			var formStudents = [];
			var i = 0;
			var j = 0;
			/*$('#jsForm .input-total').each(function(k,obj){
				formTotal[i] = $(obj).val();
				i++;
			});
			i = 0;*/
			$('#jsForm .table-marks tr').each(function(k,obj){
				if ((optionAnalytics == "quick" && k>0) || (optionAnalytics == "deep" && k>1)) {
					//i = $(obj).attr('data-sid');
					j = 0;
					formMarks[i] = [];
					formStudents[i] = $(obj).attr('data-sid');
					$(obj).find('.input-marks').each(function(k1,obj1){
						formMarks[i][j] = $(obj1).val();
						j++;
					});
					i++;
				};
			});
			//console.log(formMarks);
			//console.log(formStudents);
			//console.log(formTotal);
			var req = {};
			var res;
			req.action		= 'save-manual-exam-form';
			req.subjectId	= subjectId;
			req.courseId	= courseId;
			req.type		= optionAnalytics;
			req.questions	= inputQuestions;
			req.name		= inputExamName;
			req.marks		= formMarks;
			req.total		= formTotal;
			req.students	= formStudents;
			req.chapterId=$('#chapterSelect').val();
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndPoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					window.location = sitepathManageManualExams+res.examId;
					//$('#jsForm').html("<p class='help-block text-success'>Successfully added!</p>");
				}
			});
		}
		return false;
	});
	$('#btnGenerateCSV').click(function(){
		var inputQuestions = $('#inputQuestions').val();
		var optionAnalytics = $('input[name=optionAnalytics]:checked').val();
		if ((optionAnalytics == "deep") && (!inputQuestions || inputQuestions<=0)) {
			toastr.error("Please enter no of questions.");
		} else {
			//optionAnalytics = ((optionAnalytics=="deep")?2:1);
			/*if (optionAnalytics == "deep") {
				window.location = "generateManualExamCSV.php?type=2&questions="+inputQuestions+"&courseId=" + courseId + "&subjectId=" + subjectId;
			} else {
				window.location = "generateManualExamCSV.php?type=1&courseId=" + courseId + "&subjectId=" + subjectId;
			}*/
			if (optionAnalytics == "deep") {
				window.location = sitepathManageSubjects+subjectId+"/manual-exams/generate/2/"+inputQuestions;
			} else {
				window.location = sitepathManageSubjects+subjectId+"/manual-exams/generate/1/";
			}
		}
		return false;
	});
	$("#jsForm").on("keypress",".input-marks,.input-total", function(e){
		$('.help-block').remove();
		$('#jsForm td').removeClass("danger");
		if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
			toastr.error('Enter Only Numbers');
			return false;
		}
	});
	$("#inputQuestions,#inputStudents").keypress(function (e) {
		$('.help-block').remove();
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			$(this).after('<span class="help-block alert alert-danger">Enter Only Numbers</span>');
			return false;
		}
	});
});
</script>