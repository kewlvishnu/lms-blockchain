<script type="text/javascript">
//to add BCT dynamically
function fetchSubjectiveExamAttempts() {
	var req = {};
	var res;
	req.action = 'get-check-subjective-exam-students';
	req.examId = examId;
	req.subjectId = subjectId;
	req.courseId = courseId;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else {
			//console.log(res);
			fullSubjectiveExamAttempts(res.students);
		}
	});
}
function fullSubjectiveExamAttempts(data) {
	if (data.length>0) {
		$('#subjectiveExams').html('');
		for (var i = 0; i < data.length; i++) {
			$('#subjectiveExams').append(
				'<div class="panel panel-primary">'+
					'<div class="panel-heading" role="tab" id="heading'+data[i].userId+'">'+
						'<h4 class="panel-title">'+
							'<a class="'+((i==0)?'':'collapsed')+'" role="button" data-toggle="collapse" data-parent="#" href="#collapse'+data[i].userId+'" +aria-expanded="'+((i==0)?'true':'false')+'" aria-controls="collapse'+data[i].userId+'">'+
								data[i].name+
							'</a>'+
						'</h4>'+
					'</div>'+
					'<div id="collapse'+data[i].userId+'" class="panel-collapse collapse '+((i==0)?'in':'')+'" role="tabpanel" aria-labelledby="heading'+data[i].userId+'">'+
						'<table class="table">'+
							'<tbody>'+
								'<tr>'+
									'<th>Attempt</th>'+
									'<th>Start Date</th>'+
									'<th>End Date</th>'+
									'<th>Checked</th>'+
								'</tr>'+
							'</tbody>'+
						'</table>'+
					'</div>'+
				'</div>');
				for (var j = 0; j < data[i]["attempts"].length; j++) {
					//$('#subjectiveExams .panel-default:last-child .list-group').append('<li class="list-group-item"><a href="checkSubjectiveExam.php?attemptId='+data[i]["attempts"][j].attemptId+'">Attempt '+(j+1)+' ['+formatTime(data[i]["attempts"][j].startDate)+' - '+formatTime(data[i]["attempts"][j].endDate)+']</a></li>');
					$('#subjectiveExams .panel:last-child .table tbody').append(
						'<tr>'+
							'<td>'+
								'<a href="'+sitepathManageSubjectiveExams+examId+'/check/'+data[i]["attempts"][j].attemptId+'">Attempt '+(j+1)+'</a>'+
							'</td>'+
							'<td>'+formatTime(data[i]["attempts"][j].startDate)+'</td>'+
							'<td>'+((data[i]["attempts"][j].endDate==0)?'-':formatTime(data[i]["attempts"][j].endDate))+'</td>'+
							'<td><span class="badge">'+((data[i]["attempts"][j].checked==1)?'Yes':'No')+'</span></td>'+
						'</tr>');
				};
		};
	} else {
		$('#subjectiveExams').html('<div class="note note-info">No attempts found</div>');
	}
}
</script>