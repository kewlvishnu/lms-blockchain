<script type="text/javascript">
var sortOrder = [];
var tempStart = new Date();
var tempEnd = new Date('2099/12/31');
var examStart = new Date();
var examEnd = new Date('2099/12/31');
function fetchChapters() {
    var req = {};
    req.action = "get-chapters-for-exam";
	req.examId = examId;
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        fillChaptersSelect(res);
    });
}
function fillChaptersSelect(data) {
	if(data.status == 0) {
		toastr.error(data.message);
		return;
	}
	var opts = '';
	for(i=0; i<data.chapters.length; i++) {
		opts += '<option value="'+data.chapters[i]['id']+'">'+data.chapters[i]['name']+'</option>';
	}
	opts += '<option value="-1">Independent</option>';
	$('#chapterSelect').append(opts);
	fetchExamDetails();
}
function fetchExamDetails() {
	var req = {};
	req.action = 'get-exam';
	req.examId = examId;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		//console.log(res);
		fillExamDetails(res);
	});
}
function fillExamDetails(data) {
	//$('#examName').text(data.exam.name);
	var status = '';
	if(data.exam.status == 0)
		status = 'Draft';
	else if(data.exam.status == 1)
		status = 'Not Live';
	else if(data.exam.status == 2)
		status = 'Live';
	$('#examStatus').text(status);
	$('#noOfAttempts').val(data.exam.attempts);
	if(data.exam.showResultTill== 1)
	{
		$('#course-end').prop('checked', true);
		
	}
	else{
		$('#custom-time').prop('checked', true);
		if(data.exam.showResultTillDate!='' || data.exam.showResultTillDate!=null) {
			var tsd = formatTime(parseInt(data.exam.showResultTillDate)/1000,0);
			$('#settime input').val(tsd);
			$('.js-set-time').removeClass('hide');
		}
	}
	if(data.exam.showResult=='immediately')
	{
		$('#immediately').prop('checked', true);
	}
	else{
		$('#after-examend').prop('checked', true);	
		
	}
	//$('#noOfAttempts').val(data.exam.attempts);
	if(data.exam.chapterId == 0)
		$('#chapterSelect').val('-1');
	else
		$('#chapterSelect').val(data.exam.chapterId);
	$('#titleName').val(data.exam.name);
	if(data.exam.type == 'Assignment') {
		$('#assignmentCat').prop('checked', true);
		$('#assignmentCat').trigger('change');
	} else if(data.exam.type == 'Exam') {
		$('#examCat').prop('checked', true);
		$('#examCat').trigger('change');
		//$('.js-section-timings').removeClass('hide');
	}
	var sd = formatTime(parseInt(data.exam.startDate)/1000,0);
	var ed = formatTime(parseInt(data.exam.endDate)/1000,0);
	$('#startDate input').val(sd);
	$('#endDate input').val(ed);
	if(data.exam.endBeforeTime == 1)
		$('#endYes').prop('checked', true);
	else
		$('#endNo').prop('checked', true);
	if(data.exam.powerOption == 0)
		$('#noTimeLost').prop('checked', true);
	else
		$('#timeLost').prop('checked', true);
	$('#sections').find('*').remove();
	$('#sectionTimes .row:eq(0)').find('*').remove();
	if(data.exam.totalTime == 0 || data.exam.totalTime == 6000) {//changed to checked total time
		$('#switchNo').prop('checked', true);
		$('#sectionTimes').removeClass('hide');
		$('#totalTime').addClass('hide');
		$('#gapTime').val(data.exam.gapTime);
		for(i=0; i<data.sections.length; i++) {
			var sectionStatus = '';
			if(data.sections[i].status == 0)
				sectionStatus = 'Draft';
			else if(data.sections[i].status == 1)
				sectionStatus = 'Done';
			/*var html='<div class="form-group">'
					+ '<div class="row">'
						+ '<div class="col-lg-9">'
							+ '<input type="text" class="form-control" value="'+data.sections[i].name+'" id="'+data.sections[i].id+'">'
						+ '</div>'
						+ '<div class="col-lg-3">'
							+ '<input type="button" class="btn btn-xs btn-primary" value="' + sectionStatus + '">'
							+ '<button class="btn btn-danger btn-xs delete-button" type="button" style="margin-left:5px;"><i class="fa fa-trash-o "></i></button>'
						+ '</div>'
					+ '</div>'
				+ '</div>';*/
			var html='<div class="form-group js-section">'+
						'<div class="input-group">'+
							'<input type="text" class="form-control" value="'+data.sections[i].name+'" id="'+data.sections[i].id+'">'+
							'<span class="input-group-btn">'+
								'<button class="btn w-60 blue-steel" type="button" disabled>' + sectionStatus + '</button>'+
								/*'<button class="btn green" type="button"><i class="fa fa-edit"></i></button>'+*/
								'<button class="btn red delete-button" type="button"><i class="fa fa-trash"></i></button>'+
							'</span>'+
						'</div>'+
					'</div>';
			$('#sections').append(html);
			$('.delete-button').on('click', function() {
				//$(this).closest('.form-group').remove();
				$(this).closest('.js-section').remove();
				$('#sectionTimes .row:eq(0)').find('*').remove();
				if($('#switchNo').prop('checked'))
					$('#switchNo').trigger('click');
				if($('#sections').find('input[type="text"]').length <= 1) {
					$('#switchYes').prop('checked', true);
					$('#orderRandom').prop('checked', true);
					$('#switchSections').addClass('hide');
					$('.js-sections-manage').addClass('hide');
					$('#sectionTimes').addClass('hide');
					$('#totalTime').removeClass('hide');
				}
			});
			/*var htmlTime = '<div class="col-lg-6">'
				+ '<label for="coursename">Total Time for ' + data.sections[i].name + '</label><br>'
				+ '<div class="col-md-4">'
					+ '<div class="input-group input-small">'
						+ '<input type="text" maxlength="3" class="spinner-input form-control time hour" value="0h" disabled=true>'
						+ '<div class="spinner-buttons input-group-btn btn-group-vertical">'
							+ '<button type="button" class="btn spinner-up btn-xs btn-default generated">'
								+ '<i class="fa fa-angle-up"></i>'
							+ '</button>'
							+ '<button type="button" class="btn spinner-down btn-xs btn-default generated">'
								+ '<i class="fa fa-angle-down"></i>'
							+ '</button>'
						+ '</div>'
					+ '</div>'
				+ '</div> &nbsp;&nbsp;&nbsp;&nbsp;'
				+ '<div class="col-md-4">'
					+ '<div class="input-group input-small">'
						+ '<input type="text" class="spinner-input form-control time minute" maxlength="3" value="0m" disabled=true>'
						+ '<div class="spinner-buttons input-group-btn btn-group-vertical">'
							+ '<button class="btn spinner-up btn-xs btn-default generated" type="button">'
								+ '<i class="fa fa-angle-up"></i>'
							+ '</button>'
							+ '<button class="btn spinner-down btn-xs btn-default generated" type="button">'
								+ '<i class="fa fa-angle-down"></i>'
							+ '</button>'
						+ '</div>'
					+ '</div>'
				+ '</div>'
			+ '</div>';*/
			var htmlTime = '<div class="col-md-6">'+
                        '<div class="form-group">'+
                            '<label class="control-label">Total Time for '+data.sections[i].name+'</label>'+
                            '<div class="clearfix">'+
                                '<div class="dib">'+
                                    '<div class="input-group input-small">'+
                                        '<input type="number" class="form-control time hour" value="0" placeholder="Hours" aria-describedby="sizing-addon1">'+
                                        '<span class="input-group-addon" id="sizing-addon1">Hours</span>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="dib">'+
                                    '<div class="input-group input-small">'+
                                        '<input type="number" class="form-control time minute" value="0" placeholder="Minutes" aria-describedby="sizing-addon1">'+
                                        '<span class="input-group-addon" id="sizing-addon1">Minutes</span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'
			$('#sectionTimes .row:eq(0)').append(htmlTime);
			hour = Math.floor(data.sections[i].time / 60);
			minute = data.sections[i].time - (hour * 60);
			$('#sectionTimes input[type="number"].hour:eq('+i+')').val(hour);
			$('#sectionTimes input[type="number"].minute:eq('+i+')').val(minute);
		}
	}
	else {
		$('#switchYes').prop('checked', true);
		$('#sectionTimes').addClass('hide');
		$('#totalTime').removeClass('hide');
		for(i=0; i<data.sections.length; i++) {
			var sectionStatus = '';
			if(data.sections[i].status == 0)
				sectionStatus = 'Draft';
			else if(data.sections[i].status == 1)
				sectionStatus = 'Done';
			/*var html='<div class="form-group">'
					+ '<div class="row">'
						+ '<div class="col-lg-9">'
							+ '<input type="text" class="form-control" value="'+data.sections[i].name+'" id="'+data.sections[i].id+'">'
						+ '</div>'
						+ '<div class="col-lg-3">'
							+ '<input type="button" class="btn btn-xs btn-primary" value="' + sectionStatus + '">'
							+ '<button class="btn btn-danger btn-xs delete-button" type="button" style="margin-left:5px;"><i class="fa fa-trash-o "></i></button>'
						+ '</div>'
					+ '</div>'
				+ '</div>';*/
			var html='<div class="form-group js-section">'+
						'<div class="input-group">'+
							'<input type="text" class="form-control" value="'+data.sections[i].name+'" id="'+data.sections[i].id+'">'+
							'<span class="input-group-btn">'+
								'<button class="btn w-60 blue-steel" type="button" disabled>' + sectionStatus + '</button>'+
								/*'<button class="btn green" type="button"><i class="fa fa-edit"></i></button>'+*/
								'<button class="btn red delete-button" type="button"><i class="fa fa-trash"></i></button>'+
							'</span>'+
						'</div>'+
					'</div>';
			$('#sections').append(html);
			$('.delete-button').on('click', function() {
				//$(this).closest('.form-group').remove();
				$(this).closest('.js-section').remove();
				$('#sectionTimes .row:eq(0)').find('*').remove();
				if($('#switchNo').prop('checked'))
					$('#switchNo').trigger('click');
				if($('#sections').find('input[type="text"]').length <= 1) {
					$('#switchYes').prop('checked', true);
					$('#orderRandom').prop('checked', true);
					$('#switchSections').addClass('hide');
					$('.js-sections-manage').addClass('hide');
					$('#sectionTimes').addClass('hide');
					$('#totalTime').removeClass('hide');
				}
			});
		}
		hour = Math.floor(data.exam.totalTime / 60);
		minute = data.exam.totalTime - (hour * 60);
		$('#totalTimeHour').val(hour);
		$('#totalTimeMinute').val(minute);
	}
	if($('#sections').find('input[type="text"]').length > 1) {
		//$('#switchYes').prop('checked', false);
		//$('#orderRandom').prop('checked', false);
		$('#switchSections').removeClass('hide');
		$('.js-sections-manage').removeClass('hide');
	}
	if(data.exam.sectionOrder == 0) {
		$('#orderStart').prop('checked', true);
		$('#sortOrder').find('li').remove();
		var htm = '';
		for(var a=0; a<$('#sections').find('input[type="text"]').length; a++) {
			var value = $('#sections input[type="text"]:eq('+a+')').val();
			//htm += '<li data-original="'+a+'">' + value + '</li>';
			htm += '<li data-original="'+i+'" class="dd-item sort-li">'+
		                '<div class="dd-handle"> ' + value + ' </div>'+
		            '</li>';
			sortOrder[a] = a+'';
		}
		$('#sortOrder').append(htm);
		$('#sortOrder').removeClass('hide');
	}
	else
		$('#orderRandom').prop('checked', true);
	$('#sections input[type="text"]').on('blur', function() {
		if($('#orderStart').prop('checked')) {
			$('#orderStart').click();
		} else if($('#orderRandom').prop('checked')) {
			$('#orderRandom').click();
		}
	});
}
function fetchCourseDates() {
	var req = {};
	var res;
	req.action = 'get-course-date';
	req.examId = examId;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else {
			$('#courseStartDate').val(res.dates.liveDate);
			$('#courseEndDate').val(res.dates.endDate);
			
			//adding handlers for datetimepicker of start and end date
			var now = new Date();
			var start = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
			var courseStart = new Date(parseInt(res.dates.liveDate));
			examStart = new Date(courseStart.getTime());
			/*if(start.valueOf() < courseStart.valueOf())
				examStart = new Date(courseStart.getTime());
			else
				examStart = new Date(start.getTime());*/
			var courseEnd = new Date(parseInt(res.dates.endDate));
			var end = new Date('2099/12/31');
			if(end.valueOf() > courseEnd.valueOf())
				examEnd = new Date(courseEnd.getTime());
			else
				examEnd = new Date(end.getTime());
			if(res.dates.endDate == '') {
				// // // //$('#endDate').closest('.form-group').addClass('hide');
				$('#result-show').addClass('hide');
				//	$('.js-set-time').addClass('hide');
				examEnd = new Date(end.getTime());
			}
			$('#startDate').datetimepicker({
				format: 'dd MM yyyy hh:ii',
				minDate: examStart,
				maxDate: examEnd,
				step: 30,
				autoclose: true,
				pickerPosition: "bottom-left",
				onSelectTime: function(date) {
					if (date.valueOf() > tempEnd.valueOf()) {
						toastr.error('The start date can not be greater than the end date.');
					} else {
						tempStart = date;
					}
				}
			});
			var today=new Date($.now());
			$('#settime').datetimepicker({
				format: 'dd MM yyyy hh:ii',
				minDate: today,
				step: 30,
				autoclose: true,
				pickerPosition: "bottom-left"
			});
			$('#endDate').datetimepicker({
				format: 'dd MM yyyy hh:ii',
				minDate: examStart,
				maxDate: examEnd,
				step: 30,
				autoclose: true,
				pickerPosition: "bottom-left",
				onSelectTime: function(date) {
					if (date.valueOf() < tempStart.valueOf()) {
						toastr.error('The end date can not be less than the start date.');
					} else {
						tempEnd = date;
					}
				}
			});
		}
	});
}
function nameAvailable() {
	if($('#titleName').val().length != '' && ($('#examCat').prop('checked') || $('#assignmentCat').prop('checked'))) {
		var req = {};
		var res;
		req.action = 'check-name-for-exam';
		req.name = $('#titleName').val();
		req.examId = examId;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndPoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				toastr.error(res.message);
			else {
				if(!res.available)
					toastr.error('Please select a different name as you have already used this name.');
			}
		});
	}
}
$(document).ready(function(){
	$('#titleName').on('blur', function() {
		if(min($(this), 3)) {
			if(!max($(this), 50))
				toastr.error("Please give a shorter assignment/exam name. Maximum allowed limit is 50 characters.");
		}
		else
			toastr.error("Please give a longer assignment/exam name. Minimum allowed limit is 3 characters.");
		nameAvailable();
	});
	$('#sections input[type="text"]').on('blur', function() {
		if($('#orderStart').prop('checked')) {
			$('#orderStart').click();
		} else if($('#orderRandom').prop('checked')) {
			$('#orderRandom').click();
		}
	});
	$('#orderRandom').on('click', function() {
		$('#sortOrder').addClass('hide');
	});
	$('#orderStart').on('click', function() {
		$('#sortOrder').find('li').remove();
		var html = '';
		for(var i=0; i<$('#sections').find('input[type="text"]').length; i++) {
			var value = $('#sections input[type="text"]:eq('+i+')').val();
			//html += '<li data-original="'+i+'" class="sort-li">' + value + '</li>';
			html += '<li data-original="'+i+'" class="dd-item sort-li">'+
		                '<div class="dd-handle"> ' + value + ' </div>'+
		            '</li>';
			sortOrder[i] = i+'';
		}
		$('#sortOrder').append(html);
		UINestable.init();
		$('#sortOrder').removeClass('hide');
	});
	$('#formCreateExam').on('submit', function(){
		$('#titleName').blur();
		if($('#noOfAttempts').val() != '') {
			if($('#assignmentCat').prop('checked') || $('#examCat').prop('checked')) {
				if($('#sections').find('input[type="text"]').length != 0) {
					if($('#startDate input').val() != '') {
						if(($('#endDate input').val() == '' && $('#courseEndDate').val() == '') || $('#endDate input').val() != '') {
							if($('#course-end').prop('checked') || ($('#custom-time').prop('checked')  && $('#settime input').val() != '')) {
								if($('#immediately').prop('checked') || $('#after-examend').prop('checked')  || !$('#examCat').prop('checked')) {
									if(($('#switchYes').prop('checked') || $('#switchNo').prop('checked')) || !$('#examCat').prop('checked')) {
										if(($('#endYes').prop('checked') || $('#endNo').prop('checked')) || !$('#examCat').prop('checked')) {
											if(($('#noTimeLost').prop('checked') || $('#timeLost').prop('checked')) || !$('#examCat').prop('checked')) {
												if($('#chapterSelect').val() == 0) {
													return false;
												}
												if($('.has-error').length == 0 ) {
													var req = {};
													req.action = 'edit-exam';
													req.chapterId = $('#chapterSelect').val();
													req.examId = examId;
													req.name = $('#titleName').val();
													if($('#assignmentCat').prop('checked'))
														req.type = 'Assignment';
													else if($('#examCat').prop('checked'))
														req.type = 'Exam';
													req.status = 0;
													var showResult='immediately';
													if($('#after-examend').prop('checked'))
														showResult='after';
													req.showResult=showResult;
													var showResultTill=1;
													var showResultTillDate="";
													if($('#custom-time').prop('checked'))
													{
														showResultTill=2;
														var timestamp = new Date($('#settime input').val());
														showResultTillDate = timestamp.getTime();
														
													}
													req.showResultTill=showResultTill;
													req.showResultTillDate=showResultTillDate;
													var timestamp = new Date($('#startDate input').val());
													req.startDate = timestamp.getTime();
													req.endDate = '';
													if($('#endDate input').val() != '') {
														timestamp = new Date($('#endDate input').val());
														req.endDate = timestamp.getTime();
													}
													req.attempts = $('#noOfAttempts').val();
													req.tt_status = 0;
													if($('#switchYes').prop('checked'))
														req.tt_status = 0;
													else if($('#switchNo').prop('checked'))
														req.tt_status = 1;
													req.totalTime = 6000;
													req.gapTime = 0;
													if(req.tt_status == 0 || !$('#examCat').prop('checked')) {
														req.sectionOrder = 2;
														var tth = $('#totalTimeHour').val();
														//tth = tth.substring(0, tth.length-1);
														tth = parseInt(tth);
														var ttm = $('#totalTimeMinute').val();
														//ttm = ttm.substring(0, ttm.length-1);
														ttm = parseInt(ttm);
														req.totalTime = (tth * 60) + ttm;
														if(req.totalTime <= 0 && $('#examCat').prop('checked')) {
															toastr.error("Please specify a correct time limit.");
															return false;
														}
													}
													if(req.tt_status == 1 || !$('#examCat').prop('checked')) {
														req.totalTime=0;
														var gap = $('#gapTime').val();
														//req.gapTime = gap.substring(0, gap.length-1);
														req.gapTime = gap;
														if(!($('#orderStart').prop('checked') || $('#orderRandom').prop('checked'))) {
															if($('#examCat').prop('checked')) {
																toastr.error("Please select ordering of the sections.");
																return false;
															}
														}
													}
													req.endBeforeTime = 0;
													req.powerOption = 1;
													if($('#endYes').prop('checked'))
														req.endBeforeTime = 1;
													else if($('#endNo').prop('checked'))
														req.endBeforeTime = 0;
													if($('#noTimeLost').prop('checked'))
														req.powerOption = 0;
													else if($('#timeLost').prop('checked'))
														req.powerOption = 1;
													req.sections = [];
													var sections = $('#sections').find('input[type="text"]');
													var sectionTimesHour = $('#sectionTimes').find('input.hour');
													var sectionTimesMinute = $('#sectionTimes').find('input.minute');
													for(i=0; i<sections.length; i++) {
														var obj = {};
														obj.section = sections[i]['value'];
														obj.id = sections[i]['id'];
														obj.time = 0;
														obj.weight = sortOrder.indexOf(i+'');
														if(req.tt_status == 1 && $('#examCat').prop('checked')) {
															var sth = sectionTimesHour[i]['value'];
															//sth = sth.substring(0, sth.length-1);
															sth = parseInt(sth);
															var stm = sectionTimesMinute[i]['value'];
															//stm = stm.substring(0, stm.length-1);
															stm = parseInt(stm);
															var st = (sth * 60) +stm;
															if(st <= 0 && $('#examCat').prop('checked')) {
																toastr.error("Please select correct timings for every section.");
																return false;
															}
															obj.time = st;
														}
														req.sections.push(obj);
													}
													req.sectionOrder = 1;
													if($('#orderStart').prop('checked'))
														req.sectionOrder = 0;
													else if($('#orderRandom').prop('checked'))
														req.sectionOrder = 1;
														//console.log(req);
													$.ajax({
														'type'  : 'post',
														'url'   : ApiEndPoint,
														'data' 	: JSON.stringify(req)
													}).done(function (res) {
													res =  $.parseJSON(res);
														if(res.status == 0)
															toastr.error(res.message);
														else {
															window.location = sitepathManageExams+res.examId;
														}
													});
												}
												else
													toastr.error("Please review your forms. It contains errors.");
											}
											else
												toastr.error("Please select an option to continue.");
										}
										else
											toastr.error("Please select one option.");
									}
									else
										toastr.error("Please select a option to switch between sections or not.");
								}
								else
									toastr.error("Please select a option when "+terminologies["student_single"].toLowerCase()+" can see the result.");
							}
							else
								toastr.error("Please select a option/End date till when "+terminologies["student_single"].toLowerCase()+" can see the result.");
						}
						else
							toastr.error("Please specify a correct end date.");
					}
					else
						toastr.error("Please specify a correct start date.");
				}
				else
					toastr.error("Please add atleast one section.");
			}
			else
				toastr.error("Please select a test type.");
		}
		else
			toastr.error("Please specify number of attempts allowed.");
		return false;
	});
	$('#addSection').on('click', function() {
		$('#tempSection').parent().removeClass('has-error');
		$('#tempSection').next().remove();
		if($('#tempSection input').val() != '') {
			/*var html='<div class="form-group">'
						+ '<div class="row">'
							+ '<div class="col-lg-9">'
								+ '<input type="text" class="form-control" value="'+$('#tempSection').val()+'">'
							+ '</div>'
							+ '<div class="col-lg-3">'
								+ '<button class="btn btn-danger btn-xs delete-button" type="button"><i class="fa fa-trash-o "></i></button>'
							+ '</div>'
						+ '</div>'
					+ '</div>';*/
			var html='<div class="form-group js-section">'+
						'<div class="input-group">'+
							'<input type="text" class="form-control" value="'+$('#tempSection input').val()+'" id="0">'+
							'<span class="input-group-btn">'+
								'<button class="btn w-60 blue-steel" type="button" disabled>Draft</button>'+
								/*'<button class="btn green" type="button"><i class="fa fa-edit"></i></button>'+*/
								'<button class="btn red delete-button" type="button"><i class="fa fa-trash"></i></button>'+
							'</span>'+
						'</div>'+
					'</div>';
			$('#sections').append(html);
			$('#tempSection input').val('');
			if($('#switchNo').prop('checked'))
				$('#switchNo').click();
			$('.delete-button').on('click', function() {
				//$(this).closest('.form-group').remove();
				$(this).closest('.js-section').remove();
				$('#sectionTimes .row:eq(0)').find('*').remove();
				if($('#switchNo').prop('checked'))
					$('#switchNo').trigger('click');
				if($('#sections').find('input[type="text"]').length <= 1) {
					$('#switchYes').prop('checked', true);
					$('#orderRandom').prop('checked', true);
					$('#switchSections').addClass('hide');
					$('.js-sections-manage').addClass('hide');
					$('#sectionTimes').addClass('hide');
					$('#totalTime').removeClass('hide');
				}
				if($('#orderStart').prop('checked')) {
					$('#orderStart').click();
				} else if($('#orderRandom').prop('checked')) {
					$('#orderRandom').click();
				}
			});
			if($('#sections').find('input[type="text"]').length > 1) {
				if ($('#switchSections').hasClass('hide')) {
					$('#switchYes').prop('checked', false);
					$('#switchNo').prop('checked', true);
					$('#switchNo').click();
					$('#switchSections').removeClass('hide');
				}
				if ($('.js-sections-manage').hasClass('hide')) {
					$('#orderRandom').prop('checked', false);
					$('#orderStart').prop('checked', true);
					$('.js-sections-manage').removeClass('hide');
				}
			}
		}
		else {
			$('#tempSection').parent().addClass('has-error');
			$('#tempSection').parent().append('<span class="help-block">Please provide a section name.</span>');
		}
		
		//event listener for bluring sections name
		$('#sections input[type="text"]').off('blur');
		$('#sections input[type="text"]').on('blur', function() {
			if($('#orderStart').prop('checked')) {
				$('#orderStart').click();
			} else if($('#orderRandom').prop('checked')) {
				$('#orderRandom').click();
			}
		});
		$('#sections input[type="text"]:eq(0)').blur();
	});
	$('#switchYes').on('click', function() {
		$('#sectionTimes').addClass('hide');
		$('#totalTime').removeClass('hide');
	});
	$('#switchNo').on('click', function() {
		$('#totalTime').addClass('hide');
		var html = '';
		$('#sectionTimes .row:eq(0)').find('*').remove();
		if($('#sections').find('input[type="text"]').length == 0) {
			toastr.error('Please Add a section first.');
		}
		for(i=0; i<$('#sections').find('input[type="text"]').length; i++) {
			html+= '<div class="col-md-6">'+
                        '<div class="form-group">'+
                            '<label class="control-label">Total Time for '+$('#sections').find('input[type="text"]:eq('+i+')').val()+'</label>'+
                            '<div class="clearfix">'+
                                '<div class="dib">'+
                                    '<div class="input-group input-small">'+
                                        '<input type="number" class="form-control time hour" value="0" placeholder="Hours" aria-describedby="sizing-addon1">'+
                                        '<span class="input-group-addon" id="sizing-addon1">Hours</span>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="dib">'+
                                    '<div class="input-group input-small">'+
                                        '<input type="number" class="form-control time minute" value="0" placeholder="Minutes" aria-describedby="sizing-addon1">'+
                                        '<span class="input-group-addon" id="sizing-addon1">Minutes</span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'
		}
		$('#sectionTimes .row:eq(0)').append(html);
		$('#sectionTimes').removeClass('hide');
	});
	$("#noOfAttempts").keypress(function (e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			toastr.error('Enter Only Numbers');
			return false;
		}
	});
	
	$('#examDelete').on('click', function() {
		if(parseInt($('#examStatus').attr('data-assoc')) > 0) {
			var category = $(this).parents('tr').attr('data-category');
			if(!($('#sectionTable tbody').find('tr[data-id="'+category+'"] .required').text() < $('#sectionTable tbody').find('tr[data-id="'+category+'"] .entered').text())) {
				toastr.error('You can\'t cross the minimum required question limit as there are '+terminologies["student_plural"].toLowerCase()+' associated with this exam. Please remove them in order to delete any other question.')
				return;
			}
		}
		var con = confirm("Deleting this exam will also delete all sections and other data associated with it. Do you want to continue?");
		if(con) {
			var req = {};
			req.action = 'delete-exam';
			req.examId = examId;
			$.ajax({
				'type'	: 'post',
				'url'	: ApiEndPoint,
				'data'	: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					window.location = 'assignment-exam.php';
				}
			});
		}
	});
});
</script>