<script type="text/javascript">
function fetchInstituteInvitations() {
    var req = {};
    var res;
    req.action = 'get-subject-invitation';
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        console.log(res);
        fillInstituteInvitations(res);    
    });
}
function fillInstituteInvitations(data) {
    var html = '';
    $('#notify-list').find('li').remove();
    $.each(data.notification, function (i, notification) {
        html += '<li data-id="'+notification.id+'" class="list-group-item">'
            + '<div class="task-title">'
                + '<span class="task-title-sp"> An invitation has been sent by Institute <a href="' + sitepathMarket + 'institute/' + notification.instituteId + '">' + notification.name + '</a></span>'
                + '<div class="pull-right hidden-phone">';
                if(notification.status == 0)
                    html += '<button class="btn btn-primary btn-xs mark-notification"><i class="fa fa-check"></i> Accept </button>&emsp;<button class="btn btn-danger btn-xs unmark-notification"><i class="fa fa-times"></i> Reject</button>';
                else if(notification.status == 1)
                    html += '<button class="btn btn-success btn-xs"><i class="fa fa-check"></i> Accepted</button>&emsp;';
                else if(notification.status == 2)
                    html += '<button class="btn btn-danger btn-xs"><i class="fa fa-times"></i> Rejected</button>&emsp;';
                    html += '</div>'
            + '</div>'
        + '</li>';
    });
    if(html == '')
    html = 'No new invitations found.';
    $('#notify-list').append(html);
    
    //adding event handlers for accept button
    $('.mark-notification').on('click', function() {
        var id = $(this).parents('li').attr('data-id');
        var req = {};
        var li=$(this).parents('li');
        var res;
        req.action = 'accept-subject-invitation';
        req.notificationId  = id;
        $.ajax({
                'type'  :   'post',
                'url'   :   ApiEndPoint,
                'data'  :   JSON.stringify(req)
        }).done(function(res) {
            res = $.parseJSON(res);
            if(res.status == 0)
                toastr.error(res.message);
            else {
                toastr.success("Invitation is accepted.");
                li.find('.mark-notification').addClass('btn-success').removeClass('btn-primary').html('<i class="fa fa-check"></i> Accepted').removeClass('mark-notification').off('click');
                li.find('.unmark-notification').remove();
            }
        });
    });
    $('.unmark-notification').on('click', function() {
        var id = $(this).parents('li').attr('data-id');
        var req = {};
        var li=$(this).parents('li');
        var res;
        req.action = 'reject-subject-invitation';
        req.notificationId  = id;
        $.ajax({
                'type'  :   'post',
                'url'   :   ApiEndPoint,
                'data'  :   JSON.stringify(req)
        }).done(function(res) {
            res = $.parseJSON(res);
            if(res.status == 0)
                toastr.error(res.message);
            else {
                toastr.success("Invitation is rejected.");
                li.find('.unmark-notification').html('<i class="fa fa-times"></i> Rejected').removeClass('unmark-notification').off('click');
                li.find('.mark-notification').remove();
            }
        });
    });
}
</script>