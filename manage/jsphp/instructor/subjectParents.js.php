<script type="text/javascript">
var TableDatatablesManaged = function () {

    var initTable1 = function () {

        var table = $('#tblSubjectStudents');

        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
            "pagingType": "bootstrap_extended",

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,
            "columnDefs": [],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery('#sample_2_wrapper');

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).prop("checked", true);
                } else {
                    $(this).prop("checked", false);
                }
            });
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
        }

    };

}();
//function to get the parents and chapters for the subject
function fetchSubjectParents() {
    var req = {};
    var res;
    req.courseId = courseId;
    req.subjectId = subjectId;
    req.action ="get-subject-parents";
    $.ajax({
        'type'  :   'post',
        'url'   :   ApiEndPoint,
        'data'  :   JSON.stringify(req)
    }).done(function(res) {
        data = $.parseJSON(res);
        console.log(data);
        if(data.status == 0)
            toastr.error(data.message);
        else {
            //console.log(data);
            var html = "";
            if(data.students.length > 0) {
                for (var i = 0; i < data.students.length; i++)
                {
                    var student_id = data.students[i].id;
                    var student_username = data.students[i].username;
                    var name = data.students[i].name;
                    var password = ((data.students[i].password == "")?'<small class="text-success">REGISTERED</small>':data.students[i].password);
                    // var contactMobile = data.students[i].contactMobile;
                    // var course_key = data.students[i].key_id;
                    var email = data.students[i].email;
                    var date = data.students[i].date;
                    var checkbox = ((!data.students[i].parentid)?'<input type="checkbox" class="js-option-student" />':'');
                    var parent = ((!data.students[i].parentid)?'<td></td>':'<td><strong>'+data.students[i].userid+'</strong> / <strong>'+data.students[i].passwd+'</strong></td>');
                    html+= '<tr class="odd gradeX" data-parent="'+data.students[i].parentid+'" data-temp="'+data.students[i].temp+'">'+
                                '<td>' + data.students[i].parentid + '</td>'+
                                '<td>' + '<strong>'+data.students[i].userid+'</strong>' + '</td>'+
                                '<td>' + '<strong>'+data.students[i].passwd+'</strong>' + '</td>'+
                                '<td>' + student_id + '</td>'+
                                '<td>' + student_username + '</td>'+
                                '<td>' + name + '</td>'+
                                '<td><a href="mailto:' + email + '">' + email + '</a></td>'+
                                '<td>'+
                                    '<a href="javascript:void(0)" class="btn btn-circle btn-sm green js-notify" data-parent="'+data.students[i].parentid+'"><i class="fa fa-comment"></i> Notify</a>'+
                                '</td>'+
                            '</tr>';
                }
            } else {
                html+= '<tr class="odd gradeX">'+
                            '<td colspan="8">No '+terminologies["subject_single"].toLowerCase()+' '+terminologies["student_plural"].toLowerCase()+' yet!</td>'+
                        '</tr>';
            }
            $('#tblSubjectStudents tbody').append(html);
            TableDatatablesManaged.init();
        }
    });
}
$(document).ready(function() {
    //TableDatatablesManaged.init();
    $(".star-rating").rating({displayOnly: true});

    $('body').on("click", ".js-notify", function(){
        var parentId = $(this).attr("data-parent");
        $('#parentId').val(parentId);
        $('#modalNewParentNotification').modal('show');
    });
    $('#btnSaveNotification').click(function(){
        var req = {};
        var res;
        req.action = 'notify-parents';
        req.courseId = courseId;
        req.subjectId = subjectId;
        req.parentId = $('#parentId').val();
        req.notification = $('#notification').val();
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            //console.log(res);
            if (res.status == 0)
                toastr.error(res.message);
            else {
                toastr.success(res.message);
            }
        });
    });
});
</script>