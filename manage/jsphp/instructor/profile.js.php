<script type="text/javascript">
function fillProfileInDetail() {
	//console.log(profileDetails);
	$('.profile-userpic img').attr('src', profileDetails.profileDetails.profilePic).removeClass('invisible');
	$('#userProfilePic').attr('src', profileDetails.profileDetails.profilePic);
	$('.js-instructor').html(profileDetails.profileDetails.firstName+' '+profileDetails.profileDetails.lastName).removeClass('invisible');
	$('.profile-usertitle-job').html(profileDetails.profileDetails.tagline).removeClass('invisible');
	$('.profile-desc-title').html('About '+profileDetails.profileDetails.firstName+' '+profileDetails.profileDetails.lastName).removeClass('invisible');
	$('#description').html(profileDetails.profileDetails.description).removeClass('invisible').editable({
		showbuttons: 'bottom',
		type: 'textarea',
		pk: '1',
		placeholder: 'Enter description here ...'
	});
	$('#description').on('save', function(e, editable) {
		//console.log(editable);
		var req = {};
		var res;
		req.action = "update-profile-desc";
		req.description = editable.newValue;
		if(req.description.length>1000){
			toastr.error('Background Information should be less than 1000 characters.');
			return false;
		}
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndPoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				window.location = window.location;
				toastr.success(res.message);
			} else {
				toastr.error(res.message);
			}
			console.log(res);
		});
	});
	fillProfileStats();
}
function fillProfileStats() {
	//console.log(data);
	$('.js-courses').html(profileDetails.courses.length);
	$('.js-subjects').html(profileDetails.subjectCount);
	//$('.js-instructors').html(profileDetails.instructors.instituteProfessors.length);
	//console.log(subPage);
	if (subPage == 'profileMe') {
		fillProfileMeStats();
	} else if (subPage == 'profileEdit') {
		//fillProfileEditForm();
		getCountriesList()
	}
}
$(document).ready(function(){
});
</script>