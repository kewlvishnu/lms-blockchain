<script type="text/javascript">
	if (currAddress == "") {
		$("#modalSetAddress").modal("show");
	}
	$('body').on('click', '.btn-bookmark', function(e){
		//bookmarkThisPage(e);
		var thiz = $(this);
		var courseId = thiz.attr('data-course');
		if (!courseId) {
			toastr.error("There is some error, please refresh and try again!");
		} else {
			var req = {};
			var res;
			req.courseId = courseId;
			req.action = 'favorite-course';
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				//console.log(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					thiz.removeClass('btn-bookmark').addClass('btn-unbookmark');
					thiz.html('Unfavorite');
					thiz.closest('.mt-widget-2').find('.js-favorite').html('<div class="mt-favorite"><i class="fa fa-bookmark"></i></div>');
					toastr.success(res.message);
				}
			});
		}
	});
	$('body').on('click', '.btn-unbookmark', function(e){
		//bookmarkThisPage(e);
		var thiz = $(this);
		var courseId = thiz.attr('data-course');
		if (!courseId) {
			toastr.error("There is some error, please refresh and try again!");
		} else {
			var req = {};
			var res;
			req.courseId = courseId;
			req.action = 'unfavorite-course';
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				//console.log(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					thiz.removeClass('btn-unbookmark').addClass('btn-bookmark');
					thiz.html('<i class="fa fa-bookmark"></i> Favorite');
					thiz.closest('.mt-widget-2').find('.js-favorite').html('');
					toastr.success(res.message);
				}
			});
		}
	});

	//function to fetch counters
	function fetchCounters() {
		var req = {};
		var res;
		req.portfolioSlug = subdomain;
		req.pageUserId = $('#inputUserId').val();
		req.pageUserRole = $('#inputUserRole').val();
		req.action = 'get-institute-counters';
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status == 0)
				toastr.error(res.message);
			else
				fillCounters(res);
		});
	}

	//function to fetch graph data
	function fetchGraphs() {
		var req = {};
		var res;
		req.portfolioSlug = subdomain;
		req.pageUserId = $('#inputUserId').val();
		req.pageUserRole = $('#inputUserRole').val();
		req.action = 'get-institute-graphs';
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status == 0)
				toastr.error(res.message);
			else {
				var html = '';
				var flag = false;
				$.each(res.graph1, function(i, d) {
					courses.push(d);
					html += '<option value="' + d.courseId + '">' + d.name + '</option>';
					if(d.students != 0)
						flag = true;
					//cutting the course names
					if(d.name.length > 25) {
						var first = d.name.substring(0, 9);
						var second = d.name.substring(d.name.length - 8);
						res.graph1[i].name = first + '...' + second;
					}
				});
				$('#courseSelector').append(html);
				fetchCourseViewGraph();
				totalLength = Math.floor(res.graph1.length / 10);
				if(flag)
					splitCourseto10(0);
				else {
					$('#dashboard_amchart_1').append('<p class="note note-info">This graph will only be visible when '+terminologies["student_plural"].toLowerCase()+' are enrolled in any of the '+terminologies["course_plural"].toLowerCase()+'</p>');
				}
			}
		});
	}
	
	//to fill the couter values
	function fillCounters(data) {
		$('.courseCount').attr('data-value', data.courseCount).counterUp();
		$('.studentCount').attr('data-value', data.studentCount).counterUp();
		$('.assignedSubjectsCount').attr('data-value', data.assignedSubjects).counterUp();
		$('.assignedSubjectsStudent').attr('data-value', data.assignedSubjectsStudent).counterUp();
	}

	function splitCourseto10(number) {
		var start = number * 10;
		var temp = courses.slice(start, start + 10);
		fillGraphs(temp);
		if(number == 0 && totalLength == 0) {
			$('#next').hide();
			$('#previous').hide();
		}
		else if(number == 0 && totalLength > 0) {
			$('#next').show();
			$('#previous').hide();
		}
		else if(number == totalLength) {
			$('#next').hide();
			$('#previous').show();
		}
		else {
			$('#next').show();
			$('#previous').show();
		}
	}

	//function to fill the graphs
	function fillGraphs(data) {
		var graphdata = [];
		for(var i = 0; i < data.length; i++) {
			var obj = [];
			obj.course = data[i].name;
			obj.students = data[i].students;
			graphdata.push(obj);
		}

		var chartData = graphdata;
	    var chart = AmCharts.makeChart("dashboard_amchart_1", {
	        type: "serial",
	        fontSize: 12,
	        fontFamily: "Open Sans",
	        dataDateFormat: "YYYY-MM-DD",
	        dataProvider: chartData,

	        addClassNames: true,
	        startDuration: 1,
	        color: "#6c7b88",
	        marginLeft: 0,

	        categoryField: "course",
	        categoryAxis: {
				"labelRotation": 60
			},

	        valueAxes: [{
	            id: "a1",
	            title: "No Of "+terminologies["student_plural"],
	            gridAlpha: 0,
	            axisAlpha: 0
	        }, {
	            id: "a2",
	            position: "right",
	            gridAlpha: 0,
	            axisAlpha: 0,
	            labelsEnabled: false
	        }, {
	            id: "a3",
	            title: "course",
	            position: "right",
	            gridAlpha: 0,
	            axisAlpha: 0,
	            inside: true,
	            duration: "mm"
	        }],
	        graphs: [{
	            id: "g1",
	            valueField: "students",
	            title: "students",
	            type: "column",
	            fillAlphas: 0.7,
	            valueAxis: "a1",
	            balloonText: "[[value]] students",
	            legendValueText: "[[value]] students",
	            legendPeriodValueText: "total: [[value.sum]] "+terminologies["student_plural"].toLowerCase(),
	            lineColor: "#08a3cc",
	            alphaField: "alpha",
	        }],

	        chartCursor: {
	            zoomable: false,
	            categoryBalloonDateFormat: "DD",
	            cursorAlpha: 0,
	            categoryBalloonColor: "#e26a6a",
	            categoryBalloonAlpha: 0.8,
	            valueBalloonsEnabled: false
	        },

	        legend: {
	            bulletType: "round",
	            equalWidths: false,
	            valueWidth: 120,
	            useGraphSettings: true,
	            color: "#6c7b88"
	        }
	    });

	}

	/*function fetchSubjectInvitations() {
		var req = {};
		var res;
		req.action = "get-subject-invitation";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndPoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillSubjectInvitations(res);
		});
	}

	function fillSubjectInvitations(data) {
		if(data.notification.length>0) {
			var html = '';
			var profileLink = '';
			for(var i = 0; i < data.notification.length; i++) {
				profileLink = (data.notification[i].userRole == 1)?sitepathInstitute:sitepathInstructor;
				html += '<div class="mt-action">'+
							'<div class="mt-action-img">'+
								'<img src="'+data.notification[i].profilePic+'" /> </div>'+
							'<div class="mt-action-body">'+
								'<div class="mt-action-row">'+
									'<div class="mt-action-info ">'+
										'<div class="mt-action-details ">'+
											'<a href="'+profileLink+data.notification[i].instituteId+'" class="mt-action-author">'+data.notification[i].name+'</a>'+
											'<p class="mt-action-desc">Institute Vishwakarma Institutes has invited you to join their institution</p>'+
										'</div>'+
									'</div>'+
									'<div class="mt-action-buttons ">'+
										'<div class="">';
				if(data.notification[i].status == 0)
					html += '<button type="button" class="btn btn-outline btn-circle green btn-sm mark-invitation" data-id="'+data.notification[i].id+'">Accept Invitation</button><button type="button" class="btn btn-outline btn-circle red btn-sm unmark-invitation" data-id="'+data.notification[i].id+'">Reject Invitation</button>';
				else if(data.notification[i].status == 1)
					html += '<button type="button" class="btn green-jungle btn-circle btn-sm" data-id="'+data.notification[i].id+'" disabled><i class="fa fa-check"></i> Accepted</button>';
				else if(data.notification[i].status == 2)
					html += '<button type="button" class="btn red btn-sm" btn-circle data-id="'+data.notification[i].id+'" disabled><i class="fa fa-times"></i> Rejected</button>';
				html += 				'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>';
			}
			$('#siteNotifications').html(html);
            $('.mark-invitation').click(function(){
            	var id = $(this).attr('data-id');
				var req = {};
				var thiz=$(this);
				var res;
				req.action = 'accept-subject-invitation';
				req.notificationId  = id;
				$.ajax({
						'type'	:	'post',
						'url'	:	ApiEndPoint,
						'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 0)
						toastr.error(res.message);
					else {
						toastr.success("Invitation is accepted.");
						thiz.text('<i class="fa fa-check"></i> Accepted').attr('disabled').removeClass('green btn-outline').addClass('green-jungle');
					}
				});
            });
			$('.unmark-notification').on('click', function() {
            	var id = $(this).attr('data-id');
				var req = {};
				var thiz=$(this);
				var res;
				req.action = 'reject-subject-invitation';
				req.notificationId  = id;
				$.ajax({
						'type'	:	'post',
						'url'	:	ApiEndPoint,
						'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 0)
						toastr.error(res.message);
					else {
						toastr.error("Invitation is rejected.");
						thiz.text('<i class="fa fa-times"></i> Rejected').attr('disabled').removeClass('green btn-outline').addClass('red');
					}
				});
			});
		} else {
			//$('#siteNotifications').html('No notifications.');
			//$('#blockNotifications').remove();
		}
	}*/

	/*function fillDashboardNotifications(data) {

		if (data.chatnotifications.length>0) {
			var html = '';
            for(var i = 0; i < data.chatnotifications.length; i++) {
                var message = "";
                var d = new Date(data.chatnotifications[i].timestamp);
                //d = timeSince(d.getTime()); //returns 1340220044000
                var notiDate = d.getDate();
                var notiMonth = month[d.getMonth()];
                var notiHours = d.getHours();
                var notiMinutes = d.getMinutes();
                var strTime = getTime12Hour(notiHours,notiMinutes);
                switch(data.chatnotifications[i].type) {
                    case '0':
                        if (data.chatnotifications[i].room_type == "public") {
                            message = data.chatnotifications[i].senderName+' just started conversation in <strong>Subject : '+data.chatnotifications[i].courseName+'</strong> of <strong>Course : '+data.chatnotifications[i].subjectName+'</strong></span>';
                        } else {
                            message = data.chatnotifications[i].senderName+' just started conversation with '+data.chatnotifications[i].firstName+' '+data.chatnotifications[i].lastName+' in <strong>Subject : '+data.chatnotifications[i].courseName+'</strong> of <strong>Course : '+data.chatnotifications[i].subjectName+'</strong></span>';
                        }
                        break;
                    case '1':
                        if (data.chatnotifications[i].room_type == "public") {
                            message = data.chatnotifications[i].senderName+' just sent a message in <strong>Subject : '+data.chatnotifications[i].courseName+'</strong> of <strong>Course : '+data.chatnotifications[i].subjectName+'</strong></span>';
                        } else {
                            message = data.chatnotifications[i].senderName+' just messaged '+data.chatnotifications[i].firstName+' '+data.chatnotifications[i].lastName+' in <strong>Subject : '+data.chatnotifications[i].courseName+'</strong> of <strong>Course : '+data.chatnotifications[i].subjectName+'</strong></span>';
                        }
                        break;
                }
                html += '<div class="mt-comment chat-parent">'+
                            '<div class="mt-comment-img">'+
                                '<img src="'+data.chatnotifications[i].profilePic+'" /> </div>'+
                            '<div class="mt-comment-body">'+
                                '<div class="mt-comment-info">'+
                                    '<span class="mt-comment-author">'+data.chatnotifications[i].senderName+'</span>'+
                                    '<span class="mt-comment-date">'+notiDate+' '+notiMonth+', '+strTime+'</span>'+
                                '</div>'+
                                '<div class="mt-comment-text"> '+message+' </div>'+
                            '</div>'+
                        '</div>';
            }
            $('.portlet-chats .mt-comments').html(html);
        } else {
            //$('.portlet-chats .mt-comments').html('No chat notifications.');
            //$('#blockChat').remove();
        }
	}*/

	function fillCourses(data) {
		if (data.courses.length>0) {
            $('#listCourses').html('');
            $.each(data.courses, function (i, course) {
                //var startDate = new Date(course.liveDate);
                //startDate = timeSince(startDate.getTime()); //returns 1340220044000
                //alert(course.liveDate);
                var startDate = timeSince(course.liveDate);
                var btnFav = "";
                if (course.favorite==1) {
                	btnFav = '<a href="javascript:;" class="btn btn-unbookmark" data-course="'+course.id+'">'+
                                            'Unfavorite </a>';
                } else {
                	btnFav = '<a href="javascript:;" class="btn btn-bookmark" data-course="'+course.id+'">'+
                                            '<i class="fa fa-bookmark"></i> Favorite </a>';
                }
                $('#listCourses').append(
                    '<div class="col-lg-3 col-md-4 col-sm-6">'+
                        '<div class="mt-widget-2 bg-'+themeColors[getRandomInt(0, themeColors.length-1)]+'">'+
                            '<div class="mt-head" style="background-image: url(' + course.image + ');">'+
                                '<div class="mt-head-user">'+
                                    '<div class="mt-head-user-img">'+
                                        '<img src="' + profileDetails.profileDetails.profilePic + '"> </div>'+
                                    '<div class="mt-head-user-info">'+
                                        '<span class="mt-user-name">' + profileDetails.profileDetails.firstName + ' ' + profileDetails.profileDetails.lastName + '</span>'+
                                        '<span class="mt-user-time">'+
                                            '<i class="icon-clock"></i> '+startDate+' </span>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="js-favorite">'+
                                	((course.favorite==1)?('<div class="mt-favorite"><i class="fa fa-bookmark"></i></div>'):(''))+
                                '</div>'+
                            '</div>'+
                            '<div class="mt-body">'+
                                '<h3 class="mt-body-title"> ' + course.name + ' </h3>'+
                                '<p class="mt-body-description"> ' + course.subtitle + ' </p>'+
                                '<div class="mt-body-actions">'+
                                    '<div class="btn-group btn-group btn-group-justified">'+
                                        btnFav+
                                        '<a href="'+sitepathManageCourses+course.id+'" class="btn">'+
                                            '<i class="fa fa-edit"></i> Edit </a>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>');
            });
        } else {
            /*$('#listCourses').removeClass('row').html('Sorry :( You don\'t have any '+terminologies["course_plural"].toLowerCase()+'. You can checkout our '+terminologies["course_plural"].toLowerCase()+' <a href="'+sitePathCourses+'">here</a>.');*/
            $('#listCourses').removeClass('row').html('Sorry :( You don\'t have any '+terminologies["course_plural"].toLowerCase()+', you can start your a <a href="javascript:void(0)" data-action="new" data-toggle="modal" class="btn-course">new '+terminologies["course_single"].toLowerCase()+'</a>.');
        }
	}

	$('#courseSelector').on('change', function() {
		fetchCourseViewGraph();
	});
	function fetchCourseViewGraph() {
		var courseSelected = $("#courseSelector").val();
		//if($(this).val() != 0) {
			var req = {};
			var res;
			req.action = 'get-recent-course-view';
			req.courseId = courseSelected;
			//courseId=$(this).val();
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else
					fillCourseViewGraph(res);
			});
		//}
	};
	
	function fillCourseViewGraph(data) {
		//console.log(data);
		/*var filler = [];
		var cats = [];
		for(var i = data.graph.length - 1; i >= 0; i--) {
			cats.push(data.graph[i].date);
			filler.push(parseInt(data.graph[i].opened));
		}*/
		if (data.graph != undefined) {
			var graphdata = [];
			for(var i = data.graph.length - 1; i >= 0; i--) {
				var obj = [];
				obj.date = data.graph[i].date;
				obj.opened = parseInt(data.graph[i].opened);
				graphdata.push(obj);
			}
			//console.log(graphdata);
			var chart = AmCharts.makeChart("dashboard_amchart_3", {
		        type: "serial",
		        fontSize: 12,
		        fontFamily: "Open Sans",
		        dataDateFormat: "YYYY-MM-DD",
		        dataProvider: graphdata,

		        addClassNames: true,
		        startDuration: 1,
		        color: "#6c7b88",
		        marginLeft: 0,

		        categoryField: "date",
		        categoryAxis: {
					"labelRotation": 60
				},

		        valueAxes: [{
		            id: "a1",
		            title: "No Of Views",
		            gridAlpha: 0,
		            axisAlpha: 0
		        }, {
		            id: "a2",
		            position: "right",
		            gridAlpha: 0,
		            axisAlpha: 0,
		            labelsEnabled: false
		        }, {
		            id: "a3",
		            title: "Views",
		            position: "right",
		            gridAlpha: 0,
		            axisAlpha: 0,
		            inside: true,
		            duration: "mm"
		        }],
		        graphs: [{
		            id: "g1",
		            valueField: "opened",
		            title: "Views",
		            type: "column",
		            fillAlphas: 0.7,
		            valueAxis: "a1",
		            balloonText: "[[value]] views",
		            legendValueText: "[[value]] views",
		            legendPeriodValueText: "total: [[value.sum]] views",
		            lineColor: "#08a3cc",
		            alphaField: "alpha",
		        }],

		        chartCursor: {
		            zoomable: false,
		            categoryBalloonDateFormat: "DD",
		            cursorAlpha: 0,
		            categoryBalloonColor: "#e26a6a",
		            categoryBalloonAlpha: 0.8,
		            valueBalloonsEnabled: false
		        },

		        legend: {
		            bulletType: "round",
		            equalWidths: false,
		            valueWidth: 120,
		            useGraphSettings: true,
		            color: "#6c7b88"
		        }
		    });
		}
	}
</script>