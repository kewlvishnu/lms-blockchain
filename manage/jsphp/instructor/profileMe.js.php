<script type="text/javascript">
function fillProfileMeStats(data) {
	$('.js-email').html(profileDetails.loginDetails.email);
	if (profileDetails.profileDetails.ownerFirstName && profileDetails.profileDetails.ownerLastName) {
		$('.js-owner').html(profileDetails.profileDetails.ownerFirstName+' '+profileDetails.profileDetails.ownerLastName);
	}
	if (profileDetails.profileDetails.contactFirstName && profileDetails.profileDetails.contactLastName) {
		$('.js-contact-name').html(profileDetails.profileDetails.contactFirstName + " " + profileDetails.profileDetails.contactLastName);
	}
	if (profileDetails.profileDetails.studentCount) {
		$('.js-students').html(profileDetails.profileDetails.studentCount);
	}
	if (profileDetails.profileDetails.foundingYear>0) {
		$('.js-year').html(profileDetails.profileDetails.foundingYear);
	}
	if (profileDetails.userDetails.contactMobile) {
		$('.js-contact').html(profileDetails.userDetails.contactMobilePrefix+'-'+profileDetails.userDetails.contactMobile);
	}
	if (profileDetails.userDetails.addressCountry) {
		$('.js-country').html(profileDetails.userDetails.addressCountry);
		$('.js-address').html(formatAddress(profileDetails.userDetails));
	}
	html = '';
	if (profileDetails.courses.length>0) {
		$.each(profileDetails.courses, function(key, course){
			//console.log(course);
			html+= '<tr>'+
						'<td>'+
							'<a href="javascript:;"> '+course.name+' </a>'+
						'</td>'+
						'<td class="hidden-xs">';
			if (course.subjects.length>0) {
				$.each(course.subjects, function(key, subject){
					//console.log(subject);
					html+= '<a href="'+sitepathManageSubjects+subject.id+'">'+subject.name+'</a>'+((key==(course.subjects.length-1))?'':', ');
				});
			}
			html+= 		'</td>'+
						'<td> '+course.studentCount+' </td>'+
						'<td>'+
							'<a class="btn btn-sm grey-salsa btn-outline" href="'+sitepathManageCourses+course.id+'"> View </a>'+
						'</td>'+
					'</tr>';
		});
	} else {
		html+= '<tr><td colspan="4">Oops! No live '+terminologies["course_plural"].toLowerCase()+'</td></tr>';
	}
	$('#tableArcaneExp tbody').html(html);
}
</script>