<script type="text/javascript">
var TableDatatablesManaged = function () {

    var initTable1 = function () {

        var table = $('#sample_1');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // setup buttons extension: http://datatables.net/extensions/buttons/
            buttons: [
                { extend: 'print', className: 'btn dark btn-outline' },
                { extend: 'pdf', className: 'btn green btn-outline' },
                { extend: 'csv', className: 'btn purple btn-outline' }
            ],

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {  // set default column settings
                    'orderable': false,
                    'targets': [0]
                }, 
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery('#sample_1_wrapper');

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).prop("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).prop("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
        }

    };

}();
    
function fetchInstituteProfessors() {
    var req = {};
    var res;
    req.action = "get-institute-prof-with-sub";
    
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1)
            fillInstituteProfessors(res);
        else
            toastr.error(res.message);
    });
}

function fillInstituteProfessors(data) {
    //console.log(data);
    
    //TableDatatablesManaged.init();
    var html = "";
    var count = 0;
    if(data.instituteProfessors.length > 0) {
        $.each(data.instituteProfessors, function(i, prof) {
            html += '<tr class="odd gradeX">'+
                        '<td>'+(i+1)+'</td>'+
                        '<td><div class="img-cover img-circle"><img src="' + prof.profilePic + ' " /></div></td>'+
                        '<td><a href="'+sitepathInstructor + prof.professorId + '" target="_blank"> ' + prof.firstName + " " + prof.lastName + ' </a></td>'+
                        '<td><a href="mailto:'+ prof.email + '"> ' + prof.email + ' </a></td>'+
                        '<td>' + formatContactNumber(prof) + '</td>'+
                        '<td>';
        if(prof.courses != undefined)
        {
            html += '<ul class="list-unstyled">';
            $.each(prof.courses, function(i, course) {
                //html += '<li><span class="badge btn-sm bg-green-jungle" title="course">S</span> ' +  course.subjectName +  ' (' + course.courseName + ')</li>';
                html += '<li><span class="badge btn-sm bg-blue" title="course">C</span> ' +  course.courseName + ' : ';
                $.each(prof.courses, function(i, course) {
                    html += course.subjectName +  ', ';
                });
                html = html.substring(0,html.length-2) + '</li>';
            });
            html += '</ul>';
        }
            html +=     '</td>'+
                        '<td>'+
                            /*'<a href="javascript:void(0)" class="btn btn-circle btn-sm green"><i class="fa fa-comment"></i> Chat</a>'+*/
                            '<a href="javascript:void(0)" class="btn btn-circle btn-sm red delete-professor" data-professor="'+ prof.professorId +'"><i class="fa fa-trash"></i> Remove</a>'+
                        '</td>'+
                    '</tr>';
        });
        $(".table-instructors tbody").html(html);
    }
}
$(document).ready(function(){
    $('.table-instructors').on('click', '.delete-professor', function() {
        var req = {};
        var res;
        var thiz = $(this);
        var professor = $(this).attr('data-professor');
        con = confirm("Are you sure to delete this "+terminologies["instructor_single"].toLowerCase()+"?");
        if(con) {
            req = {};
            req.professorId = professor;
            req.action = 'delete-institute-professor';
            $.ajax({
                'type'  :   'post',
                'url'   :   ApiEndPoint,
                'data'  :   JSON.stringify(req)
            }).done(function(res) {
                res = $.parseJSON(res);
                if(res.status == 0) {
                    toastr.error(res.message);
                }
                else {
                    toastr.success(terminologies["instructor_single"]+' removed successfully');
                    thiz.parents('tr').remove();
                }
            });
        }
    });
});
</script>