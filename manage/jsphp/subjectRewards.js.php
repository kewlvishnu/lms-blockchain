<script type="text/javascript">
function fetchSubjectAllStuff() {
    var req = {};
    var res;
    req.courseId = courseId;
    req.subjectId = subjectId;
    //req.action ="get-subject-progress-details";  
    req.action ="get-subject-stuff";
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1)
            fillSubjectAllStuff(res);
        else
            toastr.error(res.message);
    });
}
function fillSubjectAllStuff(data) {
    var html = '';
    if (data.notes) {
        //console.log(data.notes);
        CKEDITOR.instances['notes'].setData(data.notes);
    }
    if (data.contents) {
        var contents = data.contents;
        for (var i = 0; i < contents.length; i++) {
            var section = contents[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            html+='<tr data-id="'+contents[i].id+'" data-type="content">'+
                    '<td>'+contents[i].title+'</td>'+
                    '<td>Content</td>'+
                    '<td>'+section+'</td>';
            var pblock = [];
            for (var j = 1; j <= 5; j++) {
                pblock[j]='<td class="text-center" valign="middle" data-position="'+j+'"><button class="btn green btn-add-reward">Add</button></td>'; //contents[i]["rewards"][j]
            }
            //console.log(contents[i]["rewards"]);
            for (var j = 0; j < contents[i]["rewards"].length; j++) {
                //console.log(contents[i]["rewards"][j]);
                pblock[contents[i]["rewards"][j].position] = '<td class="text-center" valign="middle" data-position="'+contents[i]["rewards"][j].position+'"><div class="dib-left"><span class="r-per">'+contents[i]["rewards"][j]["percent_block"]+'</span>% <br /> Reward: <br /><dl class="mb-10">';
                for (var k = 0; k < contents[i]["rewards"][j]["rewards"].length; k++) {
                    var rewards = contents[i]["rewards"][j]["rewards"];
                    //console.log(rewards);
                    pblock[contents[i]["rewards"][j].position]+= '<dd><span class="reward-ra">'+rewards[k].reward_amount+'</span> <span class="reward-rc">'+rewards[k].reward_currency+'</span></dd>';
                }
                pblock[contents[i]["rewards"][j].position]+= '</dl><button class="btn btn-xs green btn-edit-reward"><i class="fa fa-edit"></i> Edit</button></div></td>';
            }
            for (var j = 1; j <= 5; j++) {
                html+=pblock[j]; //contents[i]["rewards"][j]
            }
            html+='</tr>';
        }
    }
    if (data.exams) {
        var exams = data.exams;
        for (var i = 0; i < exams.length; i++) {
            var section = exams[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            html+='<tr data-id="'+exams[i].id+'" data-type="exam">'+
                    '<td>'+exams[i].name+'</td>'+
                    '<td>'+exams[i].type+'</td>'+
                    '<td>'+section+'</td>';
            var pblock = [];
            for (var j = 1; j <= 5; j++) {
                pblock[j]='<td class="text-center" valign="middle" data-position="'+j+'"><button class="btn green btn-add-reward">Add</button></td>';
            }
            for (var j = 0; j < exams[i]["rewards"].length; j++) {
                pblock[exams[i]["rewards"][j].position] = '<td class="text-center" valign="middle" data-position="'+exams[i]["rewards"][j].position+'"><div class="dib-left"><span class="r-per">'+exams[i]["rewards"][j]["percent_block"]+'</span>% <br /> Reward: <br /><dl class="mb-10">';
                for (var k = 0; k < exams[i]["rewards"][j]["rewards"].length; k++) {
                    var rewards = exams[i]["rewards"][j]["rewards"];
                    //console.log(rewards);
                    pblock[exams[i]["rewards"][j].position]+= '<dd><span class="reward-ra">'+rewards[k].reward_amount+'</span> <span class="reward-rc">'+rewards[k].reward_currency+'</span></dd>';
                }
                pblock[exams[i]["rewards"][j].position]+= '</dl><button class="btn btn-xs green btn-edit-reward"><i class="fa fa-edit"></i> Edit</button></div></td>';
            }
            for (var j = 1; j <= 5; j++) {
                html+=pblock[j];
            }
            html+='</tr>';
        }
    }
    if (data.subjective_exams) {
        var exams = data.subjective_exams;
        for (var i = 0; i < exams.length; i++) {
            var section = exams[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            html+='<tr data-id="'+exams[i].id+'" data-type="subjective-exam">'+
                    '<td>'+exams[i].name+'</td>'+
                    '<td>Subjective Exam</td>'+
                    '<td>'+section+'</td>';
            var pblock = [];
            for (var j = 1; j <= 5; j++) {
                pblock[j]='<td class="text-center" valign="middle" data-position="'+j+'"><button class="btn green btn-add-reward">Add</button></td>';
            }
            for (var j = 0; j < exams[i]["rewards"].length; j++) {
                pblock[exams[i]["rewards"][j].position] = '<td class="text-center" valign="middle" data-position="'+exams[i]["rewards"][j].position+'"><div class="dib-left"><span class="r-per">'+exams[i]["rewards"][j]["percent_block"]+'</span>% <br /> Reward: <br /><dl class="mb-10">';
                for (var k = 0; k < exams[i]["rewards"][j]["rewards"].length; k++) {
                    var rewards = exams[i]["rewards"][j]["rewards"];
                    //console.log(rewards);
                    pblock[exams[i]["rewards"][j].position]+= '<dd><span class="reward-ra">'+rewards[k].reward_amount+'</span> <span class="reward-rc">'+rewards[k].reward_currency+'</span></dd>';
                }
                pblock[exams[i]["rewards"][j].position]+= '</dl><button class="btn btn-xs green btn-edit-reward"><i class="fa fa-edit"></i> Edit</button></div></td>';
            }
            for (var j = 1; j <= 5; j++) {
                html+=pblock[j];
            }
            html+='</tr>';
        }
    }
    if (data.manual_exams) {
        var exams = data.manual_exams;
        for (var i = 0; i < exams.length; i++) {
            var section = exams[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            html+='<tr data-id="'+exams[i].id+'" data-type="manual-exam">'+
                    '<td>'+exams[i].name+'</td>'+
                    '<td>Manual Exam</td>'+
                    '<td>'+section+'</td>';
            var pblock = [];
            for (var j = 1; j <= 5; j++) {
                pblock[j]='<td class="text-center" valign="middle" data-position="'+j+'"><button class="btn green btn-add-reward">Add</button></td>';
            }
            for (var j = 0; j < exams[i]["rewards"].length; j++) {
                pblock[exams[i]["rewards"][j].position] = '<td class="text-center" valign="middle" data-position="'+exams[i]["rewards"][j].position+'"><div class="dib-left"><span class="r-per">'+exams[i]["rewards"][j]["percent_block"]+'</span>% <br /> Reward: <br /><dl class="mb-10">';
                for (var k = 0; k < exams[i]["rewards"][j]["rewards"].length; k++) {
                    var rewards = exams[i]["rewards"][j]["rewards"];
                    //console.log(rewards);
                    pblock[exams[i]["rewards"][j].position]+= '<dd><span class="reward-ra">'+rewards[k].reward_amount+'</span> <span class="reward-rc">'+rewards[k].reward_currency+'</span></dd>';
                }
                pblock[exams[i]["rewards"][j].position]+= '</dl><button class="btn btn-xs green btn-edit-reward"><i class="fa fa-edit"></i> Edit</button></div></td>';
            }
            for (var j = 1; j <= 5; j++) {
                html+=pblock[j];
            }
            html+='</tr>';
        }
    }
    if (data.submissions) {
        var exams = data.submissions;
        for (var i = 0; i < exams.length; i++) {
            var section = exams[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            html+='<tr data-id="'+exams[i].id+'" data-type="submission">'+
                    '<td>'+exams[i].name+'</td>'+
                    '<td>Submission</td>'+
                    '<td>'+section+'</td>';
            var pblock = [];
            for (var j = 1; j <= 5; j++) {
                pblock[j]='<td class="text-center" valign="middle" data-position="'+j+'"><button class="btn green btn-add-reward">Add</button></td>';
            }
            for (var j = 0; j < exams[i]["rewards"].length; j++) {
                pblock[exams[i]["rewards"][j].position] = '<td class="text-center" valign="middle" data-position="'+exams[i]["rewards"][j].position+'"><div class="dib-left"><span class="r-per">'+exams[i]["rewards"][j]["percent_block"]+'</span>% <br /> Reward: <br /><dl class="mb-10">';
                for (var k = 0; k < exams[i]["rewards"][j]["rewards"].length; k++) {
                    var rewards = exams[i]["rewards"][j]["rewards"];
                    //console.log(rewards);
                    pblock[exams[i]["rewards"][j].position]+= '<dd><span class="reward-ra">'+rewards[k].reward_amount+'</span> <span class="reward-rc">'+rewards[k].reward_currency+'</span></dd>';
                }
                pblock[exams[i]["rewards"][j].position]+= '</dl><button class="btn btn-xs green btn-edit-reward"><i class="fa fa-edit"></i> Edit</button></div></td>';
            }
            for (var j = 1; j <= 5; j++) {
                html+=pblock[j];
            }
            html+='</tr>';
        }
    }
    $('#listExams').html(html);
}
$(document).ready(function() {
    $(".star-rating").rating({displayOnly: true});
    $('#listExams').on('click', '.btn-add-reward', function(event) {
        event.preventDefault();
        /* Act on the event */
        var rewardItem = $(this).closest('tr');
        $("#inputRewardPercentage").val('');
        $(".js-rewards").html('<div class="row js-reward">'+
                                    '<div class="col-md-4">'+
                                        '<input type="text" name="inputRewardAmount" class="form-control reward-amount" value="">'+
                                    '</div>'+
                                    '<div class="col-md-4">'+
                                        '<select name="selectRewardCurrency" class="form-control reward-currency">'+
                                            '<option value="IGRO">IGRO</option>'+
                                            '<option value="ETH">ETH</option>'+
                                        '</select>'+
                                    '</div>'+
                                    '<div class="col-md-2">'+
                                        '<button type="button" class="btn green btn-reward"><i class="fa fa-plus-circle"></i></button>'+
                                    '</div>'+
                                '</div>');
        $("#btnAddEditReward").attr('data-id', rewardItem.attr('data-id'));
        $("#btnAddEditReward").attr('data-type', rewardItem.attr('data-type'));
        $("#btnAddEditReward").attr('data-position', $(this).closest('td').attr('data-position'));
        $("#modalAddEditReward").modal("show");
    });
    $('#listExams').on('click', '.btn-edit-reward', function(event) {
        event.preventDefault();
        /* Act on the event */
        var rewardItemParent = $(this).closest('tr');
        var rewardItem = $(this).closest('td');
        $("#inputRewardPercentage").val(rewardItem.find('.r-per').text());
        var html = "";
        rewardItem.find('dl dd').each(function(index, el) {
            var ra = $(el).find('.reward-ra').text();
            var rc = $(el).find('.reward-rc').text();
            html+= '<div class="row js-reward margin-top-10">'+
                        '<div class="col-md-4">'+
                            '<input type="text" name="inputRewardAmount" class="form-control reward-amount" value="'+ra+'">'+
                        '</div>'+
                        '<div class="col-md-4">'+
                            '<select name="selectRewardCurrency" class="form-control reward-currency">'+
                                '<option value="IGRO" '+((rc=='IGRO')?'selected':'')+'>IGRO</option>'+
                                '<option value="ETH" '+((rc=='ETH')?'selected':'')+'>ETH</option>'+
                            '</select>'+
                        '</div>'+
                        '<div class="col-md-2">'+
                            ((index==0)?('<button type="button" class="btn green btn-reward"><i class="fa fa-plus-circle"></i></button>'):('<button type="button" class="btn green btn-remove-reward"><i class="fa fa-minus-circle"></i></button>'))+
                        '</div>'+
                    '</div>';
        });
        $(".js-rewards").html(html);
        $("#btnAddEditReward").attr('data-id', rewardItemParent.attr('data-id'));
        $("#btnAddEditReward").attr('data-type', rewardItemParent.attr('data-type'));
        $("#btnAddEditReward").attr('data-position', $(this).closest('td').attr('data-position'));
        $("#modalAddEditReward").modal("show");
    });
    $('#modalAddEditReward').on("click",".btn-reward",function(event) {
        /* Act on the event */
        $(".js-rewards").append('<div class="row js-reward margin-top-10">'+
                                    '<div class="col-md-4">'+
                                        '<input type="text" name="inputRewardAmount" class="form-control reward-amount" value="">'+
                                    '</div>'+
                                    '<div class="col-md-4">'+
                                        '<select name="selectRewardCurrency" class="form-control reward-currency">'+
                                            '<option value="IGRO">IGRO</option>'+
                                            '<option value="ETH">ETH</option>'+
                                        '</select>'+
                                    '</div>'+
                                    '<div class="col-md-2">'+
                                        '<button type="button" class="btn green btn-remove-reward"><i class="fa fa-minus-circle"></i></button>'+
                                    '</div>'+
                                '</div>');
    });
    $('#modalAddEditReward').on("click",".btn-remove-reward",function(event) {
        /* Act on the event */
        $(this).closest('.js-reward').remove();
    });
    $('#btnAddEditReward').click(function(){
        var contentId = $(this).attr('data-id');
        var contentType = $(this).attr('data-type');
        var rewardPosition = $(this).attr('data-position');
        var inputRewardPercentage = $("#inputRewardPercentage").val();
        var rewards = [];
        var i = 0;
        var currenciesUsed = [];
        var errRepeatCurrency = false;
        $(".js-reward").each(function(index, el) {
            var amount = $(el).find('.reward-amount').val();
            var currency = $(el).find('.reward-currency').val();
            if (amount && currency) {
                if($.inArray(currency, currenciesUsed) !== -1) {
                    errRepeatCurrency = true;
                    return false;                    
                } else {
                    rewards[i] = {};
                    rewards[i].amount = amount;
                    rewards[i].currency = currency;
                    currenciesUsed[currenciesUsed.length] = currency;
                    i++;
                }
            }
        });
        if(!inputRewardPercentage) {
            toastr.error('Please enter reward percentage!');
        } else if (errRepeatCurrency) {
            toastr.error("Can't use the same currency twice!");
        } else if (Object.keys(rewards).length == 0) {
            toastr.error('Please enter atleast one reward!');
        } else if(!contentId || !contentType) {
            toastr.error('There was some error!');
        } else {
            var req = {};
            var res;
            req.courseId = courseId;
            req.subjectId = subjectId;
            req.contentId = contentId;
            req.contentType = contentType;
            req.percent_block = inputRewardPercentage;
            req.rewards = rewards;
            req.position = rewardPosition;
            req.action ="save-subject-reward";
            $.ajax({
                'type'  : 'post',
                'url'   : ApiEndPoint,
                'data'  : JSON.stringify(req)
            }).done(function (res) {
                res =  $.parseJSON(res);
                if(res.status == 1) {
                    toastr.success(res.message);
                    $("#modalAddEditReward").modal("hide");
                    var html='<div class="dib-left"><span class="r-per">'+inputRewardPercentage+'</span>% <br /> Reward: <br /><dl class="mb-10">';
                    for (var k = 0; k < rewards.length; k++) {
                        html+= '<dd><span class="reward-ra">'+rewards[k].amount+'</span> <span class="reward-rc">'+rewards[k].currency+'</span></dd>';
                    }
                    html+= '</dl><button class="btn btn-xs green btn-edit-reward"><i class="fa fa-edit"></i> Edit</button></div>';
                    $('#listExams tr[data-id="'+contentId+'"][data-type="'+contentType+'"]').find('td[data-position="'+rewardPosition+'"]').html(html);
                }
                else
                    toastr.error(res.message);
            });
        }
    });
});
</script>