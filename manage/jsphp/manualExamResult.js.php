<script type="text/javascript">
var sortOrder = [];
var examStart = new Date();
var examEnd = new Date('2099/12/31');
var data;
var mExam = [];
function fetchManualExam() {
	var req = {};
	var res;
	req.action = 'get-manual-exam-details';
	req.examId = examId;
	req.subjectId = subjectId;
	req.courseId = courseId;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else {
			data = res;
			//console.log(data);
			setManualExam();
		}
	});
}
function setManualExam() {
	var exam1 = data.exam;
	var rank = 1;
	if (exam1.type == "quick") {
		mExam['maxMarks']	= parseFloat(exam1.questions['marks']);
	} else {
		mExam['maxMarks']	= parseFloat(exam1.maxMarks);
	}
	for (var i = 0; i < exam1.students.length; i++) {
		mExam[i] = [];
		mExam[i]['studentId']	= exam1.students[i][0];
		mExam[i]['studentName']	= exam1.students[i][1];
		if (exam1.type == "quick") {
			mExam[i]['marks']		= parseFloat(exam1.students[i][2]['marks']);
			mExam[i]['questionId']	= parseFloat(exam1.students[i][2]['question_id']);
		} else {
			mExam[i]['marks']		= parseFloat(exam1.students[i][2]);
		}
		mExam[i]['percent']		= parseFloat(((mExam[i]['marks']/mExam['maxMarks'])*100).toFixed(2));
		mExam[i]['rank']		= 1;
		if (exam1.type == "quick") {
			mExam[i]['qMarks']	= exam1.students[i][2];
		} else {
			mExam[i]['qMarks']	= exam1.students[i][3];
		}
	};
	var temp = [];
	for (var i = 0; i < mExam.length; i++) {
		for (var j = 0; j < mExam.length; j++) {
			if (mExam[i]['marks']>mExam[j]['marks']) {
				temp = mExam[i];
				mExam[i] = mExam[j];
				mExam[j] = temp;
				temp = [];
			}
		};
	};
	var totalScore = 0;
	var totalPercent = 0;
	for (var i = 0; i < mExam.length; i++) {
		mExam[i]['rank'] = rank;
		rank++;
		//mExam[i]['percentile'] = ((i+1)/mExam.length).toFixed(2);
		//mExam[i]['percentile'] = parseFloat(((((mExam.length-(i+1))/mExam.length))*100).toFixed(2));
		mExam[i]['percentile'] = parseFloat(((((mExam.length-i)/mExam.length))*100).toFixed(2));
		if (i>0) {
			if (mExam[i]['marks']==mExam[i-1]['marks']) {
				//rank--; // line for ascending ranks
				mExam[i]['rank'] = mExam[i-1]['rank'];
				mExam[i]['percentile'] = mExam[i-1]['percentile'];
			};
		};
		totalScore+=mExam[i]['marks'];
		totalPercent+=mExam[i]['percent'];
	};
	mExam.totalScore = totalScore;
	mExam.avgScore = round2(totalScore/mExam.length);
	mExam.avgPercent = round2(totalPercent/mExam.length);

	fillManualExam();
}
function fillManualExam() {
	//console.log(data);
	var exam = data.exam;
	var questionsCount = data.questionsCount;
	var questions = exam.questions;
	var filler = '';
	$('#exam').html('<div class="portlet box green">'+
					'<div class="portlet-title">'+
						'<div class="caption">'+
                            '<i class=" icon-layers font-white"></i>'+
                            '<span class="caption-subject bold font-white uppercase">Exam Marks Table ['+((exam.type=='quick')?'Quick':'Deep')+' Analytics]</span>'+
                        '</div>'+
					'</div>'+
					'<div class="portlet-body no-space">'+
						'<table class="table table-hover table-bordered no-margin '+((exam.type == "deep")?'table-deep':'')+'">'+
							'<thead>'+
								'<tr>'+
									'<th class="text-center" width="10%">'+terminologies["student_single"]+' Id</th>'+
									'<th>'+terminologies["student_single"]+' Name</th>'+
									'<th class="text-center" width="15%">Total Marks (out of '+mExam.maxMarks+')</th>'+
									'<th class="text-center" width="15%">Percent</th>'+
									'<th class="text-center" width="15%">Rank</th>'+
									'<th class="text-center" width="15%">Percentile</th>'+
								'</tr>'+
							'</thead>'+
							'<tbody></tbody>'+
						'</table>'+
					'</div>'+
				'</div>');
	for (var i = 0; i < mExam.length; i++) {
		$('#exam tbody').append('<tr class="table-row" data-student="'+i+'" data-max="'+mExam.maxMarks+'">'+
									'<td class="text-center">'+mExam[i]['studentId']+'</td>'+
									'<td>'+mExam[i]['studentName']+'</td>'+
									'<td class="text-center">'+round2(mExam[i]['marks'])+((exam.type=='quick')?' <button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button>':'')+'</td>'+
									'<td class="text-center">'+mExam[i]['percent']+'</td>'+
									'<td class="text-center">'+mExam[i]['rank']+'</td>'+
									'<td class="text-center">'+mExam[i]['percentile']+'</td>'+
								'</tr>');
	};
	drawManualExam();
}
function drawManualExam() {
	var maxScore = mExam[0]['marks'];
	var minScore = round2(mExam[mExam.length-1]['marks']);
	var avgScore = mExam.avgScore;
	var noOfStudents = mExam.length;
	var ascMExam = [];
	var j = mExam.length-1;
	for (var i = 0; i < mExam.length; i++) {
		ascMExam[j] = mExam[i];
		j--;
	};
	if (noOfStudents%2 == 0) {
		var median2 = (noOfStudents)/2;
		var median1 = median2-1;
		var medianScore = round2((ascMExam[median1]['marks']+ascMExam[median2]['marks'])/2);
	} else {
		var median = (noOfStudents-1)/2;
		var medianScore = round2(ascMExam[median]['marks']);
	}
	var scoresBarGraph = AmCharts.makeChart("scoresBarGraph", {
            "theme": "light",
            "type": "serial",
            "startDuration": 2,

            "fontFamily": 'Open Sans',
            
            "color":    '#888',

            "dataProvider": [{
                "country": "Max Score",
                "visits": parseFloat(maxScore),
                "color": "#FF6600"
            }, {
                "country": "Avg Score",
                "visits": parseFloat(avgScore),
                "color": "#04D215"
            }, {
                "country": "Median Score",
                "visits": parseFloat(medianScore),
                "color": "#2A0CD0"
            }, {
                "country": "Min Score",
                "visits": parseFloat(minScore),
                "color": "#754DEB"
            }],
            "valueAxes": [{
                "position": "left",
                "axisAlpha": 0,
                "gridAlpha": 0
            }],
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "colorField": "color",
                "fillAlphas": 0.85,
                "lineAlpha": 0.1,
                "type": "column",
                "topRadius": 1,
                "valueField": "visits"
            }],
            "depth3D": 40,
            "angle": 30,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0

            },
            "exportConfig": {
                "menuTop": "20px",
                "menuRight": "20px",
                "menuItems": [{
                    "icon": '/lib/3/images/export.png',
                    "format": 'png'
                }]
            }
        }, 0);

	var maxPercent = mExam[0]['percent'];
	var minPercent = mExam[mExam.length-1]['percent'];
	var avgPercent = mExam.avgPercent;
	if (noOfStudents%2 == 0) {
		var median2 = (noOfStudents)/2;
		var median1 = median2-1;
		var medianPercent = (ascMExam[median1]['percent']+ascMExam[median2]['percent'])/2;
	} else {
		var median = (noOfStudents-1)/2;
		var medianPercent = ascMExam[median]['percent'];
	}
	var percentBarGraph = AmCharts.makeChart("percentBarGraph", {
            "theme": "light",
            "type": "serial",
            "startDuration": 2,

            "fontFamily": 'Open Sans',
            
            "color":    '#888',

            "dataProvider": [{
                "country": "Max Percent",
                "visits": parseFloat(maxPercent),
                "color": "#FF6600"
            }, {
                "country": "Avg Percent",
                "visits": parseFloat(avgPercent),
                "color": "#04D215"
            }, {
                "country": "Median Percent",
                "visits": parseFloat(medianPercent),
                "color": "#2A0CD0"
            }, {
                "country": "Min Percent",
                "visits": parseFloat(minPercent),
                "color": "#754DEB"
            }],
            "valueAxes": [{
                "position": "left",
                "axisAlpha": 0,
                "gridAlpha": 0
            }],
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "colorField": "color",
                "fillAlphas": 0.85,
                "lineAlpha": 0.1,
                "type": "column",
                "topRadius": 1,
                "valueField": "visits"
            }],
            "depth3D": 40,
            "angle": 30,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0

            },
            "exportConfig": {
                "menuTop": "20px",
                "menuRight": "20px",
                "menuItems": [{
                    "icon": '/lib/3/images/export.png',
                    "format": 'png'
                }]
            }
        }, 0);

	//console.log(data.total[0]['total']);
	var totalScore=mExam.maxMarks;
	var difference=totalScore-maxScore;
	var blocksDiff=(parseFloat(totalScore)-parseFloat(difference+minScore))/10;
	//console.log(blocksDiff);
	//var marksDis=[];
	var students=[];
	var markPer=[];
	var newlowest=(parseFloat(minScore)+parseFloat(difference)).toFixed(1);
	for(var i=1;i<11;i++)
	{	//marksDis[i-1]=(parseFloat(newlowest)+parseFloat(blocksDiff*i)).toFixed(1);
		students[i-1]=0;
		//checking
		var temp=(parseFloat(newlowest)+parseFloat(blocksDiff*i)).toFixed(1);
		markPer[i-1]=Math.round((temp/totalScore)*100)+'%';
		//markPer[i-1]=markPer[i-1]+'%';
	}
	for(var i=markPer.length-1;i>0;i--)
	{
		markPer[i]= markPer[i-1]+' - '+markPer[i];
	}
	
	markPer[0]=Math.round(newlowest)+'% - '+markPer[0];
	//console.log(markPer);
	$.each( mExam, function( key, value ) {
		var newscore=(parseFloat(value['marks'])+parseFloat(difference)).toFixed(1);
		var index=Math.round(((newscore-newlowest)/blocksDiff));
		if(index>0)
		{	index=index-1;
		}else{
			index=0;
		}
		students[index]=students[index]+1;
	});
	//console.log(students);
	$('#percentDistGraph').highcharts({
		chart: {
			zoomType: 'xy'
		},
		title: {
			text: 'Percentage distribution of all '+terminologies["student_plural"].toLowerCase()
		},
		subtitle: {
			text: 'This graph shows percentage w.r.t to highest scorer'
		},
        xAxis: [
		{
			title: {
				text: 'Percentage Score',
				style: {
					color: Highcharts.getOptions().colors[1]
				}
			},
			categories: markPer,
			crosshair: true
		}],
		yAxis: [{ // Primary yAxis
			labels: {
				format: '{value}',
				style: {
					color: Highcharts.getOptions().colors[1]
				}
			},
			title: {
				text: 'No. of '+terminologies["student_plural"],
				style: {
					color: Highcharts.getOptions().colors[1]
				}
			}
		}, { // Secondary yAxis
			title: {
				text: terminologies["student_plural"],
				style: {
					color: Highcharts.getOptions().colors[0]
				}
			},
			labels: {
				format: '{value}',
				style: {
					color: Highcharts.getOptions().colors[0]
				}
			},
			opposite: true
		}],
		tooltip: {
			shared: true
		},
		legend: {
			layout: 'vertical',
			align: 'left',
			x: 120,
			verticalAlign: 'top',
			y: 100,
			floating: true,
			backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
		},
		series: [{
			name: terminologies["student_plural"],
			type: 'column',
			yAxis: 1,
			data: students,
			tooltip: {
				valueSuffix: ''
			}

		}, {
			name: 'No. of '+terminologies["student_plural"],
			type: 'spline',
			yAxis: 1,
			data: students,
			tooltip: {
				valueSuffix: ''
			}
		}]
    });
}
function drawInnerManualExam(studentIndex) {
	var exam = mExam[studentIndex];
	var dataGraph = [];
	var dataMin = [];
	var dataMax = [];
	var categories = [];
	var qMarks = exam['qMarks'];
	var qName = "";
	for (var i = 0; i < qMarks.length; i++) {
		qname = 'Q'+(i+1);
		categories[i] = qname;
		dataMin[i] = parseFloat(qMarks[i]['marks']);
		dataMax[i] = round2(data.exam.questions[i]['marks']-parseFloat(qMarks[i]['marks']));
	};
	dataGraph.push({name: 'Marks received', data:dataMin});
	dataGraph.push({name: 'Marks lost', data:dataMax});
	//console.log(dataGraph);
	$('#innerAnalytics').highcharts({
		chart: {
			type: 'column'
		},
		title: {
			text: exam['studentName']
		},
		subtitle: {
			text: terminologies["student_single"]+' ID : '+exam['studentId']
		},
		xAxis: {
			categories: categories
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Marks'
			},
			stackLabels: {
				enabled: true,
				style: {
					fontWeight: 'bold',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
				}
			}
		},
		legend: {
			align: 'right',
			x: -30,
			verticalAlign: 'top',
			y: 25,
			floating: true,
			backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
			borderColor: '#CCC',
			borderWidth: 1,
			shadow: false
		},
		tooltip: {
			headerFormat: '<b>{point.x}</b><br/>',
			pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
		},
		plotOptions: {
			column: {
				stacking: 'normal',
				dataLabels: {
					enabled: true,
					color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
					style: {
						textShadow: '0 0 3px black'
					}
				}
			}
		},
		series: dataGraph
	});
}

function fillExamDeepTable(studentIndex) {
	var marks = mExam[studentIndex]['qMarks'];
	//console.log(students[studentIndex]);
	//console.log(marks);
	$('#questionMarks').html('<table class="table table-bordered">'+
								'<thead>'+
									'<tr><th colspan="3">'+terminologies["student_single"]+' ID : '+mExam[studentIndex]['studentId']+'</th></tr>'+
									'<tr><th colspan="3">'+terminologies["student_single"]+' Name : '+mExam[studentIndex]['studentName']+'</th></tr>'+
									'<tr><th width="40%">Question</th><th width="30%">Marks</th><th width="30%">Max Marks</th></tr>'+
								'</thead>'+
								'<tbody></tbody></table>');
	for (var i = 0; i < marks.length; i++) {
		$('#questionMarks tbody').append('<tr data-student="'+studentIndex+'" data-marksi="'+i+'" data-marks="'+marks[i]['id']+'" data-max="'+data.exam.questions[i]['marks']+'">'+
											'<td>Q'+(i+1)+'</td>'+
											'<td>'+marks[i]['marks']+' <button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button></td>'+
											'<td>'+data.exam.questions[i]['marks']+'</td>'+
										'</tr>');
	};
	$('#marksModal').modal('show');
	setTimeout(function(){drawInnerManualExam(studentIndex);}, 500);
}
$(document).ready(function(){

	$('#exam').on('click', '.table-deep .table-row', function(){
		var studentIndex = $(this).attr('data-student');
		fillExamDeepTable(studentIndex);
		//drawInnerManualExam(studentIndex);
	});
	$('#exam,#questionMarks').on('click', '.table tr .btn-edit', function(){
		var marks = $(this).closest('td').text();
		$(this).closest('td').html('<div class="input-group">'+
										'<input type="text" class="form-control" data-oldval="'+marks+'" value="'+$.trim(marks)+'">'+
										'<span class="input-group-btn">'+
											'<button class="btn btn-success btn-save" type="button"><i class="fa fa-save"></i></button>'+
											'<button class="btn btn-danger1 btn-cancel" type="button"><i class="fa fa-times"></i></button>'+
										'</span>'+
									'</div>');
	});
	$('#exam,#questionMarks').on('click', '.table tr .btn-cancel', function(){
		var marks = $(this).closest('td').find('.form-control').attr('data-oldval');
		$(this).closest('td').html(marks+'<button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button>');
	});
	$('#exam,#questionMarks').on('click', '.table tr .btn-save', function(e){
		e.preventDefault();
		var thiz = $(this);
		var marks = parseFloat($(this).closest('td').find('.form-control').val());
		if (data.exam.type=="quick") {
			var studentIndex = $(this).closest('tr').attr('data-student');
			var marksId = mExam[studentIndex]['qMarks']['id'];
		} else {
			var marksId = $(this).closest('tr').attr('data-marks');
		}
		var maxMarks = parseFloat($(this).closest('tr').attr('data-max'));
		if (marks=='') {
			toastr.error('Please enter some marks');
		} else if (marks>maxMarks) {
			//console.log(maxMarks);
			toastr.error('Please enter valid marks');
		} else {
			var req = {};
			var res;
			req.action = 'save-manual-exam-marks';
			req.marksId = marksId;
			req.examId = examId;
			req.marks = marks;
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndPoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					toastr.success("Successfully saved marks!");
					//window.location = window.location;
					if (data.exam.type=="deep") {
						var studentIndex = thiz.closest('tr').attr('data-student');
						var marksIndex = thiz.closest('tr').attr('data-marksi');
						mExam[studentIndex]['qMarks'][marksIndex]['marks'] = marks;
						drawInnerManualExam(studentIndex);
						fillExamDeepTable(studentIndex);
						//$('#exam').find('.table-row[data-student="'+studentIndex+'"]').click();
					} else {
						fetchManualExam();
					}
				}
			});
		}
		return false;
	});
	$('#marksModal').on('hidden.bs.modal', function () {
		$('#innerAnalytics').html('');
		$('#questionMarks').html('');
	});
	// BAR CHART
 	// Age categories
 	/*var categories = ["47% - 52%", "52% - 58%", "58% - 63%", "63% - 68%", "68% - 74%", "74% - 79%", "79% - 84%", "84% - 89%", "89% - 95%", "95% - 100%"];
    var students = [3, 10, 7, 5, 9, 2, 12, 2, 4, 1];
   
    $('#percentDistGraph').highcharts({
		chart: {
			zoomType: 'xy'
		},
		title: {
			text: 'Percentage distribution of all students'
		},
		subtitle: {
			text: 'This graph shows percentage w.r.t to highest scorer'
		},
        xAxis: [
		{
			title: {
				text: 'Percentage Score',
				style: {
					color: Highcharts.getOptions().colors[1]
				}
			},
			categories: categories,
			crosshair: true
		}],
		yAxis: [{ // Primary yAxis
			labels: {
				format: '{value}',
				style: {
					color: Highcharts.getOptions().colors[1]
				}
			},
			title: {
				text: 'No. of Students',
				style: {
					color: Highcharts.getOptions().colors[1]
				}
			}
		}, { // Secondary yAxis
			title: {
				text: 'Students',
				style: {
					color: Highcharts.getOptions().colors[0]
				}
			},
			labels: {
				format: '{value}',
				style: {
					color: Highcharts.getOptions().colors[0]
				}
			},
			opposite: true
		}],
		tooltip: {
			shared: true
		},
		legend: {
			layout: 'vertical',
			align: 'left',
			x: 120,
			verticalAlign: 'top',
			y: 100,
			floating: true,
			backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
		},
		series: [{
			name: 'Students',
			type: 'column',
			yAxis: 1,
			data: students,
			tooltip: {
				valueSuffix: ''
			}

		}, {
			name: 'No. of Students',
			type: 'spline',
			yAxis: 1,
			data: students,
			tooltip: {
				valueSuffix: ''
			}
		}]
    });

	
	$('#exam').on('click', '.table-deep .table-row', function(){
		var studentIndex = $(this).attr('data-student');
		var marks = [{"id":"182","marks":"56","question_id":"14"},{"id":"183","marks":"84","question_id":"14"},{"id":"184","marks":"65","question_id":"14"}];
		//console.log(students[studentIndex]);
		//console.log(marks);
		$('#questionMarks').html('<table class="table table-bordered">'+
									'<thead>'+
										'<tr><th colspan="3">Student ID : 1</th></tr>'+
										'<tr><th colspan="3">Student Name : Magic</th></tr>'+
										'<tr><th width="40%">Question</th><th width="30%">Marks</th><th width="30%">Max Marks</th></tr>'+
									'</thead>'+
									'<tbody></tbody></table>');
		for (var i = 0; i < marks.length; i++) {
			$('#questionMarks tbody').append('<tr data-marks="1" data-max="10">'+
												'<td>Q'+(i+1)+'</td>'+
												'<td>'+marks[i]['marks']+'<button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button></td>'+
												'<td>10</td>'+
											'</tr>');
		};
		$('#marksModal').modal('show');
		setTimeout(function(){drawInnerManualExam(studentIndex);}, 500);
		//drawInnerManualExam(studentIndex);
	});

	function drawInnerManualExam(studentIndex) {
		//var exam = mExam[studentIndex];
		var dataGraph = [];
		var dataMin = [];
		var dataMax = [];
		var categories = [];
		var qMarks = [{"id":"182","marks":"56","question_id":"14"},{"id":"183","marks":"84","question_id":"14"},{"id":"184","marks":"65","question_id":"14"}];
		var qName = "";
		for (var i = 0; i < qMarks.length; i++) {
			qname = 'Q'+(i+1);
			categories[i] = qname;
			dataMin[i] = parseInt(qMarks[i]['marks']);
			dataMax[i] = 100-parseInt(qMarks[i]['marks']);
		};
		dataGraph.push({name: 'Marks received', data:dataMin});
		dataGraph.push({name: 'Marks lost', data:dataMax});
		//console.log(dataGraph);
		$('#innerAnalytics').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'John Gotti'
	        },
	        subtitle: {
	            text: 'Student ID : 1'
	        },
	        xAxis: {
	            categories: categories
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Marks'
	            },
	            stackLabels: {
	                enabled: true,
	                style: {
	                    fontWeight: 'bold',
	                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
	                }
	            }
	        },
	        legend: {
	            align: 'right',
	            x: -30,
	            verticalAlign: 'top',
	            y: 25,
	            floating: true,
	            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
	            borderColor: '#CCC',
	            borderWidth: 1,
	            shadow: false
	        },
	        tooltip: {
	            headerFormat: '<b>{point.x}</b><br/>',
	            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	        },
	        plotOptions: {
	            column: {
	                stacking: 'normal',
	                dataLabels: {
	                    enabled: true,
	                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
	                    style: {
	                        textShadow: '0 0 3px black'
	                    }
	                }
	            }
	        },
	        series: dataGraph
	    });
	}

	$('#exam,#questionMarks').on('click', '.table tr .btn-edit', function(){
		var marks = $(this).closest('td').text();
		$(this).closest('td').html('<div class="input-group">'+
										'<input type="text" class="form-control" data-oldval="'+marks+'" value="'+marks+'">'+
										'<span class="input-group-btn">'+
											'<button class="btn btn-success btn-save" type="button"><i class="fa fa-save"></i></button>'+
											'<button class="btn btn-danger btn-cancel" type="button"><i class="fa fa-times"></i></button>'+
										'</span>'+
									'</div>');
	});
	$('#exam,#questionMarks').on('click', '.table tr .btn-cancel', function(){
		var marks = $(this).closest('td').find('.form-control').attr('data-oldval');
		$(this).closest('td').html(marks+'<button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button>');
	});
	$('#exam,#questionMarks').on('click', '.table tr .btn-save', function(){
		var marks = parseInt($(this).closest('td').find('.form-control').val());
		if (data.exam.type=="quick") {
			var studentIndex = $(this).closest('tr').attr('data-student');
			var marksId = mExam[studentIndex]['qMarks']['id'];
		} else {
			var marksId = $(this).closest('tr').attr('data-marks');
		}
		var maxMarks = parseInt($(this).closest('tr').attr('data-max'));
		if (marks=='') {
			toastr.error('Please enter some marks');
		} else if (marks>maxMarks) {
			//console.log(maxMarks);
			toastr.error('Please enter valid marks');
		} else {
			var req = {};
			var res;
			req.action = 'save-manual-exam-marks';
			req.marksId = marksId;
			req.examId = examId;
			req.marks = marks;
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndPoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else
					window.location = window.location;
			});
		}
	});

	var scoresBarGraph = AmCharts.makeChart("scoresBarGraph", {
            "theme": "light",
            "type": "serial",
            "startDuration": 2,

            "fontFamily": 'Open Sans',
            
            "color":    '#888',

            "dataProvider": [{
                "country": "China",
                "visits": 76,
                "color": "#FF6600"
            }, {
                "country": "India",
                "visits": 45,
                "color": "#04D215"
            }, {
                "country": "Russia",
                "visits": 54,
                "color": "#2A0CD0"
            }, {
                "country": "Brazil",
                "visits": 23,
                "color": "#754DEB"
            }],
            "valueAxes": [{
                "position": "left",
                "axisAlpha": 0,
                "gridAlpha": 0
            }],
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "colorField": "color",
                "fillAlphas": 0.85,
                "lineAlpha": 0.1,
                "type": "column",
                "topRadius": 1,
                "valueField": "visits"
            }],
            "depth3D": 40,
            "angle": 30,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0

            },
            "exportConfig": {
                "menuTop": "20px",
                "menuRight": "20px",
                "menuItems": [{
                    "icon": '/lib/3/images/export.png',
                    "format": 'png'
                }]
            }
        }, 0);
	
	var percentBarGraph = AmCharts.makeChart("percentBarGraph", {
            "theme": "light",
            "type": "serial",
            "startDuration": 2,

            "fontFamily": 'Open Sans',
            
            "color":    '#888',

            "dataProvider": [{
                "country": "China",
                "visits": 76,
                "color": "#FF6600"
            }, {
                "country": "India",
                "visits": 45,
                "color": "#04D215"
            }, {
                "country": "Russia",
                "visits": 54,
                "color": "#2A0CD0"
            }, {
                "country": "Brazil",
                "visits": 23,
                "color": "#754DEB"
            }],
            "valueAxes": [{
                "position": "left",
                "axisAlpha": 0,
                "gridAlpha": 0
            }],
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "colorField": "color",
                "fillAlphas": 0.85,
                "lineAlpha": 0.1,
                "type": "column",
                "topRadius": 1,
                "valueField": "visits"
            }],
            "depth3D": 40,
            "angle": 30,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0

            },
            "exportConfig": {
                "menuTop": "20px",
                "menuRight": "20px",
                "menuItems": [{
                    "icon": '/lib/3/images/export.png',
                    "format": 'png'
                }]
            }
        }, 0);*/
});
</script>