<script type="text/javascript">
//function to fetch student list who have attempted the exam
function fetchStudentList() {
    var req = {};
    var res;
    req.action = 'get-student-list';
    req.submissionId = examId;
    $.ajax({
        'type'  :   'post',
        'url'   :   ApiEndPoint,
        'data'  :   JSON.stringify(req)
    }).done(function(res) {
        res = $.parseJSON(res);
        if(res.status == 0)
            toastr.error(res.message);
        else {
            total = res.students.length;
            fetchExamReport();
            fetchAllStudentGraph();
            fetchTopperComparison();
            fillStudentList(res);
        }
    });
}
//function to fill student list table
function fillStudentList(data) {
    //console.log(data);
    var html = '';
    var highest = [];
    var lowest = [];
    //detecting the highest and lowest scorers
    if(data.toppers.length > 0) {
        var max = data.toppers[0].score;
        var min = data.toppers[0].score;

        for(var i = 0; i < data.toppers.length; i++) {
            if(data.toppers[i].score == max)
                highest.push(data.toppers[i].studentId);
            else if(highest.indexOf(data.toppers[i].studentId) == -1)
                lowest.push(data.toppers[i].studentId);
        }
    }
    if (data.students.length>0) {
        for(var i = 0; i < data.students.length; i++) {
            if(data.students[i].details.max == null)
                data.students[i].details.max = '--';
            if(data.students[i].details.avg == null)
                data.students[i].details.avg = '--';
            else
                data.students[i].details.avg=parseFloat(data.students[i].details.avg).toFixed(2);
            
            var lastscore="";
            if(data.students[i].lastScore == null  || data.students[i].lastScore == undefined)
            {
                lastscore='--';
            }               
            else{
                lastscore= data.students[i].lastScore.score;
            }
            html += '<tr class="table-row" data-student="0" data-max="100">'+
                    '<td class="text-center">' + data.students[i].userId + '</td>'+
                    '<td>';
            if(data.students[i].details.total>0)
                html += '<a href="'+sitepathManageSubmissions+examId+'/result/student/' + data.students[i].userId + '/attempt/'+data.students[i].details.attemptId+'">';
             html += data.students[i].name;
            if(highest.indexOf(data.students[i].userId) != -1)
                html += '&emsp;<i class="fa fa-thumbs-o-up text-success" title="Highest Scorer"></i>';
            else if(lowest.indexOf(data.students[i].userId) != -1)
                html += '&emsp;<i class="fa fa-thumbs-o-down text-danger" title="Lowest Scorer"></i>';
            if(data.students[i].details.total>0)
                html += '</a>';
            html += '</td>'+
                    '<td class="text-center"><a href="mailto:' + data.students[i].email + '">' + data.students[i].email + '</a></td>'+
                    '<td class="text-center">' + data.students[i].details.total + '</td>'+
                    '<td class="text-center">' + lastscore + '</td>'+
                    '<td class="text-center">' + data.students[i].details.max + '</td>'+
                    '<td class="text-center">' + data.students[i].details.avg + '</td>'+
                '</tr>';
        }
        $('#studentList tbody').append(html);
    }
    $('#studentList').show();
    // datatable call
}
//function to get exam report
function fetchExamReport() {
    var req = {};
    var res;
    req.action = 'get-exam-report';
    req.submissionId = examId;
    $.ajax({
        'type'  :   'post',
        'url'   :   ApiEndPoint,
        'data'  :   JSON.stringify(req)
    }).done(function(res) {
        res = $.parseJSON(res);
        if(res.status == 0)
            toastr.error(res.message);
        else {
            initAttemptsGraph(res);
        }
    });
}
//function to initiate all graphs
function initAttemptsGraph(data) {
    var attemptsPieGraph = AmCharts.makeChart("attemptsPieGraph", {
            "type": "pie",
            "theme": "light",

            "fontFamily": 'Open Sans',
            
            "color":    '#888',

            "dataProvider": [{
                "attemptStatus": "Attempted",
                "value": parseInt(data.attempted)
            }, {
                "attemptStatus": "Unattempted",
                "value": parseInt(total - data.attempted)
            }],
            "valueField": "value",
            "titleField": "attemptStatus",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "exportConfig": {
                menuItems: [{
                    icon: '/lib/3/images/export.png',
                    format: 'png'
                }]
            }
        });
}
function fetchAllStudentGraph() {
    var req = {};
    var res;
    req.action = 'get-allStudentNormalization-graph';
    req.submissionId = examId;
    $.ajax({
        'type'  :   'post',
        'url'   :   ApiEndPoint,
        'data'  :   JSON.stringify(req)
    }).done(function(res) {
        res = $.parseJSON(res);
        if(res.status == 0)
            toastr.error(res.message);
        else{
            initAllStudentGraph(res);
        }
            
    });
}
//function to initiate attempt graph
function initAllStudentGraph(data) {
    //console.log(data.total[0]['total']);
    //console.log(data);
    var totalScore=parseFloat(data.total[0]['total']);
    var differnece=totalScore-parseFloat(data.highest);
    var blocksDiff=(totalScore-parseFloat(differnece+parseFloat(data.lowest)))/10;
    //var marksDis=[];
    var students=[];
    var markPer=[];
    var newlowest=(parseFloat(data.lowest)+parseFloat(differnece)).toFixed(1);
    for(var i=1;i<11;i++)
    {   //marksDis[i-1]=(parseFloat(newlowest)+parseFloat(blocksDiff*i)).toFixed(1);
        students[i-1]=0;
        //checking
        var temp=(parseFloat(newlowest)+parseFloat(blocksDiff*i)).toFixed(1);
        markPer[i-1]=Math.round((temp/totalScore)*100)+'%';
        //markPer[i-1]=markPer[i-1]+'%';
    }
    for(var i=markPer.length-1;i>0;i--)
    {
        markPer[i]= markPer[i-1]+' - '+markPer[i];
    }
    
    markPer[0]=Math.round(newlowest)+'% - '+markPer[0];
    //console.log(markPer);
    $.each( data.graph, function( key, value ) {
        var newscore=(parseFloat(value['score'])+parseFloat(differnece)).toFixed(1);
        var index=Math.round(((newscore-newlowest)/blocksDiff));
        if(index>0)
        {   index=index-1;
        }else{
            index=0;
        }
        students[index]=students[index]+1;
    });

    $('#percentDistGraph').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Percentage distribution of all '+terminologies["student_plural"].toLowerCase()
        },
        subtitle: {
            text: 'This graph shows percentage w.r.t to highest scorer'
        },
        xAxis: [
        {
            title: {
                text: 'Percentage Score',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            categories: markPer,
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'No. of '+terminologies["student_plural"],
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: terminologies["student_plural"],
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 120,
            verticalAlign: 'top',
            y: 100,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: terminologies["student_plural"],
            type: 'column',
            yAxis: 1,
            data: students,
            tooltip: {
                valueSuffix: ''
            }

        }, {
            name: 'No. of '+terminologies["student_plural"],
            type: 'spline',
            yAxis: 1,
            data: students,
            tooltip: {
                valueSuffix: ''
            }
        }]
    });
    //$('.percAllStudents').show();
}
//function to get topper comparison
function fetchTopperComparison() {
    var req = {};
    var res;
    req.action = 'get-topper-of-examAll';
    req.submissionId = examId;
    $.ajax({
        'type'  :   'post',
        'url'   :   ApiEndPoint,
        'data'  :   JSON.stringify(req)
    }).done(function(res) {
        res = $.parseJSON(res);
        if(res.status == 0)
            toastr.error(res.message);
        else
            fillTopperComparison(res);
    });
}

//function to fill the compare graphs of topper
function fillTopperComparison(data) {
    //console.log(data);
    var scoresBarGraph = AmCharts.makeChart("scoresBarGraph", {
            "theme": "light",
            "type": "serial",
            "startDuration": 2,

            "fontFamily": 'Open Sans',
            
            "color":    '#888',

            "dataProvider": [/*{
                "country": "You",
                "visits": 76,
                "color": "#FF6600"
            }, */{
                "country": "Topper",
                "visits": data.avgMax.max,
                "color": "#04D215"
            }, {
                "country": "Median",
                "visits": data.median,
                "color": "#2A0CD0"
            }, {
                "country": "Average",
                "visits": data.avgMax.average,
                "color": "#754DEB"
            }],
            "valueAxes": [{
                "position": "left",
                "axisAlpha": 0,
                "gridAlpha": 0
            }],
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "colorField": "color",
                "fillAlphas": 0.85,
                "lineAlpha": 0.1,
                "type": "column",
                "topRadius": 1,
                "valueField": "visits"
            }],
            "depth3D": 40,
            "angle": 30,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0

            },
            "exportConfig": {
                "menuTop": "20px",
                "menuRight": "20px",
                "menuItems": [{
                    "icon": '/lib/3/images/export.png',
                    "format": 'png'
                }]
            }
        }, 0);
    
    var percentBarGraph = AmCharts.makeChart("percentBarGraph", {
            "theme": "light",
            "type": "serial",
            "startDuration": 2,

            "fontFamily": 'Open Sans',
            
            "color":    '#888',

            "dataProvider": [/*{
                "country": "You",
                "visits": 76,
                "color": "#FF6600"
            }, */{
                "country": "Topper",
                "visits": data.pavgMax.max,
                "color": "#04D215"
            }, {
                "country": "Median",
                "visits": data.pmedian,
                "color": "#2A0CD0"
            }, {
                "country": "Average",
                "visits": data.pavgMax.average,
                "color": "#754DEB"
            }],
            "valueAxes": [{
                "position": "left",
                "axisAlpha": 0,
                "gridAlpha": 0
            }],
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "colorField": "color",
                "fillAlphas": 0.85,
                "lineAlpha": 0.1,
                "type": "column",
                "topRadius": 1,
                "valueField": "visits"
            }],
            "depth3D": 40,
            "angle": 30,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0

            },
            "exportConfig": {
                "menuTop": "20px",
                "menuRight": "20px",
                "menuItems": [{
                    "icon": '/lib/3/images/export.png',
                    "format": 'png'
                }]
            }
        }, 0);
}
$(document).ready(function(){
	
});
</script>