<script type="text/javascript">
var pfId=0;

$('.js-remove').click(function(){
    if ($('#userProfilePic').data('Jcrop')) {
        var JcropAPI = $('#userProfilePic').data('Jcrop');
        JcropAPI.destroy();
        var originalImage = $('#userProfilePic').attr('old-image');
        $('#userProfilePic').prop('src', originalImage);
    } else {
        var req = {};
        req.action      = 'remove-profile-image';
        var res;
        ajaxRequest({
            'type'  :   'post',
            'url'   :   ApiEndPoint,
            'data'  :   JSON.stringify(req),
            success:function(res){
                res = $.parseJSON(res);
                if(res.status == 0)
                    toastr.error(res.message);
                else {
                    $('#userProfilePic').prop('src', res.profilePic);
                    $('#userProfilePic').prop('old-image', res.profilePic);
                }
            }
        });
    }
});

var _URL = window.URL || window.webkitURL;
var cropCoords,
file,
originalImageWidth, originalImageHeight,
uploadSize = 300,
previewSize = 500;

/*$("#fileProfilePic").on("change", function(){
    file = this.files[0];
    if (!!file) {
        $('.js-upload').removeClass('hide');
        var img;
        img = new Image();
        img.onload = function () {
            //console.log(this.width + " " + this.height);
            originalImageWidth = this.width;
            originalImageHeight = this.height;
            readFile(file, {
                width: originalImageWidth,
                height: originalImageHeight
            }).done(function(imgDataUrl, origImage) {
                //$("input, img, button").toggle();
                initJCrop(imgDataUrl);
            }).fail(function(msg) {
                toastr.error(msg);
            });
        };
        img.src = _URL.createObjectURL(file);
    } else {
        $('.js-upload').addClass('hide');
    }
});*/
$("#fileProfilePic").on("change", function(){
    file = this.files[0];
    if (!!file) {
        var JcropAPI = $('#userProfilePic').data('Jcrop');
        if (JcropAPI != undefined) {
            JcropAPI.destroy();
        }
        $('.js-upload').removeClass('hide');
        var img;
        img = new Image();
        img.onload = function () {
            //console.log(this.width + " " + this.height);
            originalImageWidth = this.width;
            originalImageHeight = this.height;
            readFile(file, {
                width: originalImageWidth,
                height: originalImageHeight
            }).done(function(imgDataUrl, origImage) {
                //$("input, img, button").toggle();
                initJCrop(imgDataUrl);
            }).fail(function(msg) {
                toastr.error(msg);
            });
        };
        img.src = _URL.createObjectURL(file);
    } else {
        $('.js-upload').addClass('hide');
    }
});

$("#btnUpload").on("click", function(){
    var thiz = $(this);
    $(this).text("Uploading...").prop("disabled", true);

    readFile(file, {
        width: uploadSize,
        height: uploadSize,
        crop: cropCoords
    }).done(function(imgDataURI) {
        var data = new FormData();
        var blobFile = dataURItoBlob(imgDataURI);
        data.append('portfolio-image', blobFile);
        
        ajaxRequest({
            url: FileApiPoint,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: 'json',
            success: function(res) {
                //console.log(res);
                if (res.status == 1) {
                    thiz.text('Upload');
                    var JcropAPI = $('#userProfilePic').data('Jcrop');
                    JcropAPI.destroy();
                    $('#userProfilePic').prop('style', '');
                    $('#userProfilePic').prop('src', res.image);
                    $('#userProfilePic').attr('old-image',res.image);                    
                    $('.js-upload').addClass('hide');
                    $('#portfolioPicPath').val(res.image);
                    toastr.success(res.message);
                } else {
                    toastr.error(res.msg);
                }
            },
            error: function(xhr) {
                toastr.error(blobFile);
            }
        });
    });
});
$("#btnUploadCancel").click(function() {
    $('#btnUpload').text('Upload');
    var JcropAPI = $('#userProfilePic').data('Jcrop');
    JcropAPI.destroy();
    $('#userProfilePic').prop('src', $('#userProfilePic').attr('old-image'));
    $('.js-upload').addClass('hide');
});
/*****************************
show local image and init JCrop
*****************************/
var initJCrop = function(imgDataUrl){
    var img = $("img#userProfilePic").attr("src", imgDataUrl);
    
    var storeCoords = function(c) {
        //console.log(c);
        cropCoords = c;
    };
    
    var w = img.width();
    var h = img.height();
    var s = uploadSize;
    img.Jcrop({
        onChange: storeCoords,
        onSelect: storeCoords,
        aspectRatio: 1,
        setSelect: [(w - s) / 2, (h - s) / 2, (w - s) / 2 + s, (h - s) / 2 + s],
        trueSize: [originalImageWidth, originalImageHeight]
    });
};
var ComponentsDropdowns = function () {
    
    function courseSelect(values,optionCourse){
        var subArray = values.toString().split('|');
        var subdomain = subArray[0];
        var course = parseInt(subArray[1]);
        var req = {};
        req.course       = course;
        req.optionCourse = optionCourse;
        req.subdomain    = subdomain;
        var res;
        req.action = "save-portfolio-course";
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if(res.status == 1) {
                var message = ((optionCourse==1)?'Successfully added '+terminologies["course_single"].toLowerCase()+'!':'Successfully removed '+terminologies["course_single"].toLowerCase()+'!');
                toastr.success(message);
            } else {
                toastr.error(res.message);
            }
            console.log(res);
        });
    };

    var handleMultiSelect = function () {
        $('.multi-select').multiSelect({
          afterSelect: function(values){
            courseSelect(values,1);
          },
          afterDeselect: function(values){
            courseSelect(values,0);
          }
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleMultiSelect();
        }
    };

}();
function fetchPortfolios() {
    var req = {};
    var res;
    req.action = "get-portfolios";
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        res = $.parseJSON(res);
        fillPortfoliosList(res);
        //console.log(res);
    });
}
function fillPortfoliosList(data) {
    //console.log(data);
    // General Details
    var html='';
    if(data.portfolios.length>0) {
        $('.js-portfolios').html('');
        for (var i = 0; i < data.portfolios.length; i++) {
            html+='<div class="col-md-6">'+
                '<div class="panel panel-default">'+
                    '<div class="panel-heading">'+
                        '<h3 class="panel-title"><a href="http://'+data.portfolios[i].subdomain+'.integro.io" target="_blank">'+data.portfolios[i].subdomain+'.integro.io  [Status : '+data.portfolios[i].status+']</a></h3>'+
                    '</div>'+
                    '<div class="panel-body" data-id="'+data.portfolios[i].subdomain+'">'+
                        '<form class="form-horizontal form-row-seperated">'+
                            '<div class="form-body">'+
                                '<div class="form-group">'+
                                    '<div class="col-md-12 text-center js-p-courses">';
            if(data.portfolios[i].status == 'Disapproved') {
                html+='<div class="margin-bottom-20">'+
                        '<label for="">Reason for disapproval:</label>'+
                        '<textarea name="" id="" cols="30" rows="6" class="form-control">'+data.portfolios[i].reason+'</textarea>'+
                    '</div>'+
                    '<div class="form-actions">'+
                        '<div class="row">'+
                            '<div class="col-md-12 text-center">'+
                                '<button type="button" data-pid="'+data.portfolios[i].id+'" data-name="'+data.portfolios[i].name+'" data-description="'+data.portfolios[i].description+'" data-subdomain="'+data.portfolios[i].subdomain+'" class="btn btn-sm green pf-resubmit">'+
                                    '<i class="fa fa-check"></i> Modify & Re-submit</button>'+
                            '</div>'+
                        '</div>'+
                    '</div>';
            } else {
                if(data.portfolios[i].courses.length>0) {
                    html+='<select multiple="multiple" class="multi-select" id="my_multi_select'+(i+1)+'" name="my_multi_select1[]">';
                    for (var j = 0; j < data.portfolios[i].courses.length; j++) {
                        html+='<option value="'+data.portfolios[i].subdomain+'|'+data.portfolios[i].courses[j].value+'" '+((data.portfolios[i].courses[j].optionCourse>0)?'selected':'')+'>'+data.portfolios[i].courses[j].label+'</option>';
                    }
                    html+='</select>';
                }
            }
            html+=                  '</div>'+
                                '</div>'+
                            '</div>'+
                        '</form>'+
                        '<!-- END FORM-->'+
                    '</div>'+
                '</div>'+
            '</div>';
        };
        $('.js-portfolios').append(html);
        ComponentsDropdowns.init();
    } else {
        $('.js-portfolios').html('<div class="col-md-12"><div class="note note-info">No portfolios created yet</div></div>');
    }
}
jQuery(document).ready(function() {
   //ComponentsDropdowns.init();
    $('#formNewPortfolio').submit(function(){
        var inputProfileName = $.trim($('#inputNewPortfolioName').val());
        var inputPortfolioDescription = $.trim($('#inputNewPortfolioDesc').val());
        var inputSubdomain = $.trim($('#inputNewSubdomain').val());
        var portfolioPic = $.trim($('#portfolioPicPath').val());
        
        if(!inputProfileName) {
            toastr.error('Please enter Portfolio Name');
        } else if(!inputPortfolioDescription) {
            toastr.error('Please enter Portfolio Description');
        } else if(!inputSubdomain) {
            toastr.error('Please enter Subdomain Name');
        } else if(!portfolioPic) {
            toastr.error('Please upload portfolio pic');
        } else {
            var req = {};
            req.portfolioName = inputProfileName;
            req.portfolioDesc = inputPortfolioDescription;
            req.subdomain     = inputSubdomain;
            req.portfolioPic  = portfolioPic;
            var res;
            req.action = "new-portfolio";
            $.ajax({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if(res.status == 1) {
                    //$('#frmNewPortfolio').html('<div class="jumbotron text-center"><h3>'+res.message+'</h3></div>');
                    window.location = window.location;
                } else {
                    toastr.error(res.message);
                }
                console.log(res);
            });
        }
        return false;
    });
    $('.js-portfolios').on('click', '.pf-resubmit', function(){
        pfId                     = $(this).attr('data-pid');
        var PortfolioDescription = $(this).attr('data-description');
        var inputSubdomain       = $(this).attr('data-subdomain');
        var pfname               = $(this).attr('data-name');
        //toastr.error(inputSubdomain);
        $('#inputPortfolioName').val(pfname);
        $('#inputPortfolioDescription').val(PortfolioDescription);
        $('#inputSubdomain').val(inputSubdomain);
        $('#modalEditPortfolio').modal('show');
    });
    $( "#btnEditPortfolio" ).click(function() {
        var inputPortfolioName = $.trim($('#inputPortfolioName').val());
        var inputPortfolioDescription = $.trim($('#inputPortfolioDescription').val());
        var inputSubdomain = $.trim($('#inputSubdomain').val());        
        if(!inputPortfolioName) {
            $('#frmNewPortfolio .help-block').html('<p class="text-danger text-center">Portfolio Name can\'t be left empty</p>');
        } else if(!inputPortfolioDescription) {
            $('#frmNewPortfolio .help-block').html('<p class="text-danger text-center">Portfolio Description can\'t be left empty</p>');
        } else if(!inputSubdomain) {
            $('#frmNewPortfolio .help-block').html('<p class="text-danger text-center">Subdomain Name can\'t be left empty</p>');
        } else {
            var req = {};
            req.pfId = pfId;
            req.portfolioName = inputPortfolioName;
            req.portfolioDesc = inputPortfolioDescription;
            req.subdomain     = inputSubdomain;
            var res;
            req.action = "re-submit-Portfolio";
            $.ajax({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if(res.status == 1) {
                    window.location = window.location;
                    //toastr.succcess(res.message);
                } else {
                    toastr.error(res.message);
                    return false;
                }
                console.log(res);
            });
        }
        
    });
});
</script>