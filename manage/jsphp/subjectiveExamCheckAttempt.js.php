<script type="text/javascript">
function fetchSubjectiveQuestions() {
	var req = {};
	var res;
	req.action = 'get-check-subjective-exam-questions';
	req.attemptId = attemptId;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
			console.log(res.message);
		else {
			//console.log(res);
			$("#studentName").html(res.student);
			$("#attemptNo").html(res.attemptNo);
			var checked = res.checked;
			if (checked==1) {
				$("#btnAttemptChecked").attr("disabled", true);
			};
			questions = res.questions;
			fillQuestions();
		}
	});
}
function fillQuestions() {
	$("#questionsContainer").html('');
	//console.log(questions);
	if (questions.length>0) {
		$("#questionsContainer").html('<div class="panel-group questions-panel" id="accQuestions" role="tablist" aria-multiselectable="true"></div>');
		for (var i = 0; i < questions.length; i++) {
			//questions[i]
			$("#accQuestions").append('<div class="panel panel-primary panel-questions">'+
						'<div class="panel-heading" role="tab" id="headQ'+i+'">'+
							'<h4 class="panel-title">'+
								'<a role="button" data-toggle="collapse" data-parent="#accordion" href="#bodyQ'+i+'" aria-expanded="true" aria-controls="bodyQ'+i+'">'+
								'Question #'+questions[i].qno+
								'</a>'+
							'</h4>'+
						'</div>'+
						'<div id="bodyQ'+i+'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headQ'+i+'">'+
						'</div>'+
					'</div>');
			if (questions[i].questionType == "question") {
				$('#bodyQ'+i).append('<div class="panel-body no-space">'+
					'<div class="alert alert-default no-margin">'+questions[i].question+'</div>'+
					'<div class="alert alert-success no-margin">'+
						'<label>Correct Answer :</label>'+
						'<div>'+questions[i].answer+'</div>'+
					'</div>'+
					
					((!questions[i].attempted)?('<div class="alert alert-danger no-margin">'+
						'<div class="row">'+
							'<div class="col-md-12">'+
								'<label>Not attempted</label>'+
							'</div>'+
						'</div>'+
					'</div>'):(((!questions[i].student_answer)?(''):(
					'<div class="alert alert-info no-margin">'+
						'<div class="row">'+
							'<div class="col-md-6">'+
								'<label>'+terminologies["student_single"]+'\'s Answer :</label>'+
								'<p>'+questions[i].student_answer+'</p>'+
							'</div>'+
							'<div class="col-md-6">'+
								'<label>Reviews/Comments :</label>'+
								'<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control">'+questions[i].review+'</textarea></div>'+
								'<div class="form-group">'+
									'<button class="btn btn-info js-answer-review" data-question="'+questions[i].id+'">Save Comments</button>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'))+
					'<div class="uploadsbox"></div>'))+
					'<div class="alert alert-warning no-margin marks-time">'+
						'<div class="row">'+
							'<div class="col-md-4">'+
								'<label>Time :</label>'+
								'<ul class="list-unstyled">'+
									'<li><i class="fa fa-clock-o"></i>Time Taken: '+questions[i].time+'</li>'+
									'<li><i class="fa fa-clock-o"></i>Topper Time: '+questions[i].topper_time+'</li>'+
									'<li><i class="fa fa-clock-o"></i>Average Time: '+questions[i].avg_time+'</li>'+
									((questions[i].shortest_time!=0)?('<li><i class="fa fa-clock-o"></i>Shortest Time: '+questions[i].shortest_time+'</li>'):(''))+
								'</ul>'+
							'</div>'+
							'<div class="col-md-4">'+
								'<label>Marks :</label>'+
								'<ul class="list-unstyled">'+
									'<li><i class="fa fa-check-square-o"></i>Maximum Marks: '+questions[i].max_marks+'</li>'+
									'<li><i class="fa fa-check-square-o"></i>Topper Marks: '+questions[i].topper_marks+'</li>'+
									'<li><i class="fa fa-check-square-o"></i>Average Marks: '+questions[i].avg_marks+'</li>'+
								'</ul>'+
							'</div>'+
							'<div class="col-md-4">'+
								'<label>Assign Marks :</label>'+
								((questions[i].attempted==0)?('<div class="text-danger">Not attempted</div>'):(
								'<div class="input-group">'+
									'<input type="text" class="form-control" value="'+((questions[i].check==0)?'':questions[i].marks)+'">'+
									'<span class="input-group-addon" id="sizing-addon1">/'+questions[i].max_marks+'</span>'+
									'<span class="input-group-btn">'+
										'<button class="btn btn-primary js-assign-marks" data-question="'+questions[i].id+'" data-max="'+questions[i].max_marks+'">Set</button>'+
									'</span>'+
								'</div>'))+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>');
				if (questions[i].attempted) {
					if (questions[i]["uploads"].length>0) {
						var uploads = questions[i]["uploads"];
						for (var j = 0; j < uploads.length; j++) {
							//uploads[i]
							$('#bodyQ'+i+' .uploadsbox').append(
								'<div class="alert alert-info no-margin">'+
									'<div class="row">'+
										'<div class="col-md-6">'+
											'<label>'+terminologies["student_single"]+'\'s Answer :</label>'+
											'<div>'+
												((uploads[j].type=="pdf")?('<a href="javascript:void(0)" data-path="'+uploads[j].path+'" data-type="'+uploads[j].type+'" class="btn green js-popup-answer">Checkout the answer (PDF Format)</a>'):((uploads[j].type=="jpg" || uploads[j].type=="jpeg" || uploads[j].type=="png" || uploads[j].type=="gif")?('<a href="javascript:void(0)" data-path="'+uploads[j].path+'" data-type="'+uploads[j].type+'" class="btn green js-popup-answer">Checkout the answer (Image Format)</a>'):('<a href="'+uploads[j].path+'" class="btn green text-uppercase" target="_blank">Click to download file ('+uploads[j].type+')</a>')))+
											'</div>'+
										'</div>'+
										'<div class="col-md-6">'+
											'<label>Reviews/Comments :</label>'+
											'<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control">'+uploads[j].review+'</textarea></div>'+
											'<div class="form-group">'+
												'<button class="btn btn-info js-answer-review" data-answer="'+uploads[j].id+'">Save Comments</button>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>');
						};
					}
				}
			} else if (questions[i].questionType == "questionGroup") {
				if (questions[i].listType=="one") {
					$('#bodyQ'+i).append('<div class="panel-body no-space">'+
								'<div class="alert alert-default no-margin">'+questions[i].question+'</div>'+
								'<div class="alert alert-warning no-margin marks-time">'+
									'<div class="row">'+
										'<div class="col-md-4">'+
											'<label>Time :</label>'+
											'<ul class="list-unstyled">'+
												'<li><i class="fa fa-clock-o"></i>Time Taken: '+questions[i].time+'</li>'+
												'<li><i class="fa fa-clock-o"></i>Topper Time: '+questions[i].topper_time+'</li>'+
												'<li><i class="fa fa-clock-o"></i>Average Time: '+questions[i].avg_time+'</li>'+
												((questions[i].shortest_time!=0)?('<li><i class="fa fa-clock-o"></i>Shortest Time: '+questions[i].shortest_time+'</li>'):(''))+
											'</ul>'+
										'</div>'+
										'<div class="col-md-4">'+
											'<label>Marks :</label>'+
											'<ul class="list-unstyled">'+
												'<li><i class="fa fa-check-square-o"></i>Maximum Marks: '+questions[i].max_marks+'</li>'+
												'<li><i class="fa fa-check-square-o"></i>Topper Marks: '+questions[i].topper_marks+'</li>'+
												'<li><i class="fa fa-check-square-o"></i>Average Marks: '+questions[i].avg_marks+'</li>'+
											'</ul>'+
										'</div>'+
										'<div class="col-md-4">'+
											'<label>Assign Marks :</label>'+
											((questions[i].attempted==0)?('<div class="text-danger">Not attempted</div>'):(
											'<div class="input-group">'+
												'<input type="text" class="form-control" value="'+((questions[i].check==0)?'':questions[i].marks)+'">'+
												'<span class="input-group-addon" id="sizing-addon1">/'+questions[i].max_marks+'</span>'+
												'<span class="input-group-btn"><button class="btn btn-primary js-assign-marks" data-question="'+questions[i].id+'" data-max="'+questions[i].max_marks+'">Set</button></span>'+
											'</div>'))+
										'</div>'+
									'</div>'+
								'</div>'+
							'</div>');
					if (questions[i]["subQuestions"].length>0) {
						$('#bodyQ'+i+' .marks-time').before('<div class="panel-group well bg-white" id="accQ'+i+'Subquestions" role="tablist" aria-multiselectable="true"></div>');
						var subquestions = questions[i]["subQuestions"];
						for (var j = 0; j < subquestions.length; j++) {
							$('#accQ'+i+'Subquestions').append(
								'<div class="panel panel-primary panel-questions">'+
									'<div class="panel-heading" role="tab" id="headQ'+i+'S'+j+'">'+
										'<h4 class="panel-title">'+
											'<a role="button" data-toggle="collapse" data-parent="#accordion" href="#bodyQ'+i+'S'+j+'" aria-expanded="true" aria-controls="bodyQ'+i+'S'+j+'">'+
											'Subquestion #'+(j+1)+
											'</a>'+
										'</h4>'+
									'</div>'+
									'<div id="bodyQ'+i+'S'+j+'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headQ'+i+'S'+j+'">'+
										'<div class="panel-body no-space">'+
											'<div class="alert alert-default no-margin">'+subquestions[j].question+'</div>'+
											'<div class="alert alert-success no-margin">'+
												'<label>Correct Answer :</label>'+
												'<div>'+subquestions[j].answer+'</div>'+
											'</div>'+
											((!subquestions[j].attempted)?('<div class="answer">'+
													'<div class="alert alert-danger no-margin">'+
														'<div class="row">'+
															'<div class="col-md-12">'+
																'<label>Not attempted</label>'+
															'</div>'+
														'</div>'+
													'</div>'+
												'</div>'):(((!subquestions[j].student_answer)?(''):('<div class="alert alert-info no-margin">'+
												'<div class="row">'+
													'<div class="col-md-6">'+
														'<label>'+terminologies["student_single"]+'\'s Answer :</label>'+
														'<p>'+subquestions[j].student_answer+'</p>'+
													'</div>'+
													'<div class="col-md-6">'+
														'<label>Reviews/Comments :</label>'+
														'<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control">'+subquestions[j].review+'</textarea></div>'+
														'<div class="form-group">'+
															'<button class="btn btn-info js-answer-review" data-question="'+subquestions[j].id+'">Save Comments</button>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>'))+
											'<div class="uploadsbox"></div>'))+
										'</div>'+
									'</div>'+
								'</div>');
							if (subquestions[j].attempted) {
								if (subquestions[j]["uploads"].length>0) {
									var uploads = subquestions[j]["uploads"];
									for (var k = 0; k < uploads.length; k++) {
										//uploads[i]
										$('#bodyQ'+i+'S'+j+' .uploadsbox').append(
											'<div class="alert alert-info no-margin">'+
												'<div class="row">'+
													'<div class="col-md-6">'+
														'<label>'+terminologies["student_single"]+'\'s Answer :</label>'+
														'<div>'+
															((uploads[k].type=="pdf")?('<a href="javascript:void(0)" data-path="'+uploads[k].path+'" data-type="'+uploads[k].type+'" class="btn green js-popup-answer">Checkout the answer (PDF Format)</a>'):((uploads[k].type=="jpg" || uploads[k].type=="jpeg" || uploads[k].type=="png" || uploads[k].type=="gif")?('<a href="javascript:void(0)" data-path="'+uploads[k].path+'" data-type="'+uploads[k].type+'" class="btn green js-popup-answer">Checkout the answer (Image Format)</a>'):('<a href="'+uploads[k].path+'" class="btn green text-uppercase" target="_blank">Click to download file ('+uploads[k].type+')</a>')))+
														'</div>'+
													'</div>'+
													'<div class="col-md-6">'+
														'<label>Reviews/Comments :</label>'+
														'<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control">'+uploads[k].review+'</textarea></div>'+
														'<div class="form-group">'+
															'<button class="btn btn-info js-answer-review" data-answer="'+uploads[k].id+'">Save Comments</button>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>');
									};
								}
							}
						};
					}
				} else {
					$('#bodyQ'+i).append('<div class="panel-body"></div>');
					if (questions[i]["subQuestions"].length>0) {
						$('#bodyQ'+i+' .panel-body').append('<div class="panel-group" id="accQ'+i+'Subquestions" role="tablist" aria-multiselectable="true"></div>');
						var subquestions = questions[i]["subQuestions"];
						for (var j = 0; j < subquestions.length; j++) {
							$('#accQ'+i+'Subquestions').append(
								'<div class="panel panel-primary panel-questions">'+
								'<div class="panel-heading" role="tab" id="headQ'+i+'S'+j+'">'+
									'<h4 class="panel-title">'+
										'<a role="button" data-toggle="collapse" data-parent="#accordion" href="#bodyQ'+i+'S'+j+'" aria-expanded="true" aria-controls="bodyQ'+i+'S'+j+'">'+
										'Subquestion #'+(j+1)+
										'</a>'+
									'</h4>'+
								'</div>'+
								'<div id="bodyQ'+i+'S'+j+'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headQ'+i+'S'+j+'">'+
									'<div class="panel-body no-space">'+
										'<div class="alert alert-default no-margin">'+subquestions[j].question+'</div>'+
										'<div class="alert alert-success no-margin">'+
											'<label>Correct Answer :</label>'+
											'<div>'+subquestions[j].answer+'</div>'+
										'</div>'+
											((!subquestions[j].attempted)?('<div class="answer">'+
												'<div class="alert alert-danger no-margin">'+
													'<div class="row">'+
														'<div class="col-md-12">'+
															'<label>Not attempted</label>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>'):(((!subquestions[j].student_answer)?(''):('<div class="alert alert-info no-margin">'+
											'<div class="row">'+
												'<div class="col-md-6">'+
													'<label>'+terminologies["student_single"]+'\'s Answer :</label>'+
													'<div>'+subquestions[j].student_answer+'</div>'+
												'</div>'+
												'<div class="col-md-6">'+
													'<label>Reviews/Comments :</label>'+
													'<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control">'+subquestions[j].review+'</textarea></div>'+
													'<div class="form-group">'+
														'<button class="btn btn-info js-answer-review" data-question="'+subquestions[j].id+'">Save Comments</button>'+
													'</div>'+
												'</div>'+
											'</div>'+
										'</div>'))+
										'<div class="uploadsbox"></div>'))+
										'<div class="alert alert-warning no-margin marks-time">'+
											'<div class="row">'+
												'<div class="col-md-4">'+
													'<label>Time :</label>'+
													'<ul class="list-unstyled">'+
														'<li><i class="fa fa-clock-o"></i>Time Taken: '+subquestions[j].time+'</li>'+
														'<li><i class="fa fa-clock-o"></i>Topper Time: '+subquestions[j].topper_time+'</li>'+
														'<li><i class="fa fa-clock-o"></i>Average Time: '+subquestions[j].avg_time+'</li>'+
														((subquestions[j].shortest_time!=0)?('<li><i class="fa fa-clock-o"></i>Shortest Time: '+subquestions[j].shortest_time+'</li>'):(''))+
													'</ul>'+
												'</div>'+
												'<div class="col-md-4">'+
													'<label>Marks :</label>'+
													'<ul class="list-unstyled">'+
														'<li><i class="fa fa-check-square-o"></i>Maximum Marks: '+subquestions[j].max_marks+'</li>'+
														'<li><i class="fa fa-check-square-o"></i>Topper Marks: '+subquestions[j].topper_marks+'</li>'+
														'<li><i class="fa fa-check-square-o"></i>Average Marks: '+subquestions[j].avg_marks+'</li>'+
													'</ul>'+
												'</div>'+
												'<div class="col-md-4">'+
													'<label>Assign Marks :</label>'+
													((subquestions[j].attempted==0)?('<div class="text-danger">Not attempted</div>'):(
													'<div class="input-group">'+
														'<input type="text" class="form-control" value="'+((subquestions[j].check==0)?'':subquestions[j].marks)+'">'+
														'<span class="input-group-addon" id="sizing-addon1">/'+subquestions[j].max_marks+'</span>'+
														'<span class="input-group-btn"><button class="btn btn-primary js-assign-marks" data-question="'+subquestions[j].id+'" data-max="'+subquestions[j].max_marks+'">Set</button></span>'+
													'</div>'))+
												'</div>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>'+
							'</div>');
							if (subquestions[j].attempted) {
								if (subquestions[j]["uploads"].length>0) {
									var uploads = subquestions[j]["uploads"];
									for (var k = 0; k < uploads.length; k++) {
										//uploads[i]
										$('#bodyQ'+i+'S'+j+' .uploadsbox').append(
											'<div class="alert alert-info no-margin">'+
												'<div class="row">'+
													'<div class="col-md-6">'+
														'<label>'+terminologies["student_single"]+'\'s Answer :</label>'+
														'<div>'+
															((uploads[k].type=="pdf")?('<a href="javascript:void(0)" data-path="'+uploads[k].path+'" data-type="'+uploads[k].type+'" class="btn green js-popup-answer">Checkout the answer (PDF Format)</a>'):((uploads[k].type=="jpg" || uploads[k].type=="jpeg" || uploads[k].type=="png" || uploads[k].type=="gif")?('<a href="javascript:void(0)" data-path="'+uploads[k].path+'" data-type="'+uploads[k].type+'" class="btn green js-popup-answer">Checkout the answer (Image Format)</a>'):('<a href="'+uploads[k].path+'" class="btn green text-uppercase" target="_blank">Click to download file ('+uploads[k].type+')</a>')))+
														'</div>'+
													'</div>'+
													'<div class="col-md-6">'+
														'<label>Reviews/Comments :</label>'+
														'<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control">'+uploads[k].review+'</textarea></div>'+
														'<div class="form-group">'+
															'<button class="btn btn-info js-answer-review" data-answer="'+uploads[k].id+'">Save Comments</button>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>');
									};
								}
							}
						};
					}
				}
			}
		};
	};
}
$(document).ready(function(){
	$('#questionsContainer').on("click", ".js-assign-marks", function() {
		var thiz		= $(this);
		var marks		= parseFloat($(this).closest('.input-group').find('.form-control').val());
		var questionId	= $(this).attr("data-question");
		var maxMarks	= parseFloat($(this).attr("data-max"));
		if (marks>maxMarks) {
			toastr.error("Enter valid marks please!");
		} else if(!questionId) {
			toastr.error("Please, refresh and try again!");
		} else {
			var req = {};
			var res;
			req.action		= 'save-marks-for-subjectiveExam';
			req.attemptId	= attemptId;
			req.questionId	= questionId;
			req.marks		= marks;
			thiz.attr("disabled", true);
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndPoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				thiz.attr("disabled", false);
				res =  $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					toastr.success("Marks saved!");
				}
			});
		}
	});
	$('#questionsContainer').on("click", ".js-answer-review", function() {
		var thiz		= $(this);
		var review		= $(this).closest('.col-md-6').find('.form-control').val();
		var questionId	= $(this).attr("data-question");
		var answerId	= $(this).attr("data-answer");
		if (!review) {
			toastr.error("Enter review/comment please!");
		} else if(!questionId && !answerId) {
			toastr.error("Please, refresh and try again!");
		} else {
			var req = {};
			var res;
			req.action		= 'save-review-for-subjectiveExam';
			req.attemptId	= attemptId;
			req.review		= review;
			if (questionId) {
				req.questionId	= questionId;
			} else {
				req.answerId	= answerId;
			}
			thiz.attr("disabled", true);
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndPoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				thiz.attr("disabled", false);
				res =  $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					toastr.success("Review/comment saved!");
				}
			});
		}
	});
	$("#btnAttemptChecked:enabled").click(function() {
		var thiz= $(this);
		if(thiz.attr('disabled') != 'disabled') {
			var req = {};
			var res;
			req.action		= 'mark-subjective-exam-checked';
			req.attemptId	= attemptId;
			thiz.attr("disabled", true);
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndPoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				//thiz.attr("disabled", false);
				res =  $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					toastr.success("Marked attempt as checked!");
				}
			});
		}
	});
	$('#questionsContainer').on('click', '.js-popup-answer', function(){
		var path = $(this).attr("data-path");
		var type = $(this).attr("data-type");
		if(!path || !type) {
			toastr.error("There is something wrong, please refresh and try again!");
		} else {
			if (type == "pdf") {
				$('#jsAnswer').html('<iframe src="'+path+'" frameborder="0" height="500" class="btn-block"></iframe>');
			} else {
				$('#jsAnswer').html('<img src="'+path+'" alt="Answer" class="btn-block" />');
			}
			$('#modalAnswerFile').modal('show');
		}
	});
});
</script>