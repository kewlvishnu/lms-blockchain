<script type="text/javascript">
var sortOrder = [];
var examStart = new Date();
var examEnd = new Date('2099/12/31');
var chapterId;
function fetchChapters() {
    var req = {};
    req.action = "get-chapters-for-exam";
    req.subjectId = subjectId;
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        fillChaptersSelect(res);
    });
}
function fillChaptersSelect(data) {
    if(data.status == 0) {
        toastr.error(data.message);
        return;
    }
    var opts = '';
    for(i=0; i<data.chapters.length; i++) {
        opts += '<option value="'+data.chapters[i]['id']+'">'+data.chapters[i]['name']+'</option>';
    }
    opts += '<option value="0">Independent</option>';
    $('#chapterSelect').append(opts);
    if (typeof chapterId !== 'undefined') {
        $('#chapterSelect').val(xx);
    }
}
$(function() {

	/*$('.import-manual-exam-link').attr('href','add-manualexam.php?courseId='+getUrlParameter('courseId')+'&subjectId='+getUrlParameter('subjectId'));*/
	$('.btn-upload').click(function(){
		$(this).next().click();
		return false;
	});
	$('.js-uploader').change(function(){
		var file = $(this).val();
		if (!!file) {
			$(this).closest('form').submit();
		};
	});
	// bind to the form's submit event 
	$('.frm-upload').submit(function(){
    	var thiz = $(this);
    	$('.btn-upload').attr('disabled', true);
    	$('.btn-upload').after('<span class="upload-loader"><img src="'+sitePath+'img/ajax-loader.gif" alt="loader" /></span>');
	    var options = { 
	        //target:        '#output2',   // target element(s) to be updated with server response 
	        //data: { examId: examId },
	        dataType: 'json',
	        beforeSubmit:  showRequest,  // pre-submit callback 
	        success: function(msg) {
	        	$('.js-uploader').val('');
	        	$('.upload-loader').remove();
    			$('.btn-upload').attr('disabled', false);
	        	if (msg.status == 0) {
	        		toastr.error(msg.message);
	        	} else {
	        		//console.log(msg);
	        		fillManualExamForm(msg.marks,msg.analytics,msg.questions);
	        	}
	        }  // post-submit callback 
	 
	    }; 
        // inside event callbacks 'this' is the DOM element so we first 
        // wrap it in a jQuery object and then invoke ajaxSubmit 
        $(this).ajaxSubmit(options); 
 
        // !!! Important !!! 
        // always return false to prevent standard browser submit and page navigation 
        return false;
    });
	// pre-submit callback 
	function showRequest(formData, jqForm, options) { 
	    // formData is an array; here we use $.param to convert it to a string to display it 
	    // but the form plugin does this for you automatically when it submits the data 
	    var queryString = $.param(formData); 
	 
	    // jqForm is a jQuery object encapsulating the form element.  To access the 
	    // DOM element for the form do this: 
	    // var formElement = jqForm[0]; 
	 
	    //toastr.error('About to submit: \n\n' + queryString); 
	 
	    // here we could return false to prevent the form from being submitted; 
	    // returning anything other than false will allow the form submit to continue 
	    return true; 
	}
	function fillManualExamForm (data,analytics,questions) {
		//console.log(data);
		if (data.length>0) {
			$('#jsForm').attr('data-analytics', analytics);
			$('#jsForm').attr('data-questions', questions);
			$('#jsForm').html('<div class="form-group"><label for="inputExamName">Exam Name:</label><input type="text" id="inputExamName" class="form-control" placeholder="Exam Name" /></div>');
			if (analytics == "deep") {
				var htmlTotal = '';
				var filler1 = '';
				for (var i = 0; i < questions; i++) {
					//htmlTotal+='<tr><th class="active">Q'+(i+1)+'</th><td><input type="text" class="form-control input-total" /></td></tr>';
					filler1+='<th>Q'+(i+1)+'</th>';
					//filler2+= '<td><input type="text" class="form-control input-marks" data-q="'+i+'" value="'+data[i][3]+'" /></td>';
					htmlTotal+= '<td><input type="text" class="form-control input-total" data-q="'+i+'" /></td>';
				};
				$('#jsForm').append('<table class="table table-bordered table-total">'+
									'<tr class="active">'+
										'<th colspan="'+questions+'" class="text-center">Total Marks of questions</th>'+
									'</tr>'+
									'<tr class="active">'+filler1+'</tr>'+
									'<tr>'+htmlTotal+'</tr>'+
									'</table>');
				$('#jsForm').append('<table class="table table-bordered table-marks">'+
									'<tr class="active">'+
										'<th rowspan="2">'+terminologies["student_single"]+' ID</th>'+
										'<th rowspan="2">'+terminologies["student_single"]+' Name</th>'+
										'<th colspan="'+questions+'" class="text-center">Marks</th>'+
									'</tr>'+
									'<tr class="active"></tr>'+
									'</table>');
				$('#jsForm .table-marks tr:last-child').append(filler1);
				for (var i = 0; i < data.length; i++) {
					var filler2 = '';
					for (var j = 0; j < questions; j++) {
						filler2+= '<td><input type="text" class="form-control input-marks" data-q="'+j+'" value="'+data[i][j+2]+'" /></td>';
					}
					$('#jsForm .table-marks').append('<tr data-sid="'+data[i][0]+'"><td>'+data[i][0]+'</td><td>'+data[i][1]+'</td>'+filler2+'</tr>');
				};
			} else {
				$('#jsForm').append('<div class="form-group"><label for="inputTotalMarks">Total Marks for exam:</label><input type="text" id="inputTotalMarks" class="form-control" placeholder="Exam Total Marks" /></div>');
				$('#jsForm').append('<table class="table table-bordered table-marks">'+
									'<tr class="active">'+
										'<th>'+terminologies["student_single"]+' ID</th>'+
										'<th>'+terminologies["student_single"]+' Name</th>'+
										'<th>Marks</th>'+
									'</tr>'+
									'</table>');
				for (var i = 0; i < data.length; i++) {
					$('#jsForm .table').append('<tr data-sid="'+data[i][0]+'"><td>'+data[i][0]+'</td><td>'+data[i][1]+'</td><td><input type="text" class="form-control input-marks" value="'+data[i][2]+'" /></td></tr>');
				};
			}
			$('#jsForm').append('<div><button class="btn btn-primary" id="btnSubmit">SUBMIT</button></div>');
		};
	}
	$('#jsForm').on("click", "#btnSubmit", function(){
		$('.help-block').remove();
		$('#jsForm td').removeClass("danger");
		var flagTotal = false;
		var flagMarks = false;
		var flagValidMarks = false;
		var value = "";
		var qkey  = 0;
		var i = 0;
		var optionAnalytics = $('#jsForm').attr('data-analytics');
		var inputQuestions = $('#jsForm').attr('data-questions');
		chapterId=$('#chapterSelect').val();
		if (optionAnalytics == "deep") {
			var formTotal = [];
			$('.input-total').each(function(key,obj){
				value = $(obj).val();
				if(value == '') {
					flagTotal = true;
				}
				formTotal[i] = $(obj).val();
				i++;
			});
			console.log(formTotal);
			$('.input-marks').each(function(key,obj){
				value = $(obj).val();
				if(value == '') {
					flagMarks = true;
				}
				qkey = $(obj).attr('data-q');
				//console.log(parseInt(value)+' : '+parseInt(formTotal));
				if (parseInt(value)>parseInt(formTotal[qkey])) {
					$(this).closest("td").addClass("danger");
					flagValidMarks = true;
				}
			});
		} else {
			var formTotal = $("#inputTotalMarks").val();
			if (!formTotal) {
				flagTotal = true;
			};
			$('.input-marks').each(function(key,obj){
				value = $(obj).val();
				if(value == '') {
					flagMarks = true;
				}
				if (parseInt(value)>parseInt(formTotal)) {
					$(this).closest("td").addClass("danger");
					flagValidMarks = true;
				}
			});
		}
		var inputExamName = $("#inputExamName").val();
		//console.log(flagMarks);
		if (!inputExamName) {
			$('#inputExamName').focus();
			$('#inputExamName').closest('.form-group').after("<p class='help-block alert alert-danger'>Please enter exam name</p>");
		} else if (flagTotal) {
			if (optionAnalytics == "deep") {
				$('#inputExamName').focus();
				$('#jsForm .table-total').before("<p class='help-block alert alert-danger'>Please enter total marks in all input boxes</p>");
			} else {
				$('#inputExamName').focus();
				$('#inputTotalMarks').closest('.form-group').after("<p class='help-block alert alert-danger'>Please enter valid marks</p>");
			}
		} else if (flagMarks) {
			$('#inputExamName').focus();
			$('#jsForm .table-marks').before("<p class='help-block alert alert-danger'>Please enter marks in all input boxes</p>");
		} else if (flagValidMarks) {
			$('#inputExamName').focus();
			$('#jsForm .table-marks').before("<p class='help-block alert alert-danger'>Please enter valid marks in all input boxes</p>");
		} else if (!chapterId) {
			toastr.error("Please select a section!");
		} else {
			var formMarks = [];
			var formStudents = [];
			var i = 0;
			var j = 0;
			/*$('#jsForm .input-total').each(function(k,obj){
				formTotal[i] = $(obj).val();
				i++;
			});
			i = 0;*/
			$('#jsForm .table-marks tr').each(function(k,obj){
				if ((optionAnalytics == "quick" && k>0) || (optionAnalytics == "deep" && k>1)) {
					//i = $(obj).attr('data-sid');
					j = 0;
					formMarks[i] = [];
					formStudents[i] = $(obj).attr('data-sid');
					$(obj).find('.input-marks').each(function(k1,obj1){
						formMarks[i][j] = $(obj1).val();
						j++;
					});
					i++;
				};
			});
			//console.log(formMarks);
			//console.log(formStudents);
			//console.log(formTotal);
			var req = {};
			var res;
			req.action		= 'save-manual-exam-form';
			req.subjectId	= subjectId;
			req.courseId	= courseId;
			req.type		= optionAnalytics;
			req.questions	= inputQuestions;
			req.name		= inputExamName;
			req.marks		= formMarks;
			req.total		= formTotal;
			req.students	= formStudents;
			req.chapterId=$('#chapterSelect').val();
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndPoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					window.location = sitepathManageManualExams+res.examId;
					/*window.location = 'manualexam.php?courseId='+getUrlParameter('courseId')+'&subjectId='+getUrlParameter('subjectId')+'&examId='+res.examId;*/
					//$('#jsForm').html("<p class='help-block text-success'>Successfully added!</p>");
				}
			});
		}
	});
	$("#jsForm").on("keypress",".input-marks,.input-total", function(e){
		$('.help-block').remove();
		$('#jsForm td').removeClass("danger");
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			toastr.error('Enter Only Numbers');
			return false;
		}
	});
});
</script>