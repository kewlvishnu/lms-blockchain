<script type="text/javascript">
    //function to fetch notifications
    var pageLimit = 10;
    var pageNumber = 1;
    function fetchNotificationsChat() {
        var req = {};
        var res;
        req.pageNumber = parseInt(pageNumber)-1;
        req.pageLimit  = pageLimit;
        if(userRole == 1) {
            req.action = 'get-institute-notifications-chat';
        } else {
            req.action = 'get-instructor-notifications-chat';
        }
        ajaxRequest({
            'type'  :   'post',
            'url'   :   ApiEndPoint,
            'data'  :   JSON.stringify(req),
            success:function(res){
                res = $.parseJSON(res);
                if(res.status == 0)
                    toastr.error(res.message);
                else {
                    notifications = res;
                    fillNotificationsChat(res);
                }
            }
        });
    }
    function fillNotificationsChat (data) {
        console.log(data);
        if (data.notiCount>0) {
            var html = '';
            for(var i = 0; i < data.chatnotifications.length; i++) {
                var message = "";
                var d = new Date(data.chatnotifications[i].timestamp+'  UTC');
                //d = timeSince(d.getTime()); //returns 1340220044000
                var notiDate = d.getDate();
                var notiMonth = month[d.getMonth()].substring(0,3);
                var notiHours = d.getHours();
                var notiMinutes = d.getMinutes();
                var strTime = getTime12Hour(notiHours,notiMinutes);
                switch(data.chatnotifications[i].type) {
                    case '0':
                        if (data.chatnotifications[i].room_type == "public") {
                            message = data.chatnotifications[i].senderName+' just started conversation in <strong>'+terminologies["subject_single"]+' : '+data.chatnotifications[i].courseName+'</strong> of <strong>'+terminologies["course_single"]+' : '+data.chatnotifications[i].subjectName+'</strong></span>';
                        } else {
                            message = data.chatnotifications[i].senderName+' just started conversation with <a href="'+sitepathInstructor+data.chatnotifications[i].userId+'" target="_blank">'+data.chatnotifications[i].firstName+' '+data.chatnotifications[i].lastName+'</a> in <strong>'+terminologies["subject_single"]+' : '+data.chatnotifications[i].courseName+'</strong> of <strong>'+terminologies["course_single"]+' : '+data.chatnotifications[i].subjectName+'</strong></span>';
                        }
                        break;
                    case '1':
                        if (data.chatnotifications[i].room_type == "public") {
                            message = data.chatnotifications[i].senderName+' just sent a message in <strong>'+terminologies["subject_single"]+' : '+data.chatnotifications[i].courseName+'</strong> of <strong>'+terminologies["course_single"]+' : '+data.chatnotifications[i].subjectName+'</strong></span>';
                        } else {
                            message = data.chatnotifications[i].senderName+' just messaged <a href="'+sitepathInstructor+data.chatnotifications[i].userId+'" target="_blank">'+data.chatnotifications[i].firstName+' '+data.chatnotifications[i].lastName+'</a> in <strong>'+terminologies["subject_single"]+' : '+data.chatnotifications[i].courseName+'</strong> of <strong>'+terminologies["course_single"]+' : '+data.chatnotifications[i].subjectName+'</strong></span>';
                        }
                        break;
                }
                html += '<div class="mt-comment chat-parent-all '+((data.chatnotifications[i].status==0)?(''):('disabled'))+'">'+
                            '<div class="mt-comment-img">'+
                                '<img src="'+data.chatnotifications[i].profilePic+'" /> </div>'+
                            '<div class="mt-comment-body">'+
                                '<div class="mt-comment-info">'+
                                    '<span class="mt-comment-author">'+data.chatnotifications[i].senderName+'</span>'+
                                    '<span class="mt-comment-date">'+notiDate+' '+notiMonth+', '+strTime+'</span>'+
                                '</div>'+
                                '<div class="mt-comment-text"> '+message+' </div>'+
                            '</div>'+
                        '</div>';
            }
            if ($('#pagerChatsContent').html() != '') {
                var totalNotifications = data.notiCount;
                var noOfPages = parseInt(totalNotifications/pageLimit);
                //console.log(noOfPages);
                var handleDynamicPagination = function() {
                    $('#pagerChats').bootpag({
                        paginationClass: 'pagination',
                        next: '<i class="fa fa-angle-right"></i>',
                        prev: '<i class="fa fa-angle-left"></i>',
                        total: noOfPages,
                        page: pageNumber,
                    }).on("page", function(event, num){
                    	pageNumber = num;
                        fetchNotificationsChat(num);
                    });
                }
                handleDynamicPagination();
            }
            $('#pagerChatsContent').html(html);
        } else {

            $('#pagerChatsContent').html('No chat notifications.');
        }

    }
    $('body').on('click', '.btn-bookmark', function(e){
        bookmarkThisPage(e);
    });
</script>