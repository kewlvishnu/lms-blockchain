<script type="text/javascript">
var letters = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
var player;
var videoDuration;
var reqTag = {};
reqTag.action = 'tags';
var ms = $('#magicsuggest').magicSuggest({
	valueField: 'name',
	data: TypeEndPoint,
	dataUrlParams: reqTag
});
function fetchSubjectQuestions() {
	var req = {};
	var res;
	req.action = 'get-subject-questions';
	req.subjectId = subjectId;
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 0) {
			toastr.error(res.message);
		} else {
			fillSubjectQuestions(res);
		}
	});
}
function fillSubjectQuestions(data) {
	$('#sectionQuestions').html('');
	if (data.questions>0) {
		var queshtml = '';
		var categories = data.questionGroups;
		for (var i = 0; i < categories.length; i++) {
			var questionDisplay = '';
			var greenInfo = '';
			var redInfo = '';
			//console.log(categories[i]);
			if(i == 0) {
				questionDisplay = 'Multiple Choice Question';
				greenInfo = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="Green symbol represents that the  question  is complete. The question would be counted in the no of entered questions"></i>';
				redInfo = '<i class="icon-close text-danger tooltips" data-placement="right" data-original-title="The Red symbol denotes that the  question is incomplete. The question would not be counted in the number  of entered questions<br><br>Following conditions have to met, for a question to appear green(complete)<br><br>a)  The question  text should be entered<br>b)  There should be at least 2 answer options<br>c)  One of them should be selected as correct"></i>';
			}
			else if(i == 1) {
				questionDisplay = 'True or False';
				greenInfo = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="Green symbol represents that the  question  is complete. The question would be counted in the no of entered questions"></i>';
				redInfo = '<i class="icon-close text-danger tooltips" data-placement="right" data-original-title="The Red symbol denotes that the  question is incomplete. The question would not be counted in the number  of entered questions<br><br>Following conditions have to met, for a question to appear green(complete)<br><br>a)  The question text should be entered<br>b)  The answer should be marked as either  true or false"></i>';
			}
			else if(i == 2) {
				questionDisplay = 'Fill in the blanks';
				greenInfo = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="Green symbol represents that the  question  is complete. The question would be counted in the no of entered questions"></i>';
				redInfo = '<i class="icon-close text-danger tooltips" data-placement="right" data-original-title="The Red symbol denotes that the  question is incomplete. The question would not be counted in the number  of entered questions<br><br>Following conditions have to met, for a question to appear green(complete)<br><br>a)  The question text  should be entered<br>b) The answer should be entered in the blank/box"></i>';
			}
			else if(i == 3) {
				questionDisplay = 'Match the following';
				greenInfo = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="Green symbol represents that the  question  is complete. The question would be counted in the no of entered questions"></i>';
				redInfo = '<i class="icon-close text-danger tooltips" data-placement="right" data-original-title="The Red symbol denotes that the question is incomplete. The question would not be counted in the number of entered questions<br><br>Following conditions have to met, for a question to appear green(complete)<br><br>a) The question text should be entered<br>b) There should be  corresponding entry in column B for every entry in Column A"></i>';
			}
			else if(i == 4) {
				questionDisplay = 'Comprehension Type Question';
				greenInfo = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="All Questions are Complete"></i>';
				redInfo = '<i class="icon-close text-danger tooltips" data-placement="right" data-original-title="Red represents that some part is complete and certain part is incomplete<br><br>Red symbol would appear here, if any of the conditions are not met:<br>a) The passage should be entered<br>b) There should be at least one complete question<br>c) All the questions entered should be complete"></i>';
			}
			else if(i == 5) {
				questionDisplay = 'Dependent Type Question';
				greenInfo = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="All Sub-parts are Complete"></i>';
				redInfo = '<i class="icon-close text-danger tooltips" data-placement="right" data-original-title="Red represents that some part is complete and certain part is incomplete<br>Red symbol would appear here, if any of the conditions are not met:<br><br>a)The passage should be entered<br>b)There should be minimum one  complete sub part (multiple choice/multiple answer/true or false/fill in the  blanks)<br>c)All the sub parts entered should be complete"></i>';
			}
			else if(i == 6) {
				questionDisplay = 'Multiple Answer Question';
				greenInfo = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="Green symbol represents that the  question  is complete. The question would be counted in the no of entered questions"></i>';
				redInfo = '<i class="icon-close text-danger tooltips" data-placement="right" data-original-title="The Red symbol denotes that the  question is incomplete. The question would not be counted in the number  of entered questions<br><br>Following conditions have to met, for a question to appear green(complete)<br><br>a)  The question text  should be entered<br>b)  There should be at least 2 answer options<br>c)  Minimum 1 of the options should be selected as correct"></i>';
			}
			else if(i == 7) {
				questionDisplay = 'Audio Question';
				greenInfo = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="Green symbol represents that the  question  is complete. The question would be counted in the no of entered questions"></i>';
				redInfo = '<i class="icon-close text-danger tooltips" data-placement="right" data-original-title="The Red symbol denotes that the  question is incomplete. The question would not be counted in the number  of entered questions<br><br>Following conditions have to met, for a question to appear green(complete)<br><br>a)  The question  text should be entered<br>b)  There should be at least 2 answer options<br>c)  One of them should be selected as correct"></i>';
			}
			queshtml+= '<div class="portlet light portlet-fit no-space margin-bottom-10 portlet-collapsible">'+
		                    '<div class="portlet-body no-space">'+
		                        '<div class="mt-element-list mt-element-list-sm">'+
		                            '<div class="mt-list-head list-default ext-1 green-jungle">'+
		                                '<div class="row">'+
		                                    '<div class="col-xs-8">'+
		                                        '<div class="list-head-title-container">'+
		                                            '<h3 class="list-title uppercase sbold">'+questionDisplay+'</h3>'+
		                                        '</div>'+
		                                    '</div>'+
		                                    '<div class="col-xs-4 text-right">'+
	                                            '<a class="btn dark btn-sm collapsed" role="button" data-toggle="collapse" href="#collapse'+i+'" aria-expanded="false" aria-controls="collapse'+i+'"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></a>'+
	                                        '</div>'+
		                                '</div>'+
		                            '</div>'+
		                            '<div class="collapse" id="collapse'+i+'">'+
			                            '<div class="mt-list-container list-default ext-1">'+
			                                '<div class="mt-list-title uppercase">List of questions'+
			                                    '<span class="badge badge-default pull-right bg-hover-green-jungle">'+
			                                        '<a class="font-white new-question" href="javascript:;" data-questionType="'+i+'">'+
			                                            '<i class="fa fa-plus"></i>'+
			                                        '</a>'+
			                                    '</span>'+
			                                '</div>'+
			                                '<ul class="questions">';
		    var questions = '';
		    if(categories[i].length > 0) {
				var questionNumber = 0;
				for(var j=0; j<categories[i].length; j++) {
					if(categories[i][j]['parentId'] == 0) {
						var dispQuestionNumber = (questionNumber + 1);
						if(categories[i][j]['questionType'] == 4) {
							for(var k=0;k<categories[i].length;k++) {
								if(categories[i][k]['parentId'] == categories[i][j]['id'])
									questionNumber++;
							}
						}
						else {
							questionNumber++;
						}

						var status = '<i class="fa fa-question-circle text-warning tooltips" data-original-title="The yellow symbol represents that the question has been imported, but has not been saved.<br>Please save the question so that the question is actually considered." data-placement="right"></i>';
						if(categories[i][j]['status'] == 1)
							status = greenInfo;
						else if(categories[i][j]['status'] == 0)
							status = redInfo;
						else
							status = '<i class="fa fa-question-circle text-warning tooltips" data-original-title="The yellow symbol represents that the question has been imported, but has not been saved.<br>Please save the question so that the question is actually considered." data-placement="right"></i>';
						var questionText = $(categories[i][j]['question']).text();
						if(questionText.length > 35)
							questionText = questionText.substring(0, 35) + '...';
						if(categories[i][j]['questionType'] == 0)
							questionType = 'MCQ';
						else if(categories[i][j]['questionType'] == 1)
							questionType = 'T/F';
						else if(categories[i][j]['questionType'] == 2)
							questionType = 'FTB';
						else if(categories[i][j]['questionType'] == 3)
							questionType = 'MTF';
						else if(categories[i][j]['questionType'] == 6)
							questionType = 'MAQ';
						else if(categories[i][j]['questionType'] == 7)
							questionType = 'AUD';
						else if(categories[i][j]['questionType'] == 4) {
							var subParts = categories[i][j]['subQuestions'].length;
							/*var subParts = 0;
							var parentId = categories[i][j]['id'];
							for(var k=0;k<categories[i].length;k++) {
								//console.log(parentId +' '+ categories[i][k]['parentId'] +' '+ categories[i][k]['status']);
								if(parentId == categories[i][k]['parentId'] && categories[i][k]['status'] == 1)
									subParts++;
							}*/
							questionType = '<span class="pull-right">(<span class="totalQuestions">' + subParts + '</span> Questions)</span>CMP';
						}
						else if(categories[i][j]['questionType'] == 5) {
							var subParts = categories[i][j]['subQuestions'].length;
							/*var subParts = 0;
							var parentId = categories[i][j]['id'];
							for(var k=0;k<categories[i].length;k++) {
								if(parentId == categories[i][k]['parentId']  && categories[i][k]['status'] == 1)
									subParts++;
							}*/
							questionType = '<span class="pull-right">(<span class="totalQuestions">' + subParts + '</span> Sub-parts)</span>DPN';
						}
						questions+= '<li class="mt-list-item done" data-id="'+categories[i][j]['id']+'" data-status="'+categories[i][j]['status']+'" data-category="'+categories[i]['id']+'" data-questionType="' + categories[i][j]['questionType'] + '">'+
	                                    '<div class="list-icon-container">'+
	                                        '<a href="javascript:;">'+
	                                        	status+
	                                            //'<i class="icon-check"></i>'+
	                                        '</a>'+
	                                    '</div>'+
	                                    '<div class="list-datetime"> <a href="javascript:void(0)" class="btn btn-danger btn-xs remove-question"><i class="fa fa-trash"></i></a> </div>'+
	                                    '<div class="list-item-content">'+
	                                        '<h3 class="uppercase">'+
	                                            '<a href="javascript:;">'+questionType+'</a>'+
	                                        '</h3>'+
	                                        '<p class="text-overflow">'+ questionText +'</p>'+
	                                    '</div>'+
	                                '</li>';
					}
				}
			}
			else {
				questions += '<li class="mt-list-item">No questions found</li>';
				$('#modalAddEditQuestionsCategory tr[data-id="'+categories[i]['id']+'"]').find('.questionTypeSelect').attr('disabled', false);
			}
			queshtml += questions;
			queshtml +=				'</ul>'+
	                            '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>';
		}
		$('#sectionQuestions').append(queshtml);
	}
}
function fetchQuestionDetails(id) {
	$('#sectionNewQuestion .cancel-button').click();
	var req = {};
	req.action = 'get-question-details';
	req.questionId = id;
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			$('#hiddenQuestionType').prop('disabled', true);
			if(res.question[0].questionType == 0)
				fillQuestionEditMcq(res);
			else if(res.question[0].questionType == 1)
				fillQuestionEditTrueFalse(res);
			else if(res.question[0].questionType == 2)
				fillQuestionEditFtb(res);
			else if(res.question[0].questionType == 3)
				fillQuestionEditMtf(res);
			else if(res.question[0].questionType == 4 || res.question[0].questionType == 5)
				fillQuestionEditCmp(res);
			else if(res.question[0].questionType == 6)
				fillQuestionEditMaq(res);
			else if(res.question[0].questionType == 7)
				fillQuestionEditAud(res);
		}
		else
			toastr.error(res.exception);
	});
}
function fillQuestionEditMcq(data) {
	if(data.question[0].status == 2) {
		var warningSpan = '<div class="text-danger" style="margin-left: 15px;margin-right: 15px;">Duplicate : Question copied from same Assignment/Exam. Edit question to save and increase question count in category.</div>';
		$(warningSpan).insertBefore('#questionId');
	}
	else
		$('#sectionNewQuestion').find('div.text-danger').remove();
	$('#sectionNewQuestion').removeClass('hide');
	$('#trueFalse').addClass('hide');
	$('#ftb').addClass('hide');
	$('.desc-block').removeClass('hide');
	$('#match').addClass('hide');
	$('#compro').addClass('hide');
	$('#optionsMul').addClass('hide');
	$('#audio').addClass('hide');
	$('#audio .js-file-path').html('');
	$('#audio .js-file-path').attr('data-path','');
	$('#options').removeClass('hide');
	$('#questionId').val(data.question[0]['id']);
	$('#hiddenQuestionType').val(data.question[0]['questionType']);
	$('#categorySelect option').hide();
	$('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').show();
	flagCategory = true;
	$('#categorySelect').val(data.question[0]['categoryId']);
	oldCategory = data.question[0]['categoryId'];
	CKEDITOR.instances['question'].setData(data.question[0]['question']);
	$('#selectDifficulty').val(data.question[0]['difficulty']);
	ms.setSelection(data.tags);
	CKEDITOR.instances['desc'].setData(data.question[0]['description']);
	if(data.question[0]['questionType'] == 0) {
		if(data.options.length > 2) {
			addOptions(data.options.length - 2);
			if($('#options input[type="text"]').length >= 6)
				$('#options .add-option').attr('disabled', true);
			else
				$('#options .add-option').attr('disabled', false);
		}
		for(var i=0; i<data.options.length; i++) {
			$('#options').find('input[type="text"]:eq('+i+')').val(data.options[i]['option']);
			var editor = $('#options').find('[type="text"]:eq('+i+')').attr('id');
			if(data.options[i]['correct'] == 1)
				prop = true;
			else
				prop = false;
			$('#options').find('input[type="radio"]:eq('+i+')').prop('checked', prop);
		}
	}
}
function fillQuestionEditMaq(data) {
	if(data.question[0].status == 2) {
		toastr.error('Duplicate : Question copied from same Assignment/Exam. Edit question to save and increse question count in category.');
	}
	$('#sectionNewQuestion').removeClass('hide');
	$('#trueFalse').addClass('hide');
	$('#ftb').addClass('hide');
	$('#match').addClass('hide');
	$('.desc-block').removeClass('hide');
	$('#compro').addClass('hide');
	$('#options').addClass('hide');
	$('#audio').addClass('hide');
	$('#audio .js-file-path').html('');
	$('#audio .js-file-path').attr('data-path', '');
	$('#optionsMul').removeClass('hide');
	$('#questionId').val(data.question[0]['id']);
	$('#hiddenQuestionType').val(data.question[0]['questionType']);
	$('#categorySelect option').hide();
	$('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').show();
	flagCategory = true;
	$('#categorySelect').val(data.question[0]['categoryId']);
	oldCategory = data.question[0]['categoryId'];
	CKEDITOR.instances['question'].setData(data.question[0]['question']);
	$('#selectDifficulty').val(data.question[0]['difficulty']);
	ms.setSelection(data.tags);
	CKEDITOR.instances['desc'].setData(data.question[0]['description']);
	if(data.question[0]['questionType'] == 6) {
		if(data.options.length > 2) {
			addOptionsMul(data.options.length - 2);
			if($('#optionsMul input[type="text"]').length >=6)
				$('#optionsMul .add-option').attr('disabled', true);
			else
				$('#optionsMul .add-option').attr('disabled', false);
		}
		for(var i=0; i<data.options.length; i++) {
			$('#optionsMul').find('input[type="text"]:eq('+i+')').val(data.options[i]['option']);
			var editor = $('#optionsMul').find('input[type="text"]:eq('+i+')').attr('id');
			if(data.options[i]['correct'] == 1)
				prop = true;
			else
				prop = false;
			$('#optionsMul').find('input[type="checkbox"]:eq('+i+')').prop('checked', prop);
		}
	}
}
function fillQuestionEditTrueFalse(data) {
	if(data.question[0].status == 2) {
		var warningSpan = '<div class="text-danger" style="margin-left: 15px;margin-right: 15px;">Duplicate : Question copied from same Assignment/Exam. Edit question to save and increse question count in category.</div>';
		$(warningSpan).insertBefore('#questionId');
	}
	else
		$('#sectionNewQuestion').find('div.text-danger').remove();
	$('#sectionNewQuestion').removeClass('hide');
	$('#trueFalse').removeClass('hide');
	$('#ftb').addClass('hide');
	$('#compro').addClass('hide');
	$('#optionsMul').addClass('hide');
	$('#match').addClass('hide');
	$('.desc-block').removeClass('hide');
	$('#options').addClass('hide');
	$('#audio').addClass('hide');
	$('#audio .js-file-path').html('');
	$('#audio .js-file-path').attr('data-path', '');
	$('#questionId').val(data.question[0]['id']);
	$('#hiddenQuestionType').val(data.question[0]['questionType']);
	$('#categorySelect option').hide();
	$('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').show();
	flagCategory = true;
	$('#categorySelect').val(data.question[0]['categoryId']);
	oldCategory = data.question[0]['categoryId'];
	CKEDITOR.instances['question'].setData(data.question[0]['question']);
	$('#selectDifficulty').val(data.question[0]['difficulty']);
	ms.setSelection(data.tags);
	CKEDITOR.instances['desc'].setData(data.question[0]['description']);
	if(data.answer.length > 0) {
		if(data.answer[0].answer == 1)
			$('#true').prop('checked', true);
		else if(data.answer[0].answer == 0)
			$('#false').prop('checked', true);
	}
}
function fillQuestionEditFtb(data) {
	if(data.question[0].status == 2) {
		var warningSpan = '<div class="text-danger" style="margin-left: 15px;margin-right: 15px;">Duplicate : Question copied from same Assignment/Exam. Edit question to save and increse question count in category.</div>';
		$(warningSpan).insertBefore('#questionId');
	}
	else
		$('#sectionNewQuestion').find('div.text-danger').remove();
	$('#sectionNewQuestion').removeClass('hide');
	$('#trueFalse').addClass('hide');
	$('#match').addClass('hide');
	$('#compro').addClass('hide');
	$('.desc-block').removeClass('hide');
	$('#optionsMul').addClass('hide');
	$('#options').addClass('hide');
	$('#audio').addClass('hide');
	$('#audio .js-file-path').html('');
	$('#audio .js-file-path').attr('data-path', '');
	$('#ftb').removeClass('hide');
	$('#questionId').val(data.question[0]['id']);
	$('#hiddenQuestionType').val(data.question[0]['questionType']);
	$('#categorySelect option').hide();
	$('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').show();
	flagCategory = true;
	$('#categorySelect').val(data.question[0]['categoryId']);
	oldCategory = data.question[0]['categoryId'];
	CKEDITOR.instances['question'].setData(data.question[0]['question']);
	$('#selectDifficulty').val(data.question[0]['difficulty']);
	ms.setSelection(data.tags);
	CKEDITOR.instances['desc'].setData(data.question[0]['description']);
	$('#question').blur();
	for(var i=0; i<data.blanks.length; i++) {
		$('#ftb').find('input[type="text"]:eq('+i+')').val(data.blanks[i]['blanks']);
		var editor = $('#ftb').find('input[type="text"]:eq('+i+')').attr('id');
	}
}
function fillQuestionEditMtf(data) {
	if(data.question[0].status == 2) {
		var warningSpan = '<div class="text-danger" style="margin-left: 15px;margin-right: 15px;">Duplicate : Question copied from same Assignment/Exam. Edit question to save and increse question count in category.</div>';
		$(warningSpan).insertBefore('#questionId');
	}
	else
		$('#sectionNewQuestion').find('div.text-danger').remove();
	$('#sectionNewQuestion').removeClass('hide');
	$('#trueFalse').addClass('hide');
	$('#ftb').addClass('hide');
	$('#options').addClass('hide');
	$('#audio').addClass('hide');
	$('#audio .js-file-path').html('');
	$('#audio .js-file-path').attr('data-path', '');
	$('#match').removeClass('hide');
	$('#questionId').val(data.question[0]['id']);
	$('#hiddenQuestionType').val(data.question[0]['questionType']);
	$('#categorySelect option').hide();
	$('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').show();
	flagCategory = true;
	$('#categorySelect').val(data.question[0]['categoryId']);
	oldCategory = data.question[0]['categoryId'];
	CKEDITOR.instances['question'].setData(data.question[0]['question']);
	$('#selectDifficulty').val(data.question[0]['difficulty']);
	ms.setSelection(data.tags);
	CKEDITOR.instances['desc'].setData(data.question[0]['description']);
	if(data.matches.length > 2) {
		$('#match .row.generated').remove();
		addOptionMatch(data.matches.length - 2);
		if($('#match input[type="text"].a').length >= 10)
			$('#match .add-option').attr('disabled', true);
		else
			$('#match .add-option').attr('disabled', false);
	}
	for(var i=0; i<data.matches.length; i++) {
		$('#match .match:eq('+i+')').find('input[type="text"].a').val(data.matches[i]['columnA']);
		var editor = $('#match .match:eq('+i+')').find('input[type="text"].a').attr('id');
		$('#match .match:eq('+i+')').find('input[type="text"].b').val(data.matches[i]['columnB']);
		editor = $('#match .match:eq('+i+')').find('input[type="text"].b').attr('id');
	}
}
function fillQuestionEditCmp(data) {
	if(data.question[0].status == 2) {
		var warningSpan = '<div class="text-danger" style="margin-left: 15px;margin-right: 15px;">Duplicate : Question copied from same Assignment/Exam. Edit question to save and increse question count in category.</div>';
		$(warningSpan).insertBefore('#questionId');
	}
	else
	$('#sectionNewQuestion').find('div.text-danger').remove();
	$('#sectionNewQuestion').removeClass('hide');
	$('#trueFalse').addClass('hide');
	$('#ftb').addClass('hide');
	if(data.question[0].questionType == 5)
		$('#sectionNewQuestion .add-question').html('<i class="fa fa-plus-circle"> Sub-parts</i>');
	else
		$('#sectionNewQuestion .add-question').html('<i class="fa fa-plus-circle"> Question</i>');
	$('#match').addClass('hide');
	$('#options').addClass('hide');
	$('#audio').addClass('hide');
	$('#audio .js-file-path').html('');
	$('#audio .js-file-path').attr('data-path', '');
	$('#compro .trueFalse').addClass('hide');
	$('#compro .ftbs').addClass('hide');
	$('#compro .optionsMul').addClass('hide');
	$('#compro').removeClass('hide');
	$('.desc-block').addClass('hide');
	$('#questionId').val(data.question[0]['id']);
	$('#hiddenQuestionType').val(data.question[0]['questionType']);
	$('#categorySelect option').hide();
	$('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').show();
	flagCategory = true;
	$('#categorySelect').val(data.question[0]['categoryId']);
	oldCategory = data.question[0]['categoryId'];
	CKEDITOR.instances['question'].setData(data.question[0]['question']);
	$('#selectDifficulty').val(data.question[0]['difficulty']);
	ms.setSelection(data.tags);
	CKEDITOR.instances['desc'].setData(data.question[0]['description']);
	$('#compro .sectionNewQuestion.generated').remove();
	$('#compro .questionTypeSelect').val(data.childQuestions[0].question.questionType);
	$('#compro .questionId').val(data.childQuestions[0].question.id);
	$('#compro .question').val(data.childQuestions[0].question.question);
	
	var editor = $('#compro .question').attr('id');
	//setCKEditor(editor, data.childQuestions[0].question.question);
	CKEDITOR.instances[editor].setData(data.childQuestions[0].question.question);
	$('#compro .desc').val(data.childQuestions[0].question.description);
	editor = $('#compro .desc').attr('id');
	//setCKEditor(editor, data.childQuestions[0].question.description);
	CKEDITOR.instances[editor].setData(data.childQuestions[0].question.description);
	if(data.childQuestions[0].question.questionType == 0) {
		$('#compro .ftbs').addClass('hide');
		$('#compro .trueFalse').addClass('hide');
		$('#compro .optionsMul').addClass('hide');
		$('#compro .options').removeClass('hide');
		if(data.childQuestions[0].answer.length > 2) {
			//addChildOptions(data.childQuestions[0].answer.length - 2, $('#compro .options .add-option').parents('.add-div'), 0);
			addChildOptions(data.childQuestions[0].answer.length - 2, $('#compro .options .add-div'), 0);
			if($('#compro .sectionNewQuestion:eq(0) .options').find('input[type="text"]').length >= 6)
				$('#compro .sectionNewQuestion:eq(0) .options .add-option').attr('disabled', true);
			else
				$('#compro .sectionNewQuestion:eq(0) .options .add-option').attr('disabled', false);
		}
		var options = $('#compro input[type="text"].option');
		var check = $('#compro input[type="radio"]');
		for(var j=0; j<data.childQuestions[0].answer.length; j++) {
			options[j]['value'] = data.childQuestions[0].answer[j].option;
			editor = options[j]['id'];
			if(data.childQuestions[0].answer[j].correct == 1)
				check[j]['checked'] = true;
		}
	}
	else if(data.childQuestions[0].question.questionType == 1) {
		$('#compro .ftbs').addClass('hide');
		$('#compro .options').addClass('hide');
		$('#compro .optionsMul').addClass('hide');
		$('#compro .trueFalse').removeClass('hide');
		if(data.childQuestions[0].answer.length > 0) {
			if(data.childQuestions[0].answer[0].answer == 1)
				$('#compro .trueFalse .true').prop('checked', true);
			if(data.childQuestions[0].answer[0].answer == 0)
				$('#compro .trueFalse .false').prop('checked', true);
		}
	}
	else if(data.childQuestions[0].question.questionType == 2) {
		$('#compro .options').addClass('hide');
		$('#compro .trueFalse').addClass('hide');
		$('#compro .optionsMul').addClass('hide');
		$('#compro .ftbs').removeClass('hide');
		$('#compro .question').blur();
		var blanks = $('#compro .ftbs input[type="text"].ftb');
		for(var j=0; j<data.childQuestions[0].answer.length; j++) {
			blanks[j]['value'] = data.childQuestions[0].answer[j].blanks;
			editor = blanks[j]['id'];
		}
	}
	else if(data.childQuestions[0].question.questionType == 6) {
		$('#compro .ftbs').addClass('hide');
		$('#compro .options').addClass('hide');
		$('#compro .trueFalse').addClass('hide');
		$('#compro .optionsMul').removeClass('hide');
		if(data.childQuestions[0].answer.length > 2)
			//addChildOptionsMul(data.childQuestions[0].answer.length - 2, $('#compro .optionsMul .add-option').parents('.add-div'));
			addChildOptionsMul(data.childQuestions[0].answer.length - 2, $('#compro .optionsMul .add-div'));
			if($('#compro .sectionNewQuestion:eq(0) .optionsMul').find('input[type="text"]').length >= 6)
				$('#compro .sectionNewQuestion:eq(0) .optionsMul .add-option').attr('disabled', true);
			else
				$('#compro .sectionNewQuestion:eq(0) .optionsMul .add-option').attr('disabled', false);
		var options = $('#compro input[type="text"].optionMul');
		var check = $('#compro input[type="checkbox"]');
		for(var j=0; j<data.childQuestions[0].answer.length; j++) {
			options[j]['value'] = data.childQuestions[0].answer[j].option;
			editor = options[j]['id'];
			if(data.childQuestions[0].answer[j].correct == 1)
				check[j]['checked'] = true;
		}
	}
	if(data.childQuestions.length > 1) {
		addChildQuestion(data.childQuestions.length - 1, 0);
		for(var i=1; i<data.childQuestions.length; i++) {
			$('#compro .sectionNewQuestion:eq('+i+')').find('.questionTypeSelect').val(data.childQuestions[i].question.questionType);
			$('#compro .sectionNewQuestion:eq('+i+')').find('.questionId').val(data.childQuestions[i].question.id);
			$('#compro .sectionNewQuestion:eq('+i+')').find('.question').val(data.childQuestions[i].question.question);
			var editor = $('#compro .sectionNewQuestion:eq('+i+')').find('.question').attr('id');
			//setCKEditor(editor, data.childQuestions[i].question.question);
			CKEDITOR.instances[editor].setData(data.childQuestions[i].question.question);
			$('#compro .sectionNewQuestion:eq('+i+')').find('.desc').val(data.childQuestions[i].question.description);
			editor = $('#compro .sectionNewQuestion:eq('+i+')').find('.desc').attr('id');
			//setCKEditor(editor, data.childQuestions[i].question.description);
			CKEDITOR.instances[editor].setData(data.childQuestions[i].question.description);
			if(data.childQuestions[i].question.questionType == 0) {
				$('#compro .sectionNewQuestion:eq('+i+')').find('.ftbs').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.optionsMul').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.trueFalse').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.options').removeClass('hide');
				if(data.childQuestions[i].answer.length > 2) {
					//addChildOptions(data.childQuestions[i].answer.length - 2, $('#compro .sectionNewQuestion:eq('+i+')').find('.options .add-option').parents('.add-div'), i);
					addChildOptions(data.childQuestions[i].answer.length - 2, $('#compro .sectionNewQuestion:eq('+i+')').find('.options .add-div'), i);
					if($('#compro .sectionNewQuestion:eq('+i+') .options').find('input[type="text"]').length >=6)
						$('#compro .sectionNewQuestion:eq('+i+') .options .add-option').attr('disabled', true);
					else
						$('#compro .sectionNewQuestion:eq(0) .options .add-option').attr('disabled', false);
				}
				var options = $('#compro .sectionNewQuestion:eq('+i+')').find('input[type="text"].option');
				var check = $('#compro .sectionNewQuestion:eq('+i+')').find('input[type="radio"]');
				for(var j=0; j<data.childQuestions[i].answer.length; j++) {
					options[j]['value'] = data.childQuestions[i].answer[j].option;
					editor = options[j]['id'];
					if(data.childQuestions[i].answer[j].correct == 1)
						check[j]['checked'] = true;
				}
			}
			else if(data.childQuestions[i].question.questionType == 1) {
				$('#compro .sectionNewQuestion:eq('+i+')').find('.ftbs').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.options').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.optionsMul').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.trueFalse').removeClass('hide');
				if(data.childQuestions[i].answer.length > 0) {
					if(data.childQuestions[i].answer[0].answer != 'undefined') {
						if(data.childQuestions[i].answer[0].answer == 1)
							$('#compro .sectionNewQuestion:eq('+i+')').find('.trueFalse .true').prop('checked', true);
						if(data.childQuestions[i].answer[0].answer == 0)
							$('#compro .sectionNewQuestion:eq('+i+')').find('.trueFalse .false').prop('checked', true);
					}
				}
			}
			else if(data.childQuestions[i].question.questionType == 2) {
				$('#compro .sectionNewQuestion:eq('+i+')').find('.options').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.trueFalse').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.optionsMul').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.ftbs').removeClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.question').blur();
				var blanks = $('#compro .sectionNewQuestion:eq('+i+')').find('.ftbs input[type="text"].ftb');
				for(var j=0; j<data.childQuestions[i].answer.length; j++) {
					blanks[j]['value'] = data.childQuestions[i].answer[j].blanks;
					editor = blanks[j]['id'];
				}
			}
			else if(data.childQuestions[i].question.questionType == 6) {
				$('#compro .sectionNewQuestion:eq('+i+')').find('.ftbs').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.options').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.trueFalse').addClass('hide');
				$('#compro .sectionNewQuestion:eq('+i+')').find('.optionsMul').removeClass('hide');
				if(data.childQuestions[i].answer.length > 2)
					//addChildOptionsMul(data.childQuestions[i].answer.length - 2, $('#compro .sectionNewQuestion:eq('+i+')').find('.optionsMul .add-option').parents('.add-div'));
					addChildOptionsMul(data.childQuestions[i].answer.length - 2, $('#compro .sectionNewQuestion:eq('+i+')').find('.optionsMul .add-div'));
					if($('#compro .sectionNewQuestion:eq('+i+') .optionsMul').find('input[type="text"]').length >=6)
						$('#compro .sectionNewQuestion:eq('+i+') .optionsMul .add-option').attr('disabled', true);
					else
						$('#compro .sectionNewQuestion:eq(0) .optionsMul .add-option').attr('disabled', false);
				var options = $('#compro .sectionNewQuestion:eq('+i+')').find('input[type="text"].optionMul');
				var check = $('#compro .sectionNewQuestion:eq('+i+')').find('input[type="checkbox"]');
				for(var j=0; j<data.childQuestions[i].answer.length; j++) {
					options[j]['value'] = data.childQuestions[i].answer[j].option;
					editor = options[j]['id'];
					if(data.childQuestions[i].answer[j].correct == 1)
						check[j]['checked'] = true;
				}
			}
		}
	}
	numbering();
}
function fillQuestionEditAud(data) {
	if(data.question[0].status == 2) {
		var warningSpan = '<div class="text-danger" style="margin-left: 15px;margin-right: 15px;">Duplicate : Question copied from same Assignment/Exam. Edit question to save and increase question count in category.</div>';
		$(warningSpan).insertBefore('#questionId');
	}
	else
		$('#sectionNewQuestion').find('div.text-danger').remove();
	$('#sectionNewQuestion').removeClass('hide');
	$('#trueFalse').addClass('hide');
	$('#ftb').addClass('hide');
	$('.desc-block').removeClass('hide');
	$('#match').addClass('hide');
	$('#compro').addClass('hide');
	$('#optionsMul').addClass('hide');
	$('#options').addClass('hide');
	$('#audio').removeClass('hide');
	$('#audio .js-file-path').html('');
	$('#audio .js-file-path').attr('data-path', '');
	$('#questionId').val(data.question[0]['id']);
	$('#hiddenQuestionType').val(data.question[0]['questionType']);
	$('#categorySelect option').hide();
	$('#categorySelect option[data-questionType="' + data.question[0].questionType + '"]').show();
	flagCategory = true;
	$('#categorySelect').val(data.question[0]['categoryId']);
	oldCategory = data.question[0]['categoryId'];
	CKEDITOR.instances['question'].setData(data.question[0]['question']);
	$('#selectDifficulty').val(data.question[0]['difficulty']);
	ms.setSelection(data.tags);
	CKEDITOR.instances['desc'].setData(data.question[0]['description']);
	if(data.question[0]['questionType'] == 7) {
		if(data.options.length > 2) {
			addAudioOptions(data.options.length - 2);
			if($('#audio input[type="text"]').length >= 6)
				$('#audio .add-option').attr('disabled', true);
			else
				$('#audio .add-option').attr('disabled', false);
		}
		for(var i=0; i<data.options.length; i++) {
			$('#audio').find('input[type="text"]:eq('+i+')').val(data.options[i]['option']);
			var editor = $('#audio').find('[type="text"]:eq('+i+')').attr('id');
			if(data.options[i]['correct'] == 1)
				prop = true;
			else
				prop = false;
			$('#audio').find('input[type="radio"]:eq('+i+')').prop('checked', prop);
		}
	}
	//$('#audio').val(data.question[0]['audio']);
	if (!!data.audio) {
		var mp3 = data.audio;
		$('.js-file-path').html(
			'<audio controls="control" id="audioShowcasePlayer" preload="none" src="'+mp3+'" type="audio/mp3"></audio>'
		);
		$('.js-file-path').attr('data-path', mp3);
		player = new MediaElementPlayer('#audioShowcasePlayer', {type: 'audio/mp3'});
		var sources = [
			{ src: mp3, type: 'audio/mp3' }
		];

		player.setSrc(sources);
		player.load();
	}
}
function addOptions(amount) {
	var html = '';
	var newIdNumber = $('#options input[type="text"]:eq(-1)').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	for(var i=0; i<amount; i++) {
		/*html = '<div class="col-lg-5 temp">'
				+ '<div class="row">'
					+ '<div class="col-xs-10">'
						+ '<span class="radio-inline" style="padding-top: 0px;">'
							+ '<input type="radio" class="top-magin" name="options">'
							+ '<input type="text" placeholder="Answer Option" class="form-control option" id="option' + newIdNumber + '">'
						+ '</span>'
					+ '</div>'
					+ '<div class="col-xs-2">'
						+ '<button class="btn btn-danger btn-xs delete-option" style="margin-top: 8px;"><i class="fa fa-times-circle"></i></button>'
					+ '</div>'
				+ '</div>'
			+ '</div>';*/
		//$(html).insertBefore('#options .add-div');
		html = '<div class="col-md-6 temp">'+
                    '<div class="form-group">'+
                        '<div class="input-group">'+
                            '<span class="input-group-addon">'+
                                '<input type="radio" name="options">'+
                                '<span></span>'+
                            '</span>'+
                            '<input type="text" class="form-control option" placeholder="Answer Option" id="option' + newIdNumber + '">'+
                            '<span class="input-group-btn">'+
                                '<button class="btn red delete-option" type="button"><i class="fa fa-trash"></i></button>'+
                            '</span>'+
                        '</div>'+
                    '</div>'+
                '</div>';
		$('#options .add-div').append(html);
		newIdNumber++;
	}
	$('.delete-option').on('click', function(e) {
		$(this).parents('.temp').remove();
		if($('#options input[type="text"]').length >=6)
			$('#options .add-option').attr('disabled', true);
		else
			$('#options .add-option').attr('disabled', false);
		return false;
	});
}
function addOptionsMul(amount) {
	var html = '';
	var newIdNumber = $('#optionsMul input[type="text"]:eq(-1)').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	for(var i=0; i<amount; i++) {
		/*html = '<div class="col-lg-5 temp">'
				+ '<div class="row">'
					+ '<div class="col-xs-10">'
						+ '<span class="checkbox-inline" style="padding-top: 0px;">'
							+ '<input type="checkbox" class="top-magin">'
							+ '<input type="text" placeholder="Answer Option" class="form-control optionMul" id="optionMul' + newIdNumber + '">'
						+ '</span>'
					+ '</div>'
					+ '<div class="col-xs-2">'
						+ '<button class="btn btn-danger btn-xs delete-option" style="margin-top: 8px;"><i class="fa fa-times-circle"></i></button>'
					+ '</div>'
				+ '</div>'
			+ '</div>';
		$(html).insertBefore('#optionsMul .add-div');*/
		html = '<div class="col-md-6 temp">'+
                    '<div class="form-group">'+
                        '<div class="input-group">'+
                            '<span class="input-group-addon">'+
                                '<input type="checkbox">'+
                                '<span></span>'+
                            '</span>'+
                            '<input type="text" class="form-control optionMul" placeholder="Answer Option" id="optionMul' + newIdNumber + '">'+
                            '<span class="input-group-btn">'+
                                '<button class="btn red delete-option" type="button"><i class="fa fa-trash"></i></button>'+
                            '</span>'+
                        '</div>'+
                    '</div>'+
                '</div>';
		$('#optionsMul .add-div').append(html);
		newIdNumber++;
	}
	$('.delete-option').on('click', function(e) {
		$(this).parents('.temp').remove();
		if($('#optionsMul input[type="text"]').length >=6)
			$('#optionsMul .add-option').attr('disabled', true);
		else
			$('#optionsMul .add-option').attr('disabled', false);
		return false;
	});
}
function addAudioOptions(amount) {
	var html = '';
	var newIdNumber = $('#audio input[type="text"]:eq(-1)').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	for(var i=0; i<amount; i++) {
		/*html = '<div class="col-lg-5 temp">'
				+ '<div class="row">'
					+ '<div class="col-xs-10">'
						+ '<span class="radio-inline" style="padding-top: 0px;">'
							+ '<input type="radio" class="top-magin" name="options">'
							+ '<input type="text" placeholder="Answer Option" class="form-control option" id="option' + newIdNumber + '">'
						+ '</span>'
					+ '</div>'
					+ '<div class="col-xs-2">'
						+ '<button class="btn btn-danger btn-xs delete-option" style="margin-top: 8px;"><i class="fa fa-times-circle"></i></button>'
					+ '</div>'
				+ '</div>'
			+ '</div>';
		$(html).insertBefore('#audio .add-div');*/
		html = '<div class="col-md-6 temp">'+
                    '<div class="form-group">'+
                        '<div class="input-group">'+
                            '<span class="input-group-addon">'+
                                '<input type="radio">'+
                                '<span></span>'+
                            '</span>'+
                            '<input type="text" class="form-control optionAud" placeholder="Answer Option" id="optionAud' + newIdNumber + '">'+
                            '<span class="input-group-btn">'+
                                '<button class="btn red delete-option" type="button"><i class="fa fa-trash"></i></button>'+
                            '</span>'+
                        '</div>'+
                    '</div>'+
                '</div>';
		$('#audio .add-div').append(html);
		newIdNumber++;
	}
	$('.delete-option').on('click', function(e) {
		$(this).parents('.temp').remove();
		if($('#audio input[type="text"]').length >=6)
			$('#audio .add-option').attr('disabled', true);
		else
			$('#audio .add-option').attr('disabled', false);
	});
}
function addChildOptions(amount, which, number) {
	var html = '';
	var flag;
	var newIdNumber = which.parents('.options').find('input[type="text"]:eq(-1)').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	for(var i=0; i<amount; i++) {
		/*html = '<div class="col-lg-5 temp">'
				+ '<div class="row">'
					+ '<div class="col-xs-10">'
						+ '<span class="radio-inline" style="padding-top: 0px;">'
							+ '<input type="radio" class="top-magin" name="childOptions' + number + '">'
							+ '<input type="text" placeholder="Option" class="form-control option" id="childOption' + number + '' + newIdNumber + '">'
						+ '</span>'
					+ '</div>'
					+ '<div class="col-xs-2">'
						+ '<button class="btn btn-danger btn-xs delete-option" style="margin-top: 8px;"><i class="fa fa-times-circle"></i></button>'
					+ '</div>'
				+ '</div>'
			+ '</div>';
		$(html).insertBefore(which);*/
		html = '<div class="col-md-6 temp">'+
                    '<div class="form-group">'+
                        '<div class="input-group">'+
                            '<span class="input-group-addon">'+
                                '<input type="radio" name="childOptions' + number + '">'+
                                '<span></span>'+
                            '</span>'+
                            '<input type="text" class="form-control option" placeholder="Answer Option" id="childOption' + number + '' + newIdNumber + '">'+
                            '<span class="input-group-btn">'+
                                '<button class="btn red delete-option" type="button"><i class="fa fa-trash"></i></button>'+
                            '</span>'+
                        '</div>'+
                    '</div>'+
                '</div>';
		$(which).append(html);
		newIdNumber++;
	}
	$(which).closest('.options').find('.delete-option').on('click', function(e) {
		if($(this).parents('.options input[type="text"]').length-1 >= 6)
			$(this).parents('.options').find('.add-option').attr('disabled', true);
		else
			$(this).parents('.options').find('.add-option').attr('disabled', false);
		$(this).parents('.temp').remove();
		return false;
	});
}
function addChildOptionsMul(amount, which) {
	var html = '';
	var flag;
	var number = which.parents('.optionsMul').find('input[type="text"]:eq(-1)').attr('id');
	number = parseInt(number.substring(number.length-2, number.length-1)) + 1;
	var newIdNumber = which.parents('.optionsMul').find('input[type="text"]:eq(-1)').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	for(var i=0; i<amount; i++) {
		/*html = '<div class="col-lg-5 temp">'
				+ '<div class="row">'
					+ '<div class="col-xs-10">'
						+ '<span class="checkbox-inline" style="padding-top: 0px;">'
							+ '<input type="checkbox" class="top-magin">'
							+ '<input type="text" placeholder="Option" class="form-control optionMul" id="childOptionMul' + number + '' + newIdNumber + '">'
						+ '</span>'
					+ '</div>'
					+ '<div class="col-xs-1">'
						+ '<button class="btn btn-danger btn-xs delete-option" style="margin-top: 8px;"><i class="fa fa-times-circle"></i></button>'
					+ '</div>'
				+ '</div>'
			+ '</div>';
		$(html).insertBefore(which);*/
		html = '<div class="col-md-6 temp">'+
                    '<div class="form-group">'+
                        '<div class="input-group">'+
                            '<span class="input-group-addon">'+
                                '<input type="checkbox">'+
                                '<span></span>'+
                            '</span>'+
                            '<input type="text" class="form-control optionMul" placeholder="Answer Option" id="childOptionMul' + number + '' + newIdNumber + '">'+
                            '<span class="input-group-btn">'+
                                '<button class="btn red delete-option" type="button"><i class="fa fa-trash"></i></button>'+
                            '</span>'+
                        '</div>'+
                    '</div>'+
                '</div>';
		$(which).append(html);
		newIdNumber++;
	}
	$(which).closest('.optionsMul').find('.delete-option').on('click', function(e) {
		if($(this).parents('.optionsMul input[type="text"]').length-1 >= 6)
			$(this).parents('.optionsMul').find('.add-option').attr('disabled', true);
		else
			$(this).parents('.optionsMul').find('.add-option').attr('disabled', false);
		$(this).parents('.temp').remove();
		return false;
	});
}
function addChildQuestion(amount, number) {
	var html = '';
	var flag;
	var newIdNumber = $('#compro .sectionNewQuestion:eq(-1)').find('.question').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	if(number == 0)
		flag = true;
	else
		flag = false;
	for(var i=0; i<amount; i++) {
		if(flag)
			number = i + 1;
		/*html = '<div class="panel sectionNewQuestion generated">'
				+ '<form>'
					+ '<div class="col-lg-12">Please select the question type</div>'
					+ '<input type="hidden" class="questionId" value="0">'
					+ '<div class="row panel-body">'
						+ '<div class="col-lg-1">'
							+ '<span class="subQuestionNumbers"></span>'
						+ '</div>'
						+ '<div class="col-lg-6">'
							+ '<select class="form-control input-sm questionTypeSelect">'
								+ '<option value=0>Multiple Choice Question</option>'
								+ '<option value=1>True or False</option>'
								+ '<option value=2>Fill in the blanks</option>'
								+ '<option value=6>Multiple Answer Questions</option>'
							+ '</select>'
						+ '</div>'
						+ '<div class="col-lg-5">'
							+'<span style="color: red;display: none;" class="marks"></span>'
							+ '<button type="button" class="btn btn-danger pull-right remove-question"><i class="fa fa-times-circle"></i></button>'
						+ '</div>'
					+ '</div>'
					+ '<div class="row panel-body">'
						+ '<div class="col-lg-12">'
							+ '<textarea placeholder="Please enter the question text here. The passage is already written above." rows="2" class="form-control  t-text-area question" id="childQuestion' + newIdNumber + '"></textarea>'
						+ '</div>'
					+ '</div>'
					+ '<div class="row panel-body options">'
						+ '<div class="col-lg-5">'
							+ '<div class="row">'
								+ '<div class="col-xs-10">'
									+ '<span class="radio-inline" style="padding-top: 0px;">'
										+ '<input type="radio" class="top-magin" name="childOptions' + (number) + '">'
										+ '<input type="text" placeholder="Option" class="form-control option" id="childOption'+number+'0">'
									+ '</span>'
								+ '</div>'
								+ '<div class="col-xs-2">'
								+ '</div>'
							+ '</div>'
						+ '</div>'
						+ '<div class="col-lg-5">'
							+ '<div class="row">'
								+ '<div class="col-xs-10">'
									+ '<span class="radio-inline" style="padding-top: 0px;">'
										+ '<input type="radio" class="top-magin" name="childOptions' + (number) + '">'
										+ '<input type="text" placeholder="Option" class="form-control option" id="childOption'+number+'1">'
									+ '</span>'
								+ '</div>'
								+ '<div class="col-xs-2">'
								+ '</div>'
							+ '</div>'
						+ '</div>'
						+ '<div class="col-lg-2 add-div">'
							+ '<button type="button" class="btn btn-primary pull-right add-option"><i class="fa fa-plus-circle"></i></button>'
						+ '</div>'
					+ '</div>'
					+ '<div class="row panel-body ftbs">'
						+ '<div class="col-lg-5">'
							+ '<input type="text" placeholder="Answer" class="form-control ftb" id="childftb'+number+'0">'
						+ '</div>'
					+ '</div>'
					+ '<div class="row panel-body trueFalse">'
						+ '<div class="col-lg-5">'
							+ '<label class="checkbox-inline" style="padding-top: 0px;">'
								+ '<input type="radio" class="top-magin true" name="trueFalse"> True'
							+ '</label>'
						+ '</div>'
						+ '<div class="col-lg-5">'
							+ '<label class="checkbox-inline">'
								+ '<input type="radio" class="top-magin false" name="trueFalse"> False'
							+ '</label>'
						+ '</div>'
					+ '</div>'
					+ '<div class="row panel-body optionsMul">'
						+ '<div class="col-lg-5">'
							+ '<div class="row">'
								+ '<div class="col-xs-10">'
									+ '<span class="checkbox-inline" style="padding-top: 0px;">'
										+ '<input type="checkbox" class="top-magin">'
										+ '<input type="text" placeholder="Option" class="form-control optionMul" id="childOptionMul'+number+'0">'
									+ '</span>'
								+ '</div>'
								+ '<div class="col-xs-2">'
								+ '</div>'
							+ '</div>'
						+ '</div>'
						+ '<div class="col-lg-5">'
							+ '<div class="row">'
								+ '<div class="col-xs-10">'
									+ '<span class="checkbox-inline" style="padding-top: 0px;">'
										+ '<input type="checkbox" class="top-magin">'
										+ '<input type="text" placeholder="Option" class="form-control optionMul" id="childOptionMul'+number+'1">'
									+ '</span>'
								+ '</div>'
								+ '<div class="col-xs-2">'
								+ '</div>'
							+ '</div>'
						+ '</div>'
						+ '<div class="col-lg-2 add-div">'
							+ '<button type="button" class="btn btn-primary pull-right add-option"><i class="fa fa-plus-circle"></i></button>'
						+ '</div>'
					+ '</div>'
					+ '<div class="row panel-body">'
						+ '<div class="col-lg-12">'
							+ '<textarea class="form-control t-text-area desc" rows="2" placeholder="Please enter the explanation of the question" id="childDesc' + newIdNumber + '"></textarea>'
						+ '</div>'
					+ '</div>'
				+ '</form>'
			+ '</div>';
		$(html).insertBefore('#compro .lastCol');
		ckeditorOn('childQuestion' + newIdNumber);
		ckeditorOn('childDesc' + newIdNumber);*/

		html = '<div class="well well-sm sectionNewQuestion generated">'+
                    '<input type="hidden" class="questionId" value="0">'+
                    '<div class="form-body">'+
                        '<input type="hidden" id="hiddenQuestionType" value="0">'+
                        '<div class="form-group">'+
                            '<label class="control-label">Category :</label>'+
                            '<div class="row">'+
                                '<div class="col-md-6">'+
                                    '<select class="form-control questionTypeSelect">'+
                                        '<option value="0">Multiple Choice Question</option>'+
                                        '<option value="1">True or False</option>'+
                                        '<option value="2">Fill in the blanks</option>'+
                                        '<option value="6">Multiple Answer Questions</option>'+
                                    '</select>'+
                                '</div>'+
                                '<div class="col-md-6">'+
                                    '<div class="marks text-info"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="form-group">'+
                            '<label class="control-label">Question :</label>'+
                            '<textarea placeholder="Please enter the question text here. The passage is already written above." rows="2" class="form-control ckeditor question" id="childQuestion' + newIdNumber + '"></textarea>'+
                        '</div>'+
                        '<div class="form-group options">'+
                            '<label class="control-label">Only one answer option is correct :</label>'+
                            '<div class="row">'+
                                '<div class="col-md-10">'+
                                    '<div class="row">'+
                                        '<div class="col-md-6">'+
                                            '<div class="form-group">'+
                                                '<div class="input-group">'+
                                                    '<span class="input-group-addon">'+
                                                        '<input type="radio" name="childOptions' + (number) + '">'+
                                                        '<span></span>'+
                                                    '</span>'+
                                                    '<input type="text" class="form-control option" id="childOption'+number+'0" placeholder="Answer Option">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6">'+
                                            '<div class="form-group">'+
                                                '<div class="input-group">'+
                                                    '<span class="input-group-addon">'+
                                                        '<input type="radio" name="childOptions' + (number) + '">'+
                                                        '<span></span>'+
                                                    '</span>'+
                                                    '<input type="text" class="form-control option" id="childOption'+number+'1" placeholder="Answer Option">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="row add-div"></div>'+
                                '</div>'+
                                '<div class="col-md-2">'+
                                    '<button class="btn btn-info add-option"><i class="fa fa-plus"></i></button>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="form-group trueFalse">'+
                            '<label class="control-label">Only one answer option is correct :</label>'+
                            '<div class="mt-radio-inline">'+
                                '<label class="mt-radio">'+
                                    '<input type="radio" class="true" name="trueFalse'+number+'"> True'+
                                    '<span></span>'+
                                '</label>'+
                                '<label class="mt-radio">'+
                                    '<input type="radio" class="false" name="trueFalse'+number+'"> False'+
                                    '<span></span>'+
                                '</label>'+
                            '</div>'+
                        '</div>'+
                        '<div class="form-group ftbs">'+
                            '<label class="control-label">Please write the correct answer :</label>'+
                            '<div class="row">'+
                                '<div class="col-md-6">'+
                                    '<input type="text" placeholder="Please write the answer" class="form-control ftb" id="childftb'+number+'0">'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="form-group optionsMul">'+
                            '<label class="control-label">More than one answer option can be correct :</label>'+
                            '<div class="row">'+
                                '<div class="col-md-10">'+
                                    '<div class="row">'+
                                        '<div class="col-md-6">'+
                                            '<div class="form-group">'+
                                                '<div class="input-group">'+
                                                    '<span class="input-group-addon">'+
                                                        '<input type="checkbox">'+
                                                        '<span></span>'+
                                                    '</span>'+
                                                    '<input type="text" class="form-control optionMul" id="childOptionMul'+number+'0" placeholder="Answer Option">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6">'+
                                            '<div class="form-group">'+
                                                '<div class="input-group">'+
                                                    '<span class="input-group-addon">'+
                                                        '<input type="checkbox">'+
                                                        '<span></span>'+
                                                    '</span>'+
                                                    '<input type="text" class="form-control optionMul" id="childOptionMul'+number+'1" placeholder="Answer Option">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="row add-div"></div>'+
                                '</div>'+
                                '<div class="col-md-2">'+
                                    '<button class="btn btn-info add-option"><i class="fa fa-plus"></i></button>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="form-group">'+
                            '<label class="control-label">Explanation :</label>'+
                            '<textarea class="form-control ckeditor desc" rows="2" placeholder="Please enter the explanation of the question" id="childDesc' + newIdNumber + '"></textarea>'+
                        '</div>'+
                    '</div>'+
                '</div>';
		$(html).insertBefore('#compro .lastCol');
		//$( 'textarea.ckeditor' ).ckeditor();
		addCKEditor('childQuestion' + newIdNumber);
		addCKEditor('childDesc' + newIdNumber);
		newIdNumber++;
		/*ckeditorOnOption('childOption' + number + '0');
		ckeditorOnOption('childOption' + number + '1');
		ckeditorOnOption('childOptionMul' + number + '0');
		ckeditorOnOption('childOptionMul' + number + '1');
		ckeditorOnOption('childftb' + number + '0');
		*/
	}
	$('#compro .sectionNewQuestion:eq(-1)').find('.ftbs, .trueFalse, .optionsMul').addClass('hide');
	$('#compro .questionTypeSelect').on('change', function() {
		elem = $(this);
		if($(this).val() == 0) {
			elem.parents('.sectionNewQuestion').find('.ftbs').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.trueFalse').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.optionsMul').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.options').removeClass('hide');
		}
		else if($(this).val() == 1) {
			elem.parents('.sectionNewQuestion').find('.ftbs').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.options').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.optionsMul').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.trueFalse').removeClass('hide');
		}
		else if($(this).val() == 2) {
			elem.parents('.sectionNewQuestion').find('.trueFalse').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.options').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.optionsMul').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.ftbs').removeClass('hide');
		}
		else if($(this).val() == 6) {
			elem.parents('.sectionNewQuestion').find('.trueFalse').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.options').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.ftbs').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.optionsMul').removeClass('hide');
		}
	});
	$('#compro .question').off('blur');
	$('#compro .question').on('blur', function() {
		if($(this).parents('.sectionNewQuestion').find('.questionTypeSelect').val() == 2)
			addChildBlanks(occurrences($(this).parents('.sectionNewQuestion').find('.question').val(), '____'), $(this));
	});
	$('#compro .add-option').off('click');
	$('#compro .options .add-option').on('click', function() {
		which = $(this).parents('.sectionNewQuestion').find('.options .add-div');
		var temp = $(this).parents('.options').find('input[type="radio"]:eq(0)').attr('name');
		var number = temp.substring(temp.length-1);
		addChildOptions(1, which, number);
		if($(this).parents('.sectionNewQuestion .options').find('input[type="text"]').length >= 6)
			$(this).parents('.sectionNewQuestion').find('.options .add-option').attr('disabled', true);
		else
			$(this).parents('.sectionNewQuestion').find('.options .add-option').attr('disabled', false);
		return false;
	});
	$('#compro .optionsMul .add-option').on('click', function() {
		which = $(this).parents('.sectionNewQuestion').find('.optionsMul .add-div');
		addChildOptionsMul(1, which);
		if($(this).parents('.sectionNewQuestion .optionsMul').find('input[type="text"]').length >= 6)
			$(this).parents('.sectionNewQuestion').find('.optionsMul .add-option').attr('disabled', true);
		else
			$(this).parents('.sectionNewQuestion').find('.optionsMul .add-option').attr('disabled', false);
		return false;
	});
	$('#compro .remove-question').off('click');
	$('#compro .remove-question').on('click', function() {
		$(this).parents('.sectionNewQuestion').remove();
		numbering();
		return false;
	});
}
function addCKEditor(elem) {
	CKEDITOR.replace( elem );
}
function addChildBlanks(amount, where) {
	var html = '';
	where.parents('.sectionNewQuestion').find('.temp').remove();
	var number = where.parents('.sectionNewQuestion').find('.ftbs input[type="text"]:eq(-1)').attr('id');
	number = parseInt(number.substring(number.length-2, number.length-1)) + 1;
	var newIdNumber = where.parents('.sectionNewQuestion').find('.ftbs input[type="text"]:eq(-1)').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	for(var i=0; i<(amount-1); i++) {
		html = '<div class="col-lg-5 temp">'
					+ '<input type="text" type="text" placeholder="Answer" class="form-control ftb" id="childftb'+number+''+newIdNumber+'">'
				+ '</div>';
		where.parents('.sectionNewQuestion').find('.ftbs').append(html);
		//ckeditorOnOption('childftb' + number + '' + newIdNumber);
		newIdNumber++;
	}
}
function addOptionMatch(amount) {
	var html = '';
	var newIdNumber = $('#match input[type="text"].a:eq(-1)').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	for(var i=0; i<amount; i++) {
		html = '<div class="match row generated margin-bottom-10">'+
					'<div class="col-xs-5 temp">'+
						'<input type="text" class="form-control a" placeholder="Enter option A here" id="mtfOptionA' + newIdNumber + '">'+
					'</div>'+
					'<div class="col-xs-5 temp">'+
						'<input type="text" class="form-control b" placeholder="Correct answer for column A" id="mtfOptionB' + newIdNumber + '">'+
					'</div>'+
					'<div class="col-xs-2" temp">'+
						'<button class="btn btn-danger delete-mtf"><i class="fa fa-times-circle"></i></button>'+
					'</div>'+
				'</div>';
		$('#match .custom-match').append(html);
		//ckeditorOnOption('mtfOptionA' + newIdNumber);
		//ckeditorOnOption('mtfOptionB' + newIdNumber);
		newIdNumber++;
	}
	$('#match .delete-mtf').on('click', function() {
		$(this).parents('.generated:eq(0)').remove();
		if($('#match input[type="text"].a').length >= 10)
			$('#match .add-option').attr('disabled', true);
		else
			$('#match .add-option').attr('disabled', false);
		return false;
	});
}
function addBlanks(amount) {
	var html = '';
	$('#ftb').find('.temp').remove();
	var newIdNumber = $('#ftb input[type="text"]:eq(-1)').attr('id');
	newIdNumber = parseInt(newIdNumber.substring(newIdNumber.length-1)) + 1;
	for(var i=0; i<(amount-1); i++) {
		html = '<div class="col-lg-5 temp">'
					+ '<input type="text" placeholder="Please write the answer" class="form-control ftb" id="ftbOption' + newIdNumber + '">'
				+ '</div>';
		$('#ftb').append(html);
		//ckeditorOnOption('ftbOption' + newIdNumber);
		newIdNumber++;
	}
}
function occurrences(string, subString) {
	string+=""; subString+="";
	if(subString.length<=0) return string.length+1;

	var n=0, pos=0;
	var step=subString.length;

	while(true) {
		pos=string.indexOf(subString,pos);
		if(pos>=0) {
			n++;
			pos+=step;
		}
		else
			break;
	}
	return(n);
}
function numbering() {
	var val = $('#categorySelect').val();
	var questionType = $('#categorySelect').find('option[value="'+val+'"]').attr('data-questionType');
	if(questionType == 4 || questionType == 5) {
		for(var i=0; i<$('.sectionNewQuestion').length; i++) {
			if(questionType == 4)
				$('.subQuestionNumbers:eq('+i+')').text((i+1) + '. ');
			else
				$('.subQuestionNumbers:eq('+i+')').text(letters[i] + '. ');
		}
		$('.subQuestionNumbers').show();
	}
}
function newQuestionOps() {
	$('#sectionNewQuestion').removeClass('hide');
	$('#categorySelect option').removeClass('hide');
	$('#sectionNewQuestion .cancel-button').click();
	$('#questionId').val(0);
	$('#hiddenQuestionType').prop('disabled', false);
	$('.js-file-path').html('').attr('data-path', '');
}
function changeCategory(value) {
	//destroying the ckeditor of question
	$('#qq').text('Question');
	//$('div[aria-describedby="cke_43"]').html('Please enter the question text here');
	$('.add-option').attr('disabled', false);
	$('#questionInfo').addClass('hide');
	$('.subQuestionNumbers').hide();
	$('.temp').remove();
	numbering();
	//$('#hiddenQuestionType').val(categoryQuestionType[value]);
	$('#eq').show();
	var type = value;
	if(type === '0') {
		$('.desc-block').removeClass('hide');
		$('#trueFalse').addClass('hide');
		$('#ftb').addClass('hide');
		$('#match').addClass('hide');
		$('#compro').addClass('hide');
		$('#optionsMul').addClass('hide');
		$('#audio').addClass('hide');
		$('#options').removeClass('hide');
		$('#questionInfo').attr('data-original-title', 'Multiple Choice Questions are the  most common type of questions used across exams<br>They only have one answer option as the correct answer').removeClass('hide');
	}
	else if(type == 1) {
		$('.desc-block').removeClass('hide');
		$('#options').addClass('hide');
		$('#ftb').addClass('hide');
		$('#compro').addClass('hide');
		$('#optionsMul').addClass('hide');
		$('#match').addClass('hide');
		$('#audio').addClass('hide');
		$('#trueFalse').removeClass('hide');
		$('#questionInfo').attr('data-original-title', 'True or False type of Questions  generally have a statement which is either true or false. please use the radio button to select the correct answer').removeClass('hide');
	}
	else if(type == 2) {
		$('.desc-block').removeClass('hide');
		$('#options').addClass('hide');
		$('#trueFalse').addClass('hide');
		$('#match').addClass('hide');
		$('#compro').addClass('hide');
		$('#optionsMul').addClass('hide');
		$('#audio').addClass('hide');
		$('#ftb').removeClass('hide');
		$('#questionInfo').attr('data-original-title', 'Fill in the blanks have few words/word, which are missing. Please write the exact/words in the box provided<br>'+terminologies["student_plural"]+' also will  have to write down the exact word/words.').removeClass('hide');
	}
	else if(type == 3) {
		$('.desc-block').removeClass('hide');
		$('#options').addClass('hide');
		$('#trueFalse').addClass('hide');
		$('#ftb').addClass('hide');
		$('#compro').addClass('hide');
		$('#optionsMul').addClass('hide');
		$('#audio').addClass('hide');
		$('#match').removeClass('hide');
		$('#questionInfo').attr('data-original-title', 'Match the Following basically consists of 2 columns where '+terminologies["student_plural"].toLowerCase()+' have to match the statements on both the sides<br>We have 2 columns A and B, such that every entry on A side has its corresponding entry on the B side. '+terminologies["student_plural"]+' will see the entries on the B side in jumbled order').removeClass('hide');
	}
	else if(type == 4 || type == 5) {
		$('div[aria-describedby="cke_43"]').html('Please enter the passage here. A Passage could be a few paragraphs or a problem statement.');
		if(type == 5) {
			$('#sectionNewQuestion .add-question').html('<i class="fa fa-plus-circle"> Sub-parts</i>');
			$('#questionInfo').attr('data-original-title', 'This question type has multiple sub parts.The marks allotted for this question type will be equally divided among all the sub parts. So, if  there is a dependent question of total  5 marks, with 2 sub parts, then each sub part will have 2.5 marks. The total score of the '+terminologies["student_single"].toLowerCase()+' will be a sum  of all the  sub parts of the question(dependent on all the sub parts)').removeClass('hide');
		}
		else {
			$('#sectionNewQuestion .add-question').html('<i class="fa fa-plus-circle"> Question</i>');
			$('#questionInfo').attr('data-original-title', 'This question type consists of a  common passage/ paragraphs, followed by a couple of independent questions. This passage/paragraphs will  appear every time a question appears to the '+terminologies["student_single"].toLowerCase()).removeClass('hide');
		}
		$('#qq').text('Passage/Paragraph');
		$('#eq').hide();
		$('.desc-block').addClass('hide');
		$('#options').addClass('hide');
		$('#optionsMul').addClass('hide');
		$('#trueFalse').addClass('hide');
		$('#ftb').addClass('hide');
		$('#match').addClass('hide');
		$('#audio').addClass('hide');
		////$('#desc').hide();
		$('#compro .ftbs').addClass('hide');
		$('#compro .trueFalse').addClass('hide');
		$('#compro .optionsMul').addClass('hide');
		$('#compro').removeClass('hide');
	}
	else if(type == 6) {
		$('.desc-block').removeClass('hide');
		$('#options').addClass('hide');
		$('#trueFalse').addClass('hide');
		$('#ftb').addClass('hide');
		$('#compro').addClass('hide');
		$('#match').addClass('hide');
		$('#audio').addClass('hide');
		$('#optionsMul').removeClass('hide');
		$('#questionInfo').attr('data-original-title', 'These type of questions can have more than one answer option as the correct answer. Please use the check boxes to tick more than one correct option').removeClass('hide');
	}
	else if(type == 7) {
		$('.desc-block').removeClass('hide');
		$('#trueFalse').addClass('hide');
		$('#ftb').addClass('hide');
		$('#match').addClass('hide');
		$('#compro').addClass('hide');
		$('#optionsMul').addClass('hide');
		$('#options').addClass('hide');
		$('#audio').removeClass('hide');
		$('#questionInfo').attr('data-original-title', 'Audio Questions have an audio clip in the question which you have to hear and answer').removeClass('hide');
	}
}
$(document).ready(function(){
	$('#hiddenQuestionType').on('change', function() {
		changeCategory($(this).val());
	});

	$('#sectionQuestions').on('click', '.mt-list-item', function() {
		if($(this).attr('data-id') != undefined) {
			$('.mt-list-item.done.active').removeClass('active');
			$(this).addClass('active');
			fetchQuestionDetails($(this).attr('data-id'));
		}
	});


	$('#sectionQuestions').on('click', '.remove-question', function(e) {
		e.stopPropagation();
		if(parseInt($('#examStatus').attr('data-assoc')) > 0) {
			var category = $(this).parents('.mt-list-item').attr('data-category');
			if(parseInt($('#sectionTable tbody').find('tr[data-id="'+category+'"] .required').text()) == parseInt($('#sectionTable tbody').find('tr[data-id="'+category+'"] .entered').text())) {
				toastr.error('You can\'t cross the minimum required question limit as there are '+terminologies["student_plural"].toLowerCase()+' associated with this exam. Please remove them in order to delete any other question.')
				return;
			}
		}
		var con = false;
		if($('#examStop').length != 0) {
			var category = $(this).parents('.mt-list-item').attr('data-category');
			if(parseInt($('#sectionTable tbody').find('tr[data-id="'+category+'"] .required').text()) == parseInt($('#sectionTable tbody').find('tr[data-id="'+category+'"] .entered').text())) {
				con = confirm("State of your Assignment/Exam will be changed to draft as it crosses minimum specified criteria. Are you sure you want to delete this question?");
			}
			else
				con = confirm("Are you sure to delete this question");
		}
		else
			con = confirm("Are you sure to delete this question");
		if(con) {
			$('#sectionNewQuetion .cancel-button').click();
			var req = {};
			req.action = 'delete-question';
			req.questionId = $(this).parents('.mt-list-item').attr('data-id');
			req.status = $(this).parents('.mt-list-item').attr('data-status');
			req.questionType = $(this).parents('.mt-list-item').attr('data-questionType');
			if(req.questionType == 4)
				req.amount = $(this).parents('.mt-list-item').find('.totalQuestions').text();
			//req.categoryId = $(this).parents('.mt-list-item').attr('data-category');
			req.sectionId = $('#sectionName').attr('data-id');
			req.subjectId = subjectId;
			$('#sectionNewQuestion .cancel-button').click();
			$.ajax({
				'type'	: 'post',
				'url'	: ApiEndPoint,
				'data'	: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else if(res.status == 1) {
					fetchSubjectQuestions();
					newQuestionOps();
				}
			});
		}
	});

	$('#compro .question').on('blur', function() {
		if($(this).parents('.sectionNewQuestion').find('.questionTypeSelect').val() == 2)
			addChildBlanks(occurrences($(this).parents('.sectionNewQuestion').find('.question').val(), '____'), $(this));
	});
	
	$('#options .add-option').on('click', function() {
		addOptions(1);
		if($('#options input[type="text"]').length >= 6)
			$('#options .add-option').attr('disabled', true);
		else
			$('#options .add-option').attr('disabled', false);
		return false;
	});
	
	$('#match .add-option').on('click', function() {
		addOptionMatch(1);
		if($('#match input[type="text"].a').length >= 10)
			$('#match .add-option').attr('disabled', true);
		else
			$('#match .add-option').attr('disabled', false);
		return false;
	});
	
	$('#optionsMul .add-option').on('click', function() {
		addOptionsMul(1);
		if($('#optionsMul input[type="text"]').length >= 6)
			$('#optionsMul .add-option').attr('disabled', true);
		else
			$('#optionsMul .add-option').attr('disabled', false);
		return false;
	});

	$('#audio .add-option').on('click', function() {
		addAudioOptions(1);
		if($('#audio input[type="text"]').length >= 6)
			$('#audio .add-option').attr('disabled', true);
		else
			$('#audio .add-option').attr('disabled', false);
		return false;
	});
	
	$('#compro .options .add-option').on('click', function() {
		which = $(this).parents('.sectionNewQuestion').find('.options .add-div');
		addChildOptions(1, which, 0);
		if($(this).parents('.sectionNewQuestion .options').find('input[type="text"]').length >= 6)
			$(this).parents('.sectionNewQuestion').find('.options .add-option').attr('disabled', true);
		else
			$(this).parents('.sectionNewQuestion').find('.options .add-option').attr('disabled', false);
		return false;
	});
	
	$('#compro .optionsMul .add-option').on('click', function() {
		which = $(this).parents('.sectionNewQuestion').find('.optionsMul .add-div');
		addChildOptionsMul(1, which);
		if($(this).parents('.sectionNewQuestion .optionsMul').find('input[type="text"]').length >= 6)
			$(this).parents('.sectionNewQuestion').find('.optionsMul .add-option').attr('disabled', true);
		else
			$(this).parents('.sectionNewQuestion').find('.optionsMul .add-option').attr('disabled', false);
		return false;
	});
	
	$('#compro .add-question').on('click', function() {
		//taking the previous question number
		var temp = $(this).parents('#compro').find('.sectionNewQuestion:eq(-1) .subQuestionNumbers').text();
		var number = temp.substring(0, 1);
		//converting letter into number for DPN questions
		if($('#hiddenQuestionType').val() == 5)
			number = letters.indexOf(number) + 1;
		number = parseInt(number);
		addChildQuestion(1, number);
		numbering();
		return false;
	});
	
	$('#sectionNewQuestion .cancel-button').on('click', function() {
		/*setTimeout(function() {
			if(CKEDITOR.instances['question'])
				CKEDITOR.instances['question'].destroy(true);
			$('#question').attr('placeholder', 'Please enter the question text here');
			ckeditorOn('question');
		}, 1500);*/
		$('div[aria-describedby="cke_43"]').html('Please enter the question text here');
		$('.add-option').attr('disabled', false);
		$('.temp').remove();
		$('.help-block').remove();
		$('#sectionNewQuestion').find('div.text-danger').remove();

		if ($('.sectionNewQuestion.generated .question').length>0) {
			$('.sectionNewQuestion.generated .question').each(function(){
				var childQuestionId = $(this).attr('id');
				CKEDITOR.instances[childQuestionId].destroy();
			});
		}
		if ($('.sectionNewQuestion.generated .desc').length>0) {
			$('.sectionNewQuestion.generated .desc').each(function(){
				var childDescId = $(this).attr('id');
				CKEDITOR.instances[childDescId].destroy();
			});
		}

		$('#sectionNewQuestion .generated').remove();
		$('#trueFalse').addClass('hide');
		$('#questionId').val(0);
		$('#ftb').addClass('hide');
		$('#question').val('');
		$('#selectDifficulty').val(0);
		ms.clear();
		$('#match').addClass('hide');
		$('#compro').addClass('hide');
		$('#optionsMul').addClass('hide');
		$('#audio').addClass('hide');
		$('#compro .ftbs').addClass('hide');
		$('#compro .trueFalse').addClass('hide');
		$('#compro .questionTypeSelect').val(0);
		$('#hiddenQuestionType').val(0);
		$('#compro .question').val('');
		////$('#desc').val('');
		$('#categorySelect').find('option').show();
		$('#compro .desc').val('');
		$('#categorySelect').val(0);
		flagCategory = true;
		$('.true, .false, #true, #false').prop('checked', false);
		$('#compro .options').show();
		$('#options').removeClass('hide');
		$('#sectionNewQuestion').find('input[type="text"]').val('');
		$('#sectionNewQuestion').find('input[type="radio"]').prop('checked', false);
		$('#sectionNewQuestion').find('input[type="checkbox"]').prop('checked', false);
		//removing text from ckeditor textboxes
		for(instance in CKEDITOR.instances) {
			CKEDITOR.instances[instance].setData('');
			CKEDITOR.instances[instance].fire('blur');
		}
	});
	
	$('#newQuestion').on('click', function() {
		$('.mt-list-item.done.active').removeClass('active');
		newQuestionOps();
	});

	$('body').on('click', '.new-question', function() {
		$('#newQuestion').trigger('click');
		var qType = $(this).attr('data-questionType');
		//console.log(qType);
		if (qType != undefined) {
			$('#hiddenQuestionType').val(qType);
			$('#hiddenQuestionType').trigger('change');
		}
	});
		
	$('#compro .questionTypeSelect').on('change', function() {
		elem = $(this);
		if($(this).val() == 0) {
			elem.parents('.sectionNewQuestion').find('.ftbs').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.trueFalse').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.optionsMul').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.options').removeClass('hide');
		}
		else if($(this).val() == 1) {
			elem.parents('.sectionNewQuestion').find('.ftbs').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.options').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.optionsMul').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.trueFalse').removeClass('hide');
		}
		else if($(this).val() == 2) {
			elem.parents('.sectionNewQuestion').find('.trueFalse').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.options').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.optionsMul').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.ftbs').removeClass('hide');
		}
		else if($(this).val() == 6) {
			elem.parents('.sectionNewQuestion').find('.trueFalse').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.options').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.ftbs').addClass('hide');
			elem.parents('.sectionNewQuestion').find('.optionsMul').removeClass('hide');
		}
	});
	
	$('#question').on('blur', function() {
		if($('#hiddenQuestionType').val() == 2)
			addBlanks(occurrences($('#question').val(), '____'));
	});
	
	$('#sectionNewQuestion .save-button').on('click', function() {
		if(!flagCategory) {
			var con = confirm("You have changed the category. Do you want to save like this.");
			if(!con)
				return;
		}
		if($('#categorySelect').val() != 0) {
			if($(CKEDITOR.instances['question'].getData()).text().length > 3) {
				var req = {};
				if($('#questionId').val() == 0)
					req.action = 'save-question';
				else {
					req.action = 'edit-question';
					req.questionId = $('#questionId').val();
				}
				req.parentId = 0;
				req.desc = CKEDITOR.instances['desc'].getData();
				req.subjectId = subjectId;
				req.question = CKEDITOR.instances['question'].getData();
				req.difficulty = $('#selectDifficulty').val();
				req.questionType = $('#hiddenQuestionType').val();
				req.tags = ms.getValue();
				req.categorySwitch = false;
				if(!flagCategory) {
					req.categorySwitch = true;
					req.oldCategory = oldCategory;
				}
				if(req.questionType == 0) {
					req.options = [];
					$('#options input[type="text"].option').each(function() {
						if($(this).val() != '') {
							var obj = {};
							obj.opt = '';
							obj.opt = $(this).val();
							obj.cState = 0;
							if($(this).parent().find('input[type="radio"]').prop('checked'))
								obj.cState = 1;
							req.options.push(obj);
						}
					});
					req.status = 0;
					if(req.options.length < 2)
						req.status = 0;
					else {
						for(var i=0; i<req.options.length; i++) {
							if(req.options[i]['cState'] == 1)
								req.status = 1;
						}
					}
				}
				else if(req.questionType == 1) {
					req.answer = '';
					if($('#true').prop('checked'))
						req.answer = 1;
					else if($('#false').prop('checked'))
						req.answer = 0;
					req.status = 0;
					if($('#true').prop('checked') || $('#false').prop('checked'))
						req.status = 1;
				}
				else if(req.questionType == 2) {
					req.blanks = [];
					var count = 0;
					$('#ftb input[type="text"].ftb').each(function() {
						if($(this).val() != '') {
							var obj = {};
							obj.ftb = '';
							obj.ftb = $(this).val();
							req.blanks.push(obj);
						}
						count++;
					});
					req.status = 0;
					if(req.blanks.length != count)
						req.status = 0;
					else
						req.status = 1;
				}
				else if(req.questionType == 3) {
					req.matches = [];
					$('#match .match').each(function() {
						if($(this).find('.a').val() != '' || $(this).find('.b').val() != '') {
							var obj = {};
							obj.optA = '';
							obj.optB = '';
							obj.optA = $(this).find('.a').val();
							obj.optB = $(this).find('.b').val();
							req.matches.push(obj);
						}
					});
					req.status = 0;
					if(req.matches.length < 2)
						req.status = 0;
					else {
						req.status = 1;
						//for(var i=0; i< req.matches.length; i++) {
						for(var i=0; i< 2; i++) {
							if(req.matches[i]['optA'] == '' || req.matches[i]['optB'] == '')
								req.status = 0;
						}
						for(var i=0; i< req.matches.length; i++) {
							if(req.matches[i]['optA'] == '') {
								req.matches[i]['optA'] = 'none';
							}
							if(req.matches[i]['optB'] == '') {
								req.matches[i]['optB'] = 'none';
							}
						}
					}
				}
				else if(req.questionType == 4 || req.questionType == 5) {
					req.childQuestions = [];
					$('#compro .sectionNewQuestion').each(function() {
						elem = $(this);
						newobj = {};
						newobj.desc = '';
						newobj.question = '';
						newobj.status = 0;
						var childQuestionId = elem.find('.question').attr('id');
						var childDescId = elem.find('.desc').attr('id');
						newobj.question = CKEDITOR.instances[childQuestionId].getData();
						newobj.desc = CKEDITOR.instances[childDescId].getData();
						/*if(elem.find('.desc').val().length != '')
							newobj.desc = elem.find('.desc').val();
						if(elem.find('.question').val().length != '') {
							var childQuestionId = elem.find('.question').attr('id');
							newobj.question = CKEDITOR.instances[childQuestionId].getData();
							//newobj.question = elem.find('.question').val();
						}*/ // Edited by Jitu
						newobj.questionType = elem.find('.questionTypeSelect').val();
						if(elem.find('.questionId').val() != 0)
							newobj.questionId = elem.find('.questionId').val();
						else
							newobj.questionId = 0;
						if(newobj.questionType == 0) {
							newobj.options = [];
							elem.find('.options input[type="text"].option').each(function() {
								if($(this).val() != '') {
									var obj = {};
									obj.opt = '';
									obj.opt = $(this).val();
									obj.cState = 0;
									if($(this).parent().find('input[type="radio"]').prop('checked'))
										obj.cState = 1;
									newobj.options.push(obj);
								}
							});
							newobj.status = 0;
							if(newobj.options.length < 2)
								newobj.status = 0;
							else {
								for(var i=0; i<newobj.options.length; i++) {
									if(newobj.options[i]['cState'] == 1)
										newobj.status = 1;
								}
							}
						}
						else if(newobj.questionType == 1) {
							newobj.answer = '';
							if(elem.find('.true').prop('checked'))
								newobj.answer = 1;
							else if(elem.find('.false').prop('checked'))
								newobj.answer = 0;
							newobj.status = 0;
							if(elem.find('.true').prop('checked') || elem.find('.false').prop('checked'))
								newobj.status = 1;
						}
						else if(newobj.questionType == 2) {
							newobj.blanks = [];
							var count = 0;
							elem.find('.ftbs input[type="text"].ftb').each(function() {
								if($(this).val() != '') {
									var obj = {};
									obj.ftb = '';
									obj.ftb = $(this).val();
									newobj.blanks.push(obj);
								}
								count++;
							});
							newobj.status = 0;
							if(newobj.blanks.length != count)
								newobj.status = 0;
							else
								newobj.status = 1;
						}
						else if(newobj.questionType == 6) {
							newobj.options = [];
							elem.find('.optionsMul input[type="text"].optionMul').each(function() {
								if($(this).val() != '') {
									var obj = {};
									obj.opt = '';
									obj.opt = $(this).val();
									obj.cState = 0;
									if($(this).parent().find('input[type="checkbox"]').prop('checked'))
										obj.cState = 1;
									newobj.options.push(obj);
								}
							});
							newobj.status = 0;
							if(newobj.options.length < 2)
								newobj.status = 0;
							else {
								for(var i=0; i<newobj.options.length; i++) {
									if(newobj.options[i]['cState'] == 1)
										newobj.status = 1;
								}
							}
						}
						req.subjectId = subjectId;
						//to check the length of question
						if(newobj.question.length < 3)
							newobj.status = 0;
						req.childQuestions.push(newobj);
					});
					req.status = 1;
					$.each(req.childQuestions, function(i, v) {
						if(v.status == 0)
							req.status = 0;
					});
				}
				else if(req.questionType == 6) {
					req.options = [];
					$('#optionsMul input[type="text"].optionMul').each(function() {
						if($(this).val() != '') {
							var obj = {};
							obj.opt = '';
							obj.opt = $(this).val();
							obj.cState = 0;
							if($(this).parent().find('input[type="checkbox"]').prop('checked'))
								obj.cState = 1;
							req.options.push(obj);
						}
					});
					req.status = 0;
					if(req.options.length < 2)
						req.status = 0;
					else {
						for(var i=0; i<req.options.length; i++) {
							if(req.options[i]['cState'] == 1)
								req.status = 1;
						}
					}
				}
				else if(req.questionType == 7) {
					req.options = [];
					$('#audio input[type="text"].optionAud').each(function() {
						if($(this).val() != '') {
							var obj = {};
							obj.opt = '';
							obj.opt = $(this).val();
							obj.cState = 0;
							if($(this).parent().find('input[type="radio"]').prop('checked'))
								obj.cState = 1;
							req.options.push(obj);
						}
					});
					req.audio = $('#audio .js-file-path').attr('data-path');
					req.status = 0;
					if(req.options.length < 2)
						req.status = 0;
					else {
						for(var i=0; i<req.options.length; i++) {
							if(req.options[i]['cState'] == 1)
								req.status = 1;
						}
					}
				}
				$.ajax({
					'type'	: 'post',
					'url'	: ApiEndPoint,
					'data'	: JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 0)
						toastr.error(res.message);
					else {
						fetchSubjectQuestions();
						newQuestionOps();
					}
				});
			}
			else
				toastr.error("Please give your question text here which should be longer than 3 characters.");
		}
		else
			toastr.error("Please select a category");
	});
	$('#audio').on('click', '.btn-audio-upload', function(){
		$('#frmAudioUpload input').click();
	});
	$('#frmAudioUpload .js-uploader').change(function(){
		var file = $(this).val();
		if (!!file) {
			$('#frmAudioUpload').submit();
		};
	});

	// bind to the form's submit event 
	$('#frmAudioUpload').submit(function(){
		var thiz = $(this);
		
		if (player != undefined) {
			player.pause();
		};

		$('.btn-audio-upload').attr('disabled', true);
		$('.btn-audio-upload').after('<span class="upload-loader"><img src="'+sitepathManageIncludes+'assets/custom/img/ajax-loader.gif" alt="loader" /></span>');
		
		var options = { 
			//target:        '#output2',   // target element(s) to be updated with server response 
			data: { subjectId: subjectId },
			dataType: 'json',
			beforeSubmit:  showRequest,  // pre-submit callback 
			success: function(msg) {
				$('.js-uploader').val('');
				$('.upload-loader').remove();
				$('.btn-audio-upload').attr('disabled', false);
				if (msg.status == 0) {
					toastr.error(msg.message);
				} else {
					var mp3 = msg.fileName;
					$('.js-file-path').html(
						'<audio controls="control" id="audioShowcasePlayer" preload="none" src="'+mp3+'" type="audio/mp3"></audio>'
					);
					$('.js-file-path').attr('data-path', mp3);
					player = new MediaElementPlayer('#audioShowcasePlayer', {
								type: 'audio/mp3',
								success: function (mediaElement, domObject) {
									mediaElement.addEventListener('pause', function () {
										//console.log("paused");
									}, false);
									mediaElement.addEventListener('play', function () {
										//console.log("play");
										videoDuration = mediaElement.duration;
									}, false);
									mediaElement.addEventListener('ended', function () {
										//console.log("ended");
									}, false);
								}
							});
					var sources = [
						{ src: mp3, type: 'audio/mp3' }
					];

					player.setSrc(sources);
					player.load();
					//player.play();
				}
			}  // post-submit callback 
		}; 
		// inside event callbacks 'this' is the DOM element so we first 
		// wrap it in a jQuery object and then invoke ajaxSubmit 
		$(this).ajaxSubmit(options);

		// !!! Important !!! 
		// always return false to prevent standard browser submit and page navigation 
		return false;
	});
	// pre-submit callback 
	function showRequest(formData, jqForm, options) { 
	    // formData is an array; here we use $.param to convert it to a string to display it 
	    // but the form plugin does this for you automatically when it submits the data 
	    var queryString = $.param(formData); 

	    // jqForm is a jQuery object encapsulating the form element.  To access the 
	    // DOM element for the form do this: 
	    // var formElement = jqForm[0]; 
	 
	    //toastr.error('About to submit: \n\n' + queryString); 
	 
	    // here we could return false to prevent the form from being submitted; 
	    // returning anything other than false will allow the form submit to continue 
	    return true; 
	}
});
</script>