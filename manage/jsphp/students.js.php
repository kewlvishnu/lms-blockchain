<script type="text/javascript">
var TableDatatablesManaged = function () {

    var initTable1 = function () {

        var table = $('#sample_1');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // setup buttons extension: http://datatables.net/extensions/buttons/
            buttons: [
                { extend: 'print', className: 'btn dark btn-outline' },
                { extend: 'pdf', className: 'btn green btn-outline' },
                { extend: 'csv', className: 'btn purple btn-outline' }
            ],

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 15, 20, -1],
                [10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery('#sample_1_wrapper');

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).prop("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).prop("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
        }

    };

}();
var keys_available,keys_pending;
function bindCoursesDropdown() {
    var req = {};
    var res;
    req.action = "Courses_for_student";
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        for (var i = 0; i < data.courses.length; i++)
        {
            var id = data.courses[i].id;
            var name = data.courses[i].name;
            $("#courses").append("<option value=" + id + " >" + name + "</option>");
        }
    });
}
$('#modalInviteStudentsToCourse').on('shown.bs.modal', function () {
    $('#textEmailAddresses').val('');
    CKEDITOR.instances['textEmailNote'].setData('Dear Student,<br /><br />Welcome to Integro! You have been invited to join the following course.<br /><br />[course_invite_block]<br /><br />Best Regards,<br />Integro Team<br />Integro.io');
    keys_available = get_keys_remaining();
    $('.js-remaining').html(keys_available);
    $('.js-pending').html(keys_pending);
});
function get_keys_remaining() {
    var req = {};
    req.action = 'keys_avilable';
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        if (data.status == 1) {
            keys_available = data.keys;
            keys_pending = data.invitation;
        }
        else {
            toastr.error(res.msg);
        }
    });
    return keys_available;
}
$('#btnInviteStudent').on('click', function () {
    elem = $(this);
    $('.help-block').remove();
    var invalid = '';
    var correct = true;
    var emails = $.trim($('#textEmailAddresses').val());
    var emailNote = CKEDITOR.instances['textEmailNote'].getData();
    var email = emails.split(',');
    if (emails == '') {
        toastr.error('Please enter email id');
        return false;
    }
    else if (checkDuplicates(email) == 'false') {
        toastr.error('Please remove duplicates');
        return false;
    }
    else if ((email.length > keys_available)) {
        toastr.error('You have only ' + keys_available + ' keys available');
        return false;
    }
    else if (emailNote.indexOf('[course_invite_block]') == -1) {
        toastr.error('Please add [course_invite_block] in the email note, it is necessary.');
        return false;
    }
    $.each(email, function (index, value) {
        if (validateEmail($.trim(value)) == false) {
            invalid += value + ',';
            correct = false;
        }
    });
    if (correct == false) {
        $('#textEmailAddresses').after('<span class="help-block text-danger "> Correct these email addresses:\n' + invalid+'</span>');
        return false;
    }
    var courseId = $('#courses').val();
    if (courseId == 0) {
        $('#courses').after('<span class="help-block text-danger "> Please select a '+terminologies["course_single"]+' ! </span>');
        return false;
    }
    else {
        var req = {};
        req.courseId = $('#courses').val();
        req.action = 'send_activation_link_student';
        req.email = email;
        req.emailNote = emailNote;
        req.enrollment_type = 0;
        elem.attr('disabled', true);
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            elem.attr('disabled', false);
            data = $.parseJSON(res);
            if (data.status == 1) {
                toastr.success(data.message);
            }
            else {
                if(data.msg)
                    toastr.error(data.msg);
                else
                    toastr.error(data.message);
            }
            $('#modalInviteStudentsToCourse').modal('hide');

        });
    }
});
function StudentDetailsWithCourses() {
    var req = {};
    var res;
    req.action = "Student_Details_WithCourses";
    
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        if(data.status==1){
            var html = "";
            for (var i = 0; i < data.students.length; i++)
            {
                var studentid = data.students[i].userId;
                var username = data.students[i].username;
                var temp = data.students[i].temp;
                var password = ((parseInt(temp)==1)?data.students[i].password:'REGISTERED');
                var name = data.students[i].name;
                var contactMobile = data.students[i].contactMobile;
                // var course_key = data.institutes[i].Key_Id;
                var email = data.students[i].email;
                var profilePic = data.students[i].profilePic;
                var courses = '';
                if (data.courses[i].length>0) {
                    courses += '<ul class="list-unstyled">';
                    for (var j = 0; j < data.courses[i].length; j++) {
                        courses += '<li><span class="badge btn-sm bg-blue" title="'+terminologies["subject_single"].toLowerCase()+'">C</span> '+data.courses[i][j].name + '</li>';
                    }
                    courses += '</ul>';
                };
                courses = courses.substring(0, courses.length - 2);
                //html += '<tr><td>' + name + '<br/>' + courses + '</td><td class=small-txt>' + email + '</td><td>' + contactMobile + ' </td><td>' + studentid + '</td></tr>';
                html += '<tr class="odd gradeX" data-student="'+studentid+'" data-temp="'+temp+'">'+
                            '<td>'+(i+1)+'</td>'+
                            '<td><div class="img-cover img-circle"><img src="' + profilePic + ' " /></div></td>'+
                            '<td>' + username + '</td>'+
                            '<td><div class="text-success">'+password+'</div></td>'+
                            '<td> ' + name + ' </td>'+
                            '<td><a href="mailto:' + email + '"> ' + email + ' </a></td>'+
                            '<td>' + contactMobile + '</td>'+
                            '<td>' + courses + '</td>'+
                            '<td>'+
                                '<a href="javascript:void(0)" class="btn btn-circle btn-sm red js-remove-students"><i class="fa fa-trash"></i> Remove</a>'+
                            '</td>'+
                        '</tr>';
            }
            $('#sample_1 tbody').html(html);
            $('#sample_1').DataTable().destroy();
            TableDatatablesManaged.init();
        }else{
             $('#sample_1 tbody').html('<tr><td colspan="9">No '+terminologies["student_single"]+' has joined yet</td></tr>');
        }
    });
}
$('#sample_1').on('click','.js-remove-students',function(){
    var thiz = $(this);
    bootbox.confirm("Are you sure?", function(result) {
        if (result) {
            var tempStudents = "";
            var permStudents = "";
            var student = 0;
            student = thiz.closest('tr');
            tempStudents = ((student.attr('data-temp')=='1')?student.attr('data-student'):'');
            permStudents = ((student.attr('data-temp')=='0')?student.attr('data-student'):'');
            if (tempStudents=='' && permStudents=='') {
                toastr.error("Please select at least 1 student");
            } else {
                var req = {};
                var res;
                //req.course_id = getUrlParameter('courseId');
                req.tempStudents  = tempStudents;
                req.permStudents  = permStudents;
                req.action    = "delete-students";
                
                $.ajax({
                    'type': 'post',
                    'url': ApiEndPoint,
                    'data': JSON.stringify(req)
                }).done(function (res) {
                    data = $.parseJSON(res);
                    if (data.status == 0) {
                        toastr.error(data.exception);
                    } else {
                        for (var i = 0; i < data.students.length; i++) {
                            $('[data-student="'+data.students[i]+'"]').remove();
                            keys_available++;
                        };
                        $('#remaining_keys_total').html(keys_available);
                    }
                    console.log(data);
                });
            };
        }
    });
});
</script>