<script type="text/javascript">
var sortOrder = [];
var examStart = new Date();
var examEnd = new Date('2099/12/31');
function fetchChapters() {
    var req = {};
    req.action = "get-chapters-for-exam";
    req.subjectId = subjectId;
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        fillChaptersSelect(res);
    });
}
function fillChaptersSelect(data) {
    if(data.status == 0) {
        toastr.error(data.message);
        return;
    }
    var opts = '';
    for(i=0; i<data.chapters.length; i++) {
        opts += '<option value="'+data.chapters[i]['id']+'">'+data.chapters[i]['name']+'</option>';
    }
    opts += '<option value="-1">Independent</option>';
    $('#chapterSelect').append(opts);
    if (typeof chapterId !== 'undefined') {
        $('#chapterSelect').val(xx);
    }
}
function fetchTemplates() {
    var req = {};
    req.action = "get-templates-for-exam";
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        fillTemplateSelect(res);
    });
}
function fillTemplateSelect(data) {
    if(data.status == 0) {
        toastr.error(data.message);
        return;
    }
    var opts = '';
    for(i=0; i<data.templates.length; i++) {
        opts += '<option value="'+data.templates[i]['id']+'">'+data.templates[i]['name']+'</option>';
    }
    $('#templateSelect').append(opts);
}
function fetchCourseDates() {
	var req = {};
	var res;
	req.action = 'get-course-date';
	req.courseId = courseId;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else {
			$('#courseStartDate').val(res.dates.liveDate);
			$('#courseEndDate').val(res.dates.endDate);
			
			//adding handlers for datetimepicker of start and end date
			var now = new Date();
			var start = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
			var courseStart = new Date(parseInt(res.dates.liveDate));
			if(start.valueOf() < courseStart.valueOf())
				examStart = new Date(courseStart.getTime());
			else
				examStart = new Date(start.getTime());
			
			var courseEnd = new Date(parseInt(res.dates.endDate));
			var end = new Date('2099/12/31');
			if(end.valueOf() < courseEnd.valueOf())
				examEnd = new Date(courseEnd.getTime());
			else
				examEnd = new Date(end.getTime());
			if(res.dates.endDate == '') {
				//toastr.error('hello');
				$('#endDate').closest('.form-group').addClass('hide');
				$('#result-show').addClass('hide');
				$('#set-exam-date').addClass('hide');					
				examEnd = new Date(end.getTime());
			}
			
			$('#startDate').datetimepicker({
				format: 'dd MM yyyy hh:ii',
				minDate: examStart,
				step: 30,
				autoclose: true,
				pickerPosition: "bottom-left",
				onSelectTime: function(date) {
					if (date.valueOf() > examEnd.valueOf()) {
						toastr.error('The start date can not be greater than the end date.');
					} else {
						examStart = date;
					}
				}
			});
			$('#endDate').datetimepicker({
				format: 'dd MM yyyy hh:ii',
				minDate: examStart,
				maxDate: examEnd,
				step: 30,
				autoclose: true,
				pickerPosition: "bottom-left",
				onSelectTime: function(date) {
					if (date.valueOf() < examStart.valueOf()) {
						toastr.error('The end date can not be less than the start date.');
					} else {
						examEnd = date;
					}
				}
			});
		}
	});
}
function nameAvailable() {
	if($('#titleName').val().length != '' && ($('#examCat').prop('checked') || $('#assignmentCat').prop('checked'))) {
		var req = {};
		var res;
		req.action = 'check-name-for-exam';
		req.name = $('#titleName').val();
		if($('examCat').prop('checked'))
			req.name += '_E';
		else
			req.name += '_A';
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndPoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				toastr.error(res.message);
			else {
				if(!res.available)
					toastr.error('Please select a different name as you have already used this name.');
			}
		});
	}
}
$(document).ready(function(){
	$('#titleName').on('blur', function() {
		if(min($(this), 3)) {
			if(!max($(this), 50))
				toastr.error("Please give a shorter assignment/exam name. Maximum allowed limit is 50 characters.");
		}
		else
			toastr.error("Please give a longer assignment/exam name. Minimum allowed limit is 3 characters.");
		nameAvailable();
	});
	$('#settime').datetimepicker({
		format: 'dd MM yyyy hh:ii',
		minDate: examStart,
		step: 30,
		autoclose: true,
		pickerPosition: "bottom-left"
	});
	$('#sections input[type="text"]').on('blur', function() {
		if($('#orderStart').prop('checked')) {
			$('#orderStart').click();
		} else if($('#orderRandom').prop('checked')) {
			$('#orderRandom').click();
		}
	});
	$('#orderRandom').on('click', function() {
		$('#sortOrder').addClass('hide');
	});
	$('#orderStart').on('click', function() {
		$('#sortOrder').find('li').remove();
		var html = '';
		for(var i=0; i<$('#sections').find('input[type="text"]').length; i++) {
			var value = $('#sections input[type="text"]:eq('+i+')').val();
			//html += '<li data-original="'+i+'" class="sort-li">' + value + '</li>';
			html += '<li data-original="'+i+'" class="dd-item sort-li">'+
		                '<div class="dd-handle"> ' + value + ' </div>'+
		            '</li>';
			sortOrder[i] = i+'';
		}
		$('#sortOrder').append(html);
		UINestable.init();
		$('#sortOrder').removeClass('hide');
	});
	$('#templateSelect').on('change', function() {
		if($(this).val() != 0) {
			var req  = {};
			req.action = 'get-template-details';
			req.templateId = $(this).val();
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndPoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 1)
					fillTemplateDetails(res);
				else
					toastr.error(res.message);
			});
		}
	});
	function fillTemplateDetails(data) {
		if(data.sections.length > 1) {
			$('#switchYes').prop('checked', false);
			$('#orderRandom').prop('checked', false);
			$('.js-section-timings').removeClass('hide');
			$('.js-sections-manage').removeClass('hide');
		}
		$('.js-exam-settings').removeClass('hide');
		$('#noOfAttempts').val(data.template.attempts);
		if(data.template.type == 'Assignment') {
			$('#assignmentCat').prop('checked', true);
			$('#assignmentCat').trigger('change');
		}
		else if(data.template.type == 'Exam') {
			$('#examCat').prop('checked', true);
			$('#examCat').trigger('change');
		}
		$('#startDate').val(data.template.startDate);
		$('#endDate').val(data.template.endDate);
		if(data.template.endBeforeTime == 0)
			$('#endYes').prop('checked', true);
		else
			$('#endNo').prop('checked', true);
		if(data.template.powerOption == 0)
			$('#noTimeLost').prop('checked', true);
		else
			$('#timeLost').prop('checked', true);
		if(data.template.totalTime == 0 || data.template.totalTime == 6000) {
			$('#switchNo').prop('checked', true);
			$('#sectionTimes').removeClass('hide');
			$('#totalTime').addClass('hide');
			$('#gapTime').val(data.template.gapTime);
			$('#sections .form-group').remove();
			$('#sectionTimes .row:eq(0)').find('*').remove();
			$('.delete-button').on('click', function() {
				//$(this).parents('.form-group').remove();
				$(this).closest('.js-section').remove();
				$('#sectionTimes .row:eq(0)').find('*').remove();
				$('#switchNo').click();
				$('#sectionTimes').addClass('hide');
				$('#totalTime').removeClass('hide');
				if($('#orderStart').prop('checked')) {
					$('#orderStart').click();
				} else if($('#orderRandom').prop('checked')) {
					$('#orderRandom').click();
				}
			});
				
			for(i=0; i<data.sections.length; i++) {
				/*var html='<div class="form-group">'
						+ '<div class="row">'
							+ '<div class="col-lg-9">'
								+ '<input type="text" class="form-control" value="'+data.sections[i].name+'" id="'+data.sections[i].id+'">'
							+ '</div>'
							+ '<div class="col-lg-3">'
								+ '<button class="btn btn-danger btn-xs delete-button" type="button" style="margin-left:5px;"><i class="fa fa-trash-o "></i></button>'
							+ '</div>'
						+ '</div>'
					+ '</div>';*/
				var html='<div class="form-group js-section">'+
	                        '<div class="input-group">'+
	                            '<input type="text" class="form-control" value="'+data.sections[i].name+'" id="'+data.sections[i].id+'">'+
	                            '<span class="input-group-btn">'+
	                                /*'<button class="btn green" type="button"><i class="fa fa-edit"></i></button>'+*/
	                                '<button class="btn red delete-button" type="button"><i class="fa fa-trash"></i></button>'+
	                            '</span>'+
	                        '</div>'+
	                    '</div>';
				$('#sections').append(html);
				
				/*var htmlTime = '<div class="col-lg-6">'
					+ '<label for="coursename">Total Time for '+data.sections[i].name+'</label><br>'
					+ '<div class="col-md-4">'
						+ '<div class="input-group input-small">'
							+ '<input type="text" maxlength="3" class="spinner-input form-control time hour" value="0h" disabled=true>'
							+ '<div class="spinner-buttons input-group-btn btn-group-vertical">'
								+ '<button type="button" class="btn spinner-up btn-xs btn-default generated">'
									+ '<i class="fa fa-angle-up"></i>'
								+ '</button>'
								+ '<button type="button" class="btn spinner-down btn-xs btn-default generated">'
									+ '<i class="fa fa-angle-down"></i>'
								+ '</button>'
							+ '</div>'
						+ '</div>'
					+ '</div> &nbsp;&nbsp;&nbsp;&nbsp;'
					+ '<div class="col-md-4">'
						+ '<div class="input-group input-small">'
							+ '<input type="text" class="spinner-input form-control time minute" maxlength="3" value="0m" disabled=true>'
							+ '<div class="spinner-buttons input-group-btn btn-group-vertical">'
								+ '<button class="btn spinner-up btn-xs btn-default generated" type="button">'
									+ '<i class="fa fa-angle-up"></i>'
								+ '</button>'
								+ '<button class="btn spinner-down btn-xs btn-default generated" type="button">'
									+ '<i class="fa fa-angle-down"></i>'
								+ '</button>'
							+ '</div>'
						+ '</div>'
					+ '</div>'
				+ '</div>';*/
				var htmlTime = '<div class="col-md-6">'+
	                        '<div class="form-group">'+
	                            '<label class="control-label">Total Time for '+data.sections[i].name+'</label>'+
	                            '<div class="clearfix">'+
	                                '<div class="dib">'+
	                                    '<div class="input-group input-small">'+
	                                        '<input type="number" class="form-control time hour" value="0" placeholder="Hours" aria-describedby="sizing-addon1">'+
	                                        '<span class="input-group-addon" id="sizing-addon1">Hours</span>'+
	                                    '</div>'+
	                                '</div>'+
	                                '<div class="dib">'+
	                                    '<div class="input-group input-small">'+
	                                        '<input type="number" class="form-control time minute" value="0" placeholder="Minutes" aria-describedby="sizing-addon1">'+
	                                        '<span class="input-group-addon" id="sizing-addon1">Minutes</span>'+
	                                    '</div>'+
	                                '</div>'+
	                            '</div>'+
	                        '</div>'+
	                    '</div>';
				$('#sectionTimes .row:eq(0)').append(htmlTime);
				hour = Math.floor(data.sections[i].time / 60);
				minute = data.sections[i].time - (hour * 60);
				$('#sectionTimes input[type="number"].hour:eq('+i+')').val(hour);
				$('#sectionTimes input[type="number"].minute:eq('+i+')').val(minute);
			}
		}
		else {
			$('#sections .form-group').remove();
			$('#sectionTimes .row:eq(0)').find('*').remove();
			$('.delete-button').on('click', function() {
				//$(this).parents('.form-group').remove();
				$(this).parents('.js-section').remove();
				$('#sectionTimes .row:eq(0)').find('*').remove();
				$('#switchNo').click();
				$('#sectionTimes').addClass('hide');
				$('#totalTime').removeClass('hide');
				if($('#orderStart').prop('checked')) {
					$('#orderStart').click();
				} else if($('#orderRandom').prop('checked')) {
					$('#orderRandom').click();
				}
			});
			for(i=0; i<data.sections.length; i++) {
				/*var html='<div class="form-group">'
						+ '<div class="row">'
							+ '<div class="col-lg-9">'
								+ '<input type="text" class="form-control" value="'+data.sections[i].name+'" id="'+data.sections[i].id+'">'
							+ '</div>'
							+ '<div class="col-lg-3">'
								+ '<button class="btn btn-danger btn-xs delete-button" type="button" style="margin-left:5px;"><i class="fa fa-trash-o "></i></button>'
							+ '</div>'
						+ '</div>'
					+ '</div>';*/
				var html='<div class="form-group js-section">'+
	                        '<div class="input-group">'+
	                            '<input type="text" class="form-control" value="'+data.sections[i].name+'" id="'+data.sections[i].id+'">'+
	                            '<span class="input-group-btn">'+
	                                /*'<button class="btn green" type="button"><i class="fa fa-edit"></i></button>'+*/
	                                '<button class="btn red delete-button" type="button"><i class="fa fa-trash"></i></button>'+
	                            '</span>'+
	                        '</div>'+
	                    '</div>';
				$('#sections').append(html);
				$('.delete-button').on('click', function() {
					//$(this).parents('.form-group').remove();
					$(this).parents('.js-section').remove();
					$('#sectionTimes .row:eq(0)').find('*').remove();
					if($('#switchNo').prop('checked'))
						$('#switchNo').click();
					if($('#sections').find('input[type="text"]').length <= 1) {
						$('#switchYes').prop('checked', true);
						$('#orderRandom').prop('checked', true);
						$('#switchSections').addClass('hide');
						$('.js-sections-manage').addClass('hide');
						$('#sectionTimes').addClass('hide');
						$('#totalTime').removeClass('hide');
					}
					if($('#orderStart').prop('checked')) {
						$('#orderStart').click();
					} else if($('#orderRandom').prop('checked')) {
						$('#orderRandom').click();
					}
				});
			
				if($('#orderStart').prop('checked')) {
					//$('#orderStart').click();
				} else if($('#orderRandom').prop('checked')) {
					//$('#orderRandom').click();
				}
			}
			$('#switchYes').prop('checked', true);
			$('#sectionTimes').addClass('hide');
			$('#totalTime').removeClass('hide'); // major
			hour = Math.floor(data.template.totalTime / 60);
			minute = data.template.totalTime - (hour * 60);
			$('#totalTimeHour').val(hour);
			$('#totalTimeMinute').val(minute);
		}
		if(data.template.sectionOrder == 0) {
			$('#orderStart').prop('checked', true);
			$('#orderStart').click();
		}
		else
			$('#orderRandom').prop('checked', true);
		$('#sections input[type="text"]').on('blur', function() {
			if($('#orderStart').prop('checked')) {
				$('#orderStart').click();
			} else if($('#orderRandom').prop('checked')) {
				$('#orderRandom').click();
			}
		});
	}
	$('#chapterSelect').on('blur', function() {
		if($(this).val() == 0)
			toastr.error("Please select a chapter or select independent.");
	});
	$('#formCreateExam').on('submit', function(){
		var resultshown='immediately';
		var resulttill=1;
		var resulttilldate='';
		$('#tempSection').parent().removeClass('has-error');
		$('#tempSection').next().remove();
		if($('#custom-time').prop('checked') )
		{
			resulttill=2;
		}
		if($('#after-examend').prop('checked'))
		{
			resultshown='after'
		}
		$('#titleName, #chapterSelect').blur();
		if($('#fromTemplate').prop('checked') || $('#ownSettings').prop('checked')) {
			if($('#fromTemplate').prop('checked')) {
				if($('#templateSelect').val() == 0) {
					toastr.error("Please select a template from here.");
				}
			}
			if($('#noOfAttempts').val() != '') {
				if($('#assignmentCat').prop('checked') || $('#examCat').prop('checked')) {
					if($('#sections').find('input[type="text"]').length != 0) {
						if($('#startDate input').val() != '') {
                            if($('#courseEndDate').val()!= '' && $('#endDate input').val()!='' ) {
                                var setStart = new Date($('#startDate input').val());
                                var setEnd = new Date($('#endDate input').val());
                                if(setEnd.valueOf() <= setStart.valueOf())
                                    toastr.error('Plese select proper dates.');
                            }
							if(($('#endDate input').val() == '' && $('#courseEndDate').val() == '') || $('#endDate input').val() != '') {
							if(resulttill==1 || (resulttill==2  && $('#settime input').val() != '')) {
								if(resultshown =='immediately' || resultshown=='after'  || !$('#examCat').prop('checked')) {
								if($('#switchYes').prop('checked') || $('#switchNo').prop('checked')  || !$('#examCat').prop('checked')) {
									if($('#endYes').prop('checked') || $('#endNo').prop('checked')  || !$('#examCat').prop('checked')) {
										if($('#noTimeLost').prop('checked') || $('#timeLost').prop('checked')  || !$('#examCat').prop('checked')) {
											if($('.has-error').length == 0 ) {
												var req = {};
												req.action = 'add-exam';
												req.name = $('#titleName').val();
												req.courseId = courseId;
												if($('#assignmentCat').prop('checked'))
													req.type = 'Assignment';
												else if($('#examCat').prop('checked'))
													req.type = 'Exam';
												if($('#chapterSelect').val() == -1)
													req.chapterId = 0;
												else
													req.chapterId = $('#chapterSelect').val();
												req.subjectId = subjectId;
												req.status = 0;
												var showResult='immediately';
												if($('#after-examend').prop('checked'))
													showResult='after';
												req.showResult=showResult;
												var showResultTill=1;
												var showResultTillDate="";
												if($('#custom-time').prop('checked'))
												{
													showResultTill=2;
													var timestamp = new Date($('#settime input').val());
													showResultTillDate = timestamp.getTime();
													
												}
												req.showResultTill=showResultTill;
												req.showResultTillDate=showResultTillDate;
												var timestamp = new Date($('#startDate input').val());
												req.startDate = timestamp.getTime();
												req.endDate = '';
												if($('#endDate input').val() != '' && $('#courseEndDate').val() != '') {
													timestamp = new Date($('#endDate input').val());
													req.endDate = timestamp.getTime();
												}
												req.attempts = $('#noOfAttempts').val();
												req.tt_status = 0;
												if($('#switchYes').prop('checked'))
													req.tt_status = 0;
												else if($('#switchNo').prop('checked'))
													req.tt_status = 1;
												req.totalTime = 6000;
												req.gapTime = 0;
												req.time = [];
												if(req.tt_status == 0  || !$('#examCat').prop('checked')) {
													req.sectionOrder = 2;
													var tth = $('#totalTimeHour').val();
													//tth = tth.substring(0, tth.length-1);
													tth = parseInt(tth);
													var ttm = $('#totalTimeMinute').val();
													//ttm = ttm.substring(0, ttm.length-1);
													ttm = parseInt(ttm);
													req.totalTime = (tth * 60) + ttm;
													if(req.totalTime <= 0 && $('#examCat').prop('checked')) {
														toastr.error("Please specify a correct time limit.");
														return false;
													}
												}
												if(req.tt_status == 1  || !$('#examCat').prop('checked')) {
													var gap = $('#gapTime').val();
													req.gapTime = gap;
													var sectionTimes = $('#sectionTimes').find('input.time');
													for(i=0; i<sectionTimes.length; i = i + 2) {
														var sth = sectionTimes[i]['value'];
														//sth = sth.substring(0, sth.length-1);
														sth = parseInt(sth);
														var stm = sectionTimes[i+1]['value'];
														//stm = stm.substring(0, stm.length-1);
														stm = parseInt(stm);
														var st = (sth * 60) +stm;
														if(st <= 0 && $('#examCat').prop('checked')) {
															toastr.error("Please select correct timings for every section.");
															return false;
														}
														req.time.push(st);
													}
													if(!($('#orderStart').prop('checked') || $('#orderRandom').prop('checked'))) {
														if($('#examCat').prop('checked')) {
															toastr.error("Please select ordering of the sections.");
															return false;
														}
													}
												}
												req.endBeforeTime = 0;
												req.powerOption = 1;
												if($('#endYes').prop('checked'))
													req.endBeforeTime = 1;
												else if($('#endNo').prop('checked'))
													req.endBeforeTime = 0;
												if($('#noTimeLost').prop('checked'))
													req.powerOption = 0;
												else if($('#timeLost').prop('checked'))
													req.powerOption = 1;
												req.sections = [];
												req.weights = [];
												var sections = $('#sections').find('input[type="text"]');
												for(i=0; i<sections.length; i++) {
													req.sections.push(sections[i]['value']);
													req.weights.push(sortOrder.indexOf(i+''));
												}
												if(req.tt_status == 0) {
													for(i=0; i<sections.length; i++) {
														req.time.push(0);
													}
												}
												req.sectionOrder = 0;
												if($('#orderStart').prop('checked'))
													req.sectionOrder = 0;
												else if($('#orderRandom').prop('checked'))
													req.sectionOrder = 1;
												if($('#ownSettings').prop('checked')) {
													var con = confirm("Do you want to save this as a template.");
													if(con) {
														var templateName = prompt("Please give your template a name.");
														if(templateName == '')
															templateName = 'Untitled template';
														var treq = {};
														treq.action = 'add-template';
														treq.name = templateName;
														treq.type = req.type;
														treq.attempts = req.attempts;
														treq.tt_status = req.tt_status;
														treq.totalTime = req.totalTime;
														treq.gapTime = req.gapTime;
														treq.sectionOrder = req.sectionOrder;
														treq.powerOption = req.powerOption;
														treq.endBeforeTime = req.endBeforeTime;
														treq.sections = req.sections;
														treq.time = req.time;
														treq.weights = req.weights;
														$.ajax({
															'type'  : 'post',
															'url'   : ApiEndPoint,
															'data' 	: JSON.stringify(treq)
														});
													}
												}
												$.ajax({
													'type'  : 'post',
													'url'   : ApiEndPoint,
													'data' 	: JSON.stringify(req)
												}).done(function (res) {
													res =  $.parseJSON(res);
													if(res.status == 1) {
														window.location = sitepathManageExams+res.examId;
													}
													else
														toastr.error(res.message);
												});
											}
											else
												toastr.error("Please review your forms. It contains errors.");
										}
										else
											toastr.error("Please select an option to continue.");
									}
									else
										toastr.error("Please select one option.");
								}
								else
									toastr.error("Please select a option to switch between sections or not.");
								}
								else
										toastr.error("Please select a option when "+terminologies["student_single"].toLowerCase()+" can see the result.");
							}
							else
									toastr.error("Please select a option/End date till when "+terminologies["student_single"].toLowerCase()+" can see the result.");
							}
							else
								toastr.error("Please specify a correct end date.");
						}
						else
							toastr.error("Please specify a correct start date.");
					}
					else
						toastr.error("Please add atleast one section.");
				}
				else
					toastr.error("Please select a test type.")
			}
			else
				toastr.error("Please specify number of attempts allowed.");
		}
		else
			toastr.error("Please select any one import setting.");
		return false;
	});
	$('#addSection').on('click', function() {
		$('#tempSection').parent().removeClass('has-error');
		$('#tempSection').next().remove();
		if($('#tempSection input').val() != '') {
			/*var html='<div class="form-group">'
						+ '<div class="row">'
							+ '<div class="col-lg-9">'
								+ '<input type="text" class="form-control" value="'+$('#tempSection').val()+'">'
							+ '</div>'
							+ '<div class="col-lg-3">'
								+ '<button class="btn btn-danger btn-xs delete-button" type="button"><i class="fa fa-trash-o "></i></button>'
							+ '</div>'
						+ '</div>'
					+ '</div>';*/
			var html='<div class="form-group js-section">'+
                        '<div class="input-group">'+
                            '<input type="text" class="form-control" value="'+$('#tempSection input').val()+'">'+
                            '<span class="input-group-btn">'+
                                /*'<button class="btn green" type="button"><i class="fa fa-edit"></i></button>'+*/
                                '<button class="btn red delete-button" type="button"><i class="fa fa-trash"></i></button>'+
                            '</span>'+
                        '</div>'+
                    '</div>';
			$('#sections').append(html);
			$('#tempSection input').val('');
			if($('#switchNo').prop('checked'))
				$('#switchNo').click();
			$('.delete-button').on('click', function() {
				//$(this).parents('.form-group').remove();
				$(this).parents('.js-section').remove();
				$('#sectionTimes .row:eq(0)').find('*').remove();
				if($('#switchNo').prop('checked'))
					$('#switchNo').click();
				if($('#sections').find('input[type="text"]').length <= 1) {
					$('#switchYes').prop('checked', true);
					$('#orderRandom').prop('checked', true);
					$('#switchSections').addClass('hide');
					$('.js-sections-manage').addClass('hide');
					$('#sectionTimes').addClass('hide');
					$('#totalTime').removeClass('hide');
				}
				if($('#orderStart').prop('checked')) {
					$('#orderStart').click();
				} else if($('#orderRandom').prop('checked')) {
					$('#orderRandom').click();
				}
			});
			if($('#sections').find('input[type="text"]').length > 1) {
				if ($('#switchSections').hasClass('hide')) {
					$('#switchYes').prop('checked', false);
					$('#switchNo').prop('checked', true);
					$('#switchNo').click();
					$('#switchSections').removeClass('hide');
				}
				if ($('.js-sections-manage').hasClass('hide')) {
					$('#orderRandom').prop('checked', false);
					$('#orderStart').prop('checked', true);
					$('.js-sections-manage').removeClass('hide');
				}
			}
		}
		else {
			$('#tempSection').parent().addClass('has-error');
			$('#tempSection').parent().append('<span class="help-block">Please provide a section name.</span>');
		}
		
		//event listener for bluring sections name
		$('#sections input[type="text"]').off('blur');
		$('#sections input[type="text"]').on('blur', function() {
			if($('#orderStart').prop('checked')) {
				$('#orderStart').click();
			} else if($('#orderRandom').prop('checked')) {
				$('#orderRandom').click();
			}
		});
		$('#sections input[type="text"]:eq(0)').blur();
	});
	$('#switchYes').on('click', function() {
		$('#sectionTimes').addClass('hide');
		$('#totalTime').removeClass('hide');
	});
	$('#switchNo').on('click', function() {
		$('#totalTime').addClass('hide');
		var html = '';
		$('#sectionTimes .row:eq(0)').find('*').remove();
		if($('#sections').find('input[type="text"]').length == 0) {
			toastr.error('Please Add a section first.');
		}
		for(i=0; i<$('#sections').find('input[type="text"]').length; i++) {
			html+= '<div class="col-md-6">'+
                        '<div class="form-group">'+
                            '<label class="control-label">Total Time for '+$('#sections').find('input[type="text"]:eq('+i+')').val()+'</label>'+
                            '<div class="clearfix">'+
                                '<div class="dib">'+
                                    '<div class="input-group input-small">'+
                                        '<input type="number" class="form-control time hour" value="0" placeholder="Hours" aria-describedby="sizing-addon1">'+
                                        '<span class="input-group-addon" id="sizing-addon1">Hours</span>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="dib">'+
                                    '<div class="input-group input-small">'+
                                        '<input type="number" class="form-control time minute" value="0" placeholder="Minutes" aria-describedby="sizing-addon1">'+
                                        '<span class="input-group-addon" id="sizing-addon1">Minutes</span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'
		}
		$('#sectionTimes .row:eq(0)').append(html);
		$('#sectionTimes').removeClass('hide');
	});
	$("#noOfAttempts").keypress(function (e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			toastr.error('Enter Only Numbers');
			return false;
		}
	});
});
</script>