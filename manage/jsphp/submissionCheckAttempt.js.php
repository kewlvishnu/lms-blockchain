<script type="text/javascript">
function fetchSubmissionQuestions() {
	var req = {};
	var res;
	req.action = 'get-check-submission-questions';
	req.attemptId = attemptId;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else {
			//console.log(res);
			$("#studentName").html(res.student);
			$("#attemptNo").html(res.attemptNo);
			var checked = res.checked;
			if (checked==0) {
				$("#btnAttemptChecked").attr("disabled", false);
			};
			questions = res.questions;
			fillQuestions(res.access);
		}
	});
}
function fillQuestions(access) {
	$("#questionsContainer").html('');
	//console.log(questions);
	if (questions.length>0) {
		$("#questionsContainer").html('<div class="panel-group questions-panel" id="accQuestions" role="tablist" aria-multiselectable="true"></div>');
		for (var i = 0; i < questions.length; i++) {
			//questions[i]
			$("#accQuestions").append('<div class="panel panel-primary panel-questions">'+
						'<div class="panel-heading" role="tab" id="headQ'+i+'">'+
							'<h4 class="panel-title">'+
								'<a role="button" data-toggle="collapse" data-parent="#accordion" href="#bodyQ'+i+'" aria-expanded="true" aria-controls="bodyQ'+i+'">'+
								'Question #'+questions[i].qno+
								'</a>'+
							'</h4>'+
						'</div>'+
						'<div id="bodyQ'+i+'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headQ'+i+'">'+
						'</div>'+
					'</div>');
			var html = '<div class="panel-body no-space">'+
				'<div class="alert alert-default no-margin">'+questions[i].question+'</div>'+
				
				((!questions[i].attempted)?('<div class="alert alert-danger no-margin">'+
					'<div class="row">'+
						'<div class="col-md-12">'+
							'<label>Not attempted</label>'+
						'</div>'+
					'</div>'+
				'</div>'):(
				'<div class="alert alert-info no-margin">'+
					'<div class="row">'+
						'<div class="col-md-6">'+
							'<label>'+terminologies["student_single"]+'\'s Answer :</label>'+
							'<p>'+questions[i].answerText+'</p>'+
							'<div>'+
								((questions[i].answerFile.type=="pdf")?('<a href="javascript:void(0)" data-path="'+questions[i].answerFile.path+'" data-type="'+questions[i].answerFile.type+'" class="btn green js-popup-answer">Checkout the answer (PDF Format)</a>'):((questions[i].answerFile.type=="jpg" || questions[i].answerFile.type=="jpeg" || questions[i].answerFile.type=="png" || questions[i].answerFile.type=="gif")?('<a href="javascript:void(0)" data-path="'+questions[i].answerFile.path+'" data-type="'+questions[i].answerFile.type+'" class="btn green js-popup-answer">Checkout the answer (Image Format)</a>'):('<a href="'+questions[i].answerFile.path+'" class="btn green text-uppercase" target="_blank">Click to download file ('+questions[i].answerFile.type+')</a>')))+
							'</div>'+
						'</div>'+
						'<div class="col-md-6">'+
							'<label>Reviews/Comments :</label>'+
							'<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control">'+questions[i].review+'</textarea></div>'+
							'<div class="form-group">'+
								'<button class="btn btn-info js-answer-review" data-question="'+questions[i].id+'">Save Comments</button>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'
				));				
			
				if (access == "public") {
					html+= '<div class="alert alert-warning no-margin marks-time">'+
						'<div class="row">'+
							'<div class="col-md-6">'+
								'<label>Marks :</label>'+
								'<ul class="list-unstyled">'+
									'<li><i class="fa fa-check-square-o"></i>Maximum Marks: '+questions[i].max_marks+'</li>'+
									'<li><i class="fa fa-check-square-o"></i>Topper Marks: '+questions[i].topper_marks+'</li>'+
									'<li><i class="fa fa-check-square-o"></i>Average Marks: '+questions[i].avg_marks+'</li>'+
								'</ul>'+
							'</div>'+
							'<div class="col-md-6">'+
								'<label>Assign Marks :</label>';
					if (questions[i].attempted==0) {
						html+= '<div class="text-danger">Not attempted</div>';
					} else {
						html+= '<div class="input-group">'+
									'<input type="text" class="form-control" value="'+((questions[i].check==0)?'':questions[i].marks)+'">'+
									'<span class="input-group-addon" id="sizing-addon1">/'+questions[i].max_marks+'</span>'+
									'<span class="input-group-btn">'+
										'<button class="btn btn-primary js-assign-marks" data-question="'+questions[i].id+'" data-max="'+questions[i].max_marks+'">Set</button>'+
									'</span>'+
								'</div>';
					}
					html+=		'</div>'+
							'</div>'+
						'</div>'+
					'</div>';
				} else {
					html+= '<div class="alert alert-warning no-margin marks-time text-center">'+
						'<div class="row">'+
							'<div class="col-md-12">'+
								'<ul class="list-inline no-margin">'+
									'<li><i class="fa fa-check-square-o"></i>Maximum Marks: '+questions[i].max_marks+'</li>'+
									'<li><i class="fa fa-check-square-o"></i>Topper Marks: '+questions[i].topper_marks+'</li>'+
									'<li><i class="fa fa-check-square-o"></i>Average Marks: '+questions[i].avg_marks+'</li>'+
								'</ul>'+
							'</div>'+
						'</div>'+
					'</div>';
					html+= '<div class="alert alert-warning no-margin marks-time">';
					if (questions[i].attempted==0) {
						html+= '<div class="text-danger">Not attempted</div>';
					} else {
						html+= '<div class="row">';
						for (var j = 0; j < questions[i].marks.length; j++) {
							var marks = questions[i]['marks'][j].marks;
							var studentName = questions[i]['marks'][j].name;
							var studentId = questions[i]['marks'][j].studentId;
							html+= '<div class="col-sm-4">'+
									'<form class="form-inline">'+
										'<div class="form-group">'+
											'<label class="control-label" for="">'+studentName+'</label>'+
											'<div class="input-group">'+
												'<input type="text" class="form-control" value="'+((questions[i].check==0)?'':marks)+'">'+
												'<span class="input-group-addon" id="sizing-addon1">/'+questions[i].max_marks+'</span>'+
												'<span class="input-group-btn">'+
													'<button class="btn btn-primary js-assign-marks" data-question="'+questions[i].id+'" data-max="'+questions[i].max_marks+'" data-student="'+studentId+'">Set</button>'+
												'</span>'+
											'</div>'+
										'</div>'+
									'</form>'+
								'</div>';
						}
						html+= '</div>';
					}
					html+= '</div>';
				}
			$('#bodyQ'+i).append(html);
		};
	};
}
$(document).ready(function(){
	$('#questionsContainer').on("click", ".js-assign-marks", function() {
		var thiz		= $(this);
		var marks		= parseFloat($(this).closest('.input-group').find('.form-control').val());
		var questionId	= $(this).attr("data-question");
		var maxMarks	= parseFloat($(this).attr("data-max"));
		var studentId	= parseFloat($(this).attr("data-student"));
		if (marks>maxMarks) {
			toastr.error("Enter valid marks please!");
		} else if(!questionId) {
			toastr.error("Please, refresh and try again!");
		} else {
			var req = {};
			var res;
			req.action		= 'save-marks-for-submission';
			req.attemptId	= attemptId;
			req.questionId	= questionId;
			req.studentId	= studentId;
			req.marks		= marks;
			thiz.attr("disabled", true);
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndPoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				thiz.attr("disabled", false);
				res =  $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					toastr.success("Marks saved!");
				}
			});
		}
		return false;
	});
	$('#questionsContainer').on("click", ".js-answer-review", function() {
		var thiz		= $(this);
		var review		= $(this).closest('.col-md-6').find('.form-control').val();
		var questionId	= $(this).attr("data-question");
		if (!review) {
			toastr.error("Enter review/comment please!");
		} else if(!questionId && !answerId) {
			toastr.error("Please, refresh and try again!");
		} else {
			var req = {};
			var res;
			req.action		= 'save-review-for-submission';
			req.attemptId	= attemptId;
			req.review		= review;
			req.questionId	= questionId;
			thiz.attr("disabled", true);
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndPoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				thiz.attr("disabled", false);
				res =  $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					toastr.success("Review/comment saved!");
				}
			});
		}
	});
	$("#btnAttemptChecked").click(function() {
		var thiz= $(this);
		if(!thiz.attr('disabled')) {
			var req = {};
			var res;
			req.action		= 'mark-submission-checked';
			req.attemptId	= attemptId;
			thiz.attr("disabled", true);
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndPoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				//thiz.attr("disabled", false);
				res =  $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					toastr.success("Marked attempt as checked!");
				}
			});
		}
	});
	$('#questionsContainer').on('click', '.js-popup-answer', function(){
		var path = $(this).attr("data-path");
		var type = $(this).attr("data-type");
		if(!path || !type) {
			toastr.error("There is something wrong, please refresh and try again!");
		} else {
			if (type == "pdf") {
				$('#jsAnswer').html('<iframe src="'+path+'" frameborder="0" height="500" class="btn-block"></iframe>');
			} else {
				$('#jsAnswer').html('<img src="'+path+'" alt="Answer" class="btn-block" />');
			}
			$('#modalAnswerFile').modal('show');
		}
	});
});
</script>