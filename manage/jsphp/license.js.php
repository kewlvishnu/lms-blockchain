<script type="text/javascript">
/**
* Shows applicable licenses for the course and enables adding or deleting licenses
*/
var displayLicenses = {},
fetchLicenses       = {},
initLicensing       = {},
licensesList        = [
	'Own Course', 
	'One Year Licensing', 
	'One Time Transfer', 
	'Select IP Transfer', 
	'Commition based licensing'
],
licensesEnum        = {
	'ONE_YEAR'        : 1,
	'ONE_TIME'        : 2,
	'SELECTIVE_IP'    : 3,
	'COMISSION_BASED' : 4
},
licenseHandler      = {},
taxRates = {},
calculateTaxes = {};
var licenses;

/**
* Fetches the Licenses json
* @param courseId int The courseId being modified
* @param callback function The function that displays html data
*
* @author Archit Saxena
*/
fetchLicenses = function (callback) {
	var req = {},
	res     = {};
	req.courseId = courseId;
	req.action = "fetch-applicable-licenses";
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data'  : JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		//console.log(res);
		if (typeof callback === 'function') {
			// call displayProfessorDetails
			licenses = res;
			callback(res, licenseHandler);
		}
	});
};
/**
* Create required html and add it in the page
* @param professors Array contains array of professors returned by calling function
*
* @author Archit Saxena
*/
displayLicenses = function (res, callback) {
	var licenseType,
		flag = false,
		i,
		licenseApplicable,
		licenseSaved;
	if(res.status === 0) {
		return;
	}
	taxRates = res.taxes;
	for(i = 1; i <= 4; i++) {
		licenseApplicable = false;
		licenseSaved = false;
		license = res.savedLicenses[i];
		if($.inArray(i, res.applicableLicenses) > -1)
			licenseApplicable = true;
		if(typeof res.savedLicenses[i] !== 'undefined')
			licenseSaved = true;
		if(licenseApplicable || licenseSaved) {
			flag = true;
		}
	}
	if(typeof callback === 'function' && flag === true) {
		// call event Handler
		callback(res.savedLicenses);
	}
};
licenseHandler = function (licenses) {
	$('.show-price-breakdown').on("click", function () {
		$(this).parents('.license-box').find('.price-breakdown-details').toggleClass('hide');
		if($(this).attr('data-details-shown') == '0')
			$(this).text('Hide').attr('data-details-shown', 1);
		else
			$(this).text('View Details').attr('data-details-shown', 0);
	});
    /*$('.license-container input[type=number]').on("change", function() {
        showPriceBreakDown($(this).parents('.license-box')); 
    });*/
    $('.license-container input[type=number]').on("keyup", function() {
		showPriceBreakDown($(this).parents('.license-box')); 
    });
	$('#btnSaveRateStudent').on("click", function () {
		//validations for student rates
		
		var inrRate = $('#inputStudentPriceINR').val();
		if(inrRate == '') {
			toastr.error('Please fill the INR value.');
			return;
		}
		inrRate = parseFloat(inrRate);
		if(inrRate < 0 || inrRate > 100000) {
			toastr.error('Please select a value less than 1 lakh.');
			return;
		}
		
		var usdRate = $('#inputStudentPriceUSD').val();
		if(usdRate == '') {
			toastr.error('Please fill the USD value.');
			return;
		}
		usdRate = parseFloat(usdRate);
		if(usdRate < 0 || usdRate > 100000) {
			toastr.error('Please select a value less than 100,000.');
			return;
		}
	    var req = {},
	    res     = {};
	    req.courseId = courseId;
	    req.action = "save-course-rate-student";
	    req.studentPrice=$('#inputStudentPriceUSD').val();
	    req.studentPriceINR=$('#inputStudentPriceINR').val();
	    //req.availStudentMarket=$('#availStudentMarket').prop("checked")?1:0;
	    req.availStudentMarket=1;
	    $.ajax({
	        'type'  : 'post',
	        'url'   : ApiEndPoint,
	        'data'  : JSON.stringify(req)
	    }).done(function (res) {
			//console.log(res);
	        res =  $.parseJSON(res);
	        if(res.status == 1) {
				$('#live-course-student').attr('disabled', false);
	            toastr.success("Rates saved successfully.");
	        }
			else
				toastr.error(res.message);
	    });
	});
	$('#live-course-student').on("click", function () {
	    var req = {},
	    res     = {};
	    req.courseId = courseId;
	    req.action = "live-course-student";
		//req.ignoreWarning = false;
	    req.liveForStudent = 1;
		makeCourseLiveOnStudentMarketPlace(req);
	});
	//unlive handler
	$('#unlive-course-student').on("click", function () {
	    var req = {},
	    res     = {};
	    req.courseId = courseId;
	    req.action = "unlive-course-student";
	     $.ajax({
	        'type'  : 'post',
	        'url'   : ApiEndPoint,
	        'data'  : JSON.stringify(req)
	    }).done(function (res) {
			//console.log(res);
	        res =  $.parseJSON(res);
	        if(res.status == 1) {
				//$('#market-place-modal').modal('hide');
				toastr.success("Your "+terminologies["course_single"]+" removed from "+terminologies["student_single"]+" Market Place.");
				$('#unlive-course-student').addClass('hide');
				$('#live-course-student').removeClass('hide');
				$('#live-course-student').attr('disabled', true);
				$('#availStudentMarket').prop('checked', false);
			}
			else
				toastr.error(res.message);
	    });
	});
};

calculateTaxes = function(price) {
	if(price == '')
		price = 0;
	price = parseFloat(price);
	var priceBreakdown = {},
		taxAmts = {},
		totalTax = 0;
	$.each(taxRates, function(taxId, taxDetails){
		if(taxDetails.value == 0)
			return;
		taxAmts[taxDetails.taxname] = price * parseFloat(taxDetails.value) / 100;
		
		totalTax += taxAmts[taxDetails.taxname];
	});
	priceBreakdown['totalTax'] = totalTax;
	priceBreakdown['taxAmts'] = taxAmts;
	priceBreakdown['yourPrice'] = price - totalTax;
	return priceBreakdown;
}

showPriceBreakDown = function($elem) {
	priceBreakdownINR = calculateTaxes($elem.find('.price-inr').val());
	priceBreakdownUSD = calculateTaxes($elem.find('.price-usd').val());
	$elem.find('.your-price').html( priceBreakdownINR.yourPrice.toFixed(2) + ' INR, ' + priceBreakdownUSD.yourPrice.toFixed(2) + '$' );
	$elem.find('.price-breakdown').removeClass('hide');
	
	var html = '';
				+ '<h3>Your Price  : ' + priceBreakdownINR.yourPrice.toFixed(2) + ' INR</h3>';
				+ '<h3>Total Tax : ' + priceBreakdownINR.totalTax.toFixed(2) + ' INR</h3>';
	$.each(priceBreakdownINR.taxAmts, function(taxName, taxAmt) {
		html += '<div><span>' + taxName + ' : </span><span>' + taxAmt.toFixed(2) + ' INR</span></div>'
	});
	$elem.find('.inr-breakdown').html(html);
	html = '';
				+ '<h3>Your Price  : ' + priceBreakdownUSD.yourPrice.toFixed(2) + '$</h3>';
				+ '<h3>Total Tax : ' + priceBreakdownUSD.totalTax.toFixed(2) + '$</h3>';
	$.each(priceBreakdownUSD.taxAmts, function(taxName, taxAmt) {
		html += '<div><span>' + taxName + ' : </span><span>' + taxAmt.toFixed(2) + '$</span></div>'
	});
	$elem.find('.usd-breakdown').html(html);
}

/**
* Initiates the execution process
*
* @author Archit Saxena
*/
initLicensing = function () {
    /* --------------------------- event handlers ---------------------------- //
    $(document).ajaxStart(function() {
        $('#loader_outer').show();
    });
    $(document).ajaxComplete(function() {
        $('#loader_outer').hide();
    });*/

    fetchLicenses(displayLicenses);
};

//initiate function calls
initLicensing();

function makeCourseLiveOnStudentMarketPlace(req) {
	$.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
		//console.log(res);
        res =  $.parseJSON(res);
        if(res.status == 1) {
			//$('#market-place-modal').modal('hide');
			toastr.success("Your "+terminologies["course_single"]+" is live on "+terminologies["student_single"]+" Market Place.");
			$('#live-course-student').addClass('hide');
			$('#unlive-course-student').removeClass('hide');
		}
		else {
			toastr.error(res.message);
		}
    });
}

function makeCourseLiveOnContentMarketPlace(req) {
	$.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
		//console.log(res);
        res =  $.parseJSON(res);
        if(res.status == 1) {
			//$('#market-place-modal').modal('hide');
			toastr.success("Your "+terminologies["course_single"]+" is live on Content Market Place.");
			$('#liveForContentMarket').hide();
		}
		else {
			toastr.error(res.message);
		}
	});
}
</script>