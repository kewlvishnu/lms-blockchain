<script type="text/javascript">
var studentdArray={}; 
var subjectIds={};
var alreadyAssigned=[];
var assigndStudents=[];
var TableDatatablesManaged = function () {

    var initTable1 = function () {
        var table = $('#tableStudentsSubjects');

        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // setup buttons extension: http://datatables.net/extensions/buttons/
            buttons: [
                { extend: 'print', className: 'btn dark btn-outline' },
                { extend: 'pdf', className: 'btn green btn-outline' },
                { extend: 'csv', className: 'btn purple btn-outline ' }
            ],

            // scroller extension: http://datatables.net/extensions/scroller/
            scrollY:        300,
            deferRender:    true,
            scroller:       true,

            stateSave:      true,

            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [10, 15, 20, -1],
                [10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
        }

    };

}();
function fetchCourseSubjectStudent() {
    var req = {};
    var res;
    req.courseId = courseId;
    req.action ="get-student-subject-details";
    $.ajax({
        'type'  :   'post',
        'url'   :   ApiEndPoint,
        'data'  :   JSON.stringify(req)
    }).done(function(res) {
        res = $.parseJSON(res);
        console.log(res);
        if(res.status == 0)
            toastr.error(res.message);
        else {
            fillStudentsSubjects(res);
        }
    });
}
function fillStudentsSubjects(data) {
    //console.log(data);
    var headHtml="";
    var bodyHtml="";
    headHtml   = "<tr>"
                +"<td>" +terminologies["instructor_plural"]+ "</td>";
    $.each(data.Subjects, function (i, Subject) {
        subjectIds[i]=Subject.id;
        headHtml += "<td>" + Subject.name + "</td>";
    });
    headHtml   += "</tr>"
    $('#tableStudentsSubjects thead').html('').html(headHtml);
    //for setting rows
    $.each(data.students, function (i, student) {
        studentdArray[i]=student.userId ;
        bodyHtml += "<tr data-cid='" + student.userId + "'" + "" + ">"
                    +"<td>" + student.name + "</td>";
                    $.each(data.Subjects, function (i, Subject) {
                    bodyHtml +="<td><input  type='checkbox' id="+student.userId+'_'+Subject.id+"></td>";
                    });
        bodyHtml +="</tr>";
    });
    $('#tableStudentsSubjects tbody').html('').html(bodyHtml);
    $.each(data.Student_enrolled, function (i, student_enroll) {
        var index=jQuery.inArray(student_enroll.userId, assigndStudents )
        if(index<0){
            
            assigndStudents.push(student_enroll.userId);
        }
        $('#'+student_enroll.userId+'_'+student_enroll.subjectId).prop('checked', true);
        alreadyAssigned.push(student_enroll.userId+'_'+student_enroll.subjectId);
    });
    $.each(data.students, function (i, students) {
        var index=jQuery.inArray( students.userId, assigndStudents);
        if(index<0){
            $.each(data.Subjects, function (i, Subjects){
                $('#'+students.userId+'_'+Subjects.id).prop('checked', true);
                alreadyAssigned.push(students.userId+'_'+Subjects.id);
            });
            //console.log(students.userId)
        }
    });
    TableDatatablesManaged.init();
    $('.table-toolbar').removeClass('hide');
}
$('.js-remove').click(function(){
    if ($('#coursePic').data('Jcrop')) {
        var JcropAPI = $('#coursePic').data('Jcrop');
        JcropAPI.destroy();
        var originalImage = $('#coursePic').attr('old-image');
        $('#coursePic').prop('src', originalImage);
    } else {
        var req = {};
        req.action      = 'remove-profile-image';
        var res;
        ajaxRequest({
            'type'  :   'post',
            'url'   :   ApiEndPoint,
            'data'  :   JSON.stringify(req),
            success:function(res){
                res = $.parseJSON(res);
                if(res.status == 0)
                    toastr.error(res.message);
                else {
                    $('#coursePic').prop('src', res.profilePic);
                    $('#coursePic').prop('old-image', res.profilePic);
                }
            }
        });
    }
});

var _URL = window.URL || window.webkitURL;
var cropCoords,
file,
originalImageWidth, originalImageHeight,
uploadSize = 150,
previewSize = 500;

$("#fileCoursePic").on("change", function(){
    file = this.files[0];
    if (!!file) {
        $('.js-upload').removeClass('hide');
        var img;
        img = new Image();
        img.onload = function () {
            //console.log(this.width + " " + this.height);
            originalImageWidth = this.width;
            originalImageHeight = this.height;
            readFile(file, {
                width: originalImageWidth,
                height: originalImageHeight
            }).done(function(imgDataUrl, origImage) {
                //$("input, img, button").toggle();
                initJCrop(imgDataUrl);
            }).fail(function(msg) {
                toastr.error(msg);
            });
        };
        img.src = _URL.createObjectURL(file);
    } else {
        $('.js-upload').addClass('hide');
    }
});

$("#btnUpload").on("click", function(){
    var thiz = $(this);
    $(this).text("Uploading...").prop("disabled", true);

    readFile(file, {
        width: uploadSize,
        height: uploadSize,
        crop: cropCoords
    }).done(function(imgDataURI) {
        var data = new FormData();
        var blobFile = dataURItoBlob(imgDataURI);
        data.append('course-image', blobFile);
        data.append('courseId', courseId);
        
        ajaxRequest({
            url: FileApiPoint,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: 'json',
            success: function(res) {
                //console.log(res);
                if (res.status == 1) {
                    thiz.text('Upload');
                    var JcropAPI = $('#coursePic').data('Jcrop');
                    JcropAPI.destroy();
                    $('#coursePic').prop('style', '');
                    $('#coursePic').prop('src', res.image);
                    $('#coursePic').attr('old-image',res.image);
                    $('.js-upload').addClass('hide');
                    toastr.success(res.message);
                } else {
                    toastr.error(res.message);
                }
            },
            error: function(xhr) {
                toastr.error(blobFile);
            }
        });
    });
});

/*****************************
show local image and init JCrop
*****************************/
var initJCrop = function(imgDataUrl){
    var img = $("img#coursePic").attr("src", imgDataUrl);
    
    var storeCoords = function(c) {
        //console.log(c);
        cropCoords = c;
    };
    
    var w = img.width();
    var h = img.height();
    var s = uploadSize;
    img.Jcrop({
        onChange: storeCoords,
        onSelect: storeCoords,
        aspectRatio: 1,
        setSelect: [(w - s) / 2, (h - s) / 2, (w - s) / 2 + s, (h - s) / 2 + s],
        trueSize: [originalImageWidth, originalImageHeight]
    });
};
$(document).ready(function() {
    $(".star-rating").rating({displayOnly: true});
    $('#btnSaveAssigns').on('click', function() {
        alreadyChecked=$.unique(alreadyAssigned.sort());
        var arrayStudent={};
        var arraySubjectId={};
        var req = {};
        var res;
        
        $('#tableStudentsSubjects').find('input[type="checkbox"]:checked').each(function (i, row) {
            //this is the current checkbox
            var $actualrow = $(row);
            $checkbox = $actualrow.find('input:checked').context.id;
            var index=jQuery.inArray($checkbox, alreadyChecked )
            if(index!=-1)
            {
                //alreadyAssigned[index]='';
                 alreadyChecked.splice(index, 1);
            }
            var splitvalue= $checkbox.split('_');
            arrayStudent[i]=splitvalue[0];
            arraySubjectId[i]=splitvalue[1];
        });
        req.courseId = courseId;
        req.action ="set-student-subject-details";
        req.students=arrayStudent;
        req.subjects=arraySubjectId;
        req.alreadyChecked=alreadyChecked;
        //console.log(req);
        $.ajax({
            'type'  :   'post',
            'url'   :   ApiEndPoint,
            'data'  :   JSON.stringify(req)
        }).done(function(res) {
            res = $.parseJSON(res);
            if(res.status == 1){
                toastr.success(res.message);
                //window.location.href=sitepathManageCourses+courseId;
            } else {
                toastr.error(res.message);
            }
        });
    });
});
</script>