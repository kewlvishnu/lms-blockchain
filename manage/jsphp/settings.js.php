<script type="text/javascript">
//getemailUsername();
function checkSocialConnections() {
    var req = {};
    var res;
    req.action = 'get-social-connections';
    ajaxRequest({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req),
        success:function(res){
            var data = $.parseJSON(res);
            if (data.socials.facebookid>0) {
                $('.btn-facebook').text('Disconnect').removeClass('btn-success').addClass('btn-danger');
            } else {
                $('.btn-facebook').text('Connect').removeClass('btn-danger').addClass('btn-success');
            }
        }
    });
}
function getAccountTerminology() {
    var req = {};
    var res;
    req.action = 'get-account-terminology';
    ajaxRequest({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req),
        success:function(res){
            var data = $.parseJSON(res);
            if (data.status == 0) {
                toastr.error(data.message);
            } else {
                $("#selectAccountType").val(data['terminology'].account_type);
                $("#institute_single").val(data['terminology'].institute_single);
                $("#instructor_single").val(data['terminology'].instructor_single);
                $("#student_single").val(data['terminology'].student_single);
                $("#parent_single").val(data['terminology'].parent_single);
                $("#course_single").val(data['terminology'].course_single);
                $("#subject_single").val(data['terminology'].subject_single);
                $("#syllabus_single").val(data['terminology'].syllabus_single);
                $("#institute_plural").val(data['terminology'].institute_plural);
                $("#instructor_plural").val(data['terminology'].instructor_plural);
                $("#student_plural").val(data['terminology'].student_plural);
                $("#course_plural").val(data['terminology'].course_plural);
                $("#subject_plural").val(data['terminology'].subject_plural);
                $("#syllabus_plural").val(data['terminology'].syllabus_plural);
                $("#parent_plural").val(data['terminology'].parent_plural);
            }
        }
    });
}
$(document).ready(function(){
    $('#btnSetAddress').click(function(event) {
        /* Act on the event */
        $("#modalSetAddress").modal("show");
    });
    $('#formAccountType').submit(function(event) {
        /* Act on the event */
        var account_type = $("#selectAccountType").val();
        var institute_single = $("#institute_single").val();
        var instructor_single = $("#instructor_single").val();
        var student_single = $("#student_single").val();
        var parent_single = $("#parent_single").val();
        var course_single = $("#course_single").val();
        var subject_single = $("#subject_single").val();
        var syllabus_single = $("#syllabus_single").val();
        var institute_plural = $("#institute_plural").val();
        var instructor_plural = $("#instructor_plural").val();
        var student_plural = $("#student_plural").val();
        var parent_plural = $("#parent_plural").val();
        var course_plural = $("#course_plural").val();
        var subject_plural = $("#subject_plural").val();
        var syllabus_plural = $("#syllabus_plural").val();
        if (!account_type) {
            toastr.error("Account Type is compulsory");
            $("#selectAccountType").focus();
        } else if (!institute_single) {
            toastr.error("Institute Terminology is compulsory");
            $("#institute_single").focus();
        } else if (!instructor_single) {
            toastr.error("Instructor Terminology is compulsory");
            $("#instructor_single").focus();
        } else if (!student_single) {
            toastr.error("Student Terminology is compulsory");
            $("#student_single").focus();
        } else if (!parent_single) {
            toastr.error("Parent Terminology is compulsory");
            $("#parent_single").focus();
        } else if (!course_single) {
            toastr.error("Course Terminology is compulsory");
            $("#course_single").focus();
        } else if (!subject_single) {
            toastr.error("Subject Terminology is compulsory");
            $("#subject_single").focus();
        } else if (!syllabus_single) {
            toastr.error("Syllabus Terminology is compulsory");
            $("#syllabus_single").focus();
        } else if (!institute_plural) {
            toastr.error("Institute Terminology is compulsory");
            $("#institute_plural").focus();
        } else if (!instructor_plural) {
            toastr.error("Instructor Terminology is compulsory");
            $("#instructor_plural").focus();
        } else if (!student_plural) {
            toastr.error("Student Terminology is compulsory");
            $("#student_plural").focus();
        } else if (!parent_plural) {
            toastr.error("Parent Terminology is compulsory");
            $("#parent_plural").focus();
        } else if (!course_plural) {
            toastr.error("Course Terminology is compulsory");
            $("#course_plural").focus();
        } else if (!subject_plural) {
            toastr.error("Subject Terminology is compulsory");
            $("#subject_plural").focus();
        } else if (!syllabus_plural) {
            toastr.error("Syllabus Terminology is compulsory");
            $("#syllabus_plural").focus();
        } else {
            var req = {};
            req.account_type = account_type;
            req.institute_single = institute_single;
            req.instructor_single = instructor_single;
            req.student_single = student_single;
            req.parent_single = parent_single;
            req.course_single = course_single;
            req.subject_single = subject_single;
            req.syllabus_single = syllabus_single;
            req.institute_plural = institute_plural;
            req.instructor_plural = instructor_plural;
            req.student_plural = student_plural;
            req.parent_plural = parent_plural;
            req.course_plural = course_plural;
            req.subject_plural = subject_plural;
            req.syllabus_plural = syllabus_plural;
            var res;
            req.action = 'set-account-terminology';
            ajaxRequest({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req),
                success:function(res){
                    var data = $.parseJSON(res);
                    if (data.status == 1) {
                        toastr.success("Successfully saved account terminology");
                        window.location = window.location;
                    } else {
                        toastr.error(data.message);
                    }
                }
            });
        }
        return false;
    });
    $('.account-type').change(function(event) {
        /* Act on the event */
        var account_type = $(this).val();
        if (account_type == 'institute') {
            $("#institute_single").val("Institute");
            $("#instructor_single").val("Instructor");
            $("#student_single").val("Student");
            $("#parent_single").val("Guardian");
            $("#course_single").val("Course");
            $("#subject_single").val("Subject");
            $("#syllabus_single").val("Syllabus");
            $("#institute_plural").val("Institutes");
            $("#instructor_plural").val("Instructors");
            $("#student_plural").val("Students");
            $("#parent_plural").val("Guardians");
            $("#course_plural").val("Courses");
            $("#subject_plural").val("Subjects");
            $("#syllabus_plural").val("Syllabus");
        } else {
            $("#institute_single").val("Organization");
            $("#instructor_single").val("Trainer");
            $("#student_single").val("Trainee");
            $("#parent_single").val("Mentor");
            $("#course_single").val("Track");
            $("#subject_single").val("Program");
            $("#syllabus_single").val("Curriculum");
            $("#institute_plural").val("Organizations");
            $("#instructor_plural").val("Trainers");
            $("#student_plural").val("Trainees");
            $("#parent_plural").val("Mentors");
            $("#course_plural").val("Tracks");
            $("#subject_plural").val("Programs");
            $("#syllabus_plural").val("Curriculums");
        }
    });
    $("#btnDeletePersonalData").click(function(event) {
      /* Act on the event */
      var req = {};
      req.action      = 'delete-personal-data';
      var res;
      ajaxRequest({
          'type'  :   'post',
          'url'   :   ApiEndPoint,
          'data'  :   JSON.stringify(req),
          success:function(res){
              res = $.parseJSON(res);
              if(res.status == 0)
                  toastr.error(res.message);
              else {
                  toastr.success("Successfully deleted personal data!", "Personal Data");
              }
          }
      });
    });
    $("#btnDeleteAccount").click(function(event) {
      /* Act on the event */
      var req = {};
      req.action      = 'delete-account';
      var res;
      ajaxRequest({
          'type'  :   'post',
          'url'   :   ApiEndPoint,
          'data'  :   JSON.stringify(req),
          success:function(res){
              res = $.parseJSON(res);
              if(res.status == 0)
                  toastr.error(res.message);
              else {
                  toastr.success("Successfully deleted your account!", "Delete Account");
                  window.location = sitepathLogout;
              }
          }
      });
    });
    $('.btn-facebook').click(function(){
        var req = {};
        if($(this).text() == "Disconnect") {
            req.socialAction = "disconnect";
            var res;
            req.action = 'set-social-connection';
            ajaxRequest({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req),
                success:function(res){
                    var data = $.parseJSON(res);
                    if (data.status == 1) {
                        toastr.success("Successfully disconnected");
                        $('.btn-facebook').text('Connect').removeClass('btn-danger').addClass('btn-success');
                    } else {
                        toastr.error(data.message);
                    }
                }
            });
        } else {
            FBLogin();
        }
    });

    $.ajaxSetup({ cache: true });
    $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
        FB.init({
          appId: '877235672348652',
          version: 'v2.7' // or v2.1, v2.2, v2.3, ...
        });     
        /*$('#loginbutton,#feedbutton').removeAttr('disabled');
        FB.getLoginStatus(updateStatusCallback);*/
    });

    // This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response) {

      if (response.status === 'connected') {
          // Logged into your app and Facebook.
          // we need to hide FB login button
          $('#fblogin').hide();
          //fetch data from facebook
          getUserInfo();
      } else if (response.status === 'not_authorized') {
          // The person is logged into Facebook, but not your app.
          $('#status').html('Please log into this app.');
      } else {
          // The person is not logged into Facebook, so we're not sure if
          // they are logged into this app or not.
          $('#status').html('Please log into facebook');
      }
    }

    function FBLogin()
    {
          FB.login(function(response) {
                if (response.authResponse) 
                {
                    getUserInfo(); //Get User Information.
                } else
                {
                    toastr.error('Authorization failed.');
                }
           },{scope: 'public_profile,email'});
    }

    function getUserInfo() {
        FB.api('/me', function(response) {
            console.log(response);
            var req = {};
            req.socialAction = "connect";
            var res;
            req.facebookid = response.id;
            req.action = 'set-social-connection';
            ajaxRequest({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req),
                success:function(res){
                    var data = $.parseJSON(res);
                    if (data.status == 1) {
                        toastr.success("Successfully connected");
                        $('.btn-facebook').text('Disconnect').removeClass('btn-success').addClass('btn-danger');
                    } else {
                        toastr.error(data.message);
                    }
                }
            });
        });
    }

    function FBLogout()
    {
        FB.logout(function(response) {
           $('#fblogin').show(); //showing login button again
           $('#fbstatus').hide(); //hiding the status
      });
    }

    // This function is called when someone finishes with the Login
    // Button.  See the onlogin handler attached to it in the sample code below.
    function checkLoginState() {
      FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
      });
    }
});
</script>