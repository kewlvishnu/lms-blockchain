<script type="text/javascript">
var pageLimit = 10;
var pageNumber = 1;
function fetchNotificationsActivity() {
    var req = {};
    var res;
    req.pageNumber = parseInt(pageNumber)-1;
    req.pageLimit  = pageLimit;
    req.action = 'get-institute-notifications-activity';
    ajaxRequest({
        'type'  :   'post',
        'url'   :   ApiEndPoint,
        'data'  :   JSON.stringify(req),
        success:function(res){
            res = $.parseJSON(res);
            if(res.status == 0)
                toastr.error(res.message);
            else {
                notifications = res;
                fillNotificationsActivity(res);
            }
        }
    });
}
function fillNotificationsActivity (data) {
    //console.log(data);
    if (data.notiCount>0) {
        var html = '';
        for(var i = 0; i < data.sitenotifications.length; i++) {
            var d = new Date(data.sitenotifications[i].timestamp);
            var timeAgo = timeSince(d.getTime()); //returns 1340220044000
            //console.log(timeAgo);
            var inviteLink = '';
			if (userRole == 2 && data.sitenotifications[i].message.indexOf('invited') >= 0) {
				inviteLink = ' <a href="'+sitepathManageInvitations+'" class="btn green btn-xs">Invitations</a>';
			}

            html += '<li>'+
                        '<div class="col1">'+
                            '<div class="cont">'+
                                '<div class="cont-col1">'+
                                    '<div class="label label-sm label-'+((data.sitenotifications[i].status==0)?('info'):('default'))+'">'+
                                        '<i class="fa fa-bullhorn"></i>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="cont-col2">'+
                                    '<div class="desc"> '+data.sitenotifications[i].message+inviteLink+'</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col2">'+
                            '<div class="date"> '+timeAgo+' </div>'+
                        '</div>'+
                    '</li>';
        }
        if ($('#notifContent').html() != '') {
            var totalNotifications = data.notiCount;
            var noOfPages = parseInt(totalNotifications/pageLimit);
            var handleDynamicPagination = function() {
                $('#pagerInvites').bootpag({
                    paginationClass: 'pagination',
                    next: '<i class="fa fa-angle-right"></i>',
                    prev: '<i class="fa fa-angle-left"></i>',
                    total: noOfPages,
                    page: pageNumber,
                }).on("page", function(event, num){
                	pageNumber = num;
                    fetchNotificationsActivity();
                });
            }
            handleDynamicPagination();
        }
        $('#notifContent').html(html);
    } else {

        $('#notifContent').html('No notifications.');
    }
}
$('body').on('click', '.btn-bookmark', function(e){
    bookmarkThisPage(e);
});
//function to fill notifications table
function fillNotifications(data) {
	var html = '';
	$('#notify-list').find('li').remove();
	$.each(data.notification, function (i, notification) {
		html += '<li data-id="'+notification.id+'">'
				+ '<div class="task-title">'
					+ '<span class="task-title-sp">'+notification.message+'</span>'
					+ '<div class="pull-right hidden-phone">';
				if(notification.status == 0)
						html += '<button class="btn btn-success btn-xs mark-notification"><i class=" fa fa-check"></i> Mark Read </button>&emsp;';
					html += '</div>'
				+ '</div>'
			+ '</li>';
	});
	if (!!data.chatnotifications) {
		for(var i = 0; i < data.chatnotifications.length; i++) {
			var link = '';
			switch(data.chatnotifications[i].roleId) {
				case '1':
					link = 'institute/' + data.chatnotifications[i].userId;
					link = "<a href='"+link+"' target='_blank'><strong>"+data.chatnotifications[i].senderName+"</strong></a>";
					break;
				case '2':
					link = 'instructor/' + data.chatnotifications[i].userId;
					link = "<a href='"+link+"' target='_blank'><strong>"+data.chatnotifications[i].senderName+"</strong></a>";
					break;
				case '3':
					link = 'institute/' + data.chatnotifications[i].userId;
					link = "<a href='"+link+"' target='_blank'><strong>"+data.chatnotifications[i].senderName+"</strong></a>";
					break;
				case '4':
					link = "<strong>"+data.chatnotifications[i].senderName+"</strong>";
					break;
			}
			var message = "";
			switch(data.chatnotifications[i].type) {
				case '0':
					if (data.chatnotifications[i].room_type == "public") {
						message = '<span class="task-title-sp"> '+link+' just started conversation in <strong>'+terminologies["subject_single"]+' : '+data.chatnotifications[i].courseName+'</strong> of <strong>'+terminologies["course_single"]+' : '+data.chatnotifications[i].subjectName+'</strong></span>';
					} else {
						message = '<span class="task-title-sp"> '+link+' just started conversation with you in <strong>'+terminologies["subject_single"]+' : '+data.chatnotifications[i].courseName+'</strong> of <strong>'+terminologies["course_single"]+' : '+data.chatnotifications[i].subjectName+'</strong></span>';
					}
					break;
				case '1':
					if (data.chatnotifications[i].room_type == "public") {
						message = '<span class="task-title-sp"> '+link+' just sent a message in <strong>'+terminologies["subject_single"]+' : '+data.chatnotifications[i].courseName+'</strong> of <strong>'+terminologies["course_single"]+' : '+data.chatnotifications[i].subjectName+'</strong></span>';
					} else {
						message = '<span class="task-title-sp"> '+link+' just messaged you in <strong>'+terminologies["subject_single"]+' : '+data.chatnotifications[i].courseName+'</strong> of <strong>'+terminologies["course_single"]+' : '+data.chatnotifications[i].subjectName+'</strong></span>';
					}
					break;
			}
			html += '<li data-id="'+data.chatnotifications[i].id+'">'
					+ '<div class="task-title">'
						+ message
						+ '<div class="pull-right hidden-phone">'
							+ '<button class="btn btn-success btn-xs chat-button" data-chat="1"><i class=" fa fa-check"></i> Start Chat</button>&emsp;'
							+ '<button class="btn btn-danger btn-xs chat-button" data-chat="0"><i class="fa fa-times"></i> Mark read</button>'
						+ '</div>'
					+ '</div>'
				+ '</li>';
		}
	}
	if(html == '')
		html = 'No new notification found.';
	$('#notify-list').append(html);

	//adding event handlers for accept button
	$('.chat-button').on('click', function() {
		var id = $(this).parents('li').attr('data-id');
		var req = {};
		var li = $(this).parents('li');
		var res;
		req.action = 'mark-chat-notification';
		req.notificationId  = id;
		req.chatAction = $(this).attr('data-chat');
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				toastr.error(res.message);
			else {
				if (!res.roomId) {
					toastr.errorMsg("Notification marked as Read.");
					li.remove();
					fetchNotificationCount();
				} else {
					li.remove();
					fetchNotificationCount();
					window.open('../messenger/'+res.roomId);
				}
			}
		});
	});
	
	//adding event handlers for accept button
	$('.mark-notification').on('click', function() {
		var id = $(this).parents('li').attr('data-id');
		var req = {};
		var li = $(this).parents('li');
		var res;
		req.action = 'mark-notification';
		req.notificationId  = id;
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				toastr.error(res.message);
			else {
				toastr.errorMsg("Notification marked as Read. ");
				li.find('.mark-notification').remove();
				fetchNotificationCount();
			}
		});
	});
}
</script>