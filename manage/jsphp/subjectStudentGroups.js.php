<script type="text/javascript">
var keys_available = 0;
var keys_pending = 0;
var students;
function fetchSubjectStudents() {
    var req = {};
    var res;
    req.courseId = courseId;
    req.subjectId = subjectId;
    req.action ="get-subject-students";
    req.type = 'registered';
    $.ajax({
        'type'  :   'post',
        'url'   :   ApiEndPoint,
        'data'  :   JSON.stringify(req)
    }).done(function(res) {
        var data = $.parseJSON(res);
        //console.log(data);
        if(data.status == 0)
            toastr.error(data.message);
        else {
            students = data.students;
            fetchSubjectStudentGroups();
        }
    });
}

//function to get the students and chapters for the subject
function fetchSubjectStudentGroups() {
    var req = {};
    var res;
    req.courseId = courseId;
    req.subjectId = subjectId;
    req.action ="get-subject-student-groups";
    $.ajax({
        'type'  :   'post',
        'url'   :   ApiEndPoint,
        'data'  :   JSON.stringify(req)
    }).done(function(res) {
        data = $.parseJSON(res);
        //console.log(data);
        if(data.status == 0)
            toastr.error(data.message);
        else {
            //console.log(data);
            var html = "";
            $('.js-students').html("");
            if(data.studentGroups.length > 0) {
                for (var i = 0; i < data.studentGroups.length; i++) {
                    var studentGroupId = data.studentGroups[i]['id'];
                    $('.js-students').append(
                        '<div class="col-md-6">'+
                            '<h4>'+
                                '<label class="mt-checkbox mt-checkbox-outline">'+
                                    '<input type="checkbox" name="optionStudentGroups[]" value="'+data.studentGroups[i]['id']+'" class="js-student-groups">'+data.studentGroups[i]['name']+
                                    '<span></span>'+
                                '</label>'+
                            '</h4>'+
                            '<div class="form-group"><select name="listStudentslistStudents'+i+'[]" id="listStudents'+studentGroupId+'" class="form-control list-students" multiple="multiple" data-group="'+studentGroupId+'"></select></div>'+
                        '</div>');
                    //var students = data.studentGroups[i]['students'];
                    //console.log(students);
                    var studentGroups = data.studentGroups[i]['students'];
                    var studentGroupStudents = [];
                    if (studentGroups.length>0) {
                        var k = 0;
                        for (var j = 0; j < studentGroups.length; j++) {
                            //console.log(studentGroups);
                            studentGroupStudents[k] = parseInt(studentGroups[j].studentId);
                            k++;
                        }
                    }
                    if(students.length > 0) {
                        for (var j = 0; j < students.length; j++)
                        {
                            var student_id = parseInt(students[j].id);
                            var name = students[j].name;
                            var selected = '';
                            if (studentGroupStudents.indexOf(student_id) > -1) {
                                selected = 'selected="selected"';
                            }
                            $('.js-students .col-md-6:last-child .list-students').append('<option value="'+studentGroupId+'-'+student_id+'" '+selected+'>' + name + '</option>');
                        }
                    }
                    $('#listStudents'+studentGroupId).multiSelect({
                        afterSelect: function(values){
                            processSubjectStudentGroup(values,'add');
                        },
                        afterDeselect: function(values){
                            processSubjectStudentGroup(values,'remove');
                        }
                    });
                }
            } else {
                $('.js-students').html('<div class="col-md-12"><p>No '+terminologies["student_single"].toLowerCase()+' groups yet!</p> <button class="btn blue" id="btnDivide">Divide '+terminologies["student_plural"].toLowerCase()+' among groups</button></div>');
            }
        }
    });
}
function processSubjectStudentGroup(values,action) {
    var studGroup = values[0].split("-");
    var groupId = studGroup[0];
    var studentId = studGroup[1];
    var req = {};
    var res;
    req.courseId = courseId;
    req.subjectId = subjectId;
    if (action == 'add') {
        req.action ="add-student-subject-student-group";
    } else {
        req.action ="remove-student-subject-student-group";
    }
    req.studentGroupId = groupId;
    req.studentId = studentId;
    $.ajax({
        'type'  :   'post',
        'url'   :   ApiEndPoint,
        'data'  :   JSON.stringify(req)
    }).done(function(res) {
        var data = $.parseJSON(res);
        //console.log(data);
        if(data.status == 0) {
            $('#listStudents'+groupId).multiSelect('deselect', values);
            toastr.error(data.message);
        }
        else {
            toastr.success(data.message);
        }
    });
}
function get_keys_remaining() {
    var req = {};
    req.action = 'keys_avilable';
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        //console.log(data);
        if (data.status == 1) {
            keys_available = data.keys;
            keys_pending = data.invitation;
            $('#jsRemaining').attr('data-value',keys_available);
            $('#jsPending').attr('data-value',keys_pending);
            $('#jsRemaining,#jsPending').counterUp({
                delay: 10,
                time: 2000
            });
            $('.js-pending-keys').html(keys_pending);
            $('.js-available-keys').html(keys_available);
        }
        else {
            toastr.error(res.msg);
        }
    });
}
function inviteStudent(req) {
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        if (data.status == 1) {
            toastr.error(data.message);
            $('#modalInviteStudents').modal('hide');
            get_keys_remaining();
        }
        else {
            toastr.error('<span class="help-block text-danger">' + data.message + ' </span>');
        }    
    });
}
$(document).ready(function() {
    $('#btnAddStudentGroup').click(function(event) {
        /* Act on the event */
        bootbox.prompt("Please enter "+terminologies["student_single"].toLowerCase()+" group name :", function(result) {
            result = $.trim(result);
            if (result === null) {
            } else if (result == '') {
                toastr.error("Hi <b>"+result+"</b>");
            } else {
                var req = {};
                var res;
                req.courseId = courseId;
                req.subjectId = subjectId;
                req.studentGroupName = result;
                req.action ="add-subject-student-group";
                $.ajax({
                    'type'  :   'post',
                    'url'   :   ApiEndPoint,
                    'data'  :   JSON.stringify(req)
                }).done(function(res) {
                    data = $.parseJSON(res);
                    //console.log(data);
                    if(data.status == 0)
                        toastr.error(data.message);
                    else {
                        //console.log(data);
                        toastr.success(data.message);
                        fetchSubjectStudents();
                    }
                });
            }
        });
    });
    $('#btnRemoveStudents').click(function(event) {
        /* Act on the event */
        var studentGroups = $('.js-student-groups:checked');
        if (studentGroups.length < 1) {
            toastr.error('Please select at least one '+terminologies["student_single"].toLowerCase()+' group');
        } else {
            var studentGroupIds = [];
            var i = 0;
            studentGroups.each(function(index, el) {
                studentGroupIds[i] = $(el).val();
                i++;
            });
            var req = {};
            var res;
            req.courseId = courseId;
            req.subjectId = subjectId;
            req.studentGroups = studentGroupIds;
            req.action ="remove-subject-student-groups";
            $.ajax({
                'type'  :   'post',
                'url'   :   ApiEndPoint,
                'data'  :   JSON.stringify(req)
            }).done(function(res) {
                data = $.parseJSON(res);
                //console.log(data);
                if(data.status == 0)
                    toastr.error(data.message);
                else {
                    //console.log(data);
                    toastr.success(data.message);
                    fetchSubjectStudents();
                }
            });
        }
    });
    $('.js-students').on('click', '#btnDivide', function(event) {
        event.preventDefault();
        /* Act on the event */
        bootbox.prompt("Number of "+terminologies["student_plural"].toLowerCase()+" per group?", function(result) {
            if (result === null) {
                toastr.error("Please enter a number");
            } else if (!$.isNumeric(result)) {
                toastr.error("Please enter a number");
            } else if (result<2) {
                toastr.error("Atleast 2 "+terminologies["student_plural"].toLowerCase()+" per group are needed");
            } else {
                var req = {};
                var res;
                req.courseId = courseId;
                req.subjectId = subjectId;
                req.count = result;
                req.action ="gen-subject-student-groups";
                $.ajax({
                    'type'  :   'post',
                    'url'   :   ApiEndPoint,
                    'data'  :   JSON.stringify(req)
                }).done(function(res) {
                    data = $.parseJSON(res);
                    //console.log(data);
                    if(data.status == 0)
                        toastr.error(data.message);
                    else {
                        //console.log(data);
                        toastr.success("Successfully generated "+terminologies["student_single"].toLowerCase()+" groups!");
                        fetchSubjectStudentGroups();
                    }
                });
            }
        });
    });
    $(".star-rating").rating({displayOnly: true});
    $('#btnInviteStudents').on('click', function () {
        var invalid = '';
        var correct = true
        var emails = $.trim($('#inputEmailIds').val());
        var email = emails.split(',');
        if (emails == '') {
            toastr.error('Please enter email address.');
            // toastr.error('Please enter email id ');
            return false;
        }
        if (checkDuplicates(email) == 'false') {
            toastr.error('Please remove duplicates.');
            //toastr.error('Please remove duplicates ');
            return false;
        }
        if ((email.length > keys_available)) {
            toastr.error('You have ' + keys_available + ' tokens. Please add token in your account to assign '+terminologies["student_plural"].toLowerCase()+'.');
            // toastr.error('You have only ' + keys_available + ' available ');
            return false;
        }
        $.each(email, function (index, value) {
            if (validateEmail($.trim(value)) == false) {
                invalid += value + ',';
                correct = false;
            }
        });
        if (correct == false) {
            toastr.error('Correct these email addresses:\n' + invalid + '');
            //toastr.error('Correct these email addresses:\n' + invalid);
            return false;
        }
        else {
            var req = {};
            req.courseId = courseId;
            req.action = 'send_activation_link_student';
            req.email = email;
            req.enrollment_type = 0;
            //req.ignoreWarning = false;
            inviteStudent(req);
        }
    });
});
</script>