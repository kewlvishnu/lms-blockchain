<script type="text/javascript">
var reqTag = {};
reqTag.action = 'tags';
var ms = $('#magicsuggest').magicSuggest({
	valueField: 'name',
	data: TypeEndPoint,
	dataUrlParams: reqTag
});
function fetchSubmissionQuestions() {
	var req = {};
	var res;
	req.action = 'get-subject-submission-questions';
	req.subjectId = subjectId;
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if (res.status == 0) {
			toastr.error(res.message);
		} else {
			//console.log(res);
			fillSubjectQuestions(res);
		}
	});
}
function fillSubjectQuestions(data) {
	$('#sectionQuestions').html('');
	//if (data.questions.length>0) {
		var queshtml = '';
		var questions = data.questions;
		var status = '<i class="icon-check text-success tooltips" data-placement="right" data-original-title="Green symbol represents that the  question  is complete. The question would be counted in the no of entered questions"></i>';
		queshtml+= '<div class="portlet light portlet-fit no-space margin-bottom-10">'+
	                    '<div class="portlet-body no-space">'+
	                        '<div class="mt-element-list mt-element-list-sm questions-sidebar-list">'+
								'<div class="mt-list-head list-simple ext-1 font-white bg-grey-dark">'+
								    '<div class="list-head-title-container">'+
								        '<h3 class="list-title">Submission Question Bank</h3>'+
								    '</div>'+
								'</div>'+
	                            '<div class="mt-list-head list-default ext-1 green-jungle">'+
	                                '<div class="row">'+
	                                    '<div class="col-xs-8">'+
	                                        '<div class="list-head-title-container">'+
	                                            '<h3 class="list-title uppercase sbold no-margin">SUBMISSION QUESTIONS</h3>'+
	                                        '</div>'+
	                                    '</div>'+
	                                    '<div class="col-xs-4 text-right">'+
	                                    	'<a class="btn dark btn-xs new-question" href="javascript:;">'+
	                                            '<i class="fa fa-plus"></i>'+
	                                        '</a>'+
                                            /*'<a class="btn dark btn-xs collapsed" role="button" data-toggle="collapse" href="#collapse0" aria-expanded="false" aria-controls="collapse0"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></a>'+*/
                                        '</div>'+
	                                '</div>'+
	                            '</div>'+
	                            '<div class="" id="collapse0">'+
		                            '<div class="mt-list-container list-default ext-1 no-padding">'+
		                                /*'<div class="mt-list-title uppercase">List of questions'+
		                                    '<span class="badge badge-default pull-right bg-hover-green-jungle">'+
		                                        '<a class="font-white new-question" href="javascript:;">'+
		                                            '<i class="fa fa-plus"></i>'+
		                                        '</a>'+
		                                    '</span>'+
		                                '</div>'+*/
		                                '<ul class="questions">';
	    var questionHtml = '';
	    if(questions.length > 0) {
			var questionNumber = 0;
			for(var j=0; j<questions.length; j++) {
				var dispQuestionNumber = (questionNumber + 1);
				questionNumber++;
				var questionText = $(questions[j]['question']).text();
				if(questionText.length > 35)
					questionText = questionText.substring(0, 35) + '...';
				questionHtml+= '<li class="mt-list-item done" data-id="'+questions[j]['id']+'" data-status="'+questions[j]['status']+'" data-category="'+questions['id']+'">'+
                                '<div class="list-icon-container">'+
                                    '<a href="javascript:;">'+
                                    	status+
                                        //'<i class="icon-check"></i>'+
                                    '</a>'+
                                '</div>'+
                                '<div class="list-datetime"> <a href="javascript:void(0)" class="btn btn-danger btn-xs remove-question"><i class="fa fa-trash"></i></a> </div>'+
                                '<div class="list-item-content">'+
                                    '<h3 class="uppercase">'+
                                        '<a href="javascript:;">SUBMISSION TASK</a>'+
                                    '</h3>'+
                                    '<p class="text-overflow">'+ questionText +'</p>'+
                                '</div>'+
                            '</li>';
			}
		}
		else {
			questionHtml += '<li class="mt-list-item">No question found</li>';
		}
		queshtml += questionHtml;
		queshtml +=				'</ul>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>';
        //console.log(queshtml);
		$('#sectionQuestions').append(queshtml);
	//}
}
function fetchQuestionDetails(id) {
	$('#sectionNewQuestion .cancel-button').click();
	var req = {};
	req.action = 'get-submission-question-detail';
	req.questionId = id;
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 1) {
			fillQuestionEditQuestion(res);
		}
		else
			toastr.error(res.exception);
	});
}
function fillQuestionEditQuestion(data) {
	if(data.question[0].status == 2) {
		var warningSpan = '<div class="text-danger" style="margin-left: 15px;margin-right: 15px;">Duplicate : Question copied from same Assignment/Exam. Edit question to save and increase question count in category.</div>';
		$(warningSpan).insertBefore('#questionId');
	}
	else
		$('#sectionNewQuestion').find('div.text-danger').remove();
	$('#sectionNewQuestion').removeClass('hide');
	$('#questionId').val(data.question[0]['id']);
	CKEDITOR.instances['question'].setData(data.question[0]['question']);
	$('#selectDifficulty').val(data.question[0]['difficulty']);
	ms.setSelection(data.tags);
	CKEDITOR.instances['desc'].setData(data.question[0]['description']);
}
function newQuestionOps() {
	$('#sectionNewQuestion').removeClass('hide');
	$('#sectionNewQuestion .cancel-button').click();
	$('#questionId').val(0);
	$('.js-file-path').html('').attr('data-path', '');
}
$(document).ready(function(){

	$('#sectionQuestions').on('click', '.mt-list-item', function() {
		if($(this).attr('data-id') != undefined) {
			fetchQuestionDetails($(this).attr('data-id'));
		}
	});
	$('#sectionQuestions').on('click', '.remove-question', function(e) {
		e.stopPropagation();
		var con = false;
		con = confirm("Are you sure to delete this question");
		if(con) {
			$('#sectionNewQuetion .cancel-button').click();
			var req = {};
			req.action = 'delete-submission-question';
			req.questionId = $(this).parents('.mt-list-item').attr('data-id');
			req.status = $(this).parents('.mt-list-item').attr('data-status');
			req.questionType = $(this).parents('.mt-list-item').attr('data-questionType');
			if(req.questionType == 4)
				req.amount = $(this).parents('.mt-list-item').find('.totalQuestions').text();
			//req.categoryId = $(this).parents('.mt-list-item').attr('data-category');
			req.sectionId = $('#sectionName').attr('data-id');
			req.subjectId = subjectId;
			$('#sectionNewQuestion .cancel-button').click();
			$.ajax({
				'type'	: 'post',
				'url'	: ApiEndPoint,
				'data'	: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else if(res.status == 1) {
					fetchSubmissionQuestions();
					newQuestionOps();
				}
			});
		}
	});
	
	$('#sectionNewQuestion .cancel-button').on('click', function() {
		/*setTimeout(function() {
			if(CKEDITOR.instances['question'])
				CKEDITOR.instances['question'].destroy(true);
			$('#question').attr('placeholder', 'Please enter the question text here');
			ckeditorOn('question');
		}, 1500);*/
		//$('div[aria-describedby="cke_43"]').html('Please enter the question text here');
		$('.help-block').remove();
		$('#sectionNewQuestion').find('div.text-danger').remove();

		$('#sectionNewQuestion .generated').remove();
		$('#questionId').val(0);
		$('#question').val('');
		$('#selectDifficulty').val(0);
		ms.clear();
		$('#sectionNewQuestion').find('input[type="text"]').val('');
		$('#sectionNewQuestion').find('input[type="radio"]').prop('checked', false);
		$('#sectionNewQuestion').find('input[type="checkbox"]').prop('checked', false);
		//removing text from ckeditor textboxes
		for(instance in CKEDITOR.instances) {
			CKEDITOR.instances[instance].setData('');
			CKEDITOR.instances[instance].fire('blur');
		}
	});
	
	$('#newQuestion').on('click', function() {
		newQuestionOps();
	});

	$('body').on('click', '.new-question', function() {
		$('#newQuestion').trigger('click');
	});
	
	$('#sectionNewQuestion .save-button').on('click', function() {
		if($(CKEDITOR.instances['question'].getData()).text().length <= 3) {
			toastr.error("Please give your question text here which should be longer than 3 characters.");
		} else {
			var req = {};
			if($('#questionId').val() == 0)
				req.action = 'save-submission-question';
			else {
				req.action = 'edit-submission-question';
				req.questionId = $('#questionId').val();
			}
			req.question = CKEDITOR.instances['question'].getData();
			req.difficulty = $("#selectDifficulty").val();
			req.desc = CKEDITOR.instances['desc'].getData();
			req.subjectId = subjectId;
			req.courseId=courseId;
			req.tags = ms.getValue();
			$.ajax({
				'type'	: 'post',
				'url'	: ApiEndPoint,
				'data'	: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					fetchSubmissionQuestions();
					newQuestionOps();
				}
			});
		}
	});
});
</script>