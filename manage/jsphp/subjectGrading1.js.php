<script type="text/javascript">
function fetchSubjectAllExams() {
    var req = {};
    var res;
    req.courseId = courseId;
    req.subjectId = subjectId;
    //req.action ="get-subject-progress-details";  
    req.action ="get-subject-exams";
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        if(res.status == 1)
            fillSubjectAllExams(res);
        else
            toastr.error(res.message);
    });
}
function fillSubjectAllExams(data) {
    var html = '';
    if (data.notes) {
        CKEDITOR.instances['notes'].setData(data.notes);
    }
    if (data.exams) {
        var exams = data.exams;
        for (var i = 0; i < exams.length; i++) {
            var endDate = '-';
            if (exams[i].endDate) {
                endDate = formatDate(parseInt(exams[i].endDate), '-');
            }
            var section = exams[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            var attemptsList = '';
            for (var j = 1; j <= exams[i].attempts; j++) {
                attemptsList+= '<option value="'+j+'" '+((exams[i].avg_attempts==j)?'selected':'')+'>'+j+'</option>';
            }
            html+='<tr data-id="'+exams[i].id+'" data-type="exam">'+
                    '<td>'+exams[i].name+'</td>'+
                    '<td>'+exams[i].type+'</td>'+
                    '<td>'+section+'</td>'+
                    '<td>'+formatDate(parseInt(exams[i].startDate), '-')+'</td>'+
                    '<td>'+endDate+'</td>'+
                    '<td>'+exams[i].attempts+'</td>'+
                    '<td><select name="" class="js-grade-type"><option value="percentage" '+((exams[i].grade_type=='percentage')?'selected':'')+'>Percentage</option><option value="percentile" '+((exams[i].grade_type=='percentile')?'selected':'')+'>Percentile</option></select></td>'+
                    '<td>'+
                        '<div class="js-percentage '+((exams[i].grade_type=='percentage')?'':'hide')+'">'+
                            '<select class="btn-block">'+attemptsList+'</select>'+
                        '</div>'+
                        '<div class="js-percentile '+((exams[i].grade_type=='percentile')?'':'hide')+' text-center"><span class="badge">N.A.</span></div>'+
                    '</td>'+
                    '<td><input type="text" class="form-control input-sm js-weight" value="'+exams[i].weight+'" /></td>'+
                '</tr>';
        }
    }
    if (data.subjective_exams) {
        var exams = data.subjective_exams;
        for (var i = 0; i < exams.length; i++) {
            var endDate = '-';
            if (exams[i].endDate) {
                endDate = formatDate(parseInt(exams[i].endDate), '-');
            }
            var section = exams[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            var attemptsList = '';
            for (var j = 1; j <= exams[i].attempts; j++) {
                attemptsList+= '<option value="'+j+'" '+((exams[i].avg_attempts==j)?'selected':'')+'>'+j+'</option>';
            }
            html+='<tr data-id="'+exams[i].id+'" data-type="subjective-exam">'+
                    '<td>'+exams[i].name+'</td>'+
                    '<td>Subjective Exam</td>'+
                    '<td>'+section+'</td>'+
                    '<td>'+formatDate(parseInt(exams[i].startDate), '-')+'</td>'+
                    '<td>'+endDate+'</td>'+
                    '<td>'+exams[i].attempts+'</td>'+
                    '<td><select name="" class="js-grade-type"><option value="percentage" '+((exams[i].grade_type=='percentage')?'selected':'')+'>Percentage</option><option value="percentile" '+((exams[i].grade_type=='percentile')?'selected':'')+'>Percentile</option></select></td>'+
                    '<td>'+
                        '<div class="js-percentage '+((exams[i].grade_type=='percentage')?'':'hide')+'">'+
                            '<select class="btn-block">'+attemptsList+'</select>'+
                        '</div>'+
                        '<div class="js-percentile '+((exams[i].grade_type=='percentile')?'':'hide')+' text-center"><span class="badge">N.A.</span></div>'+
                    '</td>'+
                    '<td><input type="text" class="form-control input-sm js-weight" value="'+exams[i].weight+'" /></td>'+
                '</tr>';
        }
    }
    if (data.manual_exams) {
        var exams = data.manual_exams;
        for (var i = 0; i < exams.length; i++) {
            var section = exams[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            html+='<tr data-id="'+exams[i].id+'" data-type="manual-exam">'+
                    '<td>'+exams[i].name+'</td>'+
                    '<td>Manual Exam</td>'+
                    '<td>'+section+'</td>'+
                    '<td>NA</td>'+
                    '<td>NA</td>'+
                    '<td>NA</td>'+
                    '<td><select name="" class="js-grade-type"><option value="percentage" '+((exams[i].grade_type=='percentage')?'selected':'')+'>Percentage</option><option value="percentile" '+((exams[i].grade_type=='percentile')?'selected':'')+'>Percentile</option></select></td>'+
                    '<td>'+
                        '<div class="js-percentage '+((exams[i].grade_type=='percentage')?'':'hide')+' text-center"><span class="badge">N.A.</span></div>'+
                        '<div class="js-percentile '+((exams[i].grade_type=='percentile')?'':'hide')+' text-center"><span class="badge">N.A.</span></div>'+
                    '</td>'+
                    '<td><input type="text" class="form-control input-sm js-weight" value="'+exams[i].weight+'" /></td>'+
                '</tr>';
        }
    }
    $('#listExams').html(html);
}
$(document).ready(function() {
    $(".star-rating").rating({displayOnly: true});
    $('#listExams').on('change', '.js-grade-type', function(){
        if ($(this).val() == 'percentage') {
            $(this).closest('tr').find('.js-percentage').removeClass('hide');
            $(this).closest('tr').find('.js-percentile').addClass('hide');
        } else {
            $(this).closest('tr').find('.js-percentile').removeClass('hide');
            $(this).closest('tr').find('.js-percentage').addClass('hide');
        }
    });
    $('#btnSaveGrading').click(function(){
        var exams = $('#listExams').find('tr');
        var examsData = {};
        var totalWeight = 0;
        var avgCheck = true;
        var notes = CKEDITOR.instances['notes'].getData();
        exams.each(function(k,o){
            var examId = $(o).attr('data-id');
            var examType = $(o).attr('data-type');
            var gradeType = $(o).find('.js-grade-type').val();
            var avgAttempts = $(o).find('.js-percentage>select').val();
            var weight = $(o).find('.js-weight').val();
            examsData[k] = {
                'examId':examId,
                'examType':examType,
                'gradeType':gradeType,
                'avgAttempts':avgAttempts,
                'weight':weight
            }
            if (gradeType == 'percentage' && avgAttempts<1) {
                avgCheck = false;
            }
            totalWeight+= parseInt(weight);
        });
        if(!avgCheck) {
            toastr.error('All average of last attempts are compulsory!');
        } else if(totalWeight != 100) {
            toastr.error('Total weight of exams must be equal to 100!');
        } else {
            var req = {};
            var res;
            req.courseId = courseId;
            req.subjectId = subjectId;
            req.exams = examsData;
            req.notes = notes; 
            req.action ="save-subject-grading";
            $.ajax({
                'type'  : 'post',
                'url'   : ApiEndPoint,
                'data'  : JSON.stringify(req)
            }).done(function (res) {
                res =  $.parseJSON(res);
                if(res.status == 1)
                    toastr.success(res.message);
                else
                    toastr.error(res.message);
            });
        }
    });
});
</script>