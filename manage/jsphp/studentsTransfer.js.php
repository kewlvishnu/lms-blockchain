<script type="text/javascript">
var TableDatatablesManaged = function () {

    var initTable1 = function () {

        var table = $('#sample_1');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // setup buttons extension: http://datatables.net/extensions/buttons/
            buttons: [
                { extend: 'print', className: 'btn dark btn-outline' },
                { extend: 'pdf', className: 'btn green btn-outline' },
                { extend: 'csv', className: 'btn purple btn-outline' }
            ],

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 15, 20, -1],
                [10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "searchable": false,
                    "targets": [0]
                },
                {
                    "className": "dt-right", 
                    //"targets": [2]
                }
            ],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery('#sample_1_wrapper');

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).prop("checked", true);
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).prop("checked", false);
                    $(this).parents('tr').removeClass("active");
                }
            });
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
        }

    };

}();
var dataTransfer;
var students = [];
function get_institute_students() {
    var req = {};
    var res;
    req.action = "get-institute-temp-students";
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        dataTransfer = $.parseJSON(res);
        var html = "";
        students = dataTransfer.students;
        for (var i = 0; i < students.length; i++) {
            html+= '<tr data-id="'+i+'">'+
                        '<td>'+(i+1)+'</td>'+
                        '<td>'+students[i].username+'</td>'+
                        '<td>'+students[i].password+'</td>'+
                        '<td>'+students[i].courseNames+'</td>'+
                        '<td><button class="btn green btn-xs js-transfer">Transfer</button></td>'+
                    '</tr>';
        };
        $('#sample_1 tbody').html(html);
        $('#sample_1').DataTable().destroy();
        TableDatatablesManaged.init();
        
    });
}
$('#sample_1').on("click", ".js-transfer", function(){
    $('#modalTransferStudents').modal('show');
    $('#transferDropdown').html('');
    var index = $(this).closest('tr').attr("data-id");
    var html = "";
    var iCourses = dataTransfer.courses;
    var cCourses = students[index].courses;
    
    for (var i = 0; i < iCourses.length; i++) {
        for (var j = 0; j < cCourses.length; j++) {

            if(iCourses[i]["id"] == cCourses[j]["id"]) {
                $('#transferDropdown').attr("data-course", iCourses[i]['id']);
                iCourses[i]["used"] = 1;
                //delete iCourses[i];
            } else {
                iCourses[i]["used"] = 0;
            }
        };
    };
    
    if(Object.keys(iCourses).length>0) {

        $.each(iCourses, function( index, value ) {
            if (iCourses[index]["used"] == 0) {
                html+= '<option value="'+iCourses[index]['id']+'">'+iCourses[index]['name']+'</option>';
            }
        });
    };
    $('#transferDropdown').attr("data-student", students[index]["id"]).html(html);
});
$('#btnTransfer').on('click', function () {
    var req = {};
    var res;
    req.oldCourseId = $('#transferDropdown').attr("data-course");
    req.studentId = $('#transferDropdown').attr("data-student");
    req.courseId = $('#transferDropdown').val();
    req.action = "transfer-student-course";
    if (!req.courseId) {
        toastr.error("Please select a "+terminologies["course_single"].toLowerCase()+"!");
    } else {
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            window.location = window.location;
        });
    }
    return false;
});
</script>