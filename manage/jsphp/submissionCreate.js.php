<script type="text/javascript">
var examStart = new Date();
var examEnd = new Date('2099/12/31');
//function to get the students and chapters for the subject
function fetchSubjectStudentGroups() {
	var req = {};
	var res;
	req.courseId = courseId;
	req.subjectId = subjectId;
	req.action ="get-subject-student-groups";
	$.ajax({
		'type'  :   'post',
		'url'   :   ApiEndPoint,
		'data'  :   JSON.stringify(req)
	}).done(function(res) {
		data = $.parseJSON(res);
		//console.log(data);
		if(data.status == 0)
			toastr.error(data.message);
		else {
			//console.log(data);
			var html = "";
			if(data.studentGroups.length > 0) {
				$('.js-students').append('<div class="form-group"><select name="listStudents[]" id="listStudents" class="form-control list-students" multiple="multiple"></select></div>');
				for (var i = 0; i < data.studentGroups.length; i++) {
					$('.list-students').append('<optgroup label="'+data.studentGroups[i]['name']+'"></octgroup>');
					var students = data.studentGroups[i]['students'];
					if(students.length > 0) {
						for (var j = 0; j < students.length; j++)
						{
							var student_id = students[j].studentId;
							//var student_username = students[j].username;
							var name = students[j].firstName+' '+students[j].lastName;
							$('.list-students optgroup:last-child').append('<option value="'+data.studentGroups[i]['id']+'">' + name + '</option>');
						}
					}
				}
				$('#listStudents').multiSelect({
					selectableOptgroup: true,
					afterSelect: function(values){
						if (values.length == 1) {
							if ($("#switchFlag").val() == 0) {
								$("#switchFlag").val(1);
								toastr.error("You can select only groups");
								$('#listStudents').multiSelect('deselect', values);
							} else {
								$("#switchFlag").val(0);
							}
						}
					},
					afterDeselect: function(values){
						if (values.length == 1) {
							if ($("#switchFlag").val() == 0) {
								$("#switchFlag").val(1);
								toastr.error("You can deselect only groups");
								$('#listStudents').multiSelect('select', values);
							} else {
								$("#switchFlag").val(0);
							}
						}
					},
				});
			} else {
				$('.js-students').html('<p>No '+terminologies["student_single"].toLowerCase()+' groups yet!</p> <button class="btn blue" id="btnDivide">Divide '+terminologies["student_plural"].toLowerCase()+' among groups</button>');
			}
		}
	});
}
function fetchChapters() {
    var req = {};
    req.action = "get-chapters-for-exam";
    req.subjectId = subjectId;
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        fillChaptersSelect(res);
    });
}
function fillChaptersSelect(data) {
    if(data.status == 0) {
        toastr.error(data.message);
        return;
    }
    var opts = '';
    for(i=0; i<data.chapters.length; i++) {
        opts += '<option value="'+data.chapters[i]['id']+'">'+data.chapters[i]['name']+'</option>';
    }
    opts += '<option value="-1">Independent</option>';
    $('#chapterSelect').append(opts);
    if (typeof chapterId !== 'undefined') {
        $('#chapterSelect').val(xx);
    }
}
function fetchCourseDates() {
	var req = {};
	var res;
	req.action = 'get-course-date';
	req.courseId = courseId;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else {
			$('#courseStartDate').val(res.dates.liveDate);
			$('#courseEndDate').val(res.dates.endDate);
			
			//adding handlers for datetimepicker of start and end date
			var now = new Date();
			var start = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
			var courseStart = new Date(parseInt(res.dates.liveDate));
			if(start.valueOf() < courseStart.valueOf())
				examStart = new Date(courseStart.getTime());
			else
				examStart = new Date(start.getTime());
			
			var courseEnd = new Date(parseInt(res.dates.endDate));
			var end = new Date('2099/12/31');
			if(end.valueOf() < courseEnd.valueOf())
				examEnd = new Date(courseEnd.getTime());
			else
				examEnd = new Date(end.getTime());
			
			$('#startDate').datetimepicker({
				format: 'dd MM yyyy hh:ii',
				minDate: examStart,
				step: 30,
				autoclose: true,
				pickerPosition: "bottom-left",
				onSelectTime: function(date) {
					if (date.valueOf() > examEnd.valueOf()) {
						toastr.error('The start date can not be greater than the end date.');
					} else {
						examStart = date;
					}
				}
			});
			$('#endDate').datetimepicker({
				format: 'dd MM yyyy hh:ii',
				minDate: examStart,
				maxDate: examEnd,
				step: 30,
				autoclose: true,
				pickerPosition: "bottom-left",
				onSelectTime: function(date) {
					if (date.valueOf() < examStart.valueOf()) {
						toastr.error('The end date can not be less than the start date.');
					} else {
						examEnd = date;
					}
				}
			});
		}
	});
}
function nameAvailable() {
	if($('#titleName').val().length != '' ) {
		var req = {};
		var res;
		req.action = 'check-name-for-submission';
		req.name = $('#titleName').val();
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndPoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			//console.log(res);
			if(res.status == 0)
				toastr.error(res.message);
			else {
				if(!res.available)
					toastr.error('Please select a different name as you have already used this name.');
			}
		});
	}
}
$(document).ready(function(){
	$('.js-access').click(function(){
		/* Act on the event */
		if($(this).val() == 'private') {
			$('.js-private-block').removeClass('hide');
		} else {
			$('.js-private-block').addClass('hide');
		}
	});
	$('.js-students').on('click', '#btnDivide', function(event) {
		event.preventDefault();
		/* Act on the event */
        bootbox.prompt("Number of "+terminologies["student_plural"].toLowerCase()+" per group?", function(result) {
            if (result === null) {
                toastr.error("Please enter a number");
            } else if (!$.isNumeric(result)) {
                toastr.error("Please enter a number");
            } else if (result<2) {
                toastr.error("Atleast 2 "+terminologies["student_plural"].toLowerCase()+" per group are needed");
            } else {
				var req = {};
				var res;
				req.courseId = courseId;
				req.subjectId = subjectId;
				req.count = result;
				req.action ="gen-subject-student-groups";
				$.ajax({
					'type'  :   'post',
					'url'   :   ApiEndPoint,
					'data'  :   JSON.stringify(req)
				}).done(function(res) {
					data = $.parseJSON(res);
					//console.log(data);
					if(data.status == 0)
						toastr.error(data.message);
					else {
						//console.log(data);
						toastr.success("Successfully generated "+terminologies["student_single"].toLowerCase()+" groups!");
						fetchSubjectStudentGroups();
					}
				});
			}
		});
	});
	$('#titleName').on('blur', function() {
		if(min($(this), 3)) {
			if(!max($(this), 50))
				toastr.error("Please give a shorter exam name. Maximum allowed limit is 50 characters.");
		}
		else
			toastr.error("Please give a longer exam name. Minimum allowed limit is 3 characters.");
		nameAvailable();
	});

	$('#noOfAttempts').on('blur', function() {
		var noOfAttempts=$('#noOfAttempts').val().trim();
		if(noOfAttempts<1 || !($.isNumeric( noOfAttempts )) ){
			toastr.error("Number of attempts should be greater than 0");
		}
		
	});

	$('#formCreateExam').on('submit', function(){
		var error=0;
		var req = {};
		var title=$('#titleName').val().trim();
		var startDate = new Date($('#startDate input').val());
		var access = $('[name=optionAccess]:checked').val();
		var students=$('#listStudents').val();
        //var endDate= new Date($('#endDate input').val());
		if((title.length> 3 && title.length<50)){
			if($('#startDate input').val()!=''){
				var startDate = new Date($('#startDate input').val());
				if($('#endDate input').val()!='')	{
					var enddate=new Date($('#endDate input').val());
					if(startDate.getTime()>enddate.getTime())
					{
						error=1;
						toastr.error("End date should not be less than start date.");
					}
				}
				if (access == 'private' && students.length < 1) {
					error=1;
					toastr.error("You have to select at least 1 "+terminologies["student_single"].toLowerCase()+" group.");
				}
				if(parseInt($('#noOfAttempts').val())>0){
					// all main processing here
					if( error==0){
						req.title=	title;
						req.startDate=startDate.getTime();
						if($('#endDate input').val()!='')	{
							var endDate=new Date($('#endDate input').val());
							req.endDate=endDate.getTime();	
						}
						else
							req.endDate='';
						req.totalAttempts=$('#noOfAttempts').val().trim();
						req.chapterId=$('#chapterSelect').val();
						req.access = access;
						req.students=students;
						req.subjectId=subjectId;
						req.courseId=courseId;
						req.action = 'add-submission';
						//console.log(req);
						$.ajax({
							'type'  : 'post',
							'url'   : ApiEndPoint,
							'data' 	: JSON.stringify(req)
						}).done(function (res) {
							res =  $.parseJSON(res);
							//console.log(res);
							if(res.status == 0)
								toastr.error(res.message);
							else
							{
								//console.log(res.data);
								window.location = sitepathManageSubmissions+res.data.examId;
							}
						});
					}						
				}
			}else{
				error=1;
				toastr.error("Please specify start date.");
			}
		} else {
			error=1;
			toastr.error("Please give proper exam name.");
		}
		return false;
	});
});
</script>