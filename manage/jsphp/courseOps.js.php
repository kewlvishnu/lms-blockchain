<script type="text/javascript">
function fetchCourseReviews() {
    var req = {};
    var res;
    req.action = 'get-all-reviews-for-course';
    req.courseId = courseId;
    $.ajax({
        type: "post",
        url: ApiEndPoint,
        data: JSON.stringify(req) 
    }).done(function(res) {
        res = $.parseJSON(res);
        //console.log(res);
        $('#modalViewReviews .modal-title').text(terminologies["course_single"]+' Reviews');
        var html = '';
        var c1=c2=c3=c4=c5=0;
        var rateTotal = 0;
        $.each(res.reviews, function(i, review) {
            //counting reviews
            switch(review.rating) {
                case '1.00':
                    c1++;
                    break;
                case '2.00':
                    c2++;
                    break;
                case '3.00':
                    c3++;
                    break;
                case '4.00':
                    c4++;
                    break;
                case '5.00':
                    c5++;
                    break;
            }
            rateTotal += parseFloat(review.rating);
            var date = '';
            date = formatTime(review.time);
            
            html += '<li class="comment-item">'+
                        '<div class="row">'+
                            '<div class="col-sm-3">'+
                                '<div class="user-block">'+
                                    '<div class="user-avatar">'+
                                        '<img src="' + review.studentImage + '" class="img-circle" alt="">'+
                                    '</div>'+
                                    '<span class="user-name">' + review.studentName + '</span>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-sm-9">'+
                                '<div class="rating">'+
                                    '<span title="' + review.rating + '">'+
                                        '<input class="star-rating" type="number" class="rating" min="0" max="5" step="0.1" value="' + review.rating + '" data-size="xs">'+
                                    '</span>'+
                                    '<span class="comment-time"> ' + date + '</span>'+
                                    '<span class="subject"> ('+terminologies["subject_single"]+': ' + review.subjectName + ')</span>'+
                                '</div>'+
                                '<h4>' + review.title + '</h4>'+
                                '<div class="comment">'+
                                    '<p>' + review.review + '</p>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</li>';
        });
        $('#reviews ul.list-comments').append(html);
        var total = c1 + c2 + c3 + c4 + c5;
        $('.ratingTotal').text(total + ' Ratings');
        if(total == 0) {
            $('#reviews ul').html('<li class="comment-item"><div class="user-block"><span style="margin-left: 0px;">No Reviews Found</span></div></li>');
            avg = 0;
        }
        else
            avg = (rateTotal / total).toFixed(1);
        $('.avg-rate').text(avg);
        $('.avg-rating').attr('title',avg);
        $('.avg-rating input').val(avg);
        $(".star-rating").rating({displayOnly: true});
        $(".avg-rating input").rating({displayOnly: true});
        $('.progress1').css('width', percentage(c1, total));
        $('.progress2').css('width', percentage(c2, total));
        $('.progress3').css('width', percentage(c3, total));
        $('.progress4').css('width', percentage(c4, total));
        $('.progress5').css('width', percentage(c5, total));
        $('.count1').text(c1);
        $('.count2').text(c2);
        $('.count3').text(c3);
        $('.count4').text(c4);
        $('.count5').text(c5);
    });
}
function fetchCourseStudent_details() {
    var req = {};
    var res;
    req.course_id = courseId;
    req.action = "get_students_details_of_course";
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        var html = "";
        //console.log(data);
        if(data.students.length > 0) {
            $('#jsCourseStudents').attr('data-value',data.noOfStudents);
            $('#jsCourseStudents').counterUp({
                delay: 10,
                time: 2000
            });
        }
        if (page == "courseDetail") {
            fillCourseStudent_details(data);
        }
    });
}
function fillCourseStudent_details(data) {
    var html = "";
    //console.log(data);
    if(data.students.length > 0) {
        for (var i = 0; i < data.students.length; i++)
        {
            var student_id = data.students[i].id;
            var student_username = data.students[i].username;
            var name = data.students[i].name;
            var password = ((data.students[i].password == "")?'<small class="text-success">REGISTERED</small>':data.students[i].password);
            // var contactMobile = data.students[i].contactMobile;
            // var course_key = data.students[i].key_id;
            var email = data.students[i].email;
            var date = data.students[i].date;
            var checkbox = ((!data.students[i].parentid)?'<input type="checkbox" class="js-option-student" />':'<input type="checkbox" class="js-option-student" />');
            var parent = ((!data.students[i].parentid)?'<td></td>':'<td><strong>'+data.students[i].userid+'</strong> / <strong>'+data.students[i].passwd+'</strong></td>');
            /*html += '<tr data-student="'+student_id+'" data-temp="'+data.students[i].temp+'"><td>' + checkbox + '</td><td>' + student_id + '</td><td>' + student_username + '</td><td>' + password + '</td><td>' + name + '</td><td class="small-txt">' + email + '</td>' + parent + '</tr>';*/

            html+= '<tr class="odd gradeX" data-student="'+student_id+'" data-temp="'+data.students[i].temp+'">'+
                        '<td>'+
                            '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">'+
                                '<input type="checkbox" class="checkboxes js-option-student" value="1" />'+
                                '<span></span>'+
                            '</label>'+
                        '</td>'+
                        '<td>' + student_id + '</td>'+
                        '<td>' + student_username + '</td>'+
                        '<td>' + password + '</td>'+
                        '<td>' + name + '</td>'+
                        '<td><a href="mailto:' + email + '">' + email + '</a></td>'+
                        '<td>'+data.students[i].userid+'</td>'+
                        '<td>'+data.students[i].passwd+'</td>'+
                    '</tr>';
        }
        $('#tblCourseStudents tbody').append(html);
        TableDatatablesManaged.init();
    } else {
        html+= '<tr class="odd gradeX">'+
                    '<td colspan="8">No '+terminologies["course_single"].toLowerCase()+' '+terminologies["student_plural"].toLowerCase()+' yet!</td>'+
                '</tr>';
        $('#tblCourseStudents tbody').append(html);
    }
}
function get_keys_remaining() {
    var req = {};
    req.action = 'keys_avilable';
    $.ajax({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req)
    }).done(function (res) {
        data = $.parseJSON(res);
        //console.log(data);
        if (data.status == 1) {
            keys_available = data.keys;
            keys_pending = data.invitation;
            $('#jsRemaining').attr('data-value',keys_available);
            $('#jsPending').attr('data-value',keys_pending);
            $('#jsRemaining,#jsPending').counterUp({
                delay: 10,
                time: 2000
            });
            $('.js-pending-keys').html(keys_pending);
            $('.js-available-keys').html(keys_available);
        }
        else {
            toastr.error(res.msg);
        }
    });
}
$(document).ready(function(){
    fetchCourseReviews();
});
</script>