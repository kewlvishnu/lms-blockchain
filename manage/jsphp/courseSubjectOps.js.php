<script type="text/javascript">
$(document).ready(function(){
    var reqCourseTag = {};
    reqCourseTag.action = 'course-tags';
    var mstags = $('#tagssuggest').magicSuggest({
        valueField: 'name',
        data: TypeEndPoint,
        dataUrlParams: reqCourseTag
    });
    $(mstags).on('keydown', keydownAlphaNumeric);
    $('body').on('click','[data-toggle="modal"].btn-course', function(){
        var action = $(this).attr("data-action");
        if (action == "new") {
            $('.js-course-action').html("New");
            $('#inputCourseStartDate').prop("disabled", false);
        } else {
            $('.js-course-action').html("Edit");
            $('#inputCourseStartDate').prop("disabled", true);
        }
        //console.log(getToday());
        $('.date-picker').datepicker({
            format: 'dd M yyyy',
            startDate: getToday(),
            orientation: "left",
            autoclose: true
        });
        $('#modalAddEditCourse').modal("show");
    });
    $('body').on('click','[data-toggle="modal"].btn-subject', function(){
        var action = $(this).attr("data-action");
        if (action == "new") {
            $('.js-subject-action').html("New");
        } else {
            $('.js-subject-action').html("Edit");
        }
        $(this).closest('.js-course-list').addClass('active');
        $('#modalAddEditSubject').modal("show");
    });
    $('#modalAddEditCourse').on('hidden.bs.modal', function () {
        $('#formAddEditCourse')[0].reset();
        if(page == "courses") {
            $('.js-course-list').removeClass('active');
        }
    });
    $('#modalAddEditSubject').on('hide.bs.modal', function () {
        if(page == "courses") {
            $('.js-course-list').removeClass('active');
        } else {
            $('#jsSubjects tr').removeClass('active');
        }
    });
    $('#inputCourseStartDate, #inputCourseEndDate').on('keypress', function() {
        return false;
    });
    $("#btnAddEditCourse").on("click",function(){
        if (page == "courses") {
            var courseId = $('.js-course-list.active').attr('data-course');
        } else {
            var courseId = window.courseId;
        }
        handleValidation1(courseId);
        $('#formAddEditCourse').submit();
    });
    $("#btnAddEditSubject").click(function(){
        if (page == "courses") {
            var courseId = $('.js-course-list.active').attr('data-course');
            var subjectId = $('.js-course-list.active').attr('data-subject');
        } else if(page == "courseDetail") {
            var courseId = window.courseId;
            var subjectId = $('#jsSubjects tr.active').attr('data-subject');
        } else {
            var courseId = window.courseId;
            var subjectId = window.subjectId;
        }
        handleValidation2(courseId,subjectId);
        $('#formAddEditSubject').submit();
    });
    $("#optionCourseEndDate").on("click", function() {
        if (this.checked) {
            $('.js-course-end-date').removeClass('hide');
            $('#inputCourseEndDate').rules('add','required');
        } else {
            $('.js-course-end-date').addClass('hide');
            $('#inputCourseEndDate').rules('remove');
        }
    });
    var handleValidation1 = function(courseId) {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form1 = $('#formAddEditCourse');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                inputCourseName: {
                    minlength: 2,
                    required: true
                },
                inputCourseSubtitle: {
                    minlength: 2,
                    required: true
                },
                inputCourseDescription: {
                    minlength: 2,
                    maxlength: 1000,
                    required: true
                },
                inputTargetAudience: {
                    minlength: 2,
                    maxlength: 200
                },
                /*inputTags: {
                    minlength: 2,
                    required: true
                },*/
                inputCourseStartDate: {
                    required: true
                },
                /*inputCourseEndDate: {
                    required: $('#optionCourseEndDate').is(':checked')
                },*/
                selectCourseCategory: {
                    required: true
                }
            },

            messages: { // custom messages for radio buttons and checkboxes
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) { 
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) { 
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) { 
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) { 
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   
                //success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            highlight: function (element) { // hightlight error inputs
               $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                //success1.show();
                error1.hide();
                //form[0].submit(); // submit the form
                //console.log(courseId);
                createCourse(courseId);
                //toastr.success("Hurray!");
            }

        });
    }
    var handleValidation2 = function(courseId,subjectId) {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form2 = $('#formAddEditSubject');
        var error2 = $('.alert-danger', form2);
        var success2 = $('.alert-success', form2);

        form2.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                inputSubjectName: {
                    minlength: 2,
                    required: true
                },
                inputSubjectDescription: {
                    minlength: 2,
                    required: true
                }
            },

            messages: { // custom messages for radio buttons and checkboxes
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) { 
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) { 
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) { 
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) { 
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   
                success2.hide();
                error2.show();
                App.scrollTo(error2, -200);
            },

            highlight: function (element) { // hightlight error inputs
               $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();
                //form[0].submit(); // submit the form
                createSubject(courseId,subjectId);
                //toastr.success("Hurray!");
            }

        });
    }
    var createCourse = function(courseId) {
        if ($('#optionCourseEndDate').is(':checked') && !$('#inputCourseEndDate').val()) {
            toastr.error('Please select end date');
        } else {
            var req = {};
            req.courseName = $('#inputCourseName').val();
            req.courseSubtitle = $('#inputCourseSubtitle').val();
            req.courseDesc = $('#inputCourseDescription').val();
            startDate = new Date($('#inputCourseStartDate').val());
            req.liveDate = startDate.getTime();
            req.setEndDate = 0;
            if ($('#optionCourseEndDate').is(':checked')) {
                req.setEndDate = 1;
            }
            endDate = new Date($('#inputCourseEndDate').val());
            req.endDate = endDate.getTime();
            req.courseCateg = $('#selectCourseCategory').val();
            req.targetAudience = '';
            req.tags = mstags.getValue();
            if($('#target-audience').val() != '')
            req.targetAudience = $('#inputTargetAudience').val();
            if(req.courseCateg == null)
                        req.courseCateg = [];
            req.portfolioSlug = subdomain;
            req.pageUserId = $('#inputUserId').val();
            req.pageUserRole = $('#inputUserRole').val();
            req.socialImage = $('#jsSocialImage').attr('src');
            if (courseId != undefined) {
                req.courseId = courseId;
                req.action = 'update-course';
            } else {
                req.action = 'create-course';
            }

            $.ajax({
                'type'  : 'post',
                'url'   : ApiEndPoint,
                'data'  : JSON.stringify(req)
            }).done(function (res) {
                res =  $.parseJSON(res);
                if(res.status == 1) {
                    toastr.success(res.message);
                    if (req.action == 'create-course') {
                        window.location = window.location;
                    } else {
                        $('#modalAddEditCourse').modal("hide");
                    }
                    //window.location = window.location;
                } else {
                    toastr.error(res.message);
                }
            });
        }
    };
    var createSubject = function(courseId,subjectId) {
        var req = {};
        req.courseId = courseId;
        req.subjectName = $('#inputSubjectName').val();
        req.subjectDesc = $('#inputSubjectDescription').val();
        //req.action = 'create-subject';
        if (subjectId) {
            req.subjectId = subjectId;
            req.action = 'update-subject';
        } else {
            req.action = 'create-subject';
        }

        $.ajax({
            'type'  : 'post',
            'url'   : ApiEndPoint,
            'data'  : JSON.stringify(req)
        }).done( function (res) {
            res =  $.parseJSON(res);
            if(res.status == 1) {
                toastr.success(res.message);
                window.location = window.location;
            }
            else {
                toastr.error(res.message);
            }
        });
    };
    $('.edit-course').on('click', function(e) {
        e.preventDefault();
        var req = {};
        var res;
        req.courseId = courseId;

        req.action = "get-course-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillCourseDetails(res);
            //details=res;
        });
    });
    function fillCourseDetails(data) {
        $('.js-course-action').html("Edit");
        $('#inputCourseStartDate').prop("disabled", true);
        $('.date-picker').datepicker({
            format: 'dd M yyyy',
            startDate: getToday(),
            orientation: "left",
            autoclose: true
        });
        // General Details
        if (data.courseDetails.deleted == 1)
            toastr.error('This '+terminologies["course_single"].toLowerCase()+' has been deleted!');
        var catIds = [], catNames = [];
        var catName=[];
        $('#inputCourseName').val(data.courseDetails.name);

        if (data.courseDetails.subtitle != '') {
            $('#inputCourseSubtitle').val(data.courseDetails.subtitle);
        }

        if (data.courseDetails.socialImage != '') {
            $('#jsSocialImage').attr('src',data.courseDetails.socialImage);
        }

        if (data.courseDetails.targetAudience != '') {
            $('#inputTargetAudience').val(data.courseDetails.targetAudience);
        }
        //$('#inputTags').val(data.courseDetails.tags);
        mstags.setSelection(data.tags);
        if (data.courseDetails.description != '') {
            $('#inputCourseDescription').val(data.courseDetails.description);
        }
        startDate = formatDate(parseInt(data.courseDetails.liveDate));
        $('#inputCourseStartDate').val(startDate);

        if (data.courseDetails.endDate != '') {
            endDate = formatDate(parseInt(data.courseDetails.endDate));
            $('#inputCourseEndDate').val(endDate);
            $('#optionCourseEndDate').val(1);
            $('.js-course-end-date').removeClass('hide');
        } else {
            $('#optionCourseEndDate').prop('checked', false);
            $('.js-course-end-date').addClass('hide');
        }
 
        $.each(data.courseCategories, function (k, cat) {
            //catNames.push(cat.catName);
            catIds.push(cat.categoryId);
        });
        $('#selectCourseCategory').val(catIds);
        $('#modalAddEditCourse').modal("show");
    }
    $(".btn-social-upload").click(function(event) {
        /* Act on the event */
        $("#fileSocialImage").click();
    });
    $("#fileSocialImage").change(function(event) {
        /* Act on the event */
        $("#formSocialImage").trigger('submit');
    });
    $("#formSocialImage").submit(function(event) {
        var formData = new FormData(this);

        $.ajax({
            url: FileApiPoint,
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function(res) {
            if (res.status == 1) {
                $("#jsSocialImage").attr('src', res.image);
            } else {
                toastr.error(res.message);
            }
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
        
        /*var canvas = $('#fileSocialImage').get(0);
        var dataUrl = canvas.toDataURL("image/jpeg");

        var blob = dataURItoBlob(dataUrl);

        var formData = new FormData();
        formData.append("file", formData);

        console.log(formData);*/
        /*var request = new XMLHttpRequest();
        request.onload = completeRequest;

        request.open("POST", "IdentifyFood");
        request.send(formData);*/
        return false;
    });
});
</script>