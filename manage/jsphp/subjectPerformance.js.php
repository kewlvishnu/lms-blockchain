<script type="text/javascript">
function fetchSubjectStudents() {
    var req = {};
    var res;
    req.courseId = courseId;
    req.subjectId = subjectId;
    req.type = 'registered';
    req.action ="get-subject-students";
    $.ajax({
        'type'  :   'post',
        'url'   :   ApiEndPoint,
        'data'  :   JSON.stringify(req)
    }).done(function(res) {
        data = $.parseJSON(res);
        console.log(data);
        if(data.status == 0)
            toastr.error(data.message);
        else {
            //console.log(data);
            var html = "";
            if(data.students.length > 0) {
                for (var i = 0; i < data.students.length; i++)
                {
                    var student_id = data.students[i].id;
                    var student_username = data.students[i].username;
                    var name = data.students[i].name;
                    var password = ((data.students[i].password == "")?'<small class="text-success">REGISTERED</small>':data.students[i].password);
                    // var contactMobile = data.students[i].contactMobile;
                    // var course_key = data.students[i].key_id;
                    var email = data.students[i].email;
                    var date = data.students[i].date;
                    var checkbox = ((!data.students[i].parentid)?'<input type="checkbox" class="js-option-student" />':'');
                    var parent = ((!data.students[i].parentid)?'<td></td>':'<td><strong>'+data.students[i].userid+'</strong> / <strong>'+data.students[i].passwd+'</strong></td>');
                    html+= '<tr class="odd gradeX" data-student="'+student_id+'" data-temp="'+data.students[i].temp+'">'+
                                '<td>' + student_id + '</td>'+
                                '<td>' + student_username + '</td>'+
                                '<td>' + name + '</td>'+
                                '<td><a href="mailto:' + email + '">' + email + '</a></td>'+
                                '<td>'+
                                    '<a href="'+sitepathManageSubjects+subjectId+'/performance/'+student_id+'" class="btn btn-circle btn-sm green"><i class="fa fa-line-chart"></i> Performance</a>'+
                                '</td>'+
                            '</tr>';
                }
            } else {
                html+= '<tr class="odd gradeX">'+
                            '<td colspan="7">No '+terminologies["subject_single"].toLowerCase()+' students yet!</td>'+
                        '</tr>';
            }
            $('#tblSubjectStudents tbody').append(html);
            //TableDatatablesManaged.init();
        }
    });
}
$(document).ready(function() {
});
</script>