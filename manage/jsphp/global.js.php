<script type="text/javascript">
var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
var sitePath	= '<?php echo $sitepath; ?>';
var sitePathCourses	= '<?php echo $sitepathCourses; ?>';
var sitePath404 = '<?php echo $sitepath404; ?>';
var sitepathManage = '<?php echo $sitepathManage; ?>';
var sitepathManageOld = '<?php echo $sitepathManageOld; ?>';
var sitepathManageIncludes = '<?php echo $sitepathManageIncludes; ?>';
var page = '<?php echo $page; ?>';
<?php if (isset($subPage) && !empty(isset($subPage))) { ?>
var subPage = '<?php echo $subPage; ?>';
<?php }; ?>
var ApiEndPoint = sitePath+'api/index.php';
var currency = 2;
var slug = '<?php echo ((isset($slug))?($slug):('')); ?>';
var subdomain = '<?php echo ((isset($subdomain))?($subdomain):('')); ?>';
var courses = [];
var subCourses = [];
var notifications;
var month = new Array();
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";
var themeColors = ["white","default","dark","blue","blue-madison","blue-chambray","blue-ebonyclay","blue-hoki","blue-steel","blue-soft","blue-dark","blue-sharp","blue-oleo","green","green-meadow","green-seagreen","green-turquoise","green-haze","green-jungle","green-soft","green-dark","green-sharp","green-steel","grey","grey-steel","grey-cararra","grey-gallery","grey-cascade","grey-silver","grey-salsa","grey-salt","grey-mint","red","red-pink","red-sunglo","red-intense","red-thunderbird","red-flamingo","red-soft","red-haze","red-mint","yellow","yellow-gold","yellow-casablanca","yellow-crusta","yellow-lemon","yellow-saffron","yellow-soft","yellow-haze","yellow-mint","purple","purple-plum","purple-medium","purple-studio","purple-wisteria","purple-seance","purple-intense","purple-sharp","purple-soft"];
var darkThemeColors = ["dark","blue","blue-madison","blue-chambray","blue-ebonyclay","blue-hoki","blue-steel","blue-soft","blue-dark","blue-sharp","blue-oleo","green","green-meadow","green-seagreen","green-turquoise","green-haze","green-jungle","green-soft","green-dark","green-sharp","green-steel","red","red-pink","red-sunglo","red-intense","red-thunderbird","red-flamingo","red-soft","red-haze","red-mint","yellow","yellow-gold","yellow-casablanca","yellow-crusta","yellow-lemon","yellow-saffron","yellow-soft","yellow-haze","yellow-mint","purple","purple-plum","purple-medium","purple-studio","purple-wisteria","purple-seance","purple-intense","purple-sharp","purple-soft"];
var bootstrapColors = ["info", "warning", "danger", "success"]
toastr.options = {
	"closeButton": true,
	"debug": false,
	"positionClass": "toast-top-center",
	"onclick": null,
	"showDuration": "1000",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "1000",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
}
var summerShort = {
					height: 300,
					toolbar: [
						['style', ['bold', 'italic', 'underline', 'clear']]
					]
				};
<?php if (isset($courseId) && !empty($courseId)) { ?>
var courseId = "<?php echo $courseId; ?>";
<?php }; ?>
<?php if (isset($subjectId) && !empty($subjectId)) { ?>
var subjectId = "<?php echo $subjectId; ?>";
<?php }; ?>
<?php if (isset($examId) && !empty($examId)) { ?>
var examId = "<?php echo $examId; ?>";
<?php }; ?>
<?php if (isset($attemptId)) { ?>
var attemptId = "<?php echo $attemptId; ?>";
<?php }; ?>
<?php if (isset($chapterId)) { ?>
var chapterId = "<?php echo $chapterId; ?>";
<?php }; ?>
<?php if (isset($contentId)) { ?>
var contentId = "<?php echo $contentId; ?>";
<?php }; ?>
$(document).ready(function(){

	fetchProfileDetails();

	$('[data-scroll]').click(function(e){
		e.preventDefault();
		var scrollDiv = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(scrollDiv).offset().top - 100
		}, 1000);
	});
	if (page == 'content') {
		fetchContentStuff();
	}
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndPoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}

	//function to fill user details
	function fillProfileDetails(data) {
		//console.log(data);
		if (data.profileDetails) {
			if (data.profileDetails.profilePic) {
				$('.dropdown-user .img-circle').attr("src",data.profileDetails.profilePic);
			}
		};
		if (!data.profileDetails.name) {
			$('.dropdown-user .username').text(data.profileDetails.username);
		} else {
			$('.dropdown-user .username').text(data.profileDetails.name);
		}
		$('.dropdown-user').removeClass('invisible');
	}

});
</script>