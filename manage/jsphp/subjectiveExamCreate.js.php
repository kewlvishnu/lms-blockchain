<script type="text/javascript">
var examStart = new Date();
var examEnd = new Date('2099/12/31');
function fetchChapters() {
    var req = {};
    req.action = "get-chapters-for-exam";
    req.subjectId = subjectId;
    $.ajax({
        'type'  : 'post',
        'url'   : ApiEndPoint,
        'data'  : JSON.stringify(req)
    }).done(function (res) {
        res =  $.parseJSON(res);
        fillChaptersSelect(res);
    });
}
function fillChaptersSelect(data) {
    if(data.status == 0) {
        toastr.error(data.message);
        return;
    }
    var opts = '';
    for(i=0; i<data.chapters.length; i++) {
        opts += '<option value="'+data.chapters[i]['id']+'">'+data.chapters[i]['name']+'</option>';
    }
    opts += '<option value="-1">Independent</option>';
    $('#chapterSelect').append(opts);
    if (typeof chapterId !== 'undefined') {
        $('#chapterSelect').val(xx);
    }
}
function fetchCourseDates() {
	var req = {};
	var res;
	req.action = 'get-course-date';
	req.courseId = courseId;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else {
			$('#courseStartDate').val(res.dates.liveDate);
			$('#courseEndDate').val(res.dates.endDate);
			
			//adding handlers for datetimepicker of start and end date
			var now = new Date();
			var start = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
			var courseStart = new Date(parseInt(res.dates.liveDate));
			if(start.valueOf() < courseStart.valueOf())
				examStart = new Date(courseStart.getTime());
			else
				examStart = new Date(start.getTime());
			
			var courseEnd = new Date(parseInt(res.dates.endDate));
			var end = new Date('2099/12/31');
			if(end.valueOf() < courseEnd.valueOf())
				examEnd = new Date(courseEnd.getTime());
			else
				examEnd = new Date(end.getTime());
			
			$('#startDate').datetimepicker({
				format: 'dd MM yyyy hh:ii',
				minDate: examStart,
				step: 30,
				autoclose: true,
				pickerPosition: "bottom-left",
				onSelectTime: function(date) {
					if (date.valueOf() > examEnd.valueOf()) {
						toastr.error('The start date can not be greater than the end date.');
					} else {
						examStart = date;
					}
				}
			});
			$('#endDate').datetimepicker({
				format: 'dd MM yyyy hh:ii',
				minDate: examStart,
				maxDate: examEnd,
				step: 30,
				autoclose: true,
				pickerPosition: "bottom-left",
				onSelectTime: function(date) {
					if (date.valueOf() < examStart.valueOf()) {
						toastr.error('The end date can not be less than the start date.');
					} else {
						examEnd = date;
					}
				}
			});
		}
	});
}
function nameAvailable() {
	if($('#titleName').val().length != '' ) {
		var req = {};
		var res;
		req.action = 'check-name-for-subjectiveexam';
		req.name = $('#titleName').val();
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndPoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			console.log(res);
			if(res.status == 0)
				toastr.error(res.message);
			else {
				if(!res.available)
					toastr.error('Please select a different name as you have already used this name.');
			}
		});
	}
}
$(document).ready(function(){
	$('#titleName').on('blur', function() {
		if(min($(this), 3)) {
			if(!max($(this), 50))
				toastr.error("Please give a shorter exam name. Maximum allowed limit is 50 characters.");
		}
		else
			toastr.error("Please give a longer exam name. Minimum allowed limit is 3 characters.");
		nameAvailable();
	});

	$('#mnsTest').on('blur', function() {
		var mnsTest=$('#mnsTest').val().trim();
		if(mnsTest<0 || mnsTest>59 || !($.isNumeric( mnsTest ))){
			toastr.error("Please give a proper time in between 0 to 59 minutes");
		}
		
	});

	$('#hrsTest').on('blur', function() {
		var hrsTest=$('#hrsTest').val().trim();
		if(hrsTest<0  || !($.isNumeric( hrsTest ))){
			toastr.error("Please give a proper time greater than 0 hrs");
		}
		
	});
	
	$('#noOfAttempts').on('blur', function() {
		var noOfAttempts=$('#noOfAttempts').val().trim();
		if(noOfAttempts<1 || !($.isNumeric( noOfAttempts )) ){
			toastr.error("Number of attempts should be greater than 0");
		}
		
	});

	$('#formCreateExam').on('submit', function(){
		var error=0;
		var req = {};
		var title=$('#titleName').val().trim();
		var startDate = new Date($('#startDate input').val());
        //var endDate= new Date($('#endDate input').val());
		if((title.length> 3 && title.length<50)){
			if($('#startDate input').val()!=''){
				var startDate = new Date($('#startDate input').val());
				if($('#endDate input').val()!='')	{
					var enddate=new Date($('#endDate input').val());
					if(startDate.getTime()>enddate.getTime())
					{	 error=1;
						toastr.error("End date should not be less than start date.");
					}
				}
				var hrsTest = parseInt($('#hrsTest').val());
				if(hrsTest>-1){
					var mnsTest = parseInt($('#mnsTest').val());
					if(mnsTest>-1 && mnsTest<60) {
						if ((hrsTest+mnsTest)>0) {
							if(parseInt($('#noOfAttempts').val())>0){
								if($('#switchyes').prop('checked') || $('#switchno').prop('checked')){
									if($('.js-answer-type:checked').length>0){
										// all main processing here
										if( error==0){
										totalhrs=0;
										req.title=	title;
										req.startDate=startDate.getTime();
										if($('#endDate input').val()!='')	{
											var endDate=new Date($('#endDate input').val());
											req.endDate=endDate.getTime();	
										}
										else								
											req.endDate='';
										var totalhrs=$('#hrsTest').val().trim()*60;
										var mins=$('#mnsTest').val().trim();
										req.totalTime=parseInt(totalhrs)+parseInt(mins);
										req.totalAttempts=$('#noOfAttempts').val().trim();
										//req.totalQuestions=$('#noOfQues').val().trim();
										//req.totalRequiredQuestions=$('#noOfRequiredQues').val().trim();
										//noOfRequiredQues
										if($('#switchyes').prop('checked'))
										{
											req.shuffle='yes';	
										}else{
											req.shuffle='no';
										}
										if($('#optionText').is(":checked")) {
											req.submission=1;
										}
										else if($('#optionFile').is(":checked")){
											req.submission=2;
										}else{
											req.submission=3;
										}
										req.chapterId=$('#chapterSelect').val();
										chapterId=$('#chapterSelect').val();
										req.subjectId=subjectId;
										req.courseId=courseId;
										req.action = 'add-subejctive-exam';
										//console.log(req);
										$.ajax({
											'type'  : 'post',
											'url'   : ApiEndPoint,
											'data' 	: JSON.stringify(req)
										}).done(function (res) {
											res =  $.parseJSON(res);
											//console.log(res);
											if(res.status == 0)
												toastr.error(res.message);
											else
											{
												window.location = sitepathManageSubjectiveExams+res.data.examId;
											}
										});
										}
									}else{
										error=1;
										toastr.error("Please check atleast one choice");
									}
								}else{
									error=1;
									toastr.error("Please check one choice.");
								}							
							}
							else{
								error=1;
								toastr.error("Number of attempts should be greater than 0.");
							}
						}
						else {
							error=1;
							toastr.error("Please give proper test timings.");	
						}
					}
					else{
						error=1;
						toastr.error("Please give a proper time in between 0 to 59 minutes.");	
					}
				}else{
					error=1;
					toastr.error("Please give a proper time greater than 0 hrs.");				}
			}else{
				error=1;
				toastr.error("Please specify start date.");
			}
		} else {
			error=1;
			toastr.error("Please give proper exam name.");
		}
		return false;
	});
});
</script>