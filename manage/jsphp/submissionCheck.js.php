<script type="text/javascript">
//to add BCT dynamically
function fetchSubmissionAttempts() {
	var req = {};
	var res;
	req.action = 'get-check-submission-students';
	req.examId = examId;
	req.subjectId = subjectId;
	req.courseId = courseId;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else {
			//console.log(res);
			fullSubmissionAttempts(res);
		}
	});
}
function fullSubmissionAttempts(data) {
	if (data.access == "public") {
		var students = data.students;
		if (students.length>0) {
			$('#studentSubmissions').html('');
			for (var i = 0; i < students.length; i++) {
				$('#studentSubmissions').append(
					'<div class="panel panel-primary">'+
						'<div class="panel-heading" role="tab" id="heading'+students[i].userId+'">'+
							'<h4 class="panel-title">'+
								'<a class="'+((i==0)?'':'collapsed')+'" role="button" data-toggle="collapse" data-parent="#" href="#collapse'+students[i].userId+'" +aria-expanded="'+((i==0)?'true':'false')+'" aria-controls="collapse'+students[i].userId+'">'+
									students[i].name+
								'</a>'+
							'</h4>'+
						'</div>'+
						'<div id="collapse'+students[i].userId+'" class="panel-collapse collapse '+((i==0)?'in':'')+'" role="tabpanel" aria-labelledby="heading'+students[i].userId+'">'+
							'<table class="table">'+
								'<tbody>'+
									'<tr>'+
										'<th>Attempt</th>'+
										'<th>Start Date</th>'+
										'<th>End Date</th>'+
										'<th>Checked</th>'+
									'</tr>'+
								'</tbody>'+
							'</table>'+
						'</div>'+
					'</div>');
					for (var j = 0; j < students[i]["attempts"].length; j++) {
						//$('#studentSubmissions .panel-default:last-child .list-group').append('<li class="list-group-item"><a href="checkSubmission.php?attemptId='+students[i]["attempts"][j].attemptId+'">Attempt '+(j+1)+' ['+formatTime(students[i]["attempts"][j].startDate)+' - '+formatTime(students[i]["attempts"][j].endDate)+']</a></li>');
						$('#studentSubmissions .panel:last-child .table tbody').append(
							'<tr>'+
								'<td>'+
									'<a href="'+sitepathManageSubmissions+examId+'/check/'+students[i]["attempts"][j].attemptId+'">Attempt '+(j+1)+'</a>'+
								'</td>'+
								'<td>'+formatTime(students[i]["attempts"][j].startDate)+'</td>'+
								'<td>'+((students[i]["attempts"][j].endDate==0)?'-':formatTime(students[i]["attempts"][j].endDate))+'</td>'+
								'<td><span class="badge">'+((students[i]["attempts"][j].checked==1)?'Yes':'No')+'</span></td>'+
							'</tr>');
					};
			};
		} else {
			$('#studentSubmissions').html('<div class="note note-info">No '+terminologies["student_single"].toLowerCase()+' attempts found</div>');
		}
	} else {
		var groups = data.groups;
		if (groups.length>0) {
			$('#studentSubmissions').html('');
			for (var i = 0; i < groups.length; i++) {
				$('#studentSubmissions').append(
					'<div class="panel panel-primary">'+
						'<div class="panel-heading" role="tab" id="headingGroup'+groups[i].id+'">'+
							'<h4 class="panel-title">'+
								'<a class="'+((i==0)?'':'collapsed')+'" role="button" data-toggle="collapse" data-parent="#" href="#collapseGroup'+groups[i].id+'" +aria-expanded="'+((i==0)?'true':'false')+'" aria-controls="collapseGroup'+groups[i].id+'">'+
									groups[i].name+
								'</a>'+
							'</h4>'+
						'</div>'+
						'<div id="collapseGroup'+groups[i].id+'" class="panel-collapse collapse '+((i==0)?'in':'')+'" role="tabpanel" aria-labelledby="headingGroup'+groups[i].id+'">'+
							'<table class="table">'+
								'<tbody>'+
									'<tr>'+
										'<th>Attempt</th>'+
										'<th>Start Date</th>'+
										'<th>End Date</th>'+
										'<th>Checked</th>'+
									'</tr>'+
								'</tbody>'+
							'</table>'+
						'</div>'+
					'</div>');
					for (var j = 0; j < groups[i]["attempts"].length; j++) {
						//$('#studentSubmissions .panel-default:last-child .list-group').append('<li class="list-group-item"><a href="checkSubmission.php?attemptId='+groups[i]["attempts"][j].attemptId+'">Attempt '+(j+1)+' ['+formatTime(groups[i]["attempts"][j].startDate)+' - '+formatTime(groups[i]["attempts"][j].endDate)+']</a></li>');
						$('#studentSubmissions .panel:last-child .table tbody').append(
							'<tr>'+
								'<td>'+
									'<a href="'+sitepathManageSubmissions+examId+'/check/'+groups[i]["attempts"][j].attemptId+'">Attempt '+(j+1)+'</a>'+
								'</td>'+
								'<td>'+formatTime(groups[i]["attempts"][j].startDate)+'</td>'+
								'<td>'+((groups[i]["attempts"][j].endDate==0)?'-':formatTime(groups[i]["attempts"][j].endDate))+'</td>'+
								'<td><span class="badge">'+((groups[i]["attempts"][j].checked==1)?'Yes':'No')+'</span></td>'+
							'</tr>');
					};
			};
		} else {
			$('#studentSubmissions').html('<div class="note note-info">No group attempts found</div>');
		}
	}
}
</script>