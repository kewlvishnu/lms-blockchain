<script type="text/javascript">
var filler_00 = [];
var filler_25 = [];
var filler_50 = [];
var filler_75 = [];
var filler_76 = [];
var cats = [];
var contentId=[];
var students=[];
var contentProgress=[];
var contentTimeSpent=[];
var initdt=0;
var contentName=[];
var tblData;
//function to set the courses progress byt students
function fetchCourseStudentAnalytics() {
    var req = {};
    var res;
    req.courseId = courseId;
    req.subjectId = subjectId;
    if(req.courseId == "" || req.courseId == null) {
        window.location.href=sitepathManage;
        return;
    }
    if(req.subjectId == "" || req.subjectId == null) {
        window.location.href=sitepathManage;
        return;
    }
    req.action ="get-student-progress-details";
    $.ajax({
        'type'  :   'post',
        'url'   :   ApiEndPoint,
        'data'  :   JSON.stringify(req)
    }).done(function(res) {
        res = $.parseJSON(res);
        //console.log(res);
        //console.log('Hello');
        if(res.status == 0){
            toastr.error(res.message);
            window.location.href =sitepathManageSubjects+subjectId;
        } else {
            students=res.studentEnrolled;
            contentProgress=res.content;
            fetchCourseAnalytics();
        }
    });
}
//function to get number bar for course progress
function fetchCourseAnalytics() {
    var req = {};
    var res;
    req.courseId = courseId;
    req.subjectId = subjectId;
    if(req.courseId == "" || req.courseId == null) {
        window.location.href=sitepathManage;
        return;
    }
    if(req.subjectId == "" || req.subjectId == null) {
        window.location.href=sitepathManage;
        return;
    }
    req.action ="get-subject-progress-details";
    $.ajax({
        'type'  :   'post',
        'url'   :   ApiEndPoint,
        'data'  :   JSON.stringify(req)
    }).done(function(res) {
        res = $.parseJSON(res);
        //console.log(res);
        if(res.status == 0)
            toastr.error(res.message);
        else {
            initGraphs(res);
        }
    });
}
//function to initiate all graphs
function initGraphs(data) {
    //console.log(data);
    $.each(data.content, function(i, d) {
        //console.log(d);
        contentName.push(d.title);
        if(d.title.length > 20) {
                    var first = d.title.substring(0, 20);
                    //var second = d.title.substring(d.title.length - 8);
                    d.title = first + '...';// + second;
                }
        cats.push(d.title);         
        
        
        contentId.push(d.id);
        filler_76.push(parseInt(d.content_76));
        filler_75.push(parseInt(d.content_75));
        filler_50.push(parseInt(d.content_50));
        filler_25.push(parseInt(d.content_25));
        filler_00.push(parseInt(d.content_00));
    });
    $('#subjectAnalytics').highcharts({
        colors: ['#2ecc71','#5C97BF','#4ECDC4','#F39C12','#bdc3c7'],
        chart: {
            type: 'bar',
            style: {
                fontFamily: 'Open Sans'
            }
        },
        title: {
            text: 'Content Viewed By '+terminologies["student_plural"]
        },
        tooltip: {
            followPointer: true,
            pointFormat: '<b>{point.y}</b> '+terminologies["student_plural"].toLowerCase()+' {series.name}'
        },
        xAxis: {
            categories: cats,
            type: 'category',
            labels: {
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            },
            title: {
                text: '<b>Contents</b>'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total '+terminologies["student_plural"]
            }
        },
        legend: {
            reversed: true,
            // align: 'right',
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                cursor: 'pointer',
                point: {
                    
                    events: {  
                        click: function () {
                            //console.log(contentProgress);
                            var html="";
                            var id=this.x;
                            var totalTime=0;
                            var progress=contentProgress[id].studentprogress;
                            if(contentProgress[id].totalTime !=undefined){
                                /// console.log(contentProgress[id].totalTime);
                                for (var key in contentProgress[id].totalTime) {
                                    if(contentProgress[id].totalTime[key]!='') {
                                        //totalTime=parseFloat(contentProgress[id].totalTime[key]).toFixed(2);
                                        totalTime=((contentProgress[id].totalTime[key]=='0')?'-':contentProgress[id].totalTime[key]);
                                    }
                                    else{
                                        totalTime=0;
                                    }
                                }
                            }
                            //console.log(totalTime);
                            //console.log(contentProgress);                         
                            //console.log(contentProgress[id]);     
                            $('#detailedAnalyticsModal').modal('show');
                            $('#content-name').html(' '+contentName[id] +' ');
                            //$('#tableStudentProgress tbody').html('').html('');

                            //console.log(progress);
                            $.each(progress, function(i, d) {
                                    d=Math.ceil(d);
                                    if(d>100){
                                        d=100;
                                    }
                                    //console.log(i);
                                    //console.log(contentProgress[id].totalTime);
                                    if(contentProgress[id].totalTime!='' && contentProgress[id].totalTime!=undefined){                                                      //  console.log(contentProgress[id].totalTime[i]);
                                        if(contentProgress[id].totalTime[i] != undefined){
                                            //totalTime=parseFloat(contentProgress[id].totalTime[i]).toFixed(2);
                                            totalTime=((contentProgress[id].totalTime[i]=='0')?'-':contentProgress[id].totalTime[i]);
                                        }else{
                                            totalTime=0;
                                        }
                                        //console.log(totalTime);
                                    }     
                                    else{
                                        totalTime=0;
                                    }
                                    //console.log(contentProgress[i].totalTime);
                                    //console.log(contentProgress[i]);
                                    //console.log(i);
                                    html += "<tr data-cid='" + i + "'" + "" + ">"
                                    + "<td>" + (i+1) + "</td>"
                                    + "<td>" + students[i] + "</td>"
                                    + "<td class='text-center'>" + d+ "</td>"
                                    + "<td class='text-center'>" + totalTime+ "</td>";
                            });
                            $('#tableStudentProgress tbody').html(html);
                            initTable1();
                        }
                    }//clik ended
                }//event ended
            }//points ended
        },
        series: [
        {
            name: 'More than 75 % watched',
            data: filler_76
        },{
            name: 'Between 50 -75 % Watched',
            data: filler_75
        },{
            name: 'Between 25 -50 % Watched',
            data: filler_50
        },{
            name: 'Below 25 % Watched',
            data: filler_25
        },{
            name: 'Not Watched',
            data: filler_00
        }]
    });
}
var initTable1 = function () {

    var table = $('#tableStudentProgress');

    // begin first table
    tblData = table.dataTable({

        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ records",
            "infoEmpty": "No records found",
            "infoFiltered": "(filtered1 from _MAX_ total records)",
            "lengthMenu": "Show _MENU_",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "paginate": {
                "previous":"Prev",
                "next": "Next",
                "last": "Last",
                "first": "First"
            }
        },

        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
        // So when dropdowns used the scrollable div should be removed. 
        //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 5,            
        "pagingType": "bootstrap_full_number",
        "columnDefs": [
            {  // set default column settings
                'orderable': false,
                'targets': [0]
            }, 
            {
                "searchable": false,
                "targets": [0]
            },
            {
                "className": "dt-right", 
                //"targets": [2]
            }
        ],
        "order": [
            [0, "asc"]
        ] // set first column as a default sort by asc
    });

    var tableWrapper = jQuery('#sample_1_wrapper');

    table.find('.group-checkable').change(function () {
        var set = jQuery(this).attr("data-set");
        var checked = jQuery(this).is(":checked");
        jQuery(set).each(function () {
            if (checked) {
                $(this).prop("checked", true);
                $(this).parents('tr').addClass("active");
            } else {
                $(this).prop("checked", false);
                $(this).parents('tr').removeClass("active");
            }
        });
    });

    table.on('change', 'tbody tr .checkboxes', function () {
        $(this).parents('tr').toggleClass("active");
    });
    
    // function to destroy data table each time it close button is clicked
    $('#detailedAnalyticsModal').on('hidden.bs.modal', function () {
        table.DataTable().destroy();
        $('#tableStudentProgress tbody').html('');
    });
}
</script>