<script type="text/javascript">
//function to fetch notifications
function fetchSubjectsAssigned() {
    var req = {};
    var res;
    req.action = 'get-subject-assigned-new';
    $.ajax({
            'type'  :   'post',
            'url'   :   ApiEndPoint,
            'data'  :   JSON.stringify(req)
    }).done(function(res) {
        res = $.parseJSON(res);
        if(res.status == 0)
            console.log(res.message);
        else {
            details = res;
            fillSubjectsAssigned(res);
        }
    });
}
function fillSubjectsAssigned(data) {
	//console.log(data.courses);
    $('#listCourses').html('');
	if (data.courses.length>0) {
        $.each(data.courses, function (i, course) {
            var startDate = new Date(course.liveDate);
            startDate = timeSince(startDate.getTime()); //returns 1340220044000
            $('#listCourses').append('<div class="col-md-6 col-sm-12 js-course-list" data-course="'+course.id+'">'+
                '<div class="portlet box portlet-course blue-hoki">'+
                    '<div class="portlet-title">'+
                        '<div class="caption">'+
                            '<i class="fa fa-gift"></i>'+course.name+'</div>'+
                        '<div class="tools">'+
                            '<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
                            '<a href="" class="fullscreen" data-original-title="" title=""> </a>'+
                        '</div>'+
                    '</div>'+
                    '<div class="portlet-body portlet-body-sm">'+
                        '<div class="row js-subjects-list"></div>'+
                    '</div>'+
                '</div>'+
            '</div>');
            if (course.subjects.length>0) {
                $.each(course.subjects, function (i, subject) {
                    $('#listCourses .js-course-list:last-child').find('.js-subjects-list').append(
                        '<div class="col-lg-4 col-md-6">'+
                            '<div class="mt-widget-4 margin-bottom-20">'+
                                '<div class="mt-img-container">'+
                                    '<img src="' + subject.image + '" /> </div>'+
                                '<div class="mt-container bg-'+darkThemeColors[getRandomInt(0, darkThemeColors.length-1)]+'-opacity">'+
                                    '<div class="mt-head-title"> ' + subject.name + ' </div>'+
                                    '<div class="mt-body-icons">'+
                                        '<a href="'+sitepathManageSubjects+subject.id+'" title="Lectures">'+
                                            '<i class="icon-notebook"></i><span class="count">' + subject.chapters + '</span>'+
                                        '</a>'+
                                        '<a href="'+sitepathManageSubjects+subject.id+'" title="Exams">'+
                                            '<i class="icon-note"></i><span class="count">' + subject.exams + '</span>'+
                                        '</a>'+
                                    '</div>'+
                                    '<a href="'+sitepathManageSubjects+subject.id+'">'+
                                        '<div class="mt-footer-button">'+
                                            '<button type="button" class="btn btn-circle btn-danger btn-sm"><i class="fa fa-edit"></i> Edit</button>'+
                                        '</div>'+
                                    '</a>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>');
                });
            }
        });
    }
}
function fillCourseDetails(data) {
    $('.js-course-action').html("Edit");
    $('#inputCourseStartDate').prop("disabled", true);
    $('.date-picker').datepicker({
        format: 'dd M yyyy',
        startDate: getToday(),
        orientation: "left",
        autoclose: true
    });
    // General Details
    if (data.courseDetails.deleted == 1)
        toastr.error('This '+terminologies["course_single"].toLowerCase()+' has been deleted!');
    var catIds = [], catNames = [];
    var catName=[];
    $('#inputCourseName').val(data.courseDetails.name);

    if (data.courseDetails.subtitle != '') {
        $('#inputCourseSubtitle').val(data.courseDetails.subtitle);
    }

    if (data.courseDetails.targetAudience != '') {
        $('#inputTargetAudience').val(data.courseDetails.targetAudience);
    }
    $('#inputTags').val(data.courseDetails.tags);
    if (data.courseDetails.description != '') {
        $('#inputCourseDescription').val(data.courseDetails.description);
    }
    startDate = formatDate(parseInt(data.courseDetails.liveDate));
    $('#inputCourseStartDate').val(startDate);

    if (data.courseDetails.endDate != '') {
        endDate = formatDate(parseInt(data.courseDetails.endDate));
        $('#inputCourseEndDate').val(endDate);
        $('#optionCourseEndDate').val(1);
        $('.js-course-end-date').removeClass('hide');
    } else {
        $('#optionCourseEndDate').prop('checked', false);
        $('.js-course-end-date').addClass('hide');
    }
    $('#modalAddEditCourse').modal("show");

    $.each(data.courseCategories, function (k, cat) {
        //catNames.push(cat.catName);
        catIds.push(cat.categoryId);
    });
    $('#selectCourseCategory').val(catIds);
}
</script>