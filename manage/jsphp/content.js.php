<script type="text/javascript">
var contentIds = <?php echo json_encode($contentIds); ?>;
var StuffType=0;
var presentContentId=0;
var idleCheckTimer;
var idleCheckTime=0;
var idleTimer;
var idleTime=0;
var startTime=0;
var videoDuration=0;
var videoPlaying=0;
var quizJSON={};

$(document).on("click", ".nav-link", function() {
	var thiz = $(this);
	if ($(this).attr("data-tree") == '0') {
		if (!isMobile) {
			//update_Timer();
			chapterId = thiz.attr("data-chapterid");
			contentId = thiz.parents(".nav-item").find(".sub-menu>.nav-item.start>a").attr("data-contentid");
			console.log(contentId);
			$('.nav-item').removeClass('active').removeClass('open');
			$('#contentTitle').text(thiz.parents(".nav-item").find('.sub-menu .nav-item:first-child').find('.title').text());
			$('#pageContent').html('');
			thiz.parents('.nav-item').addClass('active').addClass('open');
			fetchContentStuff();
		}
	} else {
		//update_Timer();
		contentId = thiz.attr("data-contentid");
		chapterId = thiz.attr("data-chapterid");
		$('.nav-item').removeClass('active').removeClass('open');
		$('#contentTitle').text(thiz.find('.title').text());
		$('#pageContent').html('');
		$('[data-tree="0"][data-chapterid="'+chapterId+'"]').parents('.nav-item').addClass('active').addClass('open');
		fetchContentStuff();
	}
	return false;
});

$('.h-nav-link').click(function(){
	var disabledAttr = $(this).attr('disabled');
	if (!disabledAttr) {
		contentId = $(this).attr("data-contentid");
		chapterId = $(this).attr("data-chapterid");
		$('.nav-item').removeClass('active').removeClass('open');
		$('#contentTitle').text($('[data-contentid="'+contentId+'"].nav-link').find('.title').text());
		$('#pageContent').html('');
		$('[data-tree="0"][data-chapterid="'+chapterId+'"]').parents('.nav-item').addClass('active').addClass('open');
		fetchContentStuff();
	}
});

//function to fill headings and generate side view
function fetchContentStuff() {
	if($('#contentTitle').text()=='') {
		$('.attempt-block').removeClass('hide');
		/*$('#contentTitle').text($('.page-sidebar-menu').find('.nav-item:first-child .sub-menu .nav-item:first-child').find('.title').text());*/
		$('#contentTitle').text($('[data-contentid="'+contentId+'"].nav-link').find('.title').text());
	}
	var req = {};
	var res;
	req.action = 'get-content-stuff';
	req.contentId = contentId;
	$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else
			fillContentDetails(res);
	});
}

//function to fill content details
function fillContentDetails(data) {
	idleTime = 0;
	idleCheckTime = 0;
	videoPlaying = 0;
	var contentItemHeight = $(window).height()-$(".page-top").height()-250;
	$('#activityModal').modal("hide");
	stopIdleCheckTimer();
	stopIdleTimer();
	startIdleCheckTimer();
	if(data.content.description != '') {
		$('#description').html('<div class="well bg-white"><strong>Description: </strong>'+data.content.description+'</div>');
	}
	else
		$('#description').html('<div class="note note-info">'+
									'<p> No description found. </p>'+
								'</div>');
	//$('#notes').summernote('destroy');
	$('#notes').summernote(summerShort);

	//console.log(contentIds);
	if(contentIds[contentIds.indexOf(contentId)+1] != undefined) {
		$('.next-attempt').attr('data-contentid', contentIds[contentIds.indexOf(contentId)+1]).removeAttr('disabled');
		var nextChapterId = $('[data-contentid="'+contentIds[contentIds.indexOf(contentId)+1]+'"].nav-link').attr("data-chapterid");
		$('.next-attempt').attr('data-chapterid', nextChapterId);
	} else {
		$('.next-attempt').removeAttr('data-contentid').removeAttr('data-chapterid').attr("disabled", true);
	}
	if(contentIds[contentIds.indexOf(contentId)-1] != undefined) {
		$('.previous-attempt').attr('data-contentid', contentIds[contentIds.indexOf(contentId)-1]).removeAttr('disabled');
		var previousChapterId = $('[data-contentid="'+contentIds[contentIds.indexOf(contentId)-1]+'"].nav-link').attr("data-chapterid");
		$('.previous-attempt').attr('data-chapterid', previousChapterId);
	} else {
		$('.previous-attempt').removeAttr('data-contentid').removeAttr('data-chapterid').attr("disabled", true);
	}
	if(data.notes.length > 0) {
		$('#notes').summernote('code', data.notes[0].notes);
		$('#timeNotes').html('Last updated '+data.notes[0].modified);
	} else {
		$('#notes').summernote('code', '');
		$('#timeNotes').html('');
	}
	var downloadable = $('[data-contentid='+contentId+'].nav-link').attr('data-downloadable');
	if (downloadable == '1') {
		$("#contentDownload").html('<h3 class="list-heading">DOWNLOAD LECTURE</h3>'+
                    '<ul class="media-list list-items">'+
                        '<li class="media">'+
                            '<a href="'+data.content.stuff+'" class="link-no-style" target="_blank">'+
	                            '<div class="media-status no-margin">'+
									'<i class="fa fa-download"></i>'+
								'</div>'+
								'<div class="media-body">'+
									'<h4 class="media-heading">Download</h4>'+
								'</div>'+
							'</a>'+
                        '</li>'+
                    '</ul>');
	}
	if(data.supplementary.length > 0) {
		var html = '<ul class="media-list list-items">';
		for(i = 0; i < data.supplementary.length; i++) {
			//html += '<a href="' + data.supplementary[i].signedURL + '" target="_blank"><br>' + data.supplementary[i].fileRealName + '</a><br>';
			html += '<li class="media">'+
						'<a href="' + data.supplementary[i].signedURL + '" class="link-no-style" target="_blank">'+
							'<div class="media-status no-margin">'+
								'<i class="fa fa-download"></i>'+
							'</div>'+
							'<div class="media-body">'+
								'<h4 class="media-heading">' + data.supplementary[i].fileRealName + '</h4>'+
							'</div>'+
						'</a>'+
					'</li>';
		}
		html+= '</ul">';
		$('#downloads').html(html);
	}
	else
		$('#downloads').html('<div class="note note-info">'+
								'<p> No supplementary materials found. </p>'+
							'</div>');
	switch(data.content.type) {
		case "text":
			StuffType		= 1;
			presentContentId= (data.content.contentId);
			startTime = $.now();
			$('#pageContent').html('<div class="well bg-white" style="height:'+contentItemHeight+'">'+data.content.stuff+'</div>');

			//update_Timer();

			break;
		case "dwn":
			StuffType=2;
			presentContentId = (data.content.contentId);
			startTime = $.now();
			//update_Timer();
			var html = '<div class="well no-margin" style="height:'+contentItemHeight+'">'+
							'<div class="dropzone dropzone-file-area dz-clickable" id="my-dropzone">'+
								'<a href="'+data.content.stuff+'" class="btn green btn-lg margin-bottom-20" target="_blank">Download</a>'+
								'<p> This is downloadable file, just click here to download it. </p>'+
							'<div class="dz-default dz-message"><span></span></div></div>'+
						'</div>';
			$('#pageContent').html(html);

			break;
		case "Youtube":
			StuffType		 = 3;
			presentContentId = (data.content.contentId);

			var videoId = getStringParameter('v', data.content.stuff);

			var html = '<div id="ik_player"></div>';
			
			$('#pageContent').html(html);
			var player;
			function onYouTubePlayerAPIReady() {
				player = new YT.Player('ik_player', {
					height: contentItemHeight,
					width: '100%',
					videoId: videoId,
					events: {
						'onReady': onPlayerReady,
						'onStateChange': onPlayerStateChange
					}
				});
			}

			function youtubeReadyFunction()
			{
				$.getScript("https://www.youtube.com/player_api", onYouTubePlayerAPIReady);
			}

			// autoplay video
			function onPlayerReady(event) {
			    //event.target.playVideo();
			}

			// when video ends
			function onPlayerStateChange(event) {
				videoDuration =(event.target.getDuration());     
			    if(event.data === 0) {
			    	videoPlaying = 0;        
			        callPause();
			    }
			    if (event.data == 1) {
			    	videoPlaying = 1;
					callPlay();
			    }
				else if (event.data == 2) {
			    	videoPlaying = 0;
					callPause();
				}
			}
			function callPlay () {
				startTime = $.now();
				////update_Timer();
			}
			function callPause () {
				//startTime = $.now();
				//update_Timer();
			}
			//onYouTubePlayerAPIReady();
			youtubeReadyFunction();
			break;
		case "Vimeo" :
			StuffType=4;
			presentContentId=(data.content.contentId);
			var videoId = getVimeoId(data.content.stuff);
				
			var html = '<iframe id="player1" src="https://player.vimeo.com/video/'+videoId+'?api=1&player_id=player1" id="player1" width="100%" height="'+contentItemHeight+'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
			$('#pageContent').html(html);
			
			var iframe = $('#player1')[0];
			var player = $f(iframe);

			// When the player is ready, add listeners for pause, finish, and playProgress
			player.addEvent('ready', function() {
				player.addEvent('play', onPlay);
				player.addEvent('pause', onPause);
				player.addEvent('finish', onFinish);
				//player.addEvent('playProgress', onPlayProgress);
			});

			// Call the API when a button is pressed
			$('.custom-content-part button').bind('click', function() {
				player.api($(this).text().toLowerCase());
			});

			function onPause() {
			    videoPlaying = 0;
				//update_Timer();
			}

			function onFinish() {
			    videoPlaying = 0;
				//update_Timer();
			}

			function onPlay(data) {
			    videoPlaying = 1;
				videoDuration =(data.duration);
				startTime = $.now();
			}
			break;
		case "ppt":
		case "doc":
		case "pdf":
			StuffType			=	5;

			presentContentId	=	(data.content.contentId);

			var html='<iframe id="viewer" src="'+sitepathManage+'assets/custom/js/plugins/pdfjs/web/viewer.html?‌​file='+data.content.stuff+'" class="btn-block" height="'+contentItemHeight+'" frameborder="0"></iframe>';

			$('#pageContent').html(html);

			//$("#viewer").on("load", function () {
				//update_Timer();
			//});
			break;
		case "video":
			StuffType=6;
			presentContentId=(data.content.contentId);

			var html = '<video id="videoPlayer" width="100%" height="'+contentItemHeight+'"></video>';
			$('#pageContent').html(html);

			var src = data.content.stuff;
			player = new MediaElementPlayer('#videoPlayer', {
				type: 'video/mp4',
				videoWidth:"100%",
				videoHeight: contentItemHeight+"px",
				success: function (mediaElement, domObject) {
					mediaElement.addEventListener('pause', function () {
						videoPlaying = 0;
						//update_Timer();
					}, false);
					mediaElement.addEventListener('play', function () {
						videoPlaying = 1;
						videoDuration = mediaElement.duration;
						startTime = $.now();
					}, false);
					mediaElement.addEventListener('ended', function () {
						videoPlaying = 0;
						//update_Timer();
					}, false);
				}
			});
			var sources = [
				{ src: src, type: 'video/mp4' }
			];

			player.setSrc(sources);
			player.load();
			break;
		case "quiz":
			StuffType			=	7;

			presentContentId	=	(data.content.contentId);

			quizJSON = $.parseJSON(data.content.stuff);

			var html='<div id="slickQuiz">'+

						'<h1 class="quizName"></h1>'+

						'<div class="quizArea">'+
						    '<div class="quizHeader">'+

						        '<a class="btn green startQuiz" href="#">Get Started!</a>'+
						    '</div>'+

						'</div>'+

						'<div class="quizResults">'+
						    '<h3 class="quizScore">You Scored: <span></span></h3>'+

						    '<h3 class="quizLevel"><strong>Ranking:</strong> <span></span></h3>'+

						    '<div class="quizResultsCopy">'+
						    '</div>'+
						'</div>'+
					'</div>';

			$('#pageContent').html(html);
    		$('#slickQuiz').slickQuiz();

			//$("#viewer").on("load", function () {
				//update_Timer();
			//});
			break;
		case "survey":
			StuffType		= 8;
			presentContentId= (data.content.contentId);
			var surveyStuff = $.parseJSON(data.content.stuff);
			if (surveyStuff.survey_type == 'google') {
				var surveyLink = 'https://docs.google.com/forms/d/e/'+surveyStuff.survey_id+'/viewform';
			} else {
				var surveyLink = 'https://www.surveymonkey.com/r/'+surveyStuff.survey_id;
			}
			startTime = $.now();
			$('#pageContent').html('<div class="well bg-white" style="height:300px; text-align:center; padding-top:120px;"><a href="'+surveyLink+'" class="btn btn-lg green" target="_blank">Take quick survey</a></div>');

			//update_Timer();

			break;
		default:
			break;
	}
}
$('#btnSubmitNotes').click(function(){
	update_Notes();
});
function update_Notes() {
	var notes=$('#notes').summernote('code');
	var	reqContentId=presentContentId;
	var req = {};
	var res;
	req.contentId=presentContentId;
	req.chapterId=chapterId;
	req.subjectId=subjectId;
	req.notes=notes;
	req.action = 'set-content-notes';
	$.ajax({
		'type': 'post',
		'url': ApiEndPoint,
		'data': JSON.stringify(req)
	}).done(function (res) {
		$('#loaderContent').hide();
		res = $.parseJSON(res);
		if(res.status ==1  )
		{
			toastr.success("Successfully saved notes!");
			$('#timeNotes').html('Last updated '+res.modified);
		} else {
			toastr.error(res.message);
		}
	});
}
/*function update_Timer() {
	var endTime=$.now();
	if (endTime>startTime) {
		var	reqContentId=presentContentId;
		var req = {};
		var res;
		req.stuffType=StuffType;
		req.contentId=presentContentId;
		req.chapterId=chapterId;
		req.subjectId=subjectId;
		req.startTime=startTime;
		req.endTime  =endTime;
		req.idleTime =idleTime;
		req.videoDuration=videoDuration;
		req.action = 'set-content-watched';
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			$('#loaderContent').hide();
			res = $.parseJSON(res);
			if(res.status ==1  )
			{	
				$('[data-id="'+reqContentId+'"]').children('.content-container').css('background-color',"#2ecc71")
			} else {
				toastr.error(res.message);
			}
		});
	};
}*/
//function to fetch data from url string
function getStringParameter(sParam, url) {
	var sPageURL = url.split('?')[1];
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) 
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) 
		{
			return sParameterName[1];
		}
	}
}

//function to fetch video id from vimeo link
function getVimeoId(url) {
	var sURLVariables = url.split('/');
	return sURLVariables[sURLVariables.length - 1];
}

$(document).mousemove(function () {
	idleCheckTime = 0;
	$('#activityModal').modal("hide");
	stopIdleCheckTimer();
	stopIdleTimer();
	startIdleCheckTimer();
});
$(document).click(function () {
	idleCheckTime = 0;
	$('#activityModal').modal("hide");
	stopIdleCheckTimer();
	stopIdleTimer();
	startIdleCheckTimer();
});
$(document).keypress(function (e) {
	idleCheckTime = 0;
	$('#activityModal').modal("hide");
	stopIdleCheckTimer();
	stopIdleTimer();
	startIdleCheckTimer();
});

//function to start timer for each question
function startIdleCheckTimer() {
	idleCheckTimer = setInterval(function() {
		idleCheckTime += 1;
		if (idleCheckTime==1200) {
			$('#activityModal').modal("show");
			stopIdleCheckTimer();
			startIdleTimer();
		};
		//for temporary time display of each question
		//$('#tempQuestionTimer').text(answers[needle].timer);
	},	1000);
}

//function to stop timer for each question
function stopIdleCheckTimer() {
	clearInterval(idleCheckTimer);
}

//function to start timer for each question
function startIdleTimer() {
	idleTimer = setInterval(function() {
		idleTime += 1;
		//for temporary time display of each question
		//$('#tempQuestionTimer').text(answers[needle].timer);
	},	1000);
}

//function to stop timer for each question
function stopIdleTimer() {
	clearInterval(idleTimer);
}

function initWeb3() {
	// body...
}
</script>