<script type="text/javascript">
var UINestable = function () {

	return {
		//main function to initiate the module
		init: function () {
			// activate Nestable for list 1
			$('#nestable_list_1').nestable({
				group: 1,
				noDragClass: 'no-drag'
			})
				.on('change', function() {
					for(var i=0; i< $('#sortOrder').find('li').length; i++) {
						sortOrder[i] = $('#sortOrder li:eq('+i+')').attr('data-original');
					}
				});
		}
	};

}();
$(document).ready(function(){
	UINestable.init();
	/*$(".form_datetime").datetimepicker({
		autoclose: true,
		format: "dd MM yyyy - hh:ii",
		pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
	});*/
	$(".js-exam-type").change(function(){
		var option = $(this).val();
		if (option == "exam") {
			$(".js-section-timings").removeClass("hide");
			$(".js-sections-manage").removeClass("hide");
		} else {
			$(".js-section-timings").addClass("hide");
			$(".js-sections-manage").addClass("hide");
		}
		if (page == "examCreate") {
			nameAvailable();
		}
	});
	$(".js-settings").change(function(){
		var settings = $(this).val();
		if (settings == "template") {
			$(".js-template").removeClass("invisible");
			$(".js-exam-settings").addClass("hide");
		} else {
			$(".js-template").addClass("invisible");
			$(".js-exam-settings").removeClass("hide");
		}
		$('#sections').find('div.form-group').remove();
		$('#noOfAttempts').val('');
		$('.time.hour').val('0');
		$('.time.minute').val('0');
		$('#totalTimeHour').val('0');
		$('#totalTimeMinute').val('0');
		$('#templateSelect').trigger('change');
	});
	$(".js-set-time-options").change(function(){
		var options = $(this).val();
		if (options == 1) {
			$(".js-set-time").removeClass("hide");
		} else {
			$(".js-set-time").addClass("hide");
		}
	});
});
// Workaround to fix datetimepicker position on window scroll
$( document ).scroll(function(){
	$('#form_modal1 .form_datetime, #form_modal1 .form_advance_datetime, #form_modal1 .form_meridian_datetime').datetimepicker('place'); //#modal is the id of the modal
});
</script>