<script type="text/javascript">
var questions;
var index=-1;
var reqTag = {};
reqTag.action = 'tags';
var ms = $('#magicsuggest').magicSuggest({
	valueField: 'name',
	data: TypeEndPoint,
	dataUrlParams: reqTag
});
function fetchSubjectiveQuestions() {
	var req = {};
	var res;
	req.action = 'get-subjective-questions';
	req.subjectId = subjectId;
	req.courseId = courseId;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
		{
			toastr.error(res.message);
			//window.location.href =sitePath+'404';
		}				
		else{
			//fillSubjectiveExam(res);
			questions = res.questions;
			resetQuestionModal();
		}
	});
}
/*function fillSubjectiveExam(data) {
	resetQuestionModal();
}*/
function resetQuestionModal() {
	$('#listQuestions').html('');
	CKEDITOR.instances["inputQuestion"].setData('');
	CKEDITOR.instances["inputAnswer"].setData('');
	$('#inputMarks').val('');
	$('#selectDifficulty').val(0);
	ms.clear();
	$('.marks').removeClass('hide');
	CKEDITOR.instances["inputQuestionDescription"].setData('');
	$('#inputNoOfQuestions').val('');
	$('#subquestionsMrks').val('');
	$('.chooseQuestionType').removeClass('hide');
	$('#optionQuestion').trigger('click');
	$('#allquestion').trigger('click');
	$('.js-tagging').removeClass('hide');
	$('#btnAddQuestion').attr('data-question','');
	$('#btnAddQuestion').attr('data-parent','');
	//fillSubjectiveExam();
	if (questions.length>0) {
		for (var i = 0; i < questions.length; i++) {
			if(questions[i].questionType == "question") {
				$('#listQuestions').append('<li class="mt-list-item done js-question-edit" data-question="'+i+'">'+
	                '<div class="list-icon-container">'+
	                    '<a href="javascript:;">'+
	                        (i+1)+
	                    '</a>'+
	                '</div>'+
	                '<div class="list-datetime">'+
	                	'<a href="javascript:void(0)" class="btn btn-danger btn-sm js-question-delete"><i class="fa fa-trash"></i></a>'+
	                '</div>'+
	                '<div class="list-item-content">'+
	                    '<h3 class="uppercase">'+
	                        '<a href="javascript:;">'+questions[i].questionBrief+'</a>'+
	                    '</h3>'+
	                    '<p class="text-overflow">'+questions[i].answerBrief+'</p>'+
	                    '<p class="font-green">Marks: '+questions[i].marks+'</p>'+
	                '</div>'+
	            '</li>');
			} else {
				$('#listQuestions').append('<li class="mt-list-item done js-question-edit" data-question="'+i+'">'+
	                '<div class="list-icon-container">'+
	                    '<a href="javascript:;">'+
	                        (i+1)+
	                    '</a>'+
	                '</div>'+
	                '<div class="list-datetime">'+
	                	'<a href="javascript:void(0)" class="btn green btn-sm js-subQuestions"><i class="fa fa-plus"></i></a>'+
	                	((questions[i].subQuestions.length==0)?'<a href="javascript:void(0)" class="btn btn-danger btn-sm js-question-delete"><i class="fa fa-trash"></i></a>':'')+
	                '</div>'+
	                '<div class="list-item-content">'+
	                    '<h3 class="uppercase">'+
	                        '<a href="javascript:;">'+((questions[i].questionsonPage==1)?(questions[i].questionBrief):'Question Group')+'</a>'+
	                    '</h3>'+
	                    '<p><small>'+((questions[i].questionsonPage==1)?('(QuestionGroup : In one page)'):('(QuestionGroup : One question per page)'))+'</small></p>'+
	                    '<p class="font-green">Marks per question: '+((questions[i].questionsonPage==1)?(questions[i].subquestionMrks):(questions[i].subquestionMrks))+'</p>'+
	                '</div>'+
	            '</li>');
	            var subQuestions = questions[i].subQuestions;
				if (subQuestions.length>0) {
					for (var j = 0; j < subQuestions.length; j++) {
						if(subQuestions[j].questionType == "question") {
							$('#listQuestions').append('<li class="mt-list-item done sub-question js-subquestion-edit" data-question="'+i+'" data-subquestion="'+j+'">'+
				                '<div class="list-icon-container">'+
				                    '<a href="javascript:;">'+
				                        (j+1)+
				                    '</a>'+
				                '</div>'+
				                '<div class="list-datetime">'+
				                	'<a href="javascript:void(0)" class="btn btn-danger btn-sm js-subquestion-delete"><i class="fa fa-trash"></i></a>'+
				                '</div>'+
				                '<div class="list-item-content">'+
				                    '<h3 class="uppercase">'+
				                        '<a href="javascript:;">'+subQuestions[j].questionBrief+'</a>'+
				                    '</h3>'+
				                    '<p class="text-overflow">'+subQuestions[j].answerBrief+'</p>'+
	                    			'<p class="font-green">Marks: '+questions[i].subquestionMrks+'</p>'+
				                '</div>'+
				            '</li>');
						}
					};
				};
			}
		}
	}
}
function resetQuestionShort() {
	$('#inputMarks').val('');
	$('#selectDifficulty').val(0);
	ms.clear();
	$('.marks').removeClass('hide');
	$('#inputNoOfQuestions').val('');
	$('#subquestionsMrks').val('');
	$('.chooseQuestionType').removeClass('hide');
	$('#optionQuestion').trigger('click');
	$('#allquestion').trigger('click');
	$('.js-tagging').removeClass('hide');
	$('#btnAddQuestion').attr('data-question','');
	$('#btnAddQuestion').attr('data-parent','');
}
$(document).ready(function(){
	$(".js-question-type").change(function(){
		var questionType = $(this).val();
		$(".js-question-block").addClass("hide");
		$("#"+questionType+"Block").removeClass("hide");
	});
	$(".js-sub-question-type").change(function(){
		var subQuestionType = $(this).val();
		if (subQuestionType == "one") {
			$("#descriptionBlock").removeClass("hide");
		} else {
			$("#descriptionBlock").addClass("hide");
		}
	});
	$('.new-question').click(function(){
		var questionType = $(this).attr('data-type');
		resetQuestionModal();
		if(questionType == 'question') {
			$('#optionQuestion').trigger('click');
		} else {
			$('#optionQuestionGroup').trigger('click');
		}
	});
	$('#btnAddQuestion').click(function(){
		var req={};
		var questionType = $('.js-question-type:checked').val();
		var questionjson=[];
		var parentid='';
		var marks='';
		var difficulty = $('#selectDifficulty').val();
		var tags = ms.getValue();
		//console.log(ms.getSelection());
		var tagsJson = ms.getSelection();
		if (questionType == "question") {
			var question = CKEDITOR.instances['inputQuestion'].getData();
			var answer 	 = CKEDITOR.instances['inputAnswer'].getData();
			var marks	 = $('#inputMarks').val();
			
			//console.log($('#inputMarks').val());
			if($('#inputMarks').val()=='' || $('#inputMarks').val()==undefined)
			{
				marks=0;
			}
			if (!question || !answer) {
				toastr.error('Question and Answer both are compulsory!');
			} else {
				var questionEdit = $(this).attr('data-question');
				var parent = $(this).attr('data-parent');
				if (questionEdit == '' || questionEdit == undefined) {
					if (!parent) {
						var qno = questions.length+1;
						questions.push({id:'n'+qno,questionType:questionType,question:question,answer:answer,marks:marks,difficulty:difficulty,/*tags:tagsJson,*/subQuestions:[]});
						questionjson= ({id:'n'+qno,questionType:questionType,question:question,answer:answer,marks:marks,difficulty:difficulty,/*tags:tagsJson,*/subQuestions:[]});
						/*questions.push({id:qno,questionType:questionType,question:question,answer:answer,marks:marks,subQuestions:[]});
						questionjson= ({id:qno,questionType:questionType,question:question,answer:answer,marks:marks,subQuestions:[]});*/
						parentid='';
					}
					else {
						var qno = questions[parent]["subQuestions"].length+1;
						//console.log(questions[parent]);
						parentid=questions[parent].id;
						marks=questions[parent].subquestionMrks;
						//questions[parent]["subQuestions"].push({id:'s'+qno,questionType:questionType,question:question,answer:answer,marks:marks});
						questions[parent]["subQuestions"].push({id:qno,questionType:questionType,question:question,answer:answer,marks:marks});
						//console.log(marks);
						questionjson={id:'s'+qno,questionType:questionType,question:question,answer:answer,marks:marks};
						//questionjson={id:qno,questionType:questionType,question:question,answer:answer,marks:marks};
						//console.log({id:'s'+qno,questionType:questionType,question:question,answer:answer});
					}
					//saving question in db
					//console.log(tagsJson);
					req.questions=JSON.stringify(questionjson);
					req.parent=parentid;
					req.marks=marks;
					req.difficulty=difficulty;
					req.tags=tags;
					req.subjectId=subjectId;
					req.courseId=courseId;
					req.action = 'insert-subjective-questions';
					//console.log(req);
					$.ajax({
						'type'	: 'post',
						'url'	: ApiEndPoint,
						'data'	: JSON.stringify(req)
					}).done(function (res) {
						res =  $.parseJSON(res);
						if(res.status == 0)
							toastr.error(res.message);
						else
						{
							if (!parent) {
								questions[questions.length-1].id			= res.questionId;
								questions[questions.length-1].questionBrief	= res.questionBrief;
								questions[questions.length-1].answerBrief	= res.answerBrief;
								questions[questions.length-1].tags			= res.tags;
							} else {
								questions[parent]["subQuestions"][questions[parent]["subQuestions"].length-1].id = res.questionId;
								questions[parent]["subQuestions"][questions[parent]["subQuestions"].length-1].questionBrief = res.questionBrief;
								questions[parent]["subQuestions"][questions[parent]["subQuestions"].length-1].answerBrief = res.answerBrief;
							}
							resetQuestionModal();
						}
					});
				} else {
					if (!parent) {
						questions[questionEdit].questionType = questionType;
						questions[questionEdit].question	 = question;
						questions[questionEdit].answer		 = answer;
						questions[questionEdit].marks		 = marks;
						questions[questionEdit].difficulty	 = difficulty;
						//questions[questionEdit].tags	 	 = tagsJson;
						questions[questionEdit].subQuestions = [];
						//console.log(questions[questionEdit]);
						questionjson=questions[questionEdit];
						//
					} else {
						questions[parent]["subQuestions"][questionEdit].questionType = questionType;
						questions[parent]["subQuestions"][questionEdit].question	 = question;
						questions[parent]["subQuestions"][questionEdit].answer		 = answer;
						questions[parent]["subQuestions"][questionEdit].marks		 = marks;
						//console.log(questions[parent]["subQuestions"][questionEdit]);
						//console.log(questions[parent].id);
						questionjson=questions[parent]["subQuestions"][questionEdit];
						parentid=questions[parent].id;
						marks=questions[parent].subquestionMrks;
						
					}
					req.questions=JSON.stringify(questionjson);
					req.parent=parentid;
					req.marks=marks;
					req.difficulty=difficulty;
					req.tags=tags;
					req.subjectId=subjectId;
					req.courseId=courseId;
					req.action = 'edit-subjective-questions';
					//console.log(tagsJson);
					$.ajax({
						'type'	: 'post',
						'url'	: ApiEndPoint,
						'data'	: JSON.stringify(req)
					}).done(function (res) {
						res =  $.parseJSON(res);
						if(res.status == 0)
							toastr.error(res.message);
						else
						{
							if (!parent) {
								questions[questionEdit].questionBrief	= res.questionBrief;
								questions[questionEdit].answerBrief	= res.answerBrief;
								questions[questionEdit].tags		= res.tags;
							} else {
								questions[parent]["subQuestions"][questionEdit].questionBrief = res.questionBrief;
								questions[parent]["subQuestions"][questionEdit].answerBrief = res.answerBrief;
							}
							resetQuestionModal();
						}
					});
				}
			}
		} 
		else {
			var question = CKEDITOR.instances['inputQuestionDescription'].getData();
			var noOfQuestions = $('#inputNoOfQuestions').val();		
			var subquestionMrks=$('#subquestionsMrks').val();
			var questionsonPage=1;
			var subquestions=[];
			if(!(index<0))
			{
				subquestions=questions[index].subQuestions;
			}
			if($('#onequestion').prop('checked'))
			{
				questionsonPage=2;
			}
			if (!noOfQuestions || noOfQuestions < 0 || subquestionMrks < 0) {
				$('#questionGroupBlock .help-block').html('<p class="text-danger">Question Group Description and No of Questions both are compulsory and  No of Questions and subquestions marks should be greater then 0!</p>');
			} else {
				var questionEdit = $(this).attr('data-question');
				if (!questionEdit) {
					var qno = questions.length+1;
					questions.push({id:'n'+qno,questionType:questionType,question:question,answer:noOfQuestions,subquestionMrks:subquestionMrks,questionsonPage:questionsonPage,difficulty:difficulty,/*tags:tagsJson,*/subQuestions:[]});
					questionjson		= ({id:'n'+qno,questionType:questionType,question:question,answer:noOfQuestions,subquestionMrks:subquestionMrks,questionsonPage:questionsonPage,difficulty:difficulty,/*tags:tagsJson,*/subQuestions:[]});
					/*questions.push({id:qno,questionType:questionType,question:question,answer:noOfQuestions,subquestionMrks:subquestionMrks,questionsonPage:questionsonPage,subQuestions:[]});
					questionjson		= ({id:qno,questionType:questionType,question:question,answer:noOfQuestions,subquestionMrks:subquestionMrks,questionsonPage:questionsonPage,subQuestions:[]});*/
					//console.log(questionjson);
					req.questions		= JSON.stringify(questionjson);
					req.tags			= tags;
					req.parent			= parentid;
					req.subjectId		= subjectId;
					req.courseId		= courseId;
					req.action			= 'insert-subjective-questions';
					//console.log(tagsJson);
					$.ajax({
						'type'	: 'post',
						'url'	: ApiEndPoint,
						'data'	: JSON.stringify(req)
					}).done(function (res) {
						res =  $.parseJSON(res);
						if(res.status == 0)
							toastr.error(res.message);
						else
						{
							questions[questions.length-1].id			= res.questionId;
							questions[questions.length-1].questionBrief	= res.questionBrief;
							questions[questions.length-1].answerBrief	= res.answerBrief;
							questions[questions.length-1].tags			= res.tags;
							resetQuestionModal();
						}
					});
				} else {
					questions[questionEdit].questionType = questionType;
					questions[questionEdit].question	 = question;
					questions[questionEdit].answer		 = noOfQuestions;
					questions[questionEdit].subquestionMrks	 = subquestionMrks;
					questions[questionEdit].questionsonPage	 = questionsonPage;
					questions[questionEdit].difficulty	 = difficulty;
					//questions[questionEdit].tags	 	 = tagsJson;
					questions[questionEdit].subQuestions = subquestions;
					questionjson=questions[questionEdit];
					
					req.questions=JSON.stringify(questionjson);
					req.tags			= tags;
					req.parent=parentid;
					req.marks=marks;
					req.subjectId=subjectId;
					req.courseId=courseId;
					req.action = 'edit-subjective-questions';
					//console.log(tagsJson);
					$.ajax({
						'type'	: 'post',
						'url'	: ApiEndPoint,
						'data'	: JSON.stringify(req)
					}).done(function (res) {
						res =  $.parseJSON(res);
						if(res.status == 0)
							toastr.error(res.message);
						else
						{
							questions[questionEdit].questionBrief	= res.questionBrief;
							questions[questionEdit].answerBrief		= res.answerBrief;
							questions[questionEdit].tags		= res.tags;
							resetQuestionModal();
						}
					});
				}
				//console.log(JSON.stringify(questions));
			}
		}
	});
	$('#listQuestions').on('click', '.js-subQuestions', function(){
		resetQuestionModal();
		var question = $(this).closest('.mt-list-item').attr('data-question');
		console.log(question);
		$('#btnAddQuestion').attr('data-parent',question);
		$('#optionQuestion').trigger('click');
		$('.chooseQuestionType').addClass('hide');
		$('.marks').addClass('hide');
		return false;
	});
	$('#listQuestions').on('click', '.js-question-edit', function(e){
		if(e.target.className=="btn green btn-sm js-subQuestions" || e.target.className=="fa fa-plus") {
			return;
		}
		resetQuestionShort();
		var questionEdit = $(this).attr('data-question');
		index=questionEdit;
		//console.log(questions[questionEdit]);
		var question	= questions[questionEdit].question;
		var answer		= questions[questionEdit].answer;
		var marks		= questions[questionEdit].marks;
		var difficulty	= questions[questionEdit].difficulty;
		var tags		= questions[questionEdit].tags;
		//console.log(difficulty);
		if(questions[questionEdit].questionType == "question") {
			//$('#optionQuestion').trigger('click');
			CKEDITOR.instances["inputQuestion"].setData(question);
			CKEDITOR.instances["inputAnswer"].setData(answer);
			$('#inputMarks').val(marks);
		} else {
			var subquestionMrks=questions[questionEdit].subquestionMrks;
			var questionsonPage=questions[questionEdit].questionsonPage;
			$('#optionQuestionGroup').trigger('click');
			CKEDITOR.instances["inputQuestionDescription"].setData(question);
			//$('#inputQuestionDescription').val(question);
			$('#inputNoOfQuestions').val(answer);
			$('#subquestionsMrks').val(subquestionMrks);
			if(questionsonPage==2)
			{
				//questionsonPage=2;
				$('#onequestion').trigger('click')
			}else{
				$('#allquestion').trigger('click')
			}
		}
		$('#selectDifficulty').val(difficulty);
		//console.log(tags);
		//ms.setData(tags);
		ms.setSelection(tags);
		$('.js-tagging').removeClass('hide');
		$('#btnAddQuestion').attr('data-question',questionEdit);
	});
	$('#listQuestions').on('click', '.js-subquestion-edit', function(e){
		resetQuestionShort();
		var questionEdit = $(this).attr('data-question');
		var subQuestionEdit = $(this).attr('data-subquestion');
		var question = questions[questionEdit]["subQuestions"][subQuestionEdit].question;
		var answer = questions[questionEdit]["subQuestions"][subQuestionEdit].answer;
		//var marks=  questions[questionEdit]["subQuestions"][subQuestionEdit].marks;
		var marks=  questions[questionEdit].subquestionMrks;
		if(questions[questionEdit]["subQuestions"][subQuestionEdit].questionType == "question") {
			$('#optionQuestion').trigger('click');
			CKEDITOR.instances["inputQuestion"].setData(question);
			CKEDITOR.instances["inputAnswer"].setData(answer);
			//$('#inputQuestion').val(question);
			//$('#inputAnswer').val(answer);
			$('#inputMarks').val(marks);
		}
		$('#btnAddQuestion').attr('data-question',subQuestionEdit);
		$('#btnAddQuestion').attr('data-parent',questionEdit);
		$('.chooseQuestionType').addClass('hide');
		$('.marks').addClass('hide');
		$('.js-tagging').addClass('hide');
		//$('#optionQuestionGroup').prop('disabled', true);
		$('#questionModal').modal('show');
	});
	$('#listQuestions').on('click', '.js-question-delete', function(){
		var questionEdit = $(this).closest('.mt-list-item').attr('data-question');
		//console.log(JSON.stringify(questions[questionEdit]));
		var subQuestionsCheck=questions[questionEdit]["subQuestions"];
		if(subQuestionsCheck.length>0)
		{
			toastr.error("Please delete Subquestions before deleting Main questions");
		}
		else{
			var parentid='';
			var req={};
			req.questions=JSON.stringify(questions[questionEdit]);
			req.parent=parentid;
			req.subjectId=subjectId;
			req.courseId=courseId;
			req.action = 'delete-subjective-questions';
			//console.log(req);
			$.ajax({
			'type'  : 'post',
			'url'   : ApiEndPoint,
			'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else
				{
					questions.splice(questionEdit,1);
					resetQuestionModal();
				}
			});
		}
	});
	$('#listQuestions').on('click', '.js-subquestion-delete', function(){
		var questionDiv = $(this).closest('.mt-list-item');
		var questionEdit = questionDiv.attr('data-question');
		var subQuestionEdit = questionDiv.attr('data-subquestion');
		var parentid='';
		var req={};
		req.questions=JSON.stringify(questions[questionEdit]["subQuestions"][subQuestionEdit]);
		req.parent=questions[questionEdit].id;
		req.subjectiveExamId=examId;
		req.subjectId=subjectId;
		req.courseId=courseId;
		req.action = 'delete-subjective-questions';
		//console.log(req);
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndPoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				toastr.error(res.message);
			else
			{
				questions[questionEdit]["subQuestions"].splice(subQuestionEdit,1);
				resetQuestionModal();
			}
		});
	});
});
</script>