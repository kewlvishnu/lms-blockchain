function ajaxRequest(data){
    //$("#socialAJXError,#socialAJXSuccess").html('').hide();
    if(!(data.loaderDisable!=undefined && data.loaderDisable=="true"))
    {
        data.complete=function(){
            //hideL();
        }
    }
    $.ajax(data);
}
var readFile = function(file, options) {
    var dfd = new $.Deferred();
    
    // define FileReader object
    var reader = new FileReader();
    var that = this;

    // init reader onload event handlers
    reader.onload = function(e) {
        var image = $('<img/>')
            .load(function() {
                //when image fully loaded
                var newimageurl = getCanvasImage(this, options);
                dfd.resolve(newimageurl, this);
            })
           .attr('src', e.target.result);
    };
    reader.onerror = function(e) {
        dfd.reject("Couldn't read file " + file.name);
    }
    // begin reader read operation
    reader.readAsDataURL(file);
    return dfd.promise();
}
var getCanvasImage = function(image, options) {
    // define canvas
    var canvas = document.createElement("canvas");
    canvas.height = options.height;
    canvas.width = options.width;

    var ctx = canvas.getContext("2d");
	if (!options.crop) {
    	ctx.drawImage(image, 0, 0, image.width, image.height, 0, 0, canvas.width, canvas.height);
	} else {
		// h:196
		// w:196
		// x:0
		// x2:196
		// y:81
		// y2:277
		//console.log(parseInt(options.crop.x) +','+ parseInt(options.crop.y) +','+ parseInt(options.crop.w) +','+ parseInt(options.crop.h) +','+ 0 +','+ 0 +','+ 300 +','+ 300);
		ctx.drawImage(image, parseInt(options.crop.x), parseInt(options.crop.y), parseInt(options.crop.w), parseInt(options.crop.h), 0, 0, 300,300);
	}
    // fit image to canvas
    // convert canvas to jpeg url
    return canvas.toDataURL("image/jpeg");
}
var readFilePNG = function(file, options) {
    var dfd = new $.Deferred();
    
    // define FileReader object
    var reader = new FileReader();
    var that = this;

    // init reader onload event handlers
    reader.onload = function(e) {
        var image = $('<img/>')
            .load(function() {
                //when image fully loaded
                var newimageurl = getCanvasImagePNG(this, options);
                dfd.resolve(newimageurl, this);
            })
           .attr('src', e.target.result);
    };
    reader.onerror = function(e) {
        dfd.reject("Couldn't read file " + file.name);
    }
    // begin reader read operation
    reader.readAsDataURL(file);
    return dfd.promise();
}
var getCanvasImagePNG = function(image, options) {
    // define canvas
    var canvas = document.createElement("canvas");
    canvas.height = options.height;
    canvas.width = options.width;

    var ctx = canvas.getContext("2d");
    if (!options.crop) {
        ctx.drawImage(image, 0, 0, image.width, image.height, 0, 0, canvas.width, canvas.height);
    } else {
        // h:196
        // w:196
        // x:0
        // x2:196
        // y:81
        // y2:277
        //console.log(parseInt(options.crop.x) +','+ parseInt(options.crop.y) +','+ parseInt(options.crop.w) +','+ parseInt(options.crop.h) +','+ 0 +','+ 0 +','+ 300 +','+ 300);
        ctx.drawImage(image, parseInt(options.crop.x), parseInt(options.crop.y), parseInt(options.crop.w), parseInt(options.crop.h), 0, 0, 300,300);
    }
    // fit image to canvas
    // convert canvas to jpeg url
    return canvas.toDataURL("image/png");
}
var dataURItoBlob = function(dataURI) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs
    var byteString;
    if (dataURI.split(',')[0].indexOf("base64") >= 0) {
        byteString = atob(dataURI.split(',')[1]);
    } else {
        byteString = unescape(dataURI.split(',')[1]);
    }

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], {
        type: mimeString
    });
}
function compareWidthHeight(width, height) {
	var diff = [];
	if(width > height) {
		diff[0] = width - height;
		diff[1] = 0;
	} else {
		diff[0] = 0;
		diff[1] = height - width;
	}
	return diff;
}
function getUrlParameter(sParam) {
	sParam = sParam.toLowerCase();
	var sPageURL = window.location.search.substring(1).toLowerCase();
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) 
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) 
		{
			return sParameterName[1];
		}
	}
}
function format(number) {
	if(number < 10)
		return '0' + number;
	else
		return number;
}
function formatAddress(user) {
    var address = '';
    if(user.addressStreet != '')
        address += user.addressStreet + ', ';
    if(user.addressCity != '')
        address += user.addressCity + ', ';
    if(user.addressState != '')
        address += user.addressState + ', ';
    if(user.addressPin != '')
        address += user.addressPin + ', ';
    ret = address.replace(/,\s$/, '.');
    if(ret == '')
        return 'Not Specified';
    else
        return ret;
}
function percentage(number, total) {
	if(total == 0)
		return 0;
	return (parseInt(number)/parseInt(total)) * 100;
}
function timeSince(date) {

    var seconds = Math.floor((new Date() - date) / 1000);

    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + " yrs";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hours";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " min";
    }
    return "Just Now";
}
function calcDisplayTimeWords(totalDuration) {
	var hours = parseInt( totalDuration / 3600 ) % 24;
	var minutes = parseInt( totalDuration / 60 ) % 60;
	var seconds = totalDuration % 60;

	if(hours>0) {
		if(minutes>0 && minutes<=15) {
			return hours+".25h";
		} else if(minutes>15 && minutes<=30) {
			return hours+".5h";
		} else if(minutes>30 && minutes<=45) {
			return hours+".75h";
		} else {
			return hours+"h";
		}
	} else if(minutes>0) {
		if(seconds>0 && seconds<=15) {
			return minutes+".25m";
		} else if(seconds>15 && seconds<=30) {
			return minutes+".5m";
		} else if(seconds>30 && seconds<=45) {
			return minutes+".75m";
		} else {
			return minutes+"m";
		}
	} else {
		return seconds+" s";
	}
}
function displayThroughTimestamp(totalDuration) {
    var tillDate = new Date(parseInt(totalDuration));
    var tsd = tillDate.getDate() + ' ' + MONTH[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
    if(tillDate.getHours() < 10)
        tsd += '0' + tillDate.getHours();
    else
        tsd += tillDate.getHours();
    tsd += ':';
    if(tillDate.getMinutes() < 10)
        tsd += '0' + tillDate.getMinutes();
    else
        tsd += tillDate.getMinutes();
    return tsd;
}
function getTime12Hour (hours, minutes) {
	//var hours = date.getHours();
	//var minutes = date.getMinutes();
	var ampm = hours >= 12 ? 'pm' : 'am';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0'+minutes : minutes;
	var strTime = hours + ':' + minutes + '' + ampm;
	return strTime;
}

function emailValidate(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(regex.test(email))
		return true;
	return false;
}

function bookmarkThisPage(e) {
	var bookmarkURL = window.location.href;
    var bookmarkTitle = document.title;

    if ('addToHomescreen' in window && window.addToHomescreen.isCompatible) {
        // Mobile browsers
        addToHomescreen({ autostart: false, startDelay: 0 }).show(true);
    } else if (window.sidebar && window.sidebar.addPanel) {
        // Firefox version < 23
        window.sidebar.addPanel(bookmarkTitle, bookmarkURL, '');
    } else if ((window.sidebar && /Firefox/i.test(navigator.userAgent)) || (window.opera && window.print)) {
        // Firefox version >= 23 and Opera Hotlist
        $(this).attr({
        href: bookmarkURL,
        title: bookmarkTitle,
        rel: 'sidebar'
        }).off(e);
        return true;
    } else if (window.external && ('AddFavorite' in window.external)) {
        // IE Favorite
        window.external.AddFavorite(bookmarkURL, bookmarkTitle);
    } else {
        // Other browsers (mainly WebKit - Chrome/Safari)
        alert('Press ' + (/Mac/i.test(navigator.userAgent) ? 'Cmd' : 'Ctrl') + '+D to bookmark this page.');
    }
    return false;
}

/**
 * Returns a random number between min (inclusive) and max (exclusive)
 */
function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
//function to format time for 2 digits
function formatTime2Digits(time) {
    time = parseInt(time);
    if (time<10) {
        time = '0'+time;
    };
    return time;
}
//function to format time
//function formatTime(time,seconds=1) {
function formatTime(time,seconds) {
    if (!seconds) {
        seconds = 1;
    }
    var a = new Date(time * 1000);
    var year = a.getFullYear();
    var date = formatTime2Digits(a.getDate());
    var hour = formatTime2Digits(a.getHours());
    var min = formatTime2Digits(a.getMinutes());
    if (seconds==1) {
        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var month = months[a.getMonth()];
        var sec = formatTime2Digits(a.getSeconds());
        var formattedTime = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    } else {
        var month = ["January","February","March","April","May","June","July","August","September","October","November","December"];
        var month = month[a.getMonth()];
        var formattedTime = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min ;
    }
    return formattedTime;
}
//function to format date
//function formatDate(time,delimiter = ' ') {
function formatDate(time,delimiter) {
    if (!delimiter) {
        delimiter = ' ';
    }
    var a = new Date(time);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = formatTime2Digits(a.getDate());
    var formattedDate = date + delimiter + month + delimiter + year;
    return formattedDate;
}
function getToday() {
    return formatDate(Math.floor(Date.now()));
}
function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};
//function to convert given seconds into minute and seconds
function convertSecondsIntoMinutes(sec) {
    var min = 0;
    if(sec - 60 >= 0) {
        min = Math.floor(sec / 60);
        sec = sec - (min * 60);
    }
    var time = min + ' min ' + sec + ' sec';
    return time;
}
//function to format contact number
function formatContactNumber(data) {
    var mobile = (data.contactMobile != '')?(data.contactMobilePrefix + '-' + data.contactMobile):'';
    var landline = (data.contactLandline != '')?(data.contactLandlinePrefix + '-' + data.contactLandline):'';
    if(mobile != '' && landline != '') {
        return mobile + ', ' + landline;
    }
    else if(mobile == '') {
        return landline;
    }
    else
        return mobile;
}
function validateEmail($email) {
    var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!emailReg.test($email)) {
        return false;
    } else {
        return true;
    }
}
function validateMobile(mobile) {
    regex = /^(\d{10})$/;
    if (regex.test(mobile))
        return false;
    else
        return true;
}
function checkDuplicates(a) {
    //Check all values in the incoming array and eliminate any duplicates
    var r = new Array(); //Create a new array to be returned with unique values
    //Iterate through all values in the array passed to this function
    o:for (var i = 0, n = a.length; i < n; i++) {
        //Iterate through any values in the array to be returned
        for (var x = 0, y = r.length; x < y; x++) {
            //Compare the current value in the return array with the current value in the incoming array
            if (r[x] == a[i]) {
                //If they match, then the incoming array value is a duplicate and should be skipped
                return 'false';
                //continue o;
            }
        }
        //If the value hasn't already been added to the return array (not a duplicate) then add it
        r[r.length] = a[i];
    }
    //Return the reconstructed array of unique values
    //return r;
    return 'true';
}
function scrollToElem(scrollDiv,scrollTime){
    $('html, body').animate({
        scrollTop: $(scrollDiv).offset().top - 100
    }, scrollTime);
};
function findObjByValue(obj,matches,value) {
    for(var i = 0; i < obj.length; i++)
    {
        if(obj[i][matches] == value)
        {
        return obj[i];
        }
    }
}
function findKeyByValue(obj,value) {
    for(var i = 0; i < obj.length; i++)
    {
        if(obj[i].id == value)
        {
        return i;
        }
    }
}
function findKeyByKeyValue(obj,key,value) {
    for(var i = 0; i < obj.length; i++)
    {
        if(obj[i][key] == value)
        {
            return i;
        }
    }
}
function findRemoveObjByValue(param) {
    var obj = {};
    //set data
    obj = param;
    //augment the object with a remove function
    obj.remove = function(key, val) {
        var i = 0;
        //loop through data
        while (this[i]) {
            if (this[i][key] === val) {
                //if we have that data, splice it
                //splice changes the array length so we don't increment
                this.splice(i, 1);
            } else {
                //else move on to the next item
                i++;
            }
        }
        //be sure to return the object so that the chain continues
        return this;
    }
    //return object for operation
    return obj
}
function allowDecimal(event, obj) {
    if ((event.which != 46 || obj.val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) && event.which != 8) {
        return false;
    }
    var text = obj.val();
    if ((text.indexOf('.') != -1) && (text.substring(text.indexOf('.')).length > 2)) {
        if (event.which != 8)
            return false;
    }
    return true;
}
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function min(what, length) {
    if(what.val().length < length)
        return false;
    else
        return true;
}

function max(what, length) {
    if(what.val().length > length)
        return false;
    else
        return true;
}
function getExamStatus(examStatus) {
    if(examStatus==0)
        return 'Draft';
    else if (examStatus==1)
        return 'Not Live';
    else
        return 'Live';
}
function getSubjectiveExamStatus(examStatus) {
    if(examStatus==2)
        return 'Live';
    else
        return 'Not Live';
}
function round2(number) {
    return Math.round(number*100)/100;
}
function nl2br(str){
    return str.replace(/(?:\r\n|\r|\n)/g, '<br />');
}
function validateYoutubeUrl(url) {
    return url.match(/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11,})(?:\S+)?$/);
}
function validateVimeoUrl(url) {
    return url.match(/\/\/(?:www\.)?vimeo.com\/([0-9a-z\-_]+)/i);
}
function PrintElem(elem) {
    Popup(jQuery(elem).html());
}

function Popup(data) {
    var mywindow = window.open('', 'my div', 'height=400,width=600');
    mywindow.document.write('<html><head><title></title>');
    //mywindow.document.write('<link rel="stylesheet" href="'+sitepathManageIncludes+'assets/global/plugins/bootstrap/css/bootstrap.min.css" type="text/css" />');  
    mywindow.document.write('<style type="text/css">.list-unstyled { list-style:none; padding-left:10px; } </style></head><body>');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');
    mywindow.document.close();
    mywindow.print();                        
}
function keydownAlphaNumeric(e, m, v){
    //alert('Key code # ' + v.which);
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!v.charCode ? v.which : v.charCode);
    if (regex.test(str)) {
        return true;
    }

    if(v.which == 8 || v.which == 32 || v.which == 37 || v.which == 39 || v.which == 46) {
        return true;
    }

    v.preventDefault();
    return false;
}
function intToStatus(number) {
    number = parseInt(number);
    if (number == 0) {
        return "No";
    } else {
        return "Yes";
    }
}
function checkFileType(string) {
    if (string == null) {
        return "Not Set";
    } else if(string == "ppt") {
        return "Presentation";
    } else {
        return string;
    }
}