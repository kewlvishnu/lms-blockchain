<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- END SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->

        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <?php
                $contentIds=array();
                foreach ($content as $key => $section) {
            ?>
            <li class="nav-item <?php if($key == 0) echo 'start'; ?> <?php if($section['id'] == $chapterId) echo 'active open'; ?>">
                <a href="<?php echo $sitepathManage.'content/subjects/'.$subjectId.'/chapters/'.$section['id']; ?>" data-tree="0" data-chapterid="<?php echo $section['id']; ?>" class="nav-link nav-toggle">
                    <i class="icon-note"></i>
                    <span class="title"><?php echo $section['title']; ?></span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                <?php if (count($section['content'])>0) { ?>
                        <ul class="sub-menu">
                            <?php
                                foreach ($section['content'] as $key1 => $contentStuff) {
                                    $contentIds[] = $contentStuff['id'];
                                    switch ($contentStuff['contentType']) {
                                        case 'doc':$icon = '<i class="fa fa-file-word-o"></i>';break;
                                        case 'ppt':$icon = '<i class="fa fa-file-powerpoint-o"></i>';break;
                                        case 'text':$icon = '<i class="fa fa-file-text-o"></i>';break;
                                        case 'video':$icon = '<i class="fa fa-file-video-o"></i>';break;
                                        case 'dwn':$icon = '<i class="fa fa-download"></i>';break;
                                        case 'Youtube':$icon = '<i class="fa fa-youtube"></i>';break;
                                        case 'Vimeo':$icon = '<i class="fa fa-vimeo"></i>';break;
                                        case 'quiz':$icon = '<i class="fa fa-question-circle"></i>';break;
                                        case 'survey':$icon = '<i class="fa fa-list"></i>';break;
                                        default:$icon = '';break;
                                    }
                            ?>
                                <li class="nav-item <?php if($key1 == 0) echo 'start'; ?>">
                                    <a href="<?php echo $sitepathManage.'content/subjects/'.$subjectId.'/chapters/'.$section['id']; ?>" data-tree="1" data-downloadable="<?php echo $contentStuff['downloadable']; ?>" data-chapterid="<?php echo $section['id']; ?>" data-contentid="<?php echo $contentStuff['id']; ?>" class="nav-link" data-toggle="collapse" data-target=".in">
                                        <?php echo $icon; ?>
                                        <span class="title"><?php echo $contentStuff['title']; ?></span>
                                    </a>
                                </li>
                            <?php
                                }
                            ?>
                        </ul>
                <?php } ?>
                <!-- <ul class="sub-menu">
                    <li class="nav-item start active open">
                        <a href="index.html" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title">Dashboard 1</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="dashboard_2.html" class="nav-link ">
                            <i class="icon-bulb"></i>
                            <span class="title">Dashboard 2</span>
                            <span class="badge badge-success">1</span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="dashboard_3.html" class="nav-link ">
                            <i class="icon-graph"></i>
                            <span class="title">Dashboard 3</span>
                            <span class="badge badge-danger">5</span>
                        </a>
                    </li>
                </ul> -->
            </li>
            <?php
                }
            ?>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->