<?php
	//get the last-modified-date of this very file
	$lastModified=filemtime(__FILE__);
	//get a unique hash of this file (etag)
	$etagFile = md5_file(__FILE__);
	//get the HTTP_IF_MODIFIED_SINCE header if set
	$ifModifiedSince=(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
	//get the HTTP_IF_NONE_MATCH header if set (etag: unique file hash)
	$etagHeader=(isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

	//set last-modified header
	header("Last-Modified: ".gmdate("D, d M Y H:i:s", $lastModified)." GMT");
	//set etag-header
	header("Etag: $etagFile");
	//make sure caching is turned on
	header('Cache-Control: max-age=290304000, public');
	
	//check if page has changed. If not, send 304 and exit
	if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])==$lastModified || $etagHeader == $etagFile)
	{
	       header("HTTP/1.1 304 Not Modified");
	       exit;
	}

	//	url functions and routes
	require_once('../config/url.functions.php');
	$pageArr = explode('/', $pageUrl);
	$pageGet = explode('?', $pageUrl);
	
	//$page = "student";
	/*
	**	flagInvalid : true = valid urls, false = invalid urls
	*/
	$flagInvalid = false;
	/*
	**	flagSession :
	**	0 = neutral URLs which opens regardless of user login status
	**	1 = these urls open only for non logged in users
	**	2 = these urls open only for logged in users
	*/
	$flagSession = 0;
	$pageStudents = array('', 'index.php', 'packages', 'package', 'courses', 'course', 'signup', 'verify', 'reset-password', 'init-student', 'policies', 'instructor', 'institute', 'search', 'contact', 'about', 'signout', 'laterpaypal', 'laterpay', 'mail');
	$pageTeachers = array('signup', 'reset-password', 'about');
	$pageSitemaps = array('home', 'navigation', 'courses', 'packages', 'instructors', 'institutes');
	$pageParents  = array('reset-password', 'about');
	
	if ($pageArr[1] == 'lms') {
		$pageType = "lms";
		$page = "home";
	} elseif ($pageArr[1] == 'parents') {
		$pageType = "parent";
		$page = "home";
		if (isset($pageArr[2]) && !empty($pageArr[2])) {
			if ($pageArr[2] == "about") {
				$page = "about";
			} else if ($pageArr[2] == 'reset-password') {
				if ((isset($pageArr[3]) && !empty($pageArr[3])) && (isset($pageArr[4]) && !empty($pageArr[4]))) {
					$page	  = "resetPassword";
					$flagSession = 1;
					$slug 	  = $pageArr[3].'/'.$pageArr[4];
				} else {
					$flagInvalid = true;
				}
			} else {
				$flagInvalid = true;
			}
		}
	} elseif ($pageArr[1] == 'sitemap') {
		$pageType = "sitemap";
		$page = "home";
		if (isset($pageArr[2]) && !empty($pageArr[2])) {
			if (in_array($pageArr[2], $pageSitemaps)) {
				if ($pageArr[2] == "navigation") {
					$page = "navigation";
				} else if ($pageArr[2] == 'courses') {
					$page = "courses";
				} else if ($pageArr[2] == 'packages') {
					$page = "packages";
				} else if ($pageArr[2] == 'instructors') {
					$page = "instructors";
				} else if ($pageArr[2] == 'institutes') {
					$page = "institutes";
				}
			} else {
				$flagInvalid = true;
			}
		}
	} elseif ($pageArr[1] == 'teach') {
		$pageType = "teach";
		$page	  = "home";
		if (isset($pageArr[2]) && !empty($pageArr[2])) {
			if (in_array($pageArr[2], $pageTeachers)) {
				if ($pageArr[2] == "signup") {
					$page = "signup";
					$flagSession = 1;
					if (isset($pageArr[3]) && !empty($pageArr[3])) {
						$page 	  = "signupComplete";
						$flagSession = 1;
						if (isset($pageArr[4]) && !empty($pageArr[4])) {
							$slug 	  = $pageArr[4];
						} else {
							$flagInvalid = true;
						}
					}
				} else if ($pageArr[2] == 'reset-password') {
					if ((isset($pageArr[3]) && !empty($pageArr[3])) && (isset($pageArr[4]) && !empty($pageArr[4]))) {
						$page	  = "resetPassword";
						$flagSession = 1;
						$slug 	  = $pageArr[3].'/'.$pageArr[4];
					} else {
						$flagInvalid = true;
					}
				} else if ($pageArr[2] == 'about') {
					$page = "about";
				} else {
					$flagInvalid = true;
				}
			} else {
				$flagInvalid = true;
			}
		}
	} elseif (in_array($pageArr[1], $pageStudents)) {
		$pageType = "students";
		if ($pageArr[1] == '' || $pageArr[1] == 'index.php') {
			$page	  = "home";
			$flagSession = 1;
		} else if ($pageArr[1] == 'packages') {
			$page	  = "packages";
		} else if ($pageArr[1] == 'package') {
			$page	  = "package";
			if (isset($pageArr[2]) && !empty($pageArr[2])) {
				$slug	  = $pageArr[2];
			} else {
				$flagInvalid = true;
			}
		} else if ($pageArr[1] == 'courses') {
			$page	  = "courses";
			if (isset($pageArr[2]) && !empty($pageArr[2])) {
				$slug 	  = $pageArr[2];
			}
		} else if ($pageArr[1] == 'course') {
			$page	  = "course";
			if (isset($pageArr[2]) && !empty($pageArr[2])) {
				$slug	  = $pageArr[2];
			} else {
				$flagInvalid = true;
			}
		} else if ($pageArr[1] == 'signup') {
			$page	  = "signup";
			$flagSession = 1;
			if (isset($pageArr[2]) && !empty($pageArr[2])) {
				$page 	  = "signupComplete";
				$flagSession = 1;
				if (isset($pageArr[3]) && !empty($pageArr[3])) {
					$slug 	  = $pageArr[3];
				} else {
					$flagInvalid = true;
				}
			}
		} else if ($pageArr[1] == 'verify') {
			if ((isset($pageArr[2]) && !empty($pageArr[2])) && (isset($pageArr[3]) && !empty($pageArr[3]))) {
				$page	  = "verify";
				$flagSession = 1;
				$slug 	  = $pageArr[2].'/'.$pageArr[3];
			} else {
				$flagInvalid = true;
			}
		} else if ($pageArr[1] == 'reset-password') {
			if ((isset($pageArr[2]) && !empty($pageArr[2])) && (isset($pageArr[3]) && !empty($pageArr[3]))) {
				$page	  = "resetPassword";
				$flagSession = 1;
				$slug 	  = $pageArr[2].'/'.$pageArr[3];
			} else {
				$flagInvalid = true;
			}
		} else if ($pageArr[1] == 'init-student') {
			$page	  = "initStudent";
			$flagSession = 2;
		} else if ($pageArr[1] == 'policies') {
			if (isset($pageArr[2]) && !empty($pageArr[2])) {
				if ($pageArr[2] == "terms") {
					$page	  = "policiesTerms";
				} else if ($pageArr[2] == "privacy") {
					$page	  = "policiesPrivacy";
				} else {
					$flagInvalid = true;
				}
			} else {
				$flagInvalid = true;
			}
		} else if ($pageArr[1] == 'instructor') {
			$page = "instructor";
			if (isset($pageArr[2]) && !empty($pageArr[2])) {
				$slug = $pageArr[2];
			} else {
				//$flagInvalid = true;
			}
		} else if ($pageArr[1] == 'institute') {
			$page = "institute";
			if (isset($pageArr[2]) && !empty($pageArr[2])) {
				$slug = $pageArr[2];
			} else {
				//$flagInvalid = true;
			}
		} else if ($pageArr[1] == 'search') {
			$page = "search";
			if (isset($pageArr[2]) && !empty($pageArr[2])) {
				$slug = $pageArr[2];
			} else {
				header('location:'.$sitepathCourses);
			}
		} else if ($pageArr[1] == 'contact') {
			$page = "contact";
		} else if ($pageArr[1] == 'about') {
			$page = "about";
		} else if ($pageArr[1] == 'signout') {
			$page = "logout";
		} else if ($pageArr[1] == 'laterpaypal') {
			$page = "laterpaypal";
			if (isset($pageArr[2]) && !empty($pageArr[2]) && isset($pageArr[3]) && !empty($pageArr[3])) {
				$subPage = $pageArr[2];
				if (($subPage == 'course') || ($subPage == 'keys') || ($subPage == 'package')) {
					if (($pageArr[3] == 'success') || ($pageArr[3] == 'cancel')) {
						$slug = $pageArr[3];
					} else {
						$flagInvalid = true;
					}
				} else {
					$flagInvalid = true;
				}
			} else {
				$flagInvalid = true;
			}
		} else if ($pageArr[1] == 'laterpay') {
			$page = "laterpay";
			if (isset($pageArr[2]) && !empty($pageArr[2]) && isset($pageArr[3]) && !empty($pageArr[3])) {
				$subPage = $pageArr[2];
				if (($subPage == 'course') || ($subPage == 'keys') || ($subPage == 'package')) {
					if (($pageArr[3] == 'success') || ($pageArr[3] == 'fail') || ($pageArr[3] == 'cancel')) {
						$slug = $pageArr[3];
					} else {
						$flagInvalid = true;
					}
				} else {
					$flagInvalid = true;
				}
			} else {
				$flagInvalid = true;
			}
		} else if ($pageArr[1] == 'mail') {
			$page = 'mail';
		}
		/*if (isset($pageArr[2]) && !empty($pageArr[2])) {
			$flagInvalid = true;
		}*/
	} elseif ($pageArr[1] == 'coupon_claim') {
		$pageType = "blank";
		$page = "couponClaim";
		if (isset($pageArr[2]) && !empty($pageArr[2])) {
			$slug = $pageArr[2];
		} else {
			$flagInvalid = true;
		}
	} elseif ($pageArr[1] == 'fix') {
		$pageType = "blank";
		$page = "fix";
		/*if (isset($pageArr[2]) && !empty($pageArr[2])) {
			$slug = $pageArr[2];
		} else {
			$flagInvalid = true;
		}*/
	} elseif (in_array($pageGet[0], $pageStudents)) {
		/*
		*	search page query append redirection
		*	/search?q= ====> /search/?q=
		*/
		if ($pageGet[0] == "search") {
			$redirectUrl = 'location:'.$sitepathCourses;
			if (isset($pageGet[1]) && !empty($pageGet[1])) {
				$redirectUrl = 'location:'.$sitepathMarket.$pageGet[0].'/?'.$pageGet[1];
			}
			header($redirectUrl);
		} else {
			$pageType = "students";
			$page	  = "home";
		}
	} else {
		$flagInvalid = true;
	}
	if ($flagInvalid) {
		var_dump($pageArr);
		//header('location:'.$sitepath404);
	} else {
		@session_start();
		if (!$flagInvalid && ($flagSession == 1) && isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
			if ($pageType == "teach") {
				header('location:'.$sitepathMarket.'teach');
			} else {
				header('location:'.$sitepathCourses);
			}
			
		} else if (!$flagInvalid && ($flagSession == 2) && (!isset($_SESSION['userId']) || empty($_SESSION['userId']))) {
			if ($pageType == "teach") {
				header('location:'.$sitepathMarket);
			} else {
				header('location:'.$sitepathMarket);
			}
			
		} else {
			/**
			* header checks for metas, page specific validation and portfolio subdomain validations
			**/
			if ($pageType == "students") {
				require_once('app/inc/header.functions.php');
				require_once('app/inc/meta.functions.php');
				if ($page == "home") {
					require_once('app/html-template/student/home.html.php');
				} else {
					require_once('app/html-template/student/index.html.php');
				}
			} elseif ($pageType == "teach") {
				require_once('app/inc/header.functions.php');
				require_once('app/inc/meta.functions.php');
				if ($page == "home") {
					require_once('app/html-template/teach/home.html.php');
				} else {
					require_once('app/html-template/teach/index.html.php');
				}
			} elseif ($pageType == "sitemap") {
				if ($page == "home") {
					require_once('app/html-template/sitemap/home.xml.php');
				} else {
					require_once('app/html-template/sitemap/index.xml.php');
				}
			} elseif ($pageType == "parent") {
				require_once('app/inc/header.functions.php');
				require_once('app/inc/meta.functions.php');
				if ($page == "home") {
					require_once('app/html-template/parent/home.html.php');
				} else {
					require_once('app/html-template/parent/index.html.php');
				}
			} elseif ($pageType == "blank") {
				require_once('app/inc/header.functions.php');
				require_once('app/inc/meta.functions.php');
				//if ($page == "couponClaim") {
					require_once('app/html-template/blank/index.html.php');
				//}
			} elseif ($pageType == "lms") {
				require_once('app/html-template/lms/index.html.php');
			}
			require_once('../config/tracking.functions.php');
		}
	}
?>