<?php
	$global = new stdClass();
	//loading zend framework
	$libPath = "../api/Vendor";
	include_once $libPath . "/Zend/Loader/AutoloaderFactory.php";
	require_once $libPath . "/ArcaneMind/Api.php";
	Zend\Loader\AutoloaderFactory::factory(array(
			'Zend\Loader\StandardAutoloader' => array(
					'autoregister_zf' => true,
					'db' => 'Zend\Db\Sql'
			)
	));
	if ($page == 'logout') {
		unset($_SESSION["userId"]);
		unset($_SESSION["userRole"]);
		unset($_SESSION['username']);
		unset($_SESSION['temp']);
		session_destroy();
		setcookie('mtwebLogin', "", time() - 3600, '/');
		unset($_COOKIE['mtwebLogin']);
	}
	if(isset($_COOKIE["mtwebLogin"]) && !empty($_COOKIE["mtwebLogin"])) {
		require_once '../api/Vendor/ArcaneMind/User.php';
		$cookie = $_COOKIE["mtwebLogin"];
		$res = Api::checkUserRemembered($cookie);
		$_SESSION["userId"] = $res->user['id'];
		$_SESSION["userRole"] = $res->user['roleId'];
		$_SESSION["username"] = $res->user['email'];
		$_SESSION["temp"] = $res->user['temp'];
	}
	//getting meta data for seo and smo
	/*if(isset($_GET['courseId']) && !empty($_GET['courseId'])){
		require_once '../api/Vendor/ArcaneMind/Course.php';
		$c = new Course();
		$input = new stdClass();
		$input->courseId = $_GET['courseId'];
		$result = $c->getCourseForMeta($input);
		if($result->status == 1) {
			$title = $result->details['name'] . ' | ' . $result->details['subtitle'];
			$fbTitle = $title;
			$description = $result->details['description'];
			$image = $result->details['ogimage'];
		}
		else {
			$description = 'Integro.io is a Cloud Based Learning platform & Market place for Courses, where Instructors,Tutors,Trainers,Coaching Institutes, Schools and Colleges host high quality education content in the form of Videos & Documents and conduct online tests with detailed analytics';
			$image = 'https://dhzkq6drovqw5.cloudfront.net/fb-arcanemind.jpg';
		}
	}*/

	if(isset($_GET['pageDetailId']) && !empty($_GET['pageDetailId'])) {
		$actual_link  = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$temp_link    = explode("/", $actual_link);
		$sub_link = explode(".", $temp_link[2]);
		$final_link = str_replace($sub_link[0], 'www', $actual_link);
		$subdomain = trim($_GET['pageDetailId']);
		$res = Api::getPortfolioBySlug($subdomain);
		if($res->valid) {
			$portfolioDetails = $res;
			$portfolioName 	  = $portfolioDetails->portfolio['name'];
			$logoPath = $res->portfolio['img'];
			if(!empty($res->portfolio['favicon'])) {
				$favicon  = $res->portfolio['favicon'];
			}
		} else {
			header('location:'.$final_link);
		}
	}
	//$title = 'Integro | Online Courses | Entrance Exam Preparation';
	$description = 'Integro is a platform & marketplace for online courses & entrance exam preparation. We have the best instructors and institutes in the world, we ensure that the content and questions are of the highest quality.';
	$image = 'https://dhzkq6drovqw5.cloudfront.net/fb-arcanemind.jpg';

	// setting link for logo
	if ($pageType == "teach") {
		$logolink = $sitepathMarket.'teach/';
	} else {
		$logolink = $sitepathMarket;
	}
	if (isset($_SESSION["userId"]) && !empty($_SESSION["userId"])) {
		$logolink = $sitepathCourses;
	}
	if (isset($_SESSION["temp"])) {
		//var_dump($_SESSION);
		if ($_SESSION["temp"]==1 && $page != "initStudent" && $page != "policiesTerms" && $page != "policiesPrivacy") {
			header('location:'.$sitepathMarket.'init-student/');
		}
		if ($_SESSION["temp"]==0 && $page == "initStudent") {
			header('location:'.$sitepathCourses);
		}
	}

	if(isset($logoPath) && !empty($logoPath)) {
		$logoBlock = '<a class="navbar-brand" href="'.$logolink.'">
						<span class="logo-portfolio">
							<img src="'.$logoPath.'" alt="'.$portfolioName.'" class="img-logo" />'.$portfolioName.'
						</span>
					</a>';
	} else {
		$logoBlock = '<a class="navbar-brand" href="'.$logolink.'"><img src="'.$sitepath.'img/arcanemind-logo.png" width="200" height="53" class="img-responsive" alt="Integro"></a>';
	}

	if ($page == "signupComplete") {
		require_once '../api/Vendor/ArcaneMind/User.php';
		$u = new User();
		$input = new stdClass();
		$input->userid = $slug;
		$result = $u->verifyUserID($input);
		if ($result->verified) {
			if ($pageType == "student") {
				header('location:'.$sitepathCourses);
			} else {
				header('location:'.$sitepathMarket.'teach');
			}
		}
	}
	require_once '../api/Vendor/ArcaneMind/Course.php';
	$c = new Course();
	$res = $c->getCourseCategories();
	if ($res->status == 1) {
		$global->courseCategories = $res->courseCategories;
	}
	require_once '../api/Vendor/ArcaneMind/CryptoWallet.php';
	$w = new CryptoWallet();
	$data = new stdClass();
	$global->walletAddress = '';
	$global->walletApp = '';
	if (isset($_SESSION['userId'])) {
		$data->userId = $_SESSION['userId'];
		$data->userRole = $_SESSION['userRole'];
		$res = $w->getWalletAddress($data);
		$global->walletAddress = '';
		$global->walletApp = '';
		if ($res->status == 1) {
			if (isset($res->wallet) && !empty($res->wallet)) {
				$global->walletAddress = $res->wallet;
				$global->walletApp = (($res->app=='metamask')?'m':'p');
			}
		}
	}
	$res = $w->getIGROContractData($data);
	if ($res->status == 1) {
		$global->toAddress = $res->ownerAddress;
		$global->contractAddress = $res->contractAddress;
		$global->contractCode = $res->contractCode;
		$global->decimalPrecision = $res->decimalPrecision;
		$global->balanceDivisor = $res->balanceDivisor;
		$global->displayCurrency = $res->displayCurrency;
	}
?>