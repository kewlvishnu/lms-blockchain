<?php
	if ($pageType == "students") {
		switch ($page) {
			case 'home':
				$title	= "Home | Integro | Online Courses";
				$description = 'Integro is a platform & marketplace for online courses & entrance exam preparation. We have the best instructors and institutes in the world, we ensure that the content and questions are of the highest quality.';
				break;
			case 'packages':
				$title	= "Packages | Integro | Online Courses";
				$description = 'Integro provides various range of packages to help you get best deals of courses.';
				break;
			case 'package':
				$title	= "Package in $slug | Integro | Online Courses";
				require_once '../api/Vendor/ArcaneMind/StudentPackage.php';
				$sp = new StudentPackage();
				$input = new stdClass();
				$input->slug = $slug;
				$result = $sp->getPackageForMeta($input);
				if($result->status == 1) {
					$title = $result->details['packName']." | Integro";
					if (isset($result->details['packDescription']) && !empty($result->details['packDescription'])) {
						$description = $result->details['packDescription'];
					}
					$image = $result->details['ogimage'];
				}
				break;
			case 'courses':
				$title	= "Learn Courses Online | Integro";
				$description = 'Integro provides various range of courses.';
				if (isset($slug) && !empty($slug)) {
					foreach ($global->courseCategories as $key => $value) {
						if ($slug == $value['slug']) {
							$title	= "Learn $value[category] Courses Online | Integro";
							if (isset($value['description']) && !empty($value['description'])) {
								$description = $value['description'];
							}
						}
					}
				}
				break;
			case 'course':
				$title	= "Course in $slug | Integro | Online Courses";
				$courseTitle = "";
				require_once '../api/Vendor/ArcaneMind/Course.php';
				$c = new Course();
				$input = new stdClass();
				$input->slug = $slug;
				$result = $c->getCourseForMeta($input);
				if($result->status == 1) {
					$courseTitle = $result->details['name'];
					$title = "Course in ".$result->details['name']." | Integro";
					if (isset($result->details['description']) && !empty($result->details['description'])) {
						$description = $result->details['description'];
					}
					$image = $result->details['ogimage'];
				}
				break;
			case 'signup':
				$title	= "Sign Up for students | Integro | Online Courses";
				$description = 'Sign up as a student and take advantage of the wide variety of courses that we have created by people or organisation who are passionate.';
				break;
			case 'signupComplete':
				$title	= "Sign Up Complete for students | Integro | Online Courses";
				$description = 'Signup is completed. Checkout courses and start taking them.';
				break;
			case 'verify':
				$title	= "Verify Account | Integro | Online Courses";
				$description = 'Verify your integro account.';
				break;
			case 'resetPassword':
				$title	= "Reset Password | Integro | Online Courses";
				$description = 'Reset your password for integro student account.';
				break;
			case 'policiesTerms':
				$title	= "Terms and Conditions | Integro | Online Courses";
				$description = 'Terms and conditions for use of Integro.';
				break;
			case 'policiesPrivacy':
				$title	= "Privacy Policy | Integro | Online Courses";
				$description = 'Privacy Policy for Integro.';
				break;
			case 'instructor':
				$title	= "About Instructor | Integro | Online Courses";
				require_once '../api/Vendor/ArcaneMind/Professor.php';
				$p = new Professor();
				$input = new stdClass();
				$input->slug = $slug;
				$result = $p->getProfessorForMeta($input);
				if($result->status == 1) {
					$title = "Instructor ".$result->details['firstName']." ".$result->details['lastName']." | Integro";
					if (isset($result->details['description']) && !empty($result->details['description'])) {
						$description = $result->details['description'];
					}
					$image = $result->details['ogimage'];
				}
				break;
			case 'institute':
				$title	= "About Institute | Integro | Online Courses";
				require_once '../api/Vendor/ArcaneMind/Institute.php';
				$i = new Institute();
				$input = new stdClass();
				$input->slug = $slug;
				$result = $i->getInstituteForMeta($input);
				if($result->status == 1) {
					$title = "Instructor ".$result->details['name']." | Integro";
					if (isset($result->details['description']) && !empty($result->details['description'])) {
						$description = $result->details['description'];
					}
					$image = $result->details['ogimage'];
				}
				break;
			case 'search':
				$title	= "Search Results | Integro | Online Courses";
				$description = "Search results for integro. Integro is a platform & marketplace for online courses & entrance exam preparation.";
				break;
			case 'contact':
				$title	= "Contact | Integro | Online Courses";
				$description = "Contact details for integro. Integro is a platform & marketplace for online courses & entrance exam preparation.";
				break;
			case 'about':
				$title	= "About Integro | Online Courses";
				$description = "About integro. Integro is a platform & marketplace for online courses & entrance exam preparation.";
				break;
			case 'logout':
				$title	= "Signout | Integro | Online Courses";
				$description = "Signout integro. Integro is a platform & marketplace for online courses & entrance exam preparation.";
				break;
			
			default:
				$title = 'Integro | Online Courses | Entrance Exam Preparation';
				$description = "Integro is a platform & marketplace for online courses & entrance exam preparation.";
				break;
		}
	} else if ($pageType == "teach") {
		switch ($page) {
			case 'home':
				$title	= "Home | Instructors | Institutes | Integro | Online Courses";
				$description = 'Integro is a platform & marketplace for selling online courses & entrance exam preparation material. Get access to a large number of potential students to spread your knowledge.';
				break;
			case 'signup':
				$title	= "Signup for Instructors/Institutes | Integro | Online Courses";
				$description = 'Sign up as a instructor or instructor and get access to large number of interested students.';
				break;
			case 'signupComplete':
				$title	= "Signup Complete for Instructors/Institutes | Integro | Online Courses";
				$description = 'Signup is completed. Goto dashboard and start making courses. Integro is a platform & marketplace to sell online courses & entrance exam preparation material.';
				break;
			case 'resetPassword':
				$title	= "Reset Password | Integro | Online Courses";
				$description = 'Reset your password for integro instructor/institute account. Integro is a platform & marketplace to sell online courses & entrance exam preparation material.';
				break;
			case 'about':
				$title	= "About Integro | Instructors | Institutes | Online Courses";
				$description = "About integro. Integro is a platform & marketplace to sell online courses & entrance exam preparation material.";
				break;
			
			default:
				$title = 'Integro | Instructors | Institutes | Online Courses | Entrance Exam Preparation';
				$description = 'Integro is a platform & marketplace for selling online courses & entrance exam preparation material.';
				break;
		}
	} else if ($pageType == "parent") {
		switch ($page) {
			case 'home':
				$title	= "Home | Parents | Integro | Online Courses";
				$description = 'Integro is a platform where parents can keep a track on their students progress.';
				break;
			case 'resetPassword':
				$title	= "Reset Password | Integro | Online Courses";
				$description = 'Reset your password for integro parent account. Integro is a platform where parents can keep a track on their students progress.';
				break;
			case 'about':
				$title	= "About Integro | Parents | Online Courses";
				$description = "About integro. Integro is a platform & marketplace to sell online courses & entrance exam preparation material.";
				break;
			
			default:
				$title = 'Integro | Instructors | Institutes | Online Courses | Entrance Exam Preparation';
				$description = 'Integro is a platform & marketplace for selling online courses & entrance exam preparation material.';
				break;
		}
	} else if ($pageType == "blank") {
		$title = 'Integro | Instructors | Institutes | Online Courses | Entrance Exam Preparation';
		$description = 'Integro is a platform & marketplace for selling online courses & entrance exam preparation material.';
	}
	$fbTitle = $title;
?>