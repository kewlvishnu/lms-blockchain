<?php
	require_once('includes/header.php');
?>
<section class="section-window">
	<div class="container pr htfull-md">
		<div class="row htfull-md">
			<div class="col-sm-8 text-center htfull-md js-tagline">
				<h2 class="head-md"><p><i class="fa fa-graduation-cap"></i></p>Parents can login to monitor their students performance.</h2>
			</div>
			<div class="col-sm-4 login-signup htfull-md">
				<div id="parentSignin" data-user="parent" data-role="6" class="js-cover">
					<form class="form-horizontal pad20 frm-trans" method="post" action="">
						<h3 class="block-title">Parent Login</h3>
						<div class="form-group">
							<div class="help-block text-center"></div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" class="form-control trans-control" name="inputUsername" id="inputParentUsername" placeholder="Username">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="password" class="form-control trans-control" name="inputPassword" id="inputParentPassword" placeholder="Password">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-6">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="optionRemember" id="optionParentRemember"> Remember me
									</label>
								</div>
							</div>
							<div class="col-sm-6">
								<button type="submit" class="btn btn-lg btn-success pull-right js-btn-login">Sign In</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<a href="javascript:void(0)" class="nav-down scrolltodiv" data-target="courses"><i class="fa fa-chevron-down"></i></a>
	</div>
</section>
<section class="main-section" id="courses"><?php /*
	<div class="bg-color1">
		<div class="container">
			<div id="testimonialsSlider" class="owl-carousel color-grey1 pad30">
				<div class="text-center">
					<div class="row mb10">
						<div class="col-md-8 col-md-offset-2">
							<div class="pad30 bg-grey1 color-color1 pr text-justify"><i class="arrow-down-msg"></i> <sup><i class="fa fa-quote-left font-big"></i></sup> Integro is an awesome place for Instructors like me who are passionate about teaching. The best part that I like about them is that they have an Instructor friendly revenue system. <sup><i class="fa fa-quote-right font-big"></i></sup></div>
						</div>
					</div>
					<div class="avatar-cover">
						<img src="<?php echo $sitepath; ?>img/testimonials/mehul.jpg" alt="Mehul Popat">
					</div>
					<h3>Mehul Popat</h3>
					<h5><em>(Instructor  and Expert in JAVA Spring Framework)</em></h5>
				</div>
				<div class="text-center">
					<div class="row mb10">
						<div class="col-md-8 col-md-offset-2">
							<div class="pad30 bg-grey1 color-color1 pr text-justify"><i class="arrow-down-msg"></i> <sup><i class="fa fa-quote-left font-big"></i></sup> Very happy with the overall support  provided by Integro. Not only are they open to ideas and suggestions, but the interface to upload courses is super quick. <sup><i class="fa fa-quote-right font-big"></i></sup></div>
						</div>
					</div>
					<div class="avatar-cover">
						<img src="<?php echo $sitepath; ?>img/testimonials/sunil.jpg" alt="Sunil Gupta">
					</div>
					<h3>Sunil Gupta</h3>
					<h5><em>(Instructor and Expert in C,Java and MangoDB)</em></h5>
				</div>
				<div class="text-center">
					<div class="row mb10">
						<div class="col-md-8 col-md-offset-2">
							<div class="pad30 bg-grey1 color-color1 pr text-justify"><i class="arrow-down-msg"></i> <sup><i class="fa fa-quote-left font-big"></i></sup> The best part about being an Institute on Integro is that you can invite multiple Instructors &amp; Instructors and ask them to make Subjects and Courses, in their area of expertise. This way you can have experts from different fields all together. <sup><i class="fa fa-quote-right font-big"></i></sup></div>
						</div>
					</div>
					<div class="avatar-cover">
						<img src="<?php echo $sitepath; ?>img/testimonials/gate.jpg" alt="GateOnline Forum">
					</div>
					<h3>GateOnline Forum</h3>
					<h5><em>(Institute for GATE Entrance Exam Preparation)</em></h5>
				</div>
			</div>
		</div>
	</div>*/ ?>
	<?php require_once('includes/footer-html.php'); ?>
</section>
<?php require_once('includes/footer-includes.php'); ?>