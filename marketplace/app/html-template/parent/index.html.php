<?php
	require_once('includes/header.php');

	switch ($page) {
		case 'about':
			require_once('about.inc.php');
			break;
		
		case 'resetPassword':
			require_once('resetPassword.inc.php');
			break;
		
		default:
			echo "There was some error";
			break;
	}

	require_once('includes/footer-html.php');
	require_once('includes/footer-includes.php');
?>