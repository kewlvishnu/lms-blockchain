<?php
	require_once('includes/header.php');
?>
<section class="section-window">
	<div class="container pr htfull-md">
		<div class="row htfull-md">
			<div class="col-sm-8 text-center htfull-md">
				<!-- <h2 class="head-md"><p><i class="fa fa-group"></i></p>Learn Skills  online and  empower yourself.</h2>
				<a href="javascript:void(0)" class="btn btn-lg btn-color2">Start learning</a>
				<a href="javascript:void(0)" class="btn btn-lg btn-color1 scrolltodiv" data-target="courses">Browse courses</a> -->
				<div class="row">
					<div class="col-sm-12">
						<form class="form-horizontal pad-md frm-trans" role="search" action="<?php echo $sitepathMarket; ?>search/">
							<h2 class="mb20">Learn New Skills</h2>
							<div class="row mb20">
								<div class="col-sm-6">
									<p><b class="color-color1">50+</b> lectures every week</p>
								</div>
								<div class="col-sm-6">
									<p><b class="color-color1">500+</b> students a month</p>
								</div>
							</div>
							<div class="form-group mb20">
								<div class="col-sm-12">
									<div class="input-group">
										<input type="text" class="form-control trans-control input-xlg" id="searchBox" name="q" placeholder="Search Courses">
										<span class="input-group-btn">
											<button class="btn btn-trans btn-xlg" type="submit">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
							</div>
							<div>
								<ul class="list-inline category-menu">
									<li><a class="btn btn-trans mb5" href="<?php echo $sitepathCourses; ?>school-college-prep/">School &amp; College Prep</a></li>
									<li><a class="btn btn-trans mb5" href="<?php echo $sitepathCourses; ?>hobbies-skills/">Hobbies &amp; Skills</a></li>
									<li><a class="btn btn-trans mb5" href="<?php echo $sitepathCourses; ?>business-management/">Business &amp; Management</a></li>
									<li><a class="btn btn-trans mb5" href="<?php echo $sitepathCourses; ?>entrance-exams/">Entrance Exams</a></li>
									<li><a class="btn btn-trans mb5" href="<?php echo $sitepathCourses; ?>it-software/">IT &amp; Software</a></li>
									<li><a class="btn btn-trans mb5" href="<?php echo $sitepathCourses; ?>entrepreneurship/">Entrepreneurship</a></li>
									<li><a class="btn btn-trans mb5" href="<?php echo $sitepathCourses; ?>miscellaneous/">Miscellaneous</a></li>
								</ul>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-sm-4 login-signup htfull-md">
				<div id="studentSignin" data-user="student" data-role="4" class="js-cover">
					<!-- <div class="btn-group">
					<button class="btn btn-lg btn-primary js-facebook"><i class="fa fa-facebook-square"></i> Login</button><button class="btn btn-lg btn-danger1 js-google"><i class="fa fa-google-plus"></i> Login</button><button class="btn btn-lg btn-info js-linkedin"><i class="fa fa-linkedin"></i> Login</button>
					</div> -->
					<form class="form-horizontal pad20 frm-trans" method="post" action="">
						<h3 class="block-title">Student Login</h3>
						<div class="form-group">
							<div class="help-block text-center"></div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<a href="javascript:void(0)" class="btn btn-primary btn-block btn-facebook"><i class="fa fa-facebook"></i>Facebook Login</a>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" class="form-control trans-control" name="inputUsername" id="inputStudentUsername" placeholder="Email">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="password" class="form-control trans-control" name="inputPassword" id="inputStudentPassword" placeholder="Password">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="optionRemember" id="optionStudentRemember"> Remember me
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<button type="submit" class="btn btn-lg btn-success pull-right js-btn-login">Sign In</button>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6"><a href="javascript:void(0)" class="js-forgot">Forgot Password?</a></div>
							<div class="col-md-6"><a href="javascript:void(0)" class="pull-right js-new-user">New? Register</a></div>
						</div>
						<div class="form-group" id="profile_google"></div>
					</form>
				</div>
				<div id="studentForgot" data-user="student" data-role="4" class="js-cover hide">
					<!-- <div class="btn-group">
					<button class="btn btn-lg btn-primary js-facebook"><i class="fa fa-facebook-square"></i> Login</button><button class="btn btn-lg btn-danger1 js-google"><i class="fa fa-google-plus"></i> Login</button><button class="btn btn-lg btn-info js-linkedin"><i class="fa fa-linkedin"></i> Login</button>
					</div> -->
					<form class="form-horizontal pad20 frm-trans" method="post" action="">
						<h3 class="block-title">Forgot Password?</h3>
						<div class="form-group">
							<div class="help-block text-center"></div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" class="form-control trans-control" name="inputUsername" id="inputForgotUsername" placeholder="Email">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6"><a href="javascript:void(0)" class="login-link js-login-user">Already a user? Login</a></div>
							<div class="col-md-6">
								<button type="submit" class="btn btn-lg btn-success pull-right js-btn-reset">Reset Password</button>
							</div>
						</div>
					</form>
				</div>
				<div id="studentSignup" data-user="student" class="js-cover js-signup signup-form hide">
					<!-- <div class="btn-group">
					<button class="btn btn-lg btn-primary js-facebook"><i class="fa fa-facebook-square"></i> Login</button><button class="btn btn-lg btn-danger1 js-google"><i class="fa fa-google-plus"></i> Login</button><button class="btn btn-lg btn-info js-linkedin"><i class="fa fa-linkedin"></i> Login</button>
					</div> -->
					<form class="form-horizontal pad20 frm-trans" method="post" action="">
						<h3 class="block-title">Join Us Today</h3>
						<div class="form-group">
							<div class="help-block text-center"></div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<a href="javascript:void(0)" class="btn btn-primary btn-block btn-facebook"><i class="fa fa-facebook"></i>Facebook Signup</a>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-6">
								<input type="text" class="form-control trans-control" name="inputFirstName" id="inputFirstName" placeholder="First Name">
							</div>
							<div class="col-sm-6">
								<input type="text" class="form-control trans-control" name="inputLastName" id="inputLastName" placeholder="Last Name">
							</div>
						</div>
						<div class="form-group has-feedback">
							<div class="col-sm-12">
								<input type="email" class="form-control trans-control" name="inputStudentSignupEmail" id="inputStudentSignupEmail" placeholder="Email" aria-describedby="inputSuccess1Status">
								<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
								<span id="inputSuccess1Status" class="sr-only">(success)</span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="password" class="form-control trans-control" name="inputStudentSignupPassword" id="inputStudentSignupPassword" placeholder="Password">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12 agreement">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="optionAgree" id="optionAgree"> I agree Integro's <a href="<?php echo $sitepathPolicyTerms; ?>">Terms of Use</a> and <a href="<?php echo $sitepathPolicyPrivacy; ?>">Privacy Policy</a>.
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6"><a href="javascript:void(0)" class="login-link js-login-user">Already a user? Login</a></div>
							<div class="col-md-6">
								<button type="submit" class="btn btn-lg btn-success pull-right" id="btnStudentSignup">Sign Up</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<a href="javascript:void(0)" class="nav-down scrolltodiv" data-target="courses"><i class="fa fa-chevron-down"></i></a>
	</div>
</section>
<section class="main-section" id="courses">
	<div class="container">
		<h2 class="text-center">Featured Packages</h2>
		<div class="row package-container">
			<div class="col-md-6">
				<a href="<?php echo $sitepathPackageDetail ;?>annual-subscription-membership" class="package doubleheight" style="background-image: url('../img/packages/all-courses.jpg');" >
					<span class="package-inner">
						<span class="icon-holder"><i class="fa fa-book"></i></span>
						<h3>All courses (yearly subscription)</h3>
					</span>
				</a>
			</div>
			<div class="col-md-6">
				<a href="<?php echo $sitepathPackageDetail ;?>finance-package" class="package" style="background-image: url('../img/packages/finance.jpg')">
					<span class="package-inner">
						<span class="icon-holder"><i class="fa fa-usd"></i></span>
						<h3>Finance courses</h3>
					</span>
				</a>
				<a href="<?php echo $sitepathPackages; ?>" class="package" style="background-image: url('../img/packages/browse-courses.jpg')">
					<span class="package-inner">
						<span class="icon-holder"><i class="fa fa-search"></i></span>
						<h3>Browse all packages</h3>
					</span>
				</a>
			</div>
		</div>
		<h2 class="text-center">Trending Courses</h2>
		<div class="row" id="trendingCourses">
			<!-- <div class="col-md-3 col-sm-6 col-xs-12">
				<div class="course-cover">
					<div class="course-img">
						<a href="javascript:void(0)"><img src="https://dujk9xa5fr1wz.cloudfront.net/course/240x135/258316_55e9_4.jpg" class="btn-block" alt=""></a>
					</div>
					<div class="course-dl">
						<div class="title bb-grey-3 pad10 ellipsis-2lines">
							<a href="javascript:void(0)">Learn To Code by Making Games - The Complete Unity Developer</a>
						</div>
						<div class="bb-grey-3 pad10">
							<div class="row">
								<span class="col-xs-5"><i class="fa fa-rupee"></i> 800</span>
								<span class="col-xs-7 text-right">
									<span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background fa fa-star-o" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; width: 0%;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background fa fa-star-o" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background fa fa-star-o" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background fa fa-star-o" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background fa fa-star-o" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; width: 0px;"><span class="fa fa-star gold"></span></div></div></span>
								</span>
							</div>
						</div>
						<div class="user">
							<span class="avatar">
								<a href="javascript:void(0)"><img src="https://dujk9xa5fr1wz.cloudfront.net/user/50x50/4387876_78bc.jpg" alt=""></a>
							</span>
							<span class="username">
								<a href="javascript:void(0)">PracticeGuru</a>
							</span>
						</div>
					</div>
				</div>
			</div> -->
		</div>
		<h2 class="text-center">Featured Courses</h2>
		<div class="row" id="featuredCourses">
			<!-- <div class="col-md-3 col-sm-6 col-xs-12">
				<div class="course-cover">
					<div class="course-img">
						<a href="javascript:void(0)"><img src="https://dujk9xa5fr1wz.cloudfront.net/course/240x135/258316_55e9_4.jpg" class="btn-block" alt=""></a>
					</div>
					<div class="course-dl">
						<div class="title bb-grey-3 pad10 ellipsis-2lines">
							<a href="javascript:void(0)">Learn To Code by Making Games - The Complete Unity Developer</a>
						</div>
						<div class="bb-grey-3 pad10">
							<div class="row">
								<span class="col-xs-5"><i class="fa fa-rupee"></i> 800</span>
								<span class="col-xs-7 text-right">
									<span><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background fa fa-star-o" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; width: 0%;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background fa fa-star-o" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background fa fa-star-o" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background fa fa-star-o" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; width: 0px;"><span></span></div></div><div class="rating-symbol" style="display: inline-block; position: relative;"><div class="rating-symbol-background fa fa-star-o" style="visibility: visible;"></div><div class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; width: 0px;"><span class="fa fa-star gold"></span></div></div></span>
								</span>
							</div>
						</div>
						<div class="user">
							<span class="avatar">
								<img src="https://dujk9xa5fr1wz.cloudfront.net/user/50x50/4387876_78bc.jpg" alt="">
							</span>
							<span class="username">
								<a href="javascript:void(0)">PracticeGuru</a>
							</span>
						</div>
					</div>
				</div>
			</div> -->
		</div>
		<div class="text-center pad30">
			<a href="<?php echo $sitepathCourses ?>" class="btn btn-lg btn-w50 btn-trans-color1">Browse All Courses</a>
		</div>
	</div><?php /*
	<div class="bg-color1">
		<div class="container">
			<div id="testimonialsSlider" class="owl-carousel color-grey1 pad30">
				<div class="text-center">
					<div class="row mb10">
						<div class="col-md-8 col-md-offset-2">
							<div class="pad30 bg-grey1 color-color1 pr text-justify"><i class="arrow-down-msg"></i> <sup><i class="fa fa-quote-left font-big"></i></sup> Integro ensures that the courses present on the marketplace are the best in terms of course quality. This ensures that students like me learn very quickly. <sup><i class="fa fa-quote-right font-big"></i></sup></div>
						</div>
					</div>
					<div class="avatar-cover">
						<img src="<?php echo $sitepath; ?>img/testimonials/apoorva.jpg" alt="Apoorva Jain">
					</div>
					<h3>Apoorva Jain</h3>
					<h5><em>(Sales Manager at Paytm)</em></h5>
				</div>
				<div class="text-center">
					<div class="row mb10">
						<div class="col-md-8 col-md-offset-2">
							<div class="pad30 bg-grey1 color-color1 pr text-justify"><i class="arrow-down-msg"></i> <sup><i class="fa fa-quote-left font-big"></i></sup> The part that I like about Integro is that the Instructors and Institutes that make the courses are all experts in their fields. This ensures that all the best practices &amp; tricks that they have learned are incorporated in their courses. <sup><i class="fa fa-quote-right font-big"></i></sup></div>
						</div>
					</div>
					<div class="avatar-cover">
						<img src="<?php echo $sitepath; ?>img/testimonials/ishan.jpg" alt="Ishan Gurg">
					</div>
					<h3>Ishan Gurg</h3>
					<h5><em>(MBA Student at IIM Indore)</em></h5>
				</div>
			</div>
		</div>
	</div>*/ ?>
	<?php require_once('includes/footer-html.php'); ?>
</section>
<?php require_once('includes/footer-includes.php'); ?>