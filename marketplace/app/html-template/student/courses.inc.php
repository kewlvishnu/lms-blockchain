<section class="main-body">
	<article class="category-head">
		<div class="container">
			<h1 class="page-title" id="categoryTitle"></h1>
			<p id="categoryDesc"></p>
		</div>
	</article>
	<article class="container">
		<div class="row" id="featuredCourses"></div>
	</article>
</section>