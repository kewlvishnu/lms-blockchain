<section class="main-body">
	<article class="container">
		<div class="col-md-6 col-md-offset-3">
			<form class="form-3d signup" id="studentSignup">
				<div class="text-center">
					<h3 class="form-title">Account verification</h3>
				</div>
				<div class="form-group">
					<div class="help-block text-center" id="verifyError"></div>
				</div>
				<hr class="style-two">
				<div class="mb20 text-center js-verify-more">
					<a class="btn btn-primary btn-lg mb10 btn-block resend-link" href="javascript:void(0)">Resend Verification Link</a>
					<p>or</p>
					<a class="btn btn-primary btn-lg btn-block resend-link" href="<?php echo $sitepathCourses; ?>">Check Courses</a>
				</div>
			</form>
		</div>
	</article>
</section>