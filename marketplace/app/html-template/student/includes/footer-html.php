<!--[footer start]-->
<footer class="footer color-grey1">
	<div class="grey-5 pad10">
		<div class="container">
			<div class="row">
				<div class="col-xs-6">
					<aside class="footer-nav-menu">
						<h4 class="text-uppercase mb20">About</h4>
						<div>
							<ul class="list-unstyled">
								<li class="pr mb10"><a href="<?php echo $sitepathAbout; ?>">About Us</a></li>
								<li class="pr mb10"><a href="<?php echo $sitepathContact; ?>">Contact Us</a></li>
							</ul>
						</div>
					</aside>
				</div>
				<div class="col-xs-6">
					<aside class="footer-nav-menu">
						<h4 class="text-uppercase mb20">Terms</h4>
						<div>
							<ul class="list-unstyled">
								<li class="pr mb10"><a href="<?php echo $sitepathPolicyTerms; ?>">Terms Of Use</a></li>
								<li class="pr mb10"><a href="<?php echo $sitepathPolicyPrivacy; ?>">Privacy Policy</a></li>
							</ul>
						</div>
					</aside>
				</div>
			</div>
		</div>
	</div>
	<div class="bt-grey-6 pad10">
		<div class="container">
			<div class="social-links pull-left"><?php /*
				<a target="_blank" class="btn btn-inverse" href="https://www.facebook.com/ArcanemindLive" title="Facebook" data-toggle="tooltip"><span class="fa fa-facebook"></span></a>*/ ?>
				<a target="_blank" class="btn btn-inverse" href="https://twitter.com/integro_io" title="Twitter" data-toggle="tooltip"><span class="fa fa-twitter"></span></a>
				<a target="_blank" class="btn btn-inverse" href="https://www.linkedin.com/company/integro-io/" title="LinkedIn" data-toggle="tooltip"><span class="fa fa-linkedin"></span></a><?php /*
				<a target="_blank" class="btn btn-inverse" href="https://plus.google.com/106976407713111494899" title="Google Plus" data-toggle="tooltip"><span class="fa fa-google-plus"></span></a>*/ ?>
			</div>
			<div class="copyright pull-right">&copy; 2016 <a target="_blank" href="<?php echo $sitepath; ?>" title="Integro">Integro.io, Inc.</a>. All Rights Reserved.</div>
		</div>
	</div>
    <!-- DATA HOLDERS START -->
    <input type="hidden" id="contractCode" value="<?php echo $global->contractCode; ?>">
    <input type="hidden" id="contractAddress" value="<?php echo $global->contractAddress; ?>">
    <input type="hidden" id="ownerAddress" value="<?php echo $global->toAddress; ?>">
    <!-- DATA HOLDERS END -->
</footer>