<?php
	require_once('app/html-template/student/modals/metamaskAssociateAddress.modal.php');
	require_once('app/html-template/student/modals/metamaskChangeAddress.modal.php');
	require_once('app/html-template/student/modals/metamaskNotInstalled.modal.php');
	require_once('app/html-template/student/modals/portisAssociateAddress.modal.php');
    require_once('app/html-template/student/modals/setAddress.modal.php');
	// modals
	if ($page != 'home' && (!isset($_SESSION['userId']) || empty($_SESSION['userId']))) {
		require_once('app/html-template/student/modals/login.html.php');
		require_once('app/html-template/student/modals/forgotpassword.html.php');
	}
	if (isset($page) && !empty($page)) {
		switch ($page) {
			case 'home':
				require_once('app/html-template/student/modals/emailModal.html.php');
				break;
			case 'package':
				require_once('app/html-template/student/modals/courseDetail.html.php');
				require_once('app/html-template/student/modals/videoModal.html.php');
				require_once('app/html-template/student/modals/purchaseModal.html.php');
				require_once('app/html-template/student/modals/refundModal.html.php');
				break;
			case 'course':
				require_once('app/html-template/student/modals/videoModal.html.php');
				require_once('app/html-template/student/modals/purchaseModal.html.php');
				require_once('app/html-template/student/modals/refundModal.html.php');
				break;
			default:break;
		}
	}
?>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
		<script src="app/js/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="app/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
		<script src="https://cdn.jsdelivr.net/gh/ethereum/web3.js/dist/web3.min.js"></script>
		<!-- <script src="https://cdn.jsdelivr.net/npm/portis/dist/bundle.min.js"></script> -->
        <script src="https://apis.google.com/js/platform.js" async defer></script>
<?php
	require_once('app/jsphp/global.js.php');
    require_once('app/jsphp/wallet/web3ops.js.php');
	if (isset($page) && !empty($page)) {
		if ($page != 'home' && (!isset($_SESSION['userId']) || empty($_SESSION['userId']))) {
			require_once('app/jsphp/login.js.php');
		}
		//require_once('app/jsphp/min/login.js.php');
		switch ($page) {
			case 'home':
				echo '<script src="../js/owl.carousel.min.js" type="text/javascript"></script>
					<script src="app/js/bootstrap-rating/bootstrap-ratings.min.js" type="text/javascript"></script>
					<script src="app/js/hello.polyfill.js"></script>
					<script src="app/js/hello.min.js"></script>
					<script src="app/js/modules/facebook.js"></script>';
				require_once('app/jsphp/homeStudent.js.php');
				require_once('app/jsphp/min/courses.js.php');
				break;
			case 'packages':
				echo '<script src="app/js/grid-loading/masonry.pkgd.min.js"></script>
					<script src="app/js/grid-loading/imagesloaded.js"></script>
					<script src="app/js/grid-loading/classie.js"></script>
					<script src="app/js/grid-loading/AnimOnScroll.js"></script>';
				require_once('app/jsphp/min/packages.js.php');
				break;
			case 'package':
				echo '<script src="app/js/bootstrap-rating/bootstrap-ratings.min.js" type="text/javascript"></script>
					<script src="app/js/mediaelement-and-player.min.js"></script>';
				require_once('app/jsphp/package.js.php');
				break;
			case 'courses':
				echo '<script src="app/js/bootstrap-rating/bootstrap-ratings.min.js" type="text/javascript"></script>';
				require_once('app/jsphp/min/courses.js.php');
				break;
			case 'course':
				echo '<script src="app/js/bootstrap-rating/bootstrap-ratings.min.js" type="text/javascript"></script>
					<script src="app/js/mediaelement-and-player.min.js"></script>';
				require_once('app/jsphp/course.js.php');
				break;
			case 'signup':
				require_once('app/jsphp/min/signupStudent.js.php');
				break;
			case 'verify':
				require_once('app/jsphp/min/verify.js.php');
				require_once('app/jsphp/min/resetPassword.js.php');
				break;
			case 'resetPassword':
				require_once('app/jsphp/min/resetPassword.js.php');
				break;
			case 'initStudent':
				require_once('app/jsphp/initStudent.js.php');
				break;
			case 'instructor':
				echo '<script src="app/js/grid-loading/masonry.pkgd.min.js"></script>
					<script src="app/js/grid-loading/imagesloaded.js"></script>
					<script src="app/js/grid-loading/classie.js"></script>
					<script src="app/js/grid-loading/AnimOnScroll.js"></script>';
				echo '<script src="app/js/bootstrap-rating/bootstrap-ratings.min.js" type="text/javascript"></script>';
				require_once('app/jsphp/min/courses.js.php');
				require_once('app/jsphp/min/packages.js.php');
				require_once('app/jsphp/min/instructor.js.php');
				break;
			case 'institute':
				echo '<script src="app/js/grid-loading/masonry.pkgd.min.js"></script>
					<script src="app/js/grid-loading/imagesloaded.js"></script>
					<script src="app/js/grid-loading/classie.js"></script>
					<script src="app/js/grid-loading/AnimOnScroll.js"></script>';
				echo '<script src="app/js/bootstrap-rating/bootstrap-ratings.min.js" type="text/javascript"></script>';
				require_once('app/jsphp/min/courses.js.php');
				require_once('app/jsphp/min/packages.js.php');
				require_once('app/jsphp/min/institute.js.php');
				break;
			case 'search':
				echo '<script src="app/js/bootstrap-rating/bootstrap-ratings.min.js" type="text/javascript"></script>';
				require_once('app/jsphp/min/search.js.php');
				break;
			case 'contact':
				require_once('app/jsphp/min/contact.js.php');
				break;
			case 'about':
				echo '<script src="../js/owl.carousel.min.js" type="text/javascript"></script>
					<script src="app/js/bootstrap-rating/bootstrap-ratings.min.js" type="text/javascript"></script>';
				require_once('app/jsphp/min/about.js.php');
				break;
			case 'logout':
				require_once('app/jsphp/min/logout.js.php');
				break;
			case 'laterpaypal':
				//require_once('app/jsphp/laterpaypal.js.php');
				break;
			default:break;
		}
	}
	@include_once "analytics.php";
?>
<?php
    if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){ ?>
		<!--Start of Tawk.to Script-->
		<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/58f86bb530ab263079b60876/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
		</script>
		<!--End of Tawk.to Script--><?php
    } ?>
	</body>
</html>