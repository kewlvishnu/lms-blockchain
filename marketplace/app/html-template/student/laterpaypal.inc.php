<?php
	$status			= '';
	if ($slug == "success") {
		$status = '';
		if ($subPage == 'course') {
			require_once('api/Vendor/Arcanemind/Course.php');
			$product	= new Course();
		} elseif ($subPage == 'keys') {
			require_once('api/Vendor/Arcanemind/Coursekeys.php');
			$product	= new Coursekeys();
		} elseif ($subPage == 'package') {
			require_once('api/Vendor/Arcanemind/StudentPackage.php');
			$product	= new StudentPackage();
		}
		$data			= new stdClass();
		$data->userId	= $_SESSION['userId'];
		$data->userRole	= $_SESSION['userRole'];
		$data->paypalId	= $_POST['txn_id'];
		$data->orderId	= $_POST['item_number'];
		$data->amount	= $_POST['payment_gross'];
		$data->iteminfo	= $_POST['item_name'];
		$data->payment_Status = $_POST['payment_status'];
		$result			= $product->paypalReturn($data);
	}
	if (empty($_POST) && empty($slug)) {
		header('location:'.$sitepath404);
	}
	if ($_SESSION['userRole'] != '4') {
		if ($_SESSION['userRole'] == '1') {
			$link = $sitepathManage;
		} else {
			$link = $sitePath.'admin/CourseKey.php';
		}
	} else {
		$link = $sitepathStudent;
	}
?>
<div class="main-body min-ht-800 mb20">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="pad20 bg-grey1">
					<table class="table table-bordered table-striped">
						<?php
							if ($slug == "success") {
						?>
						<tr class="success">
							<th colspan="2" class="text-center">Payment Successful</th>
						</tr>
						<tr>
							<th colspan="2" class="text-center">Order Details</th>
						</tr>
						<tr>
							<th class="text-right" width="50%">Order Date</th>
							<td><?php echo $result->date; ?></td>
						</tr>
						<tr>
							<th class="text-right" width="50%">Order ID</th>
							<td><?php echo $_POST['item_number']; ?></td>
						</tr>
						<tr>
							<th class="text-right" width="50%">Product Name</th>
							<td><?php echo $_POST['item_name']; ?></td>
						</tr>
						<tr>
							<th class="text-right" width="50%">Product Type</th>
							<td><?php echo ucfirst($subPage); ?></td>
						</tr>
						<?php
								if ($subPage == 'keys') {
						?>
						<tr>
							<th class="text-right" width="50%">Quantity</th>
							<td><?php echo $result->purchase_detail; ?></td>
						</tr>
						<tr>
							<th class="text-right" width="50%">Rate</th>
							<td><i class="fa fa-dollar"></i><?php echo $result->rate; ?></td>
						</tr>
						<?php
								}
						?>
						<tr>
							<th class="text-right" width="50%">Total</th>
							<td><i class="fa fa-dollar"></i><?php echo $result->amount; ?></td>
						</tr>
						<?php
							} else {
						?>
						<tr class="danger">
							<th class="text-center">Payment Cancelled</th>
						</tr>
						<?php
							}
						?>
					</table>
					<div class="text-center">
						<p>Goto <a href="<?php echo $link; ?>">Dashboard</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>