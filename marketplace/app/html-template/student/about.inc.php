<div class="main-body">
	<div class="bg-featured section-featured">
		<h1 class="featured-title bg-trans-bg text-uppercase text-center">We are empowering learning</h1>
	</div>
	<div class="pad-section text-center bg-grey1">
		<div class="container">
			<h2 class="mb20">Welcome To Integro</h2>
			<p>Integro.io is a Cloud Based Learning platform &amp; Market place, where Coaching institutes, Trainers, Schools and Colleges host education content &amp; conduct online tests with detailed analytics.</p>
			<p>The founders are a bunch of Mckinsey &amp; Harvard Alumni, who are really passionate about e-learning and how it can help Students/anyone with a zest for learning perform better in their career.</p>
		</div>
	</div>
	<div class="pad-section text-center">
		<div class="container">
			<h2>What do we offer?</h2>
			<div class="pad-section">
				<div class="row mb20">
					<div class="col-md-6">
						<div class="row">
							<div class="col-sm-4">
								<div class="pad10 icon-round bg-color1 color-grey1">
									<i class="fa fa-book"></i>
								</div>
							</div>
							<div class="col-sm-8 text-left">
								<p class="lead">Study Courses</p>
								<p><em>Study courses which consist of online Tests and Education Content like Pre recorded Videos, documents and Presentations on our cloud based platform.</em></p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-sm-4">
								<div class="pad10 icon-round bg-color1 color-grey1">
									<i class="fa fa-file-text-o"></i>
								</div>
							</div>
							<div class="col-sm-8 text-left">
								<p class="lead">See detailed Assessment</p>
								<p><em>Get detailed assessment of your performance across examinations.</em></p>
							</div>
						</div>
					</div>
				</div>
				<div class="row mb20">
					<div class="col-md-6">
						<div class="row">
							<div class="col-sm-4">
								<div class="pad10 icon-round bg-color1 color-grey1">
									<i class="fa fa-graduation-cap"></i>
								</div>
							</div>
							<div class="col-sm-8 text-left">
								<p class="lead">Improve Performance</p>
								<p><em>Significantly improve your performance by practicing hard and learning by mistakes.</em></p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-sm-4">
								<div class="pad10 icon-round bg-color1 color-grey1">
									<i class="fa fa-check"></i>
								</div>
							</div>
							<div class="col-sm-8 text-left">
								<p class="lead">Learn and Study as per Convenience</p>
								<p><em>Study and learn as per your own convenient timing and place.</em></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><?php /*
	<div class="bg-color1">
		<div class="container">
			<div id="testimonialsSlider" class="owl-carousel color-grey1 pad30">
				<div class="text-center">
					<div class="row mb10">
						<div class="col-md-8 col-md-offset-2">
							<div class="pad30 bg-grey1 color-color1 pr text-justify"><i class="arrow-down-msg"></i> <sup><i class="fa fa-quote-left font-big"></i></sup> Integro ensures that the courses present on the marketplace are the best in terms of course quality. This ensures that students like me learn very quickly. <sup><i class="fa fa-quote-right font-big"></i></sup></div>
						</div>
					</div>
					<div class="avatar-cover">
						<img src="<?php echo $sitepath; ?>img/testimonials/apoorva.jpg" alt="Apoorva Jain">
					</div>
					<h3>Apoorva Jain</h3>
					<h5><em>(Sales Manager at Paytm)</em></h5>
				</div>
				<div class="text-center">
					<div class="row mb10">
						<div class="col-md-8 col-md-offset-2">
							<div class="pad30 bg-grey1 color-color1 pr text-justify"><i class="arrow-down-msg"></i> <sup><i class="fa fa-quote-left font-big"></i></sup> The part that I like about Integro is that the Instructors and Institutes that make the courses are all experts in their fields. This ensures that all the best practices &amp; tricks that they have learned are incorporated in their courses. <sup><i class="fa fa-quote-right font-big"></i></sup></div>
						</div>
					</div>
					<div class="avatar-cover">
						<img src="<?php echo $sitepath; ?>img/testimonials/ishan.jpg" alt="Ishan Gurg">
					</div>
					<h3>Ishan Gurg</h3>
					<h5><em>(MBA Student at IIM Indore)</em></h5>
				</div>
			</div>
		</div>
	</div>*/ ?>
	<div class="text-center bg-color2 color-grey1">
		<div class="pad10">
			<p class="clear-mrg">We will love to hear from you. Reach out to us at <i class="fa fa-envelope"></i> contactus@integro.io or <i class="fa fa-whatsapp"></i> +91-9741436024</p>
		</div>
	</div>
</div>