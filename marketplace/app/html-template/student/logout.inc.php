<div class="main-body min-ht-800 mb20">
	<div class="container">
		<div class="pad-section bg-grey1">
			<div class="mb20">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<img src="<?php echo $sitepathMarket; ?>app/img/mono-logout.png" class="btn-block" alt="">
					</div>
				</div>
			</div>
			<div class="pad-section">
				<p class="text-center">You are successfully logged out, redirecting you in <span id="timer">5</span> seconds...</p>
			</div>
		</div>
	</div>
</div>