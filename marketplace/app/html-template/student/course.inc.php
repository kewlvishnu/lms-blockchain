<div class="affix-spy-menu" id="jsCourseMenu">
	<nav id="course-navbar" class="navbar navbar-default course-navbar">
		<div class="container">
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="col-sm-8">
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="#description" class="course-sublinks">Description</a></li>
						<li><a href="#curriculum" class="course-sublinks">Curriculum</a></li>
						<li><a href="#reviews" class="course-sublinks">Reviews</a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div>
			<div class="col-sm-4">
				<?php if (isset($global->walletAddress) && !empty($global->walletAddress)) { ?>
				<a href="javascript:;" class="btn btn-lg btn-success js-take-course btn-crypto-op" data-wallet="<?php echo $global->walletAddress; ?>">Buy this course</a>
				<span class="course-price font-md"></span>
				<?php } else { ?>
				<a href="javascript:;" class="btn btn-lg btn-success js-take-course btn-block btn-crypto-app">Buy this course</a>
				<?php } ?>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
</div>
<section class="course-section">
	<article class="course-details">
		<div class="container invisible page-load-hidden">
			<div class="row">
				<div class="col-md-8">
					<div class="course-header">
						<h1 class="course-title" id="courseName"><?php echo $courseTitle; ?></h1>
						<p class="lead mb10 course-subtitle" id="subtitle"></p>
						<div class="course-meta">
							<span class="author dib mb10">by <a href="javascript:void(0)" class="author-link mr5 js-institute-link js-institute-name"></a></span>
							<span class="dib mb10 mr5"><input type="hidden" class="rating ratingStars" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="5" disabled=""></span>
							<a href="javascript:void(0)" class="reviews-link scrolltodiv dib mb10" data-target="reviews"><span class="rating-count ratingTotal">0 ratings</span>, <span class="js-enrolled">0</span> students enrolled</a>
						</div>
					</div>
					<div class="course-showcase">
						<!-- <video id="videoShowcasePlayer"></video> -->
						<!-- <video width="640" height="360" id="player2" poster="media/echo-hereweare.jpg" controls="controls" preload="none" style="width: 100%; height: 100%;">
							MP4 source must come first for iOS
							<source type="video/mp4" src="media/echo-hereweare.mp4" />
							WebM for Firefox 4 and Opera
							<source type="video/webm" src="media/echo-hereweare.webm" />
							OGG for Firefox 3
							<source type="video/ogg" src="media/echo-hereweare.ogv" />
							Fallback flash player for no-HTML5 browsers with JavaScript turned off
							<object width="640" height="360" type="application/x-shockwave-flash" data="flashmediaelement.swf" style="width: 100%; height: 100%;"> 		
								<param name="movie" value="flashmediaelement.swf" /> 
								<param name="flashvars" value="controls=true&poster=media/echo-hereweare.jpg&file=media/echo-hereweare.mp4" /> 		
								Image fall back for non-HTML5 browser with JavaScript turned off and no Flash player installed
								<img src="media/echo-hereweare.jpg" width="640" height="360" alt="Here we are" 
									title="No video playback capabilities" />
							</object>
						</video> -->
					</div>
				</div>
				<div class="col-md-4">
					<div class="buy-course mb20"> 
						<div class="pad20">
							<div class="course-price mb10 text-center font-md" id="coursePrice">
								<!-- <span class="price-discount"><i class="fa fa-rupee"></i>300</span>
								<span class="price-now"><i class="fa fa-rupee"></i>300</span> -->
							</div>
							<div class="mb10">
								<?php if (isset($global->walletAddress) && !empty($global->walletAddress)) { ?>
								<a href="javascript:;" class="btn btn-lg btn-success btn-block js-take-course btn-crypto-op" data-wallet="<?php echo $global->walletAddress; ?>">Enroll Now</a>
								<?php } else { ?>
								<a href="javascript:;" class="btn btn-lg btn-success btn-block js-take-course btn-crypto-app">Enroll Now</a>
								<?php } ?>
							</div>
							<div>
								<ul class="list-inline list-bordered text-center">
									<li><a href="<?php echo $sitepathPolicyTerms; ?>" target="_blank">Terms Of Use</a></li><li><a data-toggle="modal" href="#refundModal">Refund Policy</a></li>
								</ul>
							</div>
							<div class="row">
								<div class="col-md-12" id="couponswid">
									<div class="input-group">
										<input type="text" name="coupon" value="" id="couponcode" class="form-control couponCodeC" placeholder="Redeem a Coupon">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id="couponapply">Apply</button>
										</span>
									</div><!-- /input-group -->
									<span class="help-block text-center"></span>
								</div>
							</div>
						</div>
					</div>
					<div class="course-meta-data bt-grey-9">
						<ul class="list-inline list-meta">
							<li class="list-item-short">
								<label for=""><i class="fa fa-book"></i> Course ID :</label>
								<span class="value" id="courseID"></span>
							</li>
							<li class="list-item-short">
								<label for=""><i class="fa fa-users"></i> Enrolled :</label>
								<span class="value js-enrolled"></span>
							</li>
							<li class="list-item-short">
								<label for=""><i class="fa fa-clock-o"></i> Subjects :</label>
								<span class="value" id="noOfSubjects"></span>
							</li>
							<li id="courseTest"></li>
							<li id="contentStats"></li>
							<li id="liveEndDates"></li>
							<li class="list-item-long" id="msgValidity">
								<div class="note"></div>
							</li>
							<li class="list-item-long">
								<label for="">Categories :</label>
								<div class="cat-list" id="courseCategory">
									<!-- <a href="#" class="btn btn-category">Hobbies &amp; Skills</a>
									<a href="#" class="btn btn-category">Miscellaneous</a> -->
								</div>
							</li>
							<li class="list-item-long">
								<label for="">Tags :</label>
								<div class="cat-list" id="courseTags">
									<!-- <a href="#" class="btn btn-category">Hobbies &amp; Skills</a>
									<a href="#" class="btn btn-category">Miscellaneous</a> -->
								</div>
							</li>
						</ul>
					</div>
					<div class="social-share bt-grey-9 bb-grey-9 pr dib w100">
						<h5 class="social-title text-center text-uppercase">Share</h5>
						<ul class="list-inline dib w100 clear-mrg">
							<li class="social-item">
								<a href="javascript:void(0)" target="_blank" class="dib js-share-facebook"><i class="fa fa-facebook color-facebook"></i></a>
							</li>
							<li class="social-item">
								<a href="javascript:void(0)" target="_blank" class="dib js-share-twitter"><i class="fa fa-twitter color-twitter"></i></a>
							</li>
							<li class="social-item">
								<a href="javascript:void(0)" target="_blank" class="dib js-share-google"><i class="fa fa-google-plus color-google"></i></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-md-8">
					<div class="course-description mb20 affix-spy-adj" id="description">
						<h3 class="desc-title">Course Description</h3>
						<div class="js-full-course-details desc-content" id="jsDescription">
							<!-- <p><strong>Have you ever finished a meeting and forget what you need to do?</strong></p>
							<p><strong>Or wrote down an important task and forgot to do it?</strong></p>
							<p><strong>Or maybe lost track of everything you need to do just to find yourself doing everything in the last minute?</strong></p>
							<p><strong>Or maybe had so many tedious meetings with your team or employees to sync everything up that by the end of the day you didn't get anything done…</strong></p>
							<p>If you were looking for a solution to help you manage and speed up the progress of your projects, or you're running several projects with people based in different locations and you want to be able to keep tab of everything going on, or you just want to increase the productivity of your business - you've reached the right place.</p>
							<p>If you ever worked as part of a Team, you know that collaboration and synchronisation are the key factors for the activity to be a success or a total failure!</p>
							<p>Everyone needs to know what everyone else is doing, when each action starts and stops. and sometimes the activity looks so complex that you don't even see the whole picture and can really get confused.</p>
							<p><strong>Meet Trello!</strong></p>
							<p><strong>Trello is a web based Free application that can boost your output dramatically. companies like Adobe, Tumblr, Trip Advisor and the New York Times are using Trello to manage their projects.</strong></p>
							<p>Trello is an amazing application, that improves your personal productivity.</p>
							<p><strong>Use it to manage and track your own tasks or use it with your team to improve collaboration.</strong></p>
							<p>With Trello you can build your projects, assign tasks to different users, assign each task a due date, build checklists, transfer files, interact with your team regarding the tasks and be informed of everything that's going on, and do all that while your team members aren't even in the same continent!</p>
							<p><strong>In this course I'm going to show you everything you need to know to get up and running with Trello.</strong></p>
							<p>in just a little over an hour you'll be able to set up your account, build your projects, assign tasks and be able to manage an effective team, increase your productivity and as a direct result you will have more time to start a new project, or maybe ,just maybe, a little bit more R&amp;R (Rest and Relaxation) time.</p>
							<p><strong>If you are ready to learn how to use Trello to make your project more efficient, less time consuming and have more free time - this course is for you.</strong></p>
							<p>I'll see you there!</p> -->
						</div>
						<a href="javascript:void(0)" class="get-details btn-block mb20 js-course-details hide" data-more="full">Full details</a>
						<div class="desc-block mb20">
							<p><strong>What is the target audience?</strong></p>
							<ul id="targetAudience">
								<!-- <li>Entrepreneurs</li>
								<li>Business Owners</li>
								<li>Project Managers</li>
								<li>Team Leaders</li>
								<li>Anyone who wants to Manage Projects or tasks and Improve productivity</li> -->
							</ul>
						</div>
					</div>
					<div class="course-index affix-spy-adj" id="curriculum">
						<!-- <table class="table table-responsive">
							<tr class="list-row-subject">
								<th class="course-subject">
									<h2 class="subject-title">
										<div class="subject-avatar">
											<img src="img/subject.jpg" alt="" class="img-responsive">
										</div>Subject: Introduction to What is Yoga as Meditation?
									</h2>
								</th>
							</tr>
							<tr class="list-row-section">
								<th class="ssection">
									<h3 class="ssection-title">
										<span class="ssection-title-txt">Section 1: Introduction to the course</span>
										<span class="ssection-meta"><p>4 Exams</p><p>4 Assignments</p><p>4 lectures, 24:25</p></span>
									</h3>
								</th>
							</tr>
							<tr class="list-row-lecture">
								<td class="lecture active">
									<h4 class="lecture-title">
										<a href="javascript:void(0)" class="lecture-link">
											<span class="lecture-play"><i class="fa fa-play-circle"></i></span>
											<div class="lecture-title-blk">
												<span class="lecture-number">1.1</span>
												<span class="lecture-title-txt">Introduction to the course</span>
												<span class="lecture-duration">03:25</span>
											</div>
											<div class="lecture-play-btn">
												<button class="btn btn-primary btn-preview" data-src="media/echo-hereweare.mp4">Preview</button>
											</div>
										</a>
									</h4>
								</td>
							</tr>
							<tr class="list-row-lecture">
								<td class="lecture">
									<h4 class="lecture-title">
										<a href="javascript:void(0)">
											<span class="lecture-play"><i class="fa fa-play-circle"></i></span>
											<div class="lecture-title-blk">
												<span class="lecture-number">1.2</span>
												<span class="lecture-title-txt">Introduction to the course</span>
												<span class="lecture-duration">03:25</span>
											</div>
										</a>
									</h4>
								</td>
							</tr>
							<tr class="list-row-lecture">
								<td class="lecture">
									<h4 class="lecture-title">
										<a href="javascript:void(0)">
											<span class="lecture-play"><i class="fa fa-play-circle"></i></span>
											<div class="lecture-title-blk">
												<span class="lecture-number">1.3</span>
												<span class="lecture-title-txt">Introduction to the course</span>
												<span class="lecture-duration">03:25</span>
											</div>
										</a>
									</h4>
								</td>
							</tr>
							<tr class="list-row-lecture">
								<td class="lecture">
									<h4 class="lecture-title">
										<a href="javascript:void(0)">
											<span class="lecture-play"><i class="fa fa-play-circle"></i></span>
											<div class="lecture-title-blk">
												<span class="lecture-number">1.4</span>
												<span class="lecture-title-txt">Introduction to the course</span>
												<span class="lecture-duration">03:25</span>
											</div>
										</a>
									</h4>
								</td>
							</tr>
							<tr class="list-row-section">
								<th class="ssection">
									<h3 class="ssection-title">
										<span class="ssection-title-txt">Section 2: Introduction to the course</span>
										<span class="ssection-meta">4 lectures, 24:25</span>
									</h3>
								</th>
							</tr>
							<tr class="list-row-lecture">
								<td class="lecture">
									<h4 class="lecture-title">
										<a href="javascript:void(0)">
											<span class="lecture-play"><i class="fa fa-lock"></i></span>
											<div class="lecture-title-blk">
												<span class="lecture-number">2.1</span>
												<span class="lecture-title-txt">Introduction to the course</span>
												<span class="lecture-duration">03:25</span>
											</div>
										</a>
									</h4>
								</td>
							</tr>
							<tr class="list-row-lecture">
								<td class="lecture">
									<h4 class="lecture-title">
										<a href="javascript:void(0)">
											<span class="lecture-play"><i class="fa fa-lock"></i></span>
											<div class="lecture-title-blk">
												<span class="lecture-number">2.2</span>
												<span class="lecture-title-txt">Introduction to the course</span>
												<span class="lecture-duration">03:25</span>
											</div>
										</a>
									</h4>
								</td>
							</tr>
							<tr class="list-row-lecture">
								<td class="lecture">
									<h4 class="lecture-title">
										<a href="javascript:void(0)">
											<span class="lecture-play"><i class="fa fa-lock"></i></span>
											<div class="lecture-title-blk">
												<span class="lecture-number">2.3</span>
												<span class="lecture-title-txt">Introduction to the course</span>
												<span class="lecture-duration">03:25</span>
											</div>
										</a>
									</h4>
								</td>
							</tr>
							<tr class="list-row-lecture">
								<td class="lecture">
									<h4 class="lecture-title">
										<a href="javascript:void(0)">
											<span class="lecture-play"><i class="fa fa-lock"></i></span>
											<div class="lecture-title-blk">
												<span class="lecture-number">2.4</span>
												<span class="lecture-title-txt">Introduction to the course</span>
												<span class="lecture-duration">03:25</span>
											</div>
										</a>
									</h4>
								</td>
							</tr>
							<tr class="list-row-section">
								<th class="ssection">
									<h3 class="ssection-title">
										<span class="ssection-title-txt">Section 3: Exams and Assignments</span>
										<span class="ssection-meta">2 Assignments, 2 Exams</span>
									</h3>
								</th>
							</tr>
							<tr class="list-row-lecture">
								<td class="lecture">
									<h4 class="lecture-title">
										<a href="javascript:void(0)">
											<span class="lecture-play"><i class="fa fa-pencil-square-o"></i></span>
											<div class="lecture-title-blk">
												<span class="lecture-number">3.1</span>
												<span class="lecture-title-txt">Assignment 1</span>
											</div>
										</a>
									</h4>
								</td>
							</tr>
							<tr class="list-row-lecture">
								<td class="lecture">
									<h4 class="lecture-title">
										<a href="javascript:void(0)">
											<span class="lecture-play"><i class="fa fa-pencil-square-o"></i></span>
											<div class="lecture-title-blk">
												<span class="lecture-number">3.2</span>
												<span class="lecture-title-txt">Assignment 2</span>
											</div>
										</a>
									</h4>
								</td>
							</tr>
							<tr class="list-row-lecture">
								<td class="lecture">
									<h4 class="lecture-title">
										<a href="javascript:void(0)">
											<span class="lecture-play"><i class="fa fa-book"></i></span>
											<div class="lecture-title-blk">
												<span class="lecture-number">3.3</span>
												<span class="lecture-title-txt">Exam 1</span>
											</div>
										</a>
									</h4>
								</td>
							</tr>
							<tr class="list-row-lecture">
								<td class="lecture">
									<h4 class="lecture-title">
										<a href="javascript:void(0)">
											<span class="lecture-play"><i class="fa fa-book"></i></span>
											<div class="lecture-title-blk">
												<span class="lecture-number">3.4</span>
												<span class="lecture-title-txt">Exam 2</span>
											</div>
										</a>
									</h4>
								</td>
							</tr>
						</table> -->
					</div>
				</div>
				<div class="col-md-4">
					<div class="about-instructor">
						<h4 class="instructor-title">About <a href="javascript:void(0)" class="js-institute-link js-institute-name"></a></h4>
						<h5 id="tagline"></h5>
						<div class="instructor-description js-full-instructor-details">
							<a href="javascript:void(0)" class="js-institute-link"><img class="instructor-avatar" src="#" alt="" id="instituteImg"></a><span id="instituteDescription"></span>
						</div>
						<a href="javascript:void(0)" class="get-details btn-block mb20 js-instructor-details hide" data-more="full">Full details</a>
					</div>
					<div class="other-courses bt-grey-9 pr">
						<h4 class="other-courses-title">Courses by <a href="javascript:void(0)" class="js-institute-link js-institute-name"></a></h4>
						<div id="listOtherCourses">
							<!-- <ul class="list-unstyled list-courses">
								<li class="other-item">
									<div class="row">
										<div class="col-md-6">
											<a href="#">
												<img class="img-responsive" src="img/course-image.jpg" alt="">
											</a>
										</div>
										<div class="col-md-6 pl-clr">
											<div>
												<a href="#" class="other-title">What Is Yoga As Meditation What Is Yoga As Meditation?</a>
											</div>
											<div>
												<span class="cost"><i class="fa fa-rupee"></i>300</span>
											</div>
											<div>
												<span class="stars"><span style="width:70%"></span></span>
											</div>
										</div>
									</div>
								</li>
								<li class="other-item">
									<div class="row">
										<div class="col-md-6">
											<a href="#">
												<img class="img-responsive" src="img/course-image.jpg" alt="">
											</a>
										</div>
										<div class="col-md-6 pl-clr">
											<div>
												<a href="#" class="other-title">What Is Yoga As Meditation What Is Yoga As Meditation?</a>
											</div>
											<div>
												<span class="cost"><i class="fa fa-rupee"></i>300</span>
											</div>
											<div>
												<span class="stars"><span style="width:70%"></span></span>
											</div>
										</div>
									</div>
								</li>
								<li class="other-item">
									<div class="row">
										<div class="col-md-6">
											<a href="#">
												<img class="img-responsive" src="img/course-image.jpg" alt="">
											</a>
										</div>
										<div class="col-md-6 pl-clr">
											<div>
												<a href="#" class="other-title">What Is Yoga As Meditation What Is Yoga As Meditation?</a>
											</div>
											<div>
												<span class="cost"><i class="fa fa-rupee"></i>300</span>
											</div>
											<div>
												<span class="stars"><span style="width:70%"></span></span>
											</div>
										</div>
									</div>
								</li>
								<li class="other-item">
									<div class="row">
										<div class="col-md-6">
											<a href="#">
												<img class="img-responsive" src="img/course-image.jpg" alt="">
											</a>
										</div>
										<div class="col-md-6 pl-clr">
											<div>
												<a href="#" class="other-title">What Is Yoga As Meditation What Is Yoga As Meditation?</a>
											</div>
											<div>
												<span class="cost"><i class="fa fa-rupee"></i>300</span>
											</div>
											<div>
												<span class="stars"><span style="width:70%"></span></span>
											</div>
										</div>
									</div>
								</li>
							</ul> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
	<article class="affix-spy-adj" id="reviews">
		<div class="container">
			<div class="row">
				<div class="col-md-10">
					<div id="reviews" class="rate-reviews">
						<h3>Reviews</h3>
						<div class="row">
							<div class="col-sm-3">
								<span class="subblock-title">Average Ratings</span>
								<div class="avg-rate">5.0</div>
								<div class="rating">
									<input type="hidden" class="rating ratingStars" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="5" disabled="">
									<span class="rating-count ratingTotal">0 ratings</span>
								</div>
							</div>
							<div class="col-sm-4">
								<span class="subblock-title dib mb10">Details</span>
								<ul class="list-unstyled list-rates">
									<li>
										<span class="item-title">5 Stars</span>
										<div class="progress">
											<div class="progress-bar progress5" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
												<span class="sr-only">60% Complete</span>
											</div>
										</div>&nbsp;<span class="count5">3</span>
									</li>
									<li>
										<span class="item-title">4 Stars</span>
										<div class="progress">
											<div class="progress-bar  progress4" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
												<span class="sr-only">50% Complete</span>
											</div>
										</div>&nbsp;<span class="count4">0</span>
									</li>
									<li>
										<span class="item-title">3 Stars</span>
										<div class="progress">
											<div class="progress-bar progress3" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
												<span class="sr-only">40% Complete</span>
											</div>
										</div>&nbsp;<span class="count3">0</span>
									</li>
									<li>
										<span class="item-title">2 Stars</span>
										<div class="progress">
											<div class="progress-bar progress2" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
												<span class="sr-only">30% Complete</span>
											</div>
										</div>&nbsp;<span class="count2">0</span>
									</li>
									<li>
										<span class="item-title">1 Stars</span>
										<div class="progress">
											<div class="progress-bar progress1" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
												<span class="sr-only">10% Complete</span>
											</div>
										</div>&nbsp;<span class="count1">0</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div id="comments" class="comments">
						<ul class="list-unstyled list-comments"></ul>
					</div>
				</div>
			</div>
		</div>
	</article>
</section>