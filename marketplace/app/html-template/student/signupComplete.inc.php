<section class="main-body">
	<article class="container">
		<div class="col-md-6 col-md-offset-3">
			<form class="form-3d signup" id="studentSignup">
				<div class="text-center">
					<h3 class="form-title">Confirm your email</h3>
					<h4 class="mb20">Great!! Just one last step</h4>
					<h4 class="mb20">We have sent you a Verification email</h4>
					<div class="mb20">
						<a class="btn btn-primary btn-lg mb10 btn-block resend-link" href="javascript:void(0)">Resend Verification Link</a>
						<p>or</p>
						<a class="btn btn-primary btn-lg btn-block resend-link" href="<?php echo $sitepathCourses; ?>">Check Courses</a>
					</div>
					<hr class="style-two">
					<div class="text-center">
						Already have an account? <a href="#loginModal" data-toggle="modal">Login</a>
					</div>
				</div>
			</form>
		</div>
	</article>
</section>