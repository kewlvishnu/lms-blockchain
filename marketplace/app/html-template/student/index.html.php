<?php
	require_once('includes/header.php');

	switch ($page) {
		case 'packages':
			require_once('packages.inc.php');
			break;
		
		case 'package':
			require_once('package.inc.php');
			break;
		
		case 'courses':
			require_once('courses.inc.php');
			break;
		
		case 'course':
			require_once('course.inc.php');
			break;
		
		case 'signup':
			require_once('signup.inc.php');
			break;
		
		case 'signupComplete':
			require_once('signupComplete.inc.php');
			break;
		
		case 'verify':
			require_once('verify.inc.php');
			break;
		
		case 'resetPassword':
			require_once('resetPassword.inc.php');
			break;
		
		case 'initStudent':
			require_once('initStudent.inc.php');
			break;
		
		case 'policiesTerms':
			require_once('policiesTerms.inc.php');
			break;
		
		case 'policiesPrivacy':
			require_once('policiesPrivacy.inc.php');
			break;
		
		case 'instructor':
			require_once('instructor.inc.php');
			break;
		
		case 'institute':
			require_once('institute.inc.php');
			break;
		
		case 'search':
			require_once('search.inc.php');
			break;
		
		case 'contact':
			require_once('contact.inc.php');
			break;
		
		case 'about':
			require_once('about.inc.php');
			break;
		
		case 'logout':
			require_once('logout.inc.php');
			break;
		
		case 'laterpaypal':
			require_once('laterpaypal.inc.php');
			break;
		
		case 'laterpay':
			require_once('laterpay.inc.php');
			break;
		
		case 'mail':
			require_once('mail.inc.php');
			break;
		
		default:
			echo "There was some error";
			break;
	}

	require_once('includes/footer-html.php');
	require_once('includes/footer-includes.php');
?>