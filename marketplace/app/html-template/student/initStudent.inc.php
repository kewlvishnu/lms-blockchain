<section class="main-body">
	<article class="container">
		<div class="col-md-6 col-md-offset-3">
			<form class="form-3d signup" id="studentSignup">
				<div class="text-center">
					<h3 class="form-title">Activate your account</h3>
				</div>
				<div class="form-group">
					<div class="help-block text-center"></div>
				</div>
				<div class="form-group">
					<label class="text-uppercase" for="inputFirstName">First Name</label>
					<input type="text" class="form-control trans-control" id="inputFirstName" name="inputFirstName" placeholder="First Name" value="">
				</div>
				<div class="form-group">
					<label class="text-uppercase" for="inputLastName">Last Name</label>
					<input type="text" class="form-control trans-control" id="inputLastName" name="inputLastName" placeholder="Last Name" value="">
				</div>
				<div class="form-group">
					<label class="text-uppercase" for="inputEmailAddress">Email address</label>
					<input type="email" class="form-control trans-control" id="inputEmailAddress" name="inputEmailAddress" placeholder="Email" value="">
				</div>
				<div class="form-group">
					<label class="text-uppercase" for="inputPassword">New Password</label>
					<input type="password" class="form-control trans-control" id="inputPassword" name="inputPassword" placeholder="Password">
				</div>
				<button type="submit" class="btn btn-color2 btn-block text-uppercase" id="btnStudentSignup">Activate your account</button>
				<div class="text-center">
					<small>By signing up, you agree to our <a href="<?php echo $sitepathPolicyTerms; ?>" target="_blank">Terms of Use</a> and <a href="<?php echo $sitepathPolicyPrivacy; ?>" target="_blank">Privacy Policy</a></small>
				</div>
				<hr class="style-two">
				<div class="text-center">
					Fill up the form to activate your account
				</div>
			</form>
		</div>
	</article>
</section>
<!-- Modal -->
<div class="modal fade" id="mergeUserModal" tabindex="-1" role="dialog" aria-labelledby="mergeUserModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="mergeUserModalLabel">Duplicate Students</h4>
      </div>
      <div class="modal-body">
	    <span class="help-block"><span class="text-danger" id="duplicateError"></span></span>
      	<div id="listDuplicates">
	      	<p>Students with same email found. Is any of them you?</p>
	        <div id="duplicateUsers"></div>
	    </div>
      	<form id="frmStudentLogin" class="hide">
      		<div class="form-group">
      			<label for="">Enter password :</label>
      			<input type="password" class="form-control" id="inputVerifyPassword" />
			</div>
			<a href="javascript:void(0)" class="btn btn-warning" id="btnDuplicateBack">Back</a>
      	</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnVerify">Verify</button>
        <button type="button" class="btn btn-primary hide" id="btnVerifyMerge">Verify &amp; Merge</button>
      </div>
    </div>
  </div>
</div>