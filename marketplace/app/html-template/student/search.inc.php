<?php
	$courseCategories = "";
	if (isset($global->courseCategories) && (count($global->courseCategories)>0)) {
		$courseCategories = '<ul class="list-filter">';
		foreach ($global->courseCategories as $key => $value) {
			$courseCategories.= '<li>
									<a href="javascript:void(0)" class="checkbox clear-mrg">
										<input type="checkbox" class="catCheck mr5" data-id="'.$value['id'].'">'.htmlentities($value['category']).'
									</a>
								</li>';
		}
		$courseCategories.=	'</ul>';
	}
	$search = array();
	$pageGet = explode('&', $pageGet[1]);
	foreach ($pageGet as $key => $value) {
		$get = explode("=", $value);
		if ($get[0] == "q") {
			$search['q'] = urldecode($get[1]);
		} else if ($get[0] == "category") {
			$search['category'] = ((isset($get[1]))?($get[1]):(''));
		} else if ($get[0] == "price") {
			$search['price'] = ((isset($get[1]))?($get[1]):(''));
		}
	}
?>
<div class="main-body">
	<div class="container">
		<div class="min-ht-500">
			<h1 class="page-title" id="searchTitle">Search Results for "<?php echo $search['q']; ?>"</h1>
			<div class="row js-list-filter">
				<div class="col-md-3">
					<div class="panel-group" id="accordionCategories" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" class="btn-block" data-toggle="collapse" data-parent="#accordionCategories" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									Categories
									</a>
								</h4>
							</div>
							<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
								<?php echo $courseCategories; ?>
								</div><!-- .panel-body -->
							</div><!-- #collapseTwo -->
						</div><!-- .panel -->
					</div><!-- #accordionCategories -->
					<div class="panel-group" id="accordionPriceRange" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" class="btn-block" data-toggle="collapse" data-parent="#accordionPriceRange" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
									Price Range
									</a>
								</h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body">
									<ul class="list-filter" id="filterPrice"></ul>
									<!-- <ul class="list-filter">
										<li>
											<a href="javascript:void(0)" class="checkbox clear-mrg">
												<input type="checkbox" class="priceCheck" data-id="1"> Free 
											</a>
										</li>
										<li>
											<a href="javascript:void(0)" class="checkbox clear-mrg">
												<input type="checkbox" class="priceCheck" data-id="2"> <i class="fa fa-rupee"></i> 1 to <i class="fa fa-rupee"></i> 100
											</a>
										</li>
										<li>
											<a href="javascript:void(0)" class="checkbox clear-mrg">
												<input type="checkbox" class="priceCheck" data-id="3"> <i class="fa fa-rupee"></i> 101 to <i class="fa fa-rupee"></i> 1000
											</a>
										</li>
										<li>
											<a href="javascript:void(0)" class="checkbox clear-mrg">
												<input type="checkbox" class="priceCheck" data-id="4"> <i class="fa fa-rupee"></i> 1001 to <i class="fa fa-rupee"></i> 2000
											</a>
										</li>
										<li>
											<a href="javascript:void(0)" class="checkbox clear-mrg">
												<input type="checkbox" class="priceCheck" data-id="5"> <i class="fa fa-rupee"></i> 2001 to <i class="fa fa-rupee"></i> 3000
											</a>
										</li>
										<li>
											<a href="javascript:void(0)" class="checkbox clear-mrg">
												<input type="checkbox" class="priceCheck" data-id="6"> <i class="fa fa-rupee"></i> 3001 to <i class="fa fa-rupee"></i> 4000
											</a>
										</li>
										<li>
											<a href="javascript:void(0)" class="checkbox clear-mrg">
												<input type="checkbox" class="priceCheck" data-id="7"> <i class="fa fa-rupee"></i> 4001 to <i class="fa fa-rupee"></i> 5000
											</a>
										</li>
										<li>
											<a href="javascript:void(0)" class="checkbox clear-mrg">
												<input type="checkbox" class="priceCheck" data-id="8"> Above <i class="fa fa-rupee"></i> 5000
											</a>
										</li>
									</ul> -->
								</div><!-- .panel-body -->
							</div><!-- #collapseTwo -->
						</div><!-- .panel -->
					</div><!-- #accordionPriceRange -->
				</div><!-- .col-md-3 -->
				<div class="col-md-9" id="searchResults">
					<!-- <div class="course gradient-grey3">
						<div class="pad10">
							<div class="row">
								<div class="col-md-3">
									<div class="pad10">
										<img src="http://lorempixel.com/400/300/abstract/" alt="" class="btn-block course-img-sm">
									</div>
								</div>
								<div class="col-md-9">
									<h4><a href="javascript:void(0)">Marginal Costing Theory and Practice <small>(ID: C0077)</small></a></h4>
									<p>by <a href="//localhost/arcanemind/instructor/undefined" data-instituteid="undefined">CA N Raja Natarajan</a></p>
									<div class="text-overflow">
										<span class="mr5"><i class="fa fa-rupee"></i> 3000</span>
										<span class="mr5"><span class="rating-symbol" style="display: inline-block; position: relative;"><span class="rating-symbol-background fa fa-star-o" style="visibility: hidden;"></span><span class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; width: auto;"><span class="fa fa-star gold"></span></span></span><span class="rating-symbol" style="display: inline-block; position: relative;"><span class="rating-symbol-background fa fa-star-o" style="visibility: hidden;"></span><span class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; width: auto;"><span class="fa fa-star gold"></span></span></span><span class="rating-symbol" style="display: inline-block; position: relative;"><span class="rating-symbol-background fa fa-star-o" style="visibility: hidden;"></span><span class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; width: auto;"><span class="fa fa-star gold"></span></span></span><span class="rating-symbol" style="display: inline-block; position: relative;"><span class="rating-symbol-background fa fa-star-o" style="visibility: hidden;"></span><span class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; width: auto;"><span class="fa fa-star gold"></span></span></span><span class="rating-symbol" style="display: inline-block; position: relative;"><span class="rating-symbol-background fa fa-star-o" style="visibility: hidden;"></span><span class="rating-symbol-foreground" style="display: inline-block; position: absolute; overflow: hidden; left: 0px; width: auto;"><span class="fa fa-star gold"></span></span></span></span>
										<span title="Java Programming from Scratch, Java Programming from Scratch, Java Programming from Scratch"><i class="fa fa-th-list"></i> Java Programming from Scratch, Java Programming from Scratch, Java Programming from Scratch</span>
									</div>
								</div>
							</div>
						</div>
					</div> -->
				</div><!-- .col-md-9 -->
			</div><!-- .row -->
		</div>
	</div>
</div>