<div id="modalMetamaskChangeAddress" class="modal modal-notification fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <div class="meta-logo text-center">
                    <img src="<?php echo $sitepathManage; ?>assets/global/img/wallet/change-address.png" alt="">
                </div>
            </div>
            <div class="modal-footer">
                <p class="notice">Your crypto address associated with your account is different from the MetaMask address.</p>
                <p class="notice">Do you want to associate the new MetaMask address?</p>
                <p class="notice">New Address: <span id="ccAddress"></span></p>
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-lg" id="btnMMChangeAddress">Yes</button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger btn-lg">No</button>
                </div>
            </div>
        </div>
    </div>
</div>