<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
	 id="purchaseNowModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" id="modelclose" data-dismiss="modal" aria-hidden="true">
					×</button>
				<h4 class="modal-title">
					Confirm Purchase
				</h4>
			</div>
			<div class="modal-body">
				<table class="table table-responsive table-bordered">
					<tr>
						<th colspan="2" class="text-center">
							<div class="pad30">
								<p class="lead course-name"></p>
							</div>
						</th>
					</tr>
					<tr>
						<th>You Pay</th>
						<th><strong><span class="amount">$150</span></strong></th>
					</tr>
					<tr>
						<td colspan="2"><?php /*
							<div class="pad20 text-center">
								<div class="btn-group" data-toggle="buttons">
								  <label class="btn btn-primary active">
								    <input type="radio" name="optionPayVia" id="option1" value="p" autocomplete="off" checked> Pay via blockchain (Portis)
								  </label>
								  <label class="btn btn-primary">
								    <input type="radio" name="optionPayVia" id="option2" value="m" autocomplete="off"> Pay via metamask
								  </label>
								</div>
							</div>*/ ?>
							<div class="pad20">
								<button class="btn btn-color1 btn-lg btn-block" id="payUMoneyButton">Pay Now</button>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2"><small>You will be redirected to payment page and then sent back once you complete your purchase.</small></td>
					</tr>
					<tr>
						<td colspan="2"><small>By clicking the 'Pay Now' button you agree these <a href="#">Terms and Conditions</a>.</small></td>
					</td>
				</table>
			</div>
		</div>
	</div>
</div>