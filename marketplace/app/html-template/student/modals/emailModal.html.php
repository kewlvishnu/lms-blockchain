<div class="modal fade" id="emailModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button data-dismiss="modal" class="close" type="button">x</button>
				<h3 class="modal-title">Please Provide your email!</h3>
			</div>
			<div class="modal-body">
				<div class="login-form clearfix">
					<form role="form" class="js-cover">
						<div class="form-group">
							<div id="fbError" class="help-block"></div>
						</div>
						<div class="form-group">
							<input type="email" id="facebookEmail" placeholder="Enter email" id="user" class="form-control">
						</div>
						<div class="pull-left">
							<button id="btnContinue" class="btn btn-primary" type="submit">Continue</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>