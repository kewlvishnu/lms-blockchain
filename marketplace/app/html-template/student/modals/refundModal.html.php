<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
	 id="refundModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					×</button>
				<h4 class="modal-title">
					Refund Policy
				</h4>
			</div>
			<div class="modal-body">
				<div class="text-justify">
					<p>We provide You a fifteen (15)-day, no-questions-asked money back guarantee on Courses. If you, as a Student, are unhappy with a Course, You must request a refund within  fifteen (15) days of the date that you paid for a Course. We will refund the full amount you paid. To request a refund, contact us via email on <a href="mailto:refund@integro.io">refund@integro.io</a></p>
				</div>
				<div class="text-right">
					<button type="button" class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>