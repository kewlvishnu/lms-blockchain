<div id="modalMetamaskNotInstalled" class="modal modal-notification fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">No MetaMask insalled</h4>
            </div> -->
            <div class="modal-body text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <div class="meta-logo text-center">
                    <img src="<?php echo $sitepathManage; ?>assets/global/img/wallet/not-installed.png" alt="">
                </div>
            </div>
            <div class="modal-footer">                
                <h4 class="text-danger">No MetaMask Detected!</h4>
                <p class="notice">Install MetaMask and associate your account with the crypto wallet address</p>
                <div class="text-center">
                    <a href="https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en" target="_blank" class="btn btn-warning btn-lg">Click here to install MetaMask</a>
                </div>
            </div>
        </div>
    </div>
</div>