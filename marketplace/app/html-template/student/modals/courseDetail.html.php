<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
	 id="courseDetailModal" class="modal fade">
	<div class="modal-dialog clear-mrg w100">
		<div class="modal-content bg-grey2">
			<div class="modal-header">
				<button type="button" class="close" id="modelclose" data-dismiss="modal" aria-hidden="true">
					×</button>
				<h4 class="modal-title">
					Course Detail
				</h4>
			</div>
			<div class="modal-body">
				<article class="course-details">
					<div class="container invisible page-load-hidden">
						<div class="row">
							<div class="col-md-8">
								<div class="course-header">
									<h1 class="course-title" id="courseName"></h1>
									<p class="lead mb10 course-subtitle" id="subtitle"></p>
									<div class="course-meta">
										<span class="author dib mb10">by <a href="javascript:void(0)" class="author-link mr5 js-institute-link js-institute-name"></a></span>
										<span class="dib mb10 mr5"><input type="hidden" class="rating ratingStars" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="5" disabled=""></span>
										<a href="javascript:void(0)" class="reviews-link scrolltodiv dib mb10" data-target="reviews"><span class="rating-count ratingTotal">0 ratings</span>, <span class="js-enrolled">0</span> students enrolled</a>
									</div>
								</div>
								<div class="course-showcase" id="courseShowcase" data-target="affix">
									<!-- <video id="videoShowcasePlayer"></video> -->
									<!-- <video width="640" height="360" id="player2" poster="media/echo-hereweare.jpg" controls="controls" preload="none" style="width: 100%; height: 100%;">
										MP4 source must come first for iOS
										<source type="video/mp4" src="media/echo-hereweare.mp4" />
										WebM for Firefox 4 and Opera
										<source type="video/webm" src="media/echo-hereweare.webm" />
										OGG for Firefox 3
										<source type="video/ogg" src="media/echo-hereweare.ogv" />
										Fallback flash player for no-HTML5 browsers with JavaScript turned off
										<object width="640" height="360" type="application/x-shockwave-flash" data="flashmediaelement.swf" style="width: 100%; height: 100%;"> 		
											<param name="movie" value="flashmediaelement.swf" /> 
											<param name="flashvars" value="controls=true&poster=media/echo-hereweare.jpg&file=media/echo-hereweare.mp4" /> 		
											Image fall back for non-HTML5 browser with JavaScript turned off and no Flash player installed
											<img src="media/echo-hereweare.jpg" width="640" height="360" alt="Here we are" 
												title="No video playback capabilities" />
										</object>
									</video> -->
								</div>
							</div>
							<div class="col-md-4">
								<div class="buy-course mb20"> 
									<div class="pad20">
										<div class="course-price mb10 text-center font-md">
											<!-- <span class="price-discount"><i class="fa fa-rupee"></i>300</span>
											<span class="price-now"><i class="fa fa-rupee"></i>300</span> -->
										</div>
										<div class="mb10">
											<a data-toggle="modal" href="#loginModal" class="btn btn-lg btn-success btn-block js-take-course">Buy Package</a>
										</div>
										<div>
											<ul class="list-inline list-bordered text-center">
												<li><a href="<?php echo $sitepathPolicyTerms; ?>" target="_blank">Terms Of Use</a></li><li><a data-toggle="modal" href="#refundModal">Refund Policy</a></li>
											</ul>
										</div>
										<small class="mb10 text-center" id="coursePrice">
											<!-- <span class="price-discount"><i class="fa fa-rupee"></i>300</span>
											<span class="price-now"><i class="fa fa-rupee"></i>300</span> -->
										</small>
									</div>
								</div>
								<div class="course-meta-data bt-grey-9">
									<ul class="list-inline list-meta">
										<li class="list-item-short">
											<label for=""><i class="fa fa-book"></i> Course ID :</label>
											<span class="value" id="courseID"></span>
										</li>
										<li class="list-item-short">
											<label for=""><i class="fa fa-users"></i> Enrolled :</label>
											<span class="value js-enrolled"></span>
										</li>
										<li class="list-item-short">
											<label for=""><i class="fa fa-clock-o"></i> Subjects :</label>
											<span class="value" id="noOfSubjects"></span>
										</li>
										<li id="courseTest"></li>
										<li id="contentStats"></li>
										<li id="liveEndDates"></li>
										<li class="list-item-long" id="msgValidity">
											<div class="note"></div>
										</li>
										<li class="list-item-long">
											<label for="">Categories :</label>
											<div class="cat-list" id="courseCategory"></div>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-md-8">
								<div class="course-description mb20 affix-spy-adj" id="description">
									<h3 class="desc-title">Course Description</h3>
									<div class="js-full-course-details desc-content" id="jsCourseDescription"></div>
									<a href="javascript:void(0)" class="get-details btn-block mb20 js-course-details hide" data-more="full">Full details</a>
									<div class="desc-block mb20">
										<p><strong>What is the target audience?</strong></p>
										<ul id="targetAudience"></ul>
									</div>
								</div>
								<div class="course-index affix-spy-adj" id="curriculum"></div>
							</div>
							<div class="col-md-4">
								<div class="about-instructor">
									<h4 class="instructor-title">About <a href="javascript:void(0)" class="js-institute-link js-institute-name"></a></h4>
									<h5 id="tagline"></h5>
									<div class="instructor-description js-full-instructor-details">
										<a href="javascript:void(0)" class="js-institute-link"><img class="instructor-avatar" src="#" alt="" id="instituteImg"></a><span id="instituteDescription"></span>
									</div>
									<a href="javascript:void(0)" class="get-details btn-block mb20 js-instructor-details hide" data-more="full">Full details</a>
								</div>
							</div>
						</div>
					</div>
				</article>
			</div>
		</div>
	</div>
</div>