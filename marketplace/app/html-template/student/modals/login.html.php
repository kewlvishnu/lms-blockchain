<div class="modal fade" id="loginModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button data-dismiss="modal" class="close" type="button">x</button>
				<h3 class="modal-title">Please Login and start learning!</h3>
			</div>
			<div class="modal-body">
				<div class="login-form clearfix">
					<form role="form">
						<input type="hidden" id="userRole" value="4" />
						<div class="form-group">
							<input type="text" placeholder="Enter username or email" id="user" class="form-control">
							<div id="userError" class="error-msg text-danger"></div>
						</div>
						<div class="form-group">
							<input type="password" placeholder="Password" id="pwd" class="form-control">
							<div id="pwdError" class="error-msg text-danger"></div>
						</div>
						<div class="checkbox pull-right">
							<label>
								<input type="checkbox" id="remember"> Remember me on this computer
							</label>
						</div>
						<div class="pull-left">
							<div id="signInError" class="error-msg"></div>
							<button id="signIn" class="btn btn-primary" type="submit">Log In</button>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<div class="pull-left">
					<a data-dismiss="modal" data-toggle="modal" href="#forgetPasswordModal">Forgot Password?</a>
				</div>
				<div class="pull-right">
					<span>Don't have an account?</span>
					<a class="btn btn-danger" href="<?php echo $sitepathMarket; ?>signup">Register</a>
				</div>
			</div>
		</div>
	</div>
</div>