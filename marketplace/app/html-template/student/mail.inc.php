<div class="main-body">
	<div class="container">
		<div class="col-md-4 col-md-offset-4">
			<form method="post" action="https://passport.yandex.com/for/integro.io?mode=auth"> 
				<div class="form-group">
					<label for="">Username:</label>
					<input type="text" class="form-control" name="login" value="" tabindex="1"/>
				</div>
				<div class="form-group">
					<label for="">Password:</label>
					<input type="hidden" name="retpath" value="http://mail.yandex.com/for/integro.io">
					<input type="password" class="form-control" name="passwd" value="" maxlength="100" tabindex="2"/>
				</div>
				<div class="form-group">
					<div class="checkbox">
						<label for="a">
							<input type="checkbox" name="twoweeks" id="a" value="yes" tabindex="4"/>Don't remember me
						</label>
					</div>
				</div>
				<input type="submit" class="btn btn-primary" name="In" value="Log in" tabindex="5"/>
			</form>
		</div>
	</div>
</div>