<?php
	$status			= '';
	/*if ($_SERVER['REMOTE_ADDR'] == '111.125.224.158') {
		echo '$subPage = '.$subPage.'<br>';
		echo '$slug = '.$slug.'<br>';
		var_dump($_POST);
		var_dump($_SESSION);
		array(49) {
			["mihpayid"]=> string(9) "473488352"
			["mode"]=> string(2) "NB"
			["status"]=> string(7) "success"
			["unmappedstatus"]=> string(8) "captured"
			["key"]=> string(6) "bgvTnU"
			["txnid"]=> string(3) "311"
			["amount"]=> string(3) "3.0"
			["addedon"]=> string(19) "2015-11-28 12:00:15"
			["productinfo"]=> string(107) "{"ProductName":"Basics of Accounting - A Complete Study","amount":"3","courseId":"69","couponCode":"DIS99"}"
			["firstname"]=> string(16) "Jinendra Khobare"
			["lastname"]=> string(0) ""
			["address1"]=> string(0) ""
			["address2"]=> string(0) ""
			["city"]=> string(0) ""
			["state"]=> string(0) ""
			["country"]=> string(0) ""
			["zipcode"]=> string(0) ""
			["email"]=> string(18) "jkhobare@gmail.com"
			["phone"]=> string(10) "9561382956"
			["udf1"]=> string(0) ""
			["udf2"]=> string(0) ""
			["udf3"]=> string(0) ""
			["udf4"]=> string(0) ""
			["udf5"]=> string(0) ""
			["udf6"]=> string(0) ""
			["udf7"]=> string(0) ""
			["udf8"]=> string(0) ""
			["udf9"]=> string(0) ""
			["udf10"]=> string(0) ""
			["hash"]=> string(128) "fd613bb7141567eec55b792ff864fe9ad12c0b77ca436b34bf5e60f015fd9f0b35eb9122c70592e174ed64f6494b6220a5188fcedbd73e9dd1df5a389a92bed8"
			["field1"]=> string(0) ""
			["field2"]=> string(0) ""
			["field3"]=> string(0) ""
			["field4"]=> string(0) ""
			["field5"]=> string(0) ""
			["field6"]=> string(0) ""
			["field7"]=> string(119) "PRN=473488352&PID=000000036076&AMT=3.00&ITC=93854764&BID=41396465&PAID=Y&MD=P&BankId=026&Txn_Dt=2015-11-28 12:07:52.009"
			["field8"]=> string(0) ""
			["field9"]=> string(4) "Paid"
			["PG_TYPE"]=> string(3) "UBI"
			["encryptedPaymentId"]=> string(32) "691E548536E7CB5975EB7D9A73441A9A"
			["bank_ref_num"]=> string(8) "41396465"
			["bankcode"]=> string(4) "UBIB"
			["error"]=> string(4) "E000"
			["error_Message"]=> string(8) "No Error"
			["amount_split"]=> string(14) "{"PAYU":"3.0"}"
			["payuMoneyId"]=> string(8) "80484340"
			["discount"]=> string(4) "0.00"
			["net_amount_debit"]=> string(1) "3"
		}
		array(0) { }
	}*/
	if ($slug == "success") {
		$status = '';
		if ($subPage == 'course') {
			include '../api/Vendor/Arcanemind/Course.php';
			$product	= new Course();
		} elseif ($subPage == 'keys') {
			require_once('../api/Vendor/Arcanemind/Coursekeys.php');
			$product	= new Coursekeys();
		} elseif ($subPage == 'package') {
			require_once('../api/Vendor/Arcanemind/StudentPackage.php');
			$product	= new StudentPackage();
		}
		$posted = array();
		foreach($_POST as $key => $post) {
			$posted[$key] = $post;
		}
		/*if ($_SERVER['REMOTE_ADDR'] == '111.125.224.158') {
			echo $subPage."<br />";
			var_dump($posted);
			die();
		}*/
		$result			= $product->afterPayU($posted);
		if ($result->status == 1) {
			$result	= $result->data;
			$productDetails = json_decode($result['productinfo']);
		} else {
			$slug = "fail";
		}
	}
	if (empty($_POST) && empty($slug)) {
		header('location:'.$sitepath404);
	}
	if ($_SESSION['userRole'] != '4') {
		if ($_SESSION['userRole'] == '1') {
			$link = $sitepathManage;
		} else {
			$link = $sitePath.'admin/CourseKey.php';
		}
	} else {
		$link = $sitepathStudent;
	}
?>
<div class="main-body min-ht-800 mb20">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="pad20 bg-grey1">
					<table class="table table-bordered table-striped">
						<?php
							if ($slug == "success") {
						?>
						<tr class="success">
							<th colspan="2" class="text-center">Payment Successful</th>
						</tr>
						<tr>
							<th colspan="2" class="text-center">Order Details</th>
						</tr>
						<tr>
							<th class="text-right" width="50%">Order Date</th>
							<td><?php echo $result['date']; ?></td>
						</tr>
						<tr>
							<th class="text-right" width="50%">Order ID</th>
							<td><?php echo $result['txnid']; ?></td>
						</tr>
						<tr>
							<th class="text-right" width="50%">Product Name</th>
							<td><?php echo $productDetails->ProductName; ?></td>
						</tr>
						<tr>
							<th class="text-right" width="50%">Product Type</th>
							<td><?php echo ucfirst($subPage); ?></td>
						</tr>
						<?php
								if ($subPage == 'keys') {
						?>
						<tr>
							<th class="text-right" width="50%">Quantity</th>
							<td><?php echo $productDetails->Quantity; ?></td>
						</tr>
						<tr>
							<th class="text-right" width="50%">Rate</th>
							<td><i class="fa fa-rupee"></i><?php echo $productDetails->Rate; ?></td>
						</tr>
						<?php
								}
						?>
						<tr>
							<th class="text-right" width="50%">Total</th>
							<td><i class="fa fa-rupee"></i><?php echo number_format((float)$productDetails->amount, 2, '.', ''); ?></td>
						</tr>
						<?php
							} else if ($slug == "fail") {
						?>
						<tr class="danger">
							<th class="text-center">Payment Failed</th>
						</tr>
						<?php
							} else {
						?>
						<tr class="danger">
							<th class="text-center">Payment Cancelled</th>
						</tr>
						<?php
							}
						?>
					</table>
					<div class="text-center">
						<p>Goto <a href="<?php echo $link; ?>">Dashboard</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>