<div class="main-body">
	<div class="container">
		<div class="min-ht-500">
			<div class="pad20 bg-grey1 img-rounded mb20">
				<div class="profile-header img-rounded" id="coverPic">
					<div class="profile-header-in img-rounded">
						<div class="row">
							<div class="col-sm-3">
								<div class="pad20 pr">
									<div class="profile-image">
										<div class="pad20 bg-grey1 img-circle">
											<img src="" alt="" class="img-responsive img-circle" id="profilePic">
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="pad10">
									<div class="profile-meta-cover">
										<div class="profile-meta">
											<div class="profile-data">
												<div class="profile-data-name font-big mb10 text-uppercase" id="profileName"></div>
											</div>
											<ul class="list-inline lh-1">
												<li>
													<label for=""><i class="fa fa-phone"></i></label>
													<span id="contactNumber"></span>
												</li>
												<li>
													<label for=""><i class="fa fa-envelope"></i></label>
													<span id="primaryEmail"></span>
												</li>
												<li>
													<label for=""><i class="fa fa-transgender"></i></label>
													<span id="gender"></span>
												</li>
												<li>
													<label for=""><i class="fa fa-flag-checkered"></i></label>
													<span id="country"></span>
												</li>
												<li>
													<label for=""><i class="fa fa-map-marker"></i></label>
													<span id="address"></span>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<!-- <div class="col-sm-3">
								<div class="pad10">
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3577.5401191557357!2d72.98329799999999!3d26.276585!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39418e85ded0d5e1%3A0x173286c141ff28da!2sSaarthi+institute!5e0!3m2!1sen!2sin!4v1443922760380" width="100%" height="100" frameborder="0" style="border:0" allowfullscreen></iframe>
								</div>
							</div> -->
						</div>
					</div>
				</div>
			</div>
			<div class="row mb20">
				<div class="col-sm-9 col-sm-offset-3">
					<div id="userBio" class="data-more js-full-instructor-details"><p></p></div>
					<a href="javascript:void(0)" class="get-details btn-block mb20 js-instructor-details hide" data-more="full">Full details</a>
				</div>
				<!-- <div class="col-sm-3 text-right">
					<div class="btn-group">
						<a href="javascript:void(0)" class="btn btn-social disabled btn-facebook"><i class="fa fa-facebook"></i></a>
						<a href="javascript:void(0)" class="btn btn-social disabled btn-twitter"><i class="fa fa-twitter"></i></a>
						<a href="javascript:void(0)" class="btn btn-social disabled btn-google"><i class="fa fa-google-plus"></i></a>
						<a href="javascript:void(0)" class="btn btn-social disabled btn-linkedin"><i class="fa fa-linkedin"></i></a>
					</div>
				</div> -->
			</div>
		</div>
		<div>
			<div class="row mb20">
				<div class="col-sm-4"><hr class="style-one"></div>
				<div class="col-sm-4">
					<!-- Nav tabs -->
					<div class="clearfix text-center">
						<ul class="list-inline list-tabs clear-mrg" role="tablist">
							<li role="presentation" class="btn-w50 pull-left active">
								<a href="#courses" class="btn btn-color1 btn-block pr" aria-controls="home" role="tab" data-toggle="tab">
									<i class="arrow-right"></i><i class="arrow-left"></i>Courses
								</a>
							</li>
							<li role="presentation" class="btn-w50 pull-left">
								<a href="#packages" class="btn btn-color1 btn-block pr" aria-controls="profile" role="tab" data-toggle="tab">
									<i class="arrow-right"></i><i class="arrow-left"></i>Packages
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4"><hr class="style-one"></div>
			</div>

			<!-- Tab panes -->
			<div class="tab-content min-ht-500">
				<div role="tabpanel" class="tab-pane active" id="courses">
					<div class="row" id="trendingCourses"></div>
				</div>
				<div role="tabpanel" class="tab-pane" id="packages">
					<ul class="grid effect-7" id="grid"></ul>
				</div>
			</div>

		</div>
	</div>
</div>