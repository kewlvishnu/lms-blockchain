<div class="affix-spy-menu" id="jsCourseMenu">
	<nav id="course-navbar" class="navbar navbar-default course-navbar">
		<div class="container">
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="col-sm-8">
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="#description" class="course-sublinks">Description</a></li>
						<li><a href="#packageCourses" class="course-sublinks">Courses</a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div>
			<div class="col-sm-4">
				<?php if (isset($global->walletAddress) && !empty($global->walletAddress)) { ?>
				<a href="javascript:;" class="btn btn-lg btn-success js-take-package btn-crypto-op" data-wallet="<?php echo $global->walletAddress; ?>">Enroll Now</a>
				<span class="course-price font-md"></span>
				<?php } else { ?>
				<a href="javascript:;" class="btn btn-lg btn-success js-take-package btn-block btn-crypto-op">Enroll Now</a>
				<?php } ?>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
</div>
<section class="course-section">
	<article class="course-details">
		<div class="container invisible page-load-hidden">
			<div class="row">
				<div class="col-md-8">
					<div class="course-header">
						<h1 class="course-title" id="packageName"></h1>
						<div class="course-meta">
							<span class="author dib mb10">by <a href="javascript:void(0)" class="author-link mr5 js-package-institute-name"></a></span>
							<span class="dib mb10 mr5"><input type="hidden" class="rating ratingStars" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="5" disabled=""></span>
							<a href="javascript:void(0)" class="reviews-link scrolltodiv dib mb10" data-target="packageCourses"><span class="rating-count packageRatingTotal">0 ratings</span>, <span class="js-package-enrolled">0</span> students enrolled</a>
						</div>
					</div>
					<div class="course-showcase" id="packageShowcase">
						<!-- <video id="videoShowcasePlayer"></video> -->
						<!-- <video width="640" height="360" id="player2" poster="media/echo-hereweare.jpg" controls="controls" preload="none" style="width: 100%; height: 100%;">
							MP4 source must come first for iOS
							<source type="video/mp4" src="media/echo-hereweare.mp4" />
							WebM for Firefox 4 and Opera
							<source type="video/webm" src="media/echo-hereweare.webm" />
							OGG for Firefox 3
							<source type="video/ogg" src="media/echo-hereweare.ogv" />
							Fallback flash player for no-HTML5 browsers with JavaScript turned off
							<object width="640" height="360" type="application/x-shockwave-flash" data="flashmediaelement.swf" style="width: 100%; height: 100%;"> 		
								<param name="movie" value="flashmediaelement.swf" /> 
								<param name="flashvars" value="controls=true&poster=media/echo-hereweare.jpg&file=media/echo-hereweare.mp4" /> 		
								Image fall back for non-HTML5 browser with JavaScript turned off and no Flash player installed
								<img src="media/echo-hereweare.jpg" width="640" height="360" alt="Here we are" 
									title="No video playback capabilities" />
							</object>
						</video> -->
					</div>
				</div>
				<div class="col-md-4">
					<div class="buy-course mb20"> 
						<div class="pad20">
							<div class="course-price mb10 text-center font-md">
								<!-- <span class="price-discount"><i class="fa fa-rupee"></i>300</span>
								<span class="price-now"><i class="fa fa-rupee"></i>300</span> -->
							</div>
							<div class="mb10">
								<?php if (isset($global->walletAddress) && !empty($global->walletAddress)) { ?>
								<a href="javascript:;" class="btn btn-lg btn-success btn-block js-take-package btn-crypto-op" data-wallet="<?php echo $global->walletAddress; ?>">Enroll Now</a>
								<?php } else { ?>
								<a href="javascript:;" class="btn btn-lg btn-success btn-block js-take-package btn-crypto-app">Enroll Now</a>
								<?php } ?>
							</div>
							<div>
								<ul class="list-inline list-bordered text-center">
									<li><a href="<?php echo $sitepathPolicyTerms; ?>" target="_blank">Terms Of Use</a></li><li><a data-toggle="modal" href="#refundModal">Refund Policy</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="course-meta-data bt-grey-9">
						<ul class="list-inline list-meta">
							<li class="list-item-short">
								<label for=""><i class="fa fa-book"></i> Course ID :</label>
								<span class="value" id="PackageID"></span>
							</li>
							<li class="list-item-short">
								<label for=""><i class="fa fa-users"></i> Enrolled :</label>
								<span class="value js-package-enrolled"></span>
							</li>
							<li class="list-item-long">
								<label for=""><i class="fa fa-clock-o"></i> Courses in this Package :</label>
								<span class="value" id="noOfCourses"></span>
							</li>
							<li id="packageLiveEndDates"></li>
							<li class="list-item-long" id="msgPackageValidity">
								<div class="note"></div>
							</li>
						</ul>
					</div>
					<div class="social-share bt-grey-9 bb-grey-9 pr dib w100">
						<h5 class="social-title text-center text-uppercase">Share</h5>
						<ul class="list-inline dib w100 clear-mrg">
							<li class="social-item">
								<a href="javascript:void(0)" target="_blank" class="dib js-share-facebook"><i class="fa fa-facebook color-facebook"></i></a>
							</li>
							<li class="social-item">
								<a href="javascript:void(0)" target="_blank" class="dib js-share-twitter"><i class="fa fa-twitter color-twitter"></i></a>
							</li>
							<li class="social-item">
								<a href="javascript:void(0)" target="_blank" class="dib js-share-google"><i class="fa fa-google-plus color-google"></i></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-md-8">
					<div class="course-description mb20 affix-spy-adj" id="description">
						<h3 class="desc-title">Package Description</h3>
						<div class="js-full-course-details desc-content" id="jsDescription"></div>
						<a href="javascript:void(0)" class="get-details btn-block mb20 js-package-details hide" data-more="full">Full details</a>
					</div>
					<div class="course-index affix-spy-adj" id="packageCourses"></div>
				</div>
				<!-- <div class="col-md-4">
					<div class="about-instructor">
						<h4 class="instructor-title">About <a href="javascript:void(0)" class="js-package-institute-name"></a></h4>
						<h5 id="tagline"></h5>
						<div class="instructor-description js-full-instructor-details">
							<a href="#"><img class="instructor-avatar" src="#" alt="" id="instituteImg"></a><span id="instituteDescription"></span>
						</div>
						<a href="javascript:void(0)" class="get-details btn-block mb20 js-instructor-details hide" data-more="full">Full details</a>
					</div>
				</div> -->
			</div>
		</div>
	</article>
</section>