<?php
   header('Content-type: application/xml');
?>
<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
       
   <sitemap>
      <loc><![CDATA[<?php echo $sitepathMarket; ?>sitemap/navigation/]]></loc>
   </sitemap>
       
   <sitemap>
      <loc><![CDATA[<?php echo $sitepathMarket; ?>sitemap/courses/]]></loc>
   </sitemap>
       
   <sitemap>
      <loc><![CDATA[<?php echo $sitepathMarket; ?>sitemap/packages/]]></loc>
   </sitemap>
       
   <sitemap>
      <loc><![CDATA[<?php echo $sitepathMarket; ?>sitemap/instructors/]]></loc>
   </sitemap>
       
   <sitemap>
      <loc><![CDATA[<?php echo $sitepathMarket; ?>sitemap/institutes/]]></loc>
   </sitemap>
      
</sitemapindex>