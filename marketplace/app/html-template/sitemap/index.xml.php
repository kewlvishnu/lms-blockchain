<?php
	header('Content-type: application/xml');
	echo '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';

	//loading zend framework
	$libPath = "api/Vendor";
	include_once $libPath . "/Zend/Loader/AutoloaderFactory.php";
	require_once $libPath . "/ArcaneMind/Api.php";
	Zend\Loader\AutoloaderFactory::factory(array(
			'Zend\Loader\StandardAutoloader' => array(
					'autoregister_zf' => true,
					'db' => 'Zend\Db\Sql'
			)
	));

	switch ($page) {
		
		case 'navigation':
			require_once('navigation.inc.php');
			break;
		
		case 'courses':
			require_once('courses.inc.php');
			break;

		case 'packages':
			require_once('packages.inc.php');
			break;
		
		case 'instructors':
			require_once('instructors.inc.php');
			break;
		
		case 'institutes':
			require_once('institutes.inc.php');
			break;
		
		default:
			echo "There was some error";
			break;
	}
	echo '</urlset>';
?>