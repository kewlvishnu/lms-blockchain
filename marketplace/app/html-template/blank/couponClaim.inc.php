<?php
	require_once('api/Vendor/ArcaneMind/Contest.php');
	$ct = new Contest();
	$data = new stdClass();
	$data->uniqueCode = $slug;
	$res = $ct->getContestValidity($data);
	if (!$res->valid) {
		header('location:'.$sitepath404);
	}
?>
<section class="container">
	<h1 class="text-center">Coupon Claim</h1>
	<div class="row" id="couponClaim">
		<?php
			if (isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
				$data->userId  = $_SESSION['userId'];
				$couponDetails = $ct->getContestCoupon($data);
				if($couponDetails->status == 1) {
		?>
			<div class="col-sm-12">
				<div class="well well-lg text-center">
					<h3 class="jumbotron">Congratulations!</h3>
					<p>You have won a coupon to take any course upto Rs.3500/- for free!</p>
					<p>Here is your coupon code</p>
					<p><strong><?php echo $couponDetails->coupon; ?></strong></p>
					<p><a href="<?php echo $sitepathCourses; ?>" class="btn btn-info">Checkout Courses</a></p>
				</div>
			</div>
		<?php
				} else {
		?>
			<div class="col-sm-12">
				<p>There was some error!</p>
			</div>
		<?php
				}
			} else {
		?>
		<div class="col-sm-6">
			<form class="form-horizontal pad20" id="frmLogin" method="post" action="">
				<h3>I'm a returning user</h3>
				<div class="form-group">
					<div class="help-block text-center"></div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="text" class="form-control" name="inputUsername" id="inputStudentUsername" placeholder="Email">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="password" class="form-control" name="inputPassword" id="inputStudentPassword" placeholder="Password">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<button type="submit" class="btn btn-lg btn-success js-btn-login">Sign In</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-sm-6">
			<form class="form-horizontal pad20" id="frmSignup" method="post" action="">
				<h3>I'm a new user</h3>
				<div class="form-group">
					<div class="help-block text-center"></div>
				</div>
				<div class="form-group">
					<div class="col-sm-6">
						<input type="text" class="form-control" name="inputFirstName" id="inputFirstName" placeholder="First Name">
					</div>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="inputLastName" id="inputLastName" placeholder="Last Name">
					</div>
				</div>
				<div class="form-group has-feedback">
					<div class="col-sm-12">
						<input type="email" class="form-control" name="inputStudentSignupEmail" id="inputStudentSignupEmail" placeholder="Email" aria-describedby="inputSuccess1Status">
						<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
						<span id="inputSuccess1Status" class="sr-only">(success)</span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="password" class="form-control" name="inputStudentSignupPassword" id="inputStudentSignupPassword" placeholder="Password">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12 agreement">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="optionAgree" id="optionAgree"> I agree Integro's <a href="<?php echo $sitepathPolicyTerms; ?>">Terms of Use</a> and <a href="<?php echo $sitepathPolicyPrivacy; ?>">Privacy Policy</a>.
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<button type="submit" class="btn btn-lg btn-success pull-right">Sign Up</button>
					</div>
				</div>
			</form>
		</div>
		<?php
			}
		?>
	</div>
</section>