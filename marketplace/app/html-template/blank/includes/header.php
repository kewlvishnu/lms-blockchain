<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="<?php echo $description; ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="integro, online courses, courses anytime anywhere, free courses, study online, best learning platform,online test platform, multiple choice questions, education market place, education content, exam preparation, education videos, udemy, edx, coursera, blackboard, instructure, canvas">
		<meta http-equiv="cache-control" content="public">
		<title><?php echo $title; ?></title>

		<!-- facebook graph share properties -->
	    <meta property="og:title" content="<?php echo $fbTitle; ?>">
		<meta property="og:url" content="<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
		<meta property="og:site_name" content="Integro - Study online Courses"/>
		<meta property="og:description" content="<?php echo $description; ?>">
		<meta property="og:image" content="<?php echo $image; ?>">
		<base href="<?php echo $sitepathMarket; ?>">
		<!-- Bootstrap -->
		<link href="app/css/style.css" rel="stylesheet">
		<link rel="icon" href="<?php echo $sitepath; ?>img/favicon.ico" type="image/x-icon" >

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		</script>
	</head>
	<body>
		