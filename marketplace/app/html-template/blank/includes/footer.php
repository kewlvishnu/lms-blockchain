		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
		<script src="app/js/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="app/js/bootstrap.min.js"></script>
<?php
	require_once('app/jsphp/global.js.php');
	if (isset($page) && !empty($page)) {
		//require_once('app/jsphp/min/login.js.php');
		switch ($page) {
			case 'couponClaim':
				require_once('app/jsphp/couponClaim.js.php');
				break;
			default:break;
		}
	}
	@include_once "analytics.php";
?>
<?php
    if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){ ?>
		<!--Start of Tawk.to Script-->
		<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/58f86bb530ab263079b60876/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
		</script>
		<!--End of Tawk.to Script--><?php
    } ?>
	</body>
</html>