<div id="forgetPasswordModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close"  data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Forgot Password?</h3>
			</div>
			<div class="modal-body">
				<div class="login-form clearfix">
					<form role="form">
						<div class="form-group">
							<select class="form-control" id="fuserRole">
								<option value="2">I am an instructor</option>
								<option value="1">I represent an institute</option>
							</select>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="fuser" placeholder="Enter Email or Username">
							<div class="error-msg" id="fuserError"></div>
						</div>
						<div class="pull-left">
							<div class="error-msg" id="forgetError"></div>
							<a class="btn btn-success" id="resetPassword">Reset</a>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<div class="pull-left">
					<a data-dismiss="modal" data-toggle="modal" href="#loginModal">Login</a>
				</div>
				<div  class="pull-right">
					<span>Don't have an account?</span>
					<a href="signup-student.php#student" class="btn btn-primary">Register</a>
				</div>
			</div>
		</div>
	</div>
</div>