<?php
	require_once('includes/header.php');

	switch ($page) {
		case 'signup':
			require_once('signup.inc.php');
			break;
		
		case 'signupComplete':
			require_once('signupComplete.inc.php');
			break;
		
		case 'resetPassword':
			require_once('resetPassword.inc.php');
			break;
		
		case 'about':
			require_once('about.inc.php');
			break;
		
		default:
			echo "There was some error";
			break;
	}

	require_once('includes/footer-html.php');
	require_once('includes/footer-includes.php');
?>