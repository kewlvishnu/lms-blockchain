<div class="main-body">
	<div class="bg-featured section-featured">
		<h1 class="featured-title bg-trans-bg text-uppercase text-center">We are empowering teaching</h1>
	</div>
	<div class="pad-section text-center bg-grey1">
		<div class="container">
			<h2 class="mb20">Welcome To Integro</h2>
			<p>Integro.io is a Cloud Based Learning platform &amp; Market place, where Coaching institutes, Trainers, Schools and Colleges host education content &amp; conduct online tests with detailed analytics.</p>
			<p>The founders are a bunch of Mckinsey &amp; Harvard Alumni, who are really passionate about e-learning and how it can help Students/anyone with a zest for learning perform better in their career.</p>
		</div>
	</div>
	<div class="pad-section text-center">
		<div class="container">
			<h2>What do we offer?</h2>
			<h3 class="mb20">As a Coaching Institute/College/Instructor you will</h3>
			<div class="pad-section">
				<div class="row mb20">
					<div class="col-md-6">
						<div class="row">
							<div class="col-sm-4">
								<div class="pad10 icon-round bg-color1 color-grey1">
									<i class="fa fa-university"></i>
								</div>
							</div>
							<div class="col-sm-8 text-left">
								<p class="lead">Create Courses using Online Platform</p>
								<p><em>Create courses consisting of online assignments/exmas &amp; educational content on our cloud based platform.</em></p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-sm-4">
								<div class="pad10 icon-round bg-color1 color-grey1">
									<i class="fa fa-book"></i>
								</div>
							</div>
							<div class="col-sm-8 text-left">
								<p class="lead">Conduct Assignments and Exams</p>
								<p><em>Conduct online tests for students with detailed analytics like rank, percentage, time spent on a question, comparison with toppers and detailed graphs.</em></p>
							</div>
						</div>
					</div>
				</div>
				<div class="row mb20">
					<div class="col-md-6">
						<div class="row">
							<div class="col-sm-4">
								<div class="pad10 icon-round bg-color1 color-grey1">
									<i class="fa fa-desktop"></i>
								</div>
							</div>
							<div class="col-sm-8 text-left">
								<p class="lead">Host Educational Content</p>
								<p><em>Host educational content in the form of videos, documents and presentations.</em></p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-sm-4">
								<div class="pad10 icon-round bg-color1 color-grey1">
									<i class="fa fa-flask"></i>
								</div>
							</div>
							<div class="col-sm-8 text-left">
								<p class="lead">Reach Students Globally via Market Place</p>
								<p><em>Reach out to students across geographies for the courses you make.</em></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><?php /*
	<div class="bg-color1">
		<div class="container">
			<div id="testimonialsSlider" class="owl-carousel color-grey1 pad30">
				<div class="text-center">
					<div class="row mb10">
						<div class="col-md-8 col-md-offset-2">
							<div class="pad30 bg-grey1 color-color1 pr text-justify"><i class="arrow-down-msg"></i> <sup><i class="fa fa-quote-left font-big"></i></sup> Integro is an awesome place for Instructors like me who are passionate about teaching. The best part that I like about them is that they have an Instructor friendly revenue system. <sup><i class="fa fa-quote-right font-big"></i></sup></div>
						</div>
					</div>
					<div class="avatar-cover">
						<img src="<?php echo $sitepath; ?>img/testimonials/mehul.jpg" alt="Mehul Popat">
					</div>
					<h3>Mehul Popat</h3>
					<h5><em>(Instructor  and Expert in JAVA Spring Framework)</em></h5>
				</div>
				<div class="text-center">
					<div class="row mb10">
						<div class="col-md-8 col-md-offset-2">
							<div class="pad30 bg-grey1 color-color1 pr text-justify"><i class="arrow-down-msg"></i> <sup><i class="fa fa-quote-left font-big"></i></sup> Very happy with the overall support  provided by Integro. Not only are they open to ideas and suggestions, but the interface to upload courses is super quick. <sup><i class="fa fa-quote-right font-big"></i></sup></div>
						</div>
					</div>
					<div class="avatar-cover">
						<img src="<?php echo $sitepath; ?>img/testimonials/sunil.jpg" alt="Sunil Gupta">
					</div>
					<h3>Sunil Gupta</h3>
					<h5><em>(Instructor and Expert in C,Java and MangoDB)</em></h5>
				</div>
				<div class="text-center">
					<div class="row mb10">
						<div class="col-md-8 col-md-offset-2">
							<div class="pad30 bg-grey1 color-color1 pr text-justify"><i class="arrow-down-msg"></i> <sup><i class="fa fa-quote-left font-big"></i></sup> The best part about being an Institute on Integro is that you can invite multiple Instructors &amp; Instructors and ask them to make Subjects and Courses, in their area of expertise. This way you can have experts from different fields all together. <sup><i class="fa fa-quote-right font-big"></i></sup></div>
						</div>
					</div>
					<div class="avatar-cover">
						<img src="<?php echo $sitepath; ?>img/testimonials/gate.jpg" alt="GateOnline Forum">
					</div>
					<h3>GateOnline Forum</h3>
					<h5><em>(Institute for GATE Entrance Exam Preparation)</em></h5>
				</div>
			</div>
		</div>
	</div>*/ ?>
	<div class="text-center bg-color2 color-grey1">
		<div class="pad10">
			<p class="clear-mrg">We will love to hear from you. Reach out to us at <i class="fa fa-envelope"></i> contactus@integro.io or <i class="fa fa-whatsapp"></i> +91-9741436024</p>
		</div>
	</div>
</div>