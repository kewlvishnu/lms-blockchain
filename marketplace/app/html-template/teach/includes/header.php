<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="<?php echo $description; ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="integro, online courses, courses anytime anywhere, free courses, study online, best learning platform,online test platform, multiple choice questions, education market place, education content, exam preparation, education videos, udemy, edx, coursera, blackboard, instructure, canvas">
		<meta http-equiv="cache-control" content="public">
        <meta name="google-signin-client_id" content="636517984343-grgpvjcac86l8ms5j10ulkga9q4oemio.apps.googleusercontent.com">
		<title><?php echo $title; ?></title>

		<!-- facebook graph share properties -->
	    <meta property="og:title" content="<?php echo $fbTitle; ?>">
		<meta property="og:url" content="<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
		<meta property="og:site_name" content="Integro - Study online Courses"/>
		<meta property="og:description" content="<?php echo $description; ?>">
		<meta property="og:image" content="<?php echo $image; ?>">
		<base href="<?php echo $sitepathMarket; ?>">
<?php
	if (isset($page) && !empty($page)) {
		$customIncludes = '';
		$bodyAppend		= '';
		$sitetheme		= 'light';
		switch ($page) {
			case 'home':
				$customIncludes.=
				'<link rel="stylesheet" href="app/css/owl.carousel.min.css">
				<link rel="stylesheet" href="app/css/owl.theme.default.min.css">';
				$sitetheme	  = 'dark';
				break;
			case 'signup':
				$bodyAppend = 'class="bg-trans-bg dark-theme"';
				$sitetheme	= 'dark';
				break;
			case 'signupComplete':
				$bodyAppend = 'class="bg-trans-bg dark-theme"';
				$sitetheme	= 'dark';
				break;
			case 'resetPassword':
				$bodyAppend = 'class="bg-trans-bg dark-theme"';
				$sitetheme	= 'dark';
				break;
			case 'about':
				$customIncludes.=
				'<link rel="stylesheet" href="app/css/owl.carousel.min.css">
				<link rel="stylesheet" href="app/css/owl.theme.default.min.css">';
				break;
			default:break;
		}
		echo $customIncludes;
		if ($sitetheme == 'dark') {
			$headerAppend = 'home';
			$topNavAppend = 'home-top-nav';
			$searchAppend = 'border-trans trans-control';
			$searchBtnAppend = 'btn-trans';
		} else {
			$headerAppend = '';
			$topNavAppend = 'top-nav';
			$searchAppend = '';
			$searchBtnAppend = 'btn-color1';
		}
	}
?>
		<!-- Bootstrap -->
		<link href="app/css/style.css" rel="stylesheet">
		<link rel="icon" href="<?php echo $sitepath; ?>img/favicon.ico" type="image/x-icon" >

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body <?php echo $bodyAppend; ?>>
		<header class="header <?php echo $headerAppend; ?>">
			<nav class="navbar navbar-default <?php echo $topNavAppend; ?>">
				<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						</button>
						<?php echo $logoBlock ?>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li><a href="https://medium.com/integro" target="_blank">Blog</a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<?php
								@session_start();
								if(!(isset($_SESSION['userId']) && !empty($_SESSION['userId']))) {
									?>
									<li><a href="<?php echo $sitepathMarket; ?>">Student</a></li>
									<li><a href="<?php echo $sitepathMarket; ?>parents">Parent</a></li>
									<li><a href="<?php echo $sitepathMarket; ?>teach/signup/">Signup</a></li>
									<?php
								}
							?>
							<?php
								if(isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
									if($_SESSION['userRole'] == 4) {
										?>
										<li><a href="<?php echo $sitepathStudent; ?>">Dashboard</a></li>
										<li><a href="javascript:;" class="btn-logout">Logout</a></li>
										<?php
									}
									else if($_SESSION['userRole'] == 1){
										?>
										<li><a href="<?php echo $sitepathManage; ?>">Dashboard</a></li>
										<li><a href="javascript:;" class="btn-logout">Logout</a></li>
										<?php
									}
									else if($_SESSION['userRole'] == 2){
										?>
										<li><a href="<?php echo $sitepath; ?>admin/dashboard.php">Dashboard</a></li>
										<li><a href="javascript:;" class="btn-logout">Logout</a></li>
										<?php
									}
									else if($_SESSION['userRole'] == 3){
										?>
										<li><a href="<?php echo $sitepath; ?>admin/dashboard.php">Dashboard</a></li>
										<li><a href="javascript:;" class="btn-logout">Logout</a></li>
										<?php
									}
									else if($_SESSION['userRole'] == 5){
										?>
										<li><a href="<?php echo $sitepath; ?>admin/approvals.php">Dashboard</a></li>
										<li><a href="javascript:;" class="btn-logout">Logout</a></li>
										<?php
									}
									else if($_SESSION['userRole'] == 6){
										?>
										<li><a href="<?php echo $sitepath; ?>parent/">Dashboard</a></li>
										<li><a href="javascript:;" class="btn-logout">Logout</a></li>
										<?php
									}
								}
							?>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
		</header>