<?php
	// modals
	if ($page != 'home' && (!isset($_SESSION['userId']) || empty($_SESSION['userId']))) {
		require_once('app/html-template/teach/modals/login.html.php');
		require_once('app/html-template/teach/modals/forgotpassword.html.php');
	}
?>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="app/js/bootstrap.min.js"></script>
        <script src="https://apis.google.com/js/platform.js" async defer></script>
<?php
	require_once('app/jsphp/globalTeach.js.php');
	if (isset($page) && !empty($page)) {
		if ($page != 'home' && (!isset($_SESSION['userId']) || empty($_SESSION['userId']))) {
			require_once('app/jsphp/min/loginTeach.js.php');
		}
		$customIncludes = '';
		switch ($page) {
			case 'home':
				echo '<script src="../js/owl.carousel.min.js" type="text/javascript"></script>
					<script src="app/js/bootstrap-rating/bootstrap-rating.min.js" type="text/javascript"></script>';
				require_once('app/jsphp/homeTeach.js.php');
				break;
			case 'signup':
				require_once('app/jsphp/min/signupTeach.js.php');
				break;
			case 'resetPassword':
				require_once('app/jsphp/min/resetPasswordTeach.js.php');
				break;
			case 'about':
				echo '<script src="../js/owl.carousel.min.js" type="text/javascript"></script>
					<script src="../app/js/bootstrap-rating/bootstrap-ratings.min.js" type="text/javascript"></script>';
				require_once('app/jsphp/min/about.js.php');
				break;
			default:break;
		}
	}
	@include_once "analytics.php";
?>
<?php
    if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){ ?>
		<!--Start of Tawk.to Script-->
		<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/58f86bb530ab263079b60876/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
		</script>
		<!--End of Tawk.to Script--><?php
    } ?>
	</body>
</html>