<section class="main-body">
	<article class="container">
		<div class="col-md-6 col-md-offset-3">
			<form class="form-3d signup" id="studentSignup">
				<div class="text-center">
					<h3 class="form-title">Reset you password</h3>
				</div>
				<div class="form-group">
					<div class="help-block text-center"></div>
				</div>
				<div class="form-group">
					<label class="text-uppercase" for="inputPassword">Password</label>
					<input type="password" class="form-control trans-control" id="inputPassword" name="inputPassword" placeholder="Password">
				</div>
				<div class="form-group">
					<label class="text-uppercase" for="inputConfirmPassword">Password</label>
					<input type="password" class="form-control trans-control" id="inputConfirmPassword" name="inputConfirmPassword" placeholder="Confirm Password">
				</div>
				<button type="submit" class="btn btn-color2 btn-block text-uppercase" id="btnResetPassword">Reset Password</button>
				<hr class="style-two">
				<div class="text-center">
					Already have an account? <a href="#loginModal" data-toggle="modal">Login</a>
				</div>
			</form>
		</div>
	</article>
</section>