<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Integro | Learning Management System</title>

	<!-- Bootstrap -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	<link rel="stylesheet" href="app/css/lms.css">
	<base href="<?php echo $sitepathMarket; ?>">
	<link rel="icon" href="<?php echo $sitepath; ?>img/favicon.ico" type="image/x-icon" >

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<!-- HEADER
	============================================================================ -->
	<header class="site-loader" role="banner">
		
		<!-- NAVBAR
		============================================================================ -->
		<div class="navbar-wrapper">
			
			<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a href="/" class="navbar-brand"><img src="<?php echo $sitepathMarket; ?>img/arcanemind-logo.png" alt="Bootstrap to Wordpress"></a>
					</div><!-- navbar-header -->
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">
							<li class="active"><a href="/b2w">Home</a></li>
							<li><a href="blog.html">Course</a></li>
							<li><a href="resources.html">Resources</a></li>
							<li><a href="contact.html">Contact</a></li>
						</ul>
					</div><!-- navbar-collapse -->
				</div><!-- container -->

			</div><!-- navbar -->

		</div><!-- navbar-wrapper -->
	</header>
	<!-- HERO
	============================================================================ -->
	<section id="hero" data-type="background" data-speed="5">
		<article>
			<div class="container clearfix">
				<div class="row">

					<div class="col-md-12 hero-text">
						<div class="hero-box">
							<h1 class="jumbotron text-center">WHAT IS ARCANEMIND ?</h1>
							<p class="lead text-justify">integro.io is an empowered Learning portal where Schools, Colleges and Instructors can securely host educational content, courses (videos, pdf, PowerPoint presentations) &amp; conduct online exam/ assignments to augment classroom education. Each stakeholder receives custom analytics that help them improve their services.</p>
						</div>
					</div>

					<div class="col-md-12 hero-text">
						<div class="hero-box">
							<h1 class="jumbotron text-center">Our Vision</h1>
							<p class="lead text-justify">To empower learning through quality educational inputs and its effective analysis</p>
						</div>
					</div>

					<div class="col-md-12 hero-text">
						<div class="hero-box">
						<h1 class="jumbotron text-center">Our Mission</h1>
							<p class="lead text-justify">To facilitate institutions in leapfrogging traditional academic models and enhance their potential by harnessing effective digital learning.</p>
							<p class="lead text-justify">To empower Instructors in easy dissemination of their professional knowledge including to distant location while evaluating its effectiveness To inculcate learning ethos among student through easy availability of study material while developing their competitive edge</p>
						</div>
					</div>

				</div><!-- row -->
			</div><!-- container -->
		</article>
	</section><!-- hero -->
	<!-- COURSE
	============================================================================ -->
	<section id="course" data-type="background" data-speed="5">
		<article>
			<div class="container clearfix">
				<div class="row">

					<div class="col-md-12 hero-text">
						<h1 class="jumbotron text-center">COURSE CONTENT CREATION</h1>
					</div>
					<div class="col-md-6">
						<p class="lead text-justify">We support various kinds of course content creation :</p>
						<ul>
							<li>Videos
								<ul>
									<li>Video file mp4 and other popular video formats</li>
									<li>YouTube Videos</li>
									<li>Vimeo Videos</li>
								</ul>
							</li>
							<li>Power point presentations files</li>
							<li>Pdf documents</li>
							<li>Text document</li>
							<li>Downloadable material</li>
						</ul>
					</div>
					<div class="col-md-6 hero-text">
						<img src="<?php echo $sitepathMarket; ?>app/img/lms/screen1.jpg" alt="" class="img-responsive">
					</div>
				</div><!-- row -->
			</div><!-- container -->
		</article>
	</section><!-- hero -->
	<!-- EXAMS
	============================================================================ -->
	<section id="exams" data-type="background" data-speed="5">
		<article>
			<div class="container clearfix">
				<div class="row">

					<div class="col-md-12 hero-text">
						<h1 class="jumbotron text-center">EXAMS AND ASSIGNMENTS</h1>
					</div>
					<div class="col-md-6 hero-text">
						<img src="<?php echo $sitepathMarket; ?>app/img/lms/screen2.jpg" alt="" class="img-responsive">
					</div>
					<div class="col-md-6">
						<p class="lead text-justify">We support various kinds of exams and assignments :</p>
						<ul>
							<li>An exam/assignment can be designed to have multiple Sections and in multiple languages like Hindi, Marathi, German, French etc…</li>
							<li>Different types of questions can be added, like
								<ul>
									<li>Multiple Choice Questions</li>
									<li>Multiple Answer Questions</li>
									<li>True and False</li>
									<li>Match the Following</li>
									<li>Comprehension Type Question</li>
									<li>Dependent Type Questions (sub parts)</li>
								</ul>
							</li>
							<li>Sequence of the questions in the exam will be different for each students, reducing the chances of copying</li>
							<li>In case of power failures the answers are not lost, and the exam can be resumed</li>
							<li>Instructors can create Exams and assignments that support one or multiple attempts</li>
							<li>Results can be displayed after each attempt or after the end of exams</li>
							<li>Results will be displayed in a graphically intuitively manner, along with real-time class performance</li>
							<li>Exam front end is similar to IIT/Gate/CAT exams</li>
							<li>Work load reduces incredibly when the subjects/contents and exams could be imported from the previous years, with simply a click of a button.</li>
						</ul>
					</div>
				</div><!-- row -->
			</div><!-- container -->
		</article>
	</section><!-- hero -->
	<!-- ANALYTICS
	============================================================================ -->
	<section id="analytics" data-type="background" data-speed="5">
		<article>
			<div class="container clearfix">
				<div class="row">

					<div class="col-md-12 hero-text">
						<h1 class="jumbotron text-center">ANALYTICS</h1>
					</div>
				</div><!-- row -->
			</div><!-- container -->
		</article>
	</section><!-- hero -->

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>