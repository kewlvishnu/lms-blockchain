<script type="text/javascript">
$(document).ready(function(){
	$('.js-user-type').click(function(){
		var userType = $(this).attr('data-type');
		if(userType == "institute") {
			$('.js-professor').addClass('hide');
			$('.js-institute').removeClass('hide');
		} else if(userType == "professor") {
			$('.js-institute').addClass('hide');
			$('.js-professor').removeClass('hide');
		}
	});
	$(".form-3d .form-control").keyup(function(e){
		if(e.which != 9) {
			var disError = $(this).closest("form").find(".help-block");
			disError.html('').closest('.form-group').removeClass('has-success').removeClass('has-error');
		}
	});
	var formProfessor = {
		'type' : "Professor",
		'userInstituteTypes' : [ ],
		'stucturedInsTypes' : { },
		'addressCountryId' : 1,
		'userSchoolBoard' : 0,
		'userSchoolClasses' : 0,
		'agreeOn1' : 0,
		'agreeOn2' : 0,
		'action' : 'register-short',
		'userType' : 'professor',
		'userRole' : 2,
		'gender' : 0
	};
	var formInstitute = {
		'type' : "Institute",
		'userInstituteTypes' : [ ],
		'stucturedInsTypes' : { },
		'addressCountryId' : 1,
		'userSchoolBoard' : 0,
		'userSchoolClasses' : 0,
		'agreeOn1' : 0,
		'agreeOn2' : 0,
		'action' : 'register-short',
		'userType' : 'institute',
		'userRole' : 1
	};
	function emailValidation(element,user) {
		var frmGroup=element.closest('.form-group');
		var disError=element.closest('form').find('.help-block');
		var frmError=disError.closest('.form-group');
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(regex.test(element.val())) {
			frmGroup.removeClass('has-error');
			var req = {};
			req.action = 'validate-email';
			req.email = element.val();
			req.role = user.userRole;
			req=JSON.stringify(req);
			$.post(ApiEndpoint, req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.valid == false) {
					frmGroup.removeClass('has-success').addClass('has-error');
					disError.html("Email address already registered please select a different one.");
					frmError.removeClass('has-success').addClass('has-error');
					//element.focus();
				}
				else {
					frmGroup.removeClass('has-error').addClass('has-success');
					disError.html("");
					frmError.removeClass('has-success').removeClass('has-error');
					return true;
				}
			});
		}
		else {
			frmGroup.removeClass('has-success').addClass('has-error');
			disError.html("Doesn't seem to be a valid email address.");
			frmError.removeClass('has-success').addClass('has-error');
			//element.focus();
		}
		return false;
	}
	$("#inputEmailAddress").blur(function(){
		if ($('[name=optionUserType]:checked').attr('data-type') == "professor") {
			emailValidation($(this),formProfessor);
		} else {
			emailValidation($(this),formInstitute);
		};
	});
	$("#btnTeachSignup").click(function(){
		var disError 				= $("#teachSignup").find('.help-block');
		var frmError 				= disError.closest('.form-group');
		var userType				= $('[name=optionUserType]:checked').attr('data-type');
		if (userType == "professor") {
			var inputFirstName		= $("#inputFirstName");
			var inputLastName		= $("#inputLastName");
		} else {
			var inputInstituteName	= $("#inputInstituteName");
		}
		var inputEmailAddress		= $("#inputEmailAddress");
		var inputPassword			= $("#inputPassword");
		var selectCountry 			= $("#selectCountry");
		var inputContactMobile		= $("#inputContactMobile");
		if(userType == "professor" && inputFirstName.val().length<3) {
			disError.html("First Name is invalid.");
			frmError.removeClass('has-success').addClass('has-error');
			inputFirstName.focus();
		} else if(userType == "professor" && inputLastName.val().length<3) {
			disError.html("Last Name is invalid.");
			frmError.removeClass('has-success').addClass('has-error');
			inputLastName.focus();
		} else if(userType == "institute" && inputInstituteName.val().length<3) {
			disError.html("Institute Name is invalid.");
			frmError.removeClass('has-success').addClass('has-error');
			inputInstituteName.focus();
		} else if($(inputEmailAddress).closest('.form-group').hasClass('has-error') ||
					(!$(inputEmailAddress).closest('.form-group').hasClass('has-error') && !$(inputEmailAddress).closest('.form-group').hasClass('has-success'))
				) {
			if (userType == "professor") {
				emailValidation(inputEmailAddress,formProfessor);
			} else {
				emailValidation(inputEmailAddress,formInstitute);
			}
		} else if(inputPassword.val().length<6) {
			disError.html("A valid password is required.");
			frmError.removeClass('has-success').addClass('has-error');
			inputPassword.focus();
		} else if(!selectCountry.val()) {
			disError.html("Country is required.");
			frmError.removeClass('has-success').addClass('has-error');
			selectCountry.focus();
		} else if(!inputContactMobile.val()) {
			disError.html("Mobile number is required.");
			frmError.removeClass('has-success').addClass('has-error');
			inputContactMobile.focus();
		} else {
			if (userType == "professor") {
				formProfessor.firstName 	  = inputFirstName.val();
				formProfessor.lastName 		  = inputLastName.val();
				formProfessor.email 		  = inputEmailAddress.val();
				formProfessor.password 		  = inputPassword.val();
				formProfessor.addressCountryId= selectCountry.val();
				formProfessor.contactMobilePrefix = $('#inputPrefixContactMobile').val();
				if(!$('#inputPrefixContactMobile').val())
					formProfessor.contactMobilePrefix="+91";
				formProfessor.contactMobile   = inputContactMobile.val();
				formProfessor 				  = JSON.stringify(formProfessor);
				$.post(ApiEndpoint, formProfessor).success(function(resp) {
					res=jQuery.parseJSON(resp);
					if(res.status == 1)
						window.location=sitepathMarket+"signup/complete/" + res.userId;
					if(res.status == 0)
						alert(res.message);
				});
			} else {
				formInstitute.instituteName	  = inputInstituteName.val();
				formInstitute.email 		  = inputEmailAddress.val();
				formInstitute.password 		  = inputPassword.val();
				formInstitute.addressCountryId= selectCountry.val();
				formInstitute.contactMobilePrefix = $('#inputPrefixContactMobile').val();
				if(!$('#inputPrefixContactMobile').val())
					formInstitute.contactMobilePrefix="+91";
				formInstitute.contactMobile   = inputContactMobile.val();
				formInstitute 				  = JSON.stringify(formInstitute);
				$.post(ApiEndpoint, formInstitute).success(function(resp) {
					res=jQuery.parseJSON(resp);
					if(res.status == 1)
						window.location=sitepathMarket+"signup/complete/" + res.userId;
					if(res.status == 0)
						alert(res.message);
				});
			}
		}
		return false;
	});
});
</script>