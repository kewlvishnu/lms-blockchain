<script type="text/javascript">
var jcrop_api;
var userRole;
var newSelectedInstitutes = {};
var schema	= {
  "@context": "http://schema.org",
  "@type": "Person",
  "address": {
    "@type": "PostalAddress",
    "addressCountry": "",
    "addressLocality": "",
    "addressRegion": "",
    "postalCode": "",
    "streetAddress": ""
  },
  "email": "",
  "image": "",
  "jobTitle": "Instructor",
  "name": "",
  "gender": "",
  "telephone": "",
  "url": ""
};

function fetchProfileDetails() {
	var req = {};
	var res;
	req.action = "get-profile-details";
	req.professorId = slug;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		
		res =  $.parseJSON(res);
		fillProfileDetails(res);
		
	});
}

function fillProfileDetails(data) {
	// General Details
	var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
	var gender = "Not Specified.";
	if(data.profileDetails.gender == 1)
		gender = "Male";
	if(data.profileDetails.gender == 2)
		gender = "Female";
	$('#gender').text(gender);
	$('#profileName').text(name);
	
	$('#contactNumber').text(formatContactNumber(data.userDetails));
	$('#primaryEmail').text(data.loginDetails.email);
	$('#country').text(data.userDetails.addressCountry);
	$('#address').text(formatAddress(data.userDetails));

	var imageRoot = '../admin/user-data/images/';

	//$('#user-type').text(formatUserRole(data.userRole));
	if(data.profileDetails.coverPic != "")
		$('#coverPic').css('background-image', 'url("'+data.profileDetails.coverPic+'")');
	if(data.profileDetails.profilePic != "") {
		$('#profilePic').attr('src', data.profileDetails.profilePic);
	}
	if(data.profileDetails.description != ""){
		$('#userBio p').html(data.profileDetails.description.replace(new RegExp('\r?\n','g'), '</p><p>'));
		//$('#userBio').val(data.profileDetails.description);
		if($('#userBio p').height()>100) {
			$('.js-instructor-details').removeClass("hide");
		}
	} else {
		$('#userBio').text("No description specified");
	}

	schema.name		= name;
	schema.email	= data.loginDetails.email;
	schema.gender	= gender;
	schema.image	= data.profileDetails.profilePic;
	schema.telephone= formatContactNumber(data.userDetails);
	schema.url		= sitepathMarket+'instructor/'+slug;
	schema.address.addressLocality	= data.userDetails.addressCity;
	schema.address.addressRegion	= data.userDetails.addressState;
	schema.address.postalCode		= data.userDetails.addressPin;
	schema.address.streetAddress	= data.userDetails.addressStreet;
	schema.address.addressCountry	= data.userDetails.addressCountry;
	
	//create JSON-LD for lastModified and citation_online_date
	var el = document.createElement('script');  
	el.type = 'application/ld+json';  
	el.id = 'jsonld';
	el.text = JSON.stringify(schema); 
	$('body').append(el);

	var html = '';
	var html = fillTemplateCourses(data);
	$('#trendingCourses').html(html);
	$('.rating').rating();
	
	if (data.packages.status == 1) {
		fillFeaturedPackages(data.packages.packages, 'grid');
	} else {
		$('#packages').html('<p>No packages by this instructor</p>');
	};
	
	// Role Specific
	userRole = data.userRole;
	removeLoader();
}

$(".js-instructor-details").click(function(){
	toggleDataMore($(this),$(".js-full-instructor-details"));
});

function formatUserRole(roleId) {
	if(roleId == '1')
		return "Institute";
	if(roleId == '2')
		return "Instructor";
	if(roleId == '3')
		return "Publisher";
}

function formatAddress(user) {
	var address = '';
	if(user.addressStreet != '')
		address += user.addressStreet + ', ';
	if(user.addressCity != '')
		address += user.addressCity + ', ';
	if(user.addressState != '')
		address += user.addressState + ', ';
	if(user.addressPin != '')
		address += user.addressPin + ', ';
	ret = address.replace(/,\s$/, '.');
	if(ret == '')
		return 'Not Specified.';
	else
		return ret;
}

function formatUserInstituteTypes(insTypes) {
	var arrangedIns = {};
	$.each(insTypes, function(i,ins) {
		if(ins.parentId == 0 ) {
			if(typeof arrangedIns[ins.id] !== "undefined" ) {
				var children = arrangedIns[ins.id]['children'];
				arrangedIns[ins.id] = {
					'data': ins.data,
					'name': ins.name,
					'children': children
				}
			}else {
				arrangedIns[ins.id] = {
					'data': ins.data,
					'name': ins.name,
					'children': {}
				}
			}
		}else {
			if(typeof arrangedIns[ins.parentId] == "undefined") {
				arrangedIns[ins.parentId] = {
					'children' : {},
				};
			}
			
			arrangedIns[ins.parentId]['children'][ins.id] = {
				'data': ins.data,
				'name': ins.name,
				'children': {}
			}
		}
	});
	console.log(arrangedIns);
	var html = "";
	var data;
	if(typeof arrangedIns['1'] !== "undefined") {
		var ins = arrangedIns['1'];
		data = $.parseJSON(ins.data);
		html += '<div class="panel-body bio-graph-info margin-top-20">'
				+ '<h1>' + ins.name + '</h1>'
				+ '<div class="row">'
				+ '<div class="col-lg-6 three-row">' + formatSchoolBoard(data) + '</div>'
				+ '<div class="col-lg-6 three-row">' + formatSchoolClass(data) + '</div>'
				+ "</div></div>";
	}
	
	if(typeof arrangedIns['2'] !== "undefined") {
		var ins = arrangedIns['2'];
		html += '<div class="panel-body bio-graph-info margin-top-20">'
				+ '<h1>' + ins.name + '</h1>'
				+ '<div class="row">'
				+ '<div class="col-lg-12 three-row">' + formatCoaching(ins, arrangedIns) + '</div>'
				+ "</div></div>";
	}
	
	if(typeof arrangedIns['3'] !== "undefined") {
		var ins = arrangedIns['3'];
		html += '<div class="panel-body bio-graph-info margin-top-20">'
				+ '<h1>' + ins.name + '</h1>'
				+ '<div class="row">'
				+ '<div class="col-lg-12 three-row">' + formatColleges(ins) + '</div>'
				+ "</div></div>";
	}
	
	if(typeof arrangedIns['4'] !== "undefined") {
		var ins = arrangedIns['4'];
		var name =  userRole == 1 ? "Training Institute": "Other Categories";
		html += '<div class="panel-body bio-graph-info margin-top-20">'
				+ '<h1>' + name + '</h1>'
				+ '<div class="row">'
				+ '<div class="col-lg-12 three-row">' + formatTrainingIns(ins) + '</div>'
				+ "</div></div>";
	}
	return html;
}

function formatSchoolBoard(data) {
	if(data.boardId == '5')
		return "Board: CBSE";
	if(data.boardId == '6')
		return "Board: ICSE";
	if(data.boardId == '7')
		return "Board: State Board(" + data.state + ")";
	if(data.boardId == '0')
		return "Board: " + data.other;
	return " ";
}

function formatSchoolClass(data) {
	return "Classes upto " + data.classes + "th" ;
}

function formatColleges(ins) {
	var html = "";
	$.each(ins.children, function(k,v) {
		html += v.name + ', ';
	});
	data = $.parseJSON(ins.data);
	var others = data.other;
	if( others != "" && others != undefined)
		html += others;
	return html.replace(/,\s$/,'.');
}

function formatTrainingIns(ins) {
	var html = "";
	$.each(ins.children, function(k,v) {
		html += v.name + ', ';
	});
	data = $.parseJSON(ins.data);
	var others = data.other;
	if( others != "" && others != undefined)
		html += others;
	return html.replace(/,\s$/,'.');
}

function formatCoaching(ins, arrangedIns) {
	var html = "";
	var others;
	$.each(ins.children, function(k,v) {
		html += "<div><b>" + v.name + ': </b>';
		if(typeof arrangedIns[k] !== "undefined"){
			$.each(arrangedIns[k].children, function(subK,subC) {
				html += subC.name + ", ";
			});
		}
		data = $.parseJSON(v.data);
		others = data.other;
		if( others != "" && others != undefined)
			html += others;
		html = html.replace(/,\s$/, '.') + "</div>";
	});
	data = $.parseJSON(ins.data);
	others = data.other;
	if( others != "" && others != undefined)
		html += "<div> <b>" + others + "</b></div>";
	return html;
}

function prepareInsDataKey(k) {
	if(k == 'other')
		return '';
	else
		return k + ': ';
}

function updateInstituteTypes(elem) {
	$('.form-msg').html('').hide();
	var newStructuredInstitutes = {};
	var board;
	if(typeof newSelectedInstitutes[1] == "undefined"
		&& typeof newSelectedInstitutes[2] == "undefined" 
		&& typeof newSelectedInstitutes[3] == "undefined" 
		&& typeof newSelectedInstitutes[4] == "undefined") {
			$('.form-msg.overall').html('Please select a category').show();
			return ;
	}
	if(typeof newSelectedInstitutes[1] !== "undefined"){
		board = $('#school-board').val();
		state = $('#school-board-state input[type=text]').val();
		otherBoard = $('#school-board-other input[type=text]').val();
		classes = $('#school-classes').val();
		if(board == 0){
			$('.form-msg.board').html('Please select a board').show();
			return;
		}
		if(board == 7 && state.length < 2) {
			$('.form-msg.board-state').html('Invalid state name.').show();
			return;
		}
		if(board == -1 && otherBoard == "") {
			$('.form-msg.board-other').html('Please specify a board.').show();
			return;
		}
		if(classes == 0) {
			$('.form-msg.classes').html('Please select classes.').show();
			return;
		}
		//Validated
		if(board == 7) {
			data = {
				"boardId" : "7",
				"state" : state
			};
		} else if(board == -1) {
			data = {
				"boardId" : "0",
				"other" : otherBoard
			};
		} else {
			data = {
				"boardId" : board
			};
		}
		data["classes"] = classes;
		newStructuredInstitutes[1] = data;
	}
	if(typeof newSelectedInstitutes[2] !== "undefined") {
		selectedSubcat = getSelectedSubcat(2);
		if(countDictKeys(selectedSubcat) == 0) {
			$('.form-msg.coaching').html('Please select a coaching type.').show();
			return;
		}
		newStructuredInstitutes[2] = {};
		inner = true;
		$.each(selectedSubcat, function(k,v) {
			selectedSubcat2 = getSelectedSubcat(k);
			if(countDictKeys(selectedSubcat2) == 0
				&& typeof newSelectedInstitutes[k]["other"] == "undefined") {
				$('.form-msg.' + k + '-msg').html('Please select a type.').show();
				inner = false;
			}
			if(typeof newSelectedInstitutes[k]["other"] !== "undefined"
				&& $('#ins-' + k + '-other input[type=text]').val() == "") {
				$('.form-msg.' + k + '-msg').html('Please specify a new type.').show();
				inner = false;
			}
			if(typeof newSelectedInstitutes[k]["other"] === "undefined") {
				newStructuredInstitutes[k] = {};
			} else {
				newStructuredInstitutes[k] = {
					"other" : $('#ins-' + k + '-other input[type=text]').val()
				};
			}
			$.each(selectedSubcat2, function(subK,subV) {
				newStructuredInstitutes[subK] = subV;
			});
		});
		if(!inner)
			return;
	}
	if(typeof newSelectedInstitutes[3] !== "undefined") {
		selectedSubcat = getSelectedSubcat(3);
		if(countDictKeys(selectedSubcat) == 0 && typeof newSelectedInstitutes[3]["other"] == "undefined") {
			$('.form-msg.college').html('Please select a college type.').show();
			return;
		}
		if(typeof newSelectedInstitutes[3]["other"] !== "undefined"
			&& $('#ins-3-other input[type=text]').val() == "") {
			$('.form-msg.college-other').html('Please specify new college type.').show();
			return;
		}
		if(typeof newSelectedInstitutes[3]["other"] === "undefined") {
			newStructuredInstitutes[3] = {};
		} else {
			newStructuredInstitutes[3] = {
				"other" : $('#ins-3-other input[type=text]').val()
			};
		}
		$.each(selectedSubcat, function(k,v){
			newStructuredInstitutes[k] = v;
		});
	}
	if(typeof newSelectedInstitutes[4] !== "undefined") {
		selectedSubcat = getSelectedSubcat(4);
		if(countDictKeys(selectedSubcat) == 0 && typeof newSelectedInstitutes[4]["other"] == "undefined") {
			$('.form-msg.training').html('Please select a training type.').show();
			return;
		}
		if(typeof newSelectedInstitutes[4]["other"] !== "undefined"
			&& $('#ins-4-other input[type=text]').val() == "") {
			$('.form-msg.training-other').html('Please specify new training type.').show();
			return;
		}
		if(typeof newSelectedInstitutes[4]["other"] === "undefined") {
			newStructuredInstitutes[4] = {};
		} else {
			newStructuredInstitutes[4] = {
				"other" : $('#ins-4-other input[type=text]').val()
			};
		}
		$.each(selectedSubcat, function(k,v){
			newStructuredInstitutes[k] = v;
		});
	}
	console.log(newStructuredInstitutes);
	var req = {};
	req.newStructuredInstitutes = newStructuredInstitutes;
	req.action = "update-user-institute-type";
	elem.attr('disabled',true);
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 1) {
			fetchProfileDetails();
			alertMsg(res.message);
			elem.parents('.panel').find('.edit-section').hide();
			elem.parents('.panel').find('.view-section').fadeIn('fast');
		}else {
			alertMsg(e);
		}
		elem.attr('disabled',false);
		elem.parents('section.panel').find('.fa-times').removeClass('fa-times').addClass('fa-pencil');
	});
}
	
	

function alertMsg(m) {
	alert(m);
}

function getSelectedSubcat(catId) {
	var catHierarchy = {
		"1": [5,6,7],
		"2": [8,9,10,11,12],
		"3": [46,47,48,49,50,51,52,53],
		"4": [54,55,56,57,58,59,60,61,62,63],
		"8": [13,14,15,16,17],
		"9": [18,19,21,22],
		"10": [23,24,25,26,27],
		"11": [28,29,30,31,32,33,34],
		"12": [35,36,37,38,39,40,41,42,43,44,45]
	};
	var subCats = catHierarchy[catId];
	var selectedSubcat = {};
	$.each(subCats, function(i,v) {
		if(typeof newSelectedInstitutes[v] !== "undefined") {
			selectedSubcat[v] = newSelectedInstitutes[v];
		}
	});
	return selectedSubcat;
}

function countDictKeys(c){
	var count = 0;
	for (var i in c) {
	   if (c.hasOwnProperty(i)) count++;
	}
	return count;
}
</script>