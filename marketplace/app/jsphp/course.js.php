<script type="text/javascript">
var MONTH = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var professors = '';
var loginstatus = 0;
var userId = 0;  
var userRole = 0;
//var courseId = getUrlParameter("courseId");
var paymentPageStudent = true;
var subjectCount = 0;
var api = false;
var couStatus=0;
var couponCode="";
var originalPrice=0;
var couponprice=0;
var enrollstatus=0;
var instituteId = 0;
var instituteName = '';
var errmsg=0;
var player;
var schema = {
			  "@context": "http://schema.org",
			  "@type": "Product",
			  "aggregateRating": {
			    "@type": "AggregateRating",
			    "ratingValue": "",
			    "reviewCount": ""
			  },
			  "description": "",
			  "name": ""
			};

$(document).ready(function () {
	//var enrollstatus=CourseEnrolled();
	//checkCryptoApp();
	//event listener for pay u button click
	$('#couponswid').on('click', '#couponapply', function(){
		var req = {};
		var res = null;

		var couponcode =  $('#couponcode').val().toUpperCase().trim();
		if(couponcode == '') {
			if(errmsg == 0)	{
				$('#couponcode').closest('.row').addClass('has-error').find('.help-block').html('Invalid Coupon Code');
				errmsg = 1;
			}
			return false;
		}

		var couponcode=  $('#couponcode').val().toUpperCase();

		var today		= new Date().getTime();

		// req.courseId	= '34';
		req.slug		= slug;
		req.couponCode	= couponcode;
		req.date		= today;
		req.action		= 'redeemCoupon';
		couponCode		= couponcode;
		if(req.slug != undefined) {
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data'  : JSON.stringify(req)
			}).done(function (res){
				data = $.parseJSON(res);
				if(data.endDate > today && data.startDate < today && (data.noOfCoupons-data.couponsUsed)>0)	{
					couStatus		= 1;
					coupons			= data;
					couponprice		= (originalPrice*((100-data.DiscountPer)/100)).toFixed(2);
					$('.course-price').html('<strike class="price-discount">' + originalPrice + ' ' + dispcurrency + '</strike>' + '<span class="price-now">' + couponprice + ' ' + dispcurrency + '</span>');
					
					$('#couponswid').html('<div class="removeCoupon">Coupon Applied sucessfully <a id="removeCoupon" href="javascript:void(0)"> Remove Coupon</a></div>');
					
				} else {
					if(errmsg == 0) {
						$('#couponcode').closest('.row').addClass('has-error').find('.help-block').html('Invalid Coupon Code');
						errmsg = 1;
						return false;
					}
				}
		 	});
		} else {
			toastr.error("No course Details");
		}
	});
	$('#couponswid').on('click', '#removeCoupon', function(){
		//$('#couponswid').html('<ul class="list-inline"><div class="col-md-5"><button type="button" id="couponapply" class="btn btn-info btn-xs">Apply</button></div><div class="col-md-5"><input type="text" class="form-control" id="couponcode"></li></div></ul>');
		couStatus	= 0;
		couponprice	= 0;
		couponCode	= '';
		$('.course-price').html('<span class="price-now">' + originalPrice + ' ' + dispcurrency + '</span>');
		$('#couponswid').html('<div class="input-group">'+
									'<input type="text" name="coupon" value="" id="couponcode" class="form-control couponCodeC" placeholder="Redeem a Coupon">'+
									'<span class="input-group-btn">'+
										'<button class="btn btn-info" type="button" id="couponapply">Apply</button>'+
									'</span>'+
								'</div><!-- /input-group -->'+
								'<span class="help-block text-center"></span>');
	});
	$('#payUMoneyButton').on('click', function() {
		var today		= new Date().getTime();
		var req 		= {};
		var res;
		req.action		= 'purchaseCourseViaPayU';
		//req.courseId	= '34';
		req.slug		= slug;
		req.date		= today;
		req.couponCode	= couponCode;
		req.couStatus	= couStatus;
		$.ajax({
			'type' : 'post',
			'url'  : ApiEndpoint,
			'data' : JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				if(res.paymentSkip == 1) {
					window.location = sitePathStudent;
				} else {
					// Metamask Payment here
					//$('#purchaseNowModal').modal('hide');
                    transferIGRO(res.amount,res.IGROTxnID);
                    toastr.warning('Please do not refresh without paying!', 'Course Payment', {timeOut: 3000});
				}
			} else {
				toastr.error(res.message);
				$('#purchaseNowModal').modal('hide');
			}
		});
	});
	$('.course-showcase').on('click', '#showVideo', function(){
		var videoPath = $(this).attr("data-video");
		var imgHeight = $('.course-img-preview').height();
		$('#showVideo').hide();
		$('#showcaseBlock').removeClass('hide');
		var playerWidth = $(".course-showcase").width();
		player = new MediaElementPlayer('#videoShowcasePlayer', {type: 'video/mp4',videoWidth: playerWidth,videoHeight: imgHeight});
		var sources = [
			{ src: videoPath, type: 'video/mp4' }
		];

		player.setSrc(sources);
		player.load();
		player.play();
	});
});
function loadCourseDetails(){
	var req		 = {};
	var startDate;
	var catIds	 = [], catNames = [];
	req.action	 = 'loadCourseDetails';
	//req.courseId = '34';
	req.slug	 = slug;
	if(req.slug != undefined) {
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			if (data.status == 1) {
				var newprice	= "";
				var today		= new Date().getTime();
				var discountenddate = data.courseDetails.discount.endDate;
				if(data.courseDetails.discount.endDate > today ) {
					newprice=data.courseDetails.studentPrice*((100 - data.courseDetails.discount.discount) / 100);
					newprice=newprice.toFixed(2);
				}
				var price="";
				var displayCurrency=dispcurrency;
				originalPrice=data.courseDetails.studentPrice;
				
				//filling demo video
				if(data.courseDetails.demoVideo != '') {
					$('.course-showcase').html('<a href="javascript:void(0)" id="showVideo" data-video="'+data.courseDetails.demoVideo+'">'+
													'<span class="course-img-preview">'+
														'<img src="'+data.courseDetails.image+'" alt="'+data.courseDetails.name+'" class="btn-block" />'+
														'<i class="fa fa-play-circle-o"></i></a>'+
													'</span>'+
												'</a>'+
												'<div id="showcaseBlock" class="hide"><video id="videoShowcasePlayer"></video></div>');
				} else {
					$('.course-showcase').html('<img src="'+data.courseDetails.image+'" alt="'+data.courseDetails.name+'" class="btn-block" />');
				}
				if(data.courseDetails.studentPrice == '0' || data.courseDetails.studentPrice == '0.00') {
					displayCurrency = '';
					data.courseDetails.studentPrice = 'Free';
				}
				//newprice> 0 $$ newprice< data.courseDetails.studentPrice
				originalPrice=data.courseDetails.studentPrice;
				dispcurrency=displayCurrency;
				
				if(newprice > 0) {
					$('.course-price').html('<strike class="price-discount">' + data.courseDetails.studentPrice + ' ' + displayCurrency + '</strike>'+
										'<span class="price-now">' + newprice + ' ' + displayCurrency + '</span>');					
				} else {	
					$('.course-price').html('<span class="price-now">' + data.courseDetails.studentPrice + ' ' + displayCurrency +'</span>');
				}
				if (!$('#courseName').text()) {
					$('#courseName').html(data.courseDetails.name);
				};
				$('#subtitle').html(data.courseDetails.subtitle);
				$('#jsDescription').html('<p>'+data.courseDetails.description+'<p>');
				if($('#jsDescription p').height()>100) {
					$('.course-description .js-course-details').removeClass("hide");
				}
				$('.js-share-facebook').attr('href','http://www.facebook.com/share.php?u='+encodeURI(window.location.href)+'&title='+data.courseDetails.name);
				$('.js-share-twitter').attr('href','http://twitter.com/intent/tweet?status='+data.courseDetails.name+' '+encodeURI(window.location.href));
				$('.js-share-google').attr('href','https://plus.google.com/share?url='+encodeURI(window.location.href));
				$('.js-share-facebook, .js-share-twitter, .js-share-google').click(function(e){
					e.preventDefault();
					var left = (window.screen.width / 2) - ((400 / 2) + 10);
					var top = (window.screen.height / 2) - ((500 / 2) + 50);

					window.open($(this).attr('href'), "MsgWindow", "top="+top+", left="+left+", width=500, height=400");
				});
				//$('#course_img').attr("src", data.courseDetails.image);
				if (data.courseDetails.targetAudience != null) {
					var targetAudience = data.courseDetails.targetAudience.split(",");
					for (var i = targetAudience.length - 1; i >= 0; i--) {
						$('#targetAudience').append('<li>'+targetAudience[i]+'</li>');
					};
				} else {
					$('#targetAudience').append('<li>Anybody</li>');
				}
				if(data.courseCategories.length == 0) {
					$('#courseCategory').html('No Categories');
				} else {
					$.each(data.courseCategories, function (k, cat) {
						//console.log(cat);
						catIds.push(cat.categoryId);
						catNames.push(cat.category);
						if (!subdomain) {
							$('#courseCategory').append('<a href="'+sitePathCourses+cat.slug+'" class="btn btn-trans-color2 mr5 mb5">'+cat.category+'</a>');
						} else {
							$('#courseCategory').append('<button class="btn btn-trans-color2 mr5 mb5">'+cat.category+'</button>');
						}
					});
				}
				if(data.courseTags.length == 0) {
					$('#courseTags').html('No Tags');
				} else {
					$.each(data.courseTags, function (k, cat) {
						$('#courseTags').append('<button class="btn btn-trans-color2 mr5 mb5">'+cat.tag+'</button>');
						/*catIds.push(cat.categoryId);
						catNames.push(cat.category);
						if (!subdomain) {
							$('#courseTags').append('<a href="'+sitePathCourses+cat.slug+'" class="btn btn-trans-color2 mr5 mb5">'+cat.category+'</a>');
						} else {
							$('#courseTags').append('<button class="btn btn-trans-color2 mr5 mb5">'+cat.category+'</button>');
						}*/
					});
				}
				instituteName = data.institute.name;
				link = sitepathMarket+'institute/';
				switch(parseInt(data.courseDetails.roleId)) {
					case 1:
						instituteName = data.institute.name;
						link = sitepathMarket+'institute/';
						break;
					case 2:
					case 3:
						instituteName = data.institute.firstName + ' ' + data.institute.lastName;
						link = sitepathMarket+'instructor/';
						break;
				}
				if (!subdomain) {
					$('.js-institute-link').attr('href', link + data.institute.userId);
				} else {
					$('.js-institute-link').attr('href', sitepathMarket);
				}
				$('.js-institute-name').html(instituteName);
				$('#instituteImg').attr("src",  data.institute.profilePic);
				$('#instituteImg').attr("alt",  instituteName);
				$('#instituteDescription').html(data.institute.description);
				if($('#instituteDescription').height()>102) {
					$('.about-instructor .js-instructor-details').removeClass("hide");
				}
				if(!data.institute.tagline) {
					$('#tagline').remove();
				} else {
					$('#tagline').html(data.institute.tagline);
				}
				$('#courseID').html('C00' + data.courseDetails.id);
				$('.js-enrolled').html(data.studentCount);
				//now working on dates
				var dateHTML 	 = '';
				var currentDate  = new Date(parseInt(data.currentDate));
				var liveDate 	 = new Date(parseInt(data.courseDetails.liveDate));
				var calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
				var endDate 	 = (data.courseDetails.endDate == '')?'':new Date(parseInt(data.courseDetails.endDate));
				//console.log(currentDate, liveDate, endDate);
				if(endDate != '') {
					var futureDate = new Date(currentDate.getFullYear() + 1, currentDate.getMonth(), currentDate.getDate(), 0, 0, 0, 0);
					var calcEndDate = endDate.getDate()+ ' ' +MONTH[endDate.getMonth()]+ ' ' + endDate.getFullYear();
					if(endDate.valueOf() > futureDate.valueOf() && liveDate.valueOf() < currentDate.valueOf()) {
						$('#liveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>');
						$('#liveEndDates').remove();
						//$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Avail Course for 1 year from Launch date');
						$('#msgValidity').remove();
					} else if(endDate.valueOf() > futureDate.valueOf() && liveDate.valueOf() > currentDate.valueOf()) {
						$('#liveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>');
						$('#liveEndDates').remove();
						//$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Avail Course for 1 year from Launch date');
						$('#msgValidity').remove();
					} else {
						$('#liveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>'+
												'<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> End Date :</label>'+
													'<span class="value" id="endDate"> '+calcEndDate+'</span>'+
												'</li>');
						$('#liveEndDates').remove();
						$('#msgValidity').remove();
					}
				} else {
					$('#liveEndDates').remove();
					$('#msgValidity').remove();
				}
				$('.page-load-hidden').removeClass("invisible");

				if(!!data.institute.userId) {
					instituteId = data.institute.userId;
					otherCoursesByInstitute(0);
				}

				//adding ratings
				if(data.courseDetails.rating.total == 0) {
					rating = 0;
				}
				else {
					rating = parseFloat(data.courseDetails.rating.rating).toFixed(1);
				}
				$('.ratingStars').rating('rate', rating);
				$('.ratingTotal').text(data.courseDetails.rating.total + ' ratings');
				$('.avg-rate').text(rating);

				schema.name			= data.courseDetails.name;
				schema.description	= data.courseDetails.subtitle;
				schema.aggregateRating.ratingValue	= rating;
				schema.aggregateRating.reviewCount	= data.courseDetails.rating.total;
				
				//create JSON-LD for lastModified and citation_online_date
				var el = document.createElement('script');  
				el.type = 'application/ld+json';  
				el.id = 'jsonld';
				el.text = JSON.stringify(schema); 
				$('body').append(el);

				//$('#courseReviews').remove();
				//if(loginstatus==1) {
					//$('#takeCourse').removeAttr('data-toggle');
					//$('.js-take-course').attr('href','javascript:;');
					$('.js-take-course:not([disabled])').on('click', function() {
						if (userId == 0) {
							//toastr.error("Please login");
							$("#loginModal").modal("show");
						} else {
						    if (walletApp == '') {
						        $("#modalSetAddress").modal("show");
						    } else {
								if(userRole != 4) {
									toastr.error('You are not authorised to purchase this course. Please login with student account');
									return;
								}
								$('#purchaseNowModal .course-name').text($('#courseName').text());
								$('#purchaseNowModal .amount').html($('.price-now').html());
								$('#purchaseNowModal').modal('show');
							}
						}
					});
					/*$('.js-take-course:not([disabled])').on('click', function() {
						if(userRole != 4) {
							toastr.error('You are not authorised to purchase this course. Please login with student account');
							return;
						}
						$('#purchaseNowModal .course-name').text($('#courseName').text());
						$('#purchaseNowModal .amount').html($('.price-now').html());
						$('#purchaseNowModal').modal('show');
					});
					$('.js-take-course[disabled]').on('click', function() {
						toastr.error("Please log into Metamask and select Main Network");
					});*/
				//}
			}
			else if(data.status == 404) {
				window.location = sitePath404;
			}
			getsubjectDetails();
		});
	}
	else
		toastr.error("No course Details");
 }

//CourseEnrolled
function CourseEnrolled() {
	var req = {};
	var res;
	// req.courseId = '34';
	req.slug	= slug;
	req.userId	= this.userId;
	req.userRole = this.userRole; 
	req.action	= 'check-courseEnrolled';
	//console.log(req);
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status== "1") {
			window.location.replace("student/MyExam.php?courseId="+courseId);
		} else {
			loadCourseDetails();
		}
		this.enrollstatus=res;
	});
}

$('#listOtherCourses').on("click", ".more-item a", function(){
	var lastId = $(this).parent().prev().find('.js-other-course').attr("data-id");
	otherCoursesByInstitute(lastId);
});
function otherCoursesByInstitute(start) {
	var req			= {};
	req.action		= 'loadOtherCoursesInstitute';
	// req.courseId	= '34';
	req.slug		= slug;
	req.instituteId	= instituteId;
	req.start 		= start;
	if (req.instituteId != undefined || req.slug != undefined) {
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			if (data.status == 1) {
				var price			= "";
				var displayCurrency	= dispcurrency;
				if(data.courses.length > 0) {
					if(data.courses.length > 1) {
						$('.other-courses-title').html('Courses by <a href="'+link + instituteId+'" class="js-institute-name">'+instituteName+'</a>');
					} else {
						$('.other-courses-title').html('Course by <a href="'+link + instituteId+'" class="js-institute-name">'+instituteName+'</a>');
					}
					if(start == 0) {
						$('#listOtherCourses').append('<ul class="list-unstyled list-courses">');
					}
					//for (var i = data.courses.length - 1; i >= 0; i--) {
					for (var i = 0; i < data.courses.length; i++) {
						if(data.courses[i].studentPrice == '0' || data.courses[i].studentPrice == '0.00') {
							displayCurrency = '';
							data.courses[i].studentPrice = 'Free';
						}
						$('#listOtherCourses .more-item').remove();
						$('#listOtherCourses .list-courses').append(
							'<li class="other-item">'+
								'<div class="row">'+
									'<div class="col-md-6">'+
										'<a href="'+sitePathCourseDetail+data.courses[i].slug+'" class="js-other-course" data-id="'+data.courses[i].id+'">'+
											'<img class="btn-block" src="'+data.courses[i].image+'" alt="'+data.courses[i].name+'">'+
										'</a>'+
									'</div>'+
									'<div class="col-md-6 other-courses-meta">'+
										'<div class="mb5">'+
											'<a href="'+sitePathCourseDetail+data.courses[i].slug+'" class="ellipsis-2lines lh-1">'+data.courses[i].name+'</a>'+
										'</div>'+
										'<div>'+
											'<span class="cost">' + displayCurrency + ' ' + data.courses[i].studentPrice + '</span>'+
										'</div>'+
										'<div>'+
											'<input type="hidden" class="courseRating" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="' + data.courses[i].rating.rating + '" DISABLED/>'+
										'</div>'+
									'</div>'+
								'</div>'+
							'</li>');
						
					};
					$('.courseRating').rating();
				}
				if($('#listOtherCourses .list-courses .other-item').length!=data.noOfCourses) {
					$('#listOtherCourses .list-courses').append('<li class="more-item text-center"><a href="javascript:void(0)" class="btn-block">View More</a></li>');
				}
			} else {
				if(start == 0) {
					$('.other-courses').remove();
				}
			}
		});
	}
	else
		toastr.error("No Subject Found Details");
}

function getsubjectDetails() {
	var req = {};
	req.action = 'loadSubjectDetails';
	// req.courseId = '34';
	req.slug = slug;
	if (req.slug != undefined) {
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			if (data.status == 1) {
				var html = "";
				var subject_html = '';
				var professor = '';
				var subjectHeading='';
				var noOfLectures = 0;
				var noOfSubjects = data.subjects.length;
				var totalDuration = 0;
				var noOfExams = 0;
				var noOfAssignments = 0;
				var currentDate = 0;
				var liveDate = 0;
				var calcLiveDate = 0;
				var endDate = 0;
				var liveHTML = '';
				for (var i = 0; i < data.subjects.length; i++) {

					if(data.subjects[i].chapters.length>0 || data.subjects[i].independentExam.length || data.subjects[i].independentAssignment.length) {
						subject_html += '<table class="table table-responsive">'+
											'<tr class="list-row-subject">'+
												'<th class="course-subject">'+
													'<h2 class="subject-title">'+
														'<div class="subject-avatar">'+
															'<img src="' + data.subjects[i].image + '" alt="' + data.subjects[i].name + '" class="img-responsive">'+
														'</div>Subject: ' + data.subjects[i].name + ''+
													'</h2>'+
												'</th>'+
											'</tr>';
						if(data.subjects[i].independentExam == null)
							data.subjects[i].independentExam = 0;
						if(data.subjects[i].independentAssignment == null)
							data.subjects[i].independentAssignment = 0;

						if(data.subjects[i].chapters.length>0) {
						
							for (var j = 0; j < data.subjects[i].chapters.length; j++) {
								var examCount;
								var assCount;
								if(data.subjects[i].chapters[j].Exam > 0) {
									examCount = data.subjects[i].chapters[j].Exam + ' Exams';
								}
								else
									examCount = '--';
								if(data.subjects[i].chapters[j].Assignment > 0) {
									assCount = data.subjects[i].chapters[j].Assignment + ' Assignments';
								}
								else
									assCount = '--';
								if(data.subjects[i].chapters[j].Exam == null)
									data.subjects[i].chapters[j].Exam = 0; 
								if(data.subjects[i].chapters[j].Assignment == null)
									data.subjects[i].chapters[j].Assignment = 0;
								if(data.subjects[i].chapters[j].content.length>0 || data.subjects[i].chapters[j].Exam.length || data.subjects[i].chapters[j].Assignment.length) {
									subject_html += '<tr class="list-row-section">'+
														'<th class="ssection">'+
															'<div class="ssection-title">'+
																'<span class="ssection-title-txt">Section ' + (j+1) + ': ' + data.subjects[i].chapters[j].name + '</span>'+
																'<span class="ssection-meta"><span>' + examCount + ' Exams</span><span>' + assCount + ' Assignments</span><span>'+data.subjects[i].chapters.length+' lectures</span></span>'+
															'</div>'+
														'</th>'+
													'</tr>';
									if(data.subjects[i].chapters[j].content.length>0) {
										subject_html += '<tr class="list-row-section">'+
																'<td class="ssection-content">'+
																	'<div class="ssection-title">'+
																		'<span class="ssection-title-txt">Section Content</span>'+
																		'<span class="ssection-meta"><span>' + examCount + ' Exams</span></span>'+
																	'</div>'+
																'</td>'+
															'</tr>';

										//now adding lectures of the exam
										for(var k = 0; k < data.subjects[i].chapters[j].content.length; k++) {
											var type = '';
											var freePreview = '';
											var durationTime = '';
											switch(data.subjects[i].chapters[j].content[k].lectureType) {
												case 'Youtube':
													type = '<i class="fa fa-play-circle-o"></i>';
													break;
												case 'Vimeo':
													type = '<i class="fa fa-play-circle-o"></i>';
													break;
												case 'video':
													type = '<i class="fa fa-play-circle-o"></i>';
													if(data.subjects[i].chapters[j].content[k].demo == 1) {
														if(data.subjects[i].chapters[j].content[k].duration>0) {
															durationTime = '<span class="lecture-duration pull-right"></span>';
															freePreview = '<div class="lecture-play-btn">'+
																				'<button class="btn btn-primary btn-preview" data-src="' + data.subjects[i].chapters[j].content[k].metastuff + '">Free Demo (' + calcDisplayTime(data.subjects[i].chapters[j].content[k].duration) + ')</button>'+
																			'</div>';
														} else {
															freePreview = '<div class="lecture-play-btn">'+
																				'<button class="btn btn-primary btn-preview" data-src="' + data.subjects[i].chapters[j].content[k].metastuff + '">Free Demo</button>'+
																			'</div>';
														}
													}
													break;
												case 'doc':
													type = '<i class="fa fa-book"></i>';
													break;
												case 'ppt':
													type = '<i class="fa fa-file-powerpoint-o"></i>';
													break;
												case 'text':
													type = '<i class="fa fa-file-text-o"></i>';
													break;
												case 'dwn':
													type = '<i class="fa fa-cloud-download"></i>';
													break;
											}

											if(data.subjects[i].chapters[j].content[k].duration>0 && data.subjects[i].chapters[j].content[k].demo != 1) {
												durationTime = '<span class="lecture-duration pull-right">' + calcDisplayTime(data.subjects[i].chapters[j].content[k].duration) + '</span>';
											}
											subject_html += '<tr class="list-row-lecture">'+
																'<td class="lecture">'+
																	'<a href="javascript:void(0)" class="lecture-link btn-block">'+
																		'<span class="lecture-play">' + type +'</span>'+
																		'<div class="lecture-title-blk">'+
																			'<span class="lecture-number">' + (j+1) + '.' + (k+1) + '</span>'+
																			'<span class="lecture-title-txt">' + data.subjects[i].chapters[j].content[k].lectureName + '</span>'+
																			durationTime +
																		'</div>' + freePreview +
																	'</a>'+
																'</td>'+
															'</tr>';
											noOfLectures++;
										}
									}
									if(data.subjects[i].chapters[j].Exam.length) {
										subject_html += '<tr class="list-row-section">'+
															'<td class="ssection-content">'+
																'<div class="ssection-title">'+
																	'<span class="ssection-title-txt">Section Exams</span>'+
																	'<span class="ssection-meta"><span>' + examCount + ' Exams</span></span>'+
																'</div>'+
															'</td>'+
														'</tr>';
										for(var k = 0; k < data.subjects[i].chapters[j].Exam.length; k++) {
											currentDate = new Date(parseInt(data.currentDate));
											liveDate = new Date(parseInt(data.subjects[i].chapters[j].Exam[k].startDate));
											calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
											if(liveDate.valueOf() > currentDate.valueOf()) {
												liveHTML = '<span class="lecture-duration pull-right">Live date : '+calcLiveDate+'</span>';
											} else {
												liveHTML = '';
											}

											subject_html += '<tr class="list-row-lecture">'+
															'<td class="lecture">'+
																'<a href="javascript:void(0)" class="lecture-link btn-block">'+
																	'<span class="lecture-play"><i class="fa fa-pencil-square-o"></i></span>'+
																	'<div class="lecture-title-blk">'+
																		'<span class="lecture-number">' + (j+1) + '.' + (k+1) + '</span>'+
																		'<span class="lecture-title-txt">' + data.subjects[i].chapters[j].Exam[k].name + '</span>'+
																		liveHTML +
																	'</div>'
																'</a>'+
															'</td>'+
														'</tr>';
											noOfExams++;
										}
									}
									if(data.subjects[i].chapters[j].Assignment.length) {
										subject_html += '<tr class="list-row-section">'+
															'<td class="ssection-content">'+
																'<div class="ssection-title">'+
																	'<span class="ssection-title-txt">Section Assignments</span>'+
																	'<span class="ssection-meta"><span>' + assCount + ' Assignments</span></span>'+
																'</div>'+
															'</td>'+
														'</tr>';
										for(var k = 0; k < data.subjects[i].chapters[j].Assignment.length; k++) {
											currentDate = new Date(parseInt(data.currentDate));
											liveDate = new Date(parseInt(data.subjects[i].chapters[j].Assignment[k].startDate));
											calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
											if(liveDate.valueOf() > currentDate.valueOf()) {
												liveHTML = '<span class="lecture-duration pull-right">Live date : '+calcLiveDate+'</span>';
											} else {
												liveHTML = '';
											}

											subject_html += '<tr class="list-row-lecture">'+
															'<td class="lecture">'+
																'<a href="javascript:void(0)" class="lecture-link btn-block">'+
																	'<span class="lecture-play"><i class="fa fa-book"></i></span>'+
																	'<div class="lecture-title-blk">'+
																		'<span class="lecture-number">' + (j+1) + '.' + (k+1) + '</span>'+
																		'<span class="lecture-title-txt">' + data.subjects[i].chapters[j].Assignment[k].name + '</span>'+
																		liveHTML +
																	'</div>'
																'</a>'+
															'</td>'+
														'</tr>';
											noOfAssignments++;
										}
									}
								}
							}
						}

						if(data.subjects[i].independentExam.length) {
							subject_html += '<tr class="list-row-section">'+
												'<th class="ssection">'+
													'<div class="ssection-title">'+
														'<span class="ssection-title-txt">Independent Exams</span>'+
														'<span class="ssection-meta"><span>' + data.subjects[i].independentExam.length + ' Exams</span></span>'+
													'</div>'+
												'</th>'+
											'</tr>';
							for(var k = 0; k < data.subjects[i].independentExam.length; k++) {
								currentDate = new Date(parseInt(data.currentDate));
								liveDate = new Date(parseInt(data.subjects[i].independentExam[k].startDate));
								calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
								if(liveDate.valueOf() > currentDate.valueOf()) {
									liveHTML = '<span class="lecture-duration pull-right">Live date : '+calcLiveDate+'</span>';
								} else {
									liveHTML = '';
								}
								subject_html += '<tr class="list-row-lecture">'+
												'<td class="lecture">'+
													'<a href="javascript:void(0)" class="lecture-link btn-block">'+
														'<span class="lecture-play"><i class="fa fa-pencil-square-o"></i></span>'+
														'<div class="lecture-title-blk">'+
															'<span class="lecture-number">' + (k+1) + '</span>'+
															'<span class="lecture-title-txt">Exam : ' + data.subjects[i].independentExam[k].name + '</span>'+
															liveHTML +
														'</div>'
													'</a>'+
												'</td>'+
											'</tr>';
								noOfExams++;
							}
						}
						if(data.subjects[i].independentAssignment.length) {
							subject_html += '<tr class="list-row-section">'+
												'<th class="ssection">'+
													'<div class="ssection-title">'+
														'<span class="ssection-title-txt">Independent Assignments</span>'+
														'<span class="ssection-meta"><span>' + data.subjects[i].independentAssignment.length + ' Assignments</span></span>'+
													'</div>'+
												'</th>'+
											'</tr>';
							for(var k = 0; k < data.subjects[i].independentAssignment.length; k++) {
								currentDate = new Date(parseInt(data.currentDate));
								liveDate = new Date(parseInt(data.subjects[i].independentAssignment[k].startDate));
								calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
								if(liveDate.valueOf() > currentDate.valueOf()) {
									liveHTML = '<span class="lecture-duration pull-right">Live date : '+calcLiveDate+'</span>';
								} else {
									liveHTML = '';
								}
								subject_html += '<tr class="list-row-lecture">'+
												'<td class="lecture">'+
													'<a href="javascript:void(0)" class="lecture-link btn-block">'+
														'<span class="lecture-play"><i class="fa fa-book"></i></span>'+
														'<div class="lecture-title-blk">'+
															'<span class="lecture-number">' + (k+1) + '</span>'+
															'<span class="lecture-title-txt">Assignment : ' + data.subjects[i].independentAssignment[k].name + '</span>'+
															liveHTML +
														'</div>'
													'</a>'+
												'</td>'+
											'</tr>';
								noOfAssignments++;
							}
						}
						
						if(data.subjects[i].totalDuration>0) {
							totalDuration = totalDuration + parseInt(data.subjects[i].totalDuration);
						}
						subject_html += '</table>';
						subjectCount++;
					}
				}
				if(totalDuration>0) {
					var result = calcDisplayTimeWords(totalDuration);
					$('#courseTest').after(
										'<li class="list-item-short">'+
											'<label for=""><i class="fa fa-youtube-play"></i> Duration :</label>'+
											'<span class="value" id="totalDuration"> '+result+'</span>'+
										'</li>');
					$('#courseTest').remove();
				} else {
					$('#courseTest').after(
										'<li class="list-item-short">'+
											'<label for=""><i class="fa fa-question-circle"></i> Questions :</label>'+
											'<span class="value" id="totalDuration"> '+data.totalQuestions+'</span>'+
										'</li>');
					$('#courseTest').remove();
				}

				$('#curriculum').append(subject_html);
				$('.viewDemo').on('click', function() {
					window.location = 'student/chapterContent.php?courseId=' + getUrlParameter('courseId') + '&subjectId=' + $(this).parents('table').attr('data-subjectId') +'&chapterId=' + $(this).parents('tr').attr('data-chapterId') + '&demo=1';
				});
				$.each($('#course-append table'), function() {
					$(this).find('.soup').hide();
					$.each($(this).find('.soup'), function() {
						if($(this).text() != '--') {
							$(this).parents('table').find('.soup').show();
							return;
						}
					});
					$(this).find('tr.chapter:eq(0), tr.chapter:eq(1), tr.chapter:eq(2)').show();
					if($(this).find('tr.chapter').length > 3)
						$(this).parents('.about').find('.viewMoreChap').show();
					$('.viewMoreChap').off('click');
					$('.viewMoreChap').on('click', function() {
						if($(this).attr('data-hidden') == 0) {
							$(this).parents('.about').find('table').find('tr').show();
							$(this).text('Hide');
							$(this).attr('data-hidden', '1');
						}
						else {
							$(this).parents('.about').find('table').find('tr').hide();
							$(this).parents('.about').find('table').find('tr.chapter:eq(0), tr.chapter:eq(1), tr.chapter:eq(2)').show();
							$(this).parents('.about').find('table').find('tr.independent').show();
							$(this).text('View More');
							$(this).attr('data-hidden', '0');
						}
					});
				});
			}
			$('#professors').html(professors);
			$('#noOfSubjects').html(noOfSubjects);

			var contentBuild = 0;
			var contentClass = 0;
			var contentHTML	 = "";
			if(noOfLectures>0) {contentBuild++;}
			if(noOfExams>0) {contentBuild++;}
			if(noOfAssignments>0) {contentBuild++;}
			if(contentBuild>0) {
				switch(contentBuild) {
					case 1: contentClass = "list-item-long";break;
					case 2: contentClass = "list-item-short";break;
					case 3: contentClass = "list-item-xshort";break;
				}
				if(noOfLectures>0) {
					contentHTML+=  '<li class="'+contentClass+'">'+
										'<label for=""><i class="fa fa-youtube-play"></i> Lectures :</label>'+
										'<span class="value" id="noOfLectures"> '+noOfLectures+'</span>'+
									'</li>';
				}
				if(noOfExams>0) {
					contentHTML+=  '<li class="'+contentClass+'">'+
										'<label for=""><i class="fa fa-pencil-square-o"></i> Exams :</label>'+
										'<span class="value" id="noOfExams"> '+noOfExams+'</span>'+
									'</li>';
				}
				if(noOfAssignments>0) {
					contentHTML+=  '<li class="'+contentClass+'">'+
										'<label for=""><i class="fa fa-book"></i> Assignments :</label>'+
										'<span class="value" id="noOfAssignments"> '+noOfAssignments+'</span>'+
									'</li>';
				}
				$('#contentStats').after(contentHTML);
				$('#contentStats').remove();
			}
			fetchCourseReviews();
		});
	}
	else
		toastr.error("No Subject Found Details");
}

/**
* funtion to fetch all reviews for all subjects of the courseDetails
*
* @author Rupesh Pandey
* @date 23/05/2015
*/
function fetchCourseReviews() {
	var req = {};
	var res;
	req.action = 'get-all-reviews-for-course';
	// req.courseId = '34';
	req.slug = slug;
	$.ajax({
		type: "post",
		url: ApiEndpoint,
		data: JSON.stringify(req) 
	}).done(function(res) {
		res = $.parseJSON(res);
		var html = '';
		var c1=c2=c3=c4=c5=0;
		$.each(res.reviews, function(i, review) {
			//counting reviews
			switch(review.rating) {
				case '1.00':
					c1++;
					break;
				case '2.00':
					c2++;
					break;
				case '3.00':
					c3++;
					break;
				case '4.00':
					c4++;
					break;
				case '5.00':
					c5++;
					break;
			}
			var date = '';
			var dateobj = new Date(parseInt(review.time + '000'));
			date = format(dateobj.getDate()) + ' ' + MONTH[dateobj.getMonth()] + ' ' + dateobj.getFullYear() + ' ' + format(dateobj.getHours()) + ':' + format(dateobj.getMinutes());
			html += '<li class="comment-item">'
						+ '<div class="row">'
							+ '<div class="col-sm-3">'
								+ '<div class="user-block">'
									+ '<div class="user-avatar">'
										+ '<img src="' + review.studentImage + '" alt="' + review.studentName + '">'
									+ '</div>'
									+ '<span>' + review.studentName + '</span>'
								+ '</div>'
							+ '</div>'
							+ '<div class="col-sm-9">'
								+ '<div class="rating">'
									+ '<input type="hidden" class="studentRating" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="' + review.rating + '" DISABLED/>'
									+ '<span class="">&emsp;' + date + '</span>';
								if(subjectCount > 1)
									html += '<span class="subject"> (Subject: ' + review.subjectName + ')</span>';
								html += '</div>'
								+ '<h4>' + review.title + '</h4>'
								+ '<div class="comment">'
									+ '<p>' + review.review + '</p>'
								+ '</div>'
							+ '</div>'
						+ '</div>'
					+ '</li>';
		});
		$('#comments ul.list-comments').append(html);
		$('.studentRating').rating();
		var total = c1 + c2 + c3 + c4 + c5;
		if(total == 0)
			$('#comments ul').html('<li class="comment-item"><div class="user-block"><span style="margin-left: 0px;">No Reviews Found</span></div></li>');
		$('.progress1').css('width', percentage(c1, total) + '%');
		$('.progress2').css('width', percentage(c2, total) + '%');
		$('.progress3').css('width', percentage(c3, total) + '%');
		$('.progress4').css('width', percentage(c4, total) + '%');
		$('.progress5').css('width', percentage(c5, total) + '%');
		$('.count1').text(c1);
		$('.count2').text(c2);
		$('.count3').text(c3);
		$('.count4').text(c4);
		$('.count5').text(c5);
		fetchCouponCode();
	});
}

function fetchCouponCode () {
	removeLoader();
	var coupon = getUrlParameter('coupon');
	if (coupon) {
		if (coupon.length > 0) {
			$('#couponcode').val(coupon);
			$('#couponapply').trigger('click');
		}
	}
}

$('#curriculum').on("click", ".btn-preview", function(){
	//event.preventDefault();
	var src = $(this).attr('data-src');
	$("#playerModal").modal('show');
	setTimeout(function(){
		var playerWidth = $("#playerModal .modal-dialog").width();
		player = new MediaElementPlayer('#videoPlayer', {type: 'video/mp4',videoWidth: playerWidth,videoHeight: '100%'});
		var sources = [
			{ src: src, type: 'video/mp4' }
		];

		player.setSrc(sources);
		player.load();
		player.play();
	},500);
});
$('#dismissVideo').click(function(){
	player.pause();
});
$(window).scroll(function(){
	if($(window).scrollTop() >= $('.course-showcase').offset().top) {
		$('#jsCourseMenu').addClass('affix');
	} else {
		$('#jsCourseMenu').removeClass('affix');
	}
});
$('.course-sublinks').click(function(){
	event.preventDefault();
	var target = $(this).attr('href');
	$('html, body').animate({
		scrollTop: $(target).offset().top
	}, 1000);
});
$(".js-course-details").click(function(){
	toggleDataMore($(this),$(".js-full-course-details"));
});
$(".js-instructor-details").click(function(){
	toggleDataMore($(this),$(".js-full-instructor-details"));
});

</script>