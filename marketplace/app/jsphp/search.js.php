<script type="text/javascript">
var priceFilter = [
					[0,0],
					[1,10],
					[11,20],
					[21,30],
					[31,40],
					[41,50],
					[51,60],
					[60,0]
				];
$(document).ready(function(){
	if(getUrlParameter('category') != undefined) {
		var category = getUrlParameter('category');
		var catId = category.split(',');
		$.each(catId, function(i,v) {
			$('.catCheck[data-id="'+v+'"]').prop('checked', true);
		})
	}
	
	if(getUrlParameter('price') != undefined) {
		var price = getUrlParameter('price');
		var priceId = price.split(',');
		$.each(priceId, function(i,v) {
			$('.priceCheck[data-id="'+v+'"]').prop('checked', true);
		})
	}

	$('.js-list-filter').on('click', '.catCheck, .priceCheck', function(){
		searchCourses();
	});

	$('.js-list-filter').on('click', '.checkbox', function(e){
		if (e.target.tagName == "A") {
			$(this).find('input').trigger('click');
		};
	});

	$('#frmTopSearch').submit(function(){
		searchCourses();
		return false;
	});
});
function searchCourses() {
	var req = {};
	var res;
	req.action = 'search-page';
	req.text = $('#searchBox').val();
	var catId = [];
	$.each($('.catCheck'), function() {
		if($(this).prop('checked'))
			catId.push($(this).attr('data-id'));
	});
	req.cat = catId.join(',');
	var priceId = [];
	$.each($('.priceCheck'), function() {
		if($(this).prop('checked'))
			priceId.push($(this).attr('data-id'));
	});
	req.price = priceId.join(',');
	window.history.pushState({"html": '',"pageTitle": 'Searching'},"Search State", sitepathMarket+'search/?q=' + req.text + '&category=' + req.cat + '&price=' + req.price);
	//console.log(req);
	/*if(req.text=='') {
		window.location = sitePathCourses;
	} else {*/
		$('#searchTitle').html('Search Results for "'+req.text+'"');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			$('#searchResults').html('');
			var html = '';
			if(res.courses.length > 0) {
				$.each(res.courses, function(i, course) {
					displayCurrency = dispcurrency;
					var startdate = new Date(parseInt(course.liveDate));	
					var endDate = (course.endDate == '')?'No Expiry':new Date(parseInt(course.endDate));
					if(course.studentPrice == '0' || course.studentPrice == '0.00') {
						course.studentPrice = 'Free';
						displayCurrency = '';
					}
					var owner="";
					if(course.owner1!=null) {
						owner=course.owner1;
						ownerLink = sitepathMarket+'institute/'+course.ownerId;
					} else {
						owner= course.owner+" "+course.lastName;
						ownerLink = sitepathMarket+'instructor/'+course.ownerId;
					}

					$('#searchResults').append('<div class="course gradient-grey3">'+
								'<div class="pad10">'+
									'<div class="row">'+
										'<div class="col-md-3">'+
											'<div class="pad10">'+
												'<a href="' + sitePathCourseDetail + course.slug + '">'+
													'<img src="' + course.image + '" alt="" class="btn-block course-img-sm">'+
												'</a>'+
											'</div>'+
										'</div>'+
										'<div class="col-md-9">'+
											'<h4><a href="' + sitePathCourseDetail + course.slug + '">' + course.name + ' <small>(ID: C00' + course.id + ')</small></a></h4>'+
											'<p>by <a href="' + ownerLink + '">' + owner + '</a></p>'+
											'<div class="text-overflow">'+
												'<span class="mr5">' + course.studentPrice + ' ' + displayCurrency + '</span>'+
												'<span class="mr5"><input type="hidden" class="rating ratingStars" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="5" disabled=""></span>'+
												'<span class="js-subjects"></span>'+
											'</div>'+
										'</div>'+
									'</div><!-- .row -->'+
								'</div><!-- .pad10 -->'+
							'</div>');
					var rating = 0;
					if(course.rating.total > 0) {
						rating = parseFloat(course.rating.rating).toFixed(1);
					}
					$('#searchResults .course:last-child').find('.ratingStars').rating('rate', rating);
					$('#searchResults .course:last-child').find('.ratingStars').after('(' + course.rating.total + ') ');
					subjects='';
					$.each(course.subjects, function (j, subject) {
						subjects += subject.name + ', ';
					});
					subjects = subjects.substring(0, subjects.length - 2);
					$('#searchResults .course:last-child').find('.js-subjects').html('<i class="fa fa-th-list"></i> '+subjects).attr('title', subjects);
				});
			}
			else
				$('#searchResults').append('<h4>No such courses found. Please Modify search</h4>');
		});
	//}
}
function fillPriceFilter() {
	var range = '';
	var price = priceFilter;
	var iconp = dispcurrency;
	for (var i = 0; i < price.length; i++) {
		if (i == 0) {
			range = 'Free';
		} else if (i < (price.length-1)) {
			range = iconp+price[i][0]+' - '+iconp+price[i][1];
		} else {
			range = 'Above '+iconp+price[i][0];
		}
		$('#filterPrice').append('<li>'+
									'<a href="javascript:void(0)" class="checkbox clear-mrg">'+
										'<input type="checkbox" class="priceCheck" data-id="'+(i+1)+'"> ' + range +
									'</a>'+
								'</li>');
	};
	searchCourses();
}
</script>