<script type="text/javascript">
$(document).ready(function(){
	$(".form-3d .form-control").keyup(function(e){
		if(e.which!=9 && e.which!=13) {
			var disError = $(this).closest("form").find(".help-block");
			disError.html('').closest('.form-group').removeClass('has-success').removeClass('has-error');
		}
	});
	function emailValidation(element) {
		var frmGroup=element.closest('.form-group');
		var disError=element.closest('form').find('.help-block');
		var frmError=disError.closest('.form-group');
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(regex.test(element.val())) {
			frmGroup.removeClass('has-error').addClass('has-success');
			disError.html("");
			frmError.removeClass('has-success').removeClass('has-error');
			return true;
		}
		else {
			frmGroup.removeClass('has-success').addClass('has-error');
			disError.html("Doesn't seem to be a valid email address.");
			frmError.removeClass('has-success').addClass('has-error');
			//element.focus();
		}
		return false;
	}
	$("#inputEmailAddress").blur(function(){
		emailValidation($(this));
	});
	$("#btnStudentSignup").click(function(){
		var disError 				= $("#studentSignup").find('.help-block');
		var frmError 				= disError.closest('.form-group');
		var inputFirstName			= $("#inputFirstName");
		var inputLastName			= $("#inputLastName");
		var inputEmailAddress		= $("#inputEmailAddress");
		var inputPassword			= $("#inputPassword");
		if(inputFirstName.val().length<2 || inputFirstName.val().length>20) {
			disError.html("First Name needs to be 2-20 characters.");
			frmError.removeClass('has-success').addClass('has-error');
			inputFirstName.focus();
		} else if(inputLastName.val().length<1 || inputLastName.val().length>20) {
			disError.html("Last Name needs to be 1-20 characters.");
			frmError.removeClass('has-success').addClass('has-error');
			inputLastName.focus();
		} else if($(inputEmailAddress).closest('.form-group').hasClass('has-error') ||
					(!$(inputEmailAddress).closest('.form-group').hasClass('has-error') && !$(inputEmailAddress).closest('.form-group').hasClass('has-success'))
				) {
			emailValidation(inputEmailAddress);
		} else if(inputPassword.val().length<6 || inputPassword.val().length>20) {
			disError.html("Password needs to be between 6-20 characters.");
			frmError.removeClass('has-success').addClass('has-error');
			inputPassword.focus();
		} else {
			var formStudent = {
				'type' : "Student",
				'addressCountryId' : 0,
				'agree' : 0,
				'action' : 'init-student',
				'userType' : 'student',
				'userRole' : 4,
				'gender' : 0
			};
			formStudent.firstName 	= inputFirstName.val();
			formStudent.lastName 	= inputLastName.val();
			formStudent.email 		= inputEmailAddress.val();
			formStudent.password 	= inputPassword.val();
			formStudent 			= JSON.stringify(formStudent);
			$.post(ApiEndpoint, formStudent).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.status == 1) {
					$('#studentSignup').html('<p>Your account has been successfully activated.</p><div class="text-center"><a href="'+sitePathStudent+'" class="btn btn-primary">Goto my dashboard</a></div>');
				} else if(res.status == 0 && res.students.length>0) {
					$('#duplicateUsers').html(
						'<table class="table table-bordered table-condensed">'+
							'<tr>'+
								'<th></th>'+
								'<th>First Name</th>'+
								'<th>Last Name</th>'+
								'<th>Email</th>'+
							'</tr>'+
						'</table>'
						);
					var students = res.students;
					for (var i = 0; i < students.length; i++) {
						$('#duplicateUsers .table').append(
							'<tr>'+
								'<td><input type="radio" name="optionMergeUser" data-id="'+students[i].userId+'" /></td>'+
								'<td>'+students[i].firstName+'</td>'+
								'<td>'+students[i].lastName+'</td>'+
								'<td>'+students[i].email+'</td>'+
							'</tr>'
							);
					};
					$('#mergeUserModal').modal("show");
				} else {
					alert(res.message);
				}
			});
		}
		return false;
	});
	$('#duplicateUsers').on("click", "[name=optionMergeUser]", function(){
		$('#duplicateError').html("");
		$('#listDuplicates').addClass('hide');
		$('#btnVerify').addClass('hide');
		$('#frmStudentLogin').removeClass('hide');
		$('#btnVerifyMerge').removeClass('hide');
	});
	$('#btnVerify').click(function(){
		$('#duplicateError').html("");
		var optionMergeUser	= $('[name=optionMergeUser]:checked').attr("data-id");
		if (!optionMergeUser) {
			$('#duplicateError').html("Please select a user first!");
		} else {
			$('#listDuplicates').addClass('hide');
			$('#btnVerify').addClass('hide');
			$('#frmStudentLogin').removeClass('hide');
			$('#btnVerifyMerge').removeClass('hide');
		}
	});
	$('#btnDuplicateBack').click(function(){
		$('#duplicateError').html("");
		$('#frmStudentLogin').addClass('hide');
		$('#btnVerifyMerge').addClass('hide');
		$('#listDuplicates').removeClass('hide');
		$('#btnVerify').removeClass('hide');
	});
	$('#frmStudentLogin').submit(function(){
		$('#btnVerifyMerge').trigger('click');
		return false;
	});
	$('#btnVerifyMerge').click(function(){
		$('#duplicateError').html("");
		var optionMergeUser	= $('[name=optionMergeUser]:checked').attr("data-id");
		var password= $('#inputVerifyPassword').val();
		if (!optionMergeUser) {
			$('#duplicateError').html("Please select a user first!");
		} else if(password.length<6 || password.length>20) {
			$('#duplicateError').html("Password needs to be between 6-20 characters.");
			$('#inputVerifyPassword').focus();
		} else {
			var req = {};
			var res;
			req.action = 'verify-merge-student';
			req.optionMergeUser = optionMergeUser;
			req.password = password;
			$.ajax({
				'type'  :   'post',
				'url'   :   ApiEndpoint,
				'data'  :   JSON.stringify(req)
			}).done(function(res) {
			    res = $.parseJSON(res);
			    if(res.status == 0)
			        $('#duplicateError').html(res.message);
			    else {
			        console.log(res);
			        window.location = sitePathStudent;
			    }
			});
		}
	});
});
</script>