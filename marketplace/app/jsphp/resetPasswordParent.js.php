<script type="text/javascript">
$(document).ready(function(){
	$(".form-3d .form-control").keyup(function(e){
		if(e.which!=9 && e.which!=13) {
			var disError = $(this).closest("form").find(".help-block");
			disError.html('').closest('.form-group').removeClass('has-success').removeClass('has-error');
		}
	});
	$("#btnResetPassword").click(function(){
		var disError 				= $("#studentSignup").find('.help-block');
		var frmError 				= disError.closest('.form-group');
		var inputPassword			= $("#inputPassword");
		var inputConfirmPassword	= $("#inputConfirmPassword");
		if(inputPassword.val().length<6) {
			disError.html("A valid password is required.");
			frmError.removeClass('has-success').addClass('has-error');
			inputPassword.focus();
		} else if(inputPassword.val() != inputConfirmPassword.val()) {
			disError.html("Both passwords don't match.");
			frmError.removeClass('has-success').addClass('has-error');
			inputConfirmPassword.focus();
		} else {
			var query = slug.split('/');
			var req = {};
			var res;
			req.action = 'reset-password';
			req.userId = query[0];
			req.token = query[1];
			req.password = inputPassword.val();
			if(req.token != undefined && req.userId != undefined) {
				$.ajax({
					'type'	:	'post',
					'url'	:	ApiEndpoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 0) {
						disError.html(res.message);
						frmError.removeClass('has-success').addClass('has-error');
					} else {
						disError.html('Your password has been updated. Please <a href="#loginModal" data-toggle="modal">login here</a>.');
						frmError.removeClass('has-error').addClass('has-success');
					}
				});
			} else {
				disError.html('Your password rest link has expired or it never existed.');
				frmError.removeClass('has-success').addClass('has-error');
			}
		}
		return false;
	});
});
</script>