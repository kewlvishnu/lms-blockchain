<script type="text/javascript">
var MONTH = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var professors = '';
var loginstatus = 0;
var userId = 0;  
var userRole = 0;
//var packId = getUrlParameter("packageId");
var paymentPageStudent = true;
var subjectCount = 0;
var api = false;
var enrollstatus=0;
var instituteId = 0;
var instituteName = '';
var errmsg=0;
var courseId=0;
var courseIdArr=[];
var amount='';
var player;
var schema = {
			  "@context": "http://schema.org",
			  "@type": "Product",
			  "aggregateRating": {
			    "@type": "AggregateRating",
			    "ratingValue": "",
			    "reviewCount": ""
			  },
			  "description": "",
			  "name": ""
			};

$(document).ready(function () {
	//checkCryptoApp();
	//var enrollstatus=CourseEnrolled();
	$('#payUMoneyButton').on('click', function() {
		var today		= new Date().getTime();
		var req 		= {};
		var res;
		req.action		= 'purchaseStudentPackage';
		//req.courseId	= '34';
		req.slug		= slug;
		req.date		= today;
		req.couponCode	= "";
		req.couStatus	= "";
		$.ajax({
			'type' : 'post',
			'url'  : ApiEndpoint,
			'data' : JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status == 1) {
				if(res.paymentSkip == 1) {
					window.location = sitePathStudent;
				} else {
					// Metamask payment here
                    transferIGRO(res.amount,res.IGROTxnID);
                    toastr.warning('Please do not refresh without paying!', 'Package Payment', {timeOut: 3000});
				}
			} else {
				toastr.error(res.message);
				$('#purchaseNowModal').modal('hide');
			}
		});
	});
	$('.course-showcase').on('click', '#showVideo', function(){
		var videoPath = $(this).attr("data-video");
		var imgHeight = $('.course-img-preview').height();
		$('#showVideo').hide();
		$('#showcaseBlock').removeClass('hide');
		var playerWidth = $(".course-showcase").width();
		player = new MediaElementPlayer('#videoShowcasePlayer', {type: 'video/mp4',videoWidth: playerWidth,videoHeight: imgHeight});
		var sources = [
			{ src: videoPath, type: 'video/mp4' }
		];

		player.setSrc(sources);
		player.load();
		player.play();
	});
});
function loadPackageDetails(){
	var req		 = {};
	var startDate;
	var catIds	 = [], catNames = [];
	req.action	 = 'loadpackageDetails';
	req.slug	 = slug;
	if(req.slug != undefined) {
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			if (data.status == 1) {
				var today		= new Date().getTime();
				instituteId=data.packageDetails.ownerId;
				var startdate = new Date(parseInt(data.packageDetails.startDate));
				var enddate = new Date(parseInt(data.packageDetails.endDate));
				var price="";
				var displayCurrency=dispcurrency;

				$('#packageShowcase').html('<img src="'+data.packageDetails.image+'" alt="'+data.packageDetails.packName+'" class="btn-block" />');
				
				if(data.packageDetails.price == '0' || data.packageDetails.price == '0.00') {
					displayCurrency = '';
					data.packageDetails.price = 'Free';
				}
				amount =	data.packageDetails.price;
				
				$('.course-price').html('<span class="price-now">' + data.packageDetails.price + ' ' + displayCurrency +'</span>');
				$('#packageName').html(data.packageDetails.packName);
				$('#jsDescription').html('<p>'+data.packageDetails.packDescription+'<p>');
				if($('#jsDescription p').height()>100) {
					$('.course-description .js-package-details').removeClass("hide");
				}
				$('.js-share-facebook').attr('href','http://www.facebook.com/share.php?u='+encodeURI(window.location.href)+'&title='+data.packageDetails.packName);
				$('.js-share-twitter').attr('href','http://twitter.com/intent/tweet?status='+data.packageDetails.packName+' '+encodeURI(window.location.href));
				$('.js-share-google').attr('href','https://plus.google.com/share?url='+encodeURI(window.location.href));
				$('.js-share-facebook, .js-share-twitter, .js-share-google').click(function(e){
					e.preventDefault();
					var left = (window.screen.width / 2) - ((400 / 2) + 10);
					var top = (window.screen.height / 2) - ((500 / 2) + 50);

					window.open($(this).attr('href'), "MsgWindow", "top="+top+", left="+left+", width=500, height=400");
				});
				instituteName = data.packageDetails.username;
				link = sitepathMarket+'institute/';
				switch(parseInt(data.packageDetails.ownerRoleId)) {
					case 1:
						link = sitepathMarket+'institute/';
						break;
					case 2:
					case 3:
						link = sitepathMarket+'instructor/';
						break;
				}
				$('.js-package-institute-name').attr('href', link + instituteId);
				$('.js-package-institute-name').html(instituteName);
				/*$('#instituteImg').attr("src",  data.institute.profilePic);
				$('#instituteDescription').html(data.institute.description);
				if($('#instituteDescription').height()>102) {
					$('.about-instructor .js-instructor-details').removeClass("hide");
				}
				if(!data.institute.tagline) {
					$('#tagline').remove();
				} else {
					$('#tagline').html(data.institute.tagline);
				}*/
				$('#PackageID').html('P00' + data.packageDetails.packId);
				
				$('#noOfCourses').html(data.packageDetails.courses.length);
				//now working on dates
				var dateHTML 	 = '';
				var currentDate  = new Date(parseInt(data.currentDate));
				var liveDate 	 = new Date(parseInt(data.packageDetails.startDate));
				var calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
				var endDate 	 = (data.packageDetails.endDate == '')?'':new Date(parseInt(data.packageDetails.endDate));
				//console.log(currentDate, liveDate, endDate);
				if(endDate == '') {
					if(liveDate.valueOf() < currentDate.valueOf()) {
						$('#packageLiveEndDates').remove();
						$('#msgPackageValidity .note').html('<i class="fa fa-calendar"></i> Avail Course for 1 year from date of purchase');
					}
					else {
						$('#packageLiveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>');
						$('#packageLiveEndDates').remove();
						$('#msgPackageValidity .note').html('<i class="fa fa-calendar"></i> Avail Course for 1 year from Launch date');
					}
				} else {
					var futureDate = new Date(currentDate.getFullYear() + 1, currentDate.getMonth(), currentDate.getDate(), 0, 0, 0, 0);
					var calcEndDate = endDate.getDate()+ ' ' +MONTH[endDate.getMonth()]+ ' ' + endDate.getFullYear();
					if(endDate.valueOf() > futureDate.valueOf() && liveDate.valueOf() < currentDate.valueOf()) {
						$('#packageLiveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>');
						$('#packageLiveEndDates').remove();
						$('#msgPackageValidity .note').html('<i class="fa fa-calendar"></i> Avail Course for 1 year from Launch date');
					} else if(endDate.valueOf() > futureDate.valueOf() && liveDate.valueOf() > currentDate.valueOf()) {
						$('#packageLiveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>');
						$('#packageLiveEndDates').remove();
						$('#msgPackageValidity .note').html('<i class="fa fa-calendar"></i> Avail Course for 1 year from Launch date');
					} else {
						$('#packageLiveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>'+
												'<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> End Date :</label>'+
													'<span class="value" id="endDate"> '+calcEndDate+'</span>'+
												'</li>');
						$('#packageLiveEndDates').remove();
						$('#msgPackageValidity').remove();
					}
				}
				$('.page-load-hidden').removeClass("invisible");

				schema.name			= data.packageDetails.packName;
				schema.description	= data.packageDetails.packDescription;

				//if(loginstatus==1) {
					//$('#takeCourse').removeAttr('data-toggle');
					//$('.js-take-package').attr('href','javascript:;');
					$('.js-take-package').on('click', function() {
						if (userId == 0) {
							//toastr.error("Please login");
							$("#loginModal").modal("show");
						} else {
						    if (walletApp == '') {
						        $("#modalSetAddress").modal("show");
						    } else {
								if(userRole != 4) {
									toastr.error('You are not authorised to purchase this package. Please login with student account');
									return;
								}
								$('#purchaseNowModal .course-name').text($('#packageName').text());
								$('#purchaseNowModal .amount').html($('.price-now').html());
								$('#purchaseNowModal').modal('show');
							}
						}
					});
					/*$('.js-take-package:not([disabled])').on('click', function() {
						if(userRole != 4) {
							toastr.error('You are not authorised to purchase this package. Please login with student account');
							return;
						}
						$('#purchaseNowModal .course-name').text($('#packageName').text());
						$('#purchaseNowModal .amount').html($('.price-now').html());
						$('#purchaseNowModal').modal('show');
					});
					$('.js-take-package[disabled]').on('click', function() {
						toastr.error("Please log into Metamask and select Main Network");
					});*/
				//}
			}
			else if(data.status == 404) {
				window.location = sitePath404;
			}
			getCourseDetails();
		});
	}
	else
		window.location = sitePath404;
}

function getCourseDetails() {
	var html='';
	var req = {};
	req.action = 'show-AllpackageCourses';
	req.slug= slug;
	if (req.slug != undefined) {
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			if (data.courses.length>0) {
				$('#packageCourses').html(
					'<table class="table table-bordered">'+
						'<tr>'+
							'<th>Courses under this Package</th>'+
						'</tr>'+
					'</table>'
				);
				var packageEnrolled = 0;
				var ratingTotal		= 0;
				var ratingCourse	= 0;
				var ratedCourses	= 0;
				$.each(data.courses, function (i, course) {
					//variable to detect payment can be made or not
					courseIdArr[i]=	course.id;
					//var paymentStatus = 1;
					var startdate = new Date(course.startDate);
					var today = new Date();
					var enddate = new Date(startdate);
					enddate.setFullYear(enddate.getFullYear() + 1);

					// var percentage = (today.valueOf() - startdate.valueOf()) / (enddate.valueOf() - startdate.valueOf()) * 100;
					// var percent = parseFloat(percentage);
					//var display = percent.toFixed(2) + '%';
					var expired="";
					//if(enddate.valueOf()<today.valueOf()){
					//  expired="disable-mycourse";
					//}
					// alert(display);
					packageEnrolled = packageEnrolled + course.enroll;
					ratingTotal		= parseInt(ratingTotal) + parseInt(course.rating.total);
					if(course.rating.total > 0) {
						ratingCourse = parseFloat(ratingCourse) + parseFloat(course.rating.rating);
						ratedCourses++;
					}

					$('#packageCourses table').append(
							'<tr class="gradient-grey3" data-cid="'+i+'">'+
								'<td>'+
									'<div class="row">'+
										'<div class="col-md-3">'+
											'<div class="pad10">'+
												'<a href="javascript:void(0)" class="btn-block course course-detail-modal" id="'+i+'" data-courseId="' + course.id + '">'+
													'<img src="' + course.image + '" alt="' + course.name + '" class="btn-block course-img-sm" />'+
												'</a>'+
											'</div>'+
										'</div>'+
										'<div class="col-md-9">'+
											'<h4>'+
												'<a href="javascript:void(0)" class="btn-block course course-detail-modal" id="'+i+'" data-courseId="' + course.id + '">'+
													course.name + ' <small>(ID: C00' + course.id + ')</small>'+
												'</a>'+
											'</h4>'+
											'<p>'+
												'by <a href="' + sitepathMarket + 'instructor/' + course.userId + '" data-instituteId="' + course.userId + '">' + course.username + '</a>'+
											'</p>'+
											'<p><strong><i class="fa fa-th-list"></i> Subjects : </strong><span class="js-subjects"></span></p>'+
										'</div>'+
									'</div>'+
								'</td>'+
							'</tr>');
					// View Details button
					/*'<button class="btn btn-default btn-block course courseDetailModal" id="'+i+'" data-courseId="' + course.course_id + '">View Details</button>'+*/
					subjectHtml='';
					if (course.subjects.length>0) {
						$.each(course.subjects, function (j, subject) {
							subjectHtml +=subject.subjectName + ', ';
						});
						subjectHtml = subjectHtml.substring(0, subjectHtml.length - 2);
					};
					$('#packageCourses table tr:last-child').find('.js-subjects').html(subjectHtml);
				});//pacSubstable
				
				$('.js-package-enrolled').html(packageEnrolled);
				$('.packageRatingTotal').text(ratingTotal + ' ratings');
				if (ratedCourses>0) {
					ratingCourse = parseFloat(ratingCourse/ratedCourses).toFixed(2);
					$('.ratingStars').rating('rate', ratingCourse);
				} else {
					$('.ratingStars').rating('rate', 0);
				}
				schema.aggregateRating.ratingValue	= ratingCourse;
				schema.aggregateRating.reviewCount	= ratingTotal;
				
				//create JSON-LD for lastModified and citation_online_date
				var el = document.createElement('script');  
				el.type = 'application/ld+json';  
				el.id = 'jsonld';
				el.text = JSON.stringify(schema); 
				$('body').append(el);

				$('#packageCourses').on( "click",'.course-detail-modal', function() {		
					 var getId=$(this).attr('id');
					 loadPackageCourseDetails(courseIdArr[getId]);
					 $('#courseDetailModal').modal('show');
				});
			} else {
				$('.js-package-enrolled').html('0');
				$('.packageRatingTotal').text('0 ratings');
				$('.ratingStars').rating('rate', 0);
				$("#packageCourses").html('No courses under this package!');
			}
			removeLoader();
		});
	}
	else
		alert("No Course Found Details");
	//fetchCourseReviews();
}


function loadPackageCourseDetails(courseIdi){
	var req = {};
	var startDate;
	var catIds = [], catNames = [];
	req.action = 'loadPackageCourseDetails';
	req.courseId =courseIdi;
	if(req.courseId != undefined) {
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			console.log(res);
			if (data.status == 1) {
				var newprice=0;
				var today= new Date().getTime();
				var discountenddate = data.courseDetails.discount.endDate;
				if(data.courseDetails.discount.endDate > today )
				{
					newprice=data.courseDetails.studentPrice*((100 - data.courseDetails.discount.discount) / 100);
					newprice=newprice.toFixed(2);
					
				}
				var price="";
				var displayCurrency=dispcurrency;
				originalPrice=data.courseDetails.studentPrice;
				//alert(data.courseDetails.demoVideo);
				//filling demo video
				if(data.courseDetails.demoVideo != '') {
					$('#courseShowcase').html('<a href="javascript:void(0)" id="showVideo" data-video="'+data.courseDetails.demoVideo+'">'+
													'<span class="course-img-preview">'+
														'<img src="'+data.courseDetails.image+'" alt="'+data.courseDetails.name+'" class="btn-block" />'+
														'<i class="fa fa-play-circle-o"></i></a>'+
													'</span>'+
												'</a>'+
												'<div id="showcaseBlock" class="hide"><video id="videoShowcasePlayer"></video></div>');
				}
				else {
					$('#courseShowcase').html('<img src="'+data.courseDetails.image+'" alt="'+data.courseDetails.name+'" class="btn-block" />');
				}
				if(data.courseDetails.studentPrice == '0' || data.courseDetails.studentPrice == '0.00') {
					displayCurrency = '';
					data.courseDetails.studentPrice = 'Free';
				}
				//newprice> 0 $$ newprice< data.courseDetails.studentPrice
				originalPrice=data.courseDetails.studentPrice;
				dispcurrency=displayCurrency;
				
				if(data.courseDetails.liveForStudent == 1){

					if(data.courseDetails.studentPrice > 0)
					{ 
						$('#coursePrice').html('<span class="price-now">' + 'Want to buy only this course at ' + data.courseDetails.studentPrice + ' ' + displayCurrency + ' <a data-toggle="modal" href="'+sitePathCourseDetail+data.courseDetails.slug+'" target="_blank">Click here</a></span>');
					}
					else
					{	
						$('#coursePrice').html('<span class="price-now">' + 'Want to buy only this course for Free <a data-toggle="modal" href="'+sitePathCourseDetail+data.courseDetails.slug+'" target="_blank">Click here</a>'+'</span>');
					}
					
					newprice=" " + data.courseDetails.studentPrice + ' ' + displayCurrency;
				}

				$('#courseName').html(data.courseDetails.name);
				$('#subtitle').html(data.courseDetails.subtitle);
				$('#jsCourseDescription').html('<p>'+data.courseDetails.description+'<p>');
				if($('#jsCourseDescription p').height()>86) {
					$('.course-description .js-course-details').removeClass("hide");
				}
				//$('#course_img').attr("src", data.courseDetails.image);
				$('#targetAudience').html('');
				$('#courseCategory').html('');
				if (data.courseDetails.targetAudience != null) {
					var targetAudience = data.courseDetails.targetAudience.split(",");
					for (var i = targetAudience.length - 1; i >= 0; i--) {
						$('#targetAudience').append('<li>'+targetAudience[i]+'</li>');
					};
				} else {
					$('#targetAudience').append('<li>Anybody</li>');
				}
				if(data.courseCategories.length == 0) {
					$('#courseCategory').html('No Categories Selected');
				} else {
					$.each(data.courseCategories, function (k, cat) {
						catIds.push(cat.categoryId);
						catNames.push(cat.category);
						$('#courseCategory').append('<a href="'+sitePathCourses+cat.slug+'" class="btn btn-trans-color2 mr5 mb5">'+cat.category+'</a>');
					});
				}
				instituteName = data.institute.name;
				link = sitepathMarket+'institute/';
				switch(parseInt(data.courseDetails.roleId)) {
					case 1:
						instituteName = data.institute.name;
						link = sitepathMarket+'institute/';
						break;
					case 2:
					case 3:
						instituteName = data.institute.firstName + ' ' + data.institute.lastName;
						link = sitepathMarket+'instructor/';
						break;
				}
				$('.js-institute-link').attr('href', link + data.institute.userId);
				$('.js-institute-name').html(instituteName);
				$('#instituteImg').attr("src",  data.institute.profilePic);
				$('#instituteDescription').html(data.institute.description);
				if($('#instituteDescription').height()>102) {
					$('.about-instructor .js-instructor-details').removeClass("hide");
				}
				if(!data.institute.tagline) {
					$('#tagline').remove();
				} else {
					$('#tagline').html(data.institute.tagline);
				}
				$('#courseID').html('C00' + data.courseDetails.id);
				$('.js-enrolled').html(data.studentCount);
				//now working on dates
				var dateHTML = '';
				var currentDate = new Date(parseInt(data.currentDate));
				var liveDate = new Date(parseInt(data.courseDetails.liveDate));
				var calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
				var endDate = (data.courseDetails.endDate == '')?'':new Date(parseInt(data.courseDetails.endDate));

				/*if(endDate == '') {
					if(liveDate.valueOf() < currentDate.valueOf()) {
						$('#liveEndDates').remove();
						$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Course Available till 1 year from date of purchase');
					}
					else {
						$('#liveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>');
						$('#liveEndDates').remove();
						$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Course available till 1 year from Launch date');
					}
				}
				else {*/
				if(endDate != '') {
					var futureDate = new Date(currentDate.getFullYear() + 1, currentDate.getMonth(), currentDate.getDate(), 0, 0, 0, 0);
					var calcEndDate = endDate.getDate()+ ' ' +MONTH[endDate.getMonth()]+ ' ' + endDate.getFullYear();
					if(endDate.valueOf() > futureDate.valueOf() && liveDate.valueOf() < currentDate.valueOf()) {
						$('#liveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>');
						$('#liveEndDates').remove();
						//$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Course available till 1 year from Launch date');
						$('#msgValidity').remove();
					} else if(endDate.valueOf() > futureDate.valueOf() && liveDate.valueOf() > currentDate.valueOf()) {
						$('#liveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>');
						$('#liveEndDates').remove();
						//$('#msgValidity .note').html('<i class="fa fa-calendar"></i> Course available till 1 year from Launch date');
						$('#msgValidity').remove();
					}else {
						$('#liveEndDates').after('<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> Launch Date :</label>'+
													'<span class="value" id="liveDate"> '+calcLiveDate+'</span>'+
												'</li>'+
												'<li class="list-item-long">'+
													'<label for=""><i class="fa fa-calendar"></i> End Date :</label>'+
													'<span class="value" id="endDate"> '+calcEndDate+'</span>'+
												'</li>');
						$('#liveEndDates').remove();
						$('#msgValidity').remove();
					}
				} else {
					$('#liveEndDates').remove();
					$('#msgValidity').remove();
				}
				//$('#courseDetail').append(dateHTML);
				$('.page-load-hidden').removeClass("invisible");
				//console.log(loginstatus);
		
				$('.js-take-course').on('click', function() {
					$('#courseDetailModal').modal('hide');
				});
				/*$('#buycourseonly').on('click', function() {
					$('#courseDetail1').modal('hide');
				});*/

				//adding ratings
				if(data.courseDetails.rating.total == 0) {
					rating = 0;
				}
				else {
					rating = parseFloat(data.courseDetails.rating.rating).toFixed(1);
				}
				//$('.ratingStars').rating('rate', rating);
				$('.ratingTotal').text(data.courseDetails.rating.total + ' Ratings');
				$('.avg-rate').text(rating);
				//$('#courseDetail1').modal('hide');
				console.log("loginstatus"+ loginstatus);
				/*if(loginstatus==1) {
					$('.js-take-course').attr('href','');
					$('.js-take-course').on('click', function() {
					if(userRole != 4) {
							alert('You are not authorised to purchase this course. Please login with student account');
							return;
						}
						$('#purchaseNowModal .course-name').text($('#packageName').text());
						$('#purchaseNowModal .amount').html($('.price-now').html());
						$('#purchaseNowModal').modal('show');
					});			
				}*/
				getsubjectDetails(courseIdi);
			}
			else {
				alert(data.message);
			}
		});
	}
	else
		alert("No course Details");
}

function getsubjectDetails(courseIdi) {
	var req = {};
	req.action = 'loadSubjectDetails';
	req.courseId = courseIdi;
	//req.slug = slug;
	if (req.courseId != undefined || req.slug != undefined) {
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			if (data.status == 1) {
				var html = "";
				var subject_html = '';
				var professor = '';
				var subjectHeading='';
				var noOfLectures = 0;
				var noOfSubjects = data.subjects.length;
				var totalDuration = 0;
				var noOfExams = 0;
				var noOfAssignments = 0;
				var currentDate = 0;
				var liveDate = 0;
				var calcLiveDate = 0;
				var endDate = 0;
				var liveHTML = '';
				for (var i = 0; i < data.subjects.length; i++) {

					if(data.subjects[i].chapters.length>0 || data.subjects[i].independentExam.length || data.subjects[i].independentAssignment.length) {
						subject_html += '<table class="table table-responsive">'+
											'<tr class="list-row-subject">'+
												'<th class="course-subject">'+
													'<h2 class="subject-title">'+
														'<div class="subject-avatar">'+
															'<img src="' + data.subjects[i].image + '" alt="' + data.subjects[i].name + '" class="img-responsive">'+
														'</div>Subject: ' + data.subjects[i].name + ''+
													'</h2>'+
												'</th>'+
											'</tr>';
						if(data.subjects[i].independentExam == null)
							data.subjects[i].independentExam = 0;
						if(data.subjects[i].independentAssignment == null)
							data.subjects[i].independentAssignment = 0;

						if(data.subjects[i].chapters.length>0) {
						
							for (var j = 0; j < data.subjects[i].chapters.length; j++) {
								var examCount;
								var assCount;
								if(data.subjects[i].chapters[j].Exam > 0) {
									examCount = data.subjects[i].chapters[j].Exam + ' Exams';
								}
								else
									examCount = '--';
								if(data.subjects[i].chapters[j].Assignment > 0) {
									assCount = data.subjects[i].chapters[j].Assignment + ' Assignments';
								}
								else
									assCount = '--';
								if(data.subjects[i].chapters[j].Exam == null)
									data.subjects[i].chapters[j].Exam = 0; 
								if(data.subjects[i].chapters[j].Assignment == null)
									data.subjects[i].chapters[j].Assignment = 0;
								if(data.subjects[i].chapters[j].content.length>0 || data.subjects[i].chapters[j].Exam.length || data.subjects[i].chapters[j].Assignment.length) {
									subject_html += '<tr class="list-row-section">'+
														'<th class="ssection">'+
															'<div class="ssection-title">'+
																'<span class="ssection-title-txt">Section ' + (j+1) + ': ' + data.subjects[i].chapters[j].name + '</span>'+
																'<span class="ssection-meta"><span>' + examCount + ' Exams</span><span>' + assCount + ' Assignments</span><span>'+data.subjects[i].chapters.length+' lectures</span></span>'+
															'</div>'+
														'</th>'+
													'</tr>';
									if(data.subjects[i].chapters[j].content.length>0) {
										subject_html += '<tr class="list-row-section">'+
																'<td class="ssection-content">'+
																	'<div class="ssection-title">'+
																		'<span class="ssection-title-txt">Section Content</span>'+
																		'<span class="ssection-meta"><span>' + examCount + ' Exams</span></span>'+
																	'</div>'+
																'</td>'+
															'</tr>';

										//now adding lectures of the exam
										for(var k = 0; k < data.subjects[i].chapters[j].content.length; k++) {
											var type = '';
											var freePreview = '';
											var durationTime = '';
											switch(data.subjects[i].chapters[j].content[k].lectureType) {
												case 'Youtube':
													type = '<i class="fa fa-play-circle-o"></i>';
													break;
												case 'Vimeo':
													type = '<i class="fa fa-play-circle-o"></i>';
													break;
												case 'video':
													type = '<i class="fa fa-play-circle-o"></i>';
													if(data.subjects[i].chapters[j].content[k].demo == 1) {
														if(data.subjects[i].chapters[j].content[k].duration>0) {
															durationTime = '<span class="lecture-duration pull-right"></span>';
															freePreview = '<div class="lecture-play-btn">'+
																				'<button class="btn btn-primary btn-preview" data-src="' + data.subjects[i].chapters[j].content[k].metastuff + '">Free Demo (' + calcDisplayTime(data.subjects[i].chapters[j].content[k].duration) + ')</button>'+
																			'</div>';
														} else {
															freePreview = '<div class="lecture-play-btn">'+
																				'<button class="btn btn-primary btn-preview" data-src="' + data.subjects[i].chapters[j].content[k].metastuff + '">Free Demo</button>'+
																			'</div>';
														}
													}
													break;
												case 'doc':
													type = '<i class="fa fa-book"></i>';
													break;
												case 'ppt':
													type = '<i class="fa fa-file-powerpoint-o"></i>';
													break;
												case 'text':
													type = '<i class="fa fa-file-text-o"></i>';
													break;
												case 'dwn':
													type = '<i class="fa fa-cloud-download"></i>';
													break;
											}

											if(data.subjects[i].chapters[j].content[k].duration>0 && data.subjects[i].chapters[j].content[k].demo != 1) {
												durationTime = '<span class="lecture-duration pull-right">' + calcDisplayTime(data.subjects[i].chapters[j].content[k].duration) + '</span>';
											}
											subject_html += '<tr class="list-row-lecture">'+
																'<td class="lecture">'+
																	'<a href="javascript:void(0)" class="lecture-link btn-block">'+
																		'<span class="lecture-play">' + type +'</span>'+
																		'<div class="lecture-title-blk">'+
																			'<span class="lecture-number">' + (j+1) + '.' + (k+1) + '</span>'+
																			'<span class="lecture-title-txt">' + data.subjects[i].chapters[j].content[k].lectureName + '</span>'+
																			durationTime +
																		'</div>' + freePreview +
																	'</a>'+
																'</td>'+
															'</tr>';
											noOfLectures++;
										}
									}
									if(data.subjects[i].chapters[j].Exam.length) {
										subject_html += '<tr class="list-row-section">'+
															'<td class="ssection-content">'+
																'<div class="ssection-title">'+
																	'<span class="ssection-title-txt">Section Exams</span>'+
																	'<span class="ssection-meta"><span>' + examCount + ' Exams</span></span>'+
																'</div>'+
															'</td>'+
														'</tr>';
										for(var k = 0; k < data.subjects[i].chapters[j].Exam.length; k++) {
											currentDate = new Date(parseInt(data.currentDate));
											liveDate = new Date(parseInt(data.subjects[i].chapters[j].Exam[k].startDate));
											calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
											if(liveDate.valueOf() > currentDate.valueOf()) {
												liveHTML = '<span class="lecture-duration pull-right">Live date : '+calcLiveDate+'</span>';
											} else {
												liveHTML = '';
											}

											subject_html += '<tr class="list-row-lecture">'+
															'<td class="lecture">'+
																'<a href="javascript:void(0)" class="lecture-link btn-block">'+
																	'<span class="lecture-play"><i class="fa fa-pencil-square-o"></i></span>'+
																	'<div class="lecture-title-blk">'+
																		'<span class="lecture-number">' + (j+1) + '.' + (k+1) + '</span>'+
																		'<span class="lecture-title-txt">' + data.subjects[i].chapters[j].Exam[k].name + '</span>'+
																		liveHTML +
																	'</div>'
																'</a>'+
															'</td>'+
														'</tr>';
											noOfExams++;
										}
									}
									if(data.subjects[i].chapters[j].Assignment.length) {
										subject_html += '<tr class="list-row-section">'+
															'<td class="ssection-content">'+
																'<div class="ssection-title">'+
																	'<span class="ssection-title-txt">Section Assignments</span>'+
																	'<span class="ssection-meta"><span>' + assCount + ' Assignments</span></span>'+
																'</div>'+
															'</td>'+
														'</tr>';
										for(var k = 0; k < data.subjects[i].chapters[j].Assignment.length; k++) {
											currentDate = new Date(parseInt(data.currentDate));
											liveDate = new Date(parseInt(data.subjects[i].chapters[j].Assignment[k].startDate));
											calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
											if(liveDate.valueOf() > currentDate.valueOf()) {
												liveHTML = '<span class="lecture-duration pull-right">Live date : '+calcLiveDate+'</span>';
											} else {
												liveHTML = '';
											}

											subject_html += '<tr class="list-row-lecture">'+
															'<td class="lecture">'+
																'<a href="javascript:void(0)" class="lecture-link btn-block">'+
																	'<span class="lecture-play"><i class="fa fa-book"></i></span>'+
																	'<div class="lecture-title-blk">'+
																		'<span class="lecture-number">' + (j+1) + '.' + (k+1) + '</span>'+
																		'<span class="lecture-title-txt">' + data.subjects[i].chapters[j].Assignment[k].name + '</span>'+
																		liveHTML +
																	'</div>'
																'</a>'+
															'</td>'+
														'</tr>';
											noOfAssignments++;
										}
									}
								}
							}
						}

						if(data.subjects[i].independentExam.length) {
							subject_html += '<tr class="list-row-section">'+
												'<th class="ssection">'+
													'<div class="ssection-title">'+
														'<span class="ssection-title-txt">Independent Exams</span>'+
														'<span class="ssection-meta"><span>' + data.subjects[i].independentExam.length + ' Exams</span></span>'+
													'</div>'+
												'</th>'+
											'</tr>';
							for(var k = 0; k < data.subjects[i].independentExam.length; k++) {
								currentDate = new Date(parseInt(data.currentDate));
								liveDate = new Date(parseInt(data.subjects[i].independentExam[k].startDate));
								calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
								if(liveDate.valueOf() > currentDate.valueOf()) {
									liveHTML = '<span class="lecture-duration pull-right">Live date : '+calcLiveDate+'</span>';
								} else {
									liveHTML = '';
								}
								subject_html += '<tr class="list-row-lecture">'+
												'<td class="lecture">'+
													'<a href="javascript:void(0)" class="lecture-link btn-block">'+
														'<span class="lecture-play"><i class="fa fa-pencil-square-o"></i></span>'+
														'<div class="lecture-title-blk">'+
															'<span class="lecture-number">' + (k+1) + '</span>'+
															'<span class="lecture-title-txt">Exam : ' + data.subjects[i].independentExam[k].name + '</span>'+
															liveHTML +
														'</div>'
													'</a>'+
												'</td>'+
											'</tr>';
								noOfExams++;
							}
						}
						if(data.subjects[i].independentAssignment.length) {
							subject_html += '<tr class="list-row-section">'+
												'<th class="ssection">'+
													'<div class="ssection-title">'+
														'<span class="ssection-title-txt">Independent Assignments</span>'+
														'<span class="ssection-meta"><span>' + data.subjects[i].independentAssignment.length + ' Assignments</span></span>'+
													'</div>'+
												'</th>'+
											'</tr>';
							for(var k = 0; k < data.subjects[i].independentAssignment.length; k++) {
								currentDate = new Date(parseInt(data.currentDate));
								liveDate = new Date(parseInt(data.subjects[i].independentAssignment[k].startDate));
								calcLiveDate = liveDate.getDate()+ ' ' +MONTH[liveDate.getMonth()]+ ' ' + liveDate.getFullYear();
								if(liveDate.valueOf() > currentDate.valueOf()) {
									liveHTML = '<span class="lecture-duration pull-right">Live date : '+calcLiveDate+'</span>';
								} else {
									liveHTML = '';
								}
								subject_html += '<tr class="list-row-lecture">'+
												'<td class="lecture">'+
													'<a href="javascript:void(0)" class="lecture-link btn-block">'+
														'<span class="lecture-play"><i class="fa fa-book"></i></span>'+
														'<div class="lecture-title-blk">'+
															'<span class="lecture-number">' + (k+1) + '</span>'+
															'<span class="lecture-title-txt">Assignment : ' + data.subjects[i].independentAssignment[k].name + '</span>'+
															liveHTML +
														'</div>'
													'</a>'+
												'</td>'+
											'</tr>';
								noOfAssignments++;
							}
						}
						
						if(data.subjects[i].totalDuration>0) {
							totalDuration = totalDuration + parseInt(data.subjects[i].totalDuration);
						}
						subject_html += '</table>';
						subjectCount++;
					}
				}
				if(totalDuration>0) {
					var result = calcDisplayTimeWords(totalDuration);
					$('#courseTest').after(
										'<li class="list-item-short">'+
											'<label for=""><i class="fa fa-youtube-play"></i> Duration :</label>'+
											'<span class="value" id="totalDuration"> '+result+'</span>'+
										'</li>');
					$('#courseTest').remove();
				} else {
					$('#courseTest').after(
										'<li class="list-item-short">'+
											'<label for=""><i class="fa fa-question-circle"></i> Questions :</label>'+
											'<span class="value" id="totalDuration"> '+data.totalQuestions+'</span>'+
										'</li>');
					$('#courseTest').remove();
				}

				$('#curriculum').html(subject_html);
				initPlayer();
				/*$('.viewDemo').on('click', function() {
					window.location = 'student/chapterContent.php?courseId=' + getUrlParameter('courseId') + '&subjectId=' + $(this).parents('table').attr('data-subjectId') +'&chapterId=' + $(this).parents('tr').attr('data-chapterId') + '&demo=1';
				});
				$.each($('#course-append table'), function() {
					$(this).find('.soup').hide();
					$.each($(this).find('.soup'), function() {
						if($(this).text() != '--') {
							$(this).parents('table').find('.soup').show();
							return;
						}
					});
					$(this).find('tr.chapter:eq(0), tr.chapter:eq(1), tr.chapter:eq(2)').show();
					if($(this).find('tr.chapter').length > 3)
						$(this).parents('.about').find('.viewMoreChap').show();
					$('.viewMoreChap').off('click');
					$('.viewMoreChap').on('click', function() {
						if($(this).attr('data-hidden') == 0) {
							$(this).parents('.about').find('table').find('tr').show();
							$(this).text('Hide');
							$(this).attr('data-hidden', '1');
						}
						else {
							$(this).parents('.about').find('table').find('tr').hide();
							$(this).parents('.about').find('table').find('tr.chapter:eq(0), tr.chapter:eq(1), tr.chapter:eq(2)').show();
							$(this).parents('.about').find('table').find('tr.independent').show();
							$(this).text('View More');
							$(this).attr('data-hidden', '0');
						}
					});
				});*/
			}
			$('#professors').html(professors);
			$('#noOfSubjects').html(noOfSubjects);

			var contentBuild = 0;
			var contentClass = 0;
			var contentHTML	 = "";
			if(noOfLectures>0) {contentBuild++;}
			if(noOfExams>0) {contentBuild++;}
			if(noOfAssignments>0) {contentBuild++;}
			if(contentBuild>0) {
				switch(contentBuild) {
					case 1: contentClass = "list-item-long";break;
					case 2: contentClass = "list-item-short";break;
					case 3: contentClass = "list-item-xshort";break;
				}
				if(noOfLectures>0) {
					contentHTML+=  '<li class="'+contentClass+'">'+
										'<label for=""><i class="fa fa-youtube-play"></i> Lectures :</label>'+
										'<span class="value" id="noOfLectures"> '+noOfLectures+'</span>'+
									'</li>';
				}
				if(noOfExams>0) {
					contentHTML+=  '<li class="'+contentClass+'">'+
										'<label for=""><i class="fa fa-pencil-square-o"></i> Exams :</label>'+
										'<span class="value" id="noOfExams"> '+noOfExams+'</span>'+
									'</li>';
				}
				if(noOfAssignments>0) {
					contentHTML+=  '<li class="'+contentClass+'">'+
										'<label for=""><i class="fa fa-book"></i> Assignments :</label>'+
										'<span class="value" id="noOfAssignments"> '+noOfAssignments+'</span>'+
									'</li>';
				}
				$('#contentStats').after(contentHTML);
				$('#contentStats').remove();
			}
		});
	}
	else
		alert("No Subject Found Details");
}

function initPlayer() {
	$('#curriculum').on("click", ".btn-preview", function(){
		//event.preventDefault();
		$('#courseDetailModal').modal('hide');
		var src = $(this).attr('data-src');
		$("#playerModal").modal('show');
		setTimeout(function(){
			var playerWidth = $("#playerModal .modal-dialog").width();
			player = new MediaElementPlayer('#videoPlayer', {type: 'video/mp4',videoWidth: playerWidth,videoHeight: '100%'});
			var sources = [
				{ src: src, type: 'video/mp4' }
			];

			player.setSrc(sources);
			player.load();
			player.play();
		},500);
		
	});
	$('#dismissVideo').click(function(){
		player.pause();
		$('#courseDetailModal').modal('show');
	});
}
$(window).scroll(function(){
	if($(window).scrollTop() >= $('.course-showcase').offset().top) {
		$('#jsCourseMenu').addClass('affix');
	} else {
		$('#jsCourseMenu').removeClass('affix');
	}
});
$('.course-sublinks').click(function(){
	event.preventDefault();
	var target = $(this).attr('href');
	$('html, body').animate({
		scrollTop: $(target).offset().top
	}, 1000);
});
$(".js-course-details").click(function(){
	toggleDataMore($(this),$(".js-full-course-details"));
});
$(".js-instructor-details").click(function(){
	toggleDataMore($(this),$(".js-full-instructor-details"));
});

</script>