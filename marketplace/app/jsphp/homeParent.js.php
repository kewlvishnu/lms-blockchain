<script type="text/javascript">
var schema = {
	"@context": "http://schema.org",
	"@type": "WebSite",
	"url": "http://www.integro.io/",
	"potentialAction": {
		"@type": "SearchAction",
		"target": "https://www.integro.io/search/?q={q}",
		"query-input": "required name=q"
	}
};
$(document).ready(function(){
	//create JSON-LD for lastModified and citation_online_date
	var el = document.createElement('script');  
	el.type = 'application/ld+json';  
	el.id = 'jsonld';
	el.text = JSON.stringify(schema); 
	$('body').append(el);
	$("#testimonialsSlider").owlCarousel({
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		items:1,
		lazyLoad:true,
		loop:true
	});
	$(".login-signup .form-control").keyup(function(e){
		if(e.which!=9 && e.which!=13) {
			var disError = $(this).closest(".js-cover").find(".help-block");
			disError.html('').closest('.form-group').removeClass('has-success').removeClass('has-error');
		}
	});
	$(".js-btn-login").click(function(){
		var role 	 = $(this).closest(".js-cover").attr("data-role");
		var username = $(this).closest(".js-cover").find("[name=inputUsername]");
		var password = $(this).closest(".js-cover").find("[name=inputPassword]");
		var remember = $(this).closest(".js-cover").find("[name=optionRemember]");
		var disError = $(this).closest(".js-cover").find(".help-block");
		if(username.val().length<5) {
			disError.html("A valid username is required.").closest('.form-group').removeClass('has-success').addClass('has-error');
			username.focus();
		} else if(password.val().length<5) {
			disError.html("A valid password is required.").closest('.form-group').removeClass('has-success').addClass('has-error');
			password.focus();
		} else {
			req = {
					'action' : 'parent-login',
					'username' : username.val(),
					'password' : password.val(),
					'userRole' : role,
					'remember' : ((remember.prop("checked"))?1:0)
				};
			req=JSON.stringify(req);
			$.post(ApiEndpoint, req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				console.log(res);
				if(res.valid == false) {
					if(res.reset == 1) {
						disError.html(res.reason + '<a href="#" class="resend-link">Resend email</a>').closest('.form-group').removeClass('has-success').addClass('has-error');
						addResetListener(res.userId);
					}
					else
						disError.html(res.reason).closest('.form-group').removeClass('has-success').addClass('has-error');
				}
				if(res.valid == true && res.userRole == 6) {
					window.location = sitePath+"parent/";
				}
				else
				{
					alert("There was some error!");
				}
			});
		}
		return false;
	});
	var formParent = {
		'type' : "Parent",
		'addressCountryId' : 0,
		'agree' : 0,
		'action' : 'register-short',
		'userType' : 'parent',
		'userRole' : 6,
		'gender' : 0
	};
	function emailValidation(element,user) {
		var frmGroup=element.closest('.form-group');
		var disError=element.closest('.js-cover').find('.help-block');
		var frmError=disError.closest('.form-group');
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(regex.test(element.val())) {
			frmGroup.removeClass('has-error');
			var req = {};
			req.action = 'validate-email';
			req.email = element.val();
			req.role = user.userRole;
			req=JSON.stringify(req);
			$.post(ApiEndpoint, req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.valid == false) {
					frmGroup.removeClass('has-success').addClass('has-error');
					disError.html("Email address already registered please select a different one.");
					frmError.removeClass('has-success').addClass('has-error');
					//element.focus();
				}
				else {
					frmGroup.removeClass('has-error').addClass('has-success');
					disError.html("");
					frmError.removeClass('has-success').removeClass('has-error');
					return true;
				}
			});
		}
		else {
			frmGroup.removeClass('has-success').addClass('has-error');
			disError.html("Doesn't seem to be a valid email address.");
			frmError.removeClass('has-success').addClass('has-error');
			//element.focus();
		}
		return false;
	}
});
</script>