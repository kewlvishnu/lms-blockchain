<script type="text/javascript">
$(document).ready(function(){
	$("#testimonialsSlider").owlCarousel({
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		items:1,
		lazyLoad:true,
		loop:true
	});
	$(".js-obtn-login").click(function(){
		var userType = $(this).attr("data-user");
		switch(userType) {
			case "professor": 
							$(".js-cover").addClass("hide");
							$("#professorSignin").removeClass("hide");
							$(".js-obtn-login").removeClass("hide");
							$(this).addClass("hide");
							$(".js-tagline").hide().html(
													'<h2 class="head-md"><p><i class="fa fa-graduation-cap"></i></p>Teach Students online. Impart knowledge and grow your income.</h2>'+
													'<a href="javascript:void(0)" class="btn btn-lg btn-color2 get-professor">Register and start teaching</a>'
													).fadeIn();
							break;
			case "institute": 
							$(".js-cover").addClass("hide");
							$("#instituteSignin").removeClass("hide");
							$(".js-obtn-login").removeClass("hide");
							$(this).addClass("hide");
							$(".js-tagline").hide().html(
													'<h2 class="head-md"><p><i class="fa fa-institution"></i></p>Teach Students online by collaborating with other Instructors. Impart knowledge and grow your Institute\'s income</h2>'+
													'<a href="javascript:void(0)" class="btn btn-lg btn-color2 get-institute">Register and start teaching &amp; collaborating</a>'
													).fadeIn();
							break;
			default:		alert("There was some error"); break;
		}
	});
	$("body").on("click", ".get-professor", function(e) {
		e.preventDefault();
		$(".js-cover").addClass("hide");
		$("#professorSignup").removeClass("hide");
		$(".js-obtn-login").removeClass("hide");
		$('.js-obtn-login[data-user="professor"]').addClass("hide");
		$(".js-tagline").hide().html(
								'<h2 class="head-md"><p><i class="fa fa-graduation-cap"></i></p>Teach Students online. Impart knowledge and grow your income.</h2>'+
								'<a href="javascript:void(0)" class="btn btn-lg btn-color2 get-professor">Register and start teaching</a>'
								).fadeIn();
	});
	$("body").on("click", ".get-institute", function(e) {
		e.preventDefault();
		$(".js-cover").addClass("hide");
		$("#instituteSignup").removeClass("hide");
		$(".js-obtn-login").removeClass("hide");
		$('.js-obtn-login[data-user="institute"]').addClass("hide");
		$(".js-tagline").hide().html(
								'<h2 class="head-md"><p><i class="fa fa-institution"></i></p>Teach Students online by collaborating with other Instructors. Impart knowledge and grow your Institute\'s income</h2>'+
								'<a href="javascript:void(0)" class="btn btn-lg btn-color2 get-institute">Register and start teaching &amp; collaborating</a>'
								).fadeIn();
	});
	$('.js-forgot').click(function(){
		var userType = $(this).closest(".js-cover").attr("data-user");
		if(!userType) {
			alert("There was some error");
		} else {
			$(".js-cover").addClass("hide");
			$("#"+userType+"Forgot").removeClass("hide");
		}
	});
	$(".js-login-user").click(function(){
		var userType = $(this).closest(".js-cover").attr("data-user");
		if(!userType) {
			alert("There was some error");
		} else {
			$(".js-cover").addClass("hide");
			$("#"+userType+"Signin").removeClass("hide");
		}
	});
	$(".js-new-user").click(function(){
		var userType = $(this).closest(".js-cover").attr("data-user");
		if(!userType) {
			alert("There was some error");
		} else {
			$(".js-cover").addClass("hide");
			$("#"+userType+"Signup").removeClass("hide");
		}
	});
	$(document).on('click','.scrolltodiv', function(event) {
	    event.preventDefault();
	    var target = "#" + this.getAttribute('data-target');
	    $('html, body').animate({
	        scrollTop: $(target).offset().top
	    }, 1000);
	});
	$(".login-signup .form-control").keyup(function(e){
		if(e.which!=9 && e.which!=13) {
			var disError = $(this).closest(".js-cover").find(".help-block");
			disError.html('').closest('.form-group').removeClass('has-success').removeClass('has-error');
		}
	});
	$(".js-btn-login").click(function(){
		var role 	 = $(this).closest(".js-cover").attr("data-role");
		var username = $(this).closest(".js-cover").find("[name=inputUsername]");
		var password = $(this).closest(".js-cover").find("[name=inputPassword]");
		var remember = $(this).closest(".js-cover").find("[name=optionRemember]");
		var disError = $(this).closest(".js-cover").find(".help-block");
		if(username.val().length<3) {
			disError.html("A valid username is required.").closest('.form-group').removeClass('has-success').addClass('has-error');
			username.focus();
		} else if(password.val().length<6) {
			disError.html("A valid password is required.").closest('.form-group').removeClass('has-success').addClass('has-error');
			password.focus();
		} else {
			req = {
					'action' : 'login',
					'username' : username.val(),
					'password' : password.val(),
					'userRole' : role,
					'remember' : ((remember.prop("checked"))?1:0)
				};
			req=JSON.stringify(req);
			$.post(ApiEndpoint, req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.valid == false) {
					if(res.reset == 1) {
						disError.html(res.reason + '<a href="#" class="resend-link">Resend email</a>').closest('.form-group').removeClass('has-success').addClass('has-error');
						addResetListener(res.userId);
					}
					else
						disError.html(res.reason).closest('.form-group').removeClass('has-success').addClass('has-error');
				}
				if(res.valid == true && res.userRole == 4) {
					if(typeof(notify) === "undefined") {
						if(typeof(paymentPageStudent) === "undefined" ){
							window.location = sitePathStudent;
						}else{
							window.location = sitePath+"courseDetails.php?courseId="+courseId;
						}
					}
					else {
						window.location = sitePath+"student/";
					}
				}
				else if(res.valid == true && (res.userRole == 1 || res.userRole == 2))
				{
					if(typeof (InstitutePurchasePage) === 'undefined'){
						window.location = sitepathManage;
					}else{
						window.location = sitePathCourseDetail+courseId;
					}
				}
				else if(res.valid == true)
				{
					if(typeof (InstitutePurchasePage) === 'undefined'){
						window.location = sitePath+"admin/dashboard.php";
					}else{
						window.location = sitePath+'courseDetailsInstitute.php?courseId='+courseId;
					}
				}
			});
		}
		return false;
	});
	$(".js-btn-reset").click(function(){
		var thiz 	 = $(this);
		var role 	 = $(this).closest(".js-cover").attr("data-role");
		var username = $(this).closest(".js-cover").find("[name=inputUsername]");
		var disError = $(this).closest(".js-cover").find(".help-block");
		if(username.val().length<3) {
			disError.html("A valid username is required.").closest('.form-group').removeClass('has-success').addClass('has-error');
			username.focus();
		} else {
			var req = {};
			req.action = 'send-reset-password-link';
			req.username = username.val();
			req.userRole = role
			req = JSON.stringify(req);
			$.post(ApiEndpoint, req).success(function(resp) {
				res = jQuery.parseJSON(resp);
				if(res.status==0) {
					disError.html(res.message).closest('.form-group').removeClass('has-success').addClass('has-error');
				} else {
					disError.html(res.message).closest('.form-group').removeClass('has-error').addClass('has-success');
					thiz.attr("disabled",true);
				}
			});
		}
		return false;
	});
	var formProfessor = {
		'type' : "Professor",
		'userInstituteTypes' : [ ],
		'stucturedInsTypes' : { },
		'addressCountryId' : 1,
		'userSchoolBoard' : 0,
		'userSchoolClasses' : 0,
		'agreeOn1' : 0,
		'agreeOn2' : 0,
		'action' : 'register-short',
		'userType' : 'professor',
		'userRole' : 2,
		'gender' : 0
	};
	var formInstitute = {
		'type' : "Institute",
		'userInstituteTypes' : [ ],
		'stucturedInsTypes' : { },
		'addressCountryId' : 1,
		'userSchoolBoard' : 0,
		'userSchoolClasses' : 0,
		'agreeOn1' : 0,
		'agreeOn2' : 0,
		'action' : 'register-short',
		'userType' : 'institute',
		'userRole' : 1
	};
	function emailValidation(element,user) {
		var frmGroup=element.closest('.form-group');
		var disError=element.closest('.js-cover').find('.help-block');
		var frmError=disError.closest('.form-group');
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(regex.test(element.val())) {
			frmGroup.removeClass('has-error');
			var req = {};
			req.action = 'validate-email';
			req.email = element.val();
			req.role = user.userRole;
			req=JSON.stringify(req);
			$.post(ApiEndpoint, req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.valid == false) {
					frmGroup.removeClass('has-success').addClass('has-error');
					disError.html("Email address already registered please select a different one.");
					frmError.removeClass('has-success').addClass('has-error');
					//element.focus();
				}
				else {
					frmGroup.removeClass('has-error').addClass('has-success');
					disError.html("");
					frmError.removeClass('has-success').removeClass('has-error');
					return true;
				}
			});
		}
		else {
			frmGroup.removeClass('has-success').addClass('has-error');
			disError.html("Doesn't seem to be a valid email address.");
			frmError.removeClass('has-success').addClass('has-error');
			//element.focus();
		}
		return false;
	}
	$("#inputProfessorSignupEmail").blur(function(){
		emailValidation($(this),formProfessor);
	});
	$("#inputInstituteSignupEmail").blur(function(){
		emailValidation($(this),formInstitute);
	});
	$("#btnProfessorSignup").click(function(){
		var disError 				= $("#professorSignup").find('.help-block');
		var frmError 				= disError.closest('.form-group');
		var inputProfessorFirstName	= $("#inputProfessorFirstName");
		var inputProfessorLastName	= $("#inputProfessorLastName");
		var inputProfessorSignupEmail = $("#inputProfessorSignupEmail");
		var inputProfessorSignupPassword = $("#inputProfessorSignupPassword");
		var selectProfessorCountry 	= $("#selectProfessorCountry");
		var inputContactMobile		= $("#inputContactMobile");
		var optionProfessorAgree	= $("#optionProfessorAgree");
		if(inputProfessorFirstName.val().length<3) {
			disError.html("First Name is invalid.");
			frmError.removeClass('has-success').addClass('has-error');
			inputProfessorFirstName.focus();
		} else if(inputProfessorLastName.val().length<3) {
			disError.html("Last Name is invalid.");
			frmError.removeClass('has-success').addClass('has-error');
			inputProfessorLastName.focus();
		} else if($(inputProfessorSignupEmail).closest('.form-group').hasClass('has-error') ||
					(!$(inputProfessorSignupEmail).closest('.form-group').hasClass('has-error') && !$(inputProfessorSignupEmail).closest('.form-group').hasClass('has-success'))
				) {
			emailValidation(inputProfessorSignupEmail,formProfessor);
		} else if(inputProfessorSignupPassword.val().length<6) {
			disError.html("A valid password is required.");
			frmError.removeClass('has-success').addClass('has-error');
			inputProfessorSignupPassword.focus();
		} else if(!selectProfessorCountry.val()) {
			disError.html("Country is required.");
			frmError.removeClass('has-success').addClass('has-error');
			selectProfessorCountry.focus();
		} else if(!inputContactMobile.val()) {
			disError.html("Mobile number is required.");
			frmError.removeClass('has-success').addClass('has-error');
			inputContactMobile.focus();
		} else if(!optionProfessorAgree.prop("checked")) {
			disError.html("Please agree to our terms and policy.");
			frmError.removeClass('has-success').addClass('has-error');
			optionProfessorAgree.focus();
		} else {
			formProfessor.firstName 	  = inputProfessorFirstName.val();
			formProfessor.lastName 		  = inputProfessorLastName.val();
			formProfessor.email 		  = inputProfessorSignupEmail.val();
			formProfessor.password 		  = inputProfessorSignupPassword.val();
			formProfessor.addressCountryId= selectProfessorCountry.val();
			formProfessor.contactMobilePrefix = $('#inputPrefixContactMobile').val();
			if(!$('#inputPrefixContactMobile').val())
				formProfessor.contactMobilePrefix="+91";
			formProfessor.contactMobile   = inputContactMobile.val();
			formProfessor.agree 		  = optionProfessorAgree.prop("checked");
			formProfessor 				  = JSON.stringify(formProfessor);
			$.post(ApiEndpoint, formProfessor).success(function(resp) {
                formProfessor = JSON.parse(formProfessor);
				res=jQuery.parseJSON(resp);
				if(res.status == 1)
					window.location=sitepathMarket+"signup/complete/" + res.userId;
				if(res.status == 0)
					alert(res.message);
			});
		}
		return false;
	});
	$("#btnInstituteSignup").click(function(){
		var disError 				= $("#instituteSignup").find('.help-block');
		var frmError 				= disError.closest('.form-group');
		var inputInstituteName		= $("#inputInstituteName");
		var inputInstituteSignupEmail = $("#inputInstituteSignupEmail");
		var inputInstituteSignupPassword = $("#inputInstituteSignupPassword");
		var selectInstituteCountry 	= $("#selectInstituteCountry");
		var inputInstituteContactMobile		= $("#inputInstituteContactMobile");
		var optionInstituteAgree	= $("#optionInstituteAgree");
		if(inputInstituteName.val().length<3) {
			disError.html("Institute Name is invalid.");
			frmError.removeClass('has-success').addClass('has-error');
			inputInstituteName.focus();
		} else if($(inputInstituteSignupEmail).closest('.form-group').hasClass('has-error') ||
					(!$(inputInstituteSignupEmail).closest('.form-group').hasClass('has-error') && !$(inputInstituteSignupEmail).closest('.form-group').hasClass('has-success'))
				) {
			emailValidation(inputInstituteSignupEmail,formInstitute);
		} else if(inputInstituteSignupPassword.val().length<6) {
			disError.html("A valid password is required.");
			frmError.removeClass('has-success').addClass('has-error');
			inputInstituteSignupPassword.focus();
		} else if(!selectInstituteCountry.val()) {
			disError.html("Country is required.");
			frmError.removeClass('has-success').addClass('has-error');
			selectInstituteCountry.focus();
		} else if(!inputInstituteContactMobile.val()) {
			disError.html("Mobile number is required.");
			frmError.removeClass('has-success').addClass('has-error');
			inputInstituteContactMobile.focus();
		} else if(!optionInstituteAgree.prop("checked")) {
			disError.html("Please agree to our terms and policy.");
			frmError.removeClass('has-success').addClass('has-error');
			optionInstituteAgree.focus();
		} else {
			formInstitute.instituteName	  = inputInstituteName.val();
			formInstitute.email 		  = inputInstituteSignupEmail.val();
			formInstitute.password 		  = inputInstituteSignupPassword.val();
			formInstitute.addressCountryId= selectInstituteCountry.val();
			formInstitute.contactMobilePrefix = $('#inputInstitutePrefixContactMobile').val();
			if(!$('#inputInstitutePrefixContactMobile').val())
				formInstitute.contactMobilePrefix="+91";
			formInstitute.contactMobile   = inputInstituteContactMobile.val();
			formInstitute.agree 		  = optionInstituteAgree.prop("checked");
			formInstitute 				  = JSON.stringify(formInstitute);
			$.post(ApiEndpoint, formInstitute).success(function(resp) {
                formInstitute = JSON.parse(formInstitute);
				res=jQuery.parseJSON(resp);
				if(res.status == 1)
					window.location=sitepathMarket+"signup/complete/" + res.userId;
				if(res.status == 0)
					alert(res.message);
			});
		}
		return false;
	});
});
</script>