<script type="text/javascript">
var sitePath	= '<?php echo $sitepath; ?>';
var sitePath404 = '<?php echo $sitepath404; ?>';
var sitepathMarket	= '<?php echo $sitepathMarket; ?>';
var sitepathPackages	 = '<?php echo $sitepathPackages; ?>';
var sitepathPackageDetail= '<?php echo $sitepathPackageDetail; ?>';
var sitePathCourses		 = '<?php echo $sitepathCourses; ?>';
var sitePathCourseDetail = '<?php echo $sitepathCourseDetail; ?>';
var sitePathStudent = '<?php echo $sitepathStudent; ?>';
var sitepathManage = '<?php echo $sitepathManage; ?>';
var sitepathLogout = '<?php echo $sitepathLogout; ?>';
var page = '<?php echo $page; ?>';
var ApiEndpoint = sitePath+'api/index.php';
var slug = '<?php echo ((isset($slug))?($slug):('')); ?>';
var subdomain = '<?php echo ((isset($subdomain))?($subdomain):('')); ?>';
var dispcurrency='<?php echo $global->displayCurrency; ?>';
toastr.options = {
	"closeButton": true,
	"debug": false,
	"positionClass": "toast-top-center",
	"onclick": null,
	"showDuration": "1000",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "1000",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
}
function signOut() {
	gapi.load('auth2', function() {
		gapi.auth2.init().then(() => {
			var auth2 = gapi.auth2.getAuthInstance();
			auth2.signOut().then(function () {
				console.log('User signed out.');
				window.location = sitepathLogout;
			});
		}).catch(function (err) {
			window.location = sitepathLogout;
		});
	});
}

$(document).ready(function() {

	Checklogin();
    
    $(".btn-logout").click(function(event) {
        /* Act on the event */
        signOut();
    });

	/*$('.btn-crypto-app').click(function(event) {
		if (userId == 0) {
			//toastr.error("Please login");
			$("#loginModal").modal("show");
		} else {
			checkCryptoApp();
		}
	});*/
});

function Checklogin(){
	var req = {};
	var res;
	req.action = 'check-login';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status==1){
			loginstatus = 1;
			userId = res.userId;
			userRole = res.userRole;
			afterLoginCalls();
		} else {
			beforeLoginCalls();
		}
	});
}
function afterLoginCalls(){
	if(page == 'home') {
		getTrendingCourses();
		getTemplateCousres();
	} else if(page == 'packages') {
		getFeaturedPackages();
	} else if(page == 'package') {
		loadPackageDetails();
	} else if(page == 'courses') {
		getTemplateCousres();
	} else if(page == 'course') {
		loadCourseDetails();
	} else if(page == 'instructor' || page == 'institute') {
		fetchProfileDetails();
	} else if(page == 'search') {
		$('#searchBox').val('<?php echo ((isset($search["q"]))?($search["q"]):('')); ?>');
		fillPriceFilter();
	} else {
		removeLoader();
	};
}

function beforeLoginCalls() {
	if(page == 'home') {
		getTrendingCourses();
		getTemplateCousres();
	} else if(page == 'packages') {
		getFeaturedPackages();
	} else if(page == 'package') {
		loadPackageDetails();
	} else if(page == 'courses') {
		getTemplateCousres();
	} else if (page == 'course') {
		CourseEnrolled();
	} else if(page == 'instructor' || page == 'institute') {
		fetchProfileDetails();
	} else if(page == 'verify') {
		verifyAccount();
	} else if(page == 'search') {
		$('#searchBox').val('<?php echo ((isset($search["q"]))?($search["q"]):('')); ?>');
		fillPriceFilter();
	} else {
		removeLoader();
	};
}

function getUrlParameter(sParam)
{
	sParam = sParam.toLowerCase();
	var sPageURL = window.location.search.substring(1).toLowerCase();
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) 
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) 
		{
			return sParameterName[1];
		}
	}
}
$(document).on('click','.scrolltodiv', function(event) {
	event.preventDefault();
	var target = "#" + this.getAttribute('data-target');
	$('html, body').animate({
		scrollTop: $(target).offset().top
	}, 1000);
});
var toggleDataMore = function(objLink,objData) {
	var dataMore = objLink.attr("data-more");
	if(dataMore == "full") {
		objData.css("height","auto");
		objLink.attr("data-more","less").html("Hide details");
	} else {
		if(objData.hasClass('js-full-instructor-details')) {
			objData.css("height","100px");
		} else {
			objData.css("height","100px");
		}
		objLink.attr("data-more","full").html("Full details");
	}
}
/**
* Funtion to format single digit number into double digit i.e. append one before item
*
* @author Rupesh Pandey
* @date 23/05/2015
* @param integer input number
* @return integer two digit number
*/
function format(number) {
	if(number < 10)
		return '0' + number;
	else
		return number;
}

function percentage(number, total) {
	if(total == 0)
		return 0;
	return (parseInt(number)/parseInt(total)) * 100;
}

function calcDisplayTimeWords(totalDuration) {
	var hours = parseInt( totalDuration / 3600 ) % 24;
	var minutes = parseInt( totalDuration / 60 ) % 60;
	var seconds = totalDuration % 60;

	if(hours>0) {
		if(minutes>0 && minutes<=15) {
			return hours+".25h";
		} else if(minutes>15 && minutes<=30) {
			return hours+".5h";
		} else if(minutes>30 && minutes<=45) {
			return hours+".75h";
		} else {
			return hours+"h";
		}
	} else if(minutes>0) {
		if(seconds>0 && seconds<=15) {
			return minutes+".25m";
		} else if(seconds>15 && seconds<=30) {
			return minutes+".5m";
		} else if(seconds>30 && seconds<=45) {
			return minutes+".75m";
		} else {
			return minutes+"m";
		}
	} else {
		return seconds+" s";
	}
}

function calcDisplayTime(totalSec) {
	var hours = parseInt( totalSec / 3600 ) % 24;
	var minutes = parseInt( totalSec / 60 ) % 60;
	var seconds = totalSec % 60;

	if(hours>0) {
		return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
	} else {
		return (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
	}
}

function formatContactNumber(user) {
	var num1= "", num2 = "";
	if(user.contactMobilePrefix != '' && user.contactMobile != '') {
		num1 = user.contactMobilePrefix + '-' + user.contactMobile;
	}
	
	if(user.contactLandlinePrefix != '' && user.contactLandline != '') {
		num2 = user.contactLandlinePrefix + '-' + user.contactLandline;
	}
	
	return (num1 + ', ' + num2).replace(/,\s$/,'').replace(/^,\s/,'');
}

function removeLoader() {
	//$('#loader').remove();
}

function emailValidate(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(regex.test(email))
		return true;
	return false;
}
</script>