<script type="text/javascript">
var schema = {
	"@context": "http://schema.org",
	"@type": "WebSite",
	"url": "http://www.integro.io/",
	"potentialAction": {
		"@type": "SearchAction",
		"target": "https://www.integro.io/search/?q={q}",
		"query-input": "required name=q"
	}
};
$(document).ready(function(){
	//create JSON-LD for lastModified and citation_online_date
	var el = document.createElement('script');  
	el.type = 'application/ld+json';  
	el.id = 'jsonld';
	el.text = JSON.stringify(schema); 
	$('body').append(el);
	$("#testimonialsSlider").owlCarousel({
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		items:1,
		lazyLoad:true,
		loop:true
	});
	$('.js-forgot').click(function(){
		var userType = $(this).closest(".js-cover").attr("data-user");
		if(!userType) {
			alert("There was some error");
		} else {
			$(".js-cover").addClass("hide");
			$("#"+userType+"Forgot").removeClass("hide");
		}
	});
	$(".js-login-user").click(function(){
		var userType = $(this).closest(".js-cover").attr("data-user");
		if(!userType) {
			alert("There was some error");
		} else {
			$(".js-cover").addClass("hide");
			$("#"+userType+"Signin").removeClass("hide");
		}
	});
	$(".js-new-user").click(function(){
		var userType = $(this).closest(".js-cover").attr("data-user");
		if(!userType) {
			alert("There was some error");
		} else {
			$(".js-cover").addClass("hide");
			$("#"+userType+"Signup").removeClass("hide");
		}
	});
	$(".login-signup .form-control").keyup(function(e){
		if(e.which!=9 && e.which!=13) {
			var disError = $(this).closest(".js-cover").find(".help-block");
			disError.html('').closest('.form-group').removeClass('has-success').removeClass('has-error');
		}
	});
	$(".js-btn-login").click(function(){
		var role 	 = $(this).closest(".js-cover").attr("data-role");
		var username = $(this).closest(".js-cover").find("[name=inputUsername]");
		var password = $(this).closest(".js-cover").find("[name=inputPassword]");
		var remember = $(this).closest(".js-cover").find("[name=optionRemember]");
		var disError = $(this).closest(".js-cover").find(".help-block");
		if(username.val().length<3) {
			disError.html("Username has to be minimum 3 characters.").closest('.form-group').removeClass('has-success').addClass('has-error');
			username.focus();
		} else if(password.val().length<6 || password.val().length>20) {
			disError.html("Password needs to be between 6-20 characters.").closest('.form-group').removeClass('has-success').addClass('has-error');
			password.focus();
		} else {
			req = {
					'action' : 'login',
					'username' : username.val(),
					'password' : password.val(),
					'userRole' : role,
					'remember' : ((remember.prop("checked"))?1:0)
				};
			req=JSON.stringify(req);
			$.post(ApiEndpoint, req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.valid == false) {
					if(res.reset == 1) {
						disError.html(res.reason + '<a href="#" class="resend-link">Resend email</a>').closest('.form-group').removeClass('has-success').addClass('has-error');
						addResetListener(res.userId);
					}
					else if(res.init == 1) {
						window.location = window.location;
					}
					else
						disError.html(res.reason).closest('.form-group').removeClass('has-success').addClass('has-error');
				}
				if(res.valid == true && res.userRole == 4) {
					if(typeof(notify) === "undefined") {
						if(typeof(paymentPageStudent) === "undefined" ){
							window.location = sitePathStudent;
						}else{
							window.location = "courseDetails.php?courseId="+courseId;
						}
					}
					else {
						window.location = "student/notification.php";
					}
				}
				else if(res.valid == true && (res.userRole == 1 || res.userRole == 2))
				{
					if(typeof (InstitutePurchasePage) === 'undefined'){
						window.location = sitepathManage;
					}else{
						window.location = sitePathCourseDetail+courseId;
					}
				}
				else if(res.valid == true)
				{
					if(typeof (InstitutePurchasePage) === 'undefined'){
						window.location = "admin/dashboard.php";
					}else{
						window.location = 'courseDetailsInstitute.php?courseId='+courseId;
					}
				}
			});
		}
		return false;
	});
	$(".js-btn-reset").click(function(){
		var thiz 	 = $(this);
		var role 	 = $(this).closest(".js-cover").attr("data-role");
		var username = $(this).closest(".js-cover").find("[name=inputUsername]");
		var disError = $(this).closest(".js-cover").find(".help-block");
		if(username.val().length<3) {
			disError.html("A valid username is required.").closest('.form-group').removeClass('has-success').addClass('has-error');
			username.focus();
		} else {
			var req = {};
			req.action = 'send-reset-password-link';
			req.username = username.val();
			req.userRole = role
			req = JSON.stringify(req);
			$.post(ApiEndpoint, req).success(function(resp) {
				res = jQuery.parseJSON(resp);
				if(res.status==0) {
					disError.html(res.message).closest('.form-group').removeClass('has-success').addClass('has-error');
				} else {
					disError.html(res.message).closest('.form-group').removeClass('has-error').addClass('has-success');
					thiz.attr("disabled",true);
				}
			});
		}
		return false;
	});
	var formStudent = {
		'type' : "Student",
		'addressCountryId' : 0,
		'agree' : 0,
		'action' : 'register-short',
		'userType' : 'student',
		'userRole' : 4,
		'gender' : 0
	};
	function emailValidation(element,user) {
		var frmGroup=element.closest('.form-group');
		var disError=element.closest('.js-cover').find('.help-block');
		var frmError=disError.closest('.form-group');
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(regex.test(element.val())) {
			frmGroup.removeClass('has-error');
			var req = {};
			req.action = 'validate-email';
			req.email = element.val();
			req.role = user.userRole;
			req=JSON.stringify(req);
			$.post(ApiEndpoint, req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.valid == false) {
					frmGroup.removeClass('has-success').addClass('has-error');
					disError.html("Email address already registered please select a different one.");
					frmError.removeClass('has-success').addClass('has-error');
					//element.focus();
				}
				else {
					frmGroup.removeClass('has-error').addClass('has-success');
					disError.html("");
					frmError.removeClass('has-success').removeClass('has-error');
					return true;
				}
			});
		}
		else {
			frmGroup.removeClass('has-success').addClass('has-error');
			disError.html("Doesn't seem to be a valid email address.");
			frmError.removeClass('has-success').addClass('has-error');
			//element.focus();
		}
		return false;
	}
	$("#inputStudentSignupEmail").blur(function(){
		emailValidation($(this),formStudent);
	});
	$("#btnStudentSignup").click(function(){
		var disError 				= $("#studentSignup").find('.help-block');
		var frmError 				= disError.closest('.form-group');
		var inputFirstName			= $("#inputFirstName");
		var inputLastName			= $("#inputLastName");
		var inputStudentSignupEmail = $("#inputStudentSignupEmail");
		var inputStudentSignupPassword = $("#inputStudentSignupPassword");
		var optionAgree				= $("#optionAgree");
		if(inputFirstName.val().length<2 || inputFirstName.val().length>20) {
			disError.html("First Name needs to be between 2-20 characters.");
			frmError.removeClass('has-success').addClass('has-error');
			inputFirstName.focus();
		} else if(inputLastName.val().length<1 || inputLastName.val().length>20) {
			disError.html("Last Name needs to be between 1-20 characters.");
			frmError.removeClass('has-success').addClass('has-error');
			inputLastName.focus();
		} else if($(inputStudentSignupEmail).closest('.form-group').hasClass('has-error') ||
					(!$(inputStudentSignupEmail).closest('.form-group').hasClass('has-error') && !$(inputStudentSignupEmail).closest('.form-group').hasClass('has-success'))
				) {
			emailValidation(inputStudentSignupEmail,formStudent);
		} else if(inputStudentSignupPassword.val().length<6 || inputStudentSignupPassword.val().length>20) {
			disError.html("Password needs to be between 6-20 characters.");
			frmError.removeClass('has-success').addClass('has-error');
			inputStudentSignupPassword.focus();
		} else if(!optionAgree.prop("checked")) {
			disError.html("Please agree to our terms and policy.");
			frmError.removeClass('has-success').addClass('has-error');
			optionAgree.focus();
		} else {
			formStudent.firstName 	= inputFirstName.val();
			formStudent.lastName 	= inputLastName.val();
			formStudent.email 		= inputStudentSignupEmail.val();
			formStudent.password 	= inputStudentSignupPassword.val();
			formStudent.agree 		= optionAgree.prop("checked");
			formStudent 			= JSON.stringify(formStudent);
			$.post(ApiEndpoint, formStudent).success(function(resp) {
                formStudent = JSON.parse(formStudent);
				res=$.parseJSON(resp);
				if(res.status == 1)
					window.location=sitepathMarket+"signup/complete/" + res.userId;
				if(res.status == 0)
					alert(res.message);
			});
		}
		return false;
	});
	$('.btn-facebook').click(function(e){
		e.preventDefault();
		//hello('facebook').login();
		var disError = $(this).closest(".js-cover").find(".help-block");
		login('facebook',disError);
		return false;
	});
	function login(network,disError) {
		var facebook = hello(network);
		facebook.login({scope:'email', force:true}).then(function(auth) {
			// get user profile data
			hello(network).api('/me').then(function responseHandler(r) {
				console.log(r);
				var inputFirstName			= r.first_name;
				var inputLastName			= r.last_name;
				var email					= r.email;
				var facebookId 				= r.id;
				if(facebookId == "") {
				} else {
					var formSocial			= {};
					formSocial.action		= "facebook-login";
					formSocial.firstName	= inputFirstName;
					formSocial.lastName		= inputLastName;
					formSocial.email		= email;
					formSocial.userRole		= 4;
					formSocial.facebookId	= facebookId;
					formSocial.network		= auth.network;
					formSocial				= JSON.stringify(formSocial);
					$.post(ApiEndpoint, formSocial).success(function(resp) {
                        formSocial = JSON.parse(formSocial);
						var res=$.parseJSON(resp);
						if(res.valid == false) {
							if(res.reset == 1) {
								disError.html(res.reason + '<a href="#" class="resend-link">Resend email</a>').closest('.form-group').removeClass('has-success').addClass('has-error');
								addResetListener(res.userId);
							}
							else
								disError.html(res.reason).closest('.form-group').removeClass('has-success').addClass('has-error');
						}
						if(res.valid == true && res.userRole == 4) {
							if(typeof(notify) === "undefined") {
								if(typeof(paymentPageStudent) === "undefined" ){
									window.location = sitePathStudent;
								}else{
									window.location = "courseDetails.php?courseId="+courseId;
								}
							}
							else {
								window.location = "student/notification.php";
							}
						}
						else if(res.valid == true && (res.userRole == 1 || res.userRole == 2))
						{
							if(typeof (InstitutePurchasePage) === 'undefined'){
								window.location = sitepathManage;
							}else{
								window.location = sitePathCourseDetail+courseId;
							}
						}
						else if(res.valid == true)
						{
							if(typeof (InstitutePurchasePage) === 'undefined'){
								window.location = "admin/dashboard.php";
							}else{
								window.location = 'courseDetailsInstitute.php?courseId='+courseId;
							}
						}
					});
				}
			});
		}, function() {
			if(!auth||auth.error){
				console.log("Signin aborted");
				return;
			}
		});
	}
	// Signin button click handler
	hello.init({
		facebook: '877235672348652'
	}, {
		redirect_uri: window.location
	});
});
function addResetListener(userId) {
	$('.resend-link').off('click');
	//resend verification link
	$('.resend-link').on('click', function() {
		var req = {};
		var res;
		req.id = userId;
		if(req.id == undefined)
			alert("Some error occurred contact admin.");
		req.action = 'resend-verification-link';
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
				alert("Verification mail sent. Please check your mailbox.");
		});
	});
}
</script>