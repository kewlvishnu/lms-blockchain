<script type="text/javascript">
function getFeaturedPackages(){
	var req = {};
	var res;
	req.action = 'all-package';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if (res.status == 1) {
			fillFeaturedPackages(res.packages, 'grid');
		}
		console.log(res);
	});
}
function fillFeaturedPackages(packages,grid){
	for (var i = 0; i < packages.length; i++) {
		if (packages[i].price == '0' || packages[i].price == '0.00') {
			packages[i].price = 'Free';
		} else {
			packages[i].price = packages[i].price+' '+dispcurrency;
		}
		$('#'+grid).append(
			'<li>'+
				'<a href="'+sitepathPackageDetail+packages[i].slug+'" class="hover-shadow">'+
					'<img src="'+packages[i].image+'" alt="'+packages[i].packName+'">'+
					'<span class="text-center pad10 dib w100 bg-grey1 color-color1">'+
						'<span class="ellipsis-2lines text-uppercase">'+packages[i].packName+'</span>'+
						'<span>'+packages[i].price+'</span>'+
					'</span>'+
				'</a>'+
			'</li>');
		if (i == (packages.length-1)) {
			new AnimOnScroll( document.getElementById( grid ), {
				minDuration : 0.4,
				maxDuration : 0.7,
				viewportFactor : 0.2
			});
		};
	};
	removeLoader();
}
</script>