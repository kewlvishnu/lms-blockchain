<script type="text/javascript">
var web3network = '';
var PortisProvider = window.Portis.PortisProvider;
var currAddress = '<?php echo $global->walletAddress; ?>';
var walletApp = '<?php echo $global->walletApp; ?>';
var IGROdecimals = parseInt(<?php echo $global->decimalPrecision; ?>);
var IGROBalanceDivisor = parseInt(<?php echo $global->balanceDivisor; ?>);
$(document).ready(function() {
  $("#btnMMAssociateAddress").click(function(event) {
    /* Act on the event */
    var address = $("#caAddress").text();
    saveAddress('m',address);
  });
  $("#btnMMChangeAddress").click(function(event) {
    /* Act on the event */
    var address = $("#ccAddress").text();
    saveAddress('m',address);
  });
  $("#btnPOAssociateAddress").click(function(event) {
    /* Act on the event */
    var address = $("#paAddress").text();
    saveAddress('p',address);
  });
  $("#btnPOChangeAddress").click(function(event) {
    /* Act on the event */
    var address = $("#pcAddress").text();
    saveAddress('p',address);
  });
  $("#btnWalletSetAddress").click(function(event) {
    /* Act on the event */
    var optionSetVia = $("[name='optionSetVia']:checked").val();
    setAddress(optionSetVia);
  });
});
function initWeb3() {
  if (walletApp == 'm') {
    if (typeof web3 !== 'undefined') {
      //console.log(web3.currentProvider);
      // Use Mist/MetaMask's provider
      setMM();
      async function setMM() {
        if (window.ethereum) {
            window.web3 = new Web3(ethereum);
            try {
                // Request account access if needed
                await ethereum.enable();
                // Acccounts now exposed
                web3network = getETHNetwork();
                // doTransaction(tokens,orderId);
            } catch (error) {
                // User denied account access...
                toastr.error("User denied account access");
            }
        }// Legacy dapp browsers...
        else if (window.web3) {
            window.web3 = new Web3(web3.currentProvider);
            // Acccounts always exposed
            web3network = getETHNetwork();
            // doTransaction(tokens,orderId);
        }
        // Non-dapp browsers...
        else {
            toastr.error("Non-Ethereum browser detected. You should consider trying MetaMask!");
            console.log('Non-Ethereum browser detected. You should consider trying MetaMask!');
        }
      };
    } else {
      $(".modal").modal("hide");
      $("#modalMetamaskNotInstalled").modal("show");
    }
  } else {
    //$("#modalMetamaskNotInstalled").modal("show");
    window.web3 = new Web3(new PortisProvider({
        apiKey: "ecf3f011229650b900669c42f4933181",
        network: 'rinkeby'
    }));
    web3.currentProvider.setDefaultEmail(userEmail,false);
    //console.log(web3);
    web3network = getETHNetwork();
    //doTransaction(tokens,orderId);
    //monitorAccountChanges();
  }
}
function getETHNetwork() {
  var output = "";
  if (walletApp == "m") {
    web3.version.getNetwork((err, res) => {

      if (!err) {
        if(res > 1000000000000) {
          output = "testrpc";
        } else {
          switch (res) {
            case "1":
              output = "mainnet";
              break
            case "2":
              output = "morden";
              break
            case "3":
              output = "ropsten";
              break
            case "4":
              output = "rinkeby";
              break
            default:
              output = "unknown network = "+res;
          }
        }
      } else {
        output = "Error";
      }
    });
  } else {
    output = "rinkeby";
  }
  //toastr.info(output);
  web3network = output;
  return output;
}
function setAddress(optionPayVia) {
  // body...
  web3.eth.getAccounts(function(err, res){
    if (res.length>0) {
      var account = res[0];
      checkAddressPromise(account, "Your address").then(function(){
        $(".modal").modal("hide");
        if (optionPayVia == 'm') {
          $("#caAddress").html(account);
          $("#modalMetamaskAssociateAddress").modal("show");
        } else {
          $("#paAddress").html(account);
          $("#modalPortisAssociateAddress").modal("show");
        }
      });
    } else {
      if (optionPayVia == 'm') {
        toastr.error("Please login into MetaMask!");
      } else {
        toastr.error("Please login into Portis!");
      }
    }
  });
}
function saveAddress(optionBlockchain,address) {
  var req = {};
  var res;
  req.action = 'save-wallet-address';
  req.network = web3network;
  req.address = address;
  req.blockchain = optionBlockchain;
  $.ajax({
      'type'  : 'post',
      'url' : ApiEndpoint,
      'data'  : JSON.stringify(req)
  }).done(function(res) {
    res = $.parseJSON(res);
    if(res.status == 0)
      toastr.error(res.message);
    else {
      $("#modalMetamaskAssociateAddress").modal("hide");
      $("#modalMetamaskChangeAddress").modal("hide");
      toastr.success(res.message);
      window.location = window.location;
    }
  });
}
function transferIGRO(tokens,orderId) {
  doTransaction(tokens,orderId);
}
function doTransaction(tokens,orderId) {
  var toAddress = $("#ownerAddress").val();
  //console.log(web3.eth.accounts);
  if (!$.isNumeric(currAddress) || currAddress=='') {
    //toastr.error("Your address is not set, please set your address");
    setAddress(walletApp);
  } else if (!$.isNumeric(toAddress) || toAddress=='') {
    toastr.error("The address is invalid, please enter a valid address");
  } else {
    // Check if Ethereum node is found
    if(web3.isConnected()){
      const contractAddress = $("#contractAddress").val();
      //const fromAddress = $("#ownerAddress").val();
      checkAddressPromise(contractAddress, "Contract address").then(function(){
        return checkAddressPromise(toAddress, "To account")
      }).then(function(){
        web3.eth.getAccounts(function(err, res){
          if (res.length>0) {
            var blockchainAddress = res[0];
            checkAddressPromise(blockchainAddress, "Your address").then(function(){
              if (blockchainAddress != currAddress) {
                toastr.error("The address you saved and the one you are using are different! Please use your saved wallet.");
              } else {
                const contract = createContract();
                contract.balanceOf(currAddress, function (err, res) {
                  if(err) {
                    toastr.error(err);
                  } else {
                    var balance = res.c[0]/IGROBalanceDivisor;
                    //toastr.info('Balance: '+balance);

                    // set the default account
                    web3.eth.defaultAccount = currAddress;
                    var defaultAccount = web3.eth.defaultAccount;
                    //console.log(defaultAccount); // ''
                    //web3.eth.defaultAccount=$.trim(web3.eth.accounts[0]);
                    if (balance>tokens) {
                      contract.transfer(toAddress, tokens * 10**IGROdecimals, function (err, res) {
                        if(res) {
                          //toastr.success("Transaction successful. Transaction Hash: " + res);
                          saveTransaction(res,orderId);
                          $("#modalPurchaseNowCourseKeys").modal("hide");
                        } else {
                          //toastr.error("showResult: "+err);
                          toastr.error("Transaction Failed!");
                        }
                      });
                    } else {
                      toastr.error("Your don't have sufficient balance!");
                    }
                  }
                });
              }
            });
          } else {
            if (walletApp == 'm') {
              toastr.error("Please login into MetaMask!");
            } else {
              toastr.error("Please login into Portis!");
            }
          }
        });

      }).catch(function(message){
        toastr.error("Not a valid "+message+".");
      });
    }
  }
}
function saveTransaction(txnHash,IGROTxnID) {
  var req = {};
  var res;
  req.action = 'save-crypto-transaction';
  req.network = web3network;
  req.address = currAddress;
  req.txnHash = txnHash;
  req.IGROTxnID = IGROTxnID;
  $.ajax({
      'type'  : 'post',
      'url' : ApiEndpoint,
      'data'  : JSON.stringify(req)
  }).done(function(res) {
    res = $.parseJSON(res);
    if(res.status == 0)
      toastr.error(res.message);
    else {
      toastr.success(res.message,'',
      {
        "timeOut": "0",
        "extendedTimeOut": "0",
      });
      if (page == 'course' || page == 'package') {
        $("#purchaseNowModal").modal("hide");
      }
    }
  });
}
function createContract(){
  // Each time you modify the DemoContract.sol and deploy it on the blockchain, you need to get the abi value.
  // Paste the abi value in web3.eth.contract(PASTE_ABI_VALUE);
  // When the contract is deployed, do not forget to change the contract address, see
  // formfield id 'contractAddress'
  // Replace contract address: 0xf1d2e0b8e09f4dda7f3fd6db26496f74079faeeb with your own.
  //
  const contractSpec = web3.eth.contract(<?php echo $global->contractCode; ?>);

  return contractSpec.at($("#contractAddress").val());
}
const checkAddressPromise = function(address, addressType) {
  return new Promise(function(resolve, reject){
    if (address != null && web3.isAddress(address)) {
      resolve();
    } else {
      //reject(addressType);
      toastr.error(addressType+" is invalid");
    }
  });
}

function getPortisInfo() {
  toastr.info("Please wait few seconds for your wallet to open");
  web3.eth.getAccounts(function(err, res){
    if (res.length>0) {
      var blockchainAddress = res[0];
      checkAddressPromise(blockchainAddress, "Your address").then(function(){
        if (blockchainAddress != currAddress) {
          toastr.error("The address you saved and the one you are using are different! Please use your saved wallet.");
        } else {
          web3.currentProvider.showPortis(() => {
            console.log('Portis window was closed by the user');
          });
        }
      });
    }
  });
}
</script>