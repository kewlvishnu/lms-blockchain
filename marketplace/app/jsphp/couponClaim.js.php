<script>
$(document).ready(function(){
	$("#frmLogin").submit(function(){
		var username = $("#frmLogin").find("[name=inputUsername]");
		var password = $("#frmLogin").find("[name=inputPassword]");
		var remember = $("#frmLogin").find("[name=optionRemember]");
		var disError = $("#frmLogin").find(".help-block");
		if(username.val().length<3) {
			disError.html("A valid username is required.").closest('.form-group').removeClass('has-success').addClass('has-error');
			username.focus();
		} else if(password.val().length<6) {
			disError.html("A valid password is required.").closest('.form-group').removeClass('has-success').addClass('has-error');
			password.focus();
		} else {
			req = {
					'action' : 'login',
					'username' : username.val(),
					'password' : password.val(),
					'userRole' : '4',
					'remember' : ((remember.prop("checked"))?1:0)
				};
			req=JSON.stringify(req);
			$.post(ApiEndpoint, req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.valid == false) {
					if(res.reset == 1) {
						disError.html(res.reason + '<a href="#" class="resend-link">Resend email</a>').closest('.form-group').removeClass('has-success').addClass('has-error');
						addResetListener(res.userId);
					}
					else
						disError.html(res.reason).closest('.form-group').removeClass('has-success').addClass('has-error');
				} else {
					getContestCoupon(res.userId);
				}
			});
		}
		return false;
	});
	var formStudent = {
		'type' : "Student",
		'addressCountryId' : 0,
		'agree' : 0,
		'action' : 'register-short',
		'userType' : 'student',
		'userRole' : 4,
		'gender' : 0
	};
	function emailValidation(element,user) {
		var frmGroup=element.closest('.form-group');
		var disError=$("#frmSignup").find('.help-block');
		var frmError=disError.closest('.form-group');
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(regex.test(element.val())) {
			frmGroup.removeClass('has-error');
			var req = {};
			req.action = 'validate-email';
			req.email = element.val();
			req.role = user.userRole;
			req=JSON.stringify(req);
			$.post(ApiEndpoint, req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.valid == false) {
					frmGroup.removeClass('has-success').addClass('has-error');
					disError.html("Email address already registered please select a different one.");
					frmError.removeClass('has-success').addClass('has-error');
					//element.focus();
				}
				else {
					frmGroup.removeClass('has-error').addClass('has-success');
					disError.html("");
					frmError.removeClass('has-success').removeClass('has-error');
					return true;
				}
			});
		}
		else {
			frmGroup.removeClass('has-success').addClass('has-error');
			disError.html("Doesn't seem to be a valid email address.");
			frmError.removeClass('has-success').addClass('has-error');
			//element.focus();
		}
		return false;
	}
	$("#inputStudentSignupEmail").blur(function(){
		emailValidation($(this),formStudent);
	});
	$("#frmSignup").submit(function(){
		var disError 				= $("#frmSignup").find('.help-block');
		var frmError 				= disError.closest('.form-group');
		var inputFirstName			= $("#inputFirstName");
		var inputLastName			= $("#inputLastName");
		var inputStudentSignupEmail = $("#inputStudentSignupEmail");
		var inputStudentSignupPassword = $("#inputStudentSignupPassword");
		var optionAgree				= $("#optionAgree");
		if(inputFirstName.val().length<3) {
			disError.html("First Name is invalid.");
			frmError.removeClass('has-success').addClass('has-error');
			inputFirstName.focus();
		} else if(inputLastName.val().length<3) {
			disError.html("Last Name is invalid.");
			frmError.removeClass('has-success').addClass('has-error');
			inputLastName.focus();
		} else if($(inputStudentSignupEmail).closest('.form-group').hasClass('has-error') ||
					(!$(inputStudentSignupEmail).closest('.form-group').hasClass('has-error') && !$(inputStudentSignupEmail).closest('.form-group').hasClass('has-success'))
				) {
			emailValidation(inputStudentSignupEmail,formStudent);
		} else if(inputStudentSignupPassword.val().length<6) {
			disError.html("A valid password is required.");
			frmError.removeClass('has-success').addClass('has-error');
			inputStudentSignupPassword.focus();
		} else if(!optionAgree.prop("checked")) {
			disError.html("Please agree to our terms and policy.");
			frmError.removeClass('has-success').addClass('has-error');
			optionAgree.focus();
		} else {
			formStudent.firstName 	= inputFirstName.val();
			formStudent.lastName 	= inputLastName.val();
			formStudent.email 		= inputStudentSignupEmail.val();
			formStudent.password 	= inputStudentSignupPassword.val();
			formStudent.agree 		= optionAgree.prop("checked");
			formStudent 			= JSON.stringify(formStudent);
			$.post(ApiEndpoint, formStudent).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.status == 1)
					getContestCoupon(res.userId);
				if(res.status == 0)
					alert(res.message);
			});
		}
		return false;
	});
});
function getContestCoupon (userId) {
	var req = {};
	req.action	   = 'get-contest-coupon';
	req.uniqueCode = slug;
	req.userId	   = userId;
	$.post(ApiEndpoint, JSON.stringify(req)).success(function(resp) {
		res=jQuery.parseJSON(resp);
		if(res.status == 1)
			$('#couponClaim').html(
				'<div class="col-sm-12">'+
					'<div class="well well-lg text-center">'+
						'<h3 class="jumbotron">Congratulations!</h3>'+
						'<p>You have won a coupon to take any course upto Rs.3500/- for free!</p>'+
						'<p>Here is your coupon code</p>'+
						'<p><strong>'+res.coupon+'</strong></p>'+
						'<p><a href="'+sitepathMarket+'courses/" class="btn btn-info">Checkout Courses</a></p>'+
					'</div>'+
				'</div>'
				);
		if(res.status == 0)
			alert(res.message);
	});
}
</script>