<script type="text/javascript">

function getTrendingCourses(){
	var req = {};
	var res;
	req.action = 'featured-template-courses';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		var html = fillTemplateCourses(res);
		//console.log(res);
		$('#trendingCourses').html(html);
		$('.rating').rating();
	});
}
function getTemplateCousres(){
	var req = {};
	var res;
	req.slug   = slug;
	req.action = 'template-courses';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		var html = fillTemplateCourses(res);
		//console.log(res);
		$('#featuredCourses').html(html);
		$('.rating').rating();
		if ($('#categoryTitle').length>0) {
			$('#categoryTitle').html(res.category.title);
			$('#categoryDesc').html(res.category.desc);
		};
		removeLoader();
	});
}

function fillTemplateCourses(data) {

	var html='';
	var licenseHtml='';
	var displayCurrency=dispcurrency;
	var discount='';
	$.each(data.courses, function(i, course) {
	
		if(course.discount.discount != null) {
				
			var newprice=0;
			var today= new Date().getTime();
			var flag=0;
			var discountenddate = course.discount.endDate;
			
			if(discountenddate > today )
			{	flag=1;
				newprice=course.studentPrice*((100 - course.discount.discount) / 100);
				newprice=newprice.toFixed(2);
				discount= Math.round(course.discount.discount);
			}
			//console.log(newprice);
		}
		
		displayCurrency = dispcurrency;
		if(course.studentPrice == '0' || course.studentPrice == '0.00') {
			displayCurrency = '';
			course.studentPrice = 'Free';
		}
		link = '';
		name = '';
		switch(parseInt(course.owner.role)) {
			case 1:
				link = sitepathMarket+'institute/';
				name = course.owner.detail.name;
				break;
			case 2:
			case 3:
				link = sitepathMarket+'instructor/';
				name = course.owner.detail.firstName + ' ' + course.owner.detail.lastName;
				break;
		}
		if(course.rating.total == 0)
			rating = 0;
		else
			rating = parseFloat(course.rating.rating).toFixed(1);
		html += '<div class="col-md-3 col-sm-6 col-xs-12">'+
					'<div class="course-cover">'+
						'<div class="course-img">'+
							'<a href="'+sitePathCourseDetail+course.slug+'"><img src="'+course.image+'" class="btn-block" alt="'+course.name+'"></a>'+
						'</div>'+
						'<div class="course-dl">'+
							'<div class="title bb-grey-3 pad10 ellipsis-2lines">'+
								'<a href="'+sitePathCourseDetail+course.slug+'" title="'+course.name+'">'+course.name+'</a>'+
							'</div>'+
							'<div class="bb-grey-3 pad10">'+
								'<div class="row">'+
									'<span class="col-xs-6">'+((flag>0)?'<strike class= "pricedlandingPage">'+course.studentPrice+ ' ' + displayCurrency+'</strike>'+' '+newprice+ ' ' +displayCurrency : course.studentPrice+ ' ' +displayCurrency)+'</span>'+
									'<span class="col-xs-6 text-right">'+
										'<input type="hidden" class="rating" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="' + rating + '" DISABLED />'+
									'</span>'+
								'</div>'+
							'</div>'+
							'<div class="user">'+
								'<span class="avatar">'+
									'<a href="' + link + course.owner.detail.userId + '"><img src="' + course.owner.detail.profilePic + '" alt=""></a>'+
								'</span>'+
								'<span class="username ellipsis-2lines">'+
									'<a href="' + link + course.owner.detail.userId + '">' + name + '</a>'+
								'</span>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>';
	});
	return html;
}
</script>