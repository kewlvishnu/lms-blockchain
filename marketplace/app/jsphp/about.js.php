<script type="text/javascript">
$(document).ready(function(){
	$("#testimonialsSlider").owlCarousel({
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		items:1,
		lazyLoad:true,
		loop:true
	});
});
</script>