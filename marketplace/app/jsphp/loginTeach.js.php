<script type="text/javascript">
var roleuser=0;
$(document).ready(function() {
	if(page == "signupComplete") {
		addResetListener(slug);
	};
	
	var notify = getUrlParameter("notify");
	$("#signIn").click(function(e) {
		e.preventDefault();
		$('.error-msg').hide();
		var user=$('#user').val();
		var pwd=$('#pwd').val();
		var remember = $('#remember').val();
		if(user.length<3)
			$('#userError').html("A valid username is required.").show();
		if(pwd.length<6)
			$('#pwdError').html("A valid password is required.").show();
		if(user.length>=3 && pwd.length>=6) {
			req = {
					'action' : 'login',
					'username' : user,
					'password' : pwd,
					'userRole' : $('#userRole').val(),
					'remember' : (($('#remember').prop("checked"))?1:0)
				};
			req=JSON.stringify(req);
			$.post(ApiEndpoint, req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.valid == false) {
					if(res.reset == 1) {
						$('#signInError').html(res.reason + '<a href="#" class="resend-link">Resend email</a>').show();
						addResetListener(res.userId);
					}
					else
						$('#signInError').html(res.reason).show();
				}
				if(res.valid == true && (res.userRole == 1 || res.userRole == 2))
				{
					if(typeof (InstitutePurchasePage) === 'undefined'){
						window.location = sitepathManage;
					}else{
						window.location = sitePathCourseDetail+slug;
					}
				}
				else if(res.valid == true)
				{
					if(typeof (InstitutePurchasePage) === 'undefined'){
						window.location = "admin/dashboard.php";
					}else{
						window.location = sitePathCourseDetail+slug;
					}
				}
			});
		}
	});
	$('#resetPassword').click(function() {
		$('.error-msg').hide();
		var fuser=$('#fuser').val();
		if(fuser.length < 6)
			$('#fuserError').html("A valid username is required.").show();
		if(fuser.length >= 6) {
			var req = {};
			req.action = 'send-reset-password-link';
			req.username = fuser;
			req.userRole = $('#fuserRole').val();
			req = JSON.stringify(req);
			$.post(ApiEndpoint, req).success(function(resp) {
				res = jQuery.parseJSON(resp);
				$('#forgetError').html(res.message).show();
			});
		}
	});
	
	function addResetListener(userId) {
		$('.resend-link').off('click');
		//resend verification link
		$('.resend-link').on('click', function() {
			var req = {};
			var res;
			req.id = userId;
			if(req.id == undefined)
				alert("Some error occurred contact admin.");
			req.action = 'resend-verification-link';
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndpoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else
					alert("Verification mail sent. Please check your mailbox.");
			});
		});
	}
	
});
</script>