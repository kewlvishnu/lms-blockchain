<script type="text/javascript">
var sitePath	= '<?php echo $sitepath; ?>';
var sitePath404 = '<?php echo $sitepath404; ?>';
var sitepathMarket	= '<?php echo $sitepathMarket; ?>';
var sitePathCourses		 = '<?php echo $sitepathCourses; ?>';
var sitePathCourseDetail = '<?php echo $sitepathCourseDetail; ?>';
var sitepathManage = '<?php echo $sitepathManage; ?>';
var sitepathLogout = '<?php echo $sitepathLogout; ?>';
var page = '<?php echo $page; ?>';
var ApiEndpoint = sitePath+'api/index.php';
var slug = '<?php echo ((isset($slug))?($slug):('')); ?>';
function signOut() {
  gapi.load('auth2', function() {
    gapi.auth2.init().then(() => {
    	var auth2 = gapi.auth2.getAuthInstance();
		auth2.signOut().then(function () {
			console.log('User signed out.');
			window.location = sitepathLogout;
		});
    })
  });
}

$(document).ready(function() {
	Checklogin();
});

function Checklogin(){
	var req = {};
	var res;
	req.action = 'check-login';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status==1){
			loginstatus = 1;
			userId = res.userId;
			userRole = res.userRole;
		}
	});
}

function getUrlParameter(sParam)
{
	sParam = sParam.toLowerCase();
	var sPageURL = window.location.search.substring(1).toLowerCase();
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) 
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) 
		{
			return sParameterName[1];
		}
	}
}
$(document).on('click','.scrolltodiv', function(event) {
	event.preventDefault();
	var target = "#" + this.getAttribute('data-target');
	$('html, body').animate({
		scrollTop: $(target).offset().top
	}, 1000);
});
var toggleDataMore = function(objLink,objData) {
	var dataMore = objLink.attr("data-more");
	if(dataMore == "full") {
		objData.css("height","auto");
		objLink.attr("data-more","less").html("Hide details");
	} else {
		if(objData.hasClass('js-full-instructor-details')) {
			objData.css("height","100px");
		} else {
			objData.css("height","100px");
		}
		objLink.attr("data-more","full").html("Full details");
	}
}
/**
* Funtion to format single digit number into double digit i.e. append one before item
*
* @author Rupesh Pandey
* @date 23/05/2015
* @param integer input number
* @return integer two digit number
*/
function format(number) {
	if(number < 10)
		return '0' + number;
	else
		return number;
}

function percentage(number, total) {
	if(total == 0)
		return 0;
	return (parseInt(number)/parseInt(total)) * 100;
}

function calcDisplayTimeWords(totalDuration) {
	var hours = parseInt( totalDuration / 3600 ) % 24;
	var minutes = parseInt( totalDuration / 60 ) % 60;
	var seconds = totalDuration % 60;

	if(hours>0) {
		if(minutes>0 && minutes<=15) {
			return hours+".25h";
		} else if(minutes>15 && minutes<=30) {
			return hours+".5h";
		} else if(minutes>30 && minutes<=45) {
			return hours+".75h";
		} else {
			return hours+"h";
		}
	} else if(minutes>0) {
		if(seconds>0 && seconds<=15) {
			return minutes+".25m";
		} else if(seconds>15 && seconds<=30) {
			return minutes+".5m";
		} else if(seconds>30 && seconds<=45) {
			return minutes+".75m";
		} else {
			return minutes+"m";
		}
	} else {
		return seconds+" s";
	}
}

function calcDisplayTime(totalSec) {
	var hours = parseInt( totalSec / 3600 ) % 24;
	var minutes = parseInt( totalSec / 60 ) % 60;
	var seconds = totalSec % 60;

	if(hours>0) {
		return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
	} else {
		return (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
	}
}
</script>