<script type="text/javascript">
$(function() {
	/*//getting pdt from paypal
	data = 'cmd=_notify-synch&tx=46B21466MG937540D&at=rlSIwjzFIRnUnBU-paOJXUzzr2_QKuhrdUB7HHhg6HYVH7I87XStv7o1Ds4';
	$.ajax({
		type	: 'POST',
		url		: 'https://www.sandbox.paypal.com/cgi-bin/webscr',
		crossDomain: true,
		data	: data
	}).done(function(res) {
		console.log(res);
	});*/

	//checking if transaction is valid or not
	var paypalId = getUrlParameter('tx');
	var item = getUrlParameter('item_number');
	var amount = getUrlParameter('amt');
	var payment_Status=getUrlParameter('st');
	
	/*var paypalId = '0TX80024ML680520D';
	var item = 235;
	var amount = 11110.10;
	var payment_Status='completed';
	console.log("hello");
	console.log(amount);*/
	if(paypalId != undefined && item != undefined && amount != undefined) {
		var req = {};
		var res;
		req.action = 'paypal-return';
		req.paypalId = paypalId;
		req.orderId = item;
		req.amount = amount;
		req.iteminfo = postVariables;
		req.payment_Status = payment_Status;
		$.ajax({
			'type'	: 'post',
			'url'	: ApiEndpoint,
			'data'	: JSON.stringify(req)
		}).done(function(res) {
			//console.log(res);
			$('#status').show();
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status == 1) {			
			$('#orderId').text(item);
				if(res.type == 1) {
					$('#admin').show();
					$('#quantity').text(res.purchase_detail);
					$('#rate').text(res.rate);
					$('#itemName').text('Enrollment Keys');
				}
				else {
					$('#itemName').text('C00' + res.purchase_detail + ' ' + res.name);
				}
				$('.total').text(res.amount);
				$('#dateTime').text(res.date);
				if(res.userRole != 4) {
					if(res.userRole == 1) {
						$('#account').attr('href', sitepathManage);
					} else {
						$('#account').attr('href', 'admin/CourseKey.php');
					}
				}
				else
					$('#account').attr('href', sitePathStudent);

			} else {
				//console.log('Transaction Failed');
				$('#dateTime').text(res.date);
				$('#status').html('<span class="pull-right custom-danger-badge">Payment Failed</span>');
				if(res.userRole != 4) {
					if(res.userRole == 1) {
						$('#account').attr('href', sitepathManage);
					} else {
						$('#account').attr('href', 'admin/CourseKey.php');
					}
				}
				else
					$('#account').attr('href', sitePathStudent);
			}
		});
	}else {
		console.log('Transaction Failed');
		$('#status').show();
		$('#dateTime').text(new Date());
		$('#status').html('<span class="pull-right custom-danger-badge">Payment Failed</span>');
		$('#account').remove();
	}
});
</script>