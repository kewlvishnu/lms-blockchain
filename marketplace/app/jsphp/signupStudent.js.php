<script type="text/javascript">
$(document).ready(function(){
	$(".form-3d .form-control").keyup(function(e){
		if(e.which!=9) {
			var disError = $(this).closest("form").find(".help-block");
			disError.html('').closest('.form-group').removeClass('has-success').removeClass('has-error');
		}
	});
	var formStudent = {
		'type' : "Student",
		'addressCountryId' : 0,
		'agree' : 0,
		'action' : 'register-short',
		'userType' : 'student',
		'userRole' : 4,
		'gender' : 0
	};
	function emailValidation(element,user) {
		var frmGroup=element.closest('.form-group');
		var disError=element.closest('form').find('.help-block');
		var frmError=disError.closest('.form-group');
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(regex.test(element.val())) {
			frmGroup.removeClass('has-error');
			var req = {};
			req.action = 'validate-email';
			req.email = element.val();
			req.role = user.userRole;
			req=JSON.stringify(req);
			$.post(ApiEndpoint, req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.valid == false) {
					frmGroup.removeClass('has-success').addClass('has-error');
					disError.html("Email address already registered please select a different one.");
					frmError.removeClass('has-success').addClass('has-error');
					//element.focus();
				}
				else {
					frmGroup.removeClass('has-error').addClass('has-success');
					disError.html("");
					frmError.removeClass('has-success').removeClass('has-error');
					return true;
				}
			});
		}
		else {
			frmGroup.removeClass('has-success').addClass('has-error');
			disError.html("Doesn't seem to be a valid email address.");
			frmError.removeClass('has-success').addClass('has-error');
			//element.focus();
		}
		return false;
	}
	$("#inputEmailAddress").blur(function(){
		emailValidation($(this),formStudent);
	});
	$("#btnStudentSignup").click(function(){
		var disError 				= $("#studentSignup").find('.help-block');
		var frmError 				= disError.closest('.form-group');
		var inputFirstName			= $("#inputFirstName");
		var inputLastName			= $("#inputLastName");
		var inputEmailAddress		= $("#inputEmailAddress");
		var inputPassword			= $("#inputPassword");
		if(inputFirstName.val().length<2 || inputFirstName.val().length>20) {
			disError.html("First Name needs to be 2-20 characters.");
			frmError.removeClass('has-success').addClass('has-error');
			inputFirstName.focus();
		} else if(inputLastName.val().length<1 || inputLastName.val().length>20) {
			disError.html("Last Name needs to be 1-20 characters.");
			frmError.removeClass('has-success').addClass('has-error');
			inputLastName.focus();
		} else if($(inputEmailAddress).closest('.form-group').hasClass('has-error') ||
					(!$(inputEmailAddress).closest('.form-group').hasClass('has-error') && !$(inputEmailAddress).closest('.form-group').hasClass('has-success'))
				) {
			emailValidation(inputEmailAddress,formStudent);
		} else if(inputPassword.val().length<6 || inputPassword.val().length>20) {
			disError.html("Password needs to be between 6-20 characters.");
			frmError.removeClass('has-success').addClass('has-error');
			inputPassword.focus();
		} else {
			formStudent.firstName 	= inputFirstName.val();
			formStudent.lastName 	= inputLastName.val();
			formStudent.email 		= inputEmailAddress.val();
			formStudent.password 	= inputPassword.val();
			formStudent 			= JSON.stringify(formStudent);
			$.post(ApiEndpoint, formStudent).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.status == 1)
					window.location=sitepathMarket+"signup/complete/" + res.userId;
				if(res.status == 0)
					alert(res.message);
			});
		}
		return false;
	});
});
</script>