<!-- Training Institute -->
				<div id="trainingInstitute">
					<h4>Training Institute</h4>
					<div class="form-group">
							<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-10">
											<select multiple="multiple" class="multi-select" id="selectTrainingInstitute" name="selectTrainingInstitute[]" data-text="trainingInstitute">
													<option value='54'>Sports and Fitness</option>
													<option value='55'>Music</option>
													<option value='56'>Dance</option>
													<option value='57'>Vocational Training</option>
													<option value='58'>Design and Multimedia</option>
													<option value='59'>Animation and Games</option>
													<option value='60'>Computer Courses</option>
													<option value='61'>Foreign Language</option>
													<option value='62'>Photography</option>
													<option value='63'>Entrepreneurship</option>
													<option value='-1'>Others</option>
											</select>
									</div>
									<div class="col-lg-6" id="trainingInstituteTypeOther">
										<label for="Institute_Name">Training Institute Type</label>
										<input type="text" placeholder="Others" id="trainingInstituteOther" class="form-control">
									</div>
							</div>
					</div>
				</div>
<!-- /Training Institute -->