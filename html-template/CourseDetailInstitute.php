<div class="container">
    <div class="row margin_top_90">
        <!--blog start-->
        <div class="col-md-9 ">
            <div class="row">
                <!--<div class="col-lg-12 col-sm-12 contact-info">
                </div>-->
                <div class="col-lg-6 col-sm-6">
                    <div   class="blog-img">
                        <img style="width: 100%;height: 220px;" id="course_img"  alt="No Image">
                    </div>
                </div>                   
                <div class="col-lg-6 col-sm-6">                            
					<h2 id="courseName" class="no-margin"></h2>
                    <h4><i class="fa fa-list-alt"></i> Course Subtitle</h4>
                    <p id="subtitle" ></p>
					<hr class="no-margin">
					<h4><i class="fa fa-folder"></i> Course Category</h4>
					<p id="courseCategory" ></p>
					<hr class="no-margin">
                    <h4 class="margin_top_15"><i class="fa fa-users"></i> Targeted Audience</h4>
                    <p id="target-audience"></p>
					<hr class="no-margin">
					<h4><i class="fa fa-info-circle"></i> Course Description</h4>
                    <p id ='description'></p>
                </div>
            </div>
            <br/>
            <div id="course-append" class="row">            
                <div class="col-lg-12 about">
					<strong>Subjects</strong>
                </div>
            </div>
       </div>
        <div class="col-md-3 right-side">
			<h4 for="courseid">License Type</h4>
            <div class="profile-nav course-info">
                <ul class="nav nav-pills nav-stacked" id="licenseType">
                </ul>
            </div>
			<a data-toggle="modal" href="#login-modal" id="takeCourse" class="btn btn-success btn-block btn-custom">Purchase</a>
			<!--<div class="profile-nav course-info">
                <ul class="nav nav-pills nav-stacked">
                    <li><a><i class="fa fa-book text-danger"></i>Course Id <span class="label label-primary pull-right r-activity" id="course-id"></span></a></li>
                </ul>
            </div>
			<div class="profile-nav course-info">
                <ul class="nav nav-pills nav-stacked">
                    <li><a><i class="fa fa-calendar text-danger"></i>Live Date <span id="live-date" class="label btn-success pull-right r-activity"></span></a></li>
                    <li><a><i class="fa fa-calendar text-danger"></i>End Date <span id="end-date" class="label btn-danger pull-right r-activity"></span></a></li>
                </ul>
            </div>-->
            <div class="testimonial margin-t-p">
                <h4>Taught by</h4>
				<img id="institute-img" src="" alt="Institute"  class="img-responsive">
                <h4 id="institute-name" style="text-align: justify;background: #fff;padding: 5px;"></h4>
            </div>
            <div>
				<span id="tagline"></span>
                <h5 >Institute Background</h5>
                <p id="institute-background" style="text-align:justify;" class="no-margin"></p>
            </div>
            <br>
        </div>
    </div>
    <br/><br/><br/></div>