
<div class="modal fade" id="playerModal" tabindex="-1" role="dialog" aria-labelledby="playerModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content bg-clr border-clr box-clr">
      <div class="modal-header border-clr">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="dismissVideo"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Preview Video</h4>
      </div>
      <div class="modal-body pad-clr">
      	<video id="videoPlayer"></video>
		<!-- <video width="640" height="360" id="playerPopup" poster="media/echo-hereweare.jpg" controls="controls" preload="none" style="width: 100%; height: 100%;">
			MP4 source must come first for iOS
			<source type="video/mp4" src="media/echo-hereweare.mp4" />
			WebM for Firefox 4 and Opera
			<source type="video/webm" src="media/echo-hereweare.webm" />
			OGG for Firefox 3
			<source type="video/ogg" src="media/echo-hereweare.ogv" />
			Fallback flash player for no-HTML5 browsers with JavaScript turned off
			<object width="640" height="360" type="application/x-shockwave-flash" data="flashmediaelement.swf" style="width: 100%; height: 100%;"> 		
				<param name="movie" value="flashmediaelement.swf" /> 
				<param name="flashvars" value="controls=true&poster=media/echo-hereweare.jpg&file=media/echo-hereweare.mp4" /> 		
				Image fall back for non-HTML5 browser with JavaScript turned off and no Flash player installed
				<img src="media/echo-hereweare.jpg" width="640" height="360" alt="Here we are" 
					title="No video playback capabilities" />
			</object> 	
		</video> -->
      </div>
    </div>
  </div>
</div>