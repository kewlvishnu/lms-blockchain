<!-- School -->
					<div id="school">
						<div class="form-group">
								<h4>School</h4>
								<div class="row">
										<div class="col-lg-6">
												<label for="Institute_Name">Board</label>
												<select class="form-control m-bot15" id="selectBoard">
														<option value="0">Please select a board...</option>
														<option value="5">CBSE</option>
														<option value="6">ICSE</option>
														<option value="7">State</option>
														<option value="-1">Other</option>
												</select>
										</div>
										<div class="col-lg-6" id="schoolBoardState">
											<label for="Institute_Name">State Name</label>
											<input type="text" placeholder="State" id="boardState" class="form-control">
										</div>
										<div class="col-lg-6" id="schoolBoardOther">
											<label for="Institute_Name">Board Name</label>
											<input type="text" placeholder="Others" id="boardOther" class="form-control">
										</div>
								</div>
						</div>
						<div class="form-group">
								<div class="row">
										<div class="col-lg-6">
												<label for="Institute_Name">Classes Upto</label>
												<select class="form-control m-bot15" id="selectClassesUpto">
														<option value='0'>Please Select Classes</option>
														<option value='5'>5</option>
														<option value='8'>8</option>
														<option value='10'>10</option>
														<option value='12'>12</option>
												</select>
										</div>
								</div>
						</div>
					</div>
				<!-- /School -->
