<!-- Coaching -->
<div id="coaching">
	<h4>Coaching</h4>
	<div class="form-group" id="coachingType">
	<div>
	<label class="control-label">
					<input type="checkbox" id="management" data-text="management" value="8"/>
					Management Exams
			</label>
			</div>
			<div class="row" id="managementOptions">
					<div class="col-md-2"></div>
					<div class="col-md-10">
							<select multiple="multiple" id="selectManagement" class="multi-select" data-text="management">
								<option value='13'>CAT</option>
								<option value='14'>MAT</option>
								<option value='15'>CMAT</option>
								<option value='16'>CET</option>
								<option value='17'>GMAT</option>
								<option value='-1'>Others</option>
							</select>
					</div>
					<div class="col-lg-6" id="managementTypeOther">
						<label for="Institute_Name">Coaching Type</label>
						<input type="text" placeholder="Others" id="managementOther" class="form-control">
					</div>
			</div>
			<div>
			<label class="control-label">
					<input type="checkbox" id="engineering" data-text="engineering" value="9"/>
					Engineering Entrance Exams
			</label>
			</div>
			<div class="row" id="engineeringOptions">
					<div class="col-md-2"></div>
					<div class="col-md-10">
							<select multiple="multiple" id="selectEngineering" class="multi-select" data-text="engineering">
								<option value='18'>JEE Mains</option>
								<option value='19'>JEE Advance</option>
								<option value='21'>GATE</option>
								<option value='22'>SAT</option>
								<option value='-1'>Others</option>
							</select>
					</div>
					<div class="col-lg-6" id="engineeringTypeOther">
						<label for="Institute_Name">Coaching Type</label>
						<input type="text" placeholder="Others" id="engineeringOther" class="form-control">
					</div>
			</div>
			<div>
			<label class="control-label">
					<input type="checkbox" id="medical" data-text="medical" value="10"/>
					Medical Entrance Exams
			</label>
			</div>
			<div class="row" id="medicalOptions">
					<div class="col-md-2"></div>
					<div class="col-md-10">
							<select multiple="multiple" id="selectMedical" class="multi-select" data-text="medical">
								<option value='23'>AIIMS</option>
								<option value='24'>PMT</option>
								<option value='25'>CPMT</option>
								<option value='26'>PG Medical</option>
								<option value='27'>AIPG Dental</option>
								<option value='-1'>Others</option>
							</select>
					</div>
					<div class="col-lg-6" id="medicalTypeOther">
						<label for="Institute_Name">Coaching Type</label>
						<input type="text" placeholder="Others" id="medicalOther" class="form-control">
					</div>
			</div>
			<div>
			<label class="control-label">
					<input type="checkbox" id="schoolLevel" data-text="schoolLevel" value="11"/>
					School Level
			</label>
			</div>
			<div class="row" id="schoolLevelOptions">
					<div class="col-md-2"></div>
					<div class="col-md-10">
							<select multiple="multiple" id="selectSchoolLevel" class="multi-select" data-text="schoolLevel">
								<option value='28'>Classes 1-5</option>
								<option value='29'>Classes 6-8</option>
								<option value='30'>Classes 9-10</option>
								<option value='31'>Pre-IIT</option>
								<option value='32'>Olympiad</option>
								<option value='33'>NTSE</option>
								<option value='34'>MTSE</option>
								<option value='-1'>Others</option>
							</select>
					</div>
					<div class="col-lg-6" id="schoolLevelTypeOther">
						<label for="Institute_Name">Coaching Type</label>
						<input type="text" placeholder="Others" id="schoolLevelOther" class="form-control">
					</div>
			</div>
			<div>
			<label class="control-label">
					<input type="checkbox" id="misc" data-text="misc" value="12"/>
					Miscellaneous
			</label>
			</div>
			<div class="row" id="miscOptions">
					<div class="col-md-2"></div>
					<div class="col-md-10">
							<select multiple="multiple" id="selectMisc" class="multi-select"  data-text="misc">
								<option value='35'>Classes XI-XII</option>
								<option value='36'>CA-CS-ICWA</option>
								<option value='37'>Bank PO</option>
								<option value='38'>Railway Entrance</option>
								<option value='39'>SSC</option>
								<option value='40'>CSAT</option>
								<option value='41'>Accounts</option>
								<option value='42'>CLAT</option>
								<option value='43'>MPSE</option>
								<option value='44'>UPSE</option>
								<option value='45'>IAS</option>
								<option value='-1'>Others</option>
							</select>
					</div>
					<div class="col-lg-6" id="miscTypeOther">
						<label for="Institute_Name">Coaching Type</label>
						<input type="text" placeholder="Others" id="miscOther" class="form-control">
					</div>
			</div>
	</div>
</div>
<!-- /Coaching -->