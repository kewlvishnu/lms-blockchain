<section class="course-section margin_top_60_f">
	<article class="package-details">
		<div class="container invisible page-load-hidden">
			<div class="row">
				<div class="col-md-8">
					<div class="course-header">
						<h1 class="course-title" id="packName"></h1>
						<h2 class="course-subtitle" id="subtitle"></h2>
						<div class="course-meta">
							<span class="author-by">by</span><span class="author"><a href="javascript:void(0)" class="author-link js-institute-name"></a></span>
							<a href="#" class="reviews-link scrolltodiv" data-target="reviews"><input type="hidden" class="rating ratingStars" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="5" disabled="">
							<span class="rating-count ratingTotal"></span>10 <span class="js-enrolled"></span> students enrolled</a>
						</div>
					</div>
					<div class="course-showcase">
      					<!-- <video id="videoShowcasePlayer"></video> -->
						<!-- <video width="640" height="360" id="player2" poster="media/echo-hereweare.jpg" controls="controls" preload="none" style="width: 100%; height: 100%;">
							MP4 source must come first for iOS
							<source type="video/mp4" src="media/echo-hereweare.mp4" />
							WebM for Firefox 4 and Opera
							<source type="video/webm" src="media/echo-hereweare.webm" />
							OGG for Firefox 3
							<source type="video/ogg" src="media/echo-hereweare.ogv" />
							Fallback flash player for no-HTML5 browsers with JavaScript turned off
							<object width="640" height="360" type="application/x-shockwave-flash" data="flashmediaelement.swf" style="width: 100%; height: 100%;"> 		
								<param name="movie" value="flashmediaelement.swf" /> 
								<param name="flashvars" value="controls=true&poster=media/echo-hereweare.jpg&file=media/echo-hereweare.mp4" /> 		
								Image fall back for non-HTML5 browser with JavaScript turned off and no Flash player installed
								<img src="media/echo-hereweare.jpg" width="640" height="360" alt="Here we are" 
									title="No video playback capabilities" />
							</object> 	
						</video> -->
					</div>
				</div>
				<div class="col-md-4">
					<div class="buy-course"> 
						<div class="buy-course-content">
							<div class="course-price" id="packagePrice">
								<!-- <span class="price-discount"><i class="fa fa-rupee"></i>300</span>
								<span class="price-now"><i class="fa fa-rupee"></i>300</span> -->
							</div>
							<div class="buy-course-btn-container">
								<a data-toggle="modal" id="takepackage" class="btn btn-lg btn-ssuccess btn-block">Buy Package</a>
							</div>
							<div class="buy-course-terms-links">
								<ul class="list-inline">
									<li><a href="<?php echo $sitePath; ?>policies/terms" target="_blank">Terms Of Use</a></li><li><a data-toggle="modal" href="#refundModal">Refund Policy</a></li>
								</ul>
							</div>

<?php /*	<div id= "couponswid" class="buy-course-content">
						 <div class="col-md-9">

               				 <input name="coupon" value=""  id="couponcode" class="form-control couponCodeC" placeholder="Redeem a Coupon">			 <span   class="help-block label label-danger pull-right"></span>

           				 </div>
            			<div class="col-md-3">
                			<button type="button" id="couponapply" class="btn btn-info btn-sm">Apply</button>
           				 </div>
					
							</div>
*/
?>
							
							<div>
							
							</div>

					
					</div>
					</div>

					<div class="course-meta-data">
						<ul class="list-inline course-meta-content">
							<li class="list-item-short">
								<label for=""><i class="fa fa-book"></i> Package Name :</label>
								<span class="value" id="PackageId"></span>
							</li>
							<li class="list-item-short">
								<label for=""><i class="fa fa-users"></i> Enrolled :</label>
								<span class="value js-enrolled"></span>
							</li>
							<li class="list-item-short" id="last">
								<label for=""><i class="fa fa-clock-o"></i>Total Courses :  </label>
								<span class="value" id="noOfCourses"></span>
							</li>
						
							
							<li class="list-item-long" id="msgValidity">
								<div class="note"></div>
							</li>
							
						</ul>
					</div>
					<div class="social-share">
						<h5 class="social-title">Share</h5>
						<ul class="list-inline">
							<li class="social-item">
								<a href="javascript:void(0)" target="_blank" class="js-share-facebook"><i class="fa fa-facebook"></i></a>
							</li>
							<li class="social-item">
								<a href="javascript:void(0)" target="_blank" class="js-share-twitter"><i class="fa fa-twitter"></i></a>
							</li>
							<li class="social-item">
								<a href="javascript:void(0)" target="_blank" class="js-share-google"><i class="fa fa-google-plus"></i></a>
							</li>
						</ul>
					</div>
					<div class="other-courses">
						
					</div>	   
						  
				</div>
				<div class="col-md-8">
					<div class="course-description">
						<h3 class="desc-title">Package Description</h3>
						<div class="js-full-course-details desc-content" id="description">
						</div>
						<a href="javascript:void(0)" class="get-details js-course-details hide" data-more="full">Full details</a>
						<div class="desc-block">
							<p><strong></strong></p>
							<ul id="targetAudience">
								<!-- <li>Entrepreneurs</li>
								<li>Business Owners</li>
								<li>Project Managers</li>
								<li>Team Leaders</li>
								<li>Anyone who wants to Manage Projects or tasks and Improve productivity</li> -->
							</ul>
						</div>
					</div>
					<div style="overflow: auto;">
				    <table class="display table table-bordered table-striped" id="pacSubstable">
				        <thead>
					            <tr>
							 <th>Courses In the package </th>
						
				                
				                                      
				            </tr>
				        </thead>
				        <tbody id="div-student-courseert" class="row" >
				        </tbody>
				    </table>
					</div>
				</div>
				
			</div>
		</div>
	</article>
<!--	<article id="reviews">
		<div class="container invisible page-load-hidden">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div id="reviews" class="rate-reviews">
						<h2 class="block-title">Reviews</h2>
						<div class="row">
							<div class="col-sm-3">
								<span class="subblock-title">Average Ratings</span>
								<div class="avg-rate">5.0</div>
								<div class="rating" style="font-size: 1.5em;">
									<input type="hidden" class="rating ratingStars" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="5" disabled="">
									<span class="rating-count ratingTotal">3 Ratings</span>
								</div>
							</div>
							<div class="col-sm-4">
								<span class="subblock-title">Details</span>
								<ul class="list-unstyled list-rates">
									<li>
										<span class="item-title">5 Stars</span>
										<div class="progress">
											<div class="progress-bar progress5" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
												<span class="sr-only">60% Complete</span>
											</div>
										</div>&nbsp;<span class="count5">3</span>
									</li>
									<li>
										<span class="item-title">4 Stars</span>
										<div class="progress">
											<div class="progress-bar  progress4" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
												<span class="sr-only">50% Complete</span>
											</div>
										</div>&nbsp;<span class="count4">0</span>
									</li>
									<li>
										<span class="item-title">3 Stars</span>
										<div class="progress">
											<div class="progress-bar progress3" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
												<span class="sr-only">40% Complete</span>
											</div>
										</div>&nbsp;<span class="count3">0</span>
									</li>
									<li>
										<span class="item-title">2 Stars</span>
										<div class="progress">
											<div class="progress-bar progress2" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
												<span class="sr-only">30% Complete</span>
											</div>
										</div>&nbsp;<span class="count2">0</span>
									</li>
									<li>
										<span class="item-title">1 Stars</span>
										<div class="progress">
											<div class="progress-bar progress1" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
												<span class="sr-only">10% Complete</span>
											</div>
										</div>&nbsp;<span class="count1">0</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div id="comments" class="comments">
						<ul class="list-unstyled list-comments"></ul>
					</div>
				</div>
			</div>
		</div>
	</article> -->
</section>
