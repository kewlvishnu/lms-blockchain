<div class="col-lg-12">
    <h2>Registration</h2>
</div>
<div class="col-lg-9 col-sm-9 address">
    <div class="row" ng-show="formStep == 1 && regComplete == false">
        <div class="col-md-2 col-sm-3">
            <label class="control-label color_blue margin_top_10" for="inputSuccess">REGISTER AS</label>
        </div>
        <div class="col-sm-2">
            <div class="radio">
                <label>
                    <a href="./signup-student.php#student"><input type="radio" name="optionsRadios" id="Radio2" value="student">
                        STUDENT</a>
                </label>
            </div>
        </div>
        <div class="col-md-3">
            <div class="radio">
                <label>
                    <a href="./signup-professor.php#professor"><input type="radio" name="optionsRadios" id="Radio3" value="professor" >
                        PROFESSOR/TUTOR</a>
                </label>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="radio">
                <label ui-sref-active="active">
                    <a href="./signup-institute.php#institute"><input type="radio" name="optionsRadios" id="Radio1" value="institute">
                        INSTITUTE</a>
                </label>
            </div>
        </div>
        <!--<div class="col-md-2">
            <div class="radio">
                <label>
                    <a href="./signup-publisher.php#publisher"><input type="radio" name="optionsRadios" id="Radio4" value="publisher" >
                        PUBLISHER</a>
                </label>
            </div>
        </div>-->
        <div class="col-sm-2">
        </div>
    </div>