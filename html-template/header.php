<?php
$libPath = "api/Vendor";
include $libPath . '/Zend/Loader/AutoloaderFactory.php';
require $libPath . "/ArcaneMind/Api.php";
Zend\Loader\AutoloaderFactory::factory(array(
        'Zend\Loader\StandardAutoloader' => array(
                'autoregister_zf' => true,
                'db' => 'Zend\Db\Sql'
        )
));
if(isset($_COOKIE["mtwebLogin"]) && !empty($_COOKIE["mtwebLogin"])) {
	require_once 'api/Vendor/ArcaneMind/User.php';
	$cookie = $_COOKIE["mtwebLogin"];
	$res = Api::checkUserRemembered($cookie);
}
$title = 'ArcaneMind | Online Courses | Entrance Exam Preparation';
$fbTitle = 'Online Courses & Entrance Exam Preparation';
$description = 'Arcanemind is a platform & marketplace for online courses & entrance exam preparation. We have the best instructors and institutes in the world, we ensure that the content and questions are of the highest quality.';
$image = 'http://dhzkq6drovqw5.cloudfront.net/fb-arcanemind.jpg';

//getting meta data for seo and smo
if(isset($_GET['courseId']) && !empty($_GET['courseId'])){
	require_once 'api/Vendor/ArcaneMind/Course.php';
	$c = new Course();
	$input = new stdClass();
	$input->courseId = $_GET['courseId'];
	$result = $c->getCourseForMeta($input);
	if($result->status == 1) {
		$title = $result->details['name'] . ' | ' . $result->details['subtitle'];
		$fbTitle = $title;
		$description = $result->details['description'];
		$image = $result->details['ogimage'];
	}
	else {
		$title = 'Arcanemind | Online Courses | Entrance Exam Preparation';
		$fbTitle = 'ArcaneMind | Online Courses | Entrance Exam Preparation';
		$description = 'Arcanemind.com is a Cloud Based Learning platform & Market place for Courses, where Instructors,Tutors,Trainers,Coaching Institutes, Schools and Colleges host high quality education content in the form of Videos & Documents and conduct online tests with detailed analytics';
		$image = 'http://dhzkq6drovqw5.cloudfront.net/fb-arcanemind.jpg';
	}
}

if(isset($_GET["pageDetailId"]) && !empty($_GET["pageDetailId"])) {
    require_once 'api/Vendor/ArcaneMind/User.php';
    
    $actual_link  = "http://$_SERVER[HTTP_HOST]";
    $temp_link    = explode("/", $actual_link);
    $sub_link = explode(".", $temp_link[2]);
    $site_link = str_replace($sub_link[0], 'www', $actual_link);
    $portfolio_link = $site_link."/portfolio/";
    $redirect_link = $site_link.$_SERVER["REQUEST_URI"];

    $portfolioName = "";

    $slug = trim($_GET['pageDetailId']);
    $actual_slug = $slug;
    $res = Api::getUserBySlug($slug);
    if($res->valid) {
        $logoPath = $res->profileDetails['profilePic'];
        if ($res->userRole == 1) {
            $portfolioName = $res->profileDetails['name'];
        } else {
            $portfolioName = $res->profileDetails['firstName'].' '.$res->profileDetails['lastName'];
        }
        if (!empty($res->profileDetails['description'])) {
            $description = $res->profileDetails['description'];
        } else {
            $description = '--------- No Description specified ---------';
        }
    } else {
        header('location:'.$redirect_link);
    }
}
if(isset($logoPath) && !empty($logoPath)) {
	$logoBlock = '<h1 class="logo-profile">
                        	<img src="'.$logoPath.'" alt="'.$portfolioName.'" class="img-logo" />
                        	<span>'.$portfolioName.'</span>
                        </h1>';
} else {
	$logoBlock = '<img src="img/arcanemind-logo.png" alt="Arcanemind" class="img-logo" />';
}
require_once 'api/Vendor/ArcaneMind/Base.php';
$b = new Base();
?>
<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  	<meta charset="utf-8" />
  	<!-- <meta http-equiv="cache-control" content="max-age=0" />
  		<meta http-equiv="cache-control" content="no-cache" />
  		<meta http-equiv="expires" content="0" />
  		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
  		<meta http-equiv="pragma" content="no-cache" /> -->
  	<meta http-equiv="expires" content="Wed, 17 Jun 2015 07:30:00 GMT" />
    <meta name="description" content="<?php echo $description; ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="arcanemind, online courses, courses anytime anywhere, free courses, study online, best learning platform,online test platform, multiple choice questions, education market place, education content, exam preparation, education videos, udemy, edx, coursera, blackboard, instructure, canvas">
		<title><?php echo $title; ?></title>
    <link rel="icon" href="img/favicon.ico" type="image/x-icon" >

    <!-- facebook graph share properties -->
    <meta property="og:title" content="<?php echo $fbTitle; ?>">
	<meta property="og:url" content="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
	<meta property="og:site_name" content="Arcanemind - Study online Courses"/>
	<meta property="og:description" content="<?php echo $description; ?>">
	<meta property="og:image" content="<?php echo $image; ?>">

    <!-- Place bootstrap theme and reset css in bower_components manually -->
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!-- build:css styles/vendor.css -->
    <!-- bower:css -->
	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css" />
	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/theme.css" />
	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.css" />
	<link rel="stylesheet" href="assets/jquery-multi-select/css/multi-select.css" />
	<link rel="stylesheet" href="admin/assets/bootstrap-datepicker/css/datepicker.css" />
		
	<!-- endbower -->
    <!-- endbuild -->
    
	<!-- build:css({.tmp,app}) styles/main.css -->
	<link rel="stylesheet" href="styles/style-responsive.css"/>
	<link rel="stylesheet" href="styles/custom.css"/>
	
	<!--sunny codes start-->
	<link rel="stylesheet" href="css/flexslider.css"/>
	<link href="assets/bxslider/jquery.bxslider.css" rel="stylesheet" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" href="assets/revolution_slider/css/rs-style.css" media="screen">
	<link rel="stylesheet" href="assets/revolution_slider/rs-plugin/css/settings.css" media="screen">
	<link rel="stylesheet" href="assets/addresspicker/demos/themes/base/jquery.ui.all.css">			
	<link rel="stylesheet" href="styles/style.css" />
	<link rel="stylesheet" href="styles/main.css" />
	<link rel="stylesheet" href="styles/responsive.css" />
	<!--sunny codes end-->
	<link rel="stylesheet" href="assets/addresspicker/demos/themes/base/jquery.ui.all.css">
	
	<link rel="stylesheet" href="css/owl.carousel.css">
	<!-- Default Theme -->
	<link rel="stylesheet" href="css/owl.theme.default.css">
			
<!-- Bootstrap core CSS -->
    <!-- <link href="css/style/bootstrap.min.css" rel="stylesheet" />
    <link href="css/style/bootstrap-reset.css" rel="stylesheet" /> -->
	<!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="admin/assets/bootstrap-rating/bootstrap-rating.css">
	<link rel="stylesheet" href="css/mediaelementplayer.min.css" />
			
			
			<!-- Respond.js proxy on external server -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js" id="ee" rel="e" />
			
			  <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
		<!--[if lt IE 9]
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>[endif]
        -->
			
			
    <!-- endbuild -->
    <!-- analytics code -->
    <?php @include_once "analytics.php"; ?>
  </head>
  <body>
    <!--[if lt IE 7]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
	
		<!--header start-->
		
    <header class="header-frontend">
	<?php
	include 'modal/login.php';
	include 'modal/forgotpassword.php';
	//include 'modal/map-view.php';
	?>
	<div id="loader" style="background: rgba(0, 0, 0, 0.31);
width: 100%;
height: 100%;
position: fixed;
top: 0;
left: 0;
z-index: 10000;
display: none;">
	<img src="img/loader.gif" style="position: fixed;
top: 50%;
left: 50%;
border: 5px solid white;
border-radius: 3px;">
	</div>
	<div class="navbar navbar-default navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="fa fa-bars"></span>
				</button>
				<a class="navbar-brand" href=".">
					<?php echo $logoBlock ?>
				</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li>
						<div class="dropdown ">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories<span class="caret"></span></button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
								<li><a href="<?php echo $b->sitePath; ?>courses/entrance-exams/">Entrance Exams</a></li>
								<li><a href="<?php echo $b->sitePath; ?>courses/school-college-prep/">School &amp; College Prep</a></li>
								<li><a href="<?php echo $b->sitePath; ?>courses/hobbies-skills/">Hobbies &amp; Skills</a></li>
								<li><a href="<?php echo $b->sitePath; ?>courses/business-management/">Business &amp; Management</a></li>
								<li><a href="<?php echo $b->sitePath; ?>courses/it-software/">IT &amp; Software</a></li>
								<li><a href="<?php echo $b->sitePath; ?>courses/entrepreneurship/">Entrepreneurship</a></li>
								<li><a href="<?php echo $b->sitePath; ?>courses/miscellaneous/">Miscellaneous</a></li>
								<li role="separator" class="divider mrg-clr"></li>
								<li><a href="<?php echo $b->sitePath; ?>courses">All Courses</a></li>
							</ul>
						</div>
					</li>
					<li>
						<form class="navbar-form mrg-clr navbar-left" role="form" action="search.php" method="get" id="searchForm">
							<div class="form-group">
								<input type="tsext" class="form-control" placeholder="Search Courses" id="searchBox" name="text">
								<div id="searchDiv"></div>
							</div>
						</form>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<?php
						@session_start();
						if(!(isset($_SESSION['userId']) && !empty($_SESSION['userId']))) {
							?>
							<li><a href="signup-student.php#student">Register</a></li>
							<?php
						}
					?>
					<!--<li><a href="courseDetailLicense.php" style="background: #F79F5B;">For Institute</a></li>
					<li><a href="Faq.php">FAQ</a></li>-->
					<?php
						@session_start();
						if(isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
							if($_SESSION['userRole'] == 4) {
								?>
								<li><a href="student/">Dashboard</a></li>
								<li><a href="logout.php">Logout</a></li>
								<?php
							}
							else if($_SESSION['userRole'] == 1){
								?>
								<li><a href="admin/dashboard.php">Dashboard</a></li>
								<li><a href="logout.php">Logout</a></li>
								<?php
							}
							else if($_SESSION['userRole'] == 2){
								?>
								<li><a href="admin/dashboard.php">Dashboard</a></li>
								<li><a href="logout.php">Logout</a></li>
								<?php
							}
							else if($_SESSION['userRole'] == 3){
								?>
								<li><a href="admin/dashboard.php">Dashboard</a></li>
								<li><a href="logout.php">Logout</a></li>
								<?php
							}
							else if($_SESSION['userRole'] == 5){
								?>
								<li><a href="admin/approvals.php">Dashboard</a></li>
								<li><a href="logout.php">Logout</a></li>
								<?php
							}
						}
						else {
							?>
							<li><a href="#login-modal" data-toggle="modal">Login</a></li>
							<?php
						}
					?>
				</ul>
			</div>
		</div>
	</div>
</header>
<!--header end-->