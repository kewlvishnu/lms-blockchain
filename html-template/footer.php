    <!--[footer start]-->
	<footer class="midnight-blue" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 mbot10">
                    &copy; 2015 <a target="_blank" href="http://www.arcanemind.com/" title="Arcanemind">Arcanemind</a>. All Rights Reserved.
               
                </div>
                <div class="col-sm-8 mbot10">
                    <ul class="pull-right">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="about.php">About Us</a></li>
                        <!--<li><a href="Faq.php">FAQ</a></li>-->
                        <li><a href="Contact-Us.php">Contact Us</a></li>
						<li><a href="TermsofUse.php">Terms of Use</a></li>
						<li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
						<li><a href="signup-student.php#student">Become a Member</a></li>
						<?php
							@session_start();
							if(!isset($_SESSION['userId'])) {
						?>
								<li><a href="#login-modal" data-toggle="modal">Login</a></li>
						<?php
							}
						?>
                    </ul>
                </div>
            </div>
        </div>
		<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-550dc80a0fe25b98" async="async"></script>
    </footer>  
<!--[footer End]-->	
    <script src="assets/es5-shim/es5-shim.js"></script>
    <script src="assets/json3/lib/json3.min.js"></script>                                       
    <!-- build:js scripts/vendor.js -->                                                             
    <!-- bower:js -->                                                                               
	<!-- endbower -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>     
  <script src="http://code.jquery.com/jquery-latest.js"></script>
	<script src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script> 
  	<!--<script src="assets/jquery/dist/jquery.js"></script>-->
	<script src="assets/bootstrap/dist/js/bootstrap.js"></script>
	<!-- endbuild -->                                                                               

		<!--common script for all pages-->
		<!-- <script src="vendor-scripts/common-scripts.js"></script> -->
		<script src="vendor-scripts/jquery.easing.min.js"></script>

		
		
		<!--sunny codes-->
		<script src="assets/jquery-multi-select/js/jquery.multi-select.js"></script>
		<script src="admin/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="scripts/login.js"></script>
		
		
		
		<script type="text/javascript" src="assets/bxslider/jquery.bxslider.js"></script>


		
		<!--<script src="js/jquery.js"></script>-->
		<!--<script src="js/jquery-1.8.3.min.js"></script>-->
		<script src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/hover-dropdown.js"></script>
		<script defer src="js/jquery.flexslider.js"></script>

		<script type="text/javascript" src="js/jquery.parallax-1.1.3.js"></script>

		<script src="js/jquery.easing.min.js"></script>
		<script src="js/link-hover.js"></script>

	<?php /*<script type="text/javascript" src="assets/fancybox/source/jquery.fancybox.pack.js"></script>*/?>
		
    <script src="admin/assets/bootstrap-rating/bootstrap-rating.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="assets/revolution_slider/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="assets/revolution_slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!--common script for all pages-->
		<script src="js/revulation-slide.js"></script>
    <script src="js/common-scripts.js"></script>
    <script src="js/mediaelement-and-player.min.js"></script>
    <script src="scripts/script.js"></script>
  <script>

      RevSlide.initRevolutionSlider();

      $(window).load(function() {
          $('[data-zlname = reverse-effect]').mateHover({
              position: 'y-reverse',
              overlayStyle: 'rolling',
              overlayBg: '#fff',
              overlayOpacity: 0.7,
              overlayEasing: 'easeOutCirc',
              rollingPosition: 'top',
              popupEasing: 'easeOutBack',
              popup2Easing: 'easeOutBack'
          });
      });

      $(window).load(function() {
          $('.flexslider').flexslider({
              animation: "slide",
              start: function(slider) {
                  $('body').removeClass('loading');
              }
          });
      });

      //    fancybox
	 
      //jQuery(".fancybox").fancybox();



  </script>

