<div class="contact-form form_bg">
	<h3>Reset your Password</h3>
	<div class="form-group">
		<input type="password" class="form-control" id="password1" placeholder="Password">
	</div>
	<div class="form-group">
		<input type="password" class="form-control" id="password2" placeholder="Re-enter Password">
	</div>
	<div class="pull-left">
		<span id="error" style="color: red;"></span><br/>
		<button class="btn btn-success" id="changePwd">Change Password</button>
	</div>
	<br />
</div>