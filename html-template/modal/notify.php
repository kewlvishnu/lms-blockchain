<div id="notify-modal" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
	<h3 class="modal-title">Verifing User!</h3>
</div>
<div class="modal-body">
	<div class="clearfix" id="notify-section">
		<h3>Please wait while we are trying to verify you!</h3>
	</div>
</div>
</div>
</div>
</div>