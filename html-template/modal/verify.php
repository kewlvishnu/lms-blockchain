<div id="verify-modal" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
	<h3 class="modal-title"></h3>
</div>
<div class="modal-body">
	<div class="login-form clearfix">
		<h3 id="verifyError" style="text-transform: none;">Please wait while we are trying to verify you!</h3>
	</div>
</div>
</div>
</div>
</div>