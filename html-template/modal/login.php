<div class="modal fade" id="login-modal">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
	<button data-dismiss="modal" class="close" type="button">x</button>
	<h3 class="modal-title">Please Login and start learning!</h3>
</div>
<div class="modal-body">
	<div class="login-form clearfix">
		<form role="form">
			<div class="form-group">
				<select id="userRole" class="form-control">
					<option value="4">I am a student</option>
					<option value="2">I am a professor/tutor</option>
					<option value="1">I represent an institute</option>
					<!--<option value="3">I am a publisher</option>-->
				</select>
			</div>
			<div class="form-group">
				<input type="text" placeholder="Enter username or email" id="user" class="form-control">
				<div id="userError" class="error-msg"></div>
			</div>
			<div class="form-group">
				<input type="password" placeholder="Password" id="pwd" class="form-control">
				<div id="pwdError" class="error-msg"></div>
			</div>
			<div class="checkbox pull-right">
				<label>
					<input type="checkbox" id="remember"> Remember me on this computer
				</label>
			</div>
			<div class="pull-left">
				<div id="signInError" class="error-msg"></div>
				<button id="signIn" class="btn btn-primary" type="submit">Log In
			</button></div>
		</form>
	</div>
</div>
<div class="modal-footer">
	<div class="pull-left">
		<a data-dismiss="modal" data-toggle="modal" href="#forget-password-modal">Forgot Password?</a>
	</div>
	<div class="pull-right">
		<span>Don't have an account?</span>
		<a style="margin-top: -7px;" class="btn btn-danger" href="signup-student.php#student">Register</a>
	</div>
</div>
</div>
</div>
</div>
	