<!--<div id="map-view" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Please select your location.</h3>
			</div>
			<div class="modal-body">--><div id="mapContainer">
			<div id="mapview">
				<div class="login-form clearfix">
					<div class="row">
						<div class="col-sm-12">
							<input class="form-control" id="address" type="text" style="display: none;">
						</div>
					</div>
					<div class='map-wrapper'>
						<center><div id="map" style="height: 400px;width: 450px;"></div>
						<div id="legend">You can drag and drop the marker to the correct location</div></center>
					</div>
					<div class="row form-group">
						<div class="col-sm-4">
							<label for="street">Street</label>
							<input type="text" placeholder="Street" id="street-tmp" class="form-control">
						</div>
						<div class="col-sm-4">
							<label for="city">City</label>
							<input type="text" placeholder="City" id="city-tmp" class="form-control">
						</div>
						<div class="col-sm-4">
							<label for="pin">Pin</label>
							<input type="text" placeholder="Pin" id="pin-tmp" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							<label for="state">State</label>
							<input type="text" placeholder="State" id="state-tmp" class="form-control">
						</div>
						<label for="submit">&nbsp;</label>
						<div  class="col-sm-6 pull-right">
							<button type="button" id="mapOK" class="btn btn-primary" data-dismiss="modal">OK</button>
							<button type="button" class="btn btn-primary" data-dismiss="modal" id="mapCancel">Cancel</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	<!--</div>
</div>-->