<div class="container featured-bg margin_top_25"> 
    <!--feature start--><!--
    <div class="row">            
        <div class="text-center feature-head">
            <h1>Welcome to Arcanemind </h1>
            <p>Discover popular topics and courses</p>
        </div>          
    </div>--><!--feature end-->
	
	
    <div class="row self-hidden" style="display: none;">
        <!--feature start-->
        <div class="col-lg-12">
            <h2 style="text-transform:none;">Trending Courses</h2>
            <hr>
        </div>
        <!--feature end-->
    </div>
    <div class="row product-list courses-join-hide" id="featured-course"></div>
</div>
<div class="container"> 
    <div class="row self-hidden" style="display: none;">
        <!--feature start-->
        <div class="col-lg-12">
            <h2 style="text-transform:none;">Featured Courses</h2>
            <hr>
        </div>
        <!--feature end-->
    </div>
    <div class="row product-list courses-join-hide" id="sample-course">
<!--        <div class="col-xs-6 col-md-3 course1">
            <section class="panel">
                <div class="pro-img-box">
                    <img src="img/product-list/pro-1.jpg" alt=""> 
                    <a href="#" class="adtocart" title="Create for Student">
                        <i class="fa fa-user"></i>
                    </a>
                </div>
                <div class="panel-body">
                    <h4 class="text-center">
                        <a href="#" class="pro-title course-name">
                            Chemistry
                        </a>
                    </h4>
                    <p class="price course-price">$300.00<span class="pull-right course-joined"><i class="fa fa-group"></i> 300</span> </p>
                </div>
            </section>
        </div>
        <div class="col-xs-6 col-md-3 course2">
            <section class="panel">
                <div class="pro-img-box">
                    <img src="img/product-list/web.jpg" alt="">
                    <a href="#" class="adtocart" title="Create for Student">
                        <i class="fa fa-user"></i>
                    </a>
                </div>                                  
                <div class="panel-body ">
                    <h4 class="text-center">
                        <a href="#" class="pro-title">
                            Web designing
                        </a>
                    </h4>
                    <p class="price">$300.00<span class="pull-right"><i class="fa fa-group"></i> 300</span> </p>
                </div>
            </section>
        </div>
        <div class="col-xs-6 col-md-3 course3">
            <section class="panel">
                <div class="pro-img-box">
                    <img src="img/product-list/java.jpg" alt="">
                    <a href="#" class="adtocart" title="Create for Licence">
                        <i class="fa fa-tags"></i>
                    </a>
                </div>
                <div class="panel-body ">
                    <h4 class="text-center">
                        <a class="pro-title" href="#">
                            Java Aplication
                        </a>
                    </h4>
                    <p class="price">$300.00<span class="pull-right"><i class="fa fa-group"></i> 300</span> </p>
                </div>
            </section>
        </div>
        <div class="col-xs-6 col-md-3 course4">
            <section class="panel">
                <div class="pro-img-box">
                    <img src="img/product-list/php.jpg" alt="">                                      
                    <a href="#" class="adtocart" title="Create for Student">
                        <i class="fa fa-user"></i>
                    </a>
                </div>
                <div class="panel-body ">
                    <h4 class="text-center">
                        <a class="pro-title" href="#">
                            PHP Development
                        </a>
                    </h4>
                    <p class="price">$300.00<span class="pull-right"><i class="fa fa-group"></i> 300</span> </p>
                </div>
            </section>
        </div>-->
    </div>
    <div class="row mbot20 self-hidden" style="display: none;">
       <div class="container text-center">
            <a class="btn btn-danger btn-lg" href="courseDetailStudent.php" data-filter="*">View All Courses</a>
        </div>
    </div>
    <!-- <div class="row">
        <div class="col-lg-12">
            <h2>Licensed Course For Institute </h2>
            <hr>
        </div>
    </div>
    <div class="row product-list " id="license-course">
        
    </div>
   <div class="row mbot50">
   
        <div class="container text-center">
            <a class="btn btn-danger btn-lg" href="courseDetailLicense.php" data-filter="*">View All Licensed Courses</a>
        </div>
    </div>
    -->
    
    <br>
    
    <!-- <div class="row">
        <div class="col-xs-12 col-sm-12 wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
            <div class="testimonial">
                <h2>
                    <span style="text-transform:none;">Why use Arcanemind</span>
                </h2>
            </div>
            <div class="tab-wrap">
                <div class="media" style="background-color: #fff;">
                    <div class="parrent pull-left">
                        <ul class="nav nav-tabs nav-stacked">
                            <li class="active"><a class="analistic-01" data-toggle="tab" href="#tab1"><strong>Students</strong></a></li>
                            <li class=""><a class="analistic-02" data-toggle="tab" href="#tab2"><strong>Coaching Institutes</strong></a></li>
                            <li class=""><a class="tehnical" data-toggle="tab" href="#tab3"><strong>Tutors &amp; Professors</strong></a></li>
                            <li class=""><a class="tehnical" data-toggle="tab" href="#tab4"><strong>Student &amp; Corporate Trainers</strong></a></li>
                            <li class=""><a class="tehnical" data-toggle="tab" href="#tab5"><strong>Schools &amp; Colleges</strong></a></li>
                        </ul>
                    </div>
                    <div class="parrent media-body">
                        <div class="tab-content">
                            <div id="tab1" class="tab-pane fade active in">
                                <div class="media">
                                    <div class="media-body">
                                        <h2>Students</h2>
                                            <ul>
                                        <li>Study Online Courses consisting of  educational content  and  Assignments/Exams</li>
                                        <li>Learn as per your own convenience of time and location</li>
                                        <li>Master  new Skills  that   will give you a flip in your career and workplace</li>
                                        <li>Choose  from a wide range of topics present on the Market Place</li>
                                        <li>Get detailed analysis of your  test performances using our rich Analytics</li>
                                            <li>Compare & analyse performance against your own peer group</li>
                                            <li>Better retention of concepts due to use of videos and presentations </li>
                                    </ul>
                                      
                                    </div>
                                </div>
                            </div>
                            <div id="tab2" class="tab-pane fade">
                                <div class="media">
                                   
                                    <div class="media-body">
                                        <h2>Coaching Institutes</h2>
                                        <ul>
                                        <li>Provide  online access to students & enhance their class room learning</li>
                                            <li>Help Improve performance  of Students</li>
                                            <li>Use your  own high quality  educational content  on our learning platform </li>
                                            <li>Save time and effort spent on checking test tapers  manually</li>
                                            <li>Compare and analyse performance of Students across Tests </li>
                                            <li>Increase revenues by reaching out to students not enrolled in the classroom coaching</li>
                                            <li>Flexibility to make course available on market place or use only for internal students</li>
                                            </ul>
                                            
                                    </div>
                                </div>
                            </div>
                            <div id="tab3" class="tab-pane fade">
                                <h2>Tutors &amp; Professors</h2>
                               <ul>
                                        <li>Grow your income by making online courses</li>
                                            <li>Help Students  improve their performance  by using our rich analytics</li>
                                            <li>Save time and effort spent on checking test papers manually</li>
                                            <li>Supplement your traditional class room teaching by going online</li>
                                            <li>Flexibility to make course available on market place or use only for internal students</li>
                                            </ul>
                            </div>
    
                            <div id="tab4" class="tab-pane fade">
                                <h2>Student &amp; Corporate Trainers</h2>
                                <ul>
                                        <li>Help Students learn better by providing them online access </li>
                                            <li>Increase your revenues  by providing online courses</li>
                                            <li>Reach out  globally to  students across geographies</li>
                                            <li>Enhance your brand  Equity  which in turn will boost your offline sessions</li>
                                            <li>Students get instant access to real time updates in content</li>
                                            <li>Flexibility to make course available on market place or use for internal students</li>
                                            </ul>
                            </div>
    
                            <div id="tab5" class="tab-pane fade">
                                <h2>Schools &amp; Colleges</h2>
                               <ul>
                                        <li>Save time and effort spent on Checking Test Papers  manually</li>
                                            <li>Use your  own high quality  educational content  on our flexible learning platform </li>
                                            <li>Help  students in better understanding of classroom teaching by giving them online assignments</li>
                                            <li>Improve performance  of students by use of analytics and  providing them flexibility to learn from mistakes</li>
                                            <li>Compare performance of students across different parameters</li>
                                            <li>Flexibility to make course available on market place or use only for internal students</li>
                                            </ul>
                            </div>
                        </div>
                        /.tab-content
                    </div>
                    /.media-body
                </div>
                /.media
            </div>
            /.tab-wrap
        </div>
        /.col-sm-6
    
        <div class="col-xs-12 col-sm-4 wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
            <div class="testimonial">
                <h2>Testimonials</h2>
                <div class="media testimonial-inner">
                    <div class="pull-left">
                        <img src="img/testimonials1.png" class="img-responsive img-circle">
                    </div>
                    <div class="media-body">
                        <p>Arcanmind ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
                        <span><strong>-kaushik/</strong>  Arcanemind.com</span>
                    </div>
                </div>
    
                <div class="media testimonial-inner">
                    <div class="pull-left">
                        <img src="img/testimonials1.png" class="img-responsive img-circle">
                    </div>
                    <div class="media-body">
                        <p>Arcanmind ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
                        <span><strong>-kaushik/</strong>  Arcanemind.com</span>
                    </div>
                </div>
    
            </div>
        </div>
    
    </div> -->
</div>
<div class="block testimonial">
    <div class="container">
      <div id="testimonialsSlider" class="owl-carousel testimonial-slider">
        <div class="text-center">
          <div class="avatar-cover">
            <img src="img/testimonials/mehul.jpg" alt="">
          </div>
          <h3>Mehul Popat </h3>
          <h5><em>(Instructor  and Expert in JAVA Spring Framework)</em></h5>
          <p>Arcanemind is an awesome place for Instructors like me who are  passionate about teaching.The best part that I like about them is that they  have an  Instructor friendly revenue system.</p>
        </div>
        <div class="text-center">
          <div class="avatar-cover">
            <img src="img/testimonials/sunil.jpg" alt="">
          </div>
          <h3>Sunil  Gupta</h3>
          <h5><em>(Instructor and Expert in C,Java and  MangoDB)</em></h5>
          <p>Very happy with the overall support  provided by Arcanemind. Not only are they open to ideas and suggestions, but the interface to upload courses is super quick.</p>
        </div>
        <div class="text-center">
          <div class="avatar-cover">
            <img src="img/testimonials/gate.jpg" alt="">
          </div>
          <h3>GateOnline Forum</h3>
          <h5><em>(Institute for GATE Entrance Exam Preparation)</em></h5>
          <p>The best part about being an Institute on Arcanemind is that you can invite multiple  Instructors &amp; Professors and ask them to make Subjects and Courses, in their area of expertise. This way you  can  have experts from different fields all together.</p>
        </div>
        <div class="text-center">
          <div class="avatar-cover">
            <img src="img/testimonials/apoorva.jpg" alt="">
          </div>
          <h3>Apoorva Jain</h3>
          <h5><em>(Sales Manager at Paytm)</em></h5>
          <p>Arcanemind ensures that the  courses present on the marketplace are  the  best  in terms of  course quality. This ensures that  students like me learn very quickly</p>
        </div>
        <div class="text-center">
          <div class="avatar-cover">
            <img src="img/testimonials/ishan.jpg" alt="">
          </div>
          <h3>Ishan Gurg</h3>
          <h5><em>(MBA Student at IIM Indore)</em></h5>
          <p>The part that I like about Arcanemind is that  the Instructors and   Institutes that make the courses are all experts in their fields.  This  ensures that all the best practices/ &amp; tricks  that  they have learned are incorporated in their courses.</p>
        </div>
      </div>
    </div>
</div>


