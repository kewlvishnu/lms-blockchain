<!-- College -->
<div id="college">
	<h4>College</h4>
	<div class="form-group">
			<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-10">
							<select multiple="multiple" class="multi-select" id="selectCollege" name="selectCollege[]"  data-text="college">
								<option value='46'>Medical</option>
								<option value='47'>Law</option>
								<option value='48'>Engineering</option>
								<option value='49'>Commerce</option>
								<option value='50'>Arts College</option>
								<option value='51'>Management</option>
								<option value='52'>Science</option>
								<option value='53'>Junior College(XI-XII)</option>
								<option value='-1'>Others</option>
							</select>
					</div>
					<div class="col-lg-6" id="collegeTypeOther">
						<label for="Institute_Name">College Type</label>
						<input type="text" placeholder="Others" id="collegeOther" class="form-control">
					</div>
			</div>
	</div>
</div>
<!-- /College -->