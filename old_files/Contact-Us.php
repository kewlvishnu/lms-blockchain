<?php
include 'html-template/header.php';
?>
<br/>
    <section id="contact-info">
	
        <div class="container">
           <div>                
            <h2>Contact Us</h2>
            <!--<p class="lead text-center">Arcanemind ipsum dolor sit amet, consectetur adipisicing elit</p>-->
        </div>
		   <div class="row contact-wrap"> 
                
                    <div class="col-sm-9 ">
					
					<div class="status alert alert-success" style="display: none"></div>
                <form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="sendemail.php">
				<span>Please fill in the requisite details in the following form.</span>
					
					<div class="row">
					<div class="col-sm-6 ">
                        <div class="form-group">
                            <label>Name/Institute *</label>
                            <input type="text" name="name" id="name" class="form-control" required="required">
                        </div>
						</div>
						<div class="col-sm-6 ">
                        <div class="form-group">
                            <label>Email *</label>
                            <input type="email" name="email" id="email" class="form-control" required="required">
                        </div>
						</div>
						</div>
						<div class="row">
					<div class="col-sm-6 ">
                        <div class="form-group">
                            <label>Phone *</label>
                            <input type="text" name="number" id="number" class="form-control" required="required">
                        </div>
						</div>
						<div class="col-sm-6 ">
						<div class="form-group">
                            <label>Subject *</label>
                            <input type="text" name="subject" id="subject" class="form-control" required="required">
                        </div>
                        <!--<div class="form-group">
                            <label>Company Name</label>
                            <input type="text" class="form-control">
                        </div>-->                        
                    </div>
					</div>
					<div class="row">
						<div class="col-sm-12 ">
							<div class="form-group">
								<label>Message *</label>
								<textarea name="message" id="message" required="required" class="form-control" rows="5"></textarea>
							</div> 
						</div>
					</div>			
					<div class="row">
						<div class="col-sm-7">
							<span id="cnf" style="display: none;"></span>
						</div>
						<div class="col-sm-5">			
							<div class="form-group pull-right">
								<button type="submit" name="submit" class="btn btn-primary btn-lg" id="sendMessage">Submit Message</button>
							</div>
						</div>
					</div>
                </form> 
				
            </div><!--/.row-->
			<div class="col-sm-3 map-content">
 
   <ul class="row">
    <li style="list-style:none">
		<address>
			<h5>Head Office</h5>
			<div><strong>Address:</strong><br>393, Mangaldas House<br>Naaz Cinema Compound<br> Below I.O. Bank<br> Mumbai-400004, INDIA</div><br>
			<div><strong>Phone:</strong><br>+91-​9741436024​ </div><br>
			<div><strong>Email:</strong><br>
				
				For Other Queries:<br><a href="mailto:contactus@arcanemind.com">contactus@arcanemind.com</a></div>
		</address>
	</li>
   </ul>
			
        </div>
    </section>  <!--/gmap_area -->

<!--
			<div class="gmap-area">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <div class="gmap">
                            <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=JoomShaper,+Dhaka,+Dhaka+Division,+Bangladesh&amp;aq=0&amp;oq=joomshaper&amp;sll=37.0625,-95.677068&amp;sspn=42.766543,80.332031&amp;ie=UTF8&amp;hq=JoomShaper,&amp;hnear=Dhaka,+Dhaka+Division,+Bangladesh&amp;ll=23.73854,90.385504&amp;spn=0.001515,0.002452&amp;t=m&amp;z=14&amp;iwloc=A&amp;cid=1073661719450182870&amp;output=embed"></iframe>
                        </div>
                    </div>

                   <!-- <div class="col-sm-7 map-content">
                        <ul class="row">
                            <li class="col-sm-6">
                                <address>
                                    <h5>Head Office</h5>
                                    <p>1537 Flint Street <br>
                                    Tumon, MP 96911</p>
                                    <p>Phone:670-898-2847 <br>
                                    Email Address:info@domain.com</p>
                                </address>

                                <address>
                                    <h5>Zonal Office</h5>
                                    <p>1537 Flint Street <br>
                                    Tumon, MP 96911</p>                                
                                    <p>Phone:670-898-2847 <br>
                                    Email Address:info@domain.com</p>
                                </address>
                            </li>


                            <li class="col-sm-6">
                                <address>
                                    <h5>Zone#2 Office</h5>
                                    <p>1537 Flint Street <br>
                                    Tumon, MP 96911</p>
                                    <p>Phone:670-898-2847 <br>
                                    Email Address:info@domain.com</p>
                                </address>

                                <address>
                                    <h5>Zone#3 Office</h5>
                                    <p>1537 Flint Street <br>
                                    Tumon, MP 96911</p>
                                    <p>Phone:670-898-2847 <br>
                                    Email Address:info@domain.com</p>
                                </address>
                            </li>
                        </ul>
                    </div>
                
				</div>
            </div>
-->    
            <!--/.container-->
<!--footer Start-->    
<?php include 'html-template/footer.php'; ?>
<script type="text/javascript" src="scripts/contactUs.js" ></script>
	<!--footer End-->

</body>
</html>