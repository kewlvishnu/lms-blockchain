<!DOCTYPE HTML>
<html>
	<head>
		<title>Entrance Exam Courses</title>
		<?php include 'html-template/header.php'; ?>
	</head>
	<body>
		<div class="container min-height">
			<div class="row  margin_top_40">
				<div class="col-lg-12">
		            <h2 style="text-transform:none;">Entrance Exam Courses</h2>
		            <hr>
		        </div>
				<div class="row product-list courses-join-hide" id="sample-course">
				</div>
			</div>
		</div>
		<?php include 'html-template/modal/verify.php'; ?>
		<?php include 'html-template/modal/notify.php'; ?>
		<?php include 'html-template/footer.php'; ?>
		<script src="scripts/accountVerify.js" type="text/javascript"></script>
		<script src="scripts/studentMarketEntranceExam.js" type="text/javascript"></script>
	</body>
</html>