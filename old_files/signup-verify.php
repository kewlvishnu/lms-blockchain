<?php
include 'html-template/signup.header.php';
?>
<div class="contact-form form-step-1 form_bg">
	<div>
		<h4 id="verifyError">Please Wait while we try to activate your account..</h4>
	</div>
</div>
<?php
include 'html-template/signup.footer.php';
?>
<script src="scripts/accountVerify.js"></script>		
</body>
</html>