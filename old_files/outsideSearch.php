<?php
include 'html-template/header.php';

?>
<br/>
<section id="contact-info">

	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="panel filter">
					<div class="categories">
						<h4>Categories</h4><hr>
						<ul>
							<li><label><input type="checkbox" class="catCheck" data-id="1"> Entrance Exams</label></li>
							<li><label><input type="checkbox" class="catCheck" data-id="2"> School & College Prep</label></li>
							<li><label><input type="checkbox" class="catCheck" data-id="3"> Hobbies & Skills</label></li>
							<li><label><input type="checkbox" class="catCheck" data-id="4"> Business & Management</label></li>
							<li><label><input type="checkbox" class="catCheck" data-id="5"> IT & Software</label></li>
							<li><label><input type="checkbox" class="catCheck" data-id="6"> Entrepreneurship</label></li>
							<li><label><input type="checkbox" class="catCheck" data-id="7"> Miscellaneous</label></li>
						</ul><hr>
						<h4>Price</h4><hr>
						<ul>
							<li><label><input type="checkbox" class="priceCheck" data-id="1"> Free </label></li>
							<li><label><input type="checkbox" class="priceCheck" data-id="2"> <i class="fa fa-dollar"></i> 1 to <i class="fa fa-dollar"></i> 10</label></li>
							<li><label><input type="checkbox" class="priceCheck" data-id="3"> <i class="fa fa-dollar"></i> 11 to <i class="fa fa-dollar"></i> 20</label></li>
							<li><label><input type="checkbox" class="priceCheck" data-id="4"> <i class="fa fa-dollar"></i> 21 to <i class="fa fa-dollar"></i> 30</label></li>
							<li><label><input type="checkbox" class="priceCheck" data-id="5"> <i class="fa fa-dollar"></i> 31 to <i class="fa fa-dollar"></i> 40</label></li>
							<li><label><input type="checkbox" class="priceCheck" data-id="6"> <i class="fa fa-dollar"></i> 41 to <i class="fa fa-dollar"></i> 50</label></li>
							<li><label><input type="checkbox" class="priceCheck" data-id="7"> <i class="fa fa-dollar"></i> 51 to <i class="fa fa-dollar"></i> 60</label></li>
							<li><label><input type="checkbox" class="priceCheck" data-id="8"> Above <i class="fa fa-dollar"></i> 60</label></li>
						</ul><hr>
					</div>
				</div>
			</div>
			
			
			<div class="col-lg-9">
				<div class="panel course" >
					<div class="row" style ="background-color: #2ecc71;" >
						<div class="col-lg-8" >
							<h2> Search Results For '<span id="text"></span>'</h2>
						</div>
						<div class="col-lg-4" style="margin-top: 2%;">
							<form role="form" action="search.php" method="get" id="searchForm">
								<input type="text" placeholder="Search Courses" class="form-control pull-right mbolt-15" id="searchBox" name="text">
								<div id="searchDiv"></div>
							</form>
						</div>
						</div>
						<div class="row">
						<div class="col-lg-12">
							<hr class="divider">
						</div>
						<div id="loadCourse">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--footer Start-->    
<?php include 'html-template/footer.php'; ?>
<script type="text/javascript" src="scripts/outsidesearch.js"></script>
<!--footer End-->

</body>
</html>