 <?php
include 'html-template/header.php';
?>
<!--<link href="styles/whitebg.css " rel="stylesheet">-->
<body style="background-color: #fff;">
	<!--container start-->
	<br/>
	<div class="container margin_top_60">
		
		<div class="row">
            <div class="col-lg-5">
                <div class="span5 about-carousel">
                    <div id="myCarousel" class="carousel slide">
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="active item">
                                <img src="img/arcanemind_about2.jpg" alt="">
                               <!-- <div class="carousel-caption">
                                    <p>Donec luctus ullamcorper nulla</p>
                                </div> -->
                            </div>
                            <div class="item">
                                <img src="img/arcanemind_about1.jpg" alt="">
                                 <!-- <div class="carousel-caption">
                                    <p>Donec luctus ullamcorper nulla</p>
                                </div> -->
                            </div>
                            <div class="item">
                                <img src="img/arcanemind_about3.jpg" alt="">
                                <!-- <div class="carousel-caption">
                                    <p>Donec luctus ullamcorper nulla</p>
                                </div> -->
                            </div>
							  <div class="item">
                                <img src="img/arcanemind_about4.jpg" alt="">
                                <!-- <div class="carousel-caption">
                                    <p>Donec luctus ullamcorper nulla</p>
                                </div> -->
                            </div>
                        </div>
                        <!-- Carousel nav -->
                        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="carousel-control right" href="#myCarousel" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 about">
                <h2>Welcome to Arcanemind</h2>
                <p>
                   Arcanemind.com is a Cloud Based Learning platform & Market place, where Coaching institutes, Trainers, Schools and Colleges
                host education content & conduct online tests  with detailed analytics.
					
				</p>
				<p>
					The founders are a bunch of Mckinsey & Harvard Alumni, who are really passionate about e-learning and how it can help Students/anyone with a zest for learning perform better in their career. 
				</p>
            </div>
			
        </div>
		<div class="row">
			
		</div>
		
		<!--<div class="row">
			<div class="col-lg-12 about">
				<h2>About us</h2>
				<p>
					Arcanemind.com is a Cloud Based Learning platform & Market place, where Coaching institutes, Trainers, Schools and Colleges
				</p>
				<p>
					<ul>
						<li>host  education content  &  conduct online tests  with detailed analytics
					</ul>
				</p>
				<p>
					The founders are a bunch of Mckinsey & Harvard Alumni, who are really passionate about e-learning and how it can help Students/anyone with a zest for learning perform better in their careers
				</p>
			</div>-->
			<!--<div class="col-lg-12">
				<h2>What do we Offer?</h2>
				<h3>As a Coaching Institute/Tutor/College/Professor, you will</h3>
				<ul>
					<li>Create academic  courses consisting of  online Tests  & academic content  on our cloud based platform</li>
					<li>Conduct Online Tests for the students  with detailed analytics like  Rank, Percentage, Time spent on a question, comparison with toppers</li>
					<li>Host content in the form of videos, documents and presentations</li>
					<li>Reach out to students across geographies for the courses you make</li>
					<li>License Content  & Tests to  other  Institutes & Tutors for a fee</li>
				</ul>
				<h3>As a Student, you will</h3>
				<ul>
					<li>Study courses, which consist of online Tests and  Education Content like Pre recorded Videos, documents and Presentations on our  cloud based platform</li>
					<li>Get detailed assessment of your performance across  examinations</li>
					<li>Significantly improve your performance  by practicing hard and learning by mistakes</li>
					<li>Study and learn as per your own convenient timing and place</li>
<br><br>
<p>We will love to hear from you. Reach out to us at <a href="mailto:contactus@arcanemind.com">contactus@arcanemind.com</a> or +91-9741436024</p>
			</div>
		</div>-->
<br>
		<!-- <div class="team">
				<div style="visibility: visible; animation-name: fadeInDown;" class="center wow fadeInDown animated">
					<h2>Team of Corlate</h2>
					<p class="lead">Arcanmind ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
				</div>

				<div class="row clearfix">
					<div class="col-md-4 col-sm-6">	
						<div style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInDown;" class="single-profile-top wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="300ms">
							<div class="media">
								<div class="pull-left">
									<a href="#"><img class="media-object" src="img/man1.jpg" alt=""></a>
								</div>
								<div class="media-body">
									<h4>Jhon Doe</h4>
									<h5>Founder and CEO</h5>
									<ul class="tag clearfix">
										<li class="btn"><a href="#">Web</a></li>
										<li class="btn"><a href="#">Ui</a></li>
										<li class="btn"><a href="#">Ux</a></li>
										<li class="btn"><a href="#">Photoshop</a></li>
									</ul>
									
									<ul class="social_icons">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li> 
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<p>There are many variations of passages of Arcanmind Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
						</div>
					</div>
					
					
					<div class="col-md-4 col-sm-6 col-md-offset-2">	
						<div style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInDown;" class="single-profile-top wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="300ms">
							<div class="media">
								<div class="pull-left">
									<a href="#"><img class="media-object" src="img/man2.jpg" alt=""></a>
								</div>
								<div class="media-body">
									<h4>Jhon Doe</h4>
									<h5>Founder and CEO</h5>
									<ul class="tag clearfix">
										<li class="btn"><a href="#">Web</a></li>
										<li class="btn"><a href="#">Ui</a></li>
										<li class="btn"><a href="#">Ux</a></li>
										<li class="btn"><a href="#">Photoshop</a></li>
									</ul>
									<ul class="social_icons">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li> 
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<p>There are many variations of passages of Arcanmind Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
						</div>
					</div>
				</div>
				<div class="row team-bar">
					<div class="first-one-arrow hidden-xs">
						<hr>
					</div>
					<div class="first-arrow hidden-xs">
						<hr> <i class="fa fa-angle-up"></i>
					</div>
					<div class="second-arrow hidden-xs">
						<hr> <i class="fa fa-angle-down"></i>
					</div>
					<div class="third-arrow hidden-xs">
						<hr> <i class="fa fa-angle-up"></i>
					</div>
					<div class="fourth-arrow hidden-xs">
						<hr> <i class="fa fa-angle-down"></i>
					</div>
				</div>

				<div class="row clearfix">   
					<div class="col-md-4 col-sm-6 col-md-offset-2">	
						<div style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInUp;" class="single-profile-bottom wow fadeInUp animated" data-wow-duration="1000ms" data-wow-delay="600ms">
							<div class="media">
								<div class="pull-left">
									<a href="#"><img class="media-object" src="img/man3.jpg" alt=""></a>
								</div>

								<div class="media-body">
									<h4>Jhon Doe</h4>
									<h5>Founder and CEO</h5>
									<ul class="tag clearfix">
										<li class="btn"><a href="#">Web</a></li>
										<li class="btn"><a href="#">Ui</a></li>
										<li class="btn"><a href="#">Ux</a></li>
										<li class="btn"><a href="#">Photoshop</a></li>
									</ul>
									<ul class="social_icons">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li> 
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<p>There are many variations of passages of Arcanmind Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-md-offset-2">
						<div style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInUp;" class="single-profile-bottom wow fadeInUp animated" data-wow-duration="1000ms" data-wow-delay="600ms">
							<div class="media">
								<div class="pull-left">
									<a href="#"><img class="media-object" src="img/man4.jpg" alt=""></a>
								</div>
								<div class="media-body">
									<h4>Jhon Doe</h4>
									<h5>Founder and CEO</h5>
									<ul class="tag clearfix">
										<li class="btn"><a href="#">Web</a></li>
										<li class="btn"><a href="#">Ui</a></li>
										<li class="btn"><a href="#">Ux</a></li>
										<li class="btn"><a href="#">Photoshop</a></li>
									</ul>
									<ul class="social_icons">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li> 
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div>
							<p>There are many variations of passages of Arcanmind Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
						</div>
					</div>
				</div>
			</div>-->
	</div>
        <div class="container">
            <div class="row">
                <div class="row">
                    <div class="text-center feature-head">
                        <h1><strong> What do we Offer? </strong></h1>
                        <p style="font-size: 20px"><strong>As a Coaching Institute/Tutor/College/Professor/Publisher you will</strong></p>
                    </div>
                    <div class="services">
                        <div class="col-lg-6 col-sm-6">
                            <div class="icon-wrap ico-bg round">
								<i class="fa fa-university"></i>
                            </div>
                            <div class="content">
                                <h3 class="title">Create Courses using Online Platform</h3>
                                <p>Create courses consisting of online assignments/exmas & educational content on our cloud based platform.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <div class="icon-wrap ico-bg round">
                                <i class="fa fa-book"></i>
                            </div>
                            <div class="content">
                                <h3 class="title">Conduct Assignments and Exams</h3>
                                <p>Conduct online tests for students with detailed analytics like  rank, percentage, time spent on a question, comparison with toppers and detailed graphs.</p>
                            </div>
                        </div>
                    </div>
                    <div class="services" style="margin-bottom: 20px;">
                        <div class="col-lg-6 col-sm-6">
                            <div class="icon-wrap ico-bg round">
                                <i class="fa fa-desktop"></i>
                            </div>
                            <div class="content">
                                <h3 class="title">Host Educational Content</h3>
                                <p>Host educational content in the form of videos, documents and presentations.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <div class="icon-wrap ico-bg round">
                                <i class="fa fa-flask"></i>
                            </div>
                            <div class="content">
                                <h3 class="title">Reach Students Globally via Market Place</h3>
                                <p>Reach out to students across geographies for the courses you make.</p>
                            </div>
                        </div>
                    </div>
                    <div class="services" style="margin-bottom: 0px;">
                        <div class="col-lg-6 col-sm-6">
                            <!--<div class="icon-wrap ico-bg round">
                                <i class=" fa fa-bookmark-o"></i>
                            </div>
                            <div class="content">
                                <h3 class="title">License Content</h3>
                                <p>License Content  & Tests to  other  Institutes & Tutors for a fee</p>
                            </div>-->
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <!--<div class="icon-wrap ico-bg round">
                                <i class=" fa fa-eye"></i>
                            </div>
                            <div class="content">
                                <h3 class="title">Bootstrap 3</h3>
                                <p>Suspendisse dignissim in sem eget pulvinar. Mauris aliquam nulla at libero pretium, eu tincidunt nulla molestie pulvinar posuere.</p>
                            </div>-->
                        </div>
                    </div>
					
					<div class="text-center" style="margin: 0px 0px 20px 0px important;">
                        <p style="color: #8a8b8b; font-size: 20px; font-weight: 300; margin-bottom: 20px;"><strong>As a Student, you will</strong></p>
                    </div>
					<div class="services">
                        <div class="col-lg-6 col-sm-6">
                            <div class="icon-wrap ico-bg round">
								<i class="fa fa-book"></i>
                            </div>
                            <div class="content">
                                <h3 class="title">Study Courses</h3>
                                <p>Study courses which consist of online Tests and  Education Content like Pre recorded Videos, documents and Presentations on our  cloud based platform.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <div class="icon-wrap ico-bg round">
								<i class="fa fa-file-text-o"></i>
                            </div>
                            <div class="content">
                                <h3 class="title">See detailed Assessment</h3>
                                <p>Get detailed assessment of your performance across  examinations.</p>
                            </div>
                        </div>
                    </div>
                    <div class="services">
                        <div class="col-lg-6 col-sm-6">
                            <div class="icon-wrap ico-bg round">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <div class="content">
                                <h3 class="title">Improve Performance</h3>
                                <p>Significantly improve your performance  by practicing hard and learning by mistakes.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <div class="icon-wrap ico-bg round">
								<i class="fa fa-check"></i>
                            </div>
                            <div class="content">
                                <h3 class="title">Learn and Study as per Convenience</h3>
                                <p>Study and learn as per your own convenient timing and place.</p>
                            </div>
                        </div>
                    </div>
					
                </div>
            </div>
        </div>
		<div style="padding:10px 0px; background-color:#591750;">
     <p style="text-align:center; font-size: 20px; margin:0px; color:#fff;">We will love to hear from you. Reach out to us at <i class="fa fa-envelope" style="font-size: 20px; color:#f37d6c;"></i> <a style="color:#fff;" href="mailto:contactus@arcanemind.com">contactus@arcanemind.com</a> or&nbsp <i class="fa fa-phone" style="font-size: 20px; color:#f37d6c;"></i> +91-9741436024</p>
		</div>
	<!--container end-->
		<br/>
	 <!--footer start-->
<?php
include 'html-template/footer.php';
?>
	<!--footer End-->

  </body>
</html>
