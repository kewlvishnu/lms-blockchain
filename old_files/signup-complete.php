<?php
include 'html-template/signup.header.php';
?>
<div class="col-lg-9 col-sm-9">
<div class="contact-form form-step-1 form_bg">
	<h1>Great!! Just one last step</h1>
	<h4 style="text-transform: none;">We have sent you a Verification email</h4>
	<br><br>
	<a class="pull-right resend-link" href="#">Resend Verification Link</a>
</div>
<?php
include 'html-template/signup.footer.php';
?>
		
</body>
</html>