<!DOCTYPE HTML>
<html>
	<head>
		<title>IT & Software Courses</title>
		<?php include 'html-template/header.php'; ?>
	</head>
	<body>
		<div class="container min-height">
			<div class="row  margin_top_40">
				<div class="col-lg-12">
		            <h2 style="text-transform:none;">IT & Software Courses</h2>
		            <hr>
		        </div>
				<div class="row product-list courses-join-hide" id="sample-coursescholl_coll">
				</div>
			</div>
		</div>
		<?php include 'html-template/modal/verify.php'; ?>
		<?php include 'html-template/modal/notify.php'; ?>
		<?php include 'html-template/footer.php'; ?>
		<script src="scripts/accountVerify.js" type="text/javascript"></script>
		<script src="scripts/studentMarketIT_Software.js" type="text/javascript"></script>
	</body>
</html>