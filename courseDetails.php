<?php
include 'html-template/header.php';
?> 
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="styles/review.css">
<link rel="stylesheet" href="student/flowplayer/skin/minimalist.css">
<br/>
<?php
include 'html-template/CourseDetail.php';   
include 'html-template/footer.php';
include 'html-template/videoModal.html.php';
include 'html-template/purchaseModal.html.php';
include 'html-template/refundModal.php';
?>
<script src="student/flowplayer/flowplayer.min.js"></script>
<script src="scripts/CourseDetail.js"></script>		
</body>
</html>