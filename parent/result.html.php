<?php
    require_once('../config/url.functions.php');
    require_once('html-template/header.php');
    require_once('html-template/breadcrumbs.php');
    //var_dump($_SESSION);
?>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <!-- START LINE CHART -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-minus-square hide-summary"></i> TimeLine Grph</h3>
                    </div>
                    <div class="panel-body">
                        <div id="graph12"></div>
                        <div class="text-center"><h3>Comparision graph w.r.t to timeline</h3></div>
                    </div>
                </div>
                <!-- END LINE CHART -->
            </div>
        </div>
        
        <?php /*
        <!-- START WIDGETS -->
        <div class="row">
            <div class="col-md-6">
                
                <!-- START LINE CHART -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Attempt Vs Score</h3>
                    </div>
                    <div class="panel-body">
                        <canvas id="attemptChart" width="400" height="400"></canvas>
                    </div>
                </div>
                <!-- END LINE CHART -->
                
            </div>
            <div class="col-md-6">
                
                <!-- START LINE CHART -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Attempt Vs Percentage</h3>
                    </div>
                    <div class="panel-body">
                        <!-- <div id="barPercentage" style="height: 300px;"></div> -->
                        <canvas id="percentageChart" width="400" height="400"></canvas>
                    </div>
                </div>
                <!-- END LINE CHART -->
                
            </div>
        </div>
        <!-- END WIDGETS -->
        */ ?>

        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        <h4><i class="fa fa-minus-square hide-summary"></i> Summary Attempt No: <span id="attemptNo"></span> </h4>                            
                    </header>
                    <div class="panel-body"> 
                        <div class="row">
                            <div class="col-lg-3 col-md-6">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th colspan="2" class="text-center"><i class="fa fa-clock-o" class="font-big"></i> Timings</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th><strong>Started on:</strong></th>
                                            <td><span id="startDate"></span></td>
                                        </tr>
                                        <tr>
                                            <th><strong>Completed on:</strong></th>
                                            <td><span id="endDate"></span></td>
                                        </tr>
                                        <tr>
                                            <th><strong>Time taken:</strong></th>
                                            <td><span id="timeTaken"></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-2 col-md-6">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th colspan="2" class="text-center"><i class="fa fa-bar-chart"></i> Score Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th><strong>Rank:</strong></th>
                                            <td><span id="rank"></span></td>
                                        </tr>
                                        <tr>
                                            <th><strong>Percentage:</strong></th>
                                            <td><span id="percentage"></span> %</td>
                                        </tr>
                                        <tr>
                                            <th><strong>Score:</strong></th>
                                            <td><span id="score"></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-3 col-md-12">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th colspan="2" class="text-center"><i class="fa fa-arrows-h"></i> Navigate Attempts</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center wd-50">
                                                <a href="javascript:void(0)" class="btn btn-primary btn-lg mtb-20 previous-attempt disabled">
                                                    <i class="fa fa-long-arrow-left"></i> Previous
                                                </a>
                                            </td>
                                            <td class="text-center wd-50">
                                                <a href="javascript:void(0)" class="btn btn-primary btn-lg mtb-20 next-attempt disabled">
                                                    Next <i class="fa fa-long-arrow-right"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!-- <div class="row">
                                    <div class="col-xs-6">
                                        <a href="javascript:void(0)" class="btn btn-primary previous-attempt disabled">
                                            <img src="../admin/img/result/previous_attempt.png"> Previous Attempt
                                        </a>
                                    </div>
                                    <div class="col-xs-6">
                                        <a href="javascript:void(0)" class="btn btn-primary next-attempt disabled">
                                            <img src="../admin/img/result/next_attempt.png"> Next Attempt
                                        </a>
                                    </div>
                                </div> -->
                            </div>
                            <div class="col-lg-2 col-md-6">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th colspan="2" class="text-center"><i class="fa fa-question-circle"></i> Exam Categories</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">
                                                <a class="btn btn-primary btn-lg mtb-20" href="#categories" data-toggle="modal">
                                                    Question Categories
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!-- <a class="btn btn-primary" href="#categories" data-toggle="modal">
                                    <img src="../admin/img/result/categories.png"> See Question Categories
                                </a> -->
                            </div>
                            <div class="col-lg-2 col-md-6">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th colspan="2" class="text-center"><i class="fa fa-balance-scale"></i> Compare</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">
                                                <a class="btn btn-primary btn-lg mtb-20" href="#topperComparison" data-toggle="modal">
                                                    Compare with toppers
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!-- <a class="btn btn-primary" href="#topperComparison" data-toggle="modal">
                                    <img src="../admin/img/result/compare.png"> Compare with toppers
                                </a> -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <table class="table table-bordered table-striped" id="sectionTable">
                                    <thead>
                                    <tr class="custom-bg">
                                        <th>Section</th>
                                        <th>Total Question</th>
                                        <th>Time Taken</th>
                                        <th>Question Response</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>                                 
                                </table>
                            </div>
                            <div class="col-lg-2">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Legend</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <button class="btn btn-success btn-sm">&nbsp;</button> Correct Answer
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <button class="btn btn-danger btn-sm">&nbsp;</button> Wrong Answer
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <button class="btn btn-black btn-sm">&nbsp;</button> Unattempted Questions
                                            </td>
                                        </tr>
                                    </tbody>                                 
                                </table>
                            </div>
                        </div>
                        <a class="btn btn-info btn-md pull-right" href="<?php echo $sitepath.'parent/'; ?>">Back</a>
                    </div>  
                </section>
                <div id="sectionDetails">
                
                </div>
            </div> 
        </div>

        <!-- <div id="hidden-topper-graph">
            <div id="topper-container">
                <span class="pull-right" style="margin-right: -27px;font-size: 1.8em;margin-top: -35px;" id="topperClose"><i class="fa fa-times-circle-o text-danger"></i></span>
                <div class="row">
                    <div class="col-lg-6">
                        <section class="panel">
                            <header class="panel-heading">
                                Score
                            </header>
                            <div class="panel-body">
                                <div id="hero-bar1" class="graph"></div>
                            </div>
                        </section>
                    </div>
                    <div class="col-lg-6">
                        <section class="panel">
                            <header class="panel-heading">
                                Percentage
                            </header>
                            <div class="panel-body">
                                <div id="hero-bar2" class="graph"></div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- <div id="hidden-time-graph">
            <div id="time-container">
                <span class="pull-right" style="margin-right: -27px;font-size: 1.8em;margin-top: -35px;" id="timeClose"><i class="fa fa-times-circle-o text-danger"></i></span>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading">
                                Time spent on a question statistics
                            </header>
                            <div class="panel-body to-be-added">
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div> -->
        
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
<?php
    require_once('html-template/modals/category-modal.html.php');
    require_once('html-template/modals/topper-modal.html.php');
    require_once('html-template/modals/compare-modal.html.php');
    require_once('html-template/footer.php');
?>