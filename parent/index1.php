<?php
    //get the last-modified-date of this very file
    $lastModified=filemtime(__FILE__);
    //get a unique hash of this file (etag)
    $etagFile = md5_file(__FILE__);
    //get the HTTP_IF_MODIFIED_SINCE header if set
    $ifModifiedSince=(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
    //get the HTTP_IF_NONE_MATCH header if set (etag: unique file hash)
    $etagHeader=(isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

    //set last-modified header
    header("Last-Modified: ".gmdate("D, d M Y H:i:s", $lastModified)." GMT");
    //set etag-header
    header("Etag: $etagFile");
    //make sure caching is turned on
    header('Cache-Control: max-age=290304000, public');
    
    //check if page has changed. If not, send 304 and exit
    if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])==$lastModified || $etagHeader == $etagFile)
    {
           header("HTTP/1.1 304 Not Modified");
           exit;
    }

    //  url functions and routes
    require_once('../config/url.functions.php');
    $pageArr = explode('/', $pageUrl);
    $pageGet = explode('?', $pageUrl);
    
    //$page = "student";
    /*
    **  flagInvalid : true = valid urls, false = invalid urls
    */
    $flagInvalid = false;
    /*
    **  flagSession :
    **  0 = neutral URLs which opens regardless of user login status
    **  1 = these urls open only for non logged in users
    **  2 = these urls open only for logged in users
    */
    $flagSession = 0;
    $pages = array('', 'index.php', 'dashboard', 'result');
    
    if (in_array($pageArr[1], $pages)) {
        if ($pageArr[1] == '' || $pageArr[1] == 'index.php') {
            $page     = "dashboard";
            $flagSession = 2;
        } else if ($pageArr[1] == 'result') {
            if (isset($pageArr[2]) && !empty($pageArr[2])) {
                if ($pageArr[2] == "exam" || $pageArr[2] == "assignment") {
                    if (isset($pageArr[3]) && !empty($pageArr[3])) {
                        $page     = "result";
                        $slug = $pageArr[3];
                    } else {
                        $flagInvalid = true;
                    }
                } else {
                    $flagInvalid = true;
                }
            } else {
                $flagInvalid = true;
            }
        }
    } else {
        $flagInvalid = true;
    }
    if ($flagInvalid) {
        header('location:'.$sitepath404);
    } else {
        @session_start();
        if (isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
            /**
            * header checks for metas, page specific validation and portfolio subdomain validations
            **/
            require_once('inc/header.functions.php');
            require_once('inc/meta.functions.php');
            if ($page == "dashboard") {
                require_once('dashboard.html.php');
            } else if ($page == "result") {
                require_once('result.html.php');
            }
        } else {
            //header('location:'.$sitepath);
        }
    }
    
?>