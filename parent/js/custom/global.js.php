<script type="text/javascript">
var sitePath	= '<?php echo $sitepath; ?>';
var sitePath404 = '<?php echo $sitepath404; ?>';
var sitepathPackages	 = '<?php echo $sitepathPackages; ?>';
var sitepathPackageDetail= '<?php echo $sitepathPackageDetail; ?>';
var sitePathCourses		 = '<?php echo $sitepathCourses; ?>';
var sitePathCourseDetail = '<?php echo $sitepathCourseDetail; ?>';
var page = '<?php echo $page; ?>';
var ApiEndPoint = sitePath+'api/index.php';
var currency = 2;
var slug = '<?php echo ((isset($slug))?($slug):('')); ?>';
var subdomain = '<?php echo ((isset($subdomain))?($subdomain):('')); ?>';
var studentId = '<?php echo ((isset($_SESSION["student_id"]))?($_SESSION["student_id"]):('')); ?>';
var courseId = '<?php echo ((isset($_SESSION["course_id"]))?($_SESSION["course_id"]):('')); ?>';

var MONTH=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

// templates
var tempDollar = '<i class="fa fa-dollar"></i>';
var tempRupee  = '<i class="fa fa-rupee"></i>';

$(document).ready(function() {
	fetchAttemptDetails();
});

function getUrlParameter(sParam)
{
	sParam = sParam.toLowerCase();
	var sPageURL = window.location.search.substring(1).toLowerCase();
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) 
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) 
		{
			return sParameterName[1];
		}
	}
}
$(document).on('click','.scrolltodiv', function(event) {
	event.preventDefault();
	var target = "#" + this.getAttribute('data-target');
	$('html, body').animate({
		scrollTop: $(target).offset().top
	}, 1000);
});
/**
* Funtion to format single digit number into double digit i.e. append one before item
*
* @author Rupesh Pandey
* @date 23/05/2015
* @param integer input number
* @return integer two digit number
*/
function format(number) {
	if(number < 10)
		return '0' + number;
	else
		return number;
}

function percentage(number, total) {
	if(total == 0)
		return 0;
	return (parseInt(number)/parseInt(total)) * 100;
}

function calcDisplayTimeWords(totalDuration) {
	var hours = parseInt( totalDuration / 3600 ) % 24;
	var minutes = parseInt( totalDuration / 60 ) % 60;
	var seconds = totalDuration % 60;

	if(hours>0) {
		if(minutes>0 && minutes<=15) {
			return hours+".25h";
		} else if(minutes>15 && minutes<=30) {
			return hours+".5h";
		} else if(minutes>30 && minutes<=45) {
			return hours+".75h";
		} else {
			return hours+"h";
		}
	} else if(minutes>0) {
		if(seconds>0 && seconds<=15) {
			return minutes+".25m";
		} else if(seconds>15 && seconds<=30) {
			return minutes+".5m";
		} else if(seconds>30 && seconds<=45) {
			return minutes+".75m";
		} else {
			return minutes+"m";
		}
	} else {
		return seconds+" s";
	}
}

function calcDisplayTime(totalSec) {
	var hours = parseInt( totalSec / 3600 ) % 24;
	var minutes = parseInt( totalSec / 60 ) % 60;
	var seconds = totalSec % 60;

	if(hours>0) {
		return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
	} else {
		return (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
	}
}

function formatContactNumber(user) {
	var num1= "", num2 = "";
	if(user.contactMobilePrefix != '' && user.contactMobile != '') {
		num1 = user.contactMobilePrefix + '-' + user.contactMobile;
	}
	
	if(user.contactLandlinePrefix != '' && user.contactLandline != '') {
		num2 = user.contactLandlinePrefix + '-' + user.contactLandline;
	}
	
	return (num1 + ', ' + num2).replace(/,\s$/,'').replace(/^,\s/,'');
}

function removeLoader() {
	//$('#loader').remove();
}

function emailValidate(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(regex.test(email))
		return true;
	return false;
}
//function to convert given seconds into minute and seconds
function convertSecondsIntoMinutes(sec) {
	var min = 0;
	if(sec - 60 >= 0) {
		min = Math.floor(sec / 60);
		sec = sec - (min * 60);
	}
	var time = min + ' minutes ' + sec + ' seconds';
	return time;
}
</script>