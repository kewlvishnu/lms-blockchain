<?php
    require_once('../config/url.functions.php');
    require_once('html-template/header.php');
    require_once('html-template/breadcrumbs.php');
    //var_dump($_SESSION);
?>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        
        <!-- START WIDGETS -->
        <div class="row">
            <div class="col-md-3">
                
                <!-- START WIDGET SLIDER -->
                <div class="widget widget-default widget-carousel">
                    <div class="owl-carousel" id="owl-example">
                        <div>
                            <div class="widget-title">Students a month</div>
                            <div class="widget-int">500+</div>
                        </div>
                        <div>
                            <div class="widget-title">Lectures every week</div>
                            <div class="widget-int">50+</div>
                        </div>
                        <div>
                            <div class="widget-title">Awesome courses</div>
                            <div class="widget-int">45+</div>
                        </div>
                    </div>
                </div>
                <!-- END WIDGET SLIDER -->
                
            </div>
            <div class="col-md-3">
                
                <!-- START WIDGET MESSAGES -->
                <div class="widget widget-default widget-item-icon" onclick="<?php echo $sitepath; ?>parent/">
                    <div class="widget-item-left">
                        <span class="fa fa-envelope"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count"><?php echo count($exams); ?></div>
                        <div class="widget-title">Exams taken</div>
                        <div class="widget-subtitle">by <?php echo $_SESSION['name']; ?></div>
                    </div>
                </div>
                <!-- END WIDGET MESSAGES -->
                
            </div>
            <div class="col-md-3">
                
                <!-- START WIDGET REGISTRED -->
                <div class="widget widget-default widget-item-icon" onclick="<?php echo $sitepath; ?>parent/">
                    <div class="widget-item-left">
                        <span class="fa fa-user"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count"><?php echo count($assignments); ?></div>
                        <div class="widget-title">Assignments taken</div>
                        <div class="widget-subtitle">by <?php echo $_SESSION['name']; ?></div>
                    </div>
                </div>
                <!-- END WIDGET REGISTRED -->
                
            </div>
            <div class="col-md-3">
                
                <!-- START WIDGET CLOCK -->
                <div class="widget widget-danger widget-padding-sm">
                    <div class="widget-big-int plugin-clock">00:00</div>
                    <div class="widget-subtitle plugin-date">Loading...</div>
                </div>
                <!-- END WIDGET CLOCK -->
                
            </div>
        </div>
        <!-- END WIDGETS -->

        <!-- <div class="container-fluid">
            <h2>Notifications</h2>
            <?php/*
                if (count($parentNotifications)) {
                    foreach ($parentNotifications as $key => $parentNotification) {
                        echo '<div class="alert alert-warning">'.$parentNotification['message'].'
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>';
                    }
                } else {
                    echo '<p>No new notifications</p>';
                }*/
            ?>
        </div> -->
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
<?php
    require_once('html-template/footer.php');
?>