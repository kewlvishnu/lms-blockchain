<?php
    require_once '../api/Vendor/ArcaneMind/Exam.php';
    require_once '../api/Vendor/ArcaneMind/Student.php';
    $ex  = new Exam();
    $st   = new Student();
    $data1 = new stdClass();
    $data1->userId   = $_SESSION['userId'];
    $data1->userRole = $_SESSION['userRole'];
    $data1->courseId = $_SESSION['course_id'];
    $res = $ex->getStudentExamAttemps($data1);
    $examsAssignements = $res->exams;
    $exams = array();
    $assignments = array();
    foreach ($examsAssignements as $key => $value) {
        if ($value['type'] == 'Exam') {
            $exams[] = $value;
        } else {
            $assignments[] = $value;
        }
    }
    $data2 = new stdClass();
    $data2->userId = $_SESSION['student_id'];
    $stu = $st->getStudentDetails($data2);
    /*var_dump($stu);
    die();*/
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Arcanemind - Parent Dashboard</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <base href="<?php echo $sitepath.'parent/'; ?>">
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- LESSCSS INCLUDE -->
        <link rel="stylesheet/less" type="text/css" href="css/styles.less"/>
        <script type="text/javascript" src="js/plugins/lesscss/less.min.js"></script>
        <!-- EOF LESSCSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="<?php echo $sitepath; ?>parent/">Arcanemind</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="<?php echo $stu->details['profilePic']; ?>" alt="<?php echo $_SESSION['name']; ?>"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="<?php echo $stu->details['profilePic']; ?>" alt="<?php echo $_SESSION['name']; ?>"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><?php echo $_SESSION['name']; ?></div>
                                <?php
                                    if ($_SESSION['gender']>0) {
                                ?>
                                    <div class="profile-data-title"><?php echo (($_SESSION['gender']==1)?'Son':'Daughter'); ?></div>
                                <?php
                                    } else {
                                ?>
                                    <div class="profile-data-title">Student</div>
                                <?php

                                    }
                                ?>
                            </div>
                        </div>
                    </li>
                    <li class="xn-title">Navigation</li>
                    <li class="active">
                        <a href="<?php echo $sitepath; ?>parent/"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>
                    </li>
                    <li class="xn-openable">
                        <a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Exams (<?php echo count($exams); ?>)</span></a>
                        <?php
                            //var_dump($exams);
                            if (count($exams)>0) {
                                echo '<ul>';
                                foreach ($exams as $key => $value) {
                                    echo "<li><a href='{$sitepath}parent/result/exam/{$value["id"]}'><span class='fa fa-image'></span> {$value["exam"]}</a></li>";
                                }
                                echo '</ul>';
                            }
                        ?>
                    </li>
                    <li class="xn-openable">
                        <a href="javascript:void(0)"><span class="fa fa-files-o"></span> <span class="xn-text">Assignments (<?php echo count($assignments); ?>)</span></a>
                        <?php
                            //var_dump($exams);
                            if (count($assignments)>0) {
                                echo '<ul>';
                                foreach ($assignments as $key => $value) {
                                    echo "<li><a href='{$sitepath}parent/result/exam/{$value["id"]}'><span class='fa fa-image'></span> {$value["exam"]}</a></li>";
                                }
                                echo '</ul>';
                            }
                        ?>
                    </li>
                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- TASKS -->
                    <li class="xn-icon-button pull-right">
                        <a href="#"><span class="fa fa-tasks"></span></a>
                        <div class="informer informer-warning"><?php echo count($parentNotifications); ?></div>
                        <?php
                            if (count($parentNotifications)) { ?>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="fa fa-tasks"></span> Notifications</h3>                                
                                <div class="pull-right">
                                    <span class="label label-warning"><?php echo count($parentNotifications); ?> active</span>
                                </div>
                            </div>
                            <div class="panel-body list-group scroll" style="height: 200px;"><?php
                                foreach ($parentNotifications as $key => $parentNotification) {
                                    /*echo '<div class="alert alert-warning">'.$parentNotification['message'].'
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>';*/
                                    echo '<a class="list-group-item" href="'.$sitepathParentNotifications.'">
                                            <strong>'.$parentNotification['message'].'</strong>
                                        </a>';
                                }
                            }
                            ?><!-- <a class="list-group-item" href="#">
                                    <strong>Aenean ac cursus</strong>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">80%</div>
                                    </div>
                                    <small class="text-muted">Dmitry Ivaniuk, 24 Sep 2014 / 80%</small>
                                </a>
                                <a class="list-group-item" href="#">
                                    <strong>Lorem ipsum dolor</strong>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">95%</div>
                                    </div>
                                    <small class="text-muted">John Doe, 23 Sep 2014 / 95%</small>
                                </a>
                                <a class="list-group-item" href="#">
                                    <strong>Cras suscipit ac quam at tincidunt.</strong>
                                    <div class="progress progress-small">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">100%</div>
                                    </div>
                                    <small class="text-muted">John Doe, 21 Sep 2014 /</small><small class="text-success"> Done</small>
                                </a> -->
                            </div>
                            <div class="panel-footer text-center">
                                <a href="<?php echo $sitepathParentNotifications; ?>">Show all notifications</a>
                            </div>                            
                        </div>
                    </li>
                    <!-- END TASKS -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->