<div id="topperComparison" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close"  data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Exam Categories</h3>
			</div>
			<div class="modal-body">
                <div class="row">
                    <section class="panel">
                            <header class="panel-heading">
                                Percentage of all students
                            </header>
                            <div class="panel-body">
                                <div id="hero-bar3" class="graph"></div>
                            </div>
                    </section>
                </div>
				<div class="row">
                    <div class="col-lg-6">
                        <section class="panel">
                            <header class="panel-heading">
                                Score
                            </header>
                            <div class="panel-body">
                                <div id="hero-bar1" class="graph"></div>
                            </div>
                        </section>
                    </div>
                    <div class="col-lg-6">
                        <section class="panel">
                            <header class="panel-heading">
                                Percentage
                            </header>
                            <div class="panel-body">
                                <div id="hero-bar2" class="graph"></div>
                            </div>
                        </section>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>