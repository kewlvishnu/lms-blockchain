<?php
	$global = new stdClass();
	//loading zend framework
	$libPath = "../api/Vendor";
	include_once $libPath . "/Zend/Loader/AutoloaderFactory.php";
	require_once $libPath . "/ArcaneMind/Api.php";
	Zend\Loader\AutoloaderFactory::factory(array(
			'Zend\Loader\StandardAutoloader' => array(
					'autoregister_zf' => true,
					'db' => 'Zend\Db\Sql'
			)
	));
	if ($page == 'logout') {
		unset($_SESSION["userId"]);
		unset($_SESSION["userRole"]);
		unset($_SESSION['username']);
		session_destroy();
		setcookie('mtwebLogin', "", time() - 3600, '/');
		unset($_COOKIE['mtwebLogin']);
	}
	if(isset($_COOKIE["mtwebLogin"]) && !empty($_COOKIE["mtwebLogin"])) {
		require_once '../api/Vendor/ArcaneMind/User.php';
		$cookie = $_COOKIE["mtwebLogin"];
		$res = Api::checkUserRemembered($cookie);
		$_SESSION["userId"] = $res->user['id'];
		$_SESSION["userRole"] = $res->user['roleId'];
		$_SESSION["username"] = $res->user['email'];
	}
	if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
		if(isset($_GET['pageDetailId']) && !empty($_GET['pageDetailId'])) {
			$actual_link  = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$temp_link    = explode("/", $actual_link);
			$sub_link = explode(".", $temp_link[2]);
			$final_link = str_replace($sub_link[0], 'www', $actual_link);
			$subdomain = trim($_GET['pageDetailId']);
			$res = Api::getPortfolioBySlug($subdomain);
			if($res->valid) {
				$portfolioDetails = $res;
				$portfolioName 	  = $portfolioDetails->portfolio['name'];
				$logoPath = $res->portfolio['img'];
				if(!empty($res->portfolio['favicon'])) {
					$favicon  = $res->portfolio['favicon'];
				}
			} else {
				//var_dump($final_link);
				//header('location:'.$final_link);
			}
		}
	}
	//$title = 'ArcaneMind | Online Courses | Entrance Exam Preparation';
	$description = 'Arcanemind is a platform & marketplace for online courses & entrance exam preparation. We have the best instructors and institutes in the world, we ensure that the content and questions are of the highest quality.';
	$image = 'http://dhzkq6drovqw5.cloudfront.net/fb-arcanemind.jpg';

	// setting link for logo
	$logolink = $sitepath.'parent/';
	if (isset($_SESSION["userId"]) && !empty($_SESSION["userId"])) {
		$logolink = $sitepathCourses;
	}

	if(isset($logoPath) && !empty($logoPath)) {
		$logoBlock = '<a class="navbar-brand" href="'.$logolink.'">
						<span class="logo-portfolio">
							<img src="'.$logoPath.'" alt="'.$portfolioName.'" class="img-logo" />'.$portfolioName.'
						</span>
					</a>';
	} else {
		$logoBlock = '<a class="navbar-brand" href="'.$logolink.'"><img src="'.$sitepath.'img/arcanemind-logo.png" width="200" height="53" class="img-responsive" alt="Arcanemind"></a>';
	}
	$data = new stdClass();
	$data->userId = $_SESSION['userId'];
	$data->userRole = $_SESSION['userRole'];
	$result = Api::getParentNotifications($data);
	if ($result->status == 0) {
		header('location:'.$sitepath404);
	}
	$parentNotifications = $result->parentNotifications;
	switch ($page) {
		case 'dashboard':
			break;
		default:
			# code...
			break;
	}
?>