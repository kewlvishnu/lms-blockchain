<?php
	switch ($page) {
		case 'dashboard':
			$title	= "Home | ArcaneMind | Online Courses";
			$description = 'Arcanemind is a platform & marketplace for online courses & entrance exam preparation. We have the best instructors and institutes in the world, we ensure that the content and questions are of the highest quality.';
			break;
		case 'result':
			$title	= "Packages | ArcaneMind | Online Courses";
			$description = 'Arcanemind provides various range of packages to help you get best deals of courses.';
			break;
		
		default:
			$title = 'ArcaneMind | Online Courses | Entrance Exam Preparation';
			$description = "Arcanemind is a platform & marketplace for online courses & entrance exam preparation.";
			break;
	}
	$fbTitle = $title;
?>