<?php
    require_once('../config/url.functions.php');
    require_once('html-template/header.php');
    require_once('html-template/breadcrumbs.php');
    //var_dump($_SESSION);
?>

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="container-fluid">
            <h2>Notifications</h2>
            <?php
                if (count($parentNotifications)) {
                    foreach ($parentNotifications as $key => $parentNotification) {
                        echo '<div class="alert alert-warning">'.$parentNotification['message'].'
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>';
                    }
                } else {
                    echo '<p>No new notifications</p>';
                }
            ?>
        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
<?php
    require_once('html-template/footer.php');
?>