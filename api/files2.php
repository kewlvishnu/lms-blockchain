<?php
	try{
		error_reporting(0);
		/**
		* This makes our life easier when dealing with paths. Everything is relative
		* to the application root now.
		*/

		chdir(dirname(__DIR__));

		$libPath = "Vendor";
		
		// Loading Zend
		include $libPath . '/Zend/Loader/AutoloaderFactory.php';
		Zend\Loader\AutoloaderFactory::factory(array(
			'Zend\Loader\StandardAutoloader' => array(
					'autoregister_zf' => true,
					'db' => 'Zend\Db\Sql'
			)
		));
		
		require "Image.php";
		require $libPath . "/ArcaneMind/Api.php";
		$images_dir = "admin/user-data/images/"; 
		
		function correctFileExt($ext) {
			$ext = strtolower($ext);
			return ($ext == 'jpg' ||  $ext == 'jpeg' ||  $ext == 'png');
		}
		
		$time = time();
		$time = 'uid';
		$userId = 7;
		if(isset($_FILES["cover-image"])) {
			//ar_dmp($_POST);die();
			$w = intval($_POST['w']);
			$h = intval($_POST['h']);
			$x = intval($_POST['x']);
			$y = intval($_POST['y']);
			$file_parts = pathinfo($_FILES["cover-image"]["name"]);
			$ext = $file_parts['extension'];
			if( !correctFileExt($ext)) {
				echo "Error: Only JPG, JPEG, PNG files are allowed.";
			}else if ($_FILES["cover-image"]["error"] > 0) {
				echo "Error: " . $_FILES["cover-image"]["error"] . "<br>";
			} else {
				$imageName = "cover-image-{$userId}-{$time}.{$ext}";
				$imagePath = $images_dir. $imageName;
				move_uploaded_file($_FILES["cover-image"]["tmp_name"], $imagePath);
				$image = new Image($images_dir. $imageName);
				$image->resize(815, null, true);
				$image->crop($w, $h, $x, $y, null);
				if($ext == 'png')
					$image->save( $imagePath, 5);
				else
					$image->save( $imagePath);
				$res = Api::saveUserImage($imageName, 'cover',$userId);
				echo $res->message;
			}
		} else if(isset($_FILES["profile-image"])) {
			$w = $_POST['w'];
			$h = $_POST['h'];
			$x = $_POST['x'];
			$y = $_POST['y'];
			$file_parts = pathinfo($_FILES["profile-image"]["name"]);
			$ext = $file_parts['extension'];
			if( !correctFileExt($ext)) {
				echo "Error: Only JPG, JPEG, PNG files are allowed.";
			}else if ($_FILES["profile-image"]["error"] > 0) {
				echo "Error: " . $_FILES["profile-image"]["error"] . "<br>";
			} else {
				$imageName = "profile-image-{$userId}-{$time}.{$ext}";
				$imagePath = $images_dir. $imageName;
				move_uploaded_file($_FILES["profile-image"]["tmp_name"], $imagePath);
				$image = new Image($images_dir. $imageName);
				$image->resize(815, null, true);
				$image->crop($w, $h, $x, $y, null);
				if($ext == 'png')
					$image->save( $imagePath, 5);
				else
					$image->save( $imagePath);
				$res = Api::saveUserImage($imageName, 'profile',$userId);
				echo $res->message;
			}
		}
	}catch(Exception $ex){
		
	}
?>