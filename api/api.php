<?php
	session_start();
	var_dump($_SESSION);
	
	die();
	error_reporting(E_ALL);
  ini_set('display_errors', 'On');
/**
	* This makes our life easier when dealing with paths. Everything is relative
	* to the application root now.
	*/
	chdir(dirname(__DIR__));

	$libPath = "Vendor";

	include $libPath . '/Zend/Loader/AutoloaderFactory.php';
	Zend\Loader\AutoloaderFactory::factory(array(
			'Zend\Loader\StandardAutoloader' => array(
					'autoregister_zf' => true,
					'db' => 'Zend\Db\Sql'
			)
	));
	
	require $libPath . "/ArcaneMind/Institute.php";
	
	$i = new Institute();
	var_dump('Hello');
	$data = new stdClass();
	$id = isset($_GET['id']) ? $_GET['id'] : 0;
	$data->email = "myemail1{$id}@email.com";
	$data->username = "myUsername";
	$data->name = "Institute";
	$data->hashedPassword = sha1("myPassword");
	$data->createdAt = time();
	
	$data->ownerName = "Test";
	$data->contactMobile			= "9999999999";
	$data->contactLandline		= "0100000000";
	$data->addressCountryId		= 1;
	$data->addressStreet			=	"7, Race course";
	$data->addressCity				= "Delhi";
	$data->addressState				=	"Delhi";
	$data->addressPin					= "201009";
	$data->userInstituteTypes 			= array(1, 2, 3);
	
	$userInstituteSubtype = new stdClass();
	$userInstituteSubtype 						= new stdClass();
	$userInstituteSubtype->id 				= 1;
	$userInstituteSubtype->data			= "{'Classes': '1-12'}";
	$data->userInstituteSubtypes 	= array($userInstituteSubtype);
	
	$newInstituteSubtype 						= new stdClass();
	$newInstituteSubtype->name 			= "Delhi Board-{$id}";
	$newInstituteSubtype->parentId 	= 1;
	$newInstituteSubtype->data 	= "{'Classes': '1-12'}";
	
	$data->newInstituteSubtypes[] 	= $newInstituteSubtype;
	
	var_dump($data);
	$r = $i->register($data);
	
	var_dump($r);
	//var_dump($r->exception->getTrace());