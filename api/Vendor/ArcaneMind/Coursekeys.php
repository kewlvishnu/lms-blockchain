<?php

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "Base.php";

class Coursekeys extends Base {

	//public $default_key_rate;
	//public $institue_key_rate;
	//function for loading data of key details such as total_key purchased, active keys etc. , on user end
	public function loadkeydetails($data) {
		try {
			if (isset($data->instituteId)) {
				$data->userId = $data->instituteId;
			}
			$result = new stdClass();
			$key_rate = $this->fetch_key_rate($data->userId);
			$result->key_rate = $key_rate->key_rate;
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('course_key_details');
			$select->columns(array('key_rate', 'date' => new \Zend\Db\Sql\Expression("DATE_FORMAT(purchase_date,'%d %b %Y')"),
				'time' => new \Zend\Db\Sql\Expression("TIME_FORMAT(purchase_date,'%r')"),
				'keys' => new \Zend\Db\Sql\Expression('COUNT(key_id)')));
			$select->group(array('date' => new \Zend\Db\Sql\Expression('purchase_date')));
			$select->order('purchase_date DESC');
			$select->where->equalTo('userId', $data->userId);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courses = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			$select = $sql->select();
			$select->from('course_key_details');
			/*$select->columns(array('active_key' => new \Zend\Db\Sql\Expression("COUNT(CASE WHEN active_date NOT LIKE '0000-00-00 00:00:00' THEN 0  end )"),
				'total_keys' => new \Zend\Db\Sql\Expression('COUNT(key_id)')));*/
			$select->columns(array('active_key' => new \Zend\Db\Sql\Expression("COUNT(CASE WHEN active_date IS NOT NULL AND active_date NOT LIKE '0000-00-00 00:00:00' THEN 0 end )"),
				'total_keys' => new \Zend\Db\Sql\Expression('COUNT(key_id)')));
			$select->where->equalTo('userId', $data->userId);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$key_consume = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
			$result->key_consume = $key_consume->toArray();
			if (count($result->courses) == 0) {
				$result->status = 0;
				$result->message = " No keys Found ";
				return $result;
			}
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	// for purchasing the course key from user end .
	// function is also used for adding keys to institute.
	public function purchase_key($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			global $siteBase;
			if (isset($data->key_user)) {
				$data->userId = $data->key_user;
			}
			$key_rate = $this->fetch_key_rate($data->userId);
			$result->key_rate = $key_rate->key_rate;

			$rate = $result->key_rate[0]['key_rate'];
			
			$adapter = $this->adapter;
			$insert = "INSERT INTO course_key_details (userId,key_rate) VALUES";
			for ($i = 0; $i < $data->keys; $i++) {
				$insert .= "('{$data->userId}','{$rate}'),";
			}
			$insert = substr($insert, 0, strlen($insert) - 1);
			//var_dump($insert);
			$adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);
			
			if(isset($data->key_user)){
				$query = "SELECT l.email,u.contactMobile, 
				CASE 
					WHEN ur.roleId= 1 THEN (SELECT name from institute_details where userId= l.id)
					WHEN ur.roleId= 2 THEN (SELECT CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
					WHEN ur.roleId= 3 THEN (SELECT CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
					END AS institute
				from login_details l 
				INNER JOIN user_details u on u.userId=l.id 
				INNER JOIN user_roles  ur on ur.userId=l.id   where l.id=$data->userId limit 1 ";
				$institute = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$institute = $institute->toArray();
				$instituteId= $data->userId;
				$institutename = $institute[0]['institute'];
				$institutemail = $institute[0]['email'];
				$contactMobile = $institute[0]['contactMobile'];			
				
				//get keys avialable;
				require_once 'StudentInvitation.php';
				$k=new StudentInvitation();
				$keys=$k->count_courseKeys($data);
				$keys=$keys->keys;
				$keysInital=$keys - $data->keys;
				
				// insert notification
				$notification=" $data->keys course keys have been added to your account. The total no of course keys are $keys. ";
				$query = "INSERT into notifications (userId,message) values ($data->userId,'$notification')";
				$notification=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				
				require_once "User.php";
				$c = new User();


				$instituteSubject = "Addition of Course Ids";
				$emailInstitute = "Dear $institutename, <br> <br>";
				$emailInstitute .= "$data->keys Course Keys have been added to your account. <br> <br>";

				$emailInstitute .= " You now have total $keys Course Keys in your institute Account. To purchase more keys click <a href='".$siteBase."admin/CourseKey.php'>here</a> or send us an email at <a href='mailto:sales@integro.io' >sales@integro.io <a><br> <br> ";
					  

				$emailInstitute .= "<table border=1 ><thead><tr><th>Institute ID</th> <th>Keys Purchased</th> <th>Initial Count of Keys </th><th>Total Keys</th></tr></thead>";
				$emailInstitute .= "<tbody><tr><td>$instituteId</td> <td>$data->keys</td><td> $keysInital </td><td> $keys</td></tr></tbody></table> ";
				
				$c->sendMail($institutemail, $instituteSubject, $emailInstitute);
			
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function keysPurchaseInvitation($data) {
		try {
			$result = new stdClass();
			$adapter = $this->adapter;
			$table = '';
			$columns = '';
			switch ($data->userRole) {
				case 1: $table = 'institute_details';
					$columns = 'name';
					break;
				case 2: $table = 'professor_details';
					$columns = "CONCAT(firstname, ' ', lastname) as name";
					break;
				case 3: $table = 'publisher_details';
					$columns = "CONCAT(firstname, ' ', lastname) as name";
					break;
			}
			$query="SELECT l.email,u.contactMobile,$columns
					FROM login_details l
					JOIN user_details u ON u.userId=l.id
					JOIN $table i ON i.userId=l.id
					WHERE l.id=$data->userId ";
			$email = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$temp = $email->toArray();
			$emailid=$temp[0]['email'];
			$contactMobile=$temp[0]['contactMobile'];
			$name=$temp[0]['name'];
			if($data->email!='' && !(is_numeric($data->email))){
				$emailid=$data->email;
			}
		   if($data->email!='' && (is_numeric ($data->email))){
				$contactMobile=$data->email;
			}
			$key_rate = $this->fetch_key_rate($data->userId);
			$result->key_rate = $key_rate->key_rate;

			$rate = $result->key_rate[0]['key_rate'];
			
			$admin_email='contactus@integro.io';
			$admin_subject="Course Key Purchase Request";
			$admin_email_body=" $name  with  Institute ID $data->userId   has made a request to purchase  $data->keys keys. <br><br>"
					. "The contact details of the institute are :\n Mobile No / Phone :$contactMobile       Email: $emailid ";
		  
			$admin_email_body .= "<table border=1 ><thead><tr><th> Institute Name</th> <th>Institute ID</th> <th>Registered Mobile / Phone No</th> <th> Additional Contact Details ( if any)</th><th>Rate / Course key</th></tr></thead>";
			$admin_email_body .= "<tbody><tr><td> $name</td><td>$data->userId</td> <td>$contactMobile</td><td> $contactMobile</td><td> $rate</td></tr></tbody></table> ";

			
			$institute_email_subject="Course Key Purchase Request ";
			$institute_email_body="Dear $name, <br><br> ";
			$institute_email_body.="Thank you for your Interest in Purchasing $data->keys Course Keys  on <a href=integro.io >Integro.io <a> . <br><br> Our Sales Team will soon get in touch with you."
					. " Please send us an email at <a href=sales@integro.io >sales@integro.io <a>  , if you have any queries.<br><br> ";
			$institute_email_body.=" For your reference, your institute ID is $data->userId ";
		   
			
			$result->status = 1;
			$result->message = "Request sent for purchasing $data->keys keys ";
	 
			require_once "User.php";
			$c = new User();
			$c->sendMail($admin_email, $admin_subject, $admin_email_body);
			$c->sendMail($emailid, $institute_email_subject, $institute_email_body);
		   
			return $result ;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	// function for loading data on back-end page Institute wise key details
	public function backendCoursekeys($data) {
		//var_dump($data);
		$result = new stdClass();
		$result->default_rate = $this->fetch_default_rate();

		$adapter = $this->adapter;
		$query = "SELECT userId ,roleId
					FROM `user_roles` u
					INNER JOIN login_details l on l.id=u.userId
					WHERE roleId NOT IN (4,5)";
		$users = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
		$temp = $users->toArray();
		$key_details = array();
		foreach ($temp as $user) {
			$tempUserKeys  = $this->fetch_all_users_keys($user['userId'], $user['roleId']);
			$key_details[] = $tempUserKeys;
		}
		$result->key_details = $key_details;
		return $result;
	}

		
	//function for loading data on freetier Institute wise key details
	public function freetier($data) {
		//var_dump($data);
		$result = new stdClass();
		
		$adapter = $this->adapter;
		$query = "SELECT userId ,roleId FROM `user_roles` u join login_details l on l.id=u.userId where roleId NOT IN (4,5)";
		$users = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
		$temp = $users->toArray();
		$instiArray=array();
		$institiIds=array();
		foreach ($temp as $user) {
			
			$roleId=$user['roleId'];
			$userId=$user['userId'];
			$columns = "CONCAT(firstName,' ',lastName) as name,a.userId";
			// this switch case is used to join table according to the user.
			// Here we change table name according to the role id of user.

			switch ($roleId) {
				case 1:$table = 'institute_details';
					$columns = "name as name,a.userId";
					break;
				case 2:$table = 'professor_details';
					break;
				case 3:$table = 'publisher_details';
					break;
			}
			$query = "SELECT distinct $columns FROM $table a where a.userId=$userId ";
			$InstituteDetails = $adapter->query("$query", $adapter::QUERY_MODE_EXECUTE);
			$InstituteDetails = $InstituteDetails->toArray();
			$InstituteDetails[0]['role']=$roleId;
			//print_r($InstituteDetails[0]);
			array_push($instiArray,$InstituteDetails[0]);
			array_push($institiIds,$InstituteDetails[0]['userId']);
			//$temp['InstituteDetails']=$InstituteDetails;
			}
			//print_r($instiArray);	
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('free_tier');
			$where = new \Zend\Db\Sql\Where();
			$where->equalTo('Active', 1);
			$select->where($where);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$freetier = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$freetier=$freetier->toArray();
			//getting values from free_tier table
			//print_r($freetier);
			foreach($freetier as $key=>$value)
			{
				//print_r($value);
				
				if(in_array($value['instituteId'], $institiIds))
					$key1 = array_search($value['instituteId'], $institiIds);				
					$instiArray[$key1]['freeTierInfo']=$value;
				
			}
		$result->users = $instiArray;
		return $result;
	}

	//function to save edited free tier component
	public function savefreetier($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('free_tier');
			$where = new \Zend\Db\Sql\Where();
			$where->equalTo('instituteId',$data->instiId);
			$select->where($where);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$freetier1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$freetier1=$freetier1->toArray();
			
			if(count($freetier1[0]) >0)
			{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('free_tier');
				$update->set(array('free_tiereligibility'		=>	$data->freeeligibility,
									'examlevelCheck'			=>	$data->elevelcheck,
									'examlevelCost'				=>	$data->elevelprice,
									'courselevelCheck' 			=> 	$data->clevelcheck,
									'courselevelCost'			=>	$data->clevelprice,
									'Active'					=>	1));
				$update->where(array('instituteId'		=>	$data->instiId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
			}else{
				$freetier = new TableGateway('free_tier', $adapter, null,new HydratingResultSet());
				$insert = array(
							'instituteId'				=>	$data->instiId,
							'free_tiereligibility'		=>	$data->freeeligibility,
							'examlevelCheck'			=>	$data->elevelcheck,
							'examlevelCost'				=>	$data->elevelprice,
							'courselevelCheck' 			=> 	$data->clevelcheck,
							'courselevelCost'			=>	$data->clevelprice,
							'Active'					=>	1
					
					
							);
				$freetier->insert($insert);
				$data->freetierid = $freetier->getLastInsertValue();
				$result->freetierid = $data->freetierid;
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();			
			
			$result->status = 1;				
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	
	// function give user key details according to user id for the back end page. 
	public function fetch_all_users_keys($userId, $roleId) {
		$result = new stdClass();
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('user_role_details');
		$select->columns(array('name'));
		$select->where->equalTo('id', $roleId);
		$selectString = $sql->getSqlStringForSqlObject($select);
		$role = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$result->role = $role->toArray();
		$columns = "CONCAT(firstName,' ',lastName) as name,a.userId";
		// this switch case is used to join table according to the user.
		// Here we change table name according to the role id of user.

		switch ($roleId) {
			case 1:$table = 'institute_details';
				$columns = "name as name,a.userId";
				break;
			case 2:$table = 'professor_details';
				break;
			case 3:$table = 'publisher_details';
				break;
		}
		/*$query = "SELECT distinct $columns,count( b.key_id ) as total_key,COUNT(CASE WHEN active_date NOT LIKE '0000-00-00 00:00:00' THEN 0  end ) as active
					FROM $table as a
					LEFT JOIN course_key_details AS b ON a.userId=b.userId
					WHERE a.userId=$userId";*/
		$query = "SELECT distinct $columns,count( b.key_id ) as total_key,COUNT(CASE WHEN active_date IS NOT NULL AND active_date NOT LIKE '0000-00-00 00:00:00' THEN 0 end ) as active
					FROM $table as a
					LEFT JOIN course_key_details AS b ON a.userId=b.userId
					WHERE a.userId=$userId";
		$key_details = $adapter->query("$query", $adapter::QUERY_MODE_EXECUTE);
		$result->key_details = $key_details->toArray();
		$key_rate = $this->fetch_key_rate($userId);
		$result->key_rate = $key_rate->key_rate;
		return $result;
	}

	// function to fetch current key rate for the institute
	public function fetch_key_rate($userId) {
		$result = new stdClass();
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('course_key_rate');
		$select->columns(array('key_rate', 'is_editable'));
		$select->order('id DESC');
		$select->limit(1);
		$select->where->equalTo('userId', $userId);
		$selectString = $sql->getSqlStringForSqlObject($select);
		$key_rate = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
		$result->key_rate = $key_rate->toArray();
		return $result;
	}

	// function to change key rate for the institute
	public function change_key_rate($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql2 = new Sql($this->adapter);
			$update2 = $sql2->update();
			$update2->table('course_key_rate');
			$update2->set(array('key_rate' => $data->key_rate, 'is_editable' => 1));
			$update2->where(array('userId' => $data->key_user));
			$statement2 = $sql2->prepareStatementForSqlObject($update2);
			$result2 = $statement2->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->user_key_id = $data->key_user;
			$result->status = 1;
			$result->message = 'key rate Changed !';

			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	// function to set institute key rate back to default rate 
	public function setInstitute_rate_back($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			$result = new stdClass();
			$result->default_rate = $this->fetch_default_rate();
			$key_rate = $result->default_rate[0]['key_rate'];

			$adapter = $this->adapter;
			$sql2 = new Sql($this->adapter);
			$update2 = $sql2->update();
			$update2->table('course_key_rate');
			$update2->set(array('key_rate' => $key_rate, 'is_editable' => 0));
			$update2->where(array('userId' => $data->key_user));
			$statement2 = $sql2->prepareStatementForSqlObject($update2);
			$result2 = $statement2->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = ' Default key rate is set !';

			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	// function to  fetch default rate which is  applicable to the new coming institute
	public function fetch_default_rate() {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('course_key_default_rate');
		$select->columns(array('key_rate'));
		$select->order('id DESC');
		$select->limit(1);
		$selectString = $sql->getSqlStringForSqlObject($select);
		$key_rate = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
		$rate = $key_rate->toArray();
		return $rate;
	}

	// fnction for get total_students attached with course keys
	public function get_students_of_course($data) {
		try {
			if (isset($data->instituteId)) {
				$data->userId = $data->instituteId;
			}

			$result = new stdClass();
			$adapter = $this->adapter;
			//$query = "SELECT COUNT(user_id) as total_student,course_id,name from student_course as sc JOIN courses  on courses.id=sc.course_id JOIN  course_key_details as ck ON ck.key_id= sc.key_id WHERE  ck.userId='{$data->userId}'  AND active_date!=0  group by course_id";
			$query = "SELECT sc.course_id, c.name, SUM(sc.key_id <> 0) as key_student, sum(sc.key_id = 0) as stand_student FROM student_course as sc JOIN courses as c ON c.id=sc.course_id AND c.ownerId={$data->userId} GROUP BY course_id";
			$students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->students = $students->toArray();
			if (count($result->students) == 0) {
				$result->message = "No students attached";
				$result->status = 0;
				return $result;
			}
			$result->status = 1;
			$result->message = ' Students Details are here !';
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function get_course_student_details($data) {
		try {
			if (isset($data->instituteId)) {
				$data->userId = $data->instituteId;
			}
			$result = new stdClass();
			$adapter = $this->adapter;
			//$query = "select CONCAT( S.firstName,' ',S.lastName) as name ,L.email,u.contactMobile, K.key_id, DATE_FORMAT(K.timestamp,'%d %b %Y') as date from student_details as S Join login_details as L on L.id=S.userId  JOIN user_details as u on u.userId=L.id Join student_course as K on K.user_id=L.id where K.course_id=$data->course_id";
			if($data->keyId == true) {
				$query = "SELECT sc.user_id as userId, sc.key_id, DATE_FORMAT(sc.timestamp,'%d %b %Y') as date, CONCAT(sd.firstName, ' ', sd.lastName) as name, ld.email, ud.contactMobile FROM student_course sc JOIN student_details sd ON sd.userId=sc.user_id JOIN login_details ld ON ld.id=sc.user_id JOIN user_details ud ON ud.userId=sc.user_id WHERE course_id={$data->course_id} AND key_id!=0";
			}
			else {
				$query = "SELECT sc.user_id as userId, sc.key_id, DATE_FORMAT(sc.timestamp,'%d %b %Y') as date, CONCAT(sd.firstName, ' ', sd.lastName) as name, ld.email, ud.contactMobile FROM student_course sc JOIN student_details sd ON sd.userId=sc.user_id JOIN login_details ld ON ld.id=sc.user_id JOIN user_details ud ON ud.userId=sc.user_id WHERE course_id={$data->course_id} AND key_id=0";
			}
			$students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->students = $students->toArray();
			if (count($result->students) == 0) {
				$result->message = "No students attached";
				$result->status = 0;
				return $result;
			}
			$result->status = 1;
			$result->message = ' Students Details are here !';
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function get_students_details_of_course($data) {
		try {
			$result = new stdClass();
			$adapter = $this->adapter;
			$query="SELECT L.id, L.username, L.temp, IFNULL(utp.password,'') AS password, u.contactMobile, CONCAT( IFNULL(S.firstName,''),' ',IFNULL(S.lastName,'')) AS name,L.email,IFNULL(plg.id,'') AS parentid,IFNULL(plg.userid,'') AS userid,IFNULL(plg.passwd,'') AS passwd
					FROM login_details AS L
					INNER JOIN student_course AS K ON K.user_id=L.id
					LEFT JOIN user_details AS u ON u.userId=L.id
					LEFT JOIN student_details AS S ON L.id=S.userId
					LEFT JOIN user_temp_passwords AS utp ON utp.userId=L.id
					LEFT JOIN parent_login_short AS plg ON plg.student_id=L.id AND plg.course_id=K.course_id
					WHERE K.course_id=$data->course_id AND L.active=1";
			$students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->students = $students->toArray();
			if (count($result->students) == 0) {
				$result->message = "No students attached";
				$result->status = 0;
				return $result;
			} else {
				$noOfStudents = 0;
				foreach ($result->students as $key => $value) {
					if($value['temp'] == 0) {
						$noOfStudents++;
					}
					$result->students[$key]['passwd'] = base64_decode($result->students[$key]['passwd']);
				}
				$result->noOfStudents = $noOfStudents;
			}
			$result->status = 1;
			$result->message = ' Students Details are here !';
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	// function to  set default rate which is  applicable to the new coming institute

	public function set_default_key_rate($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$key_rate = new TableGateway('course_key_default_rate', $adapter, null, new HydratingResultSet());
			$insert = array(
				'key_rate' => $data->key_rate
			);
	
			$key_rate->insert($insert);
			  if ($key_rate->getLastInsertValue() == 0) {
				$result->status = 0;
				return $result;
			}
			$query="UPDATE course_key_rate set key_rate=$data->key_rate where is_editable=0 ;";
			$update=$adapter->query($query,$adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
		
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	//function to calculate things for course key purchase
    public function purchaseCourseKeys($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
            $savedRate = 0;
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            //fetching course rate for calculating amount
            $select = $sql->select();
            $select->from('course_key_default_rate');
            $select->order('id DESC');
            $select->limit('1');
            $selectString = $sql->getSqlStringForSqlObject($select);
            $rate = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $rate = $rate->toArray();
            $savedRate = $rate[0]['key_rate'];
            $name = '';
            $email = '';
            $number = '';
            //fetching first name of institute/professor/publisher
            $select = $sql->select();
            $select->from(array('ld'    =>  'login_details'));
            $select->where(array('ld.id'    =>  $data->userId));
            $select->columns(array(
                'id'    =>  'id',
                'email' =>  'email'
            ));
            $select->join(
                array('ud'  =>  'user_details'),
                'ud.userId=ld.id',
                array('contactMobile'   =>  'contactMobile')
            );
            if($data->userRole == 1) {
                $select->join(
                    array('id'  =>  'institute_details'),
                    'id.userId=ld.id',
                    array('name'    =>  'name')
                );
            }
            else if($data->userRole == 2) {
                $select->join(
                    array('pd'  =>  'professor_details'),
                    'pd.userId=ld.id',
                    array('name'    =>  new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"))
                );
            }
            else {
                $select->join(
                    array('pd'  =>  'publisher_details'),
                    'pd.userId=ld.id',
                    array('name'    =>  new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"))
                );
            }
			$selectString = $sql->getSqlStringForSqlObject($select);
			$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$details = $details->toArray();
			if(count($details) > 0) {
				$name = $details[0]['name'];
				if($details[0]['contactMobile'] == '')
					$details[0]['contactMobile'] = '9876543210';
				$number = $details[0]['contactMobile'];
				$email = $details[0]['email'];
			}

			$amount = $savedRate * $data->quantity;
			//generating order id
			$orderId = new TableGateway('order_details', $adapter, null, new HydratingResultSet());
			$insert = array(
				'type'				=>  1,
				'userId'			=>  $data->userId,
				'email'				=>  $email,
				'purchase_detail'	=>  $data->quantity,
				'rate'				=>  $savedRate,
				'amount'			=>  $amount,
				'status'			=>  0,
				'payUId'			=>  0,
				'IGROTxnID'			=>  0,
				'timestamp'			=>  time()
			);
			$orderId->insert($insert);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$txnid = $orderId->getLastInsertValue();

			$result->amount = $amount;
			$result->orderId = $txnid;
			if($result->amount <= 0) {
				$result->paymentSkip = 1;
				$dataSkip = array();
				$dataSkip['paymentSkip'] = $result->paymentSkip;
				$dataSkip['orderId'] = $txnid;
				$this->afterIGROPay($dataSkip);
			} else {
				$result->paymentSkip = 0;

				require_once("CryptoWallet.php");
				$cw = new CryptoWallet();
				$userAddressDB = $cw->getWalletAddress($data);
				if ($userAddressDB->status == 0) {
					$result->status = 0;
					$result->message = "No User Crypto Wallet address!";
				}

				$IGROContractDB = $cw->getIGROContractData($data);
				if ($IGROContractDB->status == 0) {
					$result->status = 0;
					$result->message = "No IGRO Contract is set!";
				}

				global $siteBase;

				$IGROTxn = new TableGateway('igro_transactions', $adapter, null, new HydratingResultSet());
				$insert = array(
					'toAddress'			=>  $IGROContractDB->ownerAddress,
					'fromAddress'		=>  $userAddressDB->wallet,
					'contractAddress'	=>  $IGROContractDB->contractAddress,
					'transactionHash'	=>  '',
					'status'			=>  'new',
					'created'			=>  date("Y-m-d H:i:s"),
					'modified'			=>  date("Y-m-d H:i:s")
				);
				$IGROTxn->insert($insert);
				$IGROTxnID = $IGROTxn->getLastInsertValue();
				$result->IGROTxnID = $IGROTxnID;

				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('order_details');
				$update->set(array(
									'IGROTxnID'	=>	$IGROTxnID
				));
				$update->where(array('orderId'		=>	$result->orderId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
			}
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	//function to be called after payU payment
	public function afterIGROPay($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        //$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			//setting any undefined variable if not returned 
			//updating the order details table
			
			if ($data['paymentSkip'] == 0) {
				$where = array('IGROTxnID'	=>	$data['id']);
				$status = $data['txn_status'];
			} else {
				$where = array('orderId' => $data['orderId']);
				$status = 1;
			}
			//if error is E000 then actually add keys to the user account
			$send = new stdClass();
			$select = $sql->select();
			$select->from('order_details');
			$select->columns(array('orderId', 'userId', 'purchase_detail'));
			$select->where($where);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$details = $details->toArray();
			if(count($details) > 0) {

				$update = $sql->update();
				$update->table('order_details');
				$update->set(array(
					'status'	=>	$status
				));
				$update->where(array('orderId'	=>	$details[0]['orderId']));
				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$send->key_user = $details[0]['userId'];
				$send->keys = $details[0]['purchase_detail'];
				$this->purchase_key($send);
			}			
			$data['date'] = date('d M Y H:i:s');
			// If all succeed, commit the transaction and all changes are committed at once.
			//$db->commit();
			$result->status = 1;
			$result->data = $data;
			return $result;
		}catch(Exception $e) {
			//$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	//function to be called after payU payment
	public function afterPayU($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			//setting any undefined variable if not returned 
            if(!isset($data['mihpayid']))
                $data['mihpayid'] = '';
            if(!isset($data['mode']))
                $data['mode'] = '';
            if(!isset($data['status']))
                $data['status'] = '';
            if(!isset($data['unmappedstatus']))
                $data['unmappedstatus'] = '';
            if(!isset($data['key']))
                $data['key'] = '';
            if(!isset($data['txnid']))
                $data['txnid'] = '';
            if(!isset($data['amount']))
                $data['amount'] = '';
            if(!isset($data['addedon']))
                $data['addedon'] = '';
            if(!isset($data['productinfo']))
                $data['productinfo'] = '';
            if(!isset($data['firstname']))
                $data['firstname'] = '';
            if(!isset($data['lastname']))
                $data['lastname'] = '';
            if(!isset($data['address1']))
                $data['address1'] = '';
            if(!isset($data['address2']))
                $data['address2'] = '';
            if(!isset($data['city']))
                $data['city'] = '';
            if(!isset($data['state']))
                $data['state'] = '';
            if(!isset($data['country']))
                $data['country'] = '';
            if(!isset($data['zipcode']))
                $data['zipcode'] = '';
            if(!isset($data['email']))
                $data['email'] = '';
            if(!isset($data['phone']))
                $data['phone'] = '';
            if(!isset($data['udf1']))
                $data['udf1'] = '';
             if(!isset($data['udf2']))
                $data['udf2'] = '';
             if(!isset($data['udf3']))
                $data['udf3'] = '';
             if(!isset($data['udf4']))
                $data['udf4'] = '';
             if(!isset($data['udf5']))
                $data['udf5'] = '';
             if(!isset($data['udf6']))
                $data['udf6'] = '';
             if(!isset($data['udf7']))
                $data['udf7'] = '';
             if(!isset($data['udf8']))
                $data['udf8'] = '';
             if(!isset($data['udf9']))
                $data['udf9'] = '';
             if(!isset($data['udf10']))
                $data['udf10'] = '';
             if(!isset($data['hash']))
                $data['hash'] = '';
             if(!isset($data['field1']))
                $data['field1'] = '';
             if(!isset($data['field2']))
                $data['field2'] = '';
             if(!isset($data['field3']))
                $data['field3'] = '';
             if(!isset($data['field4']))
                $data['field4'] = '';
             if(!isset($data['field5']))
                $data['field5'] = '';
            if(!isset($data['field6']))
                $data['field6'] = '';
            if(!isset($data['field7']))
                $data['field7'] = '';
            if(!isset($data['field8']))
                $data['field8'] = '';
            if(!isset($data['field9']))
                $data['field9'] = '';
            if(!isset($data['PG_TYPE']))
                $data['PG_TYPE'] = '';
            if(!isset($data['bank_ref_num']))
                $data['bank_ref_num'] = '';
            if(!isset($data['bankcode']))
                $data['bankcode'] = '';
            if(!isset($data['error']))
                $data['error'] = '';
            if(!isset($data['error_Message']))
                $data['error_Message'] = '';
            if(!isset($data['cardToken']))
                $data['cardToken'] = '';
            if(!isset($data['name_on_card']))
                $data['name_on_card'] = '';
            if(!isset($data['cardnum']))
                $data['cardnum'] = '';
            if(!isset($data['cardhash']))
                $data['cardhash'] = '';
            if(!isset($data['amount_split']))
                $data['amount_split'] = '';
            if(!isset($data['payuMoneyId']))
                $data['payuMoneyId'] = '';
            if(!isset($data['discount']))
                $data['discount'] = '';
            if(!isset($data['net_amount_debit']))
                $data['net_amount_debit'] = '';

			$transcation = new TableGateway('payu_details', $adapter, null, new HydratingResultSet());
			$insert = array(
				'mihpayid'			=>	$data['mihpayid'],
				'mode'				=>	$data['mode'],
				'status'			=>	$data['status'],
				'unmappedstatus'	=>	$data['unmappedstatus'],
				'key'				=>	$data['key'],
				'txnid'				=>	$data['txnid'],
				'amount'			=>	$data['amount'],
				'addedon'			=>	$data['addedon'],
				'productinfo'		=>	$data['productinfo'],
				'firstname'			=>	$data['firstname'],
				'lastname'			=>	$data['lastname'],
				'address1'			=>	$data['address1'],
				'address2'			=>	$data['address2'],
				'city'				=>	$data['city'],
				'state'				=>	$data['state'],
				'country'			=>	$data['country'],
				'zipcode'			=>	$data['zipcode'],
				'email'				=>	$data['email'],
				'phone'				=>	$data['phone'],
				'udf1'				=>	$data['udf1'],
				'udf2'				=>	$data['udf2'],
				'udf3'				=>	$data['udf3'],
				'udf4'				=>	$data['udf4'],
				'udf5'				=>	$data['udf5'],
				'udf6'				=>	$data['udf6'],
				'udf7'				=>	$data['udf7'],
				'udf8'				=>	$data['udf8'],
				'udf9'				=>	$data['udf9'],
				'udf10'				=>	$data['udf10'],
				'hash'				=>	$data['hash'],
				'field1'			=>	$data['field1'],
				'field2'			=>	$data['field2'],
				'field3'			=>	$data['field3'],
				'field4'			=>	$data['field4'],
				'field5'			=>	$data['field5'],
				'field6'			=>	$data['field6'],
				'field7'			=>	$data['field7'],
				'field8'			=>	$data['field8'],
				'field9'			=>	$data['field9'],
				'PG_TYPE'			=>	$data['PG_TYPE'],
				'bank_ref_num'		=>	$data['bank_ref_num'],
				'bankcode'			=>	$data['bankcode'],
				'error'				=>	$data['error'],
				'error_Message'		=>	$data['error_Message'],
				'cardToken'			=>	$data['cardToken'],
				'name_on_card'		=>	$data['name_on_card'],
				'cardnum'			=>	$data['cardnum'],
				'cardhash'			=>	$data['cardhash'],
				'amount_split'		=>	$data['amount_split'],
				'payuMoneyId'		=>	$data['payuMoneyId'],
				'discount'			=>	$data['discount'],
				'net_amount_debit'	=>	$data['net_amount_debit']
			);
			$transcation->insert($insert);
			$payuId = $transcation->getLastInsertValue();

			//updating the order details table
			$update = $sql->update();
			$update->table('order_details');
			$update->set(array(
				'payUId'	=>	$payuId,
				'status'	=>	$data['error']
			));
			$update->where(array('orderId'	=>	$data['txnid']));
			$statement = $sql->getSqlStringForSqlObject($update);
			$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

			//if error is E000 then actually add keys to the user account
			if($data['error'] == 'E000') {
				$send = new stdClass();
				$select = $sql->select();
				$select->from('order_details');
				$select->columns(array('orderId', 'userId', 'purchase_detail'));
				$select->where(array('orderId'	=>	$data['txnid']));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$details = $details->toArray();
				if(count($details) > 0) {
					$send->key_user = $details[0]['userId'];
					$send->keys = $details[0]['purchase_detail'];
					$this->purchase_key($send);
				}
			}
			$data['date'] = date('d M Y H:i:s');
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->data = $data;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function paypalReturn($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('order_details');
			$select->where(array('orderId' => $data->orderId));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$order = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$order = $order->toArray();
			if(count($order) != 1) {
				$result->status = 0;
				$result->message = 'Something went wrong. Your payment was not successful';
				$result->date = date('d M Y H:i:s');
				$result->userRole = $data->userRole;
				return $result;
			}
			$order = $order[0];
			$result->amount = $order['amount'];
			$result->type = $order['type'];
			$result->purchase_detail = $order['purchase_detail'];
			$result->rate = $order['rate'];
			$result->date = date('d M Y H:i:s');
			$result->userRole = $data->userRole;
			/*
			//now inserting into paypal_details for future
			$paypal = new TableGateway('paypal_details', $adapter, null, new HydratingResultSet());
			$paypal->insert(array(
				'paypalId'	=>	$data->paypalId,
				'amount'	=>	$data->amount,
				'timestamp'	=>	time()
			));
			$paypalId = $paypal->getLastInsertValue();

			//updating the value of order with paypal details mapping
			$update = $sql->update();
			$update->table('order_details');
			$update->set(array(
				'payUId'	=>	$paypalId,
				'status'	=>	'E000'
			));
			$update->where(array('orderId' => $data->orderId));
			$statement = $sql->getSqlStringForSqlObject($update);
			$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);*/

			/*$send = new stdClass();
			$send->key_user = $data->userId;
			$send->keys = $result->purchase_detail;
			$this->purchase_key($send);*/

			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			$result->date = date('d M Y H:i:s');
			$result->userRole = $data->userRole;
			return $result;
		}
	}

}
?>