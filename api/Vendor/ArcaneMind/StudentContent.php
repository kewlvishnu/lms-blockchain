<?php
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "Base.php";

class StudentContent extends Base {

	//function to get headings and content title of chapters
	public function getHeadings($data) {
		$result = new stdClass();
		try {
			require_once 'DynamoDbOps.php';
			$u = new DynamoDbOps();
			$content_data  = $u->getAllWatchedInformation($data);
			if ($content_data->status == 1) {
				$content_data = $content_data->items;
			} else {
				$content_data = array();
			}
			//var_dump($content_data);
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('c'	=>	'chapters'))
					->order('c.weight ASC, c.id ASC');
			$select->join(array('s'	=>	'subjects'),
					's.id = c.subjectId',
					array(
						'subjectName'	=>	'name'
						)
			);
			$select->where(array('c.subjectId'	=>	$data->subjectId, 'c.deleted' => 0));
			$select->columns(array('id' => 'id', 'title' => 'name'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$headings = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$headings = $headings->toArray();
			$subjectName = "";
			foreach($headings as $key=>$heading) {
				if ($key == 0) {
					$subjectName = $heading["subjectName"];
				}
				unset($headings[$key]["subjectName"]);
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('c'	=>	'content'))
				->order('weight DESC , c.id ASC')
				->join(array('f'	=>	'files'),
					'c.id = f.contentId',
					array(
						'contentType' 	=> 	'type',
						'contentMeta'	=>	'metadata'
					)
				);

				$where = new \Zend\Db\Sql\Where();
				if(isset($data->preview) && $data->preview == 1)
					$where->equalTo('c.chapterId', $heading['id']);
				else {
					$where->equalTo('c.chapterId', $heading['id']);
					$where->equalTo('c.published', 1);
				}
				if(isset($data->demo) && $data->demo == 1)
					$where->equalTo('c.demo', 1);
				$select->where($where);
				$select->columns(array('id', 'title', 'downloadable'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$content = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$content = $content->toArray();
				foreach ($content as $key1 => $value1) {
					$content[$key1]["watched"]  = 0;
					$content[$key1]["progress"] = 0;
					$contentKey = array_search($value1['id'], array_column($content_data, 'contentId'));
					if ($contentKey > -1) {
						//var_dump($content_data[$contentKey]);
						$content[$key1]["watched"]  = 1;
						$content[$key1]["progress"] = 1;
					}
				}
				if(!empty($content)) {
					$headings[$key]['content'] = $content;
				} else {
					unset($headings[$key]);
				}
			}
			
			if (count($headings)>0) {
				$result->headings = $headings;
				$result->status = 1;
			} else {
				$result->headings = array();
				$result->status = 1;
			}
			$result->subjectName = $subjectName;

			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	//function to get content data for content
	public function getContentStuff($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('files');
			$select->where(array('contentId'	=>	$data->contentId));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$content = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$content = $content->toArray();
			$result->content = $content[0];
			if($result->content['type'] != 'text' && $result->content['type'] != 'Youtube' && $result->content['type'] != 'Vimeo' && $result->content['type'] != 'quiz') {
				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1' && $_SERVER['REMOTE_ADDR'] <> '::1') {
					require_once 'amazonRead.php';
					$result->content['stuff'] = getSignedURL($result->content['stuff']);
				}
			}/* else if ($result->content['type'] == 'text') {
				//$result->content['stuff'] = htmlspecialchars($result->content['stuff']);
				var_dump($result->content['stuff']);
			}*/

			//now fetching supplementary files
			$select = $sql->select();
			$select->from('content_notes');
			$select->where(array('contentId' =>	$data->contentId,'userId' => $data->userId,'status' => 1));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$notes = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$notes = $notes->toArray();
			/*if (count($notes)>0) {
				$notes = $notes[0];
			}*/
			$result->notes = $notes;

			//now fetching supplementary files
			$select = $sql->select();
			$select->from('supplementary_files');
			$select->where(array('contentId'	=>	$data->contentId));
			$select->columns(array('id', 'fileRealName'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$supp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$supp = $supp->toArray();
			$result->supplementary = $supp;
			foreach($result->supplementary as $key => $sup) {
				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1' && $_SERVER['REMOTE_ADDR'] <> '::1') {
					require_once 'amazonRead.php';
					$result->supplementary[$key]['signedURL'] = getSignedURL($sup['fileRealName']);
				}
				$file = explode('/', $sup['fileRealName']);
				$result->supplementary[$key]['fileRealName'] = end($file);
			}
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	//function to get content data for content
	public function setContentNotes($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$now = date('Y-m-d H:i:s');
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('content_notes');
			$select->where(array('contentId' =>	$data->contentId,'userId' => $data->userId,'status' => 1));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$notes = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$notes = $notes->toArray();
			if (count($notes)>0) {
				$notes = $notes[0];
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('content_notes');
				$update->set(array('notes' => $data->notes,'modified' => $now));
				$update->where(array('id' => $notes['id']));
				$statement = $sql->prepareStatementForSqlObject($update);
				$result = $statement->execute();
			} else {
				$notes = new TableGateway('content_notes', $adapter, null,new HydratingResultSet());
				$insert = array(
					'contentId'			=> 	$data->contentId,
					'chapterId'			=> 	$data->chapterId,
					'subjectId'			=> 	$data->subjectId,
					'userId'			=> 	$data->userId,
					'notes' 			=>	$data->notes,
					'created' 			=>	$now,
					'modified' 			=>	$now
				);
				$notes->insert($insert);
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->modified = $now;
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	//function to get content data for content
	public function getContentNotes($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$now = date('Y-m-d H:i:s');
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('cn'	=>	'content_notes'));
			$select->join(array('c'	=>	'content'),
					'c.id = cn.contentId',
					array(
						'contentTitle'	=>	'title'
						)
			);

			$select->join(array('ch'	=>	'chapters'),
					'ch.id = cn.chapterId',
					array()
			);
			if (isset($data->contentId) && !empty($data->contentId)) {
				$select->where(array('cn.contentId' =>	$data->contentId,'cn.userId' => $data->userId,'cn.status' => 1,'c.published' => 1, 'ch.deleted' => 0))->order('ch.weight ASC, ch.id ASC');
			} else {
				$select->where(array('cn.subjectId' =>	$data->subjectId,'cn.userId' => $data->userId,'cn.status' => 1,'c.published' => 1, 'ch.deleted' => 0))->order('ch.weight ASC, ch.id ASC');
			}
			$selectString = $sql->getSqlStringForSqlObject($select);
			$notes = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$notes = $notes->toArray();
			/*if (count($notes) == 0) {
				$result->message = "No note found";
				$result->status = 0;	
			}*/
			$result->notes = $notes = $notes;
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
}
?>