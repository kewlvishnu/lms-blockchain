<?php
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "Base.php";

//$adapter = require "adapter.php";

class Submission extends Base {
	/*
		function: this will check if name exists for submission			
	*/
	public function checkNameForSubmission($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			if(!isset($data->examId) || empty($data->examId)) {
				$select = $sql->select();
				$select->from('submissions');
				$select->where(array(
					'name'		=>	$data->name,
					'ownerId'	=>	$data->userId
				));
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
			}
			else {
				$select = $sql->select();
				$select->from('submissions');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('name', $data->name);
				$where->equalTo('ownerId', $data->userId);
				$where->notEqualTo('id', $data->examId);
				$select->where($where);
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
			}
			$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$temp = $temp->toArray();
			if(count($temp) > 0)
				$result->available = false;
			else
				$result->available = true;
			$result->status = 1;
			return $result;
		}
		catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	/*
		function: add new submission		
	*/
	public function addSubmission($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			require_once 'Subject.php';
			$s = new Subject();
			//print_r($data->courseId);
			if($s->subjectSubjectiveExamCheck($data)) {
				$result->status = 0;
				$result->message = 'Your course has expired. You can not add more Submissions.';
				return $result;
			}
			//$exams = new TableGateway('submissions', $adapter, null,new HydratingResultSet());
			$insert = array(
				'name'				=>	$data->title,
				'ownerId'			=>	$data->userId,
				'chapterId'			=>	$data->chapterId,
				'subjectId' 		=> 	$data->subjectId,
				'startDate'			=>	$data->startDate,
				'endDate'			=>	$data->endDate,
				'attempts'			=>	$data->totalAttempts,
				'access'			=>	$data->access
			);
			//$exams->insert($insert);
			$query="INSERT INTO submissions (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
			$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$adapter->query("SET @examId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
			//$data->examId = $exams->getLastInsertValue();
			//$result->examId = $data->examId;
			$result->data=$data;
			if ($data->access == 'private') {
				$studentGroupIds = array();
				foreach ($data->students as $key => $student) {
					if (!in_array($student, $studentGroupIds)) {
						$studentGroupIds[] = $student;
					}
				}
				foreach ($studentGroupIds as $key => $studentGroupId) {
					//$exams = new TableGateway('submission_access', $adapter, null,new HydratingResultSet());
					$insert = array(
						'submissionId'		=>	$data->examId,
						'studentGroupId'	=>	$studentGroupId
					);
					//$exams->insert($insert);
					$query="INSERT INTO submission_access (`".implode("`,`", array_keys($insert))."`,`submissionId`) VALUES ('".implode("','", array_values($insert))."',@examId)";
					$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				}
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$query = "SELECT @examId as submissionID";
			$submissionID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$submissionID = $submissionID->toArray();
			$data->examId = $submissionID[0]["submissionID"];
			/*if($data->examId == 0) {
				$result->status = 0;
				$result->message = "Some error occured";
				return $result;
			}*/
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function editSubmission($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			require_once 'Subject.php';
			$s = new Subject();
			//print_r($data->courseId);
			if($s->subjectSubjectiveExamCheck($data)) {
				$result->status = 0;
				$result->message = 'Your course has expired. You can not add more Submissions.';
				return $result;
			}
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('submissions');
			$update->set(array(
								'name'				=>	$data->title,
								'chapterId'			=>  $data->chapterId,
								'startDate'			=>	$data->startDate,
								'endDate'			=>	$data->endDate,
								'attempts'			=>	$data->totalAttempts,
								'access'			=>	$data->access
			));
			$update->where(array('id'		=>	$data->examId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$statement->execute();
			
			$result->data=$data;
			if ($data->access == 'private') {
				$selectedStudentGroups = array();
				$studentGroups = $this->getSubmissionGroups($data);					
				if ($studentGroups->status == 1) {
					$selectedStudentGroups = $studentGroups->selectedStudentGroups;
				}
				$studentGroupIds = array();
				if ($data->students != null) {
					foreach ($data->students as $key => $student) {
						if (!in_array($student, $studentGroupIds)) {
							$studentGroupIds[] = $student;
						}
					}
					foreach ($studentGroupIds as $key => $studentGroupId) {
						if (in_array($studentGroupId, $selectedStudentGroups)) {
							$key = array_search($studentGroupId, $selectedStudentGroups);
							unset($selectedStudentGroups[$key]);
						} else {
							$exams = new TableGateway('submission_access', $adapter, null,new HydratingResultSet());
							$insert = array(
								'submissionId'		=>	$data->examId,
								'studentGroupId'	=>	$studentGroupId
							);
							$exams->insert($insert);
						}
					}
					if (count($selectedStudentGroups)) {
						$deleteStudentGroupIds = array_values($selectedStudentGroups);
						$sql = new Sql($adapter);
						$delete = $sql->delete();
						$delete->from('submission_access');
						$delete->where(array('studentGroupId'	=>	$deleteStudentGroupIds));
						$statement = $sql->prepareStatementForSqlObject($delete);
						$statement->execute();
					}
				} else {
					$result->status = 0;
					$result->message = "Please select at least 1 student group.";
					return $result;
				}
			} else {
				$sql = new Sql($adapter);
				$delete = $sql->delete();
				$delete->from('submission_access');
				$delete->where(array('submissionId'	=>	$data->examId));
				$statement = $sql->prepareStatementForSqlObject($delete);
				$statement->execute();
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			if($data->examId == 0) {
				$result->status = 0;
				$result->message = "Some error occured";
				return $result;
			}
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getSubmissionDetail($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$selectString ="SELECT es.id,es.name,es.chapterId,es.status,es.startDate,es.endDate,es.attempts,es.access,
									c.name AS `chapterName`,es.subjectId, s.name AS `subjectName`,
									co.id AS `courseId`, co.name AS `courseName`,
									COUNT(qs.id) AS questions, sqli.marks
							FROM `submissions` AS `es`
							LEFT JOIN `chapters` AS `c` ON es.subjectId = c.subjectId AND es.chapterId = c.id
							INNER JOIN `subjects` AS `s` ON s.id = es.subjectId
							INNER JOIN `courses` AS `co` ON co.id = s.courseId
							LEFT JOIN `submission_question_links` AS sqli ON sqli.examId=es.id
							LEFT JOIN `submission_questions` AS qs ON qs.id = sqli.questionId AND qs.status=1
							WHERE es.id = '{$data->examId}'
							GROUP BY qs.id
							ORDER BY `id` DESC";
			$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$exams = $exams->toArray();
			if(count($exams) == 0) {
				$result->status = 0;
				$result->message = "No Exam found";
				return $result;
			} else {
				$examTotalScore = 0;
				$examQuestions = 0;
				foreach ($exams as $key => $exam) {
					$examTotalScore+= $exam['questions']*$exam['marks'];
					$examQuestions += $exam['questions'];
					unset($exams[$key]['questions']);
					unset($exams[$key]['marks']);
				}
				$data->subjectId = $exams[0]['subjectId'];
				//$result->questions	= $this->getSubmissionQuestions($data);
			}
			$result->status	= 1;
			$result->exam	= $exams[0];
			$result->exam['totalScore'] = $examTotalScore;
			$result->exam['questions'] = $examQuestions;
			return $result;
		}
		catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function getSubmissions($data, $chapterId = NULL) {
		$result = new stdClass();
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('submissions');
		$where = array(
					'subjectId' => $data->subjectId
		);
		$select->where($where);
		if (!empty($chapterId)) {
			//$where['chapterId'] = $chapterId;
			$select->where("`chapterId` = $chapterId;");
		} else {
			$select->where("`chapterId` = 0");
		}

		$select->columns(array('id', 'name','startDate','endDate','attempts','status','deleted'));
		$statement = $sql->getSqlStringForSqlObject($select);
		$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
		$exams = $exams->toArray();
		$result->exams = $exams;
		$result->status = 1;
		return $result;
	}
	public function deleteSubmission($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$subject = $this->getCourseDetailsbySubmissionId($data->examId);
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('submissions');
			$update->set(array('deleted'	=>	1));
			$update->where(array('id'	=>	$data->examId,'deleted' =>0));
			$statement = $sql->prepareStatementForSqlObject($update);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$count = $statement->execute()->getAffectedRows();
			if($count >0)
			{	
				$course = $subject->subject;
				$examId= $data->examId;              
				$exam=  $course[0]['exam'];            
				$courseId = $course[0]['courseId'];
				$coursename = $course[0]['course'];
				$subjectname = $course[0]['subject'];
				$subjectId = $course[0]['subjectId'];
				$institute = $course[0]['institute'];
				$institutemail = $course[0]['institutemail'];
				$instituteId = $course[0]['instituteId'];
					  
				// insert notification
				$notification="Submission $exam ( $data->examId ) belonging to  Subject $subjectname (subject id : {$subjectId} ) which is a part of Course $coursename ( course id : $courseId ) has been deleted. Please contact admin@integro.io for restoring the exam.";
				$query = "INSERT INTO notifications (userId,message) VALUES ($instituteId,'$notification')";
				$notification=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);

				require_once "User.php";
				$c = new User();

				$instituteSubject = "Submission deletion Notification";
				$emailInstitute = "Dear $institute, <br> <br>";
				$emailInstitute .= "There has been a request for deletion of the Submission belonging to  Course  $coursename ( Course ID: $courseId ) on <a href=integro.io >Integro.io <a>  <br> <br>";

				$emailInstitute .= "Please write to us at <a href=admin@integro.io >admin@integro.io <a> if you wish to restore the Submission  or if you did not initiate the request.<br> <br> "
						. "Please mention your Institute ID, Submission Name & Submission ID in the email <br> <br>";

				$emailInstitute .= "<table border=1 ><thead><tr><th> Institute Name</th> <th>Institute ID</th> <th>Deleted Course Name</th> <th> Deleted Course Id</th><th>Deleted Submission Name</th> <th>Deleted Submission Id</th></tr></thead>";
				$emailInstitute .= "<tbody><tr><td> $institute</td><td>$instituteId</td> <td>$coursename</td><td> $courseId</td><td> $exam</td><td> $examId</td> </tr></tbody></table> ";

				$c->sendMail($institutemail, $instituteSubject, $emailInstitute);
			}
							
			$result->status = 1;
			return $result;
		}
		catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function getCourseDetailsbySubmissionId($examId) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$query	= "SELECT  e.name exam ,s.name subject,s.id subjectId,c.name course, c.id courseId, l.email institutemail,l.id instituteId,
						CASE 
						WHEN ur.roleId= 1 then (select name from institute_details where userId= l.id)
							WHEN ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
							WHEN ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
							END as institute
						FROM submissions e
						INNER JOIN subjects s on s.id=e.subjectId
						INNER JOIN courses c on c.id=s.courseId
						INNER JOIN login_details l on c.ownerId=l.id and e.ownerId=l.id
						INNER JOIN user_roles  ur on ur.userId=l.id where e.id=$examId and ur.roleId not in (4,5)";
			$subject = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->subject = $subject->toArray();

			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function restoreSubmission($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('submissions');
			$update->set(array('deleted'	=>	0));
			$update->where(array('id'	=>	$data->examId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$statement->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			return $result;
		}
		catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function copySubmission($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$query = "CALL copySubmission($data->iexamId,'$data->iname'); ";
			$copy = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			//$result->copy = $copy->toArray();
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function saveSubmissionQuestion($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			//$question = new TableGateway('submission_questions', $adapter, null,new HydratingResultSet());
			$insert = array(
				'ownerId'			=> 	$data->userId,
				'subjectId'			=> 	$data->subjectId,
				'question'			=>	$data->question,
				'description'		=>	$data->desc,
				'difficulty'		=>	((isset($data->difficulty))?$data->difficulty:0),
				'status'			=>	1,
				'deleted'			=>	0
			);
			//$question->insert($insert);
			//$result->questionId = $question->getLastInsertValue();
			$query="INSERT INTO submission_questions (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
			$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$adapter->query("SET @questionId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
			
			/*if($result->questionId == 0) {
				$result->status = 0;
				$result->message = "Some error occured";
				return $result;
			}*/

			if (isset($data->examId)) {
				//$questionLink = new TableGateway('submission_question_links', $adapter, null,new HydratingResultSet());
				$insert = array(
								'examId' => $data->examId,
								//'questionId' => $result->questionId,
								'marks' => $data->marks
							);
				//$questionLink->insert($insert);
				$query="INSERT INTO submission_question_links (`".implode("`,`", array_keys($insert))."`,`questionId`) VALUES ('".implode("','", array_values($insert))."',@questionId)";
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			}

			$reqTags = array();
			if (!empty($data->tags)) {
				$reqTags = $data->tags;
				foreach ($data->tags as $key => $tag) {
					$query="SELECT *
							FROM `question_tags`
							WHERE `tag`= '$tag'";
					$tagCheck = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$tagCheck = $tagCheck->toArray();
					if (count($tagCheck)) {
						$tagId = $tagCheck[0]['id'];
						//$questionTags = new TableGateway('submission_question_tag_links', $adapter, null,new HydratingResultSet());
						$insert = array(
										//'question_id' => $result->questionId,
										'tag_id' => $tagId
									);
						//$questionTags->insert($insert);
						$query="INSERT INTO submission_question_links (`".implode("`,`", array_keys($insert))."`,`question_id`) VALUES ('".implode("','", array_values($insert))."',@questionId)";
						$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					} else {
						//$questionTags = new TableGateway('question_tags', $adapter, null,new HydratingResultSet());
						$insert = array(
										'tag'	=>  $tag
									);
						//$questionTags->insert($insert);
						//$tagId = $questionTags->getLastInsertValue();
						//var_dump('2: '.$tagId);
						$query="INSERT INTO question_tags (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
						$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$adapter->query("SET @tagId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
						/*$questionTags = new TableGateway('submission_question_tag_links', $adapter, null,new HydratingResultSet());
						$insert = array(
										'question_id' => $result->questionId,
										'tag_id' => $tagId
									);
						$questionTags->insert($insert);*/
						$query="INSERT INTO submission_question_tag_links (`question_id`,`tag_id`) VALUES (@questionId,@tagId)";
						$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					}
				}
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$query = "SELECT @questionId as questionID";
			$questionID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$questionID = $questionID->toArray();
			$result->questionId = $questionID[0]["questionID"];
			$query="SELECT sqtl.id, qt.tag
					FROM `submission_question_tag_links` AS sqtl
					INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
					WHERE sqtl.question_id={$result->questionId}";
			$tagsInDb = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$tagsInDb = $tagsInDb->toArray();
			if (count($tagsInDb)>0){
				if(!empty($reqTags)) {
					$deleteTagIds = array();
					foreach ($tagsInDb as $key => $tag) {
						if (in_array($tag['tag'], $reqTags)) {
							unset($tagsInDb[$key]);
						} else {
							$deleteTagIds[] = $tag['id'];
						}
					}
					if (!empty($deleteTagIds)) {
						$sql = new Sql($adapter);
						$delete = $sql->delete();
						$delete->from('question_tag_links');
						$delete->where(array('id'	=>	$deleteTagIds));
						$statement = $sql->prepareStatementForSqlObject($delete);
						$statement->execute();
					}
				} else {
					$sql = new Sql($adapter);
					$delete = $sql->delete();
					$delete->from('question_tag_links');
					$delete->where(array('question_id'	=>	$result->questionId));
					$statement = $sql->prepareStatementForSqlObject($delete);
					$statement->execute();
				}
			}
			
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function editSubmissionQuestion($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('submission_questions');
			$update->set(array(
								'question'		=>	$data->question,
								'description'	=>	$data->desc,
								'difficulty'	=>	((isset($data->difficulty))?$data->difficulty:0)
						));
			$update->where(array('id' => $data->questionId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$statement->execute();

			if (isset($data->examId)) {
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('submission_question_links');
				$update->set(array('marks'	=>	$data->marks));
				$update->where(array('questionId'	=>	$data->questionId , 'examId' => $data->examId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$count = $statement->execute()->getAffectedRows();
			}

			$reqTags = array();
			if (!empty($data->tags)) {
				$reqTags = $data->tags;
				foreach ($data->tags as $key => $tag) {
					$query="SELECT *
							FROM `question_tags`
							WHERE `tag`= '$tag'";
					$tagCheck = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$tagCheck = $tagCheck->toArray();
					if (count($tagCheck)) {
						$tagId = $tagCheck[0]['id'];
						$query="SELECT *
								FROM `submission_question_tag_links`
								WHERE `question_id`={$data->questionId} AND `tag_id`= '$tagId'";
						$tagValue = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$tagValue = $tagValue->toArray();
						if (count($tagValue)) {
							unset($data->tags[$key]);
						} else {
							$questionTags = new TableGateway('submission_question_tag_links', $adapter, null,new HydratingResultSet());
							$insert = array(
											'question_id' => $data->questionId,
											'tag_id' => $tagId
										);
							$questionTags->insert($insert);
						}
					} else {
						$questionTags = new TableGateway('question_tags', $adapter, null,new HydratingResultSet());
						$insert = array(
										'tag'	=>  $tag
									);
						$tagId = $questionTags->insert($insert);
						$questionTags = new TableGateway('submission_question_tag_links', $adapter, null,new HydratingResultSet());
						$insert = array(
										'question_id' => $data->questionId,
										'tag_id' => $tagId
									);
						$questionTags->insert($insert);
					}
				}
			}
			$query="SELECT sqtl.id, qt.tag
					FROM `submission_question_tag_links` AS sqtl
					INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
					WHERE sqtl.question_id= '$data->questionId'";
			$tagsInDb = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$tagsInDb = $tagsInDb->toArray();
			if (count($tagsInDb)>0){
				if(!empty($reqTags)) {
					$deleteTagIds = array();
					foreach ($tagsInDb as $key => $tag) {
						if (in_array($tag['tag'], $reqTags)) {
							unset($tagsInDb[$key]);
						} else {
							$deleteTagIds[] = $tag['id'];
						}
					}
					if (!empty($deleteTagIds)) {
						$delete = $sql->delete();
						$delete->from('submission_question_tag_links');
						$delete->where(array('id'	=>	$deleteTagIds));
						$statement = $sql->prepareStatementForSqlObject($delete);
						$statement->execute();
					}
				} else {
					$delete = $sql->delete();
					$delete->from('submission_question_tag_links');
					$delete->where(array('question_id'	=>	$data->questionId));
					$statement = $sql->prepareStatementForSqlObject($delete);
					$statement->execute();
				}
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$sql = new Sql($adapter);
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function deleteSubmissionQuestion($data) {
		$count=0;

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			if (isset($data->examId)) {
				$delete = $sql->delete();
				$delete->from('submission_question_links');
				$delete->where(array('questionId'	=>	$data->questionId , 'examId' => $data->examId));
				$statement = $sql->prepareStatementForSqlObject($delete);
				$statement->execute();
			} else {
				$update = $sql->update();
				$update->table('submission_questions');
				$update->set(array('deleted'	=>	1));
				$update->where(array('id'	=>	$data->questionId , 'deleted' => 0));
				$statement = $sql->prepareStatementForSqlObject($update);
				$count = $statement->execute()->getAffectedRows();
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			return $result;
		}
		catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getSubjectSubmissionQuestions($data) {
		$result =new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('s'=>'subjects'));
			$select->join(array('c' => 'courses'), 'c.id = s.courseId', array('courseName'=>'name'));
			$select->where(array('s.id' => $data->subjectId,'s.deleted'=>0));
			$statement = $sql->prepareStatementForSqlObject($select);
			$subjects = $statement->execute();
			if($subjects->count() == 0) {
				$result->status = 0;
				$result->message = "Subject not found";
				return $result;
			}
			$result->subjectDetails = $subjects->next();
			$result->questions = 0;
			
			// questions fetch based on questionType
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('q'	=>	'submission_questions'));
			$select->where(array('subjectId' => $data->subjectId, 'ownerId' => $data->userId, 'deleted'=>0));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->questions = $questions->toArray();

			$result->status  = 1;
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getSubmissionQuestions($data) {
		$result =new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('s'=>'submissions'));
			$select->where(array('s.id' => $data->examId,'s.deleted'=>0));
			$statement = $sql->prepareStatementForSqlObject($select);
			$submissions = $statement->execute();
			if($submissions->count() == 0) {
				$result->status = 0;
				$result->message = "Submission not found";
				return $result;
			}
			$result->exam = $submissions->next();
			$result->questions = 0;
			
			// questions fetch based on questionType
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('s'=>'submissions'));
			$select->join(array('sql' => 'submission_question_links'), 'sql.examId = s.id', array('marks'=>'marks'));
			$select->join(array('sq' => 'submission_questions'), 'sq.id = sql.questionId');
			$select->where(array('s.id' => $data->examId,'s.deleted'=>0));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->questions = $questions->toArray();
			$result->status  = 1;
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getSubmissionQuestionDetail($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			if (isset($data->examId)) {
				$select->from(array('sql' => 'submission_question_links'));
				$select->join(array('sq' => 'submission_questions'), 'sq.id = sql.questionId');
				$select->columns(array("marks"=>"marks"));
				$select->where(array('sq.id' => $data->questionId,'sql.examId' => $data->examId));
			} else {
				$select->from('submission_questions');
				$select->where(array('id'			=>	$data->questionId));
			}
			$selectString = $sql->getSqlStringForSqlObject($select);
			$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->question = $questions->toArray();
			
			$query="SELECT qt.id,qt.tag AS name
					FROM `submission_question_tag_links` AS qtl
					INNER JOIN `question_tags` AS qt ON qt.id=qtl.tag_id
					WHERE `question_id`={$data->questionId}";
			$tagValues = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->tags = $tagValues->toArray();

			$result->status = 1;
			return $result;
		}
		catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	
	public function getSubjectSubmissions($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('submissions');
			if($data->import == true)
				$select->where(array('subjectId'		=>	$data->subjectId));
			else
				$select->where(array('subjectId'		=>	$data->subjectId,
									'deleted'		=>	0));
			$select->where('id NOT IN ('.$data->examId.')');
			$select->columns(array('id','name'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->exams = $exams->toArray();
			if(count($exams) == 0) {
				$result->status = 0;
				$result->message = "No submissions found";
			}
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getSubmissionQuestionsImport($data) {
		$result =new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('s'=>'subjects'));
			$select->join(array('c' => 'courses'), 'c.id = s.courseId', array('courseName'=>'name'));
			$select->where(array('s.id' => $data->subjectId,'s.deleted'=>0));
			$statement = $sql->prepareStatementForSqlObject($select);
			$subjects = $statement->execute();
			if($subjects->count() == 0) {
				$result->status = 0;
				$result->message = "Subject not found";
				return $result;
			}
			$result->subjectDetails = $subjects->next();
			$result->questions = 0;
			
			// questions fetch based on questionType
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('qs' =>	'submission_questions'));
			$select->join(array('qsel' => 'submission_question_links'), 'qs.id = qsel.questionId', array());
			$select->where(array('qsel.examId' => $data->currExamId, 'qs.subjectId' => $data->subjectId, 'qs.ownerId' => $data->userId, 'qs.deleted'=>0));
			//$select->columns(array('id', 'question'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$existingQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$existingQuestions = $existingQuestions->toArray();

			$arrExistingQuestions = array();
			if (count($existingQuestions)>0) {
				foreach ($existingQuestions as $key => $q) {
					//var_dump($q);
					$arrExistingQuestions[] = $q['id'];
				}
			}

			// questions fetch based on questionType
			$where = array('q.subjectId' => $data->subjectId, 'q.ownerId' => $data->userId, 'q.deleted'=>0);
			if (isset($data->difficulty) && !empty($data->difficulty)) {
				$where['q.difficulty'] = $data->difficulty;
			}
			if (isset($data->examId) && !empty($data->examId)) {
				$where['e.id'] = $data->examId;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('q'	=>	'submission_questions'));
				$select->join(array('qsel' => 'submission_question_links'), 'q.id = qsel.questionId', array());
				$select->join(array('e' => 'submissions'), 'e.id = qsel.examId', array());
				$select->group('q.id');
			} else {
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('q'	=>	'submission_questions'));
			}
			
			if (isset($data->tags) && !empty($data->tags)) {
				$select->join(array('qtl' => 'submission_question_tag_links'), 'q.id = qtl.question_id', array());
				$select->join(array('qt' => 'question_tags'), 'qt.id = qtl.tag_id', array());

				$where['qt.tag'] = $data->tags;
			}

			if (!empty($arrExistingQuestions)) {
				$select->where('q.id NOT IN ('.implode(",", $arrExistingQuestions).')');
			}

			$select->where($where);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$questions = $questions->toArray();
			$submissionQuestions=array();
			$i			 = 0;

			foreach($questions as $key=>$subQues)
			{
				$question=array();
				$questionId = $subQues['id'];
				$questionString = $subQues['question'];

				$query="SELECT qt.id, qt.tag AS name
					FROM `submission_question_tag_links` AS sqtl
					INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
					WHERE sqtl.question_id= '$questionId'";
				$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tags = $tags->toArray();

				$i++;
				$question['id']				=$questionId;
				$question['question']		=$questionString;
				$question['questionBrief']	=$this->displayString($questionString);
				$question['difficulty']		=$subQues['difficulty'];
				$question['tags']			=$tags;
				$question['subQuestions']	=array();
				
				array_push($submissionQuestions,$question);
			}
			
			$result->questions = $submissionQuestions;

			$result->status  = 1;
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function importSubmissionQuestions($data) {
		$result = new stdClass();
		try {
			foreach($data->questions as $question) {
				$result = $this->linkSubmissionQuestion($question);
			}
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function linkSubmissionQuestion($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$selectString = "INSERT INTO submission_question_links (`questionId`, `examId`) VALUES ('{$data->questionId}', '{$data->examId}');";
			$adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function liveSubmission($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$dataCheck = new stdClass();
			$dataCheck->examId = $data->submissionId;
			$questions = $this->getSubmissionQuestions($dataCheck);
			if ($questions->status == 0) {
				$result->status = 0;
				$result->message = "Submission not found";
				return $result;
			}
			$questions = $questions->questions;
			if (count($questions) < 1) {
				$result->status = 0;
				$result->message = "No questions in the submission";
				return $result;
			}
			$qzero = 0;
			foreach ($questions as $key => $question) {
				if ($question['marks'] == 0) {
					//var_dump($question);
					$qzero++;
				}
			}
			if ($qzero > 0) {
				$result->status = 0;
				$result->message = "Marks for ".$qzero." questions are zero, please set some marks for them.";
				return $result;
			}
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->update();
			$select->table('submissions');
		    $select->set(array(
			      'status' => 2
			 ));
			$select->where(array('id'=>$data->submissionId));
			$statement = $sql->prepareStatementForSqlObject($select);
			$statement->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function unliveSubmission($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->update();
			$select->table('submissions');
			$select->set(array(
					'status' => 1
			 ));
			$select->where(array('id'=>$data->submissionId));
			$statement = $sql->prepareStatementForSqlObject($select);
			$statement->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	/*
		function: this will check if name exists for submission			
	*/
	public function attemptSubmission($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			if(!isset($data->examId) || empty($data->examId)) {
				$result->status = 0;
				$result->message = "There was some error.";
				return $result;
			}
			$select = $sql->select();
			$select->from('submissions');
			$where = new \Zend\Db\Sql\Where();
			$where->equalTo('id', $data->examId);
			$where->equalTo('deleted', 0);
			$select->where($where);
			$select->columns(array('id', 'access'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$submission = $temp->toArray();
			if (count($submission) == 0) {
				$result->status = 0;
				$result->message = "Submission not found";
				return $result;
			}
			$submission = $submission[0];
			if ($submission['access'] == 'private') {
				$selectString ="SELECT *
							FROM `submission_access` AS sa
							INNER JOIN `student_subject_groups` AS ssg ON ssg.id=sa.studentGroupId
							INNER JOIN `student_subject_group_students` AS ssgs ON ssgs.groupId=sa.studentGroupId
							WHERE sa.submissionId = '{$data->examId}' AND ssgs.studentId = '{$data->userId}'
							ORDER BY sa.id";
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				if (count($exams) == 0) {
					$result->status = 0;
					$result->message = "Submission not found";
					return $result;
				}
				$exams = $exams[0];
				$data->studentGroupId = $exams['studentGroupId'];
			}

			$select = $sql->select();
			$select->from('submission_attempts');
			$where = new \Zend\Db\Sql\Where();
			$where->equalTo('examId', $data->examId);
			if ($submission['access'] == 'private') {
				$where->equalTo('studentGroupId', $data->studentGroupId);
			} else {
				$where->equalTo('studentId', $data->userId);
			}
			$where->equalTo('completed', 0);
			$select->where($where);
			$select->columns(array('id'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$temp = $temp->toArray();
			if(count($temp) > 0) {
				$attemptId = $temp[0]['id'];
			}
			else {
				//$exams = new TableGateway('submission_attempts', $adapter, null,new HydratingResultSet());
				$insert = array(
					'examId'			=>	$data->examId,
					'startDate'			=>	time()
				);
				if ($submission['access'] == 'private') {
					$insert['studentGroupId'] = $data->studentGroupId;
				} else {
					$insert['studentId'] = $data->userId;
				}
				//$exams->insert($insert);
				//$attemptId = $exams->getLastInsertValue();
				$query="INSERT INTO submission_attempts (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$adapter->query("SET @attemptId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);

				$submissionQuestions = $this->getSubmissionQuestions($data);
				if ($submissionQuestions->status == 1) {
					$submissionQuestions = $submissionQuestions->questions;
					foreach ($submissionQuestions as $key => $submissionQuestion) {
						//$examQuestions = new TableGateway('submission_attempt_questions', $adapter, null,new HydratingResultSet());
						$insertQuestion = array(
							//'attemptId'	=>	$attemptId,
							'questionId'	=>	$submissionQuestion['id']
						);
						//$examQuestions->insert($insertQuestion);
						$query="INSERT INTO submission_attempt_questions (`".implode("`,`", array_keys($insertQuestion))."`,`attemptId`) VALUES ('".implode("','", array_values($insertQuestion))."',@attemptId)";
						$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					}
				}
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$query = "SELECT @attemptId as attemptID";
			$attemptID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$attemptID = $attemptID->toArray();
			$data->attemptId = $attemptID[0]["attemptID"];
			//$data->attemptId = $attemptId;

			$selectString ="SELECT saq.id,sq.id questionId,sq.question,sqli.marks,saa.answerText,saa.answerFile,saq.attemptId,saq.status
							FROM `submission_attempts` AS sa
							INNER JOIN `submissions` AS s ON s.id=sa.examId
							INNER JOIN `submission_attempt_questions` AS saq ON saq.attemptId=sa.id
							INNER JOIN `submission_questions` AS sq ON sq.id = saq.questionId AND sq.status=1
							INNER JOIN `submission_question_links` AS sqli ON sqli.questionId = saq.questionId AND sqli.examId=s.id
							LEFT JOIN `submission_attempt_answers` AS saa ON saa.attemptQuestionId=saq.id
							WHERE sa.id = '{$data->attemptId}'
							ORDER BY saq.id";
			$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$exams = $exams->toArray();

			$result->submission = $exams;
			$result->status = 1;
			return $result;
		}
		catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	/*
		function: this will check if name exists for submission			
	*/
	public function submissionAnswer($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			if((!isset($data->examId) || empty($data->examId))
				|| (!isset($data->attemptQuestionId) || empty($data->attemptQuestionId))) {
				$result->status = 0;
				$result->message = "There was some error.";
				return $result;
			}

			$select = $sql->select();
			$select->from('submission_attempt_answers');
			$select->where(array(
				'submissionAttemptId'	=>	$data->attemptId,
				'attemptQuestionId'		=>	$data->attemptQuestionId
			));
			$select->columns(array('id'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$temp = $temp->toArray();
			if (count($temp)>0) {
				$answerId = $temp[0]['id'];
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('submission_attempt_answers');
				$update->set(array(
									'submissionAttemptId'	=>	$data->attemptId,
									'attemptQuestionId'		=>	$data->attemptQuestionId,
									'questionId'			=>	$data->questionId,
									'answerText' 			=> 	$data->answerText,
									'answerFile'			=>	$data->answerFile
				));
				$update->where(array('id'		=>	$answerId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
			} else {
				$answer = new TableGateway('submission_attempt_answers', $adapter, null,new HydratingResultSet());
				$insert = array(
					'submissionAttemptId'	=>	$data->attemptId,
					'attemptQuestionId'		=>	$data->attemptQuestionId,
					'questionId'			=>	$data->questionId,
					'answerText' 			=> 	$data->answerText,
					'answerFile'			=>	$data->answerFile
				);
				$answer->insert($insert);
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('submission_attempt_questions');
				$update->set(array(
									'status'	=>	1
				));
				$update->where(array('id'		=>	$data->attemptQuestionId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();

			$result->status = 1;
			return $result;
		}
		catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	/*
		function: this will check if name exists for submission			
	*/
	public function submitSubmissionAttempt($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			if((!isset($data->examId) || empty($data->examId))
				|| (!isset($data->attemptId) || empty($data->attemptId))) {
				$result->status = 0;
				$result->message = "There was some error.";
				return $result;
			}

			$select = $sql->select();
			$select->from('submission_attempt_questions');
			$select->where(array(
				'attemptId'	=>	$data->attemptId
			));
			$select->columns(array('id'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$questions = $temp->toArray();
			$select = $sql->select();
			$select->from('submission_attempt_answers');
			$select->where(array(
				'submissionAttemptId'	=>	$data->attemptId
			));
			$select->columns(array('id'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$answers = $temp->toArray();
			if (count($questions) != count($answers)) {
				$result->status = 0;
				$result->message = "Please answer all questions before submission.";
				return $result;
			} else {
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('submission_attempts');
				$update->set(array(
									'endDate'			=>	time(),
									'completed'			=>	1
				));
				$update->where(array('id'		=>	$data->attemptId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();

			$result->status = 1;
			return $result;
		}
		catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	/*
		function: this will check if name exists for submission			
	*/
	public function getCheckSubmissionStudents($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$adapter = $this->adapter;
			$query = "SELECT access
						FROM `submissions`
						WHERE id={$data->examId}";
			$submissions = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$submissions = $submissions->toArray();
			if (count($submissions) == 0) {
				$result->status = 0;
				$result->message = 'There was some error.';
			}

			$access = $submissions[0]['access'];

			if ($access == 'public') {
				$submissionArray=array();
				$studentIds=array();
				$sql = new Sql($adapter);
				$query="SELECT sd.userId, concat(sd.firstName,' ',sd.lastName) AS name
						FROM `submission_attempts` AS sa
						INNER JOIN `submissions` AS s ON s.id=sa.examId
						LEFT JOIN `student_details` AS sd ON sd.userId=sa.studentId
						WHERE sa.examId={$data->examId} AND sa.completed=1 AND s.access='public'
						GROUP BY sa.studentId ORDER BY sa.studentId";
				$submissionStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$students = $submissionStudents->toArray();
				//print_r($students);
				$i = 0;
				foreach($students as $key=>$value)
				{
					$submissionArray[$i] = $value;
					$query="SELECT sa.id AS attemptId, sa.endDate, sa.startDate, sa.checked
							FROM `submission_attempts` AS sa
							WHERE sa.examId={$data->examId} AND sa.studentId={$value['userId']} AND sa.completed=1
							ORDER BY sa.studentId";
					$submissionAttempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$submissionArray[$i]["attempts"] = $submissionAttempts->toArray();
					$i++;
					//print_r($submissionArray);
				}
				//print_r($submissionArray);
				$result->students=$submissionArray;
			} else {
				$submissionArray=array();
				$studentIds=array();
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$query="SELECT ssg.id, ssg.name
						FROM `submission_attempts` AS sa
						INNER JOIN `submissions` AS s ON s.id=sa.examId
						LEFT JOIN `student_subject_groups` AS ssg ON ssg.id=sa.studentGroupId
						WHERE sa.examId={$data->examId} AND sa.completed=1 AND s.access='private'
						GROUP BY sa.studentGroupId ORDER BY sa.studentId";
				$submissionStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$groups = $submissionStudents->toArray();
				//print_r($groups);
				$i = 0;
				foreach($groups as $key=>$value)
				{
					$submissionArray[$i] = $value;
					$query="SELECT sa.id AS attemptId, sa.endDate, sa.startDate, sa.checked
							FROM `submission_attempts` AS sa
							WHERE sa.examId={$data->examId} AND sa.studentGroupId={$value['id']} AND sa.completed=1
							ORDER BY sa.studentGroupId";
					$submissionAttempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$submissionArray[$i]["attempts"] = $submissionAttempts->toArray();
					$i++;
					//print_r($submissionArray);
				}
				//print_r($submissionArray);
				$result->groups=$submissionArray;
			}
			$result->access = $access;
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	public function getCheckSubmissionQuestions($data) {
		try {
			$result = new stdClass();
			$adapter = $this->adapter;
			$query = "SELECT s.access
						FROM `submission_attempts` AS sa
						INNER JOIN `submissions` AS s ON s.id=sa.examId
						WHERE sa.completed=1 AND sa.id={$data->attemptId}
						ORDER BY sa.id";
			$submissions = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$submissions = $submissions->toArray();
			if (count($submissions) == 0) {
				$result->status = 0;
				$result->message = 'There was some error.';
			}

			$access = $submissions[0]['access'];

			if ($access == "public") {
				$query = "SELECT saq.id, sq.question, sqli.marks, IFNULL(sam.marks,0) as studentMarks, saq.checked, saa.answerText, saa.answerFile, IFNULL(saa.review,'') AS review, sa.examId, sa.studentId
						FROM `submission_attempts` AS sa
						INNER JOIN `submissions` AS s ON s.id=sa.examId
						INNER JOIN `submission_question_links` AS sqli ON sqli.examId=s.id
						INNER JOIN `submission_questions` AS sq ON sq.id=sqli.questionId
						LEFT JOIN `submission_attempt_questions` AS saq ON sa.id=saq.attemptId AND saq.questionId=sq.id
						LEFT JOIN `submission_attempt_answers` AS saa ON saa.attemptQuestionId=saq.id AND saa.status=1 AND saa.submissionAttemptid=sa.id
						LEFT JOIN `submission_attempt_marks` AS sam ON sam.attemptQuestionId=saq.id
						WHERE sa.completed=1 AND sa.id={$data->attemptId}
						ORDER BY sq.id";
				$questions = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$questions = $questions->toArray();
				if (count($questions) > 0) {
					$submissionQuestions=array();
					$i			 = 0;
					$qnoCounter  = 1;
					$data->examId = $questions[0]['examId'];
					
					//fetching attemptID of topper
					$adapter = $this->adapter;
					$select = "SELECT sa.id FROM `submission_attempts` AS sa
								INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
								WHERE sa.examId={$data->examId} AND sa.completed=1 AND sa.checked=1 AND satm.marks = (
									SELECT max(satm.marks) FROM `submission_attempts` AS sa
									INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
									WHERE sa.examId= {$data->examId} AND sa.completed =1
								) ORDER BY endDate";
					$topperAttempt = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$topperAttempt = $topperAttempt->toArray();
					if (count($topperAttempt)) {
						$topperAttempt = $topperAttempt[0]['id'];
					} else {
						$topperAttempt = 0;
					}

					foreach($questions as $key=>$question)
					{
						
						$query = "SELECT IFNULL((sam.marks),0) AS toppermarks, saq.checked
									FROM `submission_attempt_questions` as saq
									INNER JOIN `submission_attempt_answers` AS saa ON saa.attemptQuestionId=saq.id AND saa.submissionAttemptId=saq.attemptId
									INNER JOIN `submission_attempt_marks` AS sam ON sam.attemptQuestionId=saq.id
									WHERE saq.id='$question[id]' AND saq.attemptId='$topperAttempt'";
						//print_r($query);
						$toppermarkstime = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$toppermarkstime = $toppermarkstime->toArray();
						if (count($toppermarkstime)>0) {
							$toppermarks = $this->checkAttemptMarks($toppermarkstime[0]['toppermarks'],1);
						} else {
							$toppermarks = 'NA';
						}

						$uploadInfo = array();
						if (!empty($question['answerFile'])) {
							$ext = pathinfo($question['answerFile'], PATHINFO_EXTENSION);
							$uploadInfo = array(
												"path" => $question['answerFile'],
												"type" => $ext
												);
						}
						$submissionQuestions[$i]["id"]				= $question["id"];
						$submissionQuestions[$i]["qno"]				= $qnoCounter;
						$submissionQuestions[$i]["question"]		= $question['question'];
						$submissionQuestions[$i]["max_marks"]		= $question["marks"];
						/*-----------------------------MARKS----------------------------------*/
						$submissionQuestions[$i]["marks"]			= $question["studentMarks"];
						$submissionQuestions[$i]["topper_marks"]	= $toppermarks;
						$submissionQuestions[$i]["avg_marks"]		= 0;
						/*--------------------------------------------------------------------*/
						$submissionQuestions[$i]["uploads"]			= array();
						$submissionQuestions[$i]["answerText"]		= $question['answerText'];
						$submissionQuestions[$i]["answerFile"]		= $uploadInfo;
						$submissionQuestions[$i]["review"]			= $question["review"];
						$submissionQuestions[$i]["checked"]			= $question["checked"];
						
						$submissionQuestions[$i]["marks"]			= $this->checkAttemptMarks($question["studentMarks"],1);
						// average marks
						$query = "SELECT saq.id, saq.checked, AVG(sam.marks) AS avgMarks
									FROM `submission_questions` AS qs
									INNER JOIN `submission_attempt_questions` AS saq ON saq.questionId=qs.id
									INNER JOIN `submission_attempts` AS sa ON sa.id=saq.attemptId
									INNER JOIN `submission_question_links` AS qsel ON qs.id=qsel.questionId
									INNER JOIN `submission_attempt_answers` AS saa ON saa.attemptQuestionId=saq.id AND saa.submissionAttemptId=saq.attemptId
									INNER JOIN `submission_attempt_marks` AS sam ON sam.attemptQuestionId=saq.id
									WHERE qsel.examId=$question[examId] AND saq.id='$question[id]' AND qs.status=1 AND sa.checked=1 AND saq.checked=1";
						$avgResult = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$avgResult = $avgResult->toArray();
						if (count($avgResult)>0) {
							$avgMarks = round($avgResult[0]['avgMarks'],2);
						} else {
							$avgMarks = 'NA';
						}
						$submissionQuestions[$i]["avg_marks"]		= $avgMarks;

						$submissionQuestions[$i]['attempted'] = 1;

						$qnoCounter++;
						$i++;
					}
					// getting topper marks and the average marks of the exam
					$studentId = $questions[0]['studentId'];
					$query = "SELECT sa.id, sa.checked, concat(sd.firstName,' ',sd.lastName) AS name
								FROM `submission_attempts` AS sa
								INNER JOIN `login_details` AS ld ON ld.id=sa.studentId
								INNER JOIN `student_details` AS sd ON sd.userId=ld.id
								WHERE studentId={$studentId} AND examId={$data->examId} ORDER BY `id`";
					$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$attempts = $attempts->toArray();
					$attemptNo = 1;
					$name = "";
					$checked = 0;
					foreach ($attempts as $key => $value) {
						if ($value['id'] == $data->attemptId) {
							$name = $value['name'];
							$checked = $value['checked'];
							break;
						}
						$attemptNo++;
					}

					$result->student	= $name;
					$result->checked	= $checked;
					$result->attemptNo	= $attemptNo;
					$result->questions	= $submissionQuestions;
				} else {
					$result->status = 0;
					$result->message = 'There was some error.';
				}
			} else {
				$query = "SELECT saq.id, sq.question, sqli.marks, saq.checked, saa.answerText, saa.answerFile, IFNULL(saa.review,'') AS review, sa.examId, sa.studentGroupId
						FROM `submission_attempts` AS sa
						INNER JOIN `submissions` AS s ON s.id=sa.examId
						INNER JOIN `submission_question_links` AS sqli ON sqli.examId=s.id
						INNER JOIN `submission_questions` AS sq ON sq.id=sqli.questionId
						LEFT JOIN `submission_attempt_questions` AS saq ON sa.id=saq.attemptId AND saq.questionId=sq.id
						LEFT JOIN `submission_attempt_answers` AS saa ON saa.attemptQuestionId=saq.id AND saa.status=1 AND saa.submissionAttemptid=sa.id
						WHERE sa.completed=1 AND sa.id={$data->attemptId}
						ORDER BY sq.id";
				$questions = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$questions = $questions->toArray();
				if (count($questions) > 0) {
					$submissionQuestions=array();
					$i			 = 0;
					$qnoCounter  = 1;
					$data->examId = $questions[0]['examId'];
					$data->studentGroupId = $questions[0]['studentGroupId'];

					require_once 'Subject.php';
					$s = new Subject();
					$students = $s->getStudentsByStudentGroup($data);

					if ($students->status == 0) {
						$result->status = 0;
						$result->message = 'No students in the student group.';
						return $result;
					}
					$students = $students->students;

					//fetching attemptID of topper
					$adapter = $this->adapter;
					/*$select = "SELECT sa.id FROM `submission_attempts` AS sa
								INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
								WHERE sa.examId={$data->examId} AND sa.completed=1 AND sa.checked=1 AND satm.marks = (
									SELECT max(satm.marks) FROM `submission_attempts` AS sa
									INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
									WHERE sa.examId= {$data->examId} AND sa.completed=1
								) ORDER BY endDate";*/
					$select = "SELECT sa.id FROM `submission_attempts` AS sa
								INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
								WHERE sa.examId={$data->examId} AND sa.completed=1 AND sa.checked=1 AND satm.marks = (
									SELECT max(satm.marks) FROM `submission_attempts` AS sa
									INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
									WHERE sa.examId= {$data->examId} AND sa.completed=1
								) ORDER BY endDate";
					$topperAttempt = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$topperAttempt = $topperAttempt->toArray();
					if (count($topperAttempt)) {
						$topperAttempt = $topperAttempt[0]['id'];
					} else {
						$topperAttempt = 0;
					}

					foreach($questions as $key=>$question)
					{
						$studentMarks = array();
						$sti = 0;
						foreach ($students as $key => $student) {
							$studentMarks[$sti] = array(
													'studentId' => $student['studentId'],
													'name' => $student['name'],
													'marks' => 0
													);
							$select = "SELECT * FROM `submission_attempt_marks`
										WHERE attemptQuestionId=$question[id] AND studentId=$student[studentId]";
							$studAttempt = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
							$studAttempt = $studAttempt->toArray();
							if (count($studAttempt) > 0) {
								$studentMarks[$sti]['marks'] = $studAttempt[0]['marks'];
							}
							$sti++;
						}
						
						$query = "SELECT IFNULL((sam.marks),0) AS toppermarks, saq.checked
									FROM `submission_attempt_questions` as saq
									INNER JOIN `submission_attempt_answers` AS saa ON saa.attemptQuestionId=saq.id AND saa.submissionAttemptId=saq.attemptId
									INNER JOIN `submission_attempt_marks` AS sam ON sam.attemptQuestionId=saq.id
									WHERE saq.id='$question[id]' AND saq.attemptId='$topperAttempt'";
						//print_r($query);
						$toppermarkstime = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$toppermarkstime = $toppermarkstime->toArray();
						if (count($toppermarkstime)>0) {
							$toppermarks = $this->checkAttemptMarks($toppermarkstime[0]['toppermarks'],1);
						} else {
							$toppermarks = 'NA';
						}

						$uploadInfo = array();
						if (!empty($question['answerFile'])) {
							$ext = pathinfo($question['answerFile'], PATHINFO_EXTENSION);
							$uploadInfo = array(
												"path" => $question['answerFile'],
												"type" => $ext
												);
						}
						$submissionQuestions[$i]["id"]				= $question["id"];
						$submissionQuestions[$i]["qno"]				= $qnoCounter;
						$submissionQuestions[$i]["question"]		= $question['question'];
						$submissionQuestions[$i]["max_marks"]		= $question["marks"];
						/*-----------------------------MARKS----------------------------------*/
						$submissionQuestions[$i]["marks"]			= $studentMarks;
						$submissionQuestions[$i]["topper_marks"]	= $toppermarks;
						$submissionQuestions[$i]["avg_marks"]		= 0;
						/*--------------------------------------------------------------------*/
						$submissionQuestions[$i]["uploads"]			= array();
						$submissionQuestions[$i]["answerText"]		= $question['answerText'];
						$submissionQuestions[$i]["answerFile"]		= $uploadInfo;
						$submissionQuestions[$i]["review"]			= $question["review"];
						$submissionQuestions[$i]["checked"]			= $question["checked"];

						// average time
						$query = "SELECT saq.id, saq.checked, AVG(sam.marks) AS avgMarks
									FROM `submission_questions` AS qs
									INNER JOIN `submission_attempt_questions` AS saq ON saq.questionId=qs.id
									INNER JOIN `submission_attempts` AS sa ON sa.id=saq.attemptId
									INNER JOIN `submission_question_links` AS qsel ON qs.id=qsel.questionId
									INNER JOIN `submission_attempt_answers` AS saa ON saa.attemptQuestionId=saq.id AND saa.submissionAttemptId=saq.attemptId
									INNER JOIN `submission_attempt_marks` AS sam ON sam.attemptQuestionId=saq.id
									WHERE qsel.examId=$question[examId] AND saq.id='$question[id]' AND qs.status=1 AND sa.checked=1 AND saq.checked=1";
						$avgResult = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$avgResult = $avgResult->toArray();
						if (count($avgResult)>0) {
							$avgMarks = round($avgResult[0]['avgMarks'],2);
						} else {
							$avgMarks = 'NA';
						}
						$submissionQuestions[$i]["avg_marks"]		= $avgMarks;

						$submissionQuestions[$i]['attempted'] = 1;

						$qnoCounter++;
						$i++;
					}
					// getting attempt number
					$query = "SELECT sa.id, sa.checked, ssg.name
								FROM `submission_attempts` AS sa
								INNER JOIN `student_subject_groups` AS ssg ON ssg.id=sa.studentGroupId
								WHERE sa.studentGroupId={$data->studentGroupId} AND sa.examId={$data->examId}
								ORDER BY `id`";
					$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$attempts = $attempts->toArray();
					$attemptNo = 1;
					$name = "";
					$checked = 0;
					foreach ($attempts as $key => $value) {
						if ($value['id'] == $data->attemptId) {
							$name = $value['name'];
							$checked = $value['checked'];
							break;
						}
						$attemptNo++;
					}

					$result->student	= $name;
					$result->checked	= $checked;
					$result->attemptNo	= $attemptNo;
					$result->questions	= $submissionQuestions;
				} else {
					$result->status = 0;
					$result->message = 'There was some error.';
				}
			}

			$result->access = $access;
			
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function checkAttemptMarks($marks,$answers) {
		return (($answers==0)?('NA'):($marks));
	}

	public function saveReviewForSubmission($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;

			if (empty($data->questionId)) {
				$result->status = 0;
				$result->message = "There was some error.";
				return $result;
			}

			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('submission_attempt_answers');
			$update->set(array(
								'review'			=>	$data->review
			));
			$update->where(array(
						'submissionAttemptid' =>	$data->attemptId,
						'attemptQuestionId' =>	$data->questionId,
						'status'	 => 1
			));
			$statement = $sql->prepareStatementForSqlObject($update);
			$statement->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();

			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function saveMarksForSubmission($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			
			//validating max marks 
			$adapter = $this->adapter;
			
			$selectString = "SELECT sqli.marks,s.access,sa.studentId,sa.studentGroupId,sa.examId
						FROM `submission_attempts` AS sa
						INNER JOIN `submissions` AS s ON s.id=sa.examId
						INNER JOIN `submission_question_links` AS sqli ON sqli.examId=s.id
						INNER JOIN `submission_questions` AS sq ON sq.id=sqli.questionId
						WHERE sa.completed=1 AND sa.id={$data->attemptId}
						ORDER BY sq.id";
			$maks = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$maks = $maks->toArray();
			$maxMarks = $maks[0]['marks'];
			$examId = $maks[0]['examId'];
			if( $data->marks > $maxMarks) {
				$result->status = 0;
				$result->message = "Marks greater then maximum marks for this question";
				return $result;
			}

			$access = $maks[0]['access'];
			$studentId = $maks[0]['studentId'];
			$studentGroupId = $maks[0]['studentGroupId'];

			if ($access == "private") {
				$studentId = $data->studentId;
			}
			
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('submission_attempt_questions');
			$select->where(array(
						'id' =>	$data->questionId
			));
			$statement = $sql->getSqlStringForSqlObject($select);
			$submission = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			$submission = $submission->toArray();
			if(count($submission))
			{
				$id=$submission[0]['id'];
				
				//update the marks in the table for question 
				$update = $sql->update();
				$update->table('submission_attempt_questions');
				$update->set(array(
							//'marks'			=>	$data->marks,
							'checked'			=> 1
				));
				$update->where(array('id'	=>	$id));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();

				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('submission_attempt_marks');
				$select->where(array(
							'attemptId' =>	$data->attemptId,
							'attemptQuestionId' =>	$id,
							'studentId'			=>	$studentId
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$submissionMarks = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$submissionMarks = $submissionMarks->toArray();
				if (count($submissionMarks) > 0) {
					$submissionMarksId = $submissionMarks[0]['id'];
					//update the marks in the table for question 
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('submission_attempt_marks');
					$update->set(array(
								'marks'			=> $data->marks
					));
					$update->where(array('id'	=>	$submissionMarksId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
				} else {
					$exams = new TableGateway('submission_attempt_marks', $adapter, null,new HydratingResultSet());
					$insert = array(
						'attemptId' =>	$data->attemptId,
						'attemptQuestionId' =>	$id,
						'studentId'			=>	$studentId,
						'marks'			=> $data->marks
					);
					$exams->insert($insert);
				}

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('submission_attempt_total_marks');
				$select->where(array(
							'attemptId' =>	$data->attemptId,
							'studentId'			=>	$studentId
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$submissionMarks = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$submissionMarks = $submissionMarks->toArray();
				if (count($submissionMarks) > 0) {
					$submissionMarksId = $submissionMarks[0]['id'];
				} else {
					//$totalMarks = new TableGateway('submission_attempt_total_marks', $adapter, null,new HydratingResultSet());
					$insert = array(
						'attemptId' =>	$data->attemptId,
						'studentId'	=>	$studentId,
						'marks'		=>  0
					);
					//$totalMarks->insert($insert);
					//$submissionMarksId = $totalMarks->getLastInsertValue();
					$query="INSERT INTO submission_attempt_total_marks (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
					$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$adapter->query("SET @submissionMarksId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
				}
				//update the marks in the table for question
				$sql = new Sql($adapter);
				$selectString = "UPDATE submission_attempt_total_marks SET marks=(select sum(marks) from submission_attempt_marks where attemptId={$data->attemptId} AND studentId={$studentId} ) WHERE id=@submissionMarksId";
				$marks = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			} else {
				$result->status = 0;
				$result->message = "There was some error.";
				return $result;
			}

			//updating the  submission_attempts table if all questions gets checked
			$query = "SELECT IFNULL(count(id),0) AS `checked`
							FROM `submission_attempt_questions` where `attemptId`={$data->attemptId} and `checked`=1";

			$check_question = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$check_question = $check_question->toArray();
			$check_question = $check_question[0]["checked"];

			$query = "SELECT IFNULL(count(id),0) AS `total`
							FROM `submission_attempt_questions` where `attemptId`={$data->attemptId}  ";

			$total_question = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$total_question = $total_question->toArray();
			$total_question = $total_question[0]["total"];

			if($check_question == $total_question)
			{
				$query = "UPDATE `submission_attempts` SET `checked` = 1 WHERE `id` = {$data->attemptId}";
				$subjective_question = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('submission_attempt_total_marks');
				$select->where(array(
							'attemptId' => $data->attemptId,
							'studentId' => $studentId
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$exam_submission = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exam_submission = $exam_submission->toArray();

				require_once('StudentExam.php');
				$se = new StudentExam();
				$totalScore = $exam_submission[0]['marks'];


				$query = "SELECT SUM(marks) AS maxMarks
						FROM `submission_question_links`
						WHERE `examId`={$examId}";

				$submissionMaxMarks = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$submissionMaxMarks = $submissionMaxMarks->toArray();
				$maximumMarks = $submissionMaxMarks[0]["maxMarks"];

				$percentage = $totalScore/$maximumMarks * 100;
				if($percentage < 0)
					$percentage = 0;

				require 'Subject.php';
				$s = new Subject();
				$data->contentId = $examId;
				$data->contentType = 'submission';
				$data->percentage = $percentage;
				$data->studentId = $studentId;
				$resSubjectRewards = $s->putSubjectRewardsbyContent($data);
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();

			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	public function markSubmissionChecked($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$result = new stdClass();
			$adapter = $this->adapter;
			$query = "SELECT * FROM `submission_attempts` WHERE `id`={$data->attemptId}";
			//print_r($query);
			$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$attempts = $attempts->toArray();
			if (count($attempts)) {

			 	//update the marks in the table for question 
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('submission_attempt_questions');
				$update->set(array(
							'checked'			=> 1
				));
				$update->where(array('attemptId'	=>	$data->attemptId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();

				// updating total marks of the attempt
				$sql = new Sql($adapter);
				$selectString = "UPDATE submission_attempt_total_marks SET marks=(select sum(marks) from submission_attempt_marks where attemptId={$data->attemptId} ) WHERE id={$data->attemptId}";
				$marks = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

				//update the marks in the table for question 
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('submission_attempts');
				$update->set(array(
							'checked'			=> 1
				));
				$update->where(array('id'	=>	$data->attemptId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
			
				$selectString = "SELECT sa.examId,sa.studentId
							FROM `submission_attempts` AS sa
							WHERE sa.completed=1 AND sa.id={$data->attemptId}";
				$maks = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$maks = $maks->toArray();
				$examId = $maks[0]['examId'];
				$studentId = $maks[0]['studentId'];

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('submission_attempt_total_marks');
				$select->where(array(
							'attemptId' => $data->attemptId,
							'studentId' => $studentId
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$exam_submission = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exam_submission = $exam_submission->toArray();

				require_once('StudentExam.php');
				$se = new StudentExam();
				$totalScore = $exam_submission[0]['marks'];


				$query = "SELECT SUM(marks) AS maxMarks
						FROM `submission_question_links`
						WHERE `examId`={$examId}";

				$submissionMaxMarks = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$submissionMaxMarks = $submissionMaxMarks->toArray();
				$maximumMarks = $submissionMaxMarks[0]["maxMarks"];

				$percentage = $totalScore/$maximumMarks * 100;
				if($percentage < 0)
					$percentage = 0;

				require 'Subject.php';
				$s = new Subject();
				$data->contentId = $examId;
				$data->contentType = 'submission';
				$data->percentage = $percentage;
				$resSubjectRewards = $s->putSubjectRewardsbyContent($data);
				
				$result->status	 = 1;
			 } else {
				$result->status	 = 0;
				$result->message = "Invalid attempt!";
			}
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	//function to get exam attempts for graph creation
	public function getSubmissionAttemptGraph($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('submission_attempts');
			$where = array();
			if(isset($data->studentId))
				$where = array(
						'examId'	=>	$data->examId,
						'studentId'	=>	$data->studentId,
						'checked'	=>	1
						);
			else
				$where = array(
						'examId'	=>	$data->examId,
						'studentId'	=>	$data->userId,
						'checked'	=>	1
						);
			$select->where($where)->order('id ASC');
			$select->columns(array('score', 'percentage'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$graph = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$graph = $graph->toArray();
			//this is done according to ARC-333
			return $graph;
			/*$result->graph = $graph;
			$result->status = 1;
			return $result;*/
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function getSubmissionGroups($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('sa' => 'submission_access'));
			$select->where( array( 'sa.submissionId' => $data->examId ));
			$select->columns( array('studentGroupId') );
			$selectString = $sql->getSqlStringForSqlObject($select);
			$studentGroups = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$selectedStudentGroups = $studentGroups->toArray();
			$studentGroups = array();
			if (count($selectedStudentGroups)) {
				foreach ($selectedStudentGroups as $key => $selectedStudentGroup) {
					$studentGroups[] = $selectedStudentGroup['studentGroupId'];
				}
			}
			$result->selectedStudentGroups = $studentGroups;
			$result->status = 1;
			return $result;
		}
		catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function getSubmissionAccess($submissionId) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('s' => 'submissions'));
			$select->where( array( 's.id' => $submissionId ));
			$select->columns( array('access') );
			$selectString = $sql->getSqlStringForSqlObject($select);
			$submissions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$submissions = $submissions->toArray();
			if (count($submissions)) {
				$access = $submissions[0]['access'];
			} else {
				$result->status = 0;
				$result->message = "No submission found!";
				return $result;
			}
			$result->access = $access;
			$result->status = 1;
			return $result;
		}
		catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

}
?>