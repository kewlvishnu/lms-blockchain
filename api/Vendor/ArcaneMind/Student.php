<?php
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "User.php";

class Student extends User {
	public $id;
	public $role;
	
	function __construct() {
		parent::__construct();
		$this->id = 0;
		$this->role = NULL;
	}


	public function registerShort($data) {
		$result = parent::registerShort($data);
		if($result->status == 0) {
			return $result;
		}
		$data->userId = $result->userId;
		
		if(!isset($data->addressCountryId))
			$data->addressCountryId = '';
		if(!isset($data->addressStreet))
			$data->addressStreet = '';
		if(!isset($data->addressCity))
			$data->addressCity = '';
		if(!isset($data->addressState))
			$data->addressState = '';
		if(!isset($data->addressPin))
			$data->addressPin = '';
		
		$result = parent::addBasicDetails($data);
		if($result->status == 0){
			return $result;
		}
		
		$result = $this->addStudentDetails($data);
		if($result->status == 0){
			return $result;
		}
		if(($this->verifyExistingStudent($data)) || (isset($data->googleId) && !empty($data->googleId))) {
			parent::validateAccountShort($data);
			$courseStudent = 1;
		} else {
			$result = parent::sendVerificationEmail($data);
			$courseStudent = 0;
		}
		if (isset($data->courseId) && !empty($data->courseId)) {
			require_once("StudentInvitation.php");
			$si = new StudentInvitation();
			$si->process_coursekey($data);
			$si->linkStudentCourse($data,'');
		}
		if($result->status == 0){
			return $result;
		}
		$result = new stdClass();
		$result->status = 1;
		$result->userId = $data->userId;
		$result->courseStudent = $courseStudent;
		return $result;
	}

	public function register($data) {
		$result = parent::register($data);
		if($result->status == 0) {
			return $result;
		}
		$data->userId = $result->userId;
		
		if(!isset($data->addressCountryId))
			$data->addressCountryId = '';
		if(!isset($data->addressStreet))
			$data->addressStreet = '';
		if(!isset($data->addressCity))
			$data->addressCity = '';
		if(!isset($data->addressState))
			$data->addressState = '';
		if(!isset($data->addressPin))
			$data->addressPin = '';
		
		$result = parent::addBasicDetails($data);
		if($result->status == 0){
			return $result;
		}
		
		$result = $this->addStudentDetails($data);
		if($result->status == 0){
			return $result;
		}
		$result = parent::sendVerificationEmail($data);
		if($result->status == 0){
			return $result;
		}
		$result = new stdClass();
		$result->status = 1;
		$result->userId = $data->userId;
		return $result;
	}
	
	public function addStudentDetails($data, $nested = false){

		$db = $this->adapter->getDriver()->getConnection();

		if (!$nested) {
			$db->beginTransaction();
		}

		$result = new stdClass();
		try {
			$studentDetails = new TableGateway('student_details', $this->adapter, null,new HydratingResultSet());
			$studentDetails->insert(array(
				'userId' => $data->userId,
				'firstName' => $data->firstName,
				'lastName' => $data->lastName,
				'gender' => $data->gender,
				'dob' => $data->dob,
			));
			// If all succeed, commit the transaction and all changes are committed at once.
			if (!$nested) {
				$db->commit();
			}
			$result->status = 1;
			return $result;
		}catch(Exception $e){
			$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getBasicStudentDetails($data){
		$result = new stdClass();
		try{
			$adapter = $this->adapter;
			
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('student_details');
			$select->where(array('userId'	=>	$data->userId));
			$select->columns(array(
						"name" => new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"),
						'profilePic' => 'profilePic',
						'gender' => 'gender',
						'dob' => 'dob',
					));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$studentDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$studentDetails = $studentDetails->toArray();
			$studentDetails = $studentDetails[0];
			if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
				require_once 'amazonRead.php';
				$studentDetails['profilePic'] = getSignedURL($studentDetails['profilePic']);
			} else {
				$studentDetails['profilePic'] = "assets/pages/media/profile/profile_user.jpg";
			}
			$result->studentDetails = $studentDetails;
			$result->status = 1;
			return $result;
		}catch(Exception $e){
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	
	//get student name from database who is logged in for exam module.
	public function getStudentName($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('student_details');
			$select->where(array('userId'	=>	$data->userId));
			$select->columns(array(
						"name" => new \Zend\Db\Sql\Expression(
							"CONCAT(firstName, ' ', lastName)"
						)
					));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$name = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$name = $name->toArray();
			$result->name = $name[0]['name'];
			$result->status = 1;
			return $result;
		}
		catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	//function to get student details for profile page
	public function getStudentDetails($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('s'	=>	'student_details'))
			->join(array('l'	=>	'login_details'),
					's.userId = l.id',
					array(
						'username'	=>	'username',
						'email'		=>	'email'
						)
			);
			$select->columns(array(
							'name'			=>	new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"),
							'background'		=>	'background',
							'achievements'	=>	'achievements',
							'interests'		=>	'interests',
							'coverPic'		=>	'coverPic',
							'profilePic'	=>	'profilePic'
			));
			$select->where(array('userId'	=>	$data->userId));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$details = $details->toArray();
			if(count($details) >= 0) {
				$result->details = $details[0];
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->details['coverPic'] = getSignedURL($result->details['coverPic']);
					$result->details['profilePic'] = getSignedURL($result->details['profilePic']);
				}
			}
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('s'	=>	'student_course'))
			->join(array('c'	=>	'courses'),
					'c.id = s.course_id',
					array('id', 'name')
			);
			$select->where(array('user_id'	=>	$data->userId));
			$select->columns(array());
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$courses = $courses->toArray();
			if(count($courses) >= 0)
				$result->courses = $courses;
			$selectString = "SELECT COUNT(*) AS noti FROM `invited_student_details` WHERE email_id=(SELECT email FROM login_details WHERE id = {$data->userId}) AND `status`=0";
			$notiCount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$notiCount = $notiCount->toArray();
			$notiCount = $notiCount[0]['noti'];
			$result->notiCount = $notiCount;
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	//function to get student details for profile page
	public function getStudentDetailsNew($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('s'	=>	'student_details'))
			->join(array('l'	=>	'login_details'),
					's.userId = l.id',
					array(
						'username'	=>	'username',
						'email'		=>	'email'
						)
			)
			->join(array('u'	=>	'user_details'),
					'u.userId = l.id',
					array(
						'contactMobilePrefix'=>	'contactMobilePrefix',
						'contactMobile'		=>	'contactMobile',
						'country'			=>	'addressCountry',
						'street'			=>	'addressStreet',
						'city'				=>	'addressCity',
						'state'				=>	'addressState',
						'pin'				=>	'addressPin',
						'website'			=>	'website',
						'facebook'			=>	'facebook',
						'twitter'			=>	'twitter'
						)
			);
			$select->columns(array(
							'name'			=>	new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"),
							'firstName'		=> 'firstName',
							'lastName'		=> 'lastName',
							'about'			=>	'background',
							'achievements'	=>	'achievements',
							'interests'		=>	'interests',
							'dob'			=>	'dob',
							'gender'		=>	'gender',
							'profilePic'	=>	'profilePic'
			));
			$select->where(array('s.userId'	=>	$data->userId));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$details = $details->toArray();
			if(count($details) >= 0) {
				$result->details = $details[0];
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					//$result->details['coverPic'] = getSignedURL($result->details['coverPic']);
					$result->details['profilePic'] = getSignedURL($result->details['profilePic']);
				} else {
					$result->details['profilePic'] = 'assets/pages/media/profile/profile_user.jpg';
				}
				if (!empty($result->details['dob'])) {
					$dob = $result->details['dob'];
					//$dob = substr($dob, 0, strlen($dob)-3);
					$result->details['dob'] = date('d-m-Y', $dob);
				}
			}
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('s'	=>	'student_course'))
			->join(array('c'	=>	'courses'),
					'c.id = s.course_id',
					array('id', 'name', 'image')
			);
			$select->where(array('user_id'	=>	$data->userId));
			$select->columns(array());
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$courses = $courses->toArray();
			$result->courses = array();
			$result->subjects = array();
			$totalAttemptedExams = 0;
			$noOfSubjects = 0;
			$removeCourseIds;
			if(count($courses) >= 0) {
				foreach ($courses as $key => $value) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('ss'	=>	'student_subject'))
					->join(array('s'	=>	'subjects'),
							's.id = ss.subjectId',
							array('id', 'name')
					);
					$select->where(array('userId'	=>	$data->userId,'ss.courseId'	=>	$value['id'], 's.deleted'=>0,'Active'=>1));
					$select->columns(array());
					$select->group( array ("s.id") );
					$selectString = $sql->getSqlStringForSqlObject($select);
					//var_dump($selectString);
					$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$subjects = $subjects->toArray();
					$noOfSubjects+= count($subjects);
					$courses[$key]['subjects'] = array();
					$courses[$key]['exams'] = 0;
					$courses[$key]['chapters'] = 0;
					$courses[$key]['attempted_exams'] = 0;
					$courses[$key]['progress'] = 0;
					if(count($subjects) > 0) {
						$courses[$key]['subjects'] = $subjects;
						foreach ($subjects as $key1 => $value1) {
							// lectures
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from(array('c'	=>	'chapters'))
							->join(array('co'	=>	'content'),
									'c.id = co.chapterId',
									array('id')
							);
							$select->where(array('subjectId'	=>	$value1['id'], 'deleted'=>0, 'published'=>1));
							$select->columns(array('id'));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$chapters = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$chapters = $chapters->toArray();
							if(count($chapters) >= 0)
								$courses[$key]['chapters']+= count($chapters);
							// objective exams
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from(array('e'	=>	'exams'));
							$select->where(array('subjectId'	=>	$value1['id'], 'delete'=>0));
							$select->columns(array('id', 'name'));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$exams = $exams->toArray();
							if(count($exams) >= 0)
								$courses[$key]['exams']+= count($exams);
							// subjective exams
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from(array('e'	=>	'exam_subjective'));
							$select->where(array('subjectId'	=>	$value1['id'], 'Active'=>1));
							$select->columns(array('id', 'name'));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$exams = $exams->toArray();
							if(count($exams) >= 0)
								$courses[$key]['exams']+= count($exams);

							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from(array('x'	=>	'exam_attempts'))
							->join(array('e'	=>	'exams'),
									'e.id = x.examId',
									array('id', 'name')
							);
							$select->where(array('studentId'	=>	$data->userId,'subjectId'	=>	$value1['id'], 'delete'=>0));
							$select->columns(array());
							$select->group( array ("e.id") );
							$selectString = $sql->getSqlStringForSqlObject($select);
							$attemptedExams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$attemptedExams = $attemptedExams->toArray();
							if(count($attemptedExams) >= 0)
								$courses[$key]['attempted_exams']+= count($attemptedExams);
						}
						if ($courses[$key]['exams']>0) {
							$courses[$key]['progress'] = round(($courses[$key]['attempted_exams']/$courses[$key]['exams'])*100, 2);
						}
						$totalAttemptedExams+= $courses[$key]['attempted_exams'];

						if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
							require_once 'amazonRead.php';
							$courses[$key]['image'] = getSignedURL($courses[$key]['image']);
						} else {
							$courses[$key]['image'] = 'assets/pages/media/users/avatar4.jpg';
						}
					} else {
						unset($courses[$key]);
					}
				}
				$courses = array_values($courses);
				$result->courses = $courses;
				$result->subjects = $noOfSubjects;
				$result->exams = $totalAttemptedExams;
			}

			/*$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('x'	=>	'exam_attempts'))
			->join(array('e'	=>	'exams'),
					'e.id = x.examId',
					array('id', 'name')
			);
			$select->where(array('studentId'	=>	$data->userId));
			$select->columns(array());
			$selectString = $sql->getSqlStringForSqlObject($select);
			$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$exams = $exams->toArray();
			$result->exams = array();
			if(count($exams) >= 0)
				$result->exams = $exams;*/
			
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	//function to save student background from profile page
	public function saveBio($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('student_details');
			$update->set(array('background'	=>	$data->background));
			$update->where(array('userId'	=>	$data->userId));
			$statement = $sql->getSqlStringForSqlObject($update);
			$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			return $result;				
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	//function to save student achievement from profile page
	public function saveAchievement($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('student_details');
			$update->set(array('achievements'	=>	$data->achievement));
			$update->where(array('userId'	=>	$data->userId));
			$statement = $sql->getSqlStringForSqlObject($update);
			$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			return $result;				
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	//function to save student interest from profile page
	public function saveInterest($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('student_details');
			$update->set(array('interests'	=>	$data->interest));
			$update->where(array('userId'	=>	$data->userId));
			$statement = $sql->getSqlStringForSqlObject($update);
			$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			return $result;				
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	//function to save cover image
	public function saveCoverImage($imagePath, $userId) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			
			//deleting previous image from cloud
			$select = $sql->select();
			$select->from('student_details');
			$select->where(array('userId'	=>	$userId));
			$select->columns(array('coverPic'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$coverPic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$coverPic = $coverPic->toArray();
			$coverPic = $coverPic[0]['coverPic'];
			$coverPic = substr($coverPic, 37);
			if($coverPic != '') {
				require_once 'amazonDelete.php';
				deleteFile($coverPic);
			}
			
			$update = $sql->update();
			$update->table('student_details');
			$update->set(array('coverPic'	=>	$imagePath));
			$update->where(array('userId'	=>	$userId));
			$statement = $sql->getSqlStringForSqlObject($update);
			$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	//function to save profile image
	public function saveProfileImage($imagePath, $userId) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			
			//deleting previous image from cloud
			$select = $sql->select();
			$select->from('student_details');
			$select->where(array('userId'	=>	$userId));
			$select->columns(array('profilePic'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$profilePic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$profilePic = $profilePic->toArray();
			$profilePic = $profilePic[0]['profilePic'];
			$profilePic = substr($profilePic, 37);
			if($profilePic != '') {
				require_once 'amazonDelete.php';
				deleteFile($profilePic);
			}
			
			$update = $sql->update();
			$update->table('student_details');
			$update->set(array('profilePic'	=>	$imagePath));
			$update->where(array('userId'	=>	$userId));
			$statement = $sql->getSqlStringForSqlObject($update);
			$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->profilePic = $imagePath;
			if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
				require_once 'amazonRead.php';
				$result->profilePic = getSignedURL($imagePath);
			}
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	//function to save profile image
	public function removeProfileImage($userId) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			
			//deleting previous image from cloud
			$select = $sql->select();
			$select->from('student_details');
			$select->where(array('userId'	=>	$userId));
			$select->columns(array('profilePic'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$profilePic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$profilePic = $profilePic->toArray();
			$profilePic = $profilePicPath = $profilePic[0]['profilePic'];
			$profilePic = substr($profilePic, 37);
			if($profilePic != '') {
				require_once 'amazonDelete.php';
				deleteFile($profilePic);
			}
			$query = "UPDATE student_details SET profilePic = DEFAULT WHERE `userId`={$userId}";
			$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->profilePic = $profilePicPath;
			if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
				require_once 'amazonRead.php';
				$result->profilePic = getSignedURL($profilePicPath);
			}
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	//function to get student details for setting page
	public function getStudentDetailsForSetting($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('s'	=>	'student_details'))
			->join(array(
					'l'	=>	'login_details'
						),
				'l.id = s.userId',
				array(
					'email'		=>	'email',
					'username'	=>	'username',
					'id'		=>	'id'
					)
			)->join(array(
						'u'		=>	'user_details'
						),
					'u.userId = s.userId',
					array(
						'contactMobilePrefix'	=>	'contactMobilePrefix',
						'contactMobile'			=>	'contactMobile',
						'contactLandlinePrefix'	=>	'contactLandlinePrefix',
						'contactLandline'		=>	'contactLandline',
					)
			);
			$select->where(array('s.userId'	=>	$data->userId));
			$select->columns(array(
						'profilePic'	=>	'profilePic',
						'firstName'		=>	'firstName',
						'lastName'		=>	'lastName',
						'gender'		=>	'gender',
						'dob'		=>	'dob',
			));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$details = $details->toArray();
			if(count($details) > 0) {
				$result->details = $details[0];
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$result->details['profilePic'] = getSignedURL($result->details['profilePic']);
				}
			}
			$select = $sql->select();
			$select->from(array('u'	=>	'user_details'))
			->join(
				array(
					'c'	=>	'countries'
				),
				'c.id = u.addressCountry',
				array(
					'countryId'		=>	'id',
					'countryName'	=>	'name'
				)
			);
			$select->where(array(
						'userId'	=>	$data->userId
			));
			$select->columns(array());
			$selectString = $sql->getSqlStringForSqlObject($select);
			$country = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$country = $country->toArray();
			if(count($country) > 0)
				$result->country = $country[0];
			else {
				$result->country['countryId'] = 0;
				$result->country['countryName'] = 'No country selected';
			}

			$result->notiCount = $this->getNotificationCount($data);
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	//function to return the count of notifications of the user
	public function getNotificationCount($data) {
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			//notification count
			$selectString = "SELECT COUNT(*) AS noti FROM `invited_student_details` WHERE email_id=(SELECT email FROM login_details WHERE id = {$data->userId}) AND `status`=0";
			$notiCount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$notiCount = $notiCount->toArray();

			$selectString = "SELECT COUNT(*) AS noti FROM `chat_notifications` WHERE userId={$data->userId} AND `status`=0";
			$notiCount1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$notiCount1 = $notiCount1->toArray();

			$notiCount = $notiCount[0]['noti']+$notiCount1[0]['noti'];
			return $notiCount;
		}catch(Exception $e) {
			return 0;
		}
	}
	
	//function to save student details from setting page
	public function saveStudentSetting($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('student_details');
			$update->set(array(
						'firstName'	=>	$data->firstName,
						'lastName'	=>	$data->lastName,
						'dob'		=>	$data->dob
			));
			$update->where(array('userId'	=>	$data->userId));
			$statement = $sql->getSqlStringForSqlObject($update);
			$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			
			$update = $sql->update();
			$update->table('user_details');
			$update->set(array(
						'addressCountry'	=>	$data->country,
						'contactMobilePrefix'	=>	$data->mobilePrefix,
						'contactMobile'	=>	$data->mobile,
						'contactLandline'	=>	$data->landline,
						'contactLandlinePrefix'	=>	$data->landlinePrefix
			));
			$update->where(array('userId'	=>	$data->userId));
			$statement = $sql->getSqlStringForSqlObject($update);
			$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	//function to get student details for change password page
	public function getStudentDetailsForChangePassword($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('s'	=>	'student_details'))
			->join(array(
					'l'	=>	'login_details'
						),
				'l.id = s.userId',
				array(
					'username'	=>	'username'
					)
			);
			$select->where(array('s.userId'	=>	$data->userId));
			$select->columns(array(
						'profilePic'	=>	'profilePic',
						'firstName'		=>	'firstName',
						'lastName'		=>	'lastName',
			));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			//var_dump($details);
			$details = $details->toArray();
			if(count($details) > 0) {
				$result->details = $details[0];
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$result->details['profilePic'] = getSignedURL($result->details['profilePic']);
				} else {
					$result->details['profilePic'] = 'assets/layouts/layout2/img/avatar3_small.jpg';
				}
			}
			$result->notiCount = $this->getNotificationCount($data);
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	//function to get student notification details
	public function getStudentNotifications($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$selectString = "SELECT i.id, c.name AS courseName, c.image AS courseImage, ld.id AS userId, i.timestamp,
								case 
								when ur.roleId= 1 then (select name from institute_details where userId=i.inviter_id)
								when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId=i.inviter_id)
								when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId=i.inviter_id)
								end as inviter, ur.roleId 
								FROM `invited_student_details` AS i
								JOIN courses AS c ON i.course_id=c.id
								Join user_roles  ur on ur.userId=i.inviter_id
								JOIN login_details ld ON ld.id=i.inviter_id
								WHERE email_id=(SELECT email FROM login_details WHERE id={$data->userId}) AND `status`=0 AND ur.roleId NOT IN (4,5)";

			$sitenotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$sitenotifications = $sitenotifications->toArray();
			require_once 'amazonRead.php';
			foreach ($sitenotifications as $key => $value) {
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					$sitenotifications[$key]['courseImage'] = getSignedURL($value['courseImage']);
				} else {
					$sitenotifications[$key]['courseImage'] = 'assets/pages/media/users/avatar10.jpg';
				}
			}

			$selectString ="SELECT cn.*, c.name AS courseName, s.name AS subjectName, cr.room_type,
							CASE
								when ur.roleId= 1 then (select name from institute_details where userId=cn.senderId)
								when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId=cn.senderId)
								when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId=cn.senderId)
								when ur.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId=cn.senderId)
							END as senderName,
							CASE
								when ur.roleId= 1 then (select profilePic from institute_details where userId=cn.senderId)
								when ur.roleId= 2 then (select profilePic from professor_details where userId=cn.senderId)
								when ur.roleId= 3 then (select profilePic from publisher_details where userId=cn.senderId)
								when ur.roleId= 4 then (select profilePic from student_details where userId=cn.senderId)
							END as profilePic, ur.roleId 
							FROM `chat_notifications` AS cn
							INNER JOIN `chat_rooms` AS cr ON cr.id=cn.roomId
							INNER JOIN `subjects` AS s ON s.id=cr.subjectId
							INNER JOIN `courses` AS c ON c.id=cr.courseId
							INNER JOIN `login_details` AS ld ON ld.id=cn.senderId
							INNER JOIN `user_roles` AS ur on ur.userId=cn.senderId
							WHERE cn.userId={$data->userId} AND status=0";
			$chatnotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$chatnotifications = $chatnotifications->toArray();
			require_once 'amazonRead.php';
			foreach ($chatnotifications as $key => $value) {
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					$chatnotifications[$key]['profilePic'] = getSignedURL($value['profilePic']);
				} else {
					$chatnotifications[$key]['profilePic'] = 'assets/pages/media/users/avatar1.jpg';
				}
			}

			$result->sitenotifications = $sitenotifications;
			$result->chatnotifications = $chatnotifications;
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	//function to get student activity notification details
	public function getStudentNotificationsActivity($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$selectString = "SELECT COUNT(*) AS notiCount
								FROM `invited_student_details` AS i
								INNER JOIN courses AS c ON i.course_id=c.id
								INNER JOIN user_roles  ur on ur.userId=i.inviter_id
								INNER JOIN login_details ld ON ld.id=i.inviter_id
								WHERE email_id=(SELECT email FROM login_details WHERE id={$data->userId}) AND ur.roleId NOT IN (4,5)";
			$totalnotifications	= $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$totalnotifications	= $totalnotifications->toArray();
			$result->notiCount	= $totalnotifications[0]['notiCount'];

			$pageStart = ($data->pageNumber)*($data->pageLimit);
			$selectString = "SELECT i.id, c.name AS courseName, c.image AS courseImage, ld.id AS userId, i.timestamp, i.status,
								case 
								when ur.roleId= 1 then (select name from institute_details where userId=i.inviter_id)
								when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId=i.inviter_id)
								when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId=i.inviter_id)
								end as inviter, ur.roleId 
								FROM `invited_student_details` AS i
								INNER JOIN courses AS c ON i.course_id=c.id
								INNER JOIN user_roles  ur on ur.userId=i.inviter_id
								INNER JOIN login_details ld ON ld.id=i.inviter_id
								WHERE email_id=(SELECT email FROM login_details WHERE id={$data->userId}) AND ur.roleId NOT IN (4,5) LIMIT {$pageStart},{$data->pageLimit}";

			$sitenotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$sitenotifications = $sitenotifications->toArray();
			require_once 'amazonRead.php';
			foreach ($sitenotifications as $key => $value) {
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					$sitenotifications[$key]['courseImage'] = getSignedURL($value['courseImage']);
				} else {
					$sitenotifications[$key]['courseImage'] = 'assets/pages/media/users/avatar10.jpg';
				}
			}

			$result->sitenotifications = $sitenotifications;
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	//function to get student chat notification details
	public function getStudentNotificationsChat($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$selectString ="SELECT COUNT(*) AS notiCount
							FROM `chat_notifications` AS cn
							INNER JOIN `chat_rooms` AS cr ON cr.id=cn.roomId
							INNER JOIN `login_details` AS ld ON ld.id=cn.senderId
							INNER JOIN `user_roles` AS ur on ur.userId=cn.senderId
							WHERE cn.userId={$data->userId}";
			$totalnotifications	= $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$totalnotifications	= $totalnotifications->toArray();
			$result->notiCount	= $totalnotifications[0]['notiCount'];

			$pageStart = ($data->pageNumber)*($data->pageLimit);
			$selectString ="SELECT cn.*, c.name AS courseName, s.name AS subjectName, cr.room_type,
							CASE
								when ur.roleId= 1 then (select name from institute_details where userId=cn.senderId)
								when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId=cn.senderId)
								when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId=cn.senderId)
								when ur.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId=cn.senderId)
							END as senderName,
							CASE
								when ur.roleId= 1 then (select profilePic from institute_details where userId=cn.senderId)
								when ur.roleId= 2 then (select profilePic from professor_details where userId=cn.senderId)
								when ur.roleId= 3 then (select profilePic from publisher_details where userId=cn.senderId)
								when ur.roleId= 4 then (select profilePic from student_details where userId=cn.senderId)
							END as profilePic, ur.roleId 
							FROM `chat_notifications` AS cn
							INNER JOIN `chat_rooms` AS cr ON cr.id=cn.roomId
							INNER JOIN `subjects` AS s ON s.id=cr.subjectId
							INNER JOIN `courses` AS c ON c.id=cr.courseId
							INNER JOIN `login_details` AS ld ON ld.id=cn.senderId
							INNER JOIN `user_roles` AS ur on ur.userId=cn.senderId
							WHERE cn.userId={$data->userId} LIMIT {$pageStart},{$data->pageLimit}";

			$chatnotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$chatnotifications = $chatnotifications->toArray();
			require_once 'amazonRead.php';
			foreach ($chatnotifications as $key => $value) {
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					$chatnotifications[$key]['profilePic'] = getSignedURL($value['profilePic']);
				} else {
					$chatnotifications[$key]['profilePic'] = 'assets/pages/media/users/avatar1.jpg';
				}
			}

			$result->chatnotifications = $chatnotifications;
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	//
	public function getStudentCourses($data) {
		//print_r($data);
		$result = new stdClass();
		try {
			if(isset($data->pageUserId) && !empty($data->pageUserId)) {
				$adapter = $this->adapter;

				$selectString ="SELECT * FROM `pf_portfolio` WHERE `subdomain`='{$data->portfolioSlug}'";
				$portfolioDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$portfolioDetails = $portfolioDetails->toArray();
				$portfolioDetails = $portfolioDetails[0];
				$portfolioId = $portfolioDetails['id'];

				$query="SELECT sc.id,l.id userId ,c.id course_id,c.name,c.subtitle,sc.key_id,date_format(sc.timestamp,'%Y-%m-%dT%T') startDate,c.liveDate as origStartDate, c.endDate,
						CASE
							when ur.roleId= 1 then (SELECT name FROM institute_details WHERE userId=l.id)
							when ur.roleId= 2 then (SELECT CONCAT(firstname, ' ', lastname) FROM professor_details WHERE userId=l.id)
							when ur.roleId= 3 then (SELECT CONCAT(firstname, ' ', lastname) FROM publisher_details WHERE userId=l.id)
						END as inviter,
						c.image as coverPic, IF(ur.roleId=1, 'institute', 'instructor') AS inviter_role
						FROM student_course sc 
						INNER JOIN courses c ON c.id=sc.course_id
						INNER JOIN user_roles ur ON ur.userId=c.ownerId 
						INNER JOIN login_details l ON l.id=ur.userId AND ur.userId=c.ownerId
						INNER JOIN pf_main_courses AS pmc ON c.id=pmc.courseId AND pmc.portfolioId={$portfolioId}
						WHERE c.ownerId=$data->pageUserId AND sc.packId=0 AND user_id=$data->userId AND ur.roleId NOT IN (4,5)";
				//print_r($query);
				$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$courses = $courses->toArray();
				$result->courses = array();
	            foreach ($courses as $course) {
					
					/*$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('student_subject');
					$select->columns(array('id','subjectId'));
					$select->where(array('courseId' => $course['course_id'],'userId' => $data->userId,'Active'=>1));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjects1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$subjects1 = $subjects1->toArray();
					if(count($subjects1)>0)
					{
						$stringSubjectids=array();
						foreach($subjects1 as $key=>$value)
						{
							//$stringSubjectids .=.$value['subjectId'].',';
							$stringSubjectids[$key]=$value['subjectId'];
						}
					
						//rtrim($stringSubjectids, ",")
						$stringSubjectids=implode(",",$stringSubjectids);
						$stringSubjectids='('.$stringSubjectids.')';
					}
					if(count($subjects1)>0){
						$query="SELECT id subjectId, name subjectName FROM subjects WHERE courseId = $course[course_id] AND deleted=0 AND id IN $stringSubjectids";
					}else{
						$query="SELECT id subjectId, name subjectName FROM subjects WHERE courseId = $course[course_id] AND deleted=0"; 
					}

					//$query="select id subjectId, name subjectName from subjects where courseId = {$course['course_id']} and deleted=0"; 
					$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$course['subjects'] = $subjects->toArray();*/
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('ss' => 'student_subject'))
					->join(array(
							's'	=>	'subjects'
								),
							'ss.subjectId = s.id',
							array(
							'subjectId' => 'id',
							'subjectName' => 'name'
							)
					);
					$select->columns(array());
					$select->where(array('ss.courseId' => $course['course_id'],'ss.userId' => $data->userId,'ss.Active' => 1,'s.deleted' => 0));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjects = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
					$subjects = $subjects->toArray();
					//if (count($subjects)>0) {
						$course['subjects'] = $subjects;
						if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
							require_once 'amazonRead.php';
							$course['coverPic'] = getSignedURL($course['coverPic']);
							$course['profilePic'] = getSignedURL($course['profilePic']);
						}
						$result->courses[] = $course;
					//}
				}
			} else {
				$adapter = $this->adapter;

				$query="SELECT sc.id,l.id userId ,c.id course_id,c.name,c.subtitle,sc.key_id,date_format(sc.timestamp,'%Y-%m-%dT%T') startDate,c.liveDate as origStartDate, c.endDate,
						CASE
							when ur.roleId= 1 then (SELECT name FROM institute_details WHERE userId=l.id)
							when ur.roleId= 2 then (SELECT CONCAT(firstname, ' ', lastname) FROM professor_details WHERE userId=l.id)
							when ur.roleId= 3 then (SELECT CONCAT(firstname, ' ', lastname) FROM publisher_details WHERE userId=l.id)
						END as inviter,
						c.image AS coverPic, IF(ur.roleId=1, 'institute', 'instructor') AS inviter_role
						FROM student_course sc join courses c ON c.id=sc.course_id
						INNER JOIN user_roles ur ON ur.userId=c.ownerId 
						INNER JOIN login_details l ON l.id=ur.userId AND sc.packId=0 AND ur.userId=c.ownerId  WHERE user_id=$data->userId AND ur.roleId NOT IN (4,5)";
	            $courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$courses = $courses->toArray();
	            $result->courses = array();
	            foreach ($courses as $course) {
				
					/*$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('student_subject');
					$select->columns(array('id','subjectId'));
					$select->where(array('courseId' => $course['course_id'],'userId' => $data->userId,'Active'=>1));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjects1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$subjects1 = $subjects1->toArray();
					//var_dump($subjects1);
					if(count($subjects1)>0)
					{
						$stringSubjectids=array();
						foreach($subjects1 as $key=>$value)
						{
							//$stringSubjectids .=.$value['subjectId'].',';
							$stringSubjectids[$key]=$value['subjectId'];
						}
					
						//rtrim($stringSubjectids, ",")
						$stringSubjectids=implode(",",$stringSubjectids);
						$stringSubjectids='('.$stringSubjectids.')';
					}
					
					if(count($subjects1)>0){
						$query="SELECT id subjectId, name subjectName FROM subjects WHERE courseId = $course[course_id] AND deleted=0 AND id IN $stringSubjectids"; 
					}else{
						$query="SELECT id subjectId, name subjectName FROM subjects WHERE courseId = $course[course_id] AND deleted=0"; 
					}

					$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$course['subjects'] = $subjects->toArray();*/
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('ss' => 'student_subject'))
					->join(array(
							's'	=>	'subjects'
								),
							'ss.subjectId = s.id',
							array(
							'subjectId' => 'id',
							'subjectName' => 'name'
							)
					);
					$select->columns(array());
					$select->where(array('ss.courseId' => $course['course_id'],'ss.userId' => $data->userId,'ss.Active' => 1,'s.deleted' => 0));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjects = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
					$subjects = $subjects->toArray();
					//if (count($subjects)>0) {
						$course['subjects'] = $subjects;
						if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
							require_once 'amazonRead.php';
							$course['coverPic'] = getSignedURL($course['coverPic']);
							$course['profilePic'] = getSignedURL($course['profilePic']);
						}
						$result->courses[] = $course;
					//}
				}
			}
			//$result->course = $course;
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getStudentCoursesNew($data) {
		//print_r($data);
		$result = new stdClass();
		try {
			if(isset($data->pageUserId) && !empty($data->pageUserId)) {
				$adapter = $this->adapter;

				$selectString ="SELECT * FROM `pf_portfolio` WHERE `subdomain`='{$data->portfolioSlug}'";
				$portfolioDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$portfolioDetails = $portfolioDetails->toArray();
				$portfolioDetails = $portfolioDetails[0];
				$portfolioId = $portfolioDetails['id'];

				$query="SELECT sc.id,l.id userId ,c.id course_id,c.name,c.subtitle,sc.key_id,sc.timestamp as startDate, c.liveDate as origStartDate, c.endDate,
						CASE
							when ur.roleId= 1 then (SELECT name FROM institute_details WHERE userId=l.id)
							when ur.roleId= 2 then (SELECT CONCAT(firstname, ' ', lastname) FROM professor_details WHERE userId=l.id)
							when ur.roleId= 3 then (SELECT CONCAT(firstname, ' ', lastname) FROM publisher_details WHERE userId=l.id)
						END as inviter,
						CASE
							when ur.roleId= 1 then (SELECT profilePic FROM institute_details WHERE userId=l.id)
							when ur.roleId= 2 then (SELECT profilePic FROM professor_details WHERE userId=l.id)
							when ur.roleId= 3 then (SELECT profilePic FROM publisher_details WHERE userId=l.id)
						END as profilePic,
						c.image as coverPic, IF(ur.roleId=1, 'institute', 'instructor') AS inviter_role
						FROM student_course sc 
						INNER JOIN courses c ON c.id=sc.course_id
						INNER JOIN user_roles ur ON ur.userId=c.ownerId 
						INNER JOIN login_details l ON l.id=ur.userId AND ur.userId=c.ownerId
						INNER JOIN pf_main_courses AS pmc ON c.id=pmc.courseId AND pmc.portfolioId={$portfolioId}
						WHERE c.ownerId=$data->pageUserId AND c.deleted=0 AND sc.packId=0 AND user_id=$data->userId AND ur.roleId NOT IN (4,5)";
				//print_r($query);
				$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$courses = $courses->toArray();
				$result->courses = array();
	            foreach ($courses as $course) {
					
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('ss' => 'student_subject'))
					->join(array(
							's'	=>	'subjects'
								),
							'ss.subjectId = s.id',
							array(
							'subjectId' => 'id',
							'subjectName' => 'name'
							)
					);
					$select->columns(array());
					$select->where(array('ss.courseId' => $course['course_id'],'ss.userId' => $data->userId,'ss.Active' => 1,'s.deleted' => 0));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjects = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
					$subjects = $subjects->toArray();
					$course['subjects'] = $subjects;
					$course['content'] = 0;
					$course['exams'] = 0;
					if (count($subjects)>0) {
						$subjectids = array();
						foreach ($subjects as $subject) {
							$subjectids[]=$subject['subjectId'];
						}
						$subjectids = implode(",", $subjectids);
						// lectures count
						$query = "SELECT COUNT(*) AS contentCount
									FROM `content` AS co
									INNER JOIN `chapters` AS ch ON ch.id=co.chapterId
									WHERE co.published=1 AND ch.subjectId IN ($subjectids)";
						$content = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$content = $content->toArray();
						$course['content'] = $content[0]['contentCount'];
						// exams count
						$query = "SELECT COUNT(*) AS examCount
									FROM `exams` AS e
									WHERE e.delete=0 AND e.subjectId IN ($subjectids)";
						$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$exams = $exams->toArray();
						$course['exams'] = $exams[0]['examCount'];
						// subjective exams count
						$query = "SELECT COUNT(*) AS examCount
									FROM `exam_subjective` AS e
									WHERE e.Active=1 AND e.subjectId IN ($subjectids)";
						$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$exams = $exams->toArray();
						$course['exams']+= $exams[0]['examCount'];
						
						if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
							require_once 'amazonRead.php';
							$course['coverPic'] = getSignedURL($course['coverPic']);
							$course['profilePic'] = getSignedURL($course['profilePic']);
						} else {
							$course['coverPic'] = 'assets/pages/img/background/32.jpg';
							$course['profilePic'] = 'assets/pages/img/avatars/team7.jpg';
						}
						$result->courses[] = $course;
					}
				}
			} else {
				$adapter = $this->adapter;

				$query="SELECT sc.id,l.id userId ,c.id course_id,c.name,c.subtitle,sc.key_id,sc.timestamp as startDate,c.liveDate as origStartDate, c.endDate,
						CASE
							when ur.roleId= 1 then (SELECT name FROM institute_details WHERE userId=l.id)
							when ur.roleId= 2 then (SELECT CONCAT(firstname, ' ', lastname) FROM professor_details WHERE userId=l.id)
							when ur.roleId= 3 then (SELECT CONCAT(firstname, ' ', lastname) FROM publisher_details WHERE userId=l.id)
						END as inviter,
						CASE
							when ur.roleId= 1 then (SELECT profilePic FROM institute_details WHERE userId=l.id)
							when ur.roleId= 2 then (SELECT profilePic FROM professor_details WHERE userId=l.id)
							when ur.roleId= 3 then (SELECT profilePic FROM publisher_details WHERE userId=l.id)
						END as profilePic,
						c.image AS coverPic, IF(ur.roleId=1, 'institute', 'instructor') AS inviter_role
						FROM student_course sc
						INNER JOIN courses c ON c.id=sc.course_id
						INNER JOIN user_roles ur ON ur.userId=c.ownerId
						INNER JOIN login_details l ON l.id=ur.userId AND sc.packId=0 AND ur.userId=c.ownerId
						WHERE c.deleted=0 AND user_id=$data->userId AND ur.roleId NOT IN (4,5)";
				$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$courses = $courses->toArray();
				$result->courses = array();
	            foreach ($courses as $course) {
				
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('ss' => 'student_subject'))
					->join(array(
							's'	=>	'subjects'
								),
							'ss.subjectId = s.id',
							array(
							'subjectId' => 'id',
							'subjectName' => 'name'
							)
					);
					$select->columns(array());
					$select->where(array('ss.courseId' => $course['course_id'],'ss.userId' => $data->userId,'ss.Active' => 1,'s.deleted' => 0));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjects = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
					$subjects = $subjects->toArray();
					$course['subjects'] = $subjects;
					$course['content'] = 0;
					$course['exams'] = 0;
					if (count($subjects)>0) {
						$subjectids = array();
						foreach ($subjects as $subject) {
							$subjectids[]=$subject['subjectId'];
						}
						$subjectids = implode(",", $subjectids);
						// lectures count
						$query = "SELECT COUNT(*) AS contentCount
									FROM `content` AS co
									INNER JOIN `chapters` AS ch ON ch.id=co.chapterId
									WHERE co.published=1 AND ch.subjectId IN ($subjectids)";
						$content = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$content = $content->toArray();
						$course['content'] = $content[0]['contentCount'];
						// exams count
						$query = "SELECT COUNT(*) AS examCount
									FROM `exams` AS e
									WHERE e.delete=0 AND e.subjectId IN ($subjectids)";
						$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$exams = $exams->toArray();
						$course['exams'] = $exams[0]['examCount'];
						// subjective exams count
						$query = "SELECT COUNT(*) AS examCount
									FROM `exam_subjective` AS e
									WHERE e.Active=1 AND e.subjectId IN ($subjectids)";
						$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$exams = $exams->toArray();
						$course['exams']+= $exams[0]['examCount'];

						if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
							require_once 'amazonRead.php';
							$course['coverPic'] = getSignedURL($course['coverPic']);
							$course['profilePic'] = getSignedURL($course['profilePic']);
						} else {
							$course['coverPic'] = 'assets/pages/img/background/32.jpg';
							$course['profilePic'] = 'assets/pages/img/avatars/team7.jpg';
						}
						$result->courses[] = $course;
					}
				}
			}
			//$result->course = $course;
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getStudentSubsCourses($data) {
	
		$result = new stdClass();
		try {
			if(isset($data->pageUserId) && !empty($data->pageUserId)) {
				$adapter = $this->adapter;
				/*$selectString = "SELECT sc.id,ck.userId, sc.course_id,c.name, sc.key_id, date_format(sc.timestamp,'%Y-%m-%dT%T')startDate,c.endDate,
									case 
									when ur.roleId= 1 then (select name from institute_details where userId=ck.userId)
									when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname) from professor_details where userId=ck.userId)
									when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname) from publisher_details where userId=ck.userId)
									end as inviter ,c.image as coverPic 
									from student_course sc 
									join courses c on  c.id=sc.course_id
									join course_key_details ck on ck.key_id=sc.key_id
									Join user_roles  ur on ur.userId=ck.userId
									where user_id=$data->userId and ur.roleId NOT IN (4,5)";*/

				$query="SELECT sc.id,l.id userId,c.id course_id,c.name,sc.key_id,date_format(sc.timestamp,'%Y-%m-%dT%T')startDate,c.liveDate AS origStartDate,sc.courseEndDate AS endDate,
						CASE WHEN ur.roleId= 1 THEN (SELECT name FROM institute_details WHERE userId=l.id)
						WHEN ur.roleId= 2 THEN (SELECT CONCAT(firstname, ' ', lastname) FROM professor_details WHERE userId=l.id)
						WHEN ur.roleId= 3 THEN (SELECT CONCAT(firstname, ' ', lastname) FROM publisher_details WHERE userId=l.id) end AS inviter
						,c.image AS coverPic
						FROM student_course sc join courses c ON c.id=sc.course_id
						Join user_roles ur ON ur.userId=c.ownerId 
						JOIN login_details l ON l.id=ur.userId AND ur.userId=c.ownerId
						WHERE  sc.packId>0 AND sc.courseEndDate >now() AND user_id=$data->userId AND ur.roleId NOT IN (4,5)";
				//c.ownerId=$data->pageUserId and
				$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$courses = $courses->toArray();
				$result->courses = array();
				foreach ($courses as $course) {
					$query="select id subjectId, name subjectName from subjects where courseId = {$course['course_id']} and deleted=0"; 
					$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$course['subjects'] = $subjects->toArray();
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
						require_once 'amazonRead.php';
						$course['coverPic'] = getSignedURL($course['coverPic']);
					}
					$result->courses[] = $course;
				}
			} else {
				$adapter = $this->adapter;
				/*$selectString = "SELECT sc.id,ck.userId, sc.course_id,c.name, sc.key_id, date_format(sc.timestamp,'%Y-%m-%dT%T')startDate,c.endDate,
									case 
									when ur.roleId= 1 then (select name from institute_details where userId=ck.userId)
									when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname) from professor_details where userId=ck.userId)
									when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname) from publisher_details where userId=ck.userId)
									end as inviter ,c.image as coverPic 
									from student_course sc 
									join courses c on  c.id=sc.course_id
									join course_key_details ck on ck.key_id=sc.key_id
									Join user_roles  ur on ur.userId=ck.userId
									where user_id=$data->userId and ur.roleId NOT IN (4,5)";*/

				$query="SELECT sc.id,l.id userId ,c.id course_id,c.name,sc.key_id,date_format(sc.timestamp,'%Y-%m-%dT%T')startDate,c.liveDate as origStartDate, date_format(sc.courseEndDate,'%Y-%m-%dT%T') as endDate,
								case when ur.roleId= 1 then (select name from institute_details where userId=l.id)
								when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname) from professor_details where userId=l.id)
								when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname) from publisher_details where userId=l.id) end as inviter,
								c.image AS coverPic
						FROM student_course sc join courses c ON c.id=sc.course_id
						INNER JOIN user_roles ur ON ur.userId=c.ownerId 
						INNER JOIN login_details l ON l.id=ur.userId AND sc.packId>0 AND sc.courseEndDate>now() AND  ur.userId=c.ownerId WHERE user_id = $data->userId AND ur.roleId NOT IN (4,5)";

				$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$courses = $courses->toArray();
				$result->courses = array();
	            foreach ($courses as $course) {
					$query="select id subjectId, name subjectName from subjects where courseId = {$course['course_id']} and deleted=0"; 
					$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$course['subjects'] = $subjects->toArray();
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
						require_once 'amazonRead.php';
						$course['coverPic'] = getSignedURL($course['coverPic']);
					}
					$result->courses[] = $course;
				}
			}
			//$result->course = $course;
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getStudentSubsCoursesNew($data) {
	
		$result = new stdClass();
		try {
			if(isset($data->pageUserId) && !empty($data->pageUserId)) {
				$adapter = $this->adapter;

				$query="SELECT sc.id,l.id userId,c.id course_id,c.name,c.subtitle,sc.key_id,date_format(sc.timestamp,'%Y-%m-%dT%T')startDate,c.liveDate AS origStartDate,sc.courseEndDate AS endDate,
						CASE
							WHEN ur.roleId= 1 THEN (SELECT name FROM institute_details WHERE userId=l.id)
							WHEN ur.roleId= 2 THEN (SELECT CONCAT(firstname, ' ', lastname) FROM professor_details WHERE userId=l.id)
							WHEN ur.roleId= 3 THEN (SELECT CONCAT(firstname, ' ', lastname) FROM publisher_details WHERE userId=l.id)
						END AS inviter,
						CASE
							when ur.roleId= 1 then (SELECT profilePic FROM institute_details WHERE userId=l.id)
							when ur.roleId= 2 then (SELECT profilePic FROM professor_details WHERE userId=l.id)
							when ur.roleId= 3 then (SELECT profilePic FROM publisher_details WHERE userId=l.id)
						END as profilePic, c.image AS coverPic
						FROM student_course sc join courses c ON c.id=sc.course_id
						Join user_roles ur ON ur.userId=c.ownerId 
						JOIN login_details l ON l.id=ur.userId AND ur.userId=c.ownerId
						WHERE  sc.packId>0 AND sc.courseEndDate >now() AND user_id=$data->userId AND ur.roleId NOT IN (4,5)";
				//c.ownerId=$data->pageUserId and
				$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$courses = $courses->toArray();
				$result->courses = array();
				foreach ($courses as $course) {
					$query="select id subjectId, name subjectName from subjects where courseId = {$course['course_id']} and deleted=0"; 
					$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$course['subjects'] = $subjects->toArray();
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
						require_once 'amazonRead.php';
						$course['coverPic'] = getSignedURL($course['coverPic']);
						$course['profilePic'] = getSignedURL($course['profilePic']);
					} else {
						$course['coverPic'] = 'assets/pages/img/background/32.jpg';
						$course['profilePic'] = 'assets/pages/img/avatars/team7.jpg';
					}
					$result->courses[] = $course;
				}
			} else {
				$adapter = $this->adapter;

				$query="SELECT sc.id,l.id userId ,c.id course_id,c.name,c.subtitle,sc.key_id,date_format(sc.timestamp,'%Y-%m-%dT%T')startDate,c.liveDate as origStartDate, date_format(sc.courseEndDate,'%Y-%m-%dT%T') as endDate,
						CASE
							when ur.roleId= 1 then (select name from institute_details where userId=l.id)
							when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname) from professor_details where userId=l.id)
							when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname) from publisher_details where userId=l.id)
						END as inviter,
						CASE
							when ur.roleId= 1 then (SELECT profilePic FROM institute_details WHERE userId=l.id)
							when ur.roleId= 2 then (SELECT profilePic FROM professor_details WHERE userId=l.id)
							when ur.roleId= 3 then (SELECT profilePic FROM publisher_details WHERE userId=l.id)
						END as profilePic,
						c.image AS coverPic
						FROM student_course sc join courses c ON c.id=sc.course_id
						INNER JOIN user_roles ur ON ur.userId=c.ownerId 
						INNER JOIN login_details l ON l.id=ur.userId AND sc.packId>0 AND sc.courseEndDate>now() AND  ur.userId=c.ownerId WHERE user_id = $data->userId AND ur.roleId NOT IN (4,5)";

				$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$courses = $courses->toArray();
				$result->courses = array();
	            foreach ($courses as $course) {
					$query="select id subjectId, name subjectName from subjects where courseId = {$course['course_id']} and deleted=0"; 
					$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$course['subjects'] = $subjects->toArray();
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
						require_once 'amazonRead.php';
						$course['coverPic'] = getSignedURL($course['coverPic']);
						$course['profilePic'] = getSignedURL($course['profilePic']);
					} else {
						$course['coverPic'] = 'assets/pages/img/background/32.jpg';
						$course['profilePic'] = 'assets/pages/img/avatars/team7.jpg';
					}
					$result->courses[] = $course;
				}
			}
			//$result->course = $course;
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	//function to get courses for tree structure view
	public function getCoursesForStudentTree($data) {
		$result = new stdClass();
		try {
			if(isset($data->pageUserId) && !empty($data->pageUserId)) {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				//for fetching courses
				$select = $sql->select();
				$select->from(array('sc'	=>	'student_course'))
				->join(array(
						'c' => 'courses'
							),
						'sc.course_id = c.id',
						array(
						'id' => 'id',
						'name' => 'name'
						)
				)
				->join(array(
						'pmc' => 'pf_main_courses'
							),
						'pmc.courseId = c.id',
						array()
				)
				->join(array(
						'p' => 'pf_portfolio'
							),
						'p.id = pmc.portfolioId',
						array()
				);
				$select->columns(array());
				$select->where(array('c.ownerId' => $data->pageUserId,'sc.user_id' => $data->userId,'p.subdomain' => $data->portfolioSlug));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$courses = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
				$courses = $courses->toArray();
				
				
				//for fetching subjects
				foreach($courses as $key => $course) {
					$select = $sql->select();
					$select->from(array('s'	=>	'subjects'));
					$select->where(array(
						's.courseId'	=>	$course['id'],
						's.deleted'	=>	0
					));
					$select->columns(array('id', 'name'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjects = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
					$subjects = $subjects->toArray();
					//print_r($subjects);
					
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('student_subject');
					$select->columns(array('id','subjectId'));
					$select->where(array('courseId' => $course['id'],'userId' => $data->userId,'Active'=>1));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjects1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$subjects1 = $subjects1->toArray();
					$stringSubjectids=array();
					
					if(count($subjects1) > 0){
						foreach($subjects1 as $key2=>$value2)
						{
						//$stringSubjectids .=','.$value['subjectId'];
							$stringSubjectids[$key2]=$value2['subjectId'];
						}
					}		
					foreach($subjects as $key1 => $subject){
						if(count($stringSubjectids)>0){
							$idcheck=in_array($subject['id'],$stringSubjectids);
							if($idcheck>0)
							{
							
							}else{
								unset($subjects[$key1]);
								$subjects = array_values($subjects);
							}
						}
					}
					foreach($subjects as $key1 => $subject) {
						$select = $sql->select();
						$select->from('chapters');
						$select->where(array(
							'subjectId'	=>	$subject['id'],
							'deleted'	=>	0
						));
						$select->columns(array('id', 'name'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$chapters = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
						$chapters = $chapters->toArray();
						$subjects[$key1]['chapters'] = $chapters;
					}
					$courses[$key]['subjects'] = $subjects;
				}
			} 
			else {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				//for fetching courses
				$select = $sql->select();
				$select->from(array('sc'	=>	'student_course'))
				->join(array(
						'c' => 'courses'
							),
						'sc.course_id = c.id',
						array(
						'id' => 'id',
						'name' => 'name'
						)
				);
				$select->columns(array());
				$select->where(array('sc.user_id' => $data->userId));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$courses = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
				$courses = $courses->toArray();
				
				//for fetching subjects
				foreach($courses as $key => $course) {
					$select = $sql->select();
					$select->from(array('s'	=>	'subjects'));
					$select->where(array(
						's.courseId'	=>	$course['id'],
						's.deleted'	=>	0
					));
					$select->columns(array('id', 'name'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjects = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
					$subjects = $subjects->toArray();

					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('student_subject');
					$select->columns(array('id','subjectId'));
					$select->where(array('courseId' => $course['id'],'userId' => $data->userId,'Active'=>1));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjects1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$subjects1 = $subjects1->toArray();
					$stringSubjectids=array();
					
					if(count($subjects1) > 0){
						foreach($subjects1 as $key2=>$value2)
						{
						//$stringSubjectids .=','.$value['subjectId'];
							$stringSubjectids[$key2]=$value2['subjectId'];
						}
					}		
					foreach($subjects as $key1 => $subject){
						if(count($stringSubjectids)>0){
							$idcheck=in_array($subject['id'],$stringSubjectids);
							if($idcheck>0)
							{
							
							}else{
								unset($subjects[$key1]);
								$subjects = array_values($subjects);
							}
						}
					}
					foreach($subjects as $key1 => $subject) {
						$select = $sql->select();
						$select->from('chapters');
						$select->where(array(
							'subjectId'	=>	$subject['id'],
							'deleted'	=>	0
						));
						$select->columns(array('id', 'name'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$chapters = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
						$chapters = $chapters->toArray();
						$subjects[$key1]['chapters'] = $chapters;
						
						//$subjects = array_values($subjects);
					}
					$courses[$key]['subjects'] = $subjects;
				}
					
			}
			$result->courses = $courses;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	
	//getcourseEnrollStatus
	public function getcourseEnrollStatus($data) {
        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from(array('sc' => 'student_course'));
            $select->columns(array('id'));
			$where = new \Zend\Db\Sql\Where();
			$where->equalTo('sc.user_id', $data->userId);
			if (isset($data->courseId) && !empty($data->courseId)) {
				$where->equalTo('sc.course_Id', $data->courseId);
			} else {
				$where->equalTo('c.slug', $data->slug);
				$select->join(array('c'	=>	'courses'),
					'c.id=sc.course_Id',
					array('cid'	=>	'id')
				);
			}
			$where->greaterThan('sc.timestamp',	new \Zend\Db\Sql\Expression("CURRENT_TIMESTAMP - INTERVAL 1 YEAR"));
            $select->where($where);
            $selectString = $sql->getSqlStringForSqlObject($select);
            $courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $courses = $courses->toArray();
            if (count($courses) > 0) {
            	$result->courseId=$courses[0]['cid'];
				$result->status=1;
                return $result;
            }
			$result->status=0;
            return $result;
        } catch (Exception $e) {
            $result->status=0;
            return $result;
        }
	}
	public function studentJoinedCourse($data) {
        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from(array('sc' => 'student_course'));
            $select->columns(array('id'));
			$where = new \Zend\Db\Sql\Where();
			$where->equalTo('sc.user_id', $data->userId);
			$where->equalTo('sc.course_Id', $data->courseId);
			$where->greaterThan('sc.timestamp',	new \Zend\Db\Sql\Expression("CURRENT_TIMESTAMP - INTERVAL 1 YEAR"));
            $select->where($where);
            $selectString = $sql->getSqlStringForSqlObject($select);
            $courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $courses = $courses->toArray();
            if (count($courses) > 0) {
                return true;
            }
            return false;
        } catch (Exception $e) {
            return false;
        }
	}
	public function studentJoinedCourseSubject($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$query="SELECT s.id,sc.user_id from subjects s join courses c on c.id=s.courseId JOIN student_course sc on sc.course_id=c.id "
					. " and c.id=$data->courseId and sc.user_id=$data->userId and s.id=$data->subjectId ";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$courses = $courses->toArray();
			if (count($courses) > 0) {
				return true;
			}
			return false;
		} catch (Exception $e) {
			return false;
		}
	}
	public function studentJoinedCourseChapter($data) {
	    $result = new stdClass();
	    try {
	        $adapter = $this->adapter;
	        $sql = new Sql($adapter);
	        $select = $sql->select();
			$query="SELECT ch.id from chapters ch join  subjects s on ch.subjectId=s.id join courses c on c.id=s.courseId JOIN student_course sc on sc.course_id=c.id "
	                . " and c.id=$data->courseId and sc.user_id=$data->userId and s.id=$data->subjectId and ch.id=$data->chapterId";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
	        $courses = $courses->toArray();
	        if (count($courses) > 0) {
	            return true;
	        }
	        return false;
	    } catch (Exception $e) {
	        return false;
	    }
	}
	public function studentJoinedInstitute($data) {
	    $result = new stdClass();
	    try {
	        $adapter = $this->adapter;
	        $sql = new Sql($adapter);
	        $select = $sql->select();
	        // query must be changed
	    	$query="SELECT ch.id from chapters ch join  subjects s on ch.subjectId=s.id join courses c on c.id=s.courseId JOIN student_course sc on sc.course_id=c.id "
	                . " and c.id=$data->courseId and sc.user_id=$data->userId and s.id=$data->subjectId and ch.id=$data->chapterId";
	    	$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
	        $courses = $courses->toArray();
	        if (count($courses) > 0) {
	            return true;
	        }
	        return false;
	    } catch (Exception $e) {
	        return false;
	    }
	}
	
	public function getBreadcrumbResult($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$selectString = "SELECT c.name chapterName, c.id chapterId, s.name subjectName, s.id subjectId, co.name courseName, co.id courseId FROM `exams` e LEFT OUTER JOIN chapters as c on c.id=e.chapterId JOIN subjects s ON s.id=e.subjectId JOIN courses co ON co.id=s.courseId where e.id={$data->examId}";
			$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$exams = $exams->toArray();
			if(count($exams) > 0) {
				$result->exams = $exams[0];
				$result->status = 1;
			}
			else {
				$result->status = 0;
				$result->message = "Exam not Found";
			}
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function saveRating($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			if($data->reviewId == 0) {
				$rating = new TableGateway('ratings', $this->adapter, null, new HydratingResultSet());
				$rating->insert(array(
					'userId'	=>	$data->userId,
					'courseId'	=>	$data->courseId,
					'subjectId'	=>	$data->subjectId,
					'title'		=>	$data->title,
					'review'	=>	$data->review,
					'rating'	=>	$data->rating,
					'time'		=>	time()
				));
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->ratingId = $rating->getLastInsertValue();
			}
			else {
				$update = $sql->update();
				$update->table('ratings');
				$update->set(array(
					'rating'	=>	$data->rating,
					'title'		=>	$data->title,
					'review'	=>	$data->review,
					'time'		=>	time()
				));
				$update->where(array('id' => $data->reviewId));
				$string = $sql->getSqlStringForSqlObject($update);
				$adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
			}
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function fetchUserRating($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('ratings')->where(array('userId' => $data->userId, 'subjectId' => $data->subjectId));
			$string = $sql->getSqlStringForSqlObject($select);
			$rating = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
			$rating = $rating->toArray();
			if(count($rating) > 0) {
				$result->rating = $rating[0];
			}
			else {
				$result->rating = 0;
			}

			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	/**
	* function to fetch average rating of a course
	*
	* @author Rupesh Pandey
	* @date 22/05/2015
	* @param courseId
	* @return array of average and total review
	*/
	public function getCourseRatings($courseId) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('ratings')->where(array('courseId' => $courseId));
			$select->columns(array(
				'rating'	=>	new \Zend\Db\Sql\Expression("AVG(rating)"),
				'total'		=>	new \Zend\Db\Sql\Expression("COUNT(*)")
			));
			$string = $sql->getSqlStringForSqlObject($select);
			$rating = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
			$rating = $rating->toArray();
			$rating = $rating[0];
			$result->rating = ((empty($rating['rating']))?0:$rating['rating']);
			$result->total = $rating['total'];
			return $result;
		}catch(Exception $e){
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	


	/**
	* function to fetch all reviews for all subjects of a course
	*
	* @author Rupesh Pandey
	* @date 23/05/2015
	* @param JSON with all required details
	* @return JSON with all reviews of the course in timed order
	*/
	public function getAllReviewsForCourse($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			if (isset($data->courseId) && !empty($data->courseId)) {
				$select->from(array('r'	=>	'ratings'))->where(array('r.courseId' => $data->courseId))->order('time DESC');
			} else {
				$select->from(array('r'	=>	'ratings'))->where(array('c.slug' => $data->slug))->order('time DESC');
				$select->join(array('c'	=>	'courses'),
					'c.id=r.courseId',
					array('id'	=>	'id')
				);
			}
			$select->join(array('sd'	=>	'student_details'),
				'sd.userId=r.userId',
				array(
					'studentName'	=>	new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"),
					'studentImage'	=>	'profilePic'
					)
			);
			$select->join(array('s'	=>	'subjects'),
				's.id=r.subjectId',
				array('subjectName'	=>	'name')
			);
			$string = $sql->getSqlStringForSqlObject($select);
			$reviews = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
			$reviews = $reviews->toArray();

			require_once 'amazonRead.php';
			foreach ($reviews as $key => $review) {
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					$reviews[$key]['studentImage'] = getSignedURL($review['studentImage']);
				} else {
					$reviews[$key]['studentImage'] = 'assets/layouts/layout2/img/avatar3_small.jpg';
				}
			}
			$result->reviews = $reviews;

			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	/**
	* function to fetch all reviews for a single subject
	*
	* @author Rupesh Pandey
	* @date 24/05/2015
	* @param JSON with all required details
	* @return JSON with all reviews of the course in timed order
	*/
	public function getAllReviewsForSubject($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('r'	=>	'ratings'))->where(array('r.subjectId' => $data->subjectId))->order('time DESC');
			$select->join(array('sd'	=>	'student_details'),
				'sd.userId=r.userId',
				array(
					'studentName'	=>	new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"),
					'studentImage'	=>	'profilePic'
					)
			);
			$select->join(array('s'	=>	'subjects'),
				's.id=r.subjectId',
				array('subjectName'	=>	'name')
			);
			$string = $sql->getSqlStringForSqlObject($select);
			$reviews = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
			$reviews = $reviews->toArray();
			require_once 'amazonRead.php';
			foreach ($reviews as $key => $review) {
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					$reviews[$key]['studentImage'] = getSignedURL($review['studentImage']);
				} else {
					$reviews[$key]['studentImage'] = 'assets/layouts/layout2/img/avatar3_small.jpg';
				}
			}
			$result->reviews = $reviews;

			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	/**
	* function to delete a previous review
	*
	* @author Rupesh Pandey
	* @date 23/05/2015
	* @param JSON with reviewId and other details
	* @return JSON with status of query
	*/
	public function removeReview($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$string = "DELETE FROM ratings WHERE id={$data->reviewId}";
			$adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();

			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	/**
	* function to fetch average rating of a subject
	*
	* @author Rupesh Pandey
	* @date 24/05/2015
	* @param subjectId
	* @return array of average and total review
	*/
	public function getSubjectRating($subjectId) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('ratings')->where(array('subjectId' => $subjectId));
			$select->columns(array(
				'rating'	=>	new \Zend\Db\Sql\Expression("AVG(rating)"),
				'total'		=>	new \Zend\Db\Sql\Expression("COUNT(*)")
			));
			$string = $sql->getSqlStringForSqlObject($select);
			$rating = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
			$rating = $rating->toArray();
			$rating = $rating[0];
			$result->rating = $rating['rating'];
			$result->total = $rating['total'];
			return $result;
		}catch(Exception $e){
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	/**
	* function to activate generated student on first login
	*
	* @author Jinendra Khobare
	* @date 19/06/2016
	*/
	public function initStudent($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			if ((isset($data->firstName) && !empty($data->firstName)) &&
				(isset($data->lastName) && !empty($data->lastName)) &&
				(isset($data->email) && !empty($data->email)) &&
				(isset($data->password) && !empty($data->password)) ) {
				$sql = new Sql($adapter);

				$select = $sql->select();
				$select->from(array('s'	=>	'student_details'))
				->join(array('l'	=>	'login_details'),
						's.userId = l.id',
						array(
							'username'	=>	'username',
							'email'		=>	'email'
							)
				)
				->join(array('ur'	=>	'user_roles'),
						'ur.userId = l.id'
				);
				$select->columns(array(
								'name'		=>	new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"),
								'firstName'	=>	'firstName',
								'lastName'	=>	'lastName',
								'userId'	=>	'userId'
				));
				$select->where(array('email' =>	$data->email, 'roleId' => 4));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$students = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$students = $students->toArray();
				if (count($students)>0) {
					$result->students	= $students;
					$result->status		= 0;
					return $result;
				}

				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('login_details');
				$update->set(array(
					'email'		=>	$data->email,
					'password'	=>	sha1($data->password),
					'temp'		=> 0
					));
				$update->where(array('id'	=>	$data->userId));
				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('student_details');
				$update->set(array(
					'firstName' =>	$data->firstName,
					'lastName' =>	$data->lastName,
					'gender' =>	1
					));
				$update->where(array('userId'	=>	$data->userId));
				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$sql = new Sql($adapter);
				$string = "DELETE FROM user_temp_passwords WHERE userId={$data->userId}";
				$adapter->query($string, $adapter::QUERY_MODE_EXECUTE);

				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('student_subject');
				$update->set(array(
					'Active' =>	1
					));
				$update->where(array('userId'	=>	$data->userId));
				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();

				$_SESSION['temp']	= 0;
				$result->status		= 1;
			} else {
				$result->status	 = 0;
				$result->message = "All fields are compulsory!";
			}
			return $result;
		}catch(Exception $e){
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	/**
	* function to verify and merge duplicate student
	*
	* @author Jinendra Khobare
	* @date 19/06/2016
	*/
	public function verifyMergeStudent($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			/*$select = $sql->select();
			$select->from('login_details')->where(array('email' => $data->email));
			$string = $sql->getSqlStringForSqlObject($select);
			$student = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
			$student = $student->toArray();
			if (count($student)>0) {
				$result->students	= $student;
				$result->status		= 0;
				return $result;
			}*/


			$select = $sql->select();
			$select->from(array('s'	=>	'student_details'))
			->join(array('l'	=>	'login_details'),
					's.userId = l.id',
					array(
						'username'	=>	'username',
						'email'		=>	'email'
						)
			)
			->join(array('ur'	=>	'user_roles'),
					'ur.userId = l.id',
					array(
						'roleId'	=>	'roleId'
						)
			);
			$select->columns(array(
							'name'		=>	new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"),
							'firstName'	=>	'firstName',
							'lastName'	=>	'lastName',
							'userId'	=>	'userId'
			));
			$select->where(array('l.id' =>	$data->optionMergeUser, 'l.password' =>	sha1($data->password), 'roleId' => 4));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$students = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$students = $students->toArray();
			if (count($students) == 0) {
				$result->status	= 0;
				$result->message	= "Wrong password!";
				return $result;
			}

			$student = $students[0];

			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('student_course');
			$select->where(array('user_id'	=>	$data->optionMergeUser));
			$select->columns(array(
						"course_id" => "course_id"
					));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$courses = $courses->toArray();
			if (count($courses)>0) {
				$lstCourses = array();
				foreach ($courses as $key => $value) {
					$lstCourses[] = $value['course_id'];
				}
				$courses = implode(",", $lstCourses);
				$update = $sql->update();
				$update->table('student_course');
				$update->set(array('user_id' =>	$data->optionMergeUser));
				$update->where("user_id = {$data->userId} AND course_id NOT IN ($courses)");
				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$update = $sql->update();
				$update->table('student_subject');
				$update->set(array('userId' =>	$data->optionMergeUser,'Active' => 1));
				$update->where("userId = {$data->userId} AND courseId NOT IN ($courses)");
				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			} else {
				$update = $sql->update();
				$update->table('student_course');
				$update->set(array('user_id' =>	$data->optionMergeUser));
				$update->where(array('user_id' => $data->userId));
				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$update = $sql->update();
				$update->table('student_subject');
				$update->set(array('userId' =>	$data->optionMergeUser,'Active' => 1));
				$update->where("userId = {$data->userId}");
				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			}

			$update = $sql->update();
			$update->table('login_details');
			$update->set(array('active' => 0));
			$update->where(array('id' => $data->userId));

			$statement = $sql->getSqlStringForSqlObject($update);
			$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

			$sql = new Sql($adapter);
			$string = "DELETE FROM user_temp_passwords WHERE userId={$data->userId}";
			$adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();

			//var_dump($student);
			$_SESSION["userId"] = $student['userId'];
			$_SESSION["userRole"] = $student['roleId'];
			$_SESSION['username'] = $student['username'];
			$_SESSION['temp']	= 0;
			$result->status		= 1;
			return $result;
		}catch(Exception $e){
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function saveProfileNew($data){

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			if (!empty($data->dob)) {
				$data->dob = strtotime($data->dob);
			}

			$update = $sql->update();
			$update->table('student_details');
			$update->set(array(
				'firstName' => $data->firstName,
				'lastName' => $data->lastName,
				'gender' => $data->gender,
				'dob' => $data->dob,
				'background' => $data->about,
				'achievements' => $data->achievements,
				'interests' => $data->interests
			));
			$update->where(array('userId' => $data->userId));
			$string = $sql->getSqlStringForSqlObject($update);
			$adapter->query($string, $adapter::QUERY_MODE_EXECUTE);

			$update = $sql->update();
			$update->table('user_details');
			$update->set(array(
				'contactMobilePrefix' => $data->mobilePrefix,
				'contactMobile' => $data->mobile,
				'addressCountry' => $data->country,
				'addressStreet' => $data->street,
				'addressCity' => $data->city,
				'addressState' => $data->state,
				'addressPin' => $data->street,
				'website' => $data->website,
				'facebook' => $data->facebook,
				'twitter' => $data->twitter
			));
			$update->where(array('userId' => $data->userId));
			$string = $sql->getSqlStringForSqlObject($update);
			$adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();

			$result = new stdClass();
			$result->status = 1;
			return $result;
		}catch(Exception $e){
			$db->rollBack();
			$result = new stdClass();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function verifyExistingStudent($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			$select = $sql->select();
			$select->from('invited_student_details')->where(array('email_id' => $data->email));
			$string = $sql->getSqlStringForSqlObject($select);
			$student = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
			$student = $student->toArray();
			if (count($student)>0) {
				return true;
			} else {
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}

}
?>