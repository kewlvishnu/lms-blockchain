<?php 

/**
* Licensing class
*
* @author Archit Saxena
*/

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "Base.php";

class License extends Base {

    const ONE_YEAR        = 1;
    const ONE_TIME        = 2;
    const SELECTIVE_IP    = 3;
    const COMISSION_BASED = 4;

    function __construct() {
        parent::__construct();
    }

    /**
    * Return the available license codes for a given set of licenses
    * @param Array licenses The list of license codes
    * @return Array The list of license codes applicable for the course
    */
    
    public function getAvailableLicenses($licenses = array()) {
        $available_licenses = array();

        if(in_array(License::ONE_YEAR, $licenses)) {
            //$available_licenses[] = License::ONE_YEAR;
            return $available_licenses;
        }
        else if (in_array(License::ONE_TIME, $licenses) || in_array(License::COMISSION_BASED, $licenses)) {
            $available_licenses[] = License::ONE_TIME;
            $available_licenses[] = License::ONE_YEAR;
            return $available_licenses;
        }
        else if (in_array(License::SELECTIVE_IP, $licenses) || count($licenses) == 0 ) {
            $available_licenses[] = License::ONE_YEAR;
            $available_licenses[] = License::ONE_TIME;
            $available_licenses[] = License::COMISSION_BASED;
            $available_licenses[] = License::SELECTIVE_IP;
            return $available_licenses;
        }
        return array();
    }

    /**
    * Add the passed license to the course if it is applicable 
    * @param int courseId CourseId for which the license is to be added
    * @param int license License code to be added to the course
    * @param Object prices The following prices are members: INR, USD, comissionAmt, comissionPercent. The irrelevent prices will be null
    * @return boolean If added or not
    */
    public function addLicense($courseId, $licenseType, $prices) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $applicableLicenses = $this->getApplicableLicenses($courseId);
            
            if (in_array($licenseType, $applicableLicenses)) {
                try {
    				$sql = new Sql($this->adapter);
                    $insert = $sql->insert();
                    $insert->into("course_licensing");
                    $table_course_licensing = new TableGateway('course_licensing', $this->adapter, null, new HydratingResultSet());
                    $table_course_licensing->insert(array(
                        'courseId'         => $courseId,
                        'LicensingType'    => $licenseType,
                        'priceUSD'         => $prices->USD,
                        'priceINR'         => $prices->INR,
                        'comissionAmt'     => (isset($prices->comissionAmt)) ? $prices->comissionAmt : 0,
                        'comissionPercent' => (isset($prices->comissionPercent)) ? $prices->comissionPercent : 0
                        ));
                }
                catch(Exception $e) {
                    $result->status = 0;
                    $result->exception = $e->getMessage() . '11';
                    return $result;
                }
                // If all succeed, commit the transaction and all changes are committed at once.
                $db->commit();
                // Done. Send success message
                $result->status = 1;
                $result->message = "License Applied";
                
                return $result;
            }
            else {
                $db->rollBack();
                // Failed. License Not Applicable
                $result->status = 0;
                $result->message = "License Not Applicable";

                return $result;
            }
        } catch (Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }
    
    public function getApplicableLicenses($courseId) {
		$adapter = $this->adapter;
        $licensesAppliedOnChildren = License::getAppliedLicenses($courseId, $adapter);
        
        return $this->getAvailableLicenses($licensesAppliedOnChildren);
	}

    /**
    * Gets all the licenses that are added to the course
    * @param int courseId THe course Id of the course for which the license type is needed
    * @return Object The licenses applied
    */
    public function getLicenses($req) {
		require_once "Tax.php";
        $courseId = $req->courseId;
        $result = new stdClass();
        $result->status = 1;
        $adapter = $this->adapter;
        $applicableLicenses = $this->getApplicableLicenses($courseId);
        $result->applicableLicenses = $applicableLicenses;
        $t = new Tax();
        
        $result->taxes = $t->getTaxRatesForUser($req->userId); 
        try {
            $sql = new Sql($this->adapter);
            $select = $sql->select();
            $select->from("course_licensing");
            $select->where(array('courseId' => $courseId));
            $select->columns(array(
                'licenseId' => 'id', 
                'licensingType', 
                'priceUSD', 
                'priceINR', 
                'comissionAmt', 
                'comissionPercent'
            ));
            $query = $sql->getSqlStringForSqlObject($select);
            $savedLicenses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $result->savedLicenses = array();
            if ($savedLicenses->count() !== 0) {
                $savedLicenses = $savedLicenses->toArray();
            }
            
            foreach($savedLicenses as $license) {
				if(in_array($license['licensingType'], $result->applicableLicenses))
					$license['status'] = 1;
				else
					$license['status'] = 0;
				$result->savedLicenses[$license['licensingType']] = $license;
			}
            
            return $result;
        }
        catch(Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    /**
    * Get applicable licenses for the given courseId
    */
    protected static function getAppliedLicenses($courseId, $adapter) {
        try {
            $sql = new Sql($adapter);
            $select = $sql->select();
            $where_sql = 's.id in (SELECT rootParentId FROM subjects WHERE courseId = '.$courseId.' AND rootParentId != 0 AND deleted = 0)';
            $select->from(array('s' => 'subjects'))
            ->join(array('c' => 'courses'), 's.courseId = c.id', array())
            ->join(array('p' => 'purchase_history'), 'c.id = p.newCourseId', array('licensingLevel'))
            ->where($where_sql);
            $select->columns(array());
            $query = $sql->getSqlStringForSqlObject($select);
            $licenses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $appliedLicenses = array();
            if($licenses->count() == 0) {
                return $appliedLicenses;
            }
            
            foreach( $licenses->toArray() as $license) {
				if(!in_array($license['licensingLevel'], $appliedLicenses))
					$appliedLicenses[] = $license['licensingLevel'];
			}
            
            return $appliedLicenses;
        }
        catch(Exception $e) {
            return false;
        }
    }
    
    /**
    * Update, Delete or Insert new license
    * Calls the required functions accordingly
    */
    public function modifyLicenses($data) {

        $db = $this->adapter->getDriver()->getConnection();

        $result = new stdClass();
        try {
    		$delete = $data->modify->delete;
                     $update = $data->modify->update;
            $insert = $data->modify->insert;
            $adapter = $this->adapter;
            $sql = new Sql($this->adapter);
            if(count($delete) != 0) {
                foreach($delete as $licenseId) {
    				$this->deleteLicense($licenseId);
                }
            }
            
            if (count($update) != 0) {
    			foreach($update as $i => $license) {
    				$this->updateLicense($license->id, $license);
                }
            }
            if (count($insert) != 0) {
                foreach($insert as $i => $license) {
    				$this->addLicense($data->courseId, $license->type, $license);
                }
            }

            $db->beginTransaction();
           
            //$query="UPDATE courses set availContentMarket=$data->availContentMarket WHERE id=$data->courseId";
            $query="UPDATE courses set availContentMarket=1 WHERE id=$data->courseId";
            $update = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();
            $result->status = 1;
            return $result;
        } catch (Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }
    
    /**
    * Updates the license costs for which License Id is given
    * @param int licenseId The license Id which has to be updated
    * @param Array prices The new prices which have to be updated
    */
    public function updateLicense($licenseId, $prices) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $table_course_licensing = new TableGateway('course_licensing', $this->adapter, null, new HydratingResultSet());
            $updates = array();
            if (isset($prices->INR)) {
                $updates['priceINR'] = $prices->INR;
            }
            if (isset($prices->USD)) {
                $updates['priceUSD'] = $prices->USD;
            }
            if (isset($prices->comissionAmt)) {
                $updates['comissionAmt'] = isset($prices->comissionAmt)? $prices->comissionAmt : 0;
            }
            if (isset($prices->comissionPercent)) {
                $updates['comissionPercent'] = isset($prices->comissionPercent) ? $prices->comissionPercent : 0;
            }
            $table_course_licensing->update($updates, array('id' => $licenseId));
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }

    /**
    * Deletes the license for which License Id is given
    * @param int licenseId The license Id which has to be deleted
    */
    public function deleteLicense($licenseId) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $sql = new Sql($this->adapter);
            $delete = $sql->delete();
            $delete->from('course_licensing');
            $delete->where(array('id' => $licenseId));
            $statement = $sql->prepareStatementForSqlObject($delete);
            $done = $statement->execute();
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }

    public function saveCourseRateStudent($data) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($this->adapter);
            //$query = "UPDATE courses set studentPrice=$data->studentPrice,studentPriceINR=$data->studentPriceINR,availStudentMarket=$data->availStudentMarket Where id=$data->courseId";
            $query = "UPDATE courses set studentPrice=$data->studentPrice,studentPriceINR=$data->studentPriceINR,availStudentMarket=1 Where id=$data->courseId";
            $update = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();
          
            $result->status = 1;
            return $result;
        }
        catch(Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
	
   // liveCourseStudent
     public function liveCourseStudent($data) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $adapter = $this->adapter;
			require_once 'StudentInvitation.php';
			$si = new StudentInvitation();
			$result = $si->checkRestrictionAndWarningOfCourse($data);
			if($result->status == 0)
				return $result;
            $sql = new Sql($this->adapter);
            $query = "UPDATE courses set liveForStudent=1 Where id=$data->courseId";
            $update = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();
          
            $result->status = 1;
            return $result;
        }
        catch(Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
     public function liveCourseOnContentMarket($data) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $adapter = $this->adapter;
			require_once 'StudentInvitation.php';
			$si = new StudentInvitation();
			$result = $si->checkRestrictionAndWarningOfCourse($data);
			if($result->status == 0)
				return $result;
            $sql = new Sql($this->adapter);
            $query = "UPDATE courses set liveForContentMarket=1 Where id=$data->courseId";
            $update = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();
            $result->status = 1;
            return $result;
        }
        catch(Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
	// unliveCourseStudent
	public function unliveCourseStudent($data) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			$query = "UPDATE courses set liveForStudent=0 Where id=$data->courseId";
			$update = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();
			$result->status = 1;
			return $result;
		}
		catch(Exception $e) {
            $db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function unliveCourseOnContentMarket($data) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			$query = "UPDATE courses set liveForContentMarket=0 Where id=$data->courseId";
			$update = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();
			$result->status = 1;
			return $result;
		}
		catch(Exception $e) {
            $db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function fetchCourseLicense($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			$query = "SELECT liveForStudent, liveForContentMarket FROM courses where id={$data->courseId}";
			$course = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$course = $course->toArray();
			$result->course = $course[0];
			$result->status = 1;
			return $result;
		}
		catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
     /**
    * Gets all the licenses that are added to the course
    *  
    */
    public function getLicensesONCourse($req) {
		
        $courseId = $req->courseId;
        $result = new stdClass();
        $result->status = 1;
        $adapter = $this->adapter;
        $applicableLicenses = $this->getApplicableLicenses($courseId);
        $result->applicableLicenses = $applicableLicenses;
       try {
            $sql = new Sql($this->adapter);
            $select = $sql->select();
            $select->from("course_licensing");
            $select->where(array('courseId' => $courseId));
            $select->columns(array(
                'licenseId' => 'id', 
                'licensingType', 
                'priceUSD', 
                'priceINR', 
                'comissionAmt', 
                'comissionPercent'
            ));
            $query = $sql->getSqlStringForSqlObject($select);
            $savedLicenses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
             $result->savedLicenses = array();
            if ($savedLicenses->count() !== 0) {
                $savedLicenses = $savedLicenses->toArray();
            }
            foreach($savedLicenses as $license) {
				if(in_array($license['licensingType'], $result->applicableLicenses))
					$license['status'] = 1;
				else
					$license['status'] = 0;
				$result->savedLicenses[$license['licensingType']] = $license;
			}
            
            return $result;
        }
        catch(Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

}
