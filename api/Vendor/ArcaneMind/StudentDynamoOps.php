<?php
	//header('Content-Type: text/plain; charset=utf-8');
	
	// Include the AWS SDK using the Composer autoloader
	require_once 'api/Vendor/aws/aws-autoloader.php';
	use Aws\S3\S3Client;
	use Aws\DynamoDb\DynamoDbClient;
	use Aws\Common\Enum\Region;
	use Aws\DynamoDb\Enum\KeyType;
	use Aws\DynamoDb\Enum\Type;
	
	class StudentDynamoOps{
	
	/*
	@author : mackayush
	@params:  userId and SubjectId
	@return:  contentIds of all the videos watched by user from dynamo db
	*/
	public function getWatchedVideos($data) {
		try {
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$client = $aws->get("dynamodb");
			$config = require "ConfD.php";
			$result = $client->getItem(array(
				'ConsistentRead' => true,
				'TableName' => $config,
				'Key'       => array(
					'userId'   => array('N' => $data->userId),
					'subjectId' => array('N' => $data->subjectId)
				)
			));
			if(count($result['Item']['videowatched']['SS'])>0)
			return($result['Item']['videowatched']['SS']);
			else{
				return $result;
			}
		}
		catch (Exception $e) {
			return $result;
		}
		
	}
	
	/*
	@author : mackayush
	@params:  userId and chapterId
	@return:  contentIds of all the videos watched by user from dynamo db
	*/
	public function getWatchedVideosByChapter($data) {
		try {
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$client = $aws->get("dynamodb");
			$config = require "ConfD.php";	
			$response = $client->query(array(
				'TableName' => $config,
				'IndexName' => 'userId',

				'KeyConditionExpression' => 'userId = :v_userId and chapterId >= :v_chapterId',
				'ExpressionAttributeValues' =>  array (
					':v_userId' => array('N' => $data->userId),
					':v_chapterId' => array('N' => $data->chapterId)
				),
				'Select' => 'ALL_ATTRIBUTES'
			));
		 	if(count($response['Items'])>0){
				if(count($response['Items'][0]['videowatched']['SS'])>0)
				{
					return(count($response['Items'][0]['videowatched']['SS']));
				
				}
			}
			else{
				return 0;
			}
		}
		catch (Exception $e) {
			return 0;
		}		
		
	}

	/*
	@author : mackayush
	@params:  userId and SubjectId
	@return:  contentIds of all the videos watched by user from dynamo db
	check
	*/
	public function getAllWatchedInformation($data) {
		try {
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$client = $aws->get("dynamodb");
			$config = require "confD.php";
			$result = $client->getItem(array(
				'ConsistentRead' => true,
				'TableName' => $config,
				'Key'       => array(
					'userId'   => array('N' => $data->userId),
					'subjectId' => array('N' => $data->subjectId)
				)
			));
			//print_r($result);
			if(count($result['Item']['videowatched']['S'])>0)
			return($result['Item']);
			else{
				return $result;
			}
		}
		catch (Exception $e) {
			return $result;
		}
	}

	/*
	@author : mackayush
	@params:  SubjectId
	@return: return all data across subjectId
		'global secondary index by query'
	*/
	public function getStudentsProgress($data) {
		try {
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$client = $aws->get("dynamodb");
			$config = require "confD.php";
			$subjectData="";
			$subjectData = $client->query (array(
						    'TableName' => $config,
						    'IndexName' => 'subjectId-index',
						    'KeyConditionExpression' => 
						        'subjectId = :v_dt',
						    'ExpressionAttributeValues' =>  array(
						        ':v_dt' => array('N' => $data->subjectId)
						    )
						));
			if(count($subjectData['Count'])>0){
				return($subjectData['Items']);
			}				
			else{
				return $result;
			}
		}
		catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
		}
	}

	
	/*
	@author: mackayush
	@params: userId , SubjectId and array of watched videos
	@return: contentIds of all the videos watched by user from dynamo db
	check
	*/
	public function putWatchedVideos($data) {
		$result = new stdClass();
		try {
				$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
				$client = $aws->get("dynamodb");
				$config = require "confD.php";
				$updateItem = $client->putItem(array(
					'ConsistentRead' => true,
					'TableName' => $config,
					'Item'       => array(
						'userId'   => array('N'  => $data->userId),
						'subjectId' => array('N' => $data->subjectId),
						'NoOfViews' =>array('S'  =>	$data->noOfviews),
						'totalTime' =>array('S'  =>	$data->totalTime),
						'videowatched'  => array('S'  =>$data->watchedVideo),
						'ChapterProgress' => array('S'  =>$data->chapterProgress)
					)
				));
				$result->status = 1;
				return $result;
		}
		catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
	}//class ends
?>