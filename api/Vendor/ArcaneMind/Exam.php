﻿<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Select;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	
	require_once "Base.php";
	
	//$adapter = require "adapter.php";
	
	class Exam extends Base {
	
	
		public function userOwnsExam($data) {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('exams');
			$where = new \Zend\Db\Sql\Where();
			$where->equalTo('id', $data->examId);
			$select->where($where);
			$select->columns(array('subjectId'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$subjectId = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$temp = $subjectId->toArray();
			if(count($temp) == 0)
				return false;
			$subjectId = $temp[0]['subjectId'];
			require_once "Subject.php";
			$s = new Subject();
			$req = new stdClass();
			$req->userId = $data->userId;
			$req->subjectId = $subjectId;
			
			if($s->userOwnsSubject($req))
				return true;
			return false;
		}
		public function getExamDetails($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				
				$selectString = "SELECT `e`.`id` AS `id`, `e`.`name` AS `name`, `e`.`type` AS `type`, `e`.`chapterId` AS `chapterId`, `e`.`subjectId` AS `subjectId`, `e`.`status` AS `status`, `e`.`startDate` AS `startDate`, `e`.`endDate` AS `endDate`, `e`.`attempts` AS `attempts`, `e`.`delete` AS `delete`, `c`.`name` AS `chapterName`,
								(SELECT COUNT(*) FROM exam_attempts WHERE `examId` = `e`.`id`) AS `attemptCount`
								FROM `exams` AS `e`
								LEFT JOIN `chapters` AS `c` ON `e`.`subjectId` = `c`.`subjectId` AND `e`.`chapterId` = `c`.`id`
								WHERE `e`.`subjectId` = '{$data->subjectId}' AND `e`.`delete` = '0' ORDER BY `id` DESC";
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->exams = $exams->toArray();
				if(count($exams) == 0) {
					$result->status = 0;
					$result->message = "No Exams found";
				}
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getExamDetail($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				
				$selectString ="SELECT e.id AS `id`, e.name AS `name`, e.type AS `type`, e.chapterId AS `chapterId`, e.`status` AS `status`,
										e.`startDate` AS `startDate`, e.`endDate` AS `endDate`, e.`attempts` AS `attempts`, e.`delete` AS `delete`,
										c.`name` AS `chapterName`,
										e.`subjectId` AS `subjectId`, s.`name` AS `subjectName`,
										co.`id` AS `courseId`, co.`name` AS `courseName`,
										COUNT(q.id) AS questions, sc.correct AS marks
								FROM `exams` AS `e`
								LEFT JOIN `chapters` AS c ON e.subjectId = c.subjectId AND e.chapterId = c.id
								INNER JOIN `subjects` AS s ON s.id = e.subjectId
								INNER JOIN `courses` AS co ON co.id = s.courseId
								INNER JOIN `exam_sections` AS es ON es.examId=e.id AND es.delete=0 AND es.status=1
								INNER JOIN `section_categories` AS sc ON sc.sectionId=es.id AND sc.delete=0 AND sc.status=1
								INNER JOIN `question_category_links` AS qcl ON qcl.categoryId=sc.id
								INNER JOIN `questions` AS q ON q.id=qcl.questionId AND q.status=1
								WHERE `e`.`id` = '{$data->examId}' AND `e`.`delete` = '0'
								GROUP BY sc.id";
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				if(count($exams) == 0) {
					$result->status = 0;
					$result->message = "No Exam found";
					return $result;
				} else {
					$examTotalScore = 0;
					$examQuestions = 0;
					foreach ($exams as $key => $exam) {
						$examTotalScore+= $exam['questions']*$exam['marks'];
						$examQuestions += $exam['questions'];
						unset($exams[$key]['questions']);
						unset($exams[$key]['marks']);
					}
				}
				$result->status	= 1;
				$result->exam	= $exams[0];
				$result->exam['totalScore'] = $examTotalScore;
				$result->exam['questions'] = $examQuestions;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getExamDetailAdmin($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				
				$selectString ="SELECT e.id AS `id`, e.name AS `name`, e.type AS `type`, e.chapterId AS `chapterId`, e.`status` AS `status`,
										e.`startDate` AS `startDate`, e.`endDate` AS `endDate`, e.`attempts` AS `attempts`, e.`delete` AS `delete`,
										c.`name` AS `chapterName`,
										e.`subjectId` AS `subjectId`, s.`name` AS `subjectName`,
										co.`id` AS `courseId`, co.`name` AS `courseName`,
										COUNT(q.id) AS questions, sc.correct AS marks
								FROM `exams` AS `e`
								LEFT JOIN `chapters` AS c ON e.subjectId = c.subjectId AND e.chapterId = c.id
								INNER JOIN `subjects` AS s ON s.id = e.subjectId
								INNER JOIN `courses` AS co ON co.id = s.courseId
								LEFT JOIN `exam_sections` AS es ON es.examId=e.id AND es.delete=0 AND es.status=1
								LEFT JOIN `section_categories` AS sc ON sc.sectionId=es.id AND sc.delete=0 AND sc.status=1
								LEFT JOIN `question_category_links` AS qcl ON qcl.categoryId=sc.id
								LEFT JOIN `questions` AS q ON q.id=qcl.questionId AND q.status=1
								WHERE `e`.`id` = '{$data->examId}'
								GROUP BY sc.id";
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				if(count($exams) == 0) {
					$result->status = 0;
					$result->message = "No Exam found";
					return $result;
				} else {
					$examTotalScore = 0;
					$examQuestions = 0;
					foreach ($exams as $key => $exam) {
						$examTotalScore+= $exam['questions']*$exam['marks'];
						$examQuestions += $exam['questions'];
						unset($exams[$key]['questions']);
						unset($exams[$key]['marks']);
					}
				}
				$result->status	= 1;
				$result->exam	= $exams[0];
				$result->exam['totalScore'] = $examTotalScore;
				$result->exam['questions'] = $examQuestions;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getExamOnlyAdmin($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				
				$selectString ="SELECT e.id AS `id`, e.name AS `name`, e.type AS `type`, e.chapterId AS `chapterId`, e.`status` AS `status`,
										e.`startDate` AS `startDate`, e.`endDate` AS `endDate`, e.`attempts` AS `attempts`, e.`delete` AS `delete`,
										c.`name` AS `chapterName`,
										e.`subjectId` AS `subjectId`, s.`name` AS `subjectName`,
										co.`id` AS `courseId`, co.`name` AS `courseName`
								FROM `exams` AS `e`
								LEFT JOIN `chapters` AS c ON e.subjectId = c.subjectId AND e.chapterId = c.id
								INNER JOIN `subjects` AS s ON s.id = e.subjectId
								INNER JOIN `courses` AS co ON co.id = s.courseId
								WHERE `e`.`id` = '{$data->examId}'";
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				if(count($exams) == 0) {
					$result->status = 0;
					$result->message = "No Exam found";
					return $result;
				}
				$result->status	= 1;
				$result->exam	= $exams[0];
				//$result->exam['questions'] = $examQuestions;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getSubjectiveExamDetail($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$selectString ="SELECT es.id,es.name AS examName,es.chapterId,es.status,es.startDate,es.endDate,es.attempts,es.Active,
										c.name AS `chapterName`,es.subjectId, s.name AS `subjectName`,
										co.id AS `courseId`, co.name AS `courseName`,
										COUNT(qs.id) AS questions, qsel.marks
								FROM `exam_subjective` AS `es`
								LEFT JOIN `chapters` AS `c` ON es.subjectId = c.subjectId AND es.chapterId = c.id
								INNER JOIN `subjects` AS `s` ON s.id = es.subjectId
								INNER JOIN `courses` AS `co` ON co.id = s.courseId
								LEFT JOIN `question_subjective_exam_links` AS qsel ON qsel.examId=es.id
								LEFT JOIN `questions_subjective` AS qs ON qs.id = qsel.questionId AND qs.status=1
								WHERE es.id = '{$data->examId}' AND es.Active = '1'
								GROUP BY qs.id
								ORDER BY `id` DESC";
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				if(count($exams) == 0) {
					$result->status = 0;
					$result->message = "No Exam found";
					return $result;
				} else {
					$examTotalScore = 0;
					$examQuestions = 0;
					foreach ($exams as $key => $exam) {
						$examTotalScore+= $exam['questions']*$exam['marks'];
						$examQuestions += $exam['questions'];
						unset($exams[$key]['questions']);
						unset($exams[$key]['marks']);
					}
				}
				$result->status	= 1;
				$result->exam	= $exams[0];
				$result->exam['totalScore'] = $examTotalScore;
				$result->exam['questions'] = $examQuestions;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function addExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				// Need to check if subject is owned by user
				//checking if the course is expired
				require_once 'Subject.php';
				$s = new Subject();
				if($s->subjectCourseExpired($data)) {
					$result->status = 0;
					$result->message = 'Your course has expired. You can not add more Exams/Assignments.';
					return $result;
				}
				$namecheck=$this->checkNameForExam($data);
				if($namecheck->available != 'true' ) {
					$result->status = 0;
					$result->message = 'Your exam/assignment name already exists.please give some other name to more Exams/Assignments.';
					return $result;
				}
				
				$exams = new TableGateway('exams', $adapter, null,new HydratingResultSet());
				$insert = array(
					'name'				=>	$data->name,
					'type'				=>	$data->type,
					'ownerId'			=>	$data->userId,
					'chapterId'			=>	$data->chapterId,
					'subjectId' 		=> 	$data->subjectId,
					'status'			=>	$data->status,
					'startDate'			=>	$data->startDate,
					'endDate'			=>	$data->endDate,
					'attempts'			=>	$data->attempts,
					'tt_status'			=>	$data->tt_status,
					'totalTime'			=>	$data->totalTime,
					'gapTime'			=>	$data->gapTime,
					'sectionOrder'		=>	$data->sectionOrder,
					'powerOption'		=>	(($data->type == "Assignment")?1:$data->powerOption),
					'endBeforeTime'		=>	$data->endBeforeTime,
					'showResult'		=>  $data->showResult,
					'showResultTill'	=>  $data->showResultTill,
					'showResultTillDate'=>  $data->showResultTillDate,
					'delete'			=>  0
				);
				$exams->insert($insert);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$data->examId = $exams->getLastInsertValue();
				$result->examId = $data->examId;
				if($data->examId == 0) {
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}
				$e = new Exam();
				$temp = new stdClass();
				$temp = $e->addSections($data);
				if($temp->status == 1)
					$result->status = 1;
				else
					return $temp;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		//addSubejctiveExam
		public function addSubejctiveExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				require_once 'Subject.php';
				$s = new Subject();
				//print_r($data->courseId);
				if($s->subjectSubjectiveExamCheck($data)) {
					$result->status = 0;
					$result->message = 'Your course has expired. You can not add more Subjective Exam.';
					return $result;
				}
				$exams = new TableGateway('exam_subjective', $adapter, null,new HydratingResultSet());
				$insert = array(
					'name'				=>	$data->title,
					'type'				=>	'Exam',
					'ownerId'			=>	$data->userId,
					'chapterId'			=>	$data->chapterId,
					'subjectId' 		=> 	$data->subjectId,
					'status'			=>	0,
					'startDate'			=>	$data->startDate,
					'endDate'			=>	$data->endDate,
					'attempts'			=>	$data->totalAttempts,
					'totalTime'			=>	$data->totalTime,
					'totalQuestions'	=>	0,
					'totalRequiredQuestions'	=>	0,
					'shuffle'			=>	$data->shuffle,
					'submissiontype'	=>	$data->submission,
					'Active'			=>	1
				);
				$exams->insert($insert);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$data->examId = $exams->getLastInsertValue();
				//$result->examId = $data->examId;
				$result->data=$data;
				//print_r($result);
				if($data->examId == 0) {
					$db->rollBack();
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		//
		public function editSubjectiveExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				require_once 'Subject.php';
				$s = new Subject();
				//print_r($data->courseId);
				if($s->subjectSubjectiveExamCheck($data)) {
					$result->status = 0;
					$result->message = 'Your course has expired. You can not add more Subjective Exam.';
					return $result;
				}
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('exam_subjective');
				$update->set(array(
									'name'				=>	$data->title,
									'chapterId'			=>  $data->chapterId,
									'startDate'			=>	$data->startDate,
									'endDate'			=>	$data->endDate,
									'attempts'			=>	$data->totalAttempts,
									'totalTime'			=>	$data->totalTime,
									'shuffle'			=>	$data->shuffle,
									'submissiontype'	=>	$data->submission
				));
				$update->where(array('id'		=>	$data->examId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				
				$result->data=$data;
				//print_r($result);
				if($data->examId == 0) {
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function addSections($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$insert = "INSERT INTO exam_sections (`name`, `time`, `examId`, `weight`, `delete`) VALUES";
				for($i=0; $i<count($data->time); $i++) {
					$insert .= "('{$data->sections[$i]}','{$data->time[$i]}','{$data->examId}','{$data->weights[$i]}',0),";
				}
				$insert = substr($insert, 0, strlen($insert)-1);
				$adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function addTemplate($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				// Need to check if subject is owned by user
				$exams = new TableGateway('templates', $adapter, null,new HydratingResultSet());
				if(!isset($data->name))
					$data->name = 'Untitled Template';
				if($data->type == "Exam")
					$data->name .= '_E';
				else
					$data->name .= '_A';
				$insert = array(
					'name'				=>	$data->name,
					'type'				=>	$data->type,
					'ownerId'			=>	$data->userId,
									'tt_status'			=>	$data->tt_status,
									'totalTime'			=>	$data->totalTime,
									'gapTime'			=>	$data->gapTime,
										'sectionOrder'                  =>	$data->sectionOrder,
										'powerOption'                   =>	$data->powerOption,
										'endBeforeTime'                 =>	$data->endBeforeTime,
					'attempts'			=>	$data->attempts
				);
				$exams->insert($insert);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$data->templateId = $exams->getLastInsertValue();
				if($data->templateId == 0) {
					$db->rollBack();
					$result->status = 0;
					$result->message = "Some error occurred.";
					return $result;
				}
				$e = new Exam();
				$temp = new stdClass();
				$temp = $e->addTemplateSections($data);
				if($temp->status == 1)
					$result->status = 1;
				else
					return $temp;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function addTemplateSections($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$insert = "INSERT INTO template_sections VALUES";
				for($i=0; $i<count($data->sections); $i++) {
					$insert .= "('','{$data->sections[$i]}','{$data->time[$i]}','{$data->templateId}','{$data->weights[$i]}','0',''),";
				}
				$insert = substr($insert, 0, strlen($insert)-1);
				$adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function exportExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$exportSettings = $data->exportQuestions;
				$exportData = array();
				foreach ($exportSettings as $key => $setting) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('exam_sections');
					$where = new \Zend\Db\Sql\Where();
					$where->equalTo('id', $setting->sectionId);
					$select->where($where);
					$selectString = $sql->getSqlStringForSqlObject($select);
					$section = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$section = $section->toArray();
					$exportData[] = $section[0];
					foreach ($setting->category as $key1 => $category) {
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('section_categories');
						$select->where(array('id'	=>	$category->categoryId));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$categoryDetail = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$categoryDetail = $categoryDetail->toArray();
						$categoryDetail = $categoryDetail[0];
						$categoryDetail['name'] = $this->getCategoryName($categoryDetail['questionType']);
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from(array('q' => 'questions'));
						$select->join(array('qcl' => 'question_category_links'),'qcl.questionId = q.id',array());
						$select->where(array('categoryId'	=>	$categoryDetail['id'],
											'delete'		=>	0,
											'parentId'		=> 0,
											'questionType'	=> array(0,1,2,3,4,5,6)));
						$select->columns(array('id','question','questionType','status','parentId'));
						$select->limit($category->questions);
						$selectString = $sql->getSqlStringForSqlObject($select);
						$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$categoryDetail['questions'] = $questions->toArray();

						/*if ($categoryDetail['questionType'] == 4 || $categoryDetail['questionType'] == 5) {
							foreach ($categoryDetail['questions'] as $key => $question) {
								$sql = new Sql($adapter);
								$select = $sql->select();
								$select->from(array('q'	=>	'questions'));
								$select->where(array('parentId'=>$question['id'], 'status'=>1, 'delete'=>0));
								//$select->columns(array('id', 'question'));
								$selectString = $sql->getSqlStringForSqlObject($select);
								$subQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
								//var_dump($subQuestions->toArray());
								$categoryDetail['questions'][$key]['subQuestions'] = $subQuestions->toArray();
							}
						}*/

						$totalQuestions = 0;
						$tempParentQuestion = 0;
						$questionType = $categoryDetail['questionType'];
						foreach ($categoryDetail['questions'] as $key2 => $eachQuestion) {
							if ($eachQuestion['parentId'] == 0) {
								$totalQuestions++;
							} elseif ($eachQuestion['parentId'] != $tempParentQuestion) {
								$totalQuestions++;
								$tempParentQuestion = $eachQuestion['parentId'];
							}
							if($questionType == 0 || $questionType == 6) {
								$sql = new Sql($adapter);
								$select = $sql->select();
								$select->from('options_mcq');
								$select->where(array('questionId' => $eachQuestion['id']));
								$select->columns(array('id','option','correct'));
								$selectString = $sql->getSqlStringForSqlObject($select);
								$options = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
								$categoryDetail['questions'][$key2]['options'] = $options->toArray();
							}
							else if($questionType == 1) {
								$sql = new Sql($adapter);
								$select = $sql->select();
								$select->from('true_false');
								$select->where(array('questionId' => $eachQuestion['id']));
								$select->columns(array('id','answer'));
								$selectString = $sql->getSqlStringForSqlObject($select);
								$answer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
								$categoryDetail['questions'][$key2]['answer'] = $answer->toArray();
							}
							else if($questionType == 2) {
								$sql = new Sql($adapter);
								$select = $sql->select();
								$select->from('ftb');
								$select->where(array('questionId' => $eachQuestion['id']));
								$select->columns(array('id','blanks'));
								$selectString = $sql->getSqlStringForSqlObject($select);
								$blanks = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
								$categoryDetail['questions'][$key2]['blanks'] = $blanks->toArray();
							}
							else if($questionType == 3) {
								$sql = new Sql($adapter);
								$select = $sql->select();
								$select->from('options_mtf');
								$select->where(array('questionId' => $eachQuestion['id']));
								$select->columns(array('id','columnA','columnB'));
								$selectString = $sql->getSqlStringForSqlObject($select);
								$matches = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
								$categoryDetail['questions'][$key2]['matches'] = $matches->toArray();
							}
							else if($questionType == 4 || $questionType == 5) {
								$childQuestions = new stdClass();
								$e = new Exam();
								$categoryDetail['questions'][$key2]['childQuestions'] = $e->getCmpChildQuestions($eachQuestion['id']);
							}
						}
						if ($totalQuestions != $categoryDetail['entered']) {
							$update = $sql->update();
							$update->table('section_categories');
							$update->set(array('entered'			=>	$totalQuestions));
							$update->where(array('id'		=>	$categoryDetail['id']));
							$statement = $sql->prepareStatementForSqlObject($update);
							$statement->execute();
							$categoryDetail['entered'] = $totalQuestions;
						}
						$exportData[$key]['categories'][] = $categoryDetail;
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->exportData = $exportData;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function getExamSectionsForExport($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exam_sections');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('examId', $data->examId);
				$where->equalTo('status', 1);
				$where->notEqualTo('delete', 1);
				$select->where($where)->order('weight ASC', 'id DESC');
				$selectString = $sql->getSqlStringForSqlObject($select);
				$sections = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$sections = $sections->toArray();
				foreach ($sections as $key => $section) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('section_categories')->order('weight ASC, id DESC');
					$select->where(array('sectionId'	=>	$section['id'],
										'delete'		=>	0,
										'status'		=>	1));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$categories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$sections[$key]['categories'] = $categories->toArray();
					foreach ($sections[$key]['categories'] as $key1 => $category) {
						$sections[$key]['categories'][$key1]['name'] = $this->getCategoryName($sections[$key]['categories'][$key1]['questionType']);
					}
				}
				$result->sections = $sections;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getCategoryName($questionType) {
			switch ($questionType) {
				case '0':
					$name = "Multiple Choice Question";
					break;
				case '1':
					$name = "True or False";
					break;
				case '2':
					$name = "Fill in the blanks";
					break;
				case '3':
					$name = "Match the following";
					break;
				case '4':
					$name = "Comprehension Type Question";
					break;
				case '5':
					$name = "Dependent Type Question";
					break;
				case '6':
					$name = "Multiple Answer Question";
					break;
				case '7':
					$name = "Audio Question";
					break;
				
				default:
					$name = "";
					break;
			}
			return $name;
		}
		public function getExamSections($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exam_sections');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('examId', $data->examId);
								//for tempary delete feature
				//$where->notEqualTo('delete', 2);
								
						//implementing  not given facilty for restoring the section
								// and shows only sections that are not deleted
								$where->notEqualTo('delete', 1);
				$select->where($where)
										->order('weight ASC', 'id DESC');
					
				$selectString = $sql->getSqlStringForSqlObject($select);
				$sections = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->sections = $sections->toArray();
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getExamName($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exams');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('id', $data->examId);
				$where->equalTo('delete', 0);
				$select->where($where);
				$select->columns(array('name', 'type', 'status', 'association'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$tmp = $exams->toArray();
				$result->name = $tmp[0]['name'];
				$result->type = $tmp[0]['type'];
				$result->status = $tmp[0]['status'];
				$result->association = $tmp[0]['association'];
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function getSectionDetails($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('section_categories')->order('weight ASC, id DESC');
				$select->where(array('sectionId'	=>	$data->sectionId,
									'delete'		=>	0));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$categories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $categories->toArray();
				$categories = array();
				foreach($temp as $category) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('q' => 'questions'));
					$select->join(array('qcl' => 'question_category_links'),'qcl.questionId = q.id',array()/*,$select::JOIN_LEFT*/);
					$select->where(array('categoryId'	=>	$category['id'],
										'delete'		=>	0,
										'parentId'		=> 0));
					$select->columns(array('id','question','questionType','status','parentId'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$category['questions'] = $questions->toArray();

					if ($category['questionType'] == 4 || $category['questionType'] == 5) {
						foreach ($category['questions'] as $key => $question) {
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from(array('q'	=>	'questions'));
							$select->where(array('parentId'=>$question['id'], 'status'=>1, 'delete'=>0));
							//$select->columns(array('id', 'question'));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$subQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							//var_dump($subQuestions->toArray());
							$category['questions'][$key]['subQuestions'] = $subQuestions->toArray();
						}
					}

					$totalQuestions = 0;
					$tempParentQuestion = 0;
					foreach ($category['questions'] as $eachQuestion) {
						if ($eachQuestion['parentId'] == 0) {
							$totalQuestions++;
						} elseif ($eachQuestion['parentId'] != $tempParentQuestion) {
							$totalQuestions++;
							$tempParentQuestion = $eachQuestion['parentId'];
						}
					}
					if ($totalQuestions != $category['entered']) {
						$update = $sql->update();
						$update->table('section_categories');
						$update->set(array('entered'			=>	$totalQuestions));
						$update->where(array('id'		=>	$category['id']));
						$statement = $sql->prepareStatementForSqlObject($update);
						$statement->execute();
						$category['entered'] = $totalQuestions;
					}
					$categories[] = $category;
				}
				$result->categories = $categories;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function getSectionStatus($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exam_sections');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('examId', $data->examId);
				$where->notEqualTo('delete', 2);
				$select->where($where);
				$select->columns(array('id','status','randomQuestions'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$sections = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result = $sections->toArray();
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function saveCategories($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('exam_sections');
				$update->set(array('totalMarks'			=>	$data->totalMarks,
									'randomQuestions'	=>	$data->randomQuestions));
				$update->where(array('id'		=>	$data->sectionId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				$temp = false;
				$insert = "INSERT INTO section_categories (`sectionId`, `correct`, `wrong`, `required`, `entered`, `questionType`, `delete`, `status`, `weight`) VALUES";
				foreach($data->categories as $category) {
					if($category->categoryId == 0) {
						$insert .= "('{$data->sectionId}', '{$category->correct}', '{$category->wrong}', '{$category->required}', '0', '{$category->questionType}', '0','0','{$category->weight}'),";
						$temp = true;
					}
					else {
						$e = new Exam();
						$e->editCategory($category);
					}
				}

				$db->beginTransaction();

				$insert = substr($insert, 0, strlen($insert)-1);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				if($temp) {
					$exams = $adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);
					$e = new Exam();
					$e->updateSectionStatus($data);
				}
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function saveQuestion($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
			
				$questionidarr= array();
				//$question = new TableGateway('questions', $adapter, null,new HydratingResultSet());
				$insert = array(
					'ownerId'			=> 	$data->userId,
					'subjectId'			=> 	$data->subjectId,
					'questionType'		=> 	$data->questionType,
					'question'			=>	$data->question,
					'description'		=>	$data->desc,
					'difficulty'		=>	((isset($data->difficulty))?$data->difficulty:0),
					'status'			=>	$data->status,
					'parentId'			=>	$data->parentId,
					'delete'			=>	0
				);
				//$question->insert($insert);
				$query="INSERT INTO questions (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$adapter->query("SET @questionId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
				
				//$result->questionId = $question->getLastInsertValue();
				//array_push($questionidarr,$result->questionId);
				
				
				$reqTags = array();
				if (!empty($data->tags)) {
					$reqTags = $data->tags;					
					foreach ($data->tags as $key => $tag) {
						$query="SELECT *
								FROM `question_tags`
								WHERE `tag`= '$tag'";
						$tagCheck = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$tagCheck = $tagCheck->toArray();
						if (count($tagCheck)) {
							$tagId = $tagCheck[0]['id'];
							//$questionTags = new TableGateway('question_tag_links', $adapter, null,new HydratingResultSet());
							$insert = array(
											//'question_id' => $result->questionId,
											'tag_id' => $tagId
										);
							//$questionTags->insert($insert);
							$query="INSERT INTO question_tag_links (`".implode("`,`", array_keys($insert))."`,`question_id`) VALUES ('".implode("','", array_values($insert))."',@questionId)";
							$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						} else {
							//$questionTags = new TableGateway('question_tags', $adapter, null,new HydratingResultSet());
							$insert = array(
											'tag'	=>  $tag
										);
							//$questionTags->insert($insert);
							//$tagId = $questionTags->getLastInsertValue();
							$query="INSERT INTO question_tags (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
							$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$adapter->query("SET @tagId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
							//var_dump('2: '.$tagId);
							/*$questionTags = new TableGateway('question_tag_links', $adapter, null,new HydratingResultSet());
							$insert = array(
											'question_id' => $result->questionId,
											'tag_id' => $tagId
										);
							$questionTags->insert($insert);*/
							$query="INSERT INTO question_tag_links (`question_id`,`tag_id`) VALUES (@questionId,@tagId)";
							$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						}
					}
				}
				$query="SELECT sqtl.id, qt.tag
						FROM `question_tag_links` AS sqtl
						INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
						WHERE sqtl.question_id= @questionId";
				$tagsInDb = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tagsInDb = $tagsInDb->toArray();
				if (count($tagsInDb)>0){
					if(!empty($reqTags)) {
						$deleteTagIds = array();
						foreach ($tagsInDb as $key => $tag) {
							if (in_array($tag['tag'], $reqTags)) {
								unset($tagsInDb[$key]);
							} else {
								$deleteTagIds[] = $tag['id'];
							}
						}
						if (!empty($deleteTagIds)) {
							$sql = new Sql($adapter);
							$delete = $sql->delete();
							$delete->from('question_tag_links');
							$delete->where(array('id'	=>	$deleteTagIds));
							$statement = $sql->prepareStatementForSqlObject($delete);
							$statement->execute();
						}
					} else {
						$sql = new Sql($adapter);
						$delete = $sql->delete();
						$delete->from('question_tag_links');
						$delete->where(array('question_id'	=>	$result->questionId));
						$statement = $sql->prepareStatementForSqlObject($delete);
						$statement->execute();
					}
				}
				
				/*if($result->questionId == 0) {
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}*/
				if($data->questionType == 0 || $data->questionType == 6) {
					if(count($data->options) > 0) {
						foreach($data->options as $option) {
							//$options_mcq = new TableGateway('options_mcq', $adapter, null,new HydratingResultSet());
							$insert = array(
											//'questionId'	=>  $result->questionId,
											'parentId'		=>	$data->parentId,
											'option'		=>	$option->opt,
											'correct'		=>	$option->cState
										);
							//$options_mcq->insert($insert);
							$query="INSERT INTO options_mcq (`".implode("`,`", array_keys($insert))."`,`questionId`) VALUES ('".implode("','", array_values($insert))."',@questionId)";
							$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						}
					}
				}
				else if($data->questionType == 1) {
					if($data->answer !== '') {
						$insertString = "INSERT INTO true_false (questionId, parentId, answer) VALUES(@questionId,'{$data->parentId}','{$data->answer}')";
						$adapter->query($insertString, $adapter::QUERY_MODE_EXECUTE);
					}
				}
				else if($data->questionType == 2) {
					if(count($data->blanks) > 0) {
						$insertString = "INSERT INTO ftb (questionId, parentId, blanks) VALUES";
						foreach($data->blanks as $blank) {
							$insertString .= "(@questionId,'{$data->parentId}','{$blank->ftb}'),";
						}
						$insertString = substr($insertString, 0, strlen($insertString)-1);
						$adapter->query($insertString, $adapter::QUERY_MODE_EXECUTE);
					}
				}
				else if($data->questionType == 3) {
					if(count($data->matches) > 0) {
						foreach($data->matches as $match) {
							//$options_mtf = new TableGateway('options_mtf', $adapter, null,new HydratingResultSet());
							$insert = array(
											//'questionId'	=>  $result->questionId,
											'columnA'		=>	$match->optA,
											'columnB'		=>	$match->optB
										);
							//$options_mtf->insert($insert);
							$query="INSERT INTO options_mtf (`".implode("`,`", array_keys($insert))."`,`questionId`) VALUES ('".implode("','", array_values($insert))."',@questionId)";
							$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						}
					}
				}
				if($data->questionType == 7) {
					if(count($data->options) > 0) {
						foreach($data->options as $option) {
							//$options_mcq = new TableGateway('options_mcq', $adapter, null,new HydratingResultSet());
							$insert = array(
											//'questionId'	=>  $result->questionId,
											'parentId'		=>	$data->parentId,
											'option'		=>	$option->opt,
											'correct'		=>	$option->cState
										);
							//$options_mcq->insert($insert);
							$query="INSERT INTO options_mcq (`".implode("`,`", array_keys($insert))."`,`questionId`) VALUES ('".implode("','", array_values($insert))."',@questionId)";
							$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						}
					}
					//$question_files = new TableGateway('question_files', $adapter, null,new HydratingResultSet());
					$insert = array(
									//'questionId'	=>  $result->questionId,
									'stuff'			=>	$data->audio,
									'duration'		=>	123456
								);
					//$question_files->insert($insert);
					$query="INSERT INTO question_files (`".implode("`,`", array_keys($insert))."`,`questionId`) VALUES ('".implode("','", array_values($insert))."',@questionId)";
					$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				
				$query = "SELECT @questionId as questionID";
				$questionID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$questionID = $questionID->toArray();
				$result->questionId = $questionID[0]["questionID"];
				array_push($questionidarr,$result->questionId);
				
				if($data->questionType == 4 || $data->questionType == 5) {
					foreach($data->childQuestions as $newQuestion) {
						$newQuestion->parentId = $result->questionId;
						$newQuestion->subjectId = $data->subjectId;
						$newQuestion->userId = $data->userId;
						$e = new Exam();
						$e->saveQuestion($newQuestion);
					}
				}
				/*$e = new Exam();
				$e->updateCategoryStatus($data);*/
				if($data->questionType == 4 ) {
					
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('questions');
					$select->where(array('parentId'	=>	$questionidarr[0],'status' => 1,'delete' => 0));
					$select->columns(array('id'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$envalues = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$envalues = $envalues->toArray();
					$yovalue=count($envalues);
				}
				
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function saveQuestionExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;			
				$questionidarr= array();
				$resData = $this->saveQuestion($data);
				$result->questionId = $resData->questionId;

				$db->beginTransaction();

				if($result->questionId == 0) {
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}

				$qc_links = new TableGateway('question_category_links', $adapter, null,new HydratingResultSet());
				$insert = array(
					'questionId'		=>	$result->questionId,
					'categoryId'		=>	$data->categoryId,
				);
				$qc_links->insert($insert);

				array_push($questionidarr,$result->questionId);
				
				$newcountenter=0;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('section_categories');
				$select->where(array('id'	=>	$data->categoryId, 'delete' => 0));
				$select->columns(array('entered'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$enteredCount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$enteredCount = $enteredCount->toArray();
				if(count($enteredCount)>0)
				{
					$newcountenter=$enteredCount[0]['entered'];
				}
				
				if($result->questionId == 0) {
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}
				if($data->status == 1 && $data->questionType != 4 && $data->parentId == 0) {
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('section_categories');
					$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered + 1")));
					$update->where(array('id' => $data->categoryId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
				}
				else if($data->questionType == 4 && $data->status == 1) {
					$amount = 0;
					foreach($data->childQuestions as $newQuestion) {
						if($newQuestion->status == 1)
							$amount++;
					}
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('section_categories');
					$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered + ".$amount)));
					$update->where(array('id' => $data->categoryId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
				}

				if($data->questionType == 4 ) {
					
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('questions');
					$select->where(array('parentId'	=>	$questionidarr[0],'status' => 1,'delete' => 0));
					$select->columns(array('id'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$envalues = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$envalues = $envalues->toArray();
					$yovalue=count($envalues);
								
					$finalvalue=$newcountenter+$yovalue;
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('section_categories');
					$update->set(array('entered' => $finalvalue));
					$update->where(array('id' => $data->categoryId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
				}
				
				$sql = new Sql($adapter);
				$selectString = "UPDATE section_categories SET entered=(
									SELECT COUNT(*)
									FROM questions AS q
									INNER JOIN question_category_links AS qcl ON qcl.questionId = q.id
									WHERE qcl.categoryId={$data->categoryId} and q.parentId=0 
								) WHERE id={$data->categoryId}";
				$marks = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$e = new Exam();
				$e->updateCategoryStatus($data);
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function getQuestionDetails($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('questions');
				$select->where(array('id'			=>	$data->questionId,
									'parentId'		=>	0));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->question = $questions->toArray();

				/*$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('question_category_links');
				$select->where(array('questionId'	=>	$data->questionId,
										'examId'	=>	$data->examId));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->question = $questions->toArray();*/
				
				$query="SELECT qt.id,qt.tag AS name
						FROM `question_tag_links` AS qtl
						INNER JOIN `question_tags` AS qt ON qt.id=qtl.tag_id
						WHERE `question_id`={$data->questionId}";
				$tagValues = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->tags = $tagValues->toArray();

				$questionType = $result->question[0]['questionType'];
				if($questionType == 0 || $questionType == 6) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('options_mcq');
					$select->where(array('questionId' => $data->questionId));
					$select->columns(array('id','option','correct'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$options = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$result->options = $options->toArray();
				}
				else if($questionType == 1) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('true_false');
					$select->where(array('questionId' => $data->questionId));
					$select->columns(array('id','answer'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$answer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$result->answer = $answer->toArray();
				}
				else if($questionType == 2) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('ftb');
					$select->where(array('questionId' => $data->questionId));
					$select->columns(array('id','blanks'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$blanks = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$result->blanks = $blanks->toArray();
				}
				else if($questionType == 3) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('options_mtf');
					$select->where(array('questionId' => $data->questionId));
					$select->columns(array('id','columnA','columnB'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$matches = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$result->matches = $matches->toArray();
				}
				else if($questionType == 4 || $questionType == 5) {
					$childQuestions = new stdClass();
					$e = new Exam();
					$result->childQuestions = $e->getCmpChildQuestions($result->question[0]['id']);
				}
				else if($questionType == 7) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('options_mcq');
					$select->where(array('questionId' => $data->questionId));
					$select->columns(array('id','option','correct'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$options = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$result->options = $options->toArray();

					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('question_files');
					$select->where(array('questionId'	=>	$data->questionId));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$qFiles = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$qFiles = $qFiles->toArray();
					if (count($qFiles)>0) {
						$result->audio = $qFiles[0]['stuff'];
					}
				}
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function getCmpChildQuestions($parentId) {
			$result = array();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('questions');
				$select->where(array('parentId'	=>	$parentId,
									'delete'	=>	0));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$childs = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $childs->toArray();
				foreach($temp as $question) {
					$child = new stdClass();
					$questionType = $question['questionType'];
					$questionId = $question['id'];
					if($questionType == 0 || $questionType == 6) {
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('options_mcq');
						$select->where(array('questionId'	=>	$questionId,
												'delete'	=>	0));
						$select->columns(array('id','option','correct'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$options = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$answers = $options->toArray();
					}
					else if($questionType == 1) {
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('true_false');
						$select->where(array('questionId'	=>	$questionId,
												'delete'	=>	0));
						$select->columns(array('id','answer'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$answer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$answers = $answer->toArray();
					}
					else if($questionType == 2) {
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('ftb');
						$select->where(array('questionId'	=>	$questionId,
												'delete'	=>	0));
						$select->columns(array('id','blanks'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$blanks = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$answers = $blanks->toArray();
					}
					$child->answer = $answers;
					$child->question = $question;
					$result[] = $child;
				}
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function editQuestion($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('questions');
				$update->set(array(
									/*'ownerId'		=>	$data->userId,
									'subjectId'		=>	$data->subjectId,*/
									'questionType'	=>	$data->questionType,
									'question'		=>	$data->question,
									'description'	=>	$data->desc,
									'difficulty'	=>	((isset($data->difficulty))?$data->difficulty:0),
									'status'		=>	$data->status
							));
				$update->where(array('id' => $data->questionId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				$reqTags = array();
				if (!empty($data->tags)) {
					$reqTags = $data->tags;
					foreach ($data->tags as $key => $tag) {
						$query="SELECT *
								FROM `question_tags`
								WHERE `tag`= '$tag'";
						$tagCheck = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$tagCheck = $tagCheck->toArray();
						if (count($tagCheck)) {
							$tagId = $tagCheck[0]['id'];
							$query="SELECT *
									FROM `question_tag_links`
									WHERE `question_id`={$data->questionId} AND `tag_id`= '$tagId'";
							$tagValue = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$tagValue = $tagValue->toArray();
							if (count($tagValue)) {
								unset($data->tags[$key]);
							} else {
								$questionTags = new TableGateway('question_tag_links', $adapter, null,new HydratingResultSet());
								$insert = array(
												'question_id' => $data->questionId,
												'tag_id' => $tagId
											);
								$questionTags->insert($insert);
							}
						} else {
							$questionTags = new TableGateway('question_tags', $adapter, null,new HydratingResultSet());
							$insert = array(
											'tag'	=>  $tag
										);
							$questionTags->insert($insert);
							$tagId = $questionTags->getLastInsertValue();
							$questionTags = new TableGateway('question_tag_links', $adapter, null,new HydratingResultSet());
							$insert = array(
											'question_id' => $data->questionId,
											'tag_id' => $tagId
										);
							$questionTags->insert($insert);
						}
					}
				}
				$query="SELECT sqtl.id, qt.tag
						FROM `question_tag_links` AS sqtl
						INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
						WHERE sqtl.question_id= '$data->questionId'";
				$tagsInDb = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tagsInDb = $tagsInDb->toArray();
				if (count($tagsInDb)>0){
					if(!empty($reqTags)) {
						$deleteTagIds = array();
						foreach ($tagsInDb as $key => $tag) {
							if (in_array($tag['tag'], $reqTags)) {
								unset($tagsInDb[$key]);
							} else {
								$deleteTagIds[] = $tag['id'];
							}
						}
						if (!empty($deleteTagIds)) {
							$delete = $sql->delete();
							$delete->from('question_tag_links');
							$delete->where(array('id'	=>	$deleteTagIds));
							$statement = $sql->prepareStatementForSqlObject($delete);
							$statement->execute();
						}
					} else {
						$delete = $sql->delete();
						$delete->from('question_tag_links');
						$delete->where(array('question_id'	=>	$data->questionId));
						$statement = $sql->prepareStatementForSqlObject($delete);
						$statement->execute();
					}
				}
				if($data->questionType == 0 || $data->questionType == 6) {
					$sql = new Sql($this->adapter);
					$delete = $sql->delete();
					$delete->from('options_mcq');
					$delete->where(array('questionId' => $data->questionId));
					$statement = $sql->prepareStatementForSqlObject($delete);
					$statement->execute();
					if(count($data->options) > 0) {
						for($i=0; $i<count($data->options); $i++) {
							$options_mcq = new TableGateway('options_mcq', $adapter, null,new HydratingResultSet());
							$insert = array(
											'questionId'	=>  $data->questionId,
											'parentId'		=>	$data->parentId,
											'option'		=>	$data->options[$i]->opt,
											'correct'		=>	$data->options[$i]->cState
										);
							$options_mcq->insert($insert);
						}
						//$adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);
					}
				}
				else if($data->questionType == 1) {
					$sql = new Sql($this->adapter);
					$delete = $sql->delete();
					$delete->from('true_false');
					$delete->where(array('questionId' => $data->questionId));
					$statement = $sql->prepareStatementForSqlObject($delete);
					$statement->execute();
					if($data->answer !== '') {
						$insertString = "INSERT INTO true_false (questionId, parentId, answer) VALUES('{$data->questionId}','{$data->parentId}',{$data->answer})";
						$adapter->query($insertString, $adapter::QUERY_MODE_EXECUTE);
					}
				}
				else if($data->questionType == 2) {
					$sql = new Sql($this->adapter);
					$delete = $sql->delete();
					$delete->from('ftb');
					$delete->where(array('questionId' => $data->questionId));
					$statement = $sql->prepareStatementForSqlObject($delete);
					$statement->execute();
					if(count($data->blanks) > 0) {
						$insert = "INSERT INTO ftb (questionId, parentId, blanks) VALUES";
						for($i=0; $i<count($data->blanks); $i++) {
							$insert .= "('{$data->questionId}','{$data->parentId}','{$data->blanks[$i]->ftb}'),";
						}
						$insert = substr($insert, 0, strlen($insert)-1);
						$adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);
					}
				}
				else if($data->questionType == 3) {
					$sql = new Sql($this->adapter);
					$delete = $sql->delete();
					$delete->from('options_mtf');
					$delete->where(array('questionId' => $data->questionId));
					$statement = $sql->prepareStatementForSqlObject($delete);
					$statement->execute();
					if(count($data->matches) > 0) {
						/*$insert = "INSERT INTO options_mtf (questionId, columnA, columnB) VALUES";
						for($i=0; $i<count($data->matches); $i++) {
							$insert .= "('{$data->questionId}','{$data->matches[$i]->optA}','{$data->matches[$i]->optB}'),";
						}
						$insert = substr($insert, 0, strlen($insert)-1);
						$adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);*/
						for($i=0; $i<count($data->matches); $i++) {
							$options_mtf = new TableGateway('options_mtf', $adapter, null,new HydratingResultSet());
							$insert = array(
											'questionId'	=>  $data->questionId,
											'columnA'		=>	$data->matches[$i]->optA,
											'columnB'		=>	$data->matches[$i]->optB
										);
							$options_mtf->insert($insert);
						}
					}
				}
				else if($data->questionType == 4 || $data->questionType == 5) {
					$delIds = '';
					foreach($data->childQuestions as $newQuestion) {
						$newQuestion->parentId = $data->questionId;
						$newQuestion->subjectId = $data->subjectId;
						$newQuestion->userId = $data->userId;
						$e = new Exam();
						if($newQuestion->questionId == 0) {
							$temp = $e->saveQuestion($newQuestion);
							$delIds .= $temp->questionId.',';
						}
						else {
							$e->editQuestion($newQuestion);
							$delIds .= $newQuestion->questionId.',';
						}
					}
					$delIds = substr($delIds, 0, strlen($delIds) - 1);
					$deleteQuestions = "DELETE FROM questions WHERE id NOT IN (".$delIds.") AND parentId=".$data->questionId.";";
					$deleteTF = "DELETE FROM true_false WHERE questionId NOT IN (".$delIds.") AND parentId=".$data->questionId.";";
					$deleteMCQ = "DELETE FROM options_mcq WHERE questionId NOT IN (".$delIds.") AND parentId=".$data->questionId.";";
					$deleteFTB = "DELETE FROM ftb WHERE questionId NOT IN (".$delIds.") AND parentId=".$data->questionId.";";
					$adapter->query($deleteQuestions, $adapter::QUERY_MODE_EXECUTE);
					$adapter->query($deleteTF, $adapter::QUERY_MODE_EXECUTE);
					$adapter->query($deleteMCQ, $adapter::QUERY_MODE_EXECUTE);
					$adapter->query($deleteFTB, $adapter::QUERY_MODE_EXECUTE);
				}
				else if($data->questionType == 7) {
					$sql = new Sql($this->adapter);
					$delete = $sql->delete();
					$delete->from('options_mcq');
					$delete->where(array('questionId' => $data->questionId));
					$statement = $sql->prepareStatementForSqlObject($delete);
					$statement->execute();
					if(count($data->options) > 0) {
						for($i=0; $i<count($data->options); $i++) {
							$options_mcq = new TableGateway('options_mcq', $adapter, null,new HydratingResultSet());
							$insert = array(
											'questionId'	=>  $data->questionId,
											'parentId'		=>	$data->parentId,
											'option'		=>	$data->options[$i]->opt,
											'correct'		=>	$data->options[$i]->cState
										);
							$options_mcq->insert($insert);
						}
					}
					$select = $sql->select();
					$select->from('question_files');
					$select->where(array('questionId' => $data->questionId));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$qFiles = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$qFiles = $qFiles->toArray();
					if (count($qFiles)>0) {
						$select = $sql->update();
						$select->table('question_files');
						$select->set(array(
										'questionId'	=>  $data->questionId,
										'stuff'			=>	$data->audio,
										'duration'		=>	123456
									));
						$select->where(array('questionId'=>$data->questionId));
						$statement = $sql->prepareStatementForSqlObject($select);
						$statement->execute();
					} else {
						$question_files = new TableGateway('question_files', $adapter, null,new HydratingResultSet());
						$insert = array(
										'questionId'	=>  $data->questionId,
										'stuff'			=>	$data->audio,
										'duration'		=>	123456
									);
						$question_files->insert($insert);
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$sql = new Sql($adapter);
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function editQuestionExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				//if category has been switched
				if($data->categorySwitch == true) {
					//if question is other than cmp
					if($data->questionType != 4 && $data->parentId == 0) {
						//decrease 1 from old category
						//need to check if question status is 0 then don't decrease the count
						if($data->status == 1) {
							$sql = new Sql($adapter);
							$update = $sql->update();
							$update->table('section_categories');
							$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered - 1")));
							$update->where(array('id' => $data->oldCategory));
							$statement = $sql->prepareStatementForSqlObject($update);
							$statement->execute();
							//increase 1 to new category if status is complete
							$sql = new Sql($adapter);
							$update = $sql->update();
							$update->table('section_categories');
							$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered + 1")));
							$update->where(array('id' => $data->categoryId));
							$statement = $sql->prepareStatementForSqlObject($update);
							$statement->execute();
						}
					}//if question type is cmp
					else if($data->questionType == 4) {
						//decrease from old category such many question were complete inside it
						$sql = new Sql($adapter);
						$update = $sql->update();
						$update->table('section_categories');
						$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered - (SELECT COUNT(*) FROM questions WHERE `delete`=0 AND `status`=1 AND parentId=".$data->questionId.")")));
						$update->where(array('id' => $data->oldCategory));

						$statement = $sql->prepareStatementForSqlObject($update);
						$statement->execute();
						$amount = 0;
						//increse into new category such many question as are not new and status is complete
						foreach($data->childQuestions as $newQuestion) {
							if($newQuestion->questionId != 0 && $newQuestion->status == 1)
								$amount++;
						}
						if($amount != 0) {
						//print_r("amount !0");
							$sql = new Sql($adapter);
							$update = $sql->update();
							$update->table('section_categories');
							$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered + ".$amount)));
							$update->where(array('id' => $data->categoryId));
							//print_r($update);
							$statement = $sql->prepareStatementForSqlObject($update);
							$statement->execute();
						}
					}
				}//if category is not switched
				else {
					if($data->questionType != 4 && $data->parentId == 0) {
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('questions');
						$select->where(array('id'	=>	$data->questionId));
						$select->columns(array('status'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$oldStatus = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$oldStatus = $oldStatus->toArray();
						if($oldStatus[0]['status'] != 1 && $data->status == 1) {
							$sql = new Sql($adapter);
							$update = $sql->update();
							$update->table('section_categories');
							$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered + 1")));
							$update->where(array('id' => $data->categoryId));
							$statement = $sql->prepareStatementForSqlObject($update);
							$statement->execute();
						}
						else if($oldStatus[0]['status'] == 1 && ($data->status == 0 || $data->status == 2)) {
							$sql = new Sql($adapter);
							$sql = new Sql($adapter);
							$update = $sql->update();
							$update->table('section_categories');
							$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered - 1")));
							$update->where(array('id' => $data->categoryId));
							$statement = $sql->prepareStatementForSqlObject($update);
							$statement->execute();
						}
					}
					else if($data->questionType == 4) {
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('section_categories');
						$select->where(array('id'	=>	$data->categoryId, 'delete' => 0));
						$select->columns(array('entered'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$enteredCount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$enteredCount = $enteredCount->toArray();
						$newcountenter=$enteredCount[0]['entered'];
						$yovalue=0;									
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('questions');
						$select->where(array('parentId'	=>	$data->questionId,'status' => 1,'delete' => 0));
						$select->columns(array('id'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$envalues = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$envalues = $envalues->toArray();
						$yovalue=count($envalues);
						$counter=0;
						$kill=0;
						$amount = 0;//need to change							
					
						foreach($data->childQuestions as $newQuestion) {
							//print_r($newQuestion);
							$counter=$counter+1;
							if( ($newQuestion->status == 0 || $newQuestion->status == 2)){
								$kill++;
							}
						}						
						$counter=$counter-$kill;
						if($newcountenter==0)
						{
							$newenteredvalue=	(($yovalue));
						} else {
							$newenteredvalue=	(($newcountenter) - ($yovalue) +($counter));	
						}		
						$sql = new Sql($adapter);
						$update = $sql->update();
						$update->table('section_categories');
						$update->set(array('entered' => $newenteredvalue));
							
						$update->where(array('id' => $data->categoryId));
						//print_r($update);
						$statement = $sql->prepareStatementForSqlObject($update);
						$statement->execute();
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();

				$resData = $this->editQuestion($data);

				$db->beginTransaction();

				$sql = new Sql($adapter);
				$selectString = "UPDATE section_categories SET entered=(
									SELECT COUNT(*)
									FROM questions AS q
									INNER JOIN question_category_links AS qcl ON qcl.questionId = q.id
									WHERE qcl.categoryId={$data->categoryId} and q.parentId=0 
								) WHERE id={$data->categoryId}";
				$marks = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$e = new Exam();
				$e->updateCategoryStatus($data);
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function getExam($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('e' => 'exams'))
							->join(array('c' => 'chapters'),
							'e.chapterId = c.id',
							array('chapterName' => 'name'),
							$select::JOIN_LEFT)
							->order('id DESC');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('e.id', $data->examId);
				//$where->notEqualTo('delete', 2);
								//removing temprory deleted exams.
								// implementing not showing deleted exams
								$where->notEqualTo('delete',1);
				$select->where($where);
				$select->columns(array('id' 	=> 'id',
									'name' 		=> 'name',
									'type' 		=> 'type',
									'chapterId' => 'chapterId',
									'subjectId' => 'subjectId',
									'status' 	=> 'status',
									'startDate' => 'startDate',
									'endDate' 	=> 'endDate',
									'attempts' 	=> 'attempts',
									'totalTime'	=> 'totalTime',
									'gapTime'	=> 'gapTime',
									'sectionOrder'	=> 'sectionOrder',
									'powerOption'	=> 'powerOption',
									'endBeforeTime'	=> 'endBeforeTime',
									'showResult'	=>'showResult',
									'showResultTill' =>'showResultTill',
									'showResultTillDate'=>'showResultTillDate'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$tempExam = $exams->toArray();
				if(count($tempExam) == 0) {
					$result->status = 0;
					$result->message= "No such exam found.";
					return $result;
				}
				$result->exam = $tempExam[0];
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exam_sections')
						->order('weight ASC')
						->order('id ASC');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('examId', $result->exam['id']);
				//$where->notEqualTo('delete', 2);
								//removing temprory deleted sections.
								// implementing not showing deleted sections
								$where->notEqualTo('delete',1);
				$select->where($where);
				$select->columns(array('id', 'name', 'time', 'status'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$sections = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->sections = $sections->toArray();
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		//saveSubjectiveQuestion
		public function saveSubjectiveQuestion($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$questionArrays= json_decode($data->questions);
				$fileLocation='';
				foreach($questionArrays as $key=>$value)
				{	
					//print_r($value);
					$stringTOStore='';
					$stringTOStoreAns='';
					$stringTOStore=$value->question;
					$stringTOStoreAns=$value->answer;
					$noOfSubquestionRequired=0;
					$ext		 = 'txt';
					$parentId    =  0;	
					if($value->questionType == 'questionGroup')
					{
						$noOfSubquestionRequired=$value->answer;
					}
					if($stringTOStore !='')
					{	
						$fileName="Questions".$data->subjectId.$value->id.$data->courseId.time().'.'.$ext;
						$fileLocation = getcwd().'/user-data/SubjectiveQues/SubjectiveQuestions/'. $fileName;
						$file = fopen($fileLocation,"w");
						fwrite($file,$stringTOStore);
						fclose($file);	
						if(file_exists($fileLocation)){	
							$filesDirAws = "/user-data/SubjectiveQues/SubjectiveQuestions/";				
							$filePathAws = $filesDirAws.$fileName;
							//print_r($filePathAws);
							require_once (getcwd().'/api/amazonUpload.php');
							uploadFile($filePathAws,$fileLocation,$ext);
							unlink($fileLocation);
							
						}
						$config =  require "cloudFront.php";
						$completePath= $config.$filePathAws;
						//print_r($completePath);
						
						$fileName="Answers".$data->subjectId.$value->id.$data->courseId.time().'.'.$ext;
						$fileLocation = getcwd().'/user-data/SubjectiveQues/SubjectiveQuestionsAns/'. $fileName;
						$file = fopen($fileLocation,"w");
						fwrite($file,$stringTOStoreAns);
						fclose($file);	
						if(file_exists($fileLocation)){	
							$filesDirAws = "/user-data/SubjectiveQues/SubjectiveQuestionsAns/";				
							$filePathAws = $filesDirAws.$fileName;
							//print_r($filePathAws);
							require_once (getcwd().'/api/amazonUpload.php');
							uploadFile($filePathAws,$fileLocation,$ext);
							unlink($fileLocation);
							
							}
						$config =  require "cloudFront.php";
						$completePathAns= $config.$filePathAws;
						
						$questions_subjective = new TableGateway('questions_subjective', $adapter, null,new HydratingResultSet());
						$insert = array(
										'examId'				=>  $data->subjectiveExamId,
										'question'				=>	$completePath,
										'answer'					=>	$completePathAns,
										'deleted'						=>	0,
										'parentId'						=>	0,
										'noOfSubquestionRequired'		=>  $noOfSubquestionRequired,				
										'status'						=>	1
									);
						$questions_subjective->insert($insert);
						$parentId = $questions_subjective->getLastInsertValue();
						
						
					}
					if(sizeof($value->subQuestions>0))
					{		
						
						$subQuesArray=$value->subQuestions;
						foreach($subQuesArray as $key=>$value1)
						{
							$stringTOStoreSubQuestion=$value1->question;
							$stringTOStoreSubQuestionAns=$value1->answer;;
							if($stringTOStoreSubQuestion !='')
							{
								$fileName="SubQuestions".$data->subjectId.$value1->id.$data->courseId.time().'.'.$ext;
								$fileLocation = getcwd().'/user-data/SubjectiveQues/SubjectiveQuestions/'. $fileName;
								$file = fopen($fileLocation,"w");
								fwrite($file,$stringTOStore);
								fclose($file);	
								if(file_exists($fileLocation)){	
									$filesDirAws = "/user-data/SubjectiveQues/SubjectiveQuestions/";				
									$filePathAws = $filesDirAws.$fileName;
									//print_r($filePathAws);
									require_once (getcwd().'/api/amazonUpload.php');
									uploadFile($filePathAws,$fileLocation,$ext);
									unlink($fileLocation);
									
								}
								$config =  require "cloudFront.php";
								$completePathSub= $config.$filePathAws;
								//print_r($completePathSub);
								$fileName="SubAnswers".$data->subjectId.$value1->id.$data->courseId.time().'.'.$ext;
								$fileLocation = getcwd().'/user-data/SubjectiveQues/SubjectiveQuestionsAns/'. $fileName;
								$file = fopen($fileLocation,"w");
								fwrite($file,$stringTOStoreAns);
								fclose($file);	
								if(file_exists($fileLocation)){	
											$filesDirAws = "/user-data/SubjectiveQues/SubjectiveQuestionsAns/";				
											$filePathAws = $filesDirAws.$fileName;
											//print_r($filePathAws);
											require_once (getcwd().'/api/amazonUpload.php');
											uploadFile($filePathAws,$fileLocation,$ext);
											unlink($fileLocation);
									}
								$config =  require "cloudFront.php";
								$completePathAnsSub= $config.$filePathAws;
								$questions_subjective = new TableGateway('questions_subjective', $adapter, null,new HydratingResultSet());
								$insert = array(
											'examId'				=>  $data->subjectiveExamId,
											'question'				=>	$completePathSub,
											'answer'					=>	$completePathAnsSub,
											'deleted'						=>	0,
											'parentId'						=>	$parentId,
											'noOfSubquestionRequired'		=>  0,					
											'status'						=>	1
											);
								$questions_subjective->insert($insert);
								$data1 = $questions_subjective->getLastInsertValue();
								
							}																					
						}//for closed
					}				
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				//print_r($data);
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		//
		public function insertSubjectiveQuestion($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			//print_r($data);
			$questionArrays= json_decode($data->questions);
			//print_r($questionArrays);
			$fileLocation='';
			$parentId	=	0;
			$questionId	=	0;
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$question = $questionArrays->question;
				$answer   = $questionArrays->answer;
				$noOfSubquestionRequired=0;
				$questionsonPage	= 0;
				$difficulty			= 0;
				if($data->parent !='')
				{
					$parentId  =  $data->parent;
				}
							
				if($questionArrays->questionType == 'questionGroup')
				{
					$noOfSubquestionRequired=$questionArrays->answer;
					$questionsonPage=$questionArrays->questionsonPage;
				}else{
					if (!isset($data->parent) || empty($data->parent)) {
						$difficulty=$data->difficulty;
					}
				}
				$questions_subjective = new TableGateway('questions_subjective', $adapter, null,new HydratingResultSet());
				$insert = array(
								'subjectId'				=>	$data->subjectId,
								'ownerId'				=>	$data->userId,
								'question'				=>	$question,
								'answer'				=>	$answer,
								'questionsonPage'		=>  $questionsonPage,
								'deleted'				=>	0,
								'parentId'				=>	$parentId,
								'difficulty'			=>  $difficulty,
								'noOfSubquestionRequired'=> $noOfSubquestionRequired,
								'status'				=>	1
							);
				$questions_subjective->insert($insert);
				$result->questionId = $questions_subjective->getLastInsertValue();

				$reqTags = array();
				if (!empty($data->tags)) {
					$reqTags = $data->tags;
					foreach ($data->tags as $key => $tag) {
						$query="SELECT *
								FROM `question_tags`
								WHERE `tag`= '$tag'";
						$tagCheck = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$tagCheck = $tagCheck->toArray();
						if (count($tagCheck)) {
							$tagId = $tagCheck[0]['id'];
							$questionTags = new TableGateway('subjective_question_tag_links', $adapter, null,new HydratingResultSet());
							$insert = array(
											'question_id' => $result->questionId,
											'tag_id' => $tagId
										);
							$questionTags->insert($insert);
						} else {
							$questionTags = new TableGateway('question_tags', $adapter, null,new HydratingResultSet());
							$insert = array(
											'tag'	=>  $tag
										);
							$questionTags->insert($insert);
							$tagId = $questionTags->getLastInsertValue();
							//var_dump('2: '.$tagId);
							$questionTags = new TableGateway('subjective_question_tag_links', $adapter, null,new HydratingResultSet());
							$insert = array(
											'question_id' => $result->questionId,
											'tag_id' => $tagId
										);
							$questionTags->insert($insert);
						}
					}
				}
				$query="SELECT sqtl.id, qt.tag
						FROM `subjective_question_tag_links` AS sqtl
						INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
						WHERE sqtl.question_id= '$result->questionId'";
				$tagsInDb = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tagsInDb = $tagsInDb->toArray();
				if (count($tagsInDb)>0){
					if(!empty($reqTags)) {
						$deleteTagIds = array();
						foreach ($tagsInDb as $key => $tag) {
							if (in_array($tag['tag'], $reqTags)) {
								unset($tagsInDb[$key]);
							} else {
								$deleteTagIds[] = $tag['id'];
							}
						}
						if (!empty($deleteTagIds)) {
							$delete = $sql->delete();
							$delete->from('subjective_question_tag_links');
							$delete->where(array('id'	=>	$deleteTagIds));
							$statement = $sql->prepareStatementForSqlObject($delete);
							$statement->execute();
						}
					} else {
						$delete = $sql->delete();
						$delete->from('subjective_question_tag_links');
						$delete->where(array('question_id'	=>	$result->questionId));
						$statement = $sql->prepareStatementForSqlObject($delete);
						$statement->execute();
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$query="SELECT qt.id, qt.tag AS name
						FROM `subjective_question_tag_links` AS sqtl
						INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
						WHERE sqtl.question_id= '$result->questionId'";
				$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tags = $tags->toArray();

				$result->questionBrief = $this->displayString($question);
				$result->answerBrief = $this->displayString($answer);
				$result->tags = $tags;
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function insertSubjectiveQuestionExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			//print_r($data);
			$questionArrays= json_decode($data->questions);
			//print_r($questionArrays);
			$fileLocation='';
			$marks		=	0;
			$parentId	=	0;
			$questionId	=	0;
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$question = $questionArrays->question;
				$answer   = $questionArrays->answer;
				$noOfSubquestionRequired=0;
				$questionsonPage	= 0;
				$difficulty			= 0;
				if($data->parent !='')
				{
					$parentId  =  $data->parent;
				}

				//var_dump($questionArrays);
							
				if($questionArrays->questionType == 'questionGroup')
				{
					$noOfSubquestionRequired=$questionArrays->answer;
					$questionsonPage=$questionArrays->questionsonPage;
					$marks=$questionArrays->subquestionMrks;
				}else{
					$marks=$questionArrays->marks;
					if (!isset($data->parent) || empty($data->parent)) {
						$difficulty=$data->difficulty;
					}
				}
				$questions_subjective = new TableGateway('questions_subjective', $adapter, null,new HydratingResultSet());
				$insert = array(
								'subjectId'				=>	$data->subjectId,
								'ownerId'				=>	$data->userId,
								'question'				=>	$question,
								'answer'				=>	$answer,
								'questionsonPage'		=>  $questionsonPage,
								'deleted'				=>	0,
								'parentId'				=>	$parentId,
								//'marks'					=>  $marks,
								'difficulty'			=>  $difficulty,
								'noOfSubquestionRequired'=> $noOfSubquestionRequired,
								'status'				=>	1
							);
				$questions_subjective->insert($insert);
				$result->questionId = $questions_subjective->getLastInsertValue();

				//var_dump($result->questionId);

				$questions_subjective_links = new TableGateway('question_subjective_exam_links', $adapter, null,new HydratingResultSet());
				$insert = array(
								'questionId'			=>	$result->questionId,
								'examId'				=>	$data->examId,
								'marks'					=>  $marks,
							);
				$questions_subjective_links->insert($insert);

				$reqTags = array();
				if (!empty($data->tags)) {
					$reqTags = $data->tags;
					foreach ($data->tags as $key => $tag) {
						$query="SELECT *
								FROM `question_tags`
								WHERE `tag`= '$tag'";
						$tagCheck = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$tagCheck = $tagCheck->toArray();
						if (count($tagCheck)) {
							$tagId = $tagCheck[0]['id'];
							$questionTags = new TableGateway('subjective_question_tag_links', $adapter, null,new HydratingResultSet());
							$insert = array(
											'question_id' => $result->questionId,
											'tag_id' => $tagId
										);
							$questionTags->insert($insert);
						} else {
							$questionTags = new TableGateway('question_tags', $adapter, null,new HydratingResultSet());
							$insert = array(
											'tag'	=>  $tag
										);
							$questionTags->insert($insert);
							$tagId = $questionTags->getLastInsertValue();
							//var_dump('2: '.$tagId);
							$questionTags = new TableGateway('subjective_question_tag_links', $adapter, null,new HydratingResultSet());
							$insert = array(
											'question_id' => $result->questionId,
											'tag_id' => $tagId
										);
							$questionTags->insert($insert);
						}
					}
				}
				$query="SELECT sqtl.id, qt.tag
						FROM `subjective_question_tag_links` AS sqtl
						INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
						WHERE sqtl.question_id= '$result->questionId'";
				$tagsInDb = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tagsInDb = $tagsInDb->toArray();
				if (count($tagsInDb)>0){
					if(!empty($reqTags)) {
						$deleteTagIds = array();
						foreach ($tagsInDb as $key => $tag) {
							if (in_array($tag['tag'], $reqTags)) {
								unset($tagsInDb[$key]);
							} else {
								$deleteTagIds[] = $tag['id'];
							}
						}
						if (!empty($deleteTagIds)) {
							$delete = $sql->delete();
							$delete->from('subjective_question_tag_links');
							$delete->where(array('id'	=>	$deleteTagIds));
							$statement = $sql->prepareStatementForSqlObject($delete);
							$statement->execute();
						}
					} else {
						$delete = $sql->delete();
						$delete->from('subjective_question_tag_links');
						$delete->where(array('question_id'	=>	$result->questionId));
						$statement = $sql->prepareStatementForSqlObject($delete);
						$statement->execute();
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$query="SELECT qt.id, qt.tag AS name
						FROM `subjective_question_tag_links` AS sqtl
						INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
						WHERE sqtl.question_id= '$result->questionId'";
				$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tags = $tags->toArray();

				$result->questionBrief = $this->displayString($question);
				$result->answerBrief = $this->displayString($answer);
				$result->tags = $tags;
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		//editSubjectiveQuestion
		public function editSubjectiveQuestion($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$questionArrays= json_decode($data->questions);
			//print_r($questionArrays);
			$fileLocation='';
			$parentId   = 0;
			$questionId = 0;
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$question = $questionArrays->question;
				$answer   = $questionArrays->answer;
				$questionId = $questionArrays->id;
				$noOfSubquestionRequired=0;
				$questionsonPage	= 0;
				$difficulty			= 0;
				if($data->parent != '')
				{
					$parentId  =  $data->parent;
				}

				if($questionArrays->questionType == 'questionGroup')
				{
					$noOfSubquestionRequired=$questionArrays->answer;
					$questionsonPage=$questionArrays->questionsonPage;
				}else{
					if (!isset($data->parent) || empty($data->parent)) {
						$difficulty=$data->difficulty;
					}	
				}
				$reqTags = array();
				if (!empty($data->tags)) {
					$reqTags = $data->tags;
					foreach ($data->tags as $key => $tag) {
						$query="SELECT *
								FROM `question_tags`
								WHERE `tag`= '$tag'";
						$tagCheck = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$tagCheck = $tagCheck->toArray();
						if (count($tagCheck)) {
							$tagId = $tagCheck[0]['id'];
							$questionTags = new TableGateway('subjective_question_tag_links', $adapter, null,new HydratingResultSet());
							$insert = array(
											'question_id' => $questionId,
											'tag_id' => $tagId
										);
							$questionTags->insert($insert);
						} else {
							$questionTags = new TableGateway('question_tags', $adapter, null,new HydratingResultSet());
							$insert = array(
											'tag'	=>  $tag
										);
							$questionTags->insert($insert);
							$tagId = $questionTags->getLastInsertValue();
							//var_dump('2: '.$tagId);
							$questionTags = new TableGateway('subjective_question_tag_links', $adapter, null,new HydratingResultSet());
							$insert = array(
											'question_id' => $questionId,
											'tag_id' => $tagId
										);
							$questionTags->insert($insert);
						}
					}
				}
				$query="SELECT sqtl.id, qt.tag
						FROM `subjective_question_tag_links` AS sqtl
						INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
						WHERE sqtl.question_id= '$questionId'";
				$tagsInDb = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tagsInDb = $tagsInDb->toArray();
				if (count($tagsInDb)>0){
					if(!empty($reqTags)) {
						$deleteTagIds = array();
						foreach ($tagsInDb as $key => $tag) {
							if (in_array($tag['tag'], $reqTags)) {
								unset($tagsInDb[$key]);
							} else {
								$deleteTagIds[] = $tag['id'];
							}
						}
						if (!empty($deleteTagIds)) {
							$delete = $sql->delete();
							$delete->from('subjective_question_tag_links');
							$delete->where(array('id'	=>	$deleteTagIds));
							$statement = $sql->prepareStatementForSqlObject($delete);
							$statement->execute();
						}
					} else {
						$delete = $sql->delete();
						$delete->from('subjective_question_tag_links');
						$delete->where(array('question_id'	=>	$questionId));
						$statement = $sql->prepareStatementForSqlObject($delete);
						$statement->execute();
					}
				}


				$select = $sql->update();
				$select->table('questions_subjective');
				$select->set(array(
								'question'				=>	$question,
								'answer'				=>	$answer,
								'questionsonPage'		=>  $questionsonPage,
								'deleted'				=>	0,
								'parentId'				=>	$parentId,
								'difficulty'			=>  $difficulty,
								'noOfSubquestionRequired'=>  $noOfSubquestionRequired,				
								'status'				=>	1
							));
				$select->where(array('id'=>$questionId));
				$statement = $sql->prepareStatementForSqlObject($select);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();

				$query="SELECT qt.id, qt.tag AS name
						FROM `subjective_question_tag_links` AS sqtl
						INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
						WHERE sqtl.question_id= '$questionId'";
				$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tags = $tags->toArray();
				
				$result->questionBrief = $this->displayString($question);
				$result->answerBrief = $this->displayString($answer);
				$result->tags = $tags;
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		//
		public function editSubjectiveQuestionExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$questionArrays= json_decode($data->questions);
			//print_r($questionArrays);
			$fileLocation='';
			$marks		= 0;
			$parentId   = 0;
			$questionId = 0;
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$question = $questionArrays->question;
				$answer   = $questionArrays->answer;
				$questionId = $questionArrays->id;
				$noOfSubquestionRequired=0;
				$questionsonPage	= 0;
				$difficulty			= 0;
				if($data->parent != '')
				{
					$parentId  =  $data->parent;
				}

				if($questionArrays->questionType == 'questionGroup')
				{
					$noOfSubquestionRequired=$questionArrays->answer;
					$questionsonPage=$questionArrays->questionsonPage;
					$marks=$questionArrays->subquestionMrks;
				}else{
					$marks=$questionArrays->marks;
					if (!isset($data->parent) || empty($data->parent)) {
						$difficulty=$data->difficulty;
					}	
				}
				$reqTags = array();
				if (!empty($data->tags)) {
					$reqTags = $data->tags;
					foreach ($data->tags as $key => $tag) {
						$query="SELECT *
								FROM `question_tags`
								WHERE `tag`= '$tag'";
						$tagCheck = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$tagCheck = $tagCheck->toArray();
						if (count($tagCheck)) {
							$tagId = $tagCheck[0]['id'];
							$questionTags = new TableGateway('subjective_question_tag_links', $adapter, null,new HydratingResultSet());
							$insert = array(
											'question_id' => $questionId,
											'tag_id' => $tagId
										);
							$questionTags->insert($insert);
						} else {
							$questionTags = new TableGateway('question_tags', $adapter, null,new HydratingResultSet());
							$insert = array(
											'tag'	=>  $tag
										);
							$questionTags->insert($insert);
							$tagId = $questionTags->getLastInsertValue();
							//var_dump('2: '.$tagId);
							$questionTags = new TableGateway('subjective_question_tag_links', $adapter, null,new HydratingResultSet());
							$insert = array(
											'question_id' => $questionId,
											'tag_id' => $tagId
										);
							$questionTags->insert($insert);
						}
					}
				}
				$query="SELECT sqtl.id, qt.tag
						FROM `subjective_question_tag_links` AS sqtl
						INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
						WHERE sqtl.question_id= '$questionId'";
				$tagsInDb = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tagsInDb = $tagsInDb->toArray();
				if (count($tagsInDb)>0){
					if(!empty($reqTags)) {
						$deleteTagIds = array();
						foreach ($tagsInDb as $key => $tag) {
							if (in_array($tag['tag'], $reqTags)) {
								unset($tagsInDb[$key]);
							} else {
								$deleteTagIds[] = $tag['id'];
							}
						}
						if (!empty($deleteTagIds)) {
							$delete = $sql->delete();
							$delete->from('subjective_question_tag_links');
							$delete->where(array('id'	=>	$deleteTagIds));
							$statement = $sql->prepareStatementForSqlObject($delete);
							$statement->execute();
						}
					} else {
						$delete = $sql->delete();
						$delete->from('subjective_question_tag_links');
						$delete->where(array('question_id'	=>	$questionId));
						$statement = $sql->prepareStatementForSqlObject($delete);
						$statement->execute();
					}
				}


				$select = $sql->update();
				$select->table('questions_subjective');
				$select->set(array(
								'question'				=>	$question,
								'answer'				=>	$answer,
								'questionsonPage'		=>  $questionsonPage,
								'deleted'				=>	0,
								'parentId'				=>	$parentId,
								//'marks'					=>  $marks,
								'difficulty'			=>  $difficulty,
								'noOfSubquestionRequired'=>  $noOfSubquestionRequired,				
								'status'				=>	1
							));
				$select->where(array('id'=>$questionId));
				$statement = $sql->prepareStatementForSqlObject($select);
				$statement->execute();

				$select = $sql->update();
				$select->table('question_subjective_exam_links');
				$select->set(array(
								'marks'					=>  $marks
							));
				$select->where(array('questionId'=>$questionId,'examId'=>$data->subjectiveExamId));
				$statement = $sql->prepareStatementForSqlObject($select);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();

				$query="SELECT qt.id, qt.tag AS name
						FROM `subjective_question_tag_links` AS sqtl
						INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
						WHERE sqtl.question_id= '$questionId'";
				$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tags = $tags->toArray();
				
				$result->questionBrief = $this->displayString($question);
				$result->answerBrief = $this->displayString($answer);
				$result->tags = $tags;
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function deleteSubjectiveQuestion($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$questionArrays= json_decode($data->questions);
			$questionId =0;
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				/*if($data->parent =='')
				{
					$questionId=  $data->subjectiveExamId.$questionArrays->id;
				}else{
					
					$questionId=  $data->subjectiveExamId.$data->parent.$questionArrays[0]->id;
					//$parentId  =  $data->subjectiveExamId.$data->parent;
				}*/
				$questionId=  $questionArrays->id;

				$select = $sql->update();
				$select->table('questions_subjective');
			    $select->set(array(
								'deleted'	=>	1
							));
				$select->where
						    ->equalTo('id',$questionId)
							->or->equalTo('parentId',$questionId);
				$statement = $sql->prepareStatementForSqlObject($select);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function liveSubjectiveQuestion($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->update();
				$select->table('exam_subjective');
			    $select->set(array(
				      'status' => 2
				 ));
				$select->where(array('id'=>$data->subjectiveExamId));
				$statement = $sql->prepareStatementForSqlObject($select);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function unliveSubjectiveQuestion($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->update();
				$select->table('exam_subjective');
				$select->set(array(
						'status' => 1
				 ));
				$select->where(array('id'=>$data->subjectiveExamId));
				$statement = $sql->prepareStatementForSqlObject($select);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function getCheckSubjectiveExamStudents($data) {
			$result = new stdClass();
			try {
				$subjectiveExamArray=array();
				$studentIds=array();
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$query="SELECT sd.userId, concat(sd.firstName,' ',sd.lastName) AS name
						FROM `subjective_exam_attempts` AS sa
						LEFT JOIN `student_details` AS sd ON sd.userId=sa.studentId
						WHERE sa.examId={$data->examId} AND sa.completed=1 GROUP BY sa.studentId ORDER BY sa.studentId";
				$subjectiveStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$students = $subjectiveStudents->toArray();
				//print_r($students);
				$i = 0;
				foreach($students as $key=>$value)
				{
					$subjectiveExamArray[$i] = $value;
					$query="SELECT sa.id AS attemptId, sa.endDate, sa.startDate, sa.checked
							FROM `subjective_exam_attempts` AS sa
							WHERE sa.examId={$data->examId} AND sa.studentId={$value['userId']} AND sa.completed=1 ORDER BY sa.studentId";
					$subjectiveAttempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$subjectiveExamArray[$i]["attempts"] = $subjectiveAttempts->toArray();
					$i++;
					//print_r($subjectiveExamArray);
				}
				//print_r($subjectiveExamArray);
				$result->students=$subjectiveExamArray;
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		//getCheckSubjectiveExamStudentId
		public function getCheckSubjectiveExamStudentId($data) {
			$result = new stdClass();
			try {
				$subjectiveExamArray=array();
				$studentIds=array();
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$query="SELECT sd.userId, concat(sd.firstName,' ',sd.lastName) AS name,sa.id AS attemptId, sa.endDate, sa.startDate
						FROM `subjective_exam_attempts` AS sa
						LEFT JOIN `student_details` AS sd ON sd.userId=sa.studentId
						WHERE sa.examId={$data->examId} AND sa.studentId = {$data->userId} AND sa.completed=1";
				//print_r($query);
				$subjectiveStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$subjectiveExamArray = $subjectiveStudents->toArray();
				//print_r($subjectiveExamArray);
				$result->students=$subjectiveExamArray;
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getTemplateDetails($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('templates');
				$select->where(array('id'		=>	$data->templateId,
									'delete'	=>	0));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$templates = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$tempExam = $templates->toArray();
				if(count($tempExam) == 0) {
					$result->status = 0;
					$result->message= "No such Template found.";
					return $result;
				}
				$result->template = $tempExam[0];
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('template_sections')
						->order('weight ASC')
						->order('id ASC');
				$select->where(array('templateId'	=>	$result->template['id'],
									'delete'	=>	0));
				$select->columns(array('id', 'name','time'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$sections = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->sections = $sections->toArray();
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function editExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				// Need to check if subject is owned by user
				$sql = new Sql($adapter);
				if($data->chapterId == '-1')
					$data->chapterId = 0;
				$data->chapterId;
				$update = $sql->update();
				$update->table('exams');
				$update->set(array(
					'name'				=>	$data->name,
					'type'				=>	$data->type,
					'status'			=>	$data->status,
					'startDate'			=>	$data->startDate,
					'endDate'			=>	$data->endDate,
					'attempts'			=>	$data->attempts,
					'tt_status'			=>	$data->tt_status,
					'totalTime'			=>	$data->totalTime,
					'gapTime'			=>	$data->gapTime,
					'sectionOrder'		=>	$data->sectionOrder,
					'powerOption'		=>	(($data->type == "Assignment")?1:$data->powerOption),
					'endBeforeTime'		=>	$data->endBeforeTime,
					'chapterId'			=>	$data->chapterId,
					'showResult'		=>  $data->showResult,
					'showResultTill'	=>  $data->showResultTill,
					'showResultTillDate'=>  $data->showResultTillDate
					
				));
				$update->where(array('id' => $data->examId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				$newSections = array();
				$newSections = array();
				foreach($data->sections as $section) {
					if($section->id == 0) {
						$newSections[] = $section;
					}
					else
						$oldSections[] = $section;
				}
				$oldIds = '(';
				foreach($oldSections as $section) {
					$oldIds .= $section->id.',';
					$sql = new Sql($this->adapter);
					$update = $sql->update();
					$update->table('exam_sections');
					$update->set(array('name'		=>	$section->section,
									'time'	 		=>	$section->time,
									'weight'		=>	$section->weight
								));
					$update->where(array('id' => $section->id));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
				}
				$oldIds = substr($oldIds, 0, strlen($oldIds)-1).')';
				$delStatement = "DELETE FROM exam_sections WHERE id NOT IN ".$oldIds." AND examId=".$data->examId;
				$adapter->query($delStatement, $adapter::QUERY_MODE_EXECUTE);
				if(count($newSections) > 0) {
					$insert = "INSERT INTO exam_sections (name, time, examId, weight) VALUES";
					foreach($newSections as $section) {
						$insert .= "('{$section->section}','{$section->time}','{$data->examId}','{$section->weight}'),";
					}
					$insertString = substr($insert, 0, strlen($insert)-1);
					$adapter->query($insertString, $adapter::QUERY_MODE_EXECUTE);
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->examId = $data->examId;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function editCategory($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('section_categories');
				$update->set(array(
					'correct'				=>	$data->correct,
					'required'				=>	$data->required,
					'wrong'					=>	$data->wrong,
					'questionType'			=>	$data->questionType,
					'weight'				=>	$data->weight
				));
				$update->where(array('id'	=>	$data->categoryId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$e = new Exam();
				$e->updateCategoryStatus($data);
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		//@ayush changes11 made  for delete bug at01082015
		public function deleteQuestion($data) {
			$count=0;

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('questions');
				$update->set(array('delete'	=>	1));
				$update->where(array('id'	=>	$data->questionId , 'delete' => 0));
				$statement = $sql->prepareStatementForSqlObject($update);
				$count = $statement->execute()->getAffectedRows();
				if($count >0){		
					if($data->questionType != 5 && $data->questionType != 4) {
						$sql = new Sql($adapter);
						$update = $sql->update();
						if($data->questionType == 0 || $data->questionType == 6 || $data->questionType == 7)
							$update->table('options_mcq');
						else if($data->questionType == 1)
							$update->table('true_false');	
						else if($data->questionType == 2)
							$update->table('ftb');
						else if($data->questionType == 3)
							$update->table('options_mtf');
						$update->set(array('delete'	=>	1));
						$update->where(array('questionId'	=>	$data->questionId));
						$statement = $sql->prepareStatementForSqlObject($update);
						$statement->execute();
					}
					else {
						$sql = new Sql($adapter);
						$update = $sql->update();
						$update->table('questions');
						$update->set(array('delete'	=>	1));
						$update->where(array('parentId'	=>	$data->questionId));
						$statement = $sql->prepareStatementForSqlObject($update);
						$statement->execute();
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function deleteQuestionExam($data) {
			$count=0;

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$delete = $sql->delete();
				$delete->from('question_category_links');
				$delete->where(array('questionId'	=>	$data->questionId,
									'categoryId'	=>	$data->categoryId));
				$statement = $sql->prepareStatementForSqlObject($delete);
				$count = $statement->execute()->getAffectedRows();
				if($count >0){		
					if($data->questionType != 5 && $data->questionType != 4) {
						if($data->status == 1) {
							$update = $sql->update();
							$update->table('section_categories');
							$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered - 1")));
							$update->where(array('id' => $data->categoryId));
							$statement = $sql->prepareStatementForSqlObject($update);
							$statement->execute();
						}
					}
					else {
						if($data->questionType == 4) {
							$update = $sql->update();
							$update->table('section_categories');
							$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered -".$data->amount)));
							$update->where(array('id' => $data->categoryId));
							$statement = $sql->prepareStatementForSqlObject($update);
							$statement->execute();
						}
						else if($data->questionType == 5) {
							$update = $sql->update();
							$update->table('section_categories');
							$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered - 1")));
							$update->where(array('id' => $data->categoryId));
							$statement = $sql->prepareStatementForSqlObject($update);
							$statement->execute();
						}
						
						$select = $sql->select();
						$select->from('questions');
						$select->where(array('parentId'	=>	$data->questionId));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$subQuestions = $temp->toArray();

						foreach ($subQuestions as $key => $subQuestion) {
							$delete = $sql->delete();
							$delete->from('question_category_links');
							$delete->where(array('questionId'	=>	$subQuestion['id'],
												'categoryId'	=>	$data->categoryId));
							$statement = $sql->prepareStatementForSqlObject($delete);

							$statement->execute();
						}
					}
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
					$e = new Exam();
					$e->updateCategoryStatus($data);
				} else {
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
				}
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function deleteCategory($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('section_categories');
				$update->set(array('delete'	=>	1));
				$update->where(array('id'	=>	$data->categoryId, 'delete' => 0));
				$statement = $sql->prepareStatementForSqlObject($update);
				//$statement->execute();
				$count = $statement->execute()->getAffectedRows();
				if($count >0){	
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('exam_sections');
					$update->set(array('totalMarks'	=>	$data->totalMarks));
					$update->where(array('id'		=>	$data->sectionId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
					$e = new Exam();
					$e->updateSectionStatus($data);
				} else {
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
				}
				
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function deleteSection($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('exam_sections');
				$update->set(array('delete'		=>	1,
								'deleteTime' 	=>	time()));
				$update->where(array('id'	=>	$data->sectionId, 'delete' => 0));
				$statement = $sql->prepareStatementForSqlObject($update);
				//$statement->execute();
				$count = $statement->execute()->getAffectedRows();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				if($count >0){		
					$e = new Exam();
					$e->updateExamStatus($data);
				}
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function deleteExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$subject = $this->getCourseDetailsbyExamId($data->examId);
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('exams');
				$update->set(array('delete'	=>	1));
				$update->where(array('id'	=>	$data->examId,'delete' =>0));
				$statement = $sql->prepareStatementForSqlObject($update);
				//$statement->execute();
				$count = $statement->execute()->getAffectedRows();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				if($count >0)
				{	
					$course = $subject->subject;
					$examId= $data->examId;
					$exam=  $course[0]['exam'];
					$courseId = $course[0]['courseId'];
					$coursename = $course[0]['course'];
					$subjectname = $course[0]['subject'];
					$subjectId = $course[0]['subjectId'];
					$institute = $course[0]['institute'];
					$institutemail = $course[0]['institutemail'];
					$instituteId = $course[0]['instituteId'];

					// insert notification
					$notification="Assignment/Exam $exam ( $data->examId ) belonging to  Subject $subjectname (subject id : {$subjectId} ) which is a part of Course $coursename ( course id : $courseId ) has been deleted. Please contact admin@integro.io for restoring the exam.";
					$query = "INSERT INTO notifications (userId,message, status) VALUES ($instituteId,'$notification',0)";
					$notification=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);

					if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
						require_once "User.php";
						$c = new User();

						$instituteSubject = "Assignment/Exam deletion Notification";
						$emailInstitute = "Dear $institute, <br> <br>";
						$emailInstitute .= "There has been a request for deletion of the Assignment/ Exam   belonging to  Course  $coursename ( Course ID: $courseId ) on <a href=integro.io >Integro.io <a>  <br> <br>";

						$emailInstitute .= "Please write to us at <a href=admin@integro.io >admin@integro.io <a> if you wish to restore the Assignment /Exam  or if you did not initiate the request.<br> <br> "
								. "Please mention your Institute ID, Assignment Name & Assignment ID in the email <br> <br>";

						$emailInstitute .= "<table border=1 ><thead><tr><th> Institute Name</th> <th>Institute ID</th> <th>Deleted Course Name</th> <th> Deleted Course Id</th><th>Deleted Assignment/Exam   Name</th> <th>Deleted Assignment/Exam  Id</th></tr></thead>";
						$emailInstitute .= "<tbody><tr><td> $institute</td><td>$instituteId</td> <td>$coursename</td><td> $courseId</td><td> $exam</td><td> $examId</td> </tr></tbody></table> ";

						$c->sendMail($institutemail, $instituteSubject, $emailInstitute);
					}
				}

				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function checkCategoryStatus($data)
		{
			$result = new stdClass();
			try {
				
				$e = new Exam();
				$e->updateExamStatus($data);
				$e->updateCategoryStatus($data);
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function updateCategoryStatus($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('q'=>'questions'));
				$select->join(array('qcl' => 'question_category_links'),'qcl.questionId = q.id',array());
				$select->where(array('qcl.categoryId'	=>	$data->categoryId,
									 'q.parentId'	=>	0,
									 'q.delete' 	=>	0));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$questions = count($temp->toArray());

				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('section_categories');
				$update->set(array('entered'=>	$questions));
				$update->where(array('id'	=>	$data->categoryId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$asd = $statement->execute();

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('section_categories');
				$select->where(array('id'		=>	$data->categoryId,
									 'delete' 	=>	0));
				$select->columns(array('required', 'entered'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$calc = $temp->toArray();
				$required = $calc[0]['required'];
				$entered = $calc[0]['entered'];
				if($entered >= $required)
					$status = 1;
				else
					$status = 0;
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('section_categories');
				$update->set(array('status'	=>	$status));
				$update->where(array('id'	=>	$data->categoryId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$asd = $statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$e = new Exam();
				$e->updateSectionStatus($data);
				
				//print_r($asd);
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function updateSectionStatus($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$selectString = "SELECT id FROM section_categories WHERE sectionId=".$data->sectionId." AND `delete`=0 AND `status`=0";
				$getIncompleteCategories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $getIncompleteCategories->toArray();
				if(count($temp) == 0)
					$status = 1;
				else
					$status = 0;
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('exam_sections');
				$update->set(array('status'	=>	$status));
				$update->where(array('id'	=>	$data->sectionId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$e = new Exam();
				$e->updateExamStatus($data);
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function updateExamStatus($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$selectString = "SELECT id FROM exam_sections WHERE examId={$data->examId} AND `delete`=0 AND `status`=0";
				$getIncompleteCategories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $getIncompleteCategories->toArray();
				if(count($temp) == 0)
					$status = 1;
				else
					$status = 0;
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('exams');
				$update->set(array('status'	=>	$status));
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('id', $data->examId);
				if($status == 1)
					$where->notEqualTo('status', 2);
				$update->where($where);
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function makeLive($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$statement = "DELETE FROM exam_instructions WHERE examId = {$data->examId};";
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				
				$instructions = new TableGateway('exam_instructions', $adapter, null,new HydratingResultSet());
				$insert = array(
					'examId'			=>	$data->examId,
					'instructions'		=>	$data->instructions
				);
				$instructions->insert($insert);
				
				$update = $sql->update();
				$update->table('exams');
				$update->where(array('id'	=>	$data->examId));
				$update->set(array('status'	=>	2));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function makeUnlive($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('exams');
				$update->where(array('id'	=>	$data->examId));
				$update->set(array('status'	=>	1));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function makeExamEnd($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('exams');
				$update->where(array('id'	=>	$data->examId));
				$update->set(array('endDate'	=>	time().'000'));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function getTemplateForExam($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('templates');
				$select->where(array('ownerId'	=>	$data->userId,
									'delete'	=>	0));
				$select->columns(array('id','name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$templates = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$templates = $templates->toArray();
				$result->templates = $templates;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function getTemplate($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('templates');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('delete', 0);
				$where->nest->equalTo('ownerId', $data->userId)
				->or->equalTo('ownerId', 0)->unnest;
				$select->where($where);
				$select->columns(array('id', 'name', 'type'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$templates = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$templates = $templates->toArray();
				$result->templates = $templates;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function editTemplate($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('templates');
				$update->set(array(
					'name'				=>	$data->name,
					'type'				=>	$data->type,
					'status'			=>	$data->status,
					'tt_status'			=>	$data->tt_status,
					'totalTime'			=>	$data->totalTime,
					'gapTime'			=>	$data->gapTime,
					'sectionOrder'                  =>	$data->sectionOrder,
					'powerOption'                   =>	$data->powerOption,
					'endBeforeTime'                 =>	$data->endBeforeTime,
					'attempts'			=>	$data->attempts
				));
				$update->where(array('id' => $data->templateId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				$sql = new Sql($adapter);
				$delete = $sql->delete();
				$delete->from('template_sections');
				$delete->where(array('templateId'	=>	$data->templateId));
				$statement = $sql->prepareStatementForSqlObject($delete);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$e = new Exam();
				$temp = new stdClass();
				$temp = $e->addTemplateSections($data);
				if($temp->status == 1)
					$result->status = 1;
				else
					return $temp;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function deleteTemplate($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('templates');
				$update->set(array('delete'	=>	1));
				$update->where(array('id'	=>	$data->templateId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('template_sections');
				$update->set(array('delete'	=>	1));
				$update->where(array('templateId'	=>	$data->templateId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function getSubjectExams($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exams');
				if($data->import == true)
					$select->where(array('subjectId'		=>	$data->subjectId));
				else
					$select->where(array('subjectId'		=>	$data->subjectId,
										'delete'		=>	0));
				$select->columns(array('id','name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->exams = $exams->toArray();
				if(count($exams) == 0) {
					$result->status = 0;
					$result->message = "No exams found";
				}
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getAssignmentsForExam($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exams');
				if($data->import == true)
					$select->where(array('chapterId'	=>	$data->chapterId,
										'subjectId'		=>	$data->subjectId));
				else
					$select->where(array('chapterId'	=>	$data->chapterId,
										'subjectId'		=>	$data->subjectId,
										'delete'		=>	0));
				$select->columns(array('id','name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->exams = $exams->toArray();
				if(count($exams) == 0) {
					$result->status = 0;
					$result->message = "No exams found";
				}
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function getSubjectSubjectiveExams($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exam_subjective');
				if($data->import == true)
					$select->where(array('subjectId'		=>	$data->subjectId));
				else
					$select->where(array('subjectId'		=>	$data->subjectId,
										'deleted'		=>	0));
				$select->columns(array('id','name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->exams = $exams->toArray();
				if(count($exams) == 0) {
					$result->status = 0;
					$result->message = "No exams found";
				}
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getQuestionsForImport($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				/*$selectString = "SELECT id, status, question, questionType FROM questions WHERE categoryId IN (SELECT id FROM section_categories WHERE sectionId IN (SELECT id FROM exam_sections WHERE examId={$data->examId})) AND parentId=0 AND `status`!=2";*/
				$selectString = "SELECT q.id, q.status, q.question, q.questionType
					FROM questions AS q
					INNER JOIN question_category_links AS qcl ON qcl.questionId=q.id
					WHERE qcl.categoryId IN (
						SELECT id FROM section_categories WHERE sectionId IN (
							SELECT id FROM exam_sections WHERE examId={$data->examId}
						)
					) AND parentId=0 AND `status`!=2";
				$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->questions = $questions->toArray();
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function importQuestions($data) {
			$result = new stdClass();
			try {
				foreach($data->questions as $question) {
					$e = new Exam();
					$result = $e->copyQuestion($question);
				}
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function importSubjectQuestions($data) {
			$result = new stdClass();
			try {
				foreach($data->questions as $question) {
					$e = new Exam();
					$result = $e->linkQuestion($question);
				}
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function linkQuestion($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$selectString = "INSERT INTO question_category_links (`questionId`, `categoryId`) VALUES ('{$data->questionId}', '{$data->categoryId}');";
				$adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function importSubjectiveQuestions($data) {
			$result = new stdClass();
			try {
				foreach($data->questions as $question) {
					$e = new Exam();
					$result = $e->linkSubjectiveQuestion($question);
				}
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function linkSubjectiveQuestion($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$selectString = "INSERT INTO question_subjective_exam_links (`questionId`, `examId`) VALUES ('{$data->questionId}', '{$data->examId}');";
				$adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		/**
		*	used to copy a question one by one.
		*	might be implemented using a sql procedure later.
		*/
		public function copyQuestion($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				if($data->sameExam)
					$selectString = "INSERT INTO questions (`categoryId`, `questionType`, `question`, `description`, `status`, `parentId`, `delete`) SELECT '{$data->categoryId}', questionType, question, description, '2', '{$data->parentId}', `delete` FROM questions WHERE id={$data->questionId};";
				else
					$selectString = "INSERT INTO questions (`categoryId`, `questionType`, `question`, `description`, `status`, `parentId`, `delete`) SELECT '{$data->categoryId}', questionType, question, description, `status`, '{$data->parentId}', `delete` FROM questions WHERE id={$data->questionId};";
				$adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$selectString = "SELECT id, `status` FROM questions ORDER BY id DESC LIMIT 1;";
				$insert = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$insert = $insert->toArray();
				$newQuestionId = $insert[0]['id'];
				$newQuestionStatus = $insert[0]['status'];
				if($data->questionType == 0 || $data->questionType == 6) {
					$selectString = "INSERT INTO options_mcq (questionId, parentId, `option`, correct) SELECT '{$newQuestionId}', '{$data->parentId}', `option`, correct FROM options_mcq WHERE questionId={$data->questionId};";
					$adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					if($newQuestionStatus == 1 && $data->parentId == 0) {
						$e = new Exam();
						$e->increaseEntered($data);
					}
				}
				else if($data->questionType == 1) {
					$selectString = "INSERT INTO true_false (questionId, parentId, answer) SELECT '{$newQuestionId}', '{$data->parentId}', answer FROM true_false WHERE questionId={$data->questionId};";
					$adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					if($newQuestionStatus == 1  && $data->parentId == 0) {
						$e = new Exam();
						$e->increaseEntered($data);
					}
				}
				else if($data->questionType == 2) {
					$selectString = "INSERT INTO ftb (questionId, parentId, blanks) SELECT '{$newQuestionId}', '{$data->parentId}', blanks FROM ftb WHERE questionId={$data->questionId};";
					$adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					if($newQuestionStatus == 1 && $data->parentId == 0) {
						$e = new Exam();
						$e->increaseEntered($data);
					}
				}
				else if($data->questionType == 3) {
					$selectString = "INSERT INTO options_mtf (questionId, columnA, columnB) SELECT '{$newQuestionId}', columnA, columnB FROM options_mtf WHERE questionId={$data->questionId};";
					$adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					if($newQuestionStatus == 1 && $data->parentId == 0) {
						$e = new Exam();
						$e->increaseEntered($data);
					}
				}
				else if($data->questionType == 4 || $data->questionType == 5) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('questions');
					$select->where(array('parentId'	=>	$data->questionId));
					$select->columns(array('id', 'questionType'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$childQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$childQuestions = $childQuestions->toArray();
					if($data->questionType == 5 && $newQuestionStatus == 1) {
						$sql = new Sql($adapter);
						$update = $sql->update();
						$update->table('section_categories');
						$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered + 1")));
						$update->where(array('id' => $data->categoryId));
						$statement = $sql->prepareStatementForSqlObject($update);
						$statement->execute();
					}
					else if($data->questionType == 4 && $newQuestionStatus == 1) {
						$sql = new Sql($adapter);
						$update = $sql->update();
						$update->table('section_categories');
						$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered + ".count($childQuestions))));
						$update->where(array('id' => $data->categoryId));
						$statement = $sql->prepareStatementForSqlObject($update);
						$statement->execute();
					}
					foreach($childQuestions as $newQuestion) {
						$obj = new stdClass();
						$obj->categoryId = $data->categoryId;
						$obj->sectionId = $data->sectionId;
						$obj->examId = $data->examId;
						$obj->questionId = $newQuestion['id'];
						$obj->questionType = $newQuestion['questionType'];
						$obj->parentId = $newQuestionId;
						$obj->sameExam = false;
						$e = new Exam();
						$e->copyQuestion($obj);
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function increaseEntered($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('section_categories');
				$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered + 1")));
				$update->where(array('id' => $data->categoryId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$e = new Exam();
				$e->updateCategoryStatus($data);
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function restoreSection($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('exam_sections');
				$update->set(array('delete'		=>	0,
								'deleteTime' 	=>	0));
				$update->where(array('id'	=>	$data->sectionId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$e = new Exam();
				$e->updateExamStatus($data);
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function restoreExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('exams');
				$update->set(array('delete'	=>	0));
				$update->where(array('id'	=>	$data->examId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function restoreSubjectiveExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('exam_subjective');
				$update->set(array('Active'	=>	1));
				$update->where(array('id'	=>	$data->examId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function restoreManualExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('exam_manual');
				$update->set(array('status'	=>	1));
				$update->where(array('id'	=>	$data->examId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function getBreadcrumbForExam($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$select = "SELECT c.name AS chapterName, c.id AS chapterId, s.name AS subjectName, s.id AS subjectId FROM subjects AS s LEFT OUTER JOIN chapters AS c ON c.id=(SELECT chapterId FROM exams WHERE id={$data->examId}) WHERE s.id=(SELECT subjectId FROM exams WHERE id={$data->examId})";
				$temp = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				$result->subChap = $temp[0];
				$select = "SELECT name AS courseName, id AS courseId FROM courses WHERE id=(SELECT courseId FROM subjects WHERE id={$temp[0]['subjectId']})";
				$temp = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				$result->course = $temp[0];
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function getBreadcrumbForManualExam($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$select = "SELECT id AS examId, name AS examName FROM exam_manual WHERE id={$data->examId}";
				$temp = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				$result->exam = $temp[0];
				$select = "SELECT s.name AS subjectName, s.id AS subjectId
							FROM subjects AS s
							WHERE s.id=(SELECT subjectId FROM exam_manual WHERE id={$data->examId})";
				$temp = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				$result->subject = $temp[0];
				$select = "SELECT name AS courseName, id AS courseId FROM courses WHERE id=(SELECT courseId FROM subjects WHERE id={$temp[0]['subjectId']})";
				$temp = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				$result->course = $temp[0];
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function getBreadcrumbForAdd($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$select = "SELECT c.id AS courseId, c.name AS courseName, s.id AS subjectId, s.name AS subjectName FROM courses AS c JOIN subjects AS s ON s.id={$data->subjectId} WHERE c.id={$data->courseId}";
				$temp = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				$result->subjectId = $temp[0]['subjectId'];
				$result->subjectName = $temp[0]['subjectName'];
				$result->courseId = $temp[0]['courseId'];
				$result->courseName = $temp[0]['courseName'];
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function checkNameForExam($data) {
			//print_r($data);
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				if(isset($data->examId) && !empty($data->examId)) {
					$select = $sql->select();
					$select->from('exams');
					$where = new \Zend\Db\Sql\Where();
					$where->equalTo('name', $data->name);
					$where->equalTo('ownerId', $data->userId);
					$where->notEqualTo('id', $data->examId);
					$select->where($where);
					$select->columns(array('id'));
					$selectString = $sql->getSqlStringForSqlObject($select);
				} else {
					$select = $sql->select();
					$select->from('exams');
					$select->where(array(
						'name'		=>	$data->name,
						'ownerId'	=>	$data->userId
					));
					$select->columns(array('id'));
					$selectString = $sql->getSqlStringForSqlObject($select);
				}
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				if(count($temp) > 0)
					$result->available = false;
				else
					$result->available = true;
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		
		/*
			function: this will check if name exists for subjective exam
			
		*/
		public function checkNameForsubjectiveexam($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				if(!isset($data->examId)) {
					$select = $sql->select();
					$select->from('exam_subjective');
					$select->where(array(
						'name'		=>	$data->name,
						'ownerId'	=>	$data->userId
					));
					$select->columns(array('id'));
					$selectString = $sql->getSqlStringForSqlObject($select);
				}
				else {
					$select = $sql->select();
					$select->from('exam_subjective');
					$where = new \Zend\Db\Sql\Where();
					$where->equalTo('name', $data->name);
					$where->equalTo('ownerId', $data->userId);
					$where->notEqualTo('id', $data->examId);
					$select->where($where);
					$select->columns(array('id'));
					$selectString = $sql->getSqlStringForSqlObject($select);
				}
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				if(count($temp) > 0)
					$result->available = false;
				else
					$result->available = true;
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		
		
		public function checkNameForTemplate($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				if(!isset($data->templateId)) {
					$select = $sql->select();
					$select->from('templates');
					$select->where(array('name'		=>	$data->name,
										'ownerId'	=>	$data->userId));
					$select->columns(array('id'));
					$selectString = $sql->getSqlStringForSqlObject($select);
				}
				else {
					$selectString = "SELECT id FROM templates WHERE name='{$data->name}' AND id != {$data->templateId} AND ownerId={$data->userId};";
				}
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				if(count($temp) > 0)
					$result->available = false;
				else
					$result->available = true;
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		   
		public function getAlldeletedExam($data) {
			$result = new stdClass();
			$adapter = $this->adapter;
			//$sql = new Sql($adapter);
			try {
				$query = "SELECT e.id examid,e.name examname,e.type examtype,cp.id chapterid,cp.name chapter, s.id subjectid ,s.name subject,CONCAT( c.name,'(C00',c.id,')') course,l.id instituteid  ,l.email,u.contactMobile ,
							case 
							when ur.roleId= 1 then (select name from institute_details where userId= l.id)
							when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
							when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
							end as username
							from exams e
							join chapters cp on  cp.id=e.chapterId
							JOIN subjects s on s.id=cp.subjectId and s.id=e.subjectId
							join courses c on c.id=s.courseId
							JOIN login_details l on l.id=c.ownerId   Join user_roles  ur on ur.userId=l.id
							JOIN user_details  u on l.id=u.userId
							where e.delete=1 and  ur.roleId not in (4,5) order by l.id";
				$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->exams = $exams->toArray();
				if (count($result->exams) == 0) {
					$result->status = 0;
					$result->exception = "No deleted Assignments Found";
					return $result;
				}
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function restoreExamByAdmin($req) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('exams');
				$update->set(array('delete' => 0));
				$update->where(array('id' => $req->examId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$result = $statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getStudentExamAttemps($data) {
			try {
				$result = new stdClass();
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('student_subject');
				$select->columns(array('id','subjectId','Active'));
				$select->where(array('courseId' => $data->courseId,'userId' => $data->userId));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjects1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects1 = $subjects1->toArray();
				$stringSubjectids=array();
				$stringSubjectidsNotActive=array();
				if(count($subjects1)>0)
				{

					//$stringSubjectids=[];
					foreach($subjects1 as $key=>$value)
					{
						//$stringSubjectids .=','.$value['subjectId'];
						
						if($value['Active']==1 && !(in_array($value['subjectId'],$stringSubjectids)))
							$stringSubjectids[$key]=$value['subjectId'];
						if($value['Active']==0 && !(in_array($value['subjectId'],$stringSubjectidsNotActive)))
						{
							$stringSubjectidsNotActive[$key]=$value['subjectId'];
						}
					}
					if(sizeof($stringSubjectids)>0)
					{	
						
						$stringSubjectids=implode(",",$stringSubjectids);
						$stringSubjectids="(".$stringSubjectids.")";	
						$query = "SELECT e.id,e.name exam ,e.type,e.showResult,e.showResultTill,e.showResultTillDate,e.chapterId,c.name chapter,e.subjectId, s.name subject,e.startDate,e.endDate,e.attempts,
									(select count('id') from exam_attempts et where et.examId=e.id and et.studentId=$data->userId)examgiven,
								 (select count('id') from exam_attempts et where et.examId=e.id and et.studentId=$data->userId AND completed=0) paused
								from subjects s  JOIN chapters c on c.subjectId=s.id 
							JOIN exams e on e.subjectId=s.id and e.chapterId=c.id where s.courseId=$data->courseId and s.deleted=0 and c.deleted=0 and e.delete=0 AND e.status=2 and s.id   in $stringSubjectids UNION
						select e.id,e.name exam ,e.type,e.showResult,e.showResultTill,e.showResultTillDate,e.chapterId,'Independent',e.subjectId,s.name subject,e.startDate,e.endDate,e.attempts,
						(select count('id') from exam_attempts et where et.examId=e.id and et.studentId=$data->userId)examgiven,
						 (select count('id') from exam_attempts et where et.examId=e.id and et.studentId=$data->userId AND completed=0) paused from subjects s 
						JOIN exams e on e.subjectId=s.id  where s.courseId=$data->courseId and s.deleted=0 and e.delete=0 and  e.chapterId=0 and e.status=2 and s.id   in $stringSubjectids";
					//print_r($query);
					}
					else{
						$stringSubjectidsNotActive=implode(",",$stringSubjectidsNotActive);
						$stringSubjectidsNotActive="(".$stringSubjectidsNotActive.")";
						//print_r('check');
						//print_r($stringSubjectidsNotActive);
						$query = "SELECT e.id,e.name exam ,e.type,e.showResult,e.showResultTill,e.showResultTillDate,e.chapterId,c.name chapter,e.subjectId, s.name subject,e.startDate,e.endDate,e.attempts,
									(select count('id') from exam_attempts et where et.examId=e.id and et.studentId=$data->userId)examgiven,
								 (select count('id') from exam_attempts et where et.examId=e.id and et.studentId=$data->userId AND completed=0) paused
								from subjects s  JOIN chapters c on c.subjectId=s.id 
							JOIN exams e on e.subjectId=s.id and e.chapterId=c.id where s.courseId=$data->courseId and s.deleted=0 and c.deleted=0 and e.delete=0 AND e.status=2 and s.id  not in $stringSubjectidsNotActive  UNION
						select e.id,e.name exam ,e.type,e.showResult,e.showResultTill,e.showResultTillDate,e.chapterId,'Independent',e.subjectId,s.name subject,e.startDate,e.endDate,e.attempts,
						(select count('id') from exam_attempts et where et.examId=e.id and et.studentId=$data->userId)examgiven,
						 (select count('id') from exam_attempts et where et.examId=e.id and et.studentId=$data->userId AND completed=0) paused from subjects s 
						JOIN exams e on e.subjectId=s.id  where s.courseId=$data->courseId and s.deleted=0 and e.delete=0 and  e.chapterId=0 and e.status=2 and s.id   not in $stringSubjectidsNotActive  ";
					}
					
						
				} else {
						 $query = "SELECT e.id,e.name exam ,e.type,e.showResult,e.showResultTill,e.showResultTillDate,e.chapterId,c.name chapter,e.subjectId, s.name subject,e.startDate,e.endDate,e.attempts,
									(select count('id') from exam_attempts et where et.examId=e.id and et.studentId=$data->userId)examgiven,
								 (select count('id') from exam_attempts et where et.examId=e.id and et.studentId=$data->userId AND completed=0) paused
								from subjects s  JOIN chapters c on c.subjectId=s.id 
							JOIN exams e on e.subjectId=s.id and e.chapterId=c.id where s.courseId=$data->courseId and s.deleted=0 and c.deleted=0 and e.delete=0 AND e.status=2  UNION
						select e.id,e.name exam ,e.type,e.showResult,e.showResultTill,e.showResultTillDate,e.chapterId,'Independent',e.subjectId,s.name subject,e.startDate,e.endDate,e.attempts,
						(select count('id') from exam_attempts et where et.examId=e.id and et.studentId=$data->userId)examgiven,
						 (select count('id') from exam_attempts et where et.examId=e.id and et.studentId=$data->userId AND completed=0) paused from subjects s 
						JOIN exams e on e.subjectId=s.id  where s.courseId=$data->courseId and s.deleted=0 and e.delete=0 and  e.chapterId=0 and e.status=2 ";
				}	
				// print_r($stringSubjectids);
				// print_r($stringSubjectids);

				$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$temp = $exams->toArray();
				if (count($temp) == 0) {
					$result->status = 0;
					$result->exception = "No Exams Found";
					return $result;
				}
				$result->currentDate=time();
				//now filtering the exams which has to be given now according to bug ARC-454
				//we will delete all exams and assignments which have crossed the number of attempts or its date
				//var_dump($temp);

				foreach($temp as $key=>$data) {
					//commenting this according to current requirements
					if($data['showResult'] == 'immediately')
					{
						if($result->currentDate - (($data['startDate']/1000) - (7 * 24 * 60 * 60)) < 0) {
							//delete the key whose date is in future
							unset($temp[$key] );
						}
						if($data['endDate'] != '' && ($data['endDate']/1000) - $result->currentDate < 0) {
							//delete the key whose end date is in past
							unset($temp[$key]);
						}
					}
					
				}
				//var_dump($temp);
				$result->exams = $temp;
				$result->status = 1;
				//print_r($result);
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getStudentExamAttemptsNew($data) {
			$result = new stdClass();
			try {
				$time = time();
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('ss'	=>	'student_subject'))
				->join(array('s'	=>	'subjects'),
						's.id = ss.subjectId',
						array('id', 'name', 'image')
				);
				$select->where(array('userId'	=>	$data->userId,'ss.courseId'	=>	$data->courseId, 's.deleted'=>0,'Active'=>1))
							->order('weight ASC')
							->order('s.id ASC');
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects = $subjects->toArray();
				$stringSubjectids = array();
				if (count($subjects)>0) {
					foreach ($subjects as $key => $value) {
						$stringSubjectids[$key]=$value['subjectId'];
					}
					$stringSubjectids = implode(",", $stringSubjectids);

					$query="SELECT e.id,e.name exam ,e.type,e.showResult,e.showResultTill,e.showResultTillDate,e.chapterId,c.name chapter,e.subjectId, s.name subject,e.startDate,e.endDate,e.attempts,
							(SELECT count('id') FROM exam_attempts et WHERE et.examId=e.id AND et.studentId=$data->userId) examgiven,
							(SELECT count('id') FROM exam_attempts et WHERE et.examId=e.id AND et.studentId=$data->userId AND completed=0) paused
							FROM subjects s
							INNER JOIN chapters c on c.subjectId=s.id 
							INNER JOIN exams e on e.subjectId=s.id AND e.chapterId=c.id WHERE s.courseId=$data->courseId AND s.deleted=0 AND c.deleted=0 AND e.delete=0 AND e.status=2 AND s.id IN ($stringSubjectids)
							UNION
							SELECT e.id,e.name exam ,e.type,e.showResult,e.showResultTill,e.showResultTillDate,e.chapterId,'Independent',e.subjectId,s.name subject,e.startDate,e.endDate,e.attempts,
							(SELECT count('id') FROM exam_attempts et WHERE et.examId=e.id AND et.studentId=$data->userId) examgiven,
							(SELECT count('id') FROM exam_attempts et WHERE et.examId=e.id AND et.studentId=$data->userId AND completed=0) paused
							FROM subjects s 
							INNER JOIN exams e on e.subjectId=s.id
							WHERE s.courseId=$data->courseId AND s.deleted=0 AND e.delete=0 AND  e.chapterId=0 AND e.status=2 AND s.id IN ($stringSubjectids) ORDER BY endDate";
					$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$temp = $exams->toArray();
					if (count($temp) == 0) {
						$result->status = 0;
						$result->exception = "No Exams Found";
						return $result;
					}
					$result->currentDate=time();
					//now filtering the exams which has to be given now according to bug ARC-454
					//we will delete all exams and assignments which have crossed the number of attempts or its date
					//var_dump($temp);

					foreach($temp as $key=>$data) {
						//commenting this according to current requirements
						if($data['showResult'] == 'immediately')
						{
							if($result->currentDate - (($data['startDate']/1000) - (7 * 24 * 60 * 60)) < 0) {
								//delete the key whose date is in future
								unset($temp[$key] );
							}
							if($data['endDate'] != '' && ($data['endDate']/1000) - $result->currentDate < 0) {
								//delete the key whose end date is in past
								unset($temp[$key]);
							}
						}

						if (($data['attempts'] - $data['examgiven']) < 1) {
							unset($temp[$key]);
						}
					}
					$temp = array_values($temp);
					$result->exams = $temp;
					$result->status = 1;
				} else {
					$result->status = 0;
					$result->exception = "No Exams Found";
					return $result;
				}
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function getCheckSubjectiveExamQuestions($data) {
			try {
				$result = new stdClass();
				$adapter = $this->adapter;
				$query = "SELECT qs.*, qsel.marks, IFNULL(saq.marks,0) as studentMarks, IFNULL(saq.time,0) as questionTime, saq.check, asu.answer AS studentAnswer, IFNULL(asu.review,'') AS review, sa.examId, sa.studentId
							FROM `subjective_exam_attempts` AS sa
							INNER JOIN `exam_subjective` AS es ON es.id=sa.examId
							INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.examId=es.id
							INNER JOIN `questions_subjective` AS qs ON qs.id=qsel.questionId
							LEFT JOIN `subjective_attempt_questions` AS saq ON sa.id=saq.attemptId AND saq.questionId=qs.id
							LEFT JOIN `answer_subjective` AS asu ON asu.attemptQuestionId=saq.id AND asu.status=1 AND asu.answerType='text' AND asu.subjectiveExam_attemptid=sa.id
							WHERE qs.`deleted`=0 AND qs.`status`=1 AND sa.id={$data->attemptId}
							ORDER BY qs.id";
				$questions = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$questions = $questions->toArray();
				if (count($questions) > 0) {
					$subjectiveQuestions=array();
					$parentIdTemp= array();
					$i			 = 0;
					$qnoCounter  = 1;
					$data->examId = $questions[0]['examId'];
					
					//fetching attemptID of topper
					$adapter = $this->adapter;
					$select = "SELECT id FROM `subjective_exam_attempts`
								WHERE examId={$data->examId} AND completed=1 AND checked=1 AND score = (
									SELECT max(score) FROM `subjective_exam_attempts` WHERE examId= {$data->examId} AND completed =1
								) ORDER BY time";
					$topperAttempt = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$topperAttempt = $topperAttempt->toArray();
					if (count($topperAttempt)) {
						$topperAttempt = $topperAttempt[0]['id'];
					} else {
						$topperAttempt = 0;
					}

					foreach($questions as $key=>$question)
					{
						
						$query = "SELECT IFNULL((saq.marks),0) AS toppermarks, IFNULL((saq.time),0) AS toppertime, saq.check, COUNT(*) AS answers
									FROM `subjective_attempt_questions` as saq
									LEFT JOIN `answer_subjective` AS asu ON asu.attemptQuestionId=saq.id AND asu.subjectiveExam_attemptid=saq.attemptId
									WHERE saq.questionId='$question[id]' AND saq.attemptId='$topperAttempt' AND asu.id IS NOT NULL
									GROUP BY asu.attemptQuestionId";
						//print_r($query);
						$toppermarkstime = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$toppermarkstime = $toppermarkstime->toArray();
						if (count($toppermarkstime)>0) {
							$topperans	 = $toppermarkstime[0]['answers'];
							$toppertime	 = $this->checkAttemptTime($toppermarkstime[0]['toppertime'],$topperans);
							$toppermarks = $this->checkAttemptMarks($toppermarkstime[0]['toppermarks'],$topperans);
						} else {
							$toppertime  = "Not Attempted";
							$toppermarks = 'NA';
						}

						$questionString			= "";
						$answerString			= "";
						$studentAnswerString	= "";
						$studentAnswerItems		= 0;
						if (!empty($question['question'])) {
							$questionString = $question['question'];
						}
						if (!empty($question['answer'])) {
							$answerString=$question['answer'];
						}
						if (!empty($question['studentAnswer'])) {
							$studentAnswerString=$question['studentAnswer'];
							$studentAnswerItems++;
						}
						if (empty($question["parentId"])) {
							$parentIdTemp[$i]=$question['id'];
							$subjectiveQuestions[$i]["id"]				= $question["id"];
							$subjectiveQuestions[$i]["qno"]				= $qnoCounter;
							$subjectiveQuestions[$i]["question"]		= $questionString;
							$subjectiveQuestions[$i]["topper_time"]		= $toppertime;
							$subjectiveQuestions[$i]["shortest_time"]	= 0;
							$subjectiveQuestions[$i]["avg_time"]		= 0;
							$subjectiveQuestions[$i]["max_marks"]		= $question["marks"];
							/*-----------------------------MARKS----------------------------------*/
							$subjectiveQuestions[$i]["marks"]			= $question["studentMarks"];
							$subjectiveQuestions[$i]["topper_marks"]	= $toppermarks;
							$subjectiveQuestions[$i]["avg_marks"]		= 0;
							/*--------------------------------------------------------------------*/
							$subjectiveQuestions[$i]["uploads"]			= array();
							$subjectiveQuestions[$i]["answer"]			= $answerString;
							$subjectiveQuestions[$i]["student_answer"]	= $studentAnswerString;
							$subjectiveQuestions[$i]["review"]			= $question["review"];
							$subjectiveQuestions[$i]["check"]			= $question["check"];
							$subjectiveQuestions[$i]["subQuestions"]	= array();
							$subjectiveQuestions[$i]['listType']		= '';
							if ($question['questionsonPage'] == 1) {
								$subjectiveQuestions[$i]['listType']	= 'one';
								$query = "SELECT COUNT(*) AS subCount FROM `subjective_attempt_questions` WHERE parentId='$question[id]' AND attemptId='{$data->attemptId}'";
								//print_r($query);
								$subQuestions = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$subQuestions = $subQuestions->toArray();
								if (count($subQuestions)>0) {
									$subQuestions = $subQuestions[0]['subCount'];
								}
								$subjectiveQuestions[$i]["max_marks"]		= $subjectiveQuestions[$i]["max_marks"]*$subQuestions;
							} elseif ($question['questionsonPage'] == 2) {
								$subjectiveQuestions[$i]['listType']	= 'multi';
							}
							if($question['noOfSubquestionRequired']>0) {
								$subjectiveQuestions[$i]["questionType"]	= "questionGroup";
							} else {
								$subjectiveQuestions[$i]["questionType"]	= "question";
							}

							$query = "SELECT asu.id,`answer`,IFNULL(asu.review,'') AS review
										FROM `answer_subjective` AS asu
										INNER JOIN `subjective_attempt_questions` AS saq ON saq.id=asu.attemptQuestionId
										WHERE asu.answerType='upload' AND asu.subjectiveExam_attemptid='{$data->attemptId}' AND saq.questionId=$question[id] AND asu.status=1";
							//print_r($query);
							//var_dump($query);
							$uploads = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$uploads = $uploads->toArray();
							if (count($uploads)) {
								$studentAnswerItems+=count($uploads);
								foreach ($uploads as $key1 => $upload) {
									$ext = pathinfo($upload['answer'], PATHINFO_EXTENSION);
									$uploadInfo = array(
														"review" => $upload['review'],
														"path" => $upload['answer'],
														"type" => $ext,
														"id"   => $upload['id']
														);
									$subjectiveQuestions[$i]["uploads"][]		= $uploadInfo;
								}
							}
							if ($subjectiveQuestions[$i]['listType'] != 'multi') {
								// question time
								$subjectiveQuestions[$i]["time"]			= $this->checkAttemptTime($question["questionTime"],$studentAnswerItems);
								$subjectiveQuestions[$i]["marks"]			= $this->checkAttemptMarks($question["studentMarks"],$studentAnswerItems);
								// average time
								$query = "SELECT saq.id, saq.check, saq.marks, saq.time, COUNT(asu.id) AS answers
											FROM `questions_subjective` AS qs
											INNER JOIN `subjective_attempt_questions` AS saq ON saq.questionId=qs.id
											INNER JOIN `subjective_exam_attempts` AS sa ON sa.id=saq.attemptId
											INNER JOIN `question_subjective_exam_links` AS qsel ON qs.id=qsel.questionId
											LEFT JOIN `answer_subjective` AS asu ON asu.attemptQuestionId=saq.id AND asu.subjectiveExam_attemptid=saq.attemptId
											WHERE qsel.examId=$question[examId] AND saq.questionId='$question[id]' AND qs.status=1 AND qs.deleted=0 AND sa.checked=1 AND saq.check=1 AND asu.id IS NOT NULL
											GROUP BY saq.id";
								$avgResult = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$avgResult = $avgResult->toArray();
								$avgMarks  = $avgTime = 0;
								$avgMarksItems = $avgTimeItems = 0;
								if (count($avgResult)>0) {
									foreach ($avgResult as $key => $value) {
										if ($value["answers"]>0) {
											$avgMarks+=$value["marks"];
											$avgMarksItems++;
										}
									}
									if ($avgMarksItems>0) {
										$avgMarks = round($avgMarks/$avgMarksItems,2);
									} else {
										$avgMarks = 'NA';
									}
									foreach ($avgResult as $key => $value) {
										if ($value["answers"]>0 AND $value["marks"]>=$avgMarks) {
											$avgTime+=$value["time"];
											$avgTimeItems++;
										}
									}
									if ($avgTimeItems>0) {
										$avgTime = $avgTime/$avgMarksItems;
										$avgTime = gmdate("H:i:s",$avgTime);
									} else {
										$avgTime = 'NA';
									}
								} else {
									$avgMarks = 'NA';
									$avgTime  = 'NA';
								}
								$subjectiveQuestions[$i]["avg_time"]		= $avgTime;
								$subjectiveQuestions[$i]["avg_marks"]		= $avgMarks;
								// shortest time
								$query = "SELECT IFNULL(MIN(saq.time),0) AS shortest_time
											FROM `questions_subjective` AS qs
											LEFT JOIN `subjective_attempt_questions` AS saq ON saq.questionId=qs.id
											LEFT JOIN `subjective_exam_attempts` AS sa ON sa.id=saq.attemptId
											INNER JOIN `question_subjective_exam_links` AS qsel ON qs.id=qsel.questionId
											WHERE qsel.examId=$question[examId] AND saq.time!=0 AND saq.questionId='$question[id]' AND sa.checked=1 AND qs.deleted=0 AND saq.check=1 AND qs.status=1";
								$minTime = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$minTime = $minTime->toArray();
								if (count($minTime)) {
									$subjectiveQuestions[$i]["shortest_time"]		= gmdate("H:i:s",$minTime[0]['shortest_time']);
								}
							}

							$subjectiveQuestions[$i]['attempted'] = 0;
							if ($studentAnswerItems>0) {
								$subjectiveQuestions[$i]['attempted'] = 1;
							}

							$qnoCounter++;
							$i++;
						} else {
							$parentId=$question['parentId'];
							$index=array_search($question['parentId'],$parentIdTemp);

							$query = "SELECT asu.id,`answer`,IFNULL(asu.review,'') AS review
										FROM `answer_subjective` AS asu
										INNER JOIN `subjective_attempt_questions` AS saq ON saq.id=asu.attemptQuestionId
										WHERE asu.answerType='upload' AND asu.subjectiveExam_attemptid='{$data->attemptId}' AND  saq.questionId=$question[id] AND asu.status=1";
							//print_r($query);
							$uploads = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$uploads = $uploads->toArray();
							$subjectiveUploads = array();
							if (count($uploads)) {
								$studentAnswerItems+=count($uploads);
								foreach ($uploads as $key1 => $upload) {
									$ext = pathinfo($upload['answer'], PATHINFO_EXTENSION);
									$subjectiveUploads[]		= array(
																					"review" => $upload['review'],
																					"path" => $upload['answer'],
																					"type" => $ext,
																					"id"   => $upload['id']
																					);
								}
							}
							$avgMarks		= $avgTime		= 0;
							$avgMarksItems	= $avgTimeItems	= 0;
							$minTime		= 0;
							if ($subjectiveQuestions[$index]['listType'] == 'multi') {

								// average time
								$query = "SELECT saq.id, saq.check, saq.marks, saq.time, COUNT(asu.id) AS answers
											FROM `questions_subjective` AS qs
											INNER JOIN `subjective_attempt_questions` AS saq ON saq.questionId=qs.id
											INNER JOIN `subjective_exam_attempts` AS sa ON sa.id=saq.attemptId
											INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id
											LEFT JOIN `answer_subjective` AS asu ON asu.attemptQuestionId=saq.id AND asu.subjectiveExam_attemptid=saq.attemptId
											WHERE qsel.examId=$question[examId] AND saq.questionId='$question[id]' AND qs.status=1 AND qs.deleted=0 AND sa.checked=1 AND saq.check=1 AND asu.id IS NOT NULL
											GROUP BY saq.id";
								$avgResult = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$avgResult = $avgResult->toArray();
								if (count($avgResult)>0) {
									foreach ($avgResult as $key => $value) {
										if ($value["answers"]>0) {
											$avgMarks+=$value["marks"];
											$avgMarksItems++;
										}
									}
									if ($avgMarksItems>0) {
										$avgMarks = round($avgMarks/$avgMarksItems,2);
									} else {
										$avgMarks = 'NA';
									}
									foreach ($avgResult as $key => $value) {
										if ($value["answers"]>0 AND $value["marks"]>=$avgMarks) {
											$avgTime+=$value["time"];
											$avgTimeItems++;
										}
									}
									if ($avgTimeItems>0) {
										$avgTime = $avgTime/$avgMarksItems;
										$avgTime = gmdate("H:i:s",$avgTime);
									} else {
										$avgTime = 'NA';
									}
								} else {
									$avgMarks = 'NA';
									$avgTime  = 'NA';
								}

								// shortest time
								$query = "SELECT IFNULL(MIN(saq.time),0) AS shortest_time
											FROM `questions_subjective` AS qs
											LEFT JOIN `subjective_attempt_questions` AS saq ON saq.questionId=qs.id
											LEFT JOIN `subjective_exam_attempts` AS sa ON sa.id=saq.attemptId
											INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id
											WHERE qsel.examId=$question[examId]  AND saq.time!=0 AND saq.questionId='$question[id]' AND qs.status=1 AND qs.deleted=0 AND sa.checked=1 AND saq.check=1";
								$minTime = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$minTime = $minTime->toArray();
								if (count($minTime)) {
									$minTime = gmdate("H:i:s",$minTime[0]['shortest_time']);
								}
							}
							
							$attempted = 0;
							if ($studentAnswerItems>0) {
								$attempted = 1;
							}
							$studentTime = $this->checkAttemptTime($question["questionTime"],$studentAnswerItems);
							$subjectiveQuestions[$index]["subQuestions"][] = array(
																			"id"			=> $question["id"],
																			"questionType"	=> "question",
																			"question"		=> $questionString,
																			"time"			=> $studentTime,
																			"topper_time"	=> $toppertime,
																			"avg_time"		=> $avgTime,
																			"shortest_time"	=> $minTime,
																			"marks"			=> $question["studentMarks"],
																			"max_marks"		=> $subjectiveQuestions[$index]["max_marks"],
																			"topper_marks"	=> $toppermarks,
																			"avg_marks"		=> $avgMarks,
																			"uploads"		=> $subjectiveUploads,
																			"answer"		=> $answerString,
																			"student_answer"=> $studentAnswerString,
																			"review"		=> $question["review"],
																			"check"			=> $question["check"],
																			"attempted"		=> $attempted
																		);
							if ($studentTime != 'Not Attempted' && $subjectiveQuestions[$index]['listType'] == 'one') {
								// average time
								$query = "SELECT saq.time,saq.marks
											FROM `subjective_attempt_questions` AS saq
											INNER JOIN `questions_subjective` AS qs ON qs.id=saq.questionId
											WHERE qs.id='{$parentId}' AND `attemptId`={$data->attemptId}";
								//$result->query[] = $query;
								//print_r($query);
								$parentStudentMarksTime	= $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$parentStudentMarksTime	= $parentStudentMarksTime->toArray();
								$parentStudentTime		= $parentStudentMarksTime[0]['time'];
								$parentStudentMarks		= $parentStudentMarksTime[0]['marks'];

								$subjectiveQuestions[$index]["time"]	= gmdate("H:i:s",$parentStudentTime);
								$subjectiveQuestions[$index]["marks"]	= $parentStudentMarks;
								$subjectiveQuestions[$index]["attempted"] = 1;
							}
							if ($toppertime != 'Not Attempted' && $subjectiveQuestions[$index]['listType'] == 'one') {
								// average time
								$query = "SELECT saq.time,saq.marks
											FROM `subjective_attempt_questions` AS saq
											INNER JOIN `questions_subjective` AS qs ON qs.id=saq.questionId
											WHERE qs.id='{$parentId}' AND `attemptId`={$topperAttempt}";
								//$result->query[] = $query;
								//print_r($query);
								$parentTopperMarksTime	= $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$parentTopperMarksTime	= $parentTopperMarksTime->toArray();
								$parentTopperTime		= $parentTopperMarksTime[0]['time'];
								$parentTopperMarks		= $parentTopperMarksTime[0]['marks'];

								$subjectiveQuestions[$index]["topper_time"] = gmdate("H:i:s",$parentTopperTime);
								$subjectiveQuestions[$index]["topper_marks"] = $parentTopperMarks;
							}
						}
					}
					// getting topper marks and the average marks of the exam
					$studentId = $questions[0]['studentId'];
					$query = "SELECT sa.id, sa.checked, concat(sd.firstName,' ',sd.lastName) AS name
								FROM `subjective_exam_attempts` AS sa
								INNER JOIN `login_details` AS ld ON ld.id=sa.studentId
								INNER JOIN `student_details` AS sd ON sd.userId=ld.id
								WHERE studentId={$studentId} AND examId={$data->examId} ORDER BY `id`";
					$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$attempts = $attempts->toArray();
					$attemptNo = 1;
					$name = "";
					$checked = 0;
					foreach ($attempts as $key => $value) {
						if ($value['id'] == $data->attemptId) {
							$name = $value['name'];
							$checked = $value['checked'];
							break;
						}
						$attemptNo++;
					}

					$result->student	= $name;
					$result->checked	= $checked;
					$result->attemptNo	= $attemptNo;
					$result->questions	= $subjectiveQuestions;
				} else {
					$result->status = 0;
				}
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function markSubjectiveExamChecked($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$query = "SELECT * FROM `subjective_exam_attempts` WHERE `id`={$data->attemptId}";
				//print_r($query);
				$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$attempts = $attempts->toArray();
				if (count($attempts)) {

				 	//update the marks in the table for question
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('subjective_attempt_questions');
					$update->set(array(
								'check'			=> 1
					));
					$update->where(array('attemptId'	=>	$data->attemptId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();

					// updating total marks of the attempt
					$sql = new Sql($adapter);
					$selectString = "UPDATE subjective_exam_attempts SET score=(select sum(marks) from subjective_attempt_questions where attemptId={$data->attemptId} ) WHERE id={$data->attemptId}";
					$marks = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

					//update the marks in the table for question 
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('subjective_exam_attempts');
					$update->set(array(
								'checked'			=> 1
					));
					$update->where(array('id'	=>	$data->attemptId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();

					$examId	  = $attempts[0]['examId'];

					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('subjective_exam_attempts');
					$select->where(array(
								'id' => $data->attemptId
					));
					$statement = $sql->getSqlStringForSqlObject($select);
					$exam_subjective = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exam_subjective = $exam_subjective->toArray();

					require_once('StudentExam.php');
					$se = new StudentExam();
					$totalScore = $exam_subjective[0]['score'];
					$maximumMarks = $se->getMaxMarksSubjectiveExam($examId);
					$percentage = $totalScore/$maximumMarks * 100;
					if($percentage < 0)
						$percentage = 0;

					require 'Subject.php';
					$s = new Subject();
					$data->contentId = $examId;
					$data->contentType = 'subjective-exam';
					$data->percentage = $percentage;
					$data->studentId = $exam_subjective[0]['studentId'];
					$resSubjectRewards = $s->putSubjectRewardsbyContent($data);
					
					$result->status	 = 1;
				} else {
					$result->status	 = 0;
					$result->message = "Invalid attempt!";
				}
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function checkAttemptTime($time,$answers) {
			return (($answers==0)?('Not attempted'):(gmdate("H:i:s",$time)));
		}
		public function checkAttemptMarks($marks,$answers) {
			return (($answers==0)?('NA'):($marks));
		}
		public function getStudentSubjectiveExamAttemps($data) {
			try {
				$time = time();
				$result = new stdClass();
				$adapter = $this->adapter;
				$query="SELECT e.id,e.name exam ,e.type,e.chapterId,ifnull(c.name,'Independent') as chapter,e.subjectId, s.name subject,e.startDate,e.endDate,e.attempts,
							(SELECT count('id') FROM subjective_exam_attempts et WHERE et.examId=e.id AND et.studentId=$data->userId) examgiven,
							(SELECT count('id') FROM subjective_exam_attempts et WHERE et.examId=e.id AND et.studentId=$data->userId AND completed=0) paused,(SELECT count('id') FROM subjective_exam_attempts et WHERE et.examId=e.id AND et.studentId=$data->userId AND checked=1) checked 
						FROM exam_subjective e
						INNER JOIN subjects s ON e.subjectId=s.id   left join  chapters c ON e.chapterId=c.id
						INNER JOIN student_subject AS ss ON ss.subjectId=s.id AND ss.userId={$data->userId} AND ss.Active=1
						WHERE s.courseId=$data->courseId AND s.deleted=0 AND e.status=2 AND e.Active=1 AND ((e.endDate='') || (e.endDate!='' && e.endDate>{$time}000))
						GROUP BY ss.subjectId
						ORDER BY e.endDate";
				$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$temp = $exams->toArray();
				//print_r($temp);
				if (count($temp) == 0) {
					$result->status = 0;
					$result->exception = "No Exams Found";
					return $result;
				}
				$result->currentDate=time();
				//now filtering the exams which has to be given now according to bug ARC-454
				//we will delete all exams and assignments which have crossed the number of attempts or its date
				//var_dump($temp);
				foreach($temp as $key=>$data1) {
					//print_r($data1);
					$examId=$data1['id'];
					$query="SELECT sd.userId, concat(sd.firstName,' ',sd.lastName) AS name,sa.id AS attemptId, sa.endDate, sa.startDate, sa.checked
							FROM `subjective_exam_attempts` AS sa
							LEFT JOIN `student_details` AS sd ON sd.userId=sa.studentId
							WHERE sa.examId={$examId} AND sa.studentId = {$data->userId} AND sa.completed=1";
					//print_r($query);
					$subjectiveStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$temp[$key]['listAttempts'] = $subjectiveStudents->toArray();

					//commenting this according to current requirements
					if($data1['attempts']-$data1['examgiven'] <= 0) {
						//delete current key
						unset($temp[$key]);
					}
				}
				//var_dump($temp);
				$result->exams = $temp;
				$result->status = 1;
				//print_r($result);
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getStudentManualExams($data) {
			try {
				$result = new stdClass();
				$adapter = $this->adapter;
				$query="SELECT em.*, s.name AS subjectName
						FROM `exam_manual` AS em
						INNER JOIN `student_subject` AS ss ON ss.subjectId=em.subjectId AND ss.courseId={$data->courseId} AND ss.userId={$data->userId} AND ss.Active=1
						INNER JOIN `exam_manual_marks` AS emm ON emm.exam_id=em.id AND emm.student_id=ss.userId
						INNER JOIN `subjects` AS s ON s.id=ss.subjectId
						WHERE em.status=1
						GROUP BY em.id";
				$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$temp = $exams->toArray();
				//print_r($temp);
				if (count($temp) == 0) {
					$result->status = 0;
					$result->exception = "No Exams Found";
					return $result;
				}
				$result->currentDate=time();
				$result->exams = $temp;
				$result->status = 1;
				//print_r($result);
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
	
		public function getAttemptlistSubjectiveExamStudents($data) {
			$result = new stdClass();
			try {
				$subjectiveExamArray=array();
				$studentIds=array();
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$query="SELECT sd.userId, concat(sd.firstName,' ',sd.lastName) AS name
						FROM `subjective_exam_attempts` AS sa
						LEFT JOIN `student_details` AS sd ON sd.userId=sa.studentId
						WHERE sa.examId={$data->examId} AND sa.completed=1 GROUP BY sa.studentId ORDER BY sa.studentId";
				$subjectiveStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$students = $subjectiveStudents->toArray();
				$i = 0;
				foreach($students as $key=>$value)
				{
					$subjectiveExamArray[$i] = $value;
					$query="SELECT count(sa.id) AS attempts
							FROM `subjective_exam_attempts` AS sa
							WHERE sa.examId={$data->examId} AND sa.studentId={$value['userId']} AND sa.completed=1 ORDER BY sa.studentId";
					$subjectiveAttempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$subjectiveAttempts=$subjectiveAttempts->toArray();
					//print_r($subjectiveAttempts);
					$subjectiveAttempts=$subjectiveAttempts[0]['$subjectiveAttempts'];
					$subjectiveExamArray[$i]["attempts"] = $subjectiveAttempts->toArray();
					
					//print_r($subjectiveAttempts);
					/*$index='';
					if(in_array($value['userId'],$studentIds))
					{

						$subjectiveExamArray[$i] = $value;
						$query="SELECT count(sa.id) AS attempts
								FROM `subjective_exam_attempts` AS sa
								WHERE sa.examId={$data->examId} AND sa.studentId={$value['userId']} AND sa.completed=1 ORDER BY sa.studentId";
						$subjectiveAttempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$subjectiveAttempts=$subjectiveAttempts->toArray();
						//print_r($subjectiveAttempts);
						$subjectiveAttempts=$subjectiveAttempts[0]['$subjectiveAttempts'];
						$subjectiveExamArray[$i]["attempts"] = $subjectiveAttempts->toArray();
						
						//print_r($subjectiveAttempts);
						/*$index='';
						if(in_array($value['userId'],$studentIds))
						{
							$index=array_search($value['userId'],$studentIds);
							$newArray=$value;
							unset($newArray['name']);
							unset($newArray['userId']);
							array_push($subjectiveExamArray[$index]['student'],$newArray);
						}
						else{
							array_push($studentIds,$value['userId']);
							$index=count($studentIds)-1;
							
							$newArray=$value;
							unset($newArray['endDate']);
							unset($newArray['startDate']);
							unset($newArray['id']);
							
							$subjectiveExamArray[$index]['student']=$newArray;
							$newArray=$value;
							unset($newArray['name']);
							unset($newArray['userId']);
							$subjectiveExamArray[$index]['student'][0]=$newArray;
						}

						$index=array_search($value['userId'],$studentIds);
						$newArray=$value;
						unset($newArray['name']);
						unset($newArray['userId']);
						array_push($subjectiveExamArray[$index]['student'],$newArray);

					}
					else{
						array_push($studentIds,$value['userId']);
						$index=count($studentIds)-1;
						
						$newArray=$value;
						unset($newArray['endDate']);
						unset($newArray['startDate']);
						unset($newArray['id']);
						
						$subjectiveExamArray[$index]['student']=$newArray;
						$newArray=$value;
						unset($newArray['name']);
						unset($newArray['userId']);
						$subjectiveExamArray[$index]['student'][0]=$newArray;
					}*/
				}
				//print_r($subjectiveExamArray);
				$result->students=$subjectiveExamArray;
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function studentExamsChapterWise($data) {
			$result = new stdClass();
			$certification = new stdClass();
			$totalContent=0;
			$toalWatched=0;
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				//SELECT  s.name subject, c.name course FROM subjects s JOIN courses c on c.id=s.courseId  where s.id= $data->subjectId and c.id=$data->courseId";
				$select->from(array('s' => 'subjects'));
				$select->join(array('c' => 'courses'),
					'c.id = s.courseId',
					array('course'	=>	'name')
				);
				$select->where(array(
					's.id'	=>	$data->subjectId,
					'c.id'	=>	$data->courseId
				));
				$query = $sql->getSqlStringForSqlObject($select);
				$subject = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$subject=$subject->toArray();
				//var_dump($subject);
				$result->subject =$subject[0]['name'];
				$result->course =$subject[0]['course'];
				$certification->course=$subject[0]['course'];
				
				$query="SELECT syllabus FROM subjects where id= $data->subjectId and deleted=0";
				$syllabus = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$syllabus=$syllabus->toArray();
				$syllabus =$syllabus[0]['syllabus'];
				
				$result->syllabus='';
				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1' && isset($syllabus) && !empty($syllabus)) {
						require_once 'amazonRead.php';
						$result->syllabus = getSignedURL($syllabus);
				}
				$query="SELECT id, name FROM chapters where subjectId= $data->subjectId and deleted=0 ORDER BY weight ASC, id DESC";
				$chapters = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->chapters =$chapters->toArray();
				$actContent_watched=array();
				$chaptersArr=array();
				$contentCount=array();
				
				$content_videos=array();
				$chapter_progress=array();
				require_once 'DynamoDbOps.php';
				$ddo = new DynamoDbOps();
				$content_watched=$ddo->getAllWatchedInformation($data);
				$content_watched=$content_watched->items;
				foreach ($content_watched as $key => $value) {
					if ($value['percentRead'] > 70) {
						array_push($actContent_watched,$value['contentId']);
					}
				}

				if((count($actContent_watched)>0))
				{
					$query="SELECT chapterId, count( chapterId )as watched_videos FROM `content` WHERE content.id IN (".implode(',',$actContent_watched).") GROUP BY chapterId " ;
					
					$content_aggr = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$content_aggr = $content_aggr->toArray();
					
					for($i=0;$i<count($content_aggr);$i++)
					{
						$chaptersArr[$i]=$content_aggr[$i]['chapterId'];
						$contentCount[$i]=$content_aggr[$i]['watched_videos'];
					}
				}
				
				foreach ($result->chapters as $key => $chapter) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					//$select->from('content')->where(array('chapterId' => $chapter['id']));
					$select->from('content')->where(array('chapterId' => $chapter['id'],'published' => 1));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$temp = $temp->toArray();
					if(count($temp) > 0){
						$result->chapters[$key]['content'] = $temp;
						//$totalContent=$totalContent+$temp[0]['content'];
					}

					if(in_array( $chapter['id'] , $chaptersArr))
					{
						$result->chapters[$key]['seen']=$contentCount[array_search($chapter['id'] , $chaptersArr)];
					} else {
						$result->chapters[$key]['seen']=0;
					}
					$toalWatched=$toalWatched+$result->chapters[$key]['seen'];
					
					// Objective exams
					$query="SELECT e.id,e.name exam ,e.type,e.chapterId,e.showResult,e.showResultTill,e.showResultTillDate, e.startDate,e.endDate,e.attempts,c.endDate AS courseEndDate,(
								SELECT count('id')
								FROM exam_attempts et
								WHERE et.examId=e.id AND et.studentId={$data->userId} AND et.completed=1
							) examgiven,(
								SELECT count('id')
								FROM exam_attempts et
								WHERE et.examId=e.id AND et.studentId={$data->userId} AND completed=0
							) paused
							FROM exams e
							INNER JOIN subjects s ON s.id=e.subjectId
							INNER JOIN courses c ON c.id=s.courseId
							WHERE e.subjectId={$data->subjectId} AND e.delete=0 AND e.chapterid={$chapter['id']} AND e.`status` = 2";
					$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$result->chapters[$key]['exams']=$exams->toArray();

					$examLearnObjs = $result->chapters[$key]['exams'];
					foreach ($examLearnObjs as $key1 => $examObj) {
						$query="SELECT qt.id, qt.tag
								FROM `questions` AS q
								INNER JOIN `question_category_links` AS qcl ON qcl.questionId=q.id
								INNER JOIN `section_categories` AS sc ON sc.id=qcl.categoryId
								INNER JOIN `exam_sections` AS es ON es.id=sc.sectionId
								INNER JOIN `question_tag_links` AS qtl ON qtl.question_id=q.id
								INNER JOIN `question_tags` AS qt ON qt.id=qtl.tag_id
								WHERE es.examId={$examObj['id']}
								GROUP BY qt.id";
						$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$examLearnObjs[$key1]['tags']=$tags->toArray();

						/*$dataRange = new stdClass();
						$dataRange->examId = $examObj['id'];
						$examLearnObjs[$key1]['ranges']=$this->getPercentRanges($dataRange);*/

						foreach ($examLearnObjs[$key1]['tags'] as $key2 => $tagVal) {
							$dataTag = new stdClass();
							$dataTag->tagId = $tagVal['id'];
							$dataTag->examId = $examObj['id'];
							$dataTag->examType = 'exam';
							$dataTag->userId = $data->userId;
							$dataTag->userRole = $data->userRole;
							$tagData = Api::tagExamAnalysis($dataTag);
							if ($tagData->status == 1) {
								$tagData = $tagData->tagData;
							} else {
								$tagData = array();
							}
							$examLearnObjs[$key1]['tags'][$key2]['analysis'] = $tagData;
							$dataRange = new stdClass();
							$dataRange->examId = $dataTag->examId;
							$dataRange->examType = 'exam';
							$ranges=$this->getPercentRanges($dataRange);
							if ($ranges->status == 1) {
								$ranges = $ranges->score;
							} else {
								$ranges = array();
							}
							$examLearnObjs[$key1]['tags'][$key2]['ranges'] = $ranges;
						}
					}
					$result->chapters[$key]['exams']=$examLearnObjs;

					// Subjective exams
					$query="SELECT e.id,e.name exam ,e.type,e.chapterId,ifnull(c.name,'Independent') as chapter,e.subjectId, s.name subject,e.startDate,e.endDate,e.attempts,
							(SELECT count('id') FROM subjective_exam_attempts et WHERE et.examId=e.id AND et.studentId={$data->userId} AND completed=1) examgiven,
							(SELECT count('id') FROM subjective_exam_attempts et WHERE et.examId=e.id AND et.studentId={$data->userId} AND completed=0) paused,(SELECT count('id') FROM subjective_exam_attempts et WHERE et.examId=e.id AND et.studentId={$data->userId} AND checked=1) checked 
						FROM exam_subjective e
						LEFT JOIN subjects s ON e.subjectId=s.id
						LEFT JOIN chapters c ON e.chapterId=c.id
						WHERE s.deleted=0 AND e.status=2 AND Active=1 AND e.subjectId={$data->subjectId} AND e.chapterId={$chapter['id']}";
					
					$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$temp = $exams->toArray();
					if (count($temp) > 0) {

						foreach($temp as $key1=>$data1) {
							//print_r($data1);
							$examId=$data1['id'];
							$query="SELECT sd.userId, concat(sd.firstName,' ',sd.lastName) AS name,sa.id AS attemptId, sa.endDate, sa.startDate, sa.checked
									FROM `subjective_exam_attempts` AS sa
									LEFT JOIN `student_details` AS sd ON sd.userId=sa.studentId
									WHERE sa.examId={$examId} AND sa.studentId = {$data->userId} AND sa.completed=1
									ORDER BY sa.id";
							//print_r($query);
							$subjectiveStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$temp[$key1]['listAttempts'] = $subjectiveStudents->toArray();

							//commenting this according to current requirements
							/*if($data1['attempts']-$data1['examgiven'] <= 0) {
								//delete current key1
								unset($temp[$key1]);
							}*/
						}
						//var_dump($temp);
						$result->chapters[$key]['subjectiveExams']=$temp;
					} else {
						$result->chapters[$key]['subjectiveExams']=array();
					}

					$examLearnObjs = $result->chapters[$key]['subjectiveExams'];
					foreach ($examLearnObjs as $key1 => $examObj) {
						$query="SELECT qt.id, qt.tag
								FROM `questions_subjective` AS q
								INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=q.id
								INNER JOIN `exam_subjective` AS es ON es.id=qsel.examId
								INNER JOIN `subjective_question_tag_links` AS qtl ON qtl.question_id=q.id
								INNER JOIN `question_tags` AS qt ON qt.id=qtl.tag_id
								WHERE es.id={$examObj['id']}
								GROUP BY qt.id";
						$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$examLearnObjs[$key1]['tags']=$tags->toArray();

						foreach ($examLearnObjs[$key1]['tags'] as $key2 => $tagVal) {
							$dataTag = new stdClass();
							$dataTag->tagId = $tagVal['id'];
							$dataTag->examId = $examObj['id'];
							$dataTag->examType = 'subjective';
							$dataTag->userId = $data->userId;
							$dataTag->userRole = $data->userRole;
							$tagData = Api::tagExamAnalysis($dataTag);
							if ($tagData->status == 1) {
								$tagData = $tagData->tagData;
							} else {
								$tagData = array();
							}
							$examLearnObjs[$key1]['tags'][$key2]['analysis'] = $tagData;
							$dataRange = new stdClass();
							$dataRange->examId = $dataTag->examId;
							$dataRange->examType = 'subjective-exam';
							$ranges=$this->getPercentRanges($dataRange);
							if ($ranges->status == 1) {
								$ranges = $ranges->score;
							} else {
								$ranges = array();
							}
							$examLearnObjs[$key1]['tags'][$key2]['ranges'] = $ranges;
						}
					}
					$result->chapters[$key]['subjectiveExams']=$examLearnObjs;

					// Manual exams
					$query="SELECT em.*
							FROM `exam_manual` AS em
							INNER JOIN `student_subject` AS ss ON ss.subjectId=em.subjectId AND ss.courseId={$data->courseId} AND ss.userId={$data->userId} AND ss.Active=1
							INNER JOIN `exam_manual_marks` AS emm ON emm.exam_id=em.id AND emm.student_id=ss.userId
							WHERE em.status=1 AND em.subjectId={$data->subjectId} AND em.chapterId={$chapter['id']}
							GROUP BY em.id";
					$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$temp = $exams->toArray();
					//print_r($temp);
					if (count($temp) > 0) {
						$result->chapters[$key]['manualExams']=$temp;
					} else {
						$result->chapters[$key]['manualExams']=array();
					}


					$submissionExams = array();
					// Submissions
					$query="SELECT e.id,e.name exam,e.chapterId,ifnull(c.name,'Independent') as chapter,e.subjectId, s.name subject,e.startDate,e.endDate,e.attempts,
							(SELECT count('id') FROM submission_attempts et WHERE et.examId=e.id AND et.studentId={$data->userId} AND completed=1) examgiven,
							(SELECT count('id') FROM submission_attempts et WHERE et.examId=e.id AND et.studentId={$data->userId} AND completed=0) paused,
							(SELECT count('id') FROM submission_attempts et WHERE et.examId=e.id AND et.studentId={$data->userId} AND checked=1) checked 
						FROM submissions e
						INNER JOIN subjects s ON e.subjectId=s.id
						LEFT JOIN chapters c ON e.chapterId=c.id
						WHERE s.deleted=0 AND e.access='public' AND e.status=2 AND e.subjectId={$data->subjectId} AND e.chapterId={$chapter['id']}";
					$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$temp = $exams->toArray();
					$submissionExams = $temp;

					require_once("Subject.php");
					$s = new Subject();
					$dataSG = new stdClass();
					$dataSG->subjectId = $data->subjectId;
					$dataSG->studentId = $data->userId;
					$studentGroup = $s->getStudentGroup($dataSG);
					if ($studentGroup->status == 1) {
						$data->studentGroupId = $studentGroup->studentGroupId;
						// Submissions
						$query="SELECT e.id,e.name exam,e.chapterId,ifnull(c.name,'Independent') as chapter,e.subjectId, s.name subject,e.startDate,e.endDate,e.attempts,
								(SELECT count('id') FROM submission_attempts et WHERE et.examId=e.id AND et.studentGroupId={$data->studentGroupId} AND completed=1) examgiven,
								(SELECT count('id') FROM submission_attempts et WHERE et.examId=e.id AND et.studentGroupId={$data->studentGroupId} AND completed=0) paused,
								(SELECT count('id') FROM submission_attempts et WHERE et.examId=e.id AND et.studentGroupId={$data->studentGroupId} AND checked=1) checked 
							FROM submissions e
							INNER JOIN submission_access sa ON sa.submissionId=e.id
							INNER JOIN student_subject_groups ssg ON ssg.id=sa.studentGroupId
							INNER JOIN student_subject_group_students ssgs ON ssgs.groupId=ssg.id
							INNER JOIN subjects s ON e.subjectId=s.id
							LEFT JOIN chapters c ON e.chapterId=c.id
							WHERE s.deleted=0 AND e.access='private' AND e.status=2 AND e.subjectId={$data->subjectId} AND e.chapterId={$chapter['id']} AND ssgs.studentId={$data->userId}";
						//$result->submissionQueries[] = preg_replace('/\s+/S', " ", $query);
						$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$temp = $exams->toArray();
						$submissionExams = array_merge($submissionExams,$temp);
					}
					
					if (count($submissionExams) > 0) {

						foreach($submissionExams as $key1=>$data1) {
							//print_r($data1);
							$examId=$data1['id'];
							$query="SELECT sd.userId, concat(sd.firstName,' ',sd.lastName) AS name,sa.id AS attemptId, sa.endDate, sa.startDate, sa.checked
									FROM `submission_attempts` AS sa
									INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
									LEFT JOIN `student_details` AS sd ON sd.userId=sa.studentId
									WHERE sa.examId={$examId} AND satm.studentId = {$data->userId} AND sa.completed=1
									ORDER BY satm.id";
							//print_r($query);
							$submissionStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$submissionExams[$key1]['listAttempts'] = $submissionStudents->toArray();
						}
						//var_dump($submissionExams);
						$result->chapters[$key]['submissions']=$submissionExams;
					} else {
						$result->chapters[$key]['submissions']=array();
					}

					$examLearnObjs = $result->chapters[$key]['submissions'];
					foreach ($examLearnObjs as $key1 => $examObj) {
						$examLearnObjsTags=array();
						$query="SELECT qt.id, qt.tag
								FROM `submission_questions` AS q
								INNER JOIN `submission_question_links` AS qsel ON qsel.questionId=q.id
								INNER JOIN `submissions` AS es ON es.id=qsel.examId
								INNER JOIN `submission_question_tag_links` AS qtl ON qtl.question_id=q.id
								INNER JOIN `question_tags` AS qt ON qt.id=qtl.tag_id
								WHERE es.access='public' AND es.id={$examObj['id']}
								GROUP BY qt.id";
						$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$temp = $tags->toArray();
						$examLearnObjsTags=$temp;
						$query="SELECT qt.id, qt.tag
								FROM `submission_questions` AS q
								INNER JOIN `submission_question_links` AS qsel ON qsel.questionId=q.id
								INNER JOIN `submissions` AS e ON e.id=qsel.examId
								INNER JOIN `submission_question_tag_links` AS qtl ON qtl.question_id=q.id
								INNER JOIN `question_tags` AS qt ON qt.id=qtl.tag_id
								INNER JOIN `submission_access` sa ON sa.submissionId=e.id
								INNER JOIN `student_subject_groups` ssg ON ssg.id=sa.studentGroupId
								INNER JOIN `student_subject_group_students` ssgs ON ssgs.groupId=ssg.id
								WHERE e.access='private' AND e.id={$examObj['id']} AND ssgs.studentId={$data->userId}
								GROUP BY qt.id";
						$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$temp = $tags->toArray();
						$examLearnObjs[$key1]['tags'] = array_merge($examLearnObjsTags,$temp);

						foreach ($examLearnObjs[$key1]['tags'] as $key2 => $tagVal) {
							$dataTag = new stdClass();
							$dataTag->tagId = $tagVal['id'];
							$dataTag->examId = $examObj['id'];
							$dataTag->examType = 'submission';
							$dataTag->userId = $data->userId;
							$dataTag->userRole = $data->userRole;
							$tagData = Api::tagExamAnalysis($dataTag);
							if ($tagData->status == 1) {
								$tagData = $tagData->tagData;
							} else {
								$tagData = array();
							}
							$examLearnObjs[$key1]['tags'][$key2]['analysis'] = $tagData;
							$dataRange = new stdClass();
							$dataRange->examId = $dataTag->examId;
							$dataRange->examType = 'submission';
							$ranges=$this->getPercentRanges($dataRange);
							if ($ranges->status == 1) {
								$ranges = $ranges->score;
							} else {
								$ranges = array();
							}
							$examLearnObjs[$key1]['tags'][$key2]['ranges'] = $ranges;
						}
					}
					$result->chapters[$key]['submissions']=$examLearnObjs;
				}
				$query ="SELECT e.id,e.name exam ,e.type,e.chapterId,e.showResult,e.showResultTill,e.showResultTillDate, e.startDate,e.endDate,e.attempts,c.endDate AS courseEndDate, (
							SELECT count('id')
							FROM exam_attempts et
							WHERE et.examId=e.id and et.studentId = {$data->userId} and et.completed = 1
						) examgiven, (
							SELECT count('id')
							FROM exam_attempts et
							WHERE et.examId=e.id and et.studentId=$data->userId AND completed = 0
						) paused
						FROM exams e
						INNER JOIN subjects s ON s.id=e.subjectId
						INNER JOIN courses c ON c.id=s.courseId
						WHERE e.subjectId = {$data->subjectId} AND e.chapterid = 0 AND e.delete = 0 AND e.`status` = 2";
				$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->indepdentExams = $exams->toArray();

				$examLearnObjs = $result->indepdentExams;
				foreach ($examLearnObjs as $key1 => $examObj) {
					$query="SELECT qt.id, qt.tag
							FROM `questions` AS q
							INNER JOIN `question_category_links` AS qcl ON qcl.questionId=q.id
							INNER JOIN `section_categories` AS sc ON sc.id=qcl.categoryId
							INNER JOIN `exam_sections` AS es ON es.id=sc.sectionId
							INNER JOIN `question_tag_links` AS qtl ON qtl.question_id=q.id
							INNER JOIN `question_tags` AS qt ON qt.id=qtl.tag_id
							WHERE es.examId={$examObj['id']}
							GROUP BY qt.id";
					$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$examLearnObjs[$key1]['tags']=$tags->toArray();

					/*$dataRange = new stdClass();
					$dataRange->examId = $examObj['id'];
					$examLearnObjs[$key1]['ranges']=$this->getPercentRanges($dataRange);*/

					foreach ($examLearnObjs[$key1]['tags'] as $key2 => $tagVal) {
						$dataTag = new stdClass();
						$dataTag->tagId = $tagVal['id'];
						$dataTag->examId = $examObj['id'];
						$dataTag->examType = 'exam';
						$dataTag->userId = $data->userId;
						$dataTag->userRole = $data->userRole;
						$tagData = Api::tagExamAnalysis($dataTag);
						if ($tagData->status == 1) {
							$tagData = $tagData->tagData;
						} else {
							$tagData = array();
						}
						$examLearnObjs[$key1]['tags'][$key2]['analysis'] = $tagData;
						$dataRange = new stdClass();
						$dataRange->examId = $dataTag->examId;
						$dataRange->examType = 'exam';
						$ranges=$this->getPercentRanges($dataRange);
						if ($ranges->status == 1) {
							$ranges = $ranges->score;
						} else {
							$ranges = array();
						}
						$examLearnObjs[$key1]['tags'][$key2]['ranges'] = $ranges;
					}
				}
				$result->indepdentExams = $examLearnObjs;

				$query="SELECT e.id,e.name exam ,e.type,e.chapterId,ifnull(c.name,'Independent') as chapter,e.subjectId, s.name subject,e.startDate,e.endDate,e.attempts,
							(SELECT count('id') FROM subjective_exam_attempts et WHERE et.examId=e.id AND et.studentId={$data->userId} AND completed=1) examgiven,
							(SELECT count('id') FROM subjective_exam_attempts et WHERE et.examId=e.id AND et.studentId={$data->userId} AND completed=0) paused,(SELECT count('id') FROM subjective_exam_attempts et WHERE et.examId=e.id AND et.studentId={$data->userId} AND checked=1) checked 
						FROM exam_subjective e
						LEFT JOIN subjects s ON e.subjectId=s.id
						LEFT JOIN chapters c ON e.chapterId=c.id
						WHERE s.deleted=0 AND e.status=2 AND Active=1 AND e.subjectId={$data->subjectId} AND (e.chapterId='0' || e.chapterId='-1')";
					
				$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$temp = $exams->toArray();
				//print_r($temp);
				if (count($temp) > 0) {

					foreach($temp as $key1=>$data1) {
						//print_r($data1);
						$examId=$data1['id'];
						$query="SELECT sd.userId, concat(sd.firstName,' ',sd.lastName) AS name,sa.id AS attemptId, sa.endDate, sa.startDate, sa.checked
								FROM `subjective_exam_attempts` AS sa
								LEFT JOIN `student_details` AS sd ON sd.userId=sa.studentId
								WHERE sa.examId={$examId} AND sa.studentId = {$data->userId} AND sa.completed=1
								ORDER BY sa.id";
						//print_r($query);
						$subjectiveStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$temp[$key1]['listAttempts'] = $subjectiveStudents->toArray();

						//commenting this according to current requirements
						/*if($data1['attempts']-$data1['examgiven'] <= 0) {
							//delete current key1
							unset($temp[$key1]);
						}*/
					}
					//var_dump($temp);
					$temp = array_values($temp);
					$result->independentSubjectiveExams=$temp;

					$examLearnObjs = $result->independentSubjectiveExams;
					foreach ($examLearnObjs as $key1 => $examObj) {
						$query="SELECT qt.id, qt.tag
								FROM `questions_subjective` AS q
								INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=q.id
								INNER JOIN `exam_subjective` AS es ON es.id=qsel.examId
								INNER JOIN `subjective_question_tag_links` AS qtl ON qtl.question_id=q.id
								INNER JOIN `question_tags` AS qt ON qt.id=qtl.tag_id
								WHERE es.id={$examObj['id']}
								GROUP BY qt.id";
						$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$examLearnObjs[$key1]['tags']=$tags->toArray();

						foreach ($examLearnObjs[$key1]['tags'] as $key2 => $tagVal) {
							$dataTag = new stdClass();
							$dataTag->tagId = $tagVal['id'];
							$dataTag->examId = $examObj['id'];
							$dataTag->examType = 'subjective';
							$dataTag->userId = $data->userId;
							$dataTag->userRole = $data->userRole;
							$tagData = Api::tagExamAnalysis($dataTag);
							if ($tagData->status == 1) {
								$tagData = $tagData->tagData;
							} else {
								$tagData = array();
							}
							$examLearnObjs[$key1]['tags'][$key2]['analysis'] = $tagData;
							$dataRange = new stdClass();
							$dataRange->examId = $dataTag->examId;
							$dataRange->examType = 'subjective-exam';
							$ranges=$this->getPercentRanges($dataRange);
							if ($ranges->status == 1) {
								$ranges = $ranges->score;
							} else {
								$ranges = array();
							}
							$examLearnObjs[$key1]['tags'][$key2]['ranges'] = $ranges;
						}
					}
					$result->independentSubjectiveExams=$examLearnObjs;
				} else {
					$result->independentSubjectiveExams=array();
				}

				$query="SELECT em.*
						FROM `exam_manual` AS em
						INNER JOIN `student_subject` AS ss ON ss.subjectId=em.subjectId AND ss.courseId={$data->courseId} AND ss.userId={$data->userId} AND ss.Active=1
						INNER JOIN `exam_manual_marks` AS emm ON emm.exam_id=em.id AND emm.student_id=ss.userId
						WHERE em.status=1 AND em.subjectId={$data->subjectId} AND em.chapterId=0
						GROUP BY em.id";
				$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$temp = $exams->toArray();
				//print_r($temp);
				if (count($temp) > 0) {
					$result->independentManualExams=$temp;
				} else {
					$result->independentManualExams=array();
				}

				$submissionExams = array();
				$query="SELECT e.id,e.name exam,e.chapterId,ifnull(c.name,'Independent') as chapter,e.subjectId, s.name subject,e.startDate,e.endDate,e.attempts,
							(SELECT count('id') FROM submission_attempts et WHERE et.examId=e.id AND et.studentId={$data->userId} AND completed=1) examgiven,
							(SELECT count('id') FROM submission_attempts et WHERE et.examId=e.id AND et.studentId={$data->userId} AND completed=0) paused,(SELECT count('id') FROM submission_attempts et WHERE et.examId=e.id AND et.studentId=$data->userId AND checked=1) checked 
						FROM submissions e
						INNER JOIN subjects s ON e.subjectId=s.id
						LEFT JOIN chapters c ON e.chapterId=c.id
						WHERE s.deleted=0 AND e.access='public' AND e.status=2 AND e.subjectId={$data->subjectId} AND (e.chapterId='0' || e.chapterId='-1')";
				$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$temp = $exams->toArray();
				$submissionExams = $temp;
				$query="SELECT e.id,e.name exam,e.chapterId,ifnull(c.name,'Independent') as chapter,e.subjectId, s.name subject,e.startDate,e.endDate,e.attempts,
							(SELECT count('id') FROM submission_attempts et WHERE et.examId=e.id AND et.studentId={$data->userId} AND completed=1) examgiven,
							(SELECT count('id') FROM submission_attempts et WHERE et.examId=e.id AND et.studentId={$data->userId} AND completed=0) paused,(SELECT count('id') FROM submission_attempts et WHERE et.examId=e.id AND et.studentId={$data->userId} AND checked=1) checked 
						FROM submissions e
						INNER JOIN submission_access sa ON sa.submissionId=e.id
						INNER JOIN student_subject_groups ssg ON ssg.id=sa.studentGroupId
						INNER JOIN student_subject_group_students ssgs ON ssgs.groupId=ssg.id
						INNER JOIN subjects s ON e.subjectId=s.id
						LEFT JOIN chapters c ON e.chapterId=c.id
						WHERE s.deleted=0 AND e.access='private' AND e.status=2 AND e.subjectId={$data->subjectId} AND (e.chapterId='0' || e.chapterId='-1') AND ssgs.studentId={$data->userId}";
				$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$temp = $exams->toArray();
				$submissionExams = array_merge($submissionExams,$temp);
				//print_r($temp);
				if (count($submissionExams) > 0) {

					foreach($submissionExams as $key1=>$data1) {
						//print_r($data1);
						$examId=$data1['id'];
						$query="SELECT sd.userId, concat(sd.firstName,' ',sd.lastName) AS name,sa.id AS attemptId, sa.endDate, sa.startDate, sa.checked
								FROM `submission_attempts` AS sa
								LEFT JOIN `student_details` AS sd ON sd.userId=sa.studentId
								WHERE sa.examId={$examId} AND sa.studentId = {$data->userId} AND sa.completed=1
								ORDER BY sa.id";
						//print_r($query);
						$subjectiveStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$submissionExams[$key1]['listAttempts'] = $subjectiveStudents->toArray();
					}
					//var_dump($submissionExams);
					$submissionExams = array_values($submissionExams);
					$result->independentSubmissions=$submissionExams;

					$examLearnObjs = $result->independentSubmissions;
					foreach ($examLearnObjs as $key1 => $examObj) {
						$examLearnObjsTags=array();
						$query="SELECT qt.id, qt.tag
								FROM `questions_subjective` AS q
								INNER JOIN `submission_question_links` AS qsel ON qsel.questionId=q.id
								INNER JOIN `submissions` AS es ON es.id=qsel.examId
								INNER JOIN `submission_question_tag_links` AS qtl ON qtl.question_id=q.id
								INNER JOIN `question_tags` AS qt ON qt.id=qtl.tag_id
								WHERE es.access='public' AND es.id={$examObj['id']}
								GROUP BY qt.id";
						$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$temp = $tags->toArray();
						$examLearnObjsTags=$temp;
						$query="SELECT qt.id, qt.tag
								FROM `questions_subjective` AS q
								INNER JOIN `submission_question_links` AS qsel ON qsel.questionId=q.id
								INNER JOIN `submissions` AS e ON e.id=qsel.examId
								INNER JOIN `submission_question_tag_links` AS qtl ON qtl.question_id=q.id
								INNER JOIN `question_tags` AS qt ON qt.id=qtl.tag_id
								INNER JOIN `submission_access` sa ON sa.submissionId=e.id
								INNER JOIN `student_subject_groups` ssg ON ssg.id=sa.studentGroupId
								INNER JOIN `student_subject_group_students` ssgs ON ssgs.groupId=ssg.id
								WHERE e.access='private' AND e.id={$examObj['id']} AND ssgs.studentId={$data->userId}
								GROUP BY qt.id";
						$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$temp = $tags->toArray();
						$examLearnObjs[$key1]['tags'] = array_merge($examLearnObjsTags,$temp);

						foreach ($examLearnObjs[$key1]['tags'] as $key2 => $tagVal) {
							$dataTag = new stdClass();
							$dataTag->tagId = $tagVal['id'];
							$dataTag->examId = $examObj['id'];
							$dataTag->examType = 'submission';
							$dataTag->userId = $data->userId;
							$dataTag->userRole = $data->userRole;
							$tagData = Api::tagExamAnalysis($dataTag);
							if ($tagData->status == 1) {
								$tagData = $tagData->tagData;
							} else {
								$tagData = array();
							}
							$examLearnObjs[$key1]['tags'][$key2]['analysis'] = $tagData;
							$dataRange = new stdClass();
							$dataRange->examId = $dataTag->examId;
							$dataRange->examType = 'submission';
							$ranges=$this->getPercentRanges($dataRange);
							if ($ranges->status == 1) {
								$ranges = $ranges->score;
							} else {
								$ranges = array();
							}
							$examLearnObjs[$key1]['tags'][$key2]['ranges'] = $ranges;
						}
					}
					$result->independentSubmissions=$examLearnObjs;
				} else {
					$result->independentSubmissions=array();
				}
				
				//print_r($newCheck);	
				$result->currentDate=time();
				$result->status = 1;
				
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function studentSubjectiveExamsChapterWise($data)
		{
			try {
				$result = new stdClass();
				$adapter = $this->adapter;
				$query="SELECT e.id,e.name exam ,e.type,e.chapterId,ifnull(c.name,'Independent') as chapter,e.subjectId, s.name subject,e.startDate,e.endDate,e.attempts,
							(SELECT count('id') FROM subjective_exam_attempts et WHERE et.examId=e.id AND et.studentId=$data->userId) examgiven,
							(SELECT count('id') FROM subjective_exam_attempts et WHERE et.examId=e.id AND et.studentId=$data->userId AND completed=0) paused,(SELECT count('id') FROM subjective_exam_attempts et WHERE et.examId=e.id AND et.studentId=$data->userId AND checked=1) checked 
						FROM exam_subjective e
						LEFT JOIN subjects s ON e.subjectId=s.id
						LEFT JOIN chapters c ON e.chapterId=c.id
						WHERE s.courseId=$data->courseId AND s.deleted=0 AND e.status=2 AND Active=1 AND e.subjectId={$data->subjectId}";
				$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$temp = $exams->toArray();
				//print_r($temp);
				if (count($temp) == 0) {
					$result->status = 0;
					$result->exception = "No Exams Found";
					return $result;
				}
				$result->currentDate=time();
				//now filtering the exams which has to be given now according to bug ARC-454
				//we will delete all exams and assignments which have crossed the number of attempts or its date
				//var_dump($temp);
				foreach($temp as $key=>$data1) {
					//print_r($data1);
					$examId=$data1['id'];
					$query="SELECT sd.userId, concat(sd.firstName,' ',sd.lastName) AS name,sa.id AS attemptId, sa.endDate, sa.startDate, sa.checked
							FROM `subjective_exam_attempts` AS sa
							LEFT JOIN `student_details` AS sd ON sd.userId=sa.studentId
							WHERE sa.examId={$examId} AND sa.studentId = {$data->userId} AND sa.completed=1";
					//print_r($query);
					$subjectiveStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$temp[$key]['listAttempts'] = $subjectiveStudents->toArray();

					//commenting this according to current requirements
					if($data1['attempts']-$data1['examgiven'] <= 0) {
						//delete current key
						unset($temp[$key]);
					}
				}
				//var_dump($temp);
				$result->exams = $temp;
				$result->status = 1;
				//print_r($result);
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		/*
			Function: checkCertificationUser
			Parameter: (courseId,userId)
			Author: mackayush@gmail.com
			
		    this function is used to dynamic create certificate in pdf format. 
			also it upload certificate to the user-data/files/certification/
			folder of servr.
			
			return certificate path
		
		*/
		public function checkCertificationUser($data){

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$totalWatched  = 0;
				$toalVideos    = 0;
				$total_attempt = 0;
				$totalCourse=array();
				$query="SELECT count(con.id) totalVideo, sub.id subjects from subjects sub left join chapters chpt on chpt.subjectId = sub.id and chpt.deleted =0 left join content con on con.chapterId=chpt.id and con.published=1 where sub.courseId=$data->courseId and sub.deleted=0 group by sub.id" ;
				$content_aggr = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$content_aggr=$content_aggr->toArray();
				for($i=0;$i<count($content_aggr);$i++)
				{ 	
					$totalCourse[$i] = $content_aggr[$i]['subjects'];
	   				$toalVideos      = $toalVideos+ $content_aggr[$i]['totalVideo'];
				}	
				
				// checking exams/aasignments
				$query="SELECT ex.id exams from exams ex left join subjects sc ON ex.subjectId=sc.id WHERE sc.courseId =$data->courseId AND STATUS =2" ;
				$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$exams=$exams->toArray();
				$total_exams=0;
				//$total_exams=count($exams);
				for($i=0;$i<count($exams);$i++)
				{
					$Eid=$exams[$i]['exams'];
					if($Eid>0)
					{	$total_exams=$total_exams+1;
						$query="SELECT count(ex.id) ids from exam_attempts ex where ex.studentId=$data->userId and ex.examId=$Eid" ;
						$exams_Att = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$exams_Att=$exams_Att->toArray();
						//print_r($exams_Att[0]['ids']);
						if($exams_Att[0]['ids']>0)
						{
							$total_attempt=$total_attempt + 1;
						}
							
					}						
				}
				require_once 'DynamoDbOps.php';
				$ddo = new DynamoDbOps();
				for($i=0; $i<count($totalCourse);$i++)
				{	$data->subjectId=$totalCourse[$i];
					$content_watched=$ddo->getAllWatchedInformation($data);
					$content_watched=$content_watched->items;
					foreach ($content_watched as $key => $value) {
						if ($value['percentRead'] > 70) {
							$totalWatched= $totalWatched +1;
						}
					}				
				}

				if($toalVideos == $totalWatched && $toalVideos>0 &&($total_exams == $total_attempt))
				{
					$sql = new Sql($adapter);
					//checking if certication already there
					$select = $sql->select();
					$select->from('course_certification');
					$select->where(array(
						'courseId' =>  $data->courseId,
						'userId'   =>  $data->userId					
					));
					$select->columns(array('issueDate','certificationUrl','certificationId','active'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$temp = $temp->toArray();				
					if(count($temp) > 0) {
						$result->certificationStatus =1;
						if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
								require_once 'amazonRead.php';
								//$certificationUrl='http://d2o7safqof0chb.cloudfront.net/user-data/images/course-image-31-1430751298.jpg';
								$result->certification = getSignedURL($temp[0]['certificationUrl']);
								//$result->certification = getSignedURL('http://d2o7safqof0chb.cloudfront.net/user-data/images/course-image-31-1430751298.jpg');
							} 
							else{
								$result->certificationStatus =0;
								$result->certification ='';
							}
					}
					else{
						//have to create certification pdf and store in certifications table
						//print_r('$certificationUrl');
						$certificationUrl="";
						$currentTimeStamp=time();
						$certifcationUniqueId='AC-0'.$currentTimeStamp.'S'.$data->courseId.'U'.$data->userId;
						$certification->userId=$data->userId;
						$certification->cId=$certifcationUniqueId;
						$certification->courseId=$data->courseId;
						$certificationUrl= $this->getCertificationUrl($certification);
						//print_r($certificationUrl);
						$certifiaction = new TableGateway('course_certification', $adapter, null, new HydratingResultSet());
						$insert = array(
							'courseId' 				=>  $data->courseId,
							'userId'   				=>  $data->userId,
							'certificationUrl'		=>  $certificationUrl,
							'certifcationUniqueId' =>  $certifcationUniqueId,
							'active'=>1
						);
						$certifiaction->insert($insert);
						$certifiactionId = $certifiaction->getLastInsertValue();
						if($certifiactionId>0 && $certificationUrl !='' )
						{
						  $result->certificationStatus =1;
						  if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
								require_once 'amazonRead.php';
								$result->certification = getSignedURL($certificationUrl);
							}
							else{
								$result->certificationStatus =0;
								$result->certification='';
							}
						}
					}
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
				}
				else{
					$result->certificationStatus =0;
					$result->certification ='';
				}
				return $result;
				
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}

		}			
		//getcwd() function is used to get the working DirectoryIterator
		//based on that path to particular folder 
		
		/*
			Function: getCertificationUrl
			Parameter: (courseId,userId,coursename)
			Author: mackayush@gmail.com
			
		    this function is used to dynamic create certificate in pdf format. 
			also it upload certificate to the user-data/files/certification/
			folder of servr.
			
			return certificate path
		
		*/
		public function getCertificationUrl($certificationData){
			try {
				//fetching course author name
				$adapter = $this->adapter;
				$result = new stdClass();
				$query = "SELECT c.name course,l.email institutemail,l.id instituteId,
			 	case 
				when ur.roleId= 1 then (select name from institute_details where userId= l.id)
					when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
					when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
					end as institute
				from courses c
				JOIN login_details l on c.ownerId=l.id
				Join user_roles  ur on ur.userId=l.id where c.id=$certificationData->courseId and ur.roleId not in (4,5)";
				$institute = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$cer_courseData = $institute->toArray();
				// fetching user name
				$query = "select CONCAT(firstname, ' ', lastname) as name from student_details where userId= $certificationData->userId";
				$student = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$cer_userDate = $student->toArray();		
				$author=$cer_courseData[0]['institute'];
				$course=$cer_courseData[0]['course'];
				$cId=$certificationData->cId;
				$date=Date("j F Y");
				$name=$cer_userDate[0]['name'];
				$name=ucwords(strtolower($name));
				// Dynamic pdf for certification 
				//require_once('C:/wamp/www/arcanemind/assets/fpdf/fpdf.php');
				require_once(getcwd().'/assets/fpdf/fpdf.php');	
				//print_r('/var/www/html/1testing1/develop/int/assets/fpdf/fpdf.php');	
				$fpdf = new FPDF();
				// Set up the default page margins for your PDF
				// The parameters are margin left, margin top, margin right (units used are mm)
				$fpdf->SetMargins(10, 0, 10);
				// SetAutoPageBreak function will create a new page if our content exceeds the page limit. 0 stands for margin from bottom of the page before breaking.
				$fpdf->SetAutoPageBreak(true, 0);
				// AliasNbPages is optional if you want the ability to display page numbers on your PDF pages.
				//$fpdf->AliasNbPages();
				
				// We need to define the path to where our font files are located. To add additional fonts from the default ones supplied see http://www.fpdf.org/makefont/)
				define('FPDF_FONTPATH', 'font/');
				// Setting up our fonts and styles - The first parameter is the string you will use in your code to access this font, the second is the style of the font you are setting (B = bold, I = italic, BI = Bold & Italic) and finally is the php file containing your font.
				$fpdf->AddFont('Verdana', '','verdana.php'); // Add standard Arial
				$fpdf->AddFont('Verdana', 'B', 'verdanab.php'); // Add bolded version of Arial
				$fpdf->AddFont('Verdana', 'I', 'verdanai.php'); // Add italicised version of Arial
				
				$fpdf->addPage('L');
				
				//$fpdf->Image('C:/wamp/www/arcanemind/assets/fpdf/product4.jpg', 0, 0, 297, 210);
				//$fpdf->Image('C:/wamp/www/arcanemind/assets/fpdf/background.jpg', 30, 20, 249,175);
				$fpdf->Image((getcwd().'/assets/fpdf/product4.jpg'), 0, 0, 297, 210);
				$fpdf->Image((getcwd().'/assets/fpdf/background.jpg'), 30, 20, 249,175);
				//print_r('bk');
				$fpdf->SetXY(60, 50); 
				$fpdf->SetFont('Verdana', ''); 
				$fpdf->SetFontSize(22);
				$fpdf->SetTextColor(0, 0, 0);
				$fpdf->Cell(180, 26, 'This document certifies that', 0, 0, 'C');
						
				$fpdf->SetXY(70, 60);
				$fpdf->SetFont('Verdana','B',30);
				$fpdf->Cell(160, 36,$name , 0, 0, 'C');
				
				$fpdf->SetXY(50, 80); 
				$fpdf->SetFont('Verdana', ''); 
				$fpdf->SetFontSize(22);
				$fpdf->Cell(205, 26, 'successfully completed the course', 0, 0, 'C');
				
				$firsthalf=$course;
				$secondhalf='';
				if(strlen($course)>35)
				{
					$courseArray=str_word_count($course,2);
					$len = count($courseArray);
					$firsthalf = array_slice($courseArray, 0, $len / 2);
					$secondhalf = array_slice($courseArray, $len / 2);
					$firsthalf=implode(" ",$firsthalf);
					$secondhalf=implode(" ",$secondhalf);
				}
				$y=90;
				
				$fpdf->SetXY(73, $y);
				$fpdf->SetFont('Verdana','B',30);
				$fpdf->Cell(160, 36,$firsthalf , 0, 0, 'C');
				if(strlen($secondhalf)>0)
				{
				$y=$y+13;
				$fpdf->SetXY(73, $y);
				$fpdf->SetFont('Verdana','B',30);
				$fpdf->Cell(160, 36,$secondhalf , 0, 0, 'C');
				}
				
				$y=$y+10;
				$fpdf->SetXY(100, $y); 
				$fpdf->SetFont('Verdana', ''); 
				$fpdf->SetFontSize(26);
				$fpdf->Cell(95, 36, 'By ', 0, 0, 'C');
				
				$y=$y+7;
				$fpdf->SetXY(107, $y); 
				$fpdf->SetFont('Verdana', ''); 
				$fpdf->SetFontSize(26);
				$fpdf->Cell(75, 45, $author, 0, 0, 'C');

				
				//$date
				
				$fpdf->SetXY(35, 125); 
				$fpdf->SetFont('Verdana', ''); 
				$fpdf->SetFontSize(12);
				$fpdf->Cell(45, 50, 'Date :  '.$date, 0, 0, 'L');
				
				$fpdf->Image((getcwd().'/assets/fpdf/signVishnu.png'),  200, 150, 59,15);
				//print_r('sign');
				//$fpdf->Image('C:/wamp/www/arcanemind/assets/fpdf/signVishnu.png', 200, 150, 59,15);
				$fpdf->SetXY(210,165);
				$fpdf->SetFont('Verdana','',13);
				//$fpdf->Cell(30, 4, 'Vishnu Korde', 0, 1);
				$fpdf->Cell(30, 4, 'Vishnu Korde', 0, 0, 'C');
				$fpdf->SetXY(210,170);
				$fpdf->SetFont('Verdana','',9);
				$fpdf->Cell(30, 4, 'CEO and Chairman ', 0, 0, 'C');
				
				// passed the IBM exam and earned the title:
				// The above parameters are: The image file path, The X position, The Y position, width, height. Note that the X and Y position are for where the top left hand corner sits on the page.
				$fpdf->SetFont('Verdana','',9);
				$fpdf->SetXY(18,190);
				$fpdf->Cell(30, 4, 'Certificate ID: '.$cId, 0, 1);
				
				$cfootor= "www.integro.io";
				$fpdf->SetFont('Verdana','',9);
				$fpdf->SetXY(130,190);
				$fpdf->Cell(30, 4, $cfootor, 0, 1);
				
				$cfootor= "2016 Integro. All Rights Reserved.";
				$fpdf->SetFont('Verdana','',9);
				$fpdf->SetXY(220,190);
				$fpdf->Cell(30, 4, $cfootor, 0, 1);

				//$filename='C:/wamp/www/arcanemind/media/';
				//$filename='var/www/html/1testing1/develop/int/';
			
				$ext		 = 'pdf';
				$filemidName = $certificationData->userId.'_'.$certificationData->courseId;
				$fileNameAws = "Certification-{$filemidName}.{$ext}";
				$filenamePrefix=getcwd().'/user-data/certificates/';
				$filepath	 = $filenamePrefix.$fileNameAws;
				$fpdf->Output($filepath, 'F');		
				if(file_exists($filepath)){					
					$filesDirAws = "user-data/certification/";	
					$filePathAws = $filesDirAws. $fileNameAws;
					//require_once 'C:/wamp/www/arcanemind/api/amazonUpload.php';
					///var/www/html/1testing1/develop/int/api/amazonUpload.php
					require_once (getcwd().'/api/amazonUpload.php');
					uploadFile($filePathAws,$filepath,$ext);
					unlink($filepath);
				}
				//print_r($filePathAws);
				$config =  require "cloudFront.php";
				$completePath= $config.$filePathAws;
				return $completePath;

			} catch (Exception $e) {
					$result->status = 0;
					$result->exception = $e->getMessage();
					return $result;
			}
		}
		
		public function getCourseDetailsbyExamId($examId) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$query	= "SELECT  e.name exam ,s.name subject,s.id subjectId,c.name course, c.id courseId, l.email institutemail,l.id instituteId,
							CASE 
							WHEN ur.roleId= 1 then (select name from institute_details where userId= l.id)
								WHEN ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
								WHEN ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
								END as institute
							from exams e
							JOIN  subjects s on s.id=e.subjectId
							JOIN courses c on c.id=s.courseId
							JOIN login_details l on c.ownerId=l.id and e.ownerId=l.id
							JOIN user_roles  ur on ur.userId=l.id where e.id=$examId and ur.roleId not in (4,5)";
				$subject = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->subject = $subject->toArray();

				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
	
		//function to create instructions for examination
		public function createInstructions($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exams');
				$select->where(array('id'	=>	$data->examId));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exam = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exam = $exam->toArray();
				$exam = $exam[0];
				$result->dateCrossed = 0;
				
				//return if exam end date is in past
				if($exam['endDate'] != '') {
					$end = $exam['endDate']/1000;
					if($end < time()) {
						$result->dateCrossed = 1;
						/*$result->status = 0;
						$result->message = 'This exam has expired. Please extend date in settings page.';
						return $result;*/
					}
				}
				
				$select = $sql->select();
				$select->from('exam_sections');
				$select->where(array(
						'examId'	=>	$exam['id'],
						'delete'	=>	0
				));
				$select->order('weight ASC, id ASC');
				$select->columns(array('id', 'name', 'time', 'totalMarks'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$sections = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$sections = $sections->toArray();
				$sectionCount = count($sections);
				$totalMarks = 0;
				$totalTime = 0;
				$totalQuestions = 0;
				foreach($sections as $key=>$section) {
					$sectionQuestions = 0;
					$totalMarks += $section['totalMarks'];
					$totalTime += $section['time'];
					$select = $sql->select();
					$select->from('section_categories');
					$select->where(array(
							'sectionId'	=>	$section['id'],
							'delete'	=>	0
					));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$categories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$categories = $categories->toArray();
					$sections[$key]['categories'] = $categories;
					foreach($categories as $category) {
						$sectionQuestions += $category['required'];
					}
					$sections[$key]['questions'] = $sectionQuestions;
					$totalQuestions += $sectionQuestions;
				}
				if($exam['tt_status'] == 0)
					$totalTime = $exam['totalTime'];
				
				$startDate = date('d M Y H:i:s', ($exam['startDate']/1000));
				$endDate = date('d M Y H:i:s', ($exam['endDate']/1000));
				
				$instructions = 'Instructions:<br><br><hr>';
				if($exam['attempts'] < 2 && $exam['endDate'] == '')
					$instructions .= "You can take this {$exam['type']} only once.";
				else if($exam['endDate'] == '')
					$instructions .= "You can attempt this {$exam['type']} <u>{$exam['attempts']} times</u> and your highest score will be considered towards your rank.";
				else if($exam['attempts'] < 2 && $exam['endDate'] != '')
					$instructions .= "You can take this {$exam['type']} only once between {$startDate} and {$endDate} ";
				else if($exam['endDate'] != '')
					$instructions .= "You can attempt this {$exam['type']} <u>{$exam['attempts']} times</u> between {$startDate} and {$endDate}, and your highest score will be considered towards your rank.";
				$instructions .= '<br><hr><br><br>';
				
				if($sectionCount < 2)
					$instructions .= "This {$exam['type']} contains only one section – {$sections[0]['name']}.<br>";
				else {
					$instructions .= "This {$exam['type']} has {$sectionCount} sections:<br>";
					foreach($sections as $key=>$section) {
						$instructions .= chr(97 + $key).") {$section['name']}<br>";
					}
				}
				if($exam['type'] == 'Exam')
					$instructions .= "<b>Total time of your {$exam['type']} is {$totalTime} min</b><br><br>";
				else
					$instructions .= "<b>There is no time limit to this {$exam['type']}</b><br><br>";
				if($exam['tt_status'] == 0) {
					$instructions .= "You are allowed to switch between Sections.<br>You are suggested to use your time judiciously";
				}
				else {
					$instructions .= "You are not allowed to switch between Sections<br>";
					$count = 0;
					foreach($sections as $section) {
						$instructions .= chr(97 + $count).") {$section['time']} minutes for {$section['name']}<br>";
						$count++;
						if($exam['gapTime'] > 0) {
							$instructions .= chr(97 + $count).") ".$exam['gapTime']." mins break<br>";
							$count++;
						}
					}
				}
				$instructions .= "<br><hr><br>";
				if($exam['endBeforeTime'] == 1) {
					$instructions .= "<b>You will be able to leave the exam before the time completes.</b>";
				}
				else {
					$instructions .= "<b>You will have to wait until the exam ends.</b>";
				}
				$instructions .= "<br><hr><br>";
				
				if($exam['powerOption'] == 0) {
					$instructions .= "<b>If power goes off or the Browser Closes, Answers are not lost<br>You will not lose any time. Test will resume from the point when the test was interrupted.</b>";
				}
				else {
					$instructions .= "<b> If power goes off or the Browser Closes, Answers will be lost.<br>Time would continue to tick. You will lose the time during which interruption happens.</b>";
				}
				$instructions .= "<br><hr><br>";
				
				$instructions .= "<b>Total : {$totalMarks} Marks<br>";
				foreach($sections as $section) {
					$instructions .= "{$section['name']} : {$section['questions']} Questions {$section['totalMarks']} Marks<br>";
				}
				$instructions .= "</b><br><hr><br>";
				
				foreach($sections as $section) {
					$instructions .= "<b>{$section['name']} : {$section['questions']} Questions {$section['totalMarks']} Marks</b><br><table class='table table-striped'>";
					foreach($section['categories'] as $category) {
						$instructions .= '<tr><td style="width: 40%;">';
						if($category['questionType'] == 0)
							$instructions .= "Multiple Choice Questions";
						else if($category['questionType'] == 1)
							$instructions .= "True/False";
						else if($category['questionType'] == 2)
							$instructions .= "Fill in the blanks.";
						else if($category['questionType'] == 3)
							$instructions .= "Match the following";
						else if($category['questionType'] == 4)
							$instructions .= "Comprehension Type Questions";
						else if($category['questionType'] == 5)
							$instructions .= "Dependent Type Questions";
						else if($category['questionType'] == 6)
							$instructions .= "Multiple Answer Questions";
						$marks = $category['correct'] * $category['required'];
						$instructions .= "</td><td>+{$category['correct']}</td><td>-{$category['wrong']}</td><td>{$category['required']} Questions</td><td>{$marks} Marks</td></tr>";
					}
					$instructions .= '</table>';
				}
				
				$result->instruction = $instructions;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function copyExam($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$query = "CALL copyExam($data->iexamId,'$data->iname'); ";
				$copy = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				//$result->copy = $copy->toArray();
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function copySubjectiveExam($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$query = "CALL copySubjectiveExam($data->iexamId,'$data->iname'); ";
				$copy = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				//$result->copy = $copy->toArray();
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function ImportExam($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$query = "CALL importExam({$data->iexamId}, {$data->iownerId}, {$data->isubjectId}, {$data->ichapterId}); ";
				$result->copy = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				//$result->copy = $copy->toArray();
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function ImportSubjectiveExam($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$query = "CALL ImportSubjectiveExam({$data->iexamId}, {$data->iownerId}, {$data->isubjectId}, {$data->ichapterId}); ";
				$result->copy = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				//$result->copy = $copy->toArray();
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function ImportSubmission($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$query = "CALL ImportSubmission({$data->iexamId}, {$data->iownerId}, {$data->isubjectId}, {$data->ichapterId}); ";
				$result->copy = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				//$result->copy = $copy->toArray();
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function checkAssigenedExam($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$query = "SELECT subjectId from exams where id=$data->examId";
				$subjectId = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->subjectId = $subjectId->toArray();
				 $query = "SELECT courseId from subjects where id={$result->subjectId[0]['subjectId']}";
				 $courseId = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				 $result->courseId = $courseId->toArray();
				   require_once "Subject.php";
					 $s = new Subject();
					 $data = new stdClass();
					 $data->courseId = $result->courseId[0]['courseId'];
					 $data->subjectId = $result->subjectId[0]['subjectId'];
				if(($s->checkSubjectAssigned($data))) {
					return true;
				}
				return false;
			 } catch (Exception $e) {
				  return false;
			}
		}
		
		public function saveMarksForSubjectiveExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				
				//validating max marks 
				$adapter = $this->adapter;

				$sql = new Sql($adapter);
				$query = "SELECT qs.*,qsel.marks,qsel.examId
						FROM `subjective_attempt_questions` AS saq
						INNER JOIN `questions_subjective` AS qs ON qs.id=saq.questionId
						INNER JOIN `subjective_exam_attempts` AS sea ON sea.id=saq.attemptId
						INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id AND qsel.examId=sea.examId
						WHERE saq.attemptId={$data->attemptId} AND qs.id={$data->questionId}";
				$maks = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$maks = $maks->toArray();

				$maxMarks = $maks[0]['marks'];
				$examId	  = $maks[0]['examId'];
				if ($maks[0]['questionsonPage'] == 1) {
					$sql = new Sql($adapter);
					$query = "SELECT qs.*,qsel.marks
							FROM `subjective_attempt_questions` AS saq
							INNER JOIN `questions_subjective` AS qs ON qs.id=saq.questionId
							INNER JOIN `subjective_exam_attempts` AS sea ON sea.id=saq.attemptId
							INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id AND qsel.examId=sea.examId
							WHERE saq.attemptId={$data->attemptId} AND qs.parentId={$maks[0]['id']}";
					$subQuestions = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$subQuestions = $subQuestions->toArray();
				
					$totalMaxMarks = 0;
					foreach ($subQuestions as $key => $value) {
						$totalMaxMarks+= $value['marks'];
					}
					if( $data->marks > $totalMaxMarks) {
						$result->status = 0;
						$result->message = "Marks greater then $totalMaxMarks for this question";
						return $result;
					}
				} else if( $data->marks > $maxMarks) {
					$result->status = 0;
					$result->message = "Marks greater then maximum marks for this question";
					return $result;
				}
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subjective_attempt_questions');
				$select->where(array(
							'attemptId' => $data->attemptId,
							'questionId' =>	$data->questionId
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$exam_subjective = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exam_subjective = $exam_subjective->toArray();
				if(count($exam_subjective))
				{
					$id=$exam_subjective[0]['id'];
					
					//update the marks in the table for question 
					$update = $sql->update();
					$update->table('subjective_attempt_questions');
					$update->set(array(
								'marks'			=>	$data->marks,
								'check'			=> 1
					));
					$update->where(array('id'	=>	$id));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
					
					
					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					$selectString = "UPDATE subjective_exam_attempts SET score=(select sum(marks) from subjective_attempt_questions where attemptId={$data->attemptId} ) WHERE id={$data->attemptId} ";
					$marks = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

				} else {
					$quesMarks = new TableGateway('subjective_attempt_questions', $adapter, null,new HydratingResultSet());
					$quesMarks->insert(array(
						'attemptId' 	=>	$data->attemptId,
						'questionId' 	=>	$data->questionId,
						'marks' 		=>	$data->marks,
						'time'			=>	0,
						'check'			=>	1
					));
					$attemptId = $quesMarks->getLastInsertValue();
					
					
					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					$selectString = "UPDATE subjective_exam_attempts SET score=(select sum(marks) from subjective_attempt_questions where attemptId={$data->attemptId} ) WHERE id={$data->attemptId} ";
					$marks = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				}
				if (!empty($exam_subjective[0]['parentId'])) {
					$parentId=$exam_subjective[0]['parentId'];
					$query = "SELECT saq.id, IFNULL(saq.check,0) AS `check`
								FROM `subjective_attempt_questions` AS saq
								WHERE saq.attemptId= {$data->attemptId} AND saq.questionId={$parentId}";
					//var_dump($query);
					$q_attempt = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$q_attempt = $q_attempt->toArray();
					if ($q_attempt[0]["check"] < 1) {
						$questionAttemptId = $q_attempt[0]["id"];
						$query = "SELECT COUNT(saq.id) AS totalQuestions
									FROM `subjective_attempt_questions` AS saq
									WHERE saq.attemptId= {$data->attemptId} AND saq.parentId={$data->questionId}";
						//print_r($query);
						$q_subjective = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$q_subjective = $q_subjective->toArray();
						$totalQuestions = $q_subjective[0]["totalQuestions"];
						//print_r($totalQuestions);
						$query = "SELECT COUNT(saq.id) AS totalCheckedQuestions
									FROM `subjective_attempt_questions` AS saq
									WHERE saq.attemptId= {$data->attemptId} AND saq.parentId={$data->questionId} AND saq.check=1";
						$q_subjective = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$q_subjective = $q_subjective->toArray();
						$totalCheckedQuestions = $q_subjective[0]["totalCheckedQuestions"];

						if ($totalQuestions == $totalCheckedQuestions) {
							//update the marks in the table for question 
							$query = "UPDATE `subjective_attempt_questions` SET `check`=1 WHERE `id`=$questionAttemptId";
							
							$q_subjective = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						}
					}
				} else {
					if ($exam_subjective[0]['questionType']==1) {
						$sql = new Sql($adapter);
						//$parentId=$exam_subjective[0]['parentId'];
						$selectString = "UPDATE `subjective_attempt_questions` SET `check`=1 WHERE `attemptId`={$data->attemptId} AND `parentId` = {$data->questionId}";
						$subjective_question = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					}
				}

				//updating the  subjective_exam_attempts table if all questions gets checked
				$query = "SELECT IFNULL(count(id),0) AS `checked`
								FROM `subjective_attempt_questions` where `attemptId`={$data->attemptId} and `check`=1";

				$check_question = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$check_question = $check_question->toArray();
				$check_question = $check_question[0]["checked"];

				$query = "SELECT IFNULL(count(id),0) AS `total`
								FROM `subjective_attempt_questions` where `attemptId`={$data->attemptId}  ";

				$total_question = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$total_question = $total_question->toArray();
				$total_question = $total_question[0]["total"];

				if($check_question == $total_question)
				{
					$query = "UPDATE `subjective_exam_attempts` SET `checked` = 1 WHERE `id` = {$data->attemptId}";
					$subjective_question = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);

					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('subjective_exam_attempts');
					$select->where(array(
								'id' => $data->attemptId
					));
					$statement = $sql->getSqlStringForSqlObject($select);
					$exam_subjective = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exam_subjective = $exam_subjective->toArray();

					require_once('StudentExam.php');
					$se = new StudentExam();
					$totalScore = $exam_subjective[0]['score'];
					$maximumMarks = $se->getMaxMarksSubjectiveExam($examId);
					$percentage = $totalScore/$maximumMarks * 100;
					if($percentage < 0)
						$percentage = 0;

					require 'Subject.php';
					$s = new Subject();
					$data->contentId = $examId;
					$data->contentType = 'subjective-exam';
					$data->percentage = $percentage;
					$data->studentId = $exam_subjective[0]['studentId'];
					$resSubjectRewards = $s->putSubjectRewardsbyContent($data);
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();

				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function saveReviewForSubjectiveExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				if (!isset($data->questionId)) {

					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('answer_subjective');
					$update->set(array(
										'review'			=>	$data->review
					));
					$update->where(array(
								'id' =>	$data->answerId
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();

				} else {

					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->columns(array('id'));
					$select->from('subjective_attempt_questions');
					$select->where(array(
						'attemptId'	=>	$data->attemptId,
						'questionId'=>	$data->questionId
					));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subAttemptQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$subAttemptQuestions = $subAttemptQuestions->toArray();					
					$subAttemptQId = $subAttemptQuestions[0]['id'];

					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('answer_subjective');
					$update->set(array(
										'review'			=>	$data->review
					));
					$update->where(array(
								'subjectiveExam_attemptid' =>	$data->attemptId,
								'attemptQuestionId' =>	$subAttemptQId,
								'answerType' =>	'text',
								'status'	 => 1
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();

				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function getCourseDetailsbySubjectiveExamId($examId) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$query	= "SELECT  e.name exam ,s.name subject,s.id subjectId,c.name course, c.id courseId, l.email institutemail,l.id instituteId,
							CASE 
							WHEN ur.roleId= 1 then (select name from institute_details where userId= l.id)
								WHEN ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
								WHEN ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
								END as institute
							FROM exam_subjective e
							INNER JOIN subjects s on s.id=e.subjectId
							INNER JOIN courses c on c.id=s.courseId
							INNER JOIN login_details l on c.ownerId=l.id and e.ownerId=l.id
							INNER JOIN user_roles  ur on ur.userId=l.id where e.id=$examId and ur.roleId not in (4,5)";
				$subject = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->subject = $subject->toArray();

				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function deleteSubjectiveExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$subject = $this->getCourseDetailsbySubjectiveExamId($data->examId);
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('exam_subjective');
				$update->set(array('Active'	=>	0));
				$update->where(array('id'	=>	$data->examId,'Active' =>1));
				$statement = $sql->prepareStatementForSqlObject($update);
				//$statement->execute();
				$count = $statement->execute()->getAffectedRows();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				if($count >0)
				{
					$course = $subject->subject;
					$examId = $data->examId;
					$exam 	=  $course[0]['exam'];
					$courseId = $course[0]['courseId'];
					$coursename = $course[0]['course'];
					$subjectname = $course[0]['subject'];
					$subjectId = $course[0]['subjectId'];
					$institute = $course[0]['institute'];
					$institutemail = $course[0]['institutemail'];
					$instituteId = $course[0]['instituteId'];
						  
					// insert notification
					$notification="Subjective Exam $exam ( $data->examId ) belonging to  Subject $subjectname (subject id : {$subjectId} ) which is a part of Course $coursename ( course id : $courseId ) has been deleted. Please contact admin@integro.io for restoring the exam.";
					$query = "INSERT INTO notifications (userId,message,status) VALUES ($instituteId,'$notification',0)";
					$notification=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);

					if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
						require_once "User.php";
						$c = new User();

						$instituteSubject = "Subjective Exam deletion Notification";
						$emailInstitute = "Dear $institute, <br> <br>";
						$emailInstitute .= "There has been a request for deletion of the Subjective Exam belonging to  Course  $coursename ( Course ID: $courseId ) on <a href=integro.io >Integro.io <a>  <br> <br>";

						$emailInstitute .= "Please write to us at <a href=admin@integro.io >admin@integro.io <a> if you wish to restore the Subjective Exam  or if you did not initiate the request.<br> <br> "
								. "Please mention your Institute ID, Subjective Exam Name & Subjective Exam ID in the email <br> <br>";

						$emailInstitute .= "<table border=1 ><thead><tr><th> Institute Name</th> <th>Institute ID</th> <th>Deleted Course Name</th> <th> Deleted Course Id</th><th>Deleted Subjective Exam Name</th> <th>Deleted Subjective Exam Id</th></tr></thead>";
						$emailInstitute .= "<tbody><tr><td> $institute</td><td>$instituteId</td> <td>$coursename</td><td> $courseId</td><td> $exam</td><td> $examId</td> </tr></tbody></table> ";

						$c->sendMail($institutemail, $instituteSubject, $emailInstitute);
					}
				}
								
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function generateManualExamCSV($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				if (isset($data->type)) {
					if ($data->type == 2) {
						$noOfQuestions = $data->questions;
						$questions = array();
						for ($i=1; $i <= $noOfQuestions; $i++) { 
							$questions[] = "Q{$i} Marks";
						}
						$array[] = array_merge(array("Student ID", "Student Name"),$questions);
						//$array[] = array("Student Name", implode(",", $questions));
					} else {
						$array[] = array("Student ID", "Student Name", "Total Marks");
					}
					$query="SELECT ss.userId,sd.firstName,sd.lastName
							FROM `student_subject` AS ss
							INNER JOIN `student_details` AS sd ON sd.userId=ss.userId
							INNER JOIN `login_details` AS ld ON ld.id=sd.userId
							WHERE ss.courseId={$data->courseId} AND ss.subjectId={$data->subjectId} AND ss.Active=1 AND ld.temp=0 and ld.validated=1 and ld.active=1
							GROUP BY ss.userId";
					$students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$students = $students->toArray();
					foreach ($students as $key => $value) {
						$array[] = array($value['userId'],$value['firstName'].' '.$value['lastName']);
					}
					// open raw memory as file so no temp files needed, you might run out of memory though
					$f = fopen('php://memory', 'w');
					// loop over the input array
					foreach ($array as $line) {
						// generate csv lines from the inner arrays
						fputcsv($f, $line);
					}
					// reset the file pointer to the start of the file
					fseek($f, 0);
					// tell the browser it's going to be a csv file
					header('Content-Type: application/csv');
					// tell the browser we want to save it instead of displaying it
					header('Content-Disposition: attachment; filename=export.csv;');
					// make php send the generated csv lines to the browser
					fpassthru($f);
				} else {
					echo "There was some error!";
				}
			} catch (Exception $e) {
				echo $e->getMessage();
				return $result;
			}
		}

		public function generateManualExamForm($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				if (isset($data->type)) {
					$query="SELECT ss.userId,sd.firstName,sd.lastName
							FROM `student_subject` AS ss
							INNER JOIN `login_details` AS ld ON ld.id=ss.userId
							INNER JOIN `student_details` AS sd ON sd.userId=ss.userId
							WHERE ss.courseId={$data->courseId} AND ss.subjectId={$data->subjectId} AND ss.Active=1 AND ld.temp=0 and ld.validated=1 and ld.active=1
							GROUP BY ss.userId";
					$students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$students = $students->toArray();
					if (empty($students)) {
						$result->status = 0;
						$result->message = "Add students to this subject first!";
						return $result;
					} else {
						foreach ($students as $key => $value) {
							$array[] = array($value['userId'],$value['firstName'].' '.$value['lastName']);
						}
					}
				} else {
					$result->status = 0;
					$result->message = "There was some error!";
					return $result;
				}
				$result->data = $array;
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
	
		public function saveManualExamForm($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				if (isset($data->type)) {
					//var_dump($data);
					$exams = new TableGateway('exam_manual', $adapter, null,new HydratingResultSet());
					$insert = array(
						'name'				=>	$data->name,
						'type'				=>	$data->type,
						'ownerId'			=>	$data->userId,
						'courseId'			=>	$data->courseId,
						'subjectId' 		=> 	$data->subjectId,
						'chapterId' 		=> 	$data->chapterId,
						'status'			=>	1,
						'created'			=>	date('Y-m-d H:i:s'),
						'modified'			=>	date('Y-m-d H:i:s')
						/*'startDate'			=>	$data->startDate,
						'endDate'			=>	$data->endDate*/
					);
					$exams->insert($insert);
					$data->examId = $exams->getLastInsertValue();
					$insert = array();
					$questionMarks = array();
					if ($data->type == "deep") {
						foreach ($data->total as $key => $value) {
							$examQuestions = new TableGateway('exam_manual_questions', $adapter, null,new HydratingResultSet());
							$insert = array(
								'exam_id'		=>	$data->examId,
								'marks'			=>	$data->total[$key]
							);
							$examQuestions->insert($insert);
							$questionMarks[] = $exams->getLastInsertValue();
						}
					} else {
						$examQuestions = new TableGateway('exam_manual_questions', $adapter, null,new HydratingResultSet());
						$insert = array(
							'exam_id'		=>	$data->examId,
							'marks'			=>	$data->total
						);
						$examQuestions->insert($insert);
						$questionMarks = $exams->getLastInsertValue();
					}
					$insert = array();
					foreach ($data->students as $key => $value) {
						if ($data->type == "deep") {
							for ($i=0; $i < $data->questions; $i++) {
								$examMarks = new TableGateway('exam_manual_marks', $adapter, null,new HydratingResultSet());
								$insert = array(
									'exam_id'		=>	$data->examId,
									'student_id'	=>	$value,
									'question_id'	=>	$questionMarks[$i],
									'marks'			=>	$data->marks[$key][$i]
								);
								$examMarks->insert($insert);
							}
						} else {
							$examMarks = new TableGateway('exam_manual_marks', $adapter, null,new HydratingResultSet());
							$insert = array(
								'exam_id'		=>	$data->examId,
								'student_id'	=>	$value,
								'question_id'	=>	$questionMarks,
								'marks'			=>	$data->marks[$key][0]
							);
							//var_dump($insert);
							$examMarks->insert($insert);
						}
					}
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
				} else {
					$db->rollBack();
					$result->status = 0;
					$result->message = "There was some error!";
					return $result;
				}
				$result->examId = $data->examId;
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getManualExamDetails($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				if (isset($data->examId)) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('exam_manual');
					$select->where(array(
								/*'courseId' => $data->courseId,
								'subjectId' => $data->subjectId,*/
								'id' => $data->examId,
								'status'	=>	1
					));
					$select->columns(array('id', 'name', 'type', 'status', 'subjectId', 'chapterId'));
					$statement = $sql->getSqlStringForSqlObject($select);
					$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exams = $exams->toArray();
					if (count($exams) == 0) {
						$result->status = 0;
						$result->message = "Manual Exam not found!";
						return $result;
					} else {
						$exam = $exams[0];
						//$exam['subjectId'] = $exams['subjectId'];
						$select = $sql->select();
						$select->from('exam_manual_questions');
						$select->where(array(
									'exam_id' => $data->examId
						));
						$select->columns(array('id', 'marks'));
						$statement = $sql->getSqlStringForSqlObject($select);
						$questions = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$questions = $questions->toArray();
						$result->questionsCount = count($questions);
						if ($exam['type'] == "deep") {
							$exam['questions'] = $questions;
							$maxMarks = 0;
							foreach ($questions as $key => $value) {
								$maxMarks+= $value['marks'];
							}
							$exam['maxMarks'] = $maxMarks;
						} else {
							$exam['questions'] = $questions[0];
						}
						$arrStudents = array();
						/*$query="SELECT ss.userId,sd.firstName,sd.lastName
							FROM `student_subject` AS ss
							INNER JOIN `student_details` AS sd ON sd.userId=ss.userId
							INNER JOIN `exam_manual_marks` AS emm ON emm.student_id=ss.userId AND emm.exam_id={$data->examId}
							WHERE ss.courseId={$data->courseId} AND ss.subjectId={$data->subjectId} AND ss.Active=1
							GROUP BY ss.userId";*/
						$query="SELECT ss.userId,sd.firstName,sd.lastName
							FROM `student_subject` AS ss
							INNER JOIN `student_details` AS sd ON sd.userId=ss.userId
							INNER JOIN `exam_manual_marks` AS emm ON emm.student_id=ss.userId
							WHERE emm.exam_id={$data->examId} AND ss.Active=1
							GROUP BY ss.userId";
						$students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$students = $students->toArray();
						foreach ($students as $key => $value) {
							$select = $sql->select();
							$select->from('exam_manual_marks');
							$select->where(array(
										'exam_id' => $data->examId,
										'student_id' => $value['userId']
							));
							$select->columns(array('id', 'question_id', 'marks'));
							$statement = $sql->getSqlStringForSqlObject($select);
							$marks = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
							$marks = $marks->toArray();
							if ($exam['type'] == "deep") {
								$query="SELECT SUM(marks) AS totalMarks
										FROM `exam_manual_marks` AS emm
										WHERE emm.exam_id={$data->examId} AND emm.student_id={$value['userId']}";
								$totalMarks = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$totalMarks = $totalMarks->toArray();
								$totalMarks = $totalMarks[0]['totalMarks'];
								$arrStudents[] = array($value['userId'],$value['firstName'].' '.$value['lastName'],$totalMarks,$marks);
							} else {
								$arrStudents[] = array($value['userId'],$value['firstName'].' '.$value['lastName'],$marks[0]);
							}
						}
						$exam['students'] = $arrStudents;
						$result->exam = $exam;
					}
				} else {
					$result->status = 0;
					$result->message = "There was some error!";
					return $result;
				}
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getManualExamDetail($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$selectString ="SELECT em.id,em.name AS examName,em.type,em.status,
										em.subjectId, s.name AS `subjectName`,
										co.id AS `courseId`, co.name AS `courseName`
								FROM `exam_manual` AS `em`
								INNER JOIN `subjects` AS `s` ON s.id = em.subjectId
								INNER JOIN `courses` AS `co` ON co.id = s.courseId
								WHERE em.id = '{$data->examId}' AND em.status = '1'
								ORDER BY `id` DESC";
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				if(count($exams) == 0) {
					$result->status = 0;
					$result->message = "No Exam found";
					return $result;
				}
				$result->status	= 1;
				$result->exam	= $exams[0];
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function saveManualExamMarks($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				if ($data->marks == '') {
					$result->status = 0;
					$result->message = 'Marks was empty!';
					return $result;
				}
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('exam_manual_marks');
				$update->set(array(
									'marks'			=>	$data->marks
							));
				$update->where(array(
									'exam_id'		=>	$data->examId,
									'id'			=>  $data->marksId
							));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();

				$examId	  = $data->examId;

				$sql = new Sql($adapter);				
				$select = $sql->select();
				$select->from('exam_manual');
				$select->where(array(
							'id'		=>	$data->examId
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$exam_manual = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exam_manual = $exam_manual->toArray();
				$exam_type = $exam_manual[0]['type'];

				if ($exam_type == 'quick') {
					$totalScore = $data->marks;

					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('emm' => 'exam_manual_marks'));
					$select->join(array('emq' => 'exam_manual_questions'),'emq.id = emm.question_id',array('maxMarks' => 'marks'));
					$select->where(array(
								'emm.exam_id'		=>	$data->examId,
								'emm.id'			=>  $data->marksId
					));
					$statement = $sql->getSqlStringForSqlObject($select);
					$exam_manual = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exam_manual = $exam_manual->toArray();

					$maximumMarks = $exam_manual[0]['maxMarks'];
					$percentage = $totalScore/$maximumMarks * 100;
					if($percentage < 0)
						$percentage = 0;

					require 'Subject.php';
					$s = new Subject();
					$data->contentId = $examId;
					$data->contentType = 'manual-exam';
					$data->percentage = $percentage;
					$data->studentId = $exam_manual[0]['student_id'];
					$resSubjectRewards = $s->putSubjectRewardsbyContent($data);
				} else {

					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('emm' => 'exam_manual_marks'));
					$select->where(array(
								'emm.exam_id'		=>	$data->examId,
								'emm.id'			=>  $data->marksId
					));
					$statement = $sql->getSqlStringForSqlObject($select);
					$exam_manual = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exam_manual = $exam_manual->toArray();
					$studentId = $exam_manual[0]['student_id'];

					$query="SELECT SUM(emm.marks) AS score,SUM(emq.marks) AS maxMarks
							FROM `exam_manual_marks` AS emm
							INNER JOIN `exam_manual_questions` AS emq ON emq.id = emm.question_id
							WHERE emm.exam_id='$data->examId' AND emm.student_id = $studentId";
					$exam_manual = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$exam_manual = $exam_manual->toArray();

					$totalScore = $exam_manual[0]['score'];
					$maximumMarks = $exam_manual[0]['maxMarks'];

					$percentage = $totalScore/$maximumMarks * 100;
					if($percentage < 0)
						$percentage = 0;

					require 'Subject.php';
					$s = new Subject();
					$data->contentId = $examId;
					$data->contentType = 'manual-exam';
					$data->percentage = $percentage;
					$data->studentId = $studentId;
					$resSubjectRewards = $s->putSubjectRewardsbyContent($data);
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();

				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function saveManualExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				if (empty($data->examName)) {
					$result->status = 0;
					$result->message = 'Exam name was empty!';
					return $result;
				}
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('exam_manual');
				$update->set(array(
									'name'		=>	$data->examName,
									'chapterId'	=>	$data->chapterId
							));
				$update->where(array(
									'id'		=>	$data->examId
							));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = "Exam saved successfully!";
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getCourseDetailsbyManualExamId($examId) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$query	= "SELECT  e.name exam ,s.name subject,s.id subjectId,c.name course, c.id courseId, l.email institutemail,l.id instituteId,
							CASE 
							WHEN ur.roleId= 1 then (select name from institute_details where userId= l.id)
								WHEN ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
								WHEN ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
								END as institute
							from exam_manual e
							JOIN  subjects s on s.id=e.subjectId
							JOIN courses c on c.id=s.courseId
							JOIN login_details l on c.ownerId=l.id and e.ownerId=l.id
							JOIN user_roles  ur on ur.userId=l.id where e.id=$examId and ur.roleId not in (4,5)";
				$subject = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->subject = $subject->toArray();

				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function deleteManualExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$subject = $this->getCourseDetailsbyManualExamId($data->examId);
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('exam_manual');
				$update->set(array('status'	=>	0));
				$update->where(array('id'	=>	$data->examId,'status' =>1));
				$statement = $sql->prepareStatementForSqlObject($update);
				//$statement->execute();
				$count = $statement->execute()->getAffectedRows();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				if($count >0)
				{	
					$course = $subject->subject;
					$examId= $data->examId;              
					$exam=  $course[0]['exam'];            
					$courseId = $course[0]['courseId'];
					$coursename = $course[0]['course'];
					$subjectname = $course[0]['subject'];
					$subjectId = $course[0]['subjectId'];
					$institute = $course[0]['institute'];
					$institutemail = $course[0]['institutemail'];
					$instituteId = $course[0]['instituteId'];
						  
					// insert notification
					$notification="Manual Exam $exam ( $data->examId ) belonging to  Subject $subjectname (subject id : {$subjectId} ) which is a part of Course $coursename ( course id : $courseId ) has been deleted. Please contact admin@integro.io for restoring the exam.";
					$query = "INSERT INTO notifications (userId,message) VALUES ($instituteId,'$notification')";
					$notification=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);

					require_once "User.php";
					$c = new User();

					$instituteSubject = "Manual Exam deletion Notification";
					$emailInstitute = "Dear $institute, <br> <br>";
					$emailInstitute .= "There has been a request for deletion of the Manual Exam belonging to  Course  $coursename ( Course ID: $courseId ) on <a href=integro.io >Integro.io <a>  <br> <br>";

					$emailInstitute .= "Please write to us at <a href=admin@integro.io >admin@integro.io <a> if you wish to restore the Manual Exam  or if you did not initiate the request.<br> <br> "
							. "Please mention your Institute ID, Manual Exam Name & Manual Exam ID in the email <br> <br>";

					$emailInstitute .= "<table border=1 ><thead><tr><th> Institute Name</th> <th>Institute ID</th> <th>Deleted Course Name</th> <th> Deleted Course Id</th><th>Deleted Manual Exam Name</th> <th>Deleted Manual Exam Id</th></tr></thead>";
					$emailInstitute .= "<tbody><tr><td> $institute</td><td>$instituteId</td> <td>$coursename</td><td> $courseId</td><td> $exam</td><td> $examId</td> </tr></tbody></table> ";

					$c->sendMail($institutemail, $instituteSubject, $emailInstitute);
				}			
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getStudentManualExamDetails($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				if (isset($data->examId)) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('exam_manual');
					$select->where(array(
								'courseId' => $data->courseId,
								'subjectId' => $data->subjectId,
								'id' => $data->examId,
								'status'	=>	1
					));
					$select->columns(array('id', 'name', 'type', 'status'));
					$statement = $sql->getSqlStringForSqlObject($select);
					$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exams = $exams->toArray();
					if (count($exams) == 0) {
						$result->status = 0;
						$result->message = "Manual Exam not found!";
						return $result;
					} else {
						$exam = $exams[0];
						$select = $sql->select();
						$select->from('exam_manual_questions');
						$select->where(array(
									'exam_id' => $data->examId
						));
						$select->columns(array('id', 'marks'));
						$statement = $sql->getSqlStringForSqlObject($select);
						$questions = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$questions = $questions->toArray();
						$result->questionsCount = count($questions);
						if ($exam['type'] == "deep") {
							$exam['questions'] = $questions;
							$maxMarks = 0;
							foreach ($questions as $key => $value) {
								$maxMarks+= $value['marks'];
							}
							$exam['maxMarks']	= $maxMarks;
						} else {
							$exam['questions']	= $questions[0];
							$exam['maxMarks']	= $questions[0]['marks'];
						}
						$arrStudents = array();
						$percentStudents = array();
						$i=0;
						$query="SELECT ss.userId,sd.firstName,sd.lastName
							FROM `student_subject` AS ss
							INNER JOIN `student_details` AS sd ON sd.userId=ss.userId
							INNER JOIN `exam_manual_marks` AS emm ON emm.student_id=ss.userId AND emm.exam_id={$data->examId}
							WHERE ss.courseId={$data->courseId} AND ss.subjectId={$data->subjectId} AND ss.Active=1
							GROUP BY ss.userId";
						$students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$students = $students->toArray();
						foreach ($students as $key => $value) {
							$select = $sql->select();
							$select->from('exam_manual_marks');
							$select->where(array(
										'exam_id' => $data->examId,
										'student_id' => $value['userId']
							));
							$select->columns(array('id', 'question_id', 'marks'));
							$statement = $sql->getSqlStringForSqlObject($select);
							$marks = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
							$marks = $marks->toArray();
							$percentStudents[$i]['name'] = $value['firstName'].' '.$value['lastName'];
							if ($exam['type'] == "deep") {
								$query="SELECT SUM(marks) AS totalMarks
										FROM `exam_manual_marks` AS emm
										WHERE emm.exam_id={$data->examId} AND emm.student_id={$value['userId']}";
								$totalMarks = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$totalMarks = $totalMarks->toArray();
								$totalMarks = $totalMarks[0]['totalMarks'];
								$arrStudents[] = array($value['userId'],$value['firstName'].' '.$value['lastName'],$totalMarks,$marks);
								$percentStudents[$i]['score'] = $totalMarks;
							} else {
								$arrStudents[] = array($value['userId'],$value['firstName'].' '.$value['lastName'],$marks[0]);
								$percentStudents[$i]['score'] = $marks[0]['marks'];
							}
							$i++;
						}
						$students = array();
						$temp = array();
						for ($i=0; $i < count($arrStudents); $i++) { 
							for ($j=0; $j < count($arrStudents); $j++) {
								if ($exam['type'] == "quick") {
									if ($arrStudents[$i][2]['marks']>$arrStudents[$j][2]['marks']) {
										$temp = $arrStudents[$i];
										$arrStudents[$i] = $arrStudents[$j];
										$arrStudents[$j] = $temp;
										$temp = array();
									}
								} else {
									if ($arrStudents[$i][2]>$arrStudents[$j][2]) {
										$temp = $arrStudents[$i];
										$arrStudents[$i] = $arrStudents[$j];
										$arrStudents[$j] = $temp;
										$temp = array();
									}
								}
							}
						}
						if ($exam['type'] == "quick") {
							$exam["highest"] = $arrStudents[0][2]['marks'];
							$exam["lowest"] = $arrStudents[count($arrStudents)-1][2]['marks'];
						} else {
							$exam["highest"] = $arrStudents[0][2];
							$exam["lowest"] = $arrStudents[count($arrStudents)-1][2];
						}
						$rank = 1;
						foreach ($arrStudents as $key => $value) {
							$arrStudents[$key]['rank'] = $rank;
							$rank++;
							if ($exam['type'] == "quick") {
								$arrStudents[$key]['marks'] 	= $value[2]['marks'];
								$arrStudents[$key]['percent']	= ($value[2]['marks']/$exam['maxMarks'])*100;
							} else {
								$arrStudents[$key]['marks'] 	= $value[2];
								$arrStudents[$key]['percent']	= ($value[2]/$exam['maxMarks'])*100;
							}
							$arrStudents[$key]['percentile'] = ((count($arrStudents)-$key)/count($arrStudents))*100;
							if ($key>0) {
								if ($exam['type'] == "quick") {
									if ($arrStudents[$key][2]['marks']==$arrStudents[$key-1][2]['marks']) {
										//rank--; // line for ascending ranks
										$arrStudents[$key]['rank'] = $arrStudents[$key-1]['rank'];
										$arrStudents[$key]['percentile'] = round($arrStudents[$key-1]['percentile'],2);
									};
								} else {
									if ($arrStudents[$key][2]==$arrStudents[$key-1][2]) {
										//rank--; // line for ascending ranks
										$arrStudents[$key]['rank'] = $arrStudents[$key-1]['rank'];
										$arrStudents[$key]['percentile'] = round($arrStudents[$key-1]['percentile'],2);
									};
								}
							};
							if ($arrStudents[$key][0] == $data->userId) {
								$myStudentIndex = $key;
							}
						}
						$marksDis = array();
						for ($i=0; $i <= $exam['maxMarks']; $i++) {
							$marksCount = 0;
							foreach ($arrStudents as $key => $value) {
								if ($arrStudents[$key]['marks'] == $i) {
									$marksCount++;
								}
							}
							if ($marksCount>0) {
								$marksDis[$i] = $marksCount;
							}
						}
						$qMarksDis = array();
						if ($exam['type'] == "deep") {
							$questions = $exam['questions'];
							foreach ($questions as $key => $value) {
								$qMarksDis[$key] = array();
								$studentCount = 0;
								for ($i=0; $i < $value['marks']; $i++) { 
									$marksCount = 0;
									foreach ($arrStudents as $key1 => $value1) {
										if($value1[3][$key]['marks'] == $i) {
											$marksCount++;
										}
									}
									if ($marksCount>0) {
										$qMarksDis[$key][$i] = $marksCount;
										$studentCount+= $marksCount;
									}
								}
								$exam['questions'][$key]['studentCount'] = $studentCount;
							}
						}
						$exam['students']		= $arrStudents[$myStudentIndex];
						$exam['perStudents']	= $percentStudents;
						$exam['marksDis']		= $marksDis;
						$exam['studentsCount']	= count($arrStudents);
						$exam['qMarksDis']		= $qMarksDis;
						$result->exam = $exam;
						$dataRange = new stdClass();
						$dataRange->examId = $data->examId;
						$dataRange->examType = 'manual-exam';
						$ranges=$this->getPercentRanges($dataRange);
						if ($ranges->status == 1) {
							$ranges = $ranges->score;
						} else {
							$ranges = array();
						}
						$result->ranges = $ranges;
					}
				} else {
					$result->status = 0;
					$result->message = "There was some error!";
					return $result;
				}
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function savePercentBracket($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				if ($data->examType == 'exam') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('percent_bracket_objective');
					$select->where(array(
								'examId' => $data->examId,
								'type' => $data->bracketType
					));
					/*$select->where("((percentLow BETWEEN {$data->percentLow} AND {$data->percentHigh}) || 
						(percentHigh BETWEEN {$data->percentLow} AND {$data->percentHigh}))");*/
					$select->where("(((percentLow >= {$data->percentLow}) AND (percentLow < {$data->percentHigh})) || 
						((percentHigh < {$data->percentLow}) AND (percentHigh >= {$data->percentHigh})))");
					$statement = $sql->getSqlStringForSqlObject($select);
					$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exams = $exams->toArray();
					if (count($exams) > 0) {
						$result->status = 0;
						$result->message = "Invalid range!";
						return $result;
					} else {
						$exams = new TableGateway('percent_bracket_objective', $adapter, null,new HydratingResultSet());
						$insert = array(
							'examId'			=>	$data->examId,
							'type'				=>	$data->bracketType,
							'percentLow'		=>	$data->percentLow,
							'percentHigh'		=>	$data->percentHigh,
							'comment' 			=> 	$data->comment,
							'smiley' 			=> 	$data->smiley,
							'status'			=>	1
						);
						$exams->insert($insert);
						$data->bracketId = $exams->getLastInsertValue();
						if($data->bracketId == 0) {
							$result->status = 0;
							$result->message = "Some error occured";
							return $result;
						}
					}
				} else if ($data->examType == 'subjective-exam') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('percent_bracket_subjective');
					$select->where(array(
								'examId' => $data->examId,
								'type' => $data->bracketType
					));
					/*$select->where("((percentLow BETWEEN {$data->percentLow} AND {$data->percentHigh}) || 
						(percentHigh BETWEEN {$data->percentLow} AND {$data->percentHigh}))");*/
					$select->where("(((percentLow >= {$data->percentLow}) AND (percentLow < {$data->percentHigh})) || 
						((percentHigh < {$data->percentLow}) AND (percentHigh >= {$data->percentHigh})))");
					$statement = $sql->getSqlStringForSqlObject($select);
					$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exams = $exams->toArray();
					if (count($exams) > 0) {
						$result->status = 0;
						$result->message = "Invalid range!";
						return $result;
					} else {
						$exams = new TableGateway('percent_bracket_subjective', $adapter, null,new HydratingResultSet());
						$insert = array(
							'examId'			=>	$data->examId,
							'type'				=>	$data->bracketType,
							'percentLow'		=>	$data->percentLow,
							'percentHigh'		=>	$data->percentHigh,
							'comment' 			=> 	$data->comment,
							'smiley' 			=> 	$data->smiley,
							'status'			=>	1
						);
						$exams->insert($insert);
						$data->bracketId = $exams->getLastInsertValue();
						if($data->bracketId == 0) {
							$result->status = 0;
							$result->message = "Some error occured";
							return $result;
						}
					}
				} else if ($data->examType == 'submission') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('percent_bracket_submission');
					$select->where(array(
								'examId' => $data->examId,
								'type' => $data->bracketType
					));
					/*$select->where("((percentLow BETWEEN {$data->percentLow} AND {$data->percentHigh}) || 
						(percentHigh BETWEEN {$data->percentLow} AND {$data->percentHigh}))");*/
					$select->where("(((percentLow >= {$data->percentLow}) AND (percentLow < {$data->percentHigh})) || 
						((percentHigh < {$data->percentLow}) AND (percentHigh >= {$data->percentHigh})))");
					$statement = $sql->getSqlStringForSqlObject($select);
					$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exams = $exams->toArray();
					if (count($exams) > 0) {
						$result->status = 0;
						$result->message = "Invalid range!";
						return $result;
					} else {
						$exams = new TableGateway('percent_bracket_submission', $adapter, null,new HydratingResultSet());
						$insert = array(
							'examId'			=>	$data->examId,
							'type'				=>	$data->bracketType,
							'percentLow'		=>	$data->percentLow,
							'percentHigh'		=>	$data->percentHigh,
							'comment' 			=> 	$data->comment,
							'smiley' 			=> 	$data->smiley,
							'status'			=>	1
						);
						$exams->insert($insert);
						$data->bracketId = $exams->getLastInsertValue();
						if($data->bracketId == 0) {
							$result->status = 0;
							$result->message = "Some error occured";
							return $result;
						}
					}
				} else if ($data->examType == 'manual-exam') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('percent_bracket_manual');
					$select->where(array(
								'examId' => $data->examId,
								'type' => $data->bracketType
					));
					/*$select->where("((percentLow BETWEEN {$data->percentLow} AND {$data->percentHigh}) || 
						(percentHigh BETWEEN {$data->percentLow} AND {$data->percentHigh}))");*/
					$select->where("(((percentLow >= {$data->percentLow}) AND (percentLow < {$data->percentHigh})) || 
						((percentHigh < {$data->percentLow}) AND (percentHigh >= {$data->percentHigh})))");
					$statement = $sql->getSqlStringForSqlObject($select);
					$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exams = $exams->toArray();
					if (count($exams) > 0) {
						$result->status = 0;
						$result->message = "Invalid range!";
						return $result;
					} else {
						$exams = new TableGateway('percent_bracket_manual', $adapter, null,new HydratingResultSet());
						$insert = array(
							'examId'			=>	$data->examId,
							'type'				=>	$data->bracketType,
							'percentLow'		=>	$data->percentLow,
							'percentHigh'		=>	$data->percentHigh,
							'comment' 			=> 	$data->comment,
							'smiley' 			=> 	$data->smiley,
							'status'			=>	1
						);
						$exams->insert($insert);
						$data->bracketId = $exams->getLastInsertValue();
						if($data->bracketId == 0) {
							$result->status = 0;
							$result->message = "Some error occured";
							return $result;
						}
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function savePercentBracketEdit($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				if ($data->examType == 'exam') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('percent_bracket_objective');
					$select->where(array(
								'examId' => $data->examId,
								'type' => $data->bracketType
					));
					/*$select->where("((percentLow BETWEEN {$data->percentLow} AND {$data->percentHigh}) || 
						(percentHigh BETWEEN {$data->percentLow} AND {$data->percentHigh})) AND id!={$data->bracketId}");*/
					$select->where("(((percentLow >= {$data->percentLow}) AND (percentLow < {$data->percentHigh})) || 
						((percentHigh < {$data->percentLow}) AND (percentHigh >= {$data->percentHigh}))) AND id!={$data->bracketId}");
					$statement = $sql->getSqlStringForSqlObject($select);
					$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exams = $exams->toArray();
					if (count($exams) > 0) {
						$result->status = 0;
						$result->message = "Invalid range!";
						return $result;
					} else {
						$sql = new Sql($adapter);
						$update = $sql->update();
						$update->table('percent_bracket_objective');
						$update->set(array(
							'examId'			=>	$data->examId,
							'type'				=>	$data->bracketType,
							'percentLow'		=>	$data->percentLow,
							'percentHigh'		=>	$data->percentHigh,
							'comment' 			=> 	$data->comment,
							'smiley' 			=> 	$data->smiley,
							'status'			=>	1
						));
						$update->where(array('id'		=>	$data->bracketId));
						$statement = $sql->prepareStatementForSqlObject($update);
						$statement->execute();
					}
				} else if ($data->examType == 'subjective-exam') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('percent_bracket_subjective');
					$select->where(array(
								'examId' => $data->examId,
								'type' => $data->bracketType
					));
					/*$select->where("((percentLow BETWEEN {$data->percentLow} AND {$data->percentHigh}) || 
						(percentHigh BETWEEN {$data->percentLow} AND {$data->percentHigh})) AND id!={$data->bracketId}");*/
					$select->where("(((percentLow >= {$data->percentLow}) AND (percentLow < {$data->percentHigh})) || 
						((percentHigh < {$data->percentLow}) AND (percentHigh >= {$data->percentHigh}))) AND id!={$data->bracketId}");
					$statement = $sql->getSqlStringForSqlObject($select);
					$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exams = $exams->toArray();
					if (count($exams) > 0) {
						$result->status = 0;
						$result->message = "Invalid range!";
						return $result;
					} else {
						$sql = new Sql($adapter);
						$update = $sql->update();
						$update->table('percent_bracket_subjective');
						$update->set(array(
							'examId'			=>	$data->examId,
							'type'				=>	$data->bracketType,
							'percentLow'		=>	$data->percentLow,
							'percentHigh'		=>	$data->percentHigh,
							'comment' 			=> 	$data->comment,
							'smiley' 			=> 	$data->smiley,
							'status'			=>	1
						));
						$update->where(array('id'		=>	$data->bracketId));
						$statement = $sql->prepareStatementForSqlObject($update);
						$statement->execute();
					}
				} else if ($data->examType == 'submission') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('percent_bracket_submission');
					$select->where(array(
								'examId' => $data->examId,
								'type' => $data->bracketType
					));
					/*$select->where("((percentLow BETWEEN {$data->percentLow} AND {$data->percentHigh}) || 
						(percentHigh BETWEEN {$data->percentLow} AND {$data->percentHigh})) AND id!={$data->bracketId}");*/
					$select->where("(((percentLow >= {$data->percentLow}) AND (percentLow < {$data->percentHigh})) || 
						((percentHigh < {$data->percentLow}) AND (percentHigh >= {$data->percentHigh}))) AND id!={$data->bracketId}");
					$statement = $sql->getSqlStringForSqlObject($select);
					$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exams = $exams->toArray();
					if (count($exams) > 0) {
						$result->status = 0;
						$result->message = "Invalid range!";
						return $result;
					} else {
						$sql = new Sql($adapter);
						$update = $sql->update();
						$update->table('percent_bracket_submission');
						$update->set(array(
							'examId'			=>	$data->examId,
							'type'				=>	$data->bracketType,
							'percentLow'		=>	$data->percentLow,
							'percentHigh'		=>	$data->percentHigh,
							'comment' 			=> 	$data->comment,
							'smiley' 			=> 	$data->smiley,
							'status'			=>	1
						));
						$update->where(array('id'		=>	$data->bracketId));
						$statement = $sql->prepareStatementForSqlObject($update);
						$statement->execute();
					}
				} else if ($data->examType == 'manual-exam') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('percent_bracket_manual');
					$select->where(array(
								'examId' => $data->examId,
								'type' => $data->bracketType
					));
					/*$select->where("((percentLow BETWEEN {$data->percentLow} AND {$data->percentHigh}) || 
						(percentHigh BETWEEN {$data->percentLow} AND {$data->percentHigh})) AND id!={$data->bracketId}");*/
					$select->where("(((percentLow >= {$data->percentLow}) AND (percentLow < {$data->percentHigh})) || 
						((percentHigh < {$data->percentLow}) AND (percentHigh >= {$data->percentHigh}))) AND id!={$data->bracketId}");
					$statement = $sql->getSqlStringForSqlObject($select);
					$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exams = $exams->toArray();
					if (count($exams) > 0) {
						$result->status = 0;
						$result->message = "Invalid range!";
						return $result;
					} else {
						$sql = new Sql($adapter);
						$update = $sql->update();
						$update->table('percent_bracket_manual');
						$update->set(array(
							'examId'			=>	$data->examId,
							'type'				=>	$data->bracketType,
							'percentLow'		=>	$data->percentLow,
							'percentHigh'		=>	$data->percentHigh,
							'comment' 			=> 	$data->comment,
							'smiley' 			=> 	$data->smiley,
							'status'			=>	1
						));
						$update->where(array('id'		=>	$data->bracketId));
						$statement = $sql->prepareStatementForSqlObject($update);
						$statement->execute();
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function deletePercentBracket($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				if ($data->examType == 'exam') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('percent_bracket_objective');
					$select->where(array(
								'id' => $data->bracketId
					));
					$statement = $sql->getSqlStringForSqlObject($select);
					$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exams = $exams->toArray();
					if (count($exams) == 0) {
						$result->status = 0;
						$result->message = "Invalid range!";
						return $result;
					} else {
						$sql = new Sql($adapter);
						$delete = $sql->delete();
						$delete->from('percent_bracket_objective');
						$delete->where(array('id'	=>	$data->bracketId));
						$statement = $sql->prepareStatementForSqlObject($delete);
						$statement->execute();
					}
				} else if ($data->examType == 'subjective-exam') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('percent_bracket_subjective');
					$select->where(array(
								'id' => $data->bracketId
					));
					$statement = $sql->getSqlStringForSqlObject($select);
					$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exams = $exams->toArray();
					if (count($exams) == 0) {
						$result->status = 0;
						$result->message = "Invalid range!";
						return $result;
					} else {
						$sql = new Sql($adapter);
						$delete = $sql->delete();
						$delete->from('percent_bracket_subjective');
						$delete->where(array('id'	=>	$data->bracketId));
						$statement = $sql->prepareStatementForSqlObject($delete);
						$statement->execute();
					}
				} else if ($data->examType == 'submission') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('percent_bracket_submission');
					$select->where(array(
								'id' => $data->bracketId
					));
					$statement = $sql->getSqlStringForSqlObject($select);
					$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exams = $exams->toArray();
					if (count($exams) == 0) {
						$result->status = 0;
						$result->message = "Invalid range!";
						return $result;
					} else {
						$sql = new Sql($adapter);
						$delete = $sql->delete();
						$delete->from('percent_bracket_submission');
						$delete->where(array('id'	=>	$data->bracketId));
						$statement = $sql->prepareStatementForSqlObject($delete);
						$statement->execute();
					}
				} else if ($data->examType == 'manual-exam') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('percent_bracket_manual');
					$select->where(array(
								'id' => $data->bracketId
					));
					$statement = $sql->getSqlStringForSqlObject($select);
					$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$exams = $exams->toArray();
					if (count($exams) == 0) {
						$result->status = 0;
						$result->message = "Invalid range!";
						return $result;
					} else {
						$sql = new Sql($adapter);
						$delete = $sql->delete();
						$delete->from('percent_bracket_manual');
						$delete->where(array('id'	=>	$data->bracketId));
						$statement = $sql->prepareStatementForSqlObject($delete);
						$statement->execute();
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->message = "Comment successfully removed!";
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getPercentRanges($data) {
			$result = new stdClass();
			try {
				$result->score = array();
				$result->tag = array();
				$adapter = $this->adapter;
				if ($data->examType == 'exam') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('pbo' => 'percent_bracket_objective'));
					$select->join(array('s' => 'smileys'),'s.id = pbo.smiley',array('file'));
					$select->where(array(
								'examId' => $data->examId,
								'type'	 => 'score'
					));
					$select->order('percentLow');
					$statement = $sql->getSqlStringForSqlObject($select);
					$ranges = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$ranges = $ranges->toArray();
					if (count($ranges) > 0) {
						$result->score = $ranges;
					}
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('pbo' => 'percent_bracket_objective'));
					$select->join(array('s' => 'smileys'),'s.id = pbo.smiley',array('file'));
					$select->where(array(
								'examId' => $data->examId,
								'type'	 => 'tag'
					));
					$select->order('percentLow');
					$statement = $sql->getSqlStringForSqlObject($select);
					$ranges = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$ranges = $ranges->toArray();
					if (count($ranges) > 0) {
						$result->tag = $ranges;
					}
				} else if ($data->examType == 'subjective-exam') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('pbo' => 'percent_bracket_subjective'));
					$select->join(array('s' => 'smileys'),'s.id = pbo.smiley',array('file'));
					$select->where(array(
								'examId' => $data->examId,
								'type'	 => 'score'
					));
					$select->order('percentLow');
					$statement = $sql->getSqlStringForSqlObject($select);
					$ranges = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$ranges = $ranges->toArray();
					if (count($ranges) > 0) {
						$result->score = $ranges;
					}
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('pbo' => 'percent_bracket_subjective'));
					$select->join(array('s' => 'smileys'),'s.id = pbo.smiley',array('file'));
					$select->where(array(
								'examId' => $data->examId,
								'type'	 => 'tag'
					));
					$select->order('percentLow');
					$statement = $sql->getSqlStringForSqlObject($select);
					$ranges = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$ranges = $ranges->toArray();
					if (count($ranges) > 0) {
						$result->tag = $ranges;
					}
				} else if ($data->examType == 'submission') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('pbo' => 'percent_bracket_submission'));
					$select->join(array('s' => 'smileys'),'s.id = pbo.smiley',array('file'));
					$select->where(array(
								'examId' => $data->examId,
								'type'	 => 'score'
					));
					$select->order('percentLow');
					$statement = $sql->getSqlStringForSqlObject($select);
					$ranges = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$ranges = $ranges->toArray();
					if (count($ranges) > 0) {
						$result->score = $ranges;
					}
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('pbo' => 'percent_bracket_submission'));
					$select->join(array('s' => 'smileys'),'s.id = pbo.smiley',array('file'));
					$select->where(array(
								'examId' => $data->examId,
								'type'	 => 'tag'
					));
					$select->order('percentLow');
					$statement = $sql->getSqlStringForSqlObject($select);
					$ranges = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$ranges = $ranges->toArray();
					if (count($ranges) > 0) {
						$result->tag = $ranges;
					}
				} else if ($data->examType == 'manual-exam') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('pbo' => 'percent_bracket_manual'));
					$select->join(array('s' => 'smileys'),'s.id = pbo.smiley',array('file'));
					$select->where(array(
								'examId' => $data->examId,
								'type'	 => 'score'
					));
					$select->order('percentLow');
					$statement = $sql->getSqlStringForSqlObject($select);
					$ranges = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$ranges = $ranges->toArray();
					if (count($ranges) > 0) {
						$result->score = $ranges;
					}
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('pbo' => 'percent_bracket_manual'));
					$select->join(array('s' => 'smileys'),'s.id = pbo.smiley',array('file'));
					$select->where(array(
								'examId' => $data->examId,
								'type'	 => 'tag'
					));
					$select->order('percentLow');
					$statement = $sql->getSqlStringForSqlObject($select);
					$ranges = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$ranges = $ranges->toArray();
					if (count($ranges) > 0) {
						$result->tag = $ranges;
					}
				}
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
	}
?>