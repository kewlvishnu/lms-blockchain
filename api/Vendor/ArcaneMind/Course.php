<?php

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "Base.php";

//$adapter = require "adapter.php";
class Course extends Base {

	public function createCourse($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$data->name=$data->courseName;
			$ckResult=$this->checkDuplicateCourse($data);
			if ($ckResult->available == '') {
				$result->status = 0;
				$result->message = "This Course Already Exists";
				return $result;
			}
			$adapter = $this->adapter;
			//$courses = new TableGateway('courses', $adapter, null, new HydratingResultSet());
			$insert = array(
				'ownerId' => $data->userId,
				'name' => $data->courseName,
				'subtitle' => $data->courseSubtitle,
				'description' => $data->courseDesc,
				'liveDate' => $data->liveDate,
				'targetAudience' => $data->targetAudience,
				'socialImage' => $data->socialImage,
				//'tags'	=>	$data->tags,
				'studentPrice' => 0,
				'studentPriceINR' => 0,
				'availStudentMarket' => 0,
				'availContentMarket' => 0,
				'licensingLevel' => 0,
				'demo' => 0,
				'liveForStudent' => 0,
				'liveForContentMarket' => 0,
				'demoVideo' => '',
				'coursesDoc' => '',
				'chat' => 0
			);
			// slug generation begins
			$i = 0;
			$data->slug = $this->slug($data->courseName);
			do {
				if ($i>0) {
					$data->slug = $data->slug.$i;
				}
				$i++;
				$selectString = "SELECT id,name FROM courses WHERE slug = '{$data->slug}'";
				$courseSlugs = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$courseSlugs = $courseSlugs->toArray();
			} while(count($courseSlugs)>0);

			$insert['slug'] = $data->slug;
			// slug generation ends

			if ($data->setEndDate == 1){
				$insert['endDate'] = $data->endDate;
			} else {
				$insert['endDate'] = '';
			}
			
			$query="INSERT INTO courses (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
			$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$adapter->query("SET @courseId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
			/*$courses->insert($insert);
			$data->courseId = $courses->getLastInsertValue();
			if ($data->courseId == 0) {
				$result->status = 0;
				$result->message = "Some error occured";
				return $result;
			}*/

			// manage tags
			$reqTags = array();
			if (!empty($data->tags)) {
				$reqTags = $data->tags;
				foreach ($data->tags as $key => $tag) {
					$query="SELECT *
							FROM `course_tags`
							WHERE `tag`= '$tag'";
					$tagCheck = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$tagCheck = $tagCheck->toArray();
					if (count($tagCheck)) {
						$tagId = $tagCheck[0]['id'];
						//$questionTags = new TableGateway('course_tag_links', $adapter, null,new HydratingResultSet());
						$insert = array(
										//'course_id' => $data->courseId,
										'tag_id' => $tagId
									);
						//$questionTags->insert($insert);
						$query="INSERT INTO course_tag_links (`".implode("`,`", array_keys($insert))."`,`course_id`) VALUES ('".implode("','", array_values($insert))."',@courseId)";
						$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					} else {
						//$questionTags = new TableGateway('course_tags', $adapter, null,new HydratingResultSet());
						$insert = array(
										'tag'	=>  $tag
									);
						$query="INSERT INTO course_tags (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
						$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$adapter->query("SET @tagId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
						//$questionTags->insert($insert);
						//$tagId = $questionTags->getLastInsertValue();
						//var_dump('2: '.$tagId);
						//$questionTags = new TableGateway('course_tag_links', $adapter, null,new HydratingResultSet());
						/*$insert = array(
										'course_id' => $data->courseId,
										'tag_id' => $tagId
									);*/
						$query="INSERT INTO course_tag_links (`course_id`,`tag_id`) VALUES (@courseId,@tagId)";
						$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						//$questionTags->insert($insert);
					}
				}
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$query = "SELECT @courseId as courseID";
			$courseID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$courseID = $courseID->toArray();
			$data->courseId = $courseID[0]["courseID"];
			$query="SELECT sqtl.id, qt.tag
					FROM `course_tag_links` AS sqtl
					INNER JOIN `course_tags` AS qt ON sqtl.tag_id=qt.id
					WHERE sqtl.course_id='{$data->courseId}'";
			$tagsInDb = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$tagsInDb = $tagsInDb->toArray();
			if (count($tagsInDb)>0){
				if(!empty($reqTags)) {
					$deleteTagIds = array();
					foreach ($tagsInDb as $key => $tag) {
						if (in_array($tag['tag'], $reqTags)) {
							unset($tagsInDb[$key]);
						} else {
							$deleteTagIds[] = $tag['id'];
						}
					}
					if (!empty($deleteTagIds)) {
						$sql = new Sql($adapter);
						$delete = $sql->delete();
						$delete->from('course_tag_links');
						$delete->where(array('id'	=>	$deleteTagIds));
						$statement = $sql->prepareStatementForSqlObject($delete);
						$statement->execute();
					}
				} else {
					$deleteQuestions = "DELETE FROM course_tag_links WHERE course_id=@courseId";
					$adapter->query($deleteQuestions, $adapter::QUERY_MODE_EXECUTE);
					/*$sql = new Sql($adapter);
					$delete = $sql->delete();
					$delete->from('course_tag_links');
					$delete->where(array('course_id'	=>	$data->courseId));
					$statement = $sql->prepareStatementForSqlObject($delete);
					$statement->execute();*/
				}
			}

			if (isset($data->portfolioSlug) && !empty($data->portfolioSlug)) {
				
				$selectString ="SELECT * FROM `pf_portfolio` WHERE `subdomain`='{$data->portfolioSlug}'";
				$portfolioDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$portfolioDetails = $portfolioDetails->toArray();
				$portfolioDetails = $portfolioDetails[0];
				$portfolioId = $portfolioDetails['id'];

				$pf_main_courses = new TableGateway('pf_main_courses', $this->adapter, null, new HydratingResultSet());
				$insert = array(
					'portfolioId' => $portfolioId,
					'courseId' => $data->courseId
				);
				/*$query="INSERT INTO pf_main_courses (`".implode("`,`", array_keys($insert))."`,`courseId`) VALUES ('".implode("','", array_values($insert))."',@courseId)";
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);*/
				$pf_main_courses->insert($insert);
			}
			$this->addCourseCategories($data);
			$_SESSION['courseId'] = $data->courseId;
			$result->courseId=$data->courseId;
			$result->status = 1;
			$result->message = 'Course created!';
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function addCourseCategories($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			if (count($data->courseCateg) == 0) {
				$result->status = 1;
				return $result;
			}
			$adapter = $this->adapter;
			$course_categories = new TableGateway('course_categories', $adapter, null, new HydratingResultSet());
			foreach ($data->courseCateg as $cat) {
				$insert = array(
					'courseId' => $data->courseId,
					'categoryId' => $cat
				);
				$course_categories->insert($insert);
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			if ($course_categories->getLastInsertValue() == 0) {
				$result->status = 0;
				return $result;
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getCourseCategories() {
		$result = new stdClass();
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		//deleting previous image from cloud
		$select = $sql->select();
		$select->from('categories');
		$selectString = $sql->getSqlStringForSqlObject($select);
		$courseCategories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$courseCategories = $courseCategories->toArray();
		$result->courseCategories = $courseCategories;
		$result->status = 1;
		return $result;
	}

	public function saveCategoriesDesc($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('categories');
			$update->set(array('description'=>	urldecode($data->description)));
			$update->where(array('id'		=>	$data->categoriesId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$statement->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function viewCourseCategory($data) {
		$result = new stdClass();
		try {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('categories');
			$selectString = $sql->getSqlStringForSqlObject($select);
			$categories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$categories = $categories->toArray();
			$result->categories = $categories;
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function saveCourseImage($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			if (!$this->userOwnsCourse($data)) {
				$result->status = 0;
				$result->message = "An non recoverable error occured1!";
				return $result;
			}
			$adapter = $this->adapter;
			$where = array('id' => $data->courseId);
			$sql = new Sql($adapter);
			//deleting previous image from cloud
			$select = $sql->select();
			$select->from('courses');
			$select->where($where);
			$select->columns(array('image'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$pic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$pic = $pic->toArray();
			$pic = $pic[0]['image'];
			$pic = substr($pic, 37);
			if($pic != '')
			{
				require_once 'amazonDelete.php';
				deleteFile($pic);
			}
			$update = $sql->update();
			$update->table('courses');
			$update->set(array('image' => $data->imageName));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
				require_once 'amazonRead.php';
				$result->image = getSignedURL($data->imageName);
			} else {
				$result->image = 'assets/pages/media/profile/profile_user.jpg';
			}
			$result->status = 1;
			$result->message = 'Course Image Uploaded!';
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	//function to get course name availability
	public function checkDuplicateCourse($data) {
		$result = new stdClass();
		$search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
		$replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$name = trim(strtolower($data->name));
			/*$select = $sql->select();
			$select->from('courses');
			$select->columns(array('name'));
			$select->where(array(
						'ownerId'	=>	$data->userId,
						new \Zend\Db\Sql\Expression("TRIM(LCASE(name))")	=>	$name
			));
			$selectString = $sql->getSqlStringForSqlObject($select);*/
			if(isset($data->id))
				$selectString = "SELECT name FROM courses WHERE ownerId={$data->userId} AND TRIM(LCASE(name))='".$this->safeString($name)."' AND id!='{$data->id}' and deleted=0";
			else
				$selectString = "SELECT name FROM courses WHERE ownerId={$data->userId} AND TRIM(LCASE(name))='".$this->safeString($name)."' AND deleted=0 ";
			$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$temp = $temp->toArray();
			if(count($temp) == 0)
				$result->available = true;
			else
				$result->available = false;
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getCourseDetails($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$selectString ="SELECT * FROM `courses` WHERE `id`='{$data->courseId}' AND `deleted`=0";
			$select = $sql->select();
			$select->from('courses');
			$select->where(array('id' => $data->courseId,
				'deleted' => 0));
			$statement = $sql->prepareStatementForSqlObject($select);
			$courseDetails = $statement->execute();
			/*$selectString ="SELECT c.*,IFNULL(c.rating,0) AS courseRating FROM `courses` AS c WHERE `id`='{$data->courseId}' AND `deleted`=0";
			$courseDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);*/
			if ($courseDetails->count() !== 0) {
				/*$courseDetails = $courseDetails->toArray();
				$temp = $courseDetails[0];*/
				$temp = $courseDetails->next();
				$result->courseDetails = $temp;
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$result->courseDetails['image'] = getSignedURL($result->courseDetails['image']);
				} else {
					$result->courseDetails['image'] = 'assets/pages/media/profile/profile_user.jpg';
				}
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$result->courseDetails['coursesDoc'] = getSignedURL($result->courseDetails['coursesDoc']);
				} else {
					$result->courseDetails['coursesDoc'] = 'https://mwsu.edu/Assets/documents/academics/education/leadership-handbook/Tk20.pdf';
				}

				$query="SELECT qt.id,qt.tag AS name
						FROM `course_tag_links` AS qtl
						INNER JOIN `course_tags` AS qt ON qt.id=qtl.tag_id
						WHERE `course_id`={$data->courseId}";
				$tagValues = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->tags = $tagValues->toArray();
			} else {
				$result->status = 0;
				$result->exception = "Course not found";
				return $result;
			}

			//fetching rating
			require_once 'Student.php';
			$s = new Student();
			$result->courseDetails['rating'] = $s->getCourseRatings($data->courseId);
			$query="SELECT categoryId,category FROM course_categories ct JOIN categories c on ct.categoryId=c.id where courseId=$data->courseId";
			$courseCategories = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courseCategories = $courseCategories->toArray();
			$select = $sql->select();
			$select->from('course_licensing');
			$select->where(array('courseId' => $data->courseId));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courseLicensing = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->courseLicensing = $courseLicensing->toArray();

			$query="SELECT sp.*
					FROM subject_professors sp
					INNER JOIN subjects s on s.id=sp.subjectId
					WHERE courseId=$data->courseId";
			$subjectProfessors = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->subjectProfessors = count($subjectProfessors->toArray());

			$query="SELECT s.*
					FROM subjects s
					WHERE courseId=$data->courseId AND deleted=0";
			$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->subjects = count($subjects->toArray());

			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function updateCourse($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			/*
			  We need to verify if the user editing the course owns it.
			 */
			$adapter = $this->adapter;
			$where = array('id' => $data->courseId);
			$updateValues = array(
				'name' => $data->courseName,
				'subtitle' => $data->courseSubtitle,
				'description' => $data->courseDesc,
				'targetAudience' => $data->targetAudience,
				'liveDate' => $data->liveDate,
				'socialImage' => $data->socialImage,
				//'tags'	=>	$data->tags
			);


			$selectString = "SELECT name FROM courses WHERE id='{$data->courseId}'";
			$courseNameCheck = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$courseNameCheck = $courseNameCheck->toArray();
			if (count($courseNameCheck)>0) {
				if ($data->courseName != $courseNameCheck[0]['name']) {
					// slug generation begins
					$i = 0;
					$data->slug = $this->slug($data->courseName);
					do {
						if ($i>0) {
							$data->slug = $data->slug.$i;
						}
						$i++;
						$selectString = "SELECT id,name FROM courses WHERE slug = '{$data->slug}' AND id!='{$data->courseId}'";
						$courseSlugs = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$courseSlugs = $courseSlugs->toArray();
					} while(count($courseSlugs)>0);

					$updateValues['slug'] = $data->slug;
					// slug generation ends
				}
			}

			if ($data->setEndDate == 1)
				$updateValues['endDate'] = $data->endDate;
			else
				$updateValues['endDate'] = '';

			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('courses');
			$update->set($updateValues);
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$result = $statement->execute();

			$result->status = 1;

			if ($data->courseId == 0) {
				$result->status = 0;
				$result->message = "Some error occured";
				return $result;
			}

			if (!empty($data->tags)) {
				$reqTags = $data->tags;
				foreach ($data->tags as $key => $tag) {
					$query="SELECT *
							FROM `course_tags`
							WHERE `tag`= '$tag'";
					$tagCheck = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$tagCheck = $tagCheck->toArray();
					if (count($tagCheck)) {
						$tagId = $tagCheck[0]['id'];
						$query="SELECT *
								FROM `course_tag_links`
								WHERE `course_id`={$data->courseId} AND `tag_id`= '$tagId'";
						$tagValue = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$tagValue = $tagValue->toArray();
						if (count($tagValue)) {
							unset($data->tags[$key]);
						} else {
							$questionTags = new TableGateway('course_tag_links', $adapter, null,new HydratingResultSet());
							$insert = array(
											'course_id' => $data->courseId,
											'tag_id' => $tagId
										);
							$questionTags->insert($insert);
						}
					} else {
						$questionTags = new TableGateway('course_tags', $adapter, null,new HydratingResultSet());
						$insert = array(
										'tag'	=>  $tag
									);
						$questionTags->insert($insert);
						$tagId = $questionTags->getLastInsertValue();
						$questionTags = new TableGateway('course_tag_links', $adapter, null,new HydratingResultSet());
						$insert = array(
										'course_id' => $data->courseId,
										'tag_id' => $tagId
									);
						$questionTags->insert($insert);
					}
				}
			}
			$query="SELECT sqtl.id, qt.tag
					FROM `course_tag_links` AS sqtl
					INNER JOIN `course_tags` AS qt ON sqtl.tag_id=qt.id
					WHERE sqtl.course_id= '$data->courseId'";
			$tagsInDb = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$tagsInDb = $tagsInDb->toArray();
			if (count($tagsInDb)>0){
				if(!empty($reqTags)) {
					$deleteTagIds = array();
					foreach ($tagsInDb as $key => $tag) {
						if (in_array($tag['tag'], $reqTags)) {
							unset($tagsInDb[$key]);
						} else {
							$deleteTagIds[] = $tag['id'];
						}
					}
					if (!empty($deleteTagIds)) {
						$delete = $sql->delete();
						$delete->from('course_tag_links');
						$delete->where(array('id'	=>	$deleteTagIds));
						$statement = $sql->prepareStatementForSqlObject($delete);
						$statement->execute();
					}
				} else {
					$delete = $sql->delete();
					$delete->from('course_tag_links');
					$delete->where(array('course_id'	=>	$data->courseId));
					$statement = $sql->prepareStatementForSqlObject($delete);
					$statement->execute();
				}
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$this->deleteCourseCategories($data);

			$this->addCourseCategories($data);

			$_SESSION['courseId'] = $data->courseId;
			$result->status = 1;
			$result->message = 'Course updated!';
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function deleteCourseCategories($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();

		$result = new stdClass();
        try{
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$delete = $sql->delete();
			//Login Details
			$delete->from('course_categories');
			$delete->where(array('courseId' => $data->courseId));
			$statement = $sql->prepareStatementForSqlObject($delete);
			$courseCategories = $statement->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function updateCourseLicensing($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();

		$result = new stdClass();
        try {
			$this->deleteCourseLicensing($data);

			if (count($data->selectedLicensing) == 0) {
				$result->status = 1;
				return $result;
			}
			//var_dump($data->selectedLicensing); die();
			$adapter = $this->adapter;
			$courseLicensing = new TableGateway('course_licensing', $adapter, null, new HydratingResultSet());

			foreach ($data->selectedLicensing as $licId => $lic) {
				$jsonData = Zend\Json\Json::encode($lic->data, true);
				$insert = array(
					'courseId' => $data->courseId,
					'licenseId' => $licId,
					'price' => $lic->price,
					'data' => $jsonData
				);
				$courseLicensing->insert($insert);
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = "Licensing updated";
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function deleteCourseLicensing($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
        try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$delete = $sql->delete();
			//Login Details
			$delete->from('course_licensing');
			$delete->where(array('courseId' => $data->courseId));
			$statement = $sql->prepareStatementForSqlObject($delete);
			$courseLicensing = $statement->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getCourseLicensing($data) {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('course_licensing');
		$select->where(array('courseId' => $data->courseId));
		$selectString = $sql->getSqlStringForSqlObject($select);
		$courseLicensing = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$adapter->getDriver()->getConnection()->disconnect();
		return $courseLicensing->toArray();
	}
	
	
	/**
	* function to fetch Discount for a course
	* @author Ayush Pandey
	* @date 13/07/2015
	* @param courseId
	* @return result with discount variables
	*/
	
	public function getCourseDiscountArray($data) {
		try {
			$adapter = $this->adapter;
			//$result = new stdClass();
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('course_discount');
			$select->where(array('courseId' => $data));
			$select->order('discountId DESC');
			$selectString = $sql->getSqlStringForSqlObject($select);
			$course_discount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$adapter->getDriver()->getConnection()->disconnect();
			$coursearray= $course_discount->toArray();
			$coursearray=$coursearray[0];
			$result['status'] = 1;
			if(count($coursearray)>0)
			{
				$result['discount']=$coursearray['discount'];
				$result['discount']=$coursearray['startDate'];
				$result['discount']=$coursearray['endDate'];
				$result['status'] = 1;
			}
			else{
			$result['status'] = 1;
				return $result;
			}
		}catch(Exception $e){
			$result['status'] = 0;
			$result['message'] = $e->getMessage();
			return $result;
		}
	}
	
	public function getCourseDiscount($data) {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('course_discount');
		$select->where(array('courseId' => $data->courseId));
		$select->order('discountId DESC');
		$selectString = $sql->getSqlStringForSqlObject($select);
		$course_discount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$adapter->getDriver()->getConnection()->disconnect();
		$coursearray= $course_discount->toArray();
		if(count($coursearray)>0)
		{
			return $coursearray[0];
		}
		return $coursearray;
	}
	
	//changeupdateCoupon
	public function changeupdateCoupon($data) {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('course_coupons');
		$select->where(array('courseId' => $data->courseId,'couponCode' => $data->couponCode));
		$select->order('couponId DESC');
		$selectString = $sql->getSqlStringForSqlObject($select);
		$course_discount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$adapter->getDriver()->getConnection()->disconnect();
		$coursearray= $course_discount->toArray();
		if(count($coursearray)>0)
		{
			return $coursearray[0];
		}
		return $coursearray;
	}
	
	public function getCourseCoupons($data) {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('course_coupons');
		$select->where(array('courseId' => $data->courseId));
		$select->order('endDate DESC');
		$selectString = $sql->getSqlStringForSqlObject($select);
		$course_discount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$adapter->getDriver()->getConnection()->disconnect();
		$coursearray= $course_discount->toArray();
		return $coursearray;
	}
	
	public function getAllCoupons($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('courses')
					->order('weight ASC')
					->order('id DESC');
			if (isset($data->instituteId)) {
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('ownerId', $data->instituteId);
				$where->equalTo('deleted', 0);
				$where->notEqualTo('liveDate', '');
				$select->where($where);
				$selectString = "SELECT * FROM courses WHERE ownerId={$data->instituteId} AND deleted=0 AND liveDate!='' AND id NOT IN (SELECT newCourseId FROM purchase_history) ORDER BY weight ASC, id DESC;";
			} else {
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('ownerId', $data->userId);
				$where->equalTo('deleted', 0);
				$where->notEqualTo('liveDate', '');
				$select->where($where);
				$selectString = "SELECT * FROM courses WHERE ownerId={$data->userId} AND deleted=0 AND liveDate!='' AND id NOT IN (SELECT newCourseId FROM purchase_history) ORDER BY weight ASC, id DESC;";
			}
			//$selectString = $sql->getSqlStringForSqlObject($select);
			$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->tcourses = $courses->toArray();

			if (count($result->tcourses) == 0) {
				$result->status = 1;
				$result->exception = "No Courses Found";
				$result->courses = array();
				return $result;
			}
			$result->courses = array();
			$newRes  = new stdClass();
			foreach ($result->tcourses as $c) {
			   $req = new stdClass();
				$req->courseId = $c['id'];
				//$c['licensing'] = $this->getCourseLicensing($req);
				$c['Coupons'] = $this->getCourseCoupons($req);
				/*foreach($c['Coupons'] as $couponsarray)
				{
					
					$result->coupons[]=$couponsarray;
					$newRes->coursesId=$c['id'];
					
				}*/
				
				$result->courses[] = $c;
			}
			unset($result->tcourses);
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	
	public function getAllCourses($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('courses')
					->order('weight ASC')
					->order('id DESC');
					
			if (isset($data->slug) && !empty($data->slug)) {
				$selectString ="SELECT * FROM `pf_portfolio` WHERE `subdomain`='{$data->slug}' AND `userId`={$data->userId}";
				$portfolioDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				if($portfolioDetails->count() > 0) {
					$portfolios	 = $portfolioDetails->toArray();
					$portfolio	 = $portfolios[0];
					$portfolioId = $portfolio['id'];
					$instituteId = $portfolio['userId'];
					$selectString = "SELECT c.*
					FROM `pf_main_courses` AS pmc
					INNER JOIN `courses` AS c ON c.id=pmc.courseId
					WHERE pmc.portfolioId={$portfolioId} AND c.ownerId={$instituteId} AND c.deleted=0 AND c.liveDate!='' AND c.id NOT IN (SELECT newCourseId FROM purchase_history) ORDER BY c.weight ASC, c.id DESC;";
				}
			} elseif (isset($data->instituteId)) {
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('ownerId', $data->instituteId);
				$where->equalTo('deleted', 0);
				$where->notEqualTo('liveDate', '');
				$select->where($where);
				/*$selectString ="SELECT *
								FROM courses
								WHERE ownerId={$data->instituteId} AND deleted=0 AND liveDate!=''
								AND id NOT IN (
									SELECT newCourseId FROM purchase_history
								) ORDER BY weight ASC, id DESC;";*/
				$selectString ="SELECT c.*, IF(fc.id IS NULL, 0, 1) AS favorite
								FROM courses c
								LEFT JOIN favorite_courses fc ON fc.courseId=c.id AND fc.userId=c.ownerId
								WHERE c.ownerId={$data->instituteId} AND c.deleted=0 AND c.liveDate!=''
								AND c.id NOT IN (
									SELECT newCourseId FROM purchase_history
								) ORDER BY fc.courseId DESC, weight ASC, id DESC;";
			} else {
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('ownerId', $data->userId);
				$where->equalTo('deleted', 0);
				$where->notEqualTo('liveDate', '');
				$select->where($where);
				/*$selectString = "SELECT * FROM courses WHERE ownerId={$data->userId} AND deleted=0 AND liveDate!='' AND id NOT IN (SELECT newCourseId FROM purchase_history) ORDER BY weight ASC, id DESC;";*/
				$selectString ="SELECT c.*, IF(fc.id IS NULL, 0, 1) AS favorite
								FROM courses c
								LEFT JOIN favorite_courses fc ON fc.courseId=c.id AND fc.userId=c.ownerId
								WHERE c.ownerId={$data->userId} AND c.deleted=0 AND c.liveDate!=''
								AND c.id NOT IN (
									SELECT newCourseId FROM purchase_history
								) ORDER BY fc.courseId DESC, weight ASC, id DESC;";
			}
			//$selectString = $sql->getSqlStringForSqlObject($select);
			$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->tcourses = $courses->toArray();

			if (count($result->tcourses) == 0) {
				$result->status = 1;
				$result->exception = "No Courses Found";
				$result->courses = array();
				return $result;
			}
			$result->courses = array();
			foreach ($result->tcourses as $c) {
				/*if ($c['liveDate'] != '')
					$c['liveDate'] = date('j-m-Y', $c['liveDate']/1000);
				if ($c['endDate'] != '')
					$c['endDate'] = date('j-m-Y', $c['endDate']/1000);*/
				$req = new stdClass();
				$req->courseId = $c['id'];
				$c['licensing'] = $this->getCourseLicensing($req);
				$c['discount'] = $this->getCourseDiscount($req);
				require_once "Subject.php";
				$s = new Subject();
				$c['subjects'] = $s->getSubjectsForCourse($req);
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$c['image'] = getSignedURL($c['image']);
				} else {
					$c['image'] = "assets/pages/img/background/32.jpg";
				}
				$result->courses[] = $c;
			}
			unset($result->tcourses);
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function checkCourseAcess($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('student_subject');
			$select->columns(array('id','subjectId'));
			$select->where(array('courseId' => $data->courseId,'userId' => $data->userId,'Active'=>1));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$subjects1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$subjects1 = $subjects1->toArray();
			$stringSubjectids=array();	
			if(count($subjects1) > 0){
				foreach($subjects1 as $key2=>$value2)
				{
				//$stringSubjectids .=','.$value['subjectId'];
					$stringSubjectids[$key2]=$value2['subjectId'];
				}
				
				$idcheck=in_array($data->subjectId,$stringSubjectids);
				if($idcheck>0)
				{
					$result->status = 1;
				}else{
					$result->status = 0;
				}
			}
			else{
				$result->status = 1;
			}		
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	
	public function getAllCoursesForApproval($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$query = "SELECT c.id,l.id userid,c.name ,c.liveDate,c.endDate,l.email,u.contactMobile,
			  case 
				when ur.roleId= 1 then (select name from institute_details where userId=l.id)
				when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
				when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
				when ur.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId= l.id)
				else 'NA' end as username
			from courses c
				JOIN login_details l on l.id=c.ownerId
				 Join user_roles  ur on ur.userId=l.id 
				JOIN user_details u on u.userId=l.id where c.approved=0 and ur.roleId NOT IN (4,5) order by c.weight ASC, c.id  DESC  ";
			// var_dump($query);
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->tempCourses = $courses->toArray();
			if (count($result->tempCourses) == 0) {
				$result->status = 0;
				$result->exception = "No Courses Found";
				return $result;
			}
			$result->courses = array();
			foreach ($result->tempCourses as $c) {
				if ($c['liveDate'] != '')
					$c['liveDate'] = date('j-m-Y', $c['liveDate']);
				if ($c['endDate'] != '')
					$c['endDate'] = date('j-m-Y', $c['endDate']);
				$result->courses[] = $c;
			}
			unset($result->tempCourses);
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getListCoursesForAdmin($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$query="SELECT c.id,l.id userid,c.name,l.email,u.contactMobile,
					CASE 
						when ur.roleId= 1 then (select name from institute_details where userId=l.id)
						when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
						when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
						when ur.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId= l.id)
						else 'NA' end as username
					FROM courses c
						INNER JOIN login_details l on l.id=c.ownerId
						INNER JOIN user_roles  ur on ur.userId=l.id 
						INNER JOIN user_details u on u.userId=l.id where ur.roleId NOT IN (4,5) ORDER BY c.weight ASC, c.id  DESC";
			// var_dump($query);
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			if (count($result->courses) == 0) {
				$result->status = 0;
				$result->exception = "No Courses Found";
				return $result;
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getAllApprovedCourses($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$query="SELECT c.id,l.id userid,c.name,l.email,u.contactMobile,
					CASE 
						when ur.roleId= 1 then (select name from institute_details where userId=l.id)
						when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
						when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
						when ur.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId= l.id)
						else 'NA' end as username
					FROM courses c
						INNER JOIN login_details l on l.id=c.ownerId
						INNER JOIN user_roles  ur on ur.userId=l.id 
						INNER JOIN user_details u on u.userId=l.id where c.approved=1 and ur.roleId NOT IN (4,5) order by c.weight ASC, c.id  DESC";
			// var_dump($query);
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			if (count($result->courses) == 0) {
				$result->status = 0;
				$result->exception = "No Courses Found";
				return $result;
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getAlldeletedCourse($data) {
		$result = new stdClass();
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		try {
			$query = "SELECT l.id,c.name ,c.id courseid,l.email, u.contactMobile,
			 case 
			when ur.roleId= 1 then (select name from institute_details where userId= l.id)
				when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
				when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
				when ur.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId= l.id)
				else 'NA' end as username
			From login_details  l 
			join user_details  u on l.id=u.userId
			Join user_roles  ur on ur.userId=l.id 
			JOIN courses c on c.ownerId=l.id where ur.roleId not in (4,5) and  c.deleted=1 order by c.id ";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			if (count($result->courses) == 0) {
				$result->status = 0;
				$result->exception = "No deleted Courses Found";
				return $result;
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function approveCourse($req) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
			$replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('courses');
			$update->set(array('approved' => 1));
			$update->where(array('id' => $req->courseId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$result = $statement->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			 
			$course = $this->getCourseAndInstituteName($req->courseId);
		   
			$course = $course->institute;
			$courseId = $req->courseId;
			$coursename = $course[0]['course'];
			$institute = $course[0]['institute'];
			$institutemail = $course[0]['institutemail'];
			$instituteId = $course[0]['instituteId'];
			
			// insert notification
			$notification="Your course ".$this->safeString($coursename)." ( course id : $courseId ) has been approved for Marketplace ";
			$query = "Insert into notifications (userId,message) values ($instituteId,'$notification')";
			$notification=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				
			
			require_once "User.php";
			$c = new User();
		   
			
			$instituteSubject = "Course $coursename (Course ID: $courseId) accepted for Integro Market Place";
			$emailInstitute = "Dear $institute, <br> <br>";
			$emailInstitute .= " Congratulations! We are very happy to inform you that your request for addition of the Course $coursename (Course ID: $courseId) to the market place of <a href=integro.io >Integro.io <a> has been accepted .<br> <br>";

			$emailInstitute .= "Please write to us at <a href=admin@integro.io >admin@integro.io <a> if you have any queries.<br> <br> ";

			$emailInstitute .= "<table border=1 ><thead><tr><th> Institute Name</th> <th>Institute ID</th> <th>Accepted Course Name  </th> <th> Course Id</th></tr></thead>"; 
			$emailInstitute .= "<tbody><tr><td> $institute</td><td>$instituteId</td> <td>$coursename</td> <td> $courseId</td></tr></tbody></table>"; 

			$c->sendMail($institutemail, $instituteSubject, $emailInstitute);			
			
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function rejectCourse($req) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
			$replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('courses');
			$update->set(array('approved' => 2));
			$update->where(array('id' => $req->courseId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$result = $statement->execute();

			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			
			$course = $this->getCourseAndInstituteName($req->courseId);
		   
			$course = $course->institute;
			$courseId = $req->courseId;
			$coursename = $course[0]['course'];
			$institute = $course[0]['institute'];
			$institutemail = $course[0]['institutemail'];
			$instituteId = $course[0]['instituteId'];
			
			 // insert notification
				$notification="Your course ".$this->safeString($coursename)." ( course id : $courseId ) has not been approved for Marketplace. We will contact you soon on the next step of actions ";
				$query = "Insert into notifications (userId,message) values ($instituteId,'$notification')";
				$notification=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				
			
			require_once "User.php";
			$c = new User();
		   
			global $siteBase;
			$instituteSubject = "Course  $coursename (Course ID: $courseId) disapproved for Integro Marketplace";
			$emailInstitute = "Dear $institute, <br> <br>";
			$emailInstitute .= "We are very sorry to inform you that your request for addition of the Course $coursename (Course ID: $courseId) to the Market Place of <a href='$siteBase'>Integro.io</a> has been denied. <br> <br>";
			// $emailInstitute .= "Reason for disapproval:  <br> <br>";
			// $emailInstitute .= "__________________________________________________  <br> <br>";

			$emailInstitute .= "Please make sure that the following minimum requirements are met:<br><br>";
			$emailInstitute .= "a) You have at least one one subject<br><br>";
			$emailInstitute .= "b) Your subject should  contain<br>&emsp;&emsp;&emsp;&emsp; i) at least one chapter with either a content (video, presentation, pdf) or an Assignment/Exam<br>&emsp;&emsp;&emsp;&emsp; ii) independent Assignment/Exams<br><br>";
			$emailInstitute .= "c) Your course meets basic quality standards.<br><br>";
			$emailInstitute .= "Our Team would be contact you soon to assist in improving the course in order to meet the requirements.<br><br>";
			$emailInstitute .= "<span style='font-size: 12px;'>Important note: You do NOT need this approval to invite students using the course keys.<br><a href='{$siteBase}admin/CourseKey.php'>Click here to purchase course keys.</a></span><br><br>";
			$emailInstitute .= "Please write to us at <a href='mailto:admin@integro.io'>admin@integro.io</a> in case of any queries or concerns.<br><br>";
			$emailInstitute .= "<table border=1 ><thead><tr><th> Institute Name</th> <th>Institute ID</th> <th>Rejected Course Name</th> <th> Course Id</th></tr></thead>"; 
			$emailInstitute .= "<tbody><tr><td> $institute</td><td>$instituteId</td> <td>$coursename</td> <td> $courseId</td></tr></tbody></table>"; 

			$c->sendMail($institutemail, $instituteSubject, $emailInstitute);
			
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getCoursesForImport($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			//            $select = $sql->select();
			//            $select->from('courses');
			//            $where = new \Zend\Db\Sql\Where();
			//            $where->equalTo('ownerId', $data->userId);
			//            $where->notEqualTo('id', $data->courseId);
			//			$where->equalTo('deleted', 0);
			//
			//            $select->where($where);
			//            $selectString = $sql->getSqlStringForSqlObject($select);
			$selectString="SELECT * FROM courses c WHERE ownerId=$data->userId AND id NOT IN (SELECT newCourseId FROM purchase_history) AND id!={$data->courseId} AND deleted=0 ";
			 $courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			 $tempCourses = $courses->toArray();

			if (count($tempCourses) == 0) {
				$result->status = 1;
				$result->message = "No Courses Found";
				return $result;
			}
			$result->courses = array();
			foreach ($tempCourses as $c) {
				//                if ($c['liveDate'] != '') {
				//					$c['liveDate'] = $c['liveDate']/1000;
				//                    $c['liveDate'] = date('j-m-Y', $c['liveDate']);
				//				}
				//                if ($c['endDate'] != '') {
				//					$c['endDate'] = $c['endDate']/1000;
				//                    $c['endDate'] = date('j-m-Y', $c['endDate']);
				//				}
				$req = new stdClass();
				$req->courseId = $c['id'];
				$req->importToCourseId = $data->courseId;
				$c['licensing'] = $this->getCourseLicensing($req);
				require_once "Subject.php";
				$s = new Subject();
				$c['subjects'] = $s->getSubjectsForImport($req);
				if (count($c['subjects']) != 0)
					$result->courses[] = $c;
			}
			//unset($result->tempCourses);
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getPurchasedCoursesForImport($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			//            $select = $sql->select();
			//            $select->from('courses');
			//            $where = new \Zend\Db\Sql\Where();
			//            $where->equalTo('ownerId', $data->userId);
			//            $where->notEqualTo('id', $data->courseId);
			//			  $where->equalTo('deleted', 0);
			//
			//            $select->where($where);
			//            $selectString = $sql->getSqlStringForSqlObject($select);
			$selectString="SELECT c.id, c.name, c.ownerId, c.liveDate, c.endDate, p.licensingLevel, p.parentCourseId, p.purchasedAt FROM courses AS c JOIN purchase_history AS p ON p.newCourseId=c.id  WHERE ownerId={$data->userId}  and c.id!={$data->courseId} and deleted=0 ";
			$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->tempCourses = $courses->toArray();

			if (count($result->tempCourses) == 0) {
				$result->status = 1;
				$result->message = "No Courses Found";
				return $result;
			}
			$result->courses = array();
			foreach ($result->tempCourses as $c) {
				//                if ($c['liveDate'] != '') {
				//					$c['liveDate'] = $c['liveDate']/1000;
				//                    $c['liveDate'] = date('j-m-Y', $c['liveDate']);
				//				}
				//                if ($c['endDate'] != '') {
				//					$c['endDate'] = $c['endDate']/1000;
				//                    $c['endDate'] = date('j-m-Y', $c['endDate']);
				//				}
				$req = new stdClass();
				$req->courseId = $c['id'];
				$req->importToCourseId = $data->courseId;
				$c['licensing'] = $this->getCourseLicensing($req);
				require_once "Subject.php";
				$s = new Subject();
				$c['subjects'] = $s->getSubjectsForImport($req);
				if (count($c['subjects']) != 0)
					$result->courses[] = $c;
			}
			unset($result->tempCourses);
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function importSubjectsToCourse($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			if($data->userRole == 5) {
				$select = $sql->select();
				$select->from('courses');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('id', $data->courseId);
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$courseOwnerData = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$courseOwnerData = $courseOwnerData->toArray();
				$data->userId = $courseOwnerData[0]["ownerId"];
			}

			if($data->userRole != 5) {
				if (!$this->userOwnsCourse($data)) {
					$result->status = 0;
					$result->message = "An non recoverable error occurred!";
					return $result;
				}
			}

			$select = $sql->select();
			$select->from('subjects');
			$where = new \Zend\Db\Sql\Where();
			if($data->userRole != 5) {
				$where->equalTo('ownerId', $data->userId);
			}
			$where->notEqualTo('courseId', $data->courseId);
			$where->in('id', $data->subjects);
			$select->where($where);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->tempSubjects = $subjects->toArray();

			if (count($result->tempSubjects) == 0) {
				$result->status = 0;
				$result->message = "An non recoverable error occurred!";
				return $result;
			}
			$result->subjects = array();
			$count = 0;
			foreach ($result->tempSubjects as $s) {
				require_once "Subject.php";
				$sub = new Subject();
				$tempS = new stdClass();
				$tempS->importToCourseId = $data->courseId;
				$tempS->subjectId = $s['id'];
				if ($sub->subjectAlreadyImported($tempS)) {
					continue;
				}
				unset($tempS->subjectId);
				unset($tempS->importToCourseId);
				$tempS->courseId = $data->courseId;
				$tempS->userId = $data->userId;
				$tempS->subjectName = $s['name'];
				$tempS->subjectDesc = $s['description'];
				$tempS->image = $s['image'];
				$tempS->parentId = $s['id'];
				$tempS->rootParentId = 0;
				if($s['rootParentId'] == 0)
					$tempS->rootParentId = $s['id'];
				else
					$tempS->rootParentId = $s['rootParentId'];
				$newSubject = $sub->createSubject($tempS);
				$count ++;
				if ($newSubject->status == 0)
					continue;
				require_once "Subject.php";
				$subject1 = new Subject();
				$subData = new stdClass();
				$subData->subjectId = $tempS->parentId;
				$subData->courseId = $tempS->courseId;
				
				$chapters = $subject1->getSubjectChapters($subData);
				foreach ($chapters->chapters as $c) {
					require_once "Chapter.php";
					$ch = new Chapter();
					$tempC = new stdClass();
					$tempC->courseId  = $data->courseId;
					$tempC->subjectId = $newSubject->subjectId;
					$tempC->chapterName = $c['name'];
					$tempC->chapterDesc = $c['description'];
					$tempC->demo = $c['demo'];
					$oldChapterId=$c['id'];
					$newchapters = $ch->createChapter($tempC);
					// $result->newchapters[] = $newchapters;
				 
					$newChapterId=$newchapters->chapterId;
					
					// import content
					$query="SELECT id FROM content WHERE chapterId=$oldChapterId ORDER BY id ASC";
					$oldContent = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$oldContent = $oldContent->toArray();

					require_once "Content.php";
					$content = new Content();
					$contentData = new stdClass();
					foreach ($oldContent as $co) {
						$contentData->icontentId = $co['id'];
						$contentData->ichapterId = $newChapterId;
						$importContent = $content->ImportContent($contentData);
					}

					// import objective exams
					$query="SELECT id FROM exams WHERE chapterId=$oldChapterId AND `delete`=0 ORDER BY id ASC";
					$oldExam = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$oldExam = $oldExam->toArray();
					
					require_once "Exam.php";
					$exam = new Exam();
					$examData = new stdClass();
					foreach ($oldExam as $e) {
						$examData->iexamId = $e['id'];
						$examData->iownerId = $data->userId;
						$examData->isubjectId = $newSubject->subjectId;
						$examData->ichapterId = $newChapterId;
						$importExam = $exam->ImportExam($examData);
					}

					// import subjective exams
					$query="SELECT id FROM exam_subjective WHERE chapterId=$oldChapterId ORDER BY id ASC";
					$oldExam = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$oldExam = $oldExam->toArray();
					
					require_once "Exam.php";
					$exam = new Exam();
					$examData = new stdClass();
					foreach ($oldExam as $e) {
						$examData->iexamId = $e['id'];
						$examData->iownerId = $data->userId;
						$examData->isubjectId = $newSubject->subjectId;
						$examData->ichapterId = $newChapterId;
						$importExam = $exam->ImportSubjectiveExam($examData);
					}

					// import submissions
					$query="SELECT id FROM submissions WHERE chapterId=$oldChapterId ORDER BY id ASC";
					$oldExam = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$oldExam = $oldExam->toArray();
					
					require_once "Exam.php";
					$exam = new Exam();
					$examData = new stdClass();
					foreach ($oldExam as $e) {
						$examData->iexamId = $e['id'];
						$examData->iownerId = $data->userId;
						$examData->isubjectId = $newSubject->subjectId;
						$examData->ichapterId = $newChapterId;
						$importExam = $exam->ImportSubmission($examData);
					}
					$result->CopyHeadings[] = $this->Copyheadings($oldChapterId,$newChapterId);
				}
				require_once "Exam.php";
				$examData = new stdClass();
				$exam = new Exam();
				$query="SELECT id FROM exams WHERE subjectId={$s['id']} AND chapterId=0 AND `delete`=0 ORDER BY id ASC";
				$indepdentExam = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$indepdentExam = $indepdentExam->toArray();
				foreach ($indepdentExam as $e) {
					$examData->iexamId = $e['id'];
					$examData->iownerId = $data->userId;
					$examData->isubjectId = $newSubject->subjectId;
					$examData->ichapterId = 0;
					$importExam = $exam->ImportExam($examData);
				}

				require_once "Exam.php";
				$examData = new stdClass();
				$exam = new Exam();
				$query="SELECT id FROM exam_subjective WHERE subjectId={$s['id']} AND (chapterId='0' OR chapterId='-1') AND `Active`=1 ORDER BY id ASC";
				$indepdentSubExam = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$indepdentSubExam = $indepdentSubExam->toArray();
				foreach ($indepdentSubExam as $e) {
					$examData->iexamId = $e['id'];
					$examData->iownerId = $data->userId;
					$examData->isubjectId = $newSubject->subjectId;
					$examData->ichapterId = 0;
					$importExam = $exam->ImportSubjectiveExam($examData);
				}

				require_once "Exam.php";
				$examData = new stdClass();
				$exam = new Exam();
				$query="SELECT id FROM submissions WHERE subjectId={$s['id']} AND (chapterId='0' OR chapterId='-1') AND `deleted`=0 ORDER BY id ASC";
				$indepdentSubExam = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$indepdentSubExam = $indepdentSubExam->toArray();
				foreach ($indepdentSubExam as $e) {
					$examData->iexamId = $e['id'];
					$examData->iownerId = $data->userId;
					$examData->isubjectId = $newSubject->subjectId;
					$examData->ichapterId = 0;
					$importExam = $exam->ImportSubmission($examData);
				}
			}
			unset($result->tempSubjects);
			$result->status = 1;
			if ($count == 0)
				$result->message = "No subjects imported, already existing subjects!";
			else
				$result->message = "{$count} subjects imported successfully!";
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function userOwnsCourse($data) {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('courses');
		$where = new \Zend\Db\Sql\Where();
		$where->equalTo('ownerId', $data->userId);
		$where->equalTo('id', $data->courseId);
		$result = new stdClass();
		$select->where($where);
		$selectString = $sql->getSqlStringForSqlObject($select);
		$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$result->courses = $courses->toArray();
		if (count($result->courses) == 0) {
			return false;
		}
		return true;
	}

	public function deleteCourse($req) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
			$replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
			if (!$this->userOwnsCourse($req)) {
				$result->status = 0;
				$result->message = "Unspecified error!";
				return $result;
			}
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('courses');
			$update->set(array('deleted' => 1));
			$update->where(array('id' => $req->courseId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$result = $statement->execute();
			
			$course = $this->getCourseAndInstituteName($req->courseId);

			$course = $course->institute;
			$courseId = $req->courseId;
			$coursename = $course[0]['course'];
			$institute = $course[0]['institute'];
			$institutemail = $course[0]['institutemail'];
			$instituteId = $course[0]['instituteId'];			
			
			// insert notification
			$notification="Course ".$this->safeString($coursename)." ( course id : $courseId ) has been deleted. Please contact admin@integro.io for restoring the course ";
			$query = "INSERT INTO notifications (userId,message,status) VALUES ($instituteId,'$notification',0)";
			$notification=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
				
			
			require_once "User.php";
			$c = new User();
		   
			
			$instituteSubject = "Course deletion Notification";
			$emailInstitute = "Dear $institute, <br> <br>";
			$emailInstitute .= "There has been a request for deletion of the Course  $coursename ( Course ID: $courseId ) on <a href=integro.io >Integro.io <a>  <br> <br>";

			$emailInstitute .= "Your Institute $institute (Institute Id: $instituteId ) has sent an enrollment Request to $coursename ( Course ID: $courseId ) on <a href=integro.io >Integro.io <a>  <br> <br>";
			$emailInstitute .= "Please write to us at <a href=admin@integro.io >admin@integro.io <a> if you wish to restore the course or if you did not initiate the request.<br> <br> "
					. "Please mention your Institute Id $instituteId, Course Name  $coursename ( Course ID: $courseId )  in the email  <br> <br>";

			$emailInstitute .= "<table border=1 ><thead><tr><th> Institute Name</th> <th>Institute ID</th> <th>Deleted Course Name</th> <th> Deleted Course Id</th></tr></thead>"; 
			$emailInstitute .= "<tbody><tr><td> $institute</td><td>$instituteId</td> <td>$coursename</td> <td> $courseId</td></tr></tbody></table>"; 

			$c->sendMail($institutemail, $instituteSubject, $emailInstitute);

			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function restoreCourse($req) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			if (!$this->userOwnsCourse($req)) {
				$result->status = 0;
				$result->message = "Unspecified error!";
				return $result;
			}
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('courses');
			$update->set(array('deleted' => 0));
			$update->where(array('id' => $req->courseId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$result = $statement->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function savecourseDoc($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			//print_r($this->userOwnsCourse($data));
			if(!$this->userOwnsCourse($data)){
				$result->status = 0;
				$result->message = "An non recoverable error occured!";
				return $result;
			}
			$adapter = $this->adapter;
			$where = array('id' => $data->courseId);
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('courses');
			$update->set(array('coursesDoc' => $data->coursesDoc));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = 'courses Document Uploaded!';
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function restoreCourseAdmin($req) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
			$replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('courses');
			$update->set(array('deleted' => 0));
			$update->where(array('id' => $req->courseId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$result = $statement->execute();
			
			$course = $this->getCourseAndInstituteName($req->courseId);
		   
			$course = $course->institute;
			$courseId = $req->courseId;
			$coursename = $course[0]['course'];
			$institute = $course[0]['institute'];
			$institutemail = $course[0]['institutemail'];
			$instituteId = $course[0]['instituteId'];
			
			// insert notification
			$notification="Course ".$this->safeString($coursename)." ( course id : $courseId ) has been restored. ";
			$query = "Insert into notifications (userId,message) values ($instituteId,'$notification')";
			$notification=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
				
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function requestCourseApproval($req) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
			$replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
			if (!$this->userOwnsCourse($req)) {
				$result->status = 0;
				$result->message = "Unspecified error!";
				return $result;
			}
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('courses');
			$update->set(array('approved' => 0));
			$update->where(array('id' => $req->courseId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$result = $statement->execute();
			$result->status = 1;
			$result->message = "Request Sent";

			$course = $this->getCourseAndInstituteName($req->courseId);
		   
			$course = $course->institute;
			$courseId = $req->courseId;
			$coursename = $course[0]['course'];
			$institute = $course[0]['institute'];
			$institutemail = $course[0]['institutemail'];
			$instituteId = $course[0]['instituteId'];
			
			
			// insert notification
			$notification="Request for Marketplace approval has been sent to the admin for the course  ".$this->safeString($coursename)." ( course id : $courseId ).";
			$query = "Insert into notifications (userId,message) values ($instituteId,'$notification')";
			$notification=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
		 
			
			require_once "User.php";
			$c = new User();
		   
			
			require_once 'StudentInvitation.php';
			$k = new StudentInvitation();

			$arcanemind = $k->getArcanemindemail();
			$arcanemind = $arcanemind->arcanemind;
			$arcanemindEmail = $arcanemind[0]['email'];
			
			$arcanemindSubject = "Add Course $coursename ( Course ID: $courseId ) to Integro Marketplace";
			$emailarcanemind = " There has been a request to add  the Course $coursename ( Course ID: $courseId ) to Integro Market Place by  Institute $institute (Institute Id : $instituteId )  . <br> <br>";
			$emailarcanemind .= "<table border=1 ><thead><tr><th> Institute Name</th> <th>Institute ID</th> <th>Course Name for market place Approval </th> <th>Course Id</th></tr></thead>"; 
			$emailarcanemind .= "<tbody><tr><td> $institute</td><td>$instituteId</td> <td>$coursename</td> <td> $courseId</td></tr></tbody></table>"; 
	   

			$instituteSubject = "Request for Market Place Addition Pending";
			$emailInstitute = "Dear $institute, <br> <br>";
			$emailInstitute .= "Thank you for your request to add Course $coursename ( Course ID: $courseId ) to Integro Market Place . <br> <br>";

			$emailInstitute .= "Your Institute $institute (Institute Id: $instituteId ) has sent an enrollment Request to $coursename ( Course ID: $courseId ) on <a href=integro.io >Integro.io <a>  <br> <br>";
			$emailInstitute .= "Our Team is analyzing the course and will soon get back to you. In case of queries please send an email to <a href=admin@integro.io >admin@integro.io <a>  mentioning your institute Id.<br> <br> ";
				   
			 
			$emailInstitute .= "<table border=1 ><thead><tr><th> Institute Name</th> <th>Institute ID</th> <th>Course Name for market place Approval </th> <th>Course Id</th></tr></thead>"; 
			$emailInstitute .= "<tbody><tr><td> $institute</td><td>$instituteId</td> <td>$coursename</td> <td> $courseId</td></tr></tbody></table>"; 
	   
			$c->sendMail($institutemail, $instituteSubject, $emailInstitute);
			 
			$c->sendMail($arcanemindEmail, $arcanemindSubject, $emailarcanemind);
			
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function updateCourseOrder($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			foreach ($data->newOrder as $courseId => $weight) {
				$update = $sql->update();
				$update->table('courses');
				$update->set(array('weight' => $weight));
				$update->where(array('id' => $courseId, 'ownerId' => $data->userId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$result = $statement->execute();
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = "Order Updated";
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getCoursesForExam($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('courses');
			if (isset($data->import) && $data->import == true)
				$selectString = "SELECT id, name from courses where ownerId={$data->userId} and id NOT IN (select newCourseId from purchase_history)";
				//$select->where(array('ownerId' => $data->userId));
			else
				$selectString = "SELECT id, name from courses where ownerId={$data->userId} and deleted = 0 and id NOT IN (select newCourseId from purchase_history)";
				/*$select->where(array('ownerId' => $data->userId,
					'deleted' => 0));*/
			/*$select->columns(array('id', 'name'));
			$selectString = $sql->getSqlStringForSqlObject($select);*/
			$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			if (count($result->courses) == 0) {
				$result->status = 0;
				$result->message = "No Courses Found";
			}
			//adding 'Assigned subjects' option for professor
			if($data->userRole == 2) {
				$len = count($result->courses);
				$result->courses[$len]['id'] = -1;
				$result->courses[$len]['name'] = 'Assigned Subjects';
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getCourseDate($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			if (isset($data->courseId)) {
				$select = $sql->select();
				$select->from('courses');
				$select->where(array('id' => $data->courseId,
					'deleted' => 0));
				$select->columns(array('liveDate', 'endDate'));
				$selectString = $sql->getSqlStringForSqlObject($select);
			} else {
				$selectString = "SELECT liveDate, endDate FROM courses WHERE id=(SELECT courseId FROM subjects WHERE id=(SELECT subjectId FROM exams WHERE id={$data->examId})) AND deleted=0;";
			}
			$dates = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$temp = $dates->toArray();
			$result->dates = $temp[0];
			if (count($result->dates) == 0) {
				$result->status = 0;
				$result->message = "No Courses Found";
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getCourseAndInstituteName($courseId) {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$query = "SELECT c.name course,l.email institutemail,l.id instituteId,
			case 
			when ur.roleId= 1 then (select name from institute_details where userId= l.id)
				when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
				when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
				end as institute
			from courses c
			JOIN login_details l on c.ownerId=l.id
			Join user_roles  ur on ur.userId=l.id where c.id=$courseId and ur.roleId not in (4,5)";
			$institute = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->institute = $institute->toArray();
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getFeaturedTemplateCourse() {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$query = "SELECT c.id,c.name,c.image,c.slug,c.studentPrice,c.studentPriceINR, COUNT(DISTINCT user_id) as studentCount FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id=c.id WHERE c.id IN (31,20,52,27) GROUP BY c.id order by liveDate Desc LIMIT 20";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			/*$query = "SELECT id,name,image,studentPrice,studentPriceINR FROM courses where deleted=0 and approved=1 and availContentMarket=1 and liveForContentMarket=1  order by liveDate Desc limit 4";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->contentMarketCourses = $courses->toArray();*/
			require_once 'Student.php';
			$s = new Student();
			$req1 = new stdClass();
			foreach ($result->courses as $key => $course) {
				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courses[$key]['image'] = getSignedURL($course['image']);
				} else {
					$result->courses[$key]['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
				}
				$result->courses[$key]['rating'] = $s->getCourseRatings($course['id']);
				$req1->courseId=$course['id'];
				$result->courses[$key]['discount']=$this->getCourseDiscount($req1);
				/*foreach ($result->contentMarketCourses as $key => $courseContentmarket) {
					require_once 'amazonRead.php';
					$result->contentMarketCourses[$key]['image'] = getSignedURL($courseContentmarket['image']);
				}*/
				$result->courses[$key]['owner'] = $this->getCourseOwner($course['id']);
				$req1->courseId=$course['id'];
				$result->courses[$key]['discount']=$this->getCourseDiscount($req1);
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getTemplateCourse($data) {
		try {
			$adapter = $this->adapter;
			$result  = new stdClass();
			$query   = "";
			$category= array(
				"title" => "All Courses",
				"desc"	=> "Check out our wide range of courses in various categories like banking, finance, accounting, information technology etc."
				);
			if (isset($data->slug) && !empty($data->slug)) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('c' => 'categories'));
				$select->where(array('c.slug' => $data->slug));
				$select->columns(array('id', 'category', 'description'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$categories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$categories = $categories->toArray();
				if (count($categories)>0) {
					$category = array(
						"title" => $categories[0]['category'],
						"desc"	=> $categories[0]['description']
						);
					$query = "SELECT c.id,c.name,c.image,c.slug,c.studentPrice,c.studentPriceINR, COUNT(DISTINCT user_id) AS studentCount FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id = c.id LEFT OUTER JOIN course_categories cc ON cc.courseId = c.id WHERE deleted =0 AND approved =1 AND availStudentMarket =1 AND liveForStudent =1 AND cc.categoryId =".$categories[0]['id']." GROUP BY c.id ORDER BY liveDate DESC";
				}
			}
			if (empty($query)) {
				/*$query="SELECT c.id,c.name,c.image,c.slug,c.studentPrice,c.studentPriceINR, COUNT(DISTINCT user_id) as studentCount
						FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id=c.id
						WHERE  c.id IN (86,92,87,77,85,84,83,57,79,78,76,42,60,34,58,59,56,52,50,45,40,38,37,35,36,31,27,28)
						AND deleted =0 AND approved =1 AND availStudentMarket =1 AND liveForStudent =1
						GROUP BY c.id 
						ORDER BY FIELD( c.id, 86, 92, 87, 77, 85, 84, 83, 57, 79, 78, 76, 42, 60, 34, 58, 59, 56, 52, 50, 45, 40, 38, 37, 35, 36, 31, 27, 28 ) LIMIT 28";
			}*/
			$query = "SELECT c.id,c.name,c.image,c.slug,c.studentPrice,c.studentPriceINR, COUNT(DISTINCT user_id) as studentCount FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id=c.id WHERE deleted=0 and approved=1 and availStudentMarket=1 and liveForStudent=1 GROUP BY c.id order by rand()";
			}
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			$result->category= $category;
			$req1 = new stdClass();
			require_once 'Student.php';
			$s = new Student();
			
			foreach ($result->courses as $key => $course) {
				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courses[$key]['image'] = getSignedURL($course['image']);
				} else {
					$result->courses[$key]['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
				}
				$result->courses[$key]['rating'] = $s->getCourseRatings($course['id']);
				$req1->courseId=$course['id'];
				$result->courses[$key]['discount']=$this->getCourseDiscount($req1);
				$result->courses[$key]['owner'] = $this->getCourseOwner($course['id']);
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getListOfCourses($data) {
		try {
			$adapter = $this->adapter;
			$result  = new stdClass();
			$query   = "";
			$category= "All Courses";
			if (empty($query)) {
				$query="SELECT c.id,c.name,c.image,c.slug,c.studentPrice,c.studentPriceINR, COUNT(DISTINCT user_id) as studentCount
						FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id=c.id
						WHERE deleted=0 and approved=1 and availStudentMarket=1 and liveForStudent=1
						GROUP BY c.id";
			}
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			$result->category= $category;
			$req1 = new stdClass();
			require_once 'Student.php';
			$s = new Student();
			
			foreach ($result->courses as $key => $course) {
				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courses[$key]['image'] = getSignedURL($course['image']);
				} else {
					$result->courses[$key]['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
				}
				$result->courses[$key]['rating'] = $s->getCourseRatings($course['id']);
				$req1->courseId=$course['id'];
				$result->courses[$key]['discount']=$this->getCourseDiscount($req1);
				$result->courses[$key]['owner'] = $this->getCourseOwner($course['id']);
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getPageTemplateCourse($req) {
		try {
			if(!empty($req->userId)) {
				$adapter = $this->adapter;
				$result = new stdClass();
				$query = "SELECT c.id,c.name,c.image,c.studentPrice,c.studentPriceINR, COUNT(DISTINCT user_id) as studentCount FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id=c.id WHERE c.ownerId=$req->userId and deleted=0 and approved=1 and availStudentMarket=1 and liveForStudent=1 GROUP BY c.id order by liveDate Desc LIMIT 28";
				$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->courses = $courses->toArray();
				require_once 'Student.php';
				$s = new Student();
				foreach ($result->courses as $key => $course) {
					if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
						require_once 'amazonRead.php';
						$result->courses[$key]['image'] = getSignedURL($course['image']);
					} else {
						$result->courses[$key]['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
					}
					$result->courses[$key]['rating'] = $s->getCourseRatings($course['id']);
					$result->courses[$key]['owner'] = $this->getCourseOwner($course['id']);
				}
				$result->status = 1;
				return $result;
			} else {
				$result->status = 1;
				$result->valid 	= false;
				return $result;
			}
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getCourseOwner($id) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('c' => 'courses'));
			$select->where(array('c.id' => $id));
			$select->columns(array('ownerId'));
			$select->join(array('ur' => 'user_roles'), 'ur.userId = c.ownerId', array('roleId'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$role = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$role = $role->toArray();
			$role = $role[0];
			$result->role = $role['roleId'];
			$select = $sql->select();
			$table = 'institute_details';
			switch($result->role) {
				case 1:
					$table = 'institute_details';
					break;
				case 2:
					$table = 'professor_details';
					break;
				case 3:
					$table = 'publisher_details';
					break;
			}
			$select->from($table);
			$select->where(array('userId' => $role['ownerId']));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$detail = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$detail = $detail->toArray();
			$detail = $detail[0];
			if ($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
				require_once 'amazonRead.php';
				$detail['profilePic'] = getSignedURL($detail['profilePic']);
			} else {
				$detail['profilePic'] = $this->sitePath.'manage/assets/pages/media/profile/profile_user.jpg';
			}
			$result->detail = $detail;
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getContentTemplateCourse() {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$query = "SELECT id,name,image,studentPrice,studentPriceINR FROM courses where deleted=0 and approved=1 and availContentMarket=1 and liveForContentMarket=1  order by liveDate Desc";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->contentMarketCourses = $courses->toArray();
			if ($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
				foreach ($result->contentMarketCourses as $key => $courseContentmarket) {
					require_once 'amazonRead.php';
					$result->contentMarketCourses[$key]['image'] = getSignedURL($courseContentmarket['image']);
				}
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	///@ayush courses for packages page
	public function getCoursesByOwnerId($data) {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$query = "SELECT c.id, c.name, c.studentPrice, c.studentPriceINR FROM courses c  WHERE deleted =0 AND /*approved =1 AND liveForStudent =1*/  AND ownerId = $data->userId  GROUP BY c.id ORDER BY liveDate DESC";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getCoursesByOwnerIdADMIN($data) {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$query = "SELECT c.id, c.name, c.studentPrice, c.studentPriceINR FROM courses c  WHERE deleted =0 AND approved =1 AND liveForStudent =1  GROUP BY c.id ORDER BY liveDate DESC";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	
	//
	public function getEditcoursesByowner($data) {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			if($data->userRole == 5){
			$query = "SELECT c.id, c.name, c.studentPrice, c.studentPriceINR FROM courses c WHERE deleted =0 AND approved =1 AND liveForStudent =1 AND  c.id NOT IN ( SELECT courseId FROM package_courses WHERE packId = $data->packId )
					  GROUP BY c.id ORDER BY liveDate DESC LIMIT 0, 30";	
			}
			elseif($data->userRole == 1 || $data->userRole == 2){
				$query = "SELECT c.id, c.name, c.studentPrice, c.studentPriceINR FROM courses c WHERE deleted =0 AND approved =1 AND liveForStudent =1 AND ownerId = $data->userId AND c.id NOT IN ( SELECT courseId FROM package_courses WHERE packId = $data->packId )
					  GROUP BY c.id ORDER BY liveDate DESC LIMIT 0, 30";
			}
			else{
				//not now
			}
			
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	///@ayush courses for packages page
	public function getStudentforPackage($data) {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			/*$query="SELECT l.id, concat( sd.firstName, ' ', sd.lastName, '   ', l.email ) AS name
					FROM login_details l
					INNER JOIN student_details sd ON sd.userId = l.id
					WHERE l.active =1 AND l.validated = 1 AND l.temp = 0
					ORDER BY l.id DESC";*/
			$query="SELECT l.id, concat( sd.firstName, ' ', sd.lastName, '   ', l.email ) AS name
					FROM student_course sc
					INNER JOIN courses c ON c.id=sc.course_id AND c.ownerId={$data->userId}
					INNER JOIN login_details l ON l.id=sc.user_id
					INNER JOIN student_details sd ON sd.userId = l.id
					WHERE l.active =1 AND l.validated = 1 AND l.temp = 0
					ORDER BY l.id DESC";
			$students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->students = $students->toArray();
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	//for packages student when edit 
	public function getEditStudentforPackage($data) {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			/*$query="SELECT l.id, concat( sd.firstName, ' ', sd.lastName, '   ', l.email ) AS name
					FROM login_details l
					INNER JOIN student_details sd ON sd.userId = l.id
					WHERE l.active =1 AND l.validated =1 AND l.temp = 0 AND l.id NOT IN ( SELECT userId FROM package_user WHERE packId = $data->packId )
					ORDER BY l.id DESC";*/
			$query="SELECT l.id, concat( sd.firstName, ' ', sd.lastName, '   ', l.email ) AS name
					FROM student_course sc
					INNER JOIN courses c ON c.id=sc.course_id AND c.ownerId={$data->userId}
					INNER JOIN login_details l ON l.id=sc.user_id
					INNER JOIN student_details sd ON sd.userId = l.id
					WHERE l.active =1 AND l.validated =1 AND l.temp = 0
					ORDER BY l.id DESC";
			$students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->students = $students->toArray();
			$query="SELECT l.id, concat( sd.firstName, ' ', sd.lastName, '   ', l.email ) AS name
					FROM student_course sc
					INNER JOIN courses c ON c.id=sc.course_id AND c.ownerId={$data->userId}
					INNER JOIN login_details l ON l.id=sc.user_id
					INNER JOIN student_details sd ON sd.userId = l.id
					WHERE l.active =1 AND l.validated =1 AND l.temp = 0 AND l.id IN ( SELECT userId FROM package_user WHERE packId = $data->packId )
					ORDER BY l.id DESC";
			$pstudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->pstudents = $pstudents->toArray();
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	///@ayush entranceExam
	public function getStudentEntranceExamTemplateCourse() {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR, COUNT( DISTINCT user_id ) AS studentCount FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id = c.id LEFT OUTER JOIN course_categories cc ON cc.courseId = c.id WHERE deleted =0 AND approved =1 AND availStudentMarket =1 AND liveForStudent =1 AND cc.categoryId =1 GROUP BY c.id ORDER BY liveDate DESC";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			require_once 'Student.php';
			$s = new Student();
			$req1 = new stdClass();
			foreach ($result->courses as $key => $course) {
				if ($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courses[$key]['image'] = getSignedURL($course['image']);
				} else {
					$result->courses[$key]['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
				}
				$result->courses[$key]['owner'] = $this->getCourseOwner($course['id']);
				$result->courses[$key]['rating'] = $s->getCourseRatings($course['id']);
				$req1->courseId=$course['id'];
				$result->courses[$key]['discount']=$this->getCourseDiscount($req1);
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	///@ayush free course
	public function getStudentFreeCourse() {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR, COUNT( DISTINCT user_id ) AS studentCount FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id = c.id WHERE deleted =0 AND approved =1 AND availStudentMarket =1 AND liveForStudent =1 AND studentPriceINR =0 AND studentPrice =0 GROUP BY c.id ORDER BY liveDate DESC";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			require_once 'Student.php';
			$s = new Student();
			$req1 = new stdClass();
			foreach ($result->courses as $key => $course) {
				if ($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courses[$key]['image'] = getSignedURL($course['image']);
				} else {
					$result->courses[$key]['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
				}
				$result->courses[$key]['owner'] = $this->getCourseOwner($course['id']);
				$result->courses[$key]['rating'] = $s->getCourseRatings($course['id']);
				$req1->courseId=$course['id'];
				$result->courses[$key]['discount']=$this->getCourseDiscount($req1);
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	//@ ayush fo getStudentEntranceExamTemplateCourse
	public function getStudentschoolPrepTemplateCourse() {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR, COUNT( DISTINCT user_id ) AS studentCount FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id = c.id LEFT OUTER JOIN course_categories cc ON cc.courseId = c.id WHERE deleted =0 AND approved =1 AND availStudentMarket =1 AND liveForStudent =1 AND cc.categoryId =2 GROUP BY c.id ORDER BY liveDate DESC";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			require_once 'Student.php';
			$s = new Student();
			$req1 = new stdClass();
			foreach ($result->courses as $key => $course) {
				if ($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courses[$key]['image'] = getSignedURL($course['image']);
				} else {
					$result->courses[$key]['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
				}
				$result->courses[$key]['owner'] = $this->getCourseOwner($course['id']);
				$result->courses[$key]['rating'] = $s->getCourseRatings($course['id']);
				$req1->courseId=$course['id'];
				$result->courses[$key]['discount']=$this->getCourseDiscount($req1);
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	//@ayush getstudentHobbies_skills	
	public function getstudentHobbies_skills() {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR, COUNT( DISTINCT user_id ) AS studentCount FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id = c.id LEFT OUTER JOIN course_categories cc ON cc.courseId = c.id WHERE deleted =0 AND approved =1 AND availStudentMarket =1 AND liveForStudent =1 AND cc.categoryId =3 GROUP BY c.id ORDER BY liveDate DESC";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			require_once 'Student.php';
			$s = new Student();
			$req1 = new stdClass();
			foreach ($result->courses as $key => $course) {
				if ($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courses[$key]['image'] = getSignedURL($course['image']);
				} else {
					$result->courses[$key]['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
				}
				$result->courses[$key]['owner'] = $this->getCourseOwner($course['id']);
				$result->courses[$key]['rating'] = $s->getCourseRatings($course['id']);
				$req1->courseId=$course['id'];
				$result->courses[$key]['discount']=$this->getCourseDiscount($req1);
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	
	//@ayush getstudentHobbies_skills	
	public function getstudentBusiness_Management() {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR, COUNT( DISTINCT user_id ) AS studentCount FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id = c.id LEFT OUTER JOIN course_categories cc ON cc.courseId = c.id WHERE deleted =0 AND approved =1 AND availStudentMarket =1 AND liveForStudent =1 AND cc.categoryId =4 GROUP BY c.id ORDER BY liveDate DESC";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			require_once 'Student.php';
			$s = new Student();
			$req1 = new stdClass();
			foreach ($result->courses as $key => $course) {
				if ($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courses[$key]['image'] = getSignedURL($course['image']);
				} else {
					$result->courses[$key]['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
				}
				$result->courses[$key]['owner'] = $this->getCourseOwner($course['id']);
				$result->courses[$key]['rating'] = $s->getCourseRatings($course['id']);
				$req1->courseId=$course['id'];
				$result->courses[$key]['discount']=$this->getCourseDiscount($req1);
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getStudentIT_Software() {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR, COUNT( DISTINCT user_id ) AS studentCount FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id = c.id LEFT OUTER JOIN course_categories cc ON cc.courseId = c.id WHERE deleted =0 AND approved =1 AND availStudentMarket =1 AND liveForStudent =1 AND cc.categoryId =5 GROUP BY c.id ORDER BY liveDate DESC";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			require_once 'Student.php';
			$s = new Student();
			$req1 = new stdClass();
			foreach ($result->courses as $key => $course) {
				if ($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courses[$key]['image'] = getSignedURL($course['image']);
				} else {
					$result->courses[$key]['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
				}
				$result->courses[$key]['owner'] = $this->getCourseOwner($course['id']);
				$result->courses[$key]['rating'] = $s->getCourseRatings($course['id']);
				$req1->courseId=$course['id'];
				$result->courses[$key]['discount']=$this->getCourseDiscount($req1);
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	//@ayush getstudentEntrepreneurship	
	public function getstudentEntrepreneurship() {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR, COUNT( DISTINCT user_id ) AS studentCount FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id = c.id LEFT OUTER JOIN course_categories cc ON cc.courseId = c.id WHERE deleted =0 AND approved =1 AND availStudentMarket =1 AND liveForStudent =1 AND cc.categoryId =6 GROUP BY c.id ORDER BY liveDate DESC";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			require_once 'Student.php';
			$s = new Student();
			$req1 = new stdClass();
			foreach ($result->courses as $key => $course) {
				if ($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courses[$key]['image'] = getSignedURL($course['image']);
				} else {
					$result->courses[$key]['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
				}
				$result->courses[$key]['owner'] = $this->getCourseOwner($course['id']);
				$result->courses[$key]['rating'] = $s->getCourseRatings($course['id']);
				$req1->courseId=$course['id'];
				$result->courses[$key]['discount']=$this->getCourseDiscount($req1);
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}	
	
	//@ayush getstudentMiscellaneous	
	public function getstudentMiscellaneous() {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR, COUNT( DISTINCT user_id ) AS studentCount FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id = c.id LEFT OUTER JOIN course_categories cc ON cc.courseId = c.id WHERE deleted =0 AND approved =1 AND availStudentMarket =1 AND liveForStudent =1 AND cc.categoryId =7 GROUP BY c.id ORDER BY liveDate DESC";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			require_once 'Student.php';
			$s = new Student();
			$req1 = new stdClass();
			foreach ($result->courses as $key => $course) {
				if ($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courses[$key]['image'] = getSignedURL($course['image']);
				} else {
					$result->courses[$key]['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
				}
				$result->courses[$key]['owner'] = $this->getCourseOwner($course['id']);
				$result->courses[$key]['rating'] = $s->getCourseRatings($course['id']);
				$req1->courseId=$course['id'];
				$result->courses[$key]['discount']=$this->getCourseDiscount($req1);
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	///change
	public function getStudentTemplateCourse() {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR, COUNT(DISTINCT user_id) AS studentCount FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id=c.id WHERE deleted=0 AND approved=1 AND availStudentMarket=1 AND liveForStudent=1 GROUP BY c.id ORDER BY liveDate DESC";
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			require_once 'Student.php';
			$s = new Student();
			$req1 = new stdClass();
			foreach ($result->courses as $key => $course) {
				if ($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courses[$key]['image'] = getSignedURL($course['image']);
				} else {
					$result->courses[$key]['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
				}
				$result->courses[$key]['owner'] = $this->getCourseOwner($course['id']);
				$result->courses[$key]['rating'] = $s->getCourseRatings($course['id']);
				$req1->courseId=$course['id'];
				$result->courses[$key]['discount']=$this->getCourseDiscount($req1);
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getCourseDetailsStudentEnd($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			//checking if the course is already associated with the student
			if($data->userRole == 4) {
				//check if student have purchased the course by himself
				$select = $sql->select();
				$select->from('student_course');
				$select->where(array(
					'course_id'	=>	$data->courseId,
					'user_id'	=>	$data->userId
				));
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				if(count($temp) != 0) {
					$result->status = 0;
					$result->message = 'You have already purchased this course.';
					return $result;
				}
				//now checking pending invitations for the same course
				$selectString = "SELECT id FROM invited_student_details WHERE course_id={$data->courseId} AND email_id=(SELECT email FROM login_details WHERE id={$data->userId})";
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				if(count($temp) != 0) {
					$result->status = 0;
					$result->message = 'You have already been invited for this course. Please check your notifications.';
					return $result;
				}
			}
			
			
			$select = $sql->select();
			$select->from('courses');
			$select->where(array('id' => $data->courseId,
				'deleted' => 0));
			$select->columns(array('id','name','subtitle','ownerId','studentPrice','studentPriceINR','availStudentMarket','availContentMarket','licensingLevel','liveDate', 'endDate','demo','liveForStudent','liveForContentMarket'));
			$statement = $sql->prepareStatementForSqlObject($select);
			$courseDetails = $statement->execute();
			if ($courseDetails->count() !== 0) {
				$temp = $courseDetails->next();
				$result->courseDetails = $temp;
			} else {
				$result->status = 0;
				$result->exception = "Course not found";
				return $result;
			}
			
			// $select = $sql->select();
			// $select->from('course_categories');
			// $select->where(array('courseId' => $data->courseId));
			// $selectString = $sql->getSqlStringForSqlObject($select);
			// $query="SELECT categoryId,category FROM course_categories ct JOIN categories c on ct.categoryId=c.id where courseId=$data->courseId";
			// $courseCategories = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			// $result->courseCategories = $courseCategories->toArray();


			//SELECT category FROM course_categories ct  JOIN categories c on ct.categoryId=c.id where courseId=31       
			
			$select = $sql->select();
			$select->from('course_licensing');
			$select->where(array('courseId' => $data->courseId));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courseLicensing = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->courseLicensing = $courseLicensing->toArray();
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function getCourseDetailsInstituteEnd($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			//self created courses are already handled before this page
			//checking if the course is already purchased by the same institute
			//SELECT p.id FROM `purchase_history` p JOIN courses c on c.id=p.newCourseId WHERE parentCourseId=198 AND p.LicensingLevel=1 AND c.ownerId=1
			$select = $sql->select();
			$select->from(array('p'	=>	'purchase_history'));
			$select->where(array(
				'p.parentCourseId'	=>	$data->courseId,
				'p.LicensingLevel'	=>	$data->licensingLevel,
				'c.ownerId'			=>	$data->userId
			));
			$select->columns(array('id'	=>	'id'));
			$select->join(array('c'	=>	'courses'), 
				'p.newCourseId = c.id', 
				array());
			$selectString = $sql->getSqlStringForSqlObject($select);
			$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$temp = $temp->toArray();
			if(count($temp) > 0) {
				$result->status = 0;
				$result->message = 'You have already purchased this course.';
				return $result;
			}
			//now fetching the details of required course
			$select = $sql->select();
			$select->from('courses');
			$select->where(array('id' => $data->courseId,
				'deleted' => 0));
			$select->columns(array('id', 'name', 'subtitle', 'ownerId', 'studentPrice', 'studentPriceINR', 'liveDate', 'endDate'));
			$statement = $sql->prepareStatementForSqlObject($select);
			$courseDetails = $statement->execute();
			if ($courseDetails->count() !== 0) {
				$temp = $courseDetails->next();
				$result->courseDetails = $temp;
			} else {
				$result->status = 0;
				$result->exception = "Course not found";
				return $result;
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function CopyCourseandPurchase($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			// copy course on purchasing by institute. 
			$time = time() * 1000;
			if($data->LicensingLevel == 1)
				$query="INSERT into courses (name,subtitle,ownerId,description,targetAudience,image, liveDate, endDate) select name, subtitle, {$data->userId}, description, targetAudience, image, '{$time}', '".($time + (365 * 24 * 60 * 60 * 1000)) ."' from courses where id=$data->courseId";
			else
				$query="INSERT into courses (name,subtitle,ownerId,description,targetAudience,image, liveDate, endDate) select name, subtitle, {$data->userId}, description, targetAudience, image, '{$time}', '' from courses where id=$data->courseId";
	   
			$copyCourse = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			
			// selecting new courseId of purchased course
			$query="SELECT max(id) newCourseId from courses";
			$newCourseId = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$newCourseId=$newCourseId->toArray();
			$newCourseId=$newCourseId[0]['newCourseId'];
			
			// insert into purchase history
			$query="INSERT INTO purchase_history (parentCourseId,newCourseId,LicensingLevel) VALUES ($data->courseId, $newCourseId, $data->LicensingLevel)";
			$insertLicenseLevel = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			
			
			// copy subjects of old course to new purchased course
			$query="INSERT INTO subjects (ownerId,courseId,name,description,image)
					select $data->userId,$newCourseId,name,description,image from subjects
					where courseId=$data->courseId and deleted=0 order by id asc";
			$copySubjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			
			  // select old subjects of old course
			$query="SELECT id OldSubjectId from subjects where courseId=$data->courseId and deleted=0 order by id asc";
			$OldSubjectId = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$OldSubjectId=$OldSubjectId->toArray();
			$result->OldSubjectId=$OldSubjectId;
			//select new subjects id
			$query="SELECT id newSubjectId from subjects where courseId=$newCourseId and deleted=0 order by id asc";
			$newSubjectId = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$newSubjectId=$newSubjectId->toArray();
		 
			// print_r($newSubjectId);
			// $result->newSubjectId=$newSubjectId;

			//select new subjects id
			foreach ($OldSubjectId as $key =>$oldId ){
				$query="INSERT into chapters(subjectId,name,description)"
					   . " select {$newSubjectId[$key]['newSubjectId']},name,description from chapters where subjectId={$OldSubjectId[$key]['OldSubjectId']} and  deleted=0 order by id asc ";
				$insertNewChapters = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				//exit(0);
				$query="SELECT id oldChapterId from chapters where subjectId={$OldSubjectId[$key]['OldSubjectId']} and deleted=0 order by id asc  ";
				$oldChapterId = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$oldChapterId=$oldChapterId->toArray();
				//  $result->oldChapterId[$key]=$oldChapterId;

				$query="SELECT id newChapterId from chapters where subjectId={$newSubjectId[$key]['newSubjectId']} and deleted=0 order by id asc ";
				$newChapterId = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$newChapterId=$newChapterId->toArray();
				//  $result->newChapterId[$key]=$newChapterId;

				//  new code to look
				foreach ($oldChapterId as $chapterkey =>$chapter) {
					$query="SELECT id from exams where chapterId={$chapter['oldChapterId']} and `delete`=0 order by id asc " ;
					$oldExam = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$oldExam = $oldExam->toArray();

					require_once "Exam.php";
					$exam = new Exam();
					$examData = new stdClass();
					foreach ($oldExam as $e) {
						$examData->iexamId = $e['id'];
						$examData->iownerId = $data->userId;
						$examData->isubjectId = $newSubjectId[$key]['newSubjectId'];
						$examData->ichapterId =$newChapterId[$chapterkey]['newChapterId'] ;
						$result->importExam[] = $exam->ImportExam($examData);
					}
					$result->CopyHeadings[] = $this->Copyheadings($chapter['oldChapterId'],$newChapterId[$chapterkey]['newChapterId']) ; 
				}
				$query="SELECT id from exams where subjectId={$OldSubjectId[$key]['OldSubjectId']} and chapterId=0 and `delete`=0 order by id asc " ;
				$indepdentExam = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$indepdentExam = $indepdentExam->toArray();
				foreach ($indepdentExam as $e) {
					$examData->iexamId = $e['id'];
					$examData->iownerId = $data->userId;
					$examData->isubjectId =$newSubjectId[$key]['newSubjectId'];
					$examData->ichapterId =0;
					$importExam = $exam->ImportExam($examData);
				}
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
   
	public function Copyheadings($oldChapterId,$newChapterId) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		$adapter = $this->adapter;
		try {
			$query="INSERT INTO headings(title,chapterId,weight)"
					   . "select title,{$newChapterId},weight from headings where chapterId={$oldChapterId}  order by id asc ";
			$insertHeadings = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			// code for copying content
			$query="SELECT id oldHeadingid from headings where chapterId={$oldChapterId} order by id asc  ";
			$oldHeadingid = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$oldHeadingid=$oldHeadingid->toArray();
			// $result->oldHeadingid[$key]=$oldHeadingid;
	
			$query="SELECT id newHeadingid from headings where chapterId={$newChapterId} order by id asc  ";
			$newHeadingId = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$newHeadingId=$newHeadingId->toArray();

			foreach ($oldHeadingid as $headingkey =>$heading ){ 
				$result->content=  $this->CopyContent($newHeadingId[$headingkey]['newHeadingid'],$heading['oldHeadingid'],$newChapterId);
			}
			$result->status = 1;
			return $result;
		}
		catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function CopyContent($newHeadingId,$oldHeadingId,$newChapterId) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$query="INSERT INTO content(headingId,chapterId,title,weight)"
			   . "select $newHeadingId,$newChapterId,title,weight from content where headingId=$oldHeadingId  order by id asc ";
			$insertContent= $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			// code for copying content
			$query="SELECT id oldContentId from content where headingId=$oldHeadingId order by id asc  ";
			$oldContentId = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$oldContentId=$oldContentId->toArray();
			// $result->oldHeadingid[$key]=$oldHeadingid;
	
			$query="SELECT id newContentId from content where headingId=$newHeadingId order by id asc  ";
			$newContentId = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$newContentId=$newContentId->toArray();

			foreach ($oldContentId as $contentkey =>$content){ 
				$result->copyStuff=  $this->CopyFilesStuff($content['oldContentId'],$newContentId[$contentkey]['newContentId']) ;
			}
			$result->status = 1;
			return $result;   
		}
		catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function CopyFilesStuff($oldContentId,$newContentId) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$query = "INSERT INTO files(contentId,type,stuff,fileRealName,description,metadata)"
					. "select $newContentId,type,stuff,fileRealName,description,metadata from files where contentId=$oldContentId  order by id asc ";
			$insertFiles = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$query = "INSERT INTO supplementary_files(contentId,fileRealName)"
					. "select $newContentId,fileRealName from supplementary_files where contentId=$oldContentId  order by id asc ";
			$insertFiles = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();

			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function fetchPurchasedCoursesDetails($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$query= "SELECT DISTINCT p.id,parentCourseId,newCourseId,p.LicensingLevel,DATE_FORMAT(p.purchasedAt,'%d %b %Y')purchasedAt, c.name,c.ownerId,cl.priceUSD,cl.priceINR FROM purchase_history p JOIN courses c 
			 on p.newCourseId=c.id  JOIN course_licensing cl on cl.courseId=p.parentCourseId and cl.licensingType=p.LicensingLevel
			 where  c.ownerId=$data->userId";
			$purchasedCourses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->purchasedCourses=$purchasedCourses->toArray();
			foreach ($result->purchasedCourses as $key =>$course ){
				$query="SELECT "
				   . " case when ur.roleId= 1 then (select name from institute_details where userId= l.id) "
				   . " when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname) from professor_details where userId= l.id)"
				   . " when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname) from publisher_details where userId= l.id) "
				   . " end as institute FROM courses c JOIN login_details l on l.id=c.ownerId "
				   . " JOIN user_roles ur on ur.userId=l.id where ur.roleId NOT IN (4,5) and c.id=1"  ;
			
				$institute = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$institute=$institute->toArray();    
				$result->purchasedCourses[$key]['ownerName']=  $institute[0]['institute'] ;
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	/*public function searchCourse($data) {
		$result = new stdClass();
		try {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('courses');
		$select->where(array('approved' => 1,'deleted' => 0,'liveForStudent' => 1));
		$select->where("name LIKE ?", "%{$data->text}%");
		$selectString = $sql->getSqlStringForSqlObject($select);
			$names = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$names = $names->toArray();
			$result->names = $names;
			
			$select = $sql->select();
			$select->from('courses');
			$where = new \Zend\Db\Sql\Where();
			$select->columns(array(
				'tags'	=>	new \Zend\Db\Sql\Expression("DISTINCT tags")
			));
			$where
			->like('tags', '%'.$data->text.'%')
			->and
			->equalTo('deleted', 0)
			->and
			->equalTo('approved', 1)
			->and
			->equalTo('liveForStudent', 2);
			$select->where($where);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$tags = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$tags = $tags->toArray();
			$breakTags = array();
			if(count($tags) > 0) {
				$data->text = trim(strtolower($data->text));
				foreach($tags as $tag) {
					$tempbreak = explode(',', $tag['tags']);
					foreach($tempbreak as $temp) {
						$temp = strtolower(trim($temp));
						similar_text($temp, $data->text, $r);
						if($r > 60)
							$breakTags[] = $temp;
					}
				}
			}
			$result->tags = $breakTags;
			$result->status = 1;
			return $result;
			
			
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}*/	
	
	public function fetchprofessorSearch($data) {
			
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$query= "SELECT DISTINCT p.userId FROM professor_details p where  p.firstName LIKE  '%".$data."%' or p.lastName LIKE  '%".$data."%'" ;
			$professor_details = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$professor_details=$professor_details->toArray();
			return $professor_details;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	
	public function fetchInstituteSearch($data) {
			
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$query= "SELECT DISTINCT p.userId FROM institute_details p where  p.name LIKE  '%".$data."%' " ;
			$professor_details = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$professor_details=$professor_details->toArray();
			return $professor_details;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	public function searchCourseForPage($data) {
	
		$newt=$data->text;
		//str_replace(" ","+",$newt);
		$keywordsToken=explode(" ",$newt);
		//$keywordsToken=explode(" ",$newt);
		//print_r($data);
		$profdetail=$this->fetchprofessorSearch($keywordsToken[0]);
		$intituedetail=$this->fetchInstituteSearch($keywordsToken[0]);
		
		$result = new stdClass();
		try {
			$priceFilter = array(
				///array('min'	=>	0, 'max'	=>	0),
				array('min'	=>	0, 'max'	=>	0),
				array('min'	=>	1, 'max'	=>	10),
				array('min'	=>	11, 'max'	=>	20),
				array('min'	=>	21, 'max'	=>	30),
				array('min'	=>	31, 'max'	=>	40),
				array('min'	=>	41, 'max'	=>	50),
				array('min'	=>	51, 'max'	=>	60),
				array('min'	=>	61, 'max'	=>	100)
			);

			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$where = new \Zend\Db\Sql\Where();
			$cat = explode(',', $data->cat);
			if($cat[0] != '') {
				$select->from(array('cc'	=>	'course_categories'))
				->join(array('c'	=>	'courses'),
					'cc.courseId = c.id',
					array(
						'id'				=>	new \Zend\Db\Sql\Expression("DISTINCT c.id"),
						'name'				=>	'name',
						'ownerId'			=>	'ownerId',
						'liveDate'			=>	'liveDate',
						'endDate'			=>	'endDate',
						'image'				=>	'image',
						'studentPrice'		=>	'studentPrice',
						'studentPriceINR'	=>	'studentPriceINR'
					)
				);
				$where->in('categoryId', $cat);
				$select->columns(array());
			}
			else {
				$select->from(array('c'	=>	'courses'));
				$select->columns(array(
						'id'				=>	new \Zend\Db\Sql\Expression("DISTINCT c.id"),
						'name'				=>	'name',
						'ownerId'			=>	'ownerId',
						'liveDate'			=>	'liveDate',
						'endDate'			=>	'endDate',
						'image'				=>	'image',
						'slug'				=>	'slug',
						'studentPrice'		=>	'studentPrice',
						'studentPriceINR'	=>	'studentPriceINR'
					));
			}
			$where->equalTo('c.deleted', 0);
			$where->equalTo('c.liveForStudent', 1);
			$where->equalTo('c.approved', 1);
			/*$where->nest->like('c.name', '%'.$keywordsToken[0].'%')->or->like('c.subtitle', '%'.$keywordsToken[0].'%')->or->like('c.subtitle', '%'.$keywordsToken[0].'%')->or->like('c.tags', '%'.$keywordsToken[0].'%')->not->like('c.subtitle', '%'.''.'%')->not->like('c.name', '%'.''.'%')->not->like('c.tags', '%'.''.'%')->unnest;*/
			
			//$where->nest->like('c.description', '%'.$keywordsToken[0].'%')->unnest;
			
			
			$prices = explode(',', $data->price);
			if($prices[0] != '') {
				$nest = $where->and->nest();
				
				foreach($prices as $price) {
					$price=$price-1;
					$nest->between('c.studentPrice', $priceFilter[$price]['min'], $priceFilter[$price]['max'])->or;
				}
				//$where->unest;
			}
			//$where->and->nest->expression()->unnest;
			//$where->nest->like('c.description', '%'.$keywordsToken[0].'%')->unnest;
				
			$select->where($where);
			
			/*$select->where(array( new \Zend\Db\Sql\Predicate\Expression("MATCH(c.description) AGAINST ('$data->text')")
			 ));
				$in="";
					if(count($profdetail)>0){
					$in = join(',', array_fill(0, count($profdetail), '?'));
				
				}
				//print_r($in);
				
			$select->where(array(
			    new \Zend\Db\Sql\Predicate\Expression("MATCH(c.name,c.subtitle) AGAINST ('$data->text') ")
			));
			*/
		
			if(count($profdetail)==0 && count($intituedetail)==0 ){
				//$prid=$profdetail[0]['userId'];
				
				$select->where(array(
					new \Zend\Db\Sql\Predicate\Expression("MATCH(c.name,c.subtitle) AGAINST ('$data->text')")
				));
			}
			if(count($profdetail)>0 && count($intituedetail)==0 ){
				$prid=$profdetail[0]['userId'];
				$select->where(array(
					new \Zend\Db\Sql\Predicate\Expression("(MATCH(c.name,c.subtitle) AGAINST ('$data->text') OR c.ownerId ='$prid')")
				));
			}
			if(count($profdetail) == 0 && count($intituedetail)>0 ){
				$qrid=$intituedetail[0]['userId'];
				$select->where(array(
					new \Zend\Db\Sql\Predicate\Expression("(MATCH(c.name,c.subtitle) AGAINST ('$data->text') OR c.ownerId ='$qrid')")
				));
			}
			if(count($profdetail)>0 && count($intituedetail)>0 ){
				$prid=$profdetail[0]['userId'];
				$qrid=$intituedetail[0]['userId'];
				$select->where(array(
					new \Zend\Db\Sql\Predicate\Expression("(MATCH(c.name,c.subtitle) AGAINST ('$data->text') OR c.ownerId ='$prid' OR c.ownerId ='$qrid')")
				));
			}
		
			$select->join(
				array('i'	=>	'institute_details'),
				'i.userId = c.ownerId',
				array('owner1' => 'name'), $select::JOIN_LEFT
			)
			->join(
				array('p'	=>	'professor_details'),
				'p.userId = c.ownerId',
				array('owner' => 'firstName','lastName' =>'lastName'), $select::JOIN_LEFT
			);//,lastName' =>'lastName'
			//$select->order(array('c.name asc','subtitle asc','c.description desc '));
			$selectString = $sql->getSqlStringForSqlObject($select);
			//print_r($selectString);
			$names = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$names = $names->toArray();
			foreach($names as $key=>$course) {
				if($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$names[$key]['image'] = getSignedURL($course['image']);
				}
				$select = $sql->select();
				$select->from('subjects');
				$select->where(array(
					'courseId'	=>	$course['id'],
					'deleted'	=>	0
				));
				$select->columns(array('name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subs = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subs = $subs->toArray();
				$names[$key]['subjects'] = $subs;
				//adding rating with course details
				require_once 'Student.php';
				$s = new Student();
				$names[$key]['rating'] = $s->getCourseRatings($course['id']);
			}
			$result->courses = $names;
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	public function updateOpenedCourses($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('student_opened_courses');
			$select->where(array(
				'studentId'	=>	$data->userId,
				'courseId'	=>	$data->courseId,
				'date'		=>	date('Y-m-d', time())
			));
			$select->columns(array('date'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$temp = $temp->toArray();
			if(count($temp) == 0) {
				//now adding the value
				$temp = new TableGateway('student_opened_courses', $adapter, null, new HydratingResultSet());
				$insert = array(
					'studentId'	=>	$data->userId,
					'courseId'	=>	$data->courseId,
					'date'		=>	date('Y-m-d', time())
				);
				$temp->insert($insert);
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	public function getCourseExpiry($data) {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('courses')->columns(array('endDate'))->where(array('id'=>$data->courseId));
		$selectString = $sql->getSqlStringForSqlObject($select);
		$expiry = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$expiry = $expiry->toArray();
		if(count($expiry) > 0)
			$expiry = $expiry[0]['endDate'];
		else
			$expiry = '';
		return $expiry;
	}

	public function getCourseForMeta($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			if (isset($data->slug) && !empty($data->slug)) {
				$select->from('courses')->where(array('slug' => $data->slug))->columns(array('name', 'subtitle', 'description', 'image','socialImage'));
			} else {
				$select->from('courses')->where(array('id' => $data->courseId))->columns(array('name', 'subtitle', 'description', 'image','socialImage'));
			}
			$selectString = $sql->getSqlStringForSqlObject($select);
			$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$details = $details->toArray();
			if(count($details) > 0) {
				$result->details = $details[0];
				$ogImage = $result->details['socialImage'];
				if (empty($ogImage)) {
					$ogImage = $result->details['image'];
					$ogPath = explode("/", $ogImage);
					$result->details['ogimage'] = $this->ogPath.end($ogPath);
				} else {
					$result->details['ogimage'] = $ogImage;
				}
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$result->details['image'] = getSignedURL($result->details['image']);
				}
				$result->status = 1;
				return $result;
			}
			$result->status = 0;
			$result->message = 'No such course found';
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	/**
	* Function to save demo video in database
	* @author Rupesh Pandey
	* @date 25/05/2015
	* @param JSON with all required details
	* @return JSON with status code
	*/
	public function saveDemoVideoForCourse($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('courses')->set(array('demoVideo' => $data->demoFile))->where(array('id' => $data->courseId));
			$string = $sql->getSqlStringForSqlObject($update);
			$adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			//deleting previous file
			/*if($data->previousVideo != 0) {
				require_once 'amazonDelete.php';
				deleteFile(substr($data->previousVideo, 37));
			}*/
			//echo substr($data->previousVideo, 37);
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	/**
	* Function to delete demo video i.e. changing the column value to blank
	*
	* @author Rupesh Pandey
	* @date 26/05/2015
	* @param JSON with courseId
	* @return JSON with status code
	*/
	public function deleteDemoVideo($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('courses')->set(array('demoVideo' => ''))->where(array('id' => $data->courseId));
			$string = $sql->getSqlStringForSqlObject($update);
			$adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();

			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function updateDurationsForVideos() {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			require_once 'amazonRead.php';
			$dura = array();
			$query="SELECT id, stuff FROM files WHERE type='video' AND duration=0";
			$videosList = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$videos = $videosList->toArray();
			foreach ($videos as $key => $video) {
				//$file="http://dmsd6g597imqb.cloudfront.net/marketing-videos/demo-27-1435147097.mp4";
				$file = getSignedURL($video["stuff"]);
				if(function_exists('exec')) {
	    			$xyz = shell_exec("/usr/local/bin/ffmpeg -i \"{$file}\" 2>&1 | grep Duration | cut -d ' ' -f 4 | sed s/,//");
	    			$explode = explode(':', $xyz);
					$duration = $explode[0]*3600+$explode[1]*60+round($explode[2]);
					$videos[$key]['duration'] = $duration;
	    		}
			}
			foreach ($videos as $key => $video) {
				$query="UPDATE `files` SET `duration`=$video[duration] WHERE `id`=$video[id]";
				$videosUpdate = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = "Successfully Updated";
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}


	//@ayush course-discount module	
	public function setCourseDiscount($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			//$course_discount = new TableGateway('course_discount', $adapter, null, new HydratingResultSet());
			$insert = array(
				'courseId' => $data->courseId,
				'discount' => $data->discount,
				'discountINR' => $data->discountINR,
				'startDate' => $data->startDate,
				'endDate' => $data->endDate
			);
			//$course_discount->insert($insert);
			$query="INSERT INTO course_discount (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
			$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$adapter->query("SET @discountId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$query = "SELECT @discountId as discountID";
			$discountID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$discountID = $discountID->toArray();
			$data->discountId = $discountID[0]["discountID"];
			/*$data->discountId = $course_discount->getLastInsertValue();
			if ($data->discountId == 0) {
				$db->rollBack();
				$result->status = 0;
				$result->message = "Please try Again.Some error occured";
				return $result;
			}*/
			$result->status = 1;
			$result->message = 'Congratulations. Discount Created Sucessfully!';
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}


	//@ayush course-admin
	public function getApprovedCourseID() {
		$result = new stdClass();
		try {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('courses');
		$select->columns(array('id'));
		$select->where(array('approved' => 1,'deleted' => 0));
		$selectString = $sql->getSqlStringForSqlObject($select);
		$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$adapter->getDriver()->getConnection()->disconnect();
		$coursearray= $courses->toArray();
		return $coursearray;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}


	public function setContentWatched($data) {
		require_once 'DynamoDbOps.php';
		$result = new stdClass();
		
		try {
			
			$ddo = new DynamoDbOps();
			
			$result=$ddo->putWatchedContent($data);
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	
	
	public function getApprovedCourseDiscount() {
		$result = new stdClass();
		try {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('courses');
		$select->columns(array('id'));
		$select->where(array('approved' => 1,'deleted' => 0));
		$select->where->greaterThan('studentPrice', '0');
		$select->where->greaterThan('studentPriceINR', '0');
		$selectString = $sql->getSqlStringForSqlObject($select);
		$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$adapter->getDriver()->getConnection()->disconnect();
		$coursearray= $courses->toArray();
		return $coursearray;		
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	
	
	
	//adminCourseCouponsInfo
	public function adminCourseCouponsInfo($data) {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('course_coupons');
		//$select->where(array('couponCode' => $data->couponCode));
		$select->order('endDate DESC');
		$selectString = $sql->getSqlStringForSqlObject($select);
		//print_r($selectString);
		$course_discount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$adapter->getDriver()->getConnection()->disconnect();
		$coursearray= $course_discount->toArray();
		$result['status'] = 1;
		$result['coursearray'] = $coursearray;
		return $result;
	}
	
	
	public function setcourseDiscountAdmin($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		$coursearray=$this->getApprovedCourseDiscount();
		$count=count($coursearray);
		try {
			$adapter = $this->adapter;
			$query="INSERT into course_discount (courseId,discount,discountINR,startDate,endDate) VALUES " ;
			$queryVals ="";
			for ($i=0; $i<$count;$i++) {
			if($i>0){
				 $query .= ",";
			}
			$query .= "('".$coursearray[$i]['id']."','" .$data->discount."','" .$data->discountINR."','" .$data->startDate."','" .$data->endDate."')";
			}
			//print_r($query);
			
			//print_r($query);
			$couponsarr = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = 'Congratulations. Discount Created Sucessfully!';
			return $result;			
			
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	
	public function setcourseCouponsAdmin($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {			
			$course_coupons= $this->checkCoupon($data);
			if(count($course_coupons)>0)
			{
				$result->status = 0;
				$result->message = "Coupon Code already in use. Please try some other coupon code";
				return $result;
			}else{
				$adapter = $this->adapter;
				//$course_coupons = new TableGateway('course_coupons', $adapter, null, new HydratingResultSet());
				$insert = array(
					'courseId' 		=> '0',
					'couponCode'  	=> $data->couponCode, 			
					'noOfCoupons'   => $data->noOfCoupons,
					'CouponCodeType'=> '1',
					'DiscountPer' 	=> $data->DiscountPer,
					'DiscountPerINR'=> $data->DiscountPerINR,
					'startDate' 	=> $data->startDate,
					'endDate' 		=> $data->endDate,
					'Active'				=> '1'		
						);
				//$course_coupons->insert($insert);
				$query="INSERT INTO course_coupons (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$adapter->query("SET @couponId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$query = "SELECT @couponId as couponID";
				$couponID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$couponID = $couponID->toArray();
				$data->couponId = $couponID[0]["couponID"];
				/*$data->couponId = $course_coupons->getLastInsertValue();
				if ($data->couponId == 0) {
					$db->rollBack();
					$result->status = 0;
					$result->message = "Please try Again.Some error occured";
					return $result;
				}*/
				$result->status = 1;
				$result->message = 'Congratulations. Coupon Code Created Sucessfully!';
				return $result;
			}
				
			
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	
	//@ayush course-coupons module
	public function setcourseCoupons($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$course_coupons= $this->checkCoupon($data);		
			if(count($course_coupons)>0)
			{
				$result->status = 0;
				$result->message = "Coupon Code already in use. Please try some other coupon code";
				return $result;
			}
			else{
				$adapter = $this->adapter;
				//$course_coupons = new TableGateway('course_coupons', $adapter, null, new HydratingResultSet());
				$insert = array(
					'courseId' => $data->courseId,
					'couponCode' => $data->couponCode,
					'noOfCoupons' => $data->noOfCoupons,
					'CouponCodeType'=> '2',
					'DiscountPer' => $data->DiscountPer,
					'DiscountPerINR' => $data->DiscountPerINR,
					'startDate' => $data->startDate,
					'endDate' => $data->endDate	,
					'Active'			=> '1'	
							);
				//$course_coupons->insert($insert);
				$query="INSERT INTO course_coupons (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$adapter->query("SET @couponId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$query = "SELECT @couponId as couponID";
				$couponID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$couponID = $couponID->toArray();
				$data->couponId = $couponID[0]["couponID"];
				/*$data->couponId = $course_coupons->getLastInsertValue();
				if ($data->couponId == 0) {
					$db->rollBack();
					$result->status = 0;
					$result->message = "Please try Again.Some error occured";
					return $result;
				}*/
				$result->status = 1;
				$result->message = 'Congratulations. Coupon Code Created Sucessfully!';
				return $result;			
			}
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	//function to check if coupon code is already active
	public function checkCoupon($data) {
		try{
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('course_coupons');
			$select->where(array('couponCode' => $data->couponCode));
			$select->where->greaterThan('endDate', $data->endDate);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$course_coupons = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$adapter->getDriver()->getConnection()->disconnect();
			$course_coupons= $course_coupons->toArray();
			return $course_coupons;
			}
		catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	public function redeemCoupon($data) {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		if (isset($data->courseId) && !empty($data->courseId)) {
			$select->from('course_coupons');
			$select->where
				       ->nest
				           ->equalTo('courseId', $data->courseId)
				           ->or
				           ->equalTo('courseId', 0)
       					->unnest
					->and
      					 ->equalTo('couponCode', $data->couponCode);
			//$select->where(array('courseId' => $data->courseId,'couponCode' => $data->couponCode));
		} else {
			$select->from(array('cc' => 'course_coupons'));
			$select->where
				       ->nest
				           ->equalTo('slug', $data->slug)
				           ->or
				           ->equalTo('cc.courseId', 0)
       					->unnest
					->and
      					 ->equalTo('couponCode', $data->couponCode);
			//$select->where(array('slug' => $data->slug,'couponCode' => $data->couponCode));
			$select->join(array('c'	=>	'courses'),
						'c.id=cc.courseId',
						array('cid'	=>	'id'),'left'
					);
		}
		//$select->where('startDate < ?', $data->date); 
		//$select->where('endDate > ?', $data->date);
		$select->where('noOfCoupons > 0');
		$select->order('couponId DESC');
		$selectString = $sql->getSqlStringForSqlObject($select);
		///print_r($selectString);
		$course_coupons = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$adapter->getDriver()->getConnection()->disconnect();
		$course_coupons= $course_coupons->toArray();
		if(count($course_coupons)>0)
		{
			return $course_coupons[0];
			
		}
		return $course_coupons;
	}
	
	public function updateCoupon($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			/*
			  We need to verify if the user editing the course owns it.
			 */
			$where = new \Zend\Db\Sql\Where();					 
			$where
		    ->nest()
		    ->equalTo('courseId', $data->courseId)
		    ->or
		    ->equalTo('courseId', 0)
		    ->unnest()
		    ->and
		    ->equalTo('couponCode', $data->couponCode);				 
						 
			$adapter = $this->adapter;
			//$where = array('courseId' => $data->courseId,'couponCode' => $data->couponCode);
			$updateValues = array(
				'couponsUsed'	=>	$data->couponsUsed
			);
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('course_coupons');
			$update->set($updateValues);
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$result = $statement->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			return $result;
		} 
		catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}


	public function updatecourseCoupons($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			/*
			  We need to verify if the user editing the course owns it.
			 */
			$adapter = $this->adapter;
			$where = array('courseId' => $data->courseId,'couponCode' => $data->couponCode);
			$updateValues = array(
				'noOfCoupons'		=>	$data->noOfCoupons,
				'CouponCodeType'	=>  $data->CouponCodeType,
				'DiscountPer' 		=>  $data->DiscountPer,
				'DiscountPerINR'	=>	$data->DiscountPerINR,
				'startDate' 		=>	$data->startDate,
				'endDate'			=>	$data->endDate	,
				'Active'			=> '1'		
			);
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('course_coupons');
			$update->set($updateValues);
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$result = $statement->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = 'Congratulations. Coupon Code Updated Sucessfully!';
			return $result;
		} 
		catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message=$e->getMessage();
			return $result;
		}
	}

	public function slug($z){
		$z = strtolower($z);
		$z = preg_replace('/[^a-z0-9 -]+/', '', $z);
		$z = str_replace(' ', '-', $z);
		return trim($z, '-');
	}

	public function setCourseSlugs() {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('courses');
			$select->columns(array('id', 'name'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$courses = $courses->toArray();
			foreach ($courses as $key => $course) {
				$courses[$key]['name'] = $this->slug($course['name']);
				$i = 0;
				do {
					if ($i>0) {
						$courses[$key]['name'] = $courses[$key]['name'].$i;
					}
					$i++;
					$selectString = "SELECT id,name FROM courses WHERE slug = '{$courses[$key]['name']}' AND id!='{$course['id']}'";
					$courseSlugs = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$courseSlugs = $courseSlugs->toArray();
				} while(count($courseSlugs)>0);

				echo $courses[$key]['name'].'<br>';
				$update = $sql->update();
				$update->table('courses');
				$update->set(array('slug' => $courses[$key]['name']));
				$update->where(array('id' => $course['id']));
				$statement = $sql->prepareStatementForSqlObject($update);
				$count = $statement->execute()->getAffectedRows();
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = 'Course slugs set!';
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function generateStudentsForCourse($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;

			require_once "StudentInvitation.php";
			$si = new StudentInvitation();
			$res = $si->checkRestrictionAndWarningOfCourse($data);
			
			if($res->status == 1) {
			
				for ($i=0; $i < $data->students; $i++) {

					$db = $this->adapter->getDriver()->getConnection();

					$db->beginTransaction();

					//$loginDetails = new TableGateway('login_details', $this->adapter, null, new HydratingResultSet());
					//$data->username=  strtolower($data->username);
					$username = $this->getRandomAlphaString(6);
					$filler   = "";
					do {
						$usernameDb = $username.$filler;
						$sql = "SELECT id from login_details WHERE username=?";
						$res = $this->adapter->query($sql, array($usernameDb));
						if (empty($filler)) {
							$filler = 1;
						} else {
							$filler++;
						}
					} while ($res->count() > 0);
					$data->username = $usernameDb;
					$password = $this->getRandomString(6);
					$data->hashedPassword = sha1($password);
					$insert = array(
						'email' => '',
						'username' => $data->username,
						'password' => $data->hashedPassword,
						'validated'=> 1,
						'temp' => 1,
						'createdAt' => time()
					);
					//$loginDetails->insert($insert);
					$query="INSERT INTO login_details (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
					$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$adapter->query("SET @userId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
					//$userId = $loginDetails->getLastInsertValue();
					//$user_roles = new TableGateway('user_roles', $this->adapter, null, new HydratingResultSet());
					$insert = array(
						/*'userId' => $userId,*/
						'roleId' => 4
					);
					//$user_roles->insert($insert);
					$query="INSERT INTO user_roles (`".implode("`,`", array_keys($insert))."`,`userId`) VALUES ('".implode("','", array_values($insert))."',@userId)";
					$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					//$user_temp_passwords = new TableGateway('user_temp_passwords', $this->adapter, null, new HydratingResultSet());
					$insert = array(
						/*'userId' => $userId,*/
						'password' => $password
					);
					//$user_temp_passwords->insert($insert);
					$query="INSERT INTO user_temp_passwords (`".implode("`,`", array_keys($insert))."`,`userId`) VALUES ('".implode("','", array_values($insert))."',@userId)";
					$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
					
					$query = "SELECT @userId as userID";
					$userID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$userID = $userID->toArray();
					$userId = $userID[0]["userID"];

					$userData = new stdClass();
					$userData->userId = $userId;
					$userData->contactMobilePrefix	= '';
					$userData->contactMobile		= '';
					$userData->contactLandlinePrefix= '';
					$userData->contactLandline		= '';
					$userData->addressCountryId		= 1;
					$userData->addressStreet		= '';
					$userData->addressCity			= '';
					$userData->addressState			= '';
					$userData->addressPin			= '';

					$u = new User();
					$res = $u->addBasicDetailsShort($userData);

					$userData = new stdClass();
					$userData->userId		= $userId;
					$userData->firstName	= '';
					$userData->lastName		= '';
					$userData->gender		= '';
					$userData->dob			= '';

					require_once "Student.php";
					$s = new Student();
					$res = $s->addStudentDetails($userData);

					/*require_once "StudentInvitation.php";
					$si = new StudentInvitation();
					$res = $si->checkRestrictionAndWarningOfCourse($data);*/

					//if($res->status == 1) {

						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('course_key_details');
						$select->columns(array('key_id'));
						//$select->where->equalTo('userId', $data->userId);
						//$select->where->equalTo('active_date', '0000-00-00 00:00:00');
						//$select->where->isNull('active_date');
						$select->where("userId = {$data->userId} AND (active_date IS NULL OR active_date LIKE '0000-00-00 00:00:00')");
						$selectString = $sql->getSqlStringForSqlObject($select);
						$course_key = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
						$res->course_key = $course_key->toArray();

						$key_id = $res->course_key[0]['key_id'];
						$sql2 = new Sql($this->adapter);
						$update2 = $sql2->update();
						$update2->table('course_key_details');
						$update2->set(array('active_date' => new \Zend\Db\Sql\Expression('CURRENT_TIMESTAMP')));
						$update2->where(array('key_id' => $key_id));
						$statement2 = $sql2->prepareStatementForSqlObject($update2);
						$result2 = $statement2->execute();

						$student_course = new TableGateway('student_course', $adapter, null, new HydratingResultSet());
						$insert = array(
							'course_id' => $data->courseId,
							'user_id' => $userId,
							'key_id' => $key_id,
							'timestamp' => date("Y-m-d H:i:s"),
						);
						$student_course->insert($insert);
						if ($student_course->getLastInsertValue() == 0) {
							$result->status = 0;
							return $result;
						}
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('subjects');
						$select->columns(array('id'));
						$select->where(array('courseId' => $data->courseId, 'ownerId' => $data->userId));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$subjects = $subjects->toArray();
						if (count($subjects)>0) {
							foreach ($subjects as $key => $subject) {
								$subjectId = $subject['id'];
								$student_subject = new TableGateway('student_subject', $adapter, null, new HydratingResultSet());
								$insert = array(
									'courseId' => $data->courseId,
									'userId' => $userId,
									'subjectId' => $subjectId,
									'Active' => 1
								);
								$student_subject->insert($insert);
							}
						}
					//}
				}

				$result->status = 1;
			} else {
				$result->status = 0;
				$result->message = $res->message;
			}
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function exportCourseStudents($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			
			$select = $sql->select();
			$select->from('courses');
			$select->where(array('id'=>$data->courseId,'ownerId'=>$data->userId));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courseOwners = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$courseOwners = $courseOwners->toArray();
			if (count($courseOwners) == 0) {
				$result->status  = 0;
				$result->message = "You are not the course owner";
			} else {
				$query="SELECT L.id, L.username, IFNULL(utp.password,'') AS password, CONCAT( S.firstName,' ',S.lastName) AS name,L.email,plg.userid,plg.passwd
						FROM student_details AS S 
						INNER JOIN login_details AS L ON L.id=S.userId
						INNER JOIN user_details AS u ON u.userId=L.id
						INNER JOIN student_course AS K ON K.user_id=L.id
						LEFT JOIN user_temp_passwords AS utp ON utp.userId=L.id
						LEFT JOIN parent_login_short AS plg ON plg.student_id=L.id AND plg.course_id=K.course_id
						WHERE K.course_id=$data->courseId";
				$students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->students = $students->toArray();
				if (count($result->students) == 0) {
					$result->status = 0;
					$result->message = "No students attached";
					return $result;
				} else {
					foreach ($result->students as $key => $value) {
						$result->students[$key]['passwd'] = base64_decode($result->students[$key]['passwd']);
					}
				}
			}

			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function favoriteCourse($data) {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$query="SELECT *
					FROM favorite_courses fc
					INNER JOIN courses c ON c.id=fc.courseId
					WHERE fc.userId={$data->userId} AND fc.courseId={$data->courseId}";
			$fcourses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$fcourses = $fcourses->toArray();
			if (count($fcourses) == 0) {
				$temp = new TableGateway('favorite_courses', $adapter, null, new HydratingResultSet());
				$insert = array(
					'courseId'	=>	$data->courseId,
					'userId'	=>	$data->userId,
					'created'		=>	date('Y-m-d', time())
				);
				$temp->insert($insert);
			}
			$result->status = 1;
			$result->message = "Course is added to favorites successfully!";
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function unfavoriteCourse($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$sql = new Sql($adapter);
			$delete = $sql->delete();
			$delete->from('favorite_courses');
			$delete->where(array('userId' => $data->userId, 'courseId' => $data->courseId));
			$statement = $sql->prepareStatementForSqlObject($delete);
			$courseCategories = $statement->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = "Course is removed from favorites successfully!";
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
 
	public function getDeletedStuff($data) {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('subjects');
			$select->where(array('ownerId'=>$data->userId,'deleted'=>1));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->subjects = $subjects->toArray();
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('exams');
			$select->where(array('type'=>'Exam','ownerId'=>$data->userId,'delete'=>1));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->exams = $exams->toArray();
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('exams');
			$select->where(array('type'=>'Assignment','ownerId'=>$data->userId,'delete'=>1));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->assignments = $exams->toArray();
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('exam_subjective');
			$select->where(array('ownerId'=>$data->userId,'Active'=>0));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->subjectiveexams = $exams->toArray();
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('exam_manual');
			$select->where(array('ownerId'=>$data->userId,'status'=>0));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->manualexams = $exams->toArray();
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('submissions');
			$select->where(array('ownerId'=>$data->userId,'deleted'=>1));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->submissions = $exams->toArray();
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	function getRandomAlphaString($length) {
		$validCharacters = "abcdefghijklmnopqrstuvwxyz";
		$validCharNumber = strlen($validCharacters);
		$result = "";
		for ($i = 0; $i < $length; $i++) {
			$index = mt_rand(0, $validCharNumber-1);
			$result .= $validCharacters[$index];
		}
		return $result;
	}
	function getRandomString($length) {
		$validCharacters = "12345abcdefghijklmnopqrstuvwxyz67890";
		$validCharNumber = strlen($validCharacters);
		$result = "";
		for ($i = 0; $i < $length; $i++) {
			$index = mt_rand(0, $validCharNumber-1);
			$result .= $validCharacters[$index];
		}
		return $result;
	}

	//function to calculate things for course purchase
	public function purchaseCourseViaPayU($data) {

		$db = $this->adapter->getDriver()->getConnection();

		//$db->beginTransaction();
		$result = new stdClass();
		try {
			$amount = 0;
			$couponCo="";
			$productId="";
			$discount=0;
			$discountINR=0;
			$couponstatus=$data->couStatus;
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			if (isset($data->slug) && !empty($data->slug)) {
				$select = $sql->select();
				$select->from('courses');
				$select->where(array(
					'slug' =>  $data->slug
				));
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				$data->courseId = $temp[0]['id'];
			}
			//checking if user already purchase the course
			$select = $sql->select();
			$select->from('student_course');
			$select->where(array(
				'course_id' =>  $data->courseId,
				'user_id'   =>  $data->userId
			));
			$select->columns(array('id'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$temp = $temp->toArray();
			if(count($temp) != 0) {
				$result->status = 0;
				$result->message = 'You have already purchased this course.';
				return $result;
			}
			//now checking pending invitations for the same course
			$selectString = "SELECT id FROM invited_student_details WHERE course_id={$data->courseId} AND email_id=(SELECT email FROM login_details WHERE id={$data->userId})";
			$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$temp = $temp->toArray();
			if(count($temp) != 0) {
				$result->status = 0;
				$result->message = 'You have already been invited for this course. Please check your notifications.';
				return $result;
			}
			//fetching any if discount exists for course
			$req1 = new stdClass();
			$dis = new stdClass();
			$req1->courseId=$data->courseId;
			$today=$data->date;
			$discounts= $this->getCourseDiscount($req1);
			if(count($discounts)>0){
				if($discounts['endDate']>$today)
					{	
						$discount=$discounts['discount'];
						$discountINR=$discounts['discountINR'];
				}
			
			}
			//coupons
			$creq = new stdClass();
			$creq->courseId=$data->courseId;
			$creq->couponCode=$data->couponCode;
			$cres=new stdClass();
			if($couponstatus ==1)
			{	//$couponCo=$data->couponCode;
		
				$today=$data->date;
				$coupons =$this->redeemCoupon($creq);
				if($coupons['endDate']>$today &&  $coupons['startDate'] < $today && ($coupons['noOfCoupons']-$coupons['couponsUsed'])>0) {
						$couponCo=$data->couponCode;	
						$discount=$coupons['DiscountPer'];
						$discountINR=$coupons['DiscountPerINR'];
				}
			}
			
			//fetching course price for calculation
			$select = $sql->select();
			$select->from('courses');
			$select->where(array('id'   =>  $data->courseId));
			$select->columns(array('name', 'studentPrice'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courseDetail = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$courseDetail = $courseDetail->toArray();
			if(count($courseDetail) > 0) {
				$amount = $courseDetail[0]['studentPrice'];
				$amount = $amount *((100-$discount)/100);
					
			} else {
				$result->status = 0;
				$result->message = 'No such course found.';
				return $result;
			}
			$name = '';
			$email = '';
			$number = '';
			//fetching first name of student
			$select = $sql->select();
			$select->from(array('ld'    =>  'login_details'));
			$select->where(array('ld.id'    =>  $data->userId));
			$select->columns(array(
				'id'    =>  'id',
				'email' =>  'email'
			));
			$select->join(
				array('ud'  =>  'user_details'),
				'ud.userId=ld.id',
				array('contactMobile'   =>  'contactMobile')
			);
			$select->join(
				array('sd'  =>  'student_details'),
				'sd.userId=ld.id',
				array('name'    =>  new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"))
			);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$details = $details->toArray();
			if(count($details) > 0) {
				$name = $details[0]['name'];
				if($details[0]['contactMobile'] == '')
					$details[0]['contactMobile'] = '9876543210';
				$number = $details[0]['contactMobile'];
				$email = $details[0]['email'];
			}
			
			$productId=$data->courseId;
			$db->beginTransaction();
			//generating order id
			//$orderId = new TableGateway('order_details', $adapter, null, new HydratingResultSet()); // transaction comment
			$insert = array(
				'type'              =>  2,
				'userId'            =>  $data->userId,
				'email'             =>  $email,
				'purchase_detail'   =>  $data->courseId,
				'rate'              =>  0,
				'amount'            =>  $amount,
				'status'            =>  0,
				'payUId'            =>  0,
				'IGROTxnID'			=>  0,
				'timestamp'         =>  time(),
				'couponcode'		=>  $couponCo
			);
			$query="INSERT INTO order_details (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
			$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$adapter->query("SET @orderId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
			//$orderId->insert($insert); // transaction comment
			//$txnid = $orderId->getLastInsertValue(); // transaction comment
			//$db->commit();

			$result->amount = $amount;
			//$result->orderId = $txnid; // transaction comment
			if($result->amount <= 0) {
				$result->paymentSkip = 1;
				
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();

				$query = "SELECT @orderId as orderID";
				$orderID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$orderID = $orderID->toArray();
				$result->orderId = $orderID[0]["orderID"];

				$dataSkip = array();
				$dataSkip['paymentSkip'] = $result->paymentSkip;
				$dataSkip['orderId'] = $result->orderId;
				/*require_once("Coursekeys.php");
				$ck = new Coursekeys();*/
				$this->afterIGROPay($dataSkip);
			} else {
				$result->paymentSkip = 0;

				require_once("CryptoWallet.php");
				$cw = new CryptoWallet();
				$userAddressDB = $cw->getWalletAddress($data);
				if ($userAddressDB->status == 0) {
					$result->status = 0;
					$result->message = "No User Crypto Wallet address!";
				}

				$IGROContractDB = $cw->getIGROContractData($data);
				if ($IGROContractDB->status == 0) {
					$result->status = 0;
					$result->message = "No IGRO Contract is set!";
				}

				//global $siteBase; // transaction comment

				//$IGROTxn = new TableGateway('igro_transactions', $adapter, null, new HydratingResultSet()); // transaction comment
				$insert = array(
					'toAddress'			=>  $IGROContractDB->ownerAddress,
					'fromAddress'		=>  $userAddressDB->wallet,
					'contractAddress'	=>  $IGROContractDB->contractAddress,
					'transactionHash'	=>  '',
					'status'			=>  'new',
					'created'			=>  date("Y-m-d H:i:s"),
					'modified'			=>  date("Y-m-d H:i:s")
				);
				//$IGROTxn->insert($insert); // transaction comment
				$query="INSERT INTO igro_transactions (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$adapter->query("SET @igroTxnId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
				
				//$IGROTxnID = $IGROTxn->getLastInsertValue(); // transaction comment
				//$result->IGROTxnID = $IGROTxnID; // transaction comment

				/*$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('order_details');
				$update->set(array(
									'IGROTxnID'	=>	$IGROTxnID
				));
				$update->where(array('orderId'		=>	$result->orderId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();*/

				$query="UPDATE order_details SET `IGROTxnID`=@igroTxnId WHERE orderId=@orderId";
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();

				$query = "SELECT @orderId as orderID";
				$orderID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$orderID = $orderID->toArray();
				$result->orderId = $orderID[0]["orderID"];

				$query = "SELECT @igroTxnId as igroTxnID";
				$igroTxnID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$igroTxnID = $igroTxnID->toArray();
				$result->IGROTxnID = $igroTxnID[0]["igroTxnID"];
			}

			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	//function to be called after payU payment
	public function afterIGROPay($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			//setting any undefined variable if not returned 
			//updating the order details table
			
			if ($data['paymentSkip'] == 0) {
				$where = array('IGROTxnID'	=>	$data['id']);
				$status = $data['txn_status'];
			} else {
				$where = array('orderId' => $data['orderId']);
				$status = 1;
			}
			//if error is E000 then actually add keys to the user account

			$send = new stdClass();
			$select = $sql->select();
			$select->from('order_details');
			$select->columns(array('orderId', 'userId', 'purchase_detail', 'amount'));
			$select->where($where);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$details = $details->toArray();
			if(count($details) > 0)
			 {
				$send->courseId = $details[0]['purchase_detail'];
				$send->price = $details[0]['amount'];
				$send->userRole = '4';
				$send->userId = $details[0]['userId'];
				require_once 'StudentInvitation.php';
				$s = new StudentInvitation();
				$s->linkStudentCourse($send, $data['orderId']);
			}
			
			/*$send = new stdClass();
			$select = $sql->select();
			$select->from('order_details');
			$select->columns(array('orderId', 'userId', 'purchase_detail'));
			$select->where($where);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$details = $details->toArray();
			if(count($details) > 0) {

				$update = $sql->update();
				$update->table('order_details');
				$update->set(array(
					'status'	=>	$status
				));
				$update->where(array('orderId'	=>	$details[0]['orderId']));
				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

				$send->key_user = $details[0]['userId'];
				$send->keys = $details[0]['purchase_detail'];
				$this->purchase_key($send);
			}*/			
			$data['date'] = date('d M Y H:i:s');
			$result->status = 1;
			$result->data = $data;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	//function to calculate things for course purchase
	/*public function purchaseCourseViaPayU($data) {
		$result = new stdClass();
		try {
			$amount = 0;
			$couponCo="";
			$productId="";
			$discount=0;
			$discountINR=0;
			$couponstatus=$data->couStatus;
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			if (isset($data->slug) && !empty($data->slug)) {
				$select = $sql->select();
				$select->from('courses');
				$select->where(array(
					'slug' =>  $data->slug
				));
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				$data->courseId = $temp[0]['id'];
			}
			//checking if user already purchase the course
			$select = $sql->select();
			$select->from('student_course');
			$select->where(array(
				'course_id' =>  $data->courseId,
				'user_id'   =>  $data->userId
			));
			$select->columns(array('id'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$temp = $temp->toArray();
			if(count($temp) != 0) {
				$result->status = 0;
				$result->message = 'You have already purchased this course.';
				return $result;
			}
			//now checking pending invitations for the same course
			$selectString = "SELECT id FROM invited_student_details WHERE course_id={$data->courseId} AND email_id=(SELECT email FROM login_details WHERE id={$data->userId})";
			$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$temp = $temp->toArray();
			if(count($temp) != 0) {
				$result->status = 0;
				$result->message = 'You have already been invited for this course. Please check your notifications.';
				return $result;
			}
			//fetching any if discount exists for course
			$req1 = new stdClass();
			$dis = new stdClass();
			$req1->courseId=$data->courseId;
			$today=$data->date;
			$discounts= $this->getCourseDiscount($req1);
			if(count($discounts)>0){
				if($discounts['endDate']>$today)
					{	
						$discount=$discounts['discount'];
						$discountINR=$discounts['discountINR'];
				}
			
			}
			//coupons
			$creq = new stdClass();
			$creq->courseId=$data->courseId;
			$creq->couponCode=$data->couponCode;
			$cres=new stdClass();
			if($couponstatus ==1)
			{	//$couponCo=$data->couponCode;
		
				$today=$data->date;
				$coupons =$this->redeemCoupon($creq);
				//print_r($coupons);
				//print_r($data);
				//print_r($coupons);
				if($coupons['endDate']>$today &&  $coupons['startDate'] < $today && ($coupons['noOfCoupons']-$coupons['couponsUsed'])>0) {
						$couponCo=$data->couponCode;	
						$discount=$coupons['DiscountPer'];
						$discountINR=$coupons['DiscountPerINR'];
				}
			}
			
			//fetching course price for calculation
			$select = $sql->select();
			$select->from('courses');
			$select->where(array('id'   =>  $data->courseId));
			$select->columns(array('name', 'studentPrice'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courseDetail = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$courseDetail = $courseDetail->toArray();
			if(count($courseDetail) > 0) {
				$amount = $courseDetail[0]['studentPrice'];
				$amount = $amount *((100-$discount)/100);
					
			} else {
				$result->status = 0;
				$result->message = 'No such course found.';
				return $result;
			}
			$name = '';
			$email = '';
			$number = '';
			//fetching first name of student
			$select = $sql->select();
			$select->from(array('ld'    =>  'login_details'));
			$select->where(array('ld.id'    =>  $data->userId));
			$select->columns(array(
				'id'    =>  'id',
				'email' =>  'email'
			));
			$select->join(
				array('ud'  =>  'user_details'),
				'ud.userId=ld.id',
				array('contactMobile'   =>  'contactMobile')
			);
			$select->join(
				array('sd'  =>  'student_details'),
				'sd.userId=ld.id',
				array('name'    =>  new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"))
			);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$details = $details->toArray();
			if(count($details) > 0) {
				$name = $details[0]['name'];
				if($details[0]['contactMobile'] == '')
					$details[0]['contactMobile'] = '9876543210';
				$number = $details[0]['contactMobile'];
				$email = $details[0]['email'];
			}
			
			$productId=$data->courseId;
			//generating order id
			$orderId = new TableGateway('order_details', $adapter, null, new HydratingResultSet());
			$insert = array(
				'type'              =>  2,
				'userId'            =>  $data->userId,
				'email'             =>  $email,
				'purchase_detail'   =>  $data->courseId,
				'rate'              =>  0,
				'amount'            =>  $amount,
				'currency'          =>  $currency,
				'status'            =>  0,
				'payUId'            =>  0,
				'timestamp'         =>  time(),
				'couponcode'		=>  $couponCo
			);
			$orderId->insert($insert);
			$txnid = $orderId->getLastInsertValue();
			//$udf1=$productId;
			//$udf2=$couponCo;
			
			global $siteBase;
			
			//changed code here-----------------------------------------------------------------------------------
			if($currency == 2) {
				$result->method = 2;
				$productinfo = '{"ProductName":"'.$courseDetail[0]['name'].'","amount":"'.$amount.'","courseId":"'.$productId.'","couponCode":"'. $couponCo.'"}';
		
				//{"ProductName":"Mathematics","amount":"250","courseId":"249","couponCode":"NEW20"} OUTPUT;  
				//$key = 'JBZaLc';
				$key = 'bgvTnU';
				//$key = 'gtKFFx';
				//hash generation algorithm
				//$SALT = 'GQs7yium';
				$SALT = 'Zv0pJQn2';
				//$SALT = 'eCwWELxi';
				$hash = '';
				// Hash Sequence
				$hashSequence = "key|txnid|amount|productinfo|name|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
				$hashVarsSeq = explode('|', $hashSequence);
				$hash_string = '';
				foreach($hashVarsSeq as $hash_var) {
					$hash_string .= isset($$hash_var) ? $$hash_var : '';
					$hash_string .= '|';
				}
				$hash_string .= $SALT;
				$hash = strtolower(hash('sha512', $hash_string));

				//the fields to be sent to payu for transcation
				$result->amount = $amount;
				$result->firstname = $name;
				$result->email = $email;
				$result->phone = $number;
				$result->txnid = $txnid;
				$result->key = $key;
				//new fields for coupons identifications
				//$result->udf1 = $udf1;
				//$result->udf2 = $udf2;
				$result->productinfo = $productinfo;
				$result->surl = $siteBase.'marketplace/laterpay/course/success/';
				$result->furl = $siteBase.'marketplace/laterpay/course/fail/';
				$result->curl = $siteBase.'marketplace/laterpay/course/cancel/';
				//$result->surl = $siteBase.'marketplace/laterpay.php';
				//$result->furl = $siteBase.'marketplace/laterpay.php';
				//$result->curl = $siteBase.'marketplace/laterpay.php';
				$result->service_provider = 'payu_paisa';
				$result->hash = $hash;
				$result->url = 'https://secure.payu.in/_payment';
				//$result->url = 'https://test.payu.in/_payment.php';
				//skipping payu in case of zero payment
				if($result->amount <= 0) {
					$result->paymentSkip = 1;
					$newobj = array();
					$newobj['mihpayid'] = 0;
					$newobj['mode'] = 'FR';
					$newobj['status'] = 'success';
					$newobj['key'] = $key;
					$newobj['txnid'] = $txnid;
					$newobj['amount'] = 0;
					$newobj['addedon'] = date('Y-m-d H:i:s');
					$newobj['productinfo'] = $productinfo;
					$newobj['error'] = 'E000';
					$this->afterPayU($newobj);
				}
				else {
					$result->paymentSkip = 0;
				}
			}
			else if($currency == 1) {
				$result->method = 1;
	        	$result->amount = $amount;
	        	$result->orderId = $txnid;
	        	//$result->business = 'business@rupesh.com';
	        	$result->business = 'payments@integro.io';
	        	$result->surl = $siteBase.'marketplace/laterpaypal/course/success/';
	        	$result->curl = $siteBase.'marketplace/laterpaypal/course/cancel/';
	        	//$result->surl = $siteBase.'marketplace/laterpaypal.php';
				//$result->curl = $siteBase.'marketplace/laterpaypal.php';
	        	$result->nurl = $siteBase.'ipn_listener.php';
	        	$result->url = 'https://www.paypal.com/cgi-bin/webscr';
	        	//$result->quantity = $data->quantity;
				//https://www.paypal.com/cgi-bin/webscr
				//https://www.sandbox.paypal.com/cgi-bin/webscr
				// $couponCo
				$coursedetails= $courseDetail[0]['name']." ".$productId." ".$couponCo;
	        	$result->item_name = $coursedetails;
	        	//$result->rate = $savedRate;
				//print_r($result->item_name);
				//print_r (explode(" ",$result->item_name));
	        	if($result->amount > 0) {
	        		$result->paymentSkip = 0;
	        	}
	        	else {
	        		$result->paymentSkip = 1;
	        		$invoice = $txnid;
	        		$amount = 0.00;
	        		$txid = 'FREE';
					$productinfo = '{"ProductName":"'.$courseDetail[0]['name'].'","amount":"'.$amount.'","courseId":"'.$productId.'","couponCode":"'. $couponCo.'"}';
					require_once 'User.php';
					$u = new User();
					$u->completePaypalPaymentUsingIPN($invoice, $amount, $txid, $productinfo);
	        	}
			}

			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	//function to be called after payU payment
	public function afterPayU($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			//setting any undefined variable if not returned 
			if(!isset($data['mihpayid']))
				$data['mihpayid'] = '';
			if(!isset($data['mode']))
				$data['mode'] = '';
			if(!isset($data['status']))
				$data['status'] = '';
			if(!isset($data['unmappedstatus']))
				$data['unmappedstatus'] = '';
			if(!isset($data['key']))
				$data['key'] = '';
			if(!isset($data['txnid']))
				$data['txnid'] = '';
			if(!isset($data['amount']))
				$data['amount'] = '';
			if(!isset($data['addedon']))
				$data['addedon'] = '';
			if(!isset($data['productinfo']))
				$data['productinfo'] = '';
			if(!isset($data['firstname']))
				$data['firstname'] = '';
			if(!isset($data['lastname']))
				$data['lastname'] = '';
			if(!isset($data['address1']))
				$data['address1'] = '';
			if(!isset($data['address2']))
				$data['address2'] = '';
			if(!isset($data['city']))
				$data['city'] = '';
			if(!isset($data['state']))
				$data['state'] = '';
			if(!isset($data['country']))
				$data['country'] = '';
			if(!isset($data['zipcode']))
				$data['zipcode'] = '';
			if(!isset($data['email']))
				$data['email'] = '';
			if(!isset($data['phone']))
				$data['phone'] = '';
			if(!isset($data['udf1']))
				$data['udf1'] = '';
			 if(!isset($data['udf2']))
				$data['udf2'] = '';
			 if(!isset($data['udf3']))
				$data['udf3'] = '';
			 if(!isset($data['udf4']))
				$data['udf4'] = '';
			 if(!isset($data['udf5']))
				$data['udf5'] = '';
			 if(!isset($data['udf6']))
				$data['udf6'] = '';
			 if(!isset($data['udf7']))
				$data['udf7'] = '';
			 if(!isset($data['udf8']))
				$data['udf8'] = '';
			 if(!isset($data['udf9']))
				$data['udf9'] = '';
			 if(!isset($data['udf10']))
				$data['udf10'] = '';
			 if(!isset($data['hash']))
				$data['hash'] = '';
			 if(!isset($data['field1']))
				$data['field1'] = '';
			 if(!isset($data['field2']))
				$data['field2'] = '';
			 if(!isset($data['field3']))
				$data['field3'] = '';
			 if(!isset($data['field4']))
				$data['field4'] = '';
			 if(!isset($data['field5']))
				$data['field5'] = '';
			if(!isset($data['field6']))
				$data['field6'] = '';
			if(!isset($data['field7']))
				$data['field7'] = '';
			if(!isset($data['field8']))
				$data['field8'] = '';
			if(!isset($data['field9']))
				$data['field9'] = '';
			if(!isset($data['PG_TYPE']))
				$data['PG_TYPE'] = '';
			if(!isset($data['bank_ref_num']))
				$data['bank_ref_num'] = '';
			if(!isset($data['bankcode']))
				$data['bankcode'] = '';
			if(!isset($data['error']))
				$data['error'] = '';
			if(!isset($data['error_Message']))
				$data['error_Message'] = '';
			if(!isset($data['cardToken']))
				$data['cardToken'] = '';
			if(!isset($data['name_on_card']))
				$data['name_on_card'] = '';
			if(!isset($data['cardnum']))
				$data['cardnum'] = '';
			if(!isset($data['cardhash']))
				$data['cardhash'] = '';
			if(!isset($data['amount_split']))
				$data['amount_split'] = '';
			if(!isset($data['payuMoneyId']))
				$data['payuMoneyId'] = '';
			if(!isset($data['discount']))
				$data['discount'] = '';
			if(!isset($data['net_amount_debit']))
				$data['net_amount_debit'] = '';
			
				
				
			
			$transcation = new TableGateway('payu_details', $adapter, null, new HydratingResultSet());
			$insert = array(
				'mihpayid'          =>  $data['mihpayid'],
				'mode'              =>  $data['mode'],
				'status'            =>  $data['status'],
				'unmappedstatus'    =>  $data['unmappedstatus'],
				'key'               =>  $data['key'],
				'txnid'             =>  $data['txnid'],
				'amount'            =>  $data['amount'],
				'addedon'           =>  $data['addedon'],
				'productinfo'       =>  $data['productinfo'],
				'firstname'         =>  $data['firstname'],
				'lastname'          =>  $data['lastname'],
				'address1'          =>  $data['address1'],
				'address2'          =>  $data['address2'],
				'city'              =>  $data['city'],
				'state'             =>  $data['state'],
				'country'           =>  $data['country'],
				'zipcode'           =>  $data['zipcode'],
				'email'             =>  $data['email'],
				'phone'             =>  $data['phone'],
				'udf1'              =>  $data['udf1'],
				'udf2'              =>  $data['udf2'],
				'udf3'              =>  $data['udf3'],
				'udf4'              =>  $data['udf4'],
				'udf5'              =>  $data['udf5'],
				'udf6'              =>  $data['udf6'],
				'udf7'              =>  $data['udf7'],
				'udf8'              =>  $data['udf8'],
				'udf9'              =>  $data['udf9'],
				'udf10'             =>  $data['udf10'],
				'hash'              =>  $data['hash'],
				'field1'            =>  $data['field1'],
				'field2'            =>  $data['field2'],
				'field3'            =>  $data['field3'],
				'field4'            =>  $data['field4'],
				'field5'            =>  $data['field5'],
				'field6'            =>  $data['field6'],
				'field7'            =>  $data['field7'],
				'field8'            =>  $data['field8'],
				'field9'            =>  $data['field9'],
				'PG_TYPE'           =>  $data['PG_TYPE'],
				'bank_ref_num'      =>  $data['bank_ref_num'],
				'bankcode'          =>  $data['bankcode'],
				'error'             =>  $data['error'],
				'error_Message'     =>  $data['error_Message'],
				'cardToken'         =>  $data['cardToken'],
				'name_on_card'      =>  $data['name_on_card'],
				'cardnum'           =>  $data['cardnum'],
				'cardhash'          =>  $data['cardhash'],
				'amount_split'      =>  $data['amount_split'],
				'payuMoneyId'       =>  $data['payuMoneyId'],
				'discount'          =>  $data['discount'],
				'net_amount_debit'  =>  $data['net_amount_debit']
			);
			$transcation->insert($insert);
			$payuId = $transcation->getLastInsertValue();

			//updating the order details table
			$update = $sql->update();
			$update->table('order_details');
			$update->set(array(
				'payUId'    =>  $payuId,
				'status'    =>  $data['error']
			));
			$update->where(array('orderId'  =>  $data['txnid']));
			$statement = $sql->getSqlStringForSqlObject($update);
			$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

			//if error is E000 then actually add the course to user account
			if($data['error'] == 'E000') {
				$send = new stdClass();
				$select = $sql->select();
				$select->from('order_details');
				$select->columns(array('orderId', 'userId', 'purchase_detail', 'amount'));
				$select->where(array('orderId'  =>  $data['txnid']));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$details = $details->toArray();
				if(count($details) > 0)
				 {
					$send->courseId = $details[0]['purchase_detail'];
					$send->price = $details[0]['amount'];
					$send->userRole = '4';
					$send->userId = $details[0]['userId'];
					require_once 'StudentInvitation.php';
					$s = new StudentInvitation();
					$s->linkStudentCourse($send, $data['txnid']);
				}
			}
			//updating coupons	
			//status==failure  success
			$data['date'] = date('d M Y H:i:s');	
			
			$result->status = 1;
			$result->data = $data;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	public function paypalReturn($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('od' => 'order_details'));
			$select->where(array('orderId' => $data->orderId));
			//$select->join(array('c' => 'courses'), 'c.id=od.purchase_detail', array('name'));
			$select->join(array('c' => 'courses'), 'c.id=od.purchase_detail', array('name'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$order = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$order = $order->toArray();
			if(count($order) != 1) {
				$result->status = 0;
				$result->message = 'Something went wrong. Your payment was not successful';
				$result->date = date('d M Y H:i:s');
				$result->userRole = $data->userRole;
				return $result;
			}
			$order = $order[0];
			$result->amount = $order['amount'];
			$result->type = $order['type'];
			$result->purchase_detail = $order['purchase_detail'];
			$result->rate = $order['rate'];
			$result->date = date('d M Y H:i:s');
			$result->userRole = $data->userRole;
			$result->name = $order['name'];
			$result->orderId = $order['orderId'];
			$result->couponcode = $order['couponcode'];
			
			
			$result->coupo=$data->iteminfo;
			$couponscourse=explode(" ",$result->coupo);
			//print_r($couponscourse);
			$result->payment_status=$data->payment_Status;
			//$couponscourse=explode(" ",$result->orderId);
			if(  $result->purchase_detail != ''  && $result->couponcode != ''){
			if($result->payment_status == 'Completed'  || $result->payment_status == 'completed' ){			
				$redeemdata->courseId  = $result->purchase_detail;
				$redeemdata->couponCode = $result->couponcode;
				$countervalue=$this->redeemCoupon($redeemdata);
				$noCoupons=$countervalue['couponsUsed'];
				$redeemdata->couponsUsed=$noCoupons+1;
				$update=$this->updateCoupon($redeemdata);
			}
			}
			
			
			
			

			//now inserting into paypal_details for future
			$paypal = new TableGateway('paypal_details', $adapter, null, new HydratingResultSet());
			$paypal->insert(array(
				'paypalId'	=>	$data->paypalId,
				'amount'	=>	$data->amount,
				'timestamp'	=>	time()
			));
			$paypalId = $paypal->getLastInsertValue();

			//updating the value of order with paypal details mapping
			$update = $sql->update();
			$update->table('order_details');
			$update->set(array(
				'payUId'	=>	$paypalId,
				'status'	=>	'E000'
			));
			$update->where(array('orderId' => $data->orderId));
			$statement = $sql->getSqlStringForSqlObject($update);
			$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

			$send = new stdClass();
			$send->courseId = $result->purchase_detail;
			$send->price = $result->amount;
			$send->userRole = '4';
			$send->userId = $data->userId;
			require_once 'StudentInvitation.php';
			$s = new StudentInvitation();
			$s->linkStudentCourse($send, $paypalId);

			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			$result->date = date('d M Y H:i:s');
			$result->userRole = $data->userRole;
			return $result;
		}
	}*/
}

?>
