<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Select;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	
	require_once "Base.php";
	
	class StudentPackage extends Base {
	
		//get exam details for student to give exam.
		public function getExamForStudent($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('e'		=>	'exams'))
						->join(array('i'	=>	'exam_instructions'),
							'e.id=i.examId',
							'instructions',
							$select::JOIN_LEFT);
				$select->where(array('e.id'		=>	$data->examId,
									'e.delete'	=>	0,
									'e.status'	=>	2));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exam = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exam = $exam->toArray();
				if(isset($exam[0])) {
					$result->exam = $exam[0];
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('es'	=>	'exam_sections'));
					if($data->resumeMode == 0) {
						if($result->exam['sectionOrder'] == 0)
							$select->order('es.weight ASC, es.id ASC');
						else
							$select->order(new \Zend\Db\Sql\Expression("RAND()"));
					}
					else {
						$select->join(
							array('aq'	=>	'attempt_questions'),
							'aq.sectionId = es.id',
							array('sectionId'	=>	'sectionId')
						);
						$select->order('aq.order ASC');
						$select->group('aq.sectionId');
					}
					$select->where(array('es.examId'	=>	$data->examId,
										'es.delete'		=>	0,
										'es.status'		=>	1));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$sections = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$sections = $sections->toArray();
					$result->sections = $sections;
					$result->status = 1;
				}
				else {
					$result->status = 0;
					$result->message = 'Exam not found.';
				}
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		/*
			Function	: 	checkNameForPackage
			params		:   packageName
			Author		:	mackayush
			return		:	true if package exists else false
			this function is used to check if name of package Already exists or not.	
					
		*/
		public function checkNameForPackage($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$select = $sql->select();
				$select->from('package');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('packName', $data->name);
				$select->where($where);
				$select->columns(array('packId'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				if(count($temp) > 0)
					$result->available = false;
				else
					$result->available = true;
				$result->status = 1;
				return $result;
			}
			catch(Exception $e){
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		/*
			Function	: 	checkNameForEditPackage
			params		:   packageName
			Author		:	mackayush
			return		:	true if package exists else false
			this function is used to check if name of package Already exists for edti-package functionality or not.	
					
		*/
		public function checkNameForEditPackage($data) {
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$select = $sql->select();
				$select->from('package');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('packName', $data->name);
				$select->where($where);
				$select->columns(array('packId'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				if(count($temp) > 0)
					$result->available = false;
				else
					$result->available = true;
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		/*
			Function	: 	savePackage
			params		:   packageName,packDescription,packType,packCategory,catID,image,price,priceINR,ownerId,startDate,endDate
			Author		:	mackayush
			return		:	status =1 if sucessfully saved
			this function is used to save the data of packages in packages table.
			if package category category ==4 then courses also gets saved in package_courses table.
			if package_type ==2 || 3 and students >0 then student data gets saved in package_user.					
		*/
		public function savePackage($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$checkCondtion= $this->canCreatePackage($data);
				$catCheck=$this->isCatValid($data);
				//print_r($checkCondtion->status);
				if($checkCondtion->status == 1 && $catCheck->status==1)
				{
					// saving package in package table
					$adapter = $this->adapter;
					$packages = new TableGateway('package', $adapter, null, new HydratingResultSet());
					$insert = array(
						'packName' => $data->name,
						'packDescription'=>$data->packDescription,
						'packType' => $data->packType,
						'packCategory'=>$data->packCategory,
						'catID'=>$data->catID,
						'image'=>$data->image,
						'price' => $data->price,
						'priceINR'=>$data->priceINR,
						'ownerId' => $data->userId,
						'startDate'=>$data->startDate,
						'endDate' => $data->endDate,
						'Active' =>1				
						);
					// slug generation begins
					$i = 0;
					$data->slug = $this->slug($data->name);
					do {
						if ($i>0) {
							$data->slug = $data->slug.$i;
						}
						$i++;
						$selectString = "SELECT packId,packName FROM package WHERE slug = '{$data->slug}'";
						$packageSlugs = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$packageSlugs = $packageSlugs->toArray();
					} while(count($packageSlugs)>0);

					$insert['slug'] = $data->slug;
					// slug generation ends

					$packages->insert($insert);
					$data->packId = $packages->getLastInsertValue();
					$result->id =$data->packId;
					//end saving package in package table	
					//saving course if custom category is selected
					
					if($data->packCategory == 4  && count($data->courseArray)>0)
					{	
						$courseArray=$data->courseArray;
						$count=count($courseArray);
						$adapter = $this->adapter;
						$query="INSERT INTO package_courses (packId,courseId) VALUES " ;					
						$queryVals ="";
						for ($i=0; $i<$count;$i++) {
							if($i>0){
								 $query .= ",";
							}
							$query .= "('".$data->packId."','" .$courseArray[$i]."')";
						}
						$package_courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$result->status1 = 1;
						$result->message1 = 'Congratulations. Package created !!!';			
						
					}
					//assigning users to packages in package_user
					if(($data->packType == 2 || $data->packType == 3 ) && count($data->studentArray)>0)
					{
						$studentArray=$data->studentArray;
						$count=count($studentArray);
						$adapter = $this->adapter;
						
						$query="INSERT INTO package_user (userId,userType,packId,packageAssigned,Paid,packageDeactiveDate) VALUES " ;
						
						$queryVals ="";
						$paid=0;
						if( isset($data->paytype) && ($data->paytype!=null))
						{
							$paid=1;
						}
						for ($i=0; $i<$count;$i++){
							if($i>0){
								 $query .= ",";
							}
							$query .= "('".$studentArray[$i]."',4,'".$data->packId."',0,'".$paid."',DATE_ADD(now(), INTERVAL 1 YEAR) )";
						}
						//print_r($query);
						$package_user = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$result->status1 = 1;
						$result->message2 = 'Congratulations. Package created !!!';	
					}
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();

					/*
					
					if(isset($data->paytype) && ($data->paytype!=null))
					{
						if(($data->packType == 2 || $data->packType == 3 ) && count($data->studentArray)>0)
					{
						$studentArray=$data->studentArray;
						$count=count($studentArray);
						$adapter = $this->adapter;
						
						$query="INSERT INTO package_user (userId,userType,packId,packageAssigned,Paid,packageDeactiveDate) VALUES " ;
						
						$queryVals ="";
						for ($i=0; $i<$count;$i++){
							if($i>0){
								 $query .= ",";
							}
							$query .= "('".$studentArray[$i]."',4,'".$data->packId."',0,1,DATE_ADD(now(), INTERVAL 1 YEAR) )";
						}
						//print_r($query);
						$package_user = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$result->status1 = 1;
						$result->message2 = 'Congratulations. Package created !!!';	
					}
					}
					*/
					$result->status = 1;
					$result->message = 'Congratulations. Package created!!!';
				}else{
					$result->status = 0;
					$result->message = 'You cannot create package as you dont have verified course/course category. Contact Admin.';
				}
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		/*
			Function	: 	editPackage
			params		:   packageName,packDescription,packType,packCategory,catID,image,price,priceINR,ownerId,startDate,endDate
			Author		:	mackayush
			return		:	status =1 if sucessfully saved
			this function is used to save the data of packages in packages table when edit pacakge functionality is chosen
			if package category category ==4 then courses also gets saved in package_courses table.
			if package_type ==2 || 3 and students >0 then student data gets saved in package_user.					
		*/	
		public function editPackage($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$checkCondtion= $this->canCreatePackage($data);
				$catCheck=$this->isCatValid($data);
				if($checkCondtion->status == 1 && $catCheck->status==1){
				// saving package in package table
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('package');

				$updateValues = array(
					'packName' => $data->name,
					'packDescription'=>$data->packDescription,
					'packType' => $data->packType,
					'packCategory'=>$data->packCategory,
					'catID'=>$data->catID,
					'image'=>$data->image,
					'price' => $data->price,
					'priceINR'=>$data->priceINR,
					'ownerId' => $data->userId,
					'startDate'=>$data->startDate,
					'endDate' => $data->endDate,
					'Active' =>1
					);

				$selectString	  = "SELECT packName FROM package WHERE packId='{$data->packId}'";
				$packageNameCheck = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$packageNameCheck = $packageNameCheck->toArray();
				if (count($packageNameCheck)>0) {
					if ($data->name != $packageNameCheck[0]['packName']) {
						// slug generation begins
						$i = 0;
						$data->slug = $this->slug($data->name);
						do {
							if ($i>0) {
								$data->slug = $data->slug.$i;
							}
							$i++;
							$selectString = "SELECT packId,packName FROM package WHERE slug = '{$data->slug}' AND packId!='{$data->packId}'";
							$packageSlugs = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$packageSlugs = $packageSlugs->toArray();
						} while(count($packageSlugs)>0);

						$updateValues['slug'] = $data->slug;
						// slug generation ends
					}
				}
				
				$update->set($updateValues);

				$update->where(array('packId'  =>  $data->packId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$count = $statement->execute()->getAffectedRows();
				$result->id =$data->packId;
				//end saving package in package table	
				//saving course if custom category is selected	
				if($data->packCategory == 4  && count($data->courseArray)>0)
				{	
					$courseArray=$data->courseArray;
					$count=count($courseArray);
					$adapter = $this->adapter;
					$query="Insert into package_courses (packId,courseId) VALUES " ;					
					$queryVals ="";
						for ($i=0; $i<$count;$i++) {
							if($i>0){
								 $query .= ",";
							}
						     $query .= "('".$data->packId."','" .$courseArray[$i]."')";
						}
					$package_courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$result->status1 = 1;
					$result->message1 = 'Congratulations. Package created !!!';
				}
				//assigning users to packages in package_user
				if(($data->packType == 2 || $data->packType == 3 ) && count($data->studentArray)>0)
				{
					$studentArray=$data->studentArray;
					$count=count($studentArray);
					$adapter = $this->adapter;
					$query="INSERT INTO package_user (userId,userType,packId,packageAssigned,Paid,packageDeactiveDate) VALUES " ;
					$queryVals ="";
					for ($i=0; $i<$count;$i++) {
						if($i>0){
							 $query .= ",";
						}
						$query .= "('".$studentArray[$i]."',4,'".$data->packId."',0,0,DATE_ADD(now(), INTERVAL 1 YEAR) )";
					}
					$package_user = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$result->status1 = 1;
					$result->message2 = 'Congratulations. Package created !!!';
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = 'Congratulations. Package Modified!!!';
				}else{
					$result->status = 0;
					$result->message = 'You cannot Edit package as you dont have verified course/course category. Contact Admin.';
				}
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		/*
			Function	: 	canCreatePackage
			params		:   userId and userRole
			Author		:	mackayush
			return		:	status =1 if sucessfully saved
			this function is used to check if intitute and instructor have valid courses and can create package or not.					
		*/	
		public function canCreatePackage($data) {
			$result = new stdClass();
			try {
					//userRole
					if($data->userRole == 5)
					{
						$result->status = 1;
						
					}
					elseif($data->userRole == 1 || $data->userRole == 2)
					{
						$adapter= $this->adapter;
						$sql = new Sql($adapter);
						$query = "SELECT id FROM courses  where ownerId= $data->userId AND deleted =0 /*AND approved = 1 AND liveForStudent = 1*/";
						//$query = "SELECT id FROM courses  where ownerId= $data->userId AND deleted =0 AND approved = 1";
						$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$coursescheck = $courses->toArray();
						if(count($coursescheck)>0)
						{
							$result->status = 1;
						}else{
							$result->status = 0;
						}
					}
					else{
						$result->status = 0;
					}
					return $result;
			} catch (Exception $e) {
					$result->status = 0;
					$result->message = $e->getMessage();
					return $result;
			}
		}


		/*
			Function	: 	isCatValid
			params		:   category id
			Author		:	mackayush
			return		:	status =1 if sucessfully saved
			this function is used to check if intitute and instructor have valid courses and can create package or not.					
		*/	
		public function isCatValid($data){
			$result = new stdClass();
			try {	
					if($data->packCategory == 3 && ($data->userRole == 1 || $data->userRole == 2))
					{
							$adapter= $this->adapter;
							$sql = new Sql($adapter);
							$query = "SELECT c.id FROM courses c left join  course_categories cc  ON c.id = cc.courseId
										WHERE c.ownerId=$data->userId AND cc.categoryId = $data->catID AND liveForStudent =1 AND deleted =0 AND approved =1 ";
							$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$courses = $courses->toArray();
							if(count($courses)>0)
							{
								$result->status = 1;	
							}else{
								$result->status = 0;		
							}
					 }else{
						$result->status=1;
					}
					return $result;
			} catch (Exception $e) {
					$result->status = 0;
					$result->message = $e->getMessage();
					return $result;
			}
		}
		/*
			Function	: 	savePackageImage
			params		:   packageId and image url
			Author		:	mackayush
			return		:	status =1 if sucessfully saved
			this function is used to save package image url which had uploaded while creating/Edit package in amazon bucket			
		*/	
		public function savePackageImage($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$where = array('packId' => $data->packId);
				$sql = new Sql($adapter);
				//deleting previous image from cloud
				$select = $sql->select();
				$select->from('package');
				$select->where($where);
				$select->columns(array('image'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$pic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$pic = $pic->toArray();
				$pic = $pic[0]['image'];
				$pic = substr($pic, 37);
				if($pic != '')
				{
					require_once 'amazonDelete.php';
					deleteFile($pic);
				}
				$update = $sql->update();
				$update->table('package');
				$update->set(array('image' => $data->imageName));
				$update->where($where);
				$statement = $sql->prepareStatementForSqlObject($update);
				$count = $statement->execute()->getAffectedRows();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = 'Package Image Uploaded!';
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		
		/*
			Function	: 	getPackageForStudent
			Author		:	mackayush
			return		:	packages information if package is found else no package found
			/used in Package Module  for students	 for Active getStudentPackages function i.e enrolled packages
			student-package	 check	
		*/	
		public function getPackageForStudent($data) {
			$result = new stdClass();
			try {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT p.packId, p.packName, p.packDescription,p.packType,p.packCategory,p.image,p.price,p.priceINR,up. 	packageActiveDate as  startDate,up.packageDeactiveDate as endDate ,CASE 
						WHEN ur.roleId =1
						THEN (
						SELECT name
						FROM institute_details
						WHERE userId = p.ownerId
						)
						WHEN ur.roleId =2
						THEN (
						SELECT CONCAT( firstname, ' ', lastname )
						FROM professor_details
						WHERE userId = p.ownerId
						)
						WHEN ur.roleId =3
						THEN (
						
						SELECT CONCAT( firstname, ' ', lastname )
						FROM publisher_details
						WHERE userId = p.ownerId
						)
						ELSE 'Integro.io, Inc'
						END AS username
						FROM package p, user_roles ur , package_user up
						WHERE ur.userId = p.ownerId AND p.packId=up.packId AND up.Paid=1 AND p.Active =1 AND   up.userId= $data->userId";
					//print_r($query);
					$packages = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$result->packages = $packages->toArray();
					if (count($result->packages) == 0) {
						$result->status = 1;
						$result->exception = "No Packages Found";
						return $result;
					}
					//print_r($result);
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
						require_once 'amazonRead.php';
						$result->packages[0]['image'] = getSignedURL($result->packages[0]['image']);
						//print_r(getSignedURL($result->packages['image']));
					}
					//print_r($result);
					$result->status = 1;
					return $result;
			} catch (Exception $e) {
					$result->status = 0;
					$result->exception = $e->getMessage();
					return $result;
			}
		}
		/*
			Function	: 	getAllPackageForStudent
			Author		:	mackayush
			return		:	packages information if package is found else no package found
			/used in Package Module  for students	 for all-package function i.e all Active packages avialable	
		*/	
		public function getAllPackageForStudent($data) {
		
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				if (isset($data->userId) && !empty($data->userId)) {
					$query = "SELECT p.packId, p.packName, p.packDescription, p.packType, p.packCategory, p.image, p.slug, p.price, p.priceINR, p.startDate, p.endDate,
								CASE
								WHEN ur.roleId =1
								THEN (

								SELECT name
								FROM institute_details
								WHERE userId = p.ownerId
								)
								WHEN ur.roleId =2
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM professor_details
								WHERE userId = p.ownerId
								)
								WHEN ur.roleId =3
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM publisher_details
								WHERE userId = p.ownerId
								)
								ELSE 'Integro.io, Inc'
								END AS username
								FROM package p, user_roles ur
								WHERE ur.userId = p.ownerId
								AND (
								p.packType =1
								)
								AND (
								p.Active = 	1
								)
								AND p.packId NOT
								IN (
								SELECT packId
								FROM package_user
								WHERE userId =$data->userId AND Paid =1
								)";
				} elseif (isset($data->professorId) && !empty($data->professorId)) {
					$query = "SELECT p.packId, p.packName, p.packDescription, p.packType, p.packCategory, p.image, p.slug, p.price, p.priceINR, p.startDate, p.endDate
								FROM package p
								WHERE (
								p.packType =1 AND p.Active = 1 AND p.ownerId = {$data->professorId}
								)";
				} elseif (isset($data->instituteId) && !empty($data->instituteId)) {
					$query = "SELECT p.packId, p.packName, p.packDescription, p.packType, p.packCategory, p.image, p.slug, p.price, p.priceINR, p.startDate, p.endDate
								FROM package p
								WHERE (
								p.packType =1  AND p.Active = 1 AND p.ownerId = {$data->instituteId}
								)";
				} else {
					$query = "SELECT p.packId, p.packName, p.packDescription, p.packType, p.packCategory, p.image, p.slug, p.price, p.priceINR, p.startDate, p.endDate
								FROM package p
								WHERE (
								p.packType =1  AND p.Active = 1
								)";
				}
				$packages = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$packages=$packages->toArray();
				//$result->packages = $packages;
				//
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				if (isset($data->userId) && !empty($data->userId)) {
					$query = "SELECT p.packId, p.packName, p.packDescription, p.packType, p.packCategory, p.image, p.price, p.slug, p.priceINR, p.startDate, p.endDate, up.packageActiveDate AS startDate, up.packageDeactiveDate AS endDate,
								CASE
								WHEN ur.roleId =1
								THEN (
								SELECT name
								FROM institute_details
								WHERE userId = p.ownerId
								)
								WHEN ur.roleId =2
								THEN (
								SELECT CONCAT( firstname, ' ', lastname )
								FROM professor_details
								WHERE userId = p.ownerId
								)
								WHEN ur.roleId =3
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM publisher_details
								WHERE userId = p.ownerId
								)
								ELSE 'Integro.io, Inc'
								END AS username
								FROM package p, user_roles ur, package_user up
								WHERE ur.userId = p.ownerId
								AND p.packType =2
								and p.packId=up.packId
								AND Paid =0  AND p.Active = 1 AND up.userId =$data->userId";
					$packages2 = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$packages2=$packages2->toArray();
					$newresult = array_merge($packages, $packages2);
				} else {
					$newresult = $packages;
				}
				
					
				$result->packages=$newresult;
				//$result->packages.push($packages2->toArray());
				//print_r($result->packages);
				if (count($result->packages) == 0) {
					$result->status = 1;
					$result->exception = "No Packages Found";
					return $result;
				}
				/*if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$result->packages[0]['image'] = getSignedURL($result->packages[0]['image']);
				}*/
				if (count($result->packages) > 0) {
					foreach ($result->packages as $key => $value) {
						if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
							require_once 'amazonRead.php';
							$result->packages[$key]['image'] = getSignedURL($result->packages[$key]['image']);
							//print_r(getSignedURL($result->packages['image']));
						}
					}
				}
				//print_r($result);
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getPackageDetails($data) {
			$i=0;
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				if (isset($data->packId) && !empty($data->packId)) {
					$subQuery = 'p.packId = '.$data->packId;
				} else {
					$subQuery = "p.slug = '{$data->slug}'";
				}
				$query = "SELECT p.packId, p.packName, p.packDescription, p.packType, p.packCategory, p.image, p.price, p.priceINR, p.startDate, p.endDate,p.ownerId, ur.roleId as ownerRoleId, CASE
							WHEN ur.roleId =1
							THEN (

							SELECT name
							FROM institute_details
							WHERE userId = p.ownerId
							)
							WHEN ur.roleId =2
							THEN (

							SELECT CONCAT( firstname, ' ', lastname )
							FROM professor_details
							WHERE userId = p.ownerId
							)
							WHEN ur.roleId =3
							THEN (

							SELECT CONCAT( firstname, ' ', lastname )
							FROM publisher_details
							WHERE userId = p.ownerId
							)
							ELSE 'Integro.io, Inc'
							END AS username
							FROM package p, user_roles ur
							WHERE ur.userId = p.ownerId AND p.Active =1 AND $subQuery";
				$packagesCat = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$packagesCat = $packagesCat->toArray();
				$result->packages = $packagesCat;
				if(count($result->packages)==0)
				{
					$result->status = 404;
					$result->exception = "Package Not Found";
					return $result;	
				} else {
					$data->packId = $packagesCat[0]['packId'];
				}
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$result->packages[0]['image'] = getSignedURL($result->packages[0]['image']);
					//print_r(getSignedURL($result->packages['image']));
				} else {
					$result->packages[0]['image'] = $this->sitePath."student/assets/pages/img/background/32.jpg";
				}
				if($packagesCat[0]['packCategory'] == 1)
				{
					$adapter= $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR,c.description, CASE WHEN ur.roleId =1 THEN ( 
								SELECT name
								FROM institute_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =2
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM professor_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =3
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM publisher_details
								WHERE userId = c.ownerId
								)
								ELSE 'NA'
								END AS username
								FROM courses c, user_roles ur
								WHERE ur.userId = c.ownerId
								AND deleted =0
								AND approved =1
								AND c.liveForStudent =1
								ORDER BY weight ASC, id DESC";
					$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$result->packages[0]['courses'] = $courses->toArray();
					//print_r($result);
				}
				if($packagesCat[0]['packCategory'] == 2)
				{
					$adapter= $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR,c.description, CASE WHEN ur.roleId =1 THEN ( 
								SELECT name
								FROM institute_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =2
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM professor_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =3
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM publisher_details
								WHERE userId = c.ownerId
								)
								ELSE 'NA'
								END AS username FROM courses c,package p, user_roles ur
								WHERE ur.userId = c.ownerId
								AND p.packId= $data->packId AND c.ownerId= p.ownerId AND deleted =0 AND approved =1 AND c.liveForStudent =1 ORDER BY weight ASC , id DESC";
					$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$result->packages[0]['courses']  = $courses->toArray();
					//print_r($result);
				}
				if($packagesCat[0]['packCategory'] == 3)
				{
					$adapter= $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT c.id, c.name, c.image, c.studentPrice,c.description, c.studentPriceINR , CASE WHEN ur.roleId =1 THEN ( 
								SELECT name
								FROM institute_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =2
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM professor_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =3
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM publisher_details
								WHERE userId = c.ownerId
								)
								ELSE 'NA'
								END AS username FROM courses c, package p, course_categories cc , user_roles ur
								WHERE ur.userId = c.ownerId
								AND p.packId = $data->packId AND cc.categoryId = p.catID AND c.id = cc.courseId AND deleted =0 AND approved =1 AND c.liveForStudent =1  ORDER BY weight ASC , id DESC";
					$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$result->packages[0]['courses']  = $courses->toArray();
					
				}
				if($packagesCat[0]['packCategory'] == 4)
				{
					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR, c.description,c.liveDate as startDate,c.endDate , CASE WHEN ur.roleId =1 THEN ( 
								SELECT name
								FROM institute_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =2
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM professor_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =3
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM publisher_details
								WHERE userId = c.ownerId
								)
								ELSE 'NA'
								END AS username FROM courses c ,package_courses cp , user_roles ur
								WHERE ur.userId = c.ownerId
								AND cp.packId= $data->packId AND c.id=cp.courseId AND c.deleted =0 AND c.approved =1 AND  c.liveForStudent =1   ORDER BY liveDate DESC ";
					$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$result->packages[0]['courses']  = $courses->toArray();
				}	
				/*foreach ($courses as $course) {
						$query="select id subjectId, name subjectName from subjects where courseId = {$course['course_id']} and deleted=0"; 
						$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$course['subjects'] = $subjects->toArray();
						if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
							require_once 'amazonRead.php';
							$course['coverPic'] = getSignedURL($course['coverPic']);
						}
						$result->courses[] = $course;
					}*/
				$enrolled=0;
				foreach ($result->packages[0]['courses'] as $c) {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT sc.id FROM student_course sc WHERE  sc.course_id = ".$c['id']."; ";
					$courseIds=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$c['enroll'] = $courseIds->toArray();
					
					if(count($c['enroll'])>0)
					{
						$enrolled=count($c['enroll']);
					}
					$result->packages[0]['courses'][$i][$i]['enroll']=$enrolled;
					
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
						require_once 'amazonRead.php';
						$result->packages[0]['courses'][$i]['image'] = getSignedURL($result->packages[0]['courses'][$i]['image']);
					} else {
						$result->packages[0]['courses'][$i]['image'] = $this->sitePath."student/assets/pages/img/background/32.jpg";
					}
				}
				$result->packageDetails = $result->packages[0];
				//var_dump($result->packageDetails);
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getPackagestudent($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$query = "SELECT count( up.id ) as count, CASE WHEN p.packCategory =2 THEN ( 
							SELECT count(c.id) FROM courses c WHERE c.ownerId = $data->userId  AND deleted =0 AND approved =1
							)
							WHEN p.packCategory =3
							THEN (
							SELECT count(c.id) FROM courses c  LEFT OUTER JOIN course_categories cc ON cc.courseId = c.id WHERE deleted =0 AND approved =1 AND cc.categoryId = p.catID AND c.ownerId = $data->userId 
							)
							WHEN p.packCategory =4
							THEN (
							SELECT count(c.id) FROM courses c
							WHERE c.ownerId = $data->userId  AND deleted =0 AND approved =1 and c.id in(select courseId from package_courses where packId= $data->packId)
							)
							ELSE 'NA'
							END AS courses 
							FROM package p, package_user up WHERE p.ownerId =$data->userId AND p.packId =$data->packId AND up.packId = p.packId AND up.Paid=0 AND p.Active =1";
				//print_r($query);
				$packages = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->packages = $packages->toArray();
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		/*
			Function	: 	getPackageprice
			params		:   packageId and userId
			Author		:	mackayush
			return		:	status =1 if sucessfully saved
			this function is used to get the package price return price the price or else package is already subscribed	
			check	
		*/	
		public function getPackageprice($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);//$data->packId
				$query = "SELECT price,priceINR FROM package p WHERE  p.packId=$data->packId AND p.Active =1 and p.packId not in (select packId from package_user where packId = $data->packId and userId = $data->userId and Paid =1 and packageDeactiveDate > NOW())";
				//print_r($query);
				$packages = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->packages = $packages->toArray();
				$result->status = 1;
				if(count($result->packages)==0)
				{	$result->status = 0;
					$result->message = "Already Subscribed to package";
				}
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function getCreatedPackage($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$query = "SELECT p.packId, p.packName, p.packDescription, p.packType, p.packCategory, p.image, p.price, p.priceINR, p.startDate AS startDate, p.endDate AS endDate, count( up.id ) AS students
							FROM package p
							LEFT JOIN package_user up ON p.packId = up.packId and up.Paid=1
							WHERE   p.ownerId= $data->userId AND p.Active =1
							GROUP BY p.packId ";
				//print_r($query);
				$packages = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->packages = $packages->toArray();
				if (count($result->packages) == 0) {
					$result->status = 0;
					$result->exception = "No Packages Found";
					return $result;
				}
				//print_r($result);
				if (count($result->packages) > 0) {
					foreach ($result->packages as $key => $value) {
						if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
							require_once 'amazonRead.php';
							$result->packages[$key]['image'] = getSignedURL($result->packages[$key]['image']);
							//print_r(getSignedURL($result->packages['image']));
						}
					}
				}
				//print_r($result);
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		/*
			Function	: 	getViewPackage
			Author		:	mackayush
			return		:	status =1 if sucessfully saved
			this function is used to get the details admin/view_editPackage.php page for intitute/instructor
		*/	
		public function getViewPackage($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$query="SELECT p.packId, p.packName, p.packDescription,p.catID, p.packType, p.packCategory, p.image, p.price, p.priceINR, p.startDate AS startDate, p.endDate AS endDate, count( up.id ) AS students
						FROM package p
						LEFT JOIN package_user up ON up.packId=p.packId AND up.Paid=1
						WHERE p.ownerId= $data->userId AND p.Active =1 and p.packId=$data->packId
						GROUP BY p.packId ";
				//print_r($query);
				//print_r($query);
				$packages = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				
				$result->packages = $packages->toArray();
				if (count($result->packages) == 0) {
					$result->status = 0;
					$result->exception = "No Packages Found";
					return $result;
				}
				
				//courses of package.
				$cat=$result->packages[0]['catID'];
				
				if($result->packages[0]['packCategory']==2)
				{
					if($data->userRole == 5){
					$query ="SELECT c.id, c.name, c.image  FROM courses c WHERE  deleted =0 AND approved =1 AND liveForStudent =1";
					}
					elseif($data->userRole == 1 || $data->userRole == 2){
					$query ="SELECT c.id, c.name, c.image  FROM courses c WHERE c.ownerId = $data->userId  AND deleted =0 AND approved =1 AND liveForStudent =1";
					}
					$courses=	$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				}
				else if($result->packages[0]['packCategory']==1)//
				{	
					$query ="SELECT c.id, c.name, c.image  FROM courses c WHERE  deleted =0 AND approved =1 AND liveForStudent =1 ";
					$courses=	$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				}
				else if($result->packages[0]['packCategory']==3)//
				{	
					if($data->userRole == 5){
					$query ="SELECT c.id, c.name, c.image FROM courses c  LEFT OUTER JOIN course_categories cc ON cc.courseId = c.id WHERE deleted =0 AND approved =1 AND liveForStudent =1 AND cc.categoryId = $cat GROUP BY c.id ORDER BY liveDate DESC";
					}
					elseif($data->userRole == 1 || $data->userRole == 2){
					$query ="SELECT c.id, c.name, c.image FROM courses c  LEFT OUTER JOIN course_categories cc ON cc.courseId = c.id WHERE deleted =0 AND approved =1 AND liveForStudent =1 AND cc.categoryId = $cat AND c.ownerId = $data->userId GROUP BY c.id ORDER BY liveDate DESC";
					}
					$courses=	$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				}
				else if($result->packages[0]['packCategory']==4)
				{	if($data->userRole == 5){
					$query ="SELECT c.id, c.name, c.image  FROM courses c 
							WHERE  deleted =0 AND approved =1 AND liveForStudent =1 and c.id in(select courseId from package_courses where packId= $data->packId)";
					}
					elseif($data->userRole == 1 || $data->userRole == 2){
						$query ="SELECT c.id, c.name, c.image  FROM courses c 
							WHERE c.ownerId = $data->userId  AND deleted =0 AND approved =1 AND liveForStudent =1 and c.id in(select courseId from package_courses where packId= $data->packId)";					}
					$courses=	$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				}
				else{
					
				}
				$result->packages[0]['course'] = $courses->toArray();
				$result->packages[0]['image1'] = $result->packages[0]['image'];
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$result->packages[0]['image'] = getSignedURL($result->packages[0]['image']);
					//print_r(getSignedURL($result->packages['image']));
				}
				//print_r($result);
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		//get-packageCourses
		//@Ayush function to display courses depending on package in package table for courses of enrolled packges in Mysubsciption.ph page
		public function getPackagesCourses($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$query = "SELECT p.packCategory FROM package p WHERE  p.packId= $data->packId AND p.Active =1";
				$packagesCat = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$packagesCat = $packagesCat->toArray();
			
				if($packagesCat[0]['packCategory'] == 1)
				{
					$adapter= $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR,c.description, CASE WHEN ur.roleId =1 THEN ( 
								SELECT name
								FROM institute_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =2
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM professor_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =3
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM publisher_details
								WHERE userId = c.ownerId
								)
								ELSE 'NA'
								END AS username
								FROM courses c, user_roles ur
								WHERE ur.userId = c.ownerId
								AND deleted =0
								AND approved =1
								AND liveForStudent =1
								ORDER BY weight ASC , id DESC";
					$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$result->courses = $courses->toArray();
					//print_r($result);
				}
				if($packagesCat[0]['packCategory'] == 2)
				{
					$adapter= $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR,c.description, CASE WHEN ur.roleId =1 THEN ( 
					SELECT name
					FROM institute_details
					WHERE userId = c.ownerId
					)
					WHEN ur.roleId =2
					THEN (

					SELECT CONCAT( firstname, ' ', lastname )
					FROM professor_details
					WHERE userId = c.ownerId
					)
					WHEN ur.roleId =3
					THEN (

					SELECT CONCAT( firstname, ' ', lastname )
					FROM publisher_details
					WHERE userId = c.ownerId
					)
					ELSE 'NA'
					END AS username FROM courses c,package p, user_roles ur
					WHERE ur.userId = c.ownerId
					AND p.packId= $data->packId AND c.ownerId= p.ownerId AND deleted =0 AND liveForStudent =1 AND approved =1  ORDER BY weight ASC , id DESC";
						$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$result->courses = $courses->toArray();
					//print_r($result);
				}
				if($packagesCat[0]['packCategory'] == 3)
				{
					$adapter= $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT c.id, c.name, c.image, c.studentPrice,c.description, c.studentPriceINR , CASE WHEN ur.roleId =1 THEN ( 
								SELECT name
								FROM institute_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =2
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM professor_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =3
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM publisher_details
								WHERE userId = c.ownerId
								)
								ELSE 'NA'
								END AS username FROM courses c, package p, course_categories cc , user_roles ur
								WHERE ur.userId = c.ownerId
								AND p.packId = $data->packId AND cc.categoryId = p.catID AND liveForStudent =1 AND c.id = cc.courseId AND deleted =0 AND approved =1  ORDER BY weight ASC , id DESC";
									$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
									$result->courses = $courses->toArray();
					
				}
				if($packagesCat[0]['packCategory'] == 4)
				{
					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR, c.description,c.liveDate as startDate,c.endDate , CASE WHEN ur.roleId =1 THEN ( 
								SELECT name
								FROM institute_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =2
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM professor_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =3
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM publisher_details
								WHERE userId = c.ownerId
								)
								ELSE 'NA'
								END AS username FROM courses c ,package_courses cp , user_roles ur
								WHERE ur.userId = c.ownerId
								AND cp.packId= $data->packId AND c.id=cp.courseId AND c.deleted =0 AND c.approved =1 AND c.liveForStudent =1   ORDER BY liveDate DESC ";
					$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$result->courses = $courses->toArray();
				}
				
				// 
				$i=0;
					
				/*foreach ($courses as $course) {
						$query="select id subjectId, name subjectName from subjects where courseId = {$course['course_id']} and deleted=0"; 
						$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$course['subjects'] = $subjects->toArray();
						if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
							require_once 'amazonRead.php';
							$course['coverPic'] = getSignedURL($course['coverPic']);
						}
						$result->courses[] = $course;
					}*/
				foreach ($result->courses as $c) {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT sc.id FROM student_course sc WHERE sc.user_id=$data->userId AND sc.course_id = {$c['id']} AND sc.packId>0 AND sc.courseEndDate>now(); ";
					$courseIds=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$c['enroll'] = $courseIds->toArray();
					$enrolled=0;
					if(count($c['enroll'])>0)
					{
						$enrolled=1;
					}
					$result->courses[$i]['enroll']=$enrolled;
					
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
						require_once 'amazonRead.php';
						$result->courses[$i]['image'] = getSignedURL($result->courses[$i]['image']);
					}
					
					//print_r($i);
					//print_r("hello");
					$query="select id subjectId, name subjectName from subjects where courseId = {$c['id']} and deleted=0"; 
					$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$k = $subjects->toArray();
					
					$result->courses[$i]['subjects'] = $k;
				
					$i=$i+1;
				}
				
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		/*
			Function	: 	getAllpackageCourses
			Author		:	mackayush
			return		:	status =1 if sucessfully saved
			this function is used to get the all the course of package when Packages Available packages gets click in subscription.php
			
		*/			
		public function getAllpackageCourses($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$query = "SELECT p.packCategory FROM package p WHERE  p.packId= $data->packId  AND p.Active =1 ";
				$packagesCat = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$packagesCat = $packagesCat->toArray();
				$query="";
				if($packagesCat[0]['packCategory'] == 1)
				{
					$adapter= $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR,c.description,c.liveForStudent, CASE WHEN ur.roleId =1 THEN ( 
					SELECT name
					FROM institute_details
					WHERE userId = c.ownerId
					)
					WHEN ur.roleId =2
					THEN (

					SELECT CONCAT( firstname, ' ', lastname )
					FROM professor_details
					WHERE userId = c.ownerId
					)
					WHEN ur.roleId =3
					THEN (

					SELECT CONCAT( firstname, ' ', lastname )
					FROM publisher_details
					WHERE userId = c.ownerId
					)
					ELSE 'NA'
					END AS username
					FROM courses c, user_roles ur
					WHERE ur.userId = c.ownerId
					AND deleted =0
					AND approved =1
					AND liveForStudent =1
					ORDER BY weight ASC , id DESC";
				}
				if($packagesCat[0]['packCategory'] == 2)
				{
					$adapter= $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR,c.description, CASE WHEN ur.roleId =1 THEN ( 
								SELECT name
								FROM institute_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =2
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM professor_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =3
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM publisher_details
								WHERE userId = c.ownerId
								)
								ELSE 'NA'
								END AS username FROM courses c,package p, user_roles ur
								WHERE ur.userId = c.ownerId
								AND p.packId= $data->packId AND c.ownerId= p.ownerId AND deleted =0 AND  approved =1 AND liveForStudent =1 ORDER BY weight ASC , id DESC";

				}
				if($packagesCat[0]['packCategory'] == 3)
				{
					$adapter= $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT  c.id, c.name, c.image, c.studentPrice,c.description, c.studentPriceINR , CASE WHEN ur.roleId =1 THEN ( 
					SELECT name
					FROM institute_details
					WHERE userId = c.ownerId
					)
					WHEN ur.roleId =2
					THEN (

					SELECT CONCAT( firstname, ' ', lastname )
					FROM professor_details
					WHERE userId = c.ownerId
					)
					WHEN ur.roleId =3
					THEN (

					SELECT CONCAT( firstname, ' ', lastname )
					FROM publisher_details
					WHERE userId = c.ownerId
					)
					ELSE 'NA'
					END AS username FROM courses c, package p, course_categories cc , user_roles ur
					WHERE ur.userId = c.ownerId  /*AND c.ownerId =p.ownerId*/
					AND p.packId = $data->packId AND cc.categoryId = p.catID AND c.id = cc.courseId AND deleted =0  AND approved =1 AND liveForStudent =1 ORDER BY weight ASC , id DESC";
				}
				if($packagesCat[0]['packCategory'] == 4)
				{
					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR, c.description,c.liveDate as startDate,c.endDate , CASE WHEN ur.roleId =1 THEN ( 
								SELECT name
								FROM institute_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =2
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM professor_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =3
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM publisher_details
								WHERE userId = c.ownerId
								)
								ELSE 'NA'
								END AS username FROM courses c ,package_courses cp , user_roles ur
								WHERE ur.userId = c.ownerId
								AND cp.packId= $data->packId AND c.id=cp.courseId AND c.deleted =0 AND c.approved =1 AND liveForStudent =1  ORDER BY liveDate DESC ";
				}
		
				//print_r($query);
				$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->courses = $courses->toArray();
				//print_r($result);
				/*for($i=0 $i<count($result->courses);i++)
				{
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
						require_once 'amazonRead.php';
						$result->courses[$i]['image'] = getSignedURL($result->courses[$i]['image']);
					}
				// 
				}*/
				$i=0;
				
				/*foreach ($courses as $course) {
					$query="select id subjectId, name subjectName from subjects where courseId = {$course['course_id']} and deleted=0"; 
					$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$course['subjects'] = $subjects->toArray();
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
						require_once 'amazonRead.php';
						$course['coverPic'] = getSignedURL($course['coverPic']);
					}
					$result->courses[] = $course;
				}*/
				foreach ($result->courses as $c) {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT sc.id FROM student_course sc WHERE sc.user_id=$data->userId AND sc.course_id = ".$c['id']."; ";
					$courseIds=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$c['enroll'] = $courseIds->toArray();
					$enrolled=0;
					if(count($c['enroll'])>0)
					{
						$enrolled=1;
					}
					$result->courses[$i]['enroll']=$enrolled;
					
					//print_r($i);
					//print_r("hello");
					$query="SELECT id subjectId, name subjectName FROM subjects WHERE courseId = {$c['id']} AND deleted=0"; 
					$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$k = $subjects->toArray();
					
					$result->courses[$i]['subjects'] = $k;
					
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
						require_once 'amazonRead.php';
						$result->courses[$i]['image'] = getSignedURL($result->courses[$i]['image']);
					}
					//$result->courses[] = $c;
					$i=$i+1;
				}
				
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		/* 
		package detail page
		
		*/
		public function showAllpackageCourses($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				if (isset($data->packId) && !empty($data->packId)) {
					$query = "SELECT p.packId, p.packCategory FROM package p WHERE  p.packId = $data->packId AND p.Active =1";
				} else {
					$query = "SELECT p.packId, p.packCategory FROM package p WHERE  p.slug = '{$data->slug}' AND p.Active =1";
				}
				$packagesCat = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$packagesCat = $packagesCat->toArray();
				if (count($packagesCat) > 0) {
					$data->packId = $packagesCat[0]['packId'];
				}
				$query = "";
				if($packagesCat[0]['packCategory'] == 1)
				{
					$adapter= $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR,c.description,c.liveForStudent, CASE WHEN ur.roleId =1 THEN ( 
								SELECT name
								FROM institute_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =2
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM professor_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =3
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM publisher_details
								WHERE userId = c.ownerId
								)
								ELSE 'NA'
								END AS username, ld.id AS userId
								FROM courses c, user_roles ur, login_details ld
								WHERE ur.userId = c.ownerId
								AND ld.id=c.ownerId
								AND deleted =0
								AND liveDate != ''
								AND approved =1
								AND liveForStudent =1
								ORDER BY weight ASC , id DESC";
				}
				if($packagesCat[0]['packCategory'] == 2)
				{
					$adapter= $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR,c.description, CASE WHEN ur.roleId =1 THEN ( 
								SELECT name
								FROM institute_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =2
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM professor_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =3
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM publisher_details
								WHERE userId = c.ownerId
								)
								ELSE 'NA'
								END AS username, ld.id AS userId
								FROM courses c,package p, user_roles ur, login_details ld
								WHERE ur.userId = c.ownerId
								AND p.packId= $data->packId AND c.ownerId= p.ownerId AND ld.id=c.ownerId AND deleted =0 AND liveForStudent =1 AND  approved =1  ORDER BY weight ASC , id DESC";
				
				}
				if($packagesCat[0]['packCategory'] == 3)
				{
					$adapter= $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT  c.id, c.name, c.image, c.studentPrice,c.description, c.studentPriceINR , CASE WHEN ur.roleId =1 THEN ( 
								SELECT name
								FROM institute_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =2
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM professor_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =3
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM publisher_details
								WHERE userId = c.ownerId
								)
								ELSE 'NA'
								END AS username, ld.id AS userId
								FROM courses c, package p, course_categories cc , user_roles ur, login_details ld
								WHERE ur.userId = c.ownerId /* AND c.ownerId =p.ownerId*/
								AND p.packId = $data->packId AND cc.categoryId = p.catID AND c.id = cc.courseId AND ld.id=c.ownerId AND deleted =0 AND liveForStudent =1 AND approved =1  ORDER BY weight ASC , id DESC";
				}
				if($packagesCat[0]['packCategory'] == 4)
				{
					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT c.id, c.name, c.image, c.studentPrice, c.studentPriceINR, c.description,c.liveDate as startDate,c.endDate , CASE WHEN ur.roleId =1 THEN ( 
								SELECT name
								FROM institute_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =2
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM professor_details
								WHERE userId = c.ownerId
								)
								WHEN ur.roleId =3
								THEN (

								SELECT CONCAT( firstname, ' ', lastname )
								FROM publisher_details
								WHERE userId = c.ownerId
								)
								ELSE 'NA'
								END AS username, ld.id AS userId
								FROM courses c, package_courses cp, user_roles ur, login_details ld
								WHERE ur.userId = c.ownerId
								AND cp.packId= $data->packId AND c.id=cp.courseId AND ld.id=c.ownerId AND c.deleted =0 AND liveForStudent =1 AND c.approved =1  ORDER BY liveDate DESC ";
				}
				//print_r($query);
				$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->courses = $courses->toArray();
				//print_r($result);
				/*for($i=0 $i<count($result->courses);i++)
				{
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
						require_once 'amazonRead.php';
						$result->courses[$i]['image'] = getSignedURL($result->courses[$i]['image']);
					}
				// 
				}*/
				/*foreach ($courses as $course) {
					$query="select id subjectId, name subjectName from subjects where courseId = {$course['course_id']} and deleted=0"; 
					$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$course['subjects'] = $subjects->toArray();
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
						require_once 'amazonRead.php';
						$course['coverPic'] = getSignedURL($course['coverPic']);
					}
					$result->courses[] = $course;
				}*/
				foreach ($result->courses as $i=>$c) {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					$query = "SELECT sc.id FROM student_course sc WHERE sc.course_id = ".$c['id']."; ";
					$courseIds=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$c['enroll'] = $courseIds->toArray();
					$enrolled=0;
					if(count($c['enroll'])>0)
					{
						$enrolled=count($c['enroll']);
					}
					$result->courses[$i]['enroll']=$enrolled;
					
					//fetching rating
					require_once 'Student.php';
					$s = new Student();
					$result->courses[$i]['rating'] = $s->getCourseRatings($c['id']);

					//print_r($i);
					//print_r("hello");
					$query="SELECT id subjectId, name subjectName from subjects where courseId = {$c['id']} and deleted=0"; 
					$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$k = $subjects->toArray();
					
					$result->courses[$i]['subjects'] = $k;

					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
						require_once 'amazonRead.php';
						$result->courses[$i]['image'] = getSignedURL($result->courses[$i]['image']);
					} else {
						$result->courses[$i]['image'] = $this->sitePath."student/assets/pages/img/background/32.jpg";
					}
					//$result->courses[] = $c;
				}
				
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
	
		//insert function -- courseid for pakages for userid
		//@params - coureid,packageId,userId 
		public function setUserCoursePackage($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$query = "SELECT sc.id FROM student_course sc WHERE sc.user_id=$data->userId AND sc.course_id = $data->courseId ";
				$courseIds=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$Regis = $courseIds->toArray();
				if(count($Regis)>0)
				{
					$result->status = 0;
					$result->message = "You are already Enrolled in course";
					return $result;
				}
				
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$query = "INSERT INTO package_subscribe(userId,courseId,packageId) VALUES ($data->userId,$data->courseId,$data->packageId)  ";
				$userSubsCourses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				/*
				$querystudent_course = "INSERT INTO student_course(course_id,user_id,key_id,price,packId,courseEndDate) VALUES ($data->courseId,$data->userId,0,0,$data->packageId,$data->enddate)  ";
				print_r($querystudent_course);
				$userSubsCourses = $adapter->query($querystudent_course, $adapter::QUERY_MODE_EXECUTE);
				*/
				$adapter = $this->adapter;
				$packages = new TableGateway('student_course', $adapter, null, new HydratingResultSet());
				$insert = array(
					'course_id' => $data->courseId,
					'user_id'=>$data->userId,
					'key_id' =>0,
					'price'=>0,
					'packId'=>$data->packageId,
					'courseEndDate'=>$data->enddate
								
					);
				$packages->insert($insert);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$res = $packages->getLastInsertValue();

				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = "Unable to Subscribe. Please try again later";
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		/*	package purchase BY THE STUDENT
			Function	: 	purchaseStudentPackage
			Author		:	mackayush
			this function is used to insert all the order details of the package in order_details table checking if package is not already 				purchased BY THE STUDENT
					
		*/
		public function purchaseStudentPackage($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$result = new stdClass();
			try {
				$amount = 0;
				$productId="";
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				if (isset($data->slug) && !empty($data->slug)) {
					$select = $sql->select();
					$select->from('package');
					$select->where(array(
						'slug' =>  $data->slug
					));
					$select->columns(array('packId'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$temp = $temp->toArray();
					$data->packId = $temp[0]['packId'];
				}
				//checking if user already purchase the package
				//select packId from package_user where packId = $data->packId and userId = $data->userId and Paid =1
				$select = $sql->select();
				$select->from('package_user');
				$select->where(array(
					'packId' =>  $data->packId,
					'userId'   =>  $data->userId,
					'Paid' => '1'
				));
				$select->columns(array('packId'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				if(count($temp) != 0) {
					$result->status = 0;
					$result->message = 'You have already purchased this Package.';
					return $result;
				}
				
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				//checking if user already purchase the package
				$select = $sql->select();
				$select->from('package');
				$select->where(array(
					'packId' =>  $data->packId,
					'Active'       >    '1'
				));
				$select->columns(array('packId','price','priceINR','packName'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$packagedetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$packagedetails = $packagedetails->toArray();
				if(count($packagedetails) > 0) {
					$amount = $packagedetails[0]['price'];
				}
				else{
					$result->status = 0;
					$result->message = 'No such Package found.';
					return $result;
				}
				
				$packName=$packagedetails[0]['packName'];
				
				//print_r($amount);
				$name = '';
				$email = '';
				$number = '';
				//fetching first name of institute/professor/publisher
				$select = $sql->select();
				$select->from(array('ld'    =>  'login_details'));
				$select->where(array('ld.id'    =>  $data->userId));
				$select->columns(array(
					'id'    =>  'id',
					'email' =>  'email'
				));
				$select->join(
					array('ud'  =>  'user_details'),
					'ud.userId=ld.id',
					array('contactMobile'   =>  'contactMobile')
				);
				$select->join(
					array('sd'  =>  'student_details'),
					'sd.userId=ld.id',
					array('name'    =>  new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"))
				);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$details = $details->toArray();
				if(count($details) > 0) {
					$name = $details[0]['name'];
					if($details[0]['contactMobile'] == '')
						$details[0]['contactMobile'] = '9876543210';
					$number = $details[0]['contactMobile'];
					$email = $details[0]['email'];
				}
				$productId=$data->packId;
				//generating order id
				//print_r($amount);

				$db->beginTransaction();
				//$orderId = new TableGateway('order_details', $adapter, null, new HydratingResultSet());
				$insert = array(
					'type'              =>  4,
					'userId'            =>  $data->userId,
					'email'             =>  $email,
					'purchase_detail'   =>  $productId,
					'rate'              =>  0,
					'amount'            =>  $amount,
					'status'            =>  0,
					'payUId'            =>  0,
					'IGROTxnID'			=>  0,
					'timestamp'         =>  time(),
					'couponcode'		=>  ''
				);
				//$orderId->insert($insert);
				//$txnid = $orderId->getLastInsertValue();
				
				$query="INSERT INTO order_details (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$adapter->query("SET @orderId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);

				$result->amount = $amount;
				//$result->orderId = $txnid;
				if($result->amount <= 0) {
					$result->paymentSkip = 1;
				
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();

					$query = "SELECT @orderId as orderID";
					$orderID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$orderID = $orderID->toArray();
					$result->orderId = $orderID[0]["orderID"];
					
					$dataSkip = array();
					$dataSkip['paymentSkip'] = $result->paymentSkip;
					$dataSkip['orderId'] = $txnid;
					$this->afterIGROPay($dataSkip);
				} else {
					$result->paymentSkip = 0;

					require_once("CryptoWallet.php");
					$cw = new CryptoWallet();
					$userAddressDB = $cw->getWalletAddress($data);
					if ($userAddressDB->status == 0) {
						$result->status = 0;
						$result->message = "No User Crypto Wallet address!";
					}

					$IGROContractDB = $cw->getIGROContractData($data);
					if ($IGROContractDB->status == 0) {
						$result->status = 0;
						$result->message = "No IGRO Contract is set!";
					}

					global $siteBase;

					//$IGROTxn = new TableGateway('igro_transactions', $adapter, null, new HydratingResultSet());
					$insert = array(
						'toAddress'			=>  $IGROContractDB->ownerAddress,
						'fromAddress'		=>  $userAddressDB->wallet,
						'contractAddress'	=>  $IGROContractDB->contractAddress,
						'transactionHash'	=>  '',
						'status'			=>  'new',
						'created'			=>  date("Y-m-d H:i:s"),
						'modified'			=>  date("Y-m-d H:i:s")
					);
					//$IGROTxn->insert($insert);
					$query="INSERT INTO igro_transactions (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
					$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$adapter->query("SET @igroTxnId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);

					//$IGROTxnID = $IGROTxn->getLastInsertValue();
					//$result->IGROTxnID = $IGROTxnID;

					/*$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('order_details');
					$update->set(array(
										'IGROTxnID'	=>	$IGROTxnID
					));
					$update->where(array('orderId'		=>	$result->orderId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();*/

					$query="UPDATE order_details SET `IGROTxnID`=@igroTxnId WHERE orderId=@orderId";
					$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();

					$query = "SELECT @orderId as orderID";
					$orderID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$orderID = $orderID->toArray();
					$result->orderId = $orderID[0]["orderID"];

					$query = "SELECT @igroTxnId as igroTxnID";
					$igroTxnID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$igroTxnID = $igroTxnID->toArray();
					$result->IGROTxnID = $igroTxnID[0]["igroTxnID"];
				}

				//print_r($result);
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		/*	package purchase BY THE INSTITUE/INSTRUCTOR
			Function	: 	purchasePackage
			Author		:	mackayush
			this function is used to insert all the order details of the package in order_details table checking if package is not already 				purchased BY THE INSTITUE/INSTRUCTOR
			price of package is been calculated based on no. of courses and student
					
		*/
		public function purchasePackage($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$result = new stdClass();
			try {
				$amount = 1000;
				$productId="";
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				//checking if user already purchase the package
				$select = $sql->select();
				$select->from('package');
				$select->where(array(
					'packId' =>  $data->packId,
					'ownerId'   =>  $data->userId,
					'packType' => '3'	,
					'Active'       >    '1'
								
				));
				//$select->where->greaterThan('price', '0');
				//$select->where->greaterThan('priceINR', '0');
				$select->columns(array('packId','price','priceINR','packName'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				
				if($temp[0]['price'] > 0 || $temp[0]['priceINR'] > 0) {
					$result->status = 0;
					$result->message = 'You have already purchased this package .';
					return $result;
				}
				$packName=$temp[0]['packName'];
				$c = new StudentPackage();
				
				$student=$c->getPackagestudent($data);
				$count=$student->packages[0]['count'];
				$course=$student->packages[0]['courses'];
				if($course>10)
				$course=10;
				
				//print_r($amount);
				$name = '';
				$email = '';
				$number = '';
				//fetching first name of institute/professor/publisher
				$select = $sql->select();
				$select->from(array('ld'    =>  'login_details'));
				$select->where(array('ld.id'    =>  $data->userId));
				$select->columns(array(
					'id'    =>  'id',
					'email' =>  'email'
				));
				$select->join(
					array('ud'  =>  'user_details'),
					'ud.userId=ld.id',
					array('contactMobile'   =>  'contactMobile')
				);
				if($data->userRole == 1) {
					$select->join(
						array('id'  =>  'institute_details'),
						'id.userId=ld.id',
						array('name'    =>  'name')
					);
				}
				else if($data->userRole == 2) {
					$select->join(
						array('pd'  =>  'professor_details'),
						'pd.userId=ld.id',
						array('name'    =>  new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"))
					);
				}
				else {
					$select->join(
						array('pd'  =>  'publisher_details'),
						'pd.userId=ld.id',
						array('name'    =>  new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"))
					);
				}
				$selectString = $sql->getSqlStringForSqlObject($select);
				$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$details = $details->toArray();
				//print_r($details);
				//print_r(count($details));
				
				if(count($details) > 0) {
					$name = $details[0]['name'];
					if($details[0]['contactMobile'] == '')
						$details[0]['contactMobile'] = '9876543210';
					$number = $details[0]['contactMobile'];
					$email = $details[0]['email'];
				}
				
				$productId=$data->packId;
				//generating order id
				
				$factor=10; //per course price

				$amount= ($count * $factor *$course);
				//$amount= 5;

				$db->beginTransaction();

				$orderId = new TableGateway('order_details', $adapter, null, new HydratingResultSet());
				$insert = array(
					'type'              =>  3,
					'userId'            =>  $data->userId,
					'email'             =>  $email,
					'purchase_detail'   =>  $productId,
					'rate'              =>  0,
					'amount'            =>  $amount,
					'status'            =>  0,
					'payUId'            =>  0,
					'timestamp'         =>  time(),
					'couponcode'		=>  ''
				);
				$orderId->insert($insert);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$txnid = $orderId->getLastInsertValue();
				//$udf1=$productId;
				//$udf2=$couponCo;

				global $siteBase;

				// Metamask code here
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function afterIGROPay($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				//setting any undefined variable if not returned 
				//updating the order details table
				
				if ($data['paymentSkip'] == 0) {
					$where = array('IGROTxnID'	=>	$data['id']);
					$status = $data['txn_status'];
				} else {
					$where = array('orderId' => $data['orderId']);
					$status = 1;
				}
				//if error is E000 then actually add keys to the user account
				$send = new stdClass();
				$select = $sql->select();
				$select->from('order_details');
				$select->columns(array('orderId', 'userId', 'purchase_detail', 'amount'));
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$details = $details->toArray();
				if(count($details) > 0)
				{
					$userID=$details[0]['userId'];
					$packId=$details[0]['purchase_detail'];

					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					$query = "INSERT INTO package_user(userId,userType,packId,Paid,packageDeactiveDate) VALUES ($userID,'4',$packId,'1',DATE_ADD(now(), INTERVAL 1 YEAR))  ";
					$package_user = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
				}
				$data['date'] = date('d M Y H:i:s');
				$result->status = 1;
				$result->data = $data;
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		/*	package purchased BY THE STUDENT PayU
			Function	: 	afterPayU
			Author		:	mackayush
			this function is used to insert all the transactions details which had been returned from payU after transactions.
			Also if transaction is sucessfull the package is inserted into package_user table for that userId					
		*/
		public function afterPayU($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				//setting any undefined variable if not returned 
				if(!isset($data['mihpayid']))
					$data['mihpayid'] = '';
				if(!isset($data['mode']))
					$data['mode'] = '';
				if(!isset($data['status']))
					$data['status'] = '';
				if(!isset($data['unmappedstatus']))
					$data['unmappedstatus'] = '';
				if(!isset($data['key']))
					$data['key'] = '';
				if(!isset($data['txnid']))
					$data['txnid'] = '';
				if(!isset($data['amount']))
					$data['amount'] = '';
				if(!isset($data['addedon']))
					$data['addedon'] = '';
				if(!isset($data['productinfo']))
					$data['productinfo'] = '';
				if(!isset($data['firstname']))
					$data['firstname'] = '';
				if(!isset($data['lastname']))
					$data['lastname'] = '';
				if(!isset($data['address1']))
					$data['address1'] = '';
				if(!isset($data['address2']))
					$data['address2'] = '';
				if(!isset($data['city']))
					$data['city'] = '';
				if(!isset($data['state']))
					$data['state'] = '';
				if(!isset($data['country']))
					$data['country'] = '';
				if(!isset($data['zipcode']))
					$data['zipcode'] = '';
				if(!isset($data['email']))
					$data['email'] = '';
				if(!isset($data['phone']))
					$data['phone'] = '';
				if(!isset($data['udf1']))
					$data['udf1'] = '';
				if(!isset($data['udf2']))
					$data['udf2'] = '';
				if(!isset($data['udf3']))
					$data['udf3'] = '';
				if(!isset($data['udf4']))
					$data['udf4'] = '';
				if(!isset($data['udf5']))
					$data['udf5'] = '';
				if(!isset($data['udf6']))
					$data['udf6'] = '';
				if(!isset($data['udf7']))
					$data['udf7'] = '';
				if(!isset($data['udf8']))
					$data['udf8'] = '';
				if(!isset($data['udf9']))
					$data['udf9'] = '';
				if(!isset($data['udf10']))
					$data['udf10'] = '';
				if(!isset($data['hash']))
					$data['hash'] = '';
				if(!isset($data['field1']))
					$data['field1'] = '';
				if(!isset($data['field2']))
					$data['field2'] = '';
				if(!isset($data['field3']))
					$data['field3'] = '';
				if(!isset($data['field4']))
					$data['field4'] = '';
				if(!isset($data['field5']))
					$data['field5'] = '';
				if(!isset($data['field6']))
					$data['field6'] = '';
				if(!isset($data['field7']))
					$data['field7'] = '';
				if(!isset($data['field8']))
					$data['field8'] = '';
				if(!isset($data['field9']))
					$data['field9'] = '';
				if(!isset($data['PG_TYPE']))
					$data['PG_TYPE'] = '';
				if(!isset($data['bank_ref_num']))
					$data['bank_ref_num'] = '';
				if(!isset($data['bankcode']))
					$data['bankcode'] = '';
				if(!isset($data['error']))
					$data['error'] = '';
				if(!isset($data['error_Message']))
					$data['error_Message'] = '';
				if(!isset($data['cardToken']))
					$data['cardToken'] = '';
				if(!isset($data['name_on_card']))
					$data['name_on_card'] = '';
				if(!isset($data['cardnum']))
					$data['cardnum'] = '';
				if(!isset($data['cardhash']))
					$data['cardhash'] = '';
				if(!isset($data['amount_split']))
					$data['amount_split'] = '';
				if(!isset($data['payuMoneyId']))
					$data['payuMoneyId'] = '';
				if(!isset($data['discount']))
					$data['discount'] = '';
				if(!isset($data['net_amount_debit']))
					$data['net_amount_debit'] = '';

				//$transcation = new TableGateway('payu_details', $adapter, null, new HydratingResultSet());
				$insert = array(
					'mihpayid'          =>  $data['mihpayid'],
					'mode'              =>  $data['mode'],
					'status'            =>  $data['status'],
					'unmappedstatus'    =>  $data['unmappedstatus'],
					'key'               =>  $data['key'],
					'txnid'             =>  $data['txnid'],
					'amount'            =>  $data['amount'],
					'addedon'           =>  $data['addedon'],
					'productinfo'       =>  $data['productinfo'],
					'firstname'         =>  $data['firstname'],
					'lastname'          =>  $data['lastname'],
					'address1'          =>  $data['address1'],
					'address2'          =>  $data['address2'],
					'city'              =>  $data['city'],
					'state'             =>  $data['state'],
					'country'           =>  $data['country'],
					'zipcode'           =>  $data['zipcode'],
					'email'             =>  $data['email'],
					'phone'             =>  $data['phone'],
					'udf1'              =>  $data['udf1'],
					'udf2'              =>  $data['udf2'],
					'udf3'              =>  $data['udf3'],
					'udf4'              =>  $data['udf4'],
					'udf5'              =>  $data['udf5'],
					'udf6'              =>  $data['udf6'],
					'udf7'              =>  $data['udf7'],
					'udf8'              =>  $data['udf8'],
					'udf9'              =>  $data['udf9'],
					'udf10'             =>  $data['udf10'],
					'hash'              =>  $data['hash'],
					'field1'            =>  $data['field1'],
					'field2'            =>  $data['field2'],
					'field3'            =>  $data['field3'],
					'field4'            =>  $data['field4'],
					'field5'            =>  $data['field5'],
					'field6'            =>  $data['field6'],
					'field7'            =>  $data['field7'],
					'field8'            =>  $data['field8'],
					'field9'            =>  $data['field9'],
					'PG_TYPE'           =>  $data['PG_TYPE'],
					'bank_ref_num'      =>  $data['bank_ref_num'],
					'bankcode'          =>  $data['bankcode'],
					'error'             =>  $data['error'],
					'error_Message'     =>  $data['error_Message'],
					'cardToken'         =>  $data['cardToken'],
					'name_on_card'      =>  $data['name_on_card'],
					'cardnum'           =>  $data['cardnum'],
					'cardhash'          =>  $data['cardhash'],
					'amount_split'      =>  $data['amount_split'],
					'payuMoneyId'       =>  $data['payuMoneyId'],
					'discount'          =>  $data['discount'],
					'net_amount_debit'  =>  $data['net_amount_debit']
				);
				//$transcation->insert($insert);
				//$payuId = $transcation->getLastInsertValue();

				$query="INSERT INTO payu_details (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$adapter->query("SET @payuId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);

				//updating the order details table
				/*$update = $sql->update();
				$update->table('order_details');
				$update->set(array(
					'payUId'    =>  $payuId,
					'status'    =>  $data['error']
				));
				$update->where(array('orderId'  =>  $data['txnid']));
				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);*/

				$query="UPDATE order_details SET `payUId`=@payuId,`status`='".$data['error']."' WHERE orderId=".$data['txnid'];
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);

				//if error is E000 i.e no error then actually updating the packages table

				if($data['error'] == 'E000') {
					$send = new stdClass();
					$select = $sql->select();
					$select->from('order_details');
					$select->columns(array('orderId', 'userId', 'purchase_detail', 'amount'));
					$select->where(array('orderId'  =>  $data['txnid']));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$details = $details->toArray();
					if(count($details) > 0)
					{
						$userID=$details[0]['userId'];
						$packId=$details[0]['purchase_detail'];

						$adapter = $this->adapter;
						$sql = new Sql($adapter);
						$query = "INSERT INTO package_user(userId,userType,packId,Paid,packageDeactiveDate) VALUES ($userID,'4',$packId,'1',DATE_ADD(now(), INTERVAL 1 YEAR))  ";
						$package_user = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$data['date'] = date('d M Y H:i:s');
				
				$result->status = 1;
				$result->data = $data;
				
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		/*	package purchased BY THE STUDENT paypal
			Function	: 	paypalStudentReturn
			Author		:	mackayush
			this function is used to insert all the transactions details which had been returned from paypal after transactions.
			Also if transaction is sucessfull the package is inserted into package_user table for that userId					
		*/
		public function paypalStudentReturn($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('od' => 'order_details'));
				$select->where(array('orderId' => $data->orderId));
				$select->join(array('p' => 'package'), 'p.packId=od.purchase_detail', array('packName'));
				//$select->join(array('p' => 'package'), 'p.packId=od.purchase_detail', array('packName'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$order = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$order = $order->toArray();
				if(count($order) != 1) {
					$result->status = 0;
					$result->message = 'Something went wrong. Your payment was not successful';
					$result->date = date('d M Y H:i:s');
					$result->userRole = $data->userRole;
					return $result;
				}
				$order = $order[0];
				$result->amount = $order['amount'];
				$result->type = $order['type'];
				$result->purchase_detail = $order['purchase_detail'];
				$result->userId = $order['userId'];
				$result->name = $order['packName'];	
				$result->date = date('d M Y H:i:s');
				$result->userRole = $data->userRole;

				//now inserting into paypal_details for future
				//$paypal = new TableGateway('paypal_details', $adapter, null, new HydratingResultSet());
				$insert = array(
					'paypalId'	=>	$data->paypalId,
					'amount'	=>	$data->amount,
					'timestamp'	=>	time()
				);
				//$paypal->insert($insert);
				//$paypalId = $paypal->getLastInsertValue();
				
				$query="INSERT INTO paypal_details (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$adapter->query("SET @paypalId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);

				//updating the value of order with paypal details mapping
				/*$update = $sql->update();
				$update->table('order_details');
				$update->set(array(
					'payUId'	=>	$paypalId,
					'status'	=>	'E000'
				));
				$update->where(array('orderId' => $data->orderId));
				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);*/

				$query="UPDATE order_details SET `payUId`=@paypalId,`status`='E000' WHERE orderId=".$data->orderId;
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);

				//updating the package_user tables to enroll student in packages courses

				$userID=$order['userId'];
				$packId=$order['purchase_detail'];
					 
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('pu' => 'package_user'));
				$select->where(array('userId' => $userID,'userType' => 4,'packId'=>$packId));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$packageuser = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$packageuser = $packageuser->toArray();
				if(count($packageuser) ==  0 ) {

					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					$query = "INSERT INTO package_user(userId,userType,packId,Paid,packageDeactiveDate) VALUES ($userID,'4',$packId,'1',DATE_ADD(now(), INTERVAL 1 YEAR))  ";

					$package_user = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();

				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				$result->date = date('d M Y H:i:s');
				$result->userRole = $data->userRole;
				return $result;
			}
		}

		public function completePaypalStudentReturnFree($invoice, $amount, $txid) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
	            $adapter = $this->adapter;
	            $sql = new Sql($adapter);
	            $select = $sql->select();
	            $select->from('order_details')->where(array('orderId' => $invoice, 'payUId' => 0));
	            $selectString = $sql->getSqlStringForSqlObject($select);
	            $order = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
	            $order = $order->toArray();
	            if(count($order) <= 0) {
	                $result->status = 0;
	                $result->message = 'No such order found';
	                return $result;
	            }
	            $order = $order[0];

	            //now insert a row in paypal_details
	            //$paypal = new TableGateway('paypal_details', $adapter, null, new HydratingResultSet());
	            $insert = array(
	                'paypalId'  =>  $txid,
	                'amount'    =>  $amount,
	                'timestamp' =>  time()
	            );
	            //$paypal->insert($insert);
	            //$paypalId = $paypal->getLastInsertValue();
				
				$query="INSERT INTO paypal_details (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$adapter->query("SET @paypalId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);

	            //updating the value of order with paypal details mapping
	            /*$update = $sql->update();
	            $update->table('order_details');
	            $update->set(array(
	                'payUId'    =>  $paypalId,
	                'status'    =>  'E000'
	            ));
	            $update->where(array('orderId' => $invoice));
	            $statement = $sql->getSqlStringForSqlObject($update);
	            $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);*/

				$query="UPDATE order_details SET `payUId`=@paypalId,`status`='E000' WHERE orderId=".$data->orderId;
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);

	            if($amount == $order['amount']){
	                if($order['type'] == 4) {
	                    //actually inserting free package through currency dollar
	                   	$userID=$order['userId'];
						$packId=$order['purchase_detail'];
		
						$adapter = $this->adapter;
						$sql = new Sql($adapter);
						$query = "INSERT INTO package_user(userId,userType,packId,Paid,packageDeactiveDate) VALUES ($userID,'4',$packId,'1',DATE_ADD(now(), INTERVAL 1 YEAR))  ";
						$package_user = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
	    			}               
	            }
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
	            $result->status = 1;
	            return $result;
	        }catch(Exception $e) {
				$db->rollBack();
	            $result->status = 0;
	            $result->message = $e->getMessage();
	            return $result;
	        }
	    }

		
		/*	package purchased BY THE INSTITUE/INSTRUCTOR PayU
			Function	: 	afterPayUPackage
			Author		:	mackayush
			this function is used to insert all the transactions details which had been returned from payU after transactions.
			Also if transaction is sucessfull the package_user gets upadted as paid=1				
		*/
		public function afterPayUPackage($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				//setting any undefined variable if not returned 
				if(!isset($data['mihpayid']))
					$data['mihpayid'] = '';
				if(!isset($data['mode']))
					$data['mode'] = '';
				if(!isset($data['status']))
					$data['status'] = '';
				if(!isset($data['unmappedstatus']))
					$data['unmappedstatus'] = '';
				if(!isset($data['key']))
					$data['key'] = '';
				if(!isset($data['txnid']))
					$data['txnid'] = '';
				if(!isset($data['amount']))
					$data['amount'] = '';
				if(!isset($data['addedon']))
					$data['addedon'] = '';
				if(!isset($data['productinfo']))
					$data['productinfo'] = '';
				if(!isset($data['firstname']))
					$data['firstname'] = '';
				if(!isset($data['lastname']))
					$data['lastname'] = '';
				if(!isset($data['address1']))
					$data['address1'] = '';
				if(!isset($data['address2']))
					$data['address2'] = '';
				if(!isset($data['city']))
					$data['city'] = '';
				if(!isset($data['state']))
					$data['state'] = '';
				if(!isset($data['country']))
					$data['country'] = '';
				if(!isset($data['zipcode']))
					$data['zipcode'] = '';
				if(!isset($data['email']))
					$data['email'] = '';
				if(!isset($data['phone']))
					$data['phone'] = '';
				if(!isset($data['udf1']))
					$data['udf1'] = '';
				if(!isset($data['udf2']))
					$data['udf2'] = '';
				if(!isset($data['udf3']))
					$data['udf3'] = '';
				if(!isset($data['udf4']))
					$data['udf4'] = '';
				if(!isset($data['udf5']))
					$data['udf5'] = '';
				if(!isset($data['udf6']))
					$data['udf6'] = '';
				if(!isset($data['udf7']))
					$data['udf7'] = '';
				if(!isset($data['udf8']))
					$data['udf8'] = '';
				if(!isset($data['udf9']))
					$data['udf9'] = '';
				if(!isset($data['udf10']))
					$data['udf10'] = '';
				if(!isset($data['hash']))
					$data['hash'] = '';
				if(!isset($data['field1']))
					$data['field1'] = '';
				if(!isset($data['field2']))
					$data['field2'] = '';
				if(!isset($data['field3']))
					$data['field3'] = '';
				if(!isset($data['field4']))
					$data['field4'] = '';
				if(!isset($data['field5']))
					$data['field5'] = '';
				if(!isset($data['field6']))
					$data['field6'] = '';
				if(!isset($data['field7']))
					$data['field7'] = '';
				if(!isset($data['field8']))
					$data['field8'] = '';
				if(!isset($data['field9']))
					$data['field9'] = '';
				if(!isset($data['PG_TYPE']))
					$data['PG_TYPE'] = '';
				if(!isset($data['bank_ref_num']))
					$data['bank_ref_num'] = '';
				if(!isset($data['bankcode']))
					$data['bankcode'] = '';
				if(!isset($data['error']))
					$data['error'] = '';
				if(!isset($data['error_Message']))
					$data['error_Message'] = '';
				if(!isset($data['cardToken']))
					$data['cardToken'] = '';
				if(!isset($data['name_on_card']))
					$data['name_on_card'] = '';
				if(!isset($data['cardnum']))
					$data['cardnum'] = '';
				if(!isset($data['cardhash']))
					$data['cardhash'] = '';
				if(!isset($data['amount_split']))
					$data['amount_split'] = '';
				if(!isset($data['payuMoneyId']))
					$data['payuMoneyId'] = '';
				if(!isset($data['discount']))
					$data['discount'] = '';
				if(!isset($data['net_amount_debit']))
					$data['net_amount_debit'] = '';

				//$transcation = new TableGateway('payu_details', $adapter, null, new HydratingResultSet());
				$insert = array(
					'mihpayid'          =>  $data['mihpayid'],
					'mode'              =>  $data['mode'],
					'status'            =>  $data['status'],
					'unmappedstatus'    =>  $data['unmappedstatus'],
					'key'               =>  $data['key'],
					'txnid'             =>  $data['txnid'],
					'amount'            =>  $data['amount'],
					'addedon'           =>  $data['addedon'],
					'productinfo'       =>  $data['productinfo'],
					'firstname'         =>  $data['firstname'],
					'lastname'          =>  $data['lastname'],
					'address1'          =>  $data['address1'],
					'address2'          =>  $data['address2'],
					'city'              =>  $data['city'],
					'state'             =>  $data['state'],
					'country'           =>  $data['country'],
					'zipcode'           =>  $data['zipcode'],
					'email'             =>  $data['email'],
					'phone'             =>  $data['phone'],
					'udf1'              =>  $data['udf1'],
					'udf2'              =>  $data['udf2'],
					'udf3'              =>  $data['udf3'],
					'udf4'              =>  $data['udf4'],
					'udf5'              =>  $data['udf5'],
					'udf6'              =>  $data['udf6'],
					'udf7'              =>  $data['udf7'],
					'udf8'              =>  $data['udf8'],
					'udf9'              =>  $data['udf9'],
					'udf10'             =>  $data['udf10'],
					'hash'              =>  $data['hash'],
					'field1'            =>  $data['field1'],
					'field2'            =>  $data['field2'],
					'field3'            =>  $data['field3'],
					'field4'            =>  $data['field4'],
					'field5'            =>  $data['field5'],
					'field6'            =>  $data['field6'],
					'field7'            =>  $data['field7'],
					'field8'            =>  $data['field8'],
					'field9'            =>  $data['field9'],
					'PG_TYPE'           =>  $data['PG_TYPE'],
					'bank_ref_num'      =>  $data['bank_ref_num'],
					'bankcode'          =>  $data['bankcode'],
					'error'             =>  $data['error'],
					'error_Message'     =>  $data['error_Message'],
					'cardToken'         =>  $data['cardToken'],
					'name_on_card'      =>  $data['name_on_card'],
					'cardnum'           =>  $data['cardnum'],
					'cardhash'          =>  $data['cardhash'],
					'amount_split'      =>  $data['amount_split'],
					'payuMoneyId'       =>  $data['payuMoneyId'],
					'discount'          =>  $data['discount'],
					'net_amount_debit'  =>  $data['net_amount_debit']
				);
				//$transcation->insert($insert);
				//$payuId = $transcation->getLastInsertValue();

				$query="INSERT INTO payu_details (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$adapter->query("SET @payuId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);

				//updating the order details table
				/*$update = $sql->update();
				$update->table('order_details');
				$update->set(array(
					'payUId'    =>  $payuId,
					'status'    =>  $data['error']
				));
				$update->where(array('orderId'  =>  $data['txnid']));
				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);*/

				$query="UPDATE order_details SET `payUId`=@payuId,`status`='".$data['error']."' WHERE orderId=".$data['txnid'];
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);

				//if error is E000 i.e no error then actually updating the packages table
				if($data['error'] == 'E000') {
					$send = new stdClass();
					$select = $sql->select();
					$select->from('order_details');
					$select->columns(array('orderId', 'userId', 'purchase_detail', 'amount'));
					$select->where(array('orderId'  =>  $data['txnid']));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$details = $details->toArray();
					if(count($details) > 0)
					{
						/*$send->packId = $details[0]['purchase_detail'];
						$send->price = $details[0]['amount'];
						$send->ownerId = $details[0]['userId'];*/
						$update = $sql->update();
						$update->table('package');
						$update->set(array(
							'priceINR'    =>  $details[0]['amount']
						));
						$update->where(array('packId'  => $details[0]['purchase_detail'],'ownerId' => $details[0]['userId']));
						$statement = $sql->getSqlStringForSqlObject($update);
						$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

						//updating package_users table
						$update = $sql->update();
						$update->table('package_user');
						$update->set(array(
							'Paid'    =>  '1'
						));
						$update->where(array('packId'  => $order['purchase_detail']));
						$statement = $sql->getSqlStringForSqlObject($update);
						$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);

					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				
				$result->status = 1;
				$result->data = $data;
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		/*	package purchased BY THE INSTITUE/INSTRUCTOR paypal
			Function	: 	paypalReturn
			Author		:	mackayush
			this function is used to insert all the transactions details which had been returned from paypal after transactions.
			Also if transaction is sucessfull the package_user gets upadted as paid=1				
		*/
		public function paypalReturn($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('od' => 'order_details'));
				$select->where(array('orderId' => $data->orderId));
				$select->join(array('p' => 'package'), 'p.packId=od.purchase_detail', array('packName'));
				//$select->join(array('c' => 'courses'), 'c.id=od.purchase_detail', array('name'));
				//$select->join(array('c' => 'courses'), 'c.id=od.purchase_detail', array('name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$order = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$order = $order->toArray();
				if(count($order) != 1) {
					$result->status = 0;
					$result->message = 'Something went wrong. Your payment was not successful';
					$result->date = date('d M Y H:i:s');
					$result->userRole = $data->userRole;
					return $result;
				}
				$order = $order[0];
				
				$result->amount = $order['amount'];
				$result->type = $order['type'];
				$result->purchase_detail = $order['purchase_detail'];
				$result->date = date('d M Y H:i:s');
				$result->userId = $order['userId'];
				$result->name = $order['packName'];
				/*$result->rate = $order['rate'];
				$result->date = date('d M Y H:i:s');
				$result->userRole = $data->userRole;
				$result->name = $order['name'];
				$result->orderId = $order['orderId'];
				$result->couponcode = $order['couponcode'];
				
				
				$result->coupo=$data->iteminfo;
				$couponscourse=explode(" ",$result->coupo);
				//print_r($couponscourse);
				$result->payment_status=$data->payment_Status;
				//$couponscourse=explode(" ",$result->orderId);
				if(  $result->purchase_detail != ''  && $result->couponcode != ''){
				if($result->payment_status == 'Completed'  || $result->payment_status == 'completed' ){			
					$redeemdata->courseId  = $result->purchase_detail;
					$redeemdata->couponCode = $result->couponcode;
					$countervalue=$this->redeemCoupon($redeemdata);
					$noCoupons=$countervalue['couponsUsed'];
					$redeemdata->couponsUsed=$noCoupons+1;
					$update=$this->updateCoupon($redeemdata);
				}
				}*/

				//now inserting into paypal_details for future
				//$paypal = new TableGateway('paypal_details', $adapter, null, new HydratingResultSet());
				$insert = array(
					'paypalId'	=>	$data->paypalId,
					'amount'	=>	$data->amount,
					'timestamp'	=>	time()
				);
				//$paypal->insert($insert);
				//$paypalId = $paypal->getLastInsertValue();
				
				$query="INSERT INTO paypal_details (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$adapter->query("SET @paypalId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);

				//updating the value of order with paypal details mapping
				/*$update = $sql->update();
				$update->table('order_details');
				$update->set(array(
					'payUId'	=>	$paypalId,
					'status'	=>	'E000'
				));
				$update->where(array('orderId' => $data->orderId));
				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);*/

				$query="UPDATE order_details SET `payUId`=@paypalId,`status`='E000' WHERE orderId=".$data->orderId;
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			
				//updating package table
				$update = $sql->update();
				$update->table('package');
				$update->set(array(
					'price'    =>  $order['amount']
				));
				$update->where(array('packId'  => $order['purchase_detail'],'ownerId' =>  $order['userId']));
				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				//updating package_users table
				$update = $sql->update();
				$update->table('package_user');
				$update->set(array(
					'Paid'    =>  '1'
				));
				$update->where(array('packId'  => $order['purchase_detail']));
				$statement = $sql->getSqlStringForSqlObject($update);
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();

				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				$result->date = date('d M Y H:i:s');
				$result->userRole = $data->userRole;
				return $result;
			}
		}

		public function slug($z){
			$z = strtolower($z);
			$z = preg_replace('/[^a-z0-9 -]+/', '', $z);
			$z = str_replace(' ', '-', $z);
			return trim($z, '-');
		}

		public function setPackageSlugs() {

			$db = $this->adapter->getDriver()->getConnection();

			//$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('package');
				$select->columns(array('packId', 'packName'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$packages = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$packages = $packages->toArray();
				foreach ($packages as $key => $package) {
					$packages[$key]['packName'] = $this->slug($package['packName']);
					$i = 0;
					do {
						if ($i>0) {
							$packages[$key]['packName'] = $packages[$key]['packName'].$i;
						}
						$i++;
						$selectString = "SELECT packId,packName FROM package WHERE slug = '{$packages[$key]['packName']}' AND packId!='{$package['packId']}'";
						$courseSlugs = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$courseSlugs = $courseSlugs->toArray();
					} while(count($courseSlugs)>0);

					echo $packages[$key]['packName'].'<br>';
					$update = $sql->update();
					$update->table('package');
					$update->set(array('slug' => $packages[$key]['packName']));
					$update->where(array('packId' => $package['packId']));
					$statement = $sql->prepareStatementForSqlObject($update);
					$count = $statement->execute()->getAffectedRows();
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				//$db->commit();
				$result->status = 1;
				$result->message = 'Package slugs set!';
				return $result;
			} catch (Exception $e) {
				//$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function getPackageForMeta($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				if (isset($data->slug) && !empty($data->slug)) {
					$select->from('package')->where(array('slug' => $data->slug))->columns(array('packName', 'packDescription', 'image'));
				}
				$selectString = $sql->getSqlStringForSqlObject($select);
				$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$details = $details->toArray();
				if(count($details) > 0) {
					$result->details = $details[0];
					$ogImage = $result->details['image'];
					$ogPath = explode("/", $ogImage);
					$result->details['ogimage'] = $this->ogPath.end($ogPath);
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
						require_once 'amazonRead.php';
						$result->details['image'] = getSignedURL($result->details['image']);
					}
					$result->status = 1;
					return $result;
				}
				$result->status = 0;
				$result->message = 'No such course found';
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
	} //class ends
?>