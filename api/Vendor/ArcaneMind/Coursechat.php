<?php

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "Base.php";

class Coursechat extends Base {

	// function for loading data on back-end page Institute wise chat details
	public function backendCoursechats($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$query = "SELECT l.id,c.id AS courseId,c.name AS courseName,c.chat,
						CASE 
							WHEN ur.roleId= 1 THEN (SELECT name FROM institute_details WHERE userId=l.id)
							WHEN ur.roleId= 2 THEN (SELECT CONCAT(firstname, ' ', lastname)  FROM professor_details WHERE userId=l.id)
							ELSE 'NA'
						END as name
						FROM `login_details` AS l
						INNER JOIN `user_roles` AS ur ON ur.userId=l.id
						INNER JOIN `courses` AS c ON c.ownerId=l.id
						WHERE roleId IN (1,2) AND c.deleted=0";
			$users = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$temp = $users->toArray();
			$result->chat_data = $temp;
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	// function for loading data on back-end page Institute wise chat details
	public function backendCoursechatUpdate($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('courses');
			$update->set(array('chat'=>	(($data->chat=="OFF")?1:0)));
			$update->where(array('id'		=>	$data->courseId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$statement->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	// function for loading data on back-end page Institute wise chat details
	public function getStudentChatLink($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			if (isset($data->professorId)) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('ss' => 'student_subject'))->join(array('l'	=>	'login_details'),
					'ss.userId = l.id',
					array('temp'));
				$select->where(array('ss.courseId' => $data->courseId,'ss.subjectId' => $data->subjectId,'ss.userId' => $data->userId,'ss.Active'=>1,'l.temp'=>0));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjectAccess = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjectAccess = $subjectAccess->toArray();
				if (count($subjectAccess)) {
					require_once('Subject.php');
					require_once("User.php");
        			global $siteBase;
					$s = new Subject();
					$sData = new stdClass();
					$sData->subjectId = $data->subjectId;
					$subjectDetails = $s->getSubjectBasics($sData);
					if ($subjectDetails->status == 0) {
						$result->status = 0;
						$result->exception = "The subject is invalid!";
						return $result;
					}
					$subjectDetails = $subjectDetails->subjectDetails;
					$senderDetails = array();
					$u = new User();
					$cData = new stdClass();
					$cData->userId = $data->userId;
					$userRole = $u->getUserRole($data);
					if ($userRole->status == 1) {
						$senderUserRole = $userRole->role;
						if ($senderUserRole == 4) {
							require_once("Student.php");
							$s = new Student();
							$senderDetails = $s->getStudentDetails($cData);
							$senderDetails = $senderDetails->details;
						}
					}
					if ($data->professorId == 0) {
						$query="SELECT cr.id
								FROM `chat_rooms` AS cr
								WHERE cr.room_type='public' AND cr.courseId={$data->courseId} AND cr.subjectId={$data->subjectId} AND cr.active=1";
						$chatRoom = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$chatRoom = $chatRoom->toArray();
						if (count($chatRoom) == 0) {
							$chat_rooms = new TableGateway('chat_rooms', $adapter, null, new HydratingResultSet());
							$insert = array(
								'courseId' => $data->courseId,
								'subjectId' => $data->subjectId,
								'room_type' => 'public'
							);
							$chat_rooms->insert($insert);
							$roomId = $chat_rooms->getLastInsertValue();
							$result->roomId = $roomId;
							$result->status = 1;
						} else {
							$roomId = $chatRoom[0]['id'];
							$result->roomId = $chatRoom[0]['id'];
							$result->status = 1;
						}
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from(array('ss' => 'student_subject'))->join(array('l'	=>	'login_details'),
						'ss.userId = l.id',
						array('temp'));
						$select->where(array("ss.courseId={$data->courseId} AND ss.subjectId={$data->subjectId} AND ss.Active=1 AND ss.userId != {$data->userId} AND l.temp=0"));
						$select->group(array("userId"));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$subjectStudents = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$subjectStudents = $subjectStudents->toArray();
						if (count($subjectStudents)>0) {
							foreach ($subjectStudents as $key => $value) {
								$chat_notifications = new TableGateway('chat_notifications', $adapter, null, new HydratingResultSet());
								$insert = array(
									'roomId' => $roomId,
									'senderId' => $data->userId,
									'userId' => $value["userId"],
									'type' => 0
								);
								$chat_notifications->insert($insert);
								$cData = new stdClass();
								$cData->userId = $value["userId"];
								$userRole = $u->getUserRole($cData);
								if ($userRole->status == 1) {
									$cData->userRole = $userRole->role;
									if ($cData->userRole == 4) {
										require_once("Student.php");
										$s = new Student();
										$sDetails = $s->getStudentDetails($cData);
										if ($sDetails->status == 1) {
											$sDetails = $sDetails->details;
											$sName = ((empty(trim($sDetails["name"])))?'Student':$sDetails["name"]);
            								$chatNotifLink = $siteBase . "student/notifications/chat";
											$emailMessage = 'Hello '.$sName.', <br><br>'.$senderDetails["name"].' just started conversation in <strong>Subject : '.$subjectDetails["name"].'</strong> of <strong>Course : '.$subjectDetails["courseName"].'</strong>. <br> <br> <a href="'.$chatNotifLink.'">Click here</a> to check the chat notifications.';
											$subject = '[Integro.io] - New chat conversation';
											$to = $sDetails['email'];
											$body = $emailMessage;
											$u = new User();
											$u->sendMail($to, $subject, $body);
										}
									}
								}
							}
						}
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('subject_professors');
						$select->where(array("subjectId={$data->subjectId}"));
						$select->group(array("professorId"));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$subjectProfessors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$subjectProfessors = $subjectProfessors->toArray();
						if (count($subjectProfessors)>0) {
							foreach ($subjectProfessors as $key => $value) {
								$chat_notifications = new TableGateway('chat_notifications', $adapter, null, new HydratingResultSet());
								$insert = array(
									'roomId' => $roomId,
									'senderId' => $data->userId,
									'userId' => $value["professorId"],
									'type' => 0
								);
								$chat_notifications->insert($insert);
								$pData = new stdClass();
								$pData->userId = $value["professorId"];
								$userRole = $u->getUserRole($pData);
								if ($userRole->status == 1) {
									$pData->userRole = $userRole->role;
									if ($pData->userRole == 2) {
										require_once("Professor.php");
										$p = new Professor();
										$pDetails = Api::getProfileDetails($pData);
										if ($pDetails->status == 1) {
											$pName = ((empty(trim($pDetails->profileDetails['firstName'])))?'Professor':($pDetails->profileDetails['firstName'].' '.$pDetails->profileDetails['lastName']));
            								$chatNotifLink = $siteBase . "manage/instructor/notifications/chat";
											$emailMessage = 'Hello '.$pName.', <br><br>'.$senderDetails["name"].' just started conversation in <strong>Subject : '.$subjectDetails["name"].'</strong> of <strong>Course : '.$subjectDetails["courseName"].'</strong>. <br> <br> <a href="'.$chatNotifLink.'">Click here</a> to check the chat notifications.';
											$subject = '[Integro.io] - New chat conversation';
											$to = $pDetails->loginDetails['email'];
											$body = $emailMessage;
											$u = new User();
											//var_dump($body);
											$u->sendMail($to, $subject, $body);
										}
									}
								}
							}
						}
					} else {
						/*$query="SELECT cr.id
								FROM `chat_rooms` AS cr
								INNER JOIN `chat_room_users` AS cru ON cru.roomId=cr.id
								WHERE cr.room_type='private' AND cr.courseId={$data->courseId} AND cr.subjectId={$data->subjectId} AND (cru.userId={$data->userId} OR cru.userId={$data->professorId}) AND cr.active=1";*/

						$query="SELECT cr.id
								FROM `chat_room_users` AS cru1
								INNER JOIN `chat_room_users` AS cru2 ON cru1.roomId=cru2.roomId AND cru1.id<>cru2.id
								INNER JOIN `chat_rooms` AS cr ON cr.id=cru1.roomId
								WHERE cru1.userId={$data->userId} AND cru2.userId={$data->professorId} AND cr.room_type='private' AND cr.courseId={$data->courseId} AND cr.subjectId={$data->subjectId} AND cr.active=1";
						$chatRoom = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$chatRoom = $chatRoom->toArray();
						//if (count($chatRoom) >= 2) {
						if (count($chatRoom) > 0) {
							$roomId = $chatRoom[0]['id'];
							$result->roomId = $chatRoom[0]['id'];
							$result->status = 1;
						} else {
							$chat_rooms = new TableGateway('chat_rooms', $adapter, null, new HydratingResultSet());
							$insert = array(
								'courseId' => $data->courseId,
								'subjectId' => $data->subjectId,
								'room_type' => 'private'
							);
							$chat_rooms->insert($insert);
							$roomId = $chat_rooms->getLastInsertValue();
							$chat_room_users = new TableGateway('chat_room_users', $adapter, null, new HydratingResultSet());
							$insert = array(
								'roomId' => $roomId,
								'userId' => $data->userId
							);
							$chat_room_users->insert($insert);
							$chat_room_users = new TableGateway('chat_room_users', $adapter, null, new HydratingResultSet());
							$insert = array(
								'roomId' => $roomId,
								'userId' => $data->professorId
							);
							$chat_room_users->insert($insert);
							$result->roomId = $roomId;
							$result->status = 1;
						}
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('chat_room_users');
						$select->where("roomId={$roomId} AND userId!={$data->userId}");
						$select->group(array("userId"));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$subjectUsers = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$subjectUsers = $subjectUsers->toArray();
						if (count($subjectUsers)>0) {
							foreach ($subjectUsers as $key => $value) {
								$chat_notifications = new TableGateway('chat_notifications', $adapter, null, new HydratingResultSet());
								$insert = array(
									'roomId' => $roomId,
									'senderId' => $data->userId,
									'userId' => $value["userId"],
									'type' => 0
								);
								$chat_notifications->insert($insert);
								$uData = new stdClass();
								$uData->userId = $value["userId"];
								$userRole = $u->getUserRole($uData);
								if ($userRole->status == 1) {
									$uData->userRole = $userRole->role;
									if ($uData->userRole == 4) {
										require_once("Student.php");
										$s = new Student();
										$sDetails = $s->getStudentDetails($uData);
										if ($sDetails->status == 1) {
											$sDetails = $sDetails->details;
											$sName = ((empty(trim($sDetails["name"])))?'Student':$sDetails["name"]);
            								$chatNotifLink = $siteBase . "student/notifications/chat";
											$emailMessage = 'Hello '.$sName.', <br><br>'.$senderDetails["name"].' just sent a message in <strong>Subject : '.$subjectDetails["name"].'</strong> of <strong>Course : '.$subjectDetails["courseName"].'</strong>. <br> <br> <a href="'.$chatNotifLink.'">Click here</a> to check the chat notifications.';
											$subject = '[Integro.io] - New chat conversation';
											$to = $sDetails['email'];
											$body = $emailMessage;
											//var_dump($emailMessage);
											//var_dump($to);
											$u->sendMail($to, $subject, $body);
										}
									} else {
										require_once("Professor.php");
										$p = new Professor();
										$pDetails = Api::getProfileDetails($uData);
										if ($pDetails->status == 1) {
											$pName = ((empty(trim($pDetails->profileDetails['firstName'])))?'Professor':($pDetails->profileDetails['firstName'].' '.$pDetails->profileDetails['lastName']));
            								$chatNotifLink = $siteBase . "manage/instructor/notifications/chat";
											$emailMessage = 'Hello '.$pName.', <br><br>'.$senderDetails["name"].' just sent a message in <strong>Subject : '.$subjectDetails["name"].'</strong> of <strong>Course : '.$subjectDetails["courseName"].'</strong>. <br> <br> <a href="'.$chatNotifLink.'">Click here</a> to check the chat notifications.';
											$subject = '[Integro.io] - New chat conversation';
											$to = $pDetails->loginDetails['email'];
											$body = $emailMessage;
											//var_dump($emailMessage);
											//var_dump($body);
											$u->sendMail($to, $subject, $body);
										}
									}
								}
							}
						}
					}
				} else {
					$result->status = 0;
					$result->exception = "You don't have subject access!";
					return $result;
				}
			} else {
				$result->status = 0;
				$result->exception = "There was some error, please try again";
				return $result;
			}

			$users = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$temp = $users->toArray();
			
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getProfessorChatLink($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			if (isset($data->studentId)) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subject_professors');
				$select->where(array('subjectId' => $data->subjectId,'professorId' => $data->userId));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjectAccess = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjectAccess = $subjectAccess->toArray();
				if (count($subjectAccess)) {
					require_once('Subject.php');
					require_once("User.php");
        			global $siteBase;
					$s = new Subject();
					$sData = new stdClass();
					$sData->subjectId = $data->subjectId;
					$subjectDetails = $s->getSubjectBasics($sData);
					if ($subjectDetails->status == 0) {
						$result->status = 0;
						$result->exception = "The subject is invalid!";
						return $result;
					}
					$subjectDetails = $subjectDetails->subjectDetails;
					$senderDetails = array();
					$senderName = '';
					$u = new User();
					$cData = new stdClass();
					$cData->userId = $data->userId;
					$cData->userRole = $data->userRole;
					$userRole = $u->getUserRole($data);
					if ($userRole->status == 1) {
						$senderUserRole = $userRole->role;
						if ($senderUserRole == 2) {
							require_once("Professor.php");
							$p = new Professor();
							$senderDetails = Api::getProfileDetails($cData);
							$senderDetails = $senderDetails->profileDetails;
							$senderName = ((empty(trim($senderDetails['firstName'])))?'Professor':($senderDetails['firstName'].' '.$senderDetails['lastName']));
						}
					}
					if ($data->studentId == 0) {
						$query="SELECT cr.id
								FROM `chat_rooms` AS cr
								WHERE cr.room_type='public' AND cr.courseId={$data->courseId} AND cr.subjectId={$data->subjectId} AND cr.active=1";
						$chatRoom = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$chatRoom = $chatRoom->toArray();
						if (count($chatRoom) == 0) {
							$chat_rooms = new TableGateway('chat_rooms', $adapter, null, new HydratingResultSet());
							$insert = array(
								'courseId' => $data->courseId,
								'subjectId' => $data->subjectId,
								'room_type' => 'public'
							);
							$chat_rooms->insert($insert);
							$roomId = $chat_rooms->getLastInsertValue();
							$result->roomId = $roomId;
							$result->status = 1;
						} else {
							$roomId = $chatRoom[0]['id'];
							$result->roomId = $chatRoom[0]['id'];
							$result->status = 1;
						}
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('student_subject');
						$select->where(array("courseId={$data->courseId} AND subjectId={$data->subjectId} AND Active=1"));
						$select->group(array("userId"));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$subjectStudents = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$subjectStudents = $subjectStudents->toArray();
						if (count($subjectStudents)>0) {
							foreach ($subjectStudents as $key => $value) {
								$chat_notifications = new TableGateway('chat_notifications', $adapter, null, new HydratingResultSet());
								$insert = array(
									'roomId' => $roomId,
									'senderId' => $data->userId,
									'userId' => $value["userId"],
									'type' => 0
								);
								$chat_notifications->insert($insert);
								$cData = new stdClass();
								$cData->userId = $value["userId"];
								$userRole = $u->getUserRole($cData);
								if ($userRole->status == 1) {
									$cData->userRole = $userRole->role;
									if ($cData->userRole == 4) {
										require_once("Student.php");
										$s = new Student();
										$sDetails = $s->getStudentDetails($cData);
										if ($sDetails->status == 1) {
											$sDetails = $sDetails->details;
											$sName = ((empty(trim($sDetails["name"])))?'Student':$sDetails["name"]);
            								$chatNotifLink = $siteBase . "student/notifications/chat";
											$emailMessage = 'Hello '.$sName.', <br><br>'.$senderName.' just started conversation in <strong>Subject : '.$subjectDetails["name"].'</strong> of <strong>Course : '.$subjectDetails["courseName"].'</strong>. <br> <br> <a href="'.$chatNotifLink.'">Click here</a> to check the chat notifications.';
											$subject = '[Integro.io] - New chat conversation';
											$to = $sDetails['email'];
											$body = $emailMessage;
											$u = new User();
											$u->sendMail($to, $subject, $body);
										}
									}
								}
							}
						}
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('subject_professors');
						$select->where(array("subjectId={$data->subjectId} AND professorId != {$data->userId}"));
						$select->group(array("professorId"));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$subjectProfessors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$subjectProfessors = $subjectProfessors->toArray();
						if (count($subjectProfessors)>0) {
							foreach ($subjectProfessors as $key => $value) {
								$chat_notifications = new TableGateway('chat_notifications', $adapter, null, new HydratingResultSet());
								$insert = array(
									'roomId' => $roomId,
									'senderId' => $data->userId,
									'userId' => $value["professorId"],
									'type' => 0
								);
								$chat_notifications->insert($insert);
								$pData = new stdClass();
								$pData->userId = $value["professorId"];
								$userRole = $u->getUserRole($pData);
								if ($userRole->status == 1) {
									$pData->userRole = $userRole->role;
									if ($pData->userRole == 2) {
										require_once("Professor.php");
										$p = new Professor();
										$pDetails = Api::getProfileDetails($pData);
										if ($pDetails->status == 1) {
											$pName = ((empty(trim($pDetails->profileDetails['firstName'])))?'Professor':($pDetails->profileDetails['firstName'].' '.$pDetails->profileDetails['lastName']));
            								$chatNotifLink = $siteBase . "manage/instructor/notifications/chat";
											$emailMessage = 'Hello '.$pName.', <br><br>'.$senderName.' just started conversation in <strong>Subject : '.$subjectDetails["name"].'</strong> of <strong>Course : '.$subjectDetails["courseName"].'</strong>. <br> <br> <a href="'.$chatNotifLink.'">Click here</a> to check the chat notifications.';
											$subject = '[Integro.io] - New chat conversation';
											$to = $pDetails->loginDetails['email'];
											$body = $emailMessage;
											$u = new User();
											//var_dump($body);
											$u->sendMail($to, $subject, $body);
										}
									}
								}
							}
						}
					} else {
						/*$query="SELECT cr.id
								FROM `chat_rooms` AS cr
								INNER JOIN `chat_room_users` AS cru ON cru.roomId=cr.id
								WHERE cr.room_type='private' AND cr.courseId={$data->courseId} AND cr.subjectId={$data->subjectId} AND (cru.userId={$data->userId} OR cru.userId={$data->studentId}) AND cr.active=1";*/
						
						$query="SELECT cr.id
								FROM `chat_room_users` AS cru1
								INNER JOIN `chat_room_users` AS cru2 ON cru1.roomId=cru2.roomId AND cru1.id<>cru2.id
								INNER JOIN `chat_rooms` AS cr ON cr.id=cru1.roomId
								WHERE cru1.userId={$data->userId} AND cru2.userId={$data->studentId} AND cr.room_type='private' AND cr.courseId={$data->courseId} AND cr.subjectId={$data->subjectId} AND cr.active=1";
						$chatRoom = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$chatRoom = $chatRoom->toArray();
						//if (count($chatRoom) >= 2) {
						if (count($chatRoom) > 0) {
							$roomId = $chatRoom[0]['id'];
							$result->roomId = $chatRoom[0]['id'];
							$result->status = 1;
						} else {
							$chat_rooms = new TableGateway('chat_rooms', $adapter, null, new HydratingResultSet());
							$insert = array(
								'courseId' => $data->courseId,
								'subjectId' => $data->subjectId,
								'room_type' => 'private'
							);
							$chat_rooms->insert($insert);
							$roomId = $chat_rooms->getLastInsertValue();
							$chat_room_users = new TableGateway('chat_room_users', $adapter, null, new HydratingResultSet());
							$insert = array(
								'roomId' => $roomId,
								'userId' => $data->userId
							);
							$chat_room_users->insert($insert);
							$chat_room_users = new TableGateway('chat_room_users', $adapter, null, new HydratingResultSet());
							$insert = array(
								'roomId' => $roomId,
								'userId' => $data->studentId
							);
							$chat_room_users->insert($insert);
							$result->roomId = $roomId;
							$result->status = 1;
						}
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('chat_room_users');
						$select->where("roomId={$roomId} AND userId!={$data->userId}");
						$select->group(array("userId"));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$subjectUsers = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$subjectUsers = $subjectUsers->toArray();
						if (count($subjectUsers)>0) {
							foreach ($subjectUsers as $key => $value) {
								$chat_notifications = new TableGateway('chat_notifications', $adapter, null, new HydratingResultSet());
								$insert = array(
									'roomId' => $roomId,
									'senderId' => $data->userId,
									'userId' => $value["userId"],
									'type' => 0
								);
								$chat_notifications->insert($insert);
								$uData = new stdClass();
								$uData->userId = $value["userId"];
								$userRole = $u->getUserRole($uData);
								if ($userRole->status == 1) {
									$uData->userRole = $userRole->role;
									if ($uData->userRole == 4) {
										require_once("Student.php");
										$s = new Student();
										$sDetails = $s->getStudentDetails($uData);
										if ($sDetails->status == 1) {
											$sDetails = $sDetails->details;
											$sName = ((empty(trim($sDetails["name"])))?'Student':$sDetails["name"]);
            								$chatNotifLink = $siteBase . "student/notifications/chat";
											$emailMessage = 'Hello '.$sName.', <br><br>'.$senderName.' just sent a message in <strong>Subject : '.$subjectDetails["name"].'</strong> of <strong>Course : '.$subjectDetails["courseName"].'</strong>. <br> <br> <a href="'.$chatNotifLink.'">Click here</a> to check the chat notifications.';
											$subject = '[Integro.io] - New chat conversation';
											$to = $sDetails['email'];
											$body = $emailMessage;
											//var_dump($emailMessage);
											//var_dump($to);
											$u->sendMail($to, $subject, $body);
										}
									} else {
										require_once("Professor.php");
										$p = new Professor();
										$pDetails = Api::getProfileDetails($uData);
										if ($pDetails->status == 1) {
											$pName = ((empty(trim($pDetails->profileDetails['firstName'])))?'Professor':($pDetails->profileDetails['firstName'].' '.$pDetails->profileDetails['lastName']));
            								$chatNotifLink = $siteBase . "manage/instructor/notifications/chat";
											$emailMessage = 'Hello '.$pName.', <br><br>'.$senderName.' just sent a message in <strong>Subject : '.$subjectDetails["name"].'</strong> of <strong>Course : '.$subjectDetails["courseName"].'</strong>. <br> <br> <a href="'.$chatNotifLink.'">Click here</a> to check the chat notifications.';
											$subject = '[Integro.io] - New chat conversation';
											$to = $pDetails->loginDetails['email'];
											$body = $emailMessage;
											//var_dump($emailMessage);
											//var_dump($body);
											$u->sendMail($to, $subject, $body);
										}
									}
								}
							}
						}
					}
				} else {
					$result->status = 0;
					$result->exception = "You don't have subject access!";
					return $result;
				}
			} else {
				$result->status = 0;
				$result->exception = "There was some error, please try again";
				return $result;
			}

			$users = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$temp = $users->toArray();
			
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	// function for loading data on back-end page Institute wise chat details
	public function chatValidity($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('chat_rooms');
			$select->where(array('id' => $data->chatRoomId,'active'=>1));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$chatRoom = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$chatRoom = $chatRoom->toArray();
			if (count($chatRoom) == 0) {
				$result->status = 0;
				$result->exception = "Unauthorized chat attempt!";
				return $result;
			} else {
				$chatRoom = $chatRoom[0];
				if ($data->userRole == 4) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('student_subject');
					$select->where(array('courseId' => $chatRoom['courseId'],'subjectId' => $chatRoom['subjectId'],'userId' => $data->userId,'Active'=>1));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjectAccess = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$subjectAccess = $subjectAccess->toArray();
					$tableName = 'student_details';
				} else if ($data->userRole == 2) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('subject_professors');
					$select->where(array('subjectId' => $chatRoom['subjectId'],'professorId' => $data->userId));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjectAccess = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$subjectAccess = $subjectAccess->toArray();
					$tableName = 'professor_details';
				}
				if (count($subjectAccess) == 0) {
					$result->status = 0;
					$result->exception = "Unauthorized chat attempt!";
					return $result;
				} else {
					if ($chatRoom['room_type'] == 'private') {
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('chat_room_users');
						$select->where(array('roomId' => $data->chatRoomId,'userId' => $data->userId));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$chatRoomAccess = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$chatRoomAccess = $chatRoomAccess->toArray();
						if (count($chatRoomAccess) == 0) {
							$result->status = 0;
							$result->exception = "Unauthorized chat attempt!";
							return $result;
						}
					}
				}
			}
			if (isset($tableName) && !empty($tableName)) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from($tableName);
				$select->where(array('userId' => $data->userId));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$userData = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$userData = $userData->toArray();
				if (count($userData)>0) {
					$userData = $userData[0];
					$chatRoom['userName'] = $userData['firstName'].' '.$userData['lastName'];
					$chatRoom['avatar'] = $userData['profilePic'];
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
						require_once 'amazonRead.php';
						$chatRoom['avatar'] = getSignedURL($userData['profilePic']);
					}
				}
			}
			
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('courses');
			$select->where(array('id' => $chatRoom['courseId']));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$subjectData = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$subjectData = $subjectData->toArray();
			if (count($subjectData)>0) {
				$subjectData = $subjectData[0];
				$chatRoom['courseName'] = $subjectData['name'];
			}

			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('subjects');
			$select->where(array('id' => $chatRoom['subjectId']));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$subjectData = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$subjectData = $subjectData->toArray();
			if (count($subjectData)>0) {
				$subjectData = $subjectData[0];
				$chatRoom['subjectName'] = $subjectData['name'];
			}
			
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('chat_rooms');
			$update->set(array('last_accessed' => date("Y-m-d H:i:s")));
			$update->where(array('id'		=>	$data->chatRoomId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$statement->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();

			$result->chatRoom = $chatRoom;
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function markChatNotification($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			if (isset($data->notificationId) && !empty($data->notificationId)) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('chat_notifications');
				$select->where(array('id' => $data->notificationId));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$chatNoti = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$chatNoti = $chatNoti->toArray();
				if (count($chatNoti) == 0) {
					$result->status = 0;
					$result->message = "There was some error, please refresh and try again.";
					return $result;
				} else {
					//for main course student graph
					$selectString = "UPDATE chat_notifications SET status=1 WHERE id=$data->notificationId";
					$notification = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

					//if($data->chatAction == 1) {
						$result->roomId = $chatNoti[0]["roomId"];
					//}
				}
			} else {
				$result->status = 0;
				$result->message = "There was some error, please refresh and try again.";
				return $result;
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			
			$result->message ="Notification marked read";
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function setChatMessageNotification($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			if (isset($data->chatRoomId) && !empty($data->chatRoomId)) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('chat_rooms');
				$select->where(array('id' => $data->chatRoomId));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$chatRoom = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$chatRoom = $chatRoom->toArray();
				if (count($chatRoom) == 0) {
					$result->status = 0;
					$result->message = "There was some error, please refresh and try again.";
					return $result;
				} else {
					$chatRoom = $chatRoom[0];
					if ($chatRoom["room_type"] == "public") {
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('student_subject');
						if ($data->userRole == 4) {
							$select->where(array("courseId={$chatRoom['courseId']} AND subjectId={$chatRoom['subjectId']} AND userId != {$data->userId} AND Active=1"));
						} else {
							$select->where(array("courseId={$chatRoom['courseId']} AND subjectId={$chatRoom['subjectId']} AND Active=1"));
						}
						$select->group(array("userId"));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$subjectStudents = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$subjectStudents = $subjectStudents->toArray();
						if (count($subjectStudents)>0) {
							foreach ($subjectStudents as $key => $value) {
								$chat_notifications = new TableGateway('chat_notifications', $adapter, null, new HydratingResultSet());
								$insert = array(
									'roomId' => $data->chatRoomId,
									'senderId' => $data->userId,
									'userId' => $value["userId"],
									'type' => 1
								);
								$chat_notifications->insert($insert);
							}
						}
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('subject_professors');
						if ($data->userRole == 4) {
							$select->where(array("subjectId={$chatRoom['subjectId']}"));
						} else {
							$select->where(array("subjectId={$chatRoom['subjectId']} AND professorId != {$data->userId}"));
						}
						$select->group(array("professorId"));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$subjectProfessors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$subjectProfessors = $subjectProfessors->toArray();
						if (count($subjectProfessors)>0) {
							foreach ($subjectProfessors as $key => $value) {
								$chat_notifications = new TableGateway('chat_notifications', $adapter, null, new HydratingResultSet());
								$insert = array(
									'roomId' => $data->chatRoomId,
									'senderId' => $data->userId,
									'userId' => $value["professorId"],
									'type' => 1
								);
								$chat_notifications->insert($insert);
							}
						}
					} else {
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('chat_room_users');
						$select->where("roomId={$data->chatRoomId} AND userId!={$data->userId}");
						$select->group(array("userId"));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$subjectUsers = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$subjectUsers = $subjectUsers->toArray();
						if (count($subjectUsers)>0) {
							foreach ($subjectUsers as $key => $value) {
								$chat_notifications = new TableGateway('chat_notifications', $adapter, null, new HydratingResultSet());
								$insert = array(
									'roomId' => $data->chatRoomId,
									'senderId' => $data->userId,
									'userId' => $value["userId"],
									'type' => 1
								);
								$chat_notifications->insert($insert);
							}
						}
					}
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
				}
			} else {
				$result->status = 0;
				$result->message = "There was some error, please refresh and try again.";
				return $result;
			}
			
			$result->message ="Notifications sent successfully";
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

}
?>