<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Select;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Http\Client;
	
	require_once "Base.php";
	
	//$adapter = require "adapter.php";
	
	class CryptoWallet extends Base {
		/*
			function: this will check if name exists for submission			
		*/
		public function saveWalletAddress($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				if(!isset($data->address) || empty($data->address)) {
					$result->status = 0;
					$result->message = "No address detected!";
					return $result;
				}
				elseif (!isset($data->network) || empty($data->network) || $data->network!= 'rinkeby') {
					$result->status = 0;
					$result->message = "Please switch to main network on Metamask!";
					return $result;
				}
				elseif (!isset($data->blockchain) || empty($data->blockchain) || ($data->blockchain!= 'm' && $data->blockchain!= 'p')) {
					$result->status = 0;
					$result->message = "Please select a blockchain app!";
					return $result;
				}
				$select = $sql->select();
				$select->from('user_wallet');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('address', $data->address);
				$where->notEqualTo('userId', $data->userId);
				$select->where($where);
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				if (count($temp)>0) {
					$result->status = 0;
					$result->message = "Address is already being used!";
					return $result;
				}
				$select = $sql->select();
				$select->from('user_wallet');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('userId', $data->userId);
				$select->where($where);
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				if (count($temp) == 0) {
					$wallet = new TableGateway('user_wallet', $adapter, null,new HydratingResultSet());
					$insert = array(
						'userId'			=>	$data->userId,
						'address'			=>	$data->address,
						'app'				=>	(($data->blockchain=='p')?'portis':'metamask'),
						'created'			=>	date("Y-m-d H:i:s"),
						'modified'			=>	date("Y-m-d H:i:s")
					);
					$wallet->insert($insert);
				} else {
					$walletId = $temp[0]['id'];
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('user_wallet');
					$update->set(array(
										'address'			=>	$data->address,
										'app'				=>	(($data->blockchain=='p')?'portis':'metamask'),
										'modified'			=>	date("Y-m-d H:i:s")
					));
					$update->where(array('id'		=>	$walletId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = "Successfully saved your address";
				return $result;
			}
			catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		/*
			function: this will check if name exists for submission			
		*/
		public function getWalletAddress($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$select = $sql->select();
				$select->from('user_wallet');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('userId', $data->userId);
				$select->where($where);
				$select->columns(array(/*'id',*/ 'address','app'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				$result->wallet = '';
				$result->app = '';
				if (count($temp) > 0) {
					$result->wallet = $temp[0]['address'];
					$result->app	= $temp[0]['app'];
				}
				$result->status = 1;
				//$result->message = "Successfully saved your address";
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		/*
			function: this will check if name exists for submission			
		*/
		public function getIGROContractData($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$select = $sql->select();
				$select->from('igro_token_contract');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('id', 1);
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				$result->wallet = '';
				if (count($temp) > 0) {
					$result->ownerAddress = $temp[0]['ownerAddress'];
					$result->contractAddress = $temp[0]['contractAddress'];
					$result->contractCode = $temp[0]['contractCode'];
					$result->decimalPrecision = $temp[0]['decimalPrecision'];
					$result->balanceDivisor = $temp[0]['balanceDivisor'];
					$result->displayCurrency = $temp[0]['displayCurrency'];
				}
				$result->status = 1;
				//$result->message = "Successfully saved your address";
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		/*
			function: this will check if name exists for submission			
		*/
		public function saveCryptoTransaction($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('igro_transactions');
				$update->set(array(
									'transactionHash'	=>	$data->txnHash,
									'status'			=>	'process',
									'modified'			=>	date("Y-m-d H:i:s")
				));
				$update->where(array('id'		=>	$data->IGROTxnID));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = "<a href=\"https://rinkeby.etherscan.io/tx/{$data->txnHash}\" target=\"_blank\"><strong><u>Track Transaction</u></strong></a>: enrollment keys will reflect after successful transaction";
				return $result;
			}
			catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		/*
			function: this will check check IGRO transaction hash for success or fail
		*/
		public function checkIGROTransactions() {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$select = $sql->select();
				$select->from('igro_transactions');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('status', 'process');
				$where->notEqualTo('transactionHash', '');
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				if (count($temp) > 0) {
					foreach ($temp as $key => $txn) {
						//var_dump($txn);
						$uri = "https://api-rinkeby.etherscan.io/api?module=transaction&action=gettxreceiptstatus&txhash=".$txn['transactionHash']."&apikey=3USGZQT61Q245KAT319UPVVEH2V8C1NV8E";
						// This is actually exactly the same:
						/*$client = new Client($uri, array(
							'maxredirects' => 0,
							'timeout'      => 30
						));*/
						$client = new Client($uri, array(
							'maxredirects' => 0,
							'timeout'      => 30,
							'adapter'	   => 'Zend\Http\Client\Adapter\Curl'
						));
						try {
							// send request
							$response = $client->send();
							
							// print out response
							//echo "call success:";
							// in this case, it will print out xml
							// read my other tutorials on parsing xml string
							$resBody = json_decode($response->getBody());
							if ($resBody->status == 1) {
								//var_dump($resBody);
								if($resBody->result->status == '1') {
									$sql = new Sql($adapter);
									$update = $sql->update();
									$update->table('igro_transactions');
									$update->set(array(
														'status'			=>	'success',
														'modified'			=>	date("Y-m-d H:i:s")
									));
									$update->where(array('id'		=>	$txn['id']));
									$statement = $sql->prepareStatementForSqlObject($update);
									$statement->execute();

									$sql = new Sql($this->adapter);
									$select = $sql->select();
									$select->from('order_details');
									$where = new \Zend\Db\Sql\Where();
									$where->equalTo('IGROTxnID', $txn['id']);
									$select->where($where);
									$selectString = $sql->getSqlStringForSqlObject($select);
									$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
									$temp = $temp->toArray();
									$data = array_merge($txn,array('txn_status'=>$resBody->result->status,'paymentSkip'=>0));
									if (count($temp)>0) {
										$orderType = $temp[0]['type'];
										switch ($orderType) {
											case '1':
												require_once("Coursekeys.php");
												$ck = new Coursekeys();
												$ck->afterIGROPay($data);
												break;
											case '2':
												require_once("Course.php");
												$co = new Course();
												$co->afterIGROPay($data);
												break;
											case '4':
												require_once("StudentPackage.php");
												$sp = new StudentPackage();
												$sp->afterIGROPay($data);
												break;
											
											default:
												# code...
												break;
										}
									}
								} else if($resBody->result->status == '0') {
									$sql = new Sql($adapter);
									$update = $sql->update();
									$update->table('igro_transactions');
									$update->set(array(
														'status'			=>	'fail',
														'modified'			=>	date("Y-m-d H:i:s")
									));
									$update->where(array('id'		=>	$txn['id']));
									$statement = $sql->prepareStatementForSqlObject($update);
									$statement->execute();
									$statement->execute();
									//$ck = new Coursekeys();
									//$data = array_merge($txn,array('txn_status'=>$resBody->result->status));
								}
							}
							// If all succeed, commit the transaction and all changes are committed at once.
							$db->commit();
							//var_dump($resBody);							
						} 
						// Step 4. handle exception
						catch(Client_Exception $ce) {
							$db->rollBack();
							// catch Zend Http Client Exception
							$result->status = 0;
							$result->exception = $ce->getMessage();
							//echo "trace: " . var_dump($e->getTrace(), true);
							return $result;
						}
					}
				}
				$result->status = 1;
				//$result->message = "Successfully saved your address";
				return $result;
			}
			catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
	}
?>