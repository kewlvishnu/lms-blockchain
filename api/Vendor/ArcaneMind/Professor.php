<?php
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "User.php";



class Professor extends User{
	public $id;
	public $role;
	
	function __construct() {
		parent::__construct();
		$this->id = 0;
		$this->role = NULL;
	}

	public function registerShort($data) {
		$result = parent::registerShort($data);
		if($result->status == 0) {
			return $result;
		}
		$data->userId = $result->userId;
		$result = parent::addBasicDetailsShort($data);
		if($result->status == 0) {
			return $result;
		}
		
		$this->addProffesorProfileDetails($data);
		
		$this->addUserInstituteTypes($data);
		
		if(isset($data->googleId) && !empty($data->googleId)) {
			parent::validateAccountShort($data);
		} else {
			$result = parent::sendVerificationEmail($data);
			if($result->status == 0 ){
				return $result;
			}
		}
		
		$result = new stdClass();
		$result->status = 1;
		$result->userId = $data->userId;
		return $result;
	}

	public function register($data) {
		$result = parent::register($data);
		if($result->status == 0) {
			return $result;
		}
		$data->userId = $result->userId;
		$result = parent::addBasicDetails($data);
		if($result->status == 0) {
			return $result;
		}
		
		$this->addProffesorProfileDetails($data);
		
		$this->addUserInstituteTypes($data);
		
		
		$result = parent::sendVerificationEmail($data);
		if($result->status == 0 ){
			return $result;
		}
		
		$result = new stdClass();
		$result->status = 1;
		$result->userId = $data->userId;
		return $result;
	}
	
	public function addProffesorProfileDetails($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		try {
			$instituteDetails = new TableGateway('professor_details', $this->adapter, null,new HydratingResultSet());
			$instituteDetails->insert(array(
				'userId' 					=> 	$data->userId,
				'firstName' 			=>	$data->firstName,
				'lastName' 			=>	$data->lastName,
				'gender' => $data->gender,
				'dob' => $data->dob,
				// 'coverPic'			 	=>	$data->coverPic,
				// 'profilePic' 			=>	$data->profilePic,
				// 'altContact' 			=>	$data->altContact,
				// 'altEmail' 				=>	$data->altEmail,
				// 'foundedIn' 			=>	$data->foundedIn,
				// 'description' 		=>	$data->description
			));
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
		} catch (Exception $e) {
			$db->rollBack();
		}
	}
	
	/*
	*	$data 										: Object
	* $data->userId 						:	Int
	*	$data->newInstituteTypes	: Array
	*/
	
	
	public function addUserInstituteTypes($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		try {
			$userInstituteTypes = new TableGateway('user_institute_types', $this->adapter, null,new HydratingResultSet());
			foreach($data->stucturedInsTypes as $id=>$instituteData){
				if($id == -1)
					continue;
				
				$instituteJsonData = Zend\Json\Json::encode($instituteData, true);
				$userInstituteTypes->insert(array(
					'userId' 					=> $data->userId,
					'instituteTypeId'	=> $id,
					'data' => $instituteJsonData
				));
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
		} catch (Exception $e) {
			$db->rollBack();
		}
	}
	
	public function addNewInstituteSubtypes($data){

		$db = $this->adapter->getDriver()->getConnection();

		//$db->beginTransaction();
		$result = new stdClass();
		try {
			$instituteSubtypes = new TableGateway('institute_subtypes', $this->adapter, null,new HydratingResultSet());
			$result->newInstituteSubtypes = array();
			foreach($data->newInstituteSubtypes as $instituteSubtype){
				$instituteSubtypes->insert(array(
					'name' 						=> $instituteSubtype->name,
					'instituteTypeId' => $instituteSubtype->parentId,
					'addedBy' 				=> $data->userId,
					'createdAt' 			=> time(),
				));
				
				$newInstituteSubtype = new stdClass();
				$newInstituteSubtype->id = $instituteSubtypes->getLastInsertValue();
				$newInstituteSubtype->data = $instituteSubtype->data;
				
				$result->newInstituteSubtypes[] = $instituteSubtype;
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			//$db->commit();
			$result->status = 1;
			return $result;
		}catch(Exception $e){
			//$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	
	public function addUserInstituteSubtypes($data){

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		try {
			$userInstituteSubtypes = new TableGateway('user_institute_subtypes', $this->adapter, null,new HydratingResultSet());
			foreach($data->userInstituteSubtypes as $instituteSubtype){
				$userInstituteSubtypes->insert(array(
					'userId' 							=> $data->userId,
					'instituteSubtypeId'	=> $instituteSubtype->id,
					'data'								=> $instituteSubtype->data,
				));
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
		}catch(Exception $e){
			$db->rollBack();
		}
	}
	
	public function getProfileDetails($data) {
		/*
			Cover pic*
			Profile pic*
			Institute Name*
			Institute Background*
			****Categories******
			Owner Name*
			Num of students
			Founded year*
			Count Student *
			Contact Nums *
			Email
			Country *
			Address Full *
			Gender
		*/
		try {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			//Login Details
			$select->from('login_details');
			$select->columns(array('id','email', 'username'));
			
			$select->where(array('id' => $data->userId));
			$statement = $sql->prepareStatementForSqlObject($select);
			$loginDetails = $statement->execute();
			
			if($loginDetails->count() !== 0) {
				$result->loginDetails = $loginDetails->next();
			} else {
				$result->status = 0;
				$result->exception = "Login details not found";
				return $result;
			}
			
			// Basic details
			$select = $sql->select();
			$select->from(array('u' => 'user_details'))
					->join(array('c' => 'countries'),
							'u.addressCountry = c.id',
							array('addressCountry' => 'name', 'countryId' => 'id'));
			$select->columns(array('contactMobilePrefix' => 'contactMobilePrefix', 
									'contactMobile' => 'contactMobile', 
									'contactLandlinePrefix' => 'contactLandlinePrefix',
									'contactLandline' => 'contactLandline', 
									'addressStreet' => 'addressStreet', 
									'addressCity' => 'addressCity', 
									'addressState' => 'addressState', 
									'addressPin' => 'addressPin'));
			
			$select->where(array('u.userId' => $data->userId));
			$statement = $sql->prepareStatementForSqlObject($select);
			$userDetails = $statement->execute();
			//var_dump($userDetails);
			
			if($userDetails->count() !== 0) {
				$result->userDetails = $userDetails->next();
			} else {
				$result->status = 0;
				$result->exception = "User details not found";
				return $result;
			}
			
			// Fetching institute categories
			$select = $sql->select();
			$select->from(array('uit' => 'user_institute_types'))
			->join(
				array('it' => 'institute_types'),
				'it.id = uit.instituteTypeId',
				array('id','name','parentId')
			);
			$select->columns(array('data'));
			$select->where(array('uit.userId' => $data->userId));
			// $statement = $sql->prepareStatementForSqlObject($select);
			// $userInstituteTypes = $statement->execute();
			$selectString = $sql->getSqlStringForSqlObject($select);
			$userInstituteTypes = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			
				$result->userInstituteTypes = $userInstituteTypes->toArray();
			/*if($userInstituteTypes->count() !== 0) {
			} else {
				$result->status = 0;
				$result->exception = "User Institutes not found";
				return $result;
			}*/
			// Fetching Profile Details
			$select = $sql->select();
			$select->from('professor_details');
			$select->columns(array('Id'=>'id','firstName', 
				'lastName', 'coverPic', 'profilePic', 
				'description', 'gender'));
			
			$select->where(array('userId' => $data->userId));
			$statement = $sql->prepareStatementForSqlObject($select);
			$profileDetails = $statement->execute();
			
			if($profileDetails->count() !== 0) {
				$result->profileDetails = $profileDetails->next();
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$result->profileDetails['coverPic'] = getSignedURL($result->profileDetails['coverPic']);
					$result->profileDetails['profilePic'] = getSignedURL($result->profileDetails['profilePic']);
				} else {
					$result->profileDetails['profilePic'] = 'assets/layouts/layout2/img/avatar3_small.jpg';
				}
			} else {
				$result->status = 0;
				$result->exception = "Profile not found";
				return $result;
			}
			
			//fetching course details for arcanemind experience
			if (empty($query)) {
				$query="SELECT c.id,c.name,c.image,c.slug,c.studentPrice,c.studentPriceINR, COUNT(DISTINCT user_id) as studentCount
						FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id=c.id
						WHERE c.deleted =0 AND /*c.approved =1 AND c.availStudentMarket =1 AND c.liveForStudent =1 AND*/ c.ownerId = {$data->userId}
						GROUP BY c.id ORDER BY liveDate";
			}
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			$req1 = new stdClass();
			require_once 'Student.php';
			$s = new Student();
			require_once 'Course.php';
			$c = new Course();
			
			foreach ($result->courses as $key => $course) {
				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courses[$key]['image'] = getSignedURL($course['image']);
				}
				$result->courses[$key]['rating'] = $s->getCourseRatings($course['id']);
				$req1->courseId=$course['id'];
				$result->courses[$key]['discount']=$c->getCourseDiscount($req1);
				$result->courses[$key]['owner'] = $c->getCourseOwner($course['id']);
			}

			require_once 'StudentPackage.php';
			$sp = new StudentPackage();
			$professor = new stdClass();
			$professor->professorId = $data->userId;
			$result->packages = $sp->getAllPackageForStudent($professor);
			
			//fetching course details for arcanemind experience
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('courses');
			$select->where(array(
						'ownerId'				=>	$data->userId,
						'deleted'				=>	0
			));
			$select->columns(array('id', 'name', 'subtitle', 'image', 'studentPrice', 'studentPriceINR'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$courses = $courses->toArray();
			$subjectCount = 0;
			$studentCount = 0;
			foreach($courses as $key=>$course) {
				if($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$courses[$key]['image'] = getSignedURL($courses[$key]['image']);
				}
				require_once 'Student.php';
				$s = new Student();
				$courses[$key]['rating'] = $s->getCourseRatings($courses[$key]['id']);
				require_once "CourseDetail.php";
				$cd = new CourseDetail();
				$data = new stdClass();
				$data->courseId = $courses[$key]['id'];
				$courses[$key]['studentCount'] = $cd->getStudentCount($data);
				$studentCount+=$courses[$key]['studentCount'];
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subjects');
				$select->where(array(
						'courseId'	=>	$course['id'],
						'deleted'	=>	0
				));
				$select->columns(array('id', 'name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects = $subjects->toArray();
				$subjectCount+= count($subjects);
				foreach($subjects as $key1=>$subject) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('sp'	=>	'subject_professors'))
					->join(array('pd'	=>	'professor_details'),
								'sp.professorId = pd.userId',
								array('name'	=>	new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"))
					);
					$select->where(array('subjectId'	=>	$subject['id']));
					$select->columns(array());
					$selectString = $sql->getSqlStringForSqlObject($select);
					$professors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$professors = $professors->toArray();
					$subjects[$key1]['professors'] = $professors;
				}
				$courses[$key]['subjects'] = $subjects;
			}
			$result->courses = $courses;
			$result->subjectCount = $subjectCount;
			$result->studentCount = $studentCount;
			/*$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('courses');
			$select->where(array(
				'ownerId'				=>	$data->userId,
				'deleted'				=>	0,
				'approved'				=>	1,
				'availStudentMarket'	=>	1,
				'liveForStudent'		=>	1
			));
			$select->columns(array('id', 'name', 'subtitle', 'image', 'studentPrice', 'studentPriceINR'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$courses = $courses->toArray();
			foreach($courses as $key=>$course) {
				if($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$courses[$key]['image'] = getSignedURL($courses[$key]['image']);
				}
				require_once 'Student.php';
				$s = new Student();
				$courses[$key]['rating'] = $s->getCourseRatings($courses[$key]['id']);
				require_once "CourseDetail.php";
				$cd = new CourseDetail();
				$data = new stdClass();
				$data->courseId = $courses[$key]['id'];
				$courses[$key]['studentCount'] = $cd->getStudentCount($data);
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subjects');
				$select->where(array(
						'courseId'	=>	$course['id'],
						'deleted'	=>	0
				));
				$select->columns(array('id', 'name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects = $subjects->toArray();
				foreach($subjects as $key1=>$subject) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('sp'	=>	'subject_professors'))
					->join(array('pd'	=>	'professor_details'),
								'sp.professorId = pd.userId',
								array('name'	=>	new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"))
					);
					$select->where(array('subjectId'	=>	$subject['id']));
					$select->columns(array());
					$selectString = $sql->getSqlStringForSqlObject($select);
					$professors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$professors = $professors->toArray();
					$subjects[$key1]['professors'] = $professors;
				}
				$courses[$key]['subjects'] = $subjects;
			}
			$result->courses = $courses;*/
			
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function saveCoverImage($filePath, $userId) {

		$db = $this->adapter->getDriver()->getConnection();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$where = array('userId' => $userId);
			$sql = new Sql($adapter);
			
			//deleting previous image from cloud
			$select = $sql->select();
			$select->from('professor_details');
			$select->where($where);
			$select->columns(array('coverPic'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$coverPic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$coverPic = $coverPic->toArray();
			$coverPic = $coverPic[0]['coverPic'];
			$coverPic = substr($coverPic, 37);
			if($coverPic != '') {
				require_once 'amazonDelete.php';
				deleteFile($coverPic);
			}
			
			$db->beginTransaction();
			$update = $sql->update();
			$update->table('professor_details');
			$update->set(array('coverPic' => $filePath));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = 'Cover Image Uploaded!';
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	public function saveProfileImage($filePath, $userId) {

		$db = $this->adapter->getDriver()->getConnection();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$where = array('userId' => $userId);
			$sql = new Sql($adapter);
			
			//deleting previous image from cloud
			$select = $sql->select();
			$select->from('professor_details');
			$select->where($where);
			$select->columns(array('profilePic'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$profilePic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$profilePic = $profilePic->toArray();
			$profilePic = $profilePic[0]['profilePic'];
			$profilePic = substr($profilePic, 37);
			if($profilePic != '') {
				require_once 'amazonDelete.php';
				deleteFile($profilePic);
			}
			
			$db->beginTransaction();
			$update = $sql->update();
			$update->table('professor_details');
			$update->set(array('profilePic' => $filePath));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = 'Profile Image Uploaded!';
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	public function updateProfileDescription($req){

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$where = array('userId' => $req->userId);
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('professor_details');
			$update->set(array('description' => $req->description));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = 'Profile Updated!';
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	public function updateProfileGeneral($req){

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$where = array('userId' => $req->userId);
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('professor_details');
			$update->set(array('firstName' => $req->firstName,
								'lastName' => $req->lastName,
								'gender' => $req->gender));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();			
			//var_dump($statement);die();
			$update = $sql->update();
			$update->table('user_details');
			$update->set(array('contactMobilePrefix' => $req->phnPrefix,
								'contactMobile' => $req->phnNum,
								'contactLandlinePrefix' => $req->llPrefix,
								'contactLandline' => $req->llNum,
								'addressStreet' => $req->street,
								'addressCity' => $req->city,
								'addressState' => $req->state,
								'addressCountry' => $req->country,
								'addressPin' => $req->pin));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = 'Profile Updated!';
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function updateUserInstituteType($data){

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$delete = $sql->delete();
			//Login Details
			$delete->from('user_institute_types');
			$delete->where(array('userId' => $data->userId));
			$statement = $sql->prepareStatementForSqlObject($delete);
			$user_institute_types = $statement->execute();
			$userInstituteTypes = new TableGateway('user_institute_types', $adapter, null,new HydratingResultSet());
			foreach($data->newStructuredInstitutes as $id=>$instituteData) {
				if($id == -1)
					continue;
				$instituteJsonData = Zend\Json\Json::encode($instituteData, true);
				$userInstituteTypes->insert( array(
					'userId' 					=> $data->userId,
					'instituteTypeId'	=> $id,
					'data' => $instituteJsonData
				));
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = 'Profile Updated!';
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	/**
	* Invites professor if not already invited
	* @param $data mixed The request data
	* @return $result stdClass The result generated
	*
	* @Saxena A.
	*/
	public function addInvite($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('institute_invitations');
			$select->columns(array('id', 'prof_id', 'institute_id', 'status'));
			$select->where(array('prof_id' => $data->profId, 'institute_id' => $data->userId));

			$selectString = $sql->getSqlStringForSqlObject($select);
			$loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$loginDetails = $loginDetails->toArray();

			if (count($loginDetails) > 0) {
				$loginDetails = $loginDetails[0];
				if ($loginDetails['status'] < 2) {
					$result->status = 0;
					$result->message = "Already Invited";
					return $result;
				} else {
					$update = $sql->update();
					$update->table('institute_invitations');
					$updateInstituteInvite = array(
									'status'       => 0
									);
					$update->set($updateInstituteInvite);
					$where = array('id' => $loginDetails['id']);
					$update->where($where);
					$statement = $sql->prepareStatementForSqlObject($update);
					$count = $statement->execute()->getAffectedRows();
				}
			} else {
				$professorInvite = new TableGateway('institute_invitations', $this->adapter, null, new HydratingResultSet());
				$professorInvite->insert(array(
					'institute_id' => $data->userId,
					'prof_id'      => $data->profId,
					'status'       => 0
				));
			}

			/*$statement = $sql->prepareStatementForSqlObject($select);
			$loginDetails = $statement->execute();
			//if invitation exists, then do not add the same invitation again
			if($loginDetails->count() !== 0) {
				$result->status = 0;
				$result->message = "Already Invited";
				return $result;
			}
			// insert the invitation
			$professorInvite = new TableGateway('institute_invitations', $this->adapter, null, new HydratingResultSet());
			$professorInvite->insert(array(
				'institute_id' => $data->userId,
				'prof_id'      => $data->profId,
				'status'       => 0
			));*/
			$query="SELECT email,case "
					. " WHEN ur.roleId= 1 then (select name from institute_details where userId= l.id)"
					. " when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname) from professor_details where userId= l.id)"
					. " when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname) from publisher_details where userId= l.id)"
					. " end as institute "
					. " FROM login_details l JOIN user_roles ur on ur.userId=l.id where l.id={$data->userId} and ur.roleId NOT IN (4,5)";
		
			$institute = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$institute = $institute->toArray();
			$instituteId= $data->userId;
			$instituteName = $institute[0]['institute'];
			$instituteEmail = $institute[0]['email'];
		  
			$query="SELECT email,CONCAT(firstname, ' ', lastname)name FROM login_details l  JOIN professor_details p on p.userId=l.id  where l.id={$data->profId} ";
		
			$professor = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$professor = $professor->toArray();
			$professorId= $data->profId;
			$professorName = $professor[0]['name'];
			$professorEmail = $professor[0]['email'];
			
			
			// insert notification
			// now notivication for professor
			$text = "Institute $instituteName has invited you to join their institution";
			$notification="Invitation has been sent to professor  $professorName (  ID : $data->profId ). ";
			$query = "INSERT into notifications (userId, message, status) values ($instituteId,'$notification', 0), ($professorId, '$text', 0)";
			$notification=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
		
			require_once "User.php";
			$c = new User();
			
			$instituteSubject = "Invitation mail sent  to professor";
			$emailInstitute = "Dear Institute $instituteName, <br> <br>";
			$emailInstitute .= "PRofessor/Tutor <strong>$professorName ( ID : {$data->profId})</strong> has been invited by you. <br> <br>";
			global $siteBase;
			$link = $siteBase."admin/subject-invitation.php";
			$professorSubject = "Invitation mail sent by the institute ";
			$emailProfessor = "Dear Professor/Tutor $professorName, <br> <br>";
			$emailProfessor .= "Institute $instituteName  has invited you to be part of their institution.<br> <br>";
			$emailProfessor .= 'Please click on the link below to accept invitation.<br> <br>';
			$emailProfessor .= '<a href="' .$link . '" >Click here to join the Institute<a> <br> <br>';

			//$c->sendMail($instituteEmail, $instituteSubject, $emailInstitute);
			//$c->sendMail($professorEmail, $professorSubject, $emailProfessor);
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
		$result->status = 1;
		$result->message = "Invited";
		return $result;
	}
	
	/**
	* Gets the details of the professors
	* @param $data mixed The request 
	* @return $result stdClass The resultset obtained
	*
	* @Saxena A.
	*/
	public function getProfessors($data) {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$result = new stdClass();
		/*
		SELECT firstName, lastName, gender, profilePic, description, contactMobilePrefix, contactMobile, countries.name, email, institute_invitations.status
		FROM `professor_details` 
		inner join user_details on user_details.userId = professor_details.userId 
		inner join login_details on login_details.id = professor_details.userId 
		inner join countries on countries.id = user_details.addressCountry
		left outer join institute_invitations on professor_details.userId = institute_invitations.prof_id
		where professor_details.firstName = "fraaz";
		*/

		$select = $sql->select();
		$select->from(array('p' => 'professor_details'))
		->join(array('u' =>  'user_details'), 'u.userId = p.userId', array('contactMobilePrefix', 'contactMobile'))
		->join(array('l' => 'login_details'), 'l.id     = p.userId', array('email', 'id'))
		->join(array('c' => 'countries'    ), 'c.id = u.addressCountry', array('addressCountry' => 'name'),$select::JOIN_LEFT)
		->join(array('i' => 'institute_invitations'),
			new \Zend\Db\Sql\Expression("p.userId = i.prof_id AND i.institute_id={$data->userId}"),
			//"p.userId = i.prof_id",// AND i.institute_id={$data->userId}",
			array('invitedStatus' => 'status', 'instituteId' => 'institute_id'),
			$select::JOIN_LEFT
		);
		$select->columns(array(
			'profId' => 'userId',
			'firstName',
			'lastName',
			'gender',
			'profilePic',
			'description'
		));
		// type is name
		if ($data->query->type === 1) {
			$name = $data->query->text;
			$name = explode(" ", $name);
			if (array_key_exists(0, $name) && array_key_exists(1, $name)) {
				$select->where->like('p.firstName', '%'.$name[0]."%")->AND->like('p.lastName', '%'.$name[1]."%");
			}
			else {
				$select->where->like('p.firstName', '%'.$name[0]."%")->OR->like('p.lastName', '%'.$name[0]."%");
			}
		}
		//type is email
		else if ($data->query->type === 2) {
			$select->where->like('l.email', '%'.$data->query->text."%");
		}
		//type is country
		else if ($data->query->type === 3) {
			$select->where->like('c.name', '%'.$data->query->text."%");
		}
		$selectString = $sql->getSqlStringForSqlObject($select);
		//echo $selectString;
		$professors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		if($professors->count() !== 0) {
			$result->userDetails = $professors->toArray();
			foreach($result->userDetails as $key => $detail) {
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR'] <> '::1'){
					require_once 'amazonRead.php';
					$result->userDetails[$key]['profilePic'] = getSignedURL($detail['profilePic']);
				} else {
					$result->userDetails[$key]['profilePic'] = "assets/pages/media/users/avatar80_8.jpg";
				}
			}
		} else {
			$result->status = 0;
			$result->exception = "Error";
			return $result;
		}
		$result->userId = $data->userId;
		$result->status = 1;
		return $result;
	}
	public function updatePortfolio($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$portfolioName	= $data->portfolioName;
			$portfolioDesc	= $data->portfolioDesc;
			$portfolioPic	= $data->portfolioPic;
			$affiliation	= $data->affiliation;
			$favicon 		= $data->inputFavicon;
			$facebook 		= $data->inputFacebook;
			$twitter 		= $data->inputTwitter;
			$linkedin 		= $data->inputLinkedin;
			$status		    = $data->selectStatus;
			$subdomain 		= $data->slug;
			if(empty($portfolioName)) {
				$result->status = 0;
				$result->message = 'Portfolio Name is compulsory';
				return $result;
			} else if(empty($portfolioDesc)) {
				$result->status = 0;
				$result->message = 'Portfolio Description is compulsory';
				return $result;
			} else {
				$selectString ="SELECT `id`
								FROM  `pf_portfolio`
								WHERE `subdomain`='$subdomain' AND `userId`={$data->userId}";
				$subdomains = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subdomains = $subdomains->toArray();
				if(count($subdomains)>0) {
					$db->beginTransaction();
					$subdomains = $subdomains[0];
					$portfolioId  = $subdomains['id'];
					$where = array('id' => $portfolioId);
					$update = $sql->update();
					$update->table('pf_portfolio');
					$updatePortfolio = array('name' => $portfolioName,
										'description' => $portfolioDesc,
										'affiliation' => $affiliation,
										'favicon'	  => $favicon,
										'facebook'	  => $facebook,
										'twitter'	  => $twitter,
										'linkedin'	  => $linkedin,
										'status'	  => $status
									);
					if (!empty($portfolioPic)) {
						$updatePortfolio['img'] = $portfolioPic;
					}
					$update->set($updatePortfolio);
					$update->where($where);
					$statement = $sql->prepareStatementForSqlObject($update);
					$count = $statement->execute()->getAffectedRows();
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
				} else {
					$db->rollBack();
					$result->status = 0;
					$result->message = 'Invalid access';
					return $result;
				}
			}
			$result->status = 1;
			$result->message = 'Portfolio successfully updated';
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function getPageTypes($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$selectString ="SELECT `id` AS `value`, `type`
							FROM  `pf_page_types`
							WHERE (`user`='general' OR `user`='professor')";
			$pageTypes = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$pageTypes = $pageTypes->toArray();
			$result->pageTypes = array();
			$i = 0;
			foreach ($pageTypes as $key => $value) {
				switch ($value['type']) {
					case 'courses':
						$selectString ="SELECT pf.id
										FROM  `pf_courses` AS pc
										LEFT JOIN `pf_portfolio` AS pf ON pf.id=pc.portfolioId
										WHERE pf.subdomain='{$data->slug}'";
						$course = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$course = $course->toArray();
						if (count($course) == 0) {
							$result->pageTypes[$i]['value']	= $value['value'];
							$result->pageTypes[$i]['type']	= $value['type'];
							$i++;
						}
						break;
					case 'about':
						$selectString ="SELECT pf.id
										FROM  `pf_about` AS pa
										LEFT JOIN `pf_portfolio` AS pf ON pf.id=pa.portfolioId
										WHERE pf.subdomain='{$data->slug}'";
						$about = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$about = $about->toArray();
						if (count($about) == 0) {
							$result->pageTypes[$i]['value']	= $value['value'];
							$result->pageTypes[$i]['type']	= $value['type'];
							$i++;
						}
						break;
					case 'publications':
						$selectString ="SELECT pf.id
										FROM  `pf_publications` AS pp
										LEFT JOIN `pf_portfolio` AS pf ON pf.id=pp.portfolioId
										WHERE pf.subdomain='{$data->slug}'";
						$publications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$publications = $publications->toArray();
						if (count($publications) == 0) {
							$result->pageTypes[$i]['value']	= $value['value'];
							$result->pageTypes[$i]['type']	= $value['type'];
							$i++;
						}
						break;
					case 'teaching':
						$selectString ="SELECT pf.id
										FROM  `pf_teaching` AS pt
										LEFT JOIN `pf_portfolio` AS pf ON pf.id=pt.portfolioId
										WHERE pf.subdomain='{$data->slug}'";
						$teaching = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$teaching = $teaching->toArray();
						if (count($teaching) == 0) {
							$result->pageTypes[$i]['value']	= $value['value'];
							$result->pageTypes[$i]['type']	= $value['type'];
							$i++;
						}
						break;
					case 'gallery':
						$selectString ="SELECT pf.id
										FROM  `pf_gallery` AS pg
										LEFT JOIN `pf_portfolio` AS pf ON pf.id=pg.portfolioId
										WHERE pf.subdomain='{$data->slug}'";
						$gallery = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$gallery = $gallery->toArray();
						if (count($gallery) == 0) {
							$result->pageTypes[$i]['value']	= $value['value'];
							$result->pageTypes[$i]['type']	= $value['type'];
							$i++;
						}
						break;
					case 'contact':
						$selectString ="SELECT pf.id
										FROM  `pf_contact` AS pc
										LEFT JOIN `pf_portfolio` AS pf ON pf.id=pc.portfolioId
										WHERE pf.subdomain='{$data->slug}'";
						$contact = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$contact = $contact->toArray();
						if (count($contact) == 0) {
							$result->pageTypes[$i]['value']	= $value['value'];
							$result->pageTypes[$i]['type']	= $value['type'];
							$i++;
						}
						break;
					case 'custom':
						$result->pageTypes[$i]['value']	= $value['value'];
						$result->pageTypes[$i]['type']	= $value['type'];
						$i++;
						break;
					
					default:
						# code...
						break;
				}
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function getPortfolioPages($data) {
		try {
			$adapter = $this->adapter;
			$subdomain = $data->slug;
			if(isset($data->menu) && ($data->menu == 1)) {
				$sql = "SELECT pg.`portfolioId` AS portfolioId, pg.`id` AS pageId, pg.`pageTitle` AS `title`, ppt.`id` AS typeid, ppt.`type`, pg.`content`, pg.`status`, pm.`id`
						FROM `pf_pages` AS pg
						LEFT JOIN `pf_portfolio` AS pf ON pf.`id`=pg.`portfolioId`
						LEFT JOIN `pf_page_types` AS ppt ON ppt.`id`=pg.`pageType`
						LEFT JOIN `pf_menus` AS pm ON pm.`pageId`=pg.`id`
						WHERE pf.`subdomain`=? AND ISNULL(pm.`id`)";
			} else {
				$sql = "SELECT pg.`portfolioId` AS portfolioId, pg.`id` AS pageId, pg.`pageTitle` AS `title`, ppt.`id` AS typeid, ppt.`type`, pg.`content`, pg.`status`
						FROM `pf_pages` AS pg
						LEFT JOIN `pf_portfolio` AS pf ON pf.`id`=pg.`portfolioId`
						LEFT JOIN `pf_page_types` AS ppt ON ppt.`id`=pg.`pageType`
						WHERE pf.`subdomain`=?";
			}
			$portfolioPages = $adapter->query($sql, array($subdomain));
			$result = new stdClass();
			$result->status = 1;
			$result->valid = true;
			if ($portfolioPages->count() > 0) {
				$result->portfolioPages = $portfolioPages->toArray();
				foreach ($result->portfolioPages as $key => $page) {
					if($page['type'] == 'courses') {
						// Get courses page details
						$sql = "SELECT pf.`id` AS courseId, pf.`courses`
								FROM `pf_courses` AS pf
								WHERE pf.`portfolioId`=?";
						$pageDetail = $adapter->query($sql, array($page['portfolioId']));
						$pageArray 	= $pageDetail->toArray();
						$courses 	= $pageArray[0];
						$courses 	= explode(',', $courses['courses']);
						$result->portfolioPages[$key]['courses'] = $courses;
					} else if($page['type'] == 'about') {
						// Get about page details
						$sql = "SELECT pa.`id` AS aboutId, pa.`description`, pa.`academic_title`, pa.`education_title`, pa.`honors_title`
								FROM `pf_about` AS pa
								WHERE pa.`portfolioId`=?";
						$pageDetail = $adapter->query($sql, array($page['portfolioId']));
						$pageArray = $pageDetail->toArray();
						$aboutPage = $pageArray[0];

						// Get academic Positions
						$sql = "SELECT ap.`id`, ap.`fromyear`, ap.`toyear`, ap.`position`, ap.`university`, ap.`department`
								FROM `pf_academic_positions` AS ap
								WHERE ap.`aboutId`=?";
						$academicDetails = $adapter->query($sql, array($aboutPage['aboutId']));
						$academic = $academicDetails->toArray();
						foreach ($academic as $key1 => $ap) {
							if($ap['toyear'] == '0000') {
								$academic[$key1]['toyear'] = '';
							}
						}
						$aboutPage['academic'] = $academic;

						// Get education & training
						$sql = "SELECT pe.`id`, pe.`degree`, pe.`year`, pe.`title`, pe.`university`
								FROM `pf_education` AS pe
								WHERE pe.`aboutId`=?";
						$educationDetails = $adapter->query($sql, array($aboutPage['aboutId']));
						$education = $educationDetails->toArray();
						$aboutPage['education'] = $education;

						// Get honors details
						$sql = "SELECT ph.`id`, ph.`img` AS image, ph.`year`, ph.`title`, ph.`description`
								FROM `pf_honors` AS ph
								WHERE ph.`aboutId`=?";
						$honorsDetails = $adapter->query($sql, array($aboutPage['aboutId']));
						$honors = $honorsDetails->toArray();
						$aboutPage['honors'] = $honors;

						$result->portfolioPages[$key]['about'] = $aboutPage;
					} else if($page['type'] == 'publications') {
						// Get publications page details
						$sql = "SELECT pb.`id` AS publicationId, pb.`description`
								FROM `pf_publications` AS pb
								WHERE pb.`portfolioId`=?";
						$pageDetail = $adapter->query($sql, array($page['portfolioId']));
						$pageArray = $pageDetail->toArray();
						$publicationsPage = $pageArray[0];

						// Get publications
						$sql = "SELECT pd.`id`, pd.`publicationType` AS type, pt.`type` AS txtType, pd.`name`, pd.`description`, pd.`year`, pd.`isbn`
								FROM `pf_publication_detail` AS pd
								INNER JOIN `pf_publication_types` AS pt ON pt.id=pd.publicationType
								WHERE pd.`publicationId`=?";
						$publicationDetails = $adapter->query($sql, array($publicationsPage['publicationId']));
						$publications = $publicationDetails->toArray();
						$publicationsPage['publications'] = $publications;

						$result->portfolioPages[$key]['publications'] = $publicationsPage;
					} else if($page['type'] == 'teaching') {
						// Get teaching page details
						$sql = "SELECT pt.`id` AS teachingId, pt.`description`
								FROM `pf_teaching` AS pt
								WHERE pt.`portfolioId`=?";
						$pageDetail = $adapter->query($sql, array($page['portfolioId']));
						$pageArray = $pageDetail->toArray();
						$teachingPage = $pageArray[0];

						// Get teaching details
						$sql = "SELECT pd.`id`, pd.`title`, pd.`description`, pd.`fromyear` AS `from`, pd.`toyear` AS `to`
								FROM `pf_teaching_detail` AS pd
								WHERE pd.`teachingId`=?";
						$teachingDetails = $adapter->query($sql, array($teachingPage['teachingId']));
						$teaching = $teachingDetails->toArray();
						foreach ($teaching as $key1 => $ap) {
							if($ap['to'] == '0000') {
								$teaching[$key1]['to'] = '';
							}
						}
						$teachingPage['teaching'] = $teaching;

						$result->portfolioPages[$key]['teaching'] = $teachingPage;
					} else if($page['type'] == 'gallery') {
						// Get faculty page details
						$sql = "SELECT pg.`id` AS galleryId, pg.`description`
								FROM `pf_gallery` AS pg
								WHERE pg.`portfolioId`=?";
						$pageDetail = $adapter->query($sql, array($page['portfolioId']));
						$pageArray = $pageDetail->toArray();
						$galleryPage = $pageArray[0];

						// Get faculty members
						$sql = "SELECT pg.`id`, pg.`img` AS image, pg.`title`, pg.`description`
								FROM `pf_gallery_detail` AS pg
								WHERE pg.`galleryId`=?";
						$imagesDetails = $adapter->query($sql, array($galleryPage['galleryId']));
						$galleryImages = $imagesDetails->toArray();
						$galleryPage['galleryImages'] = $galleryImages;

						$result->portfolioPages[$key]['gallery'] = $galleryPage;
					} else if($page['type'] == 'contact') {
						// Get faculty page details
						$sql = "SELECT pc.`id` AS contactId, pc.`contact_description`, pc.`office_description`, pc.`work_description`, pc.`lab_description`
								FROM `pf_contact` AS pc
								WHERE pc.`portfolioId`=?";
						$pageDetail = $adapter->query($sql, array($page['portfolioId']));
						$pageArray = $pageDetail->toArray();
						$contactPage = $pageArray[0];

						// Get contacts
						$sql = "SELECT pc.`id`, pc.`type`, pc.`title`, pc.`value`
								FROM `pf_contact_detail` AS pc
								WHERE pc.`contactId`=?";
						$contactDetails = $adapter->query($sql, array($contactPage['contactId']));
						$contactInfo = $contactDetails->toArray();
						$contactPage['contactInfo'] = $contactInfo;

						$result->portfolioPages[$key]['contact'] = $contactPage;
					}
				}
			} else {
				$result->portfolioPages = array();
			}
			return $result;
		} catch (Exception $e) {
			$result = new stdClass();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function savePortfolioPage($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$pageType = $data->pageType;
			$pageTitle = $data->pageTitle;
			$subdomain 	   = $data->slug;
			$pageId = 0;
			if (isset($data->pageId) && !empty($data->pageId)) {
				$pageId = $data->pageId;
			}
			if(empty($pageType)) {
				$result->status = 0;
				$result->message = 'Please select Page Type';
				return $result;
			} else if(empty($pageTitle)) {
				$result->status = 0;
				$result->message = 'Page Title can\'t be left empty';
				return $result;
			} else if($pageType == '8' && empty($data->pageContent)) {
				$result->status = 0;
				$result->message = 'Page Content can\'t be left empty';
				return $result;
			} else {
				$selectString ="SELECT `id`
								FROM  `pf_portfolio`
								WHERE `subdomain`='$subdomain' AND `userId`={$data->userId}";
				$subdomains = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subdomains = $subdomains->toArray();
				if(count($subdomains)>0) {
					$subdomains = $subdomains[0];
					$portfolioId  = $subdomains['id'];
					if (empty($pageId)) {
						$portfolios = new TableGateway('pf_pages', $this->adapter, null, new HydratingResultSet());
						$result->portfolios = array();
						$insertPage = array(
							'portfolioId' => $portfolioId,
							'pageType' => $data->pageType,
							'pageTitle' => $pageTitle,
							'content' => '',
							'status' => 'active'
						);
						if ($pageType == '1') {
							$courses = new TableGateway('pf_courses', $this->adapter, null, new HydratingResultSet());
							$result->courses = array();
							$insertCourses = array(
								'portfolioId' => $portfolioId,
								'courses' => $data->inputCourses,
								'status' => 'active'
							);
							$courses->insert($insertCourses);
						} elseif ($pageType == '2') {
							//$about = new TableGateway('pf_about', $this->adapter, null, new HydratingResultSet());
							$result->about = array();
							$insertAbout = array(
								'portfolioId' => $portfolioId,
								'description' => $data->inputAboutDescription,
								'academic_title' => $data->inputAcademicTitle,
								'education_title' => $data->inputEducationTitle,
								'honors_title' => $data->inputHonorsTitle,
								'status' => 'active'
							);
							//$about->insert($insertAbout);
							//$aboutId = $about->getLastInsertValue();
							$query="INSERT INTO pf_about (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
							$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$adapter->query("SET @aboutId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
							//$aboutAcademicPositions = new TableGateway('pf_academic_positions', $this->adapter, null, new HydratingResultSet());
							$insertAcademicPositions = array();
							foreach ($data->aboutAcademic as $key => $value) {
								$insertAcademicPositions = array(
									//'aboutId' => $aboutId,
									'fromyear' => $value->fromyear,
									'toyear' => ((empty($value->toyear))?'NULL':$value->toyear),
									'position' => $value->position,
									'university' => $value->university,
									'department' => $value->department,
									'order' => 0
								);
								//$aboutAcademicPositions->insert($insertAcademicPositions);
								$query="INSERT INTO pf_academic_positions (`".implode("`,`", array_keys($insert))."`,`aboutId`) VALUES ('".implode("','", array_values($insert))."',@aboutId)";
								$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							}
							//$aboutEducation = new TableGateway('pf_education', $this->adapter, null, new HydratingResultSet());
							$insertEducation = array();
							foreach ($data->aboutEducation as $key => $value) {
								$insertEducation = array(
									//'aboutId' => $aboutId,
									'degree' => $value->degree,
									'year' => $value->year,
									'title' => $value->title,
									'university' => $value->university,
									'order' => 0
								);
								//$aboutEducation->insert($insertEducation);
								$query="INSERT INTO pf_education (`".implode("`,`", array_keys($insert))."`,`aboutId`) VALUES ('".implode("','", array_values($insert))."',@aboutId)";
								$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							}
							//$aboutHonors = new TableGateway('pf_honors', $this->adapter, null, new HydratingResultSet());
							$insertHonors = array();
							foreach ($data->aboutHonors as $key => $value) {
								$insertHonors = array(
									//'aboutId' => $aboutId,
									'img' => $value->image,
									'year' => $value->year,
									'title' => $value->title,
									'description' => $value->description,
									'order' => 0
								);
								//$aboutHonors->insert($insertHonors);
								$query="INSERT INTO pf_honors (`".implode("`,`", array_keys($insert))."`,`aboutId`) VALUES ('".implode("','", array_values($insert))."',@aboutId)";
								$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							}

							// fetch about page
							// Get about page details
							$query = "SELECT pa.`id` AS aboutId, pa.`description`, pa.`academic_title`, pa.`education_title`, pa.`honors_title`
									FROM `pf_about` AS pa
									WHERE pa.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$aboutPage = $pageArray[0];

							// Get academic Positions
							$query = "SELECT ap.`id`, ap.`fromyear`, ap.`toyear`, ap.`position`, ap.`university`, ap.`department`
									FROM `pf_academic_positions` AS ap
									WHERE ap.`aboutId`=?";
							$academicDetails = $adapter->query($query, array($aboutPage['aboutId']));
							$academic = $academicDetails->toArray();
							foreach ($academic as $key1 => $ap) {
								if($ap['toyear'] == '0000') {
									$academic[$key1]['toyear'] = '';
								}
							}
							$aboutPage['academic'] = $academic;

							// Get education & training
							$query = "SELECT pe.`id`, pe.`degree`, pe.`year`, pe.`title`, pe.`university`
									FROM `pf_education` AS pe
									WHERE pe.`aboutId`=?";
							$educationDetails = $adapter->query($query, array($aboutPage['aboutId']));
							$education = $educationDetails->toArray();
							$aboutPage['education'] = $education;

							// Get honors details
							$query = "SELECT ph.`id`, ph.`img` AS image, ph.`year`, ph.`title`, ph.`description`
									FROM `pf_honors` AS ph
									WHERE ph.`aboutId`=?";
							$honorsDetails = $adapter->query($query, array($aboutPage['aboutId']));
							$honors = $honorsDetails->toArray();
							$aboutPage['honors'] = $honors;

							$result->about = $aboutPage;
							$result->title = $pageTitle;
						} elseif ($pageType == '4') {
							//$publication = new TableGateway('pf_publications', $this->adapter, null, new HydratingResultSet());
							$result->publication = array();
							$insertPublication = array(
								'portfolioId'	=> $portfolioId,
								'description'	=> $data->inputPublicationsDescription,
								'status'		=> 'active'
							);
							//$publication->insert($insertPublication);
							//$publicationId = $publication->getLastInsertValue();
							$query="INSERT INTO pf_publications (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
							$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$adapter->query("SET @publicationId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
							//$publicationDetail = new TableGateway('pf_publication_detail', $this->adapter, null, new HydratingResultSet());
							$insertPublicationDetail = array();
							foreach ($data->publications as $key => $value) {
								$insertPublicationDetail = array(
									//'publicationId'	  => $publicationId,
									'publicationType' => $value->type,
									'name' 			  => $value->name,
									'description' 	  => $value->description,
									'year' 			  => $value->year,
									'isbn' 			  => $value->isbn,
									'order' => 0
								);
								//$publicationDetail->insert($insertPublicationDetail);
								$query="INSERT INTO pf_publication_detail (`".implode("`,`", array_keys($insert))."`,`publicationId`) VALUES ('".implode("','", array_values($insert))."',@publicationId)";
								$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							}

							// Get publications page details
							$query = "SELECT pb.`id` AS publicationId, pb.`description`
									FROM `pf_publications` AS pb
									WHERE pb.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$publicationsPage = $pageArray[0];

							// Get publications
							$query = "SELECT pd.`id`, pd.`publicationType` AS type, pt.`type` AS txtType, pd.`name`, pd.`description`, pd.`year`, pd.`isbn`
									FROM `pf_publication_detail` AS pd
									INNER JOIN `pf_publication_types` AS pt ON pt.id=pd.publicationType
									WHERE pd.`publicationId`=?";
							$publicationDetails = $adapter->query($query, array($publicationsPage['publicationId']));
							$publications = $publicationDetails->toArray();
							$publicationsPage['publications'] = $publications;

							$result->publications = $publicationsPage;
							$result->title		  = $pageTitle;
						} elseif ($pageType == '5') {
							//$teaching = new TableGateway('pf_teaching', $this->adapter, null, new HydratingResultSet());
							$result->teaching = array();
							$insertTeaching = array(
								'portfolioId'	=> $portfolioId,
								'description'	=> $data->inputTeachingDescription,
								'status'		=> 'active'
							);
							//$teaching->insert($insertTeaching);
							//$teachingId = $teaching->getLastInsertValue();
							$query="INSERT INTO pf_teaching (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
							$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$adapter->query("SET @teachingId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
							//$teachingDetail = new TableGateway('pf_teaching_detail', $this->adapter, null, new HydratingResultSet());
							$insertTeachingDetail = array();
							foreach ($data->teaching as $key => $value) {
								$insertTeachingDetail = array(
									//'teachingId'	=> $teachingId,
									'title' 		=> $value->title,
									'description' 	=> $value->description,
									'fromyear' 		=> $value->from,
									'toyear' 		=> $value->to,
									'order' => 0
								);
								//$teachingDetail->insert($insertTeachingDetail);
								$query="INSERT INTO pf_teaching_detail (`".implode("`,`", array_keys($insert))."`,`teachingId`) VALUES ('".implode("','", array_values($insert))."',@teachingId)";
								$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							}
						} elseif ($pageType == '6') {
							//$gallery = new TableGateway('pf_gallery', $this->adapter, null, new HydratingResultSet());
							$result->gallery = array();
							$insertGallery = array(
								'portfolioId' => $portfolioId,
								'description' => $data->inputGalleryDescription,
								'status'	  => 'active'
							);
							//$gallery->insert($insertGallery);
							//$galleryId = $gallery->getLastInsertValue();
							$query="INSERT INTO pf_gallery (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
							$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$adapter->query("SET @galleryId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
							//$galleryImages = new TableGateway('pf_gallery_detail', $this->adapter, null, new HydratingResultSet());
							$insertGalleryImages = array();
							foreach ($data->galleryImages as $key => $value) {
								$insertGalleryImages = array(
									//'galleryId'	  => $galleryId,
									'img' 		  => $value->image,
									'title'		  => $value->title,
									'description' => $value->description,
									'order' => 0
								);
								//$galleryImages->insert($insertGalleryImages);
								$query="INSERT INTO pf_gallery_detail (`".implode("`,`", array_keys($insert))."`,`galleryId`) VALUES ('".implode("','", array_values($insert))."',@galleryId)";
								$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							}

							// fetch gallery page
							// Get gallery page details
							$query = "SELECT pg.`id` AS galleryId, pg.`description`
									FROM `pf_gallery` AS pg
									WHERE pg.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$galleryPage = $pageArray[0];

							// Get gallery members
							$query = "SELECT pg.`id`, pg.`img` AS image, pg.`title`, pg.`description`
									FROM `pf_gallery_detail` AS pg
									WHERE pg.`galleryId`=?";
							$imagesDetails = $adapter->query($query, array($galleryPage['galleryId']));
							$galleryImages = $imagesDetails->toArray();
							$galleryPage['galleryImages'] = $galleryImages;

							$result->gallery = $galleryPage;
							$result->title	 = $pageTitle;
						} elseif ($pageType == '7') {
							//$contact = new TableGateway('pf_contact', $this->adapter, null, new HydratingResultSet());
							$result->contact = array();
							$insertContact = array(
								'portfolioId' 		  => $portfolioId,
								'contact_description' => $data->inputContactDescription,
								'office_description'  => $data->inputOfficeDescription,
								'work_description' 	  => $data->inputWorkDescription,
								'lab_description' 	  => $data->inputLabDescription,
								'status' 			  => 'active'
							);
							//$contact->insert($insertContact);
							//$contactId = $contact->getLastInsertValue();
							$query="INSERT INTO pf_contact (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
							$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$adapter->query("SET @contactId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
							//$contactInfo = new TableGateway('pf_contact_detail', $this->adapter, null, new HydratingResultSet());
							$insertContactInfo = array();
							foreach ($data->contactInfo as $key => $value) {
								$insertContactInfo = array(
									//'contactId' => $contactId,
									'type' => $value->type,
									'title' => $value->title,
									'value' => $value->value,
									'order' => 0
								);
								//$contactInfo->insert($insertContactInfo);
								$query="INSERT INTO pf_contact_detail (`".implode("`,`", array_keys($insert))."`,`contactId`) VALUES ('".implode("','", array_values($insert))."',@contactId)";
								$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							}

							// Fetch contact page
							// Get contact page details
							$query = "SELECT pc.`id` AS contactId, pc.`contact_description`, pc.`office_description`, pc.`work_description`, pc.`lab_description`
									FROM `pf_contact` AS pc
									WHERE pc.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$contactPage = $pageArray[0];

							// Get contacts
							$query = "SELECT pc.`id`, pc.`type`, pc.`title`, pc.`value`
									FROM `pf_contact_detail` AS pc
									WHERE pc.`contactId`=?";
							$contactDetails = $adapter->query($query, array($contactPage['contactId']));
							$contactInfo = $contactDetails->toArray();
							$contactPage['contactInfo'] = $contactInfo;

							$result->contact = $contactPage;
							$result->title	 = $pageTitle;
						} elseif ($pageType == '8') {
							$insertPage['content'] = $data->pageContent;
						}
						$portfolios->insert($insertPage);
						$result->status = 1;
						$result->message = 'Portfolio page added';
					} else {
						$where = array('portfolioId' => $portfolioId,'id' => $pageId);
						$update = $sql->update();
						$update->table('pf_pages');
						$updatePage = array('pageType' => $pageType, 'pageTitle' => $pageTitle);
						if ($pageType == '1') {
							$where1 = array('portfolioId' => $portfolioId);
							$update1 = $sql->update();
							$update1->table('pf_courses');
							$updatePage1 = array('courses' => $data->inputCourses);
							$update1->set($updatePage1);
							$update1->where($where1);
							$statement1 = $sql->prepareStatementForSqlObject($update1);
							$count = $statement1->execute()->getAffectedRows();
						} else if ($pageType == '2') {
							$where1 = array('portfolioId' => $portfolioId);
							$update1 = $sql->update();
							$update1->table('pf_about');
							$updatePage1 = array('description' => $data->inputAboutDescription,
													'academic_title' => $data->inputAcademicTitle,
													'education_title' => $data->inputEducationTitle,
													'honors_title' => $data->inputHonorsTitle);
							$update1->set($updatePage1);
							$update1->where($where1);
							$statement1 = $sql->prepareStatementForSqlObject($update1);
							$count = $statement1->execute()->getAffectedRows();
							$selectString ="SELECT `id`
											FROM  `pf_about`
											WHERE `portfolioId`='$portfolioId'";
							$aboutDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$aboutDetails = $aboutDetails->toArray();
							$aboutId 	  = $aboutDetails[0]['id'];

							// manage academinc positions
							$aboutAcademic= $data->aboutAcademic;
							$deleteIds	  = '';
							foreach ($aboutAcademic as $key => $value) {
								//var_dump($value);
								$pre =  substr($value->id, 0, 1);
								if($pre != 'n') {
									// update academinc positions
									$deleteIds.= $value->id.",";
									$where1 = array('id' => $value->id);
									$update1 = $sql->update();
									$update1->table('pf_academic_positions');
									$updatePage1 = array('fromyear' => $value->fromyear,
															'toyear' => ((empty($value->toyear))?'NULL':$value->toyear),
															'position' => $value->position,
															'university' => $value->university,
															'department' => $value->department);
									$update1->set($updatePage1);
									$update1->where($where1);
									$statement1 = $sql->prepareStatementForSqlObject($update1);
									$count = $statement1->execute()->getAffectedRows();
								} else {
									// insert new academinc positions
									$aboutSubTable = new TableGateway('pf_academic_positions', $this->adapter, null, new HydratingResultSet());
									$insertSubTableData = array(
										'aboutId' => $aboutId,
										'fromyear' => $value->fromyear,
										'toyear' => ((empty($value->toyear))?'NULL':$value->toyear),
										'position' => $value->position,
										'university' => $value->university,
										'department' => $value->department
									);
									$aboutSubTable->insert($insertSubTableData);
									$subid = $aboutSubTable->getLastInsertValue();
									$deleteIds.= $subid.",";
								}
							}
							// delete removed academinc positions
							$deleteIds = substr($deleteIds, 0, -1);
							$sql = new Sql($adapter);
							$delete = $sql->delete();
							$delete->from('pf_academic_positions');
							if (!empty($deleteIds)) {
								$delete->where("aboutId = ".$aboutId." AND id NOT IN ($deleteIds)");
							} else {
								$delete->where("aboutId = ".$aboutId);
							}
							$statement = $sql->prepareStatementForSqlObject($delete);
							$delAcademicPositions = $statement->execute();

							// manage education & training
							$aboutEducation= $data->aboutEducation;
							$deleteIds	  = '';
							foreach ($aboutEducation as $key => $value) {
								//var_dump($value);
								$pre =  substr($value->id, 0, 1);
								if($pre != 'n') {
									// update education & training
									$deleteIds.= $value->id.",";
									$where1 = array('id' => $value->id);
									$update1 = $sql->update();
									$update1->table('pf_education');
									$updatePage1 = array('degree' => $value->degree,
															'year' => $value->year,
															'title' => $value->title,
															'university' => $value->university);
									$update1->set($updatePage1);
									$update1->where($where1);
									$statement1 = $sql->prepareStatementForSqlObject($update1);
									$count = $statement1->execute()->getAffectedRows();
								} else {
									// insert new education & training
									$aboutSubTable = new TableGateway('pf_education', $this->adapter, null, new HydratingResultSet());
									$insertSubTableData = array(
										'aboutId' => $aboutId,
										'degree' => $value->degree,
										'year' => $value->year,
										'title' => $value->title,
										'university' => $value->university
									);
									$aboutSubTable->insert($insertSubTableData);
									$subid = $aboutSubTable->getLastInsertValue();
									$deleteIds.= $subid.",";
								}
							}
							// delete removed education & training
							$deleteIds = substr($deleteIds, 0, -1);
							$sql = new Sql($adapter);
							$delete = $sql->delete();
							$delete->from('pf_education');
							if (!empty($deleteIds)) {
								$delete->where("aboutId = ".$aboutId." AND id NOT IN ($deleteIds)");
							} else {
								$delete->where("aboutId = ".$aboutId);
							}
							$statement = $sql->prepareStatementForSqlObject($delete);
							$delEducation = $statement->execute();

							// manage honors
							$aboutHonors= $data->aboutHonors;
							$deleteIds	  = '';
							foreach ($aboutHonors as $key => $value) {
								//var_dump($value);
								$pre =  substr($value->id, 0, 1);
								if($pre != 'n') {
									// update honors
									$deleteIds.= $value->id.",";
									$where1 = array('id' => $value->id);
									$update1 = $sql->update();
									$update1->table('pf_honors');
									$updatePage1 = array('img' => $value->image,
															'year' => $value->year,
															'title' => $value->title,
															'description' => $value->description);
									$update1->set($updatePage1);
									$update1->where($where1);
									$statement1 = $sql->prepareStatementForSqlObject($update1);
									$count = $statement1->execute()->getAffectedRows();
								} else {
									// insert new honors
									$aboutSubTable = new TableGateway('pf_honors', $this->adapter, null, new HydratingResultSet());
									$insertSubTableData = array(
										'aboutId' => $aboutId,
										'img' => $value->image,
										'year' => $value->year,
										'title' => $value->title,
										'description' => $value->description
									);
									$aboutSubTable->insert($insertSubTableData);
									$subid = $aboutSubTable->getLastInsertValue();
									$deleteIds.= $subid.",";
								}
							}
							// delete removed honors
							$deleteIds = substr($deleteIds, 0, -1);
							$sql = new Sql($adapter);
							$delete = $sql->delete();
							$delete->from('pf_honors');
							if (!empty($deleteIds)) {
								$delete->where("aboutId = ".$aboutId." AND id NOT IN ($deleteIds)");
							} else {
								$delete->where("aboutId = ".$aboutId);
							}
							$statement = $sql->prepareStatementForSqlObject($delete);
							$delHonors = $statement->execute();

							// fetch about page
							// Get about page details
							$query = "SELECT pa.`id` AS aboutId, pa.`description`, pa.`academic_title`, pa.`education_title`, pa.`honors_title`
									FROM `pf_about` AS pa
									WHERE pa.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$aboutPage = $pageArray[0];

							// Get academic Positions
							$query = "SELECT ap.`id`, ap.`fromyear`, ap.`toyear`, ap.`position`, ap.`university`, ap.`department`
									FROM `pf_academic_positions` AS ap
									WHERE ap.`aboutId`=?";
							$academicDetails = $adapter->query($query, array($aboutPage['aboutId']));
							$academic = $academicDetails->toArray();
							foreach ($academic as $key1 => $ap) {
								if($ap['toyear'] == '0000') {
									$academic[$key1]['toyear'] = '';
								}
							}
							$aboutPage['academic'] = $academic;

							// Get education & training
							$query = "SELECT pe.`id`, pe.`degree`, pe.`year`, pe.`title`, pe.`university`
									FROM `pf_education` AS pe
									WHERE pe.`aboutId`=?";
							$educationDetails = $adapter->query($query, array($aboutPage['aboutId']));
							$education = $educationDetails->toArray();
							$aboutPage['education'] = $education;

							// Get honors details
							$query = "SELECT ph.`id`, ph.`img` AS image, ph.`year`, ph.`title`, ph.`description`
									FROM `pf_honors` AS ph
									WHERE ph.`aboutId`=?";
							$honorsDetails = $adapter->query($query, array($aboutPage['aboutId']));
							$honors = $honorsDetails->toArray();
							$aboutPage['honors'] = $honors;

							$result->about = $aboutPage;
							$result->title = $pageTitle;
						} else if ($pageType == '4') {
							$where1 = array('portfolioId' => $portfolioId);
							$update1 = $sql->update();
							$update1->table('pf_publications');
							$updatePage1 = array('description' => $data->inputPublicationsDescription);
							$update1->set($updatePage1);
							$update1->where($where1);
							$statement1 = $sql->prepareStatementForSqlObject($update1);
							$count = $statement1->execute()->getAffectedRows();
							$selectString ="SELECT `id`
											FROM  `pf_publications`
											WHERE `portfolioId`='$portfolioId'";
							$publicationDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$publicationDetails = $publicationDetails->toArray();
							$publicationId = $publicationDetails[0]['id'];

							// manage publications
							$publications= $data->publications;
							$deleteIds	  = '';
							foreach ($publications as $key => $value) {
								//var_dump($value);
								$pre =  substr($value->id, 0, 1);
								if($pre != 'n') {
									// update publications
									$deleteIds.= $value->id.",";
									$where1 = array('id' => $value->id);
									$update1 = $sql->update();
									$update1->table('pf_publication_detail');
									$updatePage1 = array('publicationType' => $value->type,
															'name' 			  => $value->name,
															'description' 	  => $value->description,
															'year' 			  => $value->year,
															'isbn' 			  => $value->isbn);
									$update1->set($updatePage1);
									$update1->where($where1);
									$statement1 = $sql->prepareStatementForSqlObject($update1);
									$count = $statement1->execute()->getAffectedRows();
								} else {
									// insert new publications
									$publicationsTable = new TableGateway('pf_publication_detail', $this->adapter, null, new HydratingResultSet());
									$insertPublicationsTableData = array(
										'publicationId' => $publicationId,
										'publicationType' => $value->type,
										'name' 			  => $value->name,
										'description' 	  => $value->description,
										'year' 			  => $value->year,
										'isbn' 			  => $value->isbn
									);
									$publicationsTable->insert($insertPublicationsTableData);
									$pbid = $publicationsTable->getLastInsertValue();
									$deleteIds.= $pbid.",";
								}
							}
							// delete removed publications
							$deleteIds = substr($deleteIds, 0, -1);
							$sql = new Sql($adapter);
							$delete = $sql->delete();
							$delete->from('pf_publication_detail');
							if (!empty($deleteIds)) {
								$delete->where("publicationId = ".$publicationId." AND id NOT IN ($deleteIds)");
							} else {
								$delete->where("publicationId = ".$publicationId);
							}
							$statement = $sql->prepareStatementForSqlObject($delete);
							$delPublications = $statement->execute();

							// Get publications page details
							$query = "SELECT pb.`id` AS publicationId, pb.`description`
									FROM `pf_publications` AS pb
									WHERE pb.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$publicationsPage = $pageArray[0];

							// Get publications
							$query = "SELECT pd.`id`, pd.`publicationType` AS type, pt.`type` AS txtType, pd.`name`, pd.`description`, pd.`year`, pd.`isbn`
									FROM `pf_publication_detail` AS pd
									INNER JOIN `pf_publication_types` AS pt ON pt.id=pd.publicationType
									WHERE pd.`publicationId`=?";
							$publicationDetails = $adapter->query($query, array($publicationsPage['publicationId']));
							$publications = $publicationDetails->toArray();
							$publicationsPage['publications'] = $publications;

							$result->publications = $publicationsPage;
							$result->title		  = $pageTitle;
						} else if ($pageType == '5') {
							$where1 = array('portfolioId' => $portfolioId);
							$update1 = $sql->update();
							$update1->table('pf_teaching');
							$updatePage1 = array('description' => $data->inputTeachingDescription);
							$update1->set($updatePage1);
							$update1->where($where1);
							$statement1 = $sql->prepareStatementForSqlObject($update1);
							$count = $statement1->execute()->getAffectedRows();
							$selectString ="SELECT `id`
											FROM  `pf_teaching`
											WHERE `portfolioId`='$portfolioId'";
							$teachingDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$teachingDetails = $teachingDetails->toArray();
							$teachingId = $teachingDetails[0]['id'];

							// manage teaching
							$teaching= $data->teaching;
							$deleteIds	  = '';
							foreach ($teaching as $key => $value) {
								//var_dump($value);
								$pre =  substr($value->id, 0, 1);
								if($pre != 'n') {
									// update teaching
									$deleteIds.= $value->id.",";
									$where1 = array('id' => $value->id);
									$update1 = $sql->update();
									$update1->table('pf_teaching_detail');
									$updatePage1 = array('title' 		=> $value->title,
															'description' 	=> $value->description,
															'fromyear' 		=> $value->from,
															'toyear' 		=> $value->to);
									$update1->set($updatePage1);
									$update1->where($where1);
									$statement1 = $sql->prepareStatementForSqlObject($update1);
									$count = $statement1->execute()->getAffectedRows();
								} else {
									// insert new teaching
									$teachingTable = new TableGateway('pf_teaching_detail', $this->adapter, null, new HydratingResultSet());
									$insertTeachingTableData = array(
										'teachingId'	=> $teachingId,
										'title' 		=> $value->title,
										'description' 	=> $value->description,
										'fromyear' 		=> $value->from,
										'toyear' 		=> $value->to
									);
									$teachingTable->insert($insertTeachingTableData);
									$tcid = $teachingTable->getLastInsertValue();
									$deleteIds.= $tcid.",";
								}
							}
							// delete removed teaching
							$deleteIds = substr($deleteIds, 0, -1);
							$sql = new Sql($adapter);
							$delete = $sql->delete();
							$delete->from('pf_teaching_detail');
							if (!empty($deleteIds)) {
								$delete->where("teachingId = ".$teachingId." AND id NOT IN ($deleteIds)");
							} else {
								$delete->where("teachingId = ".$teachingId);
							}
							$statement = $sql->prepareStatementForSqlObject($delete);
							$delPublications = $statement->execute();

							// Get teaching page details
							$query = "SELECT pt.`id` AS teachingId, pt.`description`
									FROM `pf_teaching` AS pt
									WHERE pt.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$teachingPage = $pageArray[0];

							// Get teaching details
							$query = "SELECT pd.`id`, pd.`title`, pd.`description`, pd.`fromyear` AS `from`, pd.`toyear` AS `to`
									FROM `pf_teaching_detail` AS pd
									WHERE pd.`teachingId`=?";
							$teachingDetails = $adapter->query($query, array($teachingPage['teachingId']));
							$teaching = $teachingDetails->toArray();
							foreach ($teaching as $key1 => $ap) {
								if($ap['to'] == '0000') {
									$teaching[$key1]['to'] = '';
								}
							}
							$teachingPage['teaching'] = $teaching;

							$result->teaching = $teachingPage;
							$result->title	  = $pageTitle;
						} else if ($pageType == '6') {
							$where1 = array('portfolioId' => $portfolioId);
							$update1 = $sql->update();
							$update1->table('pf_gallery');
							$updatePage1 = array('description' => $data->inputGalleryDescription);
							$update1->set($updatePage1);
							$update1->where($where1);
							$statement1 = $sql->prepareStatementForSqlObject($update1);
							$count = $statement1->execute()->getAffectedRows();
							$selectString ="SELECT `id`
											FROM  `pf_gallery`
											WHERE `portfolioId`='$portfolioId'";
							$galleryDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$galleryDetails = $galleryDetails->toArray();
							$galleryId = $galleryDetails[0]['id'];

							// manage faculty members
							$galleryImages= $data->galleryImages;
							$deleteIds	  = '';
							foreach ($galleryImages as $key => $value) {
								//var_dump($value);
								$pre =  substr($value->id, 0, 1);
								if($pre != 'n') {
									// update gallery images
									$deleteIds.= $value->id.",";
									$where1 = array('id' => $value->id);
									$update1 = $sql->update();
									$update1->table('pf_gallery_detail');
									$updatePage1 = array('img' => $value->image,
														'title' => $value->title,
														'description' => $value->description);
									$update1->set($updatePage1);
									$update1->where($where1);
									$statement1 = $sql->prepareStatementForSqlObject($update1);
									$count = $statement1->execute()->getAffectedRows();
								} else {
									// insert new gallery images
									$galleryImagesTable = new TableGateway('pf_gallery_detail', $this->adapter, null, new HydratingResultSet());
									$insertGalleryImagesTableData = array(
										'galleryId' => $galleryId,
										'img' => $value->image,
										'title' => $value->title,
										'description' => $value->description
									);
									$galleryImagesTable->insert($insertGalleryImagesTableData);
									$giid = $galleryImagesTable->getLastInsertValue();
									$deleteIds.= $giid.",";
								}
							}
							// delete removed gallery images
							$deleteIds = substr($deleteIds, 0, -1);
							$sql = new Sql($adapter);
							$delete = $sql->delete();
							$delete->from('pf_gallery_detail');
							if (!empty($deleteIds)) {
								$delete->where("galleryId = ".$galleryId." AND id NOT IN ($deleteIds)");
							} else {
								$delete->where("galleryId = ".$galleryId);
							}
							$delete->where("id NOT IN ($deleteIds)");
							$statement = $sql->prepareStatementForSqlObject($delete);
							$delGalleryImages = $statement->execute();

							// fetch gallery page
							// Get gallery page details
							$query = "SELECT pg.`id` AS galleryId, pg.`description`
									FROM `pf_gallery` AS pg
									WHERE pg.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$galleryPage = $pageArray[0];

							// Get gallery members
							$query = "SELECT pg.`id`, pg.`img` AS image, pg.`title`, pg.`description`
									FROM `pf_gallery_detail` AS pg
									WHERE pg.`galleryId`=?";
							$imagesDetails = $adapter->query($query, array($galleryPage['galleryId']));
							$galleryImages = $imagesDetails->toArray();
							$galleryPage['galleryImages'] = $galleryImages;

							$result->gallery = $galleryPage;
							$result->title	 = $pageTitle;
						} else if ($pageType == '7') {
							$where1 = array('portfolioId' => $portfolioId);
							$update1 = $sql->update();
							$update1->table('pf_contact');
							$updatePage1 = array('contact_description' => $data->inputContactDescription,
													'office_description' => $data->inputOfficeDescription,
													'work_description' => $data->inputWorkDescription,
													'lab_description' => $data->inputLabDescription);
							$update1->set($updatePage1);
							$update1->where($where1);
							$statement1 = $sql->prepareStatementForSqlObject($update1);
							$count = $statement1->execute()->getAffectedRows();
							$selectString ="SELECT `id`
											FROM  `pf_contact`
											WHERE `portfolioId`='$portfolioId'";
							$contactDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$contactDetails = $contactDetails->toArray();
							$contactId = $contactDetails[0]['id'];

							// manage contacts
							$contactInfo= $data->contactInfo;
							$deleteIds	  = '';
							foreach ($contactInfo as $key => $value) {
								//var_dump($value);
								$pre =  substr($value->id, 0, 1);
								if($pre != 'n') {
									// update faculty members
									$deleteIds.= $value->id.",";
									$where1 = array('id' => $value->id);
									$update1 = $sql->update();
									$update1->table('pf_contact_detail');
									$updatePage1 = array('type' => $value->type,
														'title' => $value->title,
														'value' => $value->value);
									$update1->set($updatePage1);
									$update1->where($where1);
									$statement1 = $sql->prepareStatementForSqlObject($update1);
									$count = $statement1->execute()->getAffectedRows();
								} else {
									// insert new faculty members
									$contactInfoTable = new TableGateway('pf_contact_detail', $this->adapter, null, new HydratingResultSet());
									$insertContactInfoTableData = array(
										'contactId' => $contactId,
										'type' => $value->type,
										'title' => $value->title,
										'value' => $value->value
									);
									$contactInfoTable->insert($insertContactInfoTableData);
									$ciid = $contactInfoTable->getLastInsertValue();
									$deleteIds.= $ciid.",";
								}
							}
							// delete removed faculty members
							$deleteIds = substr($deleteIds, 0, -1);
							if (!empty($deleteIds)) {
								$sql = new Sql($adapter);
								$delete = $sql->delete();
								$delete->from('pf_contact_detail');
								$delete->where("id NOT IN ($deleteIds)");
								$statement = $sql->prepareStatementForSqlObject($delete);
								$delContactInfo = $statement->execute();
							}

							// Fetch contact page
							// Get contact page details
							$query = "SELECT pc.`id` AS contactId, pc.`contact_description`, pc.`office_description`, pc.`work_description`, pc.`lab_description`
									FROM `pf_contact` AS pc
									WHERE pc.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$contactPage = $pageArray[0];

							// Get contacts
							$query = "SELECT pc.`id`, pc.`type`, pc.`title`, pc.`value`
									FROM `pf_contact_detail` AS pc
									WHERE pc.`contactId`=?";
							$contactDetails = $adapter->query($query, array($contactPage['contactId']));
							$contactInfo = $contactDetails->toArray();
							$contactPage['contactInfo'] = $contactInfo;

							$result->contact = $contactPage;
							$result->title	 = $pageTitle;
						} else if ($pageType == '8') {
							$updatePage['content'] = $data->pageContent;
						}
						$update->set($updatePage);
						$update->where($where);
						$statement = $sql->prepareStatementForSqlObject($update);
						$count = $statement->execute()->getAffectedRows();
						$result->status = 1;
						$result->message = 'Portfolio page updated';
					}
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
				} else {
					$db->rollBack();
					$result->status = 0;
					$result->message = 'Invalid access';
				}
			}
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function getPortfolioDetail($data) {
        try {
			$adapter = $this->adapter;
			$portfolioMenus = $this->getPortfolioMenus($data);
			$portfolioMenus = $portfolioMenus->portfolioMenus;
			$result->portfolioPages = array();
			$i=0;
			foreach ($portfolioMenus as $key => $value) {
				if($value['status'] == 'active') {
					$result->portfolioPages[$i] = $value;
					if($value['type'] == 'page') {
						$portfolioId = $value['portfolioId'];
						switch ($value['pageType']) {
							case 'courses':
								// Get courses page details
								$sql = "SELECT pf.`id` AS courseId, pf.`courses`
										FROM `pf_courses` AS pf
										WHERE pf.`portfolioId`=?";
								$pageDetail = $adapter->query($sql, array($portfolioId));
								$pageArray = $pageDetail->toArray();
								$courseIds = $pageArray[0];
								$courseIds = $courseIds['courses'];
								$query = "SELECT c.id,c.name,c.image,c.slug,c.studentPrice,c.studentPriceINR, COUNT(DISTINCT user_id) as studentCount FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id=c.id WHERE c.id IN ($courseIds) AND deleted=0 AND approved=1 AND availStudentMarket=1 AND liveForStudent=1 GROUP BY c.id order by liveDate Desc LIMIT 28";
								$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$courses = $courses->toArray();
								$req1 = new stdClass();
								require_once 'Student.php';
								$s = new Student();
								require_once 'Course.php';
								$c = new Course();
								foreach ($courses as $key => $course) {
									if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
										require_once 'amazonRead.php';
										$courses[$key]['image'] = getSignedURL($course['image']);
									}
									$courses[$key]['rating'] 	= $s->getCourseRatings($course['id']);
									$req1->courseId 			= $course['id'];
									$courses[$key]['discount']	= $c->getCourseDiscount($req1);
									$courses[$key]['owner'] 	= $c->getCourseOwner($course['id']);
								}
								$result->portfolioPages[$i]['courses'] = $courses;
								break;
							case 'about':
								// Get about page details
								$sql = "SELECT pa.`id` AS aboutId, pa.`description`, pa.`academic_title`, pa.`education_title`, pa.`honors_title`
										FROM `pf_about` AS pa
										WHERE pa.`portfolioId`=?";
								$pageDetail = $adapter->query($sql, array($portfolioId));
								$pageArray = $pageDetail->toArray();
								$aboutPage = $pageArray[0];

								// Get academic Positions
								$sql = "SELECT ap.`id`, ap.`fromyear`, ap.`toyear`, ap.`position`, ap.`university`, ap.`department`
										FROM `pf_academic_positions` AS ap
										WHERE ap.`aboutId`=?";
								$academicDetails = $adapter->query($sql, array($aboutPage['aboutId']));
								$academic = $academicDetails->toArray();
								$aboutPage['academic'] = $academic;

								// Get education & training
								$sql = "SELECT pe.`id`, pe.`degree`, pe.`year`, pe.`title`, pe.`university`
										FROM `pf_education` AS pe
										WHERE pe.`aboutId`=?";
								$educationDetails = $adapter->query($sql, array($aboutPage['aboutId']));
								$education = $educationDetails->toArray();
								$aboutPage['education'] = $education;

								// Get honors details
								$sql = "SELECT ph.`id`, ph.`img` AS image, ph.`year`, ph.`title`, ph.`description`
										FROM `pf_honors` AS ph
										WHERE ph.`aboutId`=?";
								$honorsDetails = $adapter->query($sql, array($aboutPage['aboutId']));
								$honors = $honorsDetails->toArray();
								$aboutPage['honors'] = $honors;

								$result->portfolioPages[$i]['about'] = $aboutPage;
								break;
							case 'publications':
								// Get publications page details
								$sql = "SELECT pb.`id` AS publicationId, pb.`description`
										FROM `pf_publications` AS pb
										WHERE pb.`portfolioId`=?";
								$pageDetail = $adapter->query($sql, array($portfolioId));
								$pageArray = $pageDetail->toArray();
								$publicationsPage = $pageArray[0];

								// Get publications members
								$sql = "SELECT pd.`id`, pd.`publicationType`, pd.`name`, pd.`description`, pd.`year`, pd.`isbn`
										FROM `pf_publication_detail` AS pd
										WHERE pd.`publicationId`=?";
								$publicationsDetails = $adapter->query($sql, array($publicationsPage['publicationId']));
								$publications = $publicationsDetails->toArray();
								$publicationsPage['publications'] = $publications;
								$publicationsPage['pbTypes']	  = $this->pbTypes;

								$result->portfolioPages[$i]['publications'] = $publicationsPage;
								break;
							case 'teaching':
								// Get teaching page details
								$sql = "SELECT pt.`id` AS teachingId, pt.`description`
										FROM `pf_teaching` AS pt
										WHERE pt.`portfolioId`=?";
								$pageDetail = $adapter->query($sql, array($portfolioId));
								$pageArray = $pageDetail->toArray();
								$teachingPage = $pageArray[0];

								// Get teaching members
								$sql = "SELECT pd.`id`, pd.`title`, pd.`description`, pd.`fromyear`, pd.`toyear`
										FROM `pf_teaching_detail` AS pd
										WHERE pd.`teachingId`=?";
								$teachingDetails = $adapter->query($sql, array($teachingPage['teachingId']));
								$teaching = $teachingDetails->toArray();
								$noOfCurrent = $noOfPast = 0;
								foreach ($teaching as $key1 => $ap) {
									if($ap['toyear'] == '0000') {
										$noOfCurrent++;
									} else {
										$noOfPast++;
									}
								}
								$teachingPage['noOfCurrent'] = $noOfCurrent;
								$teachingPage['noOfPast'] = $noOfPast;
								$teachingPage['teaching'] = $teaching;

								$result->portfolioPages[$i]['teaching'] = $teachingPage;
								break;
							case 'gallery':
								// Get faculty page details
								$sql = "SELECT pg.`id` AS galleryId, pg.`description`
										FROM `pf_gallery` AS pg
										WHERE pg.`portfolioId`=?";
								$pageDetail = $adapter->query($sql, array($portfolioId));
								$pageArray = $pageDetail->toArray();
								$galleryPage = $pageArray[0];

								// Get faculty members
								$sql = "SELECT pg.`id`, pg.`img` AS image, pg.`title`, pg.`description`
										FROM `pf_gallery_detail` AS pg
										WHERE pg.`galleryId`=?";
								$imagesDetails = $adapter->query($sql, array($galleryPage['galleryId']));
								$galleryImages = $imagesDetails->toArray();
								$galleryPage['galleryImages'] = $galleryImages;

								$result->portfolioPages[$i]['gallery'] = $galleryPage;
								break;
							case 'contact':
								// Get faculty page details
								$sql = "SELECT pc.`id` AS contactId, pc.`contact_description`, pc.`office_description`, pc.`work_description`
										FROM `pf_contact` AS pc
										WHERE pc.`portfolioId`=?";
								$pageDetail = $adapter->query($sql, array($portfolioId));
								$pageArray = $pageDetail->toArray();
								$contactPage = $pageArray[0];

								// Get contacts
								$sql = "SELECT pc.`id`, pc.`type`, pc.`title`, pc.`value`
										FROM `pf_contact_detail` AS pc
										WHERE pc.`contactId`=?";
								$contactDetails = $adapter->query($sql, array($contactPage['contactId']));
								$contactInfo = $contactDetails->toArray();
								$contactPage['contactInfo'] = $contactInfo;

								$result->portfolioPages[$i]['contact'] = $contactPage;
								break;
						}
					}
					$i++;
				}
			}
			return $result;
        } catch (Exception $e) {
            $result = new stdClass();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
	}
	public function getProfessorSlugs() {
		try {

			$adapter = $this->adapter;
			$selectString ="SELECT ld.`id`,ur.`roleId`
							FROM  `login_details` AS ld
							LEFT JOIN `user_roles` AS ur ON ur.userId=ld.id
							WHERE ur.roleId='1' OR ur.roleId='2'";
			$professors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$professors = $professors->toArray();
			foreach ($professors as $key => $value) {
				if ($value['roleId'] == '1') {
					echo 'http://www.integro.io/institute/'.$value['id'].'<br>';
				}
			}
			foreach ($professors as $key => $value) {
				if ($value['roleId'] == '2') {
					echo 'http://www.integro.io/instructor/'.$value['id'].'<br>';
				}
			}
			

		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function getProfessorForMeta($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('pf' => 'professor_details'))
					->join(array('ld' => 'login_details'),
							'pf.userId = ld.id')
					->where(array('ld.id' => $data->slug))
					->columns(array('firstName', 'lastName', 'description', 'profilePic'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$details = $details->toArray();
			if(count($details) > 0) {
				$result->details = $details[0];
				$ogImage = $result->details['profilePic'];
				$ogPath = explode("/", $ogImage);
				$result->details['ogimage'] = $this->ogPath.end($ogPath);
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$result->details['image'] = getSignedURL($result->details['profilePic']);
				}
				$result->status = 1;
				return $result;
			}
			$result->status = 0;
			$result->message = 'No such course found';
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function getAllProfessors($data) {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$result = new stdClass();

		$select = $sql->select();
		$select->from(array('p' => 'professor_details'))
		->join(array('u' =>  'user_details'), 'u.userId = p.userId', array('contactMobilePrefix', 'contactMobile'))
		->join(array('l' => 'login_details'), 'l.id     = p.userId', array('email', 'id'))
		->join(array('c' => 'countries'    ), 'c.id = u.addressCountry', array('addressCountry' => 'name'));
		$select->columns(array(
			'profId' => 'userId',
			'firstName',
			'lastName',
			'gender',
			'profilePic',
			'description'
		));
		$selectString = $sql->getSqlStringForSqlObject($select);
		//echo $selectString;
		$professors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		if($professors->count() !== 0) {
			$result->userDetails = $professors->toArray();
			foreach($result->userDetails as $key => $detail) {
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR'] <> '::1'){
					require_once 'amazonRead.php';
					$result->userDetails[$key]['profilePic'] = getSignedURL($detail['profilePic']);
				}
			}
		} else {
			$result->status = 0;
			$result->exception = "Error";
			return $result;
		}
		//$result->userId = $data->userId;
		$result->status = 1;
		return $result;
	}

	public function getInstructorNotifications($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			//for main course student graph
			$selectString = "SELECT COUNT(*) AS notiCount
							FROM notifications
							WHERE userId={$data->userId} AND `status`=0";
			$totalnotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$totalnotifications = $totalnotifications->toArray();
			$result->notiCount	= (int)$totalnotifications[0]['notiCount'];

			if ($result->notiCount<20) {
				//for main course student graph
				$selectString ="SELECT `id`, `message`, `status`, `timestamp`
								FROM notifications
								WHERE userId={$data->userId}
								ORDER BY timestamp DESC
								LIMIT 20";
			} else {
				//for main course student graph
				$selectString ="SELECT `id`, `message`, `status`, `timestamp`
								FROM notifications
								WHERE userId={$data->userId} AND `status`=0
								ORDER BY timestamp DESC";
			}
			$sitenotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->sitenotifications = $sitenotifications->toArray();


			$selectString ="SELECT sp.professorId
							FROM `subject_professors` AS sp
							INNER JOIN `subjects` AS s ON s.id=sp.subjectId AND s.deleted=0
							INNER JOIN `courses` AS c ON c.id=s.courseId AND c.deleted=0
							INNER JOIN `login_details` AS ld ON ld.id=c.ownerId AND ld.active=1
							WHERE ld.id={$data->userId}";
			$professors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$professors = $professors->toArray();
			$chatnotifications = array();
			$siteProfessors = array();
			$chatCount = 0;
			if (count($professors)) {
				foreach ($professors as $professor) {
					$siteProfessors[] = $professor["professorId"];
				}
				$siteProfessors = implode(",", $siteProfessors);
				$selectString ="SELECT COUNT(*) AS notiCount
								FROM `chat_notifications` AS cn
								INNER JOIN `chat_rooms` AS cr ON cr.id=cn.roomId
								INNER JOIN `subjects` AS s ON s.id=cr.subjectId
								INNER JOIN `courses` AS c ON c.id=cr.courseId
								INNER JOIN `login_details` AS ld ON ld.id=cn.senderId
								INNER JOIN `professor_details` AS pd ON pd.userId=cn.userId
								INNER JOIN `user_roles` AS ur on ur.userId=cn.senderId
								WHERE ((cn.userId IN ($siteProfessors) AND c.ownerId={$data->userId}) OR (cn.userId=$data->userId)) AND cn.status=0";
				$totalnotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$totalnotifications = $totalnotifications->toArray();
				$chatCount	= (int)$totalnotifications[0]['notiCount'];

				if ($chatCount<20) {
					$selectString ="SELECT cn.*, c.name AS courseName, s.name AS subjectName, cr.room_type,
									CASE
										when ur.roleId= 1 then (select name from institute_details where userId=cn.senderId)
										when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId=cn.senderId)
										when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId=cn.senderId)
										when ur.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId=cn.senderId)
									END as senderName, pd.firstName, pd.lastName, pd.profilePic, ur.roleId 
									FROM `chat_notifications` AS cn
									INNER JOIN `chat_rooms` AS cr ON cr.id=cn.roomId
									INNER JOIN `subjects` AS s ON s.id=cr.subjectId
									INNER JOIN `courses` AS c ON c.id=cr.courseId
									INNER JOIN `login_details` AS ld ON ld.id=cn.senderId
									INNER JOIN `professor_details` AS pd ON pd.userId=cn.userId
									INNER JOIN `user_roles` AS ur on ur.userId=cn.senderId
									WHERE ((cn.userId IN ($siteProfessors) AND c.ownerId={$data->userId}) OR (cn.userId=$data->userId))
									ORDER BY cn.timestamp DESC
									LIMIT 20";
				} else {
					$selectString ="SELECT cn.*, c.name AS courseName, s.name AS subjectName, cr.room_type,
									CASE
										when ur.roleId= 1 then (select name from institute_details where userId=cn.senderId)
										when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId=cn.senderId)
										when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId=cn.senderId)
										when ur.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId=cn.senderId)
									END as senderName, pd.firstName, pd.lastName, pd.profilePic, ur.roleId 
									FROM `chat_notifications` AS cn
									INNER JOIN `chat_rooms` AS cr ON cr.id=cn.roomId
									INNER JOIN `subjects` AS s ON s.id=cr.subjectId
									INNER JOIN `courses` AS c ON c.id=cr.courseId
									INNER JOIN `login_details` AS ld ON ld.id=cn.senderId
									INNER JOIN `professor_details` AS pd ON pd.userId=cn.userId
									INNER JOIN `user_roles` AS ur on ur.userId=cn.senderId
									WHERE ((cn.userId IN ($siteProfessors) AND c.ownerId={$data->userId}) OR (cn.userId=$data->userId))
									ORDER BY cn.timestamp DESC";
				}
				$chatnotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$chatnotifications = $chatnotifications->toArray();
				foreach ($chatnotifications as $key => $notif) {
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
						require_once 'amazonRead.php';
						$chatnotifications[$key]["profilePic"] = getSignedURL($chatnotifications[$key]["profilePic"]);
					} else {
						$chatnotifications[$key]["profilePic"] = 'assets/pages/media/users/avatar1.jpg';
					}
				}
			}

			$result->chatCount = $chatCount;
			$result->chatnotifications = $chatnotifications;
			
			/*if($data->userRole == 2) {
				$selectString ="SELECT cn.*, c.name AS courseName, s.name AS subjectName, cr.room_type,
								CASE
									when ur.roleId= 1 then (select name from institute_details where userId=cn.senderId)
									when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId=cn.senderId)
									when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId=cn.senderId)
									when ur.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId=cn.senderId)
								END as senderName, ur.roleId 
								FROM `chat_notifications` AS cn
								INNER JOIN `chat_rooms` AS cr ON cr.id=cn.roomId
								INNER JOIN `subjects` AS s ON s.id=cr.subjectId
								INNER JOIN `courses` AS c ON c.id=cr.courseId
								INNER JOIN `login_details` AS ld ON ld.id=cn.senderId
								INNER JOIN `user_roles` AS ur on ur.userId=cn.senderId
								WHERE cn.userId={$data->userId} AND status=0";
				$chatnotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$chatnotifications = $chatnotifications->toArray();

				$result->chatnotifications = $chatnotifications;
			}*/
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	public function getInstructorNotificationsActivity($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			//for main course student graph
			$selectString = "SELECT COUNT(*) AS notiCount FROM notifications  WHERE userId={$data->userId} order by timestamp desc";
			$totalnotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$totalnotifications = $totalnotifications->toArray();
			$result->notiCount	= (int)$totalnotifications[0]['notiCount'];

			$pageStart = ($data->pageNumber)*($data->pageLimit);
			//for main course student graph
			$selectString = "SELECT `id`, `message`, `status`, `timestamp` FROM notifications  WHERE userId={$data->userId} order by timestamp desc LIMIT {$pageStart},{$data->pageLimit}";
			$sitenotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->sitenotifications = $sitenotifications->toArray();
			$result->status = 1;

			$where = array('userId' => $data->userId);
			$update = $sql->update();
			$update->table('notifications');
			$updateNotifications = array('status' => 1);
			$update->set($updateNotifications);
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();

			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getInstructorNotificationsChat($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			/*$sql = new Sql($adapter);
			$selectString ="SELECT sp.professorId
							FROM `subject_professors` AS sp
							INNER JOIN `subjects` AS s ON s.id=sp.subjectId AND s.deleted=0
							INNER JOIN `courses` AS c ON c.id=s.courseId AND c.deleted=0
							INNER JOIN `login_details` AS ld ON ld.id=c.ownerId AND ld.active=1
							WHERE ld.id={$data->userId}";
			$professors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$professors = $professors->toArray();
			$chatnotifications = array();
			$siteProfessors = array();
			if (count($professors)) {*/
				/*foreach ($professors as $professor) {
					$siteProfessors[] = $professor["professorId"];
				}
				$siteProfessors = implode(",", $siteProfessors);*/
				$selectString ="SELECT cn.*, c.name AS courseName, s.name AS subjectName, cr.room_type,
								CASE
									when ur.roleId= 1 then (select name from institute_details where userId=cn.senderId)
									when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname) from professor_details where userId=cn.senderId)
									when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname) from publisher_details where userId=cn.senderId)
									when ur.roleId= 4 then (select CONCAT(firstname, ' ', lastname) from student_details where userId=cn.senderId)
								END as senderName, pd.firstName, pd.lastName, ur.roleId 
								FROM `chat_notifications` AS cn
								INNER JOIN `chat_rooms` AS cr ON cr.id=cn.roomId
								INNER JOIN `subjects` AS s ON s.id=cr.subjectId
								INNER JOIN `courses` AS c ON c.id=cr.courseId
								INNER JOIN `login_details` AS ld ON ld.id=cn.senderId
								INNER JOIN `professor_details` AS pd ON pd.userId=cn.userId
								INNER JOIN `user_roles` AS ur on ur.userId=cn.senderId
								LEFT JOIN `subject_professors` AS sp ON sp.professorId=cn.userId
								WHERE cn.userId = {$data->userId} AND (c.ownerId={$data->userId} || sp.professorId={$data->userId})";
				$totalnotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$totalnotifications = $totalnotifications->toArray();
				
				$pageStart = ($data->pageNumber)*($data->pageLimit);

				$selectString ="SELECT cn.*, c.name AS courseName, s.name AS subjectName, cr.room_type,
								CASE
									when ur.roleId= 1 then (select name from institute_details where userId=cn.senderId)
									when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId=cn.senderId)
									when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId=cn.senderId)
									when ur.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId=cn.senderId)
								END as senderName, pd.firstName, pd.lastName, pd.profilePic, ur.roleId 
								FROM `chat_notifications` AS cn
								INNER JOIN `chat_rooms` AS cr ON cr.id=cn.roomId
								INNER JOIN `subjects` AS s ON s.id=cr.subjectId
								INNER JOIN `courses` AS c ON c.id=cr.courseId
								INNER JOIN `login_details` AS ld ON ld.id=cn.senderId
								INNER JOIN `professor_details` AS pd ON pd.userId=cn.userId
								INNER JOIN `user_roles` AS ur on ur.userId=cn.senderId
								LEFT JOIN `subject_professors` AS sp ON sp.professorId=cn.userId
								WHERE cn.userId = {$data->userId} AND (c.ownerId={$data->userId} || sp.professorId={$data->userId})
								ORDER BY timestamp DESC
								LIMIT {$pageStart},{$data->pageLimit}";
				$chatnotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$chatnotifications = $chatnotifications->toArray();
				//$result->notiCount = count($totalnotifications);
				foreach ($chatnotifications as $key => $notif) {
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
						require_once 'amazonRead.php';
						$chatnotifications[$key]["profilePic"] = getSignedURL($chatnotifications[$key]["profilePic"]);
					} else {
						$chatnotifications[$key]["profilePic"] = 'assets/pages/media/users/avatar1.jpg';
					}
				}
				$result->notiCount = count($totalnotifications);
				$result->chatnotifications = $chatnotifications;
			/*} else {
				$result->notiCount = 0;
				$result->chatnotifications = array();
			}*/
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}


	public function savePortfolioImage($filePath, $subdomain, $userId) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$where = array('subdomain' => $subdomain,'userId' => $userId);
			$sql = new Sql($adapter);
			
			//deleting previous image from cloud
			$select = $sql->select();
			$select->from('pf_portfolio');
			$select->where($where);
			$select->columns(array('img'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$portfolioPic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$portfolioPic = $portfolioPic->toArray();
			$portfolioPic = $portfolioPic[0]['img'];
			$portfolioPic = substr($portfolioPic, 37);
			if($portfolioPic != '') {
				require_once 'amazonDelete.php';
				deleteFile($portfolioPic);
			}
			
			$update = $sql->update();
			$update->table('pf_portfolio');
			$update->set(array('img' => $filePath));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();

			$select = $sql->select();
			$select->from('pf_portfolio');
			$select->where($where);
			$select->columns(array('img'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$portfolioPic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$portfolioPic = $portfolioPic->toArray();
			$portfolioPic = $portfolioPic[0]['img'];
			if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
				require_once 'amazonRead.php';
				$portfolioPic = getSignedURL($portfolioPic);
			} else {
				$portfolioPic = 'assets/layouts/layout2/img/avatar3_small.jpg';
			}

			$result->status = 1;
			$result->message = 'Portfolio Image Uploaded!';
			$result->image = $portfolioPic;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
}
?>