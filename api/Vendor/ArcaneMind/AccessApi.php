<?php
	
	chdir(dirname(__DIR__));
	//echo dirname(__DIR__); die();
	$libPath = "";
	
	// Loading Zend
	include $libPath . 'Zend/Loader/AutoloaderFactory.php';
	Zend\Loader\AutoloaderFactory::factory(array(
			'Zend\Loader\StandardAutoloader' => array(
					'autoregister_zf' => true,
					'db' => 'Zend\Db\Sql'
			)
	));
	
	class AccessApi {
		public static function getRequestInfo() {
			$res = new stdClass();
			$filePath = $_SERVER['PHP_SELF'];
			$filePathTokens = explode('/', $filePath);
			$res->tokens = $filePathTokens;
			$res->fileName = end($filePathTokens);
			$res->admin = 0;
			if(count($filePathTokens) > 1 && ($filePathTokens[count($filePathTokens) - 2] == "admin") ) {
				$res->admin = 1;
			}
			//else if(count($filePathTokens) > 1 && ($filePathTokens[count($filePathTokens) - 2] == "professor")){
			//	$res->adminPage = 2;
			//}
			foreach($_GET as $k => $v) {
				$res->$k = $v;
			}
			return $res;
		}
		public static function getUserInfo() {
			@session_start();
			if(!isset($_SESSION['userId']))
				return false;
			$res = new stdClass();
			$res->userId = $_SESSION['userId'];
			$res->userRole = $_SESSION['userRole'];
			return $res;
		}
		public static function checkAccess() {
			$user = AccessApi::getUserInfo();
			$request = AccessApi::getRequestInfo();
			if(!isset($_SESSION['userRole'])) {
				$request->adminPage=0;
			}
			else {
				$request->adminPage=$_SESSION['userRole']; 
			}
			switch ($request->adminPage){
				case 1:
				case 2:
				case 3:
					return AccessApi::checkAccessAdmin($request, $user);
					break;
				case 5: return AccessApi::checkAccessSuperAdmin($request, $user);break;
				//case 2:return AccessApi::checkAccessProfessor($request, $user);break;
				//case 3:return AccessApi::checkAccessPublisher($request, $user);break;
				default: return AccessApi::checkAccessStudent($request, $user);break;
			}
			//          if($request->adminPage == 1)
			//				return AccessApi::checkAccessAdmin($request, $user);
			//			else
			//				return AccessApi::checkAccessGeneral($request, $user);
		}
        public static function checkAccessSuperAdmin($request, $user) {
			if( $user == false ) {
                                $res = new stdClass();
				$res->allow = false;
				$res->redirect = "../";
				if($res->allow === false) {
					header("Location:{$res->redirect}");
					die();
				}
			}
			$redirect = false;
                	$superRole = array(5);
                         $checkallowed=false;
			switch($request->fileName) {
				case 'backend_courses.php':
                    $checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $superRole))) {
						$redirect = '../';
                        break;
					}
                case 'backend_edit-course.php':
                    $checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $superRole))) {
						$redirect = '../';
                        break;
					}
                case 'backend_edit-subject.php':
                   $checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $superRole))) {
						$redirect = '../';
                        break;
					}
                    break;
                                        

				case "approvals.php":
				case 'course-key.php':
				case 'allApprovedCourses.php':
				case 'backendImportSubjects.php':   
				case 'all_institutes_info.php': 
				case 'portfolioAdmin.php':
				case 'allInstitutesLoginInfo.php': 
				case 'CourseCategory.php':   
				case 'deletedCourse.php':
				case 'deletedSubject.php':
				case 'deletedChapter.php':
				case 'deletedAssignment.php':
				case 'students.php':
				case 'edit-taxes.php':
				case 'discount.php': 
				case 'coupons.php':
				case 'couponsHistory.php':
				case 'admin_courseDiscounts.php';
				case 'createPackage.php':
				case 'createPackagePay.php':
				case 'view_editPackage.php':
				case 'admin_packages.php':
				case 'adminCreatePackage.php':
				case 'adminView_editPackage.php':
				case 'viewCourseAnalytics.php':
				case 'courseAnalyticsExcel.php':
				case 'assignSubs.php':
				case 'edit-subjectiveassignment.php':
				case 'checkSubjectiveExam.php':
				case 'checkSubjectiveExams.php':
				case 'freetier.php':
				case 'course-chat.php':
								
					$checkallowed=true;
                    if(!(AccessAPI::userHasRoles($user, $superRole))) {

						$redirect = '../';
						break;
					}
                    break;        
			}
			$res = new stdClass();
			$res->allow = $redirect === false ? true : false;
			$res->redirect = $redirect;
                         if( $checkallowed==false){
                            $res->allow=false;
                            $res->redirect='../';
                        }
                    	if($res->allow === false) {
				header("Location:{$res->redirect}");
				die();
			}
		}
		
		public static function checkAccessAdmin($request, $user) {
			if( $user == false ) {
				$res = new stdClass();
				$res->allow = false;
				$res->redirect = "../";
				if($res->allow === false) {
					header("Location:{$res->redirect}");
					die();
				}
			}
			$redirect = false;
			$adminRoles = array(1, 2, 3);
            $instituteRoles = array(1, 2, 3);
			$checkallowed=false;
			switch($request->fileName) {
				//case for free demo institute
				case 'chapterContent.php':
					$checkallowed=true;
					//the super condition for free chapters
					if(AccessAPI::isDemoFlag($request)) {
						$redirect = false;
						break;
					}
					if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
						$redirect = "profile.php";
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "MyCourse.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "MyExam.php?courseId=".$request->courseId;
						break;
					}
					if(!isset($request->chapterId)){
						$redirect = "chapter.php?courseId={$request->courseId}&subjectId=".$request->subjectId;
						break;
					}
					$redirect = AccessAPI::subjectIsFromCourse($request);
					if($redirect !== false)
						break;
					//checking if the corse is expired
					if(AccessAPI::courseExpired($request)) {
						$redirect = "MyCourse.php";
						break;
					}
					$redirect = AccessAPI::studentJoinedCourse($user, $request);
					if($redirect !== false)
						break;
					
					$redirect = AccessAPI::studentJoinedCourseChapter($user, $request);
					break;
				//needed to check more option on paymentDetails page like course is available on content market place and the license is applied on the course.
				case 'paymentDetails.php':
				case "dashboard.php":
				case 'invited_students_history.php':
				case 'studentsList.php':
				case 'studentsTransfer.php':
				case "profile.php":	
				case "portfolio.php":
				case "portfolioPages.php":
				case "portfolioMenus.php":
				case "CourseKey.php" :
				case "courses.php":
				case "assignment-exam.php":
				case "professors.php":
				case 'invite-professors.php':
				case 'students.php':    
				case 'chapter-content.php':
				case "add-course.php":
				case 'notification.php':
				case 'changePassword.php':
				case 'template.php':
				case 'chapterContent.php':
				case 'invitedProfessors.php':
				case 'add-template.php':
				case 'discount.php':
				case 'coupons.php':
				case 'couponsHistory.php':
				case 'packages_subs.php':
				case 'createPackage.php':
				case 'createPackagePay.php':
				case 'view_editPackage.php':
				case 'viewCourseAnalytics.php':
				case 'assignSubs.php':
				case 'edit-subjectiveassignment.php':
				case 'checkSubjectiveExam.php':
				case 'checkSubjectiveExams.php':
					$checkallowed = true;
					if($request->admin != 1) {
						$redirect = "../";
						break;
					}
					if(!(AccessAPI::userHasRoles( $user, $instituteRoles))) {
						$redirect = "profile.php";
						break;
					}
					break;
				case 'subject-invitation.php':
					$checkallowed = true;
					if($request->admin != 1) {
						$redirect = "../";
						break;
					}
					if(!(AccessAPI::userHasRoles( $user, array(2)))) {
						$redirect = "profile.php";
						break;
					}
					break;
				case 'courseDetailsInstitute.php':
					$checkallowed = true;
					if(!isset($request->courseId)) {
						$redirect = "profile.php";
						break;
					}
					break;
				case 'rating.php':
				case "edit-course.php":
				case "add-subject.php":
				case 'subjectResult.php':
				case "export-students.php":
					$checkallowed = true;
					if(!(AccessAPI::userHasRoles( $user, $instituteRoles))) {
						$redirect = "profile.php";
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					$redirect = AccessAPI::userOwnsCourse($user, $request);
					break;
				case 'subjectRating.php':
				case "edit-subject.php":
				case "add-chapter.php":
				case "add-assignment.php":
				case "add-subjectiveassignment.php":
				case "subject-students.php":
					$checkallowed=true;  
					if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
						$redirect = "../";
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "edit-course.php?courseId={$request->courseId}";
						break;
					}
					//check subject belongs to the course and then only check owns subject
					$redirect = AccessAPI::subjectIsFromCourse($request);
					if($redirect !== false)
						break;
					//this function also checks association of subject with professor
					$redirect = AccessAPI::userOwnsSubject($user, $request);
					//$redirect = AccessAPI::userOwnsCourse($user, $request);
					break;
				//	
				case "content.php":
					$checkallowed=true;  
					if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
						$redirect = "../";
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "edit-course.php?courseId={$request->courseId}";
						break;
					}
					$rediect = AccessAPI::subjectIsFromCourse($request);
					if($redirect !== false)
						break;
					$redirect = AccessAPI::userOwnsSubject($user, $request);
					/*$redirect = AccessAPI::userOwnsChapter($user, $request);
					if($redirect !== false)
						break;
					$redirect = AccessAPI::userOwnsCourse($user, $request);*/
					break;
				case 'add-assignment-2.php':
				case 'examResult.php':
				case 'examAnalysis.php':
				case 'studentResult.php':
				case 'edit-assignment.php':
					$checkallowed=true; 
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
						break;
					}
					if(!isset($request->examId)) {
						$redirect = "assignment-exam.php";
						break;
					}
					$redirect = AccessAPI::userOwnsExam($user, $request);
					if($redirect !== false)
						break;
					break;
				case 'chapterPreview.php':
					//need to set courseId and subjectId in request url while redirecting
					$checkallowed=true;      
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
						break;
					}
					if(!isset($request->chapterId)) {
						$redirect = "chapter-content.php";
						break;
					}           
					// check own chapter
					break;
				case 'subject-assigned.php':
				case 'subject-invitation.php':
					$checkallowed = true;
					if(!(AccessAPI::userHasRoles($user, array(2)))) {
						$redirect = '../';
						break;
					}
					break;
				case "add-manualexam.php":
				case "import-manualexam.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "edit-course.php?courseId={$request->courseId}";
						break;
					}
					$redirect=AccessAPI::checkSubjectAssigned($user, $request);
					if($redirect !== false){
						break;
					}
					break;
				case "manualexam.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "edit-course.php?courseId={$request->courseId}";
						break;
					}
					if(!isset($request->examId)) {
						$redirect = "courses.php";
						break;
					}
					$redirect=AccessAPI::checkSubjectAssigned($user, $request);
					if($redirect !== false){
						break;
					}
					break;
				case "manualexam-student.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
						break;
					}
					/*if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "edit-course.php?courseId={$request->courseId}";
						break;
					}*/
					if(!isset($request->examId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->studentId)) {
						$redirect = "courses.php";
						break;
					}
					/*$redirect=AccessAPI::checkSubjectAssigned($user, $request);
					if($redirect !== false){
						break;
					}*/
					break;
			}
			$res = new stdClass();
			$res->allow = $redirect === false ? true : false;
			$res->redirect=$redirect;
			if( $checkallowed==false){
				$res->allow=false;
				$res->redirect='../';
			}
			if($res->allow === false) {
				header("Location:{$res->redirect}");
				die();
			}
		}
		public static function checkAccessProfessor($request, $user) {
			if( $user == false ) {
                                $res = new stdClass();
				$res->allow = false;
				$res->redirect = "../";
				if($res->allow === false) {
					header("Location:{$res->redirect}");
					die();
				}
			}
			$redirect = false;
			$adminRoles = array(2);
			$superRole = array(5);
            $checkallowed=false;
			switch($request->fileName) {
				case "profile.php":
				case "CourseKey.php" :
                    $checkallowed=true;
                    if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
						$redirect = "../";
						break;
					}
					break; 
                case 'subject-assigned.php': 
                case 'subject-invitation.php':
                case 'chapter-content.php': 
				case 'discount.php':  
				case 'coupons.php':
				case 'couponsHistory.php':
				case 'packages_subs.php':
				case 'createPackage.php':
				case 'createPackagePay.php':
				case 'view_editPackage.php':
				case 'viewCourseAnalytics.php':
				case 'assignSubs.php':
				case 'edit-subjectiveassignment.php':
				case 'checkSubjectiveExam.php':
				case 'checkSubjectiveExams.php':
								
                    $checkallowed=true;
					if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
						$redirect = "profile.php";
						break;
					}
					if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
						$redirect = "../";
						break;
					}
					
					break;
				case "add-course.php":
                                        $checkallowed=true;
					if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
						$redirect = "profile.php";
						break;
					}
					break;
				case "edit-course.php":
				case "export-students.php":
                                        $checkallowed=true;
					if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
						$redirect = "profile.php";
						break;
					}
					if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
						$redirect = "../";
						break;
					}
					
					if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					
					$redirect = AccessAPI::userOwnsCourse($user, $request);
					break;
				case "add-subject.php":
                                         $checkallowed=true;
					if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
						$redirect = "../";
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					$redirect = AccessAPI::userOwnsCourse($user, $request);
					break;
				
				case "edit-subject.php":
				case "subject-students.php":
                    $checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = "../";
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "edit-course.php?courseId={$request->courseId}";
						break;
					}
					$redirect = AccessAPI::userOwnsCourse($user, $request);
					if($redirect !== false)
						break;
					$redirect = AccessAPI::userOwnsSubject($user, $request);
					break;
                case "add-chapter.php":
                                        $checkallowed=true;
					if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
						$redirect = "../";
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "edit-course.php?courseId={$request->courseId}";
						break;
					}
					$redirect=AccessAPI::checkSubjectAssigned($user, $request);
					if($redirect !== false){
						break;
					}
					break;
                case "content.php":
                    $checkallowed=true;
					if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
						$redirect = "../";
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "edit-course.php?courseId={$request->courseId}";
						break;
					}
					if(!isset($request->chapterId)) {
						$redirect = "edit-subject.php?courseId={$request->courseId}&subjectId={$request->subjectId}";
						break;
					}
					//$redirect = AccessAPI::userOwnsCourse($user, $request);
					$redirect = AccessAPI::checkAssigenedSubjectContent($user, $request);
					if($redirect !== false)
						break;
					break;
				
				case "institute-professors.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
						$redirect = "profile.php";
						break;
					}
					break;
				case "approvals.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles( $user, $superRole))) {
						$redirect = "../";
						break;
					}
					break;
				case "assignment-exam.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
					}
					break;
				//
				case "add-assignment.php":
				case "add-subjectiveassignment.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
                                                break;
					}
					if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "edit-course.php?courseId={$request->courseId}";
						break;
					}
					$redirect=AccessAPI::checkSubjectAssigned($user, $request);
					if($redirect !== false){
						break;
					}
					break;
				/*case "add-subjectiveassignment.php":
                                      $checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
                                                break;
					}
					if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "edit-course.php?courseId={$request->courseId}";
						break;
					}
                    $redirect=AccessAPI::checkSubjectAssigned($user, $request);
                    if($redirect !== false){
                          break;
					}
					break;*/
				case 'add-assignment-2.php':
                                     $checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
                                                break;
					}
					if(!isset($request->examId)) {
						$redirect = "assignment-exam.php";
						break;
					}
					$redirect = AccessAPI::userOwnsExam($user, $request);
					if($redirect !== false)
						break;
					break;
				case 'edit-assignment.php':
                                        $checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
                                                break;
					}
					if(!isset($request->examId)) {
						$redirect = "assignment-exam.php";
						break;
					}
					$redirect = AccessAPI::userOwnsExam($user, $request);
					if($redirect !== false)
						break;
					break;
                case 'chapterPreview.php':
                    $checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
                        break;
					}
					if(!isset($request->chapterId)) {
						$redirect = "chapter-content.php";
						break;
					}
					$redirect = AccessAPI::userOwnsExam($user, $request);
					if($redirect !== false)
						break;
					break;
				case "add-manualexam.php":
				case "import-manualexam.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "edit-course.php?courseId={$request->courseId}";
						break;
					}
					$redirect=AccessAPI::checkSubjectAssigned($user, $request);
					if($redirect !== false){
						break;
					}
					break;
				case "manualexam.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "edit-course.php?courseId={$request->courseId}";
						break;
					}
					if(!isset($request->examId)) {
						$redirect = "courses.php";
						break;
					}
					$redirect=AccessAPI::checkSubjectAssigned($user, $request);
					if($redirect !== false){
						break;
					}
					break;
				case "manualexam-student.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
						break;
					}
					/*if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "edit-course.php?courseId={$request->courseId}";
						break;
					}*/
					if(!isset($request->examId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->studentId)) {
						$redirect = "courses.php";
						break;
					}
					$redirect=AccessAPI::checkSubjectAssigned($user, $request);
					if($redirect !== false){
						break;
					}
					break;
            }
            $res = new stdClass();
            $res->allow = $redirect === false ? true : false;
            $res->redirect=$redirect;
            if( $checkallowed==false){
                $res->allow=false;
                $res->redirect='../';
            }
            if($res->allow === false) {
				header("Location:{$res->redirect}");
				die();
			}
		}
		public static function checkAccessPublisher($request, $user) {
			if( $user == false ) {
                                $res = new stdClass();
				$res->allow = false;
				$res->redirect = "../";
				if($res->allow === false) {
					header("Location:{$res->redirect}");
					die();
				}
			}
			$redirect = false;
			$adminRoles = array(3);
			$checkallowed=false;
			switch($request->fileName) {
		        case "instituteDashboard.php":
		        case "profile.php":
	            case 'notification.php':
				case "courses.php":
	            case "assignment-exam.php":
	            case "institute-professors.php":
	            case 'students.php':    
	            case 'chapter-content.php':  
				case 'viewCourseAnalytics.php':  
				case "add-course.php":
	                    $checkallowed=true;  
						if(!(AccessAPI::userHasRoles( $user, $instituteRoles))) {
							$redirect = "profile.php";
							break;
						}
						break;
	                       case "edit-course.php":
	                                        $checkallowed=true;  
						if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
							$redirect = "profile.php";
							break;
						}
						if(!isset($request->courseId)) {
							$redirect = "courses.php";
							break;
						}
						$redirect = AccessAPI::userOwnsCourse($user, $request);
	                    break;
				case "add-subject.php":
	                    $checkallowed=true;  
						if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
							$redirect = "../";
							break;
						}
						if(!isset($request->courseId)) {
							$redirect = "courses.php";
							break;
						}
						$redirect = AccessAPI::userOwnsCourse($user, $request);
						break;
				case "edit-subject.php":
				case "subject-students.php":
	                    $checkallowed=true;  
						if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
							$redirect = "../";
							break;
						}
						if(!isset($request->courseId)) {
							$redirect = "courses.php";
							break;
						}
						if(!isset($request->subjectId)) {
							$redirect = "edit-course.php?courseId={$request->courseId}";
							break;
						}
						$redirect = AccessAPI::userOwnsCourse($user, $request);
						if($redirect !== false)
							break;
						$redirect = AccessAPI::userOwnsSubject($user, $request);
						break;
	            case "add-chapter.php":
	                                        $checkallowed=true;  
						if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
							$redirect = "../";
							break;
						}
						if(!isset($request->courseId)) {
							$redirect = "courses.php";
							break;
						}
						if(!isset($request->subjectId)) {
							$redirect = "edit-course.php?courseId={$request->courseId}";
							break;
						}
						$redirect = AccessAPI::userOwnsCourse($user, $request);
						if($redirect !== false)
							break;
	                                        
						$redirect = AccessAPI::userOwnsSubject($user, $request);
						 break;
	                        case "content.php":
	                                        $checkallowed=true;  
						if(!(AccessAPI::userHasRoles( $user, $adminRoles))) {
							$redirect = "../";
							break;
						}
						if(!isset($request->courseId)) {
							$redirect = "courses.php";
							break;
						}
						if(!isset($request->subjectId)) {
							$redirect = "edit-course.php?courseId={$request->courseId}";
							break;
						}
						if(!isset($request->chapterId)) {
							$redirect = "edit-subject.php?courseId={$request->courseId}&subjectId={$request->subjectId}";
							break;
						}
						$redirect = AccessAPI::userOwnsCourse($user, $request);
						if($redirect !== false)
							break;
						$redirect = AccessAPI::userOwnsSubject($user, $request);
						if($redirect !== false)
							break;
						$redirect = AccessAPI::userOwnsChapter($user, $request);
						if($redirect !== false)
							break;
						break;
					
				case "add-assignment.php":
				case "add-subjectiveassignment.php":
						$checkallowed=true; 
						if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
							$redirect = '../';
	                                                break;
						}
						if(!isset($request->courseId)) {
							$redirect = "courses.php";
							break;
						}
						if(!isset($request->subjectId)) {
							$redirect = "edit-course.php?courseId={$request->courseId}";
							break;
						}
						$redirect = AccessAPI::userOwnsCourse($user, $request);
						if($redirect !== false)
							break;
						$redirect = AccessAPI::userOwnsSubject($user, $request);
						if($redirect !== false)
							break;
						break;
				/*case "add-subjectiveassignment.php":
						$checkallowed=true; 
						if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
							$redirect = '../';
	                                                break;
						}
						if(!isset($request->courseId)) {
							$redirect = "courses.php";
							break;
						}
						if(!isset($request->subjectId)) {
							$redirect = "edit-course.php?courseId={$request->courseId}";
							break;
						}
						$redirect = AccessAPI::userOwnsCourse($user, $request);
						if($redirect !== false)
							break;
						$redirect = AccessAPI::userOwnsSubject($user, $request);
						if($redirect !== false)
							break;
						break;*/
				case 'add-assignment-2.php':
	                    $checkallowed=true; 
						if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
							$redirect = '../';
	                                                break;
						}
						if(!isset($request->examId)) {
							$redirect = "assignment-exam.php";
							break;
						}
						$redirect = AccessAPI::userOwnsExam($user, $request);
						if($redirect !== false)
							break;
						break;
				case 'edit-assignment.php':
	                    $checkallowed=true;      
						if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
							$redirect = '../';
	                                                break;
						}
						if(!isset($request->examId)) {
							$redirect = "assignment-exam.php";
							break;
						}
						$redirect = AccessAPI::userOwnsExam($user, $request);
						if($redirect !== false)
							break;
						break;
	            case 'chapterPreview.php':
	                    $checkallowed=true;      
						if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
							$redirect = '../';
	                        break;
						}
	                    if(!isset($request->chapterId)) {
	                        $redirect = "chapter-content.php";
							break;
	                    }           
	                                  // check own chapter      
						break;
				case "add-manualexam.php":
				case "import-manualexam.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "edit-course.php?courseId={$request->courseId}";
						break;
					}
					$redirect=AccessAPI::checkSubjectAssigned($user, $request);
					if($redirect !== false){
						break;
					}
					break;
				case "manualexam.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "edit-course.php?courseId={$request->courseId}";
						break;
					}
					if(!isset($request->examId)) {
						$redirect = "courses.php";
						break;
					}
					$redirect=AccessAPI::checkSubjectAssigned($user, $request);
					if($redirect !== false){
						break;
					}
					break;
				case "manualexam-student.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $adminRoles))) {
						$redirect = '../';
						break;
					}
					/*if(!isset($request->courseId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "edit-course.php?courseId={$request->courseId}";
						break;
					}*/
					if(!isset($request->examId)) {
						$redirect = "courses.php";
						break;
					}
					if(!isset($request->studentId)) {
						$redirect = "courses.php";
						break;
					}
					$redirect=AccessAPI::checkSubjectAssigned($user, $request);
					if($redirect !== false){
						break;
					}
					break;
            }
            $res = new stdClass();
            $res->allow = $redirect === false ? true : false;
            $res->redirect=$redirect;
            if( $checkallowed==false){
                $res->allow=false;
                $res->redirect='../';
            }
            if($res->allow === false) {
				header("Location:{$res->redirect}");
				die();
			}
		}
                
		public static function checkAccessStudent($request, $user) {
			/*if( $user == false ) {
				$res = new stdClass();
				$res->allow = false;
				$res->redirect = "../";
				if($res->allow === false) {
					header("Location:{$res->redirect}");
					die();
				}
			}*/
			$redirect = false;
			$studentRole = array(4);
			$checkallowed=false;
			switch($request->fileName) {
				case "profile.php":
				case "subscription.php":
				case "MyCourse.php":
				case "setting.php":
				case "changePassword.php":
				case "notification.php":
				case "examSuccess.php":
				case "professorProfile.php":
				case "studentProfile.php":
				case "studentResult.php":
				case "showSubjectiveResult.php":
				case "paymentDetails.php":
				case "subjectiveExamModule.php":
				case "studentManualExamResult.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $studentRole))) {
						$redirect = "../";
						break;
					}
					break; 
				case "instituteProfile.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles($user, $studentRole))) {
						$redirect = "../";
						break;
					}
					if(!isset($request->instituteId)) {
						$redirect = "MyCourse.php";
						break;
					}
					break;
				//  $redirect = AccessAPI::studentJoinedInstitute($user, $request);
				//  break;    
				case "MyExam.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles( $user, $studentRole))) {
						$redirect = "profile.php";
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "MyCourse.php";
						break;
					}
					//checking if the corse is expired
					if(AccessAPI::courseExpired($request)) {
						$redirect = "MyCourse.php";
						break;
					}
					//to check student has joined the course or not and check if it is still accessible under 1 year period.
					$redirect = AccessAPI::studentJoinedCourse($user, $request);
					
					break;
				case "chapter.php":
					$checkallowed=true;
					if(!(AccessAPI::userHasRoles( $user, $studentRole))) {
						$redirect = "profile.php";
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "MyCourse.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "MyExam.php?courseId=".$request->courseId;
						break;
					}
					//checking if the corse is expired
					if(AccessAPI::courseExpired($request)) {
						$redirect = "MyCourse.php";
						break;
					}
					$redirect = AccessAPI::studentJoinedCourse($user, $request);
						if($redirect !== false)
							break;
						$redirect = AccessAPI::studentJoinedCourseSubject($user, $request);
						break;
				case "chapterContent.php":
					$checkallowed=true;
					//the super condition for free chapters
					if(AccessAPI::isDemoFlag($request)) {
						$redirect = false;
						break;
					}
					if(!(AccessAPI::userHasRoles( $user, $studentRole))) {
						$redirect = "profile.php";
						break;
					}
					if(!isset($request->courseId)) {
						$redirect = "MyCourse.php";
						break;
					}
					if(!isset($request->subjectId)) {
						$redirect = "MyExam.php?courseId=".$request->courseId;
						break;
					}
					$redirect = AccessAPI::subjectIsFromCourse($request);
					if($redirect !== false)
						break;
					//checking if the corse is expired
					if(AccessAPI::courseExpired($request)) {
						$redirect = "MyCourse.php";
						break;
					}
					$redirect = AccessAPI::studentJoinedCourse($user, $request);
					//if($redirect !== false)
						//break;
					//$redirect = AccessAPI::studentJoinedCourseChapter($user, $request);
					break;
			}
			$res = new stdClass();
			$res->allow = $redirect === false ? true : false;
			$res->redirect=$redirect;
			if( $checkallowed==false){
				$res->allow=false;
				$res->redirect='../';
			}
			if($res->allow === false) {
				header("Location:{$res->redirect}");
				die();
			}
		}
	    
		public static function userHasRoles($user, $roles) {
            foreach($roles as $roleId) {
				if(AccessAPI::userHasRole($user, $roleId) == true)
					return true;
			}
			return false;
		}
		
		public static function userHasRole($user, $roleId) {
                   if($user->userRole == $roleId)
                          return true;
                   return false;
		}
		public static function userOwnsCourse($user, $request){
			require_once "Course.php";
			$c = new Course();
			$data = new stdClass();
			$data->userId = $user->userId;
			$data->courseId = $request->courseId;
                       if(!$c->userOwnsCourse($data)) {
                           	return "courses.php";
			}
			return false;
		}
		
		public static function subjectIsFromCourse($request) {
			require_once 'Subject.php';
			$s = new Subject();
			if(!$s->subjectIsFromCourse($request)) {
				return 'courses.php';
			}
			return false;
		}
		public static function chapterIsFromSubject($request) {
			require_once 'Chapter.php';
			$c = new Chapter();
			if(!$c->chapterIsFromSubject($request)) {
				return "edit-subject.php?courseId={$request->courseId}&subjectId={$request->subjectId}";
			}
			return false;
		}
		
		public static function userOwnsChapter($user, $request) {
			return false;
        }
		public static function userOwnsSubject($user, $request){
			require_once "Subject.php";
			$s = new Subject();
			$data = new stdClass();
			$data->userId = $user->userId;
			$data->courseId = $request->courseId;
			$data->subjectId = $request->subjectId;
			if(!$s->userOwnsSubject($data)) {
				return "courses.php";
			}
			return false;
		}
		public static function userOwnsExam($user, $request){
			require_once "Exam.php";
			$e = new Exam();
			$data = new stdClass();
			$data->userId = $user->userId;
			$data->examId = $request->examId;
			if(!$e->userOwnsExam($data)) {
				return "assignment-exam.php";
			}
			return false;
		}
		public static function checkSubjectAssigned($user, $request){
			require_once "Subject.php";
			$s = new Subject();
			$data = new stdClass();
                        $data->userId = $user->userId;
                        $data->courseId = $request->courseId;
			$data->subjectId = $request->subjectId;
			if(!($s->checkSubjectAssigned($data))) {
                          	return "courses.php";
			}
			return false;
		}
		public static function checkAssigenedSubjectContent($user, $request){
			require_once "Chapter.php";
			$c = new Chapter();
			$data = new stdClass();
                        $data->userId = $user->userId;
                        $data->courseId = $request->courseId;
			$data->subjectId = $request->subjectId;
                        $data->chapterId = $request->chapterId;
			if(!$c->checkAssigenedSubjectContent($data)) {
                            
				return "courses.php";
			}
			return false;
		}
		public static function checkAssigenedExam($user, $request){
			require_once "Exam.php";
			$e = new Exam();
			$data = new stdClass();
			//	$data->userId = $user->userId;
			//  $data->courseId = $request->courseId;
			//	$data->subjectId = $request->subjectId;
            $data->userId = $user->userId;
            $data->examId = $request->examId;
			if(!$e->checkAssigenedExam($data)) {
                            
				return "assignment-exam.php";
			}
			return false;
		}
		public static function studentJoinedCourse($user, $request){
			require_once "Student.php";
			$s = new Student();
			$data = new stdClass();
			$data->userId = $user->userId;
			$data->courseId = $request->courseId;
			if(!$s->studentJoinedCourse($data)) {
				return "MyCourse.php";
			}
			return false;
		}
		public static function studentJoinedCourseSubject($user, $request){
			require_once "Student.php";
			$s = new Student();
			$data = new stdClass();
			$data->userId = $user->userId;
			$data->courseId = $request->courseId;
			$data->subjectId = $request->subjectId;
			if(!$s->studentJoinedCourseSubject($data)) {
				return "MyExam.php?courseId=".$data->courseId;
			}
			return false;
		}
		public static function studentJoinedCourseChapter($user, $request){
			require_once "Student.php";
			$s = new Student();
			$data = new stdClass();
			$data->userId = $user->userId;
			$data->courseId = $request->courseId;
                        $data->subjectId = $request->subjectId;
                        $data->chapterId = $request->chapterId;
                       if(!$s->studentJoinedCourseChapter($data)) {
				return "chapter.php?courseId=$data->courseId&subjectId=$data->subjectId";
			}
			return false;
		}
              
		public static function studentJoinedInstitute($user, $request){
			require_once "Student.php";
			$s = new Student();
			$data = new stdClass();
			$data->userId = $user->userId;
			$data->instituteId = $request->instituteId;
			if(!$s->studentJoinedInstitute($data)) {
				return "MyCourse.php";
			}
			return false;
		}
		
		public static function isFreeChapter($request) {
			require_once 'Chapter.php';
			$c = new Chapter();
			return $c->isFreeChapter($request);
		}

		public static function isDemoFlag($request) {
			if(isset($request->demo) && $request->demo == 1)
				return true;
			else
				return false;
		}
		
		public static function courseExpired($request) {
			require_once 'Course.php';
			$c = new Course();
			$expire = $c->getCourseExpiry($request);
			if($expire == '')
				return false;
			else if($expire < time() * 1000)
				return true;
			else
				return false;
		}
//               public static function checkInstituteCourseDetail($user, $request){
//                   session_start();
//                    $instituteRoles = array(1);
//                   if(isset($_SESSION['userId'])){
//                     if(!(AccessAPI::userHasRoles($user, $instituteRoles))) {
//				$redirect = "index.php";
//				return $redirect;		
//                        }else{
//                           return true;  
//                        }
//                   }else{
//                       return true;
//                   }
//               }
   }
?>