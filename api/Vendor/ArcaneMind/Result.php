<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Select;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	
	require_once "Base.php";
	
	class Result extends Base {
	
		//function get result of particular attempt of particular exam
		public function getResult($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				//getting attempt details
				$select = $sql->select();
				$select->from('exam_attempts')->limit(1)->offset($data->attemptNo)->order('id DESC');
				$where;
				if($data->userRole != 4)
					$where = array(
								'studentId'	=>	$data->studentId,
								'examId'	=>	$data->examId,
								'completed'	=>	1
					);
				else
					$where = array(
								'studentId'	=>	$data->userId,
								'examId'	=>	$data->examId,
								'completed'	=>	1
					);
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$res = $res->toArray();
				if(count($res) <= 0) {
					$result->status = 0;
					$result->message = 'No such attempt found';
					return $result;
				}
				$result->result = $res[0];
				//selecting total attempts for attempt number
				$select = $sql->select();
				$select->from('exam_attempts');
				$select->columns(array(
							'totalAttempts'	=>	new \Zend\Db\Sql\Expression("COUNT(*)")
				));
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$totalAttempts = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$totalAttempts = $totalAttempts->toArray();
				$attemptNo = $totalAttempts[0]['totalAttempts'] - $data->attemptNo;
				$result->attemptNo = $attemptNo;
				$result->totalAttempts = $totalAttempts[0]['totalAttempts'];
				if($result->totalAttempts > 1) {
					$result->graph = $this->getAttemptGraph($data);
				}
				//finding students that are not enrolled in that exam.
				
				$adapter = $this->adapter;
				$select = "SELECT e.subjectId,s.courseId FROM exams e left join subjects s ON s.id=e.subjectId  WHERE e.id = '{$data->examId}'  ";
				
				$examDetails = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$examDetails = $examDetails->toArray();
				$subjectId=$examDetails[0]['subjectId'];
				$courseId=$examDetails[0]['courseId'];
				
				//print_r($examDetails);
				$adapter = $this->adapter;
				$select = "SELECT `subjectId`,`userId` ,`Active` FROM `student_subject` WHERE `courseId` = $courseId";
				$studentDetails = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$studentDetails = $studentDetails->toArray();
				//print_r($studentDetails);
				$studentIdArray=array();
				$studentIdcheck=array();
				//$studentIdcheck= array_fill ( 0 , count($studentDetails)-1 ,0);
				
				foreach($studentDetails as $key => $value){
					
					if(in_array($value['userId'], $studentIdArray))
					{
						$index=array_search($value['userId'], $studentIdArray); 
					}else{
						//$studentIdArray.push
						array_push($studentIdArray,$value['userId']);
						array_push($studentIdcheck,0);
						$index=count($studentIdArray)-1;
						
					}
					if($value['subjectId'] == $subjectId && $value['Active']==0)
					{
						$studentIdcheck[$index]=1;
					}elseif($value['subjectId'] == $subjectId && $value['Active']==1)
					{
						$studentIdcheck[$index]=0;
					}
					elseif($value['subjectId']!= $subjectId && $value['Active']==1)
					{
						$studentIdcheck[$index]=1;
					}
					else{
						$studentIdcheck[$index]=0;
					}
				}

				foreach($studentDetails as $key => $value){
					
					
					$index=array_search($value['userId'], $studentIdArray); 
					
					if($value['subjectId'] == $subjectId && $value['Active']==0)
					{
						$studentIdcheck[$index]=1;
					}elseif($value['subjectId'] == $subjectId && $value['Active']==1)
					{
						$studentIdcheck[$index]=0;
					}
					else{
						//$studentIdcheck[$index]=0;
					}
				}
				$studentNotEnrolled=array();
				foreach($studentIdcheck as $key => $value){
				
					if($value==1)
					{
						array_push($studentNotEnrolled,$studentIdArray[$key]);
					}
				}
				
				$studentArray=array();
				//$studentNotEnrolled=array_merge($studentNotEnrolled,$studentDetailNot);
				$select = $sql->select();
				$select->from('exam_attempts')
				->group('studentId')
				->order('Mscore DESC');
				$select->where(array('examId'	=>	$data->examId));
						//->where->notIn('studentId', $studentNotEnrolled);
				$studentNotEnrolled = implode(",", $studentNotEnrolled);
				$select->where->addPredicate(new Zend\Db\Sql\Predicate\Expression('studentId NOT IN (?)',$studentNotEnrolled));
				//$select->where->addPredicate(new Zend\Db\Sql\Predicate\Expression('studentId NOT IN (?)',$studentNotEnrolled));
				$select->columns(array(
						'studentId'	=>	'studentId',
						'Mscore'	=>	new \Zend\Db\Sql\Expression("MAX(score)")
				));
				$selectString = $sql->getSqlStringForSqlObject($select);

				$tempRank = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$tempRank = $tempRank->toArray();
				
				//selecting result details
				
				$totalRank = count($tempRank);
				$rank = 1;
				$count = 1;
				$flag = false;
				if($data->userRole != 4) {
					$studentId = $data->studentId;
				} else {
					$studentId = $data->userId;
				}
				
				for($i = 0; $i < $totalRank - 1; $i++) {
					if($tempRank[$i]['studentId'] == $studentId) {
						$flag = true;
						break;
					}
					else {
						if($tempRank[$i]['Mscore'] == $tempRank[$i+1]['Mscore']) {
							$count++;
						}
						else {
							$rank += $count;
							$count = 1;
						}
					}
				}

				$result->rank = $rank;
				$result->totalRank = $totalRank;
				//getting section details for making pie charts
				$select = $sql->select();
				$select->from(array('a'	=>	'attempt_questions'))
				->join(
					array('s'	=>	'exam_sections'),
					's.id=a.sectionId',
					array('name'	=>	'name')
				)
				->group('a.sectionId')->order('a.order');
				$select->columns(array(
							'sectionId'	=>	'sectionId',
							'questions'	=>	new \Zend\Db\Sql\Expression("COUNT(*)"),
							'time'		=>	new \Zend\Db\Sql\Expression("SUM(a.time)")
				));
				$select->where(array(
							'a.attemptId'	=>	$result->result['id']
				));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$sections = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$sections = $sections->toArray();
				foreach($sections as $key=>$section) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('attempt_questions')
					->group('check');
					$select->columns(array(
								'number'	=>	new \Zend\Db\Sql\Expression("COUNT(*)"),
								'check'		=>	'check'
					));
					$select->where(array(
							'attemptId'	=>	$result->result['id'],
							'sectionId'	=>	$section['sectionId']
					));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$checks = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$checks = $checks->toArray();
					$sections[$key]['checks'] = $checks;
					$select = $sql->select();
					$select->from('section_categories');
					$select->where(array(
									'sectionId'	=>	$section['sectionId'],
									'delete'	=>	0,
									'status'	=>	1
					));
					$select->columns(array('sectionId', 'correct', 'wrong', 'questionType', 'required'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$categories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$categories = $categories->toArray();
					$sections[$key]['categories'] = $categories;
				}
				$result->sections = $sections;

				require_once('Exam.php');
				$dataRange = new stdClass();
				$dataRange->examId = $data->examId;
				$dataRange->examType = 'exam';
				$e = new Exam();
				$ranges=$e->getPercentRanges($dataRange);
				if ($ranges->status == 1) {
					$ranges = $ranges->score;
				} else {
					$ranges = array();
				}
				$result->ranges = $ranges;
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		//function to fetch questions and answers of each section for particular role and exam
		public function getSectionQuestions($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('a'	=>	'attempt_questions'))
				->join(
					array(
						'q'	=>	'questions'
					),
					'q.id = a.questionId',
					array(
						'question'		=>	'question',
						'description'	=>	'description'
					)
				)->order('a.order');
				$select->where(array(
							'attemptId'	=>	$data->attemptId,
							'sectionId'	=>	$data->sectionId
				));
				//$select->limit(12);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$questions = $questions->toArray();
				$parentId = 0;
				$parent = '';
				foreach($questions as $key=>$question) {
					if($question['parentId'] != 0) {
						if($question['parentId'] != $parentId) {
							$select = $sql->select();
							$select->from('questions');
							$select->where(array('id'	=>	$question['parentId']));
							$select->columns(array('parent'	=>	'question'));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$tparent = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$tparent = $tparent->toArray();
							if(count($tparent) > 0) {
								$parent = $tparent[0]['parent'];
								$questions[$key]['parent'] = $parent;
							}
							$parentId = $question['parentId'];
						}
						else {
							$questions[$key]['parent'] = $parent;
						}
					}
					if($question['questionType'] != 5) {
						$temp = $this->getAnswerDetails($question, $data);
						$questions[$key]['userAnswer'] = $temp->userAnswer;
						$questions[$key]['origAnswer'] = $temp->origAnswer;
					}
					else {
						$select = $sql->select();
						$select->from('questions');
						$select->where(array(
									'parentId'	=>	$question['questionId'],
									'delete'	=>	0
						));
						$select->columns(array(
									'questionId'	=>	'id',
									'questionType'	=>	'questionType'
						));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$childs = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$childs = $childs->toArray();
						foreach($childs as $key1=>$child) {
							$select = $sql->select();
							$select->from('questions');
							$select->where(array(
										'delete'	=>	0,
										'id'		=>	$child['questionId'],
										'status'	=>	1
							));
							$select->columns(array('question', 'description', 'questionType', 'id'));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$childQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$childQuestions = $childQuestions->toArray();
							$childQuestions = $childQuestions[0];
							$temp = $this->getAnswerDetails($child, $data);
							$childQuestions['userAnswer'] = $temp->userAnswer;
							$childQuestions['origAnswer'] = $temp->origAnswer;
							$questions[$key]['childs'][] = $childQuestions;
						}
					}
				}
				$result->questions = $questions;
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		
		//function to get answers of the questions attempted
		public function getAnswerDetails($question, $data) {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select1 = $sql->select();
			$select2 = $sql->select();
			$temp = new stdClass();
			if($question['questionType'] == 0 || $question['questionType'] == 6 || $question['questionType'] == 7) {
				$select1->from('answer_mcq')
				->order('id ASC');
				$select1->where(array(
						'questionId'	=>	$question['questionId'],
						'attemptId'		=>	$data->attemptId
				));
				$select1->columns(array('answer'));
				$select1String = $sql->getSqlStringForSqlObject($select1);
				$userAnswer = $adapter->query($select1String, $adapter::QUERY_MODE_EXECUTE);
				$userAnswer = $userAnswer->toArray();
				$temp->userAnswer = $userAnswer;
				$select2->from('options_mcq')
				->order('id ASC');
				$select2->where(array(
						'questionId'	=>	$question['questionId'],
						'delete'		=>	0
				));
				$select2String = $sql->getSqlStringForSqlObject($select2);
				$origAnswer = $adapter->query($select2String, $adapter::QUERY_MODE_EXECUTE);
				$origAnswer = $origAnswer->toArray();
				$temp->origAnswer = $origAnswer;
			}
			else if($question['questionType'] == 1) {
				$select1->from('answer_tf')
				->order('id ASC');
				$select1->where(array(
						'questionId'	=>	$question['questionId'],
						'attemptId'		=>	$data->attemptId
				));
				$select1->columns(array('answer'));
				$select1String = $sql->getSqlStringForSqlObject($select1);
				$userAnswer = $adapter->query($select1String, $adapter::QUERY_MODE_EXECUTE);
				$userAnswer = $userAnswer->toArray();
				$temp->userAnswer = $userAnswer;
				$select2->from('true_false')
				->order('id ASC');
				$select2->where(array(
						'questionId'	=>	$question['questionId'],
						'delete'		=>	0
				));
				$select2String = $sql->getSqlStringForSqlObject($select2);
				$origAnswer = $adapter->query($select2String, $adapter::QUERY_MODE_EXECUTE);
				$origAnswer = $origAnswer->toArray();
				$temp->origAnswer = $origAnswer;
			}
			else if($question['questionType'] == 2) {
				$select1->from('answer_ftb')
				->order('id ASC');
				$select1->where(array(
						'questionId'	=>	$question['questionId'],
						'attemptId'		=>	$data->attemptId
				));
				$select1->columns(array('answer'));
				$select1String = $sql->getSqlStringForSqlObject($select1);
				$userAnswer = $adapter->query($select1String, $adapter::QUERY_MODE_EXECUTE);
				$userAnswer = $userAnswer->toArray();
				$temp->userAnswer = $userAnswer;
				$select2->from('ftb')
				->order('id ASC');
				$select2->where(array(
						'questionId'	=>	$question['questionId'],
						'delete'		=>	0
				));
				$select2String = $sql->getSqlStringForSqlObject($select2);
				$origAnswer = $adapter->query($select2String, $adapter::QUERY_MODE_EXECUTE);
				$origAnswer = $origAnswer->toArray();
				$temp->origAnswer = $origAnswer;
			}
			else if($question['questionType'] == 3) {
				$select1->from('answer_mtf')
				->order('id ASC');
				$select1->where(array(
						'questionId'	=>	$question['questionId'],
						'attemptId'		=>	$data->attemptId
				));
				$select1->columns(array('answer'));
				$select1String = $sql->getSqlStringForSqlObject($select1);
				$userAnswer = $adapter->query($select1String, $adapter::QUERY_MODE_EXECUTE);
				$userAnswer = $userAnswer->toArray();
				$temp->userAnswer = $userAnswer;
				$select2->from('options_mtf')
				->order('id ASC');
				$select2->where(array(
						'questionId'	=>	$question['questionId'],
						'delete'		=>	0
				));
				$select2String = $sql->getSqlStringForSqlObject($select2);
				$origAnswer = $adapter->query($select2String, $adapter::QUERY_MODE_EXECUTE);
				$origAnswer = $origAnswer->toArray();
				$temp->origAnswer = $origAnswer;
			}
			return $temp;
		}
		
		//function to get topper details
		public function getTopperDetails($data) {
			//print_r($data);
			$result = new stdClass();
			try {
			
				$adapter = $this->adapter;

				$select = "SELECT ss.userId
							FROM `exams` AS e
							INNER JOIN `student_subject` AS ss ON e.subjectId=ss.subjectId AND ss.Active=1
							INNER JOIN `login_details` AS ld ON ld.id=ss.userId AND ld.temp=0
							WHERE e.id={$data->examId}
							GROUP BY ss.userId";
				$students = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$students = $students->toArray();
				$examStudents = array();
				foreach ($students as $key => $value) {
					$examStudents[] = $value['userId'];
				}
				$examStudents = implode(",", $examStudents);

				//$select = "SELECT AVG(score) AS average, MAX(score) AS max FROM `exam_attempts` WHERE examId={$data->examId} AND endDate!=0";
				$select = "SELECT AVG(score) AS average, MAX(score) AS max
							FROM `exam_attempts`
							WHERE examId={$data->examId}
							AND studentId IN ($examStudents)
							AND endDate!=0";
				$avgMax = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$avgMax = $avgMax->toArray();
				$result->avgMax = $avgMax[0];
				//print_r($result);
				//$select = "SELECT score from exam_attempts WHERE examId={$data->examId} AND id={$data->attemptId}";
				$select = "SELECT score
							FROM exam_attempts
							WHERE examId={$data->examId}
							AND studentId IN ($examStudents)
							AND id={$data->attemptId}";
				$self = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$self = $self->toArray();
				if($data->userRole == 1  || $data->userRole == 2  || $data->userRole == 5  ){
					$studentId=$data->studentId;
				}else{
					$studentId=$data->userId;
				}
				$select = "SELECT firstName , lastName FROM `student_details` WHERE userId={$studentId}";
				$selfName = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$selfName = $selfName->toArray();
				$self[0]['selfName']=$selfName;
				//print_r($select);
				//print_r($selfName);
				$result->self = $self[0];
				//print_r('$self');
				//print_r($result);
				//total attempts for median calculation
				//$select = "SELECT COUNT(*) AS total FROM exam_attempts WHERE examId={$data->examId}"; 13/09/2016
				$select = "SELECT COUNT(*) AS total FROM exam_attempts WHERE examId={$data->examId} AND studentId IN ($examStudents)";
				$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$total = $total->toArray();
				$total = $total[0]['total'];
				
				$limit = 0;
				//deciding the query string
				if($total%2 != 0) {
					$limit = (($total + 1) / 2) - 1;
					//$select = "SELECT score FROM exam_attempts WHERE examId={$data->examId} ORDER BY score LIMIT {$limit}, 1";
					$select = "SELECT score
								FROM exam_attempts
								WHERE examId={$data->examId}
								AND studentId IN ($examStudents)
								ORDER BY score
								LIMIT {$limit}, 1";
				}else {
					$limit = ($total / 2) - 1;
					//$select = "SELECT score FROM exam_attempts WHERE examId={$data->examId} ORDER BY score LIMIT {$limit}, 2";
					$select = "SELECT score
								FROM exam_attempts
								WHERE examId={$data->examId}
								AND studentId IN ($examStudents)
								ORDER BY score
								LIMIT {$limit}, 2";
				}
				//now calculating median score
				$median = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$median = $median->toArray();

				if($total%2 != 0)
					$result->median = $median[0]['score'];
				else
					$result->median = ($median[0]['score'] + $median[1]['score'])/2;

				//$select = "SELECT AVG(percentage) AS average, MAX(percentage) AS max FROM `exam_attempts` WHERE examId={$data->examId} AND endDate!=0";
				$select = "SELECT AVG(percentage) AS average, MAX(percentage) AS max
							FROM `exam_attempts`
							WHERE examId={$data->examId}
							AND studentId IN ($examStudents)
							AND endDate!=0";
				$avgMax = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$avgMax = $avgMax->toArray();
				$result->pavgMax = $avgMax[0];
				
				$select = "SELECT percentage from exam_attempts WHERE examId={$data->examId} AND id={$data->attemptId}";
				$self = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$self = $self->toArray();
				$result->pself = $self[0];
				
				$limit = 0;
				//deciding the query string
				if($total%2 != 0) {
					$limit = (($total + 1) / 2) - 1;
					//$select = "SELECT percentage FROM exam_attempts WHERE examId={$data->examId} ORDER BY percentage LIMIT {$limit}, 1";
					$select = "SELECT percentage
								FROM exam_attempts
								WHERE examId={$data->examId}
								ORDER BY percentage
								LIMIT {$limit}, 1";
				}else {
					$limit = ($total / 2) - 1;
					//$select = "SELECT percentage FROM exam_attempts WHERE examId={$data->examId} ORDER BY percentage LIMIT {$limit}, 2";
					$select = "SELECT percentage
								FROM exam_attempts
								WHERE examId={$data->examId}
								ORDER BY percentage
								LIMIT {$limit}, 2";
				}
				//now calculating median percentage
				$median = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$median = $median->toArray();
				if($total%2 != 0)
					$result->pmedian = $median[0]['percentage'];
				else
					$result->pmedian = ($median[0]['percentage'] + $median[1]['percentage'])/2;
				//print_r($result);
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		//function to get time for question
		public function getMaxiumTimeForQuestion($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;

				$select = "SELECT ss.userId
							FROM `exams` AS e
							INNER JOIN `student_subject` AS ss ON e.subjectId=ss.subjectId AND ss.Active=1
							INNER JOIN `login_details` AS ld ON ld.id=ss.userId AND ld.temp=0
							WHERE e.id={$data->examId}
							GROUP BY ss.userId";
				$students = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$students = $students->toArray();
				$examStudents = array();
				foreach ($students as $key => $value) {
					$examStudents[] = $value['userId'];
				}
				$examStudents = implode(",", $examStudents);

				$sql = new Sql($adapter);
				
				//fetching average time for this question
				/*$select = "SELECT AVG(aq.time) AS average
							FROM `attempt_questions` AS aq
							INNER JOIN `exam_attempts` AS ea ON ea.id=aq.attemptId
							WHERE questionId={$data->questionId}
							AND studentId IN ($examStudents)";*/
				$select = "SELECT AVG(aq.time) AS average
							FROM `attempt_questions` AS aq
							INNER JOIN `exam_attempts` AS ea ON ea.id=aq.attemptId
							WHERE questionId={$data->questionId}
							AND studentId IN ($examStudents)";
				$avgMax = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$avgMax = $avgMax->toArray();
				$result->avg = $avgMax[0]['average'];
				
				/*
				* Commenting as no median is needed to show on time compare.
				//fetching total attempts of the question
				$select = "SELECT COUNT(*) AS total FROM attempt_questions WHERE questionId={$data->questionId} AND time!=0";
				$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$total = $total->toArray();
				$total = $total[0]['total'];
				$limit = 0;
				//deciding the query string
				if($total%2 != 0) {
					$limit = (($total + 1) / 2) - 1;
					$select = "SELECT time FROM attempt_questions WHERE questionId={$data->questionId} AND time!=0 ORDER BY time DESC LIMIT {$limit}, 1";
				}else {
					$limit = ($total / 2) - 1;
					$select = "SELECT time FROM attempt_questions WHERE questionId={$data->questionId} AND time!=0 ORDER BY time DESC LIMIT {$limit}, 2";
				}
				//now calculating median percentage
				$median = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$median = $median->toArray();
				if($total%2 != 0)
					$result->median = $median[0]['time'];
				else
					$result->median = ($median[0]['time'] + $median[1]['time'])/2;
				*/
				
				//fetching self time
				$select = "SELECT `time`, `check` FROM attempt_questions WHERE questionId={$data->questionId} AND attemptId={$data->attemptId}";
				$self = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$self = $self->toArray();
				if($data->userRole == 1  || $data->userRole == 2  || $data->userRole == 5  ){
					$studentId=$data->studentId;
				}else{
					$studentId=$data->userId;
				}
				$select = "SELECT firstName, lastName FROM `student_details` WHERE userId={$studentId}";
				$selfName = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$selfName = $selfName->toArray();
				$self[0]['selfName']=$selfName;
				$result->self = $self[0];
				//SELECT firstName , lastName FROM `student_details` WHERE `userId`
				
				//now fetching the student count for new requirement
				/*$selectString = "SELECT count(Distinct studentId) AS total, questionId,
								SUM(`check`=0) as unattempted,
								SUM(`check`=1) as correct,
								SUM(`check`=2) as wrong
								FROM attempt_questions
								INNER JOIN exam_attempts ON exam_attempts.id=attempt_questions.attemptId
								WHERE questionId={$data->questionId}
								AND studentId IN ($examStudents)";*/
				$selectString = "SELECT count(Distinct studentId) AS total, questionId,
								SUM(`check`=0) as unattempted,
								SUM(`check`=1) as correct,
								SUM(`check`=2) as wrong
								FROM attempt_questions
								INNER JOIN exam_attempts ON exam_attempts.id=attempt_questions.attemptId
								WHERE questionId={$data->questionId}
								AND studentId IN ($examStudents)";
				$total = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$total = $total->toArray();
				$result->studentCount = $total[0];
				
				//fetching topper's time on this question
				//$selectString = "SELECT aq.`time` as `time`, aq.`check` as `check` from exam_attempts ea JOIN attempt_questions aq ON aq.questionId={$data->questionId} AND ea.id=aq.attemptId WHERE examId={$data->examId} ORDER BY score DESC LIMIT 1";
				$selectString ="SELECT aq.`time` as `time`, aq.`check` as `check`
								FROM exam_attempts ea
								INNER JOIN attempt_questions aq ON ea.id=aq.attemptId
								WHERE examId={$data->examId}
								AND ea.studentId IN ($examStudents)
								AND aq.questionId={$data->questionId}
								ORDER BY score
								DESC LIMIT 1";
				$topper = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$topper = $topper->toArray();
				if(count($topper) > 0)
					$result->topper = $topper[0];
				else {
					$result->topper['time'] = 0;
					$result->topper['check'] = 0;
				}
				
				//fetching min time to answer correctly
				/*$select = $sql->select();
				$select->from('attempt_questions');
				$select->columns(array('time'	=>	new \Zend\Db\Sql\Expression("MIN(time)")));
				$select->where(array(
						'check'		=>	1,
						'questionId'=>	$data->questionId
				));
				$selectString = $sql->getSqlStringForSqlObject($select);*/
				$selectString ="SELECT MIN(aq.time) AS `time`
								FROM attempt_questions aq
								INNER JOIN exam_attempts ea ON ea.id=aq.attemptId
								WHERE ea.examId={$data->examId}
								AND ea.studentId IN ($examStudents)
								AND aq.questionId={$data->questionId}
								AND aq.check=1
								ORDER BY score
								DESC LIMIT 1";
				$min = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$min = $min->toArray();
				if(count($min) > 0)
					$result->min = $min[0]['time'];
				else
					$result->min = 0;
				
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		//function to get exam attempts for graph creation
		public function getAttemptGraph($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exam_attempts');
				$where = array();
				if(isset($data->studentId))
					$where = array(
							'examId'	=>	$data->examId,
							'studentId'	=>	$data->studentId
							);
				else
					$where = array(
							'examId'	=>	$data->examId,
							'studentId'	=>	$data->userId
							);
				$select->where($where)->order('id ASC');
				$select->columns(array('score', 'percentage'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$graph = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$graph = $graph->toArray();
				//this is done according to ARC-333
				return $graph;
				/*$result->graph = $graph;
				$result->status = 1;
				return $result;*/
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		//getallStudentNormalizationGraph
		public function getallStudentNormalizationGraph($data) {
			$result = new stdClass();
			try {

				$adapter = $this->adapter;

				if (isset($data->examId) && !empty($data->examId)) {
					$select = "SELECT ss.userId
								FROM `exams` AS e
								INNER JOIN `student_subject` AS ss ON e.subjectId=ss.subjectId AND ss.Active=1
								INNER JOIN `login_details` AS ld ON ld.id=ss.userId
								WHERE e.id={$data->examId} AND ld.temp=0
								GROUP BY ss.userId";
					$students = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$students = $students->toArray();
					$examStudents = array();
					foreach ($students as $key => $value) {
						$examStudents[] = $value['userId'];
					}
					$examStudents = implode(",", $examStudents);

					//$selectString = "SELECT max(score) score,concat(s.firstName,' ', s.lastName) name FROM `exam_attempts` e left join student_details s ON s.userId=e.studentId WHERE examId={$data->examId} AND completed = 1  group by e.studentId ";
					$selectString ="SELECT max(score) score,concat(s.firstName,' ', s.lastName) name
									FROM `exam_attempts` e
									LEFT JOIN student_details s ON s.userId=e.studentId
									WHERE examId={$data->examId}
									AND studentId IN ($examStudents)
									AND completed = 1
									GROUP BY e.studentId";
					$graph = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$graph = $graph->toArray();
					if(!(count($graph)>0))
					{
						$result->status = 0;
						$result->message = "No student had completed the exam!";
						return $result;
					}
					$lowest=0;
					$highest=0;
					$lowest=$graph[0]['score'];
					foreach($graph as $key=>$value)
					{
						if($value['score']>$highest)
						{
							$highest=$value['score'];
						
						}	
						if($value['score']<$lowest)
						{
							$lowest=$value['score'];
						
						}					
						
					}
					$result->highest=$highest;
					$result->lowest=$lowest;
					$result->graph = $graph;
					
					$selectString = "SELECT sum(totalMarks)as total FROM `exam_sections` where examId={$data->examId} AND status = 1 ";
					$total = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$total = $total->toArray();
					//print_r($total[0]['total']);
					$result->total = $total;
				} else if (isset($data->subjectiveExamId) && !empty($data->subjectiveExamId)) {
					$select = "SELECT ss.userId
								FROM `exam_subjective` AS e
								INNER JOIN `student_subject` AS ss ON e.subjectId=ss.subjectId AND ss.Active=1
								WHERE e.id={$data->subjectiveExamId}
								GROUP BY ss.userId";
					$students = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$students = $students->toArray();
					$examStudents = array();
					foreach ($students as $key => $value) {
						$examStudents[] = $value['userId'];
					}
					$examStudents = implode(",", $examStudents);

					//$selectString = "SELECT max(score) score,concat(s.firstName,' ', s.lastName) name FROM `subjective_exam_attempts` e left join student_details s ON s.userId=e.studentId WHERE examId={$data->examId} AND completed = 1  group by e.studentId ";
					$selectString ="SELECT max(score) score,concat(s.firstName,' ', s.lastName) name
									FROM `subjective_exam_attempts` e
									LEFT JOIN student_details s ON s.userId=e.studentId
									WHERE examId={$data->subjectiveExamId}
									AND studentId IN ($examStudents)
									AND completed = 1
									GROUP BY e.studentId";
					$graph = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$graph = $graph->toArray();
					if(!(count($graph)>0))
					{
						$result->status = 0;
						$result->message = "No student had completed the exam!";
						return $result;
					}
					$lowest=0;
					$highest=0;
					$lowest=$graph[0]['score'];
					foreach($graph as $key=>$value)
					{
						if($value['score']>$highest)
						{
							$highest=$value['score'];
						
						}	
						if($value['score']<$lowest)
						{
							$lowest=$value['score'];
						
						}					
						
					}
					$result->highest=$highest;
					$result->lowest=$lowest;
					$result->graph = $graph;
					
					$selectString ="SELECT sum(marks) as total
									FROM `questions_subjective` AS qs
									INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id
									WHERE examId={$data->subjectiveExamId} AND status = 1";
					$total = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$total = $total->toArray();
					//print_r($total[0]['total']);
					$result->total = $total;
				} else if (isset($data->submissionId) && !empty($data->submissionId)) {
					require_once("Submission.php");
					$s = new Submission();
					$resAccess = $s->getSubmissionAccess($data->submissionId);
					if ($resAccess->status == 0) {
						$result->status = 0;
						$result->message = $result->status;
						return $result;
					}
					$access = $resAccess->access;
					if ($access == "public") {
						$select = "SELECT ss.userId
								FROM `submissions` AS e
								INNER JOIN `student_subject` AS ss ON e.subjectId=ss.subjectId AND ss.Active=1
								WHERE e.id={$data->submissionId}
								GROUP BY ss.userId";
						$students = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
						$students = $students->toArray();
						$examStudents = array();
						foreach ($students as $key => $value) {
							$examStudents[] = $value['userId'];
						}
						$examStudents = implode(",", $examStudents);
					} else {
						require_once("Subject.php");
						$sub = new Subject();
						$dataSG = new stdClass();
						$dataSG->examId = $data->submissionId;
						$resStudentGroups = $s->getSubmissionGroups($dataSG);
						if ($resStudentGroups->status == 0) {
							$result->status = 0;
							$result->message = $resStudentGroups->exception;
							return $result;
						}
						$studentGroups = $resStudentGroups->selectedStudentGroups;
						$arrStudents = array();
						foreach ($studentGroups as $key => $studentGroup) {
							$dataSG = new stdClass();
							$dataSG->studentGroupId = $studentGroup;
							$resStudents = $sub->getStudentsByStudentGroup($dataSG);
							if ($resStudents->status == 1) {
								foreach ($resStudents->students as $key => $student) {
									$arrStudents[] = $student['studentId'];
								}
							}
						}
						$examStudents = implode(",", $arrStudents);
					}

					//$selectString = "SELECT max(score) score,concat(s.firstName,' ', s.lastName) name FROM `submission_attempts` e left join student_details s ON s.userId=e.studentId WHERE examId={$data->examId} AND completed = 1  group by e.studentId ";
					/*$selectString ="SELECT max(score) score,concat(s.firstName,' ', s.lastName) name
									FROM `submission_attempts` e
									LEFT JOIN student_details s ON s.userId=e.studentId
									WHERE examId={$data->submissionId}
									AND studentId IN ($examStudents)
									AND completed = 1
									GROUP BY e.studentId";*/
					$selectString ="SELECT max(satm.marks) score,concat(s.firstName,' ', s.lastName) name
									FROM `submission_attempts` e
									INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=e.id
									LEFT JOIN student_details s ON s.userId=satm.studentId
									WHERE e.examId={$data->submissionId}
									AND satm.studentId IN ($examStudents)
									AND e.completed = 1
									GROUP BY satm.studentId";
					$graph = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$graph = $graph->toArray();
					if(!(count($graph)>0))
					{
						$result->status = 0;
						$result->message = "No student had completed the exam!";
						return $result;
					}
					$lowest=0;
					$highest=0;
					$lowest=$graph[0]['score'];
					foreach($graph as $key=>$value)
					{
						if($value['score']>$highest)
						{
							$highest=$value['score'];
						
						}	
						if($value['score']<$lowest)
						{
							$lowest=$value['score'];
						
						}					
						
					}
					$result->highest=$highest;
					$result->lowest=$lowest;
					$result->graph = $graph;
					
					$selectString ="SELECT sum(marks) as total
									FROM `submission_questions` AS qs
									INNER JOIN `submission_question_links` AS qsel ON qsel.questionId=qs.id
									WHERE examId={$data->submissionId} AND status = 1";
					$total = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$total = $total->toArray();
					//print_r($total[0]['total']);
					$result->total = $total;
				}
				
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		//function to get timeline graph
		public function getStudentTimeLine($data) {
			//print_r($data);
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$select = "SELECT ss.userId
							FROM `exams` AS e
							INNER JOIN `student_subject` AS ss ON e.subjectId=ss.subjectId AND ss.Active=1
							INNER JOIN `login_details` AS ld ON ld.id=ss.userId AND ld.temp=0
							WHERE e.id={$data->examId}
							GROUP BY ss.userId";
				$students = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$students = $students->toArray();
				$examStudents = array();
				foreach ($students as $key => $value) {
					$examStudents[] = $value['userId'];
				}
				$examStudents = implode(",", $examStudents);

				/*
				$selectString = "SELECT studentId FROM exam_attempts WHERE examId={$data->examId} and score=(select max(score) from exam_attempts where examId={$data->examId} )";
				$topper = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$topper = $topper->toArray();
				$topper = $topper[0]['studentId'];
				$selectString = "SELECT endDate,score FROM `exam_attempts` WHERE examId= {$data->examId} and studentId =$topper and completed =1";
				$topper = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$topper = $topper->toArray();
				$result->topper=$topper;*/
				//now fetching user-self detail
				if (isset($data->student_id) && !empty($data->student_id)) {
					$studentId=$data->student_id;
				} else {
					$studentId=$data->userId;
				}
				$selectString ="SELECT endDate, score
								FROM `exam_attempts`
								WHERE examId = {$data->examId}
								AND studentId = {$studentId}
								AND completed = 1";
				$self = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$self = $self->toArray();
				$result->self=$self;

				//now average marks of class
				$selectString ="SELECT max(score) score, endDate
								FROM exam_attempts
								WHERE examId= {$data->examId}
								AND studentId IN ($examStudents)
								AND completed = 1
								AND score>0
								GROUP BY studentId
								ORDER BY endDate";
				$avgScore = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$avgScore = $avgScore->toArray();
				$result->avgScore=$avgScore;				
				
				$selectString ="SELECT score, endDate
								FROM exam_attempts
								WHERE examId= {$data->examId}
								AND completed =1
								ORDER BY endDate DESC
								LIMIT 1";
				$lastday = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$lastday = $lastday->toArray();
				$result->lastday=$lastday;
				//print_r($result);
				$result->status=1;
				return $result;			
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		
		//function to get exam report for institute
		public function getExamReport($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				if (isset($data->examId) && !empty($data->examId)) {
					$selectString = "SELECT COUNT(A.student) as num, A.attempt as attempt
						FROM (
							SELECT COUNT(DISTINCT ea.id) as attempt, ea.studentId as student
							FROM `exam_attempts` AS ea
							INNER JOIN `login_details` AS ld ON ld.id=ea.studentId
							INNER JOIN exams AS e ON e.id=ea.examId
							INNER JOIN student_subject AS ss ON ss.userId=ea.studentId AND ss.Active=1
							WHERE ea.examId={$data->examId} AND ss.subjectId=e.subjectId AND ea.completed = 1 AND ld.temp=0
							GROUP BY studentId
						) as A GROUP BY A.attempt";
					$graph = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$graph = $graph->toArray();
					$result->graph = $graph;
					if(!(count($graph)>0))
					{
						$result->status = 0;
						$result->message = "No student had completed the exam!";
						return $result;
					}
					$selectString = "SELECT MIN(ea.score) as min, MAX(ea.score) as max, AVG(ea.score) as avg FROM exam_attempts AS ea
						INNER JOIN `login_details` AS ld ON ld.id=ea.studentId
						WHERE ea.examId={$data->examId} AND ld.temp=0";
					$mma = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$mma = $mma->toArray();
					$result->mma = $mma[0];
					
					$selectString = "SELECT COUNT(DISTINCT ea.studentId) AS attempted, e.attempts as allowed FROM exam_attempts AS ea
						INNER JOIN exams as e ON e.id=ea.examId
						INNER JOIN `login_details` AS ld ON ld.id=ea.studentId
						WHERE examId={$data->examId} AND ld.temp=0";
					$attempted = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$attempted = $attempted->toArray();
					$result->attempted = $attempted[0]['attempted'];
					$result->allowed = $attempted[0]['allowed'];
					
					
					$selectString = "SELECT COUNT(*) AS total FROM exam_attempts WHERE examId={$data->examId}";
					$total = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$total = $total->toArray();
					$result->total = $total[0]['total'];
					
					//now calculating median
					$total = $result->total;
					$limit = 0;
					//deciding the query string
					if($total%2 != 0) {
						$limit = (($total + 1) / 2) - 1;
						$select = "SELECT score FROM exam_attempts WHERE examId={$data->examId} ORDER BY score LIMIT {$limit}, 1";
					}else {
						$limit = ($total / 2) - 1;
						$select = "SELECT score FROM exam_attempts WHERE examId={$data->examId} ORDER BY score LIMIT {$limit}, 2";
					}
					//now calculating median score
					$median = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$median = $median->toArray();
					if(count($median) == 0) {
						$median[0]['score'] = $result->mma['min'];
						$median[1]['score'] = $result->mma['min'];
					}
					if($total%2 != 0)
						$result->mma['median'] = $median[0]['score'];
					else
						$result->mma['median'] = ($median[0]['score'] + $median[1]['score'])/2;
				} else if (isset($data->subjectiveExamId) && !empty($data->subjectiveExamId)) {
					$selectString = "SELECT COUNT(A.student) as num, A.attempt as attempt
						FROM (
							SELECT COUNT(DISTINCT sea.id) as attempt, sea.studentId as student
							FROM `subjective_exam_attempts` AS sea
							INNER JOIN `login_details` AS ld ON ld.id=sea.studentId
							INNER JOIN exam_subjective AS e ON e.id=sea.examId
							INNER JOIN student_subject AS ss ON ss.userId=sea.studentId AND ss.Active=1
							WHERE sea.examId={$data->subjectiveExamId} AND ss.subjectId=e.subjectId AND sea.completed = 1 AND ld.temp=0
							GROUP BY studentId
						) as A GROUP BY A.attempt";
					$graph = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$graph = $graph->toArray();
					$result->graph = $graph;
					if(!(count($graph)>0))
					{
						$result->status = 0;
						$result->message = "No student had completed the exam!";
						return $result;
					}

					$selectString = "SELECT MIN(sea.score) as min, MAX(sea.score) as max, AVG(sea.score) as avg FROM subjective_exam_attempts AS sea
						INNER JOIN `login_details` AS ld ON ld.id=sea.studentId
						WHERE sea.examId={$data->subjectiveExamId} AND ld.temp=0";
					$mma = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$mma = $mma->toArray();
					$result->mma = $mma[0];
					
					$selectString = "SELECT COUNT(DISTINCT sea.studentId) AS attempted, e.attempts as allowed FROM subjective_exam_attempts AS sea
						INNER JOIN exam_subjective as e ON e.id=examId
						INNER JOIN `login_details` AS ld ON ld.id=sea.studentId
						WHERE sea.examId={$data->subjectiveExamId} AND ld.temp=0";
					$attempted = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$attempted = $attempted->toArray();
					$result->attempted = $attempted[0]['attempted'];
					$result->allowed = $attempted[0]['allowed'];
					
					
					$selectString = "SELECT COUNT(*) AS total FROM subjective_exam_attempts WHERE examId={$data->subjectiveExamId}";
					$total = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$total = $total->toArray();
					$result->total = $total[0]['total'];

					//now calculating median
					$total = $result->total;
					$limit = 0;
					//deciding the query string
					if($total%2 != 0) {
						$limit = (($total + 1) / 2) - 1;
						$select = "SELECT score FROM subjective_exam_attempts WHERE examId={$data->subjectiveExamId} ORDER BY score LIMIT {$limit}, 1";
					}else {
						$limit = ($total / 2) - 1;
						$select = "SELECT score FROM subjective_exam_attempts WHERE examId={$data->subjectiveExamId} ORDER BY score LIMIT {$limit}, 2";
					}
					//now calculating median score
					$median = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$median = $median->toArray();
					if(count($median) == 0) {
						$median[0]['score'] = $result->mma['min'];
						$median[1]['score'] = $result->mma['min'];
					}
					if($total%2 != 0)
						$result->mma['median'] = $median[0]['score'];
					else
						$result->mma['median'] = ($median[0]['score'] + $median[1]['score'])/2;
				} else if (isset($data->submissionId) && !empty($data->submissionId)) {
					require_once("Submission.php");
					$s = new Submission();
					$resAccess = $s->getSubmissionAccess($data->submissionId);
					if ($resAccess->status == 0) {
						$result->status = 0;
						$result->message = $result->status;
						return $result;
					}
					$access = $resAccess->access;
					$selectString = "SELECT MIN(satm.marks) as min, MAX(satm.marks) as max, AVG(satm.marks) as avg
						FROM submission_attempts AS sea
						INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sea.id
						INNER JOIN `login_details` AS ld ON ld.id=satm.studentId
						WHERE sea.examId={$data->submissionId} AND ld.temp=0";
					$mma = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$mma = $mma->toArray();
					$result->mma = $mma[0];
					
					$selectString = "SELECT COUNT(DISTINCT satm.studentId) AS attempted, e.attempts as allowed
						FROM submission_attempts AS sea
						INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sea.id
						INNER JOIN `submissions` as e ON e.id=sea.examId
						INNER JOIN `login_details` AS ld ON ld.id=satm.studentId
						WHERE sea.examId={$data->submissionId} AND ld.temp=0";
					$attempted = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$attempted = $attempted->toArray();
					$result->attempted = $attempted[0]['attempted'];
					$result->allowed = $attempted[0]['allowed'];
					
					
					$selectString = "SELECT COUNT(*) AS total
						FROM submission_attempts AS sea
						INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sea.id
						WHERE examId={$data->submissionId}";
					$total = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$total = $total->toArray();
					$result->total = $total[0]['total'];

					//now calculating median
					$total = $result->total;
					$limit = 0;
					//deciding the query string
					if($total%2 != 0) {
						$limit = (($total + 1) / 2) - 1;
						$select = "SELECT satm.marks as score FROM submission_attempts AS sea
						INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sea.id
						WHERE examId={$data->submissionId}
						ORDER BY satm.marks LIMIT {$limit}, 1";
					}else {
						$limit = ($total / 2) - 1;
						$select = "SELECT satm.marks as score FROM submission_attempts AS sea
						INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sea.id
						WHERE examId={$data->submissionId} 
						ORDER BY satm.marks LIMIT {$limit}, 2";
					}
					//now calculating median score
					$median = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$median = $median->toArray();
					if(count($median) == 0) {
						$median[0]['score'] = $result->mma['min'];
						$median[1]['score'] = $result->mma['min'];
					}
					if($total%2 != 0)
						$result->mma['median'] = $median[0]['score'];
					else
						$result->mma['median'] = ($median[0]['score'] + $median[1]['score'])/2;
				}
				
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		//function to get student list who attempted the given exam
		public function getStudentList($data) {
			$result = new stdClass();
			try {
				
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				if (isset($data->examId) && !empty($data->examId)) {
					//$selectString = "SELECT sd.userId, sd.firstName, sd.lastName, ld.email FROM student_course AS sc JOIN student_details AS sd ON sd.userId=sc.user_id JOIN login_details AS ld ON ld.id=sc.user_id WHERE course_id=(SELECT courseId FROM subjects WHERE id=(SELECT subjectId FROM exams WHERE id={$data->examId}))";
					$selectString ="SELECT sd.userId, sd.firstName, sd.lastName, ld.email
									FROM student_subject AS ss
									INNER JOIN student_details AS sd ON sd.userId=ss.userId
									INNER JOIN login_details AS ld ON ld.id=ss.userId AND ld.temp=0
									WHERE subjectId=(SELECT subjectId FROM exams WHERE id={$data->examId}) AND ss.Active=1
									GROUP BY ss.userId";
					$students = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$students = $students->toArray();
					$result->students = $students;
					foreach($result->students as $key=>$student) {
						$selectString = "SELECT MAX(score) AS max, AVG(score) AS avg, COUNT(*) AS total FROM exam_attempts WHERE examId={$data->examId} AND studentId={$student['userId']}";
						$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$details = $details->toArray();
						if(count($details) > 0)
							$result->students[$key]['details'] = $details[0];
						$selectString = "SELECT score FROM exam_attempts WHERE id = (SELECT MAX(id) FROM exam_attempts WHERE examId={$data->examId} AND studentId={$student['userId']});";
						$details1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$details1 = $details1->toArray();
						if(count($details1) > 0)
							$result->students[$key]['lastScore'] = $details1[0];
					}
					
					//now detecting the highest and lowest score of the exam
					$selectString = "SELECT distinct studentId,score FROM exam_attempts WHERE examId={$data->examId} AND score=(SELECT MAX(score) FROM exam_attempts WHERE examId={$data->examId}) UNION SELECT distinct studentId,score FROM exam_attempts WHERE examId={$data->examId} AND score=(SELECT MIN(score) FROM exam_attempts WHERE examId={$data->examId})";
					$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$temp = $temp->toArray();
					$result->toppers = $temp;
				} else if (isset($data->subjectiveExamId) && !empty($data->subjectiveExamId)) {
					$selectString ="SELECT sd.userId, sd.firstName, sd.lastName, ld.email
									FROM student_subject AS ss
									INNER JOIN student_details AS sd ON sd.userId=ss.userId
									INNER JOIN login_details AS ld ON ld.id=ss.userId AND ld.temp=0
									WHERE subjectId=(SELECT subjectId FROM exam_subjective WHERE id={$data->subjectiveExamId}) AND ss.Active=1
									GROUP BY ss.userId";
					$students = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$students = $students->toArray();
					$result->students = $students;
					if (count($result->students)) {
						foreach($result->students as $key=>$student) {
							$selectString = "SELECT MAX(score) AS max, AVG(score) AS avg, MAX(id) AS attemptId,  COUNT(*) AS total FROM subjective_exam_attempts WHERE examId={$data->subjectiveExamId} AND studentId={$student['userId']}";
							$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$details = $details->toArray();
							if(count($details) > 0)
								$result->students[$key]['details'] = $details[0];
							$selectString = "SELECT score FROM subjective_exam_attempts WHERE id = (SELECT MAX(id) FROM subjective_exam_attempts WHERE examId={$data->subjectiveExamId} AND studentId={$student['userId']});";
							$details1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$details1 = $details1->toArray();
							if(count($details1) > 0)
								$result->students[$key]['lastScore'] = $details1[0];
						}
					}
					//now detecting the highest and lowest score of the exam
					$selectString ="SELECT distinct studentId,score
									FROM subjective_exam_attempts
									WHERE examId={$data->subjectiveExamId} AND score=(
										SELECT MAX(score)
										FROM subjective_exam_attempts
										WHERE examId={$data->subjectiveExamId}
									)
									UNION
									SELECT distinct studentId,score
									FROM subjective_exam_attempts
									WHERE examId={$data->subjectiveExamId} AND score=(
										SELECT MIN(score)
										FROM subjective_exam_attempts
										WHERE examId={$data->subjectiveExamId}
									)";
					$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$temp = $temp->toArray();
					$result->toppers = $temp;
				} else if (isset($data->submissionId) && !empty($data->submissionId)) {
					require_once("Submission.php");
					$s = new Submission();
					$resAccess = $s->getSubmissionAccess($data->submissionId);
					if ($resAccess->status == 0) {
						$result->status = 0;
						$result->message = $result->status;
						return $result;
					}
					$access = $resAccess->access;
					if ($access == "public") {
						$selectString ="SELECT sd.userId, concat(sd.firstName,' ',sd.lastName) AS name, ld.email
										FROM student_subject AS ss
										INNER JOIN student_details AS sd ON sd.userId=ss.userId
										INNER JOIN login_details AS ld ON ld.id=ss.userId AND ld.temp=0
										WHERE subjectId=(SELECT subjectId FROM submissions WHERE id={$data->submissionId}) AND ss.Active=1
										GROUP BY ss.userId";
						$students = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$students = $students->toArray();
						$result->students = $students;
					} else {
						require_once("Subject.php");
						$sub = new Subject();
						$dataSG = new stdClass();
						$dataSG->examId = $data->submissionId;
						$resStudentGroups = $s->getSubmissionGroups($dataSG);
						if ($resStudentGroups->status == 0) {
							$result->status = 0;
							$result->message = $resStudentGroups->exception;
							return $result;
						}
						$studentGroups = $resStudentGroups->selectedStudentGroups;
						$arrStudents = array();
						foreach ($studentGroups as $key => $studentGroup) {
							$dataSG = new stdClass();
							$dataSG->studentGroupId = $studentGroup;
							$resStudents = $sub->getStudentsByStudentGroup($dataSG);
							if ($resStudents->status == 1) {
								$arrStudents = array_merge($arrStudents,$resStudents->students);
							}
						}
						$result->students = $arrStudents;
					}
					if (count($result->students)) {
						foreach($result->students as $key=>$student) {
							/*$selectString = "SELECT MAX(score) AS max, AVG(score) AS avg, MAX(id) AS attemptId,  COUNT(*) AS total
							FROM submission_attempts
							WHERE examId={$data->submissionId} AND studentId={$student['userId']}";*/
							$selectString = "SELECT MAX(satm.marks) AS max, AVG(satm.marks) AS avg, MAX(sa.id) AS attemptId,  COUNT(*) AS total
							FROM `submission_attempts` AS sa
							INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
							WHERE sa.examId={$data->submissionId} AND satm.studentId={$student['userId']}";

							$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$details = $details->toArray();
							if(count($details) > 0)
								$result->students[$key]['details'] = $details[0];
							/*$selectString = "SELECT score FROM submission_attempts WHERE id = (SELECT MAX(id) FROM submission_attempts WHERE examId={$data->submissionId} AND studentId={$student['userId']});";*/
							$selectString = "SELECT satm.marks AS score
												FROM `submission_attempts` AS sa
												INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
												WHERE satm.id = (
													SELECT MAX(satm.id)
													FROM submission_attempts AS sa
													INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
													WHERE sa.examId={$data->submissionId} AND satm.studentId={$student['userId']}
												);";
							$details1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$details1 = $details1->toArray();
							if(count($details1) > 0)
								$result->students[$key]['lastScore'] = $details1[0];
						}
					}
					//now detecting the highest and lowest score of the exam
					/*$selectString ="SELECT distinct studentId,score
									FROM submission_attempts
									WHERE examId={$data->submissionId} AND score=(
										SELECT MAX(score)
										FROM submission_attempts
										WHERE examId={$data->submissionId}
									)
									UNION
									SELECT distinct studentId,score
									FROM submission_attempts
									WHERE examId={$data->submissionId} AND score=(
										SELECT MIN(score)
										FROM submission_attempts
										WHERE examId={$data->submissionId}
									)";*/
					$selectString ="SELECT distinct satm1.studentId,satm1.marks AS score
									FROM submission_attempts AS sa1
									INNER JOIN `submission_attempt_total_marks` AS satm1 ON satm1.attemptId=sa1.id
									WHERE sa1.examId={$data->submissionId} AND satm1.marks=(
										SELECT MAX(satm.marks)
										FROM submission_attempts AS sa
										INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
										WHERE sa.examId={$data->submissionId}
									)
									UNION
									SELECT distinct satm2.studentId,satm2.marks AS score
									FROM submission_attempts AS sa2
									INNER JOIN `submission_attempt_total_marks` AS satm2 ON satm2.attemptId=sa2.id
									WHERE sa2.examId={$data->submissionId} AND satm2.marks=(
										SELECT MIN(satm.marks)
										FROM submission_attempts AS sa
										INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
										WHERE sa.examId={$data->submissionId}
									)";
					$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$temp = $temp->toArray();
					$result->toppers = $temp;
				}
				
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//check If course belongs to that prof or not
		public function userOwnsExams($data) {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('exams');
			$where = new \Zend\Db\Sql\Where();
			$where->equalTo('ownerId', $data->userId);
			$where->equalTo('id', $data->examId);
			$result = new stdClass();
			$select->where($where);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			if (count($result->courses) == 0) {
				return false;
			}
			return true;
		}

		//check If course belongs to that prof or not
		public function userAssignedExams($data) {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('ex'	=>	'exams'))
					->join(array('sp'	=>	'subject_professors'),
							'ex.subjectId = sp.subjectId');
			$where = new \Zend\Db\Sql\Where();
			$where->equalTo('sp.professorId', $data->userId);
			$where->equalTo('ex.id', $data->examId);
			$result = new stdClass();
			$select->where($where);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			if (count($result->courses) == 0) {
				return false;
			}
			return true;
		}
			
		// return question list
		public function getQuestionList($data) {
			$result = new stdClass();
			try {				
				if (!$this->userOwnsExams($data) && !$this->userAssignedExams($data)) {
					$result->status = 0;
					$result->message = "An non recoverable error occured!";
					return $result;
				}
			
				//fetching sections of exam
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('es'	=>	'exam_sections'));
				$select->order('es.weight ASC, es.id ASC');
				$select->where(array('es.examId'	=>	$data->examId,
									'es.delete'	=>	0,
									'es.status'	=>	1));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$sections = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$sections = $sections->toArray();
				//$sectionId=$sections[0]['id'];
				$questionsArray = array();
				$result->questions = array();
				foreach($sections as $section) {
					$sectionId=$section['id'];
					
					//for calculating  questions
					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					//getting categories of the section
					$select = $sql->select();
					$select->from('section_categories');
					$select->where(array('sectionId'		=>	$sectionId,
										'delete'	=>	0,
										'status'	=>	1));
					$select->order('weight ASC');
					$selectString = $sql->getSqlStringForSqlObject($select);
					$categories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$categories = $categories->toArray();
					
					foreach($categories as $category) {
						$sql = new Sql($adapter);
						$select = $sql->select();
						if($category['questionType'] != 4) {
						$select->from(array('q'=>'questions'));
						$select->join(array('qcl'	=>	'question_category_links'),
								'q.id = qcl.questionId',
								array('categoryId')
						);							
						$select->where(array('categoryId'	=>	$category['id'],
											'delete'	=>	0,
											'status'	=>	1,
											'parentId'	=>	0));
						$select->limit($category['required']);
						$selectString = $sql->getSqlStringForSqlObject($select);
						}									
						else {
							$select->from(array('q1'	=>	'questions'))
									->join(array('q2'	=>	'questions'),
											'q2.id = q1.parentId',
											array('parent'	=>	'question'));
							$select->join(array('qcl'	=>	'question_category_links'),
									'q1.id = qcl.questionId',
									array('categoryId')
							);
							$select->order('parentId');
							//$select->order(new \Zend\Db\Sql\Expression("RAND()"));
							$where = new \Zend\Db\Sql\Where();
							$where->equalTo('qcl.categoryId', $category['id']);
							$where->equalTo('q1.delete', 0);
							$where->equalTo('q1.status', 1);
							$where->notEqualTo('q1.parentId', 0);
							$select->where($where);
							$select->limit($category['required']);
							$selectString = $sql->getSqlStringForSqlObject($select);
						}
						$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$questions = $questions->toArray();
						$result->questions[] = $questions;
						//array_push($questionsArray,$questions);
					}
				}
				
				
				foreach($result->questions as $key=>$value)
				{
					foreach($value as $key1=>$question)
					{
						$result->questions[$key][$key1]['question'] = trim(str_replace("&nbsp;", "", strip_tags($result->questions[$key][$key1]['question'])));
						array_push($questionsArray,$question['id']);
					}
				}

				$select = "SELECT ss.userId
							FROM `exams` AS e
							INNER JOIN `student_subject` AS ss ON e.subjectId=ss.subjectId AND ss.Active=1
							WHERE e.id={$data->examId}
							GROUP BY ss.userId";
				$students = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$students = $students->toArray();
				$examStudents = array();
				foreach ($students as $key => $value) {
					$examStudents[] = $value['userId'];
				}
				$examStudents = implode(",", $examStudents);
			
				$questionsArray1=implode(",",$questionsArray);
				//$select = "SELECT COUNT(id) AS total FROM attempt_questions WHERE questionId IN ($questionsArray1) AND (`check`=1 OR `check`=2 OR (`status`=1 OR (`check`=0 AND `status`=3 ))) GROUP BY questionId ORDER BY FIELD(questionId,$questionsArray1)";
				$select = "SELECT COUNT(aq.id) AS total,questionId
							FROM `attempt_questions` AS aq
							INNER JOIN `exam_attempts` AS ea ON ea.id=aq.attemptId
							WHERE ea.examId={$data->examId}
							AND ea.studentId IN ($examStudents)
							AND questionId IN ($questionsArray1) AND (`check`=1 OR `check`=2 OR (`status`=1 OR (`check`=0 AND `status`=3 )))
							GROUP BY questionId
							ORDER BY FIELD(questionId,$questionsArray1)";
				$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$total = $total->toArray();
				$totalQ=array();
				for ($i=0; $i < count($result->questions[0]); $i++) {
					$index = array_search($result->questions[0][$i]['id'], array_column($total, 'questionId'));
					if ($index>-1) {
						$totalQ[] = $total[$index]['total'];
					} else {
						$totalQ[] = 0;
					}
				}
				/*foreach($total as $key=>$qStudnets)
				{
					array_push($totalQ,$qStudnets['total']);
				}*/
				
				// print_r($totalQ);
				$result->studentGotQues=$totalQ;
				
				//$select = "SELECT COUNT(id) AS total FROM attempt_questions WHERE questionId IN ($questionsArray1) AND `check`=2 GROUP BY questionId ORDER BY FIELD(questionId,$questionsArray1)";
				/*$select = "SELECT COUNT(aq.id) as total
							FROM questions AS q
							LEFT JOIN attempt_questions AS aq ON aq.questionId=q.id AND aq.check=2
							WHERE q.id IN ($questionsArray1)
							GROUP BY q.id
							ORDER BY FIELD(q.id,$questionsArray1)";*/
				$select = "SELECT COUNT(aq.id) as total,q.id AS questionId
							FROM questions AS q
							INNER JOIN attempt_questions AS aq ON aq.questionId=q.id AND aq.check=2
							INNER JOIN `exam_attempts` AS ea ON ea.id=aq.attemptId
							WHERE ea.examId={$data->examId}
							AND ea.studentId IN ($examStudents)
							AND q.id IN ($questionsArray1)
							GROUP BY q.id
							ORDER BY FIELD(q.id,$questionsArray1)";
				$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$total = $total->toArray();
				$totalI=array();
				for ($i=0; $i < count($result->questions[0]); $i++) {
					$index = array_search($result->questions[0][$i]['id'], array_column($total, 'questionId'));
					if ($index>-1) {
						$totalI[] = $total[$index]['total'];
					} else {
						$totalI[] = 0;
					}
				}
				/*foreach($total as $key=>$iStudnets)
				{
					array_push($totalI,$iStudnets['total']);
				}*/
				$result->studentIncorrect=$totalI;
				
				//$select = "SELECT COUNT(id) AS total FROM attempt_questions WHERE questionId in($questionsArray1) AND `check`=1 group by questionId ORDER BY FIELD(questionId,$questionsArray1)";
				/*$select = "SELECT COUNT(aq.id) as total
							FROM questions AS q
							LEFT JOIN attempt_questions AS aq ON aq.questionId=q.id AND aq.check=1
							WHERE q.id IN ($questionsArray1)
							GROUP BY q.id
							ORDER BY FIELD(q.id,$questionsArray1)";*/
				$select = "SELECT COUNT(aq.id) as total,q.id AS questionId
							FROM questions AS q
							INNER JOIN attempt_questions AS aq ON aq.questionId=q.id AND aq.check=1
							INNER JOIN `exam_attempts` AS ea ON ea.id=aq.attemptId
							WHERE ea.examId={$data->examId}
							AND ea.studentId IN ($examStudents)
							AND q.id IN ($questionsArray1)
							GROUP BY q.id
							ORDER BY FIELD(q.id,$questionsArray1)";
				// print_r($select);
				$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$total = $total->toArray();
				$totalC=array();
				for ($i=0; $i < count($result->questions[0]); $i++) {
					$index = array_search($result->questions[0][$i]['id'], array_column($total, 'questionId'));
					if ($index>-1) {
						$totalC[] = $total[$index]['total'];
					} else {
						$totalC[] = 0;
					}
				}
				/*foreach($total as $key=>$cStudnets)
				{
					array_push($totalC,$cStudnets['total']);
				}*/
				
				// print_r($totalI);
				$result->studentCorrect=$totalC;
				//$result->Cquery=$select;
				
				//$select = "SELECT COUNT(id) AS total FROM attempt_questions WHERE questionId in($questionsArray1) AND (`status`=1 OR (`check`=0 AND `status`=3 )) group by questionId ORDER BY FIELD(questionId,$questionsArray1) ";
				/*$select = "SELECT COUNT(aq.id) as total
							FROM questions AS q
							LEFT JOIN attempt_questions AS aq ON aq.questionId=q.id AND (aq.status=1 OR (aq.check=0 AND aq.status=3 ))
							WHERE q.id IN ($questionsArray1)
							GROUP BY q.id
							ORDER BY FIELD(q.id,$questionsArray1)";*/
				$select = "SELECT COUNT(aq.id) as total,q.id AS questionId
							FROM questions AS q
							INNER JOIN attempt_questions AS aq ON aq.questionId=q.id AND (aq.status=1 OR (aq.check=0 AND aq.status=3 ))
							INNER JOIN `exam_attempts` AS ea ON ea.id=aq.attemptId
							WHERE ea.examId={$data->examId}
							AND ea.studentId IN ($examStudents)
							AND q.id IN ($questionsArray1)
							GROUP BY q.id
							ORDER BY FIELD(q.id,$questionsArray1)";
				$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$total = $total->toArray();
				$totalNA=array();
				for ($i=0; $i < count($result->questions[0]); $i++) {
					$index = array_search($result->questions[0][$i]['id'], array_column($total, 'questionId'));
					if ($index>-1) {
						$totalNA[] = $total[$index]['total'];
					} else {
						$totalNA[] = 0;
					}
				}
				/*foreach($total as $key=>$cStudnets)
				{
					array_push($totalNA,$cStudnets['total']);
				}*/
				
				// print_r($totalNA);
				$result->studentNA=$totalNA;
				//$result->NAquery=$select;
				
				
				//$select = "SELECT avg(`time`) AS avg FROM attempt_questions WHERE questionId in($questionsArray1) AND `check`=1 group by questionId ORDER BY FIELD(questionId,$questionsArray1) ";
				/*$select = "SELECT avg(aq.time) AS avg
							FROM questions AS q
							LEFT JOIN attempt_questions AS aq ON aq.questionId=q.id AND aq.check=1
							WHERE q.id IN ($questionsArray1)
							GROUP BY q.id
							ORDER BY FIELD(q.id,$questionsArray1)";*/
				$select = "SELECT avg(aq.time) AS avg,q.id AS questionId
							FROM questions AS q
							INNER JOIN attempt_questions AS aq ON aq.questionId=q.id AND aq.check=1
							INNER JOIN `exam_attempts` AS ea ON ea.id=aq.attemptId
							WHERE ea.examId={$data->examId}
							AND ea.studentId IN ($examStudents)
							AND q.id IN ($questionsArray1)
							GROUP BY q.id
							ORDER BY FIELD(q.id,$questionsArray1)";
				//var_dump($select);
				$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$total = $total->toArray();
				$totalavg=array();
				for ($i=0; $i < count($result->questions[0]); $i++) {
					$index = array_search($result->questions[0][$i]['id'], array_column($total, 'questionId'));
					if ($index>-1) {
						$totalavg[] = $total[$index]['avg'];
					} else {
						$totalavg[] = 0;
					}
				}
				/*foreach($total as $key=>$avgStudnets)
				{
					array_push($totalavg,$avgStudnets['avg']);
				}*/
				
				// print_r($totalI);
				
				$result->studentavg=$totalavg;
				
				//$select = "SELECT min(`time`) AS ft FROM attempt_questions WHERE questionId in($questionsArray1) AND `check`=1 group by questionId ORDER BY FIELD(questionId,$questionsArray1)";
				/*$select = "SELECT min(aq.time) AS ft
							FROM questions AS q
							LEFT JOIN attempt_questions AS aq ON aq.questionId=q.id AND aq.check=1
							WHERE q.id IN ($questionsArray1)
							GROUP BY q.id
							ORDER BY FIELD(q.id,$questionsArray1)";*/
				$select = "SELECT min(aq.time) AS ft,q.id AS questionId
							FROM questions AS q
							INNER JOIN attempt_questions AS aq ON aq.questionId=q.id AND aq.check=1
							INNER JOIN `exam_attempts` AS ea ON ea.id=aq.attemptId
							WHERE ea.examId={$data->examId}
							AND ea.studentId IN ($examStudents)
							AND q.id IN ($questionsArray1)
							GROUP BY q.id
							ORDER BY FIELD(q.id,$questionsArray1)";
				//var_dump($select);
				$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$total = $total->toArray();
				$totalfT=array();
				for ($i=0; $i < count($result->questions[0]); $i++) {
					$index = array_search($result->questions[0][$i]['id'], array_column($total, 'questionId'));
					if ($index>-1) {
						$totalfT[] = $total[$index]['ft'];
					} else {
						$totalfT[] = 0;
					}
				}
				/*foreach($total as $key=>$fTStudnets)
				{
					array_push($totalfT,$fTStudnets['ft']);
				}*/
				
				// print_r($totalI);
				
				$result->studentfT=$totalfT;
				
				// finding the attempt ids
				$select = "SELECT id FROM exam_attempts
							WHERE examId={$data->examId}
							AND studentId IN ($examStudents)
							AND completed =1";
				//var_dump($select);
				$attempt = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$attempt = $attempt->toArray();
				$attemptArray=array();
				foreach($attempt  as $key=>$value)
				{
					array_push($attemptArray,$value['id']);
				}
				$attemptArray=implode(",",$attemptArray);
				//finding toppers attempt id
				$select = "SELECT attemptId,sum(correct) as tmarks FROM `attempt_questions` where attemptId in ($attemptArray) and `check`=1 group by attemptId order by tmarks desc limit 1 ";
				/*$select = "SELECT attemptId,sum(correct) as tmarks
							FROM `attempt_questions` AS aq
							INNER JOIN `exam_attempts` AS ea ON ea.id=aq.attemptId
							WHERE ea.examId={$data->examId}
							AND ea.studentId IN ($examStudents)
							AND attemptId in ($attemptArray) AND `check`=1
							GROUP BY attemptId
							ORDER BY tmarks DESC
							LIMIT 1";*/
				//var_dump($select);
				$Topper = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$Topper = $Topper->toArray();
				$Topper=$Topper[0]['attemptId'];
				//print_r($Topper);
				//getting time for questions 
				//$select = "SELECT `time` AS ft FROM attempt_questions WHERE attemptId  = {$Topper} AND  questionId in($questionsArray1) AND `check`=1 group by questionId ORDER BY FIELD(questionId,$questionsArray1) ";
				$select = "SELECT IFNULL(aq.time,0) AS ft,q.id AS questionId
							FROM questions AS q
							LEFT JOIN attempt_questions AS aq ON aq.questionId=q.id AND aq.check=1 AND aq.attemptId = {$Topper}
							WHERE q.id IN ($questionsArray1)
							GROUP BY q.id
							ORDER BY FIELD(q.id,$questionsArray1)";
				//var_dump($select);
				$TopperM = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$TopperM = $TopperM->toArray();
				$totalTT=array();
				for ($i=0; $i < count($result->questions[0]); $i++) {
					$index = array_search($result->questions[0][$i]['id'], array_column($TopperM, 'questionId'));
					if ($index>-1) {
						$totalTT[] = $TopperM[$index]['ft'];
					} else {
						$totalTT[] = 0;
					}
				}
				/*foreach($TopperM as $key=>$TStudnets)
				{
					array_push($totalTT,$TStudnets['ft']);
				}*/
				$result->studentTT=$totalTT;
			
			
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}	

		public function getStudentTimeLineAll($data) {
			//print_r($data);
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				$select = "SELECT ss.userId
							FROM `exams` AS e
							INNER JOIN `student_subject` AS ss ON e.subjectId=ss.subjectId AND ss.Active=1
							INNER JOIN `login_details` AS ld ON ld.id=ss.userId AND ld.temp=0
							WHERE e.id={$data->examId}
							GROUP BY ss.userId";
				$students = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$students = $students->toArray();
				$examStudents = array();
				foreach ($students as $key => $value) {
					$examStudents[] = $value['userId'];
				}
				$examStudents = implode(",", $examStudents);

				//now average marks of class
				//$selectString = "SELECT max(score) score, endDate FROM exam_attempts WHERE examId= {$data->examId} and completed =1 GROUP BY studentId ORDER BY endDate";
				$selectString ="SELECT max(score) score, endDate
								FROM exam_attempts
								WHERE examId={$data->examId}
								AND studentId IN ($examStudents)
								AND completed =1
								GROUP BY studentId
								ORDER BY endDate";
				$avgScore = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$avgScore = $avgScore->toArray();
				$result->avgScore=$avgScore;
				
				//$selectString = "SELECT  score, endDate FROM exam_attempts WHERE examId= {$data->examId} and completed =1   ORDER BY endDate desc Limit 1";
				$selectString ="SELECT score, endDate
								FROM exam_attempts
								WHERE examId= {$data->examId}
								AND studentId IN ($examStudents)
								AND completed =1
								ORDER BY endDate DESC
								LIMIT 1";
				
				$lastday = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$lastday = $lastday->toArray();
				$result->lastday=$lastday;
				//print_r($result);
				$result->status=1;
				return $result;			
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//function to get exam attempts for graph creation
		public function getSubjectiveAttemptGraph($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subjective_exam_attempts');
				$where = array();
				if(isset($data->studentId))
					$where = array(
							'examId'	=>	$data->examId,
							'studentId'	=>	$data->studentId,
							'checked'	=>	1
							);
				else
					$where = array(
							'examId'	=>	$data->examId,
							'studentId'	=>	$data->userId,
							'checked'	=>	1
							);
				$select->where($where)->order('id ASC');
				$select->columns(array('score', 'percentage'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$graph = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$graph = $graph->toArray();
				//this is done according to ARC-333
				return $graph;
				/*$result->graph = $graph;
				$result->status = 1;
				return $result;*/
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function getsubjectiveTimeLine($data) {
			//print_r($data);
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				//now fetching user-self detail
				if (isset($data->student_id) && !empty($data->student_id)) {
					$studentId=$data->student_id;
				} else {
					$studentId=$data->userId;
				}
				$selectString = "SELECT endDate ,score FROM `subjective_exam_attempts` WHERE examId= {$data->examId} and studentId ={$studentId} and completed =1";
				$self = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$self = $self->toArray();
				$result->self=$self;
				//now average marks of class
				$selectString = "SELECT max(score) score, endDate FROM subjective_exam_attempts WHERE examId= {$data->examId} and completed =1 GROUP BY studentId ORDER BY endDate";
				$avgScore = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$avgScore = $avgScore->toArray();
				$result->avgScore=$avgScore;
				
				
				$selectString = "SELECT  score, endDate FROM subjective_exam_attempts WHERE examId= {$data->examId} and completed =1   ORDER BY endDate desc Limit 1";
				
				$lastday = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$lastday = $lastday->toArray();
				$result->lastday=$lastday;
				//print_r($result);
				$result->status=1;
				return $result;			
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function getSubjectiveTopperDetails($data) {
			//print_r($data);
			$result = new stdClass();
			try {
			
				$adapter = $this->adapter;

				$selectString ="SELECT sum(marks) as total
								FROM `questions_subjective` AS qs
								INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id
								WHERE examId={$data->examId} AND status = 1";
				$total = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$total = $total->toArray();
				//print_r($total[0]['total']);
				$totalMarks = 0;
				if (count($total)) {
					$totalMarks = $total[0]['total'];
				}

				$select = "SELECT AVG(score) AS average, MAX(score) AS max FROM `subjective_exam_attempts` WHERE examId={$data->examId} AND endDate!=0";
				$avgMax = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$avgMax = $avgMax->toArray();
				$result->avgMax = $avgMax[0];

				if ($totalMarks > 0) {
					$result->pavgMax = array(
										'average' => ($result->avgMax['average']/$totalMarks)*100,
										'max' => ($result->avgMax['max']/$totalMarks)*100
										);
				} else {
					$result->pavgMax = array(
										'average' => 0,
										'max' => 0
										);
				}

				//print_r($result);
				$select = "SELECT score from subjective_exam_attempts WHERE examId={$data->examId} AND id={$data->attemptId}";
				$self = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$self = $self->toArray();
				if($data->userRole == 1  || $data->userRole == 2  || $data->userRole == 5  ){
					$studentId=$data->studentId;
				}else{
					$studentId=$data->userId;
				}
				$select = "SELECT firstName , lastName FROM `student_details` WHERE userId={$studentId}";
				$selfName = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$selfName = $selfName->toArray();
				$self[0]['selfName']=$selfName;
				//print_r($select);
				//print_r($selfName);
				$result->self = $self[0];
				//print_r('$self');
				//print_r($result);
				//total attempts for median calculation
				$select = "SELECT COUNT(*) AS total FROM subjective_exam_attempts WHERE examId={$data->examId}";
				$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$total = $total->toArray();
				$total = $total[0]['total'];
				
				$limit = 0;
				//deciding the query string
				if($total%2 != 0) {
					$limit = (($total + 1) / 2) - 1;
					$select = "SELECT score FROM subjective_exam_attempts WHERE examId={$data->examId} ORDER BY score LIMIT {$limit}, 1";
				}else {
					$limit = ($total / 2) - 1;
					$select = "SELECT score FROM subjective_exam_attempts WHERE examId={$data->examId} ORDER BY score LIMIT {$limit}, 2";
				}
				//now calculating median score
				$median = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$median = $median->toArray();
				if($total%2 != 0) {
					$result->median = $median[0]['score'];
					$result->pmedian = ($median[0]['score']/$totalMarks)*100;
				}
				else {
					$result->median = ($median[0]['score'] + $median[1]['score'])/2;
					$result->pmedian = ( (($median[0]['score']/$totalMarks)*100) + (($median[1]['score']/$totalMarks)*100) )/2;
				}

				$select = "SELECT score from subjective_exam_attempts WHERE examId={$data->examId} AND id={$data->attemptId}";
				$self = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$self = $self->toArray();
				$result->pself['score'] = ($self[0]['score']/$totalMarks)*100;
				
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function getSubjectiveNormalizationGraph($data) {
			$result = new stdClass();
			try {
			
				$adapter = $this->adapter;
				$selectString = "SELECT max(score) score,concat(s.firstName,' ', s.lastName) name FROM `subjective_exam_attempts` e left join student_details s ON s.userId=e.studentId WHERE examId={$data->examId} AND completed = 1  group by e.studentId ";
				$graph = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$graph = $graph->toArray();
				if(!(count($graph)>0))
				{
					$result->status = 0;
					$result->message = "No student had completed the exam!";
					return $result;
				}
				$lowest=0;
				$highest=0;
				$lowest=$graph[0]['score'];
				foreach($graph as $key=>$value)
				{
					if($value['score']>$highest)
					{
						$highest=$value['score'];
					
					}	
					if($value['score']<$lowest)
					{
						$lowest=$value['score'];
					
					}					
					
				}
				$result->highest=$highest;
				$result->lowest=$lowest;
				$result->graph = $graph;
				
				$selectString ="SELECT sum(marks)as total
								FROM `questions_subjective` AS qs
								INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id
								WHERE qsel.examId={$data->examId} AND status = 1 AND deleted = 0";
				$total = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$total = $total->toArray();
				//print_r($total[0]['total']);
				$result->total = $total;
				
				
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function getSubjectiveMaxiumTimeForQuestion($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				//fetching average time for this question
				$select = "SELECT AVG(time) AS average FROM `subjective_attempt_questions` WHERE questionId={$data->questionId}";
				$avgMax = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$avgMax = $avgMax->toArray();
				$result->avg = $avgMax[0]['average'];
				
				//fetching self time
				$select = "SELECT `time`, `check` FROM subjective_attempt_questions WHERE questionId={$data->questionId} AND attemptId={$data->attemptId}";
				$self = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$self = $self->toArray();
				if($data->userRole == 1  || $data->userRole == 2  || $data->userRole == 5  ){
					$studentId=$data->studentId;
				}else{
					$studentId=$data->userId;
				}
				$select = "SELECT firstName , lastName FROM `student_details` WHERE userId={$studentId}";
				$selfName = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$selfName = $selfName->toArray();
				$self[0]['selfName']=$selfName;
				$result->self = $self[0];
				//SELECT firstName , lastName FROM `student_details` WHERE `userId`
				
				//now fetching the student count for new requirement
				$selectString = "SELECT count(Distinct studentId) AS total, questionId,
								ifnull(SUM(`check`=0),0) as unattempted,
								ifnull(SUM(`check`=1),0) as correct,
								ifnull(SUM(`check`=2),0) as wrong
								FROM subjective_attempt_questions saq
								JOIN subjective_exam_attempts sa ON sa.id=saq.attemptId
								WHERE questionId={$data->questionId}";
				//print_r($selectString);
				$total = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$total = $total->toArray();
				$result->studentCount = $total[0];
				
				//fetching topper's time on this question
				$selectString = "SELECT aq.`time` as `time`, aq.`check` as `check` from subjective_exam_attempts ea JOIN subjective_attempt_questions aq ON aq.questionId={$data->questionId} AND ea.id=aq.attemptId WHERE examId={$data->examId} ORDER BY score DESC LIMIT 1";
				$topper = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$topper = $topper->toArray();
				if(count($topper) > 0)
					$result->topper = $topper[0];
				else {
					$result->topper['time'] = 0;
					$result->topper['check'] = 0;
				}
				
				//fetching min time to answer correctly
				$select = $sql->select();
				$select->from('subjective_attempt_questions');
				$select->columns(array('time'	=>	new \Zend\Db\Sql\Expression("MIN(time)")));
				$select->where(array(
						'check'		=>	1,
						'questionId'=>	$data->questionId
				));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$min = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$min = $min->toArray();
				if(count($min) > 0)
					$result->min = $min[0]['time'];
				else
					$result->min = 0;
				
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		//getTopperDetailsAll
		public function getTopperDetailsAll($data) {
			//print_r($data);
			$result = new stdClass();
			try {

				$adapter = $this->adapter;

				if (isset($data->examId) && !empty($data->examId)) {
					$select = "SELECT ss.userId
								FROM `exams` AS e
								INNER JOIN `student_subject` AS ss ON e.subjectId=ss.subjectId AND ss.Active=1
								INNER JOIN `login_details` AS ld ON ld.id=ss.userId AND ld.temp=0
								/*INNER JOIN `exam_attempts` AS ea ON ea.examId=e.id AND ea.studentId=ld.id*/
								WHERE e.id={$data->examId}
								GROUP BY ss.userId";
					$students = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$students = $students->toArray();
					if (count($students) == 0) {
						$result->status = 0;
						$result->message = "No students in the exam!";
						return $result;
					}
					$examStudents = array();
					foreach ($students as $key => $value) {
						$examStudents[] = $value['userId'];
					}
					$examStudents = implode(",", $examStudents);

					//total attempts for median calculation
					$select = "SELECT COUNT(*) AS total
								FROM exam_attempts
								WHERE examId={$data->examId} AND completed=1
								AND studentId IN ($examStudents)";
					$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$total = $total->toArray();
					$total = $total[0]['total'];
					if ($total == 0) {
						$result->status = 0;
						$result->message = "No student had completed the exam!";
						return $result;
					}

					//$select = "SELECT AVG(score) AS average, MAX(score) AS max FROM `exam_attempts` WHERE examId={$data->examId} AND endDate!=0 AND completed=1";
					$select = "SELECT AVG(score) AS average, MAX(score) AS max
								FROM `exam_attempts`
								WHERE examId={$data->examId}
								AND studentId IN ($examStudents)
								AND endDate!=0 AND completed=1";
					$avgMax = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$avgMax = $avgMax->toArray();
					$result->avgMax = $avgMax[0];
					
					//print_r('$self');
					//print_r($result);
					
					$limit = 0;
					//deciding the query string
					if($total%2 != 0) {
						$limit = (($total + 1) / 2) - 1;
						//$select = "SELECT score FROM exam_attempts WHERE examId={$data->examId} ORDER BY score LIMIT {$limit}, 1";
						$select = "SELECT score
									FROM exam_attempts
									WHERE examId={$data->examId}
									AND studentId IN ($examStudents)
									ORDER BY score
									LIMIT {$limit}, 1";
					}else {
						$limit = ($total / 2) - 1;
						//$select = "SELECT score FROM exam_attempts WHERE examId={$data->examId} ORDER BY score LIMIT {$limit}, 2";
						$select = "SELECT score
									FROM exam_attempts
									WHERE examId={$data->examId}
									AND studentId IN ($examStudents)
									ORDER BY score
									LIMIT {$limit}, 2";
					}
					//now calculating median score
					$median = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$median = $median->toArray();
					if($total%2 != 0)
						$result->median = $median[0]['score'];
					else
						$result->median = ($median[0]['score'] + $median[1]['score'])/2;
					
					//$select = "SELECT AVG(percentage) AS average, MAX(percentage) AS max FROM `exam_attempts` WHERE examId={$data->examId} AND endDate!=0 AND completed=1";
					$select = "SELECT AVG(percentage) AS average, MAX(percentage) AS max
								FROM `exam_attempts`
								WHERE examId={$data->examId}
								AND studentId IN ($examStudents)
								AND endDate!=0 AND completed=1";
					$avgMax = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$avgMax = $avgMax->toArray();
					$result->pavgMax = $avgMax[0];
					
					
					
					$limit = 0;
					//deciding the query string
					if($total%2 != 0) {
						$limit = (($total + 1) / 2) - 1;
						//$select = "SELECT percentage FROM exam_attempts WHERE examId={$data->examId} ORDER BY percentage LIMIT {$limit}, 1";
						$select = "SELECT percentage
									FROM exam_attempts
									WHERE examId={$data->examId}
									AND studentId IN ($examStudents)
									ORDER BY percentage
									LIMIT {$limit}, 1";
					}else {
						$limit = ($total / 2) - 1;
						//$select = "SELECT percentage FROM exam_attempts WHERE examId={$data->examId} ORDER BY percentage LIMIT {$limit}, 2";
						$select = "SELECT percentage
									FROM exam_attempts
									WHERE examId={$data->examId}
									AND studentId IN ($examStudents)
									ORDER BY percentage
									LIMIT {$limit}, 2";
					}
					//now calculating median percentage
					$median = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$median = $median->toArray();
					if($total%2 != 0)
						$result->pmedian = $median[0]['percentage'];
					else
						$result->pmedian = ($median[0]['percentage'] + $median[1]['percentage'])/2;
					//print_r($result);
				} else if (isset($data->subjectiveExamId) && !empty($data->subjectiveExamId)) {
					$select = "SELECT ss.userId
								FROM `exam_subjective` AS e
								INNER JOIN `student_subject` AS ss ON e.subjectId=ss.subjectId AND ss.Active=1
								WHERE e.id={$data->subjectiveExamId}
								GROUP BY ss.userId";
					$students = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$students = $students->toArray();
					if (count($students) == 0) {
						$result->status = 0;
						$result->message = "No students in the exam!";
						return $result;
					}
					$examStudents = array();
					foreach ($students as $key => $value) {
						$examStudents[] = $value['userId'];
					}
					$examStudents = implode(",", $examStudents);
					
					//total attempts for median calculation
					$select = "SELECT COUNT(*) AS total
								FROM subjective_exam_attempts
								WHERE examId={$data->subjectiveExamId}
								AND studentId IN ($examStudents)";
					$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$total = $total->toArray();
					$total = $total[0]['total'];
					if ($total == 0) {
						$result->status = 0;
						$result->message = "No student had completed the exam!";
						return $result;
					}

					$selectString ="SELECT sum(marks) as total
									FROM `questions_subjective` AS qs
									INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id
									WHERE examId={$data->subjectiveExamId} AND status = 1";
					$totalMarksDB = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$totalMarksDB = $totalMarksDB->toArray();
					//print_r($total[0]['total']);
					$totalMarks = 0;
					if (count($totalMarksDB)) {
						$totalMarks = $totalMarksDB[0]['total'];
					}

					//$select = "SELECT AVG(score) AS average, MAX(score) AS max FROM `subjective_exam_attempts` WHERE examId={$data->subjectiveExamId} AND endDate!=0 AND completed=1";
					$select = "SELECT AVG(score) AS average, MAX(score) AS max
								FROM `subjective_exam_attempts`
								WHERE examId={$data->subjectiveExamId}
								AND studentId IN ($examStudents)
								AND endDate!=0 AND completed=1";
					$avgMax = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$avgMax = $avgMax->toArray();
					$result->avgMax = $avgMax[0];
					//print_r($result);
					
					//print_r('$self');
					//print_r($result);
					
					$limit = 0;
					//deciding the query string
					if($total%2 != 0) {
						$limit = (($total + 1) / 2) - 1;
						//$select = "SELECT score FROM subjective_exam_attempts WHERE examId={$data->subjectiveExamId} ORDER BY score LIMIT {$limit}, 1";
						$select = "SELECT score
									FROM subjective_exam_attempts
									WHERE examId={$data->subjectiveExamId}
									AND studentId IN ($examStudents)
									ORDER BY score
									LIMIT {$limit}, 1";
					}else {
						$limit = ($total / 2) - 1;
						//$select = "SELECT score FROM subjective_exam_attempts WHERE examId={$data->subjectiveExamId} ORDER BY score LIMIT {$limit}, 2";
						$select = "SELECT score
									FROM subjective_exam_attempts
									WHERE examId={$data->subjectiveExamId}
									AND studentId IN ($examStudents)
									ORDER BY score
									LIMIT {$limit}, 2";
					}
					//now calculating median score
					$median = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$median = $median->toArray();
					if($total%2 != 0) {
						$result->median = $median[0]['score'];
						$result->pmedian = ($median[0]['score']/$totalMarks)*100;
					}
					else {
						$result->median = ($median[0]['score'] + $median[1]['score'])/2;
						$result->pmedian = ( (($median[0]['score']/$totalMarks)*100) + (($median[1]['score']/$totalMarks)*100) )/2;
					}

					if ($totalMarks > 0) {
						$result->pavgMax = array(
											'average' => ($result->avgMax['average']/$totalMarks)*100,
											'max' => ($result->avgMax['max']/$totalMarks)*100
											);
					} else {
						$result->pavgMax = array(
											'average' => 0,
											'max' => 0
											);
					}
				} else if (isset($data->submissionId) && !empty($data->submissionId)) {
					require_once("Submission.php");
					$s = new Submission();
					$resAccess = $s->getSubmissionAccess($data->submissionId);
					if ($resAccess->status == 0) {
						$result->status = 0;
						$result->message = $result->status;
						return $result;
					}
					$access = $resAccess->access;
					if ($access == "public") {
						$select = "SELECT ss.userId
								FROM `submissions` AS e
								INNER JOIN `student_subject` AS ss ON e.subjectId=ss.subjectId AND ss.Active=1
								WHERE e.id={$data->submissionId}
								GROUP BY ss.userId";
						$students = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
						$students = $students->toArray();
						$examStudents = array();
						foreach ($students as $key => $value) {
							$examStudents[] = $value['userId'];
						}
						$examStudents = implode(",", $examStudents);
					} else {
						require_once("Subject.php");
						$sub = new Subject();
						$dataSG = new stdClass();
						$dataSG->examId = $data->submissionId;
						$resStudentGroups = $s->getSubmissionGroups($dataSG);
						if ($resStudentGroups->status == 0) {
							$result->status = 0;
							$result->message = $resStudentGroups->exception;
							return $result;
						}
						$studentGroups = $resStudentGroups->selectedStudentGroups;
						$arrStudents = array();
						foreach ($studentGroups as $key => $studentGroup) {
							$dataSG = new stdClass();
							$dataSG->studentGroupId = $studentGroup;
							$resStudents = $sub->getStudentsByStudentGroup($dataSG);
							if ($resStudents->status == 1) {
								foreach ($resStudents->students as $key => $student) {
									$arrStudents[] = $student['studentId'];
								}
							}
						}
						$examStudents = implode(",", $arrStudents);
					}

					$selectString ="SELECT sum(marks) as total
									FROM `submission_questions` AS qs
									INNER JOIN `submission_question_links` AS qsel ON qsel.questionId=qs.id
									WHERE examId={$data->submissionId} AND status = 1";
					$total = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$total = $total->toArray();
					//print_r($total[0]['total']);
					$totalMarks = 0;
					if (count($total)) {
						$totalMarks = $total[0]['total'];
					}

					/*$select = "SELECT AVG(score) AS average, MAX(score) AS max
								FROM `submission_attempts`
								WHERE examId={$data->submissionId}
								AND studentId IN ($examStudents)
								AND endDate!=0 AND completed=1";*/
					$select = "SELECT AVG(satm.marks) AS average, MAX(satm.marks) AS max
								FROM `submission_attempts` AS sa
								INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
								WHERE sa.examId={$data->submissionId}
								AND satm.studentId IN ($examStudents)
								AND sa.endDate!=0 AND sa.completed=1";
					$avgMax = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$avgMax = $avgMax->toArray();
					$result->avgMax = $avgMax[0];

					//total attempts for median calculation
					/*$select = "SELECT COUNT(*) AS total
								FROM submission_attempts
								WHERE examId={$data->submissionId}
								AND studentId IN ($examStudents)";*/
					$select = "SELECT COUNT(*) AS total
								FROM submission_attempts AS sa
								INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
								WHERE sa.examId={$data->submissionId}
								AND satm.studentId IN ($examStudents)";
					$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$total = $total->toArray();
					$total = $total[0]['total'];
					
					$limit = 0;
					//deciding the query string
					if($total%2 != 0) {
						$limit = (($total + 1) / 2) - 1;
						/*$select = "SELECT score
									FROM submission_attempts
									WHERE examId={$data->submissionId}
									AND studentId IN ($examStudents)
									ORDER BY score
									LIMIT {$limit}, 1";*/
						$select = "SELECT satm.marks AS score
									FROM submission_attempts AS sa
									INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
									WHERE sa.examId={$data->submissionId}
									AND satm.studentId IN ($examStudents)
									ORDER BY satm.marks
									LIMIT {$limit}, 1";
					}else {
						$limit = ($total / 2) - 1;
						/*$select = "SELECT score
									FROM submission_attempts
									WHERE examId={$data->submissionId}
									AND studentId IN ($examStudents)
									ORDER BY score
									LIMIT {$limit}, 2";*/
						$select = "SELECT satm.marks AS score
									FROM submission_attempts AS sa
									INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
									WHERE sa.examId={$data->submissionId}
									AND satm.studentId IN ($examStudents)
									ORDER BY satm.marks
									LIMIT {$limit}, 2";
					}
					//now calculating median score
					$median = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$median = $median->toArray();
					if($total%2 != 0) {
						$result->median = $median[0]['score'];
						$result->pmedian = ($median[0]['score']/$totalMarks)*100;
					}
					else {
						$result->median = ($median[0]['score'] + $median[1]['score'])/2;
						$result->pmedian = ( (($median[0]['score']/$totalMarks)*100) + (($median[1]['score']/$totalMarks)*100) )/2;
					}

					if ($totalMarks > 0) {
						$result->pavgMax = array(
											'average' => ($result->avgMax['average']/$totalMarks)*100,
											'max' => ($result->avgMax['max']/$totalMarks)*100
											);
					} else {
						$result->pavgMax = array(
											'average' => 0,
											'max' => 0
											);
					}
				}
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		//getCompleteQuestionAnalysis
		public function getCompleteQuestionAnalysis($data) {
			//print_r($data);
			$result = new stdClass();
			try {

				$adapter = $this->adapter;

				$select = "SELECT ss.userId
							FROM `exams` AS e
							INNER JOIN `student_subject` AS ss ON e.subjectId=ss.subjectId AND ss.Active=1
							WHERE e.id={$data->examId}
							GROUP BY ss.userId";
				$students = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$students = $students->toArray();
				$examStudents = array();
				foreach ($students as $key => $value) {
					$examStudents[] = $value['userId'];
				}
				$examStudents = implode(",", $examStudents);
			
				$studentId=array();
				$studentNames=array();
				$quesTimesArray=array();
				$correct=array();
				$inCorrect=array();
				$minTime=array();
				$avgTime=array();

				//$select = "SELECT count(aq.id) as qTime,ea.studentId as studentId FROM `attempt_questions` aq left join exam_attempts ea ON aq.attemptId=ea.id where aq.questionId ={$data->quesId} and ea.examId={$data->examId} AND ea.endDate!=0 /* AND ea.completed=1*/ AND (aq.check=1 OR aq.check=2 OR (aq.status=1 OR (aq.check=0 AND aq.status=3 ))) group by studentId";
				$select = "SELECT count(aq.id) as qTime,ea.studentId as studentId
							FROM `attempt_questions` aq
							LEFT JOIN `exam_attempts` ea ON aq.attemptId=ea.id AND ea.studentId IN ($examStudents)
							WHERE ea.examId={$data->examId}
							AND aq.questionId ={$data->quesId} AND ea.endDate!=0 AND (aq.check=1 OR aq.check=2 OR (aq.status=1 OR (aq.check=0 AND aq.status=3 )))
							GROUP BY studentId";
				$quesTimes = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$quesTimes = $quesTimes->toArray();
				//print_r($quesTimes);
				foreach($quesTimes as $key => $value)
				{
					array_push($studentId,$value['studentId']);
					array_push($quesTimesArray,$value['qTime']);
				}
				
				$studentId1=implode(",",$studentId);
				$selectString = "SELECT concat(firstName,' ',lastName) as name,userId from student_details where userId in ($studentId1) ORDER BY FIELD(userId,$studentId1)";
				$students = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$students = $students->toArray();
				foreach($students as $key => $value)
				{
					array_push($studentNames,$value['name']);
				}
				$adapter = $this->adapter;
				$select = "SELECT count(aq.id) as correct,ea.studentId as studentId FROM `attempt_questions` aq left join exam_attempts ea ON aq.attemptId=ea.id where aq.questionId ={$data->quesId} and ea.examId={$data->examId} AND ea.endDate!=0 /* AND ea.completed=1*/ AND aq.check=1 group by studentId";
				$corrects = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$corrects = $corrects->toArray();
				$correct= array_fill(0, count($studentId), 0);
				foreach($corrects as $key => $value)
				{
					$index=array_search($value['studentId'],$studentId);
					if($index<0){
						$correct[$index]=0;
					}else{
						$correct[$index]=$value['correct'];
					}
					//array_push($studentNames,$value['correct']);
				}
				$adapter = $this->adapter;
				$select = "SELECT count(aq.id) as incorrect,ea.studentId as studentId FROM `attempt_questions` aq left join exam_attempts ea ON aq.attemptId=ea.id where aq.questionId ={$data->quesId} and ea.examId={$data->examId} AND ea.endDate!=0 /* AND ea.completed=1*/ AND aq.check=2 group by studentId";
				$inCorrects = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$inCorrects = $inCorrects->toArray();
				$inCorrect= array_fill(0, count($studentId), 0);
				foreach($inCorrects as $key => $value)
				{
					$index=array_search($value['studentId'],$studentId);
					if($index<0){
						$inCorrect[$index]=0;
					}else{
						$inCorrect[$index]=$value['incorrect'];
					}
					//array_push($studentNames,$value['correct']);
				}
				//print_r($inCorrect);
				$adapter = $this->adapter;
				$select = "SELECT count(aq.id) as incorrect,ea.studentId as studentId FROM `attempt_questions` aq left join exam_attempts ea ON aq.attemptId=ea.id where aq.questionId ={$data->quesId} and ea.examId={$data->examId} AND ea.endDate!=0 /* AND ea.completed=1*/ AND (aq.status=1 OR (aq.check=0 AND aq.status=3 )) GROUP BY studentId";
				$unAttempts = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$unAttempts = $unAttempts->toArray();
				$unAttempted= array_fill(0, count($studentId), 0);
				foreach($unAttempts as $key => $value)
				{
					$index=array_search($value['studentId'],$studentId);
					if($index<0){
						$unAttempted[$index]=0;
					}else{
						$unAttempted[$index]=$value['incorrect'];
					}
					//array_push($studentNames,$value['correct']);
				}
				$adapter = $this->adapter;
				$select = "SELECT min(aq.time) as fastestTime,ea.studentId as studentId FROM `attempt_questions` aq left join exam_attempts ea ON aq.attemptId=ea.id where aq.questionId ={$data->quesId} and ea.examId={$data->examId} AND ea.endDate!=0 /* AND ea.completed=1*/ AND aq.check=1 group by studentId";
				$minTimes = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$minTimes = $minTimes->toArray();
				$minTime= array_fill(0, count($studentId), 0);
				foreach($minTimes as $key => $value)
				{
					$index=array_search($value['studentId'],$studentId);
					if($index<0){
						$minTime[$index]=0;
					}else{
						$minTime[$index]=$value['fastestTime'];
					}
					//array_push($studentNames,$value['correct']);
				}
				
				$adapter = $this->adapter;
				$select = "SELECT avg(aq.time) as avgTime,ea.studentId as studentId FROM `attempt_questions` aq left join exam_attempts ea ON aq.attemptId=ea.id where aq.questionId ={$data->quesId} and ea.examId={$data->examId} AND ea.endDate!=0 /* AND ea.completed=1*/ AND aq.check=1 group by studentId";
				$avgTimes = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$avgTimes = $avgTimes->toArray();
				$avgTime= array_fill(0, count($studentId), 0);
				foreach($avgTimes as $key => $value)
				{
					$index=array_search($value['studentId'],$studentId);
					if($index<0){
						$avgTime[$index]=0;
					}else{
						$avgTime[$index]=$value['avgTime'];
					}
					//array_push($studentNames,$value['correct']);
				}
				
				//print_r($avgTime);
				$result->avgTime=$avgTime;
				$result->minTime=$minTime;
				$result->inCorrect=$inCorrect;
				$result->correct=$correct;
				$result->unAttempted=$unAttempted;
				$result->quesTimes = $quesTimesArray;
				$result->studentNames = $studentNames;
				$result->studentId=$studentId;
				//print_r($result);
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getSubmissionTimeLine($data) {
			//print_r($data);
			$result = new stdClass();
			try{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				//now fetching user-self detail
				if (isset($data->student_id) && !empty($data->student_id)) {
					$studentId=$data->student_id;
				} else {
					$studentId=$data->userId;
				}
				/*$selectString = "SELECT endDate, score FROM `submission_attempts` WHERE examId= {$data->examId} and studentId ={$studentId} and completed =1";*/
				$selectString = "SELECT sa.endDate, satm.marks AS score
				FROM `submission_attempts` AS sa
				INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
				WHERE sa.examId= {$data->examId} and satm.studentId ={$studentId} and completed =1";
				$self = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$self = $self->toArray();
				$result->self=$self;
				//now average marks of class
				/*$selectString = "SELECT max(score) score, endDate FROM submission_attempts WHERE examId= {$data->examId} and completed =1 GROUP BY studentId ORDER BY endDate";*/
				$selectString = "SELECT max(satm.marks) score, sa.endDate
				FROM submission_attempts AS sa
				INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
				WHERE sa.examId= {$data->examId} and sa.completed =1
				GROUP BY satm.studentId
				ORDER BY sa.endDate";
				$avgScore = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$avgScore = $avgScore->toArray();
				$result->avgScore=$avgScore;			
				
				/*$selectString = "SELECT  score, endDate FROM submission_attempts WHERE examId= {$data->examId} and completed =1   ORDER BY endDate desc Limit 1";*/
				$selectString = "SELECT satm.marks AS score, sa.endDate
				FROM submission_attempts AS sa
				INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
				WHERE sa.examId={$data->examId} and sa.completed=1
				ORDER BY sa.endDate desc Limit 1";
				
				$lastday = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$lastday = $lastday->toArray();
				$result->lastday=$lastday;
				//print_r($result);
				$result->status=1;
				return $result;			
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getSubmissionTopperDetails($data) {
			//print_r($data);
			$result = new stdClass();
			try {
			
				$adapter = $this->adapter;

				$selectString ="SELECT sum(marks) as total
								FROM `submission_questions` AS qs
								INNER JOIN `submission_question_links` AS qsel ON qsel.questionId=qs.id
								WHERE examId={$data->examId} AND status = 1";
				$total = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$total = $total->toArray();
				//print_r($total[0]['total']);
				$totalMarks = 0;
				if (count($total)) {
					$totalMarks = $total[0]['total'];
				}

				/*$select = "SELECT AVG(score) AS average, MAX(score) AS max FROM `submission_attempts` WHERE examId={$data->examId} AND endDate!=0";*/
				$select = "SELECT AVG(satm.marks) AS average, MAX(satm.marks) AS max
				FROM `submission_attempts` AS sa
				INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
				WHERE sa.examId={$data->examId} AND sa.endDate!=0";
				$avgMax = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$avgMax = $avgMax->toArray();
				$result->avgMax = $avgMax[0];

				if ($totalMarks > 0) {
					$result->pavgMax = array(
										'average' => ($result->avgMax['average']/$totalMarks)*100,
										'max' => ($result->avgMax['max']/$totalMarks)*100
										);
				} else {
					$result->pavgMax = array(
										'average' => 0,
										'max' => 0
										);
				}

				//print_r($result);
				/*$select = "SELECT score from submission_attempts WHERE examId={$data->examId} AND id={$data->attemptId}";
				$self = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$self = $self->toArray();*/
				if($data->userRole == 1  || $data->userRole == 2  || $data->userRole == 5  ){
					$studentId=$data->studentId;
				}else{
					$studentId=$data->userId;
				}
				/* Moved this query down by Jackie - STARTS */
				$select = "SELECT satm.marks AS score
				FROM submission_attempts AS sa
				INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
				WHERE sa.examId={$data->examId} AND sa.id={$data->attemptId} AND satm.studentId={$studentId}";
				$self = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$self = $self->toArray();
				/* Moved this query down by Jackie - ENDS */
				$select = "SELECT firstName, lastName FROM `student_details` WHERE userId={$studentId}";
				$selfName = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$selfName = $selfName->toArray();
				$self[0]['selfName']=$selfName;
				//print_r($select);
				//print_r($selfName);
				$result->self = $self[0];
				//print_r('$self');
				//print_r($result);
				//total attempts for median calculation
				/*$select = "SELECT COUNT(*) AS total FROM submission_attempts WHERE examId={$data->examId}";*/
				$select = "SELECT COUNT(*) AS total
				FROM submission_attempts AS sa
				INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
				WHERE sa.examId={$data->examId}";
				$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$total = $total->toArray();
				$total = $total[0]['total'];
				
				$limit = 0;
				//deciding the query string
				if($total%2 != 0) {
					$limit = (($total + 1) / 2) - 1;
					$select = "SELECT satm.marks AS score
					FROM submission_attempts AS sa
					INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
					WHERE sa.examId={$data->examId}
					ORDER BY satm.marks LIMIT {$limit}, 1";
				}else {
					$limit = ($total / 2) - 1;
					$select = "SELECT satm.marks AS score
					FROM submission_attempts AS sa
					INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
					WHERE sa.examId={$data->examId}
					ORDER BY satm.marks LIMIT {$limit}, 2";
				}
				//now calculating median score
				$median = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$median = $median->toArray();
				if($total%2 != 0) {
					$result->median = $median[0]['score'];
					$result->pmedian = ($median[0]['score']/$totalMarks)*100;
				}
				else {
					$result->median = ($median[0]['score'] + $median[1]['score'])/2;
					$result->pmedian = ( (($median[0]['score']/$totalMarks)*100) + (($median[1]['score']/$totalMarks)*100) )/2;
				}

				/*$select = "SELECT score from submission_attempts WHERE examId={$data->examId} AND id={$data->attemptId}";*/
				$select = "SELECT satm.marks AS score
				FROM submission_attempts AS sa
				INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
				WHERE sa.examId={$data->examId} AND sa.id={$data->attemptId} AND satm.studentId={$studentId}";
				$self = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
				$self = $self->toArray();
				$result->pself['score'] = ($self[0]['score']/$totalMarks)*100;
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getSubmissionNormalizationGraph($data) {
			$result = new stdClass();
			try {
			
				$adapter = $this->adapter;
				/*$selectString = "SELECT max(score) score,concat(s.firstName,' ', s.lastName) name FROM `submission_attempts` e left join student_details s ON s.userId=e.studentId WHERE examId={$data->examId} AND completed = 1  group by e.studentId ";*/
				$selectString = "SELECT max(satm.marks) score,concat(s.firstName,' ', s.lastName) name
				FROM `submission_attempts` AS sa
				INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
				LEFT JOIN student_details AS s ON s.userId=satm.studentId
				WHERE sa.examId={$data->examId} AND sa.completed = 1
				GROUP BY satm.studentId";
				$graph = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$graph = $graph->toArray();
				if(!(count($graph)>0))
				{
					$result->status = 0;
					$result->message = "No student had completed the exam!";
					return $result;
				}
				$lowest=0;
				$highest=0;
				$lowest=$graph[0]['score'];
				foreach($graph as $key=>$value)
				{
					if($value['score']>$highest)
					{
						$highest=$value['score'];
					
					}	
					if($value['score']<$lowest)
					{
						$lowest=$value['score'];
					
					}					
					
				}
				$result->highest=$highest;
				$result->lowest=$lowest;
				$result->graph = $graph;
				
				$selectString ="SELECT sum(marks)as total
								FROM `submission_questions` AS qs
								INNER JOIN `submission_question_links` AS qsel ON qsel.questionId=qs.id
								WHERE qsel.examId={$data->examId} AND status = 1 AND deleted = 0";
				$total = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$total = $total->toArray();
				//print_r($total[0]['total']);
				$result->total = $total;
				
				
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
	}
?>