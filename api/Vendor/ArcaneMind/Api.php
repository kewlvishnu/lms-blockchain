<?php
	
	require_once "User.php";
	require_once "global.php";
	
	class Api {
		public static function login($req) {
			$req->hashedPassword = sha1($req->password);
			$u = new User();
			$res = $u->login($req);
			return $res;
		}
		public static function googleLogin($req) {
			$u = new User();
			$res = $u->googleLogin($req);
			return $res;
		}
		public static function googleRoleLogin($req) {
			$u = new User();
			$res = $u->googleRoleLogin($req);
			return $res;
		}
		public static function facebookLogin($req) {
			$u = new User();
			$res = $u->facebookLogin($req);
			return $res;
		}
		public static function portfolioLogin($req) {
			$req->hashedPassword = sha1($req->password);
			require_once 'Portfolio.php';
			$u = new Portfolio();
			$res = $u->portfolioLogin($req);
			return $res;
		}
		public static function parentLogin($req) {
			$req->hashedPassword = base64_encode($req->password);
			$u = new User();
			$res = $u->parentLogin($req);
			return $res;
		}
		public static function backLogin($req) {
			require_once 'User.php';
			$req->hashedPassword = sha1($req->password);
			$req->userRole = '5';
			$u = new User();
			$res = $u->login($req);
			return $res;
		}
		public static function checkUserRemembered($cookie) {
			$u = new User();
			$res = $u->checkUserRemembered($cookie);
			return $res;
		}
		public static function resendVerificationLink($req) {
			require_once 'User.php';
			$u = new User();
			$res = $u->resendVerificationLink($req);
			return $res;
		}
		
		public static function sendResetPasswordLink($req) {
			require_once 'User.php';
			$u = new User();
			$req->createdAt = time();
			$res = $u->sendResetPasswordLink($req);
			return $res;
		}
		
		public static function resetPassword($req) {
			$req->hashedPassword = sha1($req->password);
			$u = new User();
			$res = $u->resetPassword($req);
			return $res;
		}
		public static function trackUser() {
			require_once "UserTracking.php";
			$ut = new UserTracking();
			$data = $ut->saveUserTrackinDetails();
			return $data;
		}
		public static function getVerifiedStatus($req) {
			require_once "User.php";
			$ut = new User();
			$data = $ut->getVerifiedStatus($req);
			return $data;
		}
		public static function registerUserShort($req) {
			switch($req->userType) {
				case 'institute':
					require_once "Institute.php";
					$req->hashedPassword = sha1($req->password);
					$req->createdAt = time();
					//$req->contactFirstName = $req->firstName;
					//$req->contactLastName = $req->lastName;
					$req->role = 1;
					$i = new Institute();
					$res = $i->registerShort($req);
					return $res;
					break;
					
				case 'professor':
					require_once "Professor.php";
					$req->hashedPassword = sha1($req->password);
					$req->createdAt = time();
					$req->dob = time();
					$req->role = 2;
					$p = new Professor();
					$res = $p->registerShort($req);
					return $res;
					break;
				
				case 'student':
					require_once "Student.php";
					$req->hashedPassword = sha1($req->password);
					$req->createdAt = time();
					$req->gender = 1;
					$req->dob = '';
					$req->role = 4;
					$s = new Student();
					$res = $s->registerShort($req);
					//print_r($res);
					return $res;
					break;
			}
		}
		public static function registerUserSocial($req) {
			switch($req->userType) {
				case 'institute':
					require_once "Institute.php";
					$req->hashedPassword = sha1($req->password);
					$req->createdAt = time();
					//$req->contactFirstName = $req->firstName;
					//$req->contactLastName = $req->lastName;
					$req->role = 1;
					$i = new Institute();
					$res = $i->registerSocial($req);
					return $res;
					break;
					
				case 'professor':
					require_once "Professor.php";
					$req->hashedPassword = sha1($req->password);
					$req->createdAt = time();
					$req->dob = time();
					$req->role = 2;
					$p = new Professor();
					$res = $p->registerSocial($req);
					return $res;
					break;
				
				case 'student':
					require_once "Student.php";
					$req->hashedPassword = sha1($req->password);
					$req->createdAt = time();
					$req->gender = 1;
					$req->dob = '';
					$req->role = 4;
					$s = new Student();
					$res = $s->registerSocial($req);
					//print_r($res);
					return $res;
					break;
			}
		}
		
		public static function registerUser($req) {
			switch($req->userType) {
				case 'institute':
					require_once "Institute.php";
					$req->hashedPassword = sha1($req->password);
					$req->createdAt = time();
					$req->contactFirstName = $req->firstName;
					$req->contactLastName = $req->lastName;
					$req->role = 1;
					$i = new Institute();
					$res = $i->register($req);
					return $res;
					break;
					
				case 'professor':
					require_once "Professor.php";
					$req->hashedPassword = sha1($req->password);
					$req->createdAt = time();
					$req->dob = time();
					$req->role = 2;
					$p = new Professor();
					$res = $p->register($req);
					return $res;
					break;
				
				case 'publisher':
					require_once "Publisher.php";
					$req->hashedPassword = sha1($req->password);
					$req->createdAt = time();
					$req->dob = time();
					$req->role = 3;
					$p = new Publisher();
					$res = $p->register($req);
					return $res;
					break;
				
				case 'student':
					require_once "Student.php";
					$req->hashedPassword = sha1($req->password);
					$req->createdAt = time();
					$req->gender = 1;
					$req->dob = '';
					$req->role = 4;
					$s = new Student();
					$res = $s->register($req);
					return $res;
					break;
			}
		}
		
		public static function validateEmail($req) {
			$u = new User();
			$res = $u->validateEmail($req);
			return $res;
		}
		
		public static function validateUsername($req) {
			$u = new User();
			$res = $u->validateUsername($req);
			return $res;
			
		}
		
		public static function validateAccount($req) {
			$u = new User();
			$res = $u->validateAccount($req);
			return $res;
		}
		
		public static function verifyUserID($req) {
			$u = new User();
			$res = $u->verifyUserID($req);
			return $res;
			
		}

		public static function getNotificationCount($req) {
			$u = new User();
			$res = $u->getNotificationCount($req);
			return $res;
		}

		public static function getPortfolioBySlug($slug) {
			$u = new User();
			$userId = 0;
			if(isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
				$userId = $_SESSION['userId'];
			}
			$data = $u->getPortfolioBySlug($slug,$userId);
			if($data->valid == TRUE) {
				switch($data->userRole) {
					case 1:
						require_once "Institute.php";
						$i = new Institute();
						$res = $i->getProfileDetails($data);
						$res->portfolio = $data->portfolio;
						$res->listStatus = $data->portfolioStatus;
						$res->owner = $data->owner;
						$res->valid = true;
						$res->userRole = 1;
						return $res;
						break;
					case 2:
						require_once "Professor.php";
						$p = new Professor();
						$res = $p->getProfileDetails($data);
						$res->portfolio = $data->portfolio;
						$res->listStatus = $data->portfolioStatus;
						$res->owner = $data->owner;
						$res->valid = true;
						$res->userRole = 2;
						return $res;
						break;
					case 3:
						require_once "Publisher.php";
						$p = new Publisher();
						$res = $p->getProfileDetails($data);
						$res->portfolio = $data->portfolio;
						$res->listStatus = $data->portfolioStatus;
						$res->owner = $data->owner;
						$res->valid = true;
						$res->userRole = 3;
						return $res;
						break;
				}
			} else {
				return $data;	
			}
		}
		public static function getUserBySlug($slug) {
			$u = new User();
			$userId = 0;
			if(isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
				$userId = $_SESSION['userId'];
			}
			$data = $u->getUserBySlug($slug,$userId);
			if($data->valid == TRUE) {
				switch($data->userRole) {
					case 1:
						require_once "Institute.php";
						$i = new Institute();
						$res = $i->getProfileDetails($data);
						$res->valid = true;
						$res->userRole = 1;
						return $res;
						break;
					case 2:
						require_once "Professor.php";
						$p = new Professor();
						$res = $p->getProfileDetails($data);
						$res->valid = true;
						$res->userRole = 2;
						return $res;
						break;
					case 3:
						require_once "Publisher.php";
						$p = new Publisher();
						$res = $p->getProfileDetails($data);
						$res->valid = true;
						$res->userRole = 3;
						return $res;
						break;
				}
			} else {
				return $data;
			}
		}
		public static function getPortfolioCourses($data) {
			$u = new User();
			$data->userId = 0;
			if(isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
				$data->userId = $_SESSION['userId'];
			}
			$data = $u->getPortfolioCourses($data);
			return $data;
		}
		public static function getPortfolioDetail($slug) {
			$u = new User();
			$data = new stdClass();
			$data->slug = $slug;
			$data->userId = 0;
			if(isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
				$data->userId = $_SESSION['userId'];
			}
			$data = $u->getPortfolioDetail($data);
			return $data;
		}
		public static function getPortfolioApprovals() {
			require_once "Portfolio.php";
			$p = new Portfolio();
			$res = $p->getPortfolioApprovals();
			return $res;
		}
		public static function savePortfolioApproval($data) {
			require_once "Portfolio.php";
			$p = new Portfolio();
			$res = $p->savePortfolioApproval($data);
			return $res;
		}
		public static function updateDurationsForVideos() {
			require_once "Course.php";
			$c = new Course();
			$res = $c->updateDurationsForVideos();
			return $res;
		}
		//	
		public static function setContentWatched($req) {
			require_once "Course.php";
			$c = new Course();
			$res = $c->setContentWatched($req);
			return $res;
		}
		public static function getProfileDetails($req) {
			switch($req->userRole) {
				case 1:
					require_once "Institute.php";
					$i = new Institute();
					$res = $i->getProfileDetails($req);
					return $res;
					break;
				case 2:
					require_once "Professor.php";
					$p = new Professor();
					$res = $p->getProfileDetails($req);
					return $res;
					break;
				case 3:
					require_once "Publisher.php";
					$p = new Publisher();
					$res = $p->getProfileDetails($req);
					return $res;
					break;
			}
		}
		public static function getProfileStats($req) {
			switch($req->userRole) {
				case 1:
					require_once "Institute.php";
					$i = new Institute();
					$res = $i->getProfileStats($req);
					return $res;
					break;
				case 2:
					require_once "Professor.php";
					$p = new Professor();
					$res = $p->getProfileStats($req);
					return $res;
					break;
				case 3:
					require_once "Publisher.php";
					$p = new Publisher();
					$res = $p->getProfileStats($req);
					return $res;
					break;
			}
		}
		
		public static function saveTagline($req) {
			require_once "Institute.php";
			$i = new Institute();
			$res = $i->updateTagLine($req);
			return $res;
		}
	
		public static function saveUserImage($imagePath, $imageType, $userId) {
			$u = new User();
			$result = '';
			$userRole = $u->getRoleId($userId);
			$userRole = $userRole['roleId'];
			if($userRole == 0)
				return "user not found";
			switch($imageType) {
				case 'cover':
					if($userRole == '1'){
						require_once "Institute.php";
						$i = new Institute();
						$result = $i->saveCoverImage($imagePath, $userId);
					} else if($userRole == '2') {
						require_once "Professor.php";
						$p = new Professor();
						$result = $p->saveCoverImage($imagePath, $userId);
						
					} else if($userRole == '3'){	
						require_once "Publisher.php";
						$p = new Publisher();
						$result = $p->saveCoverImage($imagePath, $userId);
					}
					else if($userRole == '4'){	
						require_once "Student.php";
						$s = new Student();
						$result = $s->saveCoverImage($imagePath, $userId);
					}
					break;
				case 'profile':
					if($userRole == '1'){
						require_once "Institute.php";
						$i = new Institute();
						$result = $i->saveProfileImage($imagePath, $userId);
					} else if($userRole == '2'){
						require_once "Professor.php";
						$p = new Professor();
						$result = $p->saveProfileImage($imagePath, $userId);
						
					} else if($userRole == '3'){	
						require_once "Publisher.php";
						$p = new Publisher();
						$result = $p->saveProfileImage($imagePath, $userId);
					}
					else if($userRole == '4'){	
						require_once "Student.php";
						$s = new Student();
						$result = $s->saveProfileImage($imagePath, $userId);
					}
					break;
			}
			return $result;
		}

		public static function savePortfolioImage($imagePath, $subdomain, $userId) {
			$u = new User();
			$result = '';
			$userRole = $u->getRoleId($userId);
			$userRole = $userRole['roleId'];
			if($userRole == 0)
				return "user not found";
			if($userRole == '1'){
				require_once "Institute.php";
				$i = new Institute();
				$result = $i->savePortfolioImage($imagePath, $subdomain, $userId);
			} else if($userRole == '2'){
				require_once "Professor.php";
				$p = new Professor();
				$result = $p->savePortfolioImage($imagePath, $subdomain, $userId);
				
			} else if($userRole == '3'){	
				require_once "Publisher.php";
				$p = new Publisher();
				$result = $p->savePortfolioImage($imagePath, $subdomain, $userId);
			}
			else if($userRole == '4'){	
				require_once "Student.php";
				$s = new Student();
				$result = $s->savePortfolioImage($imagePath, $subdomain, $userId);
			}
			return $result;
		}
	
		public static function updateProfileDescription($req){
			if($req->userRole == '1') {
				require_once "Institute.php";
				$i = new Institute();
				$result = $i->updateProfileDescription($req);
			} else if($req->userRole == '2') {
				require_once "Professor.php";
				$p = new Professor();
				$result = $p->updateProfileDescription($req);
			} else if($req->userRole == '3') {
				require_once "Publisher.php";
				$p = new Publisher();
				$result = $p->updateProfileDescription($req);
			}
			return $result;
		}

		public static function updateProfileGeneral($req){
			if($req->userRole == '1') {
				require_once "Institute.php";
				$i = new Institute();
				$result = $i->updateProfileGeneral($req);
			} else if($req->userRole == '2') {
				require_once "Professor.php";
				$p = new Professor();
				$result = $p->updateProfileGeneral($req);
			} else if($req->userRole == '3') {
				require_once "Publisher.php";
				$p = new Publisher();
				$result = $p->updateProfileGeneral($req);
			}
			return $result;
		}
		
		public static function userHasRoles($roles) {
			session_start();
			foreach($roles as $roleId) {
				if(API::userHasRole($roleId) == true)
					return true;
			}
			return false;
		}
		public static function userHasRole($roleId){
			if(!isset($_SESSION['userId']))
				return false;
			$userRole = $_SESSION['userRole'];
			if($userRole == $roleId)
				return true;
			return false;
		}
		
		public static function updateUserInstituteType($req){
			if($req->userRole == '1') {
				require_once "Institute.php";
				$i = new Institute();
				$result = $i->updateUserInstituteType($req);
			} else if($req->userRole == '2') {
				require_once "Professor.php";
				$p = new Professor();
				$result = $p->updateUserInstituteType($req);
			} else if($req->userRole == '3') {
				require_once "Publisher.php";
				$p = new Publisher();
				$result = $p->updateUserInstituteType($req);
			}
			return $result;
		}
				
		public static function getFeaturedTemplateCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getFeaturedTemplateCourse();
			return $result;
		}
		
		public static function getTemplateCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getTemplateCourse($req);
			return $result;
		}
		
		public static function searchCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->searchCourse($req);
			return $result;
		}
		public static function searchCourseForPage($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->searchCourseForPage($req);
			return $result;
		}
		
		public static function getStudentTemplateCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getStudentTemplateCourse();
			return $result;
		}
		public static function getStudentFreeTemplateCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getStudentFreeCourse();
			return $result;
		}
		
		//@ayush
		public static function getStudentEntranceExamTemplateCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getStudentEntranceExamTemplateCourse();
			return $result;
		}
		
	//getStudentschoolPrepTemplateCourse
	
		public static function getStudentschoolPrepTemplateCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getStudentschoolPrepTemplateCourse();
			return $result;
		}
		
		//@ayush getstudentHobbies_skills
		public static function getstudentHobbies_skills($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getstudentHobbies_skills();
			return $result;
		}
		
		//@ayush for categories Business_Management
		public static function getstudentBusiness_Management($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getstudentBusiness_Management();
			return $result;
		}
		
	
		//@ayush for categories getStudentIT_Software
		public static function getStudentIT_Software($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getStudentIT_Software();
			return $result;
		}
		
		//@ayush for categories getstudentEntrepreneurship
		public static function getstudentEntrepreneurship($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getstudentEntrepreneurship();
			return $result;
		}
		
		//@ayush for categories getstudentMiscellaneous
		public static function getstudentMiscellaneous($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getstudentMiscellaneous();
			return $result;
		}
		
		
		
		public static function getContentTemplateCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getContentTemplateCourse();
			return $result;
		}
		
		public static function getPageTemplateCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getPageTemplateCourse($req);
			return $result;
		}
	
		public static function createCourse($req) {
			if($req->userRole == '1' ||$req->userRole == '2'||$req->userRole == '3') {
				require_once "Course.php";
				$c = new Course();
				$result = $c->createCourse($req);
			} else {
				$result = new stdClass();
				$result->status = 0;
				$result->message = "Unspecified error.";
			}
			return $result;
		}
		
		public static function saveCourseImage($data) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->saveCourseImage($data);
			return $result;
		}
		public static function savePackageImage($data) {
			require_once "StudentPackage.php";
			$e = new StudentPackage();
			$result = $e->savePackageImage($data);
			return $result;
		}
		public static function savePackage($data) {
			require_once "StudentPackage.php";
			$e = new StudentPackage();
			$result = $e->savePackage($data);
			return $result;
		}	
		public static function editPackage($data) {
			require_once "StudentPackage.php";
			$e = new StudentPackage();
			$result = $e->editPackage($data);
			return $result;
		}		//
		public static function canCreatePackage($data) {
			require_once "StudentPackage.php";
			$e = new StudentPackage();
			$result = $e->canCreatePackage($data);
			return $result;
		}	
		public static function getPackagestudent($data) {
			require_once "StudentPackage.php";
			$e = new StudentPackage();
			$result = $e->getPackagestudent($data);
			return $result;
		}
		public static function getPackageprice($data) {
			require_once "StudentPackage.php";
			$e = new StudentPackage();
			$result = $e->getPackageprice($data);
			return $result;
		}
		public static function getCreatedPackage($data) {
			require_once "StudentPackage.php";
			$e = new StudentPackage();
			$result = $e->getCreatedPackage($data);
			return $result;
		}
		public static function getViewPackage($data) {
			require_once "StudentPackage.php";
			$e = new StudentPackage();
			$result = $e->getViewPackage($data);
			return $result;
		}
		public static function getCourseDetails($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getCourseDetails($req);
			return $result;
		}
		
		public static function updateCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->updateCourse($req);
			return $result;
		}
		
		public static function updateCourseLicensing($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->updateCourseLicensing($req);
			return $result;
		}
		
		public static function createSubject($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->createSubject($req);
			return $result;
		}
		
		public static function getCourseSubjects($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getCourseSubjects($req);
			return $result;
		}
		
		public static function getSubjectDetails($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getSubjectDetails($req);
			return $result;
		}
		public static function getSubjectProgressDetails($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getSubjectProgressDetails($req);
			return $result;
		}
		public static function getSubjectQuestions($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getSubjectQuestions($req);
			return $result;
		}
		public static function getSubjectQuestionsByCategory($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getSubjectQuestionsByCategory($req);
			return $result;
		}
		public static function getSubjectiveQuestionsImport($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getSubjectiveQuestionsImport($req);
			return $result;
		}
		public static function getSubjectiveQuestions($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getSubjectiveQuestions($req);
			return $result;
		}
		public static function getStudentProgressDetails($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getProgressDetailsStudents($req);
			return $result;
		}
		public static function getStudentSubjectDetails($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getStudentSubjectDetails($req);
			return $result;
		}
		public static function setStudentSubjectDetails($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->setStudentSubjectDetails($req);
			return $result;
		}
		public static function updateSubject($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->updateSubject($req);
			return $result;
		}
		
		public static function getAllCourses($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getAllCourses($req);
			return $result;
		}
		//@Ayush to fetch the coupons for coupons history
		public static function getAllCoupons($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getAllCoupons($req);
			return $result;
		}
		public static function createChapter($req) {
			require_once "Chapter.php";
			$c = new Chapter();
			$result = $c->createChapter($req);
			return $result;
		}

		public static function getSubjectChapters($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getSubjectChapters($req);
			return $result;
		}

		public static function getSubjectChaptersNew($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getSubjectChaptersNew($req);
			return $result;
		}

		public static function getSubjectStudents($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getSubjectStudents($req);
			return $result;
		}

		public static function getSubjectParents($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getSubjectParents($req);
			return $result;
		}
		
		public static function getCoursesForImport($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getCoursesForImport($req);
			return $result;
		}
		public static function getPurchasedCoursesForImport($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getPurchasedCoursesForImport($req);
			return $result;
		}
		public static function importSubjectsToCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->importSubjectsToCourse($req);
			return $result;
		}
		
		public static function getAllCoursesForApproval($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getAllCoursesForApproval($req);
			return $result;
		}

		public static function getListCoursesForAdmin($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getListCoursesForAdmin($req);
			return $result;
		}

		public static function getAllApprovedCourses($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getAllApprovedCourses($req);
			return $result;
		}
				
		public static function getAlldeletedCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getAlldeletedCourse($req);
			return $result;
		}
				
				public static function restoreCourseAdmin($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->restoreCourseAdmin($req);
			return $result;
		}
				
				
		public static function approveCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->approveCourse($req);
			return $result;
		}
		
		public static function rejectCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->rejectCourse($req);
			return $result;
		}
		
		public static function deleteCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->deleteCourse($req);
			return $result;
		}
	
		public static function restoreCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->restoreCourse($req);
			return $result;
		}
		//@ayush for package list getCoursesByOwner
		public static function getCoursesByOwner($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getCoursesByOwnerId($req);
			return $result;
		}
		public static function getCoursesByADMIN($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getCoursesByOwnerIdADMIN($req);
			return $result;
		}
		//getCoursesByADMIN
		public static function getEditcoursesByowner($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getEditcoursesByowner($req);
			return $result;
		}
		public static function getStudentforPackage($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getStudentforPackage($req);
			return $result;
		}
		public static function getEditStudentforPackage($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getEditStudentforPackage($req);
			return $result;
		}
		//
		public static function deleteSubject($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->deleteSubject($req);
			return $result;
		}
	
		public static function restoreSubject($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->restoreSubject($req);
			return $result;
		}
		
		public static function updateChapterOrder($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->updateChapterOrder($req);
			return $result;
		}
		
		public static function getInstituteProfessors($req) {
			require_once "Institute.php";
			$i = new Institute();
			$result = $i->getInstituteProfessors($req);
			return $result;
		}
		
		public static function updateSubjectProfessors($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->updateSubjectProfessors($req);
			return $result;
		}
		
		public static function getInstituteProfessorsWithSubjects($req) {
			require_once "Institute.php";
			$i = new Institute();
			$result = $i->getInstituteProfessorsWithSubjects($req);
			return $result;
		}
		
		public static function deleteSubjectProfessor($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->deleteSubjectProfessor($req);
			return $result;
		}
		public static function deleteInstituteProfessor($req) {
			require_once "Institute.php";
			$i = new Institute();
			$result = $i->deleteInstituteProfessor($req);
			return $result;
		}

		public static function getSubjectProfessorsChat($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getSubjectProfessorsChat($req);
			return $result;
		}

		public static function getStudentChatLink($req) {
			require_once "Coursechat.php";
			$cc = new Coursechat();
			$result = $cc->getStudentChatLink($req);
			return $result;
		}

		public static function getProfessorChatLink($req) {
			require_once "Coursechat.php";
			$cc = new Coursechat();
			$result = $cc->getProfessorChatLink($req);
			return $result;
		}

		public static function requestCourseApproval($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->requestCourseApproval($req);
			return $result;
		}
		
		public static function updateCourseOrder($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->updateCourseOrder($req);
			return $result;
		}
		
		public static function updateSubjectOrder($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->updateSubjectOrder($req);
			return $result;
		}
		
		public static function saveSubjectImage($data) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->saveSubjectImage($data);
			return $result;
		}
		public static function saveSubjectSyllabus($data) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->saveSubjectSyllabus($data);
			return $result;
		}
		public static function savecourseDoc($data) {
			require_once "Course.php";
			$s = new Course();;
			$result = $s->savecourseDoc($data);
			return $result;
		}
		//
		
		public static function saveHeading($data) {
			require_once "Content.php";
			$c = new Content();
			if($data->headingId == 0)
				$result = $c->createHeading($data);
			else
				$result = $c->updateHeading($data);
			return $result;
		}
		
		public static function deleteHeading($data) {
			require_once "Content.php";
			$c = new Content();
			$result = $c->deleteHeading($data);
			return $result;
		}
		
		public static function saveContent($data) {
			require_once "Content.php";
			$c = new Content();
			if($data->contentId == 0)
				$result = $c->createContent($data);
			else
				$result = $c->updateContent($data);
			return $result;
		}
		public static function deleteContent($data) {
			require_once "Content.php";
			$c = new Content();
			$result = $c->deleteContent($data);
			return $result;
		}
		public static function updatePublishedStatus($data) {
			require_once "Content.php";
			$c = new Content();
			$result = $c->updatePublishedStatus($data);
			return $result;
		}
		public static function updateDownloadableStatus($data) {
			require_once "Content.php";
			$c = new Content();
			$result = $c->updateDownloadableStatus($data);
			return $result;
		}
		public static function saveContentStuff($data) {
			require_once "Content.php";
			$c = new Content();
			$result = $c->saveContentStuff($data);
			return $result;
		}
		public static function saveStuffDesc($data) {
			require_once "Content.php";
			$c = new Content();
			$result = $c->saveStuffDesc($data);
			return $result;
		}
		public static function getAllContent($data) {
			require_once "Content.php";
			$c = new Content();
			$result = $c->getAllContent($data);
			return $result;
		}
		
		public static function updateContentOrder($data) {
			require_once "Content.php";
			$c = new Content();
			$result = $c->updateContentOrder($data);
			return $result;
		}
		
		public static function getChapterDetails($data) {
			require_once "Chapter.php";
			$c = new Chapter();
			$result = $c->getChapterDetails($data);
			return $result;
		}
		
		public static function getChapterDetailsForView($data) {
			require_once "Chapter.php";
			$c = new Chapter();
			$result = $c->getChapterDetailsForView($data);
			return $result;
		}
		
		public static function deleteContentStuff($data) {
			require_once "Content.php";
			$c = new Content();
			$result = $c->deleteContentStuff($data);
			return $result;
		}
		public static function saveSupplementaryStuff($data) {
			require_once "Content.php";
			$c = new Content();
			$result = $c->saveSupplementaryStuff($data);
			return $result;
		}
		public static function deleteSupplementaryStuff($data) {
			require_once "Content.php";
			$c = new Content();
			$result = $c->deleteSupplementaryStuff($data);
			return $result;
		}
		/* amit*/
		public static function updateChapterDetails($data) {
			require_once "Chapter.php";
			$c = new Chapter();
			$result = $c->updateChapterDetails($data);
			return $result;
		}
				
		public static function getAllInstituteCoursesForTree($req) {
			require_once "Institute.php";
			$i = new Institute();
			$result = $i->getAllInstituteCoursesForTree($req);
			return $result;
		}
		public static function getAllInstituteSubjectsForTree($req) {
			require_once "Institute.php";
			$i = new Institute();
			$result = $i->getAllInstituteSubjectsForTree($req);
			return $result;
		}
		public static function getAllInstituteChaptersForTree($req) {
			require_once "Institute.php";
			$i = new Institute();
			$result = $i->getAllInstituteChaptersForTree($req);
			return $result;
		}
		
		public static function getCoursesForExam($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getCoursesForExam($req);
			return $result;
		}
		
		public static function getSubjectExams($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getSubjectExams($req);
			return $result;
		}
		
		public static function getAssignmentsForExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getAssignmentsForExam($req);
			return $result;
		}

		public static function getSubjectSubjectiveExams($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getSubjectSubjectiveExams($req);
			return $result;
		}
		
		public static function getSubjectsForExam($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getSubjectsForExam($req);
			return $result;
		}
		public static function getExamDetails($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getExamDetails($req);
			return $result;
		}
		public static function getExamDetail($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getExamDetail($req);
			return $result;
		}
		public static function getExamDetailAdmin($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getExamDetailAdmin($req);
			return $result;
		}
		public static function getExamOnlyAdmin($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getExamOnlyAdmin($req);
			return $result;
		}
		public static function getChaptersForExam($req) {
			require_once "Chapter.php";
			$c = new Chapter();
			$result = $c->getChaptersForExam($req);
			return $result;
		}
		public static function addExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->addExam($req);
			return $result;
		}
		public static function addSubejctiveExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->addSubejctiveExam($req);
			return $result;
		}
		public static function editSubjectiveExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->editSubjectiveExam($req);
			return $result;
		}
		public static function addTemplate($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->addTemplate($req);
			return $result;
		}
		public static function getExamSectionsForExport($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getExamSectionsForExport($req);
			return $result;
		}
		public static function exportExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->exportExam($req);
			return $result;
		}
		public static function getExamSections($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getExamSections($req);
			return $result;
		}
		public static function getExamName($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getExamName($req);
			return $result;
		}
		public static function getSectionDetails($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getSectionDetails($req);
			return $result;
		}
		public static function saveCategories($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->saveCategories($req);
			return $result;
		}
		public static function saveCategoriesDesc($req) {
			require_once "Course.php";
			$e = new Course();
			$result = $e->saveCategoriesDesc($req);
			return $result;
		}
		public static function viewCourseCategory($req) {
			require_once "Course.php";
			$e = new Course();
			$result = $e->viewCourseCategory($req);
			return $result;
		}
		public static function saveQuestion($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->saveQuestion($req);
			return $result;
		}
		public static function saveQuestionExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->saveQuestionExam($req);
			return $result;
		}
		public static function saveSubjectiveQuestion($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->saveSubjectiveQuestion($req);
			return $result;
		}
		public static function insertSubjectiveQuestion($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->insertSubjectiveQuestion($req);
			return $result;
		}
		public static function editSubjectiveQuestion($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->editSubjectiveQuestion($req);
			return $result;
		}
		public static function deleteSubjectiveQuestion($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->deleteSubjectiveQuestion($req);
			return $result;
		}
		public static function insertSubjectiveQuestionExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->insertSubjectiveQuestionExam($req);
			return $result;
		}
		public static function editSubjectiveQuestionExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->editSubjectiveQuestionExam($req);
			return $result;
		}
		public static function deleteSubjectiveQuestionExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->deleteSubjectiveQuestionExam($req);
			return $result;
		}
		public static function liveSubjectiveQuestion($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->liveSubjectiveQuestion($req);
			return $result;
		}
		public static function unliveSubjectiveQuestion($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->unliveSubjectiveQuestion($req);
			return $result;
		}
		public static function getSubjectiveExamDetail($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getSubjectiveExamDetail($req);
			return $result;
		}
		public static function getCheckSubjectiveExamStudents($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getCheckSubjectiveExamStudents($req);
			return $result;
		}
		public static function getCheckSubjectiveExamQuestions($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getCheckSubjectiveExamQuestions($req);
			return $result;
		}
		public static function getSubjectiveExamAttempt($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->getSubjectiveExamAttempt($req);
			return $result;
		}
		//
		public static function getCheckSubjectiveExamStudentId($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getCheckSubjectiveExamStudentId($req);
			return $result;
		}
		public static function markSubjectiveExamChecked($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->markSubjectiveExamChecked($req);
			return $result;
		}
		public static function deleteSubjectiveExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->deleteSubjectiveExam($req);
			return $result;
		}
		public static function getAttemptlistSubjectiveExamStudents($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getAttemptlistSubjectiveExamStudents($req);
			return $result;
		}
		public static function getQuestionDetails($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getQuestionDetails($req);
			return $result;
		}
		public static function editQuestion($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->editQuestion($req);
			return $result;
		}
		public static function editQuestionExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->editQuestionExam($req);
			return $result;
		}
		public static function checkCategoryStatus($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->checkCategoryStatus($req);
			return $result;
		}
		public static function getExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getExam($req);
			return $result;
		}
		public static function editExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->editExam($req);
			return $result;
		}
		public static function deleteQuestion($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->deleteQuestion($req);
			return $result;
		}
		public static function deleteQuestionExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->deleteQuestionExam($req);
			return $result;
		}
		public static function deleteCategory($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->deleteCategory($req);
			return $result;
		}
		public static function deleteSection($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->deleteSection($req);
			return $result;
		}
		public static function deleteExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->deleteExam($req);
			return $result;
		}
		public static function copyExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->copyExam($req);
			return $result;
		}
		public static function copySubjectiveExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->copySubjectiveExam($req);
			return $result;
		}
		public static function copySubmission($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->copySubmission($req);
			return $result;
		}
		public static function getSectionStatus($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getSectionStatus($req);
			return $result;
		}
		public static function makeLive($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->makeLive($req);
			return $result;
		}
		public static function makeUnlive($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->makeUnlive($req);
			return $result;
		}
		public static function makeExamEnd($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->makeExamEnd($req);
			return $result;
		}
		public static function getTemplateForExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getTemplateForExam($req);
			return $result;
		}
		public static function getTemplateDetails($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getTemplateDetails($req);
			return $result;
		}
		public static function getTemplate($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getTemplate($req);
			return $result;
		}
		
		public static function updateInvitation($req)
		{
			require_once "Institute.php";
			$u= new Institute();
			$status= $u->updateInvitation($req);
			return $status;
		}
		
		/* @Sexena A.*/
		
		public static function fetchProfessorDetails($req) {
			require_once "Professor.php";
			$p = new Professor();
			$result = $p->getProfessors($req);
			return $result;
		}
		
		public static function inviteProfessor($req) {
			require_once "Professor.php";
			$p = new Professor();
			$result = $p->addInvite($req);
			return $result;
		}
		
		/* @Wadhwa C.*/
		
		public static function getRoleId($userId) {
			require_once "User.php";
			$u = new User();
			$userRole = '';
			$userRole = $u->getRoleId($userId);
			return $userRole;
		}
		
		public static function getInvitedProfessors($req) {
			require_once "Institute.php";
			$i=new Institute();
			$result=$i->getInvitedProfessors($req);
			return $result;
		}
		
		public static function editTemplate($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->editTemplate($req);
			return $result;
		}
		
		public static function deleteTemplate($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->deleteTemplate($req);
			return $result;
		}
		
		public static function getQuestionsForImport($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getQuestionsForImport($req);
			return $result;
		}
		
		public static function importQuestions($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->importQuestions($req);
			return $result;
		}
		
		public static function importSubjectQuestions($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->importSubjectQuestions($req);
			return $result;
		}
		
		public static function importSubjectiveQuestions($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->importSubjectiveQuestions($req);
			return $result;
		}
		
		public static function restoreSection($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->restoreSection($req);
			return $result;
		}
		
		public static function restoreExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->restoreExam($req);
			return $result;
		}
		
		public static function restoreSubjectiveExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->restoreSubjectiveExam($req);
			return $result;
		}
		
		public static function restoreManualExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->restoreManualExam($req);
			return $result;
		}
		
		public static function restoreSubmission($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->restoreSubmission($req);
			return $result;
		}
		
		public static function getBreadcrumbForExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getBreadcrumbForExam($req);
			return $result;
		}

		public static function getBreadcrumbForManualExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getBreadcrumbForManualExam($req);
			return $result;
		}
		
		public static function getBreadcrumbForAdd($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getBreadcrumbForAdd($req);
			return $result;
		}
		
		public static function getCourseDate($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getCourseDate($req);
			return $result;
		}
		
		public static function checkNameForExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->checkNameForExam($req);
			return $result;
		}
		//
		public static function checkNameForsubjectiveexam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->checkNameForsubjectiveexam($req);
			return $result;
		}
		
		public static function checkNameForPackage($req) {
			require_once "StudentPackage.php";
			$e = new StudentPackage();
			$result = $e->checkNameForPackage($req);
			return $result;
		}
		public static function checkNameForEditPackage($req) {
			require_once "StudentPackage.php";
			$e = new StudentPackage();
			$result = $e->checkNameForEditPackage($req);
			return $result;
		}
		//
		public static function checkNameForTemplate($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->checkNameForTemplate($req);
			return $result;
		}
		
		/* course key invitation module */
		public static function getCoursekeyDetails($req) {
			require_once "Coursekeys.php";
			$c = new Coursekeys();
			$result = $c->loadkeydetails($req);
			return $result;
		}
	
		public static function purchase_key($data) {
			require_once "Coursekeys.php";
			$c = new Coursekeys();
			$result = $c->purchase_key($data);
			return $result;
		}
	
		public static function backendCoursekeys($data) {
			require_once "Coursekeys.php";
			$c = new Coursekeys();
			$result = $c->backendCoursekeys($data);
			return $result;
		}
		public static function freetier($data) {
			require_once "Coursekeys.php";
			$c = new Coursekeys();
			$result = $c->freetier($data);
			return $result;
		}
		public static function savefreetier($data) {
			require_once "Coursekeys.php";
			$c = new Coursekeys();
			$result = $c->savefreetier($data);
			return $result;
		}	
		public static function change_key_rate($data) {
			require_once "Coursekeys.php";
			$c = new Coursekeys();
			$result = $c->change_key_rate($data);
			return $result;
		}
	
		public static function setInstitute_rate_back($data) {
			require_once "Coursekeys.php";
			$c = new Coursekeys();
			$result = $c->setInstitute_rate_back($data);
			return $result;
		}
	
		public static function add_keys($data) {
			require_once "Coursekeys.php";
			$c = new Coursekeys();
			$result = $c->purchase_key($data);
			return $result;
		}
		public static function keysPurchaseInvitation($data) {
			require_once "Coursekeys.php";
			$c = new Coursekeys();
			$result = $c->keysPurchaseInvitation($data);
			return $result;
		}
				public static function set_default_key_rate($data) {
			require_once "Coursekeys.php";
			$c = new Coursekeys();
			$result = $c->set_default_key_rate($data);
			return $result;
		}
	
		public static function get_students_of_course($data) {
			require_once "Coursekeys.php";
			$c = new Coursekeys();
			$result = $c->get_students_of_course($data);
			return $result;
		}
	
		public static function get_course_student_details($data) {
			require_once "Coursekeys.php";
			$c = new Coursekeys();
			$result = $c->get_course_student_details($data);
			return $result;
		}
	
		public static function get_students_details_of_course($data) {
			require_once "Coursekeys.php";
			$c = new Coursekeys();
			$result = $c->get_students_details_of_course($data);
			return $result;
		}
		
		public static function generateParents($data) {
			$u = new User();
			$result = $u->generateParents($data);
			return $result;
		}

		public static function send_activation_link_student($data) {
			require_once "StudentInvitation.php";
			$c = new StudentInvitation();
			$result = $c->send_activation_link_student($data);
			return $result;
		}
	
		public static function verify_activation_link_student($data) {
			require_once "StudentInvitation.php";
			$c = new StudentInvitation();
			$result = $c->verify_activation_link_student($data);
			return $result;
		}
	
		public static function student_invitation_details($data) {
			require_once "StudentInvitation.php";
			$c = new StudentInvitation();
			$result = $c->student_invitation_details($data);
			return $result;
		}
	
		public static function resend_invitation_link($data) {
			require_once "StudentInvitation.php";
			$c = new StudentInvitation();
			$result = $c->resend_invitation_link($data);
			return $result;
		}
	
		public static function load_student_notification($data) {
			require_once "StudentInvitation.php";
			$c = new StudentInvitation();
			$result = $c->load_student_notification($data);
			return $result;
		}
	
		public static function accepted_student_invitation($data) {
			require_once "StudentInvitation.php";
			$c = new StudentInvitation();
			$result = $c->accepted_student_invitation($data);
			return $result;
		}
	
		public static function reject_student_invitation($data) {
			require_once "StudentInvitation.php";
			$c = new StudentInvitation();
			$result = $c->reject_student_invitation($data);
			return $result;
		}
	
		public static function reject_link_by_admin($data) {
			require_once "StudentInvitation.php";
			$c = new StudentInvitation();
			$result = $c->reject_student_invitation($data);
			return $result;
		}
	
		public static function keys_avilable($data) {
			require_once "StudentInvitation.php";
			$c = new StudentInvitation();
			$result = $c->keys_avilable($data);
			return $result;
		}
				public static function StudentDetailsWithCourses($data) {
			require_once "StudentInvitation.php";
			$c = new StudentInvitation();
			$result = $c->StudentDetailsWithCourses($data);
			return $result;
		}
				public static function CourseforStudent($data) {
			require_once "StudentInvitation.php";
			$c = new StudentInvitation();
			$result = $c->CourseforStudent($data);
			return $result;
		}
				
		public static function view_institutes_info($data) {
			require_once "viewInsitutesinfo.php";
			$c = new viewInsitutesinfo();
			$result = $c->viewInsitutesDetails($data);
			return $result;
		}
		public static function viewInstitutesLoginInfo($data) {
			require_once "viewInsitutesinfo.php";
			$c = new viewInsitutesinfo();
			$result = $c->viewInstitutesLoginInfo($data);
			return $result;
		}
				
		public static function ChangeLoginstatus($data) {
			require_once "viewInsitutesinfo.php";
			$c = new viewInsitutesinfo();
			$result = $c->ChangeLoginstatus($data);
			return $result;
		}
		public static function changePassword($data) {
			require_once "User.php";
			$u = new User();
			$result = $u->changePassword($data);
			return $result;
		}

		public static function getSocialConnections($data) {
			require_once "User.php";
			$u = new User();
			$result = $u->getSocialConnections($data);
			return $result;
		}

		public static function setSocialConnection($data) {
			require_once "User.php";
			$u = new User();
			$result = $u->setSocialConnection($data);
			return $result;
		}
				  
		public static function getemailUsername($data) {
			require_once "User.php";
			$u = new User();
			$result = $u->getemailUsername($data);
			return $result;
		}
				
		public static function loadCourseDetails($data) {
			require_once "CourseDetail.php";
			$u = new CourseDetail();
			$result = $u->getCourseDetails($data);
			return $result;
		}
		public static function loadPackageCourseDetails($data) {
			require_once "CourseDetail.php";
			$u = new CourseDetail();
			$result = $u->getPackageCourseDetails($data);
			return $result;
		}
		public static function loadSubjectDetails($data) {
			require_once "CourseDetail.php";
			$u = new CourseDetail();
			$result = $u->getCourseSubjects($data);
			return $result;
		}
		public static function otherCoursesbyProfessor($data) {
			require_once "CourseDetail.php";
			$u = new CourseDetail();
			$result = $u->otherCoursesbyProfessor($data);
			return $result;
		}
				   
	  // admin section by amit      
		public static function getAlldeletedSubject($data) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getAlldeletedSubject($data);
			return $result;
		}
		public static function restoreSubjectByAdmin($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->restoreSubjectByAdmin($req);
			return $result;
		}
		public static function getAlldeletedChapter($data) {
			require_once "Chapter.php";
			$ch = new Chapter();
			$result = $ch->getAlldeletedChapter($data);
			return $result;
		}
		public static function restoreChapterByAdmin($req) {
			require_once "Chapter.php";
			$ch = new Chapter();
			$result = $ch->restoreChapterByAdmin($req);
			return $result;
		}
		public static function getAlldeletedExam($data) {
			require_once "Exam.php";
			$ch = new Exam();
			$result = $ch->getAlldeletedExam($data);
			return $result;
		}
		public static function restoreExamByAdmin($req) {
			require_once "Exam.php";
			$ch = new Exam();
			$result = $ch->restoreExamByAdmin($req);
			return $result;
		}
				// 
				//
				// Get Exams of Student
				//
		public static function getStudentExamAttemps($req) {
			require_once "Exam.php";
			$ch = new Exam();
			$result = $ch->getStudentExamAttemps($req);
			return $result;
		}
		public static function getStudentExamAttemptsNew($req) {
			require_once "Exam.php";
			$ch = new Exam();
			$result = $ch->getStudentExamAttemptsNew($req);
			return $result;
		}
		public static function getStudentSubjectiveExamAttemps($req) {
			require_once "Exam.php";
			$ch = new Exam();
			$result = $ch->getStudentSubjectiveExamAttemps($req);
			return $result;
		}
		public static function getStudentManualExams($req) {
			require_once "Exam.php";
			$ch = new Exam();
			$result = $ch->getStudentManualExams($req);
			return $result;
		}
		public static function getManualExamDetail($req) {
			require_once "Exam.php";
			$ch = new Exam();
			$result = $ch->getManualExamDetail($req);
			return $result;
		}
		public static function studentExamsChapterWise($req) {
			require_once "Exam.php";
			$ch = new Exam();
			$result = $ch->studentExamsChapterWise($req);
			return $result;
		}
		public static function studentSubjectiveExamsChapterWise($req) {
			require_once "Exam.php";
			$ch = new Exam();
			$result = $ch->studentSubjectiveExamsChapterWise($req);
			return $result;
		}
		public static function saveMarksForSubjectiveExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->saveMarksForSubjectiveExam($req);
			return $result;
		}
		public static function saveReviewForSubjectiveExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->saveReviewForSubjectiveExam($req);
			return $result;
		}
				
				
		public static function getCourseSubjectsForStudent($req) {
			require_once "Subject.php";
			$ch = new Subject();
			//$result=1;
			$result = $ch->getCourseSubjectsForStudent($req);
			return $result;
		}

		public static function getCourseSubjectsForStudentNew($req) {
			require_once "Subject.php";
			$ch = new Subject();
			//$result=1;
			$result = $ch->getCourseSubjectsForStudentNew($req);
			return $result;
		}
				
		/*Tax Section*/
		public static function fetchAllTaxes($req) {
			require_once "Tax.php";
			$t = new Tax();
			$result = $t->getTaxRates();
			return $result;
		}

		public static function fetchDefaultTaxes($req) {
			require_once "Tax.php";
			$t = new Tax();
			$result = $t->getDefaultTaxRates();
			return $result;
		}

		public static function updateInstituteTaxes($req) {
			require_once "Tax.php";
			$t = new Tax();
			$result = $t->updateTaxes($req);
			return $result;
		}

		public static function updateDefaultTax($req) {
			require_once "Tax.php";
			$t = new Tax();
			$result = $t->updateDefaultTax($req);
			return $result;
		}

		public static function deleteDefaultTax($req) {
			require_once "Tax.php";
			$t = new Tax();
			$result = $t->deleteDefaultTax($req);
			return $result;
		}

		public static function insertDefaultTax($req) {
			require_once "Tax.php";
			$t = new Tax();
			$result = $t->insertDefaultTax($req);
			return $result;
		}


		/*Student section*/
		//to fetch student name
		public static function getPackageForStudent($req) {
			require_once "StudentPackage.php";
			$s = new StudentPackage();
			$result = $s->getPackageForStudent($req);
			return $result;
		}
		public static function getAllPackageForStudent($req) {
			require_once "StudentPackage.php";
			$s = new StudentPackage();
			$result = $s->getAllPackageForStudent($req);
			return $result;
		}//
		public static function getPackageDetails($req) {
			require_once "StudentPackage.php";
			$s = new StudentPackage();
			$result = $s->getPackageDetails($req);
			return $result;
		}
		public static function setstudentPackagecourse($req) {
			require_once "StudentPackage.php";
			$s = new StudentPackage();
			$result = $s->setUserCoursePackage($req);
			return $result;
		}
		//getpackageCourses
		public static function getpackageCourses($req) {
			require_once "StudentPackage.php";
			$s = new StudentPackage();
			$result = $s->getPackagesCourses($req);
			return $result;
		}
		//
		public static function getAllpackageCourses($req) {
			require_once "StudentPackage.php";
			$s = new StudentPackage();
			$result = $s->getAllpackageCourses($req);
			return $result;
		}
		public static function showAllpackageCourses($req) {
			require_once "StudentPackage.php";
			$s = new StudentPackage();
			$result = $s->showAllpackageCourses($req);
			return $result;
		}
		public static function getStudentName($req) {
			require_once "Student.php";
			$s = new Student();
			$result = $s->getStudentName($req);
			return $result;
		}
		
		//to get exam details to initiate view for student
		public static function getExamForStudent($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->getExamForStudent($req);
			return $result;
		}
		public static function getSubjectiveExamForStudent($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->getSubjectiveExamForStudent($req);
			return $result;
		}
		//to get questions for student to give exam
		public static function getQuestionsForExam($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->getQuestionsForExam($req);
		//$result = $s->getQuestionsForSubjectiveExam($req);
			return $result;
		}
		public static function getQuestionsForSubjectiveExam($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->getQuestionsForSubjectiveExam($req);
			return $result;
		}
		public static function getQuestionsForSubjectiveExamResumeMode($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->getQuestionsForSubjectiveExamResumeMode($req);
			return $result;
		}
		
		//to get questions and answers for student to practice assignment
		public static function getQuestionsForAssignment($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->getQuestionsForAssignment($req);
			return $result;
		}
		
		//to get questions and answers for student to practice assignment in resume mode
		public static function getQuestionsForAssignmentInResume($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->getQuestionsForAssignmentInResume($req);
			return $result;
		}
		
		//to get questions and answers for student to give exam in resume mode
		public static function getQuestionsForExamInResume($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->getQuestionsForExamInResume($req);
			return $result;
		}
		
		//to get resume status of the exam or assignment
		public static function getExamResumeStatus($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->getExamResumeStatus($req);
			return $result;
		}
		
		public static function getSubjectiveExamResumeStatus($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->getSubjectiveExamResumeStatus($req);
			return $result;
		}
		
		//
		public static function checkExam($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->checkExam($req);
			return $result;
		}
		//funcction to get hte courseId for exam
		public static function getExamCourseId($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->getExamCourseId($req);
			return $result;
		}
		
		//to get time and section of paused exam
		public static function getTimeAndSection($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->getTimeAndSection($req);
			return $result;
		}

		//to get time and section of paused exam
		public static function getSubjectiveTime($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->getSubjectiveTime($req);
			return $result;
		}
		
		//
		public static function setTimeSubjectiveExam($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->setTimeSubjectiveExam($req);
			return $result;
		}
		//to increase the attempt count for exam
		public static function increaseAttempt($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->increaseAttempt($req);
			return $result;
		}
		//
		public static function increaseSubjectiveExamAttempt($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->increaseSubjectiveExamAttempt($req);
			return $result;
		}
		//to check and save exam attempt
		public static function checkAndSave($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->checkAndSave($req);
			return $result;
		}
		//to check and save exam attempt
		public static function saveSubjectiveExam($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->saveSubjectiveExam($req);
			return $result;
		}
		public static function checkResultShow($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->checkResultShow($req);
			return $result;
		}
		//
		//to save all asked questions once and continue exam from the same point
		public static function saveAllQuestions($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->saveAllQuestions($req);
			return $result;
		}
		
		//to update user answer on change by him/her
		public static function updateUserAnswer($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->updateUserAnswer($req);
			return $result;
		}
		public static function updateSubjectiveAnswer($req) {
			require_once "StudentExam.php";
			$s = new StudentExam();
			$result = $s->updateSubjectiveAnswer($req);
			return $result;
		}
		//
		/* functions for chapter content view page */
		//to get breadcrumb for chapter content view
		public static function getBreadcrumbForChapter($req) {
			require_once "Chapter.php";
			$c = new Chapter();
			$result = $c->getBreadcrumbForChapter($req);
			return $result;
		}
		
		//to get headings for the chapter
		public static function getHeadings($req) {
			require_once "StudentContent.php";
			$c = new StudentContent();
			$result = $c->getHeadings($req);
			return $result;
		}
		
		//to get content data for student view
		public static function getContentStuff($req) {
			require_once "StudentContent.php";
			$c = new StudentContent();
			$result = $c->getContentStuff($req);
			return $result;
		}
		//@Ayush for checking enroll status of course
		public static function getcourseEnrollStatus($req) {
			require_once "Student.php";
			$s = new Student();
			$result = $s->getcourseEnrollStatus($req);
			return $result;
		}
		
		
		//to get student details for profile page
		public static function getStudentDetails($req) {
			require_once "Student.php";
			$s = new Student();
			$result = $s->getStudentDetails($req);
			return $result;
		}

		//to get student details for profile page
		public static function getStudentDetailsNew($req) {
			require_once "Student.php";
			$s = new Student();
			$result = $s->getStudentDetailsNew($req);
			return $result;
		}
		
		//to save student's background on profile page
		public static function saveBio($req) {
			require_once "Student.php";
			$s = new Student();
			$result = $s->saveBio($req);
			return $result;
		}
		
		//to save student's achievement on profile page
		public static function saveAchievement($req) {
			require_once "Student.php";
			$s = new Student();
			$result = $s->saveAchievement($req);
			return $result;
		}
		
		//to save student's interest on profile page
		public static function saveInterest($req) {
			require_once "Student.php";
			$s = new Student();
			$result = $s->saveInterest($req);
			return $result;
		}
		
		//to get student details for setting page
		public static function getStudentDetailsForSetting($req) {
			require_once "Student.php";
			$s = new Student();
			$result = $s->getStudentDetailsForSetting($req);
			return $result;
		}
		
		//function to save student detail on setting page
		public static function saveStudentSetting($req) {
			require_once "Student.php";
			$s = new Student();
			$result = $s->saveStudentSetting($req);
			return $result;
		}
		
		//to get student details for change password page
		public static function getStudentDetailsForChangePassword($req) {
			require_once "Student.php";
			$s = new Student();
			$result = $s->getStudentDetailsForChangePassword($req);
			return $result;
		}
		
		//to get student notification details
		public static function getStudentNotifications($req) {
			require_once "Student.php";
			$s = new Student();
			$result = $s->getStudentNotifications($req);
			return $result;
		}
		//to get student notification details
		public static function getStudentNotificationsActivity($req) {
			require_once "Student.php";
			$s = new Student();
			$result = $s->getStudentNotificationsActivity($req);
			return $result;
		}
		//to get student notification details
		public static function getStudentNotificationsChat($req) {
			require_once "Student.php";
			$s = new Student();
			$result = $s->getStudentNotificationsChat($req);
			return $result;
		}
		
		// to get Student Joined Courses
		public static function getStudentJoinedCourses($data) {
			require_once "Student.php";
			 $u = new Student();
			$result = $u->getStudentCourses($data);
			return $result;
		}
		public static function getStudentSubsCourses($data) {
			require_once "Student.php";
			 $u = new Student();
			$result = $u->getStudentSubsCourses($data);
			return $result;
		}
				
			 //to delete chapter by institute role
		public static function deleteChapter($req) {
			require_once "Chapter.php";
			$c = new Chapter();
			$result = $c->deleteChapter($req);
			return $result;
		}
		
		//to fetch all independent exams in a subject
		public static function getIndependentExams($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getIndependentExams($req);
			return $result;
		}

		//to fetch all independent exams in a subject
		public static function getIndependentExamsNew($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getIndependentExamsNew($req);
			return $result;
		}
		
		public static function getsubjectiveExams($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getsubjectiveExams($req);
			return $result;
		}
		
		public static function getSubjectiveExamAll($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getSubjectiveExamAll($req);
			return $result;
		}
		//
		//to fetch result given by student for a particular attempt and exam
		public static function getResult($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getResult($req);
			return $result;
		}
		
		//to fetch questions and answers of each section given by student for a particular attempt and exam
		public static function getSectionQuestions($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getSectionQuestions($req);
			return $result;
		}
		
		//to fetch topper details for exam
		public static function getTopperDetails($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getTopperDetails($req);
			return $result;
		}
		public static function getTopperDetailsAll($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getTopperDetailsAll($req);
			return $result;
		}
		public static function getCompleteQuestionAnalysis($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getCompleteQuestionAnalysis($req);
			return $result;
		}
		public static function getStudentTimeLine($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getStudentTimeLine($req);
			return $result;
		}
		//
		public static function getStudentTimeLineAll($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getStudentTimeLineAll($req);
			return $result;
		}
		//to fetch question timing
		public static function getMaxiumTimeForQuestion($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getMaxiumTimeForQuestion($req);
			return $result;
		}
		// to fetch the applicable licenses
		public static function fetchApplicableLicenses($req) {
			require_once "License.php";
			$o = new License();
			$result = $o->getLicenses($req);
			return $result;
		}
			   
		// to modify licenses
		public static function modify_licenses($req) {
			require_once "License.php";
			$o = new License();
			$result = $o->modifyLicenses($req);
			return $result;
		}
		//to get exam attempts for subject
		public static function getSubjectAttempts($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getSubjectAttempts($req);
			return $result;
		}
		
		//to fetch exam attempts for graph creation
		public static function getAttemptGraph($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getAttemptGraph($req);
			return $result;
		}
		//to get the graph for the highest comparison on student noramlization
		public static function getallStudentNormalizationGraph($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getallStudentNormalizationGraph($req);
			return $result;
		}
		
		//to fetch exam reports for institute
		public static function getExamReport($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getExamReport($req);
			return $result;
		}
		
		//to fetch student list for given exam who attempted that exam
		public static function getStudentList($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getStudentList($req);
			return $result;
		}
		
		public static function getQuestionList($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getQuestionList($req);
			return $result;
		}
		//to fetch student list for given exam who attempted that exam
		public static function getStudentListDownload($req) {
			require_once "AnalyticsToExcel.php";
			$r = new AnalyticsToExcel();
			$result = $r->getCourseAnalytics($req);
			return $result;
		}
		
		//to get course name availability
		public static function checkDuplicateCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->checkDuplicateCourse($req);
			return $result;
		}
		//to get course name availability
		public static function checkDuplicateSubject($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->checkDuplicateSubject($req);
			return $result;
		}
		public static function checkDuplicateChapter($req) {
			require_once "Chapter.php";
			$c = new Chapter();
			$result = $c->checkDuplicateChapter($req);
			return $result;
		}
		
		//to create instructions for exams and assignments
		public static function createInstructions($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->createInstructions($req);
			return $result;
		}
		
		//to create student left navigation tree
		public static function getCoursesForStudentTree($req) {
			require_once "Student.php";
			$s = new Student();
			$result = $s->getCoursesForStudentTree($req);
			return $result;
		}
		
		//to get counters data for institute dashboard
		public static function getInstituteCounters($req) {
			require_once "Institute.php";
			$i = new Institute();
			$result = $i->getInstituteCounters($req);
			return $result;
		}
		
		//to get graph details for institute dashboard
		public static function getInstituteGraphs($req) {
			require_once "Institute.php";
			$i = new Institute();
			$result = $i->getInstituteGraphs($req);
			return $result;
		}
		
		//to get graph details for institute dashboard in course recent view
		public static function getRecentCourseView($req) {
			require_once "Institute.php";
			$i = new Institute();
			$result = $i->getRecentCourseView($req);
			return $result;
		}
		
		//@ayushto get graph details for institute dashboard in course recent view
		public static function getDeatilRecentCourseView($req) {
			require_once "Institute.php";
			$i = new Institute();
			$result = $i->getDeatilRecentCourseView($req);
			return $result;
		}
		
		public static function getInstituteNotifications($req) {
			require_once "Institute.php";
			$i = new Institute();
			$result = $i->getInstituteNotifications($req);
			return $result;
		}
		public static function getInstituteNotificationsActivity($req) {
			require_once "Institute.php";
			$i = new Institute();
			$result = $i->getInstituteNotificationsActivity($req);
			return $result;
		}
		public static function getInstituteNotificationsChat($req) {
			require_once "Institute.php";
			$i = new Institute();
			$result = $i->getInstituteNotificationsChat($req);
			return $result;
		}
		
		public static function getInstructorNotifications($req) {
			require_once "Professor.php";
			$i = new Professor();
			$result = $i->getInstructorNotifications($req);
			return $result;
		}
		public static function getInstructorNotificationsActivity($req) {
			require_once "Professor.php";
			$i = new Professor();
			$result = $i->getInstructorNotificationsActivity($req);
			return $result;
		}
		public static function getInstructorNotificationsChat($req) {
			require_once "Professor.php";
			$i = new Professor();
			$result = $i->getInstructorNotificationsChat($req);
			return $result;
		}

		public static function markNotification($req) {
			require_once "Institute.php";
			$i = new Institute();
			$result = $i->markNotification($req);
			return $result;
		}
		public static function markChatNotification($req) {
			require_once "Coursechat.php";
			$cc = new Coursechat();
			$result = $cc->markChatNotification($req);
			return $result;
		}
		public static function countNotification($req) {
			require_once "Institute.php";
			$i = new Institute();
			$result = $i->countNotification($req);
			return $result;
		}

		public static function setChatMessageNotification($req) {
			require_once "Coursechat.php";
			$cc = new Coursechat();
			$result = $cc->setChatMessageNotification($req);
			return $result;
		}

		public static function getSubjectInvitation($req) {
			require_once "Subject.php";
			$p = new Subject();
			$result = $p->getSubjectInvitation($req);
			return $result;
		}
		public static function acceptSubjectInvitation($req) {
			require_once "Subject.php";
			$p = new Subject();
			$result = $p->acceptSubjectInvitation($req);
			return $result;
		}
		public static function rejectSubjectInvitation($req) {
			require_once "Subject.php";
			$p = new Subject();
			$result = $p->rejectSubjectInvitation($req);
			return $result;
		}
		public static function getSubjectAssigned($req) {
			require_once "Subject.php";
			$p = new Subject();
			$result = $p->getSubjectAssigned($req);
			return $result;
		}
		public static function getSubjectAssignedNew($req) {
			require_once "Subject.php";
			$p = new Subject();
			$result = $p->getSubjectAssignedNew($req);
			return $result;
		}
		public static function saveCourseRateStudent($req) {
			require_once "License.php";
			$l = new License();
			$result = $l->saveCourseRateStudent($req);
			return $result;
		}
				public static function liveCourseStudent($req) {
			require_once "License.php";
			$l = new License();
			$result = $l->liveCourseStudent($req);
			return $result;
		}
				
				public static function liveCourseOnContentMarket($req) {
			require_once "License.php";
			$l = new License();
			$result = $l->liveCourseOnContentMarket($req);
			return $result;
		}
		public static function unliveCourseStudent($req) {
			require_once "License.php";
			$l = new License();
			$result = $l->unliveCourseStudent($req);
			return $result;
		}
				
				public static function unliveCourseOnContentMarket($req) {
			require_once "License.php";
			$l = new License();
			$result = $l->unliveCourseOnContentMarket($req);
			return $result;
		}
		public static function fetchCourseLicense($req) {
			require_once "License.php";
			$l = new License();
			$result = $l->fetchCourseLicense($req);
			return $result;
		}
		public static function getCourseDetailsStudentEnd($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getCourseDetailsStudentEnd($req);
			return $result;
		}
		public static function getCourseDetailsInstituteEnd($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->getCourseDetailsInstituteEnd($req);
			return $result;
		}
				public static function linkStudentCourse($req) {
			require_once "StudentInvitation.php";
			$c = new StudentInvitation();
			$result = $c->linkStudentCourse($req);
			return $result;
		}
				// to fetch the applicable licenses
		public static function getLicensesONCourse($req) {
			require_once "License.php";
			$o = new License();
			$result = $o->getLicensesONCourse($req);
			return $result;
		}
				public static function CopyCourseandPurchase($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->CopyCourseandPurchase($req);
			return $result;
		}
				public static function fetchPurchasedCoursesDetails($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->fetchPurchasedCoursesDetails($req);
			return $result;
		}
		
		//to download the content stuff for student if it is downloadable
		public static function downloadContent($req) {
			require_once "Content.php";
			$c = new Content();
			$result = $c->downloadContent($req);
			return $result;
		}
		
		//to send course renew request by student
		public static function sendRenewRequest($req) {
			require_once "StudentInvitation.php";
			$si = new StudentInvitation();
			$result = $si->sendRenewRequest($req);
			return $result;
		}
		
		public static function getBreadcrumbResult($req) {
			require_once 'Student.php';
			$s = new Student();
			$result = $s->getBreadcrumbResult($req);
			return $result;
		}
		public static function resendPendingInvitation($req) {
			require_once 'StudentInvitation.php';
			$s = new StudentInvitation();
			$result = $s->resendPendingInvitation($req);
			return $result;
		}
		public static function contactUsEmail($req) {
			require_once 'User.php';
			$s = new User();
			$result = $s->contactUsEmail($req);
			return $result;
		}
		public static function purchaseCourseKeys($req) {
			require_once 'Coursekeys.php';
			$c = new Coursekeys();
			$result = $c->purchaseCourseKeys($req);
			return $result;
		}
		public static function purchaseCourseViaPayU($req) {
			require_once 'Course.php';
			$c = new Course();
			$result = $c->purchaseCourseViaPayU($req);
			return $result;
		}
		public static function checkCourseAcess($req) {
			require_once 'Course.php';
			$c = new Course();
			$result = $c->checkCourseAcess($req);
			return $result;
		}
//
		public static function purchasePackage($req) {
			require_once 'StudentPackage.php';
			$c = new StudentPackage();
			$result = $c->purchasePackage($req);
			return $result;
		}
		//
		public static function purchaseStudentPackage($req) {
			require_once 'StudentPackage.php';
			$c = new StudentPackage();
			$result = $c->purchaseStudentPackage($req);
			return $result;
		}
		
		public static function paypalReturn($req) {
			require_once 'User.php';
			$u = new User();
			$res = $u->orderUser($req);
			$req->userRole = $res->userRole;
			$req->type = $res->type;
			if($req->userRole != 4 && $req->type != 3) {
				require_once 'Coursekeys.php';
				$c = new Coursekeys();
				$result = $c->paypalReturn($req);
			}
			else if($req->userRole != 4 && $req->type == 3) {
				require_once 'StudentPackage.php';
				$c = new StudentPackage();
				$result = $c->paypalReturn($req);
			}
			else if($req->userRole == 4 && $req->type == 4) {
				require_once 'StudentPackage.php';
				$c = new StudentPackage();
				$result = $c->paypalStudentReturn($req);
			}
			else {
				require_once 'Course.php';
				$c = new Course();
				$result = $c->paypalReturn($req);
			}
			
			return $result;
		}

		public static function saveRating($req) {
			require_once 'Student.php';
			$s = new Student();
			$result = $s->saveRating($req);
			return $result;
		}

		public static function fetchUserRating($req) {
			require_once 'Student.php';
			$s = new Student();
			$result = $s->fetchUserRating($req);
			return $result;
		}

		public static function getAllReviewsForCourse($req) {
			require_once 'Student.php';
			$s = new Student();
			$result = $s->getAllReviewsForCourse($req);
			return $result;
		}

		public static function getAllReviewsForSubject($req) {
			require_once 'Student.php';
			$s = new Student();
			$result = $s->getAllReviewsForSubject($req);
			return $result;
		}

		public static function removeReview($req) {
			require_once 'Student.php';
			$s = new Student();
			$result = $s->removeReview($req);
			return $result;
		}

		public static function getSubjectRating($req) {
			require_once 'Student.php';
			$s = new Student();
			$result = $s->getSubjectRating($req->subjectId);
			return $result;
		}
		//to fetch exam attempts for graph creation
		public static function getSubjectiveAttemptGraph($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getSubjectiveAttemptGraph($req);
			return $result;
		}
		public static function getsubjectiveTimeLine($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getsubjectiveTimeLine($req);
			return $result;
		}
		public static function getSubjectiveTopperDetails($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getSubjectiveTopperDetails($req);
			return $result;
		}
		public static function getSubjectiveNormalizationGraph($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getSubjectiveNormalizationGraph($req);
			return $result;
		}
		public static function getSubjectiveMaxiumTimeForQuestion($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getSubjectiveMaxiumTimeForQuestion($req);
			return $result;
		}
		public static function saveDemoVideoForCourse($req) {
			require_once 'Course.php';
			$c = new Course();
			$result = $c->saveDemoVideoForCourse($req);
			return $result;
		}

		public static function deleteDemoVideo($req) {
			require_once 'Course.php';
			$c = new Course();
			$result = $c->deleteDemoVideo($req);
			return $result;
		}
		public static function getPortfolios($req) {
			require_once 'User.php';
			$u = new User();
			$result = $u->getPortfolios($req);
			return $result;
		}
		public static function savePortfolioCourse($req) {
			require_once 'Portfolio.php';
			$p = new Portfolio();
			$result = $p->savePortfolioCourse($req);
			return $result;
		}
		public static function newPortfolio($req) {
			require_once 'User.php';
			$u = new User();
			$result = $u->newPortfolio($req);
			return $result;
		}
		//
		public static function reSubmitPortfolio($req) {
			require_once 'User.php';
			$u = new User();
			$result = $u->reSubmitPortfolio($req);
			return $result;
		}
		public static function updatePortfolio($req) {
			switch($req->userRole) {
				case 1:
					require_once "Institute.php";
					$i = new Institute();
					$res = $i->updatePortfolio($req);
					return $res;
					break;
				case 2:
					require_once "Professor.php";
					$p = new Professor();
					$res = $p->updatePortfolio($req);
					return $res;
					break;
			}
		}
		public static function getPageTypes($req) {
			switch($req->userRole) {
				case 1:
					require_once "Institute.php";
					$i = new Institute();
					$res = $i->getPageTypes($req);
					return $res;
					break;
				case 2:
					require_once "Professor.php";
					$p = new Professor();
					$res = $p->getPageTypes($req);
					return $res;
					break;
			}
		}
		public static function getPortfolioPages($req) {
			switch($req->userRole) {
				case 1:
					require_once "Institute.php";
					$i = new Institute();
					$res = $i->getPortfolioPages($req);
					return $res;
					break;
				case 2:
					require_once "Professor.php";
					$p = new Professor();
					$res = $p->getPortfolioPages($req);
					return $res;
					break;
			}
		}
		public static function getPortfolioMenus($req) {
			require_once 'User.php';
			$u = new User();
			$result = $u->getPortfolioMenus($req);
			return $result;
		}
		public static function savePortfolioPage($req) {
			switch($req->userRole) {
				case 1:
					require_once "Institute.php";
					$i = new Institute();
					$res = $i->savePortfolioPage($req);
					return $res;
					break;
				case 2:
					require_once "Professor.php";
					$p = new Professor();
					$res = $p->savePortfolioPage($req);
					return $res;
					break;
			}
		}
		public static function savePortfolioMenu($req) {
			require_once 'User.php';
			$u = new User();
			$result = $u->savePortfolioMenu($req);
			return $result;
		}
		//@ayush- Coupons for admin
		public static function adminCourseCoupons($req){
			require_once 'Course.php';
			$c = new Course();
			$result = $c->setcourseCouponsAdmin($req);
			return $result;
		}
		//adminCourseCouponsInfo
		public static function adminCourseCouponsInfo($req){
			require_once 'Course.php';
			$c = new Course();
			$result = $c->adminCourseCouponsInfo($req);
			return $result;
		}
			//@ayush- Coupons for admin
		public static function adminCourseDiscount($req){
			require_once 'Course.php';
			$c = new Course();
			$result = $c->setcourseDiscountAdmin($req);
			return $result;
		}
		//@Ayush for coupons for courses
		public static function courseCoupons($req){
			require_once 'Course.php';
			$c = new Course();
			$result = $c->setcourseCoupons($req);
			return $result;
		}
		public static function editcourseCoupons($req){
			require_once 'Course.php';
			$c = new Course();
			$result = $c->updatecourseCoupons($req);
			return $result;
		}
		public static function redeemCoupon($req){
			require_once 'Course.php';
			$c = new Course();
			$result = $c->redeemCoupon($req);
			return $result;
		}
		
		//@ayush dicount module
		public static function courseDiscount($req) {
			require_once 'Course.php';
			$c = new Course();
			$result = $c->setCourseDiscount($req);
			return $result;
		}
		
		//@ayush dicount module ajax call
		public static function discountCourseId($req) {
			require_once 'Course.php';
			$c = new Course();
			$result = $c->getCourseDiscount($req);
			return $result;
		}
		
		//@ayush coupon edit/update module ajax call
		public static function changeupdateCoupon($req) {
			require_once 'Course.php';
			$c = new Course();
			$result = $c->changeupdateCoupon($req);
			return $result;
		}

		//claim contest coupon
		public static function getContestCoupon($req) {
			require_once 'Contest.php';
			$c = new Contest();
			$result = $c->getContestCoupon($req);
			return $result;
		}

		//generate manual exam CSV
		public static function generateManualExamCSV($req) {
			require_once 'Exam.php';
			$e = new Exam();
			$result = $e->generateManualExamCSV($req);
			return $result;
		}

		//generate manual exam form
		public static function generateManualExamForm($req) {
			require_once 'Exam.php';
			$e = new Exam();
			$result = $e->generateManualExamForm($req);
			return $result;
		}

		//save manual exam form
		public static function saveManualExamForm($req) {
			require_once 'Exam.php';
			$e = new Exam();
			$result = $e->saveManualExamForm($req);
			return $result;
		}
		public static function getManualExams($req) {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getManualExams($req);
			return $result;
		}
		public static function getManualExamDetails($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getManualExamDetails($req);
			return $result;
		}
		public static function saveManualExamMarks($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->saveManualExamMarks($req);
			return $result;
		}
		public static function saveManualExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->saveManualExam($req);
			return $result;
		}
		public static function deleteManualExam($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->deleteManualExam($req);
			return $result;
		}
		public static function getStudentManualExamDetails($req) {
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getStudentManualExamDetails($req);
			return $result;
		}
		public static function deleteSubmission($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->deleteSubmission($req);
			return $result;
		}
		public static function generateStudentsForCourse($req) {
			require_once "Course.php";
			$c = new Course();
			$result = $c->generateStudentsForCourse($req);
			return $result;
		}
		public static function initStudent($req) {
			require_once "Student.php";
			$s = new Student();
			$result = $s->initStudent($req);
			return $result;
		}
		public static function verifyMergeStudent($req) {
			require_once "Student.php";
			$s = new Student();
			$result = $s->verifyMergeStudent($req);
			return $result;
		}
		public static function backendCoursechats($data) {
			require_once "Coursechat.php";
			$cc = new Coursechat();
			$result = $cc->backendCoursechats($data);
			return $result;
		}
		public static function backendCoursechatUpdate($data) {
			require_once "Coursechat.php";
			$cc = new Coursechat();
			$result = $cc->backendCoursechatUpdate($data);
			return $result;
		}
		public static function getInstituteStudents($data) {
			require_once "Institute.php";
			$c = new Institute();
			$result = $c->getInstituteStudents($data);
			return $result;
		}
		public static function getInstituteTempStudents($data) {
			require_once "Institute.php";
			$c = new Institute();
			$result = $c->getInstituteTempStudents($data);
			return $result;
		}
		public static function transferStudentCourse($data) {
			require_once "Institute.php";
			$c = new Institute();
			$result = $c->transferStudentCourse($data);
			return $result;
		}
		public static function deleteStudentCourse($data) {
			require_once "Institute.php";
			$c = new Institute();
			$result = $c->deleteStudentCourse($data);
			return $result;
		}

		// to get Student Joined Courses
		public static function getStudentJoinedCoursesNew($data) {
			require_once "Student.php";
			 $u = new Student();
			$result = $u->getStudentCoursesNew($data);
			return $result;
		}
		public static function getStudentSubsCoursesNew($data) {
			require_once "Student.php";
			 $u = new Student();
			$result = $u->getStudentSubsCoursesNew($data);
			return $result;
		}
		public static function saveProfileNew($data) {
			if($data->userRole == '1') {
				require_once "Institute.php";
				$i = new Institute();
				$result = $i->saveProfileNew($data);
			} else if($data->userRole == '2') {
				require_once "Professor.php";
				$p = new Professor();
				$result = $p->saveProfileNew($data);
			} else if($data->userRole == '3') {
				require_once "Publisher.php";
				$p = new Publisher();
				$result = $p->saveProfileNew($data);
			} else if($data->userRole == '4') {
				require_once "Student.php";
				$s = new Student();
				$result = $s->saveProfileNew($data);
			}
			return $result;
		}
		public static function getCountriesList() {
			require_once "User.php";
			$u = new User();
			$result = $u->getCountriesList();
			return $result;
		}

		public static function saveUserProfilePic($imagePath, $userId) {
			$u = new User();
			$result = '';
			$userRole = $u->getRoleId($userId);
			$userRole = $userRole['roleId'];
			if($userRole == 0)
				return "user not found";
			if($userRole == '1'){
				require_once "Institute.php";
				$i = new Institute();
				$result = $i->saveProfileImage($imagePath, $userId);
			} else if($userRole == '2'){
				require_once "Professor.php";
				$p = new Professor();
				$result = $p->saveProfileImage($imagePath, $userId);
				
			} else if($userRole == '3'){	
				require_once "Publisher.php";
				$p = new Publisher();
				$result = $p->saveProfileImage($imagePath, $userId);
			}
			else if($userRole == '4'){	
				require_once "Student.php";
				$s = new Student();
				$result = $s->saveProfileImage($imagePath, $userId);
			}
			return $result;
		}

		public static function removeProfileImage($data) {
			$u = new User();
			$result		= '';
			$userId		= $data->userId;
			$userRole	= $data->userRole;
			if($userRole == '1'){
				require_once "Institute.php";
				$i = new Institute();
				$result = $i->removeProfileImage($userId);
			} else if($userRole == '2'){
				require_once "Professor.php";
				$p = new Professor();
				$result = $p->removeProfileImage($userId);
				
			} else if($userRole == '3'){	
				require_once "Publisher.php";
				$p = new Publisher();
				$result = $p->removeProfileImage($userId);
			}
			else if($userRole == '4'){	
				require_once "Student.php";
				$s = new Student();
				$result = $s->removeProfileImage($userId);
			}
			return $result;
		}
		
		public static function sanitize($string) {
			$string = preg_replace('/[^-a-zA-Z0-9_]/', '', $string);
			return $string;
		}

		public static function fixCourseStudentsToSubjects() {
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->fixCourseStudentsToSubjects();
			return $result;
		}

		public static function setContentNotes($data) {
			require_once "StudentContent.php";
			$s = new StudentContent();
			$result = $s->setContentNotes($data);
			return $result;
		}

		public static function getContentNotes($data) {
			require_once "StudentContent.php";
			$s = new StudentContent();
			$result = $s->getContentNotes($data);
			return $result;
		}
		public static function queryTags($data) {
			require_once "Tags.php";
			$t = new Tags();
			$result = $t->queryTags($data);
			return $result;
		}
		public static function queryCourseTags($data) {
			require_once "Tags.php";
			$t = new Tags();
			$result = $t->queryCourseTags($data);
			return $result;
		}
		public static function tagExamAnalysis($data)
		{
			require_once "Tags.php";
			$t = new Tags();
			$result = $t->tagExamAnalysis($data);
			return $result;
		}
		public static function getExamTags($data)
		{
			require_once "Tags.php";
			$t = new Tags();
			$result = $t->getExamTags($data);
			return $result;
		}
		public static function exitCTA($data) {
			$u = new User();
			$result = $u->exitCTA($data);
			return $result;
		}
		public static function notifyParents($data)
		{
			require_once "Subject.php";
			$t = new Subject();
			$result = $t->notifyParents($data);
			return $result;
		}
		public static function getParentNotifications($data)
		{
			require_once "Parents.php";
			$t = new Parents();
			$result = $t->getParentNotifications($data);
			return $result;
		}
		public static function getSmileys()
		{
			$u = new User();
			$result = $u->getSmileys();
			return $result;
		}
		public static function savePercentBracket($data)
		{
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->savePercentBracket($data);
			return $result;
		}
		public static function savePercentBracketEdit($data)
		{
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->savePercentBracketEdit($data);
			return $result;
		}
		public static function deletePercentBracket($data)
		{
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->deletePercentBracket($data);
			return $result;
		}
		public static function getPercentRanges($data)
		{
			require_once "Exam.php";
			$e = new Exam();
			$result = $e->getPercentRanges($data);
			return $result;
		}
		public static function getSubjectAllStuff($data)
		{
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getSubjectAllStuff($data);
			return $result;
		}
		public static function saveSubjectGrading($data)
		{
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->saveSubjectGrading($data);
			return $result;
		}
		public static function saveSubjectReward($data)
		{
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->saveSubjectReward($data);
			return $result;
		}
		public static function getStudentPerformance($data)
		{
			require_once "Subject.php";
			$s = new Subject();
			$result = $s->getStudentPerformance($data);
			return $result;
		}
		public static function checkNameForSubmission($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->checkNameForSubmission($req);
			return $result;
		}
		public static function addSubmission($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->addSubmission($req);
			return $result;
		}
		public static function editSubmission($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->editSubmission($req);
			return $result;
		}
		public static function getSubmissionDetail($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->getSubmissionDetail($req);
			return $result;
		}
		public static function saveSubmissionQuestion($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->saveSubmissionQuestion($req);
			return $result;
		}
		public static function editSubmissionQuestion($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->editSubmissionQuestion($req);
			return $result;
		}
		public static function getSubmissionQuestionDetail($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->getSubmissionQuestionDetail($req);
			return $result;
		}
		public static function deleteSubmissionQuestion($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->deleteSubmissionQuestion($req);
			return $result;
		}
		public static function getSubjectSubmissionQuestions($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->getSubjectSubmissionQuestions($req);
			return $result;
		}
		public static function getSubmissionQuestions($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->getSubmissionQuestions($req);
			return $result;
		}
		public static function getSubjectSubmissions($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->getSubjectSubmissions($req);
			return $result;
		}
		public static function getSubmissionQuestionsImport($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->getSubmissionQuestionsImport($req);
			return $result;
		}
		public static function liveSubmission($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->liveSubmission($req);
			return $result;
		}
		public static function unliveSubmission($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->unliveSubmission($req);
			return $result;
		}
		public static function importSubmissionQuestions($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->importSubmissionQuestions($req);
			return $result;
		}
		public static function attemptSubmission($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->attemptSubmission($req);
			return $result;
		}
		public static function submissionAnswer($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->submissionAnswer($req);
			return $result;
		}
		public static function submitSubmissionAttempt($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->submitSubmissionAttempt($req);
			return $result;
		}
		public static function getCheckSubmissionStudents($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->getCheckSubmissionStudents($req);
			return $result;
		}
		public static function getCheckSubmissionQuestions($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->getCheckSubmissionQuestions($req);
			return $result;
		}
		public static function saveReviewForSubmission($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->saveReviewForSubmission($req);
			return $result;
		}
		public static function saveMarksForSubmission($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->saveMarksForSubmission($req);
			return $result;
		}
		public static function markSubmissionChecked($req) {
			require_once "Submission.php";
			$e = new Submission();
			$result = $e->markSubmissionChecked($req);
			return $result;
		}
		public static function getSubmissionAttempt($req) {
			require_once "StudentExam.php";
			$e = new StudentExam();
			$result = $e->getSubmissionAttempt($req);
			return $result;
		}
		//to fetch exam attempts for graph creation
		public static function getSubmissionAttemptGraph($req) {
			require_once "Submission.php";
			$r = new Submission();
			$result = $r->getSubmissionAttemptGraph($req);
			return $result;
		}
		public static function getSubmissionTimeLine($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getSubmissionTimeLine($req);
			return $result;
		}
		public static function getSubmissionTopperDetails($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getSubmissionTopperDetails($req);
			return $result;
		}
		public static function getSubmissionNormalizationGraph($req) {
			require_once "Result.php";
			$r = new Result();
			$result = $r->getSubmissionNormalizationGraph($req);
			return $result;
		}
		public static function getSubjectStudentGroups($req) {
			require_once "Subject.php";
			$r = new Subject();
			$result = $r->getSubjectStudentGroups($req);
			return $result;
		}
		public static function generateSubjectStudentGroups($req) {
			require_once "Subject.php";
			$r = new Subject();
			$result = $r->generateSubjectStudentGroups($req);
			return $result;
		}
		public static function addStudentSubjectStudentGroup($req) {
			require_once "Subject.php";
			$r = new Subject();
			$result = $r->addStudentSubjectStudentGroup($req);
			return $result;
		}
		public static function removeStudentSubjectStudentGroup($req) {
			require_once "Subject.php";
			$r = new Subject();
			$result = $r->removeStudentSubjectStudentGroup($req);
			return $result;
		}
		public static function addSubjectStudentGroup($req) {
			require_once "Subject.php";
			$r = new Subject();
			$result = $r->addSubjectStudentGroup($req);
			return $result;
		}
		public static function removeSubjectStudentGroups($req) {
			require_once "Subject.php";
			$r = new Subject();
			$result = $r->removeSubjectStudentGroups($req);
			return $result;
		}
		public static function favoriteCourse($req) {
			require_once "Course.php";
			$r = new Course();
			$result = $r->favoriteCourse($req);
			return $result;
		}
		public static function unfavoriteCourse($req) {
			require_once "Course.php";
			$r = new Course();
			$result = $r->unfavoriteCourse($req);
			return $result;
		}
		public static function getAccountTerminology($req) {
			require_once "User.php";
			$r = new User();
			$result = $r->getAccountTerminology($req);
			return $result;
		}
		public static function setAccountTerminology($req) {
			require_once "User.php";
			$r = new User();
			$result = $r->setAccountTerminology($req);
			return $result;
		}
		public static function getDeletedStuff($req) {
			require_once "Course.php";
			$r = new Course();
			$result = $r->getDeletedStuff($req);
			return $result;
		}
		public static function saveWalletAddress($req) {
			require_once "CryptoWallet.php";
			$r = new CryptoWallet();
			$result = $r->saveWalletAddress($req);
			return $result;
		}
		public static function getIGROContractData($req) {
			require_once "CryptoWallet.php";
			$r = new CryptoWallet();
			$result = $r->getIGROContractData($req);
			return $result;
		}
		public static function saveCryptoTransaction($req) {
			require_once "CryptoWallet.php";
			$r = new CryptoWallet();
			$result = $r->saveCryptoTransaction($req);
			return $result;
		}
		public static function checkIGROTransactions() {
			require_once "CryptoWallet.php";
			$r = new CryptoWallet();
			$result = $r->checkIGROTransactions();
			return $result;
		}
		public static function deletePersonalData($req) {
			require_once "User.php";
			$r = new User();
			$result = $r->deletePersonalData($req);
			return $result;
		}
		public static function deleteAccount($req) {
			require_once "User.php";
			$r = new User();
			$result = $r->deleteAccount($req);
			return $result;
		}
	}
?>