<?php

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "Base.php";

class StudentInvitation extends Base {

    //fuction called when institute invites student for course by  mail_id
    public function send_activation_link_student($data) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $adapter = $this->adapter;

            if (empty($data->emailNote)) {
                $result->status = 0;
                $result->message = "Email Template can't be empty";
            }

			$result = $this->checkRestrictionAndWarningOfCourse($data);
			if($result->status == 0)
				return $result;
            $search_id = $this->fetch_last_id_before_insert();
            $emails = array_map('trim', $data->email);
            $insert = "INSERT INTO invited_student_details (course_id,inviter_id,email_id,enrollment_type) VALUES";
            $notSend = array();
            foreach ($emails as $key => $email) {
                if ($this->check_invitations_mail($email, $data->courseId) == true) {
                    $insert .= "('{$data->courseId}','{$data->userId}','{$email}',{$data->enrollment_type}),";
                } else {
                    $notSend[] = $email;
                    unset($emails[$key]);
                }
            }
            $insert = substr($insert, 0, strlen($insert) - 1);
            if (count($emails) == 0) {
                $result->status = 0;
                $result->message = 'Student already enrolled.';
                return $result;
            }
            $adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();
            if (count($notSend) != 0) {
                $result->notSend = $notSend;
            }
            $result->status = 1;
            $result->message = 'Students invited successfully';
            $this->send_invitation_emails($emails, $search_id, $data);
            return $result;
        } catch (Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->message = 'Some error occurred.';
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function check_invitations_mail($email, $courseId) {
        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
			//if each student has already purchased the course student need to skip this invitation
			$query = "SELECT sc.id FROM student_course sc JOIN login_details ls ON ls.email='{$email}'  AND sc.user_id=ls.id JOIN user_roles ur ON ur.userId=ls.id AND ur.roleId=4 WHERE course_id={$courseId};";
			$temail = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $temail = $temail->toArray();
            if (count($temail) != 0) {
                return false;
			}
			//if invitation alredy send is pending or accepted student need to skip this invitation
            $query="SELECT id from invited_student_details where email_id='{$email}' and course_id={$courseId} and (status=0 OR status=1);";
            // $select = $sql->select();
            // $select->from('invited_student_details');
            // $select->columns(array('id'));
            // $select->where->equalTo('email_id', $email);
            // $select->where->equalTo('course_id', $courseId);
            // $select->where->equalTo('status', $courseId);
            // $selectString = $sql->getSqlStringForSqlObject($select);
            $email = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $email = $email->toArray();
            if (count($email) == 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function keys_avilable($data) {
        try {
            $result = new stdClass();
            $keys = $this->count_courseKeys($data);
            $invitaion = $this->count_invitation_pending($data);
            $result->keys_pending = $keys->keys;
            $result->invitation = $invitaion->invitations[0]['invitations'];
            $keys_avilable = $result->keys_pending - $result->invitation;
            $result->keys = $keys_avilable;
            $result->status = 1;
            $result->message = "You can send $keys_avilable invitations";
            return $result;
        } catch (Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function count_courseKeys($data) {
        try {
            $result = new stdClass();
            $adapter = $this->adapter;
            //$query = "select COUNT(key_id) as keys1 from course_key_details where userId='{$data->userId}' and  active_date LIKE '0000-00-00 00:00:00'";
            $query = "SELECT COUNT(key_id) as keys1 from course_key_details where userId='{$data->userId}' and (active_date IS NULL OR active_date LIKE '0000-00-00 00:00:00')";
            $keys = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $keys = $keys->toArray();
            $result->keys = $keys[0]['keys1'];
            return $result;
        } catch (Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function count_invitation_pending($data) {
        try {
            $result = new stdClass();
            $adapter = $this->adapter;
            $query = "SELECT COUNT(id) as invitations from invited_student_details where inviter_id='{$data->userId}'  and  status=0 ";
            $invitations = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $result->invitations = $invitations->toArray();
            return $result;
        } catch (Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function send_invitation_emails($emails, $search_id, $data) {
        $adapter = $this->adapter;
        $result = new stdClass();
        require_once "User.php";
        $c = new User();
        global $siteBase;

        /*var_dump($emails);
        var_dump($search_id);
        var_dump($data);*/

        // checking   if  link is sent for first time or by resent the link of course invitation
        if ($search_id != 0) {
            $condition = " id >$search_id AND ";
            $course = $this->getCourseAndInstituteName($data->courseId);
            //var_dump($course);
            $course = $course->institute;
            $courseId = $data->courseId;
            $coursename = $course[0]['course'];
            $institute = $course[0]['institute'];
            $institutemail = $course[0]['institutemail'];
            $instituteId = $course[0]['instituteId'];
            switch($course[0]['roleId']) {
                case 1:
                    $pro = "Institute";
                    $proLink = "{$siteBase}marketplace/institute/{$instituteId}";
                    //$proLink = "{$siteBase}student/instituteProfile.php?instituteId={$instituteId}";
                    break;
                case 2:
                    $pro = "Professor";
                    $proLink = "{$siteBase}marketplace/instructor/{$instituteId}";
                    //$proLink = "{$siteBase}student/professorProfile.php?professorId={$instituteId}";
                    break;
                case 3:
                    $pro = "Publisher";
                    break;
            }
        } else {
            //  link is resent  
            $condition = " ";
            $course = $this->getDetailsFromInvationLinkid($data->link_id);
            $course = $course->details;
            //var_dump($course);
            $courseId = $course[0]['courseId'];
            $coursename = $course[0]['course'];
            $institute = $course[0]['institute'];
            $institutemail = $course[0]['institutemail'];
            $instituteId = $course[0]['instituteId'];
            switch($course[0]['roleId']) {
                case 1:
                    $pro = "Institute";
                    $proLink = "{$siteBase}marketplace/institute/{$instituteId}";
                    //$proLink = "{$siteBase}student/instituteProfile.php?instituteId={$instituteId}";
                    break;
                case 2:
                    $pro = "Professor";
                    $proLink = "{$siteBase}marketplace/instructor/{$instituteId}";
                    //$proLink = "{$siteBase}student/professorProfile.php?professorId={$instituteId}";
                    break;
                case 3:
                    $pro = "Publisher";
                    break;
            }
        }

        $subjectStudent = "Enrollment to Course $coursename (Course ID: $courseId)";
        $emailink = '';
        foreach ($emails as $email) {
            $query = "SELECT id from invited_student_details WHERE $condition email_id='{$email}' ORDER BY id DESC LIMIT 1";
            $last_id = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $last_id = $last_id->toArray();
            $link_id = $last_id[0]['id'];
            $link = $siteBase . "student/notifications/activity";
            //$link = $siteBase . "index.php?notify={$link_id}";
            $registerLink = $siteBase . 'signup';
            //$registerLink = $siteBase . 'signup-student.php#student';

            $query="SELECT concat(firstName, ' ',lastName) name,l.id   from student_details s
                    JOIN  login_details l on l.id=s.userId
                    WHERE  l.email='$email' ";
            $student = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $student = $student->toArray();
            if (count($student) == 0) {
                $studentname = "$email";
                $studentId = "$email";
            } else {
                $studentname = $student[0]['name'];
                $studentId = $student[0]['id'];
            }
            /*if (isset($data->emailNote) && !empty($data->emailNote)) {
                $emailtostudent .= $data->emailNote."<br> <br>";
            } else {
                $emailtostudent = "Dear Student, <br> <br>";
            }
            $emailtostudent.= "You have been invited to join Course <strong>$coursename</strong> ( Course ID: $courseId ) by the {$pro} <strong>$institute</strong> ( Institute Id: $instituteId ) <br> <br>";
            $emailtostudent.= 'Please create a student account <a href="' .$registerLink . '">"Click here"</a> <br> <br>';
            $emailtostudent.= "Best Regards,<br>Integro Team<br>Integro.io";*/
            if (isset($data->emailNote) && !empty($data->emailNote)) {
                $courseInviteBlock = "<a href=\"{$registerLink}\">Click here to join</a><br>
                                Course: $coursename ( Course ID: $courseId )<br>
                                Institute: $institute ( Institute Id: $instituteId )<br>";
                $data->emailNote = str_replace("[course_invite_block]", $courseInviteBlock, $data->emailNote);
                $emailtostudent = $data->emailNote;
            } else {
                $emailtostudent = "Dear Student, <br> <br>";
                $emailtostudent.= "You have been invited to join Course <strong>$coursename</strong> ( Course ID: $courseId ) by the {$pro} <strong>$institute</strong> ( Institute Id: $instituteId ) <br> <br>";
                $emailtostudent.= 'Please create a student account <a href="' .$registerLink . '">"Click here"</a> <br> <br>';
                $emailtostudent.= "Best Regards,<br>Integro Team<br>Integro.io";
            }
           /* $emailtostudent .= "You are about to be enrolled for the Course <strong>$coursename</strong> ( Course ID: $courseId ) on <a href='{$siteBase}' >www.integro.io</a> by the {$pro} <a href='$proLink'><strong>$institute</strong></a> ( Institute Id: $instituteId ) <br> <br>";
            $emailtostudent .= "Please confirm your enrollment to start learning  <br> <br>";
            $emailtostudent .= "If you already have a Student Account on <a href='{$siteBase}' >www.integro.io </a> , please click on the below link and accept the Course invitation <br> <br>";
            // $emailtostudent .= "Please confirm your enrollment to start learning  <br> <br>";
            $emailtostudent .= '<a href="' . $link . '" >Click here to accept the enrollment for the Course '.$coursename.' </a>  <br> <br>';
            $emailtostudent .= 'If you do not have a Student Account on <a href="{$siteBase}" >www.integro.io </a>, please click on the below link to create an account. <br> <br>';
            $emailtostudent .= '<a href="' .$registerLink . '">Click here to create a Student Account  </a>  <br> <br>';
            $emailtostudent .= 'Once you Register and log in, please accept the Course Invite to be officially enrolled.  <br> <br>';*/
            // $subject = 'Course Verification';

            $instituteSubject = "Enrollment Request for $studentname ";
            $emailInstitute = "Dear $institute, <br> <br>";
            $emailInstitute .= "Your {$pro} $institute ({$pro} Id: $instituteId ) has sent an enrollment Request to Student <strong>{$studentname}</strong> (Student ID : {$studentId}) for Course <strong>$coursename</strong> ( Course ID: $courseId ) on <a href='{$siteBase}' >www.integro.io<a><br> <br>";
            $emailInstitute .= "Please write to us at <a href='mailto:admin@integro.io' >admin@integro.io<a>, if you have some queries or if you did not initiate the request. Please mention your {$pro} Id $instituteId in the email  <br> <br>";

            if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {

                if (isset($data->emailNote) && !empty($data->emailNote)) {
                    $c->sendMail2($email, $subjectStudent, $emailtostudent, $institute);
                } else {
                    $c->sendMail($email, $subjectStudent, $emailtostudent);
                }

                $c->sendMail($institutemail, $instituteSubject, $emailInstitute);

            }

            $result->status = 1;
        }
    }
	
	public function resendPendingInvitation($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			//getting the details of invitation with course and other details
			$select = $sql->select();
			$select->from(array('isd' => 'invited_student_details'));
			$select->where(array('isd.id' => $data->invitationId));
			$select->join(
				array('id' => 'institute_details'),
				'id.userId=isd.inviter_id',
				array(
					'instituteId' => 'userId',
					'instituteName' => 'name'
				)
			);
			$select->join(
				array('c' => 'courses'),
				'isd.course_id=c.id',
				array(
					'courseName' => 'name'
				)
			);
			$select->columns(array(
				'id' => 'id',
				'courseId' => 'course_id',
				'email' => 'email_id'
			));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$invitation = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$invitation = $invitation->toArray();
			if(count($invitation) > 0) {
				global $siteBase;
				require_once 'User.php';
				$u = new User();
				$invitation = $invitation[0];
				$email = '';
				$email = "Dear Student, <br> <br>
				You are about to be enrolled for the Course {$invitation['courseName']} ( Course ID: {$invitation['courseId']} ) on <a href='{$siteBase}' >www.integro.io <a> by the Institute {$invitation['instituteName']} ( Institute Id: {$invitation['instituteId']} ) <br> <br>";
				$email .= "Please confirm your enrolment to start learning  <br> <br>";
				$email .= "If you already have a Student Account on <a href='{$siteBase}'>www.integro.io <a> , please click on the below link and accept the Course invitation <br> <br>";
				$email .= "<a href='{$siteBase}index.php?notify={$invitation['id']}' >Click here to accept the enrolment for the Course {$invitation['courseName']} <a>  <br> <br>";
				$email .= "If you do not have a Student Account on <a href='{$siteBase}'>www.integro.io <a>, please click on the below link to create an account. <br> <br>";
				$email .= "<a href='{$siteBase}signup' >Click here to create a Student Account  <a>  <br> <br>";
				$email .= "Once you Register and log in, please accept the Course Invite to be officially enrolled.  <br> <br>";
				
				$u->sendMail($invitation['email'], "Enrollment to Course {$invitation['courseName']} (Course ID: {$invitation['courseId']} )", $email);
			}
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

    public function fetch_last_id_before_insert() {
        $adapter = $this->adapter;
        $last_id = $adapter->query("SELECT id from invited_student_details order by id desc limit 1", $adapter::QUERY_MODE_EXECUTE);
        $last_id = $last_id->toArray();
        if(count($last_id)==0){
            return -1;
        }
        return $last_id[0]['id'];
    }

    //function to check verify the current user having the activation link
    public function verify_activation_link_student($data) {
        $adapter = $this->adapter;
        $result = new stdClass();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('login_details');
        $select->columns(array('email'));
        $select->where->equalTo('id', $data->userId);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $email = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
        $email = $email->toArray();
        $current_user_email = $email[0]['email'];

        $select = $sql->select();
        $select->from('invited_student_details');
        $select->columns(array('email_id'));
        $select->where->equalTo('id', $data->invitation_link_id);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $email = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
        $email = $email->toArray();
        $invitation_email = $email[0]['email_id'];
        if ($invitation_email == $current_user_email) {
            $result->status = 1;
            $result->message = ' User is validated !';
        } else {
            $result->status = 0;
            $result->message = ' User is Not Registered !';
        }

        return $result;
    }

    public function student_invitation_details($data) {
        try {
            $adapter = $this->adapter;
            $result = new stdClass();
            $query = "SELECT i.id, i.course_id, i.inviter_id, i.email_id, CONCAT(u.contactMobilePrefix,'-',u.contactMobile) contactMobile, i.enrollment_type, i.status, i.timestamp, c.name AS courseName, CONCAT(s.firstName,' ',s.lastName) AS studentName, sc.timestamp AS signup_time
                FROM invited_student_details AS i
                INNER JOIN courses AS c ON c.id=i.course_id
                LEFT OUTER JOIN login_details AS l ON l.email=i.email_id and l.validated=1
                LEFT OUTER JOIN user_roles AS ur ON l.id=ur.userId AND ur.roleId=4
                LEFT OUTER JOIN student_details AS s ON s.userId = ur.userId
                LEFT OUTER JOIN user_details u ON s.userId = u.userId
                LEFT OUTER JOIN student_course sc ON sc.user_id = u.userId AND sc.course_id = i.course_id
                WHERE inviter_id=$data->userId AND i.`status`!=3
                ORDER BY studentName desc";
            $invitations = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $result->invitations = $invitations->toArray();
            if (count($result->invitations) == 0) {
                $result->status = 0;
                $result->message = " No invitations Found ";
                return $result;
            }
            return $result;
        } catch (Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function resend_invitation_link($data) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $sql2 = new Sql($this->adapter);
            $update2 = $sql2->update();
            $update2->table('invited_student_details');
            $update2->set(array('status' => 3));
            $update2->where(array('id' => $data->link_id));
            $statement2 = $sql2->prepareStatementForSqlObject($update2);
            $result2 = $statement2->execute();


            $adapter = $this->adapter;
            $query = "INSERT INTO invited_student_details(course_id,inviter_id,email_id,enrollment_type) SELECT  course_id,inviter_id,email_id,enrollment_type  FROM invited_student_details where id={$data->link_id }";
            $invitations = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();

            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('invited_student_details');
            $select->columns(array('email_id'));
            $select->order('id DESC');
            $select->limit(1);
            $selectString = $sql->getSqlStringForSqlObject($select);
            $email = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
            $email = $email->toArray();
            $email = $email[0];
            // calling function  by passing email array for resending mail.

            $result->email_status = $this->send_invitation_emails($email, 0, $data);
            $result->status = 1;
            $result->message = 'activation link resent !';
            return $result;
        } catch (Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }

    public function load_student_notification($data) {
        try {
            $adapter = $this->adapter;
            $result = new stdClass();
            $query = "SELECT distinct i.id, i.course_id, i.inviter_id, i.email_id, i.enrollment_type, i.status, i.timestamp, c.name AS courseName
					FROM invited_student_details AS i  JOIN courses AS c ON c.id=i.course_id  JOIN login_details AS l 
					ON l.email=i.email_id and l.validated=1  JOIN user_roles AS ur ON l.id=ur.userId AND ur.roleId=4 WHERE  i.status=0
					and i.id={$data->link_id}  order by i.timestamp DESC";

            $invitations = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $result->invitations = $invitations->toArray();
            if (count($result->invitations) == 0) {
                $result->status = 0;
                $result->message = " No invitations Found ";
                return $result;
            }
            return $result;
        } catch (Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function accepted_student_invitation($data) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql2 = new Sql($this->adapter);
			
			//before accepting the invitation we need to check if the invitation is still pending.
			$select = $sql2->select();
			$select->from('invited_student_details');
			$select->where(array(
				'status'	=>	0,
				'id'		=>	$data->link_id
			));
			$select->columns(array('id'));
			$string = $sql2->getSqlStringForSqlObject($select);
            $temp = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
			$temp = $temp->toArray();
			if(count($temp) == 0) {
				$result->status = 0;
				$result->message = 'Some error occurred please refresh the page and try again.';
				return $result;
			}
            $update2 = $sql2->update();
            $update2->table('invited_student_details');
            $update2->set(array('status' => 1));
            $update2->where(array('id' => $data->link_id));
            $statement2 = $sql2->prepareStatementForSqlObject($update2);
            $result2 = $statement2->execute();
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();

            $result->invitation = $this->after_accepted_invitation($data);

            require_once "User.php";
            $c = new User();
            global $siteBase;

            $course = $this->getDetailsFromInvationLinkid($data->link_id);
            $course = $course->details;
            $courseId = $course[0]['courseId'];
            $coursename = $course[0]['course'];
            $institute = $course[0]['institute'];
            $institutemail = $course[0]['institutemail'];
            $instituteId = $course[0]['instituteId'];
            $studentmailId = $course[0]['studentemail'];

            $query = "SELECT concat(firstName, ' ', lastName) name, l.id
                        FROM student_details s
                        INNER JOIN login_details l on l.id=s.userId
                        WHERE  l.email='$studentmailId' ";
            $student = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $student = $student->toArray();
            if (count($student) == 0) {
                $studentname = "$studentmailId";
                //$studentId = '';
            } else {
                $studentname = $student[0]['name'] . "( Student ID : {$student[0]['id']} )";
                // $studentId = $student[0]['id'];
            }

            $arcanemind = $this->getArcanemindemail();
            $arcanemind = $arcanemind->arcanemind;
            $arcanemindEmail = $arcanemind[0]['email'];

            // get keys available 
            $data = new stdClass();
            $data->userId = $instituteId;
            $keyLeft = $this->keys_avilable($data);
            // var_dump($keyLeft);
            $keysLeft = $keyLeft->keys;
            
            // insert notification
            $notification="Student {$studentname} has accepted the course key invitation. He is enrolled to the course ".$this->safeString($coursename)." ( course id : $courseId ).";
            $not = new TableGateway('notifications', $adapter, null, new HydratingResultSet());
            $insert = array(
                'userId'    =>  $instituteId,
                'message'   =>  $notification
            );
            $not->insert($insert);

            $arcanemindSubject = "Confirmation of Student enrollment";
            $emailarcanemind = "Student $studentname has been enrolled for the Course $coursename (Course ID : $courseId ) taught by $institute (Institute Id : $instituteId) , <br> <br>";

            $emailarcanemind .= "$keysLeft Course Keys  are now left in the institute Account <br> <br>";
            // $subject = 'Course Verification';

            //fetching total students to send to institute
            $query = "(SELECT COUNT(DISTINCT user_id) AS purchased FROM student_course WHERE course_id={$courseId} AND key_id=0) UNION ALL (SELECT COUNT(DISTINCT user_id) AS purchased FROM student_course WHERE course_id={$courseId} AND key_id!=0)";
            $total = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $total = $total->toArray();
            $purchased = $total[0]['purchased'];
            $associated = $total[1]['purchased'];
            $total = $purchased + $associated;

            $instituteSubject = "Confirmation of Student Enrollment";
            $emailInstitute = "Dear $institute, <br> <br>";
            $emailInstitute .= "Congrats! Student <strong>{$studentname}</strong> has joined your Course <strong>{$coursename}</strong> through Course Key allocation<br><br>";
            $emailInstitute .= "You have Now  $keysLeft Course Keys left in your institute Account. To purchase more keys, please send us an email at contactus@integro.io  or Contact us at  +91-9741436024<br><br>";
            $emailInstitute .= "<table border=1 style='width: 100%;'><tr><td>Your Course</td><td><strong>{$coursename}</strong></td></tr><tr><td>Student Name</td><td><strong>{$studentname}</strong></td></tr><tr><td>Students via Course Keys</td><td><strong>{$associated}</strong></td></tr><tr><td>Students via Marketplace</td><td><strong>{$purchased}</strong></td></tr><tr><td>Total Students</td><td><strong>{$total}</strong></td></tr></table><br><br>";
            $emailInstitute .= "To view the entire list of Students who have enrolled or still not confirmed their enrollment, please Login on <a href='{$siteBase}' >www.integro.io <a>";

            if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
                $c->sendMail($institutemail, $instituteSubject, $emailInstitute);
                $c->sendMail($arcanemindEmail, $arcanemindSubject, $emailarcanemind);
            }

            //var_dump($emailInstitute);
            // var_dump($emailarcanemind);

            $result->status = 1;
            $result->message = 'Link is accepted !';
            return $result;
        } catch (Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function reject_student_invitation($data) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql2 = new Sql($this->adapter);
			
			//before rejecting the invitation we need to check if the invitation is still pending.
			$select = $sql2->select();
			$select->from('invited_student_details');
			$select->where(array(
				'status'	=>	0,
				'id'		=>	$data->link_id
			));
			$select->columns(array('id'));
			$string = $sql2->getSqlStringForSqlObject($select);
			$temp = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
			$temp = $temp->toArray();
			if(count($temp) == 0) {
				$result->status = 0;
				$result->message = 'Some error occurred please refresh the page and try again.';
				return $result;
			}
            $update2 = $sql2->update();
            $update2->table('invited_student_details');
            $update2->set(array('status' => 2));
            $update2->where(array('id' => $data->link_id));
            $statement2 = $sql2->prepareStatementForSqlObject($update2);
            $result2 = $statement2->execute();
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();

            // for sending mail
            require_once "User.php";
            $c = new User();
            global $siteBase;

            $course = $this->getDetailsFromInvationLinkid($data->link_id);
            $course = $course->details;
            $courseId = $course[0]['courseId'];
            $coursename = $course[0]['course'];
            $institute = $course[0]['institute'];
            $institutemail = $course[0]['institutemail'];
            $instituteId = $course[0]['instituteId'];
            $studentmailId = $course[0]['studentemail'];

            $query = "SELECT concat(firstName, ' ',lastName) name,l.id   from student_details s
                        JOIN  login_details l on l.id=s.userId
                        WHERE  l.email='$studentmailId' ";
            $student = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $student = $student->toArray();
            if (count($student) == 0) {
                $studentname = "$studentmailId";
                //$studentId = '';
            } else {
                $studentname = $student[0]['name'] . "( Student ID : {$student[0]['id']} )";
                // $studentId = $student[0]['id'];
            }

            $arcanemind = $this->getArcanemindemail();
            $arcanemind = $arcanemind->arcanemind;
            $arcanemindEmail = $arcanemind[0]['email'];

            // get keys available 
            $tmpData = new stdClass();
            $tmpData->userId = $instituteId;
            $keyLeft = $this->keys_avilable($tmpData);
            // var_dump($keyLeft);
            $keysLeft = $keyLeft->keys;
            
             // insert notification
            $notification="Student $studentname  has rejected  the course key invitation. He is not  enrolled to the course ".$this->safeString($coursename)." ( course id : $courseId ).";
            $query = "Insert into notifications (userId,message) values ($instituteId,'$notification')";
            $notification=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
     


            //  when institute cancel request 
            // mail sent to arcanemind 
            $arcanemindSubject = "Cancellation of Student  enrollment request";
            $emailarcanemind = "Institute $institute (Institute Id : $instituteId ) has cancelled the enrollment Request sent to   $studentname  for the Course $coursename (Course ID : $courseId ) . <br> <br>";


            // when  institute cancel request,  mail sent to institute
            $instituteSubject = "Cancellation of Student enrollment request  ";
            $emailInstitute = "Dear $institute, <br> <br>";
            $emailInstitute .= " Your Institute $institute (Institute Id : $instituteId ) has cancelled the enrollment Request sent to   $studentname has been enrolled for the Course  $coursename ( Course ID: $courseId ) on <a href='{$siteBase}' >www.integro.io <a>  <br> <br>";
            $emailInstitute .= "Please write to us at   <a href=mailto:admin@integro.io > admin@integro.io <a>.Please mention your Institute Id $instituteId in the email <br> <br>";

            // when  institute cancel request,  mail sent to student 
            $studentMailSubject = "Cancellation of Student enrollment request ";
            $studentMail = "Dear  Student , <br> <br>";
            $studentMail .= "Institute $institute (Institute Id : $instituteId ) has cancelled the enrollment Request, it sent you for the Course  $coursename ( Course ID: $courseId ) on <a href='{$siteBase}' >www.integro.io <a>  <br> <br>";
            $studentMail .= "Please contact the Institute at  $institutemail or write us an email at  <a href=admin@integro.io > admin@integro.io <a> .  <br> <br>";
            $studentMail .= "If you are already registered with us, please mention your Student ID or registered email id in the Email. ";

            // when  student cancel request,  mail sent to institute 
            $instituteSubjectRejectByStudent = "Request  rejected  for Course enrollment";
            $instituteMailRejectByStudent = "Dear  $institute , <br> <br>";
            $instituteMailRejectByStudent .= "Student $studentname has  rejected   the request  for enrollment  to the  Course  $coursename ( Course ID: $courseId ) on <a href='{$siteBase}' >www.integro.io <a>  <br> <br>";
            $instituteMailRejectByStudent .= " No course key has been consumed due to this rejection.   <br> <br>";
            $instituteMailRejectByStudent.= " $keysLeft course Keys  are now left in the institute Account. To purchase more keys, please send us an email at <a href=contactus@integro.io > contactus@integro.io <a>    or Contact us at  +91-9741436024";

            // checking invitation is rejected by student or Institute
            
            if ($data->userRole == 4) {
                // reject by student 
                 $c->sendMail($institutemail, $instituteSubjectRejectByStudent, $instituteMailRejectByStudent);
            } else {
                // reject by institute admin
                $c->sendMail($arcanemindEmail, $arcanemindSubject, $emailarcanemind);

                $c->sendMail($institutemail, $instituteSubject, $emailInstitute);

                $c->sendMail($studentmailId, $studentMailSubject, $studentMail);
            }

            $result->status = 1;
            $result->message = 'Link is rejected ! !';
            return $result;
        } catch (Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function after_accepted_invitation($data) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $timestamp = time();
            $result = new stdClass();
            $adapter = $this->adapter;
            // fetching course key for activation 

            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('invited_student_details');
            $select->columns(array('inviter_id', 'course_id'));
            $select->where->equalTo('id', $data->link_id);
            $selectString = $sql->getSqlStringForSqlObject($select);
            $invters = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
            $invters = $invters->toArray();
            $invters_id = $invters[0]['inviter_id'];
            $course_id = $invters[0]['course_id'];

            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('course_key_details');
            $select->columns(array('key_id'));
            //$select->where->equalTo('userId', $invters_id);
            //$select->where->equalTo('active_date', '0000-00-00 00:00:00');
            //$select->where->isNull('active_date');
            $select->where("userId = {$invters_id} AND (active_date IS NULL OR active_date LIKE '0000-00-00 00:00:00')");
            $selectString = $sql->getSqlStringForSqlObject($select);
            $course_key = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
            $result->course_key = $course_key->toArray();
            if (count($result->course_key) == 0) {
                $result->status = 0;
                $result->message = " No Course Key Found ";
                return $result;
            }

            $key_id = $result->course_key[0]['key_id'];
            $sql2 = new Sql($this->adapter);
            $update2 = $sql2->update();
            $update2->table('course_key_details');
            $update2->set(array('active_date' => new \Zend\Db\Sql\Expression('CURRENT_TIMESTAMP')));
            $update2->where(array('key_id' => $key_id));
            $statement2 = $sql2->prepareStatementForSqlObject($update2);
            $result2 = $statement2->execute();
            $result->status = 1;
            $result->message = 'key is activated !';

            //$student_course = new TableGateway('student_course', $adapter, null, new HydratingResultSet());
            $insert = array(
                'course_id' => $course_id,
                'user_id' => $data->userId,
                'key_id' => $key_id,
                'timestamp' => date("Y-m-d H:i:s"),
            );
            /*$student_course->insert($insert);
            if ($student_course->getLastInsertValue() == 0) {
                $result->status = 0;
                return $result;
            }*/
            $query="INSERT INTO student_course (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
            $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $adapter->query("SET @studentCourseId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);

            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('subjects');
            $select->columns(array('id'));
            $select->where(array('courseId' => $course_id, 'ownerId' => $invters_id));
            $selectString = $sql->getSqlStringForSqlObject($select);
            $subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $subjects = $subjects->toArray();
            if (count($subjects)>0) {
                foreach ($subjects as $key => $subject) {
                    $subjectId = $subject['id'];
                    $student_subject = new TableGateway('student_subject', $adapter, null, new HydratingResultSet());
                    $insert = array(
                        'courseId' => $course_id,
                        'userId' => $data->userId,
                        'subjectId' => $subjectId,
                        'Active' => 1
                    );
                    $student_subject->insert($insert);
                }
            }
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();
            $result->status = 1;
            return $result;
        } catch (Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function StudentDetailsWithCourses($data) {
        try {
            $adapter = $this->adapter;
            $result = new stdClass();
            $userid = $data->userId;
            if (isset($data->instituteId)) {
                $userid = $data->instituteId;
            }
            $query="SELECT l.id AS userId,CONCAT(IFNULL(s.firstName,''),' ', IFNULL(s.lastName,'')) name,l.username,l.email,u.contactMobile,l.temp,utp.password, s.profilePic
                    FROM login_details l
                    INNER JOIN student_course sc ON sc.user_id=l.id
                    INNER JOIN course_key_details ck ON ck.key_id=sc.key_id
                    LEFT JOIN user_details u ON u.userId=l.id
                    LEFT JOIN student_details as s ON l.id=s.userId
                    LEFT JOIN user_temp_passwords AS utp ON utp.userId=l.id
                    WHERE ck.userId=$userid
                    GROUP BY l.id";
            $students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $result->students = $students->toArray();
            if (count($result->students) == 0) {
                $result->status = 0;
                $result->message = "No Students Connected";
                return $result;
            }
            $c = array();
            foreach ($result->students as $key=>$students) {
                $query="SELECT distinct c.name
                        FROM student_course as sc
                        INNER JOIN courses c on c.id=sc.course_id
                        INNER JOIN course_key_details ck on ck.key_id=sc.key_id
                        WHERE ck.userId=$data->userId and sc.user_id={$students['userId']}";
                $courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
                $c[] = $courses->toArray();

                if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
                    require_once 'amazonRead.php';
                    $result->students[$key]['profilePic'] = getSignedURL($result->students[$key]['profilePic']);
                } else {
                    $result->students[$key]['profilePic'] = "http://p.imgci.com/db/PICTURES/CMS/263600/263697.20.jpg";
                }
            }
            $result->courses = $c;
            $result->status = 1;
            return $result;
        } catch (Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            //$result->query = $query;
            return $result;
        }
    }

    public function CourseforStudent($data) {
        try {
            $adapter = $this->adapter;
            $result = new stdClass();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('courses');
            $select->columns(array('id', 'name'));
            $select->where->equalTo('ownerId', $data->userId);
            $selectString = $sql->getSqlStringForSqlObject($select);
            $courses = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
            $result->courses = $courses->toArray();
            $result->status = 1;
            return $result;
        } catch (Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function getDetailsFromInvationLinkid($linkid) {
        try {
            $adapter = $this->adapter;
            $result = new stdClass();
            $query = " SELECT i.inviter_id instituteId,l.email institutemail, c.name course,c.id courseId ,email_id studentemail,ur.roleId,
                case 
            	when ur.roleId= 1 then (select name from institute_details where userId= l.id)
                    when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
                    when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
                    end as institute
                from invited_student_details i
                left join  courses c on c.id=i.course_id
                left join institute_details inst on i.inviter_id= inst.userId
                left join login_details l on l.id=i.inviter_id
                left join user_roles  ur on ur.userId=l.id
                where i.id=$linkid and ur.roleId not in (4,5)";

            $details = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $result->details = $details->toArray();
            $result->status = 1;
            return $result;
        } catch (Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
    public function getArcanemindemail() {
        try {
            $adapter = $this->adapter;
            $result = new stdClass();
            $query = "SELECT l.email from login_details l join user_roles u  on u.userId=l.id and u.roleId=5 order by l.id desc limit 1";
            $arcanemind = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $result->arcanemind = $arcanemind->toArray();
            $result->status = 1;
            return $result;
        } catch (Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
    public function getCourseAndInstituteName($courseId) {
        try {
            $adapter = $this->adapter;
            $result = new stdClass();
            $query = "SELECT c.name course,l.email institutemail,l.id instituteId, case 
        	when ur.roleId= 1 then (select name from institute_details where userId= l.id)
                when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
                when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
                end as institute, ur.roleId
            FROM courses c
            JOIN login_details l on c.ownerId=l.id
            JOIN user_roles  ur on ur.userId=l.id where c.id=$courseId and ur.roleId not in (4,5)";
            $institute = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $result->institute = $institute->toArray();
            $result->status = 1;
            return $result;
        } catch (Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }


    public function process_coursekey($data) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $timestamp = time();
            $result = new stdClass();
            $adapter = $this->adapter;
            // fetching course key for activation 

            require_once("Course.php");
            $c = new Course();
            $resCourseOwner = $c->getCourseOwner($data->courseId);
            $invters_id = $resCourseOwner->detail["userId"];
            $course_id = $data->courseId;

            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('course_key_details');
            $select->columns(array('key_id'));
            //$select->where->equalTo('userId', $invters_id);
            //$select->where->equalTo('active_date', '0000-00-00 00:00:00');
            //$select->where->isNull('active_date');
            $select->where("userId = {$invters_id} AND (active_date IS NULL OR active_date LIKE '0000-00-00 00:00:00')");
            $selectString = $sql->getSqlStringForSqlObject($select);
            $course_key = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
            $result->course_key = $course_key->toArray();
            if (count($result->course_key) == 0) {
                $result->status = 0;
                $result->message = " No Course Key Found ";
                return $result;
            }

            $key_id = $result->course_key[0]['key_id'];
            $sql2 = new Sql($this->adapter);
            $update2 = $sql2->update();
            $update2->table('course_key_details');
            $update2->set(array('active_date' => new \Zend\Db\Sql\Expression('CURRENT_TIMESTAMP')));
            $update2->where(array('key_id' => $key_id));
            $statement2 = $sql2->prepareStatementForSqlObject($update2);
            $result2 = $statement2->execute();
            $result->status = 1;
            $result->message = 'key is activated !';

            /*$insert = array(
                'course_id' => $course_id,
                'user_id' => $data->userId,
                'key_id' => $key_id,
                'timestamp' => date("Y-m-d H:i:s"),
            );
            $query="INSERT INTO student_course (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
            $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $adapter->query("SET @studentCourseId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);

            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('subjects');
            $select->columns(array('id'));
            $select->where(array('courseId' => $course_id, 'ownerId' => $invters_id));
            $selectString = $sql->getSqlStringForSqlObject($select);
            $subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $subjects = $subjects->toArray();
            if (count($subjects)>0) {
                foreach ($subjects as $key => $subject) {
                    $subjectId = $subject['id'];
                    $student_subject = new TableGateway('student_subject', $adapter, null, new HydratingResultSet());
                    $insert = array(
                        'courseId' => $course_id,
                        'userId' => $data->userId,
                        'subjectId' => $subjectId,
                        'Active' => 1
                    );
                    $student_subject->insert($insert);
                }
            }*/
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();
            $result->status = 1;
            return $result;
        } catch (Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
    public function linkStudentCourse($data, $orderId) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            $result = new stdClass();
            if (!isset($data->price)) {
                $data->price = 0;
            }
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('student_course');
            $select->where(array('course_id' => $data->courseId,'user_id' => $data->userId));
            $selectString = $sql->getSqlStringForSqlObject($select);
            $courseStudents = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $courseStudents = $courseStudents->toArray();
            if (count($courseStudents) == 0) {
                $query = "INSERT INTO student_course (course_id,user_id,key_id,price) VALUES ($data->courseId,$data->userId,'0',$data->price)";
                $institute = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);

                $course_id = $data->courseId;
                $sql = new Sql($adapter);
                $select = $sql->select();
                $select->from('subjects');
                $select->columns(array('id'));
                $select->where(array('courseId' => $course_id));
                $selectString = $sql->getSqlStringForSqlObject($select);
                $subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                $subjects = $subjects->toArray();
                if (count($subjects)>0) {
                    foreach ($subjects as $key => $subject) {
                        $subjectId = $subject['id'];
                        $student_subject = new TableGateway('student_subject', $adapter, null, new HydratingResultSet());
                        $insert = array(
                            'courseId' => $course_id,
                            'userId' => $data->userId,
                            'subjectId' => $subjectId,
                            'Active' => 1
                        );
                        $student_subject->insert($insert);
                    }
                }
                // If all succeed, commit the transaction and all changes are committed at once.
                $db->commit();

                //now sending an email to student
                //getting student details
                $select = $sql->select();
                $select->from(array('ld' => 'login_details'))->where(array('ld.id' => $data->userId))->columns(array('email'));
                $select->join(array('sd' => 'student_details'), 'sd.userId=ld.id', array('name' => new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)")), 'left');
                $selectString = $sql->getSqlStringForSqlObject($select);
                $student = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                $student = $student->toArray();
                $student = $student[0];
                
                if (isset($orderId) && !empty($orderId)) {
                    $currency = 'IGRO';
                    require_once 'CryptoWallet.php';
                    $w = new CryptoWallet();
                    $dataCW = new stdClass();
                    $dataCW->userId = $_SESSION['userId'];
                    $dataCW->userRole = $_SESSION['userRole'];
                    $res = $w->getIGROContractData($dataCW);
                    if ($res->status == 1) {
                        $currency = $res->displayCurrency;
                    }
                }

                $studentMail = $student['email'];
                
                //getting course student details
                $select = $sql->select();
                $select->from(array('c' => 'courses'))->where(array('c.id' => $data->courseId))->columns(array('id', 'name', 'studentPrice', 'studentPriceINR', 'ownerId'));
                $select->join(array('ur' => 'user_roles'), 'ur.userId = c.ownerId', array('roleId'));
                $select->join(array('ld' => 'login_details'), 'ld.id=c.ownerId', array('email'));
                $selectString = $sql->getSqlStringForSqlObject($select);
                $course = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                $course = $course->toArray();
                $course = $course[0];

                switch ($course['roleId']) {
                    case 1:
                        $table = 'institute_details';
                        $slt = 'name';
                        $institute = 'Institute ID : '.$course['ownerId'];
                        break;
                    case 2:
                        $table = 'professor_details';
                        $slt = "CONCAT(firstName, ' ', lastName)";
                        $institute = 'Professor ID : '.$course['ownerId'];
                        break;
                    case 3:
                        $table = 'publisher_details';
                        $slt = "CONCAT(firstName, ' ', lastName)";
                        $institute = 'Publisher ID : '.$course['ownerId'];
                        break;
                }
                $query = "SELECT {$slt} AS name FROM {$table} WHERE userId={$course['ownerId']}";
                $owner = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
                $owner = $owner->toArray();
                $owner = $owner[0];

                $mail = '';
                if (isset($orderId) && !empty($orderId)) {
                    $mail .= 'Dear Student,<br><br>';
                    $mail .= "Welcome to the Course <strong>{$course['name']}</strong> (Course ID: {$course['id']})<br>taught by <strong>{$owner['name']}</strong> ($institute)<br><br>";
                    $mail .= "<strong>Your Order Details are:<br>Order ID {$orderId}</strong><br><br>";
                    $mail .= "<table border=1 style='width: 100%;'><thead><tr><th>Item</th><th>Quantity</th><th>Total Paid</th></tr></thead><tbody><tr><td style='text-align: center;'>{$course['name']}</td><td style='text-align: center;'>1</td><td style='text-align: center;'>{$data->price} {$currency}</td></tr></tbody></table>";
                    $mail .= "<br><br>In case you need any help, please write to us at <a href='mailto:contactus@integro.io'>contactus@integro.io</a> or give a call on +91-9741436024";
                    require_once "User.php";
                    $u = new User();
                    $u->sendMail($studentMail, 'Course Purchase Confirmation', $mail);
                } else {
                    $mail .= 'Dear Student,<br><br>';
                    $mail .= "Welcome to the Course <strong>{$course['name']}</strong> (Course ID: {$course['id']})<br>taught by <strong>{$owner['name']}</strong> ($institute)<br><br>";
                    $mail .= "<table border=1 style='width: 100%;'><thead><tr><th>Item</th><th>Quantity</th></tr></thead><tbody><tr><td style='text-align: center;'>{$course['name']}</td><td style='text-align: center;'>1</td></tr></tbody></table>";
                    $mail .= "<br><br>In case you need any help, please write to us at <a href='mailto:contactus@integro.io'>contactus@integro.io</a> or give a call on +91-9741436024";
                    require_once "User.php";
                    $u = new User();
                    $u->sendMail($studentMail, 'Course Purchase Confirmation', $mail);
                }

                //fetching total students to send to institute
                $query = "(SELECT COUNT(DISTINCT user_id) AS purchased FROM student_course WHERE course_id={$data->courseId} AND key_id=0) UNION ALL (SELECT COUNT(DISTINCT user_id) AS purchased FROM student_course WHERE course_id={$data->courseId} AND key_id!=0)";
                $total = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
                $total = $total->toArray();
                $purchased = $total[0]['purchased'];
                $associated = $total[1]['purchased'];


                //now sending mail to owner
                $total = $purchased + $associated;
                $mail = '';
                if (isset($orderId) && !empty($orderId)) {
                    $mail .= "Dear {$owner['name']},<br><br>";
                    $mail .= "Congrats! Student <strong>{$student['name']}</strong> has joined your Course <strong>{$course['name']}</strong> by puchasing it online from Marketplace<br><br>";
                    $mail .= "Here are the details :<br><br>";
                    $mail .= "<table border=1 style='width: 100%;'><tr><td>Your Course</td><td><strong>{$course['name']}</strong></td></tr><tr><td>Student Name</td><td><strong>{$student['name']}</strong></td></tr><tr><td>Purchase Amount</td><td><strong>{$data->price} {$currency}</strong></td></tr><tr><td>Students via Course Keys</td><td><strong>{$associated}</strong></td></tr><tr><td>Students via Marketplace</td><td><strong>{$purchased}</strong></td></tr><tr><td>Total Students</td><td><strong>{$total}</strong></td></tr></table>";
                    $mail .= "<br><br>In case you need any help, please write to us at <a href='mailto:contactus@integro.io'>contactus@integro.io</a> or give a call on +91-9741436024";
                    $s = $u->sendMail($course['email'], 'Course Purchase', $mail);
                } else {
                    $mail .= "Dear {$owner['name']},<br><br>";
                    $mail .= "Congrats! Student <strong>{$student['name']}</strong> has joined your Course <strong>{$course['name']}</strong> by puchasing it online from Marketplace<br><br>";
                    $mail .= "Here are the details :<br><br>";
                    $mail .= "<table border=1 style='width: 100%;'><tr><td>Your Course</td><td><strong>{$course['name']}</strong></td>
                    </tr><tr><td>Student Name</td><td><strong>{$student['name']}</strong></td></tr><tr><td>Students via Course Keys</td><td><strong>{$associated}</strong></td></tr><tr><td>Students via Marketplace</td><td><strong>{$purchased}</strong></td></tr><tr><td>Total Students</td><td><strong>{$total}</strong></td></tr></table>";
                    $mail .= "<br><br>In case you need any help, please write to us at <a href='mailto:contactus@integro.io'>contactus@integro.io</a> or give a call on +91-9741436024";
                    $s = $u->sendMail($course['email'], 'Course Purchase', $mail);
                }
            }

            $result->status = 1;
            return $result;
        } catch (Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
     }
	 
	//send renew student
	public function sendRenewRequest($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$message = "Student id : {$data->userId} has requested to renew his course id : {$data->courseId}";
			//send this mail to $data->instituteId's email
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	
	//function to check if any course satisfies all the requirements or not.
	//needs to be called before inviting any student and before making a course live.
	public function checkRestrictionAndWarningOfCourse($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('subjects');
			$select->where(array('courseId'	=>	$data->courseId, 'deleted'	=>	0));
			$select->columns(array('id'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$subjects = $subjects->toArray();
			if(count($subjects) == 0) {
				$result->status = 0;
				$result->message = "Please add atleast one subject before your course can interact with students.";
				return $result;
			}
			$subjectId = array();
			foreach($subjects as $s) {
				$subjectId[] = $s['id'];
			}
			//fetching chapters under subjects
			$select = $sql->select();
			$select->from('chapters');
			$where = new \Zend\Db\Sql\Where();
			$where->in('subjectId', $subjectId);
			$where->equalTo('deleted', 0);
			$select->where($where);
			$select->columns(array('id'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$chapters = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$chapters = $chapters->toArray();
			//fetching exams under subjects
			$select = $sql->select();
			$select->from('exams');
			$where = new \Zend\Db\Sql\Where();
			$where->in('subjectId', $subjectId);
			$where->equalTo('delete', 0);
			$select->where($where);
			$select->columns(array('id'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$exams = $exams->toArray();
			if(count($chapters) == 0 && count($exams) == 0) {
				$result->status = 0;
				$result->message = "Please add atleast one section or one exam in any one subject before your course can interact with students.";
				return $result;
			}
			if(count($exams) == 0) {
				//check chapters contain content
				$chapterId = array();
				foreach($chapters as $c) {
					$chapterId[] = $c['id'];
				}
				$select = $sql->select();
				$select->from('content');
				$where = new \Zend\Db\Sql\Where();
				$where->in('chapterId', $chapterId);
				$select->where($where);
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$content = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$content = $content->toArray();
				if(count($content) == 0) {
					$result->status = 0;
					$result->message = "Please add some content to any section or one exam in any one subject before your course can interact with students.";
					return $result;
				}
			}
			//if($data->ignoreWarning == false) {
			//restriction cases over now checking warning cases
			/*foreach($subjectId as $subject) {
				//check if a subject contains independent exams
				$select = $sql->select();
				$select->from('exams');
				$select->where(array(
					'subjectId'	=>	$subject,
					'chapterId'	=>	0,
					'delete'	=>	0
				));
				$select->columns(array(
					'ec'	=>	new \Zend\Db\Sql\Expression("COUNT(id)")
				));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$ec = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$ec = $ec->toArray();
				if($ec[0]['ec'] != 0)
					continue;
				//if there is no independent exam then checking for some filled chapters
				$selectString ="SELECT COUNT(id) AS count FROM `exams`
                                WHERE chapterId IN (
                                    SELECT id FROM chapters
                                    WHERE subjectId={$subject}
                                ) UNION ALL
                                SELECT COUNT(id) AS count FROM content
                                WHERE chapterId IN (
                                    SELECT id FROM chapters
                                    WHERE subjectId={$subject}
                                );";
				$chapterCount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$chapterCount = $chapterCount->toArray();
				if(count($chapterCount) != 2) {
					$result->status = 0;
					$result->message = 'Some of your subjects do not contain any sections or exams. Please provide content or assignment/exam or delete the blank subjects.';
					$result->warning = true;
					return $result;
				}
				else if($chapterCount[0]['count'] == 0 && $chapterCount[1]['count'] == 0) {
					$result->status = 0;
					$result->message = 'Some of your subjects do not contain any sections or exams.  Please provide content or assignment/exam or delete the blank subjects.';
					$result->warning = true;
					return $result;
				}
			}*/
			//}
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
}

?>