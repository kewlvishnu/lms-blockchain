<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Sql;
	use Zend\Db\Sql\Select;
	use Zend\Db\ResultSet\HydratingResultSet;
	use Zend\Mail;
	use Zend\Http\Client;

	require_once "global.php";

	class Base {
		public $adapter;
		public $ogPath;
		public $pbTypes;
		public $sitePath;
		public $dbConfig;
		
		function __construct(){
			$config = require "Conf.php";
			$this->adapter = new Zend\Db\Adapter\Adapter($config);
			date_default_timezone_set("Asia/Kolkata");
			require "global.php";
			$this->dbConfig = $config;
			$this->ogPath = $ogCourseImage;
			$this->pbTypes= $publicationTypes;
			$this->sitePath = $sitePath;
			$this->urlDomains = $urlDomains;
		}
		
		//database safe string generation
		public function safeString($string) {
			$search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
			$replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
			return str_replace($search, $replace, $string);
		}

		public function displayString($string)
		{
			return substr(preg_replace( "/\r|\n/", "", strip_tags($string)), 0, 50);
		}

		public function round2($number)
		{
			return round($number, 2);
		}
	}
?>