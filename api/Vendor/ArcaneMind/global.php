<?php
	$branch = "local";
	$ogCourseImage = "https://d23h5gn2j73jqu.cloudfront.net/course-images/";
	$publicationTypes = array(
							'published'	=> 'Published Papers',
							'research'	=> 'Research Papers',
							'books'		=> 'Books'
						);
	$sitePath = "//localhost/lms-blockchain/";
	$urlDomains = array();
	$chatPrefix = "local-";
?>