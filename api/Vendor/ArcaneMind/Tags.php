<?php

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "Base.php";

//$adapter = require "adapter.php";
class Tags extends Base {

	public function queryTags($data) {
		$result = new stdClass();
		try {
			//var_dump($data);
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$query="SELECT `id`, `tag` AS `name` FROM `question_tags`
					WHERE `tag` LIKE '%{$data['query']}%'";
			$questionTags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$questionTags = $questionTags->toArray();
			return $questionTags;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function tagExamAnalysis($data) {
		$result = new stdClass();
		try {
			//var_dump($data);
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			if ($data->examType == "exam") {
				$query="SELECT sum(1) as appear_count,
						sum(case when aq.check = 1 then 1 else 0 end) as correct_count
						FROM `question_tags` AS qt
						INNER JOIN `question_tag_links` AS qtl ON qtl.tag_id=qt.id
						INNER JOIN `attempt_questions` AS aq ON aq.questionId=qtl.question_id
						INNER JOIN `exam_attempts` AS ea ON ea.id=aq.attemptId
						WHERE qt.id={$data->tagId} AND ea.examId={$data->examId} AND ea.studentId={$data->userId} AND aq.check>0
						GROUP BY ea.id
						ORDER BY ea.id DESC";
				$tagData = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tagData = $tagData->toArray();

				require_once('Exam.php');
				$dataRange = new stdClass();
				$dataRange->examId = $data->examId;
				$dataRange->examType = 'exam';
				$e = new Exam();
				$ranges=$e->getPercentRanges($dataRange);
				if ($ranges->status == 1) {
					$ranges = $ranges->tag;
				} else {
					$ranges = array();
				}
			} else if ($data->examType == "subjective") {
				$maxScore = $score = 0;
				$query="SELECT SUM(qsel.marks) AS maxScore,SUM(saq.marks) AS score
						FROM `question_tags` AS qt
						INNER JOIN `subjective_question_tag_links` AS sqtl ON sqtl.tag_id=qt.id
						INNER JOIN `subjective_attempt_questions` AS saq ON saq.questionId=sqtl.question_id
						INNER JOIN `subjective_exam_attempts` AS sea ON sea.id=saq.attemptId
						INNER JOIN `questions_subjective` AS qs ON qs.id=sqtl.question_id
						INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id AND qsel.examId=sea.examId
						WHERE qt.id={$data->tagId} AND sea.examId={$data->examId} AND sea.studentId={$data->userId} AND qs.questionsonPage=0
						GROUP BY sea.id";
				$tagData = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tagData = $tagData->toArray();

				require_once('Exam.php');
				$dataRange = new stdClass();
				$dataRange->examId = $data->examId;
				$dataRange->examType = 'subjective-exam';
				$e = new Exam();
				$ranges=$e->getPercentRanges($dataRange);
				if ($ranges->status == 1) {
					$ranges = $ranges->tag;
				} else {
					$ranges = array();
				}
			} else if ($data->examType == "submission") {
				$maxScore = $score = 0;
				$query="SELECT SUM(sqli.marks) AS maxScore,SUM(sam.marks) AS score
						FROM `question_tags` AS qt
						INNER JOIN `submission_question_tag_links` AS sqtl ON sqtl.tag_id=qt.id
						INNER JOIN `submission_attempt_questions` AS saq ON saq.questionId=sqtl.question_id
						INNER JOIN `submission_attempts` AS sa ON sa.id=saq.attemptId
						INNER JOIN `submission_attempt_marks` AS sam ON sam.attemptQuestionId=saq.id
						INNER JOIN `submission_questions` AS sq ON sq.id=sqtl.question_id
						INNER JOIN `submission_question_links` AS sqli ON sqli.questionId=sq.id AND sqli.examId=sa.examId
						WHERE qt.id={$data->tagId} AND sa.examId={$data->examId} AND sam.studentId={$data->userId}
						GROUP BY sa.id";
				$tagData = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tagData = $tagData->toArray();

				require_once('Exam.php');
				$dataRange = new stdClass();
				$dataRange->examId = $data->examId;
				$dataRange->examType = 'submission';
				$e = new Exam();
				$ranges=$e->getPercentRanges($dataRange);
				if ($ranges->status == 1) {
					$ranges = $ranges->tag;
				} else {
					$ranges = array();
				}
			}
			$result->tagData = $tagData;
			$result->ranges = $ranges;
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getExamTags($data) {
		$result = new stdClass();
		try {
			//var_dump($data);
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			if ($data->examType == "exam") {
				$query="SELECT qt.id, qt.tag, sum(1) as appear_count
						FROM `question_tags` AS qt
						INNER JOIN `question_tag_links` AS qtl ON qtl.tag_id=qt.id
						INNER JOIN `question_category_links` AS qcl ON qcl.questionId=qtl.question_id
						INNER JOIN `section_categories` AS sc ON sc.id=qcl.categoryId
						INNER JOIN `exam_sections` AS es ON es.id=sc.sectionId
						WHERE es.examId={$data->examId}
						GROUP BY qt.id
						ORDER BY qt.tag";
				$tagData = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tagData = $tagData->toArray();
			} else if ($data->examType == "subjective-exam") {
				$query="SELECT qt.id, qt.tag, sum(1) as appear_count
						FROM `question_tags` AS qt
						INNER JOIN `subjective_question_tag_links` AS sqtl ON sqtl.tag_id=qt.id
						INNER JOIN `questions_subjective` AS qs ON qs.id=sqtl.question_id
						INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id
						INNER JOIN `exam_subjective` AS es ON es.id=qsel.examId
						WHERE qsel.examId={$data->examId}
						GROUP BY qt.id
						ORDER BY qt.tag";
				$tagData = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tagData = $tagData->toArray();
			} else if ($data->examType == "submission") {
				$query="SELECT qt.id, qt.tag, sum(1) as appear_count
						FROM `question_tags` AS qt
						INNER JOIN `submission_question_tag_links` AS sqtl ON sqtl.tag_id=qt.id
						INNER JOIN `submission_questions` AS sq ON sq.id=sqtl.question_id
						INNER JOIN `submission_question_links` AS sqli ON sqli.questionId=sq.id
						INNER JOIN `submissions` AS s ON s.id=sqli.examId
						WHERE sqli.examId={$data->examId}
						GROUP BY qt.id
						ORDER BY qt.tag";
				$tagData = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tagData = $tagData->toArray();
			} else {
				$result->status = 0;
				$result->message = 'Manual exam cannot have tags!';
			}
			$result->tagData = $tagData;
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function queryCourseTags($data) {
		$result = new stdClass();
		try {
			//var_dump($data);
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$query="SELECT `id`, `tag` AS `name` FROM `course_tags`
					WHERE `tag` LIKE '%{$data['query']}%'";
			$questionTags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$questionTags = $questionTags->toArray();
			return $questionTags;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getCourseTags($data) {
		$result = new stdClass();
		try {
			//var_dump($data);
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$query="SELECT qt.id, qt.tag, sum(1) as appear_count
					FROM `course_tags` AS qt
					INNER JOIN `course_tag_links` AS qtl ON qtl.tag_id=qt.id
					WHERE qtl.course_id={$data->courseId}
					GROUP BY qt.id
					ORDER BY qt.tag";
			$tagData = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$tagData = $tagData->toArray();
			$result->tagData = $tagData;
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
}

?>
