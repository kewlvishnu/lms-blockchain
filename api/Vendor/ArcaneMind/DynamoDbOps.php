<?php
	//header('Content-Type: text/plain; charset=utf-8');
	
	// Include the AWS SDK using the Composer autoloader
	//require_once 'api/Vendor/aws/aws-autoloader.php';
	/*if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
		require_once __DIR__.'\..\aws\aws-autoloader.php';
	} else {*/
	if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
		//echo __DIR__.'/../aws/aws-autoloader.php';
		require_once __DIR__.'/../aws/aws-autoloader.php';
	} else {
		require_once __DIR__.'\..\aws\aws-autoloader.php';
	}
	//}
	
	use Aws\S3\S3Client;
	use Aws\DynamoDb\DynamoDbClient;
	use Aws\Common\Enum\Region;
	use Aws\DynamoDb\Enum\KeyType;
	use Aws\DynamoDb\Enum\Type;
	use Aws\DynamoDb\Marshaler;
	
	class DynamoDbOps{
		/*
		@author : mackayush
		@params:  SubjectId
		@return: return all data across subjectId
			'global secondary index by query'
		*/
		public function getStudentsProgress($data) {
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "confD.php";
			
			$marshaler = new Marshaler();

			$tableName = require "confD.php";

			$subjectId	= $data->subjectId;
			$userId		= $data->userId;

			$eav = $marshaler->marshalJson('
				{
					":subject": '.$subjectId.'
				}
			');

			$params = [
				'TableName' => $tableName,
				'IndexName' => "subjectIndex",
				'KeyConditionExpression' =>
					'subjectId = :subject',
				'ExpressionAttributeValues'=> $eav
			];

			try {
				$resItems = $dynamodb->query($params);
				$result->status = 1;
				if (count($resItems["Items"])>0) {
					$items = array();
					$i=0;
					foreach ($resItems['Items'] as $key=>$content) {
						$items[$i]['userId'] = $marshaler->unmarshalValue($content['userId']);
						$items[$i]['contentId'] = $marshaler->unmarshalValue($content['contentId']);
						$items[$i]['subjectId'] = $marshaler->unmarshalValue($content['subjectId']);
						$items[$i]['chapterId'] = $marshaler->unmarshalValue($content['chapterId']);
						$items[$i]['logs'] = $marshaler->unmarshalValue($content['logs']);
						$items[$i]['percentRead'] = $marshaler->unmarshalValue($content['percentRead']);
						$items[$i]['totalTime'] = $marshaler->unmarshalValue($content['totalTime']);
						$items[$i]['totalViews'] = $marshaler->unmarshalValue($content['totalViews']);
						$i++;
					}
					$result->items = $items;
				} else {
					$result->items = array();
				}
				return $result;
			} catch (DynamoDbException $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		/*
		@params:  userId and SubjectId
		@return:  contentIds of all the videos watched by user from dynamo db
		check
		*/
		public function getAllWatchedInformation($data) {
			$result = new stdClass();
			if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
				//$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
				$aws = Aws\Common\Aws::factory(__DIR__.'/configDynamo.php');
			} else {
				$aws = Aws\Common\Aws::factory(__DIR__.'\configDynamo.php');
			}
			$dynamodb = $aws->get("dynamodb");
			//$config = require "confD.php";
			
			$marshaler = new Marshaler();

			$tableName = require "confD.php";

			$subjectId	= $data->subjectId;
			$userId		= $data->userId;

			$eav = $marshaler->marshalJson('
				{
					":user" : '.$userId.',
					":subject": '.$subjectId.'
				}
			');

			$params = [
				'ConsistentRead' => true,
				'TableName' => $tableName,
    			'IndexName' => "UserSubjectIndex",
				'KeyConditionExpression' =>
					'userId = :user and subjectId = :subject',
				'ExpressionAttributeValues'=> $eav
			];
			
			try {
				$resItems = $dynamodb->query($params);
				$result->status = 1;
				if (count($resItems["Items"])>0) {
					$items = array();
					$i=0;
					foreach ($resItems['Items'] as $key=>$content) {
				        $items[$i]['userId'] = $marshaler->unmarshalValue($content['userId']);
				        $items[$i]['contentId'] = $marshaler->unmarshalValue($content['contentId']);
				        $items[$i]['subjectId'] = $marshaler->unmarshalValue($content['subjectId']);
				        $items[$i]['chapterId'] = $marshaler->unmarshalValue($content['chapterId']);
				        $items[$i]['logs'] = $marshaler->unmarshalValue($content['logs']);
				        $items[$i]['percentRead'] = $marshaler->unmarshalValue($content['percentRead']);
				        $items[$i]['totalTime'] = $marshaler->unmarshalValue($content['totalTime']);
				        $items[$i]['totalViews'] = $marshaler->unmarshalValue($content['totalViews']);
				        $i++;
				    }
					$result->items = $items;
				} else {
					$result->items = array();
				}
				return $result;
			} catch (DynamoDbException $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		/*
		@params: userId , SubjectId and array of watched videos
		@return: contentIds of all the videos watched by user from dynamo db
		check
		*/
		public function putWatchedContent($data) {
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "confD.php";

			if (isset($data->contentId) && !empty(isset($data->contentId))) {
				$marshaler	= new Marshaler();

				$tableName	= require "confD.php";

				$userId		= $data->userId;
				$stuffType	= $data->stuffType;
				$contentId	= $data->contentId;
				$chapterId	= $data->chapterId;
				$subjectId	= $data->subjectId;
				$startTime	= round($data->startTime/1000);
				$endTime	= round($data->endTime/1000);
				$idleTime	= $data->idleTime;
				$totalTime	= $endTime-$startTime-$idleTime;
				$getTheItem	= $this->checkContentExists($data);
				switch ($stuffType) {
					case 1:
					case 5:
					case 7:
						$contentLength = 10;//100;
						break;
					case 8:
					case 2:
						$contentLength = 1;
						break;
					case 3:
					case 4:
					case 6:
						$contentLength = round($data->videoDuration);
						break;
					
					default:
						$contentLength = 10;//100;
						break;
				}
				//var_dump($getTheItem);
				if(count($getTheItem) > 0) {
					$oldTotalTime = $marshaler->unmarshalValue($getTheItem['totalTime']);
					$oldTotalViews = $marshaler->unmarshalValue($getTheItem['totalViews']);
					$totalTime+= $oldTotalTime;
					$logs = $marshaler->unmarshalValue($getTheItem['logs']);
					$logCount = count($logs);
					//var_dump($totalTime);
					//var_dump($contentLength);
					if ($totalTime>$contentLength) {
						$percentRead = 100;
					} else {
						$percentRead = round(($totalTime/$contentLength)*100);
					}
					//var_dump($percentRead);

					$key = $marshaler->marshalJson('
						{
							"userId": ' . $userId . ',
							"contentId": ' . $contentId . '
						}
					');

					$eav = $marshaler->marshalJson('
						{
							":t": '.$totalTime.',
							":v": 1,
							":p": '.$percentRead.',
							":l": "'.$startTime.'|'.$endTime.'"
						}
					');

					$params = [
						'TableName' => $tableName,
						'Key' => $key,
						'UpdateExpression' => 
							'set totalTime = :t, totalViews = totalViews + :v, percentRead = :p, logs['.$logCount.'] = :l',
						'ExpressionAttributeValues'=> $eav,
						'ReturnValues' => 'UPDATED_NEW'
					];

					try {
						$resultItem = $dynamodb->updateItem($params);

						require 'Subject.php';
						$s = new Subject();
						$dataR = new stdClass();
						$dataR->contentId = $contentId;
						$dataR->contentType = 'content';
						$dataR->percentage = $percentRead;
						$dataR->studentId = $userId;
						//var_dump($dataR);
						$resSubjectRewards = $s->putSubjectRewardsbyContent($dataR);

						$result->status = 1;
						return $result;

					} catch (DynamoDbException $e) {
						$result->status = 0;
						$result->message = $e->getMessage();
						return $result;
					}
				} else {
					if ($totalTime>$contentLength) {
						$percentRead = 100;
					} else {
						//$percentRead = ($totalTime/$contentLength)*100;
						$percentRead = round(($totalTime/$contentLength)*100);
					}
					$item = $marshaler->marshalJson('
						{
							"userId": ' . $userId . ',
							"contentId": ' . $contentId . ',
							"chapterId": ' . $chapterId . ',
							"subjectId": ' . $subjectId . ',
							"totalTime": ' . $totalTime . ',
							"totalViews": 1,
							"percentRead": '.$percentRead.',
							"logs": ["'.$startTime.'|'.$endTime.'"]
						}
					');

					$params = [
						'TableName' => $tableName,
						'Item' => $item
					];


					try {
						$result = $dynamodb->putItem($params);

						require 'Subject.php';
						$s = new Subject();
						$dataR = new stdClass();
						$dataR->contentId = $contentId;
						$dataR->contentType = 'content';
						$dataR->percentage = $percentRead;
						$dataR->studentId = $userId;
						//var_dump($dataR);
						$resSubjectRewards = $s->putSubjectRewardsbyContent($dataR);

						return $result;

					} catch (DynamoDbException $e) {
						$result->status = 0;
						$result->message = $e->getMessage();
						return $result;
					}
				}
			} else {
				$result->status = 0;
				$result->message = "There was some error";
				return $result;
			}
		}

		/*
		@params: userId , SubjectId and array of watched videos
		@return: contentIds of all the videos watched by user from dynamo db
		check
		*/
		public function checkContentExists($data) {
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "confD.php";

			$marshaler = new Marshaler();

			$tableName = require "confD.php";

			$userId = $data->userId;
			$contentId = $data->contentId;

			$key = $marshaler->marshalJson('
			    {
			        "userId": ' . $userId . ', 
			        "contentId": ' . $contentId . '
			    }
			');

			$params = [
				'TableName' => $tableName,
				'Key' => $key
			];

			try {
				$result = $dynamodb->getItem($params);
			    return $result['Item'];

			} catch (DynamoDbException $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		/*
		@author : Jinendra Khobare
		@params:  SubjectId, contentId
		@return: return all data across subjectId
			'global secondary index by query'
		*/
		public function getStudentsContentProgress($data) {
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "confD.php";
			
			$marshaler = new Marshaler();

			$tableName = require "confD.php";

			$subjectId	= $data->subjectId;
			$userId		= $data->userId;

			$eav = $marshaler->marshalJson('
				{
					":subject": '.$subjectId.'
				}
			');

			$params = [
				'TableName' => $tableName,
    			'IndexName' => "subjectIndex",
				'KeyConditionExpression' =>
					'subjectId = :subject',
				'ExpressionAttributeValues'=> $eav
			];
			
			try {
				$resItems = $dynamodb->query($params);
				$result->status = 1;
				if (count($resItems["Items"])>0) {
					$item = array();
					foreach ($resItems['Items'] as $key=>$content) {
						if ($content['contentId'] == $data->contentId) {
							$item['userId'] = $marshaler->unmarshalValue($content['userId']);
							$item['contentId'] = $marshaler->unmarshalValue($content['contentId']);
							$item['subjectId'] = $marshaler->unmarshalValue($content['subjectId']);
							$item['chapterId'] = $marshaler->unmarshalValue($content['chapterId']);
							$item['logs'] = $marshaler->unmarshalValue($content['logs']);
							$item['percentRead'] = $marshaler->unmarshalValue($content['percentRead']);
							$item['totalTime'] = $marshaler->unmarshalValue($content['totalTime']);
							$item['totalViews'] = $marshaler->unmarshalValue($content['totalViews']);
						}
				    }
					$result->item = $item;
				} else {
					$result->item = array();
				}
				return $result;
			} catch (DynamoDbException $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
	}
?>