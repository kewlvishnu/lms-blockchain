﻿<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Select;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	
	require_once "Base.php";
	
	class StudentExam extends Base {
	
		//get exam details for student to give exam.
		public function getExamForStudent($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('e'		=>	'exams'))
						->join(array('i'	=>	'exam_instructions'),
							'e.id=i.examId',
							'instructions',
							$select::JOIN_LEFT);
				$select->where(array('e.id'		=>	$data->examId,
									'e.delete'	=>	0,
									'e.status'	=>	2));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exam = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exam = $exam->toArray();
				//print_r($exam);
				if(isset($exam[0])) {
					$result->exam = $exam[0];
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('es'	=>	'exam_sections'));
					if($data->resumeMode == 0) {
						if($result->exam['sectionOrder'] == 0)
							$select->order('es.weight ASC, es.id ASC');
						else
							$select->order(new \Zend\Db\Sql\Expression("RAND()"));
					}
					else {
						$select->join(
							array('aq'	=>	'attempt_questions'),
							'aq.sectionId = es.id',
							array('sectionId'	=>	'sectionId')
						);
						$select->order('aq.order ASC');
						$select->group('aq.sectionId');
					}
					$select->where(array('es.examId'	=>	$data->examId,
										'es.delete'	=>	0,
										'es.status'	=>	1));
					$selectString = $sql->getSqlStringForSqlObject($select);
					/*var_dump($selectString);*/
					$sections = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$sections = $sections->toArray();
					$result->sections = $sections;
					//print_r($result);
					$result->status = 1;
				}
				else {
					$result->status = 0;
					$result->message = 'Exam not found.';
				}
				//print_r($result);
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function getSubjectiveExamForStudent($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('e'		=>	'exam_subjective'));
				$select->where(array('e.id'		=>	$data->examId,
									'e.status'	=>	2,
									'e.Active'	=> 1));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exam = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exam = $exam->toArray();
				//print_r($exam);
				if(isset($exam[0])) {
					$result->exam	= $exam[0];
					$result->status = 1;
				}
				else {
					$result->status = 0;
					$result->message = 'Exam not found.';
				}
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//to get questions and categories of section which student is giving exam
		public function getQuestionsForExam($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				//getting categories of the section
				$select = $sql->select();
				$select->from('section_categories');
				$select->where(array('sectionId'		=>	$data->sectionId,
									'delete'	=>	0,
									'status'	=>	1));
				if($data->random)
					$select->order(new \Zend\Db\Sql\Expression("RAND()"));
				else
					$select->order('weight ASC');
				$selectString = $sql->getSqlStringForSqlObject($select);
				$categories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$categories = $categories->toArray();
				$result->categories = $categories;
				$result->questions = array();
				foreach($result->categories as $category) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					if($category['questionType'] != 4) {
						$select->from(array('q'=>'questions'))
								->order(new \Zend\Db\Sql\Expression("RAND()"));
						$select->join(array('qcl'	=>	'question_category_links'),
								'q.id = qcl.questionId',
								array('categoryId')
						);
						$select->where(array('categoryId'	=>	$category['id'],
											'delete'	=>	0,
											'status'	=>	1,
											'parentId'	=>	0));
						$select->limit($category['required']);
						$selectString = $sql->getSqlStringForSqlObject($select);
					}
					else {
						$select->from(array('q1'	=>	'questions'))
								->join(array('q2'	=>	'questions'),
										'q2.id = q1.parentId',
										array('parent'	=>	'question'),'left');
						$select->join(array('qcl'	=>	'question_category_links'),
								'q2.id = qcl.questionId',
								array('categoryId')
						);
						$select->order('parentId');
						$select->order(new \Zend\Db\Sql\Expression("RAND()"));
						$where = new \Zend\Db\Sql\Where();
						$where->equalTo('qcl.categoryId', $category['id']);
						$where->equalTo('q1.delete', 0);
						$where->equalTo('q1.status', 1);
						$where->notEqualTo('q1.parentId', 0);
						//$where->notEqualTo('q1.parentId', 0);
						$select->where($where);
						$select->limit($category['required']);
						$selectString = $sql->getSqlStringForSqlObject($select);
					}
					$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$questions = $questions->toArray();
					$parentId = 0;
					$parent = '';
					foreach($questions as $key => $q) {
						if($q['questionType'] != 5) {
							if($q['questionType'] == 7) {
								$sql = new Sql($adapter);
								$select = $sql->select();
								$select->from('question_files')
										->where(array('questionId' => $q['id']))
										->columns(array('audio' => 'stuff'));
								$string = $sql->getSqlStringForSqlObject($select);
								$qFiles = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
								$qFiles = $qFiles->toArray();
								$questions[$key]['audio'] = '';
								if (count($qFiles)) {
									$questions[$key]['audio'] = $qFiles[0]['audio'];
								}
							}
							if($q['questionType'] == 3) {
								$temp = $this->getAnswerForEachQuestion($q);
								$questions[$key]['answer']['columnA'] = $temp['opt1'];
								$questions[$key]['answer']['columnB'] = $temp['opt2'];
							}
							else
								$questions[$key]['answer'] = $this->getAnswerForEachQuestion($q);
							if($q['parentId'] != 0) {
								if($parentId == $q['parentId']) {
									$questions[$key]['parent'] = $parent;
								}
								else {
									$parentId = $q['parentId'];
									$select = $sql->select();
									$select->from('questions')->where(array('id' => $q['parentId']))->columns(array('parent' => 'question'));
									$string = $sql->getSqlStringForSqlObject($select);
									$parent = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
									$parent = $parent->toArray();
									if(count($parent) > 0)
										$parent = $parent[0]['parent'];
									$questions[$key]['parent'] = $parent;
								}
							}
						}
						else {
							//fetch child questions and then call getAnswerForEachQuestion for each child question
							/*$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from(array('q'=>'questions'))
									->order(new \Zend\Db\Sql\Expression("RAND()"));
							$select->join(array('qcl'	=>	'question_category_links'),
									'q.id = qcl.questionId',
									array('categoryId')
							);
							$select->where(array('categoryId'	=>	$category['id'],
												'delete'	=>	0,
												'status'	=>	1,
												'parentId'	=>	$q['id']));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$childQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$childQuestions = $childQuestions->toArray();
							foreach($childQuestions as $childKey	=>	$childQ) {
								$childQuestions[$childKey]['answer'] = $this->getAnswerForEachQuestion($childQ);
							}
							$questions[$key]['childQuestions'] = $childQuestions;*/


							
							//fetch child questions and then call getAnswerForEachQuestion for each child question
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from('questions')
									->order(new \Zend\Db\Sql\Expression("RAND()"));
							$select->where(array('delete'	=>	0,
												'status'	=>	1,
												'parentId'	=>	$q['id']));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$childQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$childQuestions = $childQuestions->toArray();
							foreach($childQuestions as $childKey	=>	$childQ) {
								$childQuestions[$childKey]['answer'] = $this->getAnswerForEachQuestion($childQ);
							}
							$questions[$key]['childQuestions'] = $childQuestions;
						}
					}
					$result->questions[] = $questions;
				}
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//to get questions and categories of section which student is giving assignment in resume mode
		public function getQuestionsForExamInResume($data) {
			$result = new stdClass();                               
			try {                                                   
				$adapter = $this->adapter;                          
				$sql = new Sql($adapter);                           
				$select = $sql->select();                           
				$select->from(array('aq'	=>	'attempt_questions'));
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('aq.sectionId', $data->sectionId);
				$where->equalTo('aq.attemptId', $data->attemptId);
				$where->greaterThanOrEqualTo('aq.parentId', 0);
				$select->where($where)
				->join(array('q'	=>	'questions'),
						'q.id = aq.questionId',
						array('question', 'description')
				)
				->order("order ASC");
				$selectString = $sql->getSqlStringForSqlObject($select);
				$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$questions = $questions->toArray();
				$result->questions = $questions;
				$parentId = 0;
				$parent = '';
				foreach($result->questions as $key => $q) {
					$q['resume'] = true;
					if($q['questionType'] != 5) {
						if($q['questionType'] == 7) {
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from('question_files')
									->where(array('questionId' => $q['questionId']))
									->columns(array('audio' => 'stuff'));
							$string = $sql->getSqlStringForSqlObject($select);
							$qFiles = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
							$qFiles = $qFiles->toArray();
							$result->questions[$key]['audio'] = '';
							$result->questions[$key]['q'] = $string;
							if (count($qFiles)) {
								$result->questions[$key]['audio'] = $qFiles[0]['audio'];
							}
						}
						if($q['questionType'] == 3) {
							$temp = $this->getAnswerForEachQuestion($q);
							$result->questions[$key]['answer']['columnA'] = $temp['opt1'];
							$result->questions[$key]['answer']['columnB'] = $temp['opt2'];
							$result->questions[$key]['userAns'] = $this->getUserAnswerForEachQuestion($q, $data->attemptId);
						}
						else {
							$result->questions[$key]['answer'] = $this->getAnswerForEachQuestion($q);
							$result->questions[$key]['userAns'] = $this->getUserAnswerForEachQuestion($q, $data->attemptId);
						}
						if($q['parentId'] != 0) {
							if($parentId == $q['parentId']) {
								$result->questions[$key]['parent'] = $parent;
							}
							else {
								$parentId = $q['parentId'];
								$select = $sql->select();
								$select->from('questions')->where(array('id' => $q['parentId']))->columns(array('parent' => 'question'));
								$string = $sql->getSqlStringForSqlObject($select);
								$parent = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
								$parent = $parent->toArray();
								if(count($parent) > 0)
									$parent = $parent[0]['parent'];
								$result->questions[$key]['parent'] = $parent;
							}
						}
					}
					else {
						//fetch child questions and then call getAnswerForEachQuestion for each child question
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('questions')
								->order(new \Zend\Db\Sql\Expression("RAND()"));
						$select->where(array('delete'	=>	0,
											'status'	=>	1,
											'parentId'	=>	$q['questionId']));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$childQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$childQuestions = $childQuestions->toArray();
						foreach($childQuestions as $childKey	=>	$childQ) {
							$childQuestions[$childKey]['answer'] = $this->getAnswerForEachQuestion($childQ);
							$childQuestions[$childKey]['userAns'] = $this->getUserAnswerForEachQuestion($childQ, $data->attemptId);
						}
						$result->questions[$key]['childQuestions'] = $childQuestions;
					}
				}
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//getQuestionsForSubjectiveExam : functions return the questions json format 
		public function getQuestionsForSubjectiveExam($data) {
			//$data->examId=14;
			$answeredQuestionsArray=array();
			$result = new stdClass();
			try {
				
				$adapter = $this->adapter;
				//$sql = new Sql($adapter);
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exam_subjective');
				$select->where(array(
							'id' => $data->examId,
							'Active' =>	1
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$exam_subjective = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exam_subjective = $exam_subjective->toArray();				
				
				$shuffle=$exam_subjective[0]['shuffle'];
				$requiredQuestion=$exam_subjective[0]['totalRequiredQuestions'];
				$submission=$exam_subjective[0]['submissiontype'];
				
								
				$adapter = $this->adapter;
				//$sql = new Sql($adapter);
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('q'=>'questions_subjective'));
				$select->join(array('qsel'	=>	'question_subjective_exam_links'),
						'q.id = qsel.questionId',
						array('examId', 'marks')
				);
				$select->where(array(
							'examId' => $data->examId,
							'deleted'	=>	0,
							'status' =>	1
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$questions = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$questions = $questions->toArray();
				$subjectiveQuestions=array();
				$parentIdTemp= array();
				$i			 = 0;
				//$totalMarks = 0;
				$qtimer = array();
				foreach($questions as $key=>$subQues)
				{
					$question=array();
					$questionId = $subQues['id'];
					$qId=$subQues['id'];
					/*$questionString='';
					$answerString='';
					if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1' && $subQues['questionfileUrl']!='' && $subQues['answerFileUrl'] !='') {
						require_once 'amazonRead.php';
						$questionString = getSignedURL($subQues['questionfileUrl']);
						$answerString=getSignedURL($subQues['answerFileUrl']);
					}
					$questionString = file_get_contents($questionString);
					$answerString = file_get_contents($answerString);*/
					if($subQues['parentId']==0)
					{	
						$parentIdTemp[$i]=$subQues['id'];
						$i++;
						if($subQues['noOfSubquestionRequired']>0)
						{
							$questionId					=$questionId;
							$question['id']				=$qId;
							$question['questionType']	='questionGroup';
							$question['question']		=$subQues['question'];
							$question['timer']			=0;
							$question['upload']			=array();
							$question['answer']			="";
							$question['marks']			=(int)$subQues['marks'];
							if ($subQues['questionsonPage'] == 1) {
								$question['listType']			='one';
							}
							else {
								$question['listType']			='multi';
							}
							$question['subQuestions']			=array();
						}else{							
							$question['id']				=$qId;
							$question['questionType']	='question';
							$question['question']		=$subQues['question'];
							$question['timer']			=0;
							$question['upload']			=array();
							$question['answer']			='';
							$question['marks']			=(int)$subQues['marks'];
							//$totalMarks+=$subQues['marks'];
							$question['subQuestions']	=array();
						}
						array_push($subjectiveQuestions,$question);
					}else{
						$parentId=$subQues['parentId'];
						$subquestionsArray=array();
						$subQuestionId=$qId;
						if(in_array($parentId,$parentIdTemp))
						{
							$index=array_search($subQues['parentId'],$parentIdTemp);
							$checkMulti=$subjectiveQuestions[$index]['listType'];
							$subquestionsArray['id']=$qId;
							if($checkMulti == 'multi')
							{
								//print_r('inside multi');
							} else {
								//$subjectiveQuestions[$index]['marks'] = $subjectiveQuestions[$index]['marks'] + $subQues['marks'];
							}
							$subquestionsArray['questionType']	='question';
							$subquestionsArray['question']		=$subQues['question'];
							$subquestionsArray['timer']			=0;
							$subquestionsArray['upload']		=array();
							$subquestionsArray['answer']		='';
							$subquestionsArray['marks']			=(int)$subQues['marks'];
							//$totalMarks+=$subQues['marks'];
							array_push($subjectiveQuestions[$index]['subQuestions'],$subquestionsArray);
						}
					}
					$qtimer[$qId] = 0;
				}
				
				//print_r(json_encode($subjectiveQuestions));
				//$jsonQUestions=json_encode($subjectiveQuestions);
				if($shuffle  =='yes')
				{
					shuffle($subjectiveQuestions);
				}
				
				if(count($subjectiveQuestions)> $requiredQuestion && $requiredQuestion > 0){
					$subjectiveQuestions = array_slice($subjectiveQuestions, 0, $requiredQuestion);
				}
				$totalMarks = 0;
				foreach($subjectiveQuestions as $key=>$value)
				{
					if($value['questionType'] == 'question') {
						$totalMarks+=$subjectiveQuestions[$key]['marks'];
					} elseif ($value['questionType'] == 'questionGroup') {
						$qusId=$value['id'];
						$adapter = $this->adapter;
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('questions_subjective');
						$select->where(array(
									'id' => $qusId
									));
						$statement = $sql->getSqlStringForSqlObject($select);
						$questions_subjective = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						$questions_subjective = $questions_subjective->toArray();
						$noOfsubquestions=$questions_subjective[0]['noOfSubquestionRequired'];
						//print_r($value['subQuestions']);
						//print_r($value['subQuestions']);
						if(count($value['subQuestions'])> $noOfsubquestions && $noOfsubquestions > 0){
							$subjectiveQuestions[$key]['subQuestions'] = array_slice($value['subQuestions'], 0, $noOfsubquestions);
							$subjectiveQuestions[$key]['marks'] = $subjectiveQuestions[$key]['marks']*$noOfsubquestions;
							$totalMarks = $totalMarks + ($subjectiveQuestions[$key]['marks']);
						}
						if ($shuffle == 'yes' && $value['listType']=='multi') {
							//$subjectiveQuestions[$key]['subQuestions'] = shuffle($value['subQuestions']);
						}
						//	print_r($value['subQuestions']);
					}
					
				}
				//print_r($subjectiveQuestions);
				//print_r('before 386');
				$qnoCounter =1;
				foreach($subjectiveQuestions as $key=>$value)
				{
					
					//print_r($value);
					
					$quesId=$value['id'];
					if($value['questionType'] == 'question')
					{
						$quesType=0;
						$examQuestions = new TableGateway('subjective_attempt_questions', $adapter, null,new HydratingResultSet());
						$examQuestions->insert(array(
							'attemptId' 			=> $data->attemptId,
							'questionId' 		=>	$quesId,
							'questionType' 		=>	$quesType,
							'parentId'			=>  0,
							'correct'			=>	0,
							'wrong'				=>	0,
							'time' 				=>	0,
							'status' 			=>	0,
							'check'				=>	0,
							'order'				=>	0
						));
						$examQuestionsId = $examQuestions->getLastInsertValue();
						$subjectiveQuestions[$key]['qno']	=	$qnoCounter;
						$qnoCounter							=	$qnoCounter+1;
						//print_r($examQuestionsId);
						
					} else {
						if($value['listType'] == 'one'){
							$quesType=1;
							$subjectiveQuestions[$key]['qno']	=	$qnoCounter;
							$qnoCounter							=	$qnoCounter+1;
						}	else{
							$quesType=2;
						}
						$subques=$value['subQuestions'];
						$examQuestions = new TableGateway('subjective_attempt_questions', $adapter, null,new HydratingResultSet());
						$examQuestions->insert(array(
							'attemptId' 		=> $data->attemptId,
							'questionId' 		=>	$quesId,
							'questionType' 		=>	$quesType,
							'parentId'			=>  0,
							'correct'			=>	0,
							'wrong'				=>	0,
							'time' 				=>	0,
							'status' 			=>	0,
							'check'				=>	0,
							'order'				=>	0
						));
						$examQuestionsId = $examQuestions->getLastInsertValue();
						//print_r($examQuestionsId);
						
						foreach($subques as $key1=>$value1)
						{
							$quesId1=$value1['id'];
							$examQuestions = new TableGateway('subjective_attempt_questions', $adapter, null,new HydratingResultSet());
							$examQuestions->insert(array(
								'attemptId' 		=> $data->attemptId,
								'questionId' 		=>	$quesId1,
								'questionType' 		=>	$quesType,
								'parentId'			=> 	$quesId,
								'correct'			=>	0,
								'wrong'				=>	0,
								'time' 				=>	0,
								'status' 			=>	0,
								'check'				=>	0,
								'order'				=>	0
							));
							$examQuestionsId = $examQuestions->getLastInsertValue();
							if($value['listType'] == 'multi') {
								$subjectiveQuestions[$key]['subQuestions'][$key1]['qno']	=	$qnoCounter;
								$qnoCounter							=	$qnoCounter+1;
							}
							//print_r($examQuestionsId);
						}
					}
				}
				$qnoCounter--;
				//print_r($subjectiveQuestions);
				$result->submission	= $submission;
				$result->questions	= $subjectiveQuestions;
				$result->qtimer		= $qtimer;
				$result->totalQuestions = $qnoCounter;
				$result->totalMarks = $totalMarks;
				$result->status		= 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//getQuestionsForSubjectiveExam : functions return the questions json format 
		public function getQuestionsForSubjectiveExamResumeMode($data) {
			//$data->examId=14;
			$answeredQuestionsArray=array();
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				//$sql = new Sql($adapter);
				if($data->resumeMode == 1)
				{
					$attemptId=$data->attemptId;
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('asu' => 'answer_subjective'));
					$select->join(array('saq' => 'subjective_attempt_questions'),
						'asu.attemptQuestionId = saq.id',
						array('questionId'	=>	'questionId')
					);
					$select->where(array(
								'asu.subjectiveExam_attemptid' => $attemptId,
								'asu.status'				=>1
					));
					$select->order('saq.questionId ASC');
					$statement				= $sql->getSqlStringForSqlObject($select);
					$answeredQuestions		= $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$answeredQuestionsArray = $answeredQuestions->toArray();
					$alreadyAnswered		= $answeredQuestionsArray;
					//print_r($answeredQuestionsArray);
					$subjectiveAnswers = array();
					$index = -1;
					foreach ($alreadyAnswered as $key => $value) {
						$newkey = -1;
						if ($index>-1) {
							foreach ($subjectiveAnswers as $key1 => $value1) {
								if ($value["questionId"] == $subjectiveAnswers[$key1]["questionId"]) {
									$newkey = $key1;
								}
							}
						}
						if ($newkey == -1) {
							$index++;
						} else {
							$index = $newkey;
						}
						$subjectiveAnswers[$index]['marks'] = $value['Marks'];
						$subjectiveAnswers[$index]['questionId'] = $value['questionId'];
						$subjectiveAnswers[$index]['subjectiveExam_attemptid'] = $value['subjectiveExam_attemptid'];
						if ($value['answerType'] == "text") {
							$answerString = $value['answer'];
							$subjectiveAnswers[$index]['answer'] = $answerString;
						} else {
							$subjectiveAnswers[$index]['upload'][] = $value['answer'];
						}
					}
					$result->alreadyAnswered= $subjectiveAnswers;
				}


				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exam_subjective');
				$select->where(array(
							'id' => $data->examId,
							'Active' =>	1
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$exam_subjective = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exam_subjective = $exam_subjective->toArray();

				$submission=$exam_subjective[0]['submissiontype'];
				
				/*$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('questions_subjective');
				$select->where(array(
							'exam_subjectiveId' => $data->examId,
							'deleted'	=>	0,
							'status' =>	1
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$questions = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$questions = $questions->toArray();*/
				
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$query = "SELECT qs.*,qsel.marks
							FROM `subjective_attempt_questions` AS saq
							INNER JOIN `questions_subjective` AS qs ON qs.id=saq.questionId
							INNER JOIN `subjective_exam_attempts` AS sea ON sea.id=saq.attemptId
							INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id AND qsel.examId=sea.examId
							WHERE saq.attemptId={$data->attemptId}";
				$questions = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$questions = $questions->toArray();

				$subjectiveQuestions=array();
				$parentIdTemp= array();
				$i			 = 0;
				$qnoCounter =1;
				$totalMarks = 0;

				$select = $sql->select();
				$select->from('subjective_attempt_questions');
				$select->where(array(
							'attemptId'	 => $data->attemptId
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$quesTimer = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$quesTimer = $quesTimer->toArray();
				$qtimer = array();
				$qi = 0;
				foreach($questions as $key=>$subQues)
				{
					$question=array();
					/*$questionId = preg_split('/[n.\s;]+/', $subQues['questionId']);
					$questionId= ($questionId[1]);*/
					$questionId = $subQues['id'];
					$qId=$subQues['id'];
					/*$questionString='';
					$answerString='';
					//&& $subQues['questionfileUrl']!='' && $subQues['answerFileUrl'] !=''
					if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1' && $subQues['questionfileUrl']!='' && $subQues['answerFileUrl'] !='') {
							require_once 'amazonRead.php';		
							$questionString = getSignedURL($subQues['questionfileUrl']);	
							$answerString=getSignedURL($subQues['answerFileUrl']);	
					}
					$questionString = file_get_contents($questionString);
					$answerString = file_get_contents($answerString);*/
					if($subQues['parentId']==0)
					{	
						$parentIdTemp[$i]=$subQues['id'];
						$i++;
						if($subQues['noOfSubquestionRequired']>0)
						{
							/*$questionId						=preg_split('/[s.\s;]+/', $questionId);
							$questionId							=$questionId[0];*/
							$questionId							=$questionId;
							$question['id']						=$qId;
							$question['questionType']			='questionGroup';
							$question['question']				=$subQues['question'];
							$question['timer']					=0;
							$question['upload']					=array();
							$question['answer']					="";
							$question['marks']					=0;
							//$question['questionsonPage']		=$subQues['questionsonPage'];
							/*if($subQues['questionsonPage'] == 0)
							{
								$question['qno']				= 	$qnoCounter;
								$qnoCounter						=  $qnoCounter+1;
							}
							else */if ($subQues['questionsonPage'] == 1) {
								$question['listType']			='one';
								$question['qno']				= $qnoCounter;
								$qnoCounter						= $qnoCounter+1;
							}
							else {
								$question['listType']			='multi';
							}
							$question['subQuestions']			=array();
						}else{							
							$question['id']				=$qId;
							$question['qno']			=$qnoCounter;
							$question['questionType']	='question';
							$question['question']		=$subQues['question'];
							$question['timer']			=0;
							$question['upload']			=array();
							$question['answer']			='';
							$question['marks']			=(int)$subQues['marks'];
							$qnoCounter					=$qnoCounter+1;
							$question['subQuestions']	=array();
							$totalMarks+=$subQues['marks'];
						}
						array_push($subjectiveQuestions,$question);
					}else{
						$parentId=$subQues['parentId'];
						//print_r($subjectiveQuestions[$index]);
						$subquestionsArray=array();
						/*$subQuestionId=preg_split('/[s.\s;]+/', $questionId);
						$subQuestionId= ($subQuestionId[1]);*/
						$subQuestionId=$qId;
						if(in_array($parentId,$parentIdTemp))
						{
							$index=array_search($subQues['parentId'],$parentIdTemp);
							$checkMulti=$subjectiveQuestions[$index]['listType'];
							$subquestionsArray['id']=$qId;
							if($checkMulti == 'multi')
							{
								//print_r('inside multi');
								$subquestionsArray['qno']		=$qnoCounter;
								$qnoCounter						=$qnoCounter+1;
							} else {
								$subjectiveQuestions[$index]['marks'] = $subjectiveQuestions[$index]['marks'] + $subQues['marks'];
							}
							$subquestionsArray['questionType']	='question';
							$subquestionsArray['question']		=$subQues['question'];
							$subquestionsArray['timer']			=0;
							$subquestionsArray['upload']		=array();
							$subquestionsArray['answer']		='';
							$subquestionsArray['marks']			=(int)$subQues['marks'];
							$totalMarks+=$subQues['marks'];
							array_push($subjectiveQuestions[$index]['subQuestions'],$subquestionsArray);
						}
					}
					$qtimer[$qId]	= 0;
					foreach ($quesTimer as $key1 => $qTimer) {
						if ($qTimer['questionId'] == $qId) {
							$qtimer[$qId]	=$qTimer['time'];
						}
					}
					/*$qtimer[$qi]['id']		=$qId;
					$qtimer[$qi]['timer']	=$qTimer['timer'];
					foreach ($quesTimer as $key1 => $qTimer) {
						if ($qTimer['questionId'] == $qId) {
							$qtimer[$qi]['timer']	=$qTimer['timer'];
						}
					}
					$qi++;*/
				}
				$qnoCounter--;
				//print_r($subjectiveQuestions);
				//print_r(json_encode($subjectiveQuestions));
				//$jsonQUestions=json_encode($subjectiveQuestions);
				$result->submission	= $submission;
				$result->questions	= $subjectiveQuestions;
				$result->qtimer		= $qtimer;
				$result->totalQuestions = $qnoCounter;
				$result->totalMarks = $totalMarks;
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//function to get answer or child question according to question type
		public function getAnswerForEachQuestion($q) {
			$adapter = $this->adapter;
			if($q['questionType'] == 0 || $q['questionType'] == 6 || $q['questionType'] == 7) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				if(isset($q['resume']))
					$select->where(array('questionId'	=>	$q['questionId'],
								'delete'		=>	0));
				else
					$select->where(array('questionId'	=>	$q['id'],
								'delete'		=>	0));
				$select->from('options_mcq');
				//if($q['questionType'] == 0)
				$select->columns(array('id', 'option'));
				$select->order(new \Zend\Db\Sql\Expression("RAND()"));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$options = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$options = $options->toArray();
				return $options;
			}
			else if($q['questionType'] == 1) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				if(isset($q['resume']))
					$select->where(array('questionId'	=>	$q['questionId'],
								'delete'		=>	0));
				else
					$select->where(array('questionId'	=>	$q['id'],
								'delete'		=>	0));
				$select->from('true_false');
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$options = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$options = $options->toArray();
				return $options;
			}
			else if($q['questionType'] == 2) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				if(isset($q['resume']))
					$select->where(array('questionId'	=>	$q['questionId'],
								'delete'		=>	0));
				else
					$select->where(array('questionId'	=>	$q['id'],
								'delete'		=>	0));
				$select->from('ftb');
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$options = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$options = $options->toArray();
				return $options;
			}
			else if($q['questionType'] == 3) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				if(isset($q['resume']))
					$select->where(array('questionId'	=>	$q['questionId'],
								'delete'		=>	0));
				else
					$select->where(array('questionId'	=>	$q['id'],
								'delete'		=>	0));
				$select->from('options_mtf');
				$select->columns(array('id', 'columnA'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$options1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$options1 = $options1->toArray();
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				if(isset($q['resume']))
					$select->where(array('questionId'	=>	$q['questionId'],
								'delete'		=>	0));
				else
					$select->where(array('questionId'	=>	$q['id'],
								'delete'		=>	0));
				$select->from('options_mtf');
				$select->columns(array('id', 'columnB'));
				$select->order(new \Zend\Db\Sql\Expression("RAND()"));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$options2 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$options2 = $options2->toArray();
				$temp['opt1'] = $options1;
				$temp['opt2'] = $options2;
				return $temp;
			}
		}

		//to get questions and categories of section which student is giving assignment
		public function getQuestionsForAssignment($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('section_categories');
				$select->where(array('sectionId'		=>	$data->sectionId,
									'delete'	=>	0,
									'status'	=>	1));
				if($data->random)
					$select->order(new \Zend\Db\Sql\Expression("RAND()"));
				else
					$select->order('weight ASC');
				$selectString = $sql->getSqlStringForSqlObject($select);
				$categories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$categories = $categories->toArray();
				$result->categories = $categories;
				$result->questions = array();
				foreach($result->categories as $category) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					if($category['questionType'] != 4) {
						$select->from(array('q'=>'questions'))
								->order(new \Zend\Db\Sql\Expression("RAND()"));
						$select->join(array('qcl'	=>	'question_category_links'),
								'q.id = qcl.questionId',
								array('categoryId')
						);
						$select->where(array('categoryId'	=>	$category['id'],
											'delete'	=>	0,
											'status'	=>	1,
											'parentId'	=>	0));
						$select->limit($category['required']);
						$selectString = $sql->getSqlStringForSqlObject($select);
					}
					else {
						$select->from(array('q1'	=>	'questions'))
								->join(array('q2'	=>	'questions'),
										'q2.id = q1.parentId',
										array('parent'	=>	'question'));
						$select->join(array('qcl'	=>	'question_category_links'),
								'q2.id = qcl.questionId',
								array('categoryId')
						);
						$select->order('parentId');
						$select->order(new \Zend\Db\Sql\Expression("RAND()"));
						$where = new \Zend\Db\Sql\Where();
						$where->equalTo('qcl.categoryId', $category['id']);
						$where->equalTo('q1.delete', 0);
						$where->equalTo('q1.status', 1);
						$where->notEqualTo('q1.parentId', 0);
						$select->where($where);
						$select->limit($category['required']);
						$selectString = $sql->getSqlStringForSqlObject($select);
					}
					$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$questions = $questions->toArray();
					$parentId = 0;
					$parent = '';
					foreach($questions as $key => $q) {
						if($q['questionType'] != 5) {
							if($q['questionType'] == 7) {
								$sql = new Sql($adapter);
								$select = $sql->select();
								$select->from('question_files')
										->where(array('questionId' => $q['id']))
										->columns(array('audio' => 'stuff'));
								$string = $sql->getSqlStringForSqlObject($select);
								$qFiles = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
								$qFiles = $qFiles->toArray();
								$questions[$key]['audio'] = '';
								if (count($qFiles)) {
									$questions[$key]['audio'] = $qFiles[0]['audio'];
								}
							}
							if($q['questionType'] == 3) {
								$temp = $this->getAnswerForEachQuestionInAssignment($q);
								$questions[$key]['answer']['columnA'] = $temp['opt1'];
								$questions[$key]['answer']['columnB'] = $temp['opt2'];
							}
							else
								$questions[$key]['answer'] = $this->getAnswerForEachQuestionInAssignment($q);
							if($q['parentId'] != 0) {
								if($parentId == $q['parentId']) {
									$questions[$key]['parent'] = $parent;
								}
								else {
									$parentId = $q['parentId'];
									$select = $sql->select();
									$select->from('questions')->where(array('id' => $q['parentId']))->columns(array('parent' => 'question'));
									$string = $sql->getSqlStringForSqlObject($select);
									$parent = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
									$parent = $parent->toArray();
									if(count($parent) > 0)
										$parent = $parent[0]['parent'];
									$questions[$key]['parent'] = $parent;
								}
							}
						}
						else {
							//fetch child questions and then call getAnswerForEachQuestion for each child question
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from(array('q'=>'questions'))
									->order(new \Zend\Db\Sql\Expression("RAND()"));
							$select->join(array('qcl'	=>	'question_category_links'),
									'q.id = qcl.questionId',
									array('categoryId')
							);
							$select->where(array('categoryId'	=>	$category['id'],
												'delete'	=>	0,
												'status'	=>	1,
												'parentId'	=>	$q['id']));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$childQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$childQuestions = $childQuestions->toArray();
							foreach($childQuestions as $childKey	=>	$childQ) {
								$childQuestions[$childKey]['answer'] = $this->getAnswerForEachQuestionInAssignment($childQ);
							}
							$questions[$key]['childQuestions'] = $childQuestions;
						}
					}
					$result->questions[] = $questions;
				}
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//to get questions and categories of section which student is giving assignment in resume mode
		public function getQuestionsForAssignmentInResume($data) {
			$result = new stdClass();
			try {                                                   
				$adapter = $this->adapter;                          
				$sql = new Sql($adapter);                           
				$select = $sql->select();
				$select->from(array('aq'	=>	'attempt_questions'));
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('aq.sectionId', $data->sectionId);
				$where->equalTo('aq.attemptId', $data->attemptId);
				$where->greaterThanOrEqualTo('aq.parentId', 0);
				$select->where($where)
				->join(array('q'	=>	'questions'),
						'q.id = aq.questionId',
						array('question', 'description')
				)
				->order("order ASC");
				$selectString = $sql->getSqlStringForSqlObject($select);
				$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$questions = $questions->toArray();
				$result->questions = $questions;
				$parentId = 0;
				$parent = '';
				foreach($result->questions as $key => $q) {
					$q['resume'] = true;
					if($q['questionType'] != 5) {
						if($q['questionType'] == 7) {
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from('question_files')
									->where(array('questionId' => $q['id']))
									->columns(array('audio' => 'stuff'));
							$string = $sql->getSqlStringForSqlObject($select);
							$qFiles = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
							$qFiles = $qFiles->toArray();
							$result->questions[$key]['audio'] = '';
							if (count($qFiles)) {
								$result->questions[$key]['audio'] = $qFiles[0]['audio'];
							}
						}
						if($q['questionType'] == 3) {
							$temp = $this->getAnswerForEachQuestionInAssignment($q);
							$result->questions[$key]['answer']['columnA'] = $temp['opt1'];
							$result->questions[$key]['answer']['columnB'] = $temp['opt2'];
							$result->questions[$key]['userAns'] = $this->getUserAnswerForEachQuestion($q, $data->attemptId);
						}
						else {
							$result->questions[$key]['answer'] = $this->getAnswerForEachQuestionInAssignment($q);
							$result->questions[$key]['userAns'] = $this->getUserAnswerForEachQuestion($q, $data->attemptId);
						}
						if($q['parentId'] != 0) {
							if($parentId == $q['parentId']) {
								$result->questions[$key]['parent'] = $parent;
							}
							else {
								$parentId = $q['parentId'];
								$select = $sql->select();
								$select->from('questions')->where(array('id' => $q['parentId']))->columns(array('parent' => 'question'));
								$string = $sql->getSqlStringForSqlObject($select);
								$parent = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
								$parent = $parent->toArray();
								if(count($parent) > 0)
									$parent = $parent[0]['parent'];
								$result->questions[$key]['parent'] = $parent;
							}
						}
					}
					else {
						//fetch child questions and then call getAnswerForEachQuestion for each child question
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('questions')
								->order(new \Zend\Db\Sql\Expression("RAND()"));
						$select->where(array('delete'	=>	0,
											'status'	=>	1,
											'parentId'	=>	$q['questionId']));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$childQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$childQuestions = $childQuestions->toArray();
						foreach($childQuestions as $childKey	=>	$childQ) {
							$childQuestions[$childKey]['answer'] = $this->getAnswerForEachQuestionInAssignment($childQ);
							$childQuestions[$childKey]['userAns'] = $this->getUserAnswerForEachQuestion($childQ, $data->attemptId);
						}
						$result->questions[$key]['childQuestions'] = $childQuestions;
					}
				}
				$result->status = 1;
				return $result;
			}
			catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//function to fetch user answers from database for resuming the exam/assignment
		public function getUserAnswerForEachQuestion($data, $attemptId) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				if($data['questionType'] == 0 || $data['questionType'] == 6) {
					$select = $sql->select();
					$select->from('answer_mcq');
					if(isset($data['resume']))
						$select->where(array(
							'attemptId'		=>	$attemptId,
							'questionId'	=>	$data['questionId']
						));
					else
						$select->where(array(
							'attemptId'		=>	$attemptId,
							'questionId'	=>	$data['id']
						));
					$select->columns(array('answer'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$temp = $temp->toArray();
					if($temp == null)
						$temp = array();
					return $temp;
				}
				else if($data['questionType'] == 1) {
					$select = $sql->select();
					$select->from('answer_tf');
					if(isset($data['resume']))
						$select->where(array(
							'attemptId'		=>	$attemptId,
							'questionId'	=>	$data['questionId']
						));
					else
						$select->where(array(
							'attemptId'		=>	$attemptId,
							'questionId'	=>	$data['id']
						));
					$select->columns(array('answer'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$temp = $temp->toArray();
					if($temp == null)
						$temp = array();
					return $temp;
				}
				else if($data['questionType'] == 2) {
					$select = $sql->select();
					$select->from('answer_ftb');
					if(isset($data['resume']))
						$select->where(array(
							'attemptId'		=>	$attemptId,
							'questionId'	=>	$data['questionId']
						));
					else
						$select->where(array(
							'attemptId'		=>	$attemptId,
							'questionId'	=>	$data['id']
						));
					$select->columns(array('answer'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$temp = $temp->toArray();
					if($temp == null)
						$temp = array();
					return $temp;
				}
				else if($data['questionType'] == 3) {
					$select = $sql->select();
					$select->from('answer_mtf');
					if(isset($data['resume']))
						$select->where(array(
							'attemptId'		=>	$attemptId,
							'questionId'	=>	$data['questionId']
						));
					else
						$select->where(array(
							'attemptId'		=>	$attemptId,
							'questionId'	=>	$data['id']
						));
					$select->columns(array('answer'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$temp = $temp->toArray();
					if($temp == null)
						$temp = array();
					return $temp;
				}
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//function to get answer or child question according to question type
		public function getAnswerForEachQuestionInAssignment($q) {
			$adapter = $this->adapter;
			if($q['questionType'] == 0 || $q['questionType'] == 6 || $q['questionType'] == 7) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				if(isset($q['resume']))
					$select->where(array('questionId'	=>	$q['questionId'],
								'delete'		=>	0));
				else
					$select->where(array('questionId'	=>	$q['id'],
								'delete'		=>	0));
				$select->from('options_mcq');
				$select->columns(array('id', 'option', 'correct'));
				//if($q['questionType'] == 0)
				$select->order(new \Zend\Db\Sql\Expression("RAND()"));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$options = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$options = $options->toArray();
				return $options;
			}
			else if($q['questionType'] == 1) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				if(isset($q['resume']))
					$select->where(array('questionId'	=>	$q['questionId'],
								'delete'		=>	0));
				else
					$select->where(array('questionId'	=>	$q['id'],
								'delete'		=>	0));
				$select->from('true_false');
				$select->columns(array('id', 'answer'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$options = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$options = $options->toArray();
				return $options;
			}
			else if($q['questionType'] == 2) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				if(isset($q['resume']))
					$select->where(array('questionId'	=>	$q['questionId'],
								'delete'		=>	0));
				else
					$select->where(array('questionId'	=>	$q['id'],
								'delete'		=>	0));
				$select->from('ftb');
				$select->columns(array('id', 'blanks'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$options = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$options = $options->toArray();
				return $options;
			}
			else if($q['questionType'] == 3) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				if(isset($q['resume']))
					$select->where(array('questionId'	=>	$q['questionId'],
								'delete'		=>	0));
				else
					$select->where(array('questionId'	=>	$q['id'],
								'delete'		=>	0));
				$select->from('options_mtf');
				$select->columns(array('id', 'columnA', 'columnB'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$options1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$options1 = $options1->toArray();
				
				$sql = new Sql($adapter);
				$select = $sql->select();
				if(isset($q['resume']))
					$select->where(array('questionId'	=>	$q['questionId'],
								'delete'		=>	0));
				else
					$select->where(array('questionId'	=>	$q['id'],
								'delete'		=>	0));
				$select->from('options_mtf');
				$select->columns(array('id', 'columnB'));
				$select->order(new \Zend\Db\Sql\Expression("RAND()"));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$options2 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$options2 = $options2->toArray();
				$temp['opt1'] = $options1;
				$temp['opt2'] = $options2;
				return $temp;
			}
		}

		//function to increase attempt of exam
		public function increaseAttempt($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				if($data->resumeMode == 0) {
					$examAttempts = new TableGateway('exam_attempts', $adapter, null,new HydratingResultSet());
					$examAttempts->insert(array(
						'examId' 			=> $data->examId,
						'studentId' 		=>	$data->userId,
						'startDate' 		=>	time(),
						'endDate' 			=>	0,
						'score' 			=>	0,
						'percentage'		=>	0,
						'time'				=>	0,
						'completed'			=>	0,
						'eachQuestionFlag'	=>	$data->eachQuestionFlag
					));
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
					$attemptId = $examAttempts->getLastInsertValue();
					$result->status = 1;
					$result->attemptId = $attemptId;
					$result->resumeMode = 0;
				}
				else {
					$result->status = 1;
					$result->attemptId = $data->attemptId;
					$result->resumeMode = $data->resumeMode;
				}
				return $result;
			}catch(Exception $e){
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		//
		public function increaseSubjectiveExamAttempt($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				if($data->resumeMode == 0) {
					$examAttempts = new TableGateway('subjective_exam_attempts', $adapter, null,new HydratingResultSet());
					$examAttempts->insert(array(
						'examId' 			=> $data->examId,
						'studentId' 		=>	$data->userId,
						'startDate' 		=>	time(),
						'completed'			=>	0,
						'eachQuestionFlag'	=>	$data->eachQuestionFlag
					));
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
					$attemptId = $examAttempts->getLastInsertValue();
					$result->status = 1;
					$result->attemptId = $attemptId;
					$result->resumeMode = 0;
				}
				else {
					$result->status = 1;
					$result->attemptId = $data->attemptId;
					$result->resumeMode = $data->resumeMode;
				}
				return $result;
			}catch(Exception $e){
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		//function save all question at once and later resume exam from same questions
		public function saveAllQuestions($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$totalScore = 0;
				$totalTime = 0;
				$maximumMarks = 0;
				$currentTime = time();
				$attemptId = $data->attemptId;
				//inserting each question
				$insert = 'INSERT INTO attempt_questions (attemptId, sectionId, questionId, parentId, questionType, correct, wrong, time, `status`, `check`, `order`) VALUES';
				foreach($data->questions as $key => $question) {
					$sectionId = $question->sectionId;
					$questionId = $question->questionId;
					$parentId = $question->parentId;
					$questionType = $question->questionType;
					$correct = $question->correct;
					$wrong = $question->wrong;
					$time = $question->timer;
					$status = $question->status;
					$order = $question->order;
					$check = 0;
					//question type is DPN(5) then it should also insert the child question values
					if($questionType == 5) {
						//insert each child question first (we dont have those questions in the data coming from user side so we have to fetch it from questions table)
						$inner = "INSERT INTO attempt_questions (attemptId, sectionId, questionId, parentId, questionType, correct, wrong, `time`, `status`, `check`, `order`) SELECT '{$attemptId}', '{$sectionId}', id, '-1', questionType, '0', '0', '0', '0', '0', '0' FROM questions WHERE parentId=926 AND `status`=1 AND `delete`=0;";
						$adapter->query($inner, $adapter::QUERY_MODE_EXECUTE);
					}
					$maximumMarks += $correct;
					$totalTime += $time;
					$insert .= "({$attemptId}, {$sectionId}, {$questionId}, {$parentId}, {$questionType}, {$correct}, {$wrong}, {$time}, {$status}, {$check}, {$order}),";
				}
				//saving the questions asked in this attempt with their right or wrong status
				$insert = substr($insert, 0, strlen($insert)-1).';';
				$adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);
				//updating attempt table for exam completion date and total score
				$percentage = $totalScore/$maximumMarks * 100;
				if($percentage < 0)
					$percentage = 0;
				
				//if time has to pause then we can't update system time;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exams');
				$select->where(array(
					'id'	=>	$data->examId
				));
				$select->columns(array('powerOption'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$powerOption = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$powerOption = $powerOption->toArray();
				$powerOption = $powerOption[0]['powerOption'];
				if($powerOption == 0) {
					$select = $sql->select();
					$select->from('attempt_questions');
					$select->where(array(
						'attemptId'	=>	$attemptId
					));
					$select->columns(array(
						'totalTime'	=>	new \Zend\Db\Sql\Expression("SUM(`time`)")
					));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$time = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$time = $time->toArray();
					$time = $time[0]['totalTime'];
					$update = $sql->update();
					$update->table('exam_attempts');
					$update->set(array(
								'endDate'		=>	new \Zend\Db\Sql\Expression("startDate + {$time}"),
								'score'			=>	$totalScore,
								'percentage'            =>	$percentage,
								'time'			=>	$totalTime,
								'completed'		=>	$data->completed
					));
					$update->where(array('id'	=>	$attemptId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
				}
				else {				
					$update = $sql->update();
					$update->table('exam_attempts');
					$update->set(array(
								'endDate'		=>	$currentTime,
								'score'			=>	$totalScore,
								'percentage'	=>	$percentage,
								'time'			=>	$totalTime,
								'completed'		=>	$data->completed
					));
					$update->where(array('id'	=>	$attemptId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		//function save all question at once and later resume exam from same questions
		public function checkAndSave($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result = new stdClass();
			$adapter = $this->adapter;
			require_once('html2text.php');
			try {
				$adapter = $this->adapter;
				//require 'html2text.php';
				$totalScore = 0;
				$totalTime = 0;
				$maximumMarks = 0;
				$currentTime = time();
				$attemptId = $data->attemptId;
				$examId = $data->examId;
				$check = 0;
				//fetching each question from database
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('attempt_questions');
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('attemptId', $attemptId);
				$where->greaterThanOrEqualTo('parentId', 0);
				$select->where($where);
				$selectStatement = $sql->getSqlStringForSqlObject($select);
				$questions = $adapter->query($selectStatement, $adapter::QUERY_MODE_EXECUTE);
				$questions = $questions->toArray();
				foreach($questions as $key => $question) {
					$sectionId = $question['sectionId'];
					$questionId = $question['questionId'];
					$parentId = $question['parentId'];
					$questionType = $question['questionType'];
					$correct = $question['correct'];
					$wrong = $question['wrong'];
					$time = $question['time'];
					$status = $question['status'];
					$check = 0;
					$maximumMarks += $correct;
					$totalTime += $time;
					if($questionType == 0) {
						//if user has given answer fetch original ans else no need to check answer
						if($status == 2 || $status == 4) {
							//fetch original ans and compare with this ans (original is html)
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from('options_mcq');
							$select->where(array(
										'questionId'	=>	$questionId,
										'correct'		=>	1,
										'delete'		=>	0
							));
							$select->columns(array('option'));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$answer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							if ($answer->count() !== 0) {
								$answer = $answer->toArray();
								//$origAns = strtolower(trim(html2text($answer[0]['option'])));
								$origAns = strtolower(trim($answer[0]['option']));
								//fetch user answer from database (in plain text)
								$select = $sql->select();
								$select->from('answer_mcq');
								$select->where(array(
										'attemptId'		=>	$attemptId,
										'questionId'	=>	$questionId
								));
								$select->columns(array('answer'));
								$selectString = $sql->getSqlStringForSqlObject($select);
								$userAns = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
								$userAns = $userAns->toArray();
								$userAns = $userAns[0]['answer'];
								$userAns = strtolower(trim($userAns));
								if($origAns == $userAns) {
									$totalScore += $correct;
									$check = 1;
								}
								else {
									$totalScore -= $wrong;
									$check = 2;
								}
							}
						}
					}
					else if($questionType == 6) {
						//if user has given answer fetch original ans else no need to check answer
						if($status == 2 || $status == 4) {
							//fetch original ans and compare with this ans (original is html)
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from('options_mcq');
							$select->where(array(
										'questionId'	=>	$questionId,
										'correct'		=>	1,
										'delete'		=>	0
							));
							$select->columns(array('option'));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$answer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$answer = $answer->toArray();
							//coping all correct answers into single array
							$origAns = array();
							for($i = 0; $i < count($answer); $i++) {
								$origAns[] = strtolower(trim($answer[$i]['option']));
							}
							$checkFlag = true;
							//fetch user answer from database (in plain text)
							$select = $sql->select();
							$select->from('answer_mcq');
							$select->where(array(
									'attemptId'		=>	$attemptId,
									'questionId'	=>	$questionId
							));
							$select->columns(array('answer'));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$userAnswer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$userAnswer = $userAnswer->toArray();
							for($i = 0; $i < count($answer); $i++) {
								//$origAns = strtolower(trim(html2text($answer[$i]['option'])));
								$userAns = strtolower(trim($userAnswer[$i]['answer']));
								if(!in_array($userAns, $origAns))
									$checkFlag = false;
							}
							if($checkFlag) {
								$totalScore += $correct;
								$check = 1;
							}
							else {
								$totalScore -= $wrong;
								$check = 2;
							}
						}
					}
					else if($questionType == 1) {
						//if user has given answer fetch original ans else no need to check answer
						if($status == 2 || $status == 4) {
							//fetch original ans and compare with this ans (original is html)
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from('true_false');
							$select->where(array(
										'questionId'	=>	$questionId,
										'delete'		=>	0
							));
							$select->columns(array('answer'));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$answer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$answer = $answer->toArray();
							$origAns = $answer[0]['answer'];
							//fetch user answer from database (in plain text)
							$select = $sql->select();
							$select->from('answer_tf');
							$select->where(array(
									'attemptId'		=>	$attemptId,
									'questionId'	=>	$questionId
							));
							$select->columns(array('answer'));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$userAnswer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$userAnswer = $userAnswer->toArray();
							$userAns = $userAnswer[0]['answer'];
							if($origAns == $userAns) {
								$totalScore += $correct;
								$check = 1;
							}
							else {
								$totalScore -= $wrong;
								$check = 2;
							}
						}
					}
					else if($questionType == 2) {
						//if user has given answer fetch original ans else no need to check answer
						if($status == 2 || $status == 4) {
							//fetch original ans and compare with this ans (original is html)
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from('ftb');
							$select->where(array(
										'questionId'	=>	$questionId,
										'delete'		=>	0
							));
							$select->columns(array('blanks'));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$answer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$answer = $answer->toArray();
							$checkFlag = true;
							//fetch user answer from database (in plain text)
							$select = $sql->select();
							$select->from('answer_ftb');
							$select->where(array(
									'attemptId'		=>	$attemptId,
									'questionId'	=>	$questionId
							));
							$select->columns(array('answer'));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$userAnswer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$userAnswer = $userAnswer->toArray();
							for($i = 0; $i < count($answer); $i++) {
								//$origAns = strtolower(trim(html2text($answer[$i]['blanks'])));
								$origAns = html_entity_decode(strtolower(trim($answer[$i]['blanks'])));
								$userAns = html_entity_decode(strtolower(trim($userAnswer[$i]['answer'])));
								if($origAns != $userAns)
									$checkFlag = false;
							}
							if($checkFlag) {
								$totalScore += $correct;
								$check = 1;
							}
							else {
								$totalScore -= $wrong;
								$check = 2;
							}
						}
					}
					else if($questionType == 3) {
						//if user has given answer fetch original ans else no need to check answer
						if($status == 2 || $status == 4) {
							//fetch original ans and compare with this ans (original is html)
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from('options_mtf');
							$select->where(array(
										'questionId'	=>	$questionId,
										'delete'		=>	0
							));
							$select->columns(array('columnB'));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$answer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$answer = $answer->toArray();
							$checkFlag = true;
							//fetch user answer from database (in plain text)
							$select = $sql->select();
							$select->from('answer_mtf');
							$select->where(array(
									'attemptId'		=>	$attemptId,
									'questionId'	=>	$questionId
							));
							$select->columns(array('answer'));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$userAnswer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$userAnswer = $userAnswer->toArray();
							for($i = 0; $i < count($answer); $i++) {
								//$origAns = strtolower(trim(html2text($answer[$i]['columnB'])));
								$origAns = strtolower(trim($answer[$i]['columnB']));
								$userAns = strtolower(trim($userAnswer[$i]['answer']));
								//$insertAnswer .= "('{$attemptId}', '{$questionId}', '{$question->answer[$i]}'),";
								if($origAns != $userAns)
									$checkFlag = false;
							}
							if($checkFlag) {
								$totalScore += $correct;
								$check = 1;
							}
							else {
								$totalScore -= $wrong;
								$check = 2;
							}
						}
					}
					else if($questionType == 5) {
						if($status == 2 || $status == 4) {
							//fetch all the questions under this dpn question
							//"SELECT id, questionType from questions WHERE parentId=$questionId AND status=1 AND delete=0";
							$select = $sql->select();
							$select->from('questions');
							$select->columns(array('id', 'questionType'));
							$select->where(array(
								'parentId'	=>	$questionId,
								'status'	=>	1,
								'delete'	=>	0
							));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$childs = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$childs = $childs->toArray();
							$factor = count($childs);
							if($factor == 0)
								$factor = 1;
							$flag = 0;
							foreach($childs as $c) {
								$childCheck = 0;
								
								// fetch user given answer according to question type
								// check if there is some data i.e. user has actually given the answer or not
								// if not given leave the tables as it is
								// else check the answer and add or subtract marks accordingly
								// and also update the check status accordingly
								
								//for MCQ questions
								if($c['questionType'] == 0) {
									$select = $sql->select();
									$select->from('answer_mcq');
									$select->where(array(
										'attemptId'		=>	$attemptId,
										'questionId'	=>	$c['id']
									));
									$select->columns(array('answer'));
									$selectString = $sql->getSqlStringForSqlObject($select);
									$userAns = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
									$userAns = $userAns->toArray();
									if(count($userAns) > 0) {
										$userAns = $userAns[0]['answer'];
										$userAns = strtolower(trim($userAns));
										//now fetching original answer of this question
										$select = $sql->select();
										$select->from('options_mcq');
										$select->where(array(
													'questionId'	=>	$c['id'],
													'correct'		=>	1,
													'delete'		=>	0
										));
										$select->columns(array('option'));
										$selectString = $sql->getSqlStringForSqlObject($select);
										$answer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
										$answer = $answer->toArray();
										//$origAns = strtolower(trim(html2text($answer[0]['option'])));
										$origAns = strtolower(trim($answer[0]['option']));
										//checking if the answer is correct or not
										if($origAns == $userAns) {
											$totalScore += $correct / $factor;
											$childCheck = 1;
										}
										else {
											$totalScore -= $wrong / $factor;
											$childCheck = 2;
											$flag = 1;
										}
									}
									//else we dont have to disturb the tables
								}
								//for T/F questions
								else if($c['questionType'] == 1) {
									$select = $sql->select();
									$select->from('answer_tf');
									$select->where(array(
										'attemptId'		=>	$attemptId,
										'questionId'	=>	$c['id']
									));
									$select->columns(array('answer'));
									$selectString = $sql->getSqlStringForSqlObject($select);
									$userAns = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
									$userAns = $userAns->toArray();
									if(count($userAns) > 0) {
										$userAns = $userAns[0]['answer'];
										//now fetching original answer of this question
										$select = $sql->select();
										$select->from('true_false');
										$select->where(array(
											'questionId'	=>	$c['id'],
											'delete'		=>	0
										));
										$select->columns(array('answer'));
										$selectString = $sql->getSqlStringForSqlObject($select);
										$answer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
										$answer = $answer->toArray();
										$origAns = $answer[0]['answer'];
										//checking if the answer is correct or not
										if($origAns == $userAns) {
											$totalScore += $correct / $factor;
											$childCheck = 1;
										}
										else {
											$totalScore -= $wrong / $factor;
											$childCheck = 2;
											$flag = 1;
										}
									}
								}
								//for fill in the blanks
								else if($c['questionType'] == 2) {
									$select = $sql->select();
									$select->from('answer_ftb');
									$select->where(array(
										'attemptId'		=>	$attemptId,
										'questionId'	=>	$c['id']
									));
									$select->columns(array('answer'));
									$selectString = $sql->getSqlStringForSqlObject($select);
									$userAnswer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
									$userAnswer = $userAnswer->toArray();
									if(count($userAnswer) > 0) {
										//fetching the original answer of this question
										$select = $sql->select();
										$select->from('ftb');
										$select->where(array(
													'questionId'	=>	$c['id'],
													'delete'		=>	0
										));
										$select->columns(array('blanks'));
										$selectString = $sql->getSqlStringForSqlObject($select);
										$answer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
										$answer = $answer->toArray();
										$checkFlag = true;
										//checking the answer it it is correct
										for($i = 0; $i < count($answer); $i++) {
											//$origAns = strtolower(trim(html2text($answer[$i]['blanks'])));
											$origAns = strtolower(trim($answer[$i]['blanks']));
											$userAns = strtolower(trim($userAnswer[$i]['answer']));
											if($origAns != $userAns)
												$checkFlag = false;
										}
										if($checkFlag) {
											$totalScore += $correct / $factor;
											$childCheck = 1;
										}
										else {
											$totalScore -= $wrong / $factor;
											$childCheck = 2;
											$flag = 1;
										}
									}
								}
								//for MAQ questions
								else if($c['questionType'] == 6) {
									$select = $sql->select();
									$select->from('answer_mcq');
									$select->where(array(
										'attemptId'		=>	$attemptId,
										'questionId'	=>	$c['id']
									));
									$select->columns(array('answer'));
									$selectString = $sql->getSqlStringForSqlObject($select);
									$userAnswer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
									$userAnswer = $userAnswer->toArray();
									if(count($userAnswer) > 0) {
										//fetching the correct answer of this question
										$select = $sql->select();
										$select->from('options_mcq');
										$select->where(array(
													'questionId'	=>	$c['id'],
													'correct'		=>	1,
													'delete'		=>	0
										));
										$select->columns(array('option'));
										$selectString = $sql->getSqlStringForSqlObject($select);
										$answer = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
										$answer = $answer->toArray();
										//coping all correct answers into single array
										$origAns = array();
										for($i = 0; $i < count($answer); $i++) {
											$origAns[] = strtolower(trim($answer[$i]['option']));
										}
										$checkFlag = true;
										//checking the answer
										for($i = 0; $i < count($answer); $i++) {
											//$origAns = strtolower(trim(html2text($answer[$i]['option'])));
											$userAns = strtolower(trim($userAnswer[$i]['answer']));
											if(!in_array($userAns, $origAns))
												$checkFlag = false;
										}
										if($checkFlag) {
											$totalScore += $correct / $factor;
											$childCheck = 1;
										}
										else {
											$totalScore -= $wrong / $factor;
											$childCheck = 2;
											$flag = 1;
										}
									}
								}
								
								//now updating the check of the child question if it was given by user
								if($childCheck != 0) {
									$update = $sql->update();
									$update->table('attempt_questions');
									$update->set(array(
										'check'	=>	$childCheck
									));
									$update->where(array(
										'attemptId'		=>	$attemptId,
										'questionId'	=>	$c['id']
									));
								}
							}
							$check = 1;
							if($flag == 1)
								$check = 2;
						}
					}
					//updating the check status
					$update = $sql->update();
					$update->table('attempt_questions');
					$update->set(array(
								'check'		=>	$check
					));
					$update->where(array(
							'attemptId'		=>	$attemptId,
							'questionId'	=>	$questionId
					));
					$update = $sql->getSqlStringForSqlObject($update);
					$adapter->query($update, $adapter::QUERY_MODE_EXECUTE);
				}
				if ($maximumMarks <= 0) {
					$result->status = 0;
					$result->message = "Maximum marks is 0! There is some error, please try again!";
					return $result;
				}
				//updating attempt table for exam completion date and total score
				$percentage = $totalScore/$maximumMarks * 100;
				if($percentage < 0)
					$percentage = 0;
				
				//completing the exam when user submits it not forcefully
				$update = $sql->update();
				$update->table('exam_attempts');
				$update->set(array(
							'endDate'		=>	$currentTime,
							'score'			=>	$totalScore,
							'percentage'	=>	$percentage,
							'time'			=>	$totalTime,
							'completed'		=>	1
				));
				$update->where(array('id'	=>	$attemptId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();

				$selectString ="SELECT subjectId,studentId
								FROM exam_attempts ea
								INNER JOIN exams e on e.id=ea.examId
								WHERE ea.id=$attemptId";
				$subjectId = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjectId = $subjectId->toArray();
				$result->subjectId = $subjectId[0]['subjectId'];
				//$percentage = 67.77;

				require 'Subject.php';
				$s = new Subject();
				$data->contentId = $data->examId;
				$data->contentType = 'exam';
				$data->percentage = $percentage;
				$data->studentId = $subjectId[0]['studentId'];
				$resSubjectRewards = $s->putSubjectRewardsbyContent($data);
				
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		//function save all question at once and later resume exam from same questions
		public function saveSubjectiveExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result = new stdClass();
			$adapter = $this->adapter;
			require_once('html2text.php');
			try {
				$adapter = $this->adapter;
				//require 'html2text.php';
				$totalTime = 0;
				$currentTime = time();
				$attemptId = $data->attemptId;
				$examId = $data->examId;
				
				$statement = "UPDATE subjective_exam_attempts AS ea, exam_subjective AS e
								SET ea.endDate=$currentTime, ea.completed=1, ea.time=(
									SELECT SUM(aq.time)
									FROM subjective_attempt_questions AS aq
									WHERE aq.attemptId=ea.id)
								WHERE ea.completed=0 AND ea.examId=e.id AND ea.id={$attemptId};";
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();

				//fetching total questions sum time
				/*$sql = new Sql($adapter);
				$select = $sql->select();
				$select->columns(array('SUM(time)'));
				$select->from('subjective_attempt_questions');
				$select->where(array(
					'attemptid'					=>	$attemptId,
				));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$uploads = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$uploads = $uploads->toArray();*/
				//$totalTime = $totalTime[0]['sum_time'];
				//completing the exam when user submits it not forcefully
				/*$update = $sql->update();
				$update->table('exam_attempts');
				$update->set(array(
							'endDate'		=>	$currentTime,
							'time'			=>	$totalTime,
							'completed'		=>	1
				));
				$update->where(array('id'	=>	$attemptId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();*/
				
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		//checkResultShow to check if result can be shown or not
		public function checkResultShow($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exams');
				$select->where(array(
					'id'	=>	$data->examId
				));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exam = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exam = $exam->toArray();
				//print_r($exam);
				$result->status = 1;
				$result->exam = $exam;
				return $result;
			
			}catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		//function to update each answer according to user input
		public function updateUserAnswer($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				$this->handleAnswers($data);
				
				//if time has to pause then we can't update system time;
				//this will be executed when time is not lost
				$statement = "UPDATE exam_attempts AS ea, exams AS e SET ea.endDate=ea.startDate+(SELECT SUM(aq.time) FROM attempt_questions AS aq WHERE aq.attemptId=ea.id) WHERE ea.completed=0 AND ea.examId=e.id AND e.powerOption=0 AND ea.id={$data->attemptId};";
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				//this will execute when time is lost
				$statement = "UPDATE exam_attempts AS ea, exams AS e SET ea.endDate=".time()." WHERE ea.examId=e.id AND e.powerOption=1 AND ea.completed=0 AND ea.id={$data->attemptId};";
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();

				$selectString ="SELECT subjectId
								FROM exam_attempts ea
								INNER JOIN exams e on e.id=ea.examId
								WHERE ea.id=$data->attemptId";
				$subjectId = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjectId = $subjectId->toArray();
				$result->subjectId = $subjectId[0]['subjectId'];
				
				$result->status = 1;
				return $result;
			}catch(Exception $e){
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		//function to handle the answers in db given by user
		public function handleAnswers($data) {

			/*$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();*/
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				
				//checking if the function is called from php itself for question type DPN(5) then we don't have to update time
				if(!(isset($data->phpFlag) && $data->phpFlag == true)) {
					$update = $sql->update();
					$update->table('attempt_questions');
					$update->set(array(
							'time'		=>	$data->question->timer,
							'status'	=>	$data->question->status
								));
					$update->where(array(
							'attemptId'		=>	$data->attemptId,
							'questionId'	=>	$data->question->questionId
					));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
				}
				
				if(isset($data->question->answer)) {
					//updating database according to question type
					if($data->question->questionType == 0) {
						//deleting old answers
						$query = "DELETE FROM answer_mcq WHERE attemptId={$data->attemptId} AND questionId={$data->question->questionId};";
						$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						//adding new answers
						if(isset($data->question->answer)) {
							if(count($data->question->answer) > 0) {
								$insert = new TableGateway('answer_mcq', $adapter, null, new HydratingResultSet());
								$insert->insert(array(
										'attemptId' 	=>	$data->attemptId,
										'questionId' 	=>	$data->question->questionId,
										'answer' 		=>	$data->question->answer
								));
							}
						}
					}
					else if($data->question->questionType == 7) {
						//deleting old answers
						$query = "DELETE FROM answer_mcq WHERE attemptId={$data->attemptId} AND questionId={$data->question->questionId};";
						$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						//adding new answers
						if(isset($data->question->answer)) {
							if(count($data->question->answer) > 0) {
								$insert = new TableGateway('answer_mcq', $adapter, null, new HydratingResultSet());
								$insert->insert(array(
										'attemptId' 	=>	$data->attemptId,
										'questionId' 	=>	$data->question->questionId,
										'answer' 		=>	$data->question->answer
								));
							}
						}
					}
					else if($data->question->questionType == 6) {
						//deleting old answers
						$query = "DELETE FROM answer_mcq WHERE attemptId={$data->attemptId} AND questionId={$data->question->questionId};";
						$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						//adding new answers
						if(isset($data->question->answer)) {
							if(count($data->question->answer) > 0) {
								$insert = "INSERT INTO answer_mcq (attemptId, questionId, answer) VALUES";
								foreach($data->question->answer as $answer) {
									$insert .= "({$data->attemptId}, {$data->question->questionId}, '{$answer}'),";
								}
								$insert = substr($insert, 0, strlen($insert)-1).';';
								$adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);
							}
						}	
					}
					else if($data->question->questionType == 1) {
						//deleting old answers
						$query = "DELETE FROM answer_tf WHERE attemptId={$data->attemptId} AND questionId={$data->question->questionId};";
						$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						//adding new answers
						if(isset($data->question->answer)) {
							$insert = new TableGateway('answer_tf', $adapter, null, new HydratingResultSet());
							$insert->insert(array(
									'attemptId' 	=>	$data->attemptId,
									'questionId' 	=>	$data->question->questionId,
									'answer' 		=>	$data->question->answer
							));
						}	
					}
					else if($data->question->questionType == 2) {
						//deleting old answers
						$query = "DELETE FROM answer_ftb WHERE attemptId={$data->attemptId} AND questionId={$data->question->questionId};";
						$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						//adding new answers
						if(isset($data->question->answer)) {
							if(count($data->question->answer) > 0) {
								$insert = "INSERT INTO answer_ftb (attemptId, questionId, answer) VALUES";
								foreach($data->question->answer as $answer) {
									$insert .= "({$data->attemptId}, {$data->question->questionId}, '{$answer}'),";
								}
								$insert = substr($insert, 0, strlen($insert)-1).';';
								$adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);
							}
						}
					}
					else if($data->question->questionType == 3) {
						//deleting old answers
						$query = "DELETE FROM answer_mtf WHERE attemptId={$data->attemptId} AND questionId={$data->question->questionId};";
						$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						//adding new answers
						if(isset($data->question->answer)) {
							if(count($data->question->answer) > 0) {
								$insert = "INSERT INTO answer_mtf (attemptId, questionId, answer) VALUES";
								foreach($data->question->answer as $answer) {
									$insert .= "({$data->attemptId}, {$data->question->questionId}, '{$answer}'),";
								}
								$insert = substr($insert, 0, strlen($insert)-1).';';
								$adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);
							}
						}
					}
				}
				if(isset($data->question->childs) && $data->question->questionType == 5) {
					foreach($data->question->childs as $children) {
						$child = new stdClass();
						$child->question = new stdClass();
						$child->attemptId = $data->attemptId;
						$child->examId = $data->examId;
						$child->question->questionType = $children->questionType;
						$child->question->questionId = $children->id;
						$child->phpFlag = true;
						if(isset($children->answer)) {
							$child->question->answer = $children->answer;
							$this->handleAnswers($child);
						}
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				//$db->commit();
			}catch(Exception $e) {
				//$db->rollBack();
				return;
			}
		}

		//function to handle the subjective answers in db given by user
		public function updateSubjectiveAnswer($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result = new stdClass();
			try {
				$ext = "txt";
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$answerArrays= $data->questions;
				$fileLocation='';
				if(count($answerArrays->subQuestions)==0)
				{
					//print_r($answerArrays);
					$answer=trim($answerArrays->answer);
					//print_r($answer);
					$answerUpload=$answerArrays->upload;
					$questionId=$answerArrays->id;

					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->columns(array('id'));
					$select->from('subjective_attempt_questions');
					$select->where(array(
						'attemptId'	=>	$data->subjectiveExamId,
						'questionId'=>	$questionId
					));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subAttemptQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$subAttemptQuestions = $subAttemptQuestions->toArray();					
					$subAttemptQId = $subAttemptQuestions[0]['id'];

					$completePath='';
					if($answer != '') {

						//$answer=$answerArrays->answer;
						$adapter = $this->adapter;
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('answer_subjective');
						$select->where(array(
							'subjectiveExam_attemptid'	=>	$data->subjectiveExamId,
							'attemptQuestionId'         =>	$subAttemptQId,
							'answerType'				=>	'text'
						));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$ans = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$ans = $ans->toArray();
						if (count($ans)>0) {
							$answerString = $ans[0]['answer'];
							if ($answerString != $answer) {
								/*$update = $sql->update();
								$update->table('answer_subjective');
								$update->set(array(
										'answer'					=>	$answerString,
										'status'					=>	1
								));
								$update->where(array(
										'subjectiveExam_attemptid'	=>  $data->subjectiveExamId,
										'questionId'				=>	$questionId,
										'answerType'				=>	'text'
								));
								$statement = $sql->getSqlStringForSqlObject($update);*/
								$statement = "UPDATE `answer_subjective` AS asu
								INNER JOIN `subjective_attempt_questions` AS saq ON saq.`id`=asu.`attemptQuestionId` AND asu.`subjectiveExam_attemptid`=saq.`attemptId`
								SET asu.answer='{$answerString}', asu.status=1
								WHERE asu.subjectiveExam_attemptid={$data->subjectiveExamId}
								AND asu.attemptQuestionId={$subAttemptQId}
								AND asu.answerType='text'";
								$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
							}
						} else {
							$answer_subjective = new TableGateway('answer_subjective', $adapter, null,new HydratingResultSet());
							$insert = array(
											'subjectiveExam_attemptid'		=>  $data->subjectiveExamId,
											'attemptQuestionId'				=>	$subAttemptQId,
											//'questionId'					=>	$questionId,
											'answer'						=>	$answer,
											'answerType'					=>	'text',
											'status'						=>	1
										);
							$answer_subjective->insert($insert);
						}
					}
	
					if(count($answerUpload)>0){
						// for upto 4 screenshot
						//$completePath=$answerPDF;

						$adapter = $this->adapter;
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->columns(array('id','answer'));
						$select->from('answer_subjective');
						$select->where(array(
							'subjectiveExam_attemptid'	=>	$data->subjectiveExamId,
							'attemptQuestionId'			=>	$subAttemptQId,
							'answerType'				=>	'upload'
						));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$uploads = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$uploads = $uploads->toArray();
						$uploadsDB = array();
						foreach ($uploads as $key => $value) {
							$uploadsDB[$value['id']] = $value['answer'];
						}
						foreach($answerUpload as $key=>$value)
						{
							$completePath=$value;
							
							//$answer=$answerArrays->answer;
							$adapter = $this->adapter;
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from('answer_subjective');
							$select->where(array(
								'subjectiveExam_attemptid'	=>	$data->subjectiveExamId,
								'attemptQuestionId'         =>	$subAttemptQId,
								'answer'					=>	$completePath,
								'answerType'				=>	'upload'
							));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$ans = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$ans = $ans->toArray();
							if (count($ans) == 0) {
								$answer_subjective = new TableGateway('answer_subjective', $adapter, null,new HydratingResultSet());
								$insert = array(
												'subjectiveExam_attemptid'		=>  $data->subjectiveExamId,
												'attemptQuestionId'				=>	$subAttemptQId,
												//'questionId'					=>	$questionId,
												'answer'						=>	$completePath,
												'answerType'					=>	'upload',
												'status'						=>	1
											);
								$answer_subjective->insert($insert);
							} else {
								if (isset($uploadsDB[$ans[0]['id']]) && !empty($uploadsDB[$ans[0]['id']])) {
									unset($uploadsDB[$ans[0]['id']]);
								}
							}
						}
						if (count($uploadsDB)>0) {
							foreach ($uploadsDB as $key => $value) {
								$update = $sql->update();
								$update->table('answer_subjective');
								$update->set(array(
										'status'						=>	0
								));
								$update->where(array(
										'id'			=>  $key,
										'answer'	=>	$value,
										'answerType'	=>	'upload'
								));
								$statement = $sql->getSqlStringForSqlObject($update);
								$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
							}
						}
					}
				}
				else{
					$subquestionType=$answerArrays->listType;
					$subquestion=$answerArrays->subQuestions;

					foreach($subquestion as $key=>$value)
					{
						$answer=$value->answer;
						$answerPDF=$value->upload;
						$questionId=$value->id;

						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->columns(array('id'));
						$select->from('subjective_attempt_questions');
						$select->where(array(
							'attemptId'	=>	$data->subjectiveExamId,
							'questionId'=>	$questionId
						));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$subAttemptQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$subAttemptQuestions = $subAttemptQuestions->toArray();						
						$subAttemptQId = $subAttemptQuestions[0]['id'];

						$completePath='';
						if($answer != '') {
							$answer=$value->answer;

							//$answer=$answerArrays->answer;
							$adapter = $this->adapter;
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from('answer_subjective');
							$select->where(array(
								'subjectiveExam_attemptid'	=>	$data->subjectiveExamId,
								'attemptQuestionId'			=>	$subAttemptQId,
								'answerType'				=>	'text'
							));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$ans = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$ans = $ans->toArray();
							if (count($ans)>0) {
								$update = $sql->update();
								$update->table('answer_subjective');
								$update->set(array(
										'answer'					=>	$answer,
										'status'					=>	1
								));
								$update->where(array(
										'subjectiveExam_attemptid'	=>  $data->subjectiveExamId,
										'attemptQuestionId'			=>	$subAttemptQId,
										'answerType'				=>	'text'
								));
								$statement = $sql->getSqlStringForSqlObject($update);
								$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
							} else {
								$answer_subjective = new TableGateway('answer_subjective', $adapter, null,new HydratingResultSet());
								$insert = array(
												'subjectiveExam_attemptid'	=>  $data->subjectiveExamId,
												'attemptQuestionId'			=>	$subAttemptQId,
												//'questionId'				=>	$questionId,
												'answer'					=>	$answer,
												'answerType'				=>	'text',
												'status'					=>	1
											);
								$answer_subjective->insert($insert);
							}
						}
						if(count($answerPDF)>0){
							
							// for upto 4 screenshot
							//$answer=$answerPDF;

							$adapter = $this->adapter;
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->columns(array('id','answer'));
							$select->from('answer_subjective');
							$select->where(array(
								'subjectiveExam_attemptid'	=>	$data->subjectiveExamId,
								'attemptQuestionId'			=>	$subAttemptQId,
								'answerType'				=>	'upload'
							));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$uploads = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$uploads = $uploads->toArray();
							$uploadsDB = array();
							foreach ($uploads as $key => $value) {
								$uploadsDB[$value['id']] = $value['answer'];
							}

							foreach($answerPDF as $key=>$value)
							{
								$completePath=$value;
								
								//$answer=$answerArrays->answer;
								$adapter = $this->adapter;
								$sql = new Sql($adapter);
								$select = $sql->select();
								$select->from('answer_subjective');
								$select->where(array(
									'subjectiveExam_attemptid'	=>	$data->subjectiveExamId,
									'attemptQuestionId'			=>	$subAttemptQId,
									'answer'					=>	$completePath,
									'answerType'				=>	'upload'
								));
								$selectString = $sql->getSqlStringForSqlObject($select);
								$ans = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
								$ans = $ans->toArray();
								if (count($ans) == 0) {
									$answer_subjective = new TableGateway('answer_subjective', $adapter, null,new HydratingResultSet());
									$insert = array(
													'subjectiveExam_attemptid'	=>  $data->subjectiveExamId,
													'attemptQuestionId'			=>	$subAttemptQId,
													//'questionId'				=>	$questionId,
													'answer'					=>	$completePath,
													'answerType'				=>	'upload',
													'status'					=>	1
												);
									$answer_subjective->insert($insert);
								} else {
									if (isset($uploadsDB[$ans[0]['id']]) && !empty($uploadsDB[$ans[0]['id']])) {
										unset($uploadsDB[$ans[0]['id']]);
									}
								}
							}
							if (count($uploadsDB)>0) {
								foreach ($uploadsDB as $key => $value) {
									$update = $sql->update();
									$update->table('answer_subjective');
									$update->set(array(
											'status'						=>	0
									));
									$update->where(array(
											'id'			=>  $key,
											'answer'	=>	$value,
											'answerType'	=>	'upload'
									));
									$statement = $sql->getSqlStringForSqlObject($update);
									$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
								}
							}
						}
					}
				}
				$quesTimer= $data->qtimer;
				//$answer=$answerArrays->answer;
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subjective_attempt_questions');
				$select->where(array(
					'attemptId'		=>	$data->subjectiveExamId
				));
				$select->order('questionId ASC');
				$selectString = $sql->getSqlStringForSqlObject($select);
				$qAttempted = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$qAttempted = $qAttempted->toArray();
				foreach ($quesTimer as $key => $value) {
					if ($value>0) {
						$flagAttempt = false;
						if (count($qAttempted)>0) {
							foreach ($qAttempted as $key1 => $qAttempt) {
								if ($qAttempt['questionId'] == $key) {
									$flagAttempt = true;

									$update = $sql->update();
									$update->table('subjective_attempt_questions');
									$update->set(array(
											'time'		=>	$qAttempt['time']+$value
									));
									$update->where(array(
											'id'		=>  $qAttempt['id']
									));
									$statement = $sql->getSqlStringForSqlObject($update);
									$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
								}
							}
						}
						if (!$flagAttempt) {
							$subjective_attempt_questions = new TableGateway('subjective_attempt_questions', $adapter, null,new HydratingResultSet());
							$insert = array(
											'attemptId'		=>	$data->subjectiveExamId,
											'questionId'	=>	$key,
											'time'			=>	$value
										);
							$subjective_attempt_questions->insert($insert);
						}
						$quesTimer->$key = 0;
					}
				}
				//this will execute when time is lost
				$time = time();
				$statement = "UPDATE subjective_exam_attempts AS ea
								SET ea.endDate={$time}, ea.lastQuestionId = $questionId
								WHERE ea.completed=0 AND ea.id={$data->subjectiveExamId}";
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->time	= date("h:i:s A", time());
				$result->qtimer = $quesTimer;
				$result->status = 1;
				return $result;
			}
			catch(Exception $e){
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		//function to get time and present section of paused exam
		public function getTimeAndSection($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('ea'	=>	'exam_attempts'))
				->join(array('e'	=>	'exams'),
						'e.id = ea.examId',
						array('type'	=>	'type',
							'tt_status'	=>	'tt_status',
							'totalTime'	=>	'totalTime'
						)
				);
				$select->where(array('ea.id'	=>	$data->attemptId));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				$temp = $temp[0];
				$timeTaken = $temp['endDate'] - $temp['startDate'];
				$result->type = $temp['type'];
				if($temp['type'] == 'Exam') {
					if($temp['tt_status'] == 0)
						$timeTaken = ($temp['totalTime'] * 60) - $timeTaken;
					else {
						$select = $sql->select();
						$select->from(array('aq'	=>	'attempt_questions'))
						->join(array('es'	=>	'exam_sections'),
								'aq.sectionId = es.id',
								array('time'	=>	'time')
						)
						->order('order ASC');
						$select->where(array(
							'examId'	=>	$temp['examId'],
							'attemptId'	=>	$data->attemptId
						));
						$select->columns(array('sectionId'	=>	new \Zend\Db\Sql\Expression("DISTINCT(sectionId)")));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$sectionTime = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$sectionTime = $sectionTime->toArray();
						$timing = 0;
						$sectionCount = -1;
						for($i = 0; $i < count($sectionTime); $i++) {
							$timing += ($sectionTime[$i]['time'] * 60);
							if($timing > $timeTaken) {
								$sectionCount = ($i + 1);
								$timeTaken = $timing - $timeTaken;
								break;
							}
						}
						$result->sectionCount = $sectionCount;
					}
				}
				$result->time = $timeTaken;
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//function to get time and present section of paused exam
		public function getSubjectiveTime($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('ea'	=>	'subjective_exam_attempts'))
				->join(array('e'	=>	'exam_subjective'),
						'e.id = ea.examId',
						array('totalTime'	=>	'totalTime'
						)
				);
				$select->where(array('ea.id'	=>	$data->attemptId));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				$temp = $temp[0];
				$timeTaken = 0;
				if (!!$temp['endDate']) {
					$timeTaken = $temp['endDate'] - $temp['startDate'];
				}
				
				$timeTaken = ($temp['totalTime'] * 60) - $timeTaken;
				$result->time = $timeTaken;
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//function to get exam assignment resume status
		public function getExamResumeStatus($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exam_attempts');
				$select->where(array(
						'examId'	=>	$data->examId,
						'studentId'	=>	$data->userId,
						'completed'	=>	0
				));
				$select->columns(array('id', 'eachQuestionFlag'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$tempData = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$tempData = $tempData->toArray();
				if(count($tempData) == 0)
					$result->resumeMode = 0;
				else {
					$result->resumeMode = 1;
					$result->data = $tempData[0];
				}
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function getSubjectiveExamResumeStatus($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subjective_exam_attempts');
				$select->where(array(
						'examId'	=>	$data->examId,
						'studentId'	=>	$data->userId,
						'completed'	=>	0
				));
				$select->columns(array('id', 'eachQuestionFlag','time','lastQuestionId'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$tempData = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$tempData = $tempData->toArray();
				if(count($tempData) == 0)
					$result->resumeMode = 0;
				else {
					$result->resumeMode = 1;
					$result->data = $tempData[0];
				}
				//for storing courseId 
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$selectString = "SELECT courseId FROM exam_subjective e left join subjects s on s.id=e.subjectId WHERE e.id=$data->examId";
				$courseId = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$courseId = $courseId->toArray();
				//print_r($courseId[0]['courseId'] );
				$result->courseId =$courseId[0]['courseId'];
				
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//
		public function checkExam($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);

				$selectString= "SELECT courseId,e.subjectId,e.attempts,COUNT(ea.id) AS student_attempts
								FROM exams e
								LEFT JOIN subjects s on s.id=e.subjectId
								LEFT JOIN exam_attempts ea ON ea.examId=e.id AND ea.studentId=$data->userId AND ea.completed=1
								WHERE e.id=$data->examId
								GROUP BY e.id";
				$courseId = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$courseId = $courseId->toArray();
				//print_r($courseId[0]['subjectId']);
				$courseId =$courseId[0];//['courseId'];
				$attemptsLeft = $courseId['attempts']-$courseId['student_attempts'];

				if ($attemptsLeft<1) {
					$result->subjectId = $courseId['subjectId'];
					$result->status = 0;
					$result->message = 'Sorry, you are out of attempts.';
					return $result;
				}
	
				$sql = new Sql($adapter);
				//$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('student_subject');
				$select->columns(array('id','subjectId','Active'));
				$select->where(array('courseId' => $courseId['courseId'],'subjectId'=>$courseId['subjectId'],'userId' => $data->userId));
				$select->order('id DESC');
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjects1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects1 = $subjects1->toArray();	
				//print_r(count($subjects1));		
				if(count($subjects1)>0)
				{	$subjects1=$subjects1[0];
					$Active	 =    $subjects1['Active'];
					if($Active == 1)
					{
						$result->status = 1;
					}else{
						$result->status = 0;
						$result->message = 'You Are not allowed To give this exam';
					}
					
				}else{
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('student_course');
					$select->columns(array('id'));
					$select->where(array('course_id' => $courseId['courseId'],'user_id' => $data->userId));
					$select->order('id DESC');
					$selectString = $sql->getSqlStringForSqlObject($select);
					$course = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$course = $course->toArray();
					if(count($course)>0)
					{	
						$result->status = 1;
					}else{
						$result->status = 0;
						$result->message = 'You Are not allowed To give this exam';
					}	
				}
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//
		public function setTimeSubjectiveExam($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result = new stdClass();
			try {
				//SELECT courseId FROM exams e left join subjects s on s.id=e.subjectId WHERE e.id=192
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$selectString = "SELECT time FROM subjective_exam_attempts e WHERE e.id=$data->attemtId";
				$time = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$time = $time->toArray();
				if(count($time)>0)
				{
					$time =$time[0]['time'];
					$time=$time+120;
					$update = $sql->update();
					$update->table('subjective_exam_attempts');
					$update->set(array(
							'time'	=>	$time
					));
					$update->where(array(
							'id'	=>	$data->attemtId
					));
					$statement = $sql->getSqlStringForSqlObject($update);
					$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
				}else{
					$db->rollBack();
					$result->status = 0;
					$result->message = 'No time FOund for exam';
				}
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//function to getcourseId of exam
		public function getExamCourseId($data) {
			$result = new stdClass();
			try {
				//SELECT courseId FROM exams e left join subjects s on s.id=e.subjectId WHERE e.id=192
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$selectString = "SELECT courseId FROM exams e left join subjects s on s.id=e.subjectId WHERE e.id=$data->examId";
				$courseId = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$courseId = $courseId->toArray();
				//print_r($courseId[0]['courseId'] );
				$result->courseId =$courseId[0]['courseId'];
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//function to get exam assignment resume status
		public function completeAllExams() {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				//this will be executed when time is not lost
				$statement = "UPDATE exam_attempts AS ea, exams AS e SET ea.endDate=ea.startDate+(SELECT SUM(aq.time) FROM attempt_questions AS aq WHERE aq.attemptId=ea.id) WHERE ea.completed=0 AND ea.examId=e.id AND e.powerOption=0;";
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				//this will execute when time is lost
				$statement = "UPDATE exam_attempts AS ea, exams AS e SET ea.endDate=".time()." WHERE ea.examId=e.id AND e.powerOption=1 AND ea.completed=0;";
				$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				//selecting all attempts which are not completed and needed to be checked that their time is over or not
				$selectString = "SELECT ea.*, e.tt_status AS tt_status,
								CASE e.tt_status
								WHEN 0 THEN e.totalTime
								WHEN 1 THEN (SELECT SUM(`time`) FROM exam_sections WHERE examId=e.id AND `delete`=0)
								END AS totalTime FROM exam_attempts AS ea 
								JOIN exams AS e ON 
								e.id=ea.examId 
								WHERE completed=0";
				$tempData = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$tempData = $tempData->toArray();
				if(count($tempData) != 0) {
					foreach($tempData as $attempt) {
						$timeTaken = $attempt['endDate'] - $attempt['startDate'];
						$totalTime = $attempt['totalTime'] * 60;
						//if time taken is greater than allowed time complete the exam explicitly
						if($timeTaken >= $totalTime) {
							$check = new stdClass();
							$check->attemptId = $attempt['id'];
							$check->examId = $attempt['examId'];
							$this->checkAndSave($check);
							$update = $sql->update();
							$update->table('exam_attempts');
							$update->set(array(
									'completed'	=>	1,
									'endDate'	=>	new \Zend\Db\Sql\Expression("startDate + {$totalTime}")
							));
							$update->where(array(
									'id'	=>	$attempt['id']
							));
							$statement = $sql->getSqlStringForSqlObject($update);
							$adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
						}
						//else do nothing
					}
				}
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function getSubjectiveExamAttempt($data) {
			try {
				$result = new stdClass();
				$adapter = $this->adapter;


				$sql = new Sql($adapter);
				$select = $sql->select();
				
				$select->from('subjective_exam_attempts')->order('id DESC');
				$where;
				if($data->userRole != 4)
					$where = array(
								'studentId'	=>	$data->studentId,
								'examId'	=>	$data->examId,
								'completed'	=>	1
					);
				else
					$where = array(
								'studentId'	=>	$data->userId,
								'examId'	=>	$data->examId,
								'completed'	=>	1
					);
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$examAttempts = $res->toArray();
				if ($examAttempts>1) {
					$i=1;
					foreach ($examAttempts as $key => $attempt) {
						if ($attempt['id']==$data->attemptId) {
							$data->attemptNo = $i;
							break;
						}
					}
				} else {
					$data->attemptNo = 1;
				}

				$result->attemptNo = $data->attemptNo;

				if($data->userRole != 4) {
					$query = "SELECT qs.*,qsel.examId, qsel.marks, IFNULL(saq.marks,0) as studentMarks, IFNULL(saq.time,0) as questionTime, saq.check, asu.answer AS studentAnswer, IFNULL(asu.review,'') AS review, sa.examId, sa.score AS totalScore, sa.studentId, sa.startDate, sa.endDate, IFNULL(sa.time,0) as totalTime
								FROM `subjective_attempt_questions` AS saq
								LEFT JOIN `subjective_exam_attempts` AS sa ON sa.id=saq.attemptId
								LEFT JOIN `exam_subjective` AS es ON es.id=sa.examId
								LEFT JOIN `questions_subjective` AS qs ON qs.id=saq.questionId
								LEFT JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id AND qsel.examId=es.id
								LEFT JOIN `answer_subjective` AS asu ON asu.attemptQuestionId=saq.id AND asu.status=1 AND asu.answerType='text' AND asu.subjectiveExam_attemptid=sa.id
								WHERE qs.`deleted`=0 AND qs.`status`=1 AND saq.attemptId={$data->attemptId} AND sa.studentId={$data->studentId} AND es.status=2 AND es.Active=1
								ORDER BY qs.id";
				} else {
					$query = "SELECT qs.*,qsel.examId, qsel.marks, IFNULL(saq.marks,0) as studentMarks, IFNULL(saq.time,0) as questionTime, saq.check, asu.answer AS studentAnswer, IFNULL(asu.review,'') AS review, sa.examId, sa.score AS totalScore, sa.studentId, sa.startDate, sa.endDate, IFNULL(sa.time,0) as totalTime
								FROM `subjective_attempt_questions` AS saq
								LEFT JOIN `subjective_exam_attempts` AS sa ON sa.id=saq.attemptId
								LEFT JOIN `exam_subjective` AS es ON es.id=sa.examId
								LEFT JOIN `questions_subjective` AS qs ON qs.id=saq.questionId
								LEFT JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id AND qsel.examId=es.id
								LEFT JOIN `answer_subjective` AS asu ON asu.attemptQuestionId=saq.id AND asu.status=1 AND asu.answerType='text' AND asu.subjectiveExam_attemptid=sa.id
								WHERE qs.`deleted`=0 AND qs.`status`=1 AND saq.attemptId={$data->attemptId} AND sa.studentId={$data->userId} AND es.status=2 AND es.Active=1
								ORDER BY qs.id";
				}
				//print_r($query);
				$questions = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$questions = $questions->toArray();
				if (count($questions) > 0) {
					$subjectiveQuestions=array();
					$parentIdTemp= array();
					$i			 = 0;
					$qnoCounter  = 1;
					$data->examId = $questions[0]['examId'];
					$totalScore	  = $questions[0]['totalScore'];
					$startDate	  = $questions[0]['startDate'];
					$endDate	  = $questions[0]['endDate'];
					$totalTime	  = $questions[0]['totalTime'];
					$maxMarks = 0;
					
					//fetching attemptID of topper
					$select = "SELECT id FROM `subjective_exam_attempts`
								WHERE examId={$data->examId} AND completed=1 AND checked=1 AND score = (
									SELECT max(score) FROM `subjective_exam_attempts` WHERE examId= {$data->examId} AND completed =1
								) ORDER BY time";
					$topperAttempt	= $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$topperAttempt	= $topperAttempt->toArray();
					$topperAttempt	= $topperAttempt[0]['id'];

					$select = "SELECT COUNT(*) AS rank FROM `subjective_exam_attempts` WHERE `score`>{$totalScore} AND examId={$data->examId} AND completed=1 AND checked=1";
					$myRank	= $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$myRank	= $myRank->toArray();
					$myRank	= $myRank[0]['rank']+1;

					$select = "SELECT COUNT(*) AS totalStudents FROM `subjective_exam_attempts` WHERE examId={$data->examId} AND completed=1 AND checked=1";
					$totalRank	= $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$totalRank	= $totalRank->toArray();
					$totalRank	= $totalRank[0]['totalStudents'];

					foreach($questions as $key=>$question)
					{

						$query = "SELECT IFNULL((saq.marks),0) AS toppermarks, IFNULL((saq.time),0) AS toppertime, saq.check, COUNT(*) AS answers
									FROM `subjective_attempt_questions` as saq
									LEFT JOIN `answer_subjective` AS asu ON asu.attemptQuestionId=saq.id AND asu.subjectiveExam_attemptid=saq.attemptId
									WHERE saq.questionId='$question[id]' AND saq.attemptId='$topperAttempt' AND asu.id IS NOT NULL
									GROUP BY asu.attemptQuestionId";
						//print_r($query);
						$toppermarkstime = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$toppermarkstime = $toppermarkstime->toArray();
						if (count($toppermarkstime)>0) {
							$topperans	 = $toppermarkstime[0]['answers'];
							$toppertime	 = $this->checkAttemptTime($toppermarkstime[0]['toppertime'],$topperans);
							$toppermarks = $this->checkAttemptMarks($toppermarkstime[0]['toppermarks'],$topperans);
						} else {
							$toppertime  = "Not Attempted";
							$toppermarks = 'NA';
						}

						$topperAnswerString = "";
						$query = "SELECT asu.id,asu.answer AS topperAnswer,asu.answerType
									FROM `answer_subjective` AS asu
									INNER JOIN `subjective_attempt_questions` AS saq ON asu.attemptQuestionId=saq.id
									WHERE saq.questionId='$question[id]' AND asu.subjectiveExam_attemptid='$topperAttempt' AND asu.status=1";
						//print_r($query);
						$topperanswers = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$topperanswers = $topperanswers->toArray();
						$topperAnswerUploads = array();
						if (count($topperanswers)>0) {
							foreach ($topperanswers as $key1 => $answer) {
								if ($answer["answerType"] == "text") {
									$topperAnswerString = $answer["topperAnswer"];
								} else {
									$ext = pathinfo($answer["topperAnswer"], PATHINFO_EXTENSION);
									$topperAnswerUploads[] = array(
																"path" => $answer["topperAnswer"],
																"type" => $ext,
																"id"   => $answer['id']
																);
								}
							}
						}
						$studentAnswerItems		= 0;
						if (!empty($question['studentAnswer'])) {
							$studentAnswerItems++;
						}
						if (empty($question["parentId"])) {
							$parentIdTemp[$i]=$question['id'];
							$subjectiveQuestions[$i]["id"]				= $question["id"];
							$subjectiveQuestions[$i]["qno"]				= $qnoCounter;
							$subjectiveQuestions[$i]["question"]		= $question['question'];
							$subjectiveQuestions[$i]["topper_time"]		= $toppertime;
							$subjectiveQuestions[$i]["shortest_time"]	= 0;
							$subjectiveQuestions[$i]["avg_time"]		= 0;
							$subjectiveQuestions[$i]["max_marks"]		= (int)$question["marks"];
							$maxMarks+=$question["marks"];
							// MARKS
							$subjectiveQuestions[$i]["marks"]			= (int)$question["studentMarks"];
							$subjectiveQuestions[$i]["topper_marks"]	= (int)$toppermarks;
							$subjectiveQuestions[$i]["avg_marks"]		= 0;
							// MARKS END
							$subjectiveQuestions[$i]["uploads"]			= array();
							$subjectiveQuestions[$i]["answer"]			= $question['answer'];
							$subjectiveQuestions[$i]["student_answer"]	= $question['studentAnswer'];
							$subjectiveQuestions[$i]["topper_answer"]	= $topperAnswerString;
							$subjectiveQuestions[$i]["review"]			= $question["review"];
							$subjectiveQuestions[$i]["check"]			= $question["check"];
							$subjectiveQuestions[$i]["subQuestions"]	= array();
							$subjectiveQuestions[$i]['listType']		= '';
							if ($question['questionsonPage'] == 1) {
								$subjectiveQuestions[$i]['listType']	= 'one';
								$query = "SELECT COUNT(*) AS subCount FROM `subjective_attempt_questions` WHERE parentId='$question[id]' AND attemptId='{$data->attemptId}'";
								//print_r($query);
								$subQuestions = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$subQuestions = $subQuestions->toArray();
								if (count($subQuestions)>0) {
									$subQuestions = $subQuestions[0]['subCount'];
								}
								$subjectiveQuestions[$i]["max_marks"]		= (int)($subjectiveQuestions[$i]["max_marks"]*$subQuestions);
								$maxMarks = $maxMarks - $question["marks"] + ($subjectiveQuestions[$i]["max_marks"]*$subQuestions);
							} elseif ($question['questionsonPage'] == 2) {
								$subjectiveQuestions[$i]['listType']	= 'multi';
							}
							if($question['noOfSubquestionRequired']>0) {
								$subjectiveQuestions[$i]["questionType"]	= "questionGroup";
							} else {
								$subjectiveQuestions[$i]["questionType"]	= "question";
							}

							$query = "SELECT asu.id,asu.answer,IFNULL(asu.review,'') AS review
										FROM `answer_subjective` AS asu
										INNER JOIN `subjective_attempt_questions` AS saq ON saq.id=asu.attemptQuestionId
										WHERE asu.answerType='upload' AND asu.subjectiveExam_attemptid='{$data->attemptId}' AND saq.questionId=$question[id] AND asu.status=1";
							//print_r($query);
							$uploads = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$uploads = $uploads->toArray();
							if (count($uploads)) {
								$studentAnswerItems+=count($uploads);
								foreach ($uploads as $key1 => $upload) {
									$ext = pathinfo($upload['answer'], PATHINFO_EXTENSION);
									$uploadInfo = array(
														"review" => $upload['review'],
														"path" => $upload['answer'],
														"type" => $ext,
														"id"   => $upload['id']
														);
									$subjectiveQuestions[$i]["uploads"][]		= $uploadInfo;
								}
							}
							$subjectiveQuestions[$i]["topper_uploads"] = $topperAnswerUploads;
							if ($subjectiveQuestions[$i]['listType'] != 'multi') {
								// question time
								$subjectiveQuestions[$i]["time"]			= $this->checkAttemptTime($question["questionTime"],$studentAnswerItems);
								$subjectiveQuestions[$i]["marks"]			= (int)$this->checkAttemptMarks($question["studentMarks"],$studentAnswerItems);
								// average time
								$query = "SELECT saq.id, saq.check, saq.marks, saq.time, COUNT(asu.id) AS answers
											FROM `questions_subjective` AS qs
											INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id
											INNER JOIN `subjective_attempt_questions` AS saq ON saq.questionId=qs.id
											INNER JOIN `subjective_exam_attempts` AS sa ON sa.id=saq.attemptId
											LEFT JOIN `answer_subjective` AS asu ON asu.attemptQuestionId=saq.id AND asu.subjectiveExam_attemptid=saq.attemptId
											WHERE qsel.examId=$question[examId] AND saq.questionId='$question[id]' AND qs.status=1 AND qs.deleted=0 AND sa.checked=1 AND saq.check=1 AND asu.id IS NOT NULL
											GROUP BY saq.id";
								$avgResult = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$avgResult = $avgResult->toArray();
								$avgMarks  = $avgTime = 0;
								$avgMarksItems = $avgTimeItems = 0;
								if (count($avgResult)>0) {
									foreach ($avgResult as $key => $value) {
										if ($value["answers"]>0) {
											$avgMarks+=$value["marks"];
											$avgMarksItems++;
										}
									}
									if ($avgMarksItems>0) {
										$avgMarks = round($avgMarks/$avgMarksItems,2);
									} else {
										$avgMarks = 'NA';
									}
									foreach ($avgResult as $key => $value) {
										if ($value["answers"]>0 AND $value["marks"]>=$avgMarks) {
											$avgTime+=$value["time"];
											$avgTimeItems++;
										}
									}
									if ($avgTimeItems>0) {
										$avgTime = $avgTime/$avgMarksItems;
										$avgTime = gmdate("H:i:s",$avgTime);
									} else {
										$avgTime = 'NA';
									}
								} else {
									$avgMarks = 'NA';
									$avgTime  = 'NA';
								}
								$subjectiveQuestions[$i]["avg_time"]		= $avgTime;
								$subjectiveQuestions[$i]["avg_marks"]		= (int)$avgMarks;
								// shortest time
								$query = "SELECT IFNULL(MIN(saq.time),0) AS shortest_time
											FROM `questions_subjective` AS qs
											INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id
											LEFT JOIN `subjective_attempt_questions` AS saq ON saq.questionId=qs.id
											LEFT JOIN `subjective_exam_attempts` AS sa ON sa.id=saq.attemptId
											WHERE qsel.examId=$question[examId] AND saq.time!=0 AND saq.questionId='$question[id]' AND sa.checked=1 AND qs.deleted=0 AND saq.check=1 AND qs.status=1";
								$minTime = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$minTime = $minTime->toArray();
								if (count($minTime)) {
									$subjectiveQuestions[$i]["shortest_time"]		= gmdate("H:i:s",$minTime[0]['shortest_time']);
								}
							}

							$subjectiveQuestions[$i]['attempted'] = 0;
							if ($studentAnswerItems>0) {
								$subjectiveQuestions[$i]['attempted'] = 1;
							}

							$qnoCounter++;
							$i++;
						} else {
							$parentId=$question['parentId'];
							$index=array_search($question['parentId'],$parentIdTemp);

							$query = "SELECT asu.id,`answer`,IFNULL(asu.review,'') AS review
									FROM `answer_subjective` AS asu
									INNER JOIN `subjective_attempt_questions` AS saq ON saq.id=asu.attemptQuestionId
									WHERE asu.answerType='upload' AND asu.subjectiveExam_attemptid='{$data->attemptId}' AND saq.questionId=$question[id] AND asu.status=1";
							//print_r($query);
							$uploads = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$uploads = $uploads->toArray();
							$subjectiveUploads = array();
							if (count($uploads)) {
								$studentAnswerItems+=count($uploads);
								foreach ($uploads as $key1 => $upload) {
									$ext = pathinfo($upload['answer'], PATHINFO_EXTENSION);
									$subjectiveUploads[]		= array(
																		"review" => $upload['review'],
																		"path" => $upload['answer'],
																		"type" => $ext,
																		"id"   => $upload['id']
																		);
								}
							}
							$avgMarks		= $avgTime		= 0;
							$avgMarksItems	= $avgTimeItems	= 0;
							$minTime		= 0;
							if ($subjectiveQuestions[$index]['listType'] == 'multi') {

								// average time
								$query = "SELECT saq.id, saq.check, saq.marks, saq.time, COUNT(asu.id) AS answers
											FROM `questions_subjective` AS qs
											INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id
											INNER JOIN `subjective_attempt_questions` AS saq ON saq.questionId=qs.id
											INNER JOIN `subjective_exam_attempts` AS sa ON sa.id=saq.attemptId
											LEFT JOIN `answer_subjective` AS asu ON asu.attemptQuestionId=saq.id AND asu.subjectiveExam_attemptid=saq.attemptId
											WHERE qsel.examId=$question[examId] AND saq.questionId='$question[id]' AND qs.status=1 AND qs.deleted=0 AND sa.checked=1 AND saq.check=1 AND asu.id IS NOT NULL
											GROUP BY saq.id";
								$avgResult = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$avgResult = $avgResult->toArray();
								if (count($avgResult)>0) {
									foreach ($avgResult as $key => $value) {
										if ($value["answers"]>0) {
											$avgMarks+=$value["marks"];
											$avgMarksItems++;
										}
									}
									if ($avgMarksItems>0) {
										$avgMarks = round($avgMarks/$avgMarksItems,2);
									} else {
										$avgMarks = 'NA';
									}
									foreach ($avgResult as $key => $value) {
										if ($value["answers"]>0 AND $value["marks"]>=$avgMarks) {
											$avgTime+=$value["time"];
											$avgTimeItems++;
										}
									}
									if ($avgTimeItems>0) {
										$avgTime = $avgTime/$avgMarksItems;
										$avgTime = gmdate("H:i:s",$avgTime);
									} else {
										$avgTime = 'NA';
									}
								} else {
									$avgMarks = 'NA';
									$avgTime  = 'NA';
								}

								// shortest time
								$query = "SELECT IFNULL(MIN(saq.time),0) AS shortest_time
											FROM `questions_subjective` AS qs
											INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id
											LEFT JOIN `subjective_attempt_questions` AS saq ON saq.questionId=qs.id
											LEFT JOIN `subjective_exam_attempts` AS sa ON sa.id=saq.attemptId
											WHERE qsel.examId=$question[examId]  AND saq.time!=0 AND saq.questionId='$question[id]' AND qs.status=1 AND qs.deleted=0 AND sa.checked=1 AND saq.check=1";
								$minTime = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$minTime = $minTime->toArray();
								if (count($minTime)) {
									$minTime = gmdate("H:i:s",$minTime[0]['shortest_time']);
								}
							}
						
							$attempted = 0;
							if ($studentAnswerItems>0) {
								$attempted = 1;
							}
							$studentTime = $this->checkAttemptTime($question["questionTime"],$studentAnswerItems);
							$subjectiveQuestions[$index]["subQuestions"][] = array(
																			"id"			=> $question["id"],
																			"questionType"	=> "question",
																			"question"		=> $question['question'],
																			"time"			=> $studentTime,
																			"topper_time"	=> $toppertime,
																			"avg_time"		=> $avgTime,
																			"shortest_time"	=> $minTime,
																			"marks"			=> (int)$question["studentMarks"],
																			"max_marks"		=> (int)$subjectiveQuestions[$index]["max_marks"],
																			"topper_marks"	=> (int)$toppermarks,
																			"avg_marks"		=> (int)$avgMarks,
																			"uploads"		=> $subjectiveUploads,
																			"topper_uploads"=> $topperAnswerUploads,
																			"answer"		=> $question['answer'],
																			"student_answer"=> $question['studentAnswer'],
																			"review"		=> $question["review"],
																			"check"			=> $question["check"],
																			"topper_answer" => $topperAnswerString,
																			"attempted"		=> $attempted
																		);
							$maxMarks+=$subjectiveQuestions[$index]["max_marks"];
							if ($studentTime != 'Not Attempted' && $subjectiveQuestions[$index]['listType'] == 'one') {
								// average time
								$query = "SELECT saq.time,saq.marks
											FROM `subjective_attempt_questions` AS saq
											INNER JOIN `questions_subjective` AS qs ON qs.id=saq.questionId
											WHERE qs.id='{$parentId}' AND `attemptId`={$data->attemptId}";
								//$result->query[] = $query;
								//print_r($query);
								$parentStudentMarksTime	= $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$parentStudentMarksTime	= $parentStudentMarksTime->toArray();
								$parentStudentTime		= $parentStudentMarksTime[0]['time'];
								$parentStudentMarks		= $parentStudentMarksTime[0]['marks'];

								$subjectiveQuestions[$index]["time"]	= gmdate("H:i:s",$parentStudentTime);
								$subjectiveQuestions[$index]["marks"]	= (int)$parentStudentMarks;
								$subjectiveQuestions[$index]["attempted"] = 1;
							}
							if ($toppertime != 'Not Attempted' && $subjectiveQuestions[$index]['listType'] == 'one') {
								// average time
								$query = "SELECT saq.time,saq.marks
											FROM `subjective_attempt_questions` AS saq
											INNER JOIN `questions_subjective` AS qs ON qs.id=saq.questionId
											WHERE qs.id='{$parentId}' AND `attemptId`={$topperAttempt}";
								//$result->query[] = $query;
								//print_r($query);
								$parentTopperMarksTime	= $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$parentTopperMarksTime	= $parentTopperMarksTime->toArray();
								$parentTopperTime		= $parentTopperMarksTime[0]['time'];
								$parentTopperMarks		= $parentTopperMarksTime[0]['marks'];

								$subjectiveQuestions[$index]["topper_time"] = gmdate("H:i:s",$parentTopperTime);
								$subjectiveQuestions[$index]["topper_marks"] = (int)$parentTopperMarks;
							}
						}
					}
					// getting topper marks and the average marks of the exam
					
					if($data->userRole != 4) {
						$query = "SELECT `id` FROM `subjective_exam_attempts` WHERE `completed`=1 AND `examId` = {$data->examId} AND `studentId` = {$data->studentId} ORDER BY id";
					} else {
						$query = "SELECT `id` FROM `subjective_exam_attempts` WHERE `completed`=1 AND `examId` = {$data->examId} AND `studentId` = {$data->userId} ORDER BY id";
					}
					//print_r($query);
					$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$attempts = $attempts->toArray();
					$result->prevAttemptId = '';
					$result->nextAttemptId = '';
					foreach ($attempts as $key => $value) {
						if ($value['id'] == $data->attemptId) {
							if (isset($attempts[$key-1]['id']) && !empty($attempts[$key-1]['id'])) {
								$result->prevAttemptId = $attempts[$key-1]['id'];
							}
							if (isset($attempts[$key+1]['id']) && !empty($attempts[$key+1]['id'])) {
								$result->nextAttemptId = $attempts[$key+1]['id'];
							}
						}
					}
					$result->questions	= $subjectiveQuestions;
					$result->totalScore	= (int)$totalScore;
					$result->maxScore	= (int)$maxMarks;
					$result->maxScore	= $this->getMaxMarksSubjectiveExam($data->examId);
					$result->startDate	= $startDate;
					$result->endDate	= $endDate;
					$result->totalTime	= $totalTime;
					$result->rank		= $myRank;
					$result->totalRank	= $totalRank;

					$dataRange = new stdClass();
					$dataRange->examId = $data->examId;
					$dataRange->examType = 'subjective-exam';
					$ranges=$this->getPercentRanges($dataRange);
					if ($ranges->status == 1) {
						$ranges = $ranges->score;
					} else {
						$ranges = array();
					}
					$result->ranges = $ranges;
					$result->status		= 1;
				} else {
					$result->status = 0;
				}
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		/*public function getSubjectiveExamAttempt($data) {
			try {
				$result = new stdClass();
				$adapter = $this->adapter;


				$sql = new Sql($adapter);
				$select = $sql->select();
				
				$select->from('subjective_exam_attempts')->order('id DESC');
				$where;
				if($data->userRole != 4)
					$where = array(
								'studentId'	=>	$data->studentId,
								'examId'	=>	$data->examId,
								'completed'	=>	1
					);
				else
					$where = array(
								'studentId'	=>	$data->userId,
								'examId'	=>	$data->examId,
								'completed'	=>	1
					);
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$examAttempts = $res->toArray();
				if ($examAttempts>1) {
					$i=1;
					foreach ($examAttempts as $key => $attempt) {
						if ($attempt['id']==$data->attemptId) {
							$data->attemptNo = $i;
							break;
						}
					}
				} else {
					$data->attemptNo = 1;
				}

				$result->attemptNo = $data->attemptNo;

				if($data->userRole != 4) {
					$query = "SELECT qs.*,qsel.examId, qsel.marks, IFNULL(saq.marks,0) as studentMarks, IFNULL(saq.time,0) as questionTime, saq.check, asu.answer AS studentAnswer, IFNULL(asu.review,'') AS review, sa.examId, sa.score AS totalScore, sa.studentId, sa.startDate, sa.endDate, IFNULL(sa.time,0) as totalTime
								FROM `subjective_attempt_questions` AS saq
								LEFT JOIN `subjective_exam_attempts` AS sa ON sa.id=saq.attemptId
								LEFT JOIN `exam_subjective` AS es ON es.id=sa.examId
								LEFT JOIN `questions_subjective` AS qs ON qs.id=saq.questionId
								LEFT JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id AND qsel.examId=es.id
								LEFT JOIN `answer_subjective` AS asu ON asu.attemptQuestionId=saq.id AND asu.status=1 AND asu.answerType='text' AND asu.subjectiveExam_attemptid=sa.id
								WHERE qs.`deleted`=0 AND qs.`status`=1 AND saq.attemptId={$data->attemptId} AND sa.studentId={$data->studentId} AND es.status=2 AND es.Active=1
								ORDER BY qs.id";
				} else {
					$query = "SELECT qs.*,qsel.examId, qsel.marks, IFNULL(saq.marks,0) as studentMarks, IFNULL(saq.time,0) as questionTime, saq.check, asu.answer AS studentAnswer, IFNULL(asu.review,'') AS review, sa.examId, sa.score AS totalScore, sa.studentId, sa.startDate, sa.endDate, IFNULL(sa.time,0) as totalTime
								FROM `subjective_attempt_questions` AS saq
								LEFT JOIN `subjective_exam_attempts` AS sa ON sa.id=saq.attemptId
								LEFT JOIN `exam_subjective` AS es ON es.id=sa.examId
								LEFT JOIN `questions_subjective` AS qs ON qs.id=saq.questionId
								LEFT JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id AND qsel.examId=es.id
								LEFT JOIN `answer_subjective` AS asu ON asu.attemptQuestionId=saq.id AND asu.status=1 AND asu.answerType='text' AND asu.subjectiveExam_attemptid=sa.id
								WHERE qs.`deleted`=0 AND qs.`status`=1 AND saq.attemptId={$data->attemptId} AND sa.studentId={$data->userId} AND es.status=2 AND es.Active=1
								ORDER BY qs.id";
				}
				//print_r($query);
				$questions = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$questions = $questions->toArray();
				if (count($questions) > 0) {
					$subjectiveQuestions=array();
					$parentIdTemp= array();
					$i			 = 0;
					$qnoCounter  = 1;
					$data->examId = $questions[0]['examId'];
					$totalScore	  = $questions[0]['totalScore'];
					$startDate	  = $questions[0]['startDate'];
					$endDate	  = $questions[0]['endDate'];
					$totalTime	  = $questions[0]['totalTime'];
					$maxMarks = 0;
					
					//fetching attemptID of topper
					$select = "SELECT id FROM `subjective_exam_attempts`
								WHERE examId={$data->examId} AND completed=1 AND checked=1 AND score = (
									SELECT max(score) FROM `subjective_exam_attempts` WHERE examId= {$data->examId} AND completed =1
								) ORDER BY time";
					$topperAttempt	= $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$topperAttempt	= $topperAttempt->toArray();
					$topperAttempt	= $topperAttempt[0]['id'];

					$select = "SELECT COUNT(*) AS rank FROM `subjective_exam_attempts` WHERE `score`>{$totalScore} AND  examId={$data->examId} AND completed=1 AND checked=1";
					$myRank	= $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$myRank	= $myRank->toArray();
					$myRank	= $myRank[0]['rank']+1;

					$select = "SELECT COUNT(*) AS totalStudents FROM `subjective_exam_attempts` WHERE examId={$data->examId} AND completed=1 AND checked=1";
					$totalRank	= $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$totalRank	= $totalRank->toArray();
					$totalRank	= $totalRank[0]['totalStudents'];

					foreach($questions as $key=>$question)
					{

						$query = "SELECT IFNULL((saq.marks),0) AS toppermarks, IFNULL((saq.time),0) AS toppertime, saq.check, COUNT(*) AS answers
									FROM `subjective_attempt_questions` as saq
									LEFT JOIN `answer_subjective` AS asu ON asu.attemptQuestionId=saq.id AND asu.subjectiveExam_attemptid=saq.attemptId
									WHERE saq.questionId='$question[id]' AND saq.attemptId='$topperAttempt' AND asu.id IS NOT NULL
									GROUP BY asu.attemptQuestionId";
						//print_r($query);
						$toppermarkstime = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$toppermarkstime = $toppermarkstime->toArray();
						if (count($toppermarkstime)>0) {
							$topperans	 = $toppermarkstime[0]['answers'];
							$toppertime	 = $this->checkAttemptTime($toppermarkstime[0]['toppertime'],$topperans);
							$toppermarks = $this->checkAttemptMarks($toppermarkstime[0]['toppermarks'],$topperans);
						} else {
							$toppertime  = "Not Attempted";
							$toppermarks = 'NA';
						}

						$topperAnswerString = "";
						$query = "SELECT asu.id,asu.answer AS topperAnswer,asu.answerType
									FROM `answer_subjective` AS asu
									INNER JOIN `subjective_attempt_questions` AS saq ON asu.attemptQuestionId=saq.id
									WHERE saq.questionId='$question[id]' AND asu.subjectiveExam_attemptid='$topperAttempt' AND asu.status=1";
						//print_r($query);
						$topperanswers = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$topperanswers = $topperanswers->toArray();
						$topperAnswerUploads = array();
						if (count($topperanswers)>0) {
							foreach ($topperanswers as $key1 => $answer) {
								if ($answer["answerType"] == "text") {
									$topperAnswerString = $answer["topperAnswer"];
								} else {
									$ext = pathinfo($answer["topperAnswer"], PATHINFO_EXTENSION);
									$topperAnswerUploads[] = array(
																"path" => $answer["topperAnswer"],
																"type" => $ext,
																"id"   => $answer['id']
																);
								}
							}
						}
						$studentAnswerItems		= 0;
						if (!empty($question['studentAnswer'])) {
							$studentAnswerItems++;
						}
						if (empty($question["parentId"])) {
							$parentIdTemp[$i]=$question['id'];
							$subjectiveQuestions[$i]["id"]				= $question["id"];
							$subjectiveQuestions[$i]["qno"]				= $qnoCounter;
							$subjectiveQuestions[$i]["question"]		= $question['question'];
							$subjectiveQuestions[$i]["topper_time"]		= $toppertime;
							$subjectiveQuestions[$i]["shortest_time"]	= 0;
							$subjectiveQuestions[$i]["avg_time"]		= 0;
							$subjectiveQuestions[$i]["max_marks"]		= (int)$question["marks"];
							$maxMarks+=$question["marks"];
							// MARKS
							$subjectiveQuestions[$i]["marks"]			= (int)$question["studentMarks"];
							$subjectiveQuestions[$i]["topper_marks"]	= (int)$toppermarks;
							$subjectiveQuestions[$i]["avg_marks"]		= 0;
							// MARKS END
							$subjectiveQuestions[$i]["uploads"]			= array();
							$subjectiveQuestions[$i]["answer"]			= $question['answer'];
							$subjectiveQuestions[$i]["student_answer"]	= $question['studentAnswer'];
							$subjectiveQuestions[$i]["topper_answer"]	= $topperAnswerString;
							$subjectiveQuestions[$i]["review"]			= $question["review"];
							$subjectiveQuestions[$i]["check"]			= $question["check"];
							$subjectiveQuestions[$i]["subQuestions"]	= array();
							$subjectiveQuestions[$i]['listType']		= '';
							if ($question['questionsonPage'] == 1) {
								$subjectiveQuestions[$i]['listType']	= 'one';
								$query = "SELECT COUNT(*) AS subCount FROM `subjective_attempt_questions` WHERE parentId='$question[id]' AND attemptId='{$data->attemptId}'";
								//print_r($query);
								$subQuestions = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$subQuestions = $subQuestions->toArray();
								if (count($subQuestions)>0) {
									$subQuestions = $subQuestions[0]['subCount'];
								}
								$subjectiveQuestions[$i]["max_marks"]		= (int)($subjectiveQuestions[$i]["max_marks"]*$subQuestions);
								$maxMarks = $maxMarks - $question["marks"] + ($subjectiveQuestions[$i]["max_marks"]*$subQuestions);
							} elseif ($question['questionsonPage'] == 2) {
								$subjectiveQuestions[$i]['listType']	= 'multi';
							}
							if($question['noOfSubquestionRequired']>0) {
								$subjectiveQuestions[$i]["questionType"]	= "questionGroup";
							} else {
								$subjectiveQuestions[$i]["questionType"]	= "question";
							}

							$query = "SELECT asu.id,asu.answer,IFNULL(asu.review,'') AS review
										FROM `answer_subjective` AS asu
										INNER JOIN `subjective_attempt_questions` AS saq ON saq.id=asu.attemptQuestionId
										WHERE asu.answerType='upload' AND asu.subjectiveExam_attemptid='{$data->attemptId}' AND saq.questionId=$question[id] AND asu.status=1";
							//print_r($query);
							$uploads = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$uploads = $uploads->toArray();
							if (count($uploads)) {
								$studentAnswerItems+=count($uploads);
								foreach ($uploads as $key1 => $upload) {
									$ext = pathinfo($upload['answer'], PATHINFO_EXTENSION);
									$uploadInfo = array(
														"review" => $upload['review'],
														"path" => $upload['answer'],
														"type" => $ext,
														"id"   => $upload['id']
														);
									$subjectiveQuestions[$i]["uploads"][]		= $uploadInfo;
								}
							}
							$subjectiveQuestions[$i]["topper_uploads"] = $topperAnswerUploads;
							if ($subjectiveQuestions[$i]['listType'] != 'multi') {
								// question time
								$subjectiveQuestions[$i]["time"]			= $this->checkAttemptTime($question["questionTime"],$studentAnswerItems);
								$subjectiveQuestions[$i]["marks"]			= (int)$this->checkAttemptMarks($question["studentMarks"],$studentAnswerItems);
								// average time
								$query = "SELECT saq.id, saq.check, saq.marks, saq.time, COUNT(asu.id) AS answers
											FROM `questions_subjective` AS qs
											INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id
											INNER JOIN `subjective_attempt_questions` AS saq ON saq.questionId=qs.id
											INNER JOIN `subjective_exam_attempts` AS sa ON sa.id=saq.attemptId
											LEFT JOIN `answer_subjective` AS asu ON asu.attemptQuestionId=saq.id AND asu.subjectiveExam_attemptid=saq.attemptId
											WHERE qsel.examId=$question[examId] AND saq.questionId='$question[id]' AND qs.status=1 AND qs.deleted=0 AND sa.checked=1 AND saq.check=1 AND asu.id IS NOT NULL
											GROUP BY saq.id";
								$avgResult = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$avgResult = $avgResult->toArray();
								$avgMarks  = $avgTime = 0;
								$avgMarksItems = $avgTimeItems = 0;
								if (count($avgResult)>0) {
									foreach ($avgResult as $key => $value) {
										if ($value["answers"]>0) {
											$avgMarks+=$value["marks"];
											$avgMarksItems++;
										}
									}
									if ($avgMarksItems>0) {
										$avgMarks = round($avgMarks/$avgMarksItems,2);
									} else {
										$avgMarks = 'NA';
									}
									foreach ($avgResult as $key => $value) {
										if ($value["answers"]>0 AND $value["marks"]>=$avgMarks) {
											$avgTime+=$value["time"];
											$avgTimeItems++;
										}
									}
									if ($avgTimeItems>0) {
										$avgTime = $avgTime/$avgMarksItems;
										$avgTime = gmdate("H:i:s",$avgTime);
									} else {
										$avgTime = 'NA';
									}
								} else {
									$avgMarks = 'NA';
									$avgTime  = 'NA';
								}
								$subjectiveQuestions[$i]["avg_time"]		= $avgTime;
								$subjectiveQuestions[$i]["avg_marks"]		= (int)$avgMarks;
								// shortest time
								$query = "SELECT IFNULL(MIN(saq.time),0) AS shortest_time
											FROM `questions_subjective` AS qs
											INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id
											LEFT JOIN `subjective_attempt_questions` AS saq ON saq.questionId=qs.id
											LEFT JOIN `subjective_exam_attempts` AS sa ON sa.id=saq.attemptId
											WHERE qsel.examId=$question[examId] AND saq.time!=0 AND saq.questionId='$question[id]' AND sa.checked=1 AND qs.deleted=0 AND saq.check=1 AND qs.status=1";
								$minTime = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$minTime = $minTime->toArray();
								if (count($minTime)) {
									$subjectiveQuestions[$i]["shortest_time"]		= gmdate("H:i:s",$minTime[0]['shortest_time']);
								}
							}

							$subjectiveQuestions[$i]['attempted'] = 0;
							if ($studentAnswerItems>0) {
								$subjectiveQuestions[$i]['attempted'] = 1;
							}

							$qnoCounter++;
							$i++;
						} else {
							$parentId=$question['parentId'];
							$index=array_search($question['parentId'],$parentIdTemp);

							$query = "SELECT asu.id,`answer`,IFNULL(asu.review,'') AS review
									FROM `answer_subjective` AS asu
									INNER JOIN `subjective_attempt_questions` AS saq ON saq.id=asu.attemptQuestionId
									WHERE asu.answerType='upload' AND asu.subjectiveExam_attemptid='{$data->attemptId}' AND saq.questionId=$question[id] AND asu.status=1";
							//print_r($query);
							$uploads = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$uploads = $uploads->toArray();
							$subjectiveUploads = array();
							if (count($uploads)) {
								$studentAnswerItems+=count($uploads);
								foreach ($uploads as $key1 => $upload) {
									$ext = pathinfo($upload['answer'], PATHINFO_EXTENSION);
									$subjectiveUploads[]		= array(
																		"review" => $upload['review'],
																		"path" => $upload['answer'],
																		"type" => $ext,
																		"id"   => $upload['id']
																		);
								}
							}
							$avgMarks		= $avgTime		= 0;
							$avgMarksItems	= $avgTimeItems	= 0;
							$minTime		= 0;
							if ($subjectiveQuestions[$index]['listType'] == 'multi') {

								// average time
								$query = "SELECT saq.id, saq.check, saq.marks, saq.time, COUNT(asu.id) AS answers
											FROM `questions_subjective` AS qs
											INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id
											INNER JOIN `subjective_attempt_questions` AS saq ON saq.questionId=qs.id
											INNER JOIN `subjective_exam_attempts` AS sa ON sa.id=saq.attemptId
											LEFT JOIN `answer_subjective` AS asu ON asu.attemptQuestionId=saq.id AND asu.subjectiveExam_attemptid=saq.attemptId
											WHERE qsel.examId=$question[examId] AND saq.questionId='$question[id]' AND qs.status=1 AND qs.deleted=0 AND sa.checked=1 AND saq.check=1 AND asu.id IS NOT NULL
											GROUP BY saq.id";
								$avgResult = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$avgResult = $avgResult->toArray();
								if (count($avgResult)>0) {
									foreach ($avgResult as $key => $value) {
										if ($value["answers"]>0) {
											$avgMarks+=$value["marks"];
											$avgMarksItems++;
										}
									}
									if ($avgMarksItems>0) {
										$avgMarks = round($avgMarks/$avgMarksItems,2);
									} else {
										$avgMarks = 'NA';
									}
									foreach ($avgResult as $key => $value) {
										if ($value["answers"]>0 AND $value["marks"]>=$avgMarks) {
											$avgTime+=$value["time"];
											$avgTimeItems++;
										}
									}
									if ($avgTimeItems>0) {
										$avgTime = $avgTime/$avgMarksItems;
										$avgTime = gmdate("H:i:s",$avgTime);
									} else {
										$avgTime = 'NA';
									}
								} else {
									$avgMarks = 'NA';
									$avgTime  = 'NA';
								}

								// shortest time
								$query = "SELECT IFNULL(MIN(saq.time),0) AS shortest_time
											FROM `questions_subjective` AS qs
											INNER JOIN `question_subjective_exam_links` AS qsel ON qsel.questionId=qs.id
											LEFT JOIN `subjective_attempt_questions` AS saq ON saq.questionId=qs.id
											LEFT JOIN `subjective_exam_attempts` AS sa ON sa.id=saq.attemptId
											WHERE qsel.examId=$question[examId]  AND saq.time!=0 AND saq.questionId='$question[id]' AND qs.status=1 AND qs.deleted=0 AND sa.checked=1 AND saq.check=1";
								$minTime = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$minTime = $minTime->toArray();
								if (count($minTime)) {
									$minTime = gmdate("H:i:s",$minTime[0]['shortest_time']);
								}
							}
						
							$attempted = 0;
							if ($studentAnswerItems>0) {
								$attempted = 1;
							}
							$studentTime = $this->checkAttemptTime($question["questionTime"],$studentAnswerItems);
							$subjectiveQuestions[$index]["subQuestions"][] = array(
																			"id"			=> $question["id"],
																			"questionType"	=> "question",
																			"question"		=> $question['question'],
																			"time"			=> $studentTime,
																			"topper_time"	=> $toppertime,
																			"avg_time"		=> $avgTime,
																			"shortest_time"	=> $minTime,
																			"marks"			=> (int)$question["studentMarks"],
																			"max_marks"		=> (int)$subjectiveQuestions[$index]["max_marks"],
																			"topper_marks"	=> (int)$toppermarks,
																			"avg_marks"		=> (int)$avgMarks,
																			"uploads"		=> $subjectiveUploads,
																			"topper_uploads"=> $topperAnswerUploads,
																			"answer"		=> $question['answer'],
																			"student_answer"=> $question['studentAnswer'],
																			"review"		=> $question["review"],
																			"check"			=> $question["check"],
																			"topper_answer" => $topperAnswerString,
																			"attempted"		=> $attempted
																		);
							$maxMarks+=$subjectiveQuestions[$index]["max_marks"];
							if ($studentTime != 'Not Attempted' && $subjectiveQuestions[$index]['listType'] == 'one') {
								// average time
								$query = "SELECT saq.time,saq.marks
											FROM `subjective_attempt_questions` AS saq
											INNER JOIN `questions_subjective` AS qs ON qs.id=saq.questionId
											WHERE qs.id='{$parentId}' AND `attemptId`={$data->attemptId}";
								//$result->query[] = $query;
								//print_r($query);
								$parentStudentMarksTime	= $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$parentStudentMarksTime	= $parentStudentMarksTime->toArray();
								$parentStudentTime		= $parentStudentMarksTime[0]['time'];
								$parentStudentMarks		= $parentStudentMarksTime[0]['marks'];

								$subjectiveQuestions[$index]["time"]	= gmdate("H:i:s",$parentStudentTime);
								$subjectiveQuestions[$index]["marks"]	= (int)$parentStudentMarks;
								$subjectiveQuestions[$index]["attempted"] = 1;
							}
							if ($toppertime != 'Not Attempted' && $subjectiveQuestions[$index]['listType'] == 'one') {
								// average time
								$query = "SELECT saq.time,saq.marks
											FROM `subjective_attempt_questions` AS saq
											INNER JOIN `questions_subjective` AS qs ON qs.id=saq.questionId
											WHERE qs.id='{$parentId}' AND `attemptId`={$topperAttempt}";
								//$result->query[] = $query;
								//print_r($query);
								$parentTopperMarksTime	= $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$parentTopperMarksTime	= $parentTopperMarksTime->toArray();
								$parentTopperTime		= $parentTopperMarksTime[0]['time'];
								$parentTopperMarks		= $parentTopperMarksTime[0]['marks'];

								$subjectiveQuestions[$index]["topper_time"] = gmdate("H:i:s",$parentTopperTime);
								$subjectiveQuestions[$index]["topper_marks"] = (int)$parentTopperMarks;
							}
						}
					}
					// getting topper marks and the average marks of the exam
					
					if($data->userRole != 4) {
						$query = "SELECT `id` FROM `subjective_exam_attempts` WHERE `completed`=1 AND `examId` = {$data->examId} AND `studentId` = {$data->studentId} ORDER BY id";
					} else {
						$query = "SELECT `id` FROM `subjective_exam_attempts` WHERE `completed`=1 AND `examId` = {$data->examId} AND `studentId` = {$data->userId} ORDER BY id";
					}
					//print_r($query);
					$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$attempts = $attempts->toArray();
					$result->prevAttemptId = '';
					$result->nextAttemptId = '';
					foreach ($attempts as $key => $value) {
						if ($value['id'] == $data->attemptId) {
							if (isset($attempts[$key-1]['id']) && !empty($attempts[$key-1]['id'])) {
								$result->prevAttemptId = $attempts[$key-1]['id'];
							}
							if (isset($attempts[$key+1]['id']) && !empty($attempts[$key+1]['id'])) {
								$result->nextAttemptId = $attempts[$key+1]['id'];
							}
						}
					}
					$result->questions	= $subjectiveQuestions;
					$result->totalScore	= (int)$totalScore;
					$result->maxScore	= (int)$maxMarks;
					$result->startDate	= $startDate;
					$result->endDate	= $endDate;
					$result->totalTime	= $totalTime;
					$result->rank		= $myRank;
					$result->totalRank	= $totalRank;

					$dataRange = new stdClass();
					$dataRange->examId = $data->examId;
					$dataRange->examType = 'subjective-exam';
					$ranges=$this->getPercentRanges($dataRange);
					if ($ranges->status == 1) {
						$ranges = $ranges->score;
					} else {
						$ranges = array();
					}
					$result->ranges = $ranges;
					$result->status		= 1;
				} else {
					$result->status = 0;
				}
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}*/
		public function checkAttemptTime($value) {
			return (empty($value)?('Not attempted'):(gmdate("H:i:s",$value)));
		}
		public function checkAttemptMarks($value,$time) {
			if (empty($value) && (empty($time) || $time=='Not attempted')) {
				return 'NA';
			}
			return $value;
		}
		public function getPercentRanges($data) {
			$result = new stdClass();
			try {
				$result->score = array();
				$result->tag = array();
				$adapter = $this->adapter;
				if ($data->examType == 'exam') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('pbo' => 'percent_bracket_objective'));
					$select->join(array('s' => 'smileys'),'s.id = pbo.smiley',array('file'));
					$select->where(array(
								'examId' => $data->examId,
								'type'	 => 'score'
					));
					$select->order('percentLow');
					$statement = $sql->getSqlStringForSqlObject($select);
					$ranges = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$ranges = $ranges->toArray();
					if (count($ranges) > 0) {
						$result->score = $ranges;
					}
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('pbo' => 'percent_bracket_objective'));
					$select->join(array('s' => 'smileys'),'s.id = pbo.smiley',array('file'));
					$select->where(array(
								'examId' => $data->examId,
								'type'	 => 'tag'
					));
					$select->order('percentLow');
					$statement = $sql->getSqlStringForSqlObject($select);
					$ranges = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$ranges = $ranges->toArray();
					if (count($ranges) > 0) {
						$result->tag = $ranges;
					}
				} else if ($data->examType == 'subjective-exam') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('pbo' => 'percent_bracket_subjective'));
					$select->join(array('s' => 'smileys'),'s.id = pbo.smiley',array('file'));
					$select->where(array(
								'examId' => $data->examId,
								'type'	 => 'score'
					));
					$select->order('percentLow');
					$statement = $sql->getSqlStringForSqlObject($select);
					$ranges = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$ranges = $ranges->toArray();
					if (count($ranges) > 0) {
						$result->score = $ranges;
					}
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('pbo' => 'percent_bracket_subjective'));
					$select->join(array('s' => 'smileys'),'s.id = pbo.smiley',array('file'));
					$select->where(array(
								'examId' => $data->examId,
								'type'	 => 'tag'
					));
					$select->order('percentLow');
					$statement = $sql->getSqlStringForSqlObject($select);
					$ranges = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$ranges = $ranges->toArray();
					if (count($ranges) > 0) {
						$result->tag = $ranges;
					}
				} else if ($data->examType == 'submission') {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('pbo' => 'percent_bracket_submission'));
					$select->join(array('s' => 'smileys'),'s.id = pbo.smiley',array('file'));
					$select->where(array(
								'examId' => $data->examId,
								'type'	 => 'score'
					));
					$select->order('percentLow');
					$statement = $sql->getSqlStringForSqlObject($select);
					$ranges = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$ranges = $ranges->toArray();
					if (count($ranges) > 0) {
						$result->score = $ranges;
					}
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('pbo' => 'percent_bracket_submission'));
					$select->join(array('s' => 'smileys'),'s.id = pbo.smiley',array('file'));
					$select->where(array(
								'examId' => $data->examId,
								'type'	 => 'tag'
					));
					$select->order('percentLow');
					$statement = $sql->getSqlStringForSqlObject($select);
					$ranges = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
					$ranges = $ranges->toArray();
					if (count($ranges) > 0) {
						$result->tag = $ranges;
					}
				}
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getMaxMarksSubjectiveExam($examId)
		{
			$adapter = $this->adapter;
			$query="SELECT qs.id,qsel.marks,qs.questionsonPage,qs.parentId
					FROM `question_subjective_exam_links` AS qsel
					LEFT JOIN `questions_subjective` AS qs ON qs.id=qsel.questionId
					WHERE qsel.examId={$examId} AND qs.`deleted`=0 AND qs.`status`=1
					ORDER BY qsel.id";
			$questions = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$questions = $questions->toArray();
			$maxMarks = 0;
			$subjectiveQuestions=array();
			$parentIdTemp= array();
			$i			 = 0;
			foreach ($questions as $key => $question) {
				$maxMarks+=$question["marks"];
				if (empty($question['parentId'])) {
					if ($question['questionsonPage'] > 0) {
						$maxMarks = $maxMarks - $question["marks"];
					}
				}
			}
			return $maxMarks;
		}
		public function getSubmissionAttempt($data) {
			try {
				$result = new stdClass();
				$adapter = $this->adapter;

				require_once("Submission.php");
				$s = new Submission();
				$resAccess = $s->getSubmissionAccess($data->examId);
				if ($resAccess->status == 0) {
					$result->status = 0;
					$result->message = $result->status;
					return $result;
				}
				$access = $resAccess->access;

				$sql = new Sql($adapter);
				$select = $sql->select();
				
				$select->from('submission_attempts')->order('id DESC');
				$where;
				if ($access == "public") {
					if($data->userRole != 4)
						$where = array(
									'studentId'	=>	$data->studentId,
									'examId'	=>	$data->examId,
									'completed'	=>	1
						);
					else
						$where = array(
									'studentId'	=>	$data->userId,
									'examId'	=>	$data->examId,
									'completed'	=>	1
						);
				} else {
					require_once("Subject.php");
					$s = new Subject();
					$dataSG = new stdClass();
					$dataSG->subjectId = $data->subjectId;
					if($data->userRole != 4) {
						$dataSG->studentId = $data->studentId;
					} else {
						$dataSG->studentId = $data->userId;
					}
					$studentGroup = $s->getStudentGroup($dataSG);
					if ($studentGroup->status == 0) {
						$result->status = 0;
						$result->message = $studentGroup->message;
						return $result;
					}
					$studentGroupId = $studentGroup->studentGroupId;
					$where = array(
								'studentGroupId'=>	$studentGroupId,
								'examId'	=>	$data->examId,
								'completed'	=>	1
					);
				}

				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$res = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$examAttempts = $res->toArray();
				if ($examAttempts>1) {
					$i=1;
					foreach ($examAttempts as $key => $attempt) {
						if ($attempt['id']==$data->attemptId) {
							$data->attemptNo = $i;
							break;
						}
					}
				} else {
					$data->attemptNo = 1;
				}

				$result->attemptNo = $data->attemptNo;

				if($data->userRole != 4) {
					$query = "SELECT qs.*,qsel.examId,qsel.marks, IFNULL(sam.marks,0) as studentMarks, saq.checked, asu.answerText, asu.answerFile, IFNULL(asu.review,'') AS review, sa.examId, satm.marks AS totalScore, sam.studentId, sa.startDate, sa.endDate
								FROM `submission_attempt_questions` AS saq
								LEFT JOIN `submission_attempts` AS sa ON sa.id=saq.attemptId
								LEFT JOIN `submissions` AS es ON es.id=sa.examId
								LEFT JOIN `submission_questions` AS qs ON qs.id=saq.questionId
								LEFT JOIN `submission_question_links` AS qsel ON qsel.questionId=qs.id AND qsel.examId=es.id
								LEFT JOIN `submission_attempt_answers` AS asu ON asu.attemptQuestionId=saq.id AND asu.status=1 AND asu.submissionAttemptId=sa.id
								LEFT JOIN `submission_attempt_marks` AS sam ON sam.attemptQuestionId=saq.id
								LEFT JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id AND satm.studentId=sam.studentId
								WHERE qs.`status`=1 AND saq.attemptId={$data->attemptId} AND sam.studentId={$data->studentId} AND es.status=2
								ORDER BY qs.id";
				} else {
					$query = "SELECT qs.*,qsel.examId,qsel.marks, IFNULL(sam.marks,0) as studentMarks, saq.checked, asu.answerText, asu.answerFile, IFNULL(asu.review,'') AS review, sa.examId, satm.marks AS totalScore, sam.studentId, sa.startDate, sa.endDate
								FROM `submission_attempt_questions` AS saq
								LEFT JOIN `submission_attempts` AS sa ON sa.id=saq.attemptId
								LEFT JOIN `submissions` AS es ON es.id=sa.examId
								LEFT JOIN `submission_questions` AS qs ON qs.id=saq.questionId
								LEFT JOIN `submission_question_links` AS qsel ON qsel.questionId=qs.id AND qsel.examId=es.id
								LEFT JOIN `submission_attempt_answers` AS asu ON asu.attemptQuestionId=saq.id AND asu.status=1 AND asu.submissionAttemptId=sa.id
								LEFT JOIN `submission_attempt_marks` AS sam ON sam.attemptQuestionId=saq.id
								LEFT JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id AND satm.studentId=sam.studentId
								WHERE qs.`status`=1 AND saq.attemptId={$data->attemptId} AND sam.studentId={$data->userId} AND es.status=2
								ORDER BY qs.id";
				}
				//print_r($query);
				$questions = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$questions = $questions->toArray();
				if (count($questions) > 0) {
					$submissionQuestions=array();
					$i			 = 0;
					$qnoCounter  = 1;
					$data->examId = $questions[0]['examId'];
					$totalScore	  = $questions[0]['totalScore'];
					$startDate	  = $questions[0]['startDate'];
					$endDate	  = $questions[0]['endDate'];
					//$maxMarks = 0;
					
					//fetching attemptID of topper
					$select = "SELECT sa.id FROM `submission_attempts` AS sa
								INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
								WHERE sa.examId={$data->examId} AND sa.completed=1 AND sa.checked=1 AND satm.marks = (
									SELECT max(satm.marks) FROM `submission_attempts` AS sa
									INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
									WHERE sa.examId= {$data->examId} AND sa.completed =1
								) ORDER BY id";
					$topperAttempt	= $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$topperAttempt	= $topperAttempt->toArray();
					$topperAttempt	= $topperAttempt[0]['id'];

					/*$select = "SELECT COUNT(*) AS rank
							FROM `submission_attempts`
							WHERE `score`>{$totalScore} AND  examId={$data->examId} AND completed=1 AND checked=1";*/
					$select = "SELECT COUNT(*) AS rank
							FROM `submission_attempts` AS sa
							INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
							WHERE satm.marks>{$totalScore} AND sa.examId={$data->examId} AND sa.completed=1 AND sa.checked=1";
					$myRank	= $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$myRank	= $myRank->toArray();
					$myRank	= $myRank[0]['rank']+1;

					/*$select = "SELECT COUNT(*) AS totalStudents
							FROM `submission_attempts`
							WHERE examId={$data->examId} AND completed=1 AND checked=1";*/
					$select = "SELECT COUNT(*) AS totalStudents
							FROM `submission_attempts` AS sa
							INNER JOIN `submission_attempt_total_marks` AS satm ON satm.attemptId=sa.id
							WHERE sa.examId={$data->examId} AND sa.completed=1 AND sa.checked=1";
					$totalRank	= $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$totalRank	= $totalRank->toArray();
					$totalRank	= $totalRank[0]['totalStudents'];

					foreach($questions as $key=>$question)
					{

						/*$query = "SELECT IFNULL((saq.marks),0) AS toppermarks, saq.checked
									FROM `submission_attempt_questions` as saq
									WHERE saq.questionId='$question[id]' AND saq.attemptId='$topperAttempt'";*/
						$query = "SELECT IFNULL((sam.marks),0) AS toppermarks, saq.checked
									FROM `submission_attempt_questions` as saq
									INNER JOIN `submission_attempt_marks` AS sam ON sam.attemptQuestionId=saq.id
									WHERE saq.questionId='$question[id]' AND saq.attemptId='$topperAttempt'";
						//print_r($query);
						$toppermarkstime = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$toppermarkstime = $toppermarkstime->toArray();
						if (count($toppermarkstime)>0) {
							$toppermarks = $this->checkAttemptMarks($toppermarkstime[0]['toppermarks'],1);
						} else {
							$toppermarks = 'NA';
						}

						$topperAnswerText = "";
						$topperAnswerFile = array();
						$query = "SELECT asu.id,asu.answerText,asu.answerFile
									FROM `submission_attempt_answers` AS asu
									INNER JOIN `submission_attempt_questions` AS saq ON asu.attemptQuestionId=saq.id
									WHERE saq.questionId='$question[id]' AND asu.submissionAttemptId='$topperAttempt' AND asu.status=1";
						//print_r($query);
						$topperanswers = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$topperanswers = $topperanswers->toArray();
						if (count($topperanswers)>0) {
							$topperAnswerText = $topperanswers[0]["answerText"];
							if (!empty($topperanswers[0]['answerFile'])) {
								$ext = pathinfo($topperanswers[0]['answerFile'], PATHINFO_EXTENSION);
								$topperAnswerFile = array(
													"path" => $topperanswers[0]['answerFile'],
													"type" => $ext
													);
							}
						}

						$uploadInfo = array();
						if (!empty($question['answerFile'])) {
							$ext = pathinfo($question['answerFile'], PATHINFO_EXTENSION);
							$uploadInfo = array(
												"path" => $question['answerFile'],
												"type" => $ext
												);
						}
						$submissionQuestions[$i]["id"]				= $question["id"];
						$submissionQuestions[$i]["qno"]				= $qnoCounter;
						$submissionQuestions[$i]["question"]		= $question['question'];
						$submissionQuestions[$i]["max_marks"]		= (int)$question["marks"];
						//$maxMarks+=$question["marks"];
						// MARKS
						$submissionQuestions[$i]["marks"]			= (int)$question["studentMarks"];
						$submissionQuestions[$i]["topper_marks"]	= (int)$toppermarks;
						$submissionQuestions[$i]["avg_marks"]		= 0;
						// MARKS END
						$submissionQuestions[$i]["student_answer_text"]	= $question['answerText'];
						$submissionQuestions[$i]["student_answer_file"]	= $uploadInfo;
						$submissionQuestions[$i]["topper_answer_text"]	= $topperAnswerText;
						$submissionQuestions[$i]["topper_answer_file"]	= $topperAnswerFile;
						$submissionQuestions[$i]["review"]			= $question["review"];
						$submissionQuestions[$i]["checked"]			= $question["checked"];
						$submissionQuestions[$i]['listType']		= '';
						
						$submissionQuestions[$i]["marks"]			= (int)$this->checkAttemptMarks($question["studentMarks"],1);
						// average marks
						/*$query = "SELECT saq.id, saq.checked, saq.marks
									FROM `submission_questions` AS qs
									INNER JOIN `submission_question_links` AS qsel ON qsel.questionId=qs.id
									INNER JOIN `submission_attempt_questions` AS saq ON saq.questionId=qs.id
									INNER JOIN `submission_attempts` AS sa ON sa.id=saq.attemptId
									INNER JOIN `submission_attempt_answers` AS asu ON asu.attemptQuestionId=saq.id AND asu.submissionAttemptId=saq.attemptId
									WHERE qsel.examId=$question[examId] AND saq.questionId='$question[id]' AND qs.status=1 AND qs.deleted=0 AND sa.checked=1 AND saq.checked=1";*/
						$query = "SELECT saq.id, saq.checked, AVG(sam.marks) AS avgMarks
									FROM `submission_questions` AS qs
									INNER JOIN `submission_attempt_questions` AS saq ON saq.questionId=qs.id
									INNER JOIN `submission_attempts` AS sa ON sa.id=saq.attemptId
									INNER JOIN `submission_question_links` AS qsel ON qs.id=qsel.questionId
									INNER JOIN `submission_attempt_answers` AS saa ON saa.attemptQuestionId=saq.id AND saa.submissionAttemptId=saq.attemptId
									INNER JOIN `submission_attempt_marks` AS sam ON sam.attemptQuestionId=saq.id
									WHERE qsel.examId=$question[examId] AND saq.questionId='$question[id]' AND qs.status=1 AND sa.checked=1 AND saq.checked=1";
						$avgResult = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$avgResult = $avgResult->toArray();
						if (count($avgResult)>0) {
							$avgMarks = round($avgResult[0]['avgMarks'],2);
						} else {
							$avgMarks = 'NA';
						}
						$submissionQuestions[$i]["avg_marks"]		= $avgMarks;

						$submissionQuestions[$i]['attempted'] = 1;

						$qnoCounter++;
						$i++;
					}
					// getting topper marks and the average marks of the exam
					
					if ($access == "public") {
						if($data->userRole != 4) {
							$query = "SELECT `id` FROM `submission_attempts` WHERE `completed`=1 AND `checked`=1 AND `examId` = {$data->examId} AND `studentId` = {$data->studentId} ORDER BY id";
						} else {
							$query = "SELECT `id` FROM `submission_attempts` WHERE `completed`=1 AND `checked`=1 AND `examId` = {$data->examId} AND `studentId` = {$data->userId} ORDER BY id";
						}
					} else{
						if($data->userRole != 4) {
							$query = "SELECT `id` FROM `submission_attempts` WHERE `completed`=1 AND `checked`=1 AND `examId` = {$data->examId} AND `studentGroupId` = {$studentGroupId} ORDER BY id";
						} else {
							$query = "SELECT `id` FROM `submission_attempts` WHERE `completed`=1 AND `checked`=1 AND `examId` = {$data->examId} AND `studentGroupId` = {$studentGroupId} ORDER BY id";
						}
					}
					//print_r($query);
					$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$attempts = $attempts->toArray();
					$result->prevAttemptId = '';
					$result->nextAttemptId = '';
					foreach ($attempts as $key => $value) {
						if ($value['id'] == $data->attemptId) {
							if (isset($attempts[$key-1]['id']) && !empty($attempts[$key-1]['id'])) {
								$result->prevAttemptId = $attempts[$key-1]['id'];
							}
							if (isset($attempts[$key+1]['id']) && !empty($attempts[$key+1]['id'])) {
								$result->nextAttemptId = $attempts[$key+1]['id'];
							}
						}
					}
					$result->questions	= $submissionQuestions;
					$result->totalScore	= (int)$totalScore;
					//$result->maxScore	= (int)$maxMarks;
					$result->maxScore	= $this->getMaxMarksSubmission($data->examId);
					$result->startDate	= $startDate;
					$result->endDate	= $endDate;
					$result->rank		= $myRank;
					$result->totalRank	= $totalRank;

					$dataRange = new stdClass();
					$dataRange->examId = $data->examId;
					$dataRange->examType = 'submission';
					$ranges=$this->getPercentRanges($dataRange);
					if ($ranges->status == 1) {
						$ranges = $ranges->score;
					} else {
						$ranges = array();
					}
					$result->ranges = $ranges;
					$result->status		= 1;
				} else {
					$result->status = 0;
				}
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function getMaxMarksSubmission($examId)
		{
			$adapter = $this->adapter;
			$query="SELECT qs.id,SUM(qsel.marks) maxMarks
					FROM `submission_question_links` AS qsel
					INNER JOIN `submission_questions` AS qs ON qs.id=qsel.questionId
					WHERE qsel.examId={$examId} AND qs.`status`=1
					ORDER BY qsel.id";
			$questions = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$questions = $questions->toArray();
			if (count($questions)) {
				$maxMarks = $questions[0]['maxMarks'];
			}
			return $maxMarks;
		}

	}
?>