<?php 

/**
* Tax class
*
* @author Archit Saxena
*/

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\HydratingResultSet;


require_once "Base.php";

class Tax extends Base {

    function __construct() {
        parent::__construct();
    }

    /**
    * Constructs an array of institutes with their respective tax rates if specified in tax_rates table
    * @return Array institute_taxes
    */
    public function getTaxRates() {
        $result = new stdClass();
        $adapter = $this->adapter;
        $result->defaultTaxes = Tax::defaultRates($adapter);
        $specificTaxes = Tax::specificRates($adapter);
        $institutes = Tax::allInstitutes($adapter);
        
        // Adding specific taxes to institutes
        foreach ($institutes as $key => $value) {
            foreach ($specificTaxes as $specificTax) {
                if ($value['userId'] == $specificTax['instituteId']) {
                    if(!isset($institutes[$key]['taxes']))
                        $institutes[$key]['taxes'] = array();
                    $institutes[$key]['taxes'][] = array(
                        'taxId'    => $specificTax['taxId'], 
                        'taxname'  => $specificTax['taxname'], 
                        'newValue' => $specificTax['newValue']
                    );
                }
            }
        }
        if ($institutes) {
            $result->status = 1;
            $result->institutes = $institutes;
        }
        else {
            $result->status = 0;
            $result->message = "No institutes found";
        }
        return $result;
    }
	
	/**
    * Constructs an array of institutes with their respective tax rates if specified in tax_rates table
    * @return Array institute_taxes
    */
    public function getTaxRatesForUser($userId) {
        $result = new stdClass();
        $adapter = $this->adapter;
        $defaultTaxes = Tax::defaultRates($adapter);
        $specificTaxes = Tax::specificRatesForUser($adapter, $userId);
        
        $formattedTaxes = array();
        $allTaxes = array();
        if(is_array($specificTaxes)) {
			foreach($specificTaxes as $specificTax) {
				$formattedTaxes[$specificTax['taxId']] = $specificTax['newValue'];
			}
		}
		
		foreach($defaultTaxes as $tax) {
			if(isset($formattedTaxes[$tax['taxId']]))
				$tax['value'] = $formattedTaxes[$tax['taxId']];
			else
				$tax['value'] = $tax['defaultValue'];
			if(isset($tax['defaultValue']))
				unset($tax['defaultValue']);
			$allTaxes[$tax['taxId']] = $tax;
		}
		return $allTaxes;
    }
	
    /**
    * Updates the taxes with values passed
    */
    public function updateTaxes($data) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $result = new stdClass();
            $adapter = $this->adapter;
            $sql = new Sql($this->adapter);
            foreach ($data->modify->taxes as $tax) {
                // check whether specific tax already present
                $isSpecificTaxPresent = Tax::isSpecificTaxPresent($tax->taxId, $data->modify->instituteId, $adapter);
                // if exception thrown by the function
                if (isset($isSpecificTaxPresent->status)) {
                    throw new Exception('Could not get data');
                }
                else {
                    if ($isSpecificTaxPresent) {
                        // Update the tax rate with the given value
                        $update = $sql->update();
                        $update->table('taxes_rate');
                        $update->set(array('newValue' => $tax->newValue));
                        $update->where(array(
                            'instituteId' => $data->modify->instituteId,
                            'taxId'       => $tax->taxId
                        ));
                        $statement = $sql->prepareStatementForSqlObject($update);
                        $count = $statement->execute()->getAffectedRows();
                    }
                    else {
                        // Insert the tax rate into taxes_rate
                        $taxes_rate = new TableGateway('taxes_rate', $adapter, null, new HydratingResultSet());
                        $taxes_rate->insert(array(
                            'instituteId' => $data->modify->instituteId,
                            'taxId'       => $tax->taxId,
                            'newValue'    => $tax->newValue
                        ));
                    }
                }
            }
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();
        }
        catch (Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }

    /**
    * Updates the default tax entry, taxname and taxvalue
    */
    public function updateDefaultTax($req) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $sql = new Sql($this->adapter);
            $update = $sql->update();
            $update->table('taxes');
            $update->set(array(
                'taxname' => $req->update->taxName,
                'default' => $req->update->taxValue
            ));
            $update->where(array('id' => $req->update->taxId));
            $statement = $sql->prepareStatementForSqlObject($update);
            $done = $statement->execute();
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();
        }
        catch (Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }

    /**
    * Deletes the default tax entry
    */
    public function deleteDefaultTax($req) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $sql = new Sql($this->adapter);
            $delete = $sql->delete();
            $delete->from('taxes');
            $delete->where(array('id' => $req->deleted->taxId));
            $statement = $sql->prepareStatementForSqlObject($delete);
            $deleted = $statement->execute();
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();
        }
        catch (Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }

    /**
    * Inserts a new default tax entry, taxName and taxValue
    */
    public function insertDefaultTax($req) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();
        try {
            $taxes_rate = new TableGateway('taxes', $this->adapter, null, new HydratingResultSet());
            $taxes_rate->insert(array(
                'taxname' => $req->insert->taxName,
                'default' => $req->insert->taxValue
            ));
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();
        }
        catch (Exception $e) {
            $db->rollBack();
        }
    }

    protected static function isSpecificTaxPresent($taxId, $instituteId, $adapter) {
        try {
            $result = new stdClass();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('taxes_rate');
            $select->columns(array('id'));

            $select->where(array(
                'instituteId' => $instituteId,
                'taxId'       => $taxId
                ));
            $statement = $sql->prepareStatementForSqlObject($select);
            $record = $statement->execute();

            if($record->count() !== 0) {
                return true;
            } else {
                return false;
            }
        }
        catch(Exception $e) {
            $result->exception = $e->getMessage();
            $result->status = 0;
            return $result;
        }
    }

    /**
    * Gets an array of default tax rates for each institute
    */
    protected static function defaultRates($adapter) {
        try {
            $query = 'SELECT `id` AS `taxId`, `taxname`, `default` AS `defaultValue` FROM `taxes`';
            $taxes = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            if ($taxes->count() !== 0) {
                $taxes = $taxes->toArray();
            }
            return $taxes;
        }
        catch(Exception $e) {
            return false;
        }
    }

    /**
    * Gets all the institutes with taxes different from Default values, specified in taxes_rate table
    */
    protected static function specificRates($adapter) {
        try {
            $query = 'SELECT `taxes_rate`.`instituteId`,  `taxes_rate`.`taxId`, `taxes`.`taxname`, `taxes_rate`.`newValue` FROM `taxes_rate` inner join `taxes` on `taxes`.`id` = `taxes_rate`.`taxId`';
            $specificTaxes = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            if ($specificTaxes->count() !== 0) {
                $specificTaxes = $specificTaxes->toArray();
            }
            return $specificTaxes;
        }
        catch(Exception $e) {
            return false;
        }
    }
    
    /**
    * Gets all the taxes different from Default values for a user, specified in taxes_rate table
    */
    protected static function specificRatesForUser($adapter, $userId) {
        try {
            $query = "SELECT `taxes_rate`.`instituteId`,  `taxes_rate`.`taxId`, `taxes`.`taxname`, `taxes_rate`.`newValue` FROM `taxes_rate` inner join `taxes` on `taxes`.`id` = `taxes_rate`.`taxId` WHERE `taxes_rate`.`instituteId` = {$userId}";
            $specificTaxes = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            if ($specificTaxes->count() !== 0) {
                $specificTaxes = $specificTaxes->toArray();
            }
            return $specificTaxes;
        }
        catch(Exception $e) {
            return false;
        }
    }

    /**
    * Gets a list of all the institutes
    */
    protected static function allInstitutes($adapter) {
        try {
            $query = 'SELECT `userId`, `name` AS `instituteName` FROM `institute_details`';
            $institutes = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            if ($institutes->count() !== 0) {
                $institutes = $institutes->toArray();
            }
            return $institutes;
        }
        catch(Exception $e) {
            return false;
        }
    }
}
