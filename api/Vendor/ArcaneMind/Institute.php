<?php

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "User.php";

//$adapter = require "adapter.php";

class Institute extends User {

	public $id;
	public $role;

	function __construct() {
		parent::__construct();
		$this->id = 0;
		$this->role = NULL;
	}

	public function registerShort($data) {
		$result = parent::registerShort($data);
		if ($result->status == 0) {
			return $result;
		}
		$data->userId = $result->userId;
		$result = parent::addBasicDetailsShort($data);
		if ($result->status == 0) {
			return $result;
		}

		$this->addInstituteProfileDetailsShort($data);

		$this->addUserInstituteTypes($data);


		if(isset($data->googleId) && !empty($data->googleId)) {
			parent::validateAccountShort($data);
		} else {
			$result = parent::sendVerificationEmail($data);
			if ($result->status == 0) {
				return $result;
			}
		}

		$result = new stdClass();
		$result->status = 1;
		$result->userId = $data->userId;
		return $result;
	}

	public function register($data) {
		$result = parent::register($data);
		if ($result->status == 0) {
			return $result;
		}
		$data->userId = $result->userId;
		$result = parent::addBasicDetails($data);
		if ($result->status == 0) {
			return $result;
		}

		$this->addInstituteProfileDetails($data);

		$this->addUserInstituteTypes($data);


		$result = parent::sendVerificationEmail($data);
		if ($result->status == 0) {
			return $result;
		}

		$result = new stdClass();
		$result->status = 1;
		$result->userId = $data->userId;
		return $result;
	}

	public function addInstituteProfileDetailsShort($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		try {
			if(!isset($data->instituteName) || empty($data->instituteName)) {
				$data->instituteName = "";
			}
			$instituteDetails = new TableGateway('institute_details', $this->adapter, null, new HydratingResultSet());
			$instituteDetails->insert(array(
				'userId' => $data->userId,
				'name' => $data->instituteName
			));
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
		} catch (Exception $e) {
			$db->rollBack();
		}
	}
	
	public function addInstituteProfileDetails($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		try {
			$instituteDetails = new TableGateway('institute_details', $this->adapter, null, new HydratingResultSet());
			$instituteDetails->insert(array(
				'userId' => $data->userId,
				'name' => $data->instituteName,
				'contactFirstName' => $data->contactFirstName,
				'contactLastName' => $data->contactLastName
			));
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
		} catch (Exception $e) {
			$db->rollBack();
		}
	}

	public function prepareUserInstituteType($data) {
		
	}

	/*
	 * 	$data						: Object
	 *  $data->userId				:	Int
	 * 	$data->newInstituteTypes	: Array
	 */

	public function addNewInstituteTypes($data) {
		/*         * ***
		  IMPROVEMENT: Currently inserting just one row at a time, limited by zend functionality.
		  -We need to extent ZEND DB class
		 * *** */

		$db = $this->adapter->getDriver()->getConnection();

		//$db->beginTransaction();

		$result = new stdClass();
		try {
			$instituteTypes = new TableGateway('institute_types', $this->adapter, null, new HydratingResultSet());
			$result->newInstituteTypes = array();
			foreach ($data->newInstituteTypes as $instituteType) {
				$instituteTypes->insert(array(
					'name' => $instituteType->name,
					'parentId' => $instituteType->parentId,
					'addedBy' => $data->userId,
					'createdAt' => $data->createdAt,
				));
				$result->newInstituteTypes[] = $instituteTypes->getLastInsertValue();
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			//$db->commit();
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			//$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function addUserInstituteTypes($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		try {
			$userInstituteTypes = new TableGateway('user_institute_types', $this->adapter, null, new HydratingResultSet());
			foreach ($data->stucturedInsTypes as $id => $instituteData) {
				if ($id == -1)
					continue;

				$instituteJsonData = Zend\Json\Json::encode($instituteData, true);
				$userInstituteTypes->insert(array(
					'userId' => $data->userId,
					'instituteTypeId' => $id,
					'data' => $instituteJsonData
				));
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
		} catch (Exception $e) {
			$db->rollBack();
		}
	}

	public function addNewInstituteSubtypes($data) {

		$db = $this->adapter->getDriver()->getConnection();

		//$db->beginTransaction();

		$result = new stdClass();
		try {
			$instituteSubtypes = new TableGateway('institute_subtypes', $this->adapter, null, new HydratingResultSet());
			$result->newInstituteSubtypes = array();
			foreach ($data->newInstituteSubtypes as $instituteSubtype) {
				$instituteSubtypes->insert(array(
					'name' => $instituteSubtype->name,
					'instituteTypeId' => $instituteSubtype->parentId,
					'addedBy' => $data->userId,
					'createdAt' => time(),
				));

				$newInstituteSubtype = new stdClass();
				$newInstituteSubtype->id = $instituteSubtypes->getLastInsertValue();
				$newInstituteSubtype->data = $instituteSubtype->data;

				$result->newInstituteSubtypes[] = $instituteSubtype;
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			//$db->commit();
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			//$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function addUserInstituteSubtypes($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		try {
			$userInstituteSubtypes = new TableGateway('user_institute_subtypes', $this->adapter, null, new HydratingResultSet());
			foreach ($data->userInstituteSubtypes as $instituteSubtype) {
				$userInstituteSubtypes->insert(array(
					'userId' => $data->userId,
					'instituteSubtypeId' => $instituteSubtype->id,
					'data' => $instituteSubtype->data,
				));
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
		} catch (Exception $e) {
			$db->rollBack();
		}
	}

	public function getProfileDetails($data) {
		try {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			//Login Details
			$select->from('login_details');
			$select->columns(array('id','email', 'username'));

			$select->where(array('id' => $data->userId));
			$statement = $sql->prepareStatementForSqlObject($select);
			$loginDetails = $statement->execute();

			if ($loginDetails->count() !== 0) {
				$result->loginDetails = $loginDetails->next();
			} else {
				$result->status = 0;
				$result->exception = "Login details not found";
				return $result;
			}

			// Basic details
			$select = $sql->select();
			$select->from(array('u' => 'user_details'))
					->join(array('c' => 'countries'), 'u.addressCountry = c.id', array('addressCountry' => 'name',
						'countryId' => 'id'),$select::JOIN_LEFT);
			$select->columns(array('contactMobilePrefix' => 'contactMobilePrefix',
				'contactMobile' => 'contactMobile',
				'contactLandlinePrefix' => 'contactLandlinePrefix',
				'contactLandline' => 'contactLandline',
				'addressStreet' => 'addressStreet',
				'addressCity' => 'addressCity',
				'addressState' => 'addressState',
				'addressPin' => 'addressPin'));

			$select->where(array('u.userId' => $data->userId));
			$statement = $sql->prepareStatementForSqlObject($select);
			$userDetails = $statement->execute();

			if ($userDetails->count() !== 0) {
				$result->userDetails = $userDetails->next();
			} else {
				$result->status = 0;
				$result->exception = "User details not found";
				return $result;
			}

			// Fetching institute categories
			if ($result->userDetails["countryId"] == 1) {
				$select = $sql->select();
				$select->from(array('uit' => 'user_institute_types'))
						->join(array('it' => 'institute_types'), 'it.id = uit.instituteTypeId', array('id', 'name', 'parentId'));
				$select->columns(array('data'));
				$select->where(array('uit.userId' => $data->userId));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$userInstituteTypes = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

				if ($userInstituteTypes->count() !== 0) {
					$result->userInstituteTypes = $userInstituteTypes->toArray();
				} else {
					/*$result->status = 0;
					$result->exception = "User Institutes not found";
					return $result;*/
				}
			} else {
				$result->userInstituteTypes = false;
			}
			// Fetching Profile Details
			$select = $sql->select();
			$select->from('institute_details');
			$select->columns(array('name', 'ownerFirstName',
				'ownerLastName', 'contactFirstName',
				'contactLastName', 'coverPic', 'profilePic',
				'description', 'foundingYear', 'studentCount','tagline'));

			$select->where(array('userId' => $data->userId));
			$statement = $sql->prepareStatementForSqlObject($select);
			$profileDetails = $statement->execute();

			if ($profileDetails->count() !== 0) {
				$result->profileDetails = $profileDetails->next();
			} else {
				$result->status = 0;
				$result->exception = "Profile not found";
				return $result;
			}
			$result->profileDetails['time'] = time();
			if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
				require_once 'amazonRead.php';
				$result->profileDetails['coverPic'] = getSignedURL($result->profileDetails['coverPic']);
				$result->profileDetails['profilePic'] = getSignedURL($result->profileDetails['profilePic']);
			} else {
				$result->profileDetails['profilePic'] = 'assets/layouts/layout2/img/avatar3_small.jpg';
			}

			// get courses by this institute
			if (empty($query)) {
				$query="SELECT c.id,c.name,c.image,c.slug,c.studentPrice,c.studentPriceINR, COUNT(DISTINCT user_id) as studentCount
						FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id=c.id
						WHERE c.deleted =0 AND c.ownerId = {$data->userId}
						GROUP BY c.id ORDER BY liveDate";
			}
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();
			$req1 = new stdClass();
			require_once 'Student.php';
			$s = new Student();
			require_once 'Course.php';
			$c = new Course();
			
			foreach ($result->courses as $key => $course) {
				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courses[$key]['image'] = getSignedURL($course['image']);
				}
				$result->courses[$key]['rating'] = $s->getCourseRatings($course['id']);
				$req1->courseId=$course['id'];
				$result->courses[$key]['discount']=$c->getCourseDiscount($req1);
				$result->courses[$key]['owner'] = $c->getCourseOwner($course['id']);
			}

			// get packages by this institute
			require_once 'StudentPackage.php';
			$sp = new StudentPackage();
			$institute = new stdClass();
			$institute->instituteId = $data->userId;
			$result->packages = $sp->getAllPackageForStudent($institute);

			// get instructors for this institute
			$result->instructors = $this->getInstituteProfessorsWithSubjects($data);
			//fetching course details for arcanemind experience
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('courses');
			$select->where(array(
						'ownerId'				=>	$data->userId,
						'deleted'				=>	0
			));
			$select->columns(array('id', 'name', 'subtitle', 'image', 'studentPrice', 'studentPriceINR'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$courses = $courses->toArray();
			$subjectCount = 0;
			$studentCount = 0;
			foreach($courses as $key=>$course) {
				if($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$courses[$key]['image'] = getSignedURL($courses[$key]['image']);
				}
				require_once 'Student.php';
				$s = new Student();
				$courses[$key]['rating'] = $s->getCourseRatings($courses[$key]['id']);
				require_once "CourseDetail.php";
				$cd = new CourseDetail();
				$data = new stdClass();
				$data->courseId = $courses[$key]['id'];
				$courses[$key]['studentCount'] = $cd->getStudentCount($data);
				$studentCount+=$courses[$key]['studentCount'];
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subjects');
				$select->where(array(
						'courseId'	=>	$course['id'],
						'deleted'	=>	0
				));
				$select->columns(array('id', 'name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects = $subjects->toArray();
				$subjectCount+= count($subjects);
				foreach($subjects as $key1=>$subject) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('sp'	=>	'subject_professors'))
					->join(array('pd'	=>	'professor_details'),
								'sp.professorId = pd.userId',
								array('name'	=>	new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"))
					);
					$select->where(array('subjectId'	=>	$subject['id']));
					$select->columns(array());
					$selectString = $sql->getSqlStringForSqlObject($select);
					$professors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$professors = $professors->toArray();
					$subjects[$key1]['professors'] = $professors;
				}
				$courses[$key]['subjects'] = $subjects;
			}
			$result->courses = $courses;
			$result->subjectCount = $subjectCount;
			$result->studentCount = $studentCount;
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	/*public function getProfileStats($data) {
		try {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			//Login Details
			$select->from('login_details');
			$select->columns(array('id','email', 'username'));

			$select->where(array('id' => $data->userId));
			$statement = $sql->prepareStatementForSqlObject($select);
			$loginDetails = $statement->execute();

			if ($loginDetails->count() !== 0) {
				$result->loginDetails = $loginDetails->next();
			} else {
				$result->status = 0;
				$result->exception = "Login details not found";
				return $result;
			}

			// get courses by this institute
			if (empty($query)) {
				$query="SELECT c.id,c.name,c.image,c.slug,c.studentPrice,c.studentPriceINR, COUNT(DISTINCT user_id) as studentCount
						FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id=c.id
						WHERE c.deleted =0 AND c.ownerId = {$data->userId}
						GROUP BY c.id ORDER BY liveDate";
			}
			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courses = $courses->toArray();

			$req1 = new stdClass();
			require_once 'Student.php';
			$s = new Student();
			require_once 'Course.php';
			$c = new Course();
			$studentCount = 0;
			
			foreach ($result->courses as $key => $course) {
				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courses[$key]['image'] = getSignedURL($course['image']);
				}
				$result->courses[$key]['rating'] = $s->getCourseRatings($course['id']);
				$req1->courseId=$course['id'];
				$result->courses[$key]['discount']=$c->getCourseDiscount($req1);
				$result->courses[$key]['owner'] = $c->getCourseOwner($course['id']);
			}

			// get packages by this institute
			require_once 'StudentPackage.php';
			$sp = new StudentPackage();
			$institute = new stdClass();
			$institute->instituteId = $data->userId;
			$result->packages = $sp->getAllPackageForStudent($institute);

			// get instructors for this institute
			$result->instructors = $this->getInstituteProfessorsWithSubjects($data);
			//fetching course details for arcanemind experience
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('courses');
			$select->where(array(
						'ownerId'				=>	$data->userId,
						'deleted'				=>	0
			));
			$select->columns(array('id', 'name', 'subtitle', 'image', 'studentPrice', 'studentPriceINR'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$courses = $courses->toArray();
			foreach($courses as $key=>$course) {
				if($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$courses[$key]['image'] = getSignedURL($courses[$key]['image']);
				}
				require_once 'Student.php';
				$s = new Student();
				$courses[$key]['rating'] = $s->getCourseRatings($courses[$key]['id']);
				require_once "CourseDetail.php";
				$cd = new CourseDetail();
				$data = new stdClass();
				$data->courseId = $courses[$key]['id'];
				$courses[$key]['studentCount'] = $cd->getStudentCount($data);
				$studentCount+=$courses[$key]['studentCount'];
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subjects');
				$select->where(array(
						'courseId'	=>	$course['id'],
						'deleted'	=>	0
				));
				$select->columns(array('id', 'name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects = $subjects->toArray();
				foreach($subjects as $key1=>$subject) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('sp'	=>	'subject_professors'))
					->join(array('pd'	=>	'professor_details'),
								'sp.professorId = pd.userId',
								array('name'	=>	new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"))
					);
					$select->where(array('subjectId'	=>	$subject['id']));
					$select->columns(array());
					$selectString = $sql->getSqlStringForSqlObject($select);
					$professors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$professors = $professors->toArray();
					$subjects[$key1]['professors'] = $professors;
				}
				$courses[$key]['subjects'] = $subjects;
			}
			$result->courses = $courses;
			$result->studentCount = $studentCount;
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}*/

	public function saveCoverImage($filePath, $userId) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$where = array('userId' => $userId);
			
			//deleting previous image from cloud
			$select = $sql->select();
			$select->from('institute_details');
			$select->where($where);
			$select->columns(array('coverPic'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$coverPic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$coverPic = $coverPic->toArray();
			$coverPic = $coverPic[0]['coverPic'];
			$coverPic = substr($coverPic, 37);
			if($coverPic != '') {
				require_once 'amazonDelete.php';
				deleteFile($coverPic);
			}
			
			//updating new cover pic in database
			$update = $sql->update();
			$update->table('institute_details');
			$update->set(array('coverPic' => $filePath));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = 'Cover Image Uploaded!';
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function saveProfileImage($filePath, $userId) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$where = array('userId' => $userId);
			$sql = new Sql($adapter);
			
			//deleting previous image from cloud
			$select = $sql->select();
			$select->from('institute_details');
			$select->where($where);
			$select->columns(array('profilePic'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$profilePic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$profilePic = $profilePic->toArray();
			$profilePic = $profilePic[0]['profilePic'];
			$profilePic = substr($profilePic, 37);
			if($profilePic != '') {
				require_once 'amazonDelete.php';
				deleteFile($profilePic);
			}
			
			$update = $sql->update();
			$update->table('institute_details');
			$update->set(array('profilePic' => $filePath));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();

			$select = $sql->select();
			$select->from('institute_details');
			$select->where($where);
			$select->columns(array('profilePic'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$profilePic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$profilePic = $profilePic->toArray();
			$profilePic = $profilePic[0]['profilePic'];
			if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
				require_once 'amazonRead.php';
				$profilePic = getSignedURL($profilePic);
			} else {
				$profilePic = 'assets/layouts/layout2/img/avatar3_small.jpg';
			}

			$result->status = 1;
			$result->message = 'Profile Image Uploaded!';
			$result->image = $profilePic;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function updateProfileDescription($req) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$where = array('userId' => $req->userId);
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('institute_details');
			$update->set(array('description' => $req->description));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = 'Profile Updated!';
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function updateProfileGeneral($req) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$where = array('userId' => $req->userId);
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('institute_details');
			$update->set(array('name' => $req->instituteName,
				'tagline' => $req->tagline,
				'ownerFirstName' => $req->ownerFirstName,
				'ownerLastName' => $req->ownerLastName,
				'contactFirstName' => $req->contactFirstName,
				'contactLastName' => $req->contactLastName,
				'studentCount' => $req->studentCount,
				'foundingYear' => $req->foundedIn));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			//var_dump($statement);die();
			$update = $sql->update();
			$update->table('user_details');
			$update->set(array('contactMobilePrefix' => $req->phnPrefix,
				'contactMobile' => $req->phnNum,
				'contactLandlinePrefix' => $req->llPrefix,
				'contactLandline' => $req->llNum,
				'addressStreet' => $req->street,
				'addressCity' => $req->city,
				'addressState' => $req->state,
				'addressCountry' => $req->country,
				'addressPin' => $req->pin));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = 'Profile Updated!';
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function updateUserInstituteType($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$delete = $sql->delete();
			//Login Details
			$delete->from('user_institute_types');
			$delete->where(array('userId' => $data->userId));
			$statement = $sql->prepareStatementForSqlObject($delete);
			$user_institute_types = $statement->execute();
			$userInstituteTypes = new TableGateway('user_institute_types', $adapter, null, new HydratingResultSet());
			foreach ($data->newStructuredInstitutes as $id => $instituteData) {
				if ($id == -1)
					continue;
				$instituteJsonData = Zend\Json\Json::encode($instituteData, true);
				$userInstituteTypes->insert(array(
					'userId' => $data->userId,
					'instituteTypeId' => $id,
					'data' => $instituteJsonData
				));
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = 'Profile Updated!';
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getInstituteProfessors($data) {
		try {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			//checking course ownership
			$select = $sql->select();
			$select->from('courses');
			$select->where(array('id'   =>  $data->courseId));
			$select->columns(array('ownerId'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$owner = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$owner = $owner->toArray();
			if(count($owner) > 0) {
				$owner = $owner[0]['ownerId'];
			}

			if($data->userId != $owner) {
				$result->status = 2;
				$result->message = 'You can not modify professors';
				return $result;
			}

			$select = $sql->select();
			$select->from(array('ip' => 'institute_professors'))
			->join(array('p' => 'professor_details'), 'ip.professorId = p.userId', array('firstName', 'lastName'));
			//$select->columns(array());
			if (isset($data->InstituteId)) {
				$select->where(array('ip.instituteId' => $data->InstituteId));
			} else {
				$select->where(array('ip.instituteId' => $data->userId));
			}
			$selectString = $sql->getSqlStringForSqlObject($select);
			$instituteProfessors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->instituteProfessors = $instituteProfessors->toArray();

			$select = $sql->select();
			$select->from('subject_professors');
			$select->where(array('subjectId' => $data->subjectId));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$subjectProfessors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->subjectProfessors = $subjectProfessors->toArray();

			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function instituteHasProfessor($data) {
		$result = new stdClass();
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('institute_professors');
		$where = new \Zend\Db\Sql\Where();
		$where->equalTo('instituteId', $data->userId);
		$where->equalTo('professorId', $data->professorId);
		 $select->where($where);
		$selectString = $sql->getSqlStringForSqlObject($select);
		$professor = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$result->professor = $professor->toArray();

		if (count($result->professor) == 0) {
			return false;
		}
		return true;
	}

	public function getInstituteProfessorsWithSubjects($data) {
		try {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();

			$select->from(array('ip' => 'institute_professors'))
					->join(array('p' => 'professor_details'), 'ip.professorId = p.userId', array('firstName', 'lastName', 'profilePic'))
					->join(array('u' => 'user_details'), 'ip.professorId = u.userId', array('contactMobilePrefix', 'contactMobile', 'contactLandlinePrefix', 'contactLandline'))
					->join(array('l' => 'login_details'), 'l.id = p.userId', array('email'));


			$select->where(array('ip.instituteId' => $data->userId));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$instituteProfessors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->tempInstituteProfessors = $instituteProfessors->toArray();

			$instituteSubjects = $this->getInstituteSubjectIds($data);
			$temp = array();
			if(count($instituteSubjects)==0)
			{
				$result->instituteProfessors = $result->tempInstituteProfessors;
				unset($result->tempInstituteProfessors);
				$result->status = 1;
				return $result;
			}
			foreach ($result->tempInstituteProfessors as $prof) {
				
				$select = $sql->select();
				$select->from(array('sp' => 'subject_professors'))
						->join(array('s' => 'subjects'), 'sp.subjectId = s.id', array('subjectName' => 'name'))
						->join(array('c' => 'courses'), 's.courseId = c.id', array('courseId' => 'id','courseName' => 'name'));
				//subject.course
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('professorId', $prof['professorId']);
				$where->in('subjectId', $instituteSubjects);
				$select->where($where);
				$select->group('courseId');
				$selectString = $sql->getSqlStringForSqlObject($select);
				$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$prof['courses'] = $courses = $courses->toArray();

				foreach ($courses as $key1 => $course) {
					$select = $sql->select();
					$select->from(array('sp' => 'subject_professors'))
							->join(array('s' => 'subjects'), 'sp.subjectId = s.id', array('subjectName' => 'name'))
							->join(array('c' => 'courses'), 's.courseId = c.id', array('courseName' => 'name'));
					//subject.course
					$where = new \Zend\Db\Sql\Where();
					$where->equalTo('professorId', $prof['professorId']);
					$where->equalTo('courseId', $course['courseId']);
					$where->in('subjectId', $instituteSubjects);
					$select->where($where);
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$prof['courses'][$key1]['subjects'] = $subjects->toArray();
				}
				
				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$prof['profilePic'] = getSignedURL($prof['profilePic']);
				} else {
					$prof['profilePic'] = "http://p.imgci.com/db/PICTURES/CMS/263600/263697.20.jpg";
				}
				$temp[] = $prof;
			}

			unset($result->tempInstituteProfessors);
			$result->instituteProfessors = $temp;
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getInstituteSubjectIds($data) {
		$result = new stdClass();
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('subjects');
		$select->where(array('ownerId' => $data->userId));
		$selectString = $sql->getSqlStringForSqlObject($select);
		$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);

		$result->subjects = $subjects->toArray();
		$temp = array();
		//var_dump($subjects); die();
		foreach ($result->subjects as $s) {
			$temp[] = $s['id'];
		}
		return $temp;
	}

	public function getAllInstituteCoursesForTree($data) {
		$result = new stdClass();
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		
	   	if (isset($data->slug) && !empty($data->slug)) {
			$selectString ="SELECT * FROM `pf_portfolio` WHERE `subdomain`='{$data->slug}' AND `userId`={$data->userId}";
			$portfolioDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			if($portfolioDetails->count() > 0) {
				$portfolios	 = $portfolioDetails->toArray();
				$portfolio	 = $portfolios[0];
				$portfolioId = $portfolio['id'];
				$instituteId = $portfolio['userId'];
				$selectString="SELECT c.id,c.name,COUNT(s.id) subjects
								FROM `pf_main_courses` AS pmc
								INNER JOIN `courses` AS c ON c.id=pmc.courseId
								LEFT OUTER JOIN subjects s
								ON c.id = s.courseId
								WHERE pmc.portfolioId={$portfolioId} AND c.ownerId= $instituteId AND c.deleted = 0 AND c.id NOT IN (select newCourseId from purchase_history) GROUP BY c.id ORDER BY c.weight ASC, c.id DESC";
			}
		} else {
	   		$selectString="SELECT c.id,c.name,COUNT(s.id) subjects
	   						FROM courses c
	   						LEFT OUTER JOIN subjects s on c.id = s.courseId
	   						WHERE c.ownerId= $data->userId AND c.deleted = 0 AND c.id NOT IN (SELECT newCourseId FROM purchase_history)
	   						GROUP BY c.id
	   						ORDER BY c.weight ASC, c.id DESC";
		}
		
		$courses = $adapter->query("$selectString", $adapter::QUERY_MODE_EXECUTE);
		$result->courses = $courses->toArray();

		$result->subjects = $this->getAllInstituteSubjectsForTree($data)->subjects;
		$result->chapters = $this->getAllInstituteChaptersForTree($data)->chapters;
		return $result;
	}

	public function getAllInstituteSubjectsForTree($data) {
		$result = new stdClass();
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from(array('s' => 'subjects'))
				->join(array('c' => 'chapters'), 's.id = c.subjectId', array('chapters' => new \Zend\Db\Sql\Expression('COUNT(c.id)')), $select::JOIN_LEFT)
				->group('s.id')
				->order('s.weight, s.id ASC');
		$select->columns(array('id' => 'id',
			'name' => 'name',
			'courseId' => 'courseId'));
		$select->where(array('s.ownerId' => $data->userId, 's.deleted'	=>	0));
		$selectString = $sql->getSqlStringForSqlObject($select);
		$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$result->subjects = $subjects->toArray();
		foreach($result->subjects as $key=>$subject) {
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('exams');
			$select->where(array(
					'subjectId'	=>	$subject['id'],
					'chapterId'	=>	0,
					'delete'	=>	0
			));
			$select->columns(array('id', 'name', 'type'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$exams = $exams->toArray();
			$result->subjects[$key]['exams'] = $exams;
		}
		return $result;
	}

	public function getAllInstituteChaptersForTree($data) {
		$result = new stdClass();
		$adapter = $this->adapter;
		/* $sql = new Sql($adapter);
		  $select = $sql->select();
		  $select->from('chapters'))
		  ->order('id DESC');
		  $select->columns('id','subjectId','name');
		  $select->where(array('ownerId' => $data->userId));
		  $selectString = $sql->getSqlStringForSqlObject($select); */
		$chapters = $adapter->query("SELECT id,subjectId,name FROM chapters WHERE subjectId IN (SELECT id FROM subjects WHERE ownerId=$data->userId) AND deleted=0 ORDER BY weight ASC, id ASC", $adapter::QUERY_MODE_EXECUTE);
		$result->chapters = $chapters->toArray();
		foreach($result->chapters as $key=>$chapter) {
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('exams');
			$select->where(array(
					'chapterId'	=>	$chapter['id'],
					'delete'	=>	0
			));
			$select->columns(array('id', 'name', 'type'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$exams = $exams->toArray();
			$result->chapters[$key]['exams'] = $exams;
		}
		return $result;
	}

	/* @Wadhwa C. */

	public function getInvitedProfessors($data) {
		$result = new stdClass();
		$adapter = $this->adapter;
		$sql = new Sql($adapter);

		$select = $sql->select();

		$select->from(array('ii' => 'institute_invitations'))
		->join(array('pd' => 'professor_details'), 'ii.prof_id  = pd.userId', array('firstName', 'lastName'))
		->join(array('ld' => 'login_details'), 'ld.id = pd.userId', array('email'))
		->join(array('ud' => 'user_details'), 'ud.userId = ld.id', array('contactMobilePrefix', 'contactMobile'));
		$select->columns(array('id', 'status'));


		$select->where(array('ii.institute_id' => $data->userId));
		$selectString = $sql->getSqlStringForSqlObject($select);
		$courseCategories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$result->check = $courseCategories->toArray();
		return $result;
	}

	public function updateinvitation($req) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$where = array('id' => $req->sno);
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('institute_invitations');
			$update->set(array('status' => 0));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	public function updateTagLine($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$where = array('userId' => $data->userId);
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('institute_details');
			$update->set(array('tagline' => $data->tagline));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = 'Tagline changed succesfully!';
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	//function to get institute counter details for institute dashboard
	public function getInstituteCounters($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			if (isset($data->portfolioSlug) && !empty($data->portfolioSlug)) {
				//to select courses count
				$selectString ="SELECT * FROM `pf_portfolio` WHERE `subdomain`='{$data->portfolioSlug}'";
				$portfolioDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$portfolioDetails = $portfolioDetails->toArray();
				$portfolioDetails = $portfolioDetails[0];
				$portfolioId = $portfolioDetails['id'];
				$selectString ="SELECT COUNT(c.id) AS courses
								FROM `courses` AS c
								INNER JOIN `pf_main_courses` AS pmc ON c.id=pmc.courseId
								WHERE c.ownerId={$data->userId} AND c.deleted=0 AND pmc.portfolioId={$portfolioId}";
				$courseCount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$courseCount = $courseCount->toArray();
				$courseCount = $courseCount[0]['courses'];
				$result->courseCount = $courseCount;
				
				//to select student count
				$selectString ="SELECT COUNT(*) AS students
								FROM student_course AS sc
								INNER JOIN `pf_main_courses` AS pmc ON sc.course_id=pmc.courseId
								INNER JOIN `login_details` AS ld ON ld.id=sc.user_id AND ld.temp=0
								WHERE sc.course_id IN (SELECT id FROM courses WHERE ownerId={$data->userId} AND deleted=0) AND pmc.portfolioId={$portfolioId}";
				$studentCount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$studentCount = $studentCount->toArray();
				$studentCount = $studentCount[0]['students'];
				$result->studentCount = $studentCount;
			} else {
			
				//to select courses count
				$select = $sql->select();
				$select->from('courses');
				$select->where(array(
					'ownerId'	=>	$data->userId,
					'deleted'	=>	0
				));
				$select->columns(array('courses'	=>	new \Zend\Db\Sql\Expression("COUNT(id)")));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$courseCount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$courseCount = $courseCount->toArray();
				$courseCount = $courseCount[0]['courses'];
				$result->courseCount = $courseCount;
				
				//to select student count
				$selectString = "SELECT COUNT(*) AS students
					FROM student_course AS sc
					INNER JOIN `login_details` AS ld ON ld.id=sc.user_id AND ld.temp=0
					WHERE sc.course_id IN (SELECT id FROM courses WHERE ownerId={$data->userId} AND deleted=0)";
				$studentCount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$studentCount = $studentCount->toArray();
				$studentCount = $studentCount[0]['students'];
				$result->studentCount = $studentCount;

				//to select student count
				/*$selectString = "SELECT COUNT(*) AS courseKeys FROM course_key_details WHERE userId ={$data->userId} AND active_date!='0000-00-00 00:00:00'";
				$courseKeys = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$courseKeys = $courseKeys->toArray();
				$courseKeys = $courseKeys[0]['courseKeys'];
				$result->courseKeys = $courseKeys;*/
				/*$selectString = "SELECT COUNT(*) AS courseKeys FROM course_key_details WHERE userId ={$data->userId} AND active_date IS NOT NULL";*/
				$selectString = "SELECT COUNT(*) AS courseKeys FROM course_key_details WHERE userId ={$data->userId} AND (`active_date` IS NULL OR `active_date` LIKE '0000-00-00 00:00:00')";
				$courseKeys = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$courseKeys = $courseKeys->toArray();
				$courseKeys = $courseKeys[0]['courseKeys'];
				$result->courseKeys = $courseKeys;
			}
			
			//to select professor count
			$select = $sql->select();
			$select->from('institute_professors');
			$select->where(array('instituteId'	=>	$data->userId));
			$select->columns(array('professors'	=>	new \Zend\Db\Sql\Expression("COUNT(*)")));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$professorCount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$professorCount = $professorCount->toArray();
			$professorCount = $professorCount[0]['professors'];
			$result->professorCount = $professorCount;
			
			//to select assigned subjects to a professor
			if($data->userRole == 2) {
				$select = $sql->select();
				$select->from('subject_professors');
				$select->where(array('professorId'  =>  $data->userId));
				$select->columns(array('subjects' =>  new \Zend\Db\Sql\Expression("COUNT(*)")));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjectCount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjectCount = $subjectCount->toArray();
				$subjectCount = $subjectCount[0]['subjects'];
				$result->assignedSubjects = $subjectCount;

				//now counting the number of students tregistered in assigned subjects
				$selectString = "SELECT COUNT(DISTINCT user_id) AS count FROM student_course WHERE course_id IN (SELECT courseId FROM subjects WHERE id IN (SELECT subjectId FROM `subject_professors` WHERE professorId={$data->userId}));";
				$count = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$count = $count->toArray();
				$count = $count[0]['count'];
				$result->assignedSubjectsStudent = $count;				
			}

			$result->userRole = $data->userRole;
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	//function to get institute graph details for institute dashboard
	public function getInstituteGraphs($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			if (isset($data->portfolioSlug) && !empty($data->portfolioSlug)) {
				$selectString ="SELECT * FROM `pf_portfolio` WHERE `subdomain`='{$data->portfolioSlug}'";
				$portfolioDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$portfolioDetails = $portfolioDetails->toArray();
				$portfolioDetails = $portfolioDetails[0];
				$portfolioId = $portfolioDetails['id'];
				
				//for main course student graph
				$selectString ="SELECT c.id AS courseId, c.name, COUNT(sc.id) AS students
								FROM courses AS c
								LEFT JOIN student_course AS sc ON c.id=sc.course_id
								LEFT JOIN subjects AS s ON s.courseId=c.id AND s.deleted=0
								LEFT JOIN subject_professors AS sp ON sp.subjectId=s.id
								INNER JOIN pf_main_courses AS pmc ON pmc.courseId=c.id
								WHERE (c.ownerId={$data->userId} || sp.professorId={$data->userId}) AND c.deleted=0 AND pmc.portfolioId={$portfolioId}
								GROUP BY c.id";	
			} else {
				//for main course student graph
				$selectString = "SELECT c.id AS courseId, c.name, COUNT(sc.id) AS students
								FROM courses AS c
								LEFT JOIN student_course AS sc ON c.id=sc.course_id
								LEFT JOIN subjects AS s ON s.courseId=c.id AND s.deleted=0
								LEFT JOIN subject_professors AS sp ON sp.subjectId=s.id
								WHERE (c.ownerId={$data->userId} || sp.professorId={$data->userId}) AND c.deleted=0
								GROUP BY c.id";
			}
			$graph1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$graph1 = $graph1->toArray();
			$result->graph1 = $graph1;

			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	public function getRecentCourseView($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			if (isset($data->courseId) && !empty($data->courseId)) {			
				//for main course student graph
				$selectString = "SELECT COUNT(studentId) AS opened, `date`  FROM `student_opened_courses` WHERE courseId={$data->courseId} GROUP BY `date` ORDER BY `date` DESC LIMIT 7";
				$graph = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$graph = $graph->toArray();
				$result->graph = $graph;
			}

			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getDeatilRecentCourseView($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			
			//for main course student graph
			$selectString = "SELECT concat(`firstName`, ' ', `lastName` ) as student, date_format( `date`, '%d-%M-%Y' ) as visdate
							FROM `student_opened_courses` so
							INNER JOIN student_details sd ON sd.userId = so.studentId
							WHERE courseId={$data->courseId} ORDER BY `date` DESC LIMIT 7";
			$graph = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$graph = $graph->toArray();
			$result->graph = $graph;

			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getInstituteNotifications($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			//for main course student graph
			$selectString = "SELECT COUNT(*) AS notiCount
							FROM notifications
							WHERE userId={$data->userId} AND `status`=0";
			$totalnotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$totalnotifications = $totalnotifications->toArray();
			$result->notiCount	= (int)$totalnotifications[0]['notiCount'];

			if ($result->notiCount<20) {
				//for main course student graph
				$selectString ="SELECT `id`, `message`, `status`, `timestamp`
								FROM notifications
								WHERE userId={$data->userId}
								ORDER BY timestamp DESC
								LIMIT 20";
			} else {
				//for main course student graph
				$selectString ="SELECT `id`, `message`, `status`, `timestamp`
								FROM notifications
								WHERE userId={$data->userId} AND `status`=0
								ORDER BY timestamp DESC";
			}
			$sitenotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->sitenotifications = $sitenotifications->toArray();


			$selectString ="SELECT sp.professorId
							FROM `subject_professors` AS sp
							INNER JOIN `subjects` AS s ON s.id=sp.subjectId AND s.deleted=0
							INNER JOIN `courses` AS c ON c.id=s.courseId AND c.deleted=0
							INNER JOIN `login_details` AS ld ON ld.id=c.ownerId AND ld.active=1
							WHERE ld.id={$data->userId}";
			$professors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$professors = $professors->toArray();
			$chatnotifications = array();
			$siteProfessors = array();
			$chatCount = 0;
			if (count($professors)) {
				foreach ($professors as $professor) {
					$siteProfessors[] = $professor["professorId"];
				}
				$siteProfessors = implode(",", $siteProfessors);
				$selectString ="SELECT COUNT(*) AS notiCount
								FROM `chat_notifications` AS cn
								INNER JOIN `chat_rooms` AS cr ON cr.id=cn.roomId
								INNER JOIN `subjects` AS s ON s.id=cr.subjectId
								INNER JOIN `courses` AS c ON c.id=cr.courseId
								INNER JOIN `login_details` AS ld ON ld.id=cn.senderId
								INNER JOIN `professor_details` AS pd ON pd.userId=cn.userId
								INNER JOIN `user_roles` AS ur on ur.userId=cn.senderId
								WHERE cn.userId IN ($siteProfessors) AND c.ownerId={$data->userId} AND cn.status=0";
				$totalnotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$totalnotifications = $totalnotifications->toArray();
				$chatCount	= (int)$totalnotifications[0]['notiCount'];

				if ($chatCount<20) {
					$selectString ="SELECT cn.*, c.name AS courseName, s.name AS subjectName, cr.room_type,
									CASE
										when ur.roleId= 1 then (select name from institute_details where userId=cn.senderId)
										when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId=cn.senderId)
										when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId=cn.senderId)
										when ur.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId=cn.senderId)
									END as senderName, pd.firstName, pd.lastName, pd.profilePic, ur.roleId 
									FROM `chat_notifications` AS cn
									INNER JOIN `chat_rooms` AS cr ON cr.id=cn.roomId
									INNER JOIN `subjects` AS s ON s.id=cr.subjectId
									INNER JOIN `courses` AS c ON c.id=cr.courseId
									INNER JOIN `login_details` AS ld ON ld.id=cn.senderId
									INNER JOIN `professor_details` AS pd ON pd.userId=cn.userId
									INNER JOIN `user_roles` AS ur on ur.userId=cn.senderId
									WHERE cn.userId IN ($siteProfessors) AND c.ownerId={$data->userId}
									ORDER BY cn.timestamp DESC
									LIMIT 20";
				} else {
					$selectString ="SELECT cn.*, c.name AS courseName, s.name AS subjectName, cr.room_type,
									CASE
										when ur.roleId= 1 then (select name from institute_details where userId=cn.senderId)
										when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId=cn.senderId)
										when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId=cn.senderId)
										when ur.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId=cn.senderId)
									END as senderName, pd.firstName, pd.lastName, pd.profilePic, ur.roleId 
									FROM `chat_notifications` AS cn
									INNER JOIN `chat_rooms` AS cr ON cr.id=cn.roomId
									INNER JOIN `subjects` AS s ON s.id=cr.subjectId
									INNER JOIN `courses` AS c ON c.id=cr.courseId
									INNER JOIN `login_details` AS ld ON ld.id=cn.senderId
									INNER JOIN `professor_details` AS pd ON pd.userId=cn.userId
									INNER JOIN `user_roles` AS ur on ur.userId=cn.senderId
									WHERE cn.userId IN ($siteProfessors) AND c.ownerId={$data->userId}
									ORDER BY cn.timestamp DESC";
				}
				$chatnotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$chatnotifications = $chatnotifications->toArray();
				foreach ($chatnotifications as $key => $notif) {
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
						require_once 'amazonRead.php';
						$chatnotifications[$key]["profilePic"] = getSignedURL($chatnotifications[$key]["profilePic"]);
					} else {
						$chatnotifications[$key]["profilePic"] = 'assets/pages/media/users/avatar1.jpg';
					}
				}
				$result->chatCount = $chatCount;
				$result->chatnotifications = $chatnotifications;
			} else {
				$result->chatCount = 0;
				$result->chatnotifications = array();
			}
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	public function getInstituteNotificationsActivity($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			//for main course student graph
			$selectString = "SELECT COUNT(*) AS notiCount FROM notifications  WHERE userId={$data->userId} order by timestamp desc";
			$totalnotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$totalnotifications = $totalnotifications->toArray();
			$result->notiCount	= (int)$totalnotifications[0]['notiCount'];

			$pageStart = ($data->pageNumber)*($data->pageLimit);
			//for main course student graph
			$selectString = "SELECT `id`, `message`, `status`, `timestamp` FROM notifications  WHERE userId={$data->userId} order by timestamp desc LIMIT {$pageStart},{$data->pageLimit}";
			$sitenotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->sitenotifications = $sitenotifications->toArray();
			$result->status = 1;

			$where = array('userId' => $data->userId);
			$update = $sql->update();
			$update->table('notifications');
			$updateNotifications = array('status' => 1);
			$update->set($updateNotifications);
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();

			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getInstituteNotificationsChat($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$selectString ="SELECT sp.professorId
							FROM `subject_professors` AS sp
							INNER JOIN `subjects` AS s ON s.id=sp.subjectId AND s.deleted=0
							INNER JOIN `courses` AS c ON c.id=s.courseId AND c.deleted=0
							INNER JOIN `login_details` AS ld ON ld.id=c.ownerId AND ld.active=1
							WHERE ld.id={$data->userId}";
			$professors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$professors = $professors->toArray();
			$chatnotifications = array();
			$siteProfessors = array();
			if (count($professors)) {
				foreach ($professors as $professor) {
					$siteProfessors[] = $professor["professorId"];
				}
				$siteProfessors = implode(",", $siteProfessors);
				$selectString ="SELECT cn.*, c.name AS courseName, s.name AS subjectName, cr.room_type,
								CASE
									when ur.roleId= 1 then (select name from institute_details where userId=cn.senderId)
									when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId=cn.senderId)
									when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId=cn.senderId)
									when ur.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId=cn.senderId)
								END as senderName, pd.firstName, pd.lastName, ur.roleId 
								FROM `chat_notifications` AS cn
								INNER JOIN `chat_rooms` AS cr ON cr.id=cn.roomId
								INNER JOIN `subjects` AS s ON s.id=cr.subjectId
								INNER JOIN `courses` AS c ON c.id=cr.courseId
								INNER JOIN `login_details` AS ld ON ld.id=cn.senderId
								INNER JOIN `professor_details` AS pd ON pd.userId=cn.userId
								INNER JOIN `user_roles` AS ur on ur.userId=cn.senderId
								WHERE cn.userId IN ($siteProfessors) AND c.ownerId={$data->userId}";
				$totalnotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$totalnotifications = $totalnotifications->toArray();
				
				$pageStart = ($data->pageNumber)*($data->pageLimit);

				$selectString ="SELECT cn.*, c.name AS courseName, s.name AS subjectName, cr.room_type,
								CASE
									when ur.roleId= 1 then (select name from institute_details where userId=cn.senderId)
									when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId=cn.senderId)
									when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId=cn.senderId)
									when ur.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId=cn.senderId)
								END as senderName, pd.firstName, pd.lastName, pd.profilePic, ur.roleId 
								FROM `chat_notifications` AS cn
								INNER JOIN `chat_rooms` AS cr ON cr.id=cn.roomId
								INNER JOIN `subjects` AS s ON s.id=cr.subjectId
								INNER JOIN `courses` AS c ON c.id=cr.courseId
								INNER JOIN `login_details` AS ld ON ld.id=cn.senderId
								INNER JOIN `professor_details` AS pd ON pd.userId=cn.userId
								INNER JOIN `user_roles` AS ur on ur.userId=cn.senderId
								WHERE cn.userId IN ($siteProfessors) AND c.ownerId={$data->userId}
								ORDER BY timestamp DESC
								LIMIT {$pageStart},{$data->pageLimit}";
				$chatnotifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$chatnotifications = $chatnotifications->toArray();
			}
			//$result->notiCount = count($totalnotifications);
			foreach ($chatnotifications as $key => $notif) {
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$chatnotifications[$key]["profilePic"] = getSignedURL($chatnotifications[$key]["profilePic"]);
				} else {
					$chatnotifications[$key]["profilePic"] = 'assets/pages/media/users/avatar1.jpg';
				}
			}
			$result->notiCount = count($totalnotifications);
			$result->chatnotifications = $chatnotifications;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function markNotification($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			//for main course student graph
			$selectString = "UPDATE notifications set status=1  WHERE id=$data->notificationId";
			$notification = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			
			$result->message =" Notification marked read";
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function countNotification($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			//for main course student graph
			$selectString = "SELECT Count(id) from  notifications  WHERE userId={$data->userId} AND status=0";
			$notification = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->notification = $notification->toArray();
						$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function updatePortfolio($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$portfolioName = $data->portfolioName;
			$portfolioDesc = $data->portfolioDesc;
			$portfolioPic  = $data->portfolioPic;
			$favicon 	   = $data->inputFavicon;
			$facebook 	   = $data->inputFacebook;
			$twitter 	   = $data->inputTwitter;
			$linkedin	   = $data->inputLinkedin;
			$status		   = $data->selectStatus;
			$subdomain 	   = $data->slug;
			if(empty($portfolioName)) {
				$result->status = 0;
				$result->message = 'Portfolio Name is compulsory';
				return $result;
			} else if(empty($portfolioDesc)) {
				$result->status = 0;
				$result->message = 'Portfolio Description is compulsory';
				return $result;
			} else {
				$selectString ="SELECT `id`
								FROM  `pf_portfolio`
								WHERE `subdomain`='$subdomain' AND `userId`={$data->userId}";
				$subdomains = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subdomains = $subdomains->toArray();
				if(count($subdomains)>0) {
	                $subdomains = $subdomains[0];
	                $portfolioId  = $subdomains['id'];
					$where = array('id' => $portfolioId);
					$update = $sql->update();
					$update->table('pf_portfolio');
					$updatePortfolio = array('name' => $portfolioName,
										'description' => $portfolioDesc,
										'favicon'	  => $favicon,
										'facebook'	  => $facebook,
										'twitter'	  => $twitter,
										'linkedin'	  => $linkedin,
										'status'	  => $status
									);
					if (!empty($portfolioPic)) {
						$updatePortfolio['img'] = $portfolioPic;
					}
					$update->set($updatePortfolio);
					$update->where($where);
					$statement = $sql->prepareStatementForSqlObject($update);
					$count = $statement->execute()->getAffectedRows();
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
				} else {
					$db->rollBack();
					$result->status = 0;
					$result->message = 'Invalid access';
					return $result;
				}
			}
			$result->status = 1;
			$result->message = 'Portfolio successfully updated';
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function getPageTypes($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$selectString ="SELECT `id` AS `value`, `type`
							FROM  `pf_page_types`
							WHERE (`user`='general' OR `user`='institute')";
			$pageTypes = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$pageTypes = $pageTypes->toArray();
			$result->pageTypes = array();
			$i = 0;
			foreach ($pageTypes as $key => $value) {
				switch ($value['type']) {
					case 'courses':
						$selectString ="SELECT pf.id
										FROM  `pf_courses` AS pc
										LEFT JOIN `pf_portfolio` AS pf ON pf.id=pc.portfolioId
										WHERE pf.subdomain='{$data->slug}'";
						$course = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$course = $course->toArray();
						if (count($course) == 0) {
							$result->pageTypes[$i]['value']	= $value['value'];
							$result->pageTypes[$i]['type']	= $value['type'];
							$i++;
						}
						break;
					case 'about':
						$selectString ="SELECT pf.id
										FROM  `pf_about` AS pa
										LEFT JOIN `pf_portfolio` AS pf ON pf.id=pa.portfolioId
										WHERE pf.subdomain='{$data->slug}'";
						$about = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$about = $about->toArray();
						if (count($about) == 0) {
							$result->pageTypes[$i]['value']	= $value['value'];
							$result->pageTypes[$i]['type']	= $value['type'];
							$i++;
						}
						break;
					case 'faculty':
						$selectString ="SELECT pf.id
										FROM  `pf_faculty` AS pc
										LEFT JOIN `pf_portfolio` AS pf ON pf.id=pc.portfolioId
										WHERE pf.subdomain='{$data->slug}'";
						$faculty = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$faculty = $faculty->toArray();
						if (count($faculty) == 0) {
							$result->pageTypes[$i]['value']	= $value['value'];
							$result->pageTypes[$i]['type']	= $value['type'];
							$i++;
						}
						break;
					case 'gallery':
						$selectString ="SELECT pf.id
										FROM  `pf_gallery` AS pg
										LEFT JOIN `pf_portfolio` AS pf ON pf.id=pg.portfolioId
										WHERE pf.subdomain='{$data->slug}'";
						$gallery = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$gallery = $gallery->toArray();
						if (count($gallery) == 0) {
							$result->pageTypes[$i]['value']	= $value['value'];
							$result->pageTypes[$i]['type']	= $value['type'];
							$i++;
						}
						break;
					case 'contact':
						$selectString ="SELECT pf.id
										FROM  `pf_contact` AS pc
										LEFT JOIN `pf_portfolio` AS pf ON pf.id=pc.portfolioId
										WHERE pf.subdomain='{$data->slug}'";
						$contact = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$contact = $contact->toArray();
						if (count($contact) == 0) {
							$result->pageTypes[$i]['value']	= $value['value'];
							$result->pageTypes[$i]['type']	= $value['type'];
							$i++;
						}
						break;
					case 'custom':
						$result->pageTypes[$i]['value']	= $value['value'];
						$result->pageTypes[$i]['type']	= $value['type'];
						$i++;
						break;
					
					default:
						# code...
						break;
				}
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function getPortfolioPages($data) {
		try {
			$adapter = $this->adapter;
			$subdomain = $data->slug;
			if(isset($data->menu) && ($data->menu == 1)) {
				$sql = "SELECT pg.`portfolioId` AS portfolioId, pg.`id` AS pageId, pg.`pageTitle` AS `title`, ppt.`id` AS typeid, ppt.`type`, pg.`content`, pg.`status`, pm.`id`
						FROM `pf_pages` AS pg
						LEFT JOIN `pf_portfolio` AS pf ON pf.`id`=pg.`portfolioId`
						LEFT JOIN `pf_page_types` AS ppt ON ppt.`id`=pg.`pageType`
						LEFT JOIN `pf_menus` AS pm ON pm.`pageId`=pg.`id`
						WHERE pf.`subdomain`=? AND ISNULL(pm.`id`)";
			} else {
				$sql = "SELECT pg.`portfolioId` AS portfolioId, pg.`id` AS pageId, pg.`pageTitle` AS `title`, ppt.`id` AS typeid, ppt.`type`, pg.`content`, pg.`status`
						FROM `pf_pages` AS pg
						LEFT JOIN `pf_portfolio` AS pf ON pf.`id`=pg.`portfolioId`
						LEFT JOIN `pf_page_types` AS ppt ON ppt.`id`=pg.`pageType`
						WHERE pf.`subdomain`=?";
			}
			$portfolioPages = $adapter->query($sql, array($subdomain));
			$result = new stdClass();
			$result->status = 1;
			$result->valid = true;
			if ($portfolioPages->count() > 0) {
				$result->portfolioPages = $portfolioPages->toArray();
				foreach ($result->portfolioPages as $key => $page) {
					if($page['type'] == 'courses') {
						// Get courses page details
						$sql = "SELECT pf.`id` AS courseId, pf.`courses`
								FROM `pf_courses` AS pf
								WHERE pf.`portfolioId`=?";
						$pageDetail = $adapter->query($sql, array($page['portfolioId']));
						$pageArray 	= $pageDetail->toArray();
						$courses 	= $pageArray[0];
						$courses 	= explode(',', $courses['courses']);
						$result->portfolioPages[$key]['courses'] = $courses;
					} else if($page['type'] == 'about') {
						// Get about page details
						$sql = "SELECT pa.`id` AS aboutId, pa.`description`, pa.`academic_title`, pa.`education_title`, pa.`honors_title`
								FROM `pf_about` AS pa
								WHERE pa.`portfolioId`=?";
						$pageDetail = $adapter->query($sql, array($page['portfolioId']));
						$pageArray = $pageDetail->toArray();
						$aboutPage = $pageArray[0];

						// Get academic Positions
						$sql = "SELECT ap.`id`, ap.`fromyear`, ap.`toyear`, ap.`position`, ap.`university`, ap.`department`
								FROM `pf_academic_positions` AS ap
								WHERE ap.`aboutId`=?";
						$academicDetails = $adapter->query($sql, array($aboutPage['aboutId']));
						$academic = $academicDetails->toArray();
						foreach ($academic as $key1 => $ap) {
							if($ap['toyear'] == '0000') {
								$academic[$key1]['toyear'] = '';
							}
						}
						$aboutPage['academic'] = $academic;

						// Get education & training
						$sql = "SELECT pe.`id`, pe.`degree`, pe.`year`, pe.`title`, pe.`university`
								FROM `pf_education` AS pe
								WHERE pe.`aboutId`=?";
						$educationDetails = $adapter->query($sql, array($aboutPage['aboutId']));
						$education = $educationDetails->toArray();
						$aboutPage['education'] = $education;

						// Get honors details
						$sql = "SELECT ph.`id`, ph.`img` AS image, ph.`year`, ph.`title`, ph.`description`
								FROM `pf_honors` AS ph
								WHERE ph.`aboutId`=?";
						$honorsDetails = $adapter->query($sql, array($aboutPage['aboutId']));
						$honors = $honorsDetails->toArray();
						$aboutPage['honors'] = $honors;

						$result->portfolioPages[$key]['about'] = $aboutPage;
					} else if($page['type'] == 'faculty') {
						// Get faculty page details
						$sql = "SELECT pf.`id` AS facultyId, pf.`description`
								FROM `pf_faculty` AS pf
								WHERE pf.`portfolioId`=?";
						$pageDetail = $adapter->query($sql, array($page['portfolioId']));
						$pageArray = $pageDetail->toArray();
						$facultyPage = $pageArray[0];

						// Get faculty members
						$sql = "SELECT fm.`id`, fm.`img` AS image, fm.`name`, fm.`university`, fm.`department`
								FROM `pf_faculty_members` AS fm
								WHERE fm.`facultyId`=?";
						$membersDetails = $adapter->query($sql, array($facultyPage['facultyId']));
						$facultyMembers = $membersDetails->toArray();
						$facultyPage['facultyMembers'] = $facultyMembers;

						$result->portfolioPages[$key]['faculty'] = $facultyPage;
					} else if($page['type'] == 'gallery') {
						// Get gallery page details
						$sql = "SELECT pg.`id` AS galleryId, pg.`description`
								FROM `pf_gallery` AS pg
								WHERE pg.`portfolioId`=?";
						$pageDetail = $adapter->query($sql, array($page['portfolioId']));
						$pageArray = $pageDetail->toArray();
						$galleryPage = $pageArray[0];

						// Get gallery members
						$sql = "SELECT pg.`id`, pg.`img` AS image, pg.`title`, pg.`description`
								FROM `pf_gallery_detail` AS pg
								WHERE pg.`galleryId`=?";
						$imagesDetails = $adapter->query($sql, array($galleryPage['galleryId']));
						$galleryImages = $imagesDetails->toArray();
						$galleryPage['galleryImages'] = $galleryImages;

						$result->portfolioPages[$key]['gallery'] = $galleryPage;
					} else if($page['type'] == 'contact') {
						// Get faculty page details
						$sql = "SELECT pc.`id` AS contactId, pc.`contact_description`, pc.`office_description`, pc.`work_description`
								FROM `pf_contact` AS pc
								WHERE pc.`portfolioId`=?";
						$pageDetail = $adapter->query($sql, array($page['portfolioId']));
						$pageArray = $pageDetail->toArray();
						$contactPage = $pageArray[0];

						// Get contacts
						$sql = "SELECT pc.`id`, pc.`type`, pc.`title`, pc.`value`
								FROM `pf_contact_detail` AS pc
								WHERE pc.`contactId`=?";
						$contactDetails = $adapter->query($sql, array($contactPage['contactId']));
						$contactInfo = $contactDetails->toArray();
						$contactPage['contactInfo'] = $contactInfo;

						$result->portfolioPages[$key]['contact'] = $contactPage;
					}
				}
			} else {
				$result->portfolioPages = array();
			}
			return $result;
		} catch (Exception $e) {
			$result = new stdClass();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	/*public function getPortfolioMenus($data) {
		try {
			$adapter = $this->adapter;
			$subdomain = $data->slug;
			$sql = "SELECT pm.`id` AS menuId, pm.`portfolioId`, pm.`name`, pm.`description`, pm.`type`, pm.`pageId`, pg.`pageTitle`, pg.`content`, pg.`status` AS `pageStatus`, ppt.`type` AS pageType, pm.`url`, pm.`order`, pm.`status`
					FROM `pf_menus` AS pm
					LEFT JOIN `pf_portfolio` AS pf ON pf.`id`=pm.`portfolioId`
					LEFT JOIN `pf_pages` 	 AS pg ON pg.`id`=pm.`pageId`
					LEFT JOIN `pf_page_types` AS ppt ON ppt.`id`=pg.`pageType`
					WHERE pf.`subdomain`=? ORDER BY pm.`order`";
			$portfolioMenus = $adapter->query($sql, array($subdomain));
			$result = new stdClass();
			$result->status = 1;
			$result->valid = true;
			if ($portfolioMenus->count() > 0) {
				$result->portfolioMenus = $portfolioMenus->toArray();
			} else {
				$result->portfolioMenus = array();
			}
			return $result;
		} catch (Exception $e) {
			$result = new stdClass();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}*/
	public function savePortfolioPage($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$pageType = $data->pageType;
			$pageTitle = $data->pageTitle;
			$subdomain 	   = $data->slug;
			$pageId = 0;
			if (isset($data->pageId) && !empty($data->pageId)) {
				$pageId = $data->pageId;
			}
			if(empty($pageType)) {
				$result->status = 0;
				$result->message = 'Please select Page Type';
				return $result;
			} else if(empty($pageTitle)) {
				$result->status = 0;
				$result->message = 'Page Title can\'t be left empty';
				return $result;
			} else if($pageType == '8' && empty($data->pageContent)) {
				$result->status = 0;
				$result->message = 'Page Content can\'t be left empty';
				return $result;
			} else {
				$selectString ="SELECT `id`
								FROM  `pf_portfolio`
								WHERE `subdomain`='$subdomain' AND `userId`={$data->userId}";
				$subdomains = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subdomains = $subdomains->toArray();
				if(count($subdomains)>0) {
					$subdomains = $subdomains[0];
					$portfolioId  = $subdomains['id'];
					if (empty($pageId)) {
						$portfolios = new TableGateway('pf_pages', $this->adapter, null, new HydratingResultSet());
						$result->portfolios = array();
						$insertPage = array(
							'portfolioId' => $portfolioId,
							'pageType' => $data->pageType,
							'pageTitle' => $pageTitle,
							'content' => '',
							'status' => 'active'
						);
						if ($pageType == '1') {
							$courses = new TableGateway('pf_courses', $this->adapter, null, new HydratingResultSet());
							$result->courses = array();
							$insertCourses = array(
								'portfolioId' => $portfolioId,
								'courses' => $data->inputCourses,
								'status' => 'active'
							);
							$courses->insert($insertCourses);
						} elseif ($pageType == '2') {
							//$about = new TableGateway('pf_about', $this->adapter, null, new HydratingResultSet());
							$result->about = array();
							$insertAbout = array(
								'portfolioId' => $portfolioId,
								'description' => $data->inputAboutDescription,
								'academic_title' => $data->inputAcademicTitle,
								'education_title' => $data->inputEducationTitle,
								'honors_title' => $data->inputHonorsTitle,
								'status' => 'active'
							);
							//$about->insert($insertAbout);
							//$aboutId = $about->getLastInsertValue();
							$query="INSERT INTO pf_about (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
							$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$adapter->query("SET @aboutId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
							//$aboutAcademicPositions = new TableGateway('pf_academic_positions', $this->adapter, null, new HydratingResultSet());
							$insertAcademicPositions = array();
							foreach ($data->aboutAcademic as $key => $value) {
								$insertAcademicPositions = array(
									'aboutId' => $aboutId,
									'fromyear' => $value->fromyear,
									'toyear' => ((empty($value->toyear))?'NULL':$value->toyear),
									'position' => $value->position,
									'university' => $value->university,
									'department' => $value->department,
									'order' => 0
								);
								//$aboutAcademicPositions->insert($insertAcademicPositions);
								$query="INSERT INTO pf_academic_positions (`".implode("`,`", array_keys($insert))."`,`aboutId`) VALUES ('".implode("','", array_values($insert))."',@aboutId)";
								$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							}
							//$aboutEducation = new TableGateway('pf_education', $this->adapter, null, new HydratingResultSet());
							$insertEducation = array();
							foreach ($data->aboutEducation as $key => $value) {
								$insertEducation = array(
									'aboutId' => $aboutId,
									'degree' => $value->degree,
									'year' => $value->year,
									'title' => $value->title,
									'university' => $value->university,
									'order' => 0
								);
								//$aboutEducation->insert($insertEducation);
								$query="INSERT INTO pf_education (`".implode("`,`", array_keys($insert))."`,`aboutId`) VALUES ('".implode("','", array_values($insert))."',@aboutId)";
								$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							}
							//$aboutHonors = new TableGateway('pf_honors', $this->adapter, null, new HydratingResultSet());
							$insertHonors = array();
							foreach ($data->aboutHonors as $key => $value) {
								$insertHonors = array(
									'aboutId' => $aboutId,
									'img' => $value->image,
									'year' => $value->year,
									'title' => $value->title,
									'description' => $value->description,
									'order' => 0
								);
								//$aboutHonors->insert($insertHonors);
								$query="INSERT INTO pf_honors (`".implode("`,`", array_keys($insert))."`,`aboutId`) VALUES ('".implode("','", array_values($insert))."',@aboutId)";
								$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							}

							// fetch about page
							// Get about page details
							$query = "SELECT pa.`id` AS aboutId, pa.`description`, pa.`academic_title`, pa.`education_title`, pa.`honors_title`
									FROM `pf_about` AS pa
									WHERE pa.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$aboutPage = $pageArray[0];

							// Get academic Positions
							$query = "SELECT ap.`id`, ap.`fromyear`, ap.`toyear`, ap.`position`, ap.`university`, ap.`department`
									FROM `pf_academic_positions` AS ap
									WHERE ap.`aboutId`=?";
							$academicDetails = $adapter->query($query, array($aboutPage['aboutId']));
							$academic = $academicDetails->toArray();
							foreach ($academic as $key1 => $ap) {
								if($ap['toyear'] == '0000') {
									$academic[$key1]['toyear'] = '';
								}
							}
							$aboutPage['academic'] = $academic;

							// Get education & training
							$query = "SELECT pe.`id`, pe.`degree`, pe.`year`, pe.`title`, pe.`university`
									FROM `pf_education` AS pe
									WHERE pe.`aboutId`=?";
							$educationDetails = $adapter->query($query, array($aboutPage['aboutId']));
							$education = $educationDetails->toArray();
							$aboutPage['education'] = $education;

							// Get honors details
							$query = "SELECT ph.`id`, ph.`img` AS image, ph.`year`, ph.`title`, ph.`description`
									FROM `pf_honors` AS ph
									WHERE ph.`aboutId`=?";
							$honorsDetails = $adapter->query($query, array($aboutPage['aboutId']));
							$honors = $honorsDetails->toArray();
							$aboutPage['honors'] = $honors;

							$result->about = $aboutPage;
							$result->title = $pageTitle;
						} elseif ($pageType == '3') {
							//$faculty = new TableGateway('pf_faculty', $this->adapter, null, new HydratingResultSet());
							$result->faculty = array();
							$insertFaculty = array(
								'portfolioId' => $portfolioId,
								'description' => $data->inputFacultyDescription,
								'status' => 'active'
							);
							//$faculty->insert($insertFaculty);
							//$facultyId = $faculty->getLastInsertValue();
							$query="INSERT INTO pf_faculty (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
							$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$adapter->query("SET @facultyId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
							//$facultyMembers = new TableGateway('pf_faculty_members', $this->adapter, null, new HydratingResultSet());
							$insertFacultyMem = array();
							foreach ($data->facultyMembers as $key => $value) {
								$insertFacultyMem = array(
									//'facultyId' => $facultyId,
									'img' => $value->image,
									'name' => $value->name,
									'university' => $value->university,
									'department' => $value->department,
									'order' => 0
								);
								//$facultyMembers->insert($insertFacultyMem);
								$query="INSERT INTO pf_faculty_members (`".implode("`,`", array_keys($insert))."`,`facultyId`) VALUES ('".implode("','", array_values($insert))."',@facultyId)";
								$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							}

							// fetch faculty page
							// Get faculty page details
							$query = "SELECT pf.`id` AS facultyId, pf.`description`
									FROM `pf_faculty` AS pf
									WHERE pf.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$facultyPage = $pageArray[0];

							// Get faculty members
							$query = "SELECT fm.`id`, fm.`img` AS image, fm.`name`, fm.`university`, fm.`department`
									FROM `pf_faculty_members` AS fm
									WHERE fm.`facultyId`=?";
							$membersDetails = $adapter->query($query, array($facultyPage['facultyId']));
							$facultyMembers = $membersDetails->toArray();
							$facultyPage['facultyMembers'] = $facultyMembers;

							$result->faculty = $facultyPage;
							$result->title	 = $pageTitle;
						} elseif ($pageType == '6') {
							//$gallery = new TableGateway('pf_gallery', $this->adapter, null, new HydratingResultSet());
							$result->gallery = array();
							$insertGallery = array(
								'portfolioId' => $portfolioId,
								'description' => $data->inputGalleryDescription,
								'status' => 'active'
							);
							//$gallery->insert($insertGallery);
							//$galleryId = $gallery->getLastInsertValue();
							$query="INSERT INTO pf_gallery (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
							$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$adapter->query("SET @galleryId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
							//$galleryImages = new TableGateway('pf_gallery_detail', $this->adapter, null, new HydratingResultSet());
							$insertGalleryImages = array();
							foreach ($data->galleryImages as $key => $value) {
								$insertGalleryImages = array(
									'galleryId' => $galleryId,
									'img' => $value->image,
									'title' => $value->title,
									'description' => $value->description,
									'order' => 0
								);
								//$galleryImages->insert($insertGalleryImages);
								$query="INSERT INTO pf_gallery_detail (`".implode("`,`", array_keys($insert))."`,`galleryId`) VALUES ('".implode("','", array_values($insert))."',@galleryId)";
								$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							}

							// fetch gallery page
							// Get gallery page details
							$query = "SELECT pg.`id` AS galleryId, pg.`description`
									FROM `pf_gallery` AS pg
									WHERE pg.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$galleryPage = $pageArray[0];

							// Get gallery members
							$query = "SELECT pg.`id`, pg.`img` AS image, pg.`title`, pg.`description`
									FROM `pf_gallery_detail` AS pg
									WHERE pg.`galleryId`=?";
							$imagesDetails = $adapter->query($query, array($galleryPage['galleryId']));
							$galleryImages = $imagesDetails->toArray();
							$galleryPage['galleryImages'] = $galleryImages;

							$result->gallery = $galleryPage;
							$result->title	 = $pageTitle;
						} elseif ($pageType == '7') {
							//$contact = new TableGateway('pf_contact', $this->adapter, null, new HydratingResultSet());
							$result->contact = array();
							$insertContact = array(
								'portfolioId' => $portfolioId,
								'contact_description' => $data->inputContactDescription,
								'office_description' => $data->inputOfficeDescription,
								'work_description' => $data->inputWorkDescription,
								'lab_description'  => '',
								'status' => 'active'
							);
							//$contact->insert($insertContact);
							//$contactId = $contact->getLastInsertValue();
							$query="INSERT INTO pf_contact (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
							$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							$adapter->query("SET @contactId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
							//$contactInfo = new TableGateway('pf_contact_detail', $this->adapter, null, new HydratingResultSet());
							$insertContactInfo = array();
							foreach ($data->contactInfo as $key => $value) {
								$insertContactInfo = array(
									'contactId' => $contactId,
									'type' => $value->type,
									'title' => $value->title,
									'value' => $value->value,
									'order' => 0
								);
								//$contactInfo->insert($insertContactInfo);
								$query="INSERT INTO pf_contact_detail (`".implode("`,`", array_keys($insert))."`,`contactId`) VALUES ('".implode("','", array_values($insert))."',@contactId)";
								$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							}
							
							// Fetch contact page
							// Get contact page details
							$query = "SELECT pc.`id` AS contactId, pc.`contact_description`, pc.`office_description`, pc.`work_description`
									FROM `pf_contact` AS pc
									WHERE pc.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$contactPage = $pageArray[0];

							// Get contacts
							$query = "SELECT pc.`id`, pc.`type`, pc.`title`, pc.`value`
									FROM `pf_contact_detail` AS pc
									WHERE pc.`contactId`=?";
							$contactDetails = $adapter->query($query, array($contactPage['contactId']));
							$contactInfo = $contactDetails->toArray();
							$contactPage['contactInfo'] = $contactInfo;

							$result->contact = $contactPage;
							$result->title	 = $pageTitle;
						} elseif ($pageType == '8') {
							$insertPage['content'] = $data->pageContent;
						}
						$portfolios->insert($insertPage);
						$result->status = 1;
						$result->message = 'Portfolio page added';
					} else {
						$where = array('portfolioId' => $portfolioId,'id' => $pageId);
						$update = $sql->update();
						$update->table('pf_pages');
						$updatePage = array('pageType' => $pageType, 'pageTitle' => $pageTitle);
						if ($pageType == '1') {
							$where1 = array('portfolioId' => $portfolioId);
							$update1 = $sql->update();
							$update1->table('pf_courses');
							$updatePage1 = array('courses' => $data->inputCourses);
							$update1->set($updatePage1);
							$update1->where($where1);
							$statement1 = $sql->prepareStatementForSqlObject($update1);
							$count = $statement1->execute()->getAffectedRows();
						} else if ($pageType == '2') {
							$where1 = array('portfolioId' => $portfolioId);
							$update1 = $sql->update();
							$update1->table('pf_about');
							$updatePage1 = array('description' => $data->inputAboutDescription,
													'academic_title' => $data->inputAcademicTitle,
													'education_title' => $data->inputEducationTitle,
													'honors_title' => $data->inputHonorsTitle);
							$update1->set($updatePage1);
							$update1->where($where1);
							$statement1 = $sql->prepareStatementForSqlObject($update1);
							$count = $statement1->execute()->getAffectedRows();
							$selectString ="SELECT `id`
											FROM  `pf_about`
											WHERE `portfolioId`='$portfolioId'";
							$aboutDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$aboutDetails = $aboutDetails->toArray();
							$aboutId 	  = $aboutDetails[0]['id'];

							// manage academinc positions
							$aboutAcademic= $data->aboutAcademic;
							$deleteIds	  = '';
							foreach ($aboutAcademic as $key => $value) {
								//var_dump($value);
								$pre =  substr($value->id, 0, 1);
								if($pre != 'n') {
									// update academinc positions
									$deleteIds.= $value->id.",";
									$where1 = array('id' => $value->id);
									$update1 = $sql->update();
									$update1->table('pf_academic_positions');
									$updatePage1 = array('fromyear' => $value->fromyear,
															'toyear' => ((empty($value->toyear))?'NULL':$value->toyear),
															'position' => $value->position,
															'university' => $value->university,
															'department' => $value->department);
									$update1->set($updatePage1);
									$update1->where($where1);
									$statement1 = $sql->prepareStatementForSqlObject($update1);
									$count = $statement1->execute()->getAffectedRows();
								} else {
									// insert new academinc positions
									$aboutSubTable = new TableGateway('pf_academic_positions', $this->adapter, null, new HydratingResultSet());
									$insertSubTableData = array(
										'aboutId' => $aboutId,
										'fromyear' => $value->fromyear,
										'toyear' => ((empty($value->toyear))?'NULL':$value->toyear),
										'position' => $value->position,
										'university' => $value->university,
										'department' => $value->department
									);
									$aboutSubTable->insert($insertSubTableData);
									$subid = $aboutSubTable->getLastInsertValue();
									$deleteIds.= $subid.",";
								}
							}
							// delete removed academinc positions
							$deleteIds = substr($deleteIds, 0, -1);
							$sql = new Sql($adapter);
							$delete = $sql->delete();
							$delete->from('pf_academic_positions');
							if (!empty($deleteIds)) {
								$delete->where("aboutId = ".$aboutId." AND id NOT IN ($deleteIds)");
							} else {
								$delete->where("aboutId = ".$aboutId);
							}
							$statement = $sql->prepareStatementForSqlObject($delete);
							$delAcademicPositions = $statement->execute();

							// manage education & training
							$aboutEducation= $data->aboutEducation;
							$deleteIds	  = '';
							foreach ($aboutEducation as $key => $value) {
								//var_dump($value);
								$pre =  substr($value->id, 0, 1);
								if($pre != 'n') {
									// update education & training
									$deleteIds.= $value->id.",";
									$where1 = array('id' => $value->id);
									$update1 = $sql->update();
									$update1->table('pf_education');
									$updatePage1 = array('degree' => $value->degree,
															'year' => $value->year,
															'title' => $value->title,
															'university' => $value->university);
									$update1->set($updatePage1);
									$update1->where($where1);
									$statement1 = $sql->prepareStatementForSqlObject($update1);
									$count = $statement1->execute()->getAffectedRows();
								} else {
									// insert new education & training
									$aboutSubTable = new TableGateway('pf_education', $this->adapter, null, new HydratingResultSet());
									$insertSubTableData = array(
										'aboutId' => $aboutId,
										'degree' => $value->degree,
										'year' => $value->year,
										'title' => $value->title,
										'university' => $value->university
									);
									$aboutSubTable->insert($insertSubTableData);
									$subid = $aboutSubTable->getLastInsertValue();
									$deleteIds.= $subid.",";
								}
							}
							// delete removed education & training
							$deleteIds = substr($deleteIds, 0, -1);
							$sql = new Sql($adapter);
							$delete = $sql->delete();
							$delete->from('pf_education');
							if (!empty($deleteIds)) {
								$delete->where("aboutId = ".$aboutId." AND id NOT IN ($deleteIds)");
							} else {
								$delete->where("aboutId = ".$aboutId);
							}
							$statement = $sql->prepareStatementForSqlObject($delete);
							$delEducation = $statement->execute();

							// manage honors
							$aboutHonors= $data->aboutHonors;
							$deleteIds	  = '';
							foreach ($aboutHonors as $key => $value) {
								//var_dump($value);
								$pre =  substr($value->id, 0, 1);
								if($pre != 'n') {
									// update honors
									$deleteIds.= $value->id.",";
									$where1 = array('id' => $value->id);
									$update1 = $sql->update();
									$update1->table('pf_honors');
									$updatePage1 = array('img' => $value->image,
															'year' => $value->year,
															'title' => $value->title,
															'description' => $value->description);
									$update1->set($updatePage1);
									$update1->where($where1);
									$statement1 = $sql->prepareStatementForSqlObject($update1);
									$count = $statement1->execute()->getAffectedRows();
								} else {
									// insert new honors
									$aboutSubTable = new TableGateway('pf_honors', $this->adapter, null, new HydratingResultSet());
									$insertSubTableData = array(
										'aboutId' => $aboutId,
										'img' => $value->image,
										'year' => $value->year,
										'title' => $value->title,
										'description' => $value->description
									);
									$aboutSubTable->insert($insertSubTableData);
									$subid = $aboutSubTable->getLastInsertValue();
									$deleteIds.= $subid.",";
								}
							}
							// delete removed honors
							$deleteIds = substr($deleteIds, 0, -1);
							$sql = new Sql($adapter);
							$delete = $sql->delete();
							$delete->from('pf_honors');
							if (!empty($deleteIds)) {
								$delete->where("aboutId = ".$aboutId." AND id NOT IN ($deleteIds)");
							} else {
								$delete->where("aboutId = ".$aboutId);
							}
							$statement = $sql->prepareStatementForSqlObject($delete);
							$delHonors = $statement->execute();

							// fetch about page
							// Get about page details
							$query = "SELECT pa.`id` AS aboutId, pa.`description`, pa.`academic_title`, pa.`education_title`, pa.`honors_title`
									FROM `pf_about` AS pa
									WHERE pa.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$aboutPage = $pageArray[0];

							// Get academic Positions
							$query = "SELECT ap.`id`, ap.`fromyear`, ap.`toyear`, ap.`position`, ap.`university`, ap.`department`
									FROM `pf_academic_positions` AS ap
									WHERE ap.`aboutId`=?";
							$academicDetails = $adapter->query($query, array($aboutPage['aboutId']));
							$academic = $academicDetails->toArray();
							foreach ($academic as $key1 => $ap) {
								if($ap['toyear'] == '0000') {
									$academic[$key1]['toyear'] = '';
								}
							}
							$aboutPage['academic'] = $academic;

							// Get education & training
							$query = "SELECT pe.`id`, pe.`degree`, pe.`year`, pe.`title`, pe.`university`
									FROM `pf_education` AS pe
									WHERE pe.`aboutId`=?";
							$educationDetails = $adapter->query($query, array($aboutPage['aboutId']));
							$education = $educationDetails->toArray();
							$aboutPage['education'] = $education;

							// Get honors details
							$query = "SELECT ph.`id`, ph.`img` AS image, ph.`year`, ph.`title`, ph.`description`
									FROM `pf_honors` AS ph
									WHERE ph.`aboutId`=?";
							$honorsDetails = $adapter->query($query, array($aboutPage['aboutId']));
							$honors = $honorsDetails->toArray();
							$aboutPage['honors'] = $honors;

							$result->about = $aboutPage;
							$result->title = $pageTitle;
						} else if ($pageType == '3') {
							$where1 = array('portfolioId' => $portfolioId);
							$update1 = $sql->update();
							$update1->table('pf_faculty');
							$updatePage1 = array('description' => $data->inputFacultyDescription);
							$update1->set($updatePage1);
							$update1->where($where1);
							$statement1 = $sql->prepareStatementForSqlObject($update1);
							$count = $statement1->execute()->getAffectedRows();
							$selectString ="SELECT `id`
											FROM  `pf_faculty`
											WHERE `portfolioId`='$portfolioId'";
							$facultyDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$facultyDetails = $facultyDetails->toArray();
							$facultyId = $facultyDetails[0]['id'];

							// manage faculty members
							$facultyMembers= $data->facultyMembers;
							$deleteIds	  = '';
							foreach ($facultyMembers as $key => $value) {
								//var_dump($value);
								$pre =  substr($value->id, 0, 1);
								if($pre != 'n') {
									// update faculty members
									$deleteIds.= $value->id.",";
									$where1 = array('id' => $value->id);
									$update1 = $sql->update();
									$update1->table('pf_faculty_members');
									$updatePage1 = array('img' => $value->image,
														'name' => $value->name,
														'university' => $value->university,
														'department' => $value->department);
									$update1->set($updatePage1);
									$update1->where($where1);
									$statement1 = $sql->prepareStatementForSqlObject($update1);
									$count = $statement1->execute()->getAffectedRows();
								} else {
									// insert new faculty members
									$facultyMembersTable = new TableGateway('pf_faculty_members', $this->adapter, null, new HydratingResultSet());
									$insertFacultyMembersTableData = array(
										'facultyId' => $facultyId,
										'img' => $value->image,
										'name' => $value->name,
										'university' => $value->university,
										'department' => $value->department
									);
									$facultyMembersTable->insert($insertFacultyMembersTableData);
									$fmid = $facultyMembersTable->getLastInsertValue();
									$deleteIds.= $fmid.",";
								}
							}
							// delete removed faculty members
							$deleteIds = substr($deleteIds, 0, -1);
							$sql = new Sql($adapter);
							$delete = $sql->delete();
							$delete->from('pf_faculty_members');
							if (!empty($deleteIds)) {
								$delete->where("facultyId = ".$facultyId." AND id NOT IN ($deleteIds)");
							} else {
								$delete->where("facultyId = ".$facultyId);
							}
							$statement = $sql->prepareStatementForSqlObject($delete);
							$delFacultyMembers = $statement->execute();

							// fetch faculty page
							// Get faculty page details
							$query = "SELECT pf.`id` AS facultyId, pf.`description`
									FROM `pf_faculty` AS pf
									WHERE pf.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$facultyPage = $pageArray[0];

							// Get faculty members
							$query = "SELECT fm.`id`, fm.`img` AS image, fm.`name`, fm.`university`, fm.`department`
									FROM `pf_faculty_members` AS fm
									WHERE fm.`facultyId`=?";
							$membersDetails = $adapter->query($query, array($facultyPage['facultyId']));
							$facultyMembers = $membersDetails->toArray();
							$facultyPage['facultyMembers'] = $facultyMembers;

							$result->faculty = $facultyPage;
							$result->title	 = $pageTitle;
						} else if ($pageType == '6') {
							$where1 = array('portfolioId' => $portfolioId);
							$update1 = $sql->update();
							$update1->table('pf_gallery');
							$updatePage1 = array('description' => $data->inputGalleryDescription);
							$update1->set($updatePage1);
							$update1->where($where1);
							$statement1 = $sql->prepareStatementForSqlObject($update1);
							$count = $statement1->execute()->getAffectedRows();
							$selectString ="SELECT `id`
											FROM  `pf_gallery`
											WHERE `portfolioId`='$portfolioId'";
							$galleryDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$galleryDetails = $galleryDetails->toArray();
							$galleryId = $galleryDetails[0]['id'];

							// manage faculty members
							$galleryImages= $data->galleryImages;
							$deleteIds	  = '';
							foreach ($galleryImages as $key => $value) {
								//var_dump($value);
								$pre =  substr($value->id, 0, 1);
								if($pre != 'n') {
									// update gallery images
									$deleteIds.= $value->id.",";
									$where1 = array('id' => $value->id);
									$update1 = $sql->update();
									$update1->table('pf_gallery_detail');
									$updatePage1 = array('img' => $value->image,
														'title' => $value->title,
														'description' => $value->description);
									$update1->set($updatePage1);
									$update1->where($where1);
									$statement1 = $sql->prepareStatementForSqlObject($update1);
									$count = $statement1->execute()->getAffectedRows();
								} else {
									// insert new gallery images
									$galleryImagesTable = new TableGateway('pf_gallery_detail', $this->adapter, null, new HydratingResultSet());
									$insertGalleryImagesTableData = array(
										'galleryId' => $galleryId,
										'img' => $value->image,
										'title' => $value->title,
										'description' => $value->description
									);
									$galleryImagesTable->insert($insertGalleryImagesTableData);
									$giid = $galleryImagesTable->getLastInsertValue();
									$deleteIds.= $giid.",";
								}
							}
							// delete removed gallery images
							$deleteIds = substr($deleteIds, 0, -1);
							$sql = new Sql($adapter);
							$delete = $sql->delete();
							$delete->from('pf_gallery_detail');
							if (!empty($deleteIds)) {
								$delete->where("galleryId = ".$galleryId." AND id NOT IN ($deleteIds)");
							} else {
								$delete->where("galleryId = ".$galleryId);
							}
							$delete->where("id NOT IN ($deleteIds)");
							$statement = $sql->prepareStatementForSqlObject($delete);
							$delGalleryImages = $statement->execute();

							// fetch gallery page
							// Get gallery page details
							$query = "SELECT pg.`id` AS galleryId, pg.`description`
									FROM `pf_gallery` AS pg
									WHERE pg.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$galleryPage = $pageArray[0];

							// Get gallery members
							$query = "SELECT pg.`id`, pg.`img` AS image, pg.`title`, pg.`description`
									FROM `pf_gallery_detail` AS pg
									WHERE pg.`galleryId`=?";
							$imagesDetails = $adapter->query($query, array($galleryPage['galleryId']));
							$galleryImages = $imagesDetails->toArray();
							$galleryPage['galleryImages'] = $galleryImages;

							$result->gallery = $galleryPage;
							$result->title	 = $pageTitle;
						} else if ($pageType == '7') {
							$where1 = array('portfolioId' => $portfolioId);
							$update1 = $sql->update();
							$update1->table('pf_contact');
							$updatePage1 = array('contact_description' => $data->inputContactDescription,
													'office_description' => $data->inputOfficeDescription,
													'work_description' => $data->inputWorkDescription);
							$update1->set($updatePage1);
							$update1->where($where1);
							$statement1 = $sql->prepareStatementForSqlObject($update1);
							$count = $statement1->execute()->getAffectedRows();
							$selectString ="SELECT `id`
											FROM  `pf_contact`
											WHERE `portfolioId`='$portfolioId'";
							$contactDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							$contactDetails = $contactDetails->toArray();
							$contactId = $contactDetails[0]['id'];

							// manage contacts
							$contactInfo= $data->contactInfo;
							$deleteIds	  = '';
							foreach ($contactInfo as $key => $value) {
								//var_dump($value);
								$pre =  substr($value->id, 0, 1);
								if($pre != 'n') {
									// update faculty members
									$deleteIds.= $value->id.",";
									$where1 = array('id' => $value->id);
									$update1 = $sql->update();
									$update1->table('pf_contact_detail');
									$updatePage1 = array('type' => $value->type,
														'title' => $value->title,
														'value' => $value->value);
									$update1->set($updatePage1);
									$update1->where($where1);
									$statement1 = $sql->prepareStatementForSqlObject($update1);
									$count = $statement1->execute()->getAffectedRows();
								} else {
									// insert new faculty members
									$contactInfoTable = new TableGateway('pf_contact_detail', $this->adapter, null, new HydratingResultSet());
									$insertContactInfoTableData = array(
										'contactId' => $contactId,
										'type' => $value->type,
										'title' => $value->title,
										'value' => $value->value
									);
									$contactInfoTable->insert($insertContactInfoTableData);
									$ciid = $contactInfoTable->getLastInsertValue();
									$deleteIds.= $ciid.",";
								}
							}
							// delete removed faculty members
							$deleteIds = substr($deleteIds, 0, -1);
							if (!empty($deleteIds)) {
								$sql = new Sql($adapter);
								$delete = $sql->delete();
								$delete->from('pf_contact_detail');
								$delete->where("id NOT IN ($deleteIds)");
								$statement = $sql->prepareStatementForSqlObject($delete);
								$delContactInfo = $statement->execute();
							}

							// Fetch contact page
							// Get contact page details
							$query = "SELECT pc.`id` AS contactId, pc.`contact_description`, pc.`office_description`, pc.`work_description`
									FROM `pf_contact` AS pc
									WHERE pc.`portfolioId`=?";
							$pageDetail = $adapter->query($query, array($portfolioId));
							$pageArray = $pageDetail->toArray();
							$contactPage = $pageArray[0];

							// Get contacts
							$query = "SELECT pc.`id`, pc.`type`, pc.`title`, pc.`value`
									FROM `pf_contact_detail` AS pc
									WHERE pc.`contactId`=?";
							$contactDetails = $adapter->query($query, array($contactPage['contactId']));
							$contactInfo = $contactDetails->toArray();
							$contactPage['contactInfo'] = $contactInfo;

							$result->contact = $contactPage;
							$result->title	 = $pageTitle;
						} else if ($pageType == '8') {
							$updatePage['content'] = $data->pageContent;
						}
						$update->set($updatePage);
						$update->where($where);
						$statement = $sql->prepareStatementForSqlObject($update);
						$count = $statement->execute()->getAffectedRows();
						$result->status = 1;
						$result->message = 'Portfolio page updated';
					}
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
				} else {
					$db->rollBack();
					$result->status = 0;
					$result->message = 'Invalid access';
				}
			}
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
    public function getPortfolioDetail($data) {
        try {
			$adapter = $this->adapter;
			$portfolioMenus = $this->getPortfolioMenus($data);
			$portfolioMenus = $portfolioMenus->portfolioMenus;
			$result->portfolioPages = array();
			$i=0;
			foreach ($portfolioMenus as $key => $value) {
				if($value['status'] == 'active') {
					$result->portfolioPages[$i] = $value;
					if($value['type'] == 'page') {
						$portfolioId = $value['portfolioId'];
						switch ($value['pageType']) {
							case 'courses':
								// Get courses page details
								$sql = "SELECT pf.`id` AS courseId, pf.`courses`
										FROM `pf_courses` AS pf
										WHERE pf.`portfolioId`=?";
								$pageDetail = $adapter->query($sql, array($portfolioId));
								$pageArray = $pageDetail->toArray();
								$courseIds = $pageArray[0];
								$courseIds = $courseIds['courses'];
								$query = "SELECT c.id,c.name,c.image,c.slug,c.studentPrice,c.studentPriceINR, COUNT(DISTINCT user_id) as studentCount FROM courses c LEFT OUTER JOIN student_course sc ON sc.course_id=c.id WHERE c.id IN ($courseIds) AND deleted=0 AND approved=1 AND availStudentMarket=1 AND liveForStudent=1 GROUP BY c.id order by liveDate Desc LIMIT 28";
								$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$courses = $courses->toArray();
								$req1 = new stdClass();
								require_once 'Student.php';
								$s = new Student();
								require_once 'Course.php';
								$c = new Course();
								foreach ($courses as $key => $course) {
									if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
										require_once 'amazonRead.php';
										$courses[$key]['image'] = getSignedURL($course['image']);
									}
									$courses[$key]['rating'] 	= $s->getCourseRatings($course['id']);
									$req1->courseId 			= $course['id'];
									$courses[$key]['discount']	= $c->getCourseDiscount($req1);
									$courses[$key]['owner'] 	= $c->getCourseOwner($course['id']);
								}
								$result->portfolioPages[$i]['courses'] = $courses;
								break;
							case 'about':
								// Get about page details
								$sql = "SELECT pa.`id` AS aboutId, pa.`description`, pa.`academic_title`, pa.`education_title`, pa.`honors_title`
										FROM `pf_about` AS pa
										WHERE pa.`portfolioId`=?";
								$pageDetail = $adapter->query($sql, array($portfolioId));
								$pageArray = $pageDetail->toArray();
								$aboutPage = $pageArray[0];

								// Get academic Positions
								$sql = "SELECT ap.`id`, ap.`fromyear`, ap.`toyear`, ap.`position`, ap.`university`, ap.`department`
										FROM `pf_academic_positions` AS ap
										WHERE ap.`aboutId`=?";
								$academicDetails = $adapter->query($sql, array($aboutPage['aboutId']));
								$academic = $academicDetails->toArray();
								$aboutPage['academic'] = $academic;

								// Get education & training
								$sql = "SELECT pe.`id`, pe.`degree`, pe.`year`, pe.`title`, pe.`university`
										FROM `pf_education` AS pe
										WHERE pe.`aboutId`=?";
								$educationDetails = $adapter->query($sql, array($aboutPage['aboutId']));
								$education = $educationDetails->toArray();
								$aboutPage['education'] = $education;

								// Get honors details
								$sql = "SELECT ph.`id`, ph.`img` AS image, ph.`year`, ph.`title`, ph.`description`
										FROM `pf_honors` AS ph
										WHERE ph.`aboutId`=?";
								$honorsDetails = $adapter->query($sql, array($aboutPage['aboutId']));
								$honors = $honorsDetails->toArray();
								$aboutPage['honors'] = $honors;

								$result->portfolioPages[$i]['about'] = $aboutPage;
								break;
							case 'faculty':
								// Get faculty page details
								$sql = "SELECT pf.`id` AS facultyId, pf.`description`
										FROM `pf_faculty` AS pf
										WHERE pf.`portfolioId`=?";
								$pageDetail = $adapter->query($sql, array($portfolioId));
								$pageArray = $pageDetail->toArray();
								$facultyPage = $pageArray[0];

								// Get faculty members
								$sql = "SELECT fm.`id`, fm.`img` AS image, fm.`name`, fm.`university`, fm.`department`
										FROM `pf_faculty_members` AS fm
										WHERE fm.`facultyId`=?";
								$membersDetails = $adapter->query($sql, array($facultyPage['facultyId']));
								$facultyMembers = $membersDetails->toArray();
								$facultyPage['facultyMembers'] = $facultyMembers;

								$result->portfolioPages[$i]['faculty'] = $facultyPage;
								break;
							case 'gallery':
								// Get faculty page details
								$sql = "SELECT pg.`id` AS galleryId, pg.`description`
										FROM `pf_gallery` AS pg
										WHERE pg.`portfolioId`=?";
								$pageDetail = $adapter->query($sql, array($portfolioId));
								$pageArray = $pageDetail->toArray();
								$galleryPage = $pageArray[0];

								// Get faculty members
								$sql = "SELECT pg.`id`, pg.`img` AS image, pg.`title`, pg.`description`
										FROM `pf_gallery_detail` AS pg
										WHERE pg.`galleryId`=?";
								$imagesDetails = $adapter->query($sql, array($galleryPage['galleryId']));
								$galleryImages = $imagesDetails->toArray();
								$galleryPage['galleryImages'] = $galleryImages;

								$result->portfolioPages[$i]['gallery'] = $galleryPage;
								break;
							case 'contact':
								// Get faculty page details
								$sql = "SELECT pc.`id` AS contactId, pc.`contact_description`, pc.`office_description`, pc.`work_description`
										FROM `pf_contact` AS pc
										WHERE pc.`portfolioId`=?";
								$pageDetail = $adapter->query($sql, array($portfolioId));
								$pageArray = $pageDetail->toArray();
								$contactPage = $pageArray[0];

								// Get contacts
								$sql = "SELECT pc.`id`, pc.`type`, pc.`title`, pc.`value`
										FROM `pf_contact_detail` AS pc
										WHERE pc.`contactId`=?";
								$contactDetails = $adapter->query($sql, array($contactPage['contactId']));
								$contactInfo = $contactDetails->toArray();
								$contactPage['contactInfo'] = $contactInfo;

								$result->portfolioPages[$i]['contact'] = $contactPage;
								break;
						}
					}
					$i++;
				}
			}
			return $result;
        } catch (Exception $e) {
            $result = new stdClass();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
	public function getInstituteForMeta($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('id' => 'institute_details'))
					->join(array('ld' => 'login_details'),
							'id.userId = ld.id')
					->where(array('ld.id' => $data->slug))
					->columns(array('name', 'description', 'profilePic'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$details = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$details = $details->toArray();
			if(count($details) > 0) {
				$result->details = $details[0];
				$ogImage = $result->details['profilePic'];
				$ogPath = explode("/", $ogImage);
				$result->details['ogimage'] = $this->ogPath.end($ogPath);
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$result->details['image'] = getSignedURL($result->details['profilePic']);
				}
				$result->status = 1;
				return $result;
			}
			$result->status = 0;
			$result->message = 'No such course found';
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function getAllInstitutes($data) {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$result = new stdClass();
		/*
		SELECT firstName, lastName, gender, profilePic, description, contactMobilePrefix, contactMobile, countries.name, email, institute_invitations.status
		FROM `professor_details` 
		inner join user_details on user_details.userId = professor_details.userId 
		inner join login_details on login_details.id = professor_details.userId 
		inner join countries on countries.id = user_details.addressCountry
		left outer join institute_invitations on professor_details.userId = institute_invitations.prof_id
		where professor_details.firstName = "fraaz";
		*/

		$select = $sql->select();
		$select->from(array('i' => 'institute_details'))
		->join(array('u' =>  'user_details'), 'u.userId = i.userId', array('contactMobilePrefix', 'contactMobile'))
		->join(array('l' => 'login_details'), 'l.id     = i.userId', array('email', 'id'))
		->join(array('c' => 'countries'    ), 'c.id = u.addressCountry', array('addressCountry' => 'name'));
		$select->columns(array(
			'insId' => 'userId',
			'name',
			'profilePic',
			'description'
		));
		$selectString = $sql->getSqlStringForSqlObject($select);
		//echo $selectString;
		$professors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		if($professors->count() !== 0) {
			$result->userDetails = $professors->toArray();
			foreach($result->userDetails as $key => $detail) {
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR'] <> '::1'){
					require_once 'amazonRead.php';
					$result->userDetails[$key]['profilePic'] = getSignedURL($detail['profilePic']);
				}
			}
		} else {
			$result->status = 0;
			$result->exception = "Error";
			return $result;
		}
		//$result->userId = $data->userId;
		$result->status = 1;
		return $result;
	}
	public function getInstituteStudents($data) {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();

			$query="SELECT L.id, L.username, IFNULL(utp.password,'') AS password, u.contactMobile, CONCAT( S.firstName,' ',S.lastName) AS name,L.email,c.id AS courseId, c.name AS courseName,IFNULL(plg.id,'') AS parentid,plg.userid,plg.passwd
					FROM student_details AS S 
					INNER JOIN login_details AS L ON L.id=S.userId
					INNER JOIN user_details AS u ON u.userId=L.id
					INNER JOIN student_course AS K ON K.user_id=L.id
					INNER JOIN courses AS c ON c.id=K.course_id
					LEFT JOIN user_temp_passwords AS utp ON utp.userId=L.id
					LEFT JOIN parent_login_short AS plg ON plg.student_id=L.id AND plg.course_id=K.course_id
					WHERE c.ownerId={$data->userId}";

			$students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$students = $students->toArray();
			if (count($students) == 0) {
				$result->status = 0;
				$result->message = "No students Found";
				return $result;
			}
			$iStudents = array();
			$i=0;
			foreach ($students as $key => $value) {
				$s_key = array_search($value["id"],  array_column($iStudents, 'id'));
				if ($s_key === false) {
					$iStudents[$i] = $value;
					$iStudents[$i]["courseNames"] = $value["courseName"];
					$iStudents[$i]["courses"][] = array($value["courseId"],$value["courseName"]);
					$i++;
				} else {
					$iStudents[$s_key]["courseNames"].= ", ".$value["courseName"];
					$iStudents[$s_key]["courses"][] = array($value["courseId"],$value["courseName"]);
				}
			}
			$result->students = $iStudents;
            return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function getInstituteTempStudents($data) {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();

			$query="SELECT L.id, L.username, IFNULL(utp.password,'') AS password, u.contactMobile, CONCAT( S.firstName,' ',S.lastName) AS name,L.email,c.id AS courseId, c.name AS courseName,IFNULL(plg.id,'') AS parentid,plg.userid,plg.passwd
					FROM student_details AS S 
					INNER JOIN login_details AS L ON L.id=S.userId
					INNER JOIN user_details AS u ON u.userId=L.id
					INNER JOIN student_course AS K ON K.user_id=L.id
					INNER JOIN courses AS c ON c.id=K.course_id AND c.deleted=0
					LEFT JOIN user_temp_passwords AS utp ON utp.userId=L.id
					LEFT JOIN parent_login_short AS plg ON plg.student_id=L.id AND plg.course_id=K.course_id
					WHERE c.ownerId={$data->userId} AND L.temp=1";

			$students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$students = $students->toArray();
			if (count($students) == 0) {
				$result->status = 0;
				$result->message = "No students Found";
				return $result;
			}
			$iStudents = array();
			$i=0;
			foreach ($students as $key => $value) {
				$s_key = array_search($value["id"],  array_column($iStudents, 'id'));
				if ($s_key === false) {
					$iStudents[$i] = $value;
					$iStudents[$i]["courseNames"] = $value["courseName"];
					$iStudents[$i]["courses"][] = array("id"=>$value["courseId"],"name"=>$value["courseName"]);
					$i++;
				} else {
					$iStudents[$s_key]["courseNames"].= ", ".$value["courseName"];
					$iStudents[$s_key]["courses"][] = array("id"=>$value["courseId"],"name"=>$value["courseName"]);
				}
			}
			$result->students = $iStudents;

			$query="SELECT *
					FROM courses AS c
					WHERE c.deleted=0 AND c.ownerId={$data->userId}";

			$courses = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$courses = $courses->toArray();
			$result->courses = $courses;

            return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function transferStudentCourse($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			$query="SELECT * FROM `student_course` WHERE `course_id`={$data->oldCourseId} AND `user_id`={$data->studentId}";

			$courseStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$courseStudents = $courseStudents->toArray();
			if (count($courseStudents) == 0) {
				$result->status = 0;
				$result->message = "No students Found";
				return $result;
			}
			$courseStudents = $courseStudents[0];
			
			$update = $sql->update();
			$update->table('student_course');
			$update->set(array('course_id' => $data->courseId));
			$update->where(array('id' => $courseStudents['id']));
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();

			$update = $sql->update();
			$update->table('student_subject');
			$update->set(array('Active' => 0));
			$update->where(array('courseId' => $data->courseId,'userId' => $data->studentId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();

			$query="SELECT * FROM `subjects` WHERE `courseId`={$data->courseId} AND `ownerId`={$data->userId}";

			$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$subjects = $subjects->toArray();
			if (count($subjects)>0) {
				foreach ($subjects as $key => $value) {
					$student_subject = new TableGateway('student_subject', $this->adapter, null, new HydratingResultSet());
					$student_subject->insert(array(
						'courseId' => $data->courseId,
						'userId' => $data->studentId,
						'subjectId' => $value['id'],
						'Active' => 1
					));	
				}
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			
			$result->status = 1;
            return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function deleteStudentCourse($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$noOfStudents = 0;
			$tempStudents = array();
			if (!empty($data->tempStudents)) {
				$tempStudents = explode(",", $data->tempStudents);
			}
			$data->tempStudents = $tempStudents;
			$permStudents = array();
			if (!empty($data->permStudents)) {
				$permStudents = explode(",", $data->permStudents);
			}
			$data->permStudents = $permStudents;
			$noOfStudents = count($data->tempStudents) + count($data->permStudents);
			if ($noOfStudents>0) {
				if (count($data->tempStudents)>0) {
					//$students = explode(",", $data->tempStudents);
					$students = $data->tempStudents;
					$delStudents = array();
					foreach ($students as $student) {
						if (isset($data->course_id) && !empty($data->course_id)) {
							$query="SELECT * FROM `student_course` WHERE `course_id`={$data->course_id} AND `user_id`={$student}";
						} else {
							$query="SELECT sc.*
									FROM `student_course` AS sc
									INNER JOIN `courses` AS c ON c.id=sc.course_id
									WHERE c.ownerId={$data->userId} AND sc.user_id={$student}";
						}

						$courseStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$courseStudents = $courseStudents->toArray();
						if (count($courseStudents) > 0) {
							$courseStudents = $courseStudents[0];

							$update = $sql->update();
							$update->table('course_key_details');
							//$update->set(array('active_date' => '0000-00-00 00:00:00'));
							$update->set(array('active_date' => NULL));
							$update->where(array('key_id' => $courseStudents['key_id']));
							$statement = $sql->prepareStatementForSqlObject($update);
							$count = $statement->execute()->getAffectedRows();

							$delete = $sql->delete();
							$delete->from('student_course');
							$delete->where(array('id' => $courseStudents['id']));
							$statement = $sql->prepareStatementForSqlObject($delete);
							$count = $statement->execute();

							if (isset($data->course_id) && !empty($data->course_id)) {
								$delete = $sql->delete();
								$delete->from('student_subject');
								$delete->where(array('courseId' => $data->course_id,'userId' => $student));
								$statement = $sql->prepareStatementForSqlObject($delete);
								$count = $statement->execute();
							} else {
								$selectString = "DELETE FROM `student_subject` WHERE `courseId` IN (SELECT id FROM courses WHERE ownerId={$data->userId}) AND `userId` = {$student}";
								$delProfessor = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							}

							$delStudents[] = $student;
						}
					}
				}
				if (count($data->permStudents)>0) {
					//$students = explode(",", $data->permStudents);
					$students = $data->permStudents;
					
					//$delStudents = array();
					foreach ($students as $student) {
						if (isset($data->course_id) && !empty($data->course_id)) {
							$query="SELECT * FROM `student_course` WHERE `course_id`={$data->course_id} AND `user_id`={$student}";
						} else {
							$query="SELECT sc.*
									FROM `student_course` AS sc
									INNER JOIN `courses` AS c ON c.id=sc.course_id
									WHERE c.ownerId={$data->userId} AND sc.user_id={$student}";
						}
						$courseStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$courseStudents = $courseStudents->toArray();
						if (count($courseStudents) > 0) {
							//var_dump($courseStudents);
							$courseStudents = $courseStudents[0];

							$update = $sql->update();
							$update->table('course_key_details');
							//$update->set(array('active_date' => '0000-00-00 00:00:00'));
							$update->set(array('active_date' => NULL));
							$update->where(array('key_id' => $courseStudents['key_id']));
							$statement = $sql->prepareStatementForSqlObject($update);
							$count = $statement->execute()->getAffectedRows();

							$delete = $sql->delete();
							$delete->from('student_course');
							$delete->where(array('id' => $courseStudents['id']));
							$statement = $sql->prepareStatementForSqlObject($delete);
							$count = $statement->execute();

							if (isset($data->course_id) && !empty($data->course_id)) {
								$delete = $sql->delete();
								$delete->from('student_subject');
								$delete->where(array('courseId' => $data->course_id,'userId' => $student));
								$statement = $sql->prepareStatementForSqlObject($delete);
								$count = $statement->execute();
								$delete = $sql->delete();
								$delete->from('student_course');
								$delete->where(array('course_id' => $data->course_id,'user_id' => $student));
								$statement = $sql->prepareStatementForSqlObject($delete);
								$count = $statement->execute();
							} else {
								$selectString = "DELETE FROM `student_subject` WHERE `courseId` IN (SELECT id FROM courses WHERE ownerId={$data->userId}) AND `userId` = {$student}";
								$delStudent = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
								$selectString = "DELETE FROM `student_course` WHERE `course_id` IN (SELECT id FROM courses WHERE ownerId={$data->userId}) AND `user_id` = {$student}";
								$delStudent = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							}

							$delStudents[] = $student;
						}
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
			} else {
				$db->rollBack();
				$result->status = 0;
				$result->exception = "No students selected";
				return $result;
			}
			$result->students = $delStudents;
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function deleteInstituteProfessor($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
		
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			
			$selectString ="SELECT GROUP_CONCAT(`id`) AS subjects_list FROM `subjects`
							WHERE `ownerId`={$data->userId}";
			$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$subjects = $subjects->toArray();
			$subjects = $subjects[0]["subjects_list"];
			if ($subjects) {
				$selectString = "DELETE FROM `subject_professors` WHERE `subjectId` IN ($subjects) AND `professorId` = {$data->professorId}";
				$delProfessor = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$selectString = "DELETE FROM `institute_professors` WHERE `instituteId` = {$data->userId} AND `professorId` = {$data->professorId}";
				$delProfessor = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();

				$result->status = 1;
				$result->message = "Updated";
			} else {
				$db->rollBack();
				$result->status = 0;
				$result->message = "Unspecified error!";
			}
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function savePortfolioImage($filePath, $subdomain, $userId) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();

		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$where = array('subdomain' => $subdomain,'userId' => $userId);
			$sql = new Sql($adapter);
			
			//deleting previous image from cloud
			$select = $sql->select();
			$select->from('pf_portfolio');
			$select->where($where);
			$select->columns(array('img'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$portfolioPic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$portfolioPic = $portfolioPic->toArray();
			$portfolioPic = $portfolioPic[0]['img'];
			$portfolioPic = substr($portfolioPic, 37);
			if($portfolioPic != '') {
				require_once 'amazonDelete.php';
				deleteFile($portfolioPic);
			}
			
			$update = $sql->update();
			$update->table('pf_portfolio');
			$update->set(array('img' => $filePath));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();

			$select = $sql->select();
			$select->from('pf_portfolio');
			$select->where($where);
			$select->columns(array('img'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$portfolioPic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$portfolioPic = $portfolioPic->toArray();
			$portfolioPic = $portfolioPic[0]['img'];
			if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1') {
				require_once 'amazonRead.php';
				$portfolioPic = getSignedURL($portfolioPic);
			} else {
				$portfolioPic = 'assets/layouts/layout2/img/avatar3_small.jpg';
			}

			$result->status = 1;
			$result->message = 'Portfolio Image Uploaded!';
			$result->image = $portfolioPic;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
}

?>