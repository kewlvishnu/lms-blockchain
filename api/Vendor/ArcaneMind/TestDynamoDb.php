<?php
	// Include the AWS SDK using the Composer autoloader
	require_once 'api/Vendor/aws/aws-autoloader.php';
	use Aws\S3\S3Client;
	use Aws\DynamoDb\DynamoDbClient;
	use Aws\Common\Enum\Region;
	use Aws\DynamoDb\Enum\KeyType;
	use Aws\DynamoDb\Enum\Type;
	use Aws\DynamoDb\Marshaler;
	
	class TestDynamoDb {
		// creates a table
		public function create($data) {
			//try {
				$result = new stdClass();
				$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
				$client = $aws->get("dynamodb");
				//$config = require "ConfD.php";
				
				//$dynamodb = $client->createDynamoDb();

				$params = [
					'TableName' => 'content_tracking',
					'KeySchema' => [
						[
							'AttributeName' => 'userId',
							'KeyType' => 'HASH'  //Partition key
						],
						[
							'AttributeName' => 'contentId',
							'KeyType' => 'RANGE'  //Sort key
						]
					],
					'AttributeDefinitions' => [
						[
							'AttributeName' => 'contentId',
							'AttributeType' => 'N'
						],
						[
							'AttributeName' => 'userId',
							'AttributeType' => 'N'
						],
					],
					'ProvisionedThroughput' => [
					    'ReadCapacityUnits' => 10,
					    'WriteCapacityUnits' => 10
					]
				];

				try {
				    $result = $client->createTable($params);
				    echo 'Created table.  Status: ' . 
				        $result['TableDescription']['TableStatus'] ."\n";

				} catch (DynamoDbException $e) {
				    echo "Unable to create table:\n";
				    echo $e->getMessage() . "\n";
				}
			/*}
			catch (Exception $e) {
				return $result;
			}*/
			
		}
		// creates a table
		public function createIndex($data) {
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "ConfD.php";
			$tableName = 'prod.content_tracking';

			$params = [
			    'TableName' => $tableName,
			    'AttributeDefinitions' => [
			        [ 'AttributeName' => 'userId', 'AttributeType' => 'N' ],
			        [ 'AttributeName' => 'contentId', 'AttributeType' => 'N' ],
			        [ 'AttributeName' => 'subjectId', 'AttributeType' => 'N' ]
			    ],
			    'KeySchema' => [
			        [ 'AttributeName' => 'userId', 'KeyType' => 'HASH' ],
			        [ 'AttributeName' => 'contentId', 'KeyType' => 'RANGE' ]
			    ],
			    'LocalSecondaryIndexes' => [
			        [
			            'IndexName' => 'UserSubjectIndex',
			            'KeySchema' => [
			                [ 'AttributeName' => 'userId', 'KeyType' => 'HASH' ],
			                [ 'AttributeName' => 'subjectId', 'KeyType' => 'RANGE' ]
			            ],
			            'Projection' => ['ProjectionType' => 'ALL']
			        ]
			    ],
				'GlobalSecondaryIndexes' => [
					[
					    'IndexName' => 'subjectIndex',
					    'KeySchema' => [
					        [ 'AttributeName' => 'subjectId', 'KeyType' => 'HASH' ],  //Partition key
					    ],
					    'Projection' => [ 'ProjectionType' => 'ALL' ],
					    'ProvisionedThroughput' => [
					        'ReadCapacityUnits' => 8,
					        'WriteCapacityUnits' => 8
					    ]
					]
				],
			    'ProvisionedThroughput' => [
			        'ReadCapacityUnits' => 8,
			        'WriteCapacityUnits' => 8
			    ]
			];
			try {
			    $result = $dynamodb->createTable($params);
			    echo 'Created table.  Status: ' . 
			        $result['TableDescription']['TableStatus'] ."\n";

			} catch (DynamoDbException $e) {
			    echo "Unable to create table:\n";
			    echo $e->getMessage() . "\n";
			}
		}
		// creates a table
		public function test1($data) {
			//try {
				$result = new stdClass();
				$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
				$client = $aws->get("dynamodb");
				//$config = require "ConfD.php";
				
				//$dynamodb = $client->createDynamoDb();

				$params = [
					'TableName' => 'Movies',
					'KeySchema' => [
						[
							'AttributeName' => 'year',
							'KeyType' => 'HASH'  //Partition key
						],
						[
							'AttributeName' => 'title',
							'KeyType' => 'RANGE'  //Sort key
						]
					],
					'AttributeDefinitions' => [
						[
							'AttributeName' => 'year',
							'AttributeType' => 'N'
						],
						[
							'AttributeName' => 'title',
							'AttributeType' => 'S'
						],
					],
					'ProvisionedThroughput' => [
					    'ReadCapacityUnits' => 10,
					    'WriteCapacityUnits' => 10
					]
				];

				try {
				    $result = $client->createTable($params);
				    echo 'Created table.  Status: ' . 
				        $result['TableDescription']['TableStatus'] ."\n";

				} catch (DynamoDbException $e) {
				    echo "Unable to create table:\n";
				    echo $e->getMessage() . "\n";
				}
			/*}
			catch (Exception $e) {
				return $result;
			}*/
			
		}
		// adds multiple items
		public function test2($data)
		{
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "ConfD.php";

			$marshaler = new Marshaler();

			$tableName = 'Movies';

			$movies = json_decode(file_get_contents('moviedata.json'), true);

			foreach ($movies as $movie) {

				$year = $movie['year']; 
				$title = $movie['title'];
				$info = $movie['info'];

				$json = json_encode([
					'year' => $year,
					'title' => $title,
					'info' => $info
				]);

				$params = [
					'TableName' => $tableName,
					'Item' => $marshaler->marshalJson($json)
				];

				try {
					$result = $dynamodb->putItem($params);
					echo "Added movie: " . $movie['year'] . " " . $movie['title'] . "\n";
				} catch (DynamoDbException $e) {
					echo "Unable to add movie:\n";
					echo $e->getMessage() . "\n";
					break;
				}

			}
		}
		// adds an item
		public function test3($data)
		{
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "ConfD.php";

			$marshaler = new Marshaler();

			$tableName = 'Movies';

			$year = 2015;
			$title = 'The Big New Movie';

			$item = $marshaler->marshalJson('
				{
					"year": ' . $year . ',
					"title": "' . $title . '",
					"info": {
						"plot": "Nothing happens at all.",
						"rating": 0
					}
				}
			');

			$params = [
				'TableName' => 'Movies',
				'Item' => $item
			];


			try {
				$result = $dynamodb->putItem($params);
				echo "Added item: $year - $title\n";

			} catch (DynamoDbException $e) {
				echo "Unable to add item:\n";
				echo $e->getMessage() . "\n";
			}
		}
		// reads an item
		public function test4($data)
		{
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "ConfD.php";

			$marshaler = new Marshaler();

			$tableName = 'Movies';

			$year = 2015;
			$title = 'The Big New Movie';

			$key = $marshaler->marshalJson('
				{
					"year": ' . $year . ', 
					"title": "' . $title . '"
				}
			');

			$params = [
				'TableName' => $tableName,
				'Key' => $key
			];

			try {
				$result = $dynamodb->getItem($params);
				print_r($result["Item"]);

			} catch (DynamoDbException $e) {
				echo "Unable to get item:\n";
				echo $e->getMessage() . "\n";
			}
		}
		// updates an item
		public function test5($data)
		{
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "ConfD.php";

			$marshaler = new Marshaler();

			$tableName = 'Movies';

			$year = 2015;
			$title = 'The Big New Movie';

			$key = $marshaler->marshalJson('
				{
					"year": ' . $year . ', 
					"title": "' . $title . '"
				}
			');

			 
			$eav = $marshaler->marshalJson('
				{
					":r": 5.5 ,
					":p": "Everything happens all at once.",
					":a": [ "Larry", "Moe", "Curly" ]
				}
			');

			$params = [
				'TableName' => $tableName,
				'Key' => $key,
				'UpdateExpression' => 
					'set info.rating = :r, info.plot=:p, info.actors=:a',
				'ExpressionAttributeValues'=> $eav,
				'ReturnValues' => 'UPDATED_NEW'
			];

			try {
				$result = $dynamodb->updateItem($params);
				echo "Updated item.\n";
				print_r($result['Attributes']);

			} catch (DynamoDbException $e) {
				echo "Unable to update item:\n";
				echo $e->getMessage() . "\n";
			}
		}
		// Increment an Atomic Counter
		public function test6($data)
		{
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "ConfD.php";

			$marshaler = new Marshaler();

			$tableName = 'Movies';

			$year = 2015;
			$title = 'The Big New Movie';

			$key = $marshaler->marshalJson('
			    {
			        "year": ' . $year . ', 
			        "title": "' . $title . '"
			    }
			');

			$eav = $marshaler->marshalJson('
			    {
			        ":val": 1
			    }
			');

			$params = [
			    'TableName' => $tableName,
			    'Key' => $key,
			    'UpdateExpression' => 'set info.rating = info.rating + :val',
			    'ExpressionAttributeValues'=> $eav,
			    'ReturnValues' => 'UPDATED_NEW'
			];

			try {
			    $result = $dynamodb->updateItem($params);
			    echo "Updated item. ReturnValues are:\n";
			    print_r($result['Attributes']);

			} catch (DynamoDbException $e) {
			    echo "Unable to update item:\n";
			    echo $e->getMessage() . "\n";
			}
		}
		// Update an Item (Conditionally)
		public function test7($data)
		{
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "ConfD.php";

			$marshaler = new Marshaler();

			$tableName = 'Movies';

			$year = 2015;
			$title = 'The Big New Movie';

			$key = $marshaler->marshalJson('
			    {
			        "year": ' . $year . ', 
			        "title": "' . $title . '"
			    }
			');

			$eav = $marshaler->marshalJson('
			    {
			        ":num": 3
			    }
			');

			$params = [
			    'TableName' => $tableName,
			    'Key' => $key,
			    'UpdateExpression' => 'remove info.actors[0]',
			    'ConditionExpression' => 'size(info.actors) >= :num',
			    'ExpressionAttributeValues'=> $eav,
			    'ReturnValues' => 'UPDATED_NEW'
			];

			try {
			    $result = $dynamodb->updateItem($params);
			    echo "Updated item. ReturnValues are:\n";
			    print_r($result['Attributes']);

			} catch (DynamoDbException $e) {
			    echo "Unable to update item:\n";
			    echo $e->getMessage() . "\n";
			}
		}
		// Delete an Item
		public function test8($data)
		{
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "ConfD.php";

			$marshaler = new Marshaler();

			$tableName = 'Movies';

			$year = 2015;
			$title = 'The Big New Movie';

			$key = $marshaler->marshalJson('
				{
					"year": ' . $year . ', 
					"title": "' . $title . '"
				}
			');

			$eav = $marshaler->marshalJson('
				{
					":val": 5 
				}
			');

			$params = [
				'TableName' => $tableName,
				'Key' => $key, 
				//'ConditionExpression' => 'info.rating <= :val',
				//'ExpressionAttributeValues'=> $eav
			];

			try {
				$result = $dynamodb->deleteItem($params);
				echo "Deleted item.\n";

			} catch (DynamoDbException $e) {
				echo "Unable to delete item:\n";
				echo $e->getMessage() . "\n";
			}
		}
		// Query - All Movies Released in a Year
		public function test9($data)
		{
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "ConfD.php";

			$marshaler = new Marshaler();

			$tableName = 'Movies';

			$eav = $marshaler->marshalJson('
			    {
			        ":yyyy": 1985 
			    }
			');

			$params = [
			    'TableName' => $tableName,
			    'KeyConditionExpression' => '#yr = :yyyy',
			    'ExpressionAttributeNames'=> [ '#yr' => 'year' ],
			    'ExpressionAttributeValues'=> $eav
			];

			echo "Querying for movies from 1985.\n";

			try {
			    $result = $dynamodb->query($params);

			    echo "Query succeeded.\n";

			    foreach ($result['Items'] as $movie) {
			        echo $marshaler->unmarshalValue($movie['year']) . ': ' .
			            $marshaler->unmarshalValue($movie['title']) . "\n";
			    }

			} catch (DynamoDbException $e) {
			    echo "Unable to query:\n";
			    echo $e->getMessage() . "\n";
			}
		}
		// Query - All Movies Released in a Year with Certain Titles
		public function test10($data)
		{
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "ConfD.php";

			$marshaler = new Marshaler();

			$tableName = 'Movies';

			$eav = $marshaler->marshalJson('
				{
					":yyyy":1992,
					":letter1": "A",
					":letter2": "L"
				}
			');

			$params = [
				'TableName' => $tableName,
				'ProjectionExpression' => '#yr, title, info.genres, info.actors[0]',
				'KeyConditionExpression' =>
					'#yr = :yyyy and title between :letter1 and :letter2',
				'ExpressionAttributeNames'=> [ '#yr' => 'year' ],
				'ExpressionAttributeValues'=> $eav
			];

			echo "Querying for movies from 1992 - titles A-L, with genres and lead actor\n";

			try {
				$result = $dynamodb->query($params);

				echo "Query succeeded.\n";

				foreach ($result['Items'] as $i) {
					$movie = $marshaler->unmarshalItem($i);
						print $movie['year'] . ': ' . $movie['title'] . ' ... ';

					foreach ($movie['info']['genres'] as $gen) {
						print $gen . ' ';
					}

					echo ' ... ' . $movie['info']['actors'][0] . "\n";
				}

			} catch (DynamoDbException $e) {
				echo "Unable to query:\n";
				echo $e->getMessage() . "\n";
			}
		}
		// Scan the table
		public function test11($data)
		{
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "ConfD.php";

			$marshaler = new Marshaler();

			$tableName = 'Movies';

			//Expression attribute values
			$eav = $marshaler->marshalJson('
			    {
			        ":start_yr": 1950,
			        ":end_yr": 1959
			    }
			');

			$params = [
			    'TableName' => $tableName,
			    'ProjectionExpression' => '#yr, title, info.rating',
			    'FilterExpression' => '#yr between :start_yr and :end_yr',
			    'ExpressionAttributeNames'=> [ '#yr' => 'year' ],
			    'ExpressionAttributeValues'=> $eav
			];

			echo "Scanning Movies table.\n";

			try {
			    while (true) {
			        $result = $dynamodb->scan($params);

			        foreach ($result['Items'] as $i) {
			            $movie = $marshaler->unmarshalItem($i);
			            echo $movie['year'] . ': ' . $movie['title'];
			            echo ' ... ' . $movie['info']['rating']
			                . "\n";
			        }

			        if (isset($result['LastEvaluatedKey'])) {
			            $params['ExclusiveStartKey'] = $result['LastEvaluatedKey'];
			            echo "Scanning for more...\n";
			            $result = $dynamodb->scan($params);
			        } else {
			            break;
			        }
			    }

			} catch (DynamoDbException $e) {
			    echo "Unable to scan:\n";
			    echo $e->getMessage() . "\n";
			}
		}
		// Delete the table
		public function test12($data)
		{
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "ConfD.php";

			$marshaler = new Marshaler();

			$tableName = 'Movies';

			$params = [
			    'TableName' => $tableName
			];

			try {
			    $result = $dynamodb->deleteTable($params);
			    echo "Deleted table.\n";

			} catch (DynamoDbException $e) {
			    echo "Unable to delete table:\n";
			    echo $e->getMessage() . "\n";
			}

		}
		// Delete the table
		public function drop($data)
		{
			$result = new stdClass();
			$aws = Aws\Common\Aws::factory('api/Vendor/ArcaneMind/configDynamo.php');
			$dynamodb = $aws->get("dynamodb");
			//$config = require "ConfD.php";

			$marshaler = new Marshaler();

			$tableName = 'content_tracking';

			$params = [
			    'TableName' => $tableName
			];

			try {
			    $result = $dynamodb->deleteTable($params);
			    echo "Deleted table.\n";

			} catch (DynamoDbException $e) {
			    echo "Unable to delete table:\n";
			    echo $e->getMessage() . "\n";
			}

		}
	}

?>