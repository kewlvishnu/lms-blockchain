<?php

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "Base.php";

class viewInsitutesinfo extends Base {

    public function viewInsitutesDetails($data) {
        try {
            $result = new stdClass();
            $adapter = $this->adapter;
            /*$query = "select A.id,A.Name,A.contactMobile, A.email,A.Course , B.KeyCount, C.StudentCount from
                (select L.id,I.Name,u.contactMobile, L.email, COUNT(C.id) as Course from institute_details as I 
                Join courses as C on I.userId=C.ownerId Join user_details as u on I.Userid=u.userid 
                Join login_details as L on I.UserId=L.id and C.deleted=0 Group by I.userId) A left join
                (select I.id,count(K.Key_id) as KeyCount from institute_details as I Left Join course_key_details as K on K.userid=I.userid
                group by I.userid) B on  A.id = B.id left join (select Distinct I.id,count( Distinct SC.user_id) as StudentCount from student_course SC join courses C  on SC.course_id = C.id 
		join institute_details I on I.userId= C.ownerId group by I.id) C on B.ID =C.Id ORDER BY A.id";*/
		$query = "SELECT A.id, A.Name, A.contactMobile, A.email, A.Course, B.KeyCount, C.StudentCount
                    FROM (

                    SELECT L.id, I.Name, u.contactMobile, L.email, COUNT( C.id ) AS Course
                    FROM institute_details AS I
                    JOIN user_details AS u ON I.Userid = u.userid
                    JOIN login_details AS L ON I.UserId = L.id
                    LEFT OUTER JOIN courses AS C ON C.ownerId = L.id
                    GROUP BY I.userId
                    )A
                    LEFT OUTER JOIN (

                    SELECT I.userId, COUNT( K.Key_id ) AS KeyCount
                    FROM institute_details AS I
                    LEFT JOIN course_key_details AS K ON K.userid = I.userid
                    GROUP BY I.userid
                    )B ON A.id = B.userId
                    LEFT OUTER JOIN (

                    SELECT DISTINCT I.userId, COUNT( DISTINCT SC.user_id ) AS StudentCount
                    FROM student_course SC
                    JOIN courses C ON SC.course_id = C.id
                    JOIN institute_details I ON I.userId = C.ownerId
                    GROUP BY I.id
                    )C ON B.userId = C.userId
                    ORDER BY A.id";
            $institutes = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $result->institutes = $institutes->toArray();
            $result->status = 1;
            return $result;
        } catch (Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function viewInstitutesLoginInfo($data) {
        try {
            $result = new stdClass();
            $adapter = $this->adapter;
            /*$query = "Select U.Id,case 
        	when UR.roleId= 1 then (select name from institute_details where userId=U.Id) when UR.roleId= 2
                then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId=U.Id)
                when UR.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId=U.Id)
                when UR.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId=U.Id)
                else 'NA' end as Name,(select name from user_role_details where id=UR.roleid) as Role,
                L.Username,L.email, U.contactMobile,L.LastLogin,L.Active From login_details as L 
                join user_details as U on L.id=U.UserId Join user_roles as UR on UR.userId=U.userId
                Where UR.roleId <5 Order By U.Id";*/
            $query = "SELECT UR.roleId, LD.id Id, LD.username Username, LD.email, LD.active Active, LD.lastLogin LastLogin, URD.name Role, UD.contactMobile, CASE WHEN UR.roleId=1 THEN (SELECT name FROM institute_details WHERE userId=LD.id)
                WHEN UR.roleId=2 THEN (SELECT CONCAT(firstName, ' ', lastName) FROM professor_details WHERE userId=LD.id)
                WHEN UR.roleId=3 THEN (SELECT CONCAT(firstName, ' ', lastName) FROM publisher_details WHERE userId=LD.id)
                WHEN UR.roleId=4 THEN (SELECT CONCAT(firstName, ' ', lastName) FROM student_details WHERE userId=LD.id)
                ELSE 'NA' END AS Name
                FROM login_details LD JOIN user_roles UR ON UR.userId=LD.id JOIN user_role_details URD ON UR.roleId = URD.id JOIN user_details UD ON UD.userId=LD.id WHERE UR.roleId!=5;";
            $institutes = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
            $result->institutes = $institutes->toArray();
            $result->status = 1;
            return $result;
        } catch (Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function ChangeLoginstatus($data) {

        $db = $this->adapter->getDriver()->getConnection();

        $db->beginTransaction();

        $result = new stdClass();
        try {
            $sql = new Sql($this->adapter);
            $update = $sql->update();
            $update->table('login_details');
            $update->set(array('active' => $data->status));
            $update->where(array('id' => $data->instituteId));
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            // If all succeed, commit the transaction and all changes are committed at once.
            $db->commit();
            $result->status = 1;
            $result->message = "Status changed successfully ";
            return $result;
        } catch (Exception $e) {
            $db->rollBack();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

}

?>