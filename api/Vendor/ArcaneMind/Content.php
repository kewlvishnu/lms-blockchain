<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Select;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	
	require_once "Base.php";
	
	
	class Content extends Base {
		
		public function createHeading($data) {

	        $db = $this->adapter->getDriver()->getConnection();
	        
	        $db->beginTransaction();
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				//checking if course is not expired
				require_once 'Subject.php';
				$s = new Subject();
				if($s->subjectCourseExpired($data)) {
					$result->status = 0;
					$result->message = 'Your course has expired. You can not add more Headings.';
					return $result;
				}
				require_once "Chapter.php";
				$ch = new Chapter();
				if(!$ch->userOwnsChapter($data)) {
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}
				$headings = new TableGateway('headings', $adapter, null,new HydratingResultSet());
				$insert = array(
					'chapterId'			=> 	$data->chapterId,
					'title' 			=>	$data->title
				);
				$headings->insert($insert);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$data->headingId = $headings->getLastInsertValue();
				if($data->headingId == 0) {
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}
				$result->headingId = $data->headingId;
				$result->status = 1;
				$result->message = 'Heading created!';
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function updateHeading($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result =new stdClass();
			try {
				$adapter = $this->adapter;
				
				require_once "Chapter.php";
				$ch = new Chapter();
				
				if(!$ch->userOwnsChapter($data)) {
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}
				
				$adapter = $this->adapter;
				
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('headings');
				$update->set(array('title' => $data->title));
				$update->where(array('id' => $data->headingId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$result = $statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				
				if($data->headingId == 0) {
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}
				$result->headingId = $data->headingId;
				$result->status = 1;
				$result->message = 'Heading updated!';
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function deleteHeading($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				// Check if user owns
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$delete = $sql->delete();
				
				$delete->from('headings');
				$delete->where(array('id' => $data->hid));
				$statement = $sql->prepareStatementForSqlObject($delete);
				$headings = $statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result = new stdClass();
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function createContent($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result =new stdClass();
			try {
				$adapter = $this->adapter;
				
				require_once "Chapter.php";
				$ch = new Chapter();
				if(!$ch->userOwnsChapter($data)) {
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}
				
				$content = new TableGateway('content', $adapter, null,new HydratingResultSet());
				$insert = array(
					'chapterId'			=> 	$data->headingId,
					'headingId'			=>  0,
					'title' 			=>	$data->title,
					'demo'				=>	$data->demo
				);
				$content->insert($insert);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$data->contentId = $content->getLastInsertValue();
				if($data->contentId == 0) {
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}
				$result->contentId = $data->contentId;
				$result->status = 1;
				$result->message = 'Content created!';
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function updateContent($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result =new stdClass();
			try {
				$adapter = $this->adapter;
				
				require_once "Chapter.php";
				$ch = new Chapter();
				
				if(!$ch->userOwnsChapter($data)) {
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}
				
				$adapter = $this->adapter;
				
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('content');
				$update->set(array('title' => $data->title, 'headingId' => $data->headingId, 'demo'	=>	$data->demo));
				$update->where(array('id' => $data->contentId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$result = $statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				
				if($data->headingId == 0) {
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}
				$result->contentId = $data->contentId;
				$result->status = 1;
				$result->message = 'Content updated!';
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function updatePublishedStatus($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result =new stdClass();
			try {
				$adapter = $this->adapter;
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('content');
				$update->set(array('published' => $data->status));
				$update->where(array('id' => $data->conid));
				$statement = $sql->prepareStatementForSqlObject($update);
				$result = $statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();

				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					if ($data->status == 1 && $data->mail == 1) {
						require_once("Subject.php");
						$s = new Subject();
						require_once("Course.php");
						$c = new Course();
						$dataSS = new stdClass();
						$dataSS->subjectId = $data->subjectId;
						$dataSS->courseId = $data->courseId;
						$dataSS->userId = $data->userId;
						$dataSS->userRole = $data->userRole;
						$dataSS->type = 'registered';
						$subjectStudents = $s->getSubjectStudents($dataSS);
						$subjectDetail = $s->getSubjectDetails($dataSS);
						$courseDetail = $c->getCourseDetails($dataSS);

						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('content');
						$select->where( array('id' => $data->conid) );						
						$selectString = $sql->getSqlStringForSqlObject($select);
						$contentData = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$contentData = $contentData->toArray()[0];

						if ($subjectStudents->status == 1 && $subjectDetail->status == 1 && $courseDetail->status == 1) {
							$subjectName = $subjectDetail->subjectDetails['name'];
							$courseName = $courseDetail->courseDetails['name'];
							require_once "User.php";
							$us = new User();
							$subjectStudents = $subjectStudents->students;
							foreach ($subjectStudents as $key => $subjectStudent) {
								$dataU = new stdClass();
								$dataU->userId = $subjectStudent['id'];
								$userData = $us->getBasicUserDetails($dataU);
								$userEmail = $userData->email['email'];
								//var_dump($userEmail);
								$userName = $userData->details['name'];
								//var_dump($userName);

								$subject = "New Content Published";
								$message = "Dear {$userName}, <br> <br>";
								$message .= "New Content has been published in the subject {$subjectName} of the course {$courseName} <br>";
								$message .= "Please visit <a href='{$this->sitePath}manage/content/subjects/{$subjectDetail->subjectDetails['id']}/chapters/{$contentData['id']}/content/{$data->conid}'>this link</a>, to check the content. <br>";
								$message .= "Regards, <br>Arcanemind Team";

								$us->sendMail($userEmail, $subject, $message);
							}
						}
					}
				}
				
				$result->status = 1;
				$result->message = 'updated!';
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function updateDownloadableStatus($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result =new stdClass();
			try {				
				$adapter = $this->adapter;
				
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('content');
				$update->set(array('downloadable' => $data->status));
				$update->where(array('id' => $data->conid));
				$statement = $sql->prepareStatementForSqlObject($update);
				$result = $statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				
				$result->status = 1;
				$result->message = 'Updated!';
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function deleteContent($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				// Check if user owns
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$delete = $sql->delete();
				
				$delete->from('content');
				$delete->where(array('id' => $data->conid));
				$statement = $sql->prepareStatementForSqlObject($delete);
				$content = $statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result = new stdClass();
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function getAllContent($data) {
			$result = new stdClass();
			$adapter = $this->adapter;				
			/*require_once "Chapter.php";
			$ch = new Chapter();
			if(!$ch->userOwnsChapter($data)) {
				$result->status = 0;
				$result->message = "Some error occured";
				return $result;
			}*/
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('chapters')
					->order('weight ASC')
					->order('id ASC');
			$select->where( array('subjectId' => $data->subjectId, 'deleted' => 0) );
			
			$selectString = $sql->getSqlStringForSqlObject($select);
			$headings = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result = new stdClass();
			$tempHeadings = $headings->toArray();
			$result->headings = array();
			foreach($tempHeadings as $h) {
				$t = new stdClass();
				$t->headingId = $h['id'];
				$h['content'] = $this->getHeadingContent($t);
				$result->headings[] = $h;
			}
			//$result->headlessContent = $this->getHeadingLessContent($data);
			$result->courseId = $data->courseId;
			$result->status = 1;
			return $result;
		}

		public function getChapterContent($data) {
			$result = new stdClass();
			$adapter = $this->adapter;
			/*require_once "Chapter.php";
			$ch = new Chapter();
			if(!$ch->userOwnsChapter($data)) {
				$result->status = 0;
				$result->message = "Some error occured";
				return $result;
			}*/
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('chapters')
					->order('weight ASC')
					->order('id ASC');
			$select->where( array('id' => $data->chapterId) );			
			$selectString = $sql->getSqlStringForSqlObject($select);
			$headings = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result = new stdClass();
			$tempHeadings = $headings->toArray();
			$result->content = array();
			if (count($tempHeadings) > 0) {
				$h = $tempHeadings[0];
				$t = new stdClass();
				$t->headingId = $h['id'];
				$h['content'] = $this->getHeadingContent($t);
				$result->content = $h;
			}
			$result->status = 1;
			return $result;
		}

		public function getHeadingLessContent($data) {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			
			$select = $sql->select();
			$select->from(array('c' => 'content'))
					->join(array('f' => 'files'),
								'c.id = f.contentId',
								array('fileType' => 'type',
										'stuff' => 'stuff',
										'fileName' => 'fileRealName',
										'descText' => 'description',
										'metadata' => 'metadata'
								), $select::JOIN_LEFT)
					->order('weight ASC')
					->order('id ASC');
			$select->where( array('headingId' => 0, 'chapterId' => $data->chapterId));
			
			$selectString = $sql->getSqlStringForSqlObject($select);
			$content = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$tempContent = $content->toArray();
			$content = array();
			foreach($tempContent as $c) {
				if(isset($c['stuff'])  && $c['stuff'] != null) {
					$d = new stdClass();
					$d->contentId = $c['id'];
					$c['supplementaryFiles'] = $this->getSupplementaryFiles($d);
				}
				$content[] = $c;
			}
			return $content;
		}
		
		public function getHeadingContent($data) {  
			$result = new stdClass();
			$adapter = $this->adapter;				
			$sql = new Sql($adapter);
			
			$select = $sql->select();
			$select->from(array('c' => 'content'))
					->join(array('f' => 'files'),
								'c.id = f.contentId',
								array('fileType' => 'type',
										'stuff' => 'stuff',
										'fileName' => 'fileRealName',
										'descText' => 'description',
										'metadata' => 'metadata'
								), $select::JOIN_LEFT)
					->order('weight ASC')
					->order('id ASC');
			$select->where( array('chapterId' => $data->headingId));
			
			$selectString = $sql->getSqlStringForSqlObject($select);
			$content = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$tempContent = $content->toArray();
			$content = array();
			foreach($tempContent as $c) {
				if(isset($c['stuff'])  && $c['stuff'] != null) {
					$d = new stdClass();
					$d->contentId = $c['id'];
					$c['supplementaryFiles'] = $this->getSupplementaryFiles($d);
				}
				if ($c['descText'] == null) {
					$c['descText'] = '';
				}
				$content[] = $c;
			}
			return $content;
		}
		
		public function saveContentStuff($data) {

			$db = $this->adapter->getDriver()->getConnection();
			$result =new stdClass();
			try {
				$adapter = $this->adapter;
				
				// Check if user own content
				$this->deleteContentStuff($data);

				$db->beginTransaction();
				$files = new TableGateway('files', $adapter, null,new HydratingResultSet());
				$insert = array(
					'contentId'		=> 	$data->contentId,
					'type'			=>      $data->contentType,
					'stuff' 		=>	$data->stuff,
					'fileRealName' 		=>	$data->fileRealName,
					'metadata'		=>      $data->metadata
				);
				$files->insert($insert);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$data->fileId = $files->getLastInsertValue();
				if($data->fileId == 0) {
					$db->rollBack();
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}
				$result->fileId = $data->fileId;
				$result->status = 1;
				$result->message = 'File saved!';
				$result->metadata = $data->metadata;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function deleteContentStuff($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				if(isset($data->publicAccess) && $data->publicAccess == true){
					// if(!$this->userOwnsContent($req)){
						// $result->status = 0;
						// $result->message = "Unspecified error!";
						// return $result;
					// }
				}
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$delete = $sql->delete();
				$delete->from('files');
				$delete->where(array('contentId' => $data->contentId));
				$statement = $sql->prepareStatementForSqlObject($delete);
				$result = $statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = 'Deleted';
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
	
		public function saveStuffDesc($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result =new stdClass();
			try {				
				$adapter = $this->adapter;
				
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('files');
				$update->set(array('description' => $data->descText));
				$update->where(array('contentId' => $data->contentId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$result = $statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				
				$result->status = 1;
				$result->message = 'Updated!';
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function updateContentOrder($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result = new stdClass();
			$result2 = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				
				foreach($data->order->headings as $hid => $heading) {
					// Check if user owns heading
					if($hid != '0'){
						$update = $sql->update();
						$update->table('chapters');
						//var_dump($heading);die();
						$update->set(array('weight' => $heading->weight));
						$update->where(array('id' => $hid));
						$statement = $sql->prepareStatementForSqlObject($update);
						$result = $statement->execute();
					}
					// Check if user owns content
					$sql2 = new Sql($this->adapter);
					foreach($heading->content as $conid => $weight) {
						$update2 = $sql2->update();
						$update2->table('content');
						$update2->set(array('weight' => $weight, 'chapterId' => $hid));
						$update2->where(array('id' => $conid));
						$statement2 = $sql2->prepareStatementForSqlObject($update2);
						$result2 = $statement2->execute();
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = 'Updated!';
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function saveSupplementaryStuff($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				
				// Check if user own content
				$files = new TableGateway('supplementary_files', $adapter, null,new HydratingResultSet());
				$insert = array(
					'contentId'			=>	$data->contentId,
					'fileRealName'		=>	$data->fileRealName,
				);
				$files->insert($insert);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$data->fileId = $files->getLastInsertValue();
				if($data->fileId == 0) {
					$db->rollBack();
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}
				$result->fileId = $data->fileId;
				$result->generatedFileName = "supp-content-{$data->fileId}.{$data->fileExt}";
				$result->status = 1;
				$result->fileId = $data->fileId;
				$result->fileRealName = $data->fileRealName;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
	
		public function getSupplementaryFiles($data) {
			$result = new stdClass();
			$adapter = $this->adapter;
			// Check if user owns content
			// if(!$ch->userOwnsChapter($data)) {
				// $result->status = 0;
				// $result->message = "Some error occured";
				// return $result;
			// }
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('supplementary_files');
			$select->where( array('contentId' => $data->contentId));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$suppFiles = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$temp = $suppFiles->toArray();
			if(count($temp) > 0)
				return $temp;
			return null;
		}
		
		public function deleteSupplementaryStuff($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				// if(isset($data->publicAccess) && $data->publicAccess == true){
				// if(!$this->userOwnsContent($req)){
					// $result->status = 0;
					// $result->message = "Unspecified error!";
					// return $result;
				// }
				// }
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$delete = $sql->delete();
				$delete->from('supplementary_files');
				$delete->where(array('id' => $data->suppFileId));
				$statement = $sql->prepareStatementForSqlObject($delete);
				$result = $statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = 'Deleted';
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		//to download the content stuff for student if it is downloadable
		public function downloadContent($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('c'	=>	'content'))
				->join(array('f'	=>	'files'),
					'f.contentId = c.id',
					array('contentLink'	=>	'stuff')
				);
				$select->where(array(
					'c.id'			=>	$data->contentId,
					'downloadable'	=>	1
				));
				$select->columns(array('id'	=>	'id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$link = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$link = $link->toArray();
				if(count($link) > 0) {
					//need to add signature
					$result->link = $link[0];
					if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1') {
						require_once 'amazonRead.php';
						$result->link['contentLink'] = getSignedURL($result->link['contentLink']);
					}
					$result->status = 1;
					return $result;
				}
				$result->status = 0;
				$result->message = 'Content not available for download.';
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function ImportContent($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$query = "CALL importContent({$data->icontentId}, {$data->ichapterId});";
				$result->copy = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				//$result->copy = $copy->toArray();
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
	}
?>