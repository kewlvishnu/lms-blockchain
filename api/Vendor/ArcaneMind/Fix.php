<?php

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "Base.php";

//$adapter = require "adapter.php";
class Fix extends Base {

	public function fixCourseSubjects($data) {
		$result = new stdClass();
		$adapter = $this->adapter;
		try {
			$query="SELECT * FROM `student_course` WHERE `packId`=0";
			$courseStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$courseStudents = $courseStudents->toArray();
			if (count($courseStudents)) {
				//var_dump($courseStudents);
				foreach ($courseStudents as $key => $cstudent) {
					$course_id = $cstudent['course_id'];
					$user_id = $cstudent['user_id'];
					$query="SELECT * FROM `student_subject` WHERE `courseId`=$course_id AND `userId`=$user_id";
					$subjStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$subjStudents = $subjStudents->toArray();
					if (count($subjStudents) == 0) {
						//var_dump($subjStudents);
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('subjects');
						$select->columns(array('id'));
						$select->where(array('courseId' => $course_id));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$subjects = $subjects->toArray();
						if (count($subjects)>0) {
							foreach ($subjects as $key => $subject) {
								$subjectId = $subject['id'];
								$student_subject = new TableGateway('student_subject', $adapter, null, new HydratingResultSet());
								$insert = array(
									'courseId' => $course_id,
									'userId' => $user_id,
									'subjectId' => $subjectId,
									'Active' => 1
								);
								$student_subject->insert($insert);
							}
						}
					}
				}
			}
			$result->status = 1;
			$result->message = 'Course Subjects fixed!';
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
}

?>