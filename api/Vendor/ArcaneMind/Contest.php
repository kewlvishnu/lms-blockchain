<?php

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Mail;
use Zend\Mime\Message;
use Zend\Mime\Part;

class Contest {

	public $id;
	public $role;
	public $adapter;

	function __construct() {
		$config = require "Conf.php";
		$this->adapter = new Zend\Db\Adapter\Adapter($config);
	}

	public function getContestValidity($data) {
		$adapter = $this->adapter;
		$selectString ="SELECT *
						FROM `contest_coupon_claim`
						WHERE `uniqueCode`='{$data->uniqueCode}' AND used=0";
		$contest = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$result = new stdClass();
		$contest = $contest->toArray();
		if (count($contest) > 0) {
			$result->valid = true;
			//$result->contest = $contest[0];
		} else {
			$result->valid = false;
		}
		return $result;
	}
	public function getContestCoupon($data) {
		$adapter = $this->adapter;
		$sql 		 = new Sql($adapter);
		$selectString ="SELECT *
						FROM `contest_coupon_claim`
						WHERE `uniqueCode`='{$data->uniqueCode}'";
		$contest = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$result = new stdClass();
		$contest = $contest->toArray();
		if (count($contest) > 0) {
			$result->status = 1;
			$result->coupon = $contest[0]['coupon'];
			$result->userId = $data->userId;
			//updating new cover pic in database
			$update = $sql->update();
			$update->table('contest_coupon_claim');
			$update->set(
				array('used' => '1', 'userId' => $data->userId)
			);
			$update->where(array('id' => $contest[0]['id']));
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			$resMail = $this->sendContestCouponEmail($result);
			if($resMail->status == 0){
				return $resMail;
			}
		} else {
			$result->status = 0;
		}
		return $result;
	}
	public function sendContestCouponEmail($data) {
        $result = new stdClass();
        try {
			// Need to make sure that token is unique
			$adapter = $this->adapter;
			$selectString = "SELECT L.email,L.username,
				case 
				when UR.roleId= 1 then (SELECT name FROM institute_details WHERE userId=L.id)
				when UR.roleId= 2 then (SELECT CONCAT(firstname, ' ', lastname)  FROM professor_details WHERE userId=L.id)
				when UR.roleId= 3 then (SELECT CONCAT(firstname, ' ', lastname)  FROM publisher_details WHERE userId=L.id)
				when UR.roleId= 4 then (SELECT CONCAT(firstname, ' ', lastname)  FROM student_details WHERE userId=L.id)
				else 'NA' end as Name,
				(SELECT name FROM user_role_details WHERE id=UR.roleId) as Role
				From login_details as L 
				Join user_roles as UR on UR.userId=L.id
				Where L.id=$data->userId";

            $loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $result = new stdClass();
            if ($loginDetails->count() == 0) {
                $result->status = 0;
                $result->message = "Invalid Username or Email.";
                return $result;
            }
            $loginDetails->email = $loginDetails->toArray();
            $data->email = $loginDetails->email[0]['email'];
            $data->loginUserName=$loginDetails->email[0]['username'];
            $data->name=$loginDetails->email[0]['Name'];
            $data->role=$loginDetails->email[0]['Role'];

            global $siteBase;
            
            $emailBody = "Dear  $data->name,<br><br>";

            $emailBody .= "<h3>Congratulations!</h3><p>You have won a coupon to take any course upto Rs.3500/- for free!<p>";

            $emailBody .= "<p>Here is your coupon code : <strong>{$data->coupon}</strong><p>";

            $emailBody .= "<p>Thank you for participating  in 'ANSWER TO WIN' contest arranged by Integro.io<p>";

            $emailBody .= "<p><a href='{$siteBase}'>Integro</a> is a platform & marketplace for online courses & entrance exam preparation. We have courses on various domains, if you want to check them out click <a href='{$siteBase}marketplace/courses/'>here</a>.<p>";

            $subject = 'ANSWER TO WIN - COUPON CODE - INTEGRO';
            $to = $data->email;
             
            require_once('User.php');
            $u = new User();
            $u->sendMail($to, $subject, $emailBody);
            
            $result = $data;
            //$result->status = 1;
            return $result;
        } catch (Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
}

?>