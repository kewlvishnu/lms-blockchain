<?php
	$branch = "production";
	$ogCourseImage = "https://d23h5gn2j73jqu.cloudfront.net/course-images/";
	$publicationTypes = array(
							'published'	=> 'Published Papers',
							'research'	=> 'Research Papers',
							'books'		=> 'Books'
						);
	$sitePath = "https://www.integro.io/";
	$urlDomains = array('integro.io','www.integro.io');
	$chatPrefix = "prod-";
?>