cd /var/www/arcanemind/prod/html/



chmod -R 0777 /var/www/arcanemind/prod/html/admin/*
chmod 0777 /var/www/arcanemind/prod/html/user-data
chmod 0777 /var/www/arcanemind/prod/html/ipn.log
chmod 0777 /var/www/arcanemind/prod/html/user-data/certificates


################################# Database Connection #################################

cd /var/www/arcanemind/prod/html/api/Vendor/ArcaneMind/

rm -rf Conf.php

cp -r /var/www/arcanemind/prod/html/api/Vendor/ArcaneMind/conf/PRODUCTION-Z-ACTIVE-PROD/Conf.php    /var/www/arcanemind/prod/html/api/Vendor/ArcaneMind/

rm -rf global.php

cp -r /var/www/arcanemind/prod/html/api/Vendor/ArcaneMind/conf/PRODUCTION-Z-ACTIVE-PROD/global.php    /var/www/arcanemind/prod/html/api/Vendor/ArcaneMind/

cp -r /var/www/arcanemind/prod/html/api/Vendor/ArcaneMind/conf/PRODUCTION-Z-ACTIVE-PROD/cloudFront.php  /var/www/arcanemind/prod/html/api/Vendor/ArcaneMind/


cd /var/www/arcanemind/prod/html/api/
rm -rf index.php


cp -r /var/www/arcanemind/prod/html/api/Vendor/ArcaneMind/conf/PRODUCTION-Z-ACTIVE-PROD/index.php    /var/www/arcanemind/prod/html/api/


cp -r /var/www/arcanemind/prod/html/api/Vendor/ArcaneMind/conf/PRODUCTION-Z-ACTIVE-PROD/confD.php  /var/www/arcanemind/prod/html/api/Vendor/ArcaneMind/

cp -r /var/www/arcanemind/prod/html/api/Vendor/ArcaneMind/conf/PRODUCTION-Z-ACTIVE-PROD/.htaccess /var/www/arcanemind/prod/html/