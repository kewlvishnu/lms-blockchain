<?php
	try{
		
		//error_reporting();
		/**
		* This makes our life easier when dealing with paths. Everything is relative
		* to the application root now.
		*/

		chdir(dirname(__DIR__));
		
		$libPath = "Vendor";
		
		// Loading Zend
		include $libPath . '/Zend/Loader/AutoloaderFactory.php';
		Zend\Loader\AutoloaderFactory::factory(array(
			'Zend\Loader\StandardAutoloader' => array(
					'autoregister_zf' => true,
					'db' => 'Zend\Db\Sql'
			)
		));
		require "Image.php";
		require $libPath . "/ArcaneMind/Api.php";
		session_start();
		$userId = $_SESSION['userId'];
		$userRole = $_SESSION['userRole'];
		if($userRole == 4)
			$images_dir = "user-data/images/";
		else
			$images_dir = "user-data/images/";
		$public_images_dir = "course-images/";
		
		function correctFileExt($ext) {
			$ext = strtolower($ext);
			return ($ext == 'jpg' ||  $ext == 'jpeg' ||  $ext == 'png' || $ext == 'gif' || $ext == 'bmp');
		}
		function maliciousFileExt($ext) {
			$ext = strtolower($ext);
			$listExt = array("exe", "pif", "application", "gadget", "msi", "msp", "com", "scr", "hta", "cpl", "msc", "jar", "bat", "cmd", "vb", "vbs", "vbe", "js", "jse", "ws", "wsf", "wsc", "wsh", "ps1", "ps1xml", "ps2", "ps2xml", "psc1", "psc2", "msh", "msh1", "msh2", "mshxml", "msh1xml", "msh2xml", "scf", "lnk", "inf", "reg");
			return in_array($ext, $listExt);
		}
		$time = time();
		//$time = 'uid';
		if(isset($_FILES["manual-exam-upload"])) {
			$result = new stdClass();
			$file_parts = pathinfo($_FILES["manual-exam-upload"]["name"]);
			$ext = $file_parts['extension'];
			if( $ext != 'csv' ) {
				$result->status  = 0;
				$result->message = "Error: Only CSV files are allowed.";
				echo json_encode($result);
			} else if ($_FILES["manual-exam-upload"]["error"] > 0) {
				$result->status  = 0;
				$result->message = "Error: " . $_FILES["manual-exam-upload"]["error"] . "<br>";
				echo json_encode($result);
			} else {
				//var_dump($_POST);
				//var_dump($_FILES);
				$i=0;
				$cols=0;
				$marks = array();
				$fileData=fopen($_FILES["manual-exam-upload"]['tmp_name'],'r');
				while($row=fgetcsv($fileData)){  
					// can parse further $row by usingstr_getcsv
					if ($i>0) {
						for ($j=0; $j < $cols; $j++) { 
							if (isset($row[$j])) {
								$marks[$i-1][$j] = $row[$j];
								//echo $row[$j];
							} else {
								$marks[$i-1][$j] = 0;
								//echo 0;
							}
						}
						//var_dump($row);
					} else {
						$cols = count($row);
						if ($row[2] == "Total Marks") {
							$result->analytics = "quick";
						} else {
							$result->analytics = "deep";
							$result->questions = $cols-2;
						}
					}
					$i++;
				}
				$result->status= 1;
				$result->marks = $marks;
				echo json_encode($result);
			}
		} else if(isset($_FILES["subjective-upload"])) {
			/*$w = $_POST['w'];
			$h = $_POST['h'];
			$x = $_POST['x'];
			$y = $_POST['y'];*/
			$file_parts = pathinfo($_FILES["subjective-upload"]["name"]);
			//$ext = $file_parts['extension'];
			$ext = $file_parts['extension'];
			if( ($ext != 'pdf') &&  ($ext != 'doc') &&  ($ext != 'docx') && !correctFileExt($ext)) {
				$result = new stdClass();
				$result->status  = 0;
				$result->message = "Error: Only PDF, JPG, JPEG, PNG files are allowed.";
				echo json_encode($result);
			} else if ($_FILES["subjective-upload"]["error"] > 0) {
				$result->status  = 0;
				$result->message = "Error: " . $_FILES["subjective-upload"]["error"] . "<br>";
				echo json_encode($result);
			} else {
				if(!isset($_POST['examId'])){
					echo "Invalid request";
					die();
				}
				$examId = $_POST['examId'];

				//unset($_SESSION['examId']);
				$filesDir = "user-data/files/";
				$fileName = "subjective-upload-{$examId}-{$time}.{$ext}";
				$filePath = $filesDir. $fileName;
				//move_uploaded_file($_FILES["subjective-upload"]["tmp_name"], $filePath);
				
				require_once 'amazonPublicUpload.php';
				$data = new stdClass();
				$data->fileName = uploadFilePublic($filePath, $_FILES["subjective-upload"]["tmp_name"], $_FILES["subjective-upload"]["type"]);
				$data->examId = $examId;
				$data->userId = $userId;
				//$res = Api::saveCourseImage($data);
				echo json_encode($data);
			}
		} else if(isset($_FILES["audio-upload"])) {
			
			$file_parts = pathinfo($_FILES["audio-upload"]["name"]);
			//$ext = $file_parts['extension'];
			$ext = $file_parts['extension'];
			if($ext != 'mp3') {
				$result = new stdClass();
				$result->status  = 0;
				$result->message = "Error: Only MP3 files are allowed.";
				echo json_encode($result);
			} else if ($_FILES["audio-upload"]["error"] > 0) {
				$result->status  = 0;
				$result->message = "Error: " . $_FILES["audio-upload"]["error"] . "<br>";
				echo json_encode($result);
			} else {
				if(!isset($_POST['subjectId'])){
					echo "Invalid request";
					die();
				}
				$subjectId = $_POST['subjectId'];

				//unset($_SESSION['subjectId']);
				$filesDir = "user-data/files/";
				$fileName = "audio-upload-{$subjectId}-{$time}.{$ext}";
				$filePath = $filesDir. $fileName;
				//move_uploaded_file($_FILES["audio-upload"]["tmp_name"], $filePath);
				
				require_once 'amazonPublicUpload.php';
				$data = new stdClass();
				$data->fileName = uploadFilePublic($filePath, $_FILES["audio-upload"]["tmp_name"], $_FILES["audio-upload"]["type"]);
				$data->subjectId = $subjectId;
				$data->userId = $userId;
				//$res = Api::saveCourseImage($data);
				echo json_encode($data);
			}
		} else if(isset($_FILES["cover-image"])) {
			//ar_dmp($_POST);die();
			/*$w = intval($_POST['w']);
			$h = intval($_POST['h']);
			$x = intval($_POST['x']);
			$y = intval($_POST['y']);*/
			$file_parts = pathinfo($_FILES["cover-image"]["name"]);
			//$ext = $file_parts['extension'];
			$ext = 'jpg';
			if( !correctFileExt($ext)) {
				echo "Error: Only JPG, JPEG, PNG files are allowed.";
			}else if ($_FILES["cover-image"]["error"] > 0) {
				echo "Error: " . $_FILES["cover-image"]["error"] . "<br>";
			} else {
				$imageName = "cover-image-{$userId}-{$time}.{$ext}";
				$imagePath = $images_dir. $imageName;
				require_once 'amazonUpload.php';
				uploadFile($imagePath, $_FILES["cover-image"]["tmp_name"], $_FILES["cover-image"]["type"]);
				/*move_uploaded_file($_FILES["cover-image"]["tmp_name"], $imagePath);
				$image = new Image($images_dir. $imageName);
				$image->resize(815, null, true);
				$image->crop($w, $h, $x, $y, null);
				if($ext == 'png')
					$image->save( $imagePath, 5);
				else
					$image->save( $imagePath);
				require_once 'amazonUpload.php';
				uploadImage($imagePath, $_FILES["cover-image"]["type"]);*/
				$res = Api::saveUserImage('https://d2o7safqof0chb.cloudfront.net/'.$imagePath, 'cover',$userId);
				echo json_encode($res);
			}
		} else if(isset($_FILES["profile-image"])) {
			/*$w = $_POST['w'];
			$h = $_POST['h'];
			$x = $_POST['x'];
			$y = $_POST['y'];*/
			$file_parts = pathinfo($_FILES["profile-image"]["name"]);
			//$ext = $file_parts['extension'];
			$ext = 'jpg';
			if( !correctFileExt($ext)) {
				echo "Error: Only JPG, JPEG files are allowed.";
			}else if ($_FILES["profile-image"]["error"] > 0) {
				echo "Error: " . $_FILES["profile-image"]["error"] . "<br>";
			} else {
				$imageName = "profile-image-{$userId}-{$time}.{$ext}";
				$imagePath = $images_dir. $imageName;
				//move_uploaded_file($_FILES["profile-image"]["tmp_name"], $imagePath);
				require_once 'amazonUpload.php';
				uploadFile($imagePath, $_FILES["profile-image"]["tmp_name"], $_FILES["profile-image"]["type"]);
				$res = Api::saveUserImage('https://d2o7safqof0chb.cloudfront.net/'.$imagePath, 'profile',$userId);
				//$res->imageName = 'https://d2o7safqof0chb.cloudfront.net/'.$imagePath;
				echo json_encode($res);
			}
		} else if(isset($_FILES["package-image"])) {
		
			/*$w = $_POST['w'];
			$h = $_POST['h'];
			$x = $_POST['x'];
			$y = $_POST['y'];*/
			$file_parts = pathinfo($_FILES["package-image"]["name"]);
			//$ext = $file_parts['extension'];
			$ext = 'jpg';
			if( !correctFileExt($ext)) {
				echo "Error: Only JPG, JPEG, PNG files are allowed.";
			}else if ($_FILES["package-image"]["error"] > 0) {
				echo "Error: " . $_FILES["package-image"]["error"] . "<br>";
			} else {
				/*if(!isset($_POST['courseId'])){
					echo "Invalid request";
					die();
				}*/
				$packId = $_POST['packId'];
				//unset($_SESSION['courseId']);
				$imageName = "package-image-{$time}.{$ext}";
				//$imageName = "package-image-{$packId}-{$time}.{$ext}";
				$imagePath = $images_dir. $imageName;
				//move_uploaded_file($_FILES["course-image"]["tmp_name"], $imagePath);
				require_once 'amazonUpload.php';
				uploadFile($imagePath, $_FILES["package-image"]["tmp_name"], $_FILES["package-image"]["type"]);

				$imagePath1 = $public_images_dir. $imageName;
				require_once 'amazonPublicUpload.php';
				uploadFilePublic($imagePath1, $_FILES["package-image"]["tmp_name"], $_FILES["package-image"]["type"]);
				
				/*$image = new Image($images_dir. $imageName);
				$image->resize(382, null, true);
				$image->crop($w, $h, $x, $y, null);
				if($ext == 'png')
					$image->save( $imagePath, 5);
				else
					$image->save( $imagePath );*/
				$data = new stdClass();
				$data->imageName = 'https://d2o7safqof0chb.cloudfront.net/'.$imagePath;
				//$data->packId = $packId;
				$data->userId = $userId;
				$data->status = 1;
				
				//$res = Api::savePackageImage($data);
				echo json_encode($data);
			}
		} else if(isset($_FILES["course-image"])) {
			/*$w = $_POST['w'];
			$h = $_POST['h'];
			$x = $_POST['x'];
			$y = $_POST['y'];*/
			$file_parts = pathinfo($_FILES["course-image"]["name"]);
			//$ext = $file_parts['extension'];
			$ext = 'jpg';
			if( !correctFileExt($ext)) {
				echo "Error: Only JPG, JPEG, PNG files are allowed.";
			}else if ($_FILES["course-image"]["error"] > 0) {
				echo "Error: " . $_FILES["course-image"]["error"] . "<br>";
			} else {
				if(!isset($_POST['courseId'])){
					echo "Invalid request";
					die();
				}
				$courseId = $_POST['courseId'];
				//unset($_SESSION['courseId']);
				$imageName = "course-image-{$courseId}-{$time}.{$ext}";
				$imagePath = $images_dir. $imageName;
				//move_uploaded_file($_FILES["course-image"]["tmp_name"], $imagePath);
				require_once 'amazonUpload.php';
				uploadFile($imagePath, $_FILES["course-image"]["tmp_name"], $_FILES["course-image"]["type"]);

				$imagePath1 = $public_images_dir. $imageName;
				require_once 'amazonPublicUpload.php';
				uploadFilePublic($imagePath1, $_FILES["course-image"]["tmp_name"], $_FILES["course-image"]["type"]);
				
				/*$image = new Image($images_dir. $imageName);
				$image->resize(382, null, true);
				$image->crop($w, $h, $x, $y, null);
				if($ext == 'png')
					$image->save( $imagePath, 5);
				else
					$image->save( $imagePath );*/
				$data = new stdClass();
				$data->imageName = 'https://d2o7safqof0chb.cloudfront.net/'.$imagePath;
				$data->courseId = $courseId;
				$data->userId = $userId;
				$res = Api::saveCourseImage($data);
				//$res->imageName = 'https://d2o7safqof0chb.cloudfront.net/'.$imagePath;
				echo json_encode($res);
			}
		} else if(isset($_FILES["subject-image"])) {
			/*$w = $_POST['w'];
			$h = $_POST['h'];
			$x = $_POST['x'];
			$y = $_POST['y'];*/
			$file_parts = pathinfo($_FILES["subject-image"]["name"]);
			//$ext = $file_parts['extension'];
			$ext = 'jpg';
			if( !correctFileExt($ext)) {
				echo "Error: Only JPG, JPEG, PNG files are allowed.";
			}else if ($_FILES["subject-image"]["error"] > 0) {
				echo "Error: " . $_FILES["subject-image"]["error"] . "<br>";
			} else {
				if(!isset($_POST['subjectId'])) {
					echo "Invalid request";
					die();
				}
				$subjectId = $_POST['subjectId'];
				//unset($_SESSION['subjectId']);
				$imageName = "subject-image-{$subjectId}-{$time}.{$ext}";
				$imagePath = $images_dir. $imageName;
				//move_uploaded_file($_FILES["subject-image"]["tmp_name"], $imagePath);
				require_once 'amazonUpload.php';
				uploadFile($imagePath, $_FILES["subject-image"]["tmp_name"], $_FILES["subject-image"]["type"]);

				/*$image = new Image($images_dir. $imageName);
				$image->resize(382, null, true);
				$image->crop($w, $h, $x, $y, null);
				if($ext == 'png')
					$image->save( $imagePath, 5);
				else
					$image->save( $imagePath );*/
				$data = new stdClass();
				$data->imageName = 'https://d2o7safqof0chb.cloudfront.net/'.$imagePath;
				$data->subjectId = $subjectId;
				$data->userId = $userId;
				$res = Api::saveSubjectImage($data);
				echo json_encode($res);
			}
		} else if(isset($_FILES["course-syllabus"])) {
			$subjectId = isset($_POST['subjectId']) ? $_POST['subjectId'] : 0;
			$result = new stdClass();
			//$courseId = $_POST['courseId'];
			$FILE = $_FILES['course-syllabus'];
			if($FILE['type'] == 'application/pdf' ) {
				$filesDir = "user-data/syllabus/";
				$file_parts = pathinfo($FILE["name"]);
				$ext = $file_parts['extension'];
				$fileName = "syllabus-{$subjectId}-".time().".{$ext}";
				$filePath = $filesDir. $fileName;
				//require_once 'amazonPublicUpload.php';
				//$url = uploadFilePublic($filePath, $FILE["tmp_name"], $FILE["type"]);				
				require_once 'amazonUpload.php';
				uploadFile($filePath, $FILE["tmp_name"], $FILE["type"]);
				//saving in db
				$data = new stdClass();
				$data->metadata = '';//$metadata;
				$data->syllabus = 'https://d2o7safqof0chb.cloudfront.net/'.$filePath;
				@session_start();
				$data->userId = $_SESSION['userId'];
				$data->userRole = $_SESSION['userRole'];
				$data->subjectId=$subjectId;
				//print_r($data);
				$res = Api::saveSubjectSyllabus($data);
				echo json_encode($res);
			}
		} else if(isset($_FILES["course-doc"])) {
			$courseId = isset($_POST['courseId']) ? $_POST['courseId'] : 0;
			$result = new stdClass();
			//$courseId = $_POST['courseId'];
			$FILE 			= $_FILES['course-doc'];
			if($FILE['type'] == 'application/pdf' ) {
				$filesDir 	= "user-data/syllabus/";
				$file_parts = pathinfo($FILE["name"]);
				$ext 		= $file_parts['extension'];
				$fileName = "Course-Doc-{$courseId}-".time().".{$ext}";
				$filePath = $filesDir. $fileName;
				//require_once 'amazonPublicUpload.php';
				//$url = uploadFilePublic($filePath, $FILE["tmp_name"], $FILE["type"]);				
				require_once 'amazonUpload.php';
				uploadFile($filePath, $FILE["tmp_name"], $FILE["type"]);
				//saving in db
				$data = new stdClass();
				$data->metadata = '';//$metadata;
				$data->coursesDoc = 'https://d2o7safqof0chb.cloudfront.net/'.$filePath;
				@session_start();
				$data->userId	 = $_SESSION['userId'];
				$data->userRole  = $_SESSION['userRole'];
				$data->courseId  = $courseId;
				//print_r($data);
				$res = Api::savecourseDoc($data);
				echo json_encode($res);
			}
		} else if(isset($_FILES["uploaded-content-stuff"])) {
			// if( !correctFileExt($ext)) {
				// echo "Error: Only JPG, JPEG, PNG files are allowed.";
			// }else 
			
			if ($_FILES["uploaded-content-stuff"]["error"] > 0) {
				
				$data = new stdClass();
				$data->status = 0;
				$data->message = "There was some error";
				echo json_encode($data);
				//echo "Error: " . $_FILES["uploaded-content-stuff"]["error"] . "<br>";
			} else {
				$filesDir = "user-data/files/";
				$contentId = isset($_POST['contentId']) ? $_POST['contentId'] : 0;
				$contentType = isset($_POST['contentType']) ? $_POST['contentType'] : 'video';
				$fileRealName = isset($_POST['fileName']) ? $_POST['fileName'] : 'none';
				$realName = $_FILES["uploaded-content-stuff"]["name"];
				$file_parts = pathinfo($_FILES["uploaded-content-stuff"]["name"]);
				$ext = $file_parts['extension'];

				$fileName = "content-{$contentId}.{$ext}";
				$filePath = $filesDir. $fileName;
				//move_uploaded_file($_FILES["uploaded-content-stuff"]["tmp_name"], $filePath);
				require_once 'amazonUpload.php';
				uploadFile($filePath, $_FILES["uploaded-content-stuff"]["tmp_name"], $_FILES["uploaded-content-stuff"]["type"]);
				$data = new stdClass();
				/*if($contentType == 'video') {
					require_once $libPath . '/getid3/getid3.php';
					$getID3 = new getID3();
					$ThisFileInfo = $getID3->analyze($filePath);
					$metadata = 'Play Time ' . $ThisFileInfo['playtime_string'];
				} else if($contentType == 'ppt') {
					$count = getNumPagesPdf($filePath);
					$metadata = $count . ' Slides';
					
				} else if($contentType == 'doc') {
					$count = getNumPagesPdf($filePath);
					$metadata = $count . ' Pages';
				} else {
					$metadata = '';
				}*/
				$data->metadata = '';//$metadata;
				$data->stuff = 'https://d2o7safqof0chb.cloudfront.net/'.$filePath;
				$data->realName = $realName;
				$data->contentId = $contentId;
				$data->contentType = $contentType;
				$data->fileRealName = $fileRealName;
				$res = Api::saveContentStuff($data);
				echo json_encode($res);
			}
		} else if(isset($_FILES["uploaded-supplementary-stuff"])) {
			$filesDir = "user-data/files/";
			$contentId = isset($_POST['contentId']) ? $_POST['contentId'] : 0;
			$contentType = isset($_POST['contentType']) ? $_POST['contentType'] : 'video';
			$file = isset($_POST['fileName']) ? $_POST['fileName'] : 'none';
			$fileRealName = str_replace(' ', '_', $file);;
			$realName = $_FILES["uploaded-supplementary-stuff"]["name"];
			$file_parts = pathinfo($_FILES["uploaded-supplementary-stuff"]["name"]);
			$ext = $file_parts['extension'];
			// if( !correctFileExt($ext)) {
				// echo "Error: Only JPG, JPEG, PNG files are allowed.";
			// }else 
			
			if ($_FILES["uploaded-supplementary-stuff"]["error"] > 0) {
				echo "Error: " . $_FILES["uploaded-supplementary-stuff"]["error"] . "<br>";
			} else {
				$data = new stdClass();
				$data->fileExt = $ext;
				$data->realName = $realName;
				$data->contentId = $contentId;
				$data->contentType = $contentType;
				$data->fileRealName = 'https://d2o7safqof0chb.cloudfront.net/'.'user-data/files/'.'_'.time().$fileRealName;
				$res = Api::saveSupplementaryStuff($data);
				//move_uploaded_file($_FILES["uploaded-supplementary-stuff"]["tmp_name"], $filesDir . $res->generatedFileName);
				require_once 'amazonUpload.php';
				uploadFile('user-data/files/'.'_'.time().$fileRealName, $_FILES["uploaded-supplementary-stuff"]["tmp_name"], $_FILES["uploaded-supplementary-stuff"]["type"]);
				$tempRes = new stdClass();
				$tempRes->id = $res->fileId;
				$tempRes->status = $res->status;
				$tempRes->fileRealName = $res->fileRealName;
				echo json_encode($tempRes);
			}
		} else if(isset($_FILES['demo-course-video'])) {
			$result = new stdClass();
			$courseId = $_POST['courseId'];
			$FILE = $_FILES['demo-course-video'];
			if($FILE['type'] == 'video/mp4' || $FILE['type'] == 'video/mov') {
				$filesDir = "marketing-videos/";
				$file_parts = pathinfo($FILE["name"]);
				$ext = $file_parts['extension'];

				$fileName = "demo-{$courseId}-".time().".{$ext}";
				$filePath = $filesDir. $fileName;
				require_once 'amazonPublicUpload.php';
				$url = uploadFilePublic($filePath, $FILE["tmp_name"], $FILE["type"]);

				//now updating database
				$data = new stdClass();
				@session_start();
				$data->userId = $_SESSION['userId'];
				$data->userRole = $_SESSION['userRole'];
				$data->courseId = $courseId;
				$data->demoFile = $url;
				$data->previousVideo = $_POST['demoFlag'];
				$result = Api::saveDemoVideoForCourse($data);
				echo json_encode($result);
			}else {
				$result->status = 0;
				$result->message = 'Only mp4 and mov files are allowed';
				echo json_encode($result);
			}
		} else if(isset($_FILES["portfolio-image"])) {
			$file_parts = pathinfo($_FILES["portfolio-image"]["name"]);
			$ext = 'jpg';
			if( !correctFileExt($ext)) {
				echo "Error: Only JPG, JPEG files are allowed.";
			}else if ($_FILES["portfolio-image"]["error"] > 0) {
				echo "Error: " . $_FILES["portfolio-image"]["error"] . "<br>";
			} else {
				$imageName = 'portfolio'.time().'.jpg';
				$imagePath = $images_dir.$imageName;
				require_once 'amazonPublicUpload.php';
				$data = new stdClass();
				$data->image = uploadFilePublic($imagePath, $_FILES["portfolio-image"]["tmp_name"], $_FILES["portfolio-image"]["type"]);
				$data->userId = $userId;
				$data->status = 1;
				echo json_encode($data);
			}
		} else if(isset($_FILES["portfolio-image-update"])) {
			$file_parts = pathinfo($_FILES["portfolio-image-update"]["name"]);
			$ext = 'jpg';
			if( !correctFileExt($ext)) {
				echo "Error: Only JPG, JPEG files are allowed.";
			}else if ($_FILES["portfolio-image-update"]["error"] > 0) {
				echo "Error: " . $_FILES["portfolio-image-update"]["error"] . "<br>";
			} else {
				$imageName = 'portfolio'.time().'.jpg';
				$imagePath = $images_dir.$imageName;
				/*require_once 'amazonPublicUpload.php';
				uploadFilePublic($imagePath, $_FILES["portfolio-image-update"]["tmp_name"], $_FILES["portfolio-image-update"]["type"]);*/
				$data = new stdClass();
				require_once 'amazonUpload.php';
				uploadFile($imagePath, $_FILES["portfolio-image-update"]["tmp_name"], $_FILES["portfolio-image-update"]["type"]);
				$res = Api::savePortfolioImage('https://d2o7safqof0chb.cloudfront.net/'.$imagePath, $_SESSION['subdomain'],$userId);
				$data->userId = $userId;
				$data->status = 1;
				$data->image = $res->image;
				echo json_encode($data);
			}
		} else if(isset($_FILES["fileOHImage"])) {
			$filename = $_FILES["fileOHImage"]["name"];
			$ext 	  = pathinfo($filename, PATHINFO_EXTENSION);
			if( !correctFileExt($ext)) {
				echo "Error: Only JPG, JPEG, PNG files are allowed.";
			}else if ($_FILES["fileOHImage"]["error"] > 0) {
				echo "Error: " . $_FILES["fileOHImage"]["error"] . "<br>";
			} else {
				$imageName = 'portfolioHonors'.time().'.jpg';
				$imagePath = $images_dir.$imageName;
				// echo getcwd(); -- C:\wamp\www\arcanemind
				require_once 'amazonPublicUpload.php';
				$data = new stdClass();
				$data->status = 1;
				$data->imageName = uploadFilePublic($imagePath, $_FILES["fileOHImage"]["tmp_name"], $_FILES["fileOHImage"]["type"]);
				$data->userId = $userId;
				echo json_encode($data);
			}
		} else if(isset($_FILES["fileFMImage"])) {
			$filename = $_FILES["fileFMImage"]["name"];
			$ext 	  = pathinfo($filename, PATHINFO_EXTENSION);
			if( !correctFileExt($ext)) {
				echo "Error: Only JPG, JPEG, PNG files are allowed.";
			}else if ($_FILES["fileFMImage"]["error"] > 0) {
				echo "Error: " . $_FILES["fileFMImage"]["error"] . "<br>";
			} else {
				$imageName = 'portfolioFaculty'.time().'.jpg';
				$imagePath = $images_dir.$imageName;
				// echo getcwd(); -- C:\wamp\www\arcanemind
				require_once 'amazonPublicUpload.php';
				$data = new stdClass();
				$data->status = 1;
				$data->imageName = uploadFilePublic($imagePath, $_FILES["fileFMImage"]["tmp_name"], $_FILES["fileFMImage"]["type"]);
				$data->userId = $userId;
				echo json_encode($data);
			}
		} else if(isset($_FILES["fileGIImage"])) {
			$filename = $_FILES["fileGIImage"]["name"];
			$ext 	  = pathinfo($filename, PATHINFO_EXTENSION);
			if( !correctFileExt($ext)) {
				echo "Error: Only JPG, JPEG, PNG files are allowed.";
			}else if ($_FILES["fileGIImage"]["error"] > 0) {
				echo "Error: " . $_FILES["fileGIImage"]["error"] . "<br>";
			} else {
				$imageName = 'portfolioFaculty'.time().'.jpg';
				$imagePath = $images_dir.$imageName;
				// echo getcwd(); -- C:\wamp\www\arcanemind
				require_once 'amazonPublicUpload.php';
				$data = new stdClass();
				$data->status = 1;
				$data->imageName = uploadFilePublic($imagePath, $_FILES["fileGIImage"]["tmp_name"], $_FILES["fileGIImage"]["type"]);
				$data->userId = $userId;
				echo json_encode($data);
			}
		} else if(isset($_FILES["fileFavicon"])) {
			$filename  = $_FILES["fileFavicon"]["name"];
			$ext 	   = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
			$imagesize = getimagesize($_FILES["fileFavicon"]["tmp_name"]);
			if ($_FILES["fileFavicon"]["error"] > 0) {
				echo "Error: " . $_FILES["fileFavicon"]["error"] . "<br>";
			} else if($_FILES["fileFavicon"]["type"] != 'image/png') {
				echo "Error: Only PNG files are allowed.";
			} else if (($imagesize[0]!=16 && $imagesize[0]!=32) || (($imagesize[0]/$imagesize[1])!=1)) {
				echo "Only 16x16 or 32x32 sizes allowed";
			} else {
				$imageName = 'favicon'.time().'.jpg';
				$imagePath = $images_dir.$imageName;
				require_once 'amazonPublicUpload.php';
				$data = new stdClass();
				$data->status = 1;
				$data->imageName = uploadFilePublic($imagePath, $_FILES["fileFavicon"]["tmp_name"], $_FILES["fileFavicon"]["type"]);
				$data->userId = $userId;
				echo json_encode($data);
			}
		} else if(isset($_FILES["profile-pic"])) {
			$imageType = $_FILES["profile-pic"]["type"];
			if ($imageType == "image/jpeg" || "image/pjpeg") {
				$ext = 'jpg';
			} else if ($imageType == "image/png") {
				$ext = 'png';
			}
			if( !correctFileExt($ext)) {
				echo "Error: Only JPG, JPEG, PNG files are allowed.";
			}else if ($_FILES["profile-pic"]["error"] > 0) {
				echo "Error: " . $_FILES["profile-pic"]["error"] . "<br>";
			} else {
				$imageName = "profile-pic-{$userId}-{$time}.{$ext}";
				$imagePath = $images_dir. $imageName;
				require_once 'amazonUpload.php';
				uploadFile($imagePath, $_FILES["profile-pic"]["tmp_name"], $_FILES["profile-pic"]["type"]);
				$res = Api::saveUserProfilePic('https://d2o7safqof0chb.cloudfront.net/'.$imagePath, $userId);
				echo json_encode($res);
			}
		} else if(isset($_FILES["submission-upload"])) {
			/*$w = $_POST['w'];
			$h = $_POST['h'];
			$x = $_POST['x'];
			$y = $_POST['y'];*/
			$file_parts = pathinfo($_FILES["submission-upload"]["name"]);
			$file_size = $_FILES['submission-upload']['size'];
			//$ext = $file_parts['extension'];
			$ext = $file_parts['extension'];
			if( maliciousFileExt($ext)) {
				$result = new stdClass();
				$result->status  = 0;
				$result->message = "Error: Malicious file type found, please try some other file type.";
				echo json_encode($result);
			} else if ($file_size > 31457280) {
				$result = new stdClass();
				$result->status  = 0;
				$result->message = "File size is greater than 30MB, please upload a smaller file.";
				echo json_encode($result);
			} else if ($_FILES["submission-upload"]["error"] > 0) {
				$result->status  = 0;
				$result->message = "Error: " . $_FILES["submission-upload"]["error"] . "<br>";
				echo json_encode($result);
			} else {
				if(!isset($_POST['examId'])){
					echo "Invalid request";
					die();
				}
				$examId = $_POST['examId'];

				//unset($_SESSION['examId']);
				$filesDir = "user-data/files/";
				$fileName = "submission-upload-{$examId}-{$time}.{$ext}";
				$filePath = $filesDir. $fileName;
				//move_uploaded_file($_FILES["submission-upload"]["tmp_name"], $filePath);
				
				require_once 'amazonPublicUpload.php';
				$data = new stdClass();
				$data->fileName = uploadFilePublic($filePath, $_FILES["submission-upload"]["tmp_name"], $_FILES["submission-upload"]["type"]);
				$data->examId = $examId;
				$data->userId = $userId;
				//$res = Api::saveCourseImage($data);
				echo json_encode($data);
			}
		} else if(isset($_FILES["fileSocialImage"])) {
			$file_parts = pathinfo($_FILES["fileSocialImage"]["name"]);
			$ext = 'jpg';
			if( !correctFileExt($ext)) {
				echo "Error: Only JPG, JPEG files are allowed.";
			} else if ($_FILES["fileSocialImage"]["error"] > 0) {
				echo "Error: " . $_FILES["fileSocialImage"]["error"] . "<br>";
			} else {
				$imageName = 'social'.time().'.jpg';
				$imagePath = $images_dir.$imageName;
				require_once 'amazonPublicUpload.php';
				$data = new stdClass();
				$data->image = uploadFilePublic($imagePath, $_FILES["fileSocialImage"]["tmp_name"], $_FILES["fileSocialImage"]["type"]);
				$data->userId = $userId;
				$data->status = 1;
				echo json_encode($data);
			}
		}
		die();
	} catch(Exception $ex) {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	}
	function image_data($gdimage)
	{
	    ob_start();
	    imagejpeg($gdimage);
	    return(ob_get_clean());
	}
	function getNumPagesPdf($filepath) {
		$fp = fopen(preg_replace("/\[(.*?)\]/i", "", $filepath), "r");
		$max = 0;
		if (!$fp) {
		    return "0";
		} else {
		    while (!@feof($fp)) {
		        $line = @fgets($fp, 255);
		        if (preg_match('/\/Count [0-9]+/', $line, $matches)) {
		            preg_match('/[0-9]+/', $matches[0], $matches2);
		            if ($max < $matches2[0]) {
		                $max = trim($matches2[0]);
		                break;
		            }
		        }
		    }
		    @fclose($fp);
		}
		return $max;
	}
?>