<?php
	try{
                /**
		* This makes our life easier when dealing with paths. Everything is relative
		* to the application root now.
		*/
		//~ ini_set('display_errors',1);
		//~ ini_set('display_startup_errors',1);
		//~ error_reporting(-1);
		chdir(dirname(__DIR__));

		$libPath = "Vendor";
		
		// Loading Zend
		include $libPath . '/Zend/Loader/AutoloaderFactory.php';
		Zend\Loader\AutoloaderFactory::factory(array(
				'Zend\Loader\StandardAutoloader' => array(
						'autoregister_zf' => true,
						'db' => 'Zend\Db\Sql'
				)
		));

		// Get the JSON request
		$req = json_decode(file_get_contents("php://input"));
		
		//Kill the request if action is not specified
		if(!isset($req->action)) {
			$res = new stdClass();
			$res->status = 0;
			$res->message = "No action specified";
			echo json_encode($res);
			die();
		}
		
		require $libPath . "/ArcaneMind/Api.php";
		
		//Used for verification email
		$siteBase = "http://dev.am/";
		$siteBase = "http://www.integro.io/1testing1/develop/perf/";
		
		
		$action = $req->action;
		switch($action) {
			case 'search-page':
				$res = Api::searchCourseForPage($req);
				break;
			case 'search-course':
				$res = Api::searchCourse($req);
				break;
			case 'featured-template-courses':
				$res = Api::getFeaturedTemplateCourse($req);
				break;
			case 'template-courses':
				$res = Api::getTemplateCourse($req);
				break;
			case 'student-template-courses':
				$res = Api::getStudentTemplateCourse($req);
				break;
			case 'content-template-courses':
				$res = Api::getContentTemplateCourse($req);
				break;
			case 'page-template-courses':
				$res = Api::getPageTemplateCourse($req);
				break;
			case 'register-short':
				$res = Api::registerUserShort($req);
				break;
			case 'register':
				$res = Api::registerUser($req);
				break;
			case 'resend-verification-link':
				$res = Api::resendVerificationLink($req);
				break;
			case 'validate-email':
				$res = Api::validateEmail($req);
				break;
			case 'validate-username':
				$res = Api::validateUsername($req);
				break;
			case 'account-validation':
				$res = Api::validateAccount($req);
				break;
			case 'get-profile-details':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION["userRole"];
				if(isset($req->instituteId)){
					$req->userId=$req->instituteId;
					$req->userRole=1;
				}
				$res = Api::getProfileDetails($req);
				$res->userRole = $req->userRole;
				break;
                        
			case 'edit-tagline':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::saveTagline($req);
				break;    
			case 'login':
				$res = Api::login($req);
				if($res->valid == false)
					break;
				session_start();
				$_SESSION["userId"] = $res->userId;
				$_SESSION["userRole"] = $res->userRole;
				//performance improvement ARC-360
				$_SESSION['username'] = $res->username;
				break;
			case 'back-login':
				$res = Api::backLogin($req);
				if($res->valid == false)
					break;
				session_start();
				$_SESSION["userId"] = $res->userId;
				$_SESSION["userRole"] = $res->userRole;
				$_SESSION['username'] = $res->username;
				break;
			case 'send-reset-password-link':
				$res = Api::sendResetPasswordLink($req);
				break;
			case 'reset-password':
				$res = Api::resetPassword($req);
				break;
			case  'update-profile-desc':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateProfileDescription($req);
				break;
			case  'update-profile-general':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateProfileGeneral($req);
				break;
			case  'update-user-institute-type':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateUserInstituteType($req);
				break;
			case  'create-course':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::createCourse($req);
				break;
			case  'get-course-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getCourseDetails($req);
				break;
			case  'update-course':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateCourse($req);
				break;
			case  'update-course-licensing':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateCourseLicensing($req);
				break;
			case  'create-subject':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::createSubject($req);
				break;
			case  'get-course-subjects':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getCourseSubjects($req);
				break;
			case  'get-subject-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectDetails($req);
				break;
			case  'update-subject':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateSubject($req);
				break;
			case  'get-all-courses':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getAllCourses($req);
				break;
			case  'create-chapter':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::createChapter($req);
				break;
			case  'get-subject-chapters':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectChapters($req);
				break;
			case  'get-courses-for-import':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getCoursesForImport($req);
				break;
                        case  'get-purchased-courses-for-import':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getPurchasedCoursesForImport($req);
				break;    
                            
			case  'import-subjects-to-course':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::importSubjectsToCourse($req);
				break;
			case  'get-all-courses-admin':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				if($req->userRole != 5) {
					$res = new stdClass();
					$res->status = 0;
					$res->message = "Invalid Action";
					break;
				}
				$res = Api::getAllCoursesForApproval($req);
				break;
                        case  'get-all-deleted-course':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				if($req->userRole != 5) {
					$res = new stdClass();
					$res->status = 0;
					$res->message = "Invalid Action";
					break;
				}
				$res = Api::getAlldeletedCourse($req);
				break;
                               
                          case  'restore-course-admin':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				if($req->userRole != 5) {
					$res = new stdClass();
					$res->status = 0;
					$res->message = "Invalid Action";
					break;
				}
				$res = Api::restoreCourseAdmin($req);
				break;
                                
			case  'approve-course':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				if($req->userRole != 5) {
					$res = new stdClass();
					$res->status = 0;
					$res->message = "Invalid Action";
					break;
				}
				$res = Api::approveCourse($req);
				break;
			case  'reject-course':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				if($req->userRole != 5) {
					$res = new stdClass();
					$res->status = 0;
					$res->message = "Invalid Action";
					break;
				}
				$res = Api::rejectCourse($req);
				break;
			case  'delete-course':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteCourse($req);
				break;
			case  'restore-course':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::restoreCourse($req);
				break;
			case  'delete-subject':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteSubject($req);
				break;
			case  'restore-subject':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::restoreSubject($req);
				break;
			case  'update-chapter-order':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateChapterOrder($req);
				break;
			case  'get-institute-professors':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getInstituteProfessors($req);
				break;
				
			case  'update-subject-professors':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateSubjectProfessors($req);
				break;
				
			case 'delete-single-professor':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteSubjectProfessor($req);
				break;
			
			case  'get-institute-prof-with-sub':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getInstituteProfessorsWithSubjects($req);
				break;
			
			case  'delete-subject-professor':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteSubjectProfessor($req);
				break;
			
			case  'request-course-approval':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::requestCourseApproval($req);
				break;
			
			case  'update-course-order':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateCourseOrder($req);
				break;
			
			case  'update-subject-order':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateSubjectOrder($req);
				break;
			
			case  'save-heading':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::saveHeading($req);
				break;
			case  'delete-heading':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteHeading($req);
				break;
			case  'save-content':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::saveContent($req);
				break;
			case  'delete-content':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteContent($req);
				break;
			case  'get-chapter-content':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getAllContent($req);
				break;
			case  'save-text-stuff':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$req->contentType = 'text';
				$req->fileRealName = 'Text';
				$req->metadata = '';
				$res = Api::saveContentStuff($req);
				break;
			case  'save-link-stuff':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$req->contentType = $req->contentType;
				$req->fileRealName = 'Link';
				$req->metadata = '';
				$res = Api::saveContentStuff($req);
				break;
			case  'update-published-status':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$req->status = $req->status == "1" ? '1' : '0';
				$res = Api::updatePublishedStatus($req);
				break;
			case  'update-downloadable-status':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$req->status = $req->status == "1" ? '1' : '0';
				$res = Api::updateDownloadableStatus($req);
				break;
			case  'save-stuff-desc':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::saveStuffDesc($req);
				break;
			case  'update-content-order':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateContentOrder($req);
				break;
			case  'get-chapter-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getChapterDetails($req);
				break;
			case  'get-chapter-details-for-view':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getChapterDetailsForView($req);
				break;
			case  'delete-content-stuff':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$req->publicAccess = true;
				$res = Api::deleteContentStuff($req);
				break;
			case  'delete-supp-stuff':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteSupplementaryStuff($req);
				break;
			case 'get-all-institute-courses':
				session_start();
				$req->userId = $_SESSION["userId"];
				$res = Api::getAllInstituteCoursesForTree($req);
				break;
			case 'get-all-institute-subjects':
				session_start();
				$req->userId = $_SESSION["userId"];
				$res = Api::getAllInstituteSubjectsForTree($req);
				break;
			case 'get-all-institute-chapters':
				session_start();
				$req->userId = $_SESSION["userId"];
				$res = Api::getAllInstituteChaptersForTree($req);
				break;
			case  'get-courses-for-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getCoursesForExam($req);
				break;
			case  'get-subjects-for-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectsForExam($req);
				break;
			case  'get-exam-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getExamDetails($req);
				break;
			case 'get-chapters-for-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getChaptersForExam($req);
				break;
			case 'get-assignments-for-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getAssignmentsForExam($req);
				break;
			case 'add-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::addExam($req);
				break;
			case 'add-template':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::addTemplate($req);
				break;
			case 'get-exam-sections':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getExamSections($req);
				break;
			case 'get-exam-name':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getExamName($req);
				break;
			case 'get-section-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSectionDetails($req);
				break;
			case 'save-categories':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::saveCategories($req);
				break;
			case 'save-question':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::saveQuestion($req);
				break;
			case 'get-question-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getQuestionDetails($req);
				break;
			case 'edit-question':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::editQuestion($req);
				break;
			case 'get-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getExam($req);
				break;
			case 'edit-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::editExam($req);
				break;
			case 'delete-question':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteQuestion($req);
				break;
			case 'delete-category':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteCategory($req);
				break;
			case 'delete-section':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteSection($req);
				break;
			case 'delete-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteExam($req);
				break;
                         case 'copy-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::copyExam($req);
				break;   
			case 'get-section-status':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSectionStatus($req);
				break;
			case 'make-live':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::makeLive($req);
				break;
			case 'make-unlive':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::makeUnlive($req);
				break;
			case 'get-templates-for-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getTemplateForExam($req);
				break;
			case 'get-template-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getTemplateDetails($req);
				break;
			case 'get-template':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getTemplate($req);
				break;
            case 'get-individual-details':
				session_start();
				$req->userId = $req->uid;
				$req->userRole = $req->userRole;
				$res = Api::getProfileDetails($req);
				break;
			case 'update-invitation':
				$req->sno = $req->sno;
				$res = Api::updateInvitation($req);
            case 'get-role':
				session_start();
				$res = Api::getRoleId($req->uid);
				break;
            case 'get-invited-professors-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getInvitedProfessors($req);
				break;
			case  'fetch-professor-details':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::fetchProfessorDetails($req);
				break;
            case  'invite-professor':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::inviteProfessor($req);
                break;
			case  'edit-template':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::editTemplate($req);
                break;
			case  'delete-template':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::deleteTemplate($req);
                break;
			case  'get-questions-for-import':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getQuestionsForImport($req);
                break;
			case  'import-questions':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::importQuestions($req);
                break;
			case  'restore-section':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::restoreSection($req);
                break;
			case  'restore-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::restoreExam($req);
                break;
			case  'get-breadcrumb-for-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getBreadcrumbForExam($req);
                break;
			case  'get-breadcrumb-for-add':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getBreadcrumbForAdd($req);
                break;
			case  'get-course-date':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getCourseDate($req);
                break;
			case  'check-name-for-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkNameForExam($req);
                break;
			case  'check-name-for-template':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkNameForTemplate($req);
                break;
			/* course key invitation module */
			case 'get-course-keys-details':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getCoursekeyDetails($req);
				break;
			case 'purchase_key':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::purchase_key($req);
				break;
                       
                        case 'keys-purchase_invitation':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api:: keysPurchaseInvitation($req);
				break;    
			case 'back_end_course_keys':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::backendCoursekeys($req);
				break;
			case 'change_key_rate':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::change_key_rate($req);
				break;
			case 'setInstitute_rate_back':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::setInstitute_rate_back($req);
				break;
			case 'add_keys':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::add_keys($req);
				break;
			case 'set_default_key_rate':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::set_default_key_rate($req);
				break;
			case 'send_activation_link_student':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::send_activation_link_student($req);
				break;
			case 'verify_activation_link_student':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::verify_activation_link_student($req);
				break;
			case 'student_invitation_details':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::student_invitation_details($req);
				break;
			case 'resend_invitation_link':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::resend_invitation_link($req);
				break;
			case 'load_student_notification':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::load_student_notification($req);
				break;
			case 'accepted_student_invitation':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::accepted_student_invitation($req);
				break;
			case 'rejected_student_invitation':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::reject_student_invitation($req);
				break;
				;
			case 'reject_link_by_admin':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::reject_link_by_admin($req);
				break;
			case 'keys_avilable':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::keys_avilable($req);
				break;
			case 'get_students_of_course':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::get_students_of_course($req);
				break;
			case 'get_course_student_details':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::get_course_student_details($req);
				break;
			case 'get_students_details_of_course':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::get_students_details_of_course($req);
				break;
			case 'view_institutes_info':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::view_institutes_info($req);
				break;

			case 'fetch-applicable-licenses':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::fetchApplicableLicenses($req);
				break;
                            
                        case 'fetch-applicable-licenses-FrontPage':
				session_start();
				$res = Api::getLicensesONCourse($req);
				break;
                        
			case 'modify-licenses':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::modify_licenses($req);
				break;

			/*Taxes*/
			case 'fetch-all-taxes': 
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::fetchAllTaxes($req);
				break;
			case 'fetch-default-taxes': 
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::fetchDefaultTaxes($req);
				break;
			case 'update-institute-taxes':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::updateInstituteTaxes($req);
				break;
			case 'update-default-tax':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::updateDefaultTax($req);
				break;
			case 'delete-default-tax':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::deleteDefaultTax($req);
				break;
			case 'insert-default-tax':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::insertDefaultTax($req);
				break;

                        case 'Student_Details_WithCourses':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::StudentDetailsWithCourses($req);
                                break; 
                        case 'Courses_for_student':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::CourseforStudent($req);
                                break;  
                        case 'viewInstitutesLoginInfo':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::viewInstitutesLoginInfo($req);
                                break; 
                       case 'ChangeLoginstatus':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::ChangeLoginstatus($req);
                                break;     
                        case 'changePassword':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::changePassword($req);
                                break;     
                        case 'getemailUsername':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::getemailUsername($req);
                                break;
                        case 'LoadCurrencySetting':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::LoadCurrencySetting($req);
                                break;  
                        case 'SetCurrencySetting':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::SetCurrencySetting($req);
                                break;      
						case 'loadCourseDetails':
							session_start();
							if(isset($_SESSION['userId'])&&isset($_SESSION['userRole'])){
								$req->userId = $_SESSION['userId'];
								$req->userRole = $_SESSION['userRole'];
							}
							$res = Api::loadCourseDetails($req);
							break;
						case 'loadSubjectDetails':
							session_start();
							if(isset($_SESSION['userId'])&&isset($_SESSION['userRole'])){
								$req->userId = $_SESSION['userId'];
								$req->userRole = $_SESSION['userRole'];
							}
							$res = Api::loadSubjectDetails($req);
							break;   
						case  'loadOtherCoursesInstitute':
							session_start();
							if(isset($_SESSION['userId'])&&isset($_SESSION['userRole'])){
								$req->userId = $_SESSION['userId'];
								$req->userRole = $_SESSION['userRole'];
							}
							$res = Api::otherCoursesbyProfessor($req);
							break;
           /*admin section modules
            * 
            * 
            */      
                         case 'get-all-deleted-subject':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::getAlldeletedSubject($req);
                                break; 
                         case 'restore-subject-admin':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                 $res = Api::restoreSubjectByAdmin($req);
                                break;
                          case 'get-all-deleted-chapter':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::getAlldeletedChapter($req);
                                break;
                           case 'restore-chapter-admin':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::restoreChapterByAdmin($req);
                                break;
                           case 'get-all-deleted-exams':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::getAlldeletedExam($req);
                                break;
                           case 'restore-exam-admin':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::restoreExamByAdmin($req);
                                break;
                            case 'update-chapter-details':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::updateChapterDetails($req);
                                break;
                            
			/*student section*/
			//to fetch student name
			case  'get-student-name':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentName($req);
                break;
			//get exam details for student
			case  'get-exam-for-student':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getExamForStudent($req);
                break;
			//to get only questions for giving exam
			case  'get-questions-for-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getQuestionsForExam($req);
                break;
			//to get question and answers to solve assignment
			case  'get-questions-for-assignment':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getQuestionsForAssignment($req);
                break;
				
			//to get questions and answers to solve assignment but in resume mode
			case  'get-questions-for-assignment-in-resume':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getQuestionsForAssignmentInResume($req);
                break;
				
			//to get questions and answers to give exam but in resume mode
			case  'get-questions-for-exam-in-resume':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getQuestionsForExamInResume($req);
                break;
				
			//to get resume mode of the exam
			case  'get-exam-resume-status':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getExamResumeStatus($req);
                break;
				
			//to get time of exam/assignment
			case  'get-time-and-section':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getTimeAndSection($req);
                break;
				
			//to increase the attempt of exam
			case  'increase-attempt':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::increaseAttempt($req);
                break;
			//to check and save exam attempt
			case  'check-and-save':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkAndSave($req);
                break;
			//to save all questions for the auto save feature
			case 'save-all-questions':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveAllQuestions($req);
                break;
				
			//to update each answer as it will be given by user
			case 'update-user-answer':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::updateUserAnswer($req);
                break;
				
			/* cases for chapter content view page */
			//to fetch breadcrumb of chapter
			case  'get-breadcrumb-for-chapter':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getBreadcrumbForChapter($req);
                break;
			//to get headings of a chapter
			case  'get-headings':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getHeadings($req);
                break;
			//to get stuff of content
			case  'get-content-stuff':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getContentStuff($req);
                break;
			//to get student profile details
			case  'get-student-details':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentDetails($req);
                break;
			//to save student background details
			case  'save-student-bio':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveBio($req);
                break;
			//to save student achievements details
			case  'save-student-achievement':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveAchievement($req);
                break;
			//to save student interest details
			case  'save-student-interest':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveInterest($req);
                break;
			//to get student details for setting page
			case  'get-student-details-for-setting':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentDetailsForSetting($req);
                break;
			//to save student setting data
			case  'save-student-setting':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveStudentSetting($req);
                break;
			//to get student details for change password page
			case  'get-student-details-for-changePassword':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentDetailsForChangePassword($req);
                break;
			//to get student notifications
			case  'get-student-notifications':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentNotifications($req);
                break;
                    
                       //to get joined courses @amit
                        case  'student-joined-courses':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentJoinedCourses($req);
                break;
                   
            // to get exam details and count no of attempts of students
            /* 

             * 
             * 
             * 
             *              */
            
                      case 'student-exam-attempts':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::getStudentExamAttemps($req);
                            break;
                        
                     //   get courseDetails for student
                      case 'courseDetails-for-student':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::getCourseSubjectsForStudent($req);
                       break;  
                        
                     case 'exam-attempts-chapterwise':
                                session_start();
                                $req->userId = $_SESSION['userId'];
                                $req->userRole = $_SESSION['userRole'];
                                $res = Api::studentExamsChapterWise($req);
                       break; 
			case 'subjective-exam-attempts-chapterwise':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::studentSubjectiveExamsChapterWise($req);
				break;
                   
                   
                   
                   
                   


        //to delete chapter by institute roles
			case  'delete-chapter':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::deleteChapter($req);
                break;
			//to fetch all independent exams in a subject
			case  'get-independent-exams':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getIndependentExams($req);
                break;
			//to delete chapter by institute roles
			case  'student-session-check':
				$r = new stdClass();
				session_start();
				$r->status = 0;
				if(isset($_SESSION['userId']) && isset($_SESSION['userRole'])) {
					if($_SESSION['userRole'] == 4)
						$r->status = 1;
				}
				$res = $r;
                break;
				
			/* result creation section for given exams */
			//to fetch result details according to exam student and attempt
			case  'get-result':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getResult($req);
                break;
			//to fetch the questions and answers of each section for result
			case  'get-section-questions':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSectionQuestions($req);
                break;
			//to get topper details
			case 'get-topper-of-exam';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getTopperDetails($req);
                break;
			//to get maximum time for question
			case 'get-maximum-time-for-question';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getMaxiumTimeForQuestion($req);
                break;
			//to get exam attempts for subject
			case 'get-subject-attempts';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectAttempts($req);
                break;
			//to get exam attempts for graph creation
			case 'get-attempt-graph';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getAttemptGraph($req);
                break;
			//to get exam reports for institute
			case 'get-exam-report';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getExamReport($req);
                break;
			//to get student list who have given exams
			case 'get-student-list';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentList($req);
                break;
			
			//to create instructions for exams and assignments
			case 'create-instructions';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::createInstructions($req);
                break;
			/* bug fixes */
			//to get course name availability
			case 'check-duplicate-course';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkDuplicateCourse($req);
				break;
			//to get course name availability
			case 'check-duplicate-subject';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkDuplicateSubject($req);
				break;
			case 'check-duplicate-chapter':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkDuplicateChapter($req);
				break;
			//to create student left navigation tree
			case 'get-courses-for-student-tree':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getCoursesForStudentTree($req);
				break;
			//to get counter values for institute dashboard
			case 'get-institute-counters':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getInstituteCounters($req);
				break;
			//to get graph details for institute dashboard
			case 'get-institute-graphs':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getInstituteGraphs($req);
				break;
			case 'get-recent-course-view':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getRecentCourseView($req);
				break;
			// to get student end notifications
			case 'get-institute-notifications';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getInstituteNotifications($req);
                                break;   
                            
                         case 'mark-notification';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::markNotification($req);
                                break;
                         case 'count-notification';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::countNotification($req);
                                break;  
                         
                            // get professor invitations
                         case 'get-subject-invitation';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectInvitation($req);
                                break; 
                         case 'accept-subject-invitation';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::acceptSubjectInvitation($req);
                                break;
                          case 'get-subject-assigned';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectAssigned($req);
                                break;
                       /* student rate saved for course
                        * 
                        * 
                        */     
                        case 'save-course-rate-student';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveCourseRateStudent($req);
                                break;
                         case 'live-course-student';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::liveCourseStudent($req);
                                break;    
                         case 'live-course-content-market';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::liveCourseOnContentMarket($req);
                                break;
				case 'unlive-course-student';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::unliveCourseStudent($req);
                                break;    
                         case 'unlive-course-content-market';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::unliveCourseOnContentMarket($req);
                                break;
			case 'fetch-licensing-status';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::fetchCourseLicense($req);
				break;
                        
                          case 'check-login';
				session_start();
                                $res = new stdClass();
                                if(isset($_SESSION['userId']))
				{
                                $res->status = 1;
                                $res->message = " user log in";
                                }else{
                                    $res->status = 0;
                                    $res->message = " User not log in";
                                }
                                break;
                         case 'get-currency';
				session_start();
                                $req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
                                $res = Api::getCurrency($req);
                                break;
                         case 'get-course-details-studentEnd';
				session_start();
                                $req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
                                $res = Api::getCourseDetailsStudentEnd($req);
                                break;    
                          case 'link-student-course';
				session_start();
                                $req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
                                $res = Api::linkStudentCourse($req);
                                break;
				case 'puchase-copy-course-Institute';
					session_start();
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
					$res = Api::CopyCourseandPurchase($req);
					break;  
                            
                          case 'fetch-purchased-courses-details';
				session_start();
                                $req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
                                $res = Api::fetchPurchasedCoursesDetails($req);
                                break;   
                            
				//case to download the downloadable stuff by student
				case 'download-content':
					session_start();
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
					$res = Api::downloadContent($req);
					break;
				case 'send-renew request':
					session_start();
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
					$res = Api::sendRenewRequest($req);
					break;
			//case for breadcrumb on result page
			case 'get-breadcrumb-result':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getBreadcrumbResult($req);
				break;
			default:
				$res = new stdClass();
				$res->status = 0;
				$res->message = "Invalid Action";
				$res->action = $req->action;
				break;
		}
		if(isset($res->exception)) {
			$res->message = $res->exception;
		}
		echo json_encode($res);
	}catch(Exception $ex) {
		$res = new stdClass();
		$res->status = 0;
		$res->message = $ex->getMessage();
		echo json_encode($res);
	}
	
