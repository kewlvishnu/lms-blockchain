<?php
	$branch = "develop";
	$ogCourseImage = "https://d23h5gn2j73jqu.cloudfront.net/course-images/";
	$publicationTypes = array(
							'published'	=> 'Published Papers',
							'research'	=> 'Research Papers',
							'books'		=> 'Books'
						);
	$sitePath = "//www.arcanemind.org/";
	$urlDomains = array('arcanemind.org','www.arcanemind.org');
	$chatPrefix = "dev-";
?>