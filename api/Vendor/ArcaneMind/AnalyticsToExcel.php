<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Select;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	
	require_once "Base.php";
	
	require_once(getcwd().'/assets/PHPExcel/PHPExcel.php');	
	/** PHPExcel_Writer_Excel2007 */
	require_once(getcwd().'/assets/PHPExcel/PHPExcel/Writer/Excel2007.php');	
	
	class AnalyticsToExcel extends Base {
	
	
		public function getCourseAnalytics($data) {
			try {
				$result = new stdClass();
				require_once "Result.php";
				$r = new Result();
				$result = $r->getStudentList($data);
				if($result->students !=NULL || sizeof($result->students)!=0 ){
					$row = 2;
					$objPHPExcel = new PHPExcel();
					// Set properties
					$objPHPExcel->getProperties()->setCreator("Integro.io");
					$objPHPExcel->getProperties()->setLastModifiedBy("Integro.io");
					$objPHPExcel->getProperties()->setTitle("Exam_".$data->examId." Result");
					$objPHPExcel->setActiveSheetIndex(0);
					$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Serial No');
					$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Student Name!');
					$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
					$colhead=3;
					if($data->avg == 1)
					{
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colhead, 1,'Avergae Marks');
						$colhead++;
					}
					if($data->highest == 1)
					{
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colhead, 1,'Highest Marks');
						$colhead++;
					}if($data->attempts == 1)
					{
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colhead, 1,'Number of Attempts');
						$colhead++;
					}if($data->lastscore == 1)
					{
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colhead, 1,'Score in Last attempts');
						$colhead++;
					}
					
					//print_r($result->students);
					foreach($result->students as $key=>$student) {
						$col = 0;
						$noofattempts='';
						$highest='';
						$avgmarks='';
						$lastattempts='';
						$studentname= $student['firstName'].' '.$student['lastName'];
						$avgmarks=number_format((float)$student['details']['avg'], 2, '.', '');
						$highest=number_format((float)$student['details']['max'], 2, '.', '');
						$noofattempts=number_format((float)$student['details']['total'], 2, '.', '');
						if($student['lastScore']!=null)
						$lastattempts=number_format((float)$student['lastScore']['score'], 2, '.', '');
						
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $row-1);
        				$col++;
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $studentname);
        				$col++;
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row,$student['email']);
        				$col++;
						if($data->avg == 1)
						{
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $avgmarks);
							$col++;
						}
						if($data->highest == 1)
						{
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row,$highest);
							$col++;
						}if($data->attempts == 1)
						{
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$noofattempts);
							$col++;
						}if($data->lastscore == 1)
						{
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row,$lastattempts);
							$col++;
						}
						$row++;
					}
					
					$ext		 = 'xlsx';
					$timestamp=time();
					$fileNameAws = "ExamReport-{$data->examId}";
					$filenamePrefix=getcwd()."/user-data/analyticsExcel/";
					$filepath	 = $filenamePrefix."".$fileNameAws.'.xlsx';
					$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
					//$objWriter->save('php://output');
					//$objWriter->save( "Exam Report_".$data->examId.'.xlsx', __FILE__);
					
					//dirname(__FILE__).$filenamePrefix."/".$fileNameAws.".xlsx"
					$objWriter->save( $filepath, __FILE__);	
					if(file_exists($filepath) &&$_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1'){				
						$filesDirAws = "user-data/analyticsExcel/";
						$fileNameAws=$fileNameAws.$timestamp.'.xlsx';
						$filePathAws = $filesDirAws. $fileNameAws;
						require_once (getcwd().'/api/amazonUpload.php');
						uploadFile($filePathAws,$filepath);
						unlink($filepath);
						$config =  require "cloudFront.php";
						$path=$config.$filePathAws;
						require_once 'amazonRead.php';
						$result->path = getSignedURL($path);
						$result->status=1;
						return $result;
					}
				}
				$result->status=2;
				$result->message = 'Please Try Again';
				return $result;				
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
	}
	
	
?>
