<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Select;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	
	require_once "Base.php";
	
	//$adapter = require "adapter.php";
	
	class Subject extends Base {
		
		public function createSubject($data) {

			$db = $this->adapter->getDriver()->getConnection();
			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				//checking if course has not expired yet
				$result = $this->checkDuplicateSubject($data);
				if($result->available == false) {
					$result->status = 2;
					$result->message = 'Please select a different name as you have already created a subject by this name';
					return $result;
				}
				if($this->subjectCourseExpired($data)) {
					$result->status = 0;
					$result->message = 'Your course has expired. You can not add more subjects';
					return $result;
				}
				//$subjects = new TableGateway('subjects', $adapter, null,new HydratingResultSet());
				if(!isset($data->image))
					$data->image = "http://d2wqzjmvp96657.cloudfront.net/user-data/default_images/subject_dp.jpeg";
				if(!isset($data->parentId))
					$data->parentId = 0;
				if(!isset($data->rootParentId))
					$data->rootParentId = 0;
				$insert = array(
					'ownerId' 			=> 	$data->userId,
					'courseId' 			=> 	$data->courseId,
					'name' 				=>	$data->subjectName,
					'description' 		=>	$data->subjectDesc,
					'parentId' 			=>  $data->parentId,
					'rootParentId' 		=>  $data->rootParentId,
					'image'				=>	$data->image,
					'syllabus'			=>	''
				);
				$query="INSERT INTO subjects (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
				//var_dump($query);
				//die();
				$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$adapter->query("SET @subjectId = LAST_INSERT_ID() ;", $adapter::QUERY_MODE_EXECUTE);
				/*$subjects->insert($insert);
				$data->subjectId = $subjects->getLastInsertValue();
				if($data->subjectId == 0) {
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}*/

				//$_SESSION['subjectId'] = $data->subjectId;
				$query = "SELECT * FROM `student_subject` WHERE `courseId`=$data->courseId GROUP BY `userId`";
				$subjectUsers = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$subjectUsers = $subjectUsers->toArray();
				foreach ($subjectUsers as $key => $value) {
					/*$studentSubjects = new TableGateway('student_subject', $adapter, null,new HydratingResultSet());
					$insert = array(
						'userId' 			=> 	$value["userId"],
						'courseId' 			=> 	$data->courseId,
						'subjectId'			=>	$data->subjectId,
						'Active'			=>	1
					);
					$studentSubjects->insert($insert);*/
					$insert = array(
						'userId' 			=> 	$value["userId"],
						'courseId' 			=> 	$data->courseId,
						//'subjectId'			=>	$data->subjectId,
						'Active'			=>	1
					);
					$query="INSERT INTO student_subject (`".implode("`,`", array_keys($insert))."`,`subjectId`) VALUES ('".implode("','", array_values($insert))."',@subjectId)";
					//var_dump($query);
					$student_subject = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				}
				$query = "SELECT @subjectId as subjectID";
				$subjectID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$subjectID = $subjectID->toArray();
				$result->subjectId = $subjectID[0]["subjectID"];
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				//$result->subjectId = $data->subjectId;
				$result->message = 'Subject created!';
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
	
		public function getCourseSubjects($data) {
			$result =new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subjects');
				$select->where( array( 'courseId'	=>	$data->courseId, 'deleted'	=>	0))
						->order('weight ASC')
						->order('id ASC');
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->tempSubjects = $subjects->toArray();
				$result->subjects = array();
				foreach($result->tempSubjects as $sub) {
					
					$select = $sql->select();
					$select->from(array('sp' => 'subject_professors'))
						->join(array('p' => 'professor_details'),
								'sp.professorId = p.userId',
								array('firstName','lastName', 'userId'));
					$select->where( array( 'subjectId' => $sub['id'] ));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$prof = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$sub['professors'] = $prof->toArray();
					
					$select = $sql->select();
					$select->from(array('sp' => 'student_subject'));
					$select->where( array( 'subjectId' => $sub['id'], 'Active' => 1 ));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$stud = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$sub['students'] = count($stud->toArray());

					// lectures
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('c'	=>	'chapters'))
					->join(array('co'	=>	'content'),
							'c.id = co.chapterId',
							array('id')
					);
					$select->where(array('subjectId'	=>	$sub['id'], 'deleted'=>0, 'published'=>1));
					$select->columns(array('id'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$chapters = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$sub['chapters'] = count($chapters->toArray());

					// objective exams
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('e'	=>	'exams'));
					$select->where(array('subjectId'	=>	$sub['id'], 'delete'=>0));
					$select->columns(array('id', 'name'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$sub['exams'] = count($exams->toArray());

					// subjective exams
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('e'	=>	'exam_subjective'));
					$select->where(array('subjectId'	=>	$sub['id'], 'Active'=>1));
					$select->columns(array('id', 'name'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$sub['exams']+= count($exams->toArray());
					
					$result->subjects[] = $sub;
				}
				unset($result->tempSubjects);
				$result->status  = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getSubjectDetails($data) {
			$result =new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('s'=>'subjects'));
				$select->join(array('c'=>'courses'), 'c.id = s.courseId', array('courseName'=>'name'));
				if ($data->userRole == 4) {
					$select->join(array('sc' => 'student_subject'), "sc.subjectId = s.id", array());
					$select->where(array('s.id' => $data->subjectId, 'sc.userId'=>$data->userId, 'sc.Active'=>1, 's.deleted'=>0));
				} else if ($data->userRole == 1) {
					$select->where(array('s.id' => $data->subjectId, 's.ownerId' => $data->userId, 's.deleted'=>0));
				} else {
					$select->join(array('sp'=>'subject_professors'),'sp.subjectId=s.id',array(), $select::JOIN_LEFT);
					$select->where("s.id={$data->subjectId} AND s.deleted=0 AND (s.ownerId={$data->userId} OR sp.professorId={$data->userId})");
				}
				$statement = $sql->prepareStatementForSqlObject($select);
				$subjects = $statement->execute();
				if($subjects->count() == 0) {
					$result->status = 0;
					$result->message = "Subject not found";
					return $result;
				}
				$result->subjectDetails = $subjects->next();
				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1' && $_SERVER['REMOTE_ADDR'] <> '::1') {
					require_once 'amazonRead.php';
					$result->subjectDetails['image'] = getSignedURL($result->subjectDetails['image']);
				} else {
					$result->subjectDetails['image'] = "assets/pages/img/background/34.jpg";
				}
				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1' && $_SERVER['REMOTE_ADDR'] <> '::1') {
					require_once 'amazonRead.php';
					$result->subjectDetails['syllabus'] = getSignedURL($result->subjectDetails['syllabus']);
				} else {
					$result->subjectDetails['syllabus'] = "assets/pages/img/background/34.jpg";
				}

				require_once "Student.php";
				$st = new Student();
				$result->subjectDetails['rating'] = $st->getSubjectRating($data->subjectId);

				$query="SELECT sp.*
					FROM subject_professors sp
					WHERE subjectId=$data->subjectId";
				$subjectProfessors = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->subjectDetails['instructors'] = count($subjectProfessors->toArray());

				$select = $sql->select();
				$select->from(array('sp' => 'student_subject'));
				$select->join(array('ld' =>	'login_details'),
						'ld.id = sp.userId'
				);
				$select->where( array( 'sp.subjectId' => $data->subjectId, 'sp.Active' => 1, 'ld.temp' => 0 ));
				$select->group('sp.userId');
				$selectString = $sql->getSqlStringForSqlObject($select);
				$stud = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->subjectDetails['students'] = count($stud->toArray());

				// lectures
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('c'	=>	'chapters'))
				->join(array('co'	=>	'content'),
						'c.id = co.chapterId',
						array('id')
				);
				$select->where(array('subjectId'	=>	$data->subjectId, 'deleted'=>0, 'published'=>1));
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$chapters = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->subjectDetails['chapters'] = count($chapters->toArray());

				// objective exams
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('e'	=>	'exams'));
				$select->where(array('subjectId'	=>	$data->subjectId, 'delete'=>0));
				$select->columns(array('id', 'name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->subjectDetails['exams'] = count($exams->toArray());

				// subjective exams
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('e'	=>	'exam_subjective'));
				$select->where(array('subjectId'	=>	$data->subjectId, 'Active'=>1));
				$select->columns(array('id', 'name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->subjectDetails['exams']+= count($exams->toArray());
				
				// fetch objective questions
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('q'	=>	'questions'));
				$select->where(array('subjectId'	=>	$data->subjectId, 'delete'=>0));
				$select->columns(array('id', 'question'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->subjectDetails['questions'] = count($questions->toArray());

				// fetch subjective questions
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('q'	=>	'questions_subjective'));
				$select->where(array('subjectId'	=>	$data->subjectId, 'deleted'=>0));
				$select->columns(array('id', 'question'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->subjectDetails['subjective_questions'] = count($questions->toArray());

				// fetch submission questions
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('q'	=>	'submission_questions'));
				$select->where(array('subjectId'	=>	$data->subjectId, 'deleted'=>0));
				$select->columns(array('id', 'question'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->subjectDetails['submission_questions'] = count($questions->toArray());

				$result->status  = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getSubjectBasics($data) {
			$result =new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('s'=>'subjects'));
				$select->join(array('c'=>'courses'), 'c.id = s.courseId', array('courseName'=>'name'));
				$select->where(array('s.id' => $data->subjectId, 's.deleted'=>0));
				$statement = $sql->prepareStatementForSqlObject($select);
				$subjects = $statement->execute();
				if($subjects->count() == 0) {
					$result->status = 0;
					$result->message = "Subject not found";
					return $result;
				}
				$result->subjectDetails = $subjects->next();
				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1' && $_SERVER['REMOTE_ADDR'] <> '::1') {
					require_once 'amazonRead.php';
					$result->subjectDetails['image'] = getSignedURL($result->subjectDetails['image']);
				} else {
					$result->subjectDetails['image'] = "assets/pages/img/background/34.jpg";
				}
				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1' && $_SERVER['REMOTE_ADDR'] <> '::1') {
					require_once 'amazonRead.php';
					$result->subjectDetails['syllabus'] = getSignedURL($result->subjectDetails['syllabus']);
				} else {
					$result->subjectDetails['syllabus'] = "assets/pages/img/background/34.jpg";
				}

				$result->status  = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getSubjectQuestions($data) {
			$result =new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('s'=>'subjects'));
				$select->join(array('c' => 'courses'), 'c.id = s.courseId', array('courseName'=>'name'));
				$select->where(array('s.id' => $data->subjectId,'s.deleted'=>0));
				$statement = $sql->prepareStatementForSqlObject($select);
				$subjects = $statement->execute();
				if($subjects->count() == 0) {
					$result->status = 0;
					$result->message = "Subject not found";
					return $result;
				}
				$result->subjectDetails = $subjects->next();
				$result->questions = 0;
				
				// questions fetch based on questionType
				for ($i=0; $i < 8; $i++) { 
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('q'	=>	'questions'));
					$select->where(array('subjectId' => $data->subjectId, 'ownerId' => $data->userId, 'questionType' => $i, 'parentId'=>0, 'delete'=>0));
					//$select->columns(array('id', 'question'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$result->questionGroups[$i] = $questions->toArray();
					if ($i == 4 || $i == 5) {
						foreach ($result->questionGroups[$i] as $key => $question) {
							$sql = new Sql($adapter);
							$select = $sql->select();
							$select->from(array('q'	=>	'questions'));
							$select->where(array('parentId'=>$question['id'], 'status'=>1, 'delete'=>0));
							//$select->columns(array('id', 'question'));
							$selectString = $sql->getSqlStringForSqlObject($select);
							$subQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
							//var_dump($subQuestions->toArray());
							$result->questionGroups[$i][$key]['subQuestions'] = $subQuestions->toArray();
						}
					}
					$result->questions+= count($result->questionGroups[$i]);
				}

				$result->status  = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getSubjectQuestionsByCategory($data) {
			$result =new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('sc'=>'section_categories'));
				$select->where(array('sc.id' => $data->categoryId,'sc.delete'=>0));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$categories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$categories = $categories->toArray();
				if (count($categories) == 0) {
					$result->status = 0;
					$result->message = "Category not found";
					return $result;
				}
				$questionType = $categories[0]['questionType'];
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('s'=>'subjects'));
				$select->join(array('c' => 'courses'), 'c.id = s.courseId', array('courseName'=>'name'));
				$select->where(array('s.id' => $data->subjectId,'s.deleted'=>0));
				$statement = $sql->prepareStatementForSqlObject($select);
				$subjects = $statement->execute();
				if($subjects->count() == 0) {
					$result->status = 0;
					$result->message = "Subject not found";
					return $result;
				}
				//$result->subjectDetails = $subjects->next();
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('q'	=>	'questions'));
				$select->join(array('qcl' => 'question_category_links'), 'q.id = qcl.questionId', array());
				$select->where(array('qcl.categoryId' => $data->categoryId));
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$existingQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$existingQuestions = $existingQuestions->toArray();
				$arrExistingQuestions = array();
				if (count($existingQuestions)>0) {
					foreach ($existingQuestions as $key => $q) {
						//var_dump($q);
						$arrExistingQuestions[] = $q['id'];
					}
				}
				
				// questions fetch based on questionType
				$where = array('q.subjectId' => $data->subjectId, 'q.ownerId' => $data->userId, 'q.questionType' => $questionType, 'q.parentId'=>0, 'q.delete'=>0);
				if (isset($data->difficulty) && !empty($data->difficulty)) {
					$where['q.difficulty'] = $data->difficulty;
				}
				if (isset($data->examId) && !empty($data->examId)) {
					$where['e.id'] = $data->examId;
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('q'	=>	'questions'));
					$select->join(array('qcl' => 'question_category_links'), 'q.id = qcl.questionId', array());
					$select->join(array('sc' => 'section_categories'), 'sc.id = qcl.categoryId', array());
					$select->join(array('es' => 'exam_sections'), 'es.id = sc.sectionId', array());
					$select->join(array('e' => 'exams'), 'e.id = es.examId', array());
				} else {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('q'	=>	'questions'));
				}
				if (isset($data->tags) && !empty($data->tags)) {
					$select->join(array('qtl' => 'question_tag_links'), 'q.id = qtl.question_id', array());
					$select->join(array('qt' => 'question_tags'), 'qt.id = qtl.tag_id', array());

					$where['qt.tag'] = $data->tags;
				}
				if (!empty($arrExistingQuestions)) {
					$select->where('q.id NOT IN ('.implode(",", $arrExistingQuestions).')');
				}
				$select->where($where);
				//$select->columns(array('id', 'question'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				//var_dump($selectString);
				$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->questions = $questions->toArray();

				$result->status  = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getSubjectiveQuestions($data) {
			$result =new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('s'=>'subjects'));
				$select->join(array('c' => 'courses'), 'c.id = s.courseId', array('courseName'=>'name'));
				$select->where(array('s.id' => $data->subjectId,'s.deleted'=>0));
				$statement = $sql->prepareStatementForSqlObject($select);
				$subjects = $statement->execute();
				if($subjects->count() == 0) {
					$result->status = 0;
					$result->message = "Subject not found";
					return $result;
				}
				$result->subjectDetails = $subjects->next();
				$result->questions = 0;
				
				// questions fetch based on questionType
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('qs' =>	'questions_subjective'));
				$select->where(array('subjectId' => $data->subjectId, 'ownerId' => $data->userId, 'deleted'=>0));
				//$select->columns(array('id', 'question'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$questions = $questions->toArray();
				$subjectiveQuestions=array();
				$parentIdTemp= array();
				$i			 = 0;
				foreach($questions as $key=>$subQues)
				{
					$question=array();
					$questionId = $subQues['id'];
					$questionString = $subQues['question'];
					$answerString = $subQues['answer'];

					if($subQues['parentId']==0)
					{
						$query="SELECT qt.id, qt.tag AS name
							FROM `subjective_question_tag_links` AS sqtl
							INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
							WHERE sqtl.question_id= '$questionId'";
						$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$tags = $tags->toArray();

						$parentIdTemp[$i]=$subQues['id'];
						$i++;
						if($subQues['noOfSubquestionRequired']>0)
						{
							$question['id']						=$subQues['id'];
							$question['questionType']			='questionGroup';
							$question['question']				=$questionString;
							$question['questionBrief']			=$this->displayString($questionString);
							$question['answer']					=$subQues['noOfSubquestionRequired'];
							//$question['subquestionMrks']		=$subQues['marks'];
							$question['questionsonPage']		=$subQues['questionsonPage'];
							$question['difficulty']				=$subQues['difficulty'];
							$question['tags']					=$tags;

							$question['subQuestions']			=array();
						} else {
							$question['id']				=$questionId;
							$question['questionType']	='question';
							$question['question']		=$questionString;
							$question['questionBrief']	=$this->displayString($questionString);
							$question['answer']			=$answerString;
							$question['answerBrief']	=$this->displayString($answerString);
							//$question['marks']			=$subQues['marks'];
							$question['difficulty']		=$subQues['difficulty'];
							$question['tags']			=$tags;
							$question['subQuestions']	=array();
						}
						array_push($subjectiveQuestions,$question);
						
					} else {
						$parentId=$subQues['parentId'];
						$subquestionsArray=array();
						$subQuestionId=$subQues['id'];
						if(in_array($parentId,$parentIdTemp))
						{
							$index=array_search($subQues['parentId'],$parentIdTemp);

							$subquestionsArray['id']=$subQuestionId;
							$subquestionsArray['questionType']='question';
							$subquestionsArray['question']=$questionString;
							$subquestionsArray['questionBrief']	=$this->displayString($questionString);
							$subquestionsArray['answer']=$answerString;
							$subquestionsArray['answerBrief']	=$this->displayString($answerString);
							array_push($subjectiveQuestions[$index]['subQuestions'],$subquestionsArray);
						}
						
					}
				}
				
				$result->questions = $subjectiveQuestions;

				$result->status  = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getSubjectiveQuestionsImport($data) {
			$result =new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('s'=>'subjects'));
				$select->join(array('c' => 'courses'), 'c.id = s.courseId', array('courseName'=>'name'));
				$select->where(array('s.id' => $data->subjectId,'s.deleted'=>0));
				$statement = $sql->prepareStatementForSqlObject($select);
				$subjects = $statement->execute();
				if($subjects->count() == 0) {
					$result->status = 0;
					$result->message = "Subject not found";
					return $result;
				}
				$result->subjectDetails = $subjects->next();
				$result->questions = 0;
				
				// questions fetch based on questionType
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('qs' =>	'questions_subjective'));
				$select->join(array('qsel' => 'question_subjective_exam_links'), 'qs.id = qsel.questionId', array('marks'));
				$select->where(array('qsel.examId' => $data->currExamId, 'qs.subjectId' => $data->subjectId, 'qs.ownerId' => $data->userId, 'qs.deleted'=>0));
				//$select->columns(array('id', 'question'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$existingQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$existingQuestions = $existingQuestions->toArray();

				$arrExistingQuestions = array();
				if (count($existingQuestions)>0) {
					foreach ($existingQuestions as $key => $q) {
						//var_dump($q);
						$arrExistingQuestions[] = $q['id'];
					}
				}

				// questions fetch based on questionType
				$where = array('q.subjectId' => $data->subjectId, 'q.ownerId' => $data->userId, 'q.deleted'=>0);
				if (isset($data->difficulty) && !empty($data->difficulty)) {
					$where['q.difficulty'] = $data->difficulty;
				}
				if (isset($data->examId) && !empty($data->examId)) {
					$where['e.id'] = $data->examId;
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('q'	=>	'questions_subjective'));
					$select->join(array('qsel' => 'question_subjective_exam_links'), 'q.id = qsel.questionId', array('marks'));
					$select->join(array('e' => 'exam_subjective'), 'e.id = qsel.examId', array());
					$select->group('q.id');
				} else {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('q'	=>	'questions_subjective'));
				}
				
				if (isset($data->tags) && !empty($data->tags)) {
					$select->join(array('qtl' => 'subjective_question_tag_links'), 'q.id = qtl.question_id', array());
					$select->join(array('qt' => 'question_tags'), 'qt.id = qtl.tag_id', array());

					$where['qt.tag'] = $data->tags;
				}

				if (!empty($arrExistingQuestions)) {
					$select->where('q.id NOT IN ('.implode(",", $arrExistingQuestions).')');
				}

				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				//var_dump($selectString);
				$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$questions = $questions->toArray();
				$subjectiveQuestions=array();
				$parentIdTemp= array();
				$i			 = 0;

				foreach($questions as $key=>$subQues)
				{
					$question=array();
					$questionId = $subQues['id'];
					$questionString = $subQues['question'];
					$answerString = $subQues['answer'];

					if($subQues['parentId']==0)
					{
						$query="SELECT qt.id, qt.tag AS name
							FROM `subjective_question_tag_links` AS sqtl
							INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
							WHERE sqtl.question_id= '$questionId'";
						$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$tags = $tags->toArray();

						$parentIdTemp[$i]=$subQues['id'];
						$i++;
						if($subQues['noOfSubquestionRequired']>0)
						{
							$question['id']						=$subQues['id'];
							$question['questionType']			='questionGroup';
							$question['question']				=$questionString;
							$question['questionBrief']			=$this->displayString($questionString);
							$question['answer']					=$subQues['noOfSubquestionRequired'];
							$question['subquestionMrks']		=$subQues['marks'];
							$question['questionsonPage']		=$subQues['questionsonPage'];
							$question['difficulty']				=$subQues['difficulty'];
							$question['tags']					=$tags;

							$question['subQuestions']			=array();
						} else {
							$question['id']				=$questionId;
							$question['questionType']	='question';
							$question['question']		=$questionString;
							$question['questionBrief']	=$this->displayString($questionString);
							$question['answer']			=$answerString;
							$question['answerBrief']	=$this->displayString($answerString);
							$question['marks']			=$subQues['marks'];
							$question['difficulty']		=$subQues['difficulty'];
							$question['tags']			=$tags;
							$question['subQuestions']	=array();
						}
						array_push($subjectiveQuestions,$question);
						
					} else {
						$parentId=$subQues['parentId'];
						$subquestionsArray=array();
						$subQuestionId=$subQues['id'];
						if(in_array($parentId,$parentIdTemp))
						{
							$index=array_search($subQues['parentId'],$parentIdTemp);

							$subquestionsArray['id']=$subQuestionId;
							$subquestionsArray['questionType']='question';
							$subquestionsArray['question']=$questionString;
							$subquestionsArray['questionBrief']	=$this->displayString($questionString);
							$subquestionsArray['answer']=$answerString;
							$subquestionsArray['answerBrief']	=$this->displayString($answerString);
							array_push($subjectiveQuestions[$index]['subQuestions'],$subquestionsArray);
						}
						
					}
				}
				
				$result->questions = $subjectiveQuestions;

				$result->status  = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function fixCourseStudentsToSubjects() {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$query="SELECT ld.id,sc.course_id
						FROM `login_details` AS ld
						INNER JOIN `user_roles` AS ur ON ur.userId=ld.id AND ur.roleId=4
						INNER JOIN `student_course` AS sc ON sc.user_id=ld.id AND sc.user_id=379 AND sc.course_id=92
						GROUP BY sc.course_id";
				$users = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$users = $users->toArray();
				//var_dump($users);
				foreach ($users as $key => $value) {
					$query="SELECT COUNT(*) AS count
							FROM `student_subject`
							WHERE courseId={$value['course_id']} AND userId={$value['id']} AND Active=1";
					$ssubjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$ssubjects = $ssubjects->toArray();
					//var_dump($ssubjects);
					if($ssubjects[0]['count'] == 0) {
						$query="SELECT id
								FROM `subjects`
								WHERE courseId={$value['course_id']}";
						$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$subjects = $subjects->toArray();
						if (count($subjects)>0) {
							foreach ($subjects as $key1 => $value1) {
								$studentSubjects = new TableGateway('student_subject', $adapter, null,new HydratingResultSet());
								$insert = array (
									'courseId' 	=> $value['course_id'],
									'userId'	=> $value['id'],
									'subjectId'	=> $value1['id'],
									'Active'	=> 1
								);
								$res = $studentSubjects->insert($insert);
							}
						}
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				//die();
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		//to track subject progress by prof/insti
		public function getSubjectProgressDetails($data) {
			$result 	=	new stdClass();
			$contentV    = 	array();
			$content_25	=	array();
			$content_50	=	array();
			$content_75	=	array();
			$content_76	=	array();
			$totalStudent = 0;
			try {
				$adapter    =   $this->adapter;
				$query="SELECT ch.id chapterID,ch.name,title,cn.id
				FROM `chapters`ch left join content cn on ch.id=cn.chapterId
				WHERE ch.subjectId=$data->subjectId and ch.deleted=0 and cn.published=1
				ORDER BY ch.id,cn.id";
				$content = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$content=$content->toArray();
				//print_r($content);
				foreach( $content as $key => $value){
				$contentV[$key]=$value['id'];
				}
				
				//total student enrolled in course
				//$query="SELECT count(id) as students FROM student_course WHERE course_id=$data->courseId";
				$query="SELECT count(ss.id) as students
				FROM `student_subject` AS ss
				INNER JOIN `login_details` AS ld ON ld.id=ss.userId
				WHERE ss.courseId=$data->courseId AND ss.subjectId=$data->subjectId AND ss.Active=1 AND ld.temp=0
				GROUP BY ss.userId";
				//$query="SELECT concat(firstName,' ',lastName) as name, sd.userId FROM student_subject ss left join student_details sd ON sd.userId=ss.userId WHERE ss.courseId =$data->courseId AND ss.subjectId=$data->subjectId AND ss.Active=1 GROUP BY ss.userId";
				$student = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$student = $student->toArray();
				// counting no of rows to get unique students in the subject
				$totalStudent = count($student);
				
				require_once 'DynamoDbOps.php';
				$ddo = new DynamoDbOps();
				$content_watched=$ddo->getStudentsProgress($data);
				$content_watched=$content_watched->items;
				$content_25=array_fill(0,count($contentV),0);
				$content_50=array_fill(0,count($contentV),0);
				$content_75=array_fill(0,count($contentV),0);
				$content_76=array_fill(0,count($contentV),0);
				foreach( $content_watched as $key => $value){
					$contentId			= $value['contentId'];
					$ChapterProgress	= $value['percentRead'];
					$index=array_search($contentId, $contentV);
					if($ChapterProgress>75)
					{	
						$content_76[$index]=$content_76[$index]+1;
					}
					elseif($ChapterProgress>50)
					{
						$content_75[$index]=$content_75[$index]+1;
					}
					elseif($ChapterProgress>25)
					{
						$content_50[$index]=$content_50[$index]+1;
					}
					else
					{
						$content_25[$index]=$content_25[$index]+1;
					}
					/*$videowatched		=	explode(',',$value['videowatched']['S']);
					$ChapterProgress	=	explode(',',$value['ChapterProgress']['S']);
					for($i=0;$i<count($videowatched);$i++)
					{		
						if(in_array($videowatched[$i], $contentV)){		
						$index=array_search($videowatched[$i], $contentV);
							if($ChapterProgress[$i]>75)
							{	
								$content_76[$index]=$content_76[$index]+1;
							}
							elseif($ChapterProgress[$i]>50)
							{
								$content_75[$index]=$content_75[$index]+1;
								
							}
							elseif($ChapterProgress[$i]>25)
							{
								$content_50[$index]=$content_50[$index]+1;
								
							}
							else
							{
								$content_25[$index]=$content_25[$index]+1;
								
							}
						}
					}*/
					
				}
				
				foreach( $content as $key => $value){
				$content[$key]['content_76']=$content_76[ $key];
				$content[$key]['content_75']=$content_75[ $key];
				$content[$key]['content_50']=$content_50[ $key];
				$content[$key]['content_25']=$content_25[ $key];
				$content[$key]['content_00']= $totalStudent-($content_76[ $key]+$content_75[ $key]+$content_50[ $key]+$content_25[ $key]);
				}
				
				$result->content=$content;
				$result->status  = 1;
				//print_r($result);
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		//to track subject progress by prof/insti retuning studnets name
		public function getProgressDetailsStudents($data) {
			$result			=	new stdClass();
			$contentV		=	array();
			$totalStudent	=	0;
			$studentEnrolled=array();
			$studentEnrolledId=array();
			$studentprogress=array();
			$studentprogressTime=array();
			try {
				$adapter    =   $this->adapter;
				$query="SELECT ch.id chapterID,ch.name,title,cn.id FROM chapters ch left join content cn on ch.id=cn.chapterId  WHERE ch.subjectId=$data->subjectId and ch.deleted =0 and cn.published=1 order by ch.id,cn.id";
				$content = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$content=$content->toArray();
				
				//fetching all the students enrolled in course
				//$query="SELECT concat(firstName,' ',lastName) as name, userId FROM student_course sc left join student_details sd ON sd.userId=sc.user_id WHERE course_id =$data->courseId";
				$query="SELECT concat(firstName,' ',lastName) as name, sd.userId
				FROM student_subject ss
				LEFT JOIN student_details sd ON sd.userId=ss.userId
				INNER JOIN login_details ld ON ld.id=ss.userId
				WHERE ss.courseId =$data->courseId AND ss.subjectId=$data->subjectId AND ss.Active=1 AND ld.temp=0
				GROUP BY ss.userId";
				$student = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$student=$student->toArray();
				$allStudent = $student;
				$totalStudent=0;
				$totalStudent= count($allStudent);
				if($totalStudent==0)
				{
					$result->status = 0;
					$result->message = 'No students Enrolled in the course';
					return $result;
				}
				//student progress array to fill on per content id
				$studentprogress=array_fill(0,$totalStudent,0);
				$totalTime=array_fill(0,$totalStudent,0);
				foreach( $allStudent as $key => $value){
					$studentEnrolled[$key] =$allStudent[$key]['name'];
					$studentEnrolledId[$key]=$allStudent[$key]['userId'];
				}
				// adding to result object
				foreach( $content as $key => $value){
					$contentV[$key]=$value['id'];
					$content[$key]['studentprogress']=$studentprogress;
					$content[$key]['totalTime']=$totalTime;
				}
				require_once 'DynamoDbOps.php';
				$ddo = new DynamoDbOps();
				$content_watched=$ddo->getStudentsProgress($data);
				$content_watched=$content_watched->items;
				foreach( $content_watched as $key => $value){
					//var_dump($value);
					$userId				=	$value['userId'];
					$contentId			=	$value['contentId'];
					$index1				=	array_search($userId, $studentEnrolledId);	
					$index				=	array_search($contentId, $contentV);
					if ($index1>-1) {
						$content[$index]['studentprogress'][$index1]=$value['percentRead'];
						$content[$index]['totalTime'][$index1]=gmdate("H:i:s",$value['totalTime']);
					}
				}

				$result->content=$content;
				$result->studentEnrolled=$studentEnrolled;
				$result->studentEnrolledId=$studentEnrolledId;
				//print_r($result);
				$result->status  = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}	
		
		//to track subject progress by prof/insti retuning studnets name
		public function getStudentSubjectDetails($data) {
			$result 	=	new stdClass();
			try {
					$adapter = $this->adapter;
					$sql = new Sql($adapter);
					$select = $sql->select();	
					$select->from('subjects');
					$select->where( array( 'courseId'	=>	$data->courseId,
											'deleted'	=>	0))
							->order('weight ASC')
							->order('id ASC');
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$result->Subjects = $subjects->toArray();
					$query="SELECT S.userId, CONCAT( S.firstName,' ',S.lastName) as name
							FROM student_details as S
							INNER JOIN student_course as K on K.user_id=S.userId
							INNER JOIN login_details AS ld ON ld.id=S.userId
							WHERE K.course_id=$data->courseId AND ld.active=1 AND ld.temp=0";
					$students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$result->students = $students->toArray();
					if (count($result->students) == 0) {
						$result->message = "No students Enrolled in the Course";
						$result->status = 0;
						return $result;
					}
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('student_subject');
					$select->where(array('courseId' => $data->courseId,'Active'=>1));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjects1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$subjects1 = $subjects1->toArray();
					$result->Student_enrolled=$subjects1;
					$result->status=1;
					return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function setStudentSubjectDetails($data) {
			$result 	=	new stdClass();
			try {
				$countInactive=	count($data->alreadyChecked);
				if($countInactive>0)
				{
					foreach($data->alreadyChecked as $key =>$value)
					{	
						$updateStu=explode("_",$value);
						$stuInActive[$key]=$updateStu[0];
						$subInActive[$key]=$updateStu[1];
					}
					$adapter = $this->adapter;
					$query="UPDATE student_subject ss INNER JOIN login_details ld ON ld.id=ss.userId SET ss.Active=0  WHERE " ;
					$query .= "(ss.courseId=".$data->courseId." AND ss.userId=" .$stuInActive[0]." AND ss.subjectId=" .$subInActive[0]." AND ld.temp=0)";
					for ($i=1; $i<$countInactive;$i++) {
					$query .=" OR ";
					$query .= "(ss.courseId=".$data->courseId." AND ss.userId=" .$stuInActive[$i]." AND ss.subjectId=" .$subInActive[$i]." AND ld.temp=0)";
					}
					$student_InActive = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				}
				$students	=	json_decode(json_encode($data->students), true);
				$course		=	json_decode(json_encode($data->subjects), true);
				$count=count($students);
				$adapter = $this->adapter;
				$query="INSERT INTO student_subject (courseId,userId,subjectId) VALUES " ;
				$query .= "('".$data->courseId."','" .$students[0]."','" .$course[0]."')";
				for ($i=1; $i<$count;$i++) {
					$query .= ",('".$data->courseId."','" .$students[$i]."','" .$course[$i]."')";
				}
				$student_subject = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->status = 1;
				$result->message = 'Subjects assigned to students successfully!';	
				return $result;

			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}		
		public function updateSubject($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				/*
					We need to verify if the user editing the course owns it.
				*/
				$adapter = $this->adapter;
				$where = array('id' => $data->subjectId,
					'ownerId' => $data->userId
				);
				$updateValues = array(
					'name' 				=>	$data->subjectName,
					'description' 		=>	$data->subjectDesc,
				);
				//var_dump($updateValues);die();
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('subjects');
				$update->set($updateValues);
				$update->where($where);
				$statement = $sql->prepareStatementForSqlObject($update);
				$result = $statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				
				if($data->courseId == 0) {
					$result->status = 0;
					$result->message = "Some error occured";
					return $result;
				}
				$_SESSION['subjectId'] = $data->subjectId;
				$result->status = 1;
				$result->message = 'Subject updated!';
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function getSubjectsForCourse($data) {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('subjects');
			$select->where(array('courseId' => $data->courseId,'deleted'=>0));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$adapter->getDriver()->getConnection()->disconnect();
			$subjects = $subjects->toArray();
			foreach ($subjects as $key => $subject) {
				$subjects[$key]['chapters'] = $this->getSubjectChaptersCount($subject['id']);
				$subjects[$key]['exams'] = $this->getSubjectChaptersCount($subject['id']);
				/*$subjects[$key]['chapters']	= 0;
				$subjects[$key]['exams']	= 0;
				// lectures
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('c'	=>	'chapters'))
				->join(array('co'	=>	'content'),
						'c.id = co.chapterId',
						array('id')
				);
				$select->where(array('subjectId'	=>	$subject['id'], 'deleted'=>0, 'published'=>1));
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$chapters = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$chapters = $chapters->toArray();
				if(count($chapters) >= 0)
					$subjects[$key]['chapters']+= count($chapters);

				// objective exams
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('e'	=>	'exams'));
				$select->where(array('subjectId'	=>	$subject['id'], 'delete'=>0));
				$select->columns(array('id', 'name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				if(count($exams) >= 0)
					$subjects[$key]['exams']+= count($exams);
				// subjective exams
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('e'	=>	'exam_subjective'));
				$select->where(array('subjectId'	=>	$subject['id'], 'Active'=>1));
				$select->columns(array('id', 'name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				if(count($exams) >= 0)
					$subjects[$key]['exams']+= count($exams);*/
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$subjects[$key]['image'] = getSignedURL($subjects[$key]['image']);
				} else {
					$subjects[$key]['image'] = "assets/pages/img/background/34.jpg";
				}
			}
			return $subjects;
		}
	
		public function getSubjectChapters($data) {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('chapters')
					->order('weight ASC')
					->order('id ASC');
			$select->where(array('subjectId' => $data->subjectId,
								'deleted'	=>	0));
			
			$selectString = $sql->getSqlStringForSqlObject($select);
			$chapters = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$chapters = $chapters->toArray();
			foreach($chapters as $key=>$chapter) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exams');
				$select->where(array(
							'chapterId'	=>	$chapter['id'],
							'delete'	=>	0
				));
				$select->columns(array('id', 'name', 'type','startDate','endDate','attempts','status'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				$chapters[$key]['exams'] = $exams;
			}
			$result->chapters = $chapters;
			$result->courseId = $data->courseId;
			$result->status = 1;
			return $result;
		}

		public function getSubjectChaptersNew($data) {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('chapters')
					->order('weight ASC')
					->order('id ASC');
			$select->where(array('subjectId' => $data->subjectId,
								'deleted'	=>	0));
			
			$selectString = $sql->getSqlStringForSqlObject($select);
			$chapters = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$chapters = $chapters->toArray();
			foreach($chapters as $key=>$chapter) {
				require_once("Content.php");
				$co = new Content();
				$dataContent = new stdClass();
				$dataContent->chapterId = $chapter['id'];
				$resContent = $co->getChapterContent($dataContent);
				$chapters[$key]['content'] = array();
				if ($resContent->status == 1) {
					$chapters[$key]['content'] = $resContent->content['content'];
				}
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exams');
				$where = array(
							'type'		=>	'Exam',
							'chapterId'	=>	$chapter['id']
				);
				//if ($data->userRole == 4) {
					$where['delete'] = 0;
				//}
				$select->where($where);
				$select->columns(array('id', 'name', 'type','startDate','endDate','attempts','status','delete'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				foreach ($exams as $key1 => $exam) {
					$exams[$key1]['result'] = 0;
					if ($exams[$key1]['status'] > 0) {
						$select = "SELECT COUNT(*) AS total
									FROM exam_attempts AS ea
									INNER JOIN student_subject AS ss ON ss.userId=ea.studentId AND ss.Active=1
									WHERE ss.subjectId={$data->subjectId} AND ea.examId={$exam['id']} AND ea.completed=1";
						$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
						$total = $total->toArray();
						$total = $total[0]['total'];
						if ($total > 0) {
							$exams[$key1]['result'] = 1;
						}
					}
				}
				$chapters[$key]['exams'] = $exams;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exams');
				$where = array(
							'type'		=>	'Assignment',
							'chapterId'	=>	$chapter['id']
				);
				//if ($data->userRole == 4) {
					$where['delete'] = 0;
				//}
				$select->where($where);
				$select->columns(array('id', 'name', 'type','startDate','endDate','attempts','status','delete'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				foreach ($exams as $key1 => $exam) {
					$exams[$key1]['result'] = 0;
					if ($exams[$key1]['status'] > 0) {
						$select = "SELECT COUNT(*) AS total
									FROM exam_attempts AS ea
									INNER JOIN student_subject AS ss ON ss.userId=ea.studentId AND ss.Active=1
									WHERE ss.subjectId={$data->subjectId} AND ea.examId={$exam['id']} AND ea.completed=1";
						$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
						$total = $total->toArray();
						$total = $total[0]['total'];
						if ($total > 0) {
							$exams[$key1]['result'] = 1;
						}
					}
				}
				$chapters[$key]['assignments'] = $exams;

				// get subjective exams
				$subjectiveExams = $this->getsubjectiveExams($data,$chapter['id']);
				if ($subjectiveExams->status == 1) {
					$subjectiveExams = $subjectiveExams->exams;
				} else {
					$subjectiveExams = array();
				}

				if (count($subjectiveExams)) {

					foreach ($subjectiveExams as $key1 => $subjectiveExam) {

						$selectString = "SELECT COUNT(A.student) as num, A.attempt as attempt
						FROM (
							SELECT COUNT(DISTINCT ea.id) as attempt, ea.studentId as student
							FROM `subjective_exam_attempts` AS ea
							INNER JOIN student_subject AS ss ON ss.userId=ea.studentId AND ss.Active=1
							WHERE ss.subjectId={$data->subjectId} AND ea.examId={$subjectiveExam['id']} AND ea.completed = 1
							GROUP BY ea.studentId
						) as A GROUP BY A.attempt";
						$graph = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$graph = $graph->toArray();
						if(!(count($graph)>0))
						{
							$subjectiveExams[$key1]['result'] = 0;
						} else {
							$subjectiveExams[$key1]['result'] = 1;
						}
					}

				}
				
				$chapters[$key]['subjectiveExams'] = $subjectiveExams;

				$manualExams = $this->getManualExams($data,$chapter['id']);
				if ($manualExams->status == 1) {
					$manualExams = $manualExams->exams;
				} else {
					$manualExams = array();
				}
				$chapters[$key]['manualExams'] = $manualExams;

				require_once("Submission.php");
				$sub = new Submission();
				$submissions = $sub->getSubmissions($data,$chapter['id']);
				if ($submissions->status == 1) {
					$submissions = $submissions->exams;
				} else {
					$submissions = array();
				}

				if (count($submissions)) {

					foreach ($submissions as $key1 => $submission) {

						$selectString = "SELECT COUNT(A.student) as num, A.attempt as attempt FROM (
									SELECT COUNT(DISTINCT ea.id) as attempt, ea.studentId as student
									FROM `submission_attempts` AS ea
									INNER JOIN `student_subject` AS ss ON ss.userId=ea.studentId AND ss.Active=1
									WHERE ea.examId={$submission['id']} AND ea.completed = 1
									GROUP BY ea.studentId
								) as A GROUP BY A.attempt
								UNION
								SELECT COUNT(A.student) as num, A.attempt as attempt FROM (
									SELECT COUNT(DISTINCT ea.id) as attempt, ea.studentId as student
									FROM `submission_attempts` AS ea
									INNER JOIN `student_subject` AS ss ON ss.userId=ea.studentId AND ss.Active=1
									WHERE ea.examId={$submission['id']} AND ea.completed = 1
									GROUP BY ea.studentGroupId
								) as A GROUP BY A.attempt";
						$graph = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$graph = $graph->toArray();
						if(!(count($graph)>0))
						{
							$submissions[$key1]['result'] = 0;
						} else {
							$submissions[$key1]['result'] = 1;
						}
					}

				}
				
				$chapters[$key]['submissions'] = $submissions;
			}
			$result->chapters = $chapters;
			$result->courseId = $data->courseId;
			$result->status = 1;
			return $result;
		}
		
		public function getSubjectsForImport($data) {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('subjects');
			$where = array(
				'courseId' => $data->courseId,'deleted'=>0
			);
			$select->where($where);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$result = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$tempSubjects = $result->toArray();
			$temp = new stdClass();
			$subjects = array();
			foreach($tempSubjects as $s) {
				$temp->subjectId = $s['id'];
				$temp->importToCourseId = $data->importToCourseId;
				if($this->subjectAlreadyImported($temp))
					$s['alreadyImported'] = 1;
				else
					$s['alreadyImported'] = 0;
				$subjects[] = $s;
			}
			return $subjects;
		}
		
		public function deleteSubject($req) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				if(!$this->userOwnsSubject($req)){
					$result->status = 0;
					$result->message = "Unspecified error!";
					return $result;
				}
				$subject = $this->getCourseDetailsbySubject($req->subjectId);
				$course = $subject->subject;
				$courseId = $course[0]['courseId'];
				$coursename = $course[0]['course'];
				$subjectname = $course[0]['subject'];
				$institute = $course[0]['institute'];
				$institutemail = $course[0]['institutemail'];
				$instituteId = $course[0]['instituteId'];
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				/*$update = $sql->update();
				$update->table('subjects');
				$update->set(array('deleted' => 1));
				$update->where(array('id' => $req->subjectId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$result = $statement->execute();*/

				$query="UPDATE subjects SET deleted=1 WHERE id={$req->subjectId}";
				$subjects=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				
				$query="SELECT @countSubjectRemains = COUNT(id) from subjects where courseId=$courseId and deleted=0 ";
				$subjects=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);

				$query="UPDATE courses set approved=-1 WHERE id=$courseId AND @countSubjectRemains=0";
				$subjects=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							
				// count no of subjects if all sujects deleted then course must be not live.
				/*$query="SELECT COUNT(id) subjectRemains from subjects where courseId=$courseId and deleted=0 ";
				$subjects=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$subjects = $subjects->toArray();
				if($subjects[0]['subjectRemains']==0){
					$query="UPDATE courses set approved=-1  where id=$courseId";
					$subjects=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				}*/
				// insert notification
				$notification=" Subject ".$this->safeString($subjectname)." (Subject id : $req->subjectId ) belonging to Course ".$this->safeString($coursename)." ( course id : $courseId ) has been deleted. Please contact admin@integro.io for restoring the Subject ";
				$query = "INSERT INTO notifications (userId,message) VALUES ($instituteId,'$notification')";
				$notification=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
		  
				require_once "User.php";
				$c = new User();
				$instituteSubject = "Subject deletion Notification";
				$emailInstitute = "Dear $institute, <br> <br>";
				$emailInstitute .= "There has been a request for deletion of the Subject $subjectname (Subject ID: $req->subjectId) belonging to  Course  $coursename ( Course ID: $courseId ) on <a href=integro.io >Integro.io <a>  <br> <br>";

				$emailInstitute .= "Please write to us at <a href=admin@integro.io >admin@integro.io <a> if you wish to restore the course or if you did not initiate the request.<br> <br> "
					. "Please mention your Institute Id,Subject Name & Subject ID in the email <br> <br>";

				$emailInstitute .= "<table border=1 ><thead><tr><th> Institute Name</th> <th>Institute ID</th> <th>Deleted Course Name</th> <th> Deleted Course Id</th><th>Deleted Subject Name</th> <th>Deleted Subject Id</th></tr></thead>";
				$emailInstitute .= "<tbody><tr><td> $institute</td><td>$instituteId</td> <td>$coursename</td><td> $courseId</td><td> $subjectname</td><td> $req->subjectId</td> </tr></tbody></table> ";

				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1' && !empty($professor['profilePic']) ) {
					$c->sendMail($institutemail, $instituteSubject, $emailInstitute);
				}
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function restoreSubject($req) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				if(!$this->userOwnsSubject($req)) {
					$result->status = 0;
					$result->message = "Unspecified error!";
					return $result;
				}
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('subjects');
				$update->set(array('deleted' => 0));
				$update->where(array('id' => $req->subjectId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$result = $statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function userOwnsSubject($data) {
			$result =new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('subjects');
			$where = new \Zend\Db\Sql\Where();
			$where->equalTo('ownerId', $data->userId);
			$where->equalTo('id', $data->subjectId);
			$select->where($where);
			$select->columns(array('id'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->subjects = $subjects->toArray();
			if(count($result->subjects) == 0 && !$this->userAssociateSubject($data)) {
				return false;
			}
			return true;
		}
		
		public function userAssociateSubject($data) {
			$result =new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('subject_professors');
			$where = new \Zend\Db\Sql\Where();
			$where->equalTo('professorId', $data->userId);
			$where->equalTo('subjectId', $data->subjectId);
			$select->where($where);
			$select->columns(array('subjectId'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->subjects = $subjects->toArray();
			if(count($result->subjects) == 0) {
				return false;
			}
			return true;
		}
		public function subjectSubjectiveExamCheck($data) {
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('courses');
				$select->where(array('id'	=>	$data->courseId));
				$select->columns(array('id','endDate'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				if(count($temp) > 0) {
					$end = $temp[0]['endDate'];
					if($end == '')
						return false;
					$end = $end/1000;
					if((int)$end < time())
						return true;
					else
						return false;
				}
				return false;
			}catch(Exception $e) {
				return true;
			}
		}
		
		public function updateChapterOrder($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				if(!$this->userOwnsSubject($data)) {
					$result->status = 0;
					$result->message = "Unspecified error!";
					return $result;
				}
				
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				foreach($data->newOrder as $chapterId => $weight) {
					$update = $sql->update();
					$update->table('chapters');
					$update->set(array('weight' => $weight));
					$update->where(array('id' => $chapterId, 'subjectId' => $data->subjectId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$result = $statement->execute();
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = "Order Updated";
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function updateSubjectOrder($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				require_once "Course.php";
				$c = new Course();
				$result = new stdClass();
				if(!$c->userOwnsCourse($data)) {
					$result->status = 0;
					$result->message = "Unspecified error!";
					return $result;
				}
				
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				foreach($data->newOrder as $subjectId => $weight) {
					$update = $sql->update();
					$update->table('subjects');
					$update->set(array('weight' => $weight));
					$update->where(array('id' => $subjectId, 'courseId' => $data->courseId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$result = $statement->execute();
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = "Order Updated";
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function updateSubjectProfessors($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$result = new stdClass();
				if(!$this->userOwnsSubject($data)) {
					$result->status = 0;
					$result->message = "Unspecified error!";
					return $result;
				}
				
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				
				$delete = $sql->delete();
				$delete->from('subject_professors');
				$delete->where(array('subjectId' => $data->subjectId));
				$statement = $sql->prepareStatementForSqlObject($delete);
				$subjectProfessors = $statement->execute();
				//die();
				$subjectProfessors = new TableGateway('subject_professors', $adapter, null,new HydratingResultSet());
				foreach($data->subjectProfessors as $prof) {
					$req = new stdClass();
					$req->professorId = $prof;
					$req->userId = $data->userId;
					require_once "Institute.php";
					$i = new Institute();
					if(!$i->instituteHasProfessor($req)) {
						continue;
					}
					$subjectProfessors->insert(array (
						'subjectId' 	=> $data->subjectId,
						'professorId'	=> $prof
					));	
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = "Updated";
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function deleteSubjectProfessor($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				if(!$this->userOwnsSubject($data)) {
					$result->status = 0;
					$result->message = "Unspecified error!";
					return $result;
				}
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				
				$delete = $sql->delete();
				$delete->from('subject_professors');
				$delete->where(array('subjectId' => $data->subjectId, 'professorId' => $data->professorId));
				$statement = $sql->prepareStatementForSqlObject($delete);
				$subjectProfessors = $statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = "Updated";
				return $result;
			} catch (Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		public function getSubjectProfessorsChat($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('courses');
				$where = array('id' => $data->courseId);
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$chatFeature = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$chatFeature = $chatFeature->toArray();
				$chatFeature = $chatFeature[0];
				if($chatFeature["chat"] == 0) {
					$result->status = 0;
					$result->message = "Chat feature not enabled";
					return $result;
				}
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('sp' => 'subject_professors'));
				$select->join(array('pd' => 'professor_details'), 'pd.userId = sp.professorId', array('firstName', 'lastName', 'profilePic'));
				$select->join(array('ld' => 'login_details'), 'ld.id = sp.professorId', array('username'));
				$where = array('subjectId' => $data->subjectId);
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$professors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$professors = $professors->toArray();
				foreach ($professors as $key => $professor) {
					if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1' && !empty($professor['profilePic']) ) {
						require_once 'amazonRead.php';
						$professors[$key]['profilePic'] = getSignedURL($professor['profilePic']);
					} else {
						$professors[$key]['profilePic'] = 'assets/pages/media/users/avatar80_8.jpg';
					}
				}
				$result->professors = $professors;
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function saveSubjectImage($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				if(!$this->userOwnsSubject($data)){
					$result->status = 0;
					$result->message = "An non recoverable error occured1!";
					return $result;
				}
				$adapter = $this->adapter;
				$where = array('id' => $data->subjectId);
				$sql = new Sql($adapter);
				
				//deleting previous image from cloud
				$select = $sql->select();
				$select->from('subjects');
				$select->where($where);
				$select->columns(array('image'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$pic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$pic = $pic->toArray();
				$pic = $pic[0]['image'];
				$pic = substr($pic, 37);
				if($pic != '') {
					require_once 'amazonDelete.php';
					deleteFile($pic);
				}
				$update = $sql->update();
				$update->table('subjects');
				$update->set(array('image' => $data->imageName));
				$update->where($where);
				$statement = $sql->prepareStatementForSqlObject($update);
				$count = $statement->execute()->getAffectedRows();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$result->image = getSignedURL($data->imageName);
				} else {
					$result->image = 'assets/pages/media/profile/profile_user.jpg';
				}
				$result->status = 1;
				$result->message = 'Subject Image Uploaded!';
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function saveSubjectSyllabus($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				if(!$this->userOwnsSubject($data)){
					$result->status = 0;
					$result->message = "An non recoverable error occured!";
					return $result;
				}
				$adapter = $this->adapter;
				$where = array('id' => $data->subjectId);
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('subjects');
				$update->set(array('syllabus' => $data->syllabus));
				$update->where($where);
				$statement = $sql->prepareStatementForSqlObject($update);
				$count = $statement->execute()->getAffectedRows();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = 'Subject syllabus Uploaded!';
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		//
		public function subjectAlreadyImported($data) {
			$result =new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('subjects');
			$where = new \Zend\Db\Sql\Where();
			$where->equalTo('parentId', $data->subjectId);
			$where->equalTo('courseId', $data->importToCourseId);
			$where->equalTo('deleted', 0);
			$select->where($where);
			$selectString = $sql->getSqlStringForSqlObject($select);
			// var_dump($selectString);
			// die();
			$subject = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$subject = $subject->toArray();
			if(count($subject) == 0)
				return false;
			return true;
		}
		
		public function getSubjectsForExam($data) {
			$result =new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				//checking if course id is -1 i.e. 'assigned subjects' from professor account
				if($data->courseId == -1) {
					$result->subjects = $this->getAssignedSubjectNames($data);
					$result->status = 1;
					return $result;
				}
				$select->from('subjects');
				if(isset($data->import) && $data->import == true)
					$select->where(array('courseId'	=>	$data->courseId,
										'ownerId'	=>	$data->userId));
				else
					$select->where(array('courseId'	=>	$data->courseId,
										'ownerId'	=>	$data->userId,
										'deleted'	=>	0));
				$select->columns(array('id','name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->subjects = $subjects->toArray();
				$result->courseId = $data->courseId;
				if(count($subjects) == 0) {
					$result->status = 0;
					$result->message = "No Subjects found";
				}
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}

		public function getAssignedSubjectNames($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('sp' => 'subject_professors'));
				$select->join(array('s' => 'subjects'), 's.id = sp.subjectId', array('id', 'name'));
				$select->where(array('professorId' => $data->userId));
				$select->columns(array());
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects = $subjects->toArray();
				return $subjects;
			}catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function getAlldeletedSubject($data) {
			$result = new stdClass();
			$adapter = $this->adapter;
			//$sql = new Sql($adapter);
			try {
				$query = "SELECT s.id subjectid ,s.name subject,CONCAT( c.name,'(C00',c.id,')') course,l.id instituteid  ,l.email,u.contactMobile ,
				case 
				when ur.roleId= 1 then (select name from institute_details where userId= l.id)
				when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
				when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
				end as username
				from subjects s join courses c on c.id=s.courseId
				JOIN login_details l on l.id=c.ownerId   Join user_roles  ur on ur.userId=l.id
				JOIN user_details  u on l.id=u.userId
				where s.deleted=1 and  ur.roleId not in (4,5) order by l.id ";
			$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->subjects = $subjects->toArray();
			if (count($result->subjects) == 0) {
				$result->status = 0;
				$result->exception = "No deleted subjects Found";
				return $result;
			}
			$result->status = 1;
			return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
	
		public function restoreSubjectByAdmin($req) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('subjects');
				$update->set(array('deleted' => 0));
				$update->where(array('id' => $req->subjectId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$result = $statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
							
				$subject = $this->getCourseDetailsbySubject($req->subjectId);

				$course = $subject->subject;
				$courseId = $course[0]['courseId'];
				$coursename = $course[0]['course'];
				$subjectname = $course[0]['subject'];
				$institute = $course[0]['institute'];
				$institutemail = $course[0]['institutemail'];
				$instituteId = $course[0]['instituteId'];
			
			
				// insert notification
				$notification="Subject ".$this->safeString($subjectname)." (subject id : $req->subjectId  belonging to course ".$this->safeString($coursename)." ( course id : $courseId ) has been restored.";
				$query = "Insert into notifications (userId,message) values ($instituteId,'$notification')";
				$notification=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);

				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		//function to fetch all independent exams in a subject
		public function getIndependentExams($data) {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('exams');
			$select->where(array(
						'subjectId' => $data->subjectId,
						'delete'	=> 0,
						'chapterId' => 0
			));
						
			$select->columns(array('id', 'name', 'type','startDate','endDate','attempts','status'));
			$statement = $sql->getSqlStringForSqlObject($select);
			$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			$exams = $exams->toArray();
			$result->exams = $exams;
			$result->status = 1;
			return $result;
		}
		//function to fetch all independent exams in a subject
		public function getIndependentExamsNew($data) {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('exams');
			$where = array(
						'subjectId' => $data->subjectId,
						'chapterId' => 0,
						'type'		=> 'Exam'
			);
			//if ($data->userRole == 4) {
				$where['delete'] = 0;
			//}
			$select->where($where);
						
			$select->columns(array('id', 'name', 'type','startDate','endDate','attempts','status','delete'));
			$statement = $sql->getSqlStringForSqlObject($select);
			$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			$exams = $exams->toArray();
			foreach ($exams as $key => $exam) {
				$exams[$key]['result'] = 0;
				if ($exams[$key]['status'] > 0) {
					$select = "SELECT COUNT(*) AS total
								FROM exam_attempts AS ea
								INNER JOIN student_subject AS ss ON ss.userId=ea.studentId AND ss.Active=1
								WHERE ss.subjectId={$data->subjectId} AND examId={$exam['id']} AND completed=1";
					$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$total = $total->toArray();
					$total = $total[0]['total'];
					if ($total > 0) {
						$exams[$key]['result'] = 1;
					}
				}
			}
			$result->exams = $exams;

			$select = $sql->select();
			$select->from('exams');
			$where = array(
						'subjectId' => $data->subjectId,
						'chapterId' => 0,
						'type'		=> 'Assignment'
			);
			//if ($data->userRole == 4) {
				$where['delete'] = 0;
			//}
			$select->where($where);
						
			$select->columns(array('id', 'name', 'type','startDate','endDate','attempts','status','delete'));
			$statement = $sql->getSqlStringForSqlObject($select);
			$assignments = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			$assignments = $assignments->toArray();
			foreach ($assignments as $key => $assignment) {
				$assignments[$key]['result'] = 0;
				if ($assignments[$key]['status'] > 0) {
					$select = "SELECT COUNT(*) AS total
								FROM exam_attempts AS ea
								INNER JOIN student_subject AS ss ON ss.userId=ea.studentId AND ss.Active=1
								WHERE ss.subjectId={$data->subjectId} AND examId={$assignment['id']} AND completed=1";
					$total = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
					$total = $total->toArray();
					$total = $total[0]['total'];
					if ($total > 0) {
						$assignments[$key]['result'] = 1;
					}
				}
			}
			$result->assignments = $assignments;

			$subjectiveExams = $this->getsubjectiveExams($data);
			if ($subjectiveExams->status == 1) {
				$subjectiveExams = $subjectiveExams->exams;
			} else {
				$subjectiveExams = array();
			}

			if (count($subjectiveExams)) {

				foreach ($subjectiveExams as $key1 => $subjectiveExam) {

					$selectString = "SELECT COUNT(A.student) as num, A.attempt as attempt
					FROM (
						SELECT COUNT(DISTINCT ea.id) as attempt, ea.studentId as student
						FROM `subjective_exam_attempts` AS ea
						INNER JOIN student_subject AS ss ON ss.userId=ea.studentId AND ss.Active=1
						WHERE ss.subjectId={$data->subjectId} AND ea.examId={$subjectiveExam['id']} AND ea.completed = 1
						GROUP BY ea.studentId
					) as A GROUP BY A.attempt";
					$graph = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$graph = $graph->toArray();
					if(!(count($graph)>0))
					{
						$subjectiveExams[$key1]['result'] = 0;
					} else {
						$subjectiveExams[$key1]['result'] = 1;
					}
				}

			}

			$result->subjectiveExams = $subjectiveExams;

			$manualExams = $this->getManualExams($data);
			if ($manualExams->status == 1) {
				$manualExams = $manualExams->exams;
			} else {
				$manualExams = array();
			}
			$result->manualExams = $manualExams;

			require_once("Submission.php");
			$sub = new Submission();
			$submissions = $sub->getSubmissions($data);
			if ($submissions->status == 1) {
				$submissions = $submissions->exams;
			} else {
				$submissions = array();
			}

			if (count($submissions)) {

				foreach ($submissions as $key1 => $submission) {

					$selectString = "SELECT COUNT(A.student) as num, A.attempt as attempt FROM (
							SELECT COUNT(DISTINCT ea.id) as attempt, ea.studentId as student
							FROM `submission_attempts` AS ea
							INNER JOIN student_subject AS ss ON ss.userId=ea.studentId AND ss.Active=1
							WHERE ss.subjectId={$data->subjectId} AND ea.examId={$submission['id']} AND ea.completed = 1
							GROUP BY ea.studentId
						) as A GROUP BY A.attempt
						UNION
						SELECT COUNT(A.student) as num, A.attempt as attempt FROM (
							SELECT COUNT(DISTINCT ea.id) as attempt, ea.studentId as student
							FROM `submission_attempts` AS ea
							INNER JOIN student_subject AS ss ON ss.userId=ea.studentId AND ss.Active=1
							WHERE ss.subjectId={$data->subjectId} AND ea.examId={$submission['id']} AND ea.completed = 1
							GROUP BY ea.studentGroupId
						) as A GROUP BY A.attempt";
					$graph = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$graph = $graph->toArray();
					if(!(count($graph)>0))
					{
						$submissions[$key1]['result'] = 0;
					} else {
						$submissions[$key1]['result'] = 1;
					}
				}

			}
			$result->submissions = $submissions;

			$result->status = 1;
			return $result;
		}
		//
		public function getsubjectiveExams($data, $chapterId = NULL) {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('exam_subjective');
			$where = array(
						'subjectId' => $data->subjectId
			);
			//if ($data->userRole == 4) {
				$where['Active'] = 1;
			//}
			$select->where($where);
			if (!empty($chapterId)) {
				//$where['chapterId'] = $chapterId;
				$select->where("`chapterId` = $chapterId;");
			} else {
				$select->where("`chapterId` IN (0,-1)");
			}

			$select->columns(array('id', 'name', 'type','startDate','endDate','attempts','status','Active'));
			$statement = $sql->getSqlStringForSqlObject($select);
			$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			$exams = $exams->toArray();
			$result->exams = $exams;
			$result->status = 1;
			return $result;
		}
		//
		public function getManualExams($data, $chapterId = NULL) {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('exam_manual');
			$where = array(
						'subjectId' => $data->subjectId,
						'chapterId' => 0
			);
			//if ($data->userRole == 4) {
				$where['status'] = 1;
			//}
			if (!empty($chapterId)) {
				$where['chapterId'] = $chapterId;
			}
			$select->where($where);

			$select->columns(array('id', 'name', 'type', 'status'));
			$statement = $sql->getSqlStringForSqlObject($select);
			$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			$exams = $exams->toArray();
			$result->exams = $exams;
			$result->status = 1;
			return $result;
		}
		public function getSubjectiveExamAll($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exam_subjective');
				$select->where(array(
							'id' => $data->examId,
							'Active'	=>	1
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				$result->exams = $exams;
				//fetching questions for exams
				if(!(sizeof($exams)>0))
				{
					$result->status = 0;
					$result->message = 'Invalid Exam';
					return $result;
				}
				//print_r(sizeof($exams));
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('qs'=>'questions_subjective'));
				$select->join(array('l'	=>'question_subjective_exam_links'), 'l.questionId = qs.id', array('marks'));
				$select->where(array(
							'examId' => $data->examId,
							'deleted'	=>	0
				));
				$statement = $sql->getSqlStringForSqlObject($select);
				$questions = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$questions = $questions->toArray();
				$subjectiveQuestions=array();
				$parentIdTemp= array();
				$i			 = 0;
				foreach($questions as $key=>$subQues)
				{
					$question=array();
					/*$questionId = preg_split('/[n.\s;]+/', $subQues['questionId']);
					$questionId= ('n'.$questionId[1]);*/
					$questionId = $subQues['id'];
					$questionString = $subQues['question'];
					$answerString = $subQues['answer'];

					if($subQues['parentId']==0)
					{
						$query="SELECT qt.id, qt.tag AS name
							FROM `subjective_question_tag_links` AS sqtl
							INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
							WHERE sqtl.question_id= '$questionId'";
						$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$tags = $tags->toArray();

						$parentIdTemp[$i]=$subQues['id'];
						$i++;
						if($subQues['noOfSubquestionRequired']>0)
						{
							/*$questionId						=preg_split('/[s.\s;]+/', $questionId);
							$questionId							=$questionId[0];*/
							$question['id']						=$subQues['id'];
							$question['questionType']			='questionGroup';
							$question['question']				=$questionString;
							$question['questionBrief']			=$this->displayString($questionString);
							$question['answer']					=$subQues['noOfSubquestionRequired'];
							$question['subquestionMrks']		=$subQues['marks'];
							$question['questionsonPage']		=$subQues['questionsonPage'];
							$question['difficulty']				=$subQues['difficulty'];
							$question['tags']					=$tags;

							$question['subQuestions']			=array();
						} else {
							$question['id']				=$questionId;
							$question['questionType']	='question';
							$question['question']		=$questionString;
							$question['questionBrief']	=$this->displayString($questionString);
							$question['answer']			=$answerString;
							$question['answerBrief']	=$this->displayString($answerString);
							$question['marks']			=$subQues['marks'];
							$question['difficulty']		=$subQues['difficulty'];
							$question['tags']			=$tags;
							$question['subQuestions']	=array();
						}
						array_push($subjectiveQuestions,$question);
						
					} else {
						$parentId=$subQues['parentId'];
						$subquestionsArray=array();
						/*$subQuestionId=preg_split('/[s.\s;]+/', $questionId);
						$subQuestionId= ('s'.$subQuestionId[1]);*/
						$subQuestionId=$subQues['id'];
						if(in_array($parentId,$parentIdTemp))
						{
							$index=array_search($subQues['parentId'],$parentIdTemp);

							$subquestionsArray['id']=$subQuestionId;
							$subquestionsArray['questionType']='question';
							$subquestionsArray['question']=$questionString;
							$subquestionsArray['questionBrief']	=$this->displayString($questionString);
							$subquestionsArray['answer']=$answerString;
							$subquestionsArray['answerBrief']	=$this->displayString($answerString);
							array_push($subjectiveQuestions[$index]['subQuestions'],$subquestionsArray);
						}
					}
				}
				
				$result->questions = $subjectiveQuestions;

				$result->totalMarks = $this->getMaxMarksSubjectiveExam($data->examId);
				
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getCourseSubjectsForStudent($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$query="SELECT c.name course,c.image courseimg,c.availStudentMarket,
						CASE 
							WHEN u.roleId= 1 THEN (SELECT name FROM institute_details WHERE userId=c.ownerId)
							WHEN u.roleId= 2 THEN (SELECT CONCAT(firstname, ' ', lastname)  FROM professor_details WHERE userId=c.ownerId)
							WHEN u.roleId= 3 THEN (SELECT CONCAT(firstname, ' ', lastname)  FROM publisher_details WHERE userId=c.ownerId)
						END Institute FROM courses c
						JOIN user_roles u ON u.userId=c.ownerId AND roleId NOT IN (4,5)
						WHERE c.id=$data->courseId";
				$course = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->course = $course->toArray();
				
				foreach($result->course as $key => $detail) {
								 if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$result->course[$key]['courseimg'] = getSignedURL($detail['courseimg']);
								 }
				}
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('student_subject');
				$select->columns(array('id','subjectId'));
				$select->where(array('courseId' => $data->courseId,'userId' => $data->userId,'Active'=>1));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjects1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects1 = $subjects1->toArray();
				$result->subjects = array();		
				if(count($subjects1)>0)
				{
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('subjects');
					$select->columns(array('id','name','image'));
					$select->where(array('courseId' => $data->courseId,'deleted'=>0))
							->order('weight ASC')
							->order('id ASC');
					//$stringSubjectids=[];
					foreach($subjects1 as $key=>$value)
					{
						//$stringSubjectids .=','.$value['subjectId'];
						$stringSubjectids[$key]=$value['subjectId'];
					}
				
					$select->where->In('id', $stringSubjectids);	
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$result->subjects = $subjects->toArray();
					foreach($result->subjects as $key => $detail) {
						if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
							require_once 'amazonRead.php';
							$result->subjects[$key]['image'] = getSignedURL($detail['image']);
						}
					}
				}
				if($result->course[0]['availStudentMarket'] == 1)
				{
					require_once "Exam.php";
					$ch = new Exam();
					$certification=$ch->checkCertificationUser($data);
					$result->certificationStatus=$certification->certificationStatus;
					$result->certification=$certification->certification;
				}
				else{
						$result->certificationStatus =0;
						$result->certification ='';
				}
				$query="SELECT coursesDoc FROM courses where id= $data->courseId and deleted=0";
				$coursesDoc = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$coursesDoc=$coursesDoc->toArray();
				$coursesDoc =$coursesDoc[0]['coursesDoc'];
				$result->coursesDoc='';
				if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1' && isset($coursesDoc) && !empty($coursesDoc)) {
						require_once 'amazonRead.php';
						$result->coursesDoc = getSignedURL($coursesDoc);
				}
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function getCourseSubjectsForStudentNew($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$query="SELECT c.name course,c.image courseimg,c.availStudentMarket,
						CASE 
							WHEN u.roleId= 1 THEN (SELECT name FROM institute_details WHERE userId=c.ownerId)
							WHEN u.roleId= 2 THEN (SELECT CONCAT(firstname, ' ', lastname)  FROM professor_details WHERE userId=c.ownerId)
							WHEN u.roleId= 3 THEN (SELECT CONCAT(firstname, ' ', lastname)  FROM publisher_details WHERE userId=c.ownerId)
						END Institute
						FROM courses c
						INNER JOIN user_roles u ON u.userId=c.ownerId AND roleId NOT IN (4,5)
						INNER JOIN student_course sc ON sc.course_id={$data->courseId} AND sc.user_id={$data->userId}
						WHERE c.id=$data->courseId";
				$course = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->course = $course->toArray();
				
				if (count($result->course) == 0) {
					$result->status = 0;
					$result->exception = "Course not found";
					return $result;
				}

				foreach($result->course as $key => $detail) {
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
						require_once 'amazonRead.php';
						$result->course[$key]['courseimg'] = getSignedURL($detail['courseimg']);
					}
				}

				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('ss'	=>	'student_subject'))
				->join(array('s'	=>	'subjects'),
						's.id = ss.subjectId',
						array('id', 'name', 'image')
				);
				$select->where(array('userId'	=>	$data->userId,'ss.courseId'	=>	$data->courseId, 's.deleted'=>0,'Active'=>1))
							->group('s.id')
							->order('weight ASC')
							->order('s.id ASC');
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects = $subjects->toArray();
				if(count($subjects) > 0) {
					foreach ($subjects as $key => $subject) {
						$subjects[$key]['chapters']	= 0;
						$subjects[$key]['exams']	= 0;
						// lectures
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from(array('c'	=>	'chapters'))
						->join(array('co'	=>	'content'),
								'c.id = co.chapterId',
								array('id')
						);
						$select->where(array('subjectId'	=>	$subject['id'], 'deleted'=>0, 'published'=>1));
						$select->columns(array('id'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$chapters = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$chapters = $chapters->toArray();
						if(count($chapters) >= 0)
							$subjects[$key]['chapters']+= count($chapters);

						// objective exams
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from(array('e'	=>	'exams'));
						$select->where(array('subjectId'	=>	$subject['id'], 'delete'=>0));
						$select->columns(array('id', 'name'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$exams = $exams->toArray();
						if(count($exams) >= 0)
							$subjects[$key]['exams']+= count($exams);
						// subjective exams
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from(array('e'	=>	'exam_subjective'));
						$select->where(array('subjectId'	=>	$subject['id'], 'Active'=>1));
						$select->columns(array('id', 'name'));
						$selectString = $sql->getSqlStringForSqlObject($select);
						$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$exams = $exams->toArray();
						if(count($exams) >= 0)
							$subjects[$key]['exams']+= count($exams);

						if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
							require_once 'amazonRead.php';
							$subjects[$key]['image'] = getSignedURL($subject['image']);
						} else {
							$subjects[$key]['image'] = 'assets/pages/img/background/34.jpg';
						}
					}
					$result->subjects = $subjects;
				}

				if($result->course[0]['availStudentMarket'] == 1)
				{
					require_once "Exam.php";
					$ch = new Exam();
					$certification=$ch->checkCertificationUser($data);
					$result->certificationStatus=$certification->certificationStatus;
					$result->certification=$certification->certification;
				}
				else{
						$result->certificationStatus =0;
						$result->certification ='';
				}
				$query="SELECT coursesDoc FROM courses where id= $data->courseId and deleted=0";
				$coursesDoc = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$coursesDoc = $coursesDoc->toArray();
				$result->coursesDoc='';
				if (count($coursesDoc)) {
					$coursesDoc = $coursesDoc[0]['coursesDoc'];
					if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1' && isset($coursesDoc) && !empty($coursesDoc)) {
						require_once 'amazonRead.php';
						$result->coursesDoc = getSignedURL($coursesDoc);
					}
				}
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		//to get exam attempts for subject
		public function getSubjectAttempts($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('exams');
				$select->columns(array('id','name'));
				$select->where(array(
						'subjectId'	=>	$data->subjectId,
						'delete'	=>	0,
						'status'	=>	2
				));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->exams = $exams->toArray();
				foreach($result->exams as $key=>$exam) {
					$select = $sql->select();
					$select->from('exam_attempts');
					$select->where(array('examId'	=>	$exam['id']));
					$select->columns(array('count'	=>	new \Zend\Db\Sql\Expression("COUNT(DISTINCT studentId)")));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$count = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$count = $count->toArray();
					$result->exams[$key]['count'] = $count[0]['count'];
				}
				$select = $sql->select();
				$select->from('student_course');
				$select->where(array('course_id'	=>	$data->courseId));
				$select->columns(array('totalStudents'	=>	new \Zend\Db\Sql\Expression("COUNT(DISTINCT user_id)")));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$count = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$count = $count->toArray();
				$result->totalStudents = $count[0]['totalStudents'];
	
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}				
		public function getCourseDetailsbySubject($subjectId) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$query="select s.name subject,c.name course, c.id courseId, l.email institutemail,l.id instituteId,
						case 
						when ur.roleId= 1 then (select name from institute_details where userId= l.id)
							when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
							when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
							end as institute
						from subjects s
						JOIN courses c on c.id=s.courseId
						JOIN login_details l on c.ownerId=l.id
						Join user_roles  ur on ur.userId=l.id where s.id=$subjectId and ur.roleId not in (4,5)";
				$subject = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->subject = $subject->toArray();
				
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		//function to get subject name availability
		public function checkDuplicateSubject($data) {
			$result = new stdClass();
			$search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
			$replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$name = trim(strtolower($data->subjectName));
				if(isset($data->subjectId)) {
					/*$select = $sql->select();
					$select->from('subjects');
					$where = new \Zend\Db\Sql\Where();
					$where->equalTo('courseId', $data->courseId);
					$where->equalTo('deleted', 0);
					$where->equalTo('name', $name);
					$where->notEqualTo('id', $data->subjectId);
					$select->where($where);
					$select->columns(array('name'));
					$selectString = $sql->getSqlStringForSqlObject($select);*/
					$selectString = "SELECT name FROM subjects WHERE courseId={$data->courseId} AND TRIM(LCASE(name))='".str_replace($search, $replace, $name)."' AND id!='{$data->subjectId}' and deleted=0";
				}
				else {
					/*$asd = new \Zend\Db\Sql\Expression("TRIM(LCASE(name))");
					$select = $sql->select();
					$select->from('subjects');
					$where = new \Zend\Db\Sql\Where();
					$where->equalTo('courseId', $data->courseId);
					$where->equalTo('deleted', 0);
					$where->equalTo("name", str_replace($search, $replace, $name));
					$select->where($where);
					$select->columns(array('name'));
					$selectString = $sql->getSqlStringForSqlObject($select);*/
					$selectString = "SELECT name FROM subjects WHERE courseId={$data->courseId} AND TRIM(LCASE(name))='".str_replace($search, $replace, $name)."' AND deleted=0";
				}
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				if(count($temp) == 0)
					$result->available = true;
				else
					$result->available = false;
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function getSubjectInvitation($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$selectString= "SELECT n.id, i.name, i.userId AS instituteId, n.`status`, ur.roleId AS userRole
								FROM `institute_invitations` n
								INNER JOIN institute_details i ON n.institute_id=i.userId
								INNER JOIN user_roles ur ON ur.userId=i.userId
								WHERE n.prof_id={$data->userId}";
				$notifications = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$notifications = $notifications->toArray();
				foreach($notifications AS $key=>$notification) {
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
						require_once 'amazonRead.php';
						$notifications[$key]['profilePic'] = getSignedURL($notification['profilePic']);
					} else {
						$notifications[$key]['profilePic'] = 'assets/pages/media/users/avatar10.jpg';
					}
				}
				$result->notification = $notifications;
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$result->status = 0;
							$result->exception = $e->getMessage();
							return $result;
			}
		} 
		public function acceptSubjectInvitation($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$selectString="UPDATE  institute_invitations set status=1 WHERE id={$data->notificationId}";
				$update = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$selectString="INSERT INTO institute_professors (instituteId,professorId) SELECT institute_id, prof_id FROM institute_invitations WHERE id={$data->notificationId};";
				$insert = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function rejectSubjectInvitation($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$selectString="UPDATE  institute_invitations set status=2 where id={$data->notificationId}";
				$update = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function getSubjectAssigned($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$selectString = "SELECT s.id subjectId, s.image subjectImage, s.name, c.liveDate, c.endDate, c.id courseId, c.name course, c.ownerId instituteId,case when ur.roleId= 1 then (SELECT name FROM institute_details WHERE userId = c.ownerId) WHEN ur.roleId= 2 then (SELECT CONCAT(firstname, ' ', lastname) FROM professor_details WHERE userId=  c.ownerId) when ur.roleId= 3 then (SELECT CONCAT(firstname, ' ', lastname) FROM publisher_details WHERE userId= c.ownerId) end AS institute, c.image AS courseImage FROM `subject_professors` sp JOIN subjects  s ON sp.subjectId=s.id JOIN courses c ON s.courseId=c.id Join user_roles ur ON ur.userId= c.ownerId  WHERE sp.professorId={$data->userId} AND ur.roleId NOT IN (4,5)";
				$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects = $subjects->toArray();
				foreach($subjects AS $key=>$subject) {
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
						require_once 'amazonRead.php';
						$subjects[$key]['courseImage'] = getSignedURL($subject['courseImage']);
						$subjects[$key]['subjectImage'] = getSignedURL($subject['subjectImage']);
					}
					$subjects[$key]['subjects'] = $this->getCourseSubjectsNameOnly($subject['courseId']);
				}
				$result->subjects = $subjects;
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function getSubjectAssignedNew($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$selectString ="SELECT c.*
								FROM `subject_professors` sp
								INNER JOIN subjects s ON sp.subjectId=s.id
								INNER JOIN courses c ON s.courseId=c.id
								WHERE sp.professorId={$data->userId}
								GROUP BY c.id";
				$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$courses = $courses->toArray();
				foreach($courses AS $key=>$course) {
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
						require_once 'amazonRead.php';
						$courses[$key]['image'] = getSignedURL($course['image']);
					} else {
						$course['image'] = "assets/pages/img/background/32.jpg";
					}
					/*$req = new stdClass();
					$req->courseId = $course['id'];
					$subjects = $this->getSubjectsForCourse($req);
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
						require_once 'amazonRead.php';
						$course['image'] = getSignedURL($c['image']);
					} else {
						$course['image'] = "assets/pages/img/background/32.jpg";
					}
					$courses[$key]['subjects'] = $subjects;*/
					$selectString ="SELECT s.*
									FROM `subject_professors` sp
									INNER JOIN subjects s ON sp.subjectId=s.id
									INNER JOIN courses c ON s.courseId=c.id
									WHERE c.id = {$course['id']} AND sp.professorId={$data->userId}";
					$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$subjects = $subjects->toArray();
					foreach($subjects AS $key1=>$subject) {
						if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
							require_once 'amazonRead.php';
							$subjects[$key1]['image'] = getSignedURL($subject['image']);
						} else {
							$subjects[$key1]['image'] = "assets/pages/img/background/34.jpg";
						}
						$subjects[$key1]['chapters'] = $this->getSubjectChaptersCount($subject['id']);
						$subjects[$key1]['exams'] = $this->getSubjectChaptersCount($subject['id']);
					}
					$courses[$key]['subjects'] = $subjects;
				}
				$result->courses = $courses;
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function checkSubjectAssigned($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$adapter = $this->adapter;
				$query = "SELECT id from subjects s left join subject_professors sp on sp.subjectId=s.id where ( s.ownerId=$data->userId or sp.professorId=$data->userId) and s.id=$data->subjectId";
				$subjects = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$result->subjects = $subjects->toArray();
				if(count($result->subjects)>0){
					$query = "SELECT s.id from subjects s join courses c  on c.id=s.courseId where  s.id=$data->subjectId and c.id=$data->courseId";
					$checkCourse = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$result->course = $checkCourse->toArray();
					if (count($result->course) > 0) {
							  return true;
						}
					return false;
				}
				return false;
			} catch (Exception $e) {
				// $result->status = 0;
				// $result->exception = $e->getMessage();
				return false;
			}
		}	
		public function subjectIsFromCourse($data) {
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subjects');
				$select->where(array(
					'courseId'	=>	$data->courseId,
					'id'		=>	$data->subjectId
				));
				$select->columns(array('id'));
				$string = $sql->getSqlStringForSqlObject($select);
				$owns = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
				$owns = $owns->toArray();
				if(count($owns) == 0)
					return false;
				else
					return true;
			} catch(Exception $e) {
				return false;
			}
		}	
		public function subjectCourseExpired($data) {
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('courses');
				$select->where(array('id'	=>	$data->courseId));
				$select->columns(array('id','endDate'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$temp = $temp->toArray();
				if(count($temp) > 0) {
					$end = $temp[0]['endDate'];
					if($end == '')
						return false;
					$end = $end/1000;
					if((int)$end < time())
						return true;
					else
						return false;
				}
				return false;
			}catch(Exception $e) {
				return true;
			}
		}
		public function getCourseSubjectsNameOnly($courseId) {
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subjects')->where(array('courseId' => $courseId, 'deleted' => 0))->columns(array('name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$names = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$names = $names->toArray();
				$temp = array();
				foreach($names as $name) {
					$temp[] = $name['name'];
				}
				return implode(', ', $temp);
			}catch(Exception $e) {
				return false;
			}
		}
		public function getSubjectStudents($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('subjects');
				$select->where( array( 'id'	=>	$data->subjectId,
										'deleted'	=>	0));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects = $subjects->toArray();
				if (count($subjects) == 0) {
					$result->status = 0;
					$result->exception = "Invalid subject!";
					return $result;
				}
				if ( (isset($data->type)) && ($data->type == "registered") ) {
					$query="SELECT L.id, L.username, IFNULL(utp.password,'') AS password, u.contactMobile, CONCAT( S.firstName,' ',S.lastName) AS name,L.email,IFNULL(plg.id,'') AS parentid,IFNULL(plg.userid,'') AS userid,IFNULL(plg.passwd,'') AS passwd, L.temp
					FROM student_subject AS ss
					INNER JOIN login_details AS L ON L.id=ss.userId
					LEFT JOIN user_details AS u ON u.userId=L.id
					LEFT JOIN student_details AS S ON S.userId=L.id
					LEFT JOIN user_temp_passwords AS utp ON utp.userId=L.id
					LEFT JOIN parent_login_short AS plg ON plg.student_id=L.id AND plg.course_id=ss.courseId
					WHERE ss.courseId=$data->courseId AND ss.subjectId=$data->subjectId AND ss.Active=1 AND L.temp=0
					GROUP BY L.id";
				} else {
					$query="SELECT L.id, L.username, IFNULL(utp.password,'') AS password, u.contactMobile, CONCAT( S.firstName,' ',S.lastName) AS name,L.email,IFNULL(plg.id,'') AS parentid,IFNULL(plg.userid,'') AS userid,IFNULL(plg.passwd,'') AS passwd, L.temp
					FROM student_subject AS ss
					INNER JOIN login_details AS L ON L.id=ss.userId
					LEFT JOIN user_details AS u ON u.userId=L.id
					LEFT JOIN student_details AS S ON S.userId=L.id
					LEFT JOIN user_temp_passwords AS utp ON utp.userId=L.id
					LEFT JOIN parent_login_short AS plg ON plg.student_id=L.id AND plg.course_id=ss.courseId
					WHERE ss.courseId=$data->courseId AND ss.subjectId=$data->subjectId AND ss.Active=1
					GROUP BY L.id";
				}
				$students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$students = $students->toArray();
				if (count($students) == 0) {
					$result->message = "No students Enrolled in the Subject";
					$result->status = 0;
					return $result;
				}
				foreach ($students as $key => $student) {
					$students[$key]['passwd'] = base64_decode($students[$key]['passwd']);
				}
				$result->students=$students;
				$result->status=1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function getSubjectParents($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();	
				$select->from('subjects');
				$select->where( array( 'id'	=>	$data->subjectId,
										'deleted'	=>	0));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects = $subjects->toArray();
				if (count($subjects) == 0) {
					$result->status = 0;
					$result->exception = "Invalid subject!";
					return $result;
				}
				$query="SELECT L.id, L.username, IFNULL(utp.password,'') AS password, u.contactMobile, CONCAT( S.firstName,' ',S.lastName) AS name,L.email,IFNULL(plg.id,'') AS parentid,IFNULL(plg.userid,'') AS userid,IFNULL(plg.passwd,'') AS passwd, L.temp
						FROM student_subject AS ss
						INNER JOIN login_details AS L ON L.id=ss.userId
						LEFT JOIN user_details AS u ON u.userId=L.id
						LEFT JOIN student_details AS S ON S.userId=L.id
						LEFT JOIN user_temp_passwords AS utp ON utp.userId=L.id
						INNER JOIN parent_login_short AS plg ON plg.student_id=L.id AND plg.course_id=ss.courseId
						WHERE ss.courseId=$data->courseId AND ss.subjectId=$data->subjectId AND ss.Active=1
						GROUP BY L.id";
				$students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$students = $students->toArray();
				if (count($students) == 0) {
					$result->message = "No students Enrolled in the Subject";
					$result->status = 0;
					return $result;
				}
				foreach ($students as $key => $student) {
					$students[$key]['passwd'] = base64_decode($students[$key]['passwd']);
				}
				$result->students=$students;
				$result->status=1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		public function getSubjectChaptersCount($subjectId) {
			$result = new stdClass();
			$result->chapters = 0;
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('c'	=>	'chapters'))
			->join(array('co'	=>	'content'),
					'c.id = co.chapterId',
					array('id')
			);
			$select->where(array('subjectId'	=>	$subjectId, 'deleted'=>0, 'published'=>1));
			$select->columns(array('id'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$chapters = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$chapters = $chapters->toArray();
			if(count($chapters) >= 0)
				$result->chapters+= count($chapters);
			return $result->chapters;
		}
		public function getSubjectExamsCount($subjectId) {
			$result = new stdClass();
			$result->exams = 0;
			$adapter = $this->adapter;
			// objective exams
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('e'	=>	'exams'));
			$select->where(array('subjectId'	=>	$subjectId, 'delete'=>0));
			$select->columns(array('id', 'name'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$exams = $exams->toArray();
			if(count($exams) >= 0)
				$result->exams+= count($exams);
			// subjective exams
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('e'	=>	'exam_subjective'));
			$select->where(array('subjectId'	=>	$subjectId, 'Active'=>1));
			$select->columns(array('id', 'name'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$exams = $exams->toArray();
			if(count($exams) >= 0)
				$result->exams+= count($exams);
			return $result->exams;
		}
		public function notifyParents($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$notification=$data->notification;
				if (isset($notification) && !empty($notification)) {
					if (empty($data->parentId)) {
						$query="SELECT pls.id
								FROM `parent_login_short` AS pls
								INNER JOIN `subjects` AS s on s.courseId=pls.course_id
								INNER JOIN `student_subject` AS ss ON ss.userId=pls.student_id
								WHERE ss.subjectId=$data->subjectId AND ss.Active=1";
						$parents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$parents = $parents->toArray();
						if (count($parents)) {
							$parentIds = array();
							foreach ($parents as $key => $parent) {
								//$parentIds[] = $parent['id'];
								// insert notification
								$query = "INSERT INTO parent_notifications (parentId,professorId,courseId,subjectId,message) VALUES (".$parent['id'].",{$data->userId},{$data->courseId},{$data->subjectId},'$notification')";
								$notif = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
							}
						}
					} else {
						// insert notification
						$query = "INSERT INTO parent_notifications (parentId,professorId,courseId,subjectId,message) VALUES ({$data->parentId},{$data->userId},{$data->courseId},{$data->subjectId},'$notification')";
						$notif = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					}
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
					/*require_once "User.php";
					$c = new User();
					$instituteSubject = "New Notification";
					$emailMessage = "Dear parent, <br> <br>";
					$emailMessage .= "You received a new notification on <a href='".$this->sitePath."'>Integro</a>.";
					$emailMessage .= '"'.$notification.'"';
					$c->sendMail($institutemail, $instituteSubject, $emailMessage);*/
					$result->status = 1;
					$result->message = 'Notification sent!';
				} else {
					$result->status = 0;
					$result->message = 'No notification entered!';
				}
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getSubjectAllStuff($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('s'	=>	'subjects'));
				$select->where(array('id'	=>	$data->subjectId));
				$select->columns(array('id', 'grading_notes'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects = $subjects->toArray();
				if(count($subjects) >= 0) {
					$result->notes = $subjects[0]['grading_notes'];
				}
				// subject content
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('e'	=>	'content'));
				$select->join(array('c'=>'chapters'), 'c.id = e.chapterId', array('chapterName'=>'name'), 'left');
				$select->where(array('c.subjectId'	=>	$data->subjectId, 'c.deleted'=>0));
				$select->columns(array('id', 'title', 'weight_grading'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$contents = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$contents = $contents->toArray();
				if(count($contents) >= 0) {
					foreach ($contents as $key => $content) {
						$dataR = new stdClass();
						$dataR->contentId = $content['id'];
						$dataR->contentType = 'content';
						$resSubjectRewards = $this->getSubjectRewardsbyContent($dataR);
						$contents[$key]['rewards'] = array();
						if ($resSubjectRewards->status == 1) {
							$contents[$key]['rewards'] = $resSubjectRewards->subjectRewards;
						}
					}
					$result->contents = $contents;
				}
				// objective exams
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('e'	=>	'exams'));
				$select->join(array('c'=>'chapters'), 'c.id = e.chapterId', array('chapterName'=>'name'), 'left');
				$select->where(array('e.subjectId'	=>	$data->subjectId, 'e.delete'=>0));
				$select->columns(array('id', 'name', 'type', 'startDate', 'endDate', 'attempts', 'grade_type', 'avg_attempts', 'weight'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				if(count($exams) >= 0) {
					foreach ($exams as $key => $exam) {
						$dataR = new stdClass();
						$dataR->contentId = $exam['id'];
						$dataR->contentType = 'exam';
						$resSubjectRewards = $this->getSubjectRewardsbyContent($dataR);
						$exams[$key]['rewards'] = array();
						if ($resSubjectRewards->status == 1) {
							$exams[$key]['rewards'] = $resSubjectRewards->subjectRewards;
						}
					}
					$result->exams = $exams;
				}
				// subjective exams
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('e'	=>	'exam_subjective'));
				$select->join(array('c'=>'chapters'), 'c.id = e.chapterId', array('chapterName'=>'name'), 'left');
				$select->where(array('e.subjectId'	=>	$data->subjectId, 'e.Active'=>1));
				$select->columns(array('id', 'name', 'type', 'startDate', 'endDate', 'attempts', 'grade_type', 'avg_attempts', 'weight'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				if(count($exams) >= 0) {
					foreach ($exams as $key => $exam) {
						$dataR = new stdClass();
						$dataR->contentId = $exam['id'];
						$dataR->contentType = 'subjective-exam';
						$resSubjectRewards = $this->getSubjectRewardsbyContent($dataR);
						$exams[$key]['rewards'] = array();
						if ($resSubjectRewards->status == 1) {
							$exams[$key]['rewards'] = $resSubjectRewards->subjectRewards;
						}
					}
					$result->subjective_exams = $exams;
				}
				// submissions
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('e'	=>	'submissions'));
				$select->join(array('c'=>'chapters'), 'c.id = e.chapterId', array('chapterName'=>'name'), 'left');
				$select->where(array('e.subjectId'	=>	$data->subjectId, 'e.deleted'=>0));
				$select->columns(array('id', 'name', 'startDate', 'endDate', 'attempts', 'grade_type', 'avg_attempts', 'weight'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				if(count($exams) >= 0) {
					foreach ($exams as $key => $exam) {
						$dataR = new stdClass();
						$dataR->contentId = $exam['id'];
						$dataR->contentType = 'submission';
						$resSubjectRewards = $this->getSubjectRewardsbyContent($dataR);
						$exams[$key]['rewards'] = array();
						if ($resSubjectRewards->status == 1) {
							$exams[$key]['rewards'] = $resSubjectRewards->subjectRewards;
						}
					}
					$result->submissions = $exams;
				}
				// manual exams
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('e'	=>	'exam_manual'));
				$select->join(array('c'=>'chapters'), 'c.id = e.chapterId', array('chapterName'=>'name'), 'left');
				$select->where(array('e.subjectId'	=>	$data->subjectId, 'e.status'=>1));
				$select->columns(array('id', 'name', 'grade_type', 'weight'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$exams = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				if(count($exams) >= 0) {
					foreach ($exams as $key => $exam) {
						$dataR = new stdClass();
						$dataR->contentId = $exam['id'];
						$dataR->contentType = 'manual-exam';
						$resSubjectRewards = $this->getSubjectRewardsbyContent($dataR);
						$exams[$key]['rewards'] = array();
						if ($resSubjectRewards->status == 1) {
							$exams[$key]['rewards'] = $resSubjectRewards->subjectRewards;
						}
					}
					$result->manual_exams = $exams;
				}
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getSubjectRewardsbyContent($data)
		{
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$where = array(
					'contentId'	=>	$data->contentId,
					'contentType'	=>	$data->contentType
				);
				$select->from(array('s'	=>	'subject_rewards_percent_blocks'));
				$select->where($where);
				$select->order('percent_block ASC');
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjectRewardPercents = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjectRewardPercents = $subjectRewardPercents->toArray();
				if (count($subjectRewardPercents) > 0) {
					foreach ($subjectRewardPercents as $key => $subjectRewardPercent) {
						$sql = new Sql($adapter);
						$select = $sql->select();
						$where = array(
							'subjectRewardPercentId'	=>	$subjectRewardPercent["id"]
						);
						$select->from(array('s'	=>	'subject_rewards'));
						$select->where($where);
						$selectString = $sql->getSqlStringForSqlObject($select);
						$subjectRewards = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$subjectRewardPercents[$key]["rewards"] = $subjectRewards->toArray();
					}
				}
				$result->status = 1;
				$result->subjectRewards = $subjectRewardPercents;
				return $result;
			} catch(Exception $e) {
				//$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getStudentSubjectRewardsbyContent($data)
		{
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$query="SELECT SUM(reward_amount) AS reward_amount,reward_currency
						FROM `student_subject_rewards`
						WHERE contentId={$data->contentId} AND contentType='{$data->contentType}' AND studentId={$data->studentId}
						GROUP BY reward_currency
						ORDER BY reward_amount ASC";
				$subjectRewardPercents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$subjectRewardPercents = $subjectRewardPercents->toArray();
				/*$select = $sql->select();
				$where = array(
					'contentId'	=>	$data->contentId,
					'contentType'	=>	$data->contentType,
					'studentId'			=>	$data->studentId
				);
				$select->from(array('s'	=>	'student_subject_rewards'));
				$select->where($where);
				$select->order('reward_amount ASC');
				$selectString = $sql->getSqlStringForSqlObject($select);
				//var_dump($selectString);
				$subjectRewardPercents = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjectRewardPercents = $subjectRewardPercents->toArray();*/
				$result->status = 1;
				$result->subjectRewards = $subjectRewardPercents;
				return $result;
			} catch(Exception $e) {
				//$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function putSubjectRewardsbyContent($data)
		{

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$dataR = new stdClass();
				$dataR->contentId = $data->contentId;
				$dataR->contentType = $data->contentType;
				$resSubjectRewards = $this->getSubjectRewardsbyContent($dataR);
				$percentage = $data->percentage;
				$rewardIds = array();
				if ($resSubjectRewards->status == 1) {
					$subjectRewards = $resSubjectRewards->subjectRewards;
					foreach ($subjectRewards as $key => $subjectReward) {
						//var_dump($subjectReward);
						$rewardIds[] = $subjectReward["id"];
						if ($percentage<=$subjectReward["percent_block"]) {
							break;
						}
					}
				}
				//var_dump($rewardIds);
				foreach ($rewardIds as $key => $rewardId) {
					$sql = new Sql($this->adapter);
					$select = $sql->select();
					$where = array(
						'subjectRewardPercentId'	=>	$rewardId,
						'studentId'	=>	$data->studentId
					);
					$select->from(array('s'	=>	'student_subject_rewards'));
					$select->where($where);
					$selectString = $sql->getSqlStringForSqlObject($select);
					$resStudentSubjectRewards = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$resStudentSubjectRewards = $resStudentSubjectRewards->toArray();
					if (count($resStudentSubjectRewards) == 0) {
						$sql = new Sql($this->adapter);
						$select = $sql->select();
						$where = array(
							'subjectRewardPercentId'	=>	$rewardId
						);
						$select->from(array('s'	=>	'subject_rewards'));
						$select->where($where);
						$selectString = $sql->getSqlStringForSqlObject($select);
						$resSubjectRewards = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$resSubjectRewards = $resSubjectRewards->toArray();
						foreach ($resSubjectRewards as $key => $subjectReward) {
							$subjectRewards = new TableGateway('student_subject_rewards', $adapter, null,new HydratingResultSet());
							$insert = array(
								'studentId'			=>	$data->studentId,
								'subjectRewardPercentId' =>	$rewardId,
								'subjectRewardId'	=>	$subjectReward['id'],
								'contentId'			=>	$data->contentId,
								'contentType'		=>	$data->contentType,
								'reward_amount'		=>	$subjectReward['reward_amount'],
								'reward_currency'	=>	$subjectReward['reward_currency'],
								'distributed'		=>	'0',
								'created'			=>	date('Y-m-d H:i:s'),
								'modified'			=>	date('Y-m-d H:i:s')
							);
							$subjectRewards->insert($insert);
						}						
					}
				}
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function saveSubjectGrading($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$notes=$data->notes;
				if (isset($notes) && !empty($notes)) {
					$where = array('id' => $data->subjectId);
					$updateValues = array(
						'grading_notes' =>	$data->notes
					);
					//var_dump($updateValues);die();
					$sql = new Sql($this->adapter);
					$update = $sql->update();
					$update->table('subjects');
					$update->set($updateValues);
					$update->where($where);
					$statement = $sql->prepareStatementForSqlObject($update);
					$result = $statement->execute();
				}
				$exams=$data->exams;
				if (isset($exams) && !empty($exams)) {
					foreach ($exams as $key => $exam) {
						switch ($exam->examType) {
							case 'content':
								$tableName = 'content';
								$updateValues = array(
									'weight_grading' 	=>	$exam->weight
								);
								break;
							case 'exam':
								$tableName = 'exams';
								$updateValues = array(
									'grade_type' 	=>	$exam->gradeType,
									'avg_attempts' 	=>	$exam->avgAttempts,
									'weight' 	=>	$exam->weight
								);
								break;
							case 'subjective-exam':
								$tableName = 'exam_subjective';
								$updateValues = array(
									'grade_type' 	=>	$exam->gradeType,
									'avg_attempts' 	=>	$exam->avgAttempts,
									'weight' 	=>	$exam->weight
								);
								break;
							case 'manual-exam':
								$tableName = 'exam_manual';
								$updateValues = array(
									'grade_type' 	=>	$exam->gradeType,
									'avg_attempts' 	=>	0,
									'weight' 	=>	$exam->weight
								);
								break;
							case 'submission':
								$tableName = 'submissions';
								$updateValues = array(
									'grade_type' 	=>	$exam->gradeType,
									'avg_attempts' 	=>	$exam->avgAttempts,
									'weight' 	=>	$exam->weight
								);
								break;
							default:
								break;
						}
						if (!empty($tableName)) {
							$where = array('id' => $exam->examId);
							//var_dump($updateValues);die();
							$sql = new Sql($this->adapter);
							$update = $sql->update();
							$update->table($tableName);
							$update->set($updateValues);
							$update->where($where);
							$statement = $sql->prepareStatementForSqlObject($update);
							$result = $statement->execute();
						}
					}
					// If all succeed, commit the transaction and all changes are committed at once.
					$db->commit();
					//$result->exams = $exams;
					$result->status = 1;
					$result->message = 'Grading saved!';
				} else {
					$result->status = 0;
					$result->message = 'No exams entered!';
				}
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function saveSubjectReward($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$query="SELECT *
						FROM `subject_rewards_percent_blocks`
						WHERE contentId={$data->contentId} AND contentType='{$data->contentType}' AND 
						((position<{$data->position} AND percent_block>={$data->percent_block}) || (position>{$data->position} AND percent_block<={$data->percent_block}))";
				$subjectRewardPerCheck = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$subjectRewardPerCheck = $subjectRewardPerCheck->toArray();
				if (count($subjectRewardPerCheck) > 0) {
					$result->status = 0;
					$result->message = "Percent block value is invalid. It has to be higher than previous percent blocks and lower than next previous blocks";
					return $result;
				}

				$sql = new Sql($this->adapter);
				$select = $sql->select();
				$where = array(
					'contentId'	=>	$data->contentId,
					'contentType'	=>	$data->contentType,
					'position'	=>	$data->position
				);
				$select->from(array('s'	=>	'subject_rewards_percent_blocks'));
				$select->where($where);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$resSubjectRewardPercents = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$resSubjectRewardPercents = $resSubjectRewardPercents->toArray();
				if (count($resSubjectRewardPercents) == 0) {
					$subjectRewards = new TableGateway('subject_rewards_percent_blocks', $adapter, null,new HydratingResultSet());
					$insert = array(
						'courseId'			=>	$data->courseId,
						'subjectId'			=>	$data->subjectId,
						'userId'			=>	$data->userId,
						'contentId'			=>	$data->contentId,
						'contentType'		=>	$data->contentType,
						'percent_block'		=>	$data->percent_block,
						'position'			=>	$data->position,
						'created'			=>	date('Y-m-d H:i:s'),
						'modified'			=>	date('Y-m-d H:i:s')
					);
					$subjectRewards->insert($insert);
					$rewardPercentBlockId = $subjectRewards->getLastInsertValue();
					foreach ($data->rewards as $key => $reward) {
						$subjectRewards = new TableGateway('subject_rewards', $adapter, null,new HydratingResultSet());
						$insert = array(
							'subjectRewardPercentId' =>	$rewardPercentBlockId,
							'reward_amount'		=>	$reward->amount,
							'reward_currency'	=>	$reward->currency,
							'created'			=>	date('Y-m-d H:i:s'),
							'modified'			=>	date('Y-m-d H:i:s')
						);
						$subjectRewards->insert($insert);
					}
				} else {
					$resSubjectRewardPercents = $resSubjectRewardPercents[0];
					$subjectRewardPercentId = $resSubjectRewardPercents["id"];

					$where = array('id' => $subjectRewardPercentId);
					$updateValues = array(
						'percent_block'		=>	$data->percent_block
					);
					//var_dump($updateValues);die();
					$sql = new Sql($this->adapter);
					$update = $sql->update();
					$update->table('subject_rewards_percent_blocks');
					$update->set($updateValues);
					$update->where($where);
					$statement = $sql->prepareStatementForSqlObject($update);
					$result = $statement->execute();

					$sql = new Sql($this->adapter);
					$select = $sql->select();
					$where = array(
						'subjectRewardPercentId'	=>	$subjectRewardPercentId
					);
					$select->columns(array('id'));
					$select->from(array('s'	=>	'subject_rewards'));
					$select->where($where);
					$selectString = $sql->getSqlStringForSqlObject($select);
					$resSubjectRewards = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$resSubjectRewards = $resSubjectRewards->toArray();
					$dbRewards = array();
					foreach ($resSubjectRewards as $key => $value) {
						$dbRewards[] = $value["id"];
					};

					foreach ($data->rewards as $key => $reward) {
						$sql = new Sql($this->adapter);
						$select = $sql->select();
						$where = array(
							'subjectRewardPercentId'	=>	$subjectRewardPercentId,
							'reward_currency'			=>	$reward->currency
						);
						$select->from(array('s'	=>	'subject_rewards'));
						$select->where($where);
						$selectString = $sql->getSqlStringForSqlObject($select);
						$resSubjectRewards = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$resSubjectRewards = $resSubjectRewards->toArray();
						if (count($resSubjectRewards) == 0) {
							$subjectRewards = new TableGateway('subject_rewards', $adapter, null,new HydratingResultSet());
							$insert = array(
								'subjectRewardPercentId' =>	$subjectRewardPercentId,
								'reward_amount'		=>	$reward->amount,
								'reward_currency'	=>	$reward->currency,
								'created'			=>	date('Y-m-d H:i:s'),
								'modified'			=>	date('Y-m-d H:i:s')
							);
							$subjectRewards->insert($insert);
							$subjectRewardId = $subjectRewards->getLastInsertValue();

							$dbRewardkey = array_search($subjectRewardId, $dbRewards);
							unset($dbRewards[$dbRewardkey]);
						} else {
							$resSubjectRewards = $resSubjectRewards[0];
							$subjectRewardId = $resSubjectRewards["id"];

							$where = array('id' => $subjectRewardId);
							$updateValues = array(
								'reward_amount'		=>	$reward->amount,
								'reward_currency'		=>	$reward->currency
							);
							//var_dump($updateValues);die();
							$sql = new Sql($this->adapter);
							$update = $sql->update();
							$update->table('subject_rewards');
							$update->set($updateValues);
							$update->where($where);
							$statement = $sql->prepareStatementForSqlObject($update);
							$result = $statement->execute();

							$dbRewardkey = array_search($subjectRewardId, $dbRewards);
							unset($dbRewards[$dbRewardkey]);
						}
					}

					if (count($dbRewards)>0) {
						$dbRewards = array_values($dbRewards);
						//$dbRewards = implode(",", $dbRewards);
						foreach ($dbRewards as $key => $dbReward) {
							$sql = new Sql($this->adapter);
							$delete = $sql->delete();
							$delete->from('subject_rewards');
							//$where = ;
							$delete->where(array('id'=>$dbReward));
							$statement = $sql->prepareStatementForSqlObject($delete);
							//var_dump($statement);
							$statement->execute();
						}
						//var_dump($dbRewards);
					}
				}
				$result->status = 1;
				$result->message = "Reward set successfully";
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getStudentPerformance($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('s'	=>	'subjects'));
				$select->where(array('id'	=>	$data->subjectId));
				$select->columns(array('id', 'grading_notes'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$subjects = $subjects->toArray();
				if(count($subjects) >= 0) {
					$result->notes = $subjects[0]['grading_notes'];
				}
				$subjectScores = array();
				$i = 0;
				$studentId = $data->userId;
				if ( isset($data->studentId) && !empty($data->studentId) ) {
					$studentId = $data->studentId;
				}
				// subject content
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('e'	=>	'content'));
				$select->join(array('c'=>'chapters'), 'c.id = e.chapterId', array('chapterName'=>'name'), 'left');
				$select->where(array('c.subjectId'	=>	$data->subjectId, 'c.deleted'=>0));
				$select->columns(array('id', 'title', 'weight_grading'));
				$select->order('c.weight ASC');
				$select->order('c.id ASC');
				$selectString = $sql->getSqlStringForSqlObject($select);
				$contents = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$contents = $contents->toArray();
				if(count($contents) >= 0) {
					require_once 'DynamoDbOps.php';
					$ddo = new DynamoDbOps();
					$content_watched=$ddo->getStudentsProgress($data);
					$content_watched=$content_watched->items;
					//print_r($content_watched);
					foreach ($contents as $key => $content) {
						$contents[$key]['performance'] = '0%';
						$itemKey = $this->searchForContentId($content['id'], $content_watched);
						if ($itemKey) {
							$percentage = round($content_watched[$itemKey]['percentRead'],2);
							$contents[$key]['performance'] = $percentage.'%';
							/*$dataR = new stdClass();
							$dataR->contentId = $content['id'];
							$dataR->contentType = 'content';
							$resSubjectRewards = $this->getSubjectRewardsbyContent($dataR);
							$rewards = array();
							if ($resSubjectRewards->status == 1) {
								$rewards = $resSubjectRewards->subjectRewards;
								$percent_block_prev = 0;
								foreach ($rewards as $key1 => $reward) {
									if(isset($reward['percent_block']) && !empty($reward['percent_block'])) {
										foreach ($reward['rewards'] as $key2 => $rewardItem) {
											$contents[$key]['rewards_max'][$key2] = $rewardItem['reward_amount'].' '.$rewardItem['reward_currency'];
										}
										if($percentage<=$reward['percent_block'] && $percentage>$percent_block_prev) {
											foreach ($contents[$key]['rewards_max'] as $key2 => $rewardItem) {
												$contents[$key]['rewards'][$key2] = (int)$contents[$key]['rewards_max'][$key2];
											}
										}
										$percent_block_prev = $reward['percent_block'];
									}
								}
							}*/
							$dataR = new stdClass();
							$dataR->contentId = $content['id'];
							$dataR->contentType = 'content';
							$dataR->studentId = $studentId;
							$resSubjectRewards = $this->getStudentSubjectRewardsbyContent($dataR);
							$rewards = array();
							if ($resSubjectRewards->status == 1) {
								$rewards = $resSubjectRewards->subjectRewards;
							}
							$contents[$key]['rewards'] = $rewards;
						}
						if ($content['weight_grading']>0) {
							$subjectScores[$i]['weight'] = $content['weight_grading']/100;
							$subjectScores[$i]['percent'] = 0;
							if ($itemKey) {
								$subjectScores[$i]['percent'] = ((float)$contents[$key]['performance'])/100;
							}
							$i++;
						}
					}
					$result->contents = $contents;
				}

				// objective exams				
				$query="SELECT e.id, e.name, e.type, e.startDate, e.endDate, e.attempts, e.grade_type, e.avg_attempts, e.weight, c.name AS chapterName, COUNT(ea.id) AS user_attempts
						FROM `exams` AS e
						LEFT JOIN `chapters` AS c on c.id = e.chapterId
						LEFT JOIN `exam_attempts` AS ea ON ea.examId=e.id AND ea.studentId={$studentId}
						WHERE e.subjectId={$data->subjectId} AND e.delete=0
						GROUP BY e.id
						ORDER BY c.weight ASC, c.id ASC";
				$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				if(count($exams) >= 0) {
					foreach ($exams as $key => $exam) {
						$exams[$key]['performance'] = 'NA';
						if ($exam['user_attempts']>0) {
							if ($exam['grade_type'] == 'percentage') {
								$query="SELECT ea.score AS score, SUM(es.totalMarks) AS totalMarks
										FROM `exam_attempts` AS ea
										INNER JOIN `exam_sections` AS es ON es.examId=ea.examId
										WHERE ea.examId={$exam['id']} AND ea.studentId={$studentId}
										GROUP BY ea.studentId
										ORDER BY ea.id DESC
										LIMIT {$exam['avg_attempts']}";
								$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$attempts = $attempts->toArray();
								$totalScore = $totalMarks = 0;
								if (count($attempts)) {
									foreach ($attempts as $key1 => $attempt) {
										$totalScore+= $attempt['score'];
										$totalMarks+= $attempt['totalMarks'];
									}
									$percentage = round(($totalScore/$totalMarks)*100,2);
									$exams[$key]['performance'] = $percentage.'%';
									/*$resSubjectRewards = $this->getSubjectRewardsbyContent($dataR);
									$rewards = array();
									if ($resSubjectRewards->status == 1) {
										$rewards = $resSubjectRewards->subjectRewards;
										$percent_block_prev = 0;
										foreach ($rewards as $key1 => $reward) {
											if(isset($reward['percent_block']) && !empty($reward['percent_block'])) {
												foreach ($reward['rewards'] as $key2 => $rewardItem) {
													$exams[$key]['rewards_max'][$key2] = $rewardItem['reward_amount'].' '.$rewardItem['reward_currency'];
												}
												if($percentage<=$reward['percent_block'] && $percentage>$percent_block_prev) {
													foreach ($exams[$key]['rewards_max'] as $key2 => $rewardItem) {
														$exams[$key]['rewards'][$key2] = (int)$exams[$key]['rewards_max'][$key2];
													}
												}
												$percent_block_prev = $reward['percent_block'];
											}
										}
									}*/
								}
							} else {
								$query="SELECT AVG(ea.score) AS userScore, es.totalMarks
										FROM `exam_attempts` AS ea
										INNER JOIN `exam_sections` AS es ON es.examId=ea.examId
										WHERE ea.examId={$exam['id']} AND ea.studentId={$studentId}
										ORDER BY ea.id DESC
										LIMIT {$exam['avg_attempts']}";
								$userscores = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$userscores = $userscores->toArray();
								$userScore  = $this->round2($userscores[0]['userScore']);
								$totalMarks  = $this->round2($userscores[0]['totalMarks']);
								$query="SELECT ea.studentId
										FROM `exam_attempts` AS ea
										WHERE ea.examId={$exam['id']}
										GROUP BY ea.studentId
										ORDER BY ea.score DESC";
								$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$attempts = $attempts->toArray();
								$students = array();
								if (count($attempts)) {
									foreach ($attempts as $key1 => $attempt) {
										$students[$key1] = $attempt['studentId'];
									}
								}
								$totalStudents = count($students);
								$scoreStudents = 0;
								if (count($students)) {
									foreach ($students as $key1 => $student) {
										$query="SELECT AVG(ea.score) AS avgScore
												FROM `exam_attempts` AS ea
												WHERE ea.examId={$exam['id']} AND ea.studentId={$student}
												ORDER BY ea.id DESC
												LIMIT {$exam['avg_attempts']}";
										$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
										$attempts = $attempts->toArray();
										if (count($attempts)) {
											$avgScore = 0;
											$avgScore = $this->round2($attempts[0]['avgScore']);
											if ($avgScore<=$userScore) {
												$scoreStudents++;
											}
										}
									}
								}
								$exams[$key]['performance'] = $this->round2(($scoreStudents/$totalStudents)*100).'<sup>th</sup> Percentile';
							}
							$dataR = new stdClass();
							$dataR->contentId = $exam['id'];
							$dataR->contentType = 'exam';
							$dataR->studentId = $data->userId;
							$resSubjectRewards = $this->getStudentSubjectRewardsbyContent($dataR);
							$rewards = array();
							if ($resSubjectRewards->status == 1) {
								$rewards = $resSubjectRewards->subjectRewards;
							}
							$exams[$key]['rewards'] = $rewards;
						}
						if ($exam['weight']>0) {
							$subjectScores[$i]['weight'] = $exam['weight']/100;
							$subjectScores[$i]['percent'] = ((float)$exams[$key]['performance'])/100;
							$i++;
						}
					}
					$result->exams = $exams;
				}
				// subjective exams
				$query="SELECT e.id, e.name, e.type, e.startDate, e.endDate, e.attempts, e.grade_type, e.avg_attempts, e.weight, c.name AS chapterName, COUNT(ea.id) AS user_attempts
						FROM `exam_subjective` AS e
						LEFT JOIN `chapters` AS c on c.id = e.chapterId
						LEFT JOIN `subjective_exam_attempts` AS ea ON ea.examId=e.id AND ea.studentId={$studentId}
						WHERE e.subjectId={$data->subjectId} AND e.Active=1
						GROUP BY e.id
						ORDER BY c.weight ASC, c.id ASC";
				$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				if(count($exams) >= 0) {
					foreach ($exams as $key => $exam) {
						$exams[$key]['performance'] = 'NA';
						if ($exam['user_attempts']>0) {
							if ($exam['grade_type'] == 'percentage') {
								$query="SELECT ea.id, ea.score
										FROM `subjective_exam_attempts` AS ea
										WHERE ea.examId={$exam['id']} AND ea.studentId={$studentId}
										ORDER BY ea.id DESC
										LIMIT {$exam['avg_attempts']}";
								$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$attempts = $attempts->toArray();
								$totalScore = $totalMarks = 0;
								$scores = array();
								foreach ($attempts as $key1 => $attempt) {
									$totalScore+= $attempt['score'];
									$totalMarks+= $this->getMaxMarksSubjectiveExam($exam['id']);
									$scores[] = array($attempt['score'],$this->getMaxMarksSubjectiveExam($exam['id']));
								}
								$percentage = 0;
								if ($totalMarks>0) {
									$percentage = round(($totalScore/$totalMarks)*100,2);
								}
								$exams[$key]['performance'] = $percentage.'%';
								/*$resSubjectRewards = $this->getSubjectRewardsbyContent($dataR);
								$rewards = array();
								if ($resSubjectRewards->status == 1) {
									$rewards = $resSubjectRewards->subjectRewards;
									$percent_block_prev = 0;
									foreach ($rewards as $key1 => $reward) {
										if(isset($reward['percent_block']) && !empty($reward['percent_block'])) {
											foreach ($reward['rewards'] as $key2 => $rewardItem) {
												$exams[$key]['rewards_max'][$key2] = $rewardItem['reward_amount'].' '.$rewardItem['reward_currency'];
											}
											if($percentage<=$reward['percent_block'] && $percentage>$percent_block_prev) {
												foreach ($exams[$key]['rewards_max'] as $key2 => $rewardItem) {
													$exams[$key]['rewards'][$key2] = (int)$exams[$key]['rewards_max'][$key2];
												}
											}
											$percent_block_prev = $reward['percent_block'];
										}
									}
								}*/
							} else {
								$query="SELECT AVG(ea.score) AS userScore
										FROM `subjective_exam_attempts` AS ea
										WHERE ea.examId={$exam['id']} AND ea.studentId={$studentId}
										ORDER BY ea.id DESC
										LIMIT {$exam['avg_attempts']}";
								$userscores = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$userscores = $userscores->toArray();
								$userScore  = $this->round2($userscores[0]['userScore']);
								$totalMarks  = $this->round2($this->getMaxMarksSubjectiveExam($exam['id']));
								$query="SELECT ea.studentId
										FROM `subjective_exam_attempts` AS ea
										WHERE ea.examId={$exam['id']}
										GROUP BY ea.studentId
										ORDER BY ea.score DESC";
								$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$attempts = $attempts->toArray();
								$students = array();
								if (count($attempts)) {
									foreach ($attempts as $key1 => $attempt) {
										$students[$key1] = $attempt['studentId'];
									}
								}
								$totalStudents = count($students);
								$scoreStudents = 0;
								if (count($students)) {
									foreach ($students as $key1 => $student) {
										$query="SELECT AVG(ea.score) AS avgScore
												FROM `subjective_exam_attempts` AS ea
												WHERE ea.examId={$exam['id']} AND ea.studentId={$student}
												ORDER BY ea.id DESC
												LIMIT {$exam['avg_attempts']}";
										$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
										$attempts = $attempts->toArray();
										if (count($attempts)) {
											$avgScore = 0;
											$avgScore = $this->round2($attempts[0]['avgScore']);
											if ($avgScore<=$userScore) {
												$scoreStudents++;
											}
										}
									}
								}
								$exams[$key]['performance'] = $this->round2(($scoreStudents/$totalStudents)*100).'<sup>th</sup> Percentile';
							}
							$dataR = new stdClass();
							$dataR->contentId = $exam['id'];
							$dataR->contentType = 'subjective-exam';
							$dataR->studentId = $studentId;
							$resSubjectRewards = $this->getStudentSubjectRewardsbyContent($dataR);
							$rewards = array();
							if ($resSubjectRewards->status == 1) {
								$rewards = $resSubjectRewards->subjectRewards;
							}
							$exams[$key]['rewards'] = $rewards;
						}
						if ($exam['weight']>0) {
							$subjectScores[$i]['weight'] = $exam['weight']/100;
							$subjectScores[$i]['percent'] = ((float)$exams[$key]['performance'])/100;
							$i++;
						}
					}
					$result->subjective_exams = $exams;
				}
				// manual exams
				$query="SELECT e.id, e.name, e.weight, c.name AS chapterName, COUNT(emm.id) AS user_attempts, e.grade_type, e.avg_attempts, e.weight
						FROM `exam_manual` AS e
						LEFT JOIN `chapters` AS c on c.id = e.chapterId
						LEFT JOIN `exam_manual_questions` AS emq ON emq.exam_id=e.id
						LEFT JOIN `exam_manual_marks` AS emm ON emq.id=emm.question_id AND emm.student_id={$studentId}
						WHERE e.subjectId={$data->subjectId} AND e.status=1
						GROUP BY e.id
						ORDER BY c.weight ASC, c.id ASC";
				$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				if(count($exams) >= 0) {
					//var_dump($exams);
					foreach ($exams as $key => $exam) {
						$exams[$key]['performance'] = 'NA';
						if ($exam['user_attempts']>0) {
							if ($exam['grade_type'] == 'percentage') {
								$query="SELECT SUM(emm.marks) AS score, SUM(emq.marks) AS totalMarks
										FROM `exam_manual_marks` AS emm
										INNER JOIN `exam_manual_questions` AS emq ON emq.id=emm.question_id
										WHERE emq.exam_id={$exam['id']} AND emm.student_id={$studentId}
										GROUP BY emq.exam_id";
								$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$attempts = $attempts->toArray();
								//var_dump($query);
								$totalScore = $totalMarks = 0;
								if (count($attempts)) {
									foreach ($attempts as $key1 => $attempt) {
										$totalScore+= $attempt['score'];
										$totalMarks+= $attempt['totalMarks'];
									}
									$percentage = round(($totalScore/$totalMarks)*100,2);
									$exams[$key]['performance'] = $percentage.'%';
									/*$resSubjectRewards = $this->getSubjectRewardsbyContent($dataR);
									$rewards = array();
									if ($resSubjectRewards->status == 1) {
										$rewards = $resSubjectRewards->subjectRewards;
										$percent_block_prev = 0;
										foreach ($rewards as $key1 => $reward) {
											if(isset($reward['percent_block']) && !empty($reward['percent_block'])) {
												foreach ($reward['rewards'] as $key2 => $rewardItem) {
													$exams[$key]['rewards_max'][$key2] = $rewardItem['reward_amount'].' '.$rewardItem['reward_currency'];
												}
												if($percentage<=$reward['percent_block'] && $percentage>$percent_block_prev) {
													foreach ($exams[$key]['rewards_max'] as $key2 => $rewardItem) {
														$exams[$key]['rewards'][$key2] = (int)$exams[$key]['rewards_max'][$key2];
													}
												}
												$percent_block_prev = $reward['percent_block'];
											}
										}
									}*/
								}
							} else {
								$query="SELECT SUM(emm.marks) AS userScore, SUM(emq.marks) AS totalMarks
										FROM `exam_manual_marks` AS emm
										INNER JOIN `exam_manual_questions` AS emq ON emq.id=emm.question_id
										WHERE emq.exam_id={$exam['id']} AND emm.student_id={$studentId}
										GROUP BY emq.exam_id";
								$userscores = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$userscores = $userscores->toArray();
								$userScore  = $this->round2($userscores[0]['userScore']);
								$totalMarks  = $this->round2($userscores[0]['totalMarks']);
								$query="SELECT emm.student_id
										FROM `exam_manual_marks` AS emm
										INNER JOIN `exam_manual_questions` AS emq ON emq.id=emm.question_id
										WHERE emq.exam_id={$exam['id']}
										GROUP BY emm.student_id";
								$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$attempts = $attempts->toArray();
								$students = array();
								if (count($attempts)) {
									foreach ($attempts as $key1 => $attempt) {
										$students[$key1] = $attempt['student_id'];
									}
								}
								$totalStudents = count($students);
								$scoreStudents = 0;
								if (count($students)) {
									foreach ($students as $key1 => $student) {
										$query="SELECT SUM(emm.marks) AS avgScore
												FROM `exam_manual_marks` AS emm
												INNER JOIN `exam_manual_questions` AS emq ON emq.id=emm.question_id
												WHERE emq.exam_id={$exam['id']} AND emm.student_id={$student}
												GROUP BY emq.exam_id";
										$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
										$attempts = $attempts->toArray();
										if (count($attempts)) {
											$avgScore = 0;
											$avgScore = $this->round2($attempts[0]['avgScore']);
											if ($avgScore<=$userScore) {
												$scoreStudents++;
											}
										}
									}
								}
								$exams[$key]['performance'] = $this->round2(($scoreStudents/$totalStudents)*100).'<sup>th</sup> Percentile';
							}
							$dataR = new stdClass();
							$dataR->contentId = $exam['id'];
							$dataR->contentType = 'manual-exam';
							$dataR->studentId = $studentId;
							$resSubjectRewards = $this->getStudentSubjectRewardsbyContent($dataR);
							$rewards = array();
							if ($resSubjectRewards->status == 1) {
								$rewards = $resSubjectRewards->subjectRewards;
							}
							$exams[$key]['rewards'] = $rewards;
						}
						if ($exam['weight']>0) {
							$subjectScores[$i]['weight'] = $exam['weight']/100;
							$subjectScores[$i]['percent'] = ((float)$exams[$key]['performance'])/100;
							$i++;
						}
					}
					$result->manual_exams = $exams;
				}
				// submissions
				$query="SELECT e.id, e.name, e.startDate, e.endDate, e.attempts, e.grade_type, e.avg_attempts, e.weight, c.name AS chapterName, COUNT(ea.id) AS user_attempts
						FROM `submissions` AS e
						LEFT JOIN `chapters` AS c on c.id = e.chapterId
						LEFT JOIN `submission_attempts` AS ea ON ea.examId=e.id AND ea.studentId={$studentId}
						WHERE e.subjectId={$data->subjectId} AND e.deleted=0
						GROUP BY e.id
						ORDER BY c.weight ASC, c.id ASC";
				$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$exams = $exams->toArray();
				if(count($exams) >= 0) {
					foreach ($exams as $key => $exam) {
						$exams[$key]['performance'] = 'NA';
						if ($exam['user_attempts']>0) {
							if ($exam['grade_type'] == 'percentage') {
								$query="SELECT ea.id, ea.score
										FROM `submission_attempts` AS ea
										WHERE ea.examId={$exam['id']} AND ea.studentId={$studentId}
										ORDER BY ea.id DESC
										LIMIT {$exam['avg_attempts']}";
								$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$attempts = $attempts->toArray();
								$totalScore = $totalMarks = 0;
								$scores = array();
								foreach ($attempts as $key1 => $attempt) {
									$totalScore+= $attempt['score'];
									$totalMarks+= $this->getMaxMarksSubjectiveExam($exam['id']);
									$scores[] = array($attempt['score'],$this->getMaxMarksSubjectiveExam($exam['id']));
								}
								$percentage = 0;
								if ($totalMarks>0) {
									$percentage = round(($totalScore/$totalMarks)*100,2);
								}
								$exams[$key]['performance'] = $percentage.'%';
								/*$resSubjectRewards = $this->getSubjectRewardsbyContent($dataR);
								$rewards = array();
								if ($resSubjectRewards->status == 1) {
									$rewards = $resSubjectRewards->subjectRewards;
									$percent_block_prev = 0;
									foreach ($rewards as $key1 => $reward) {
										if(isset($reward['percent_block']) && !empty($reward['percent_block'])) {
											foreach ($reward['rewards'] as $key2 => $rewardItem) {
												$exams[$key]['rewards_max'][$key2] = $rewardItem['reward_amount'].' '.$rewardItem['reward_currency'];
											}
											if($percentage<=$reward['percent_block'] && $percentage>$percent_block_prev) {
												foreach ($exams[$key]['rewards_max'] as $key2 => $rewardItem) {
													$exams[$key]['rewards'][$key2] = (int)$exams[$key]['rewards_max'][$key2];
												}
											}
											$percent_block_prev = $reward['percent_block'];
										}
									}
								}*/
							} else {
								$query="SELECT AVG(ea.score) AS userScore
										FROM `submission_attempts` AS ea
										WHERE ea.examId={$exam['id']} AND ea.studentId={$studentId}
										ORDER BY ea.id DESC
										LIMIT {$exam['avg_attempts']}";
								$userscores = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
								$userscores = $userscores->toArray();
								if (count($userscores)>0) {
									$userScore  = $this->round2($userscores[0]['userScore']);
									$totalMarks  = $this->round2($this->getMaxMarksSubjectiveExam($exam['id']));
									$query="SELECT ea.studentId
											FROM `submission_attempts` AS ea
											WHERE ea.examId={$exam['id']}
											GROUP BY ea.studentId
											ORDER BY ea.score DESC";
									$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
									$attempts = $attempts->toArray();
									$students = array();
									if (count($attempts)) {
										foreach ($attempts as $key1 => $attempt) {
											$students[$key1] = $attempt['studentId'];
										}
									}
									$totalStudents = count($students);
									$scoreStudents = 0;
									if (count($students)) {
										foreach ($students as $key1 => $student) {
											$query="SELECT AVG(ea.score) AS avgScore
													FROM `submission_attempts` AS ea
													WHERE ea.examId={$exam['id']} AND ea.studentId={$student}
													ORDER BY ea.id DESC
													LIMIT {$exam['avg_attempts']}";
											$attempts = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
											$attempts = $attempts->toArray();
											if (count($attempts)) {
												$avgScore = 0;
												$avgScore = $this->round2($attempts[0]['avgScore']);
												if ($avgScore<=$userScore) {
													$scoreStudents++;
												}
											}
										}
									}
									$exams[$key]['performance'] = $this->round2(($scoreStudents/$totalStudents)*100).'<sup>th</sup> Percentile';
								}
							}
							$dataR = new stdClass();
							$dataR->contentId = $exam['id'];
							$dataR->contentType = 'submission';
							$dataR->studentId = $studentId;
							$resSubjectRewards = $this->getStudentSubjectRewardsbyContent($dataR);
							$rewards = array();
							if ($resSubjectRewards->status == 1) {
								$rewards = $resSubjectRewards->subjectRewards;
							}
							$exams[$key]['rewards'] = $rewards;
						}
						if ($exam['weight']>0) {
							$subjectScores[$i]['weight'] = $exam['weight']/100;
							$subjectScores[$i]['percent'] = ((float)$exams[$key]['performance'])/100;
							$i++;
						}
					}
					$result->submissions = $exams;
				}
				$subjectScore = 0;
				if (count($subjectScores)) {
					foreach ($subjectScores as $key => $score) {
						$subjectScore+= $score['percent']*$score['weight'];
					}
					$subjectScore = $this->round2($subjectScore*100);
				}
				$dataStu = new stdClass();
				$dataStu->userId = $studentId;
				require_once("Student.php");
				$st = new Student();
				$result->student	= $st->getBasicStudentDetails($dataStu);
				$result->studentId	= $studentId;
				$result->status		= 1;
				$result->subjectScore = $subjectScore;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}

		function getMaxMarksSubjectiveExam($examId)
		{
			$adapter = $this->adapter;
			$query="SELECT qs.id,qsel.marks,qs.questionsonPage,qs.parentId
					FROM `question_subjective_exam_links` AS qsel
					LEFT JOIN `questions_subjective` AS qs ON qs.id=qsel.questionId
					WHERE qsel.examId={$examId} AND qs.`deleted`=0 AND qs.`status`=1
					ORDER BY qsel.id";
			$questions = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$questions = $questions->toArray();
			$maxMarks = 0;
			$subjectiveQuestions=array();
			$parentIdTemp= array();
			$i			 = 0;
			foreach ($questions as $key => $question) {
				$maxMarks+=$question["marks"];
				if (empty($question['parentId'])) {
					if ($question['questionsonPage'] > 0) {
						$maxMarks = $maxMarks - $question["marks"];
					}
				}
			}
			return $maxMarks;
		}
		function searchForContentId($id, $array) {
			foreach ($array as $key => $val) {
				if ($val['contentId'] == $id) {
					return $key;
				}
			}
			return null;
		}
		public function getSubjectStudentGroups($data)
		{
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('ssg' => 'student_subject_groups'));
				$select->where( array( 'ssg.subjectId' => $data->subjectId,'ssg.active' => 1 ));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$studentGroups = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$result->studentGroups = $studentGroups->toArray();

				foreach ($result->studentGroups as $key => $studentGroup) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from(array('ssgs' => 'student_subject_group_students'))
						->join(array('ssg' => 'student_subject_groups'),'ssg.id = ssgs.groupId',array())
						->join(array('s' => 'subjects'),'s.id = ssg.subjectId',array())
						->join(array('sd' => 'student_details'),'sd.userId = ssgs.studentId',array('firstName','lastName'));
					$select->where( array( 'ssg.id' => $studentGroup['id'] ));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$prof = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$result->studentGroups[$key]['students'] = $prof->toArray();
				}

				if (isset($data->examId) && !empty(isset($data->examId))) {
					if ($data->examType == 'submission') {
						$result->selectedStudentGroups = array();

						require_once("Submission.php");
						$sub = new Submission();
						$studentGroups = $sub->getSubmissionGroups($data);
						
						if ($studentGroups->status == 1) {
							$result->selectedStudentGroups = $studentGroups->selectedStudentGroups;
						}
					}
				}
				
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function generateSubjectStudentGroups($data)
		{

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$data->type = "registered";
				$subjectStudents = $this->getSubjectStudents($data);
				if ($subjectStudents->status == 0) {
					$result->status = 0;
					$result->message = "There was some error.";
				}
				$subjectStudents = $subjectStudents->students;
				$countValid = (int)(count($subjectStudents)/2);
				if(($data->count > $countValid) || ($data->count < 2)) {
					$result->status = 0;
					$result->message = "There are ".count($subjectStudents)." students in this subject, so max {$countValid} students can be there in 1 group";
					return $result;
				}
				$limit = count($subjectStudents)/$data->count;
				$subjectStudents = array_chunk($subjectStudents, $data->count);
				foreach ($subjectStudents as $key => $studentGroup) {
					$studentGroups = new TableGateway('student_subject_groups', $adapter, null,new HydratingResultSet());
					$insert = array(
						'courseId'			=>	$data->courseId,
						'subjectId'			=>	$data->subjectId,
						'ownerId'			=>	$data->userId,
						'name'				=>	'Student Group '.($key+1)
					);
					$studentGroups->insert($insert);
					$studentGroupId = $studentGroups->getLastInsertValue();
					foreach ($studentGroup as $key1 => $student) {
						$studentGroupStudents = new TableGateway('student_subject_group_students', $adapter, null,new HydratingResultSet());
						$insertStudent = array(
							'groupId'			=>	$studentGroupId,
							'studentId'			=>	$student['id']
						);
						$studentGroupStudents->insert($insertStudent);
						//$data->subjectId = $studentGroupStudents->getLastInsertValue();
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function addStudentSubjectStudentGroup($data)
		{

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$studentGroupId = $data->studentGroupId;
				$studentId = $data->studentId;
				
				$query="SELECT *
						FROM `student_subject_group_students` AS ssgs
						INNER JOIN `student_subject_groups` AS ssg ON ssg.id=ssgs.groupId
						WHERE ssg.subjectId={$data->subjectId} AND ssgs.studentId={$studentId}";
				$subjectStudents = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$subjectStudents = $subjectStudents->toArray();
				if (count($subjectStudents)) {
					$result->status = 0;
					$result->message = "Student is already added in a student group";
					return $result;
				}

				$studentGroupStudents = new TableGateway('student_subject_group_students', $adapter, null,new HydratingResultSet());
				$insertStudent = array(
					'groupId'			=>	$studentGroupId,
					'studentId'			=>	$studentId
				);
				$studentGroupStudents->insert($insertStudent);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = "Student added successfully.";
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function removeStudentSubjectStudentGroup($data)
		{

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$studentGroupId = $data->studentGroupId;
				$studentId = $data->studentId;

				$sql = new Sql($this->adapter);
				$delete = $sql->delete();
				$delete->from('student_subject_group_students');
				$where = array(
					'groupId'			=>	$studentGroupId,
					'studentId'			=>	$studentId
				);
				$delete->where($where);
				$statement = $sql->prepareStatementForSqlObject($delete);
				$statement->execute();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = "Student removed successfully.";
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function addSubjectStudentGroup($data)
		{

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$studentGroupName = $data->studentGroupName;
				$studentGroups = new TableGateway('student_subject_groups', $adapter, null,new HydratingResultSet());
				$insert = array(
					'courseId'			=>	$data->courseId,
					'subjectId'			=>	$data->subjectId,
					'ownerId'			=>	$data->userId,
					'name'				=>	$studentGroupName
				);
				$studentGroups->insert($insert);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$studentGroupId = $studentGroups->getLastInsertValue();
				$result->status = 1;
				$result->message = "Student group added successfully.";
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function removeSubjectStudentGroups($data)
		{

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$studentGroupIds = $data->studentGroups;

				foreach ($studentGroupIds as $key => $studentGroupId) {

					$sql = new Sql($this->adapter);
					$delete = $sql->delete();
					$delete->from('student_subject_group_students');
					$where = array(
						'groupId'			=>	$studentGroupId
					);
					$delete->where($where);
					$statement = $sql->prepareStatementForSqlObject($delete);
					$statement->execute();

					$sql = new Sql($this->adapter);
					$delete = $sql->delete();
					$delete->from('student_subject_groups');
					$where = array(
						'id'			=>	$studentGroupId
					);
					$delete->where($where);
					$statement = $sql->prepareStatementForSqlObject($delete);
					$statement->execute();

				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();

				$result->status = 1;
				$result->message = "Student group removed successfully.";
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getStudentGroup($data)
		{
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('ssgs' => 'student_subject_group_students'))
					->join(array('ssg' => 'student_subject_groups'),'ssg.id = ssgs.groupId',array());
				$select->where( array( 'ssg.subjectId' => $data->subjectId,'ssg.active' => 1, 'ssgs.studentId' => $data->studentId ));
				$select->columns( array('studentGroupId' => 'groupId') );
				$selectString = $sql->getSqlStringForSqlObject($select);
				$prof = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$studentGroupIds = $prof->toArray();
				if (count($studentGroupIds) == 0) {
					$result->status = 0;
					$result->message = "No student group found";
					return $result;
				}
				$result->studentGroupId = $studentGroupIds[0]['studentGroupId'];
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getStudentsByStudentGroup($data)
		{
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				/*$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from(array('ssgs' => 'student_subject_group_students'));
				$select->where( array( 'ssgs.groupId' => $data->studentGroupId ));
				$select->columns( array('studentId' => 'studentId') );
				$selectString = $sql->getSqlStringForSqlObject($select);
				$prof = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$students = $prof->toArray();*/

				$query = "SELECT ssgs.studentId, sd.userId, concat(sd.firstName,' ',sd.lastName) AS name, ld.email
							FROM `student_subject_group_students` AS ssgs
							INNER JOIN `login_details` AS ld ON ld.id=ssgs.studentId
							INNER JOIN `student_details` AS sd ON sd.userId=ld.id
							WHERE ssgs.groupId={$data->studentGroupId}";
				$students = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$students = $students->toArray();
				if (count($students) == 0) {
					$result->status = 0;
					$result->message = 'No students in the student group.';
					return $result;
				}
				/*$arrStudents = array();
				foreach ($students as $key => $student) {
					$arrStudents[] = $student['studentId'];
				}*/
				$result->students = $students;
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
	}
	
?>