<?php
//getQuestionsForSubjectiveExam : functions return the questions json format 
public function getQuestionsForSubjectiveExam($data) {
	//$data->examId=14;
	$answeredQuestionsArray=array();
	$result = new stdClass();
	try {
		
		$adapter = $this->adapter;
		//$sql = new Sql($adapter);
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('exam_subjective');
		$select->where(array(
					'id' => $data->examId,
					'Active' =>	1
		));
		$statement = $sql->getSqlStringForSqlObject($select);
		$exam_subjective = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
		$exam_subjective = $exam_subjective->toArray();				
		
		$shuffle=$exam_subjective[0]['shuffle'];
		$requiredQuestion=$exam_subjective[0]['totalRequiredQuestions'];
		$submission=$exam_subjective[0]['submissiontype'];
		
						
		$adapter = $this->adapter;
		//$sql = new Sql($adapter);
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('questions_subjective');
		$select->where(array(
					'exam_subjectiveId' => $data->examId,
					'deleted'	=>	0,
					'status' =>	1
		));
		$statement = $sql->getSqlStringForSqlObject($select);
		$questions = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
		$questions = $questions->toArray();
		//print_r($questions);
		$subjectiveQuestions=array();
		$parentIdTemp= array();
		$i			 = 0;
		//$totalMarks = 0;
		$qtimer = array();
		foreach($questions as $key=>$subQues)
		{
			$question=array();
			$questionId = $subQues['id'];
			$qId=$subQues['id'];
			$questionString='';
			$answerString='';
			if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1'  && $_SERVER['REMOTE_ADDR']<>'::1' && $subQues['questionfileUrl']!='' && $subQues['answerFileUrl'] !='') {
				require_once 'amazonRead.php';
				$questionString = getSignedURL($subQues['questionfileUrl']);
				$answerString=getSignedURL($subQues['answerFileUrl']);
			}
			$questionString = file_get_contents($questionString);
			$answerString = file_get_contents($answerString);
			if($subQues['parentId']==0)
			{	
				$parentIdTemp[$i]=$subQues['questionId'];
				$i++;
				if($subQues['noOfSubquestionRequired']>0)
				{
					$questionId					=$questionId;
					$question['id']				=$qId;
					$question['questionType']	='questionGroup';
					$question['question']		=$questionString;
					$question['timer']			=0;
					$question['upload']			=array();
					$question['answer']			="";
					$question['marks']			=(int)$subQues['marks'];
					if ($subQues['questionsonPage'] == 1) {
						$question['listType']			='one';
					}
					else {
						$question['listType']			='multi';
					}
					$question['subQuestions']			=array();
				}else{							
					$question['id']				=$qId;
					$question['questionType']	='question';
					$question['question']		=$questionString;
					$question['timer']			=0;
					$question['upload']			=array();
					$question['answer']			='';
					$question['marks']			=(int)$subQues['marks'];
					//$totalMarks+=$subQues['marks'];
					$question['subQuestions']	=array();
				}
				array_push($subjectiveQuestions,$question);
			}else{
				$parentId=$subQues['parentId'];
				$subquestionsArray=array();
				$subQuestionId=$qId;
				if(in_array($parentId,$parentIdTemp))
				{
					$index=array_search($subQues['parentId'],$parentIdTemp);
					$checkMulti=$subjectiveQuestions[$index]['listType'];
					$subquestionsArray['id']=$qId;
					if($checkMulti == 'multi')
					{
						//print_r('inside multi');
					} else {
						//$subjectiveQuestions[$index]['marks'] = $subjectiveQuestions[$index]['marks'] + $subQues['marks'];
					}
					$subquestionsArray['questionType']	='question';
					$subquestionsArray['question']		=$questionString;
					$subquestionsArray['timer']			=0;
					$subquestionsArray['upload']		=array();
					$subquestionsArray['answer']		='';
					$subquestionsArray['marks']			=(int)$subQues['marks'];
					//$totalMarks+=$subQues['marks'];
					array_push($subjectiveQuestions[$index]['subQuestions'],$subquestionsArray);
				}
			}
			$qtimer[$qId] = 0;
		}
		
		//print_r(json_encode($subjectiveQuestions));
		//$jsonQUestions=json_encode($subjectiveQuestions);
		if($shuffle  =='yes')
		{
			shuffle($subjectiveQuestions);
		}
		
		if(count($subjectiveQuestions)> $requiredQuestion && $requiredQuestion > 0){
			$subjectiveQuestions = array_slice($subjectiveQuestions, 0, $requiredQuestion);
		}
		$totalMarks = 0;
		foreach($subjectiveQuestions as $key=>$value)
		{
			if($value['questionType'] == 'question') {
				$totalMarks+=$subjectiveQuestions[$key]['marks'];
			} elseif ($value['questionType'] == 'questionGroup') {
				$qusId=$value['id'];
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('questions_subjective');
				$select->where(array(
							'id' => $qusId
							));
				$statement = $sql->getSqlStringForSqlObject($select);
				$questions_subjective = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
				$questions_subjective = $questions_subjective->toArray();
				$noOfsubquestions=$questions_subjective[0]['noOfSubquestionRequired'];
				//print_r($value['subQuestions']);
				//print_r($value['subQuestions']);
				if(count($value['subQuestions'])> $noOfsubquestions && $noOfsubquestions > 0){
					$subjectiveQuestions[$key]['subQuestions'] = array_slice($value['subQuestions'], 0, $noOfsubquestions);
					$subjectiveQuestions[$key]['marks'] = $subjectiveQuestions[$key]['marks']*$noOfsubquestions;
					$totalMarks = $totalMarks + ($subjectiveQuestions[$key]['marks']);
				}
				if ($shuffle == 'yes' && $value['listType']=='multi') {
					//$subjectiveQuestions[$key]['subQuestions'] = shuffle($value['subQuestions']);
				}
				//	print_r($value['subQuestions']);
			}
			
		}
		//print_r($subjectiveQuestions);
		//print_r('before 386');
		$qnoCounter =1;
		foreach($subjectiveQuestions as $key=>$value)
		{
			
			//print_r($value);
			
			$quesId=$value['id'];
			if($value['questionType'] == 'question')
			{
				$quesType=0;
				$examQuestions = new TableGateway('subjective_attempt_questions', $adapter, null,new HydratingResultSet());
				$examQuestions->insert(array(
					'attemptId' 			=> $data->attemptId,
					'questionId' 		=>	$quesId,
					'questionType' 		=>	$quesType,
					'parentId'			=>  0,
					'correct'			=>	0,
					'wrong'				=>	0,
					'time' 				=>	0,
					'status' 			=>	0,
					'check'				=>	0,
					'order'				=>	0
				));
				$examQuestionsId = $examQuestions->getLastInsertValue();
				$subjectiveQuestions[$key]['qno']	=	$qnoCounter;
				$qnoCounter							=	$qnoCounter+1;
				//print_r($examQuestionsId);
				
			} else {
				if($value['listType'] == 'one'){
					$quesType=1;
					$subjectiveQuestions[$key]['qno']	=	$qnoCounter;
					$qnoCounter							=	$qnoCounter+1;
				}	else{
					$quesType=2;
				}
				$subques=$value['subQuestions'];
				$examQuestions = new TableGateway('subjective_attempt_questions', $adapter, null,new HydratingResultSet());
				$examQuestions->insert(array(
					'attemptId' 		=> $data->attemptId,
					'questionId' 		=>	$quesId,
					'questionType' 		=>	$quesType,
					'parentId'			=>  0,
					'correct'			=>	0,
					'wrong'				=>	0,
					'time' 				=>	0,
					'status' 			=>	0,
					'check'				=>	0,
					'order'				=>	0
				));
				$examQuestionsId = $examQuestions->getLastInsertValue();
				//print_r($examQuestionsId);
				
				foreach($subques as $key1=>$value1)
				{
					$quesId1=$value1['id'];
					$examQuestions = new TableGateway('subjective_attempt_questions', $adapter, null,new HydratingResultSet());
					$examQuestions->insert(array(
						'attemptId' 		=> $data->attemptId,
						'questionId' 		=>	$quesId1,
						'questionType' 		=>	$quesType,
						'parentId'			=> 	$quesId,
						'correct'			=>	0,
						'wrong'				=>	0,
						'time' 				=>	0,
						'status' 			=>	0,
						'check'				=>	0,
						'order'				=>	0
					));
					$examQuestionsId = $examQuestions->getLastInsertValue();
					if($value['listType'] == 'multi') {
						$subjectiveQuestions[$key]['subQuestions'][$key1]['qno']	=	$qnoCounter;
						$qnoCounter							=	$qnoCounter+1;
					}
					//print_r($examQuestionsId);
				}
			}
		}
		$qnoCounter--;
		//print_r($subjectiveQuestions);
		$result->submission	= $submission;
		$result->questions	= $subjectiveQuestions;
		$result->qtimer		= $qtimer;
		$result->totalQuestions = $qnoCounter;
		$result->totalMarks = $totalMarks;
		$result->status		= 1;
		return $result;
	} catch (Exception $e) {
		$result->status = 0;
		$result->message = $e->getMessage();
		return $result;
	}
}
//to get questions and categories of section which student is giving assignment in resume mode
public function getQuestionsForExamInResume($data) {
	$result = new stdClass();                               
	try {                                                   
		$adapter = $this->adapter;                          
		$sql = new Sql($adapter);                           
		$select = $sql->select();                           
		$select->from(array('aq'	=>	'attempt_questions'));
		$where = new \Zend\Db\Sql\Where();
		$where->equalTo('aq.sectionId', $data->sectionId);
		$where->equalTo('aq.attemptId', $data->attemptId);
		$where->greaterThanOrEqualTo('aq.parentId', 0);
		$select->where($where)
		->join(array('q'	=>	'questions'),
				'q.id = aq.questionId',
				array('question', 'description')
		)
		->order("order ASC");
		$selectString = $sql->getSqlStringForSqlObject($select);
		$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$questions = $questions->toArray();
		$result->questions = $questions;
		$parentId = 0;
		$parent = '';
		foreach($result->questions as $key => $q) {
			$q['resume'] = true;
			if($q['questionType'] != 5) {
				if($q['questionType'] == 7) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('question_files')
							->where(array('questionId' => $q['questionId']))
							->columns(array('audio' => 'stuff'));
					$string = $sql->getSqlStringForSqlObject($select);
					$qFiles = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
					$qFiles = $qFiles->toArray();
					$result->questions[$key]['audio'] = '';
					$result->questions[$key]['q'] = $string;
					if (count($qFiles)) {
						$result->questions[$key]['audio'] = $qFiles[0]['audio'];
					}
				}
				if($q['questionType'] == 3) {
					$temp = $this->getAnswerForEachQuestion($q);
					$result->questions[$key]['answer']['columnA'] = $temp['opt1'];
					$result->questions[$key]['answer']['columnB'] = $temp['opt2'];
					$result->questions[$key]['userAns'] = $this->getUserAnswerForEachQuestion($q, $data->attemptId);
				}
				else {
					$result->questions[$key]['answer'] = $this->getAnswerForEachQuestion($q);
					$result->questions[$key]['userAns'] = $this->getUserAnswerForEachQuestion($q, $data->attemptId);
				}
				if($q['parentId'] != 0) {
					if($parentId == $q['parentId']) {
						$result->questions[$key]['parent'] = $parent;
					}
					else {
						$parentId = $q['parentId'];
						$select = $sql->select();
						$select->from('questions')->where(array('id' => $q['parentId']))->columns(array('parent' => 'question'));
						$string = $sql->getSqlStringForSqlObject($select);
						$parent = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
						$parent = $parent->toArray();
						if(count($parent) > 0)
							$parent = $parent[0]['parent'];
						$result->questions[$key]['parent'] = $parent;
					}
				}
			}
			else {
				//fetch child questions and then call getAnswerForEachQuestion for each child question
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('questions')
						->order(new \Zend\Db\Sql\Expression("RAND()"));
				$select->where(array('delete'	=>	0,
									'status'	=>	1,
									'parentId'	=>	$q['questionId']));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$childQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$childQuestions = $childQuestions->toArray();
				foreach($childQuestions as $childKey	=>	$childQ) {
					$childQuestions[$childKey]['answer'] = $this->getAnswerForEachQuestion($childQ);
					$childQuestions[$childKey]['userAns'] = $this->getUserAnswerForEachQuestion($childQ, $data->attemptId);
				}
				$result->questions[$key]['childQuestions'] = $childQuestions;
			}
		}
		$result->status = 1;
		return $result;
	}
	catch(Exception $e) {
		$result->status = 0;
		$result->message = $e->getMessage();
		return $result;
	}
}
//to get questions and categories of section which student is giving exam
public function getQuestionsForExam($data) {
	$result = new stdClass();
	try {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		//getting categories of the section
		$select = $sql->select();
		$select->from('section_categories');
		$select->where(array('sectionId'		=>	$data->sectionId,
							'delete'	=>	0,
							'status'	=>	1));
		if($data->random)
			$select->order(new \Zend\Db\Sql\Expression("RAND()"));
		else
			$select->order('weight ASC');
		$selectString = $sql->getSqlStringForSqlObject($select);
		$categories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$categories = $categories->toArray();
		$result->categories = $categories;
		$result->questions = array();
		foreach($result->categories as $category) {
			$sql = new Sql($adapter);
			$select = $sql->select();
			if($category['questionType'] != 4) {
				$select->from('questions')
						->order(new \Zend\Db\Sql\Expression("RAND()"));
				$select->where(array('categoryId'	=>	$category['id'],
									'delete'	=>	0,
									'status'	=>	1,
									'parentId'	=>	0));
				$select->limit($category['required']);
				$selectString = $sql->getSqlStringForSqlObject($select);
			}
			else {
				$select->from(array('q1'	=>	'questions'))
						->join(array('q2'	=>	'questions'),
								'q2.id = q1.parentId',
								array('parent'	=>	'question'));
				$select->order('parentId');
				$select->order(new \Zend\Db\Sql\Expression("RAND()"));
				$where = new \Zend\Db\Sql\Where();
				$where->equalTo('q1.categoryId', $category['id']);
				$where->equalTo('q1.delete', 0);
				$where->equalTo('q1.status', 1);
				$where->notEqualTo('q1.parentId', 0);
				$select->where($where);
				$select->limit($category['required']);
				$selectString = $sql->getSqlStringForSqlObject($select);
			}
			$questions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$questions = $questions->toArray();
			$parentId = 0;
			$parent = '';
			foreach($questions as $key => $q) {
				if($q['questionType'] != 5) {
					if($q['questionType'] == 7) {
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from('question_files')
								->where(array('questionId' => $q['id']))
								->columns(array('audio' => 'stuff'));
						$string = $sql->getSqlStringForSqlObject($select);
						$qFiles = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
						$qFiles = $qFiles->toArray();
						$questions[$key]['audio'] = '';
						if (count($qFiles)) {
							$questions[$key]['audio'] = $qFiles[0]['audio'];
						}
					}
					if($q['questionType'] == 3) {
						$temp = $this->getAnswerForEachQuestion($q);
						$questions[$key]['answer']['columnA'] = $temp['opt1'];
						$questions[$key]['answer']['columnB'] = $temp['opt2'];
					}
					else
						$questions[$key]['answer'] = $this->getAnswerForEachQuestion($q);
					if($q['parentId'] != 0) {
						if($parentId == $q['parentId']) {
							$questions[$key]['parent'] = $parent;
						}
						else {
							$parentId = $q['parentId'];
							$select = $sql->select();
							$select->from('questions')->where(array('id' => $q['parentId']))->columns(array('parent' => 'question'));
							$string = $sql->getSqlStringForSqlObject($select);
							$parent = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
							$parent = $parent->toArray();
							if(count($parent) > 0)
								$parent = $parent[0]['parent'];
							$questions[$key]['parent'] = $parent;
						}
					}
				}
				else {
					//fetch child questions and then call getAnswerForEachQuestion for each child question
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('questions')
							->order(new \Zend\Db\Sql\Expression("RAND()"));
					$select->where(array('categoryId'	=>	$category['id'],
										'delete'	=>	0,
										'status'	=>	1,
										'parentId'	=>	$q['id']));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$childQuestions = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$childQuestions = $childQuestions->toArray();
					foreach($childQuestions as $childKey	=>	$childQ) {
						$childQuestions[$childKey]['answer'] = $this->getAnswerForEachQuestion($childQ);
					}
					$questions[$key]['childQuestions'] = $childQuestions;
				}
			}
			$result->questions[] = $questions;
		}
		$result->status = 1;
		return $result;
	}
	catch(Exception $e) {
		$result->status = 0;
		$result->message = $e->getMessage();
		return $result;
	}
}
public function saveQuestion($data) {
			
	$questionidarr= array();
	$result = new stdClass();
	try {
		$adapter = $this->adapter;
		$question = new TableGateway('questions', $adapter, null,new HydratingResultSet());
		$insert = array(
			'categoryId'		=>	$data->categoryId,
			'questionType'		=> 	$data->questionType,
			'question'			=>	$data->question,
			'description'		=>	$data->desc,
			'difficulty'		=>	((isset($data->difficulty))?$data->difficulty:0),
			'status'			=>	$data->status,
			'parentId'			=>	$data->parentId,
			'delete'			=>	0
		);
		$question->insert($insert);
		$result->questionId = $question->getLastInsertValue();
		array_push($questionidarr,$result->questionId);
		
		
		$reqTags = array();
		if (!empty($data->tags)) {
			$reqTags = $data->tags;					
			foreach ($data->tags as $key => $tag) {
				$query="SELECT *
						FROM `question_tags`
						WHERE `tag`= '$tag'";
				$tagCheck = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tagCheck = $tagCheck->toArray();
				if (count($tagCheck)) {
					$tagId = $tagCheck[0]['id'];
					$questionTags = new TableGateway('question_tag_links', $adapter, null,new HydratingResultSet());
					$insert = array(
									'question_id' => $result->questionId,
									'tag_id' => $tagId
								);
					$questionTags->insert($insert);
				} else {
					$questionTags = new TableGateway('question_tags', $adapter, null,new HydratingResultSet());
					$insert = array(
									'tag'	=>  $tag
								);
					$questionTags->insert($insert);
					$tagId = $questionTags->getLastInsertValue();
					//var_dump('2: '.$tagId);
					$questionTags = new TableGateway('question_tag_links', $adapter, null,new HydratingResultSet());
					$insert = array(
									'question_id' => $result->questionId,
									'tag_id' => $tagId
								);
					$questionTags->insert($insert);
				}
			}
		}
		$query="SELECT sqtl.id, qt.tag
				FROM `question_tag_links` AS sqtl
				INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
				WHERE sqtl.question_id= '$result->questionId'";
		$tagsInDb = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
		$tagsInDb = $tagsInDb->toArray();
		if (count($tagsInDb)>0){
			if(!empty($reqTags)) {
				$deleteTagIds = array();
				foreach ($tagsInDb as $key => $tag) {
					if (in_array($tag['tag'], $reqTags)) {
						unset($tagsInDb[$key]);
					} else {
						$deleteTagIds[] = $tag['id'];
					}
				}
				if (!empty($deleteTagIds)) {
					$delete = $sql->delete();
					$delete->from('question_tag_links');
					$delete->where(array('id'	=>	$deleteTagIds));
					$statement = $sql->prepareStatementForSqlObject($delete);
					$statement->execute();
				}
			} else {
				$delete = $sql->delete();
				$delete->from('question_tag_links');
				$delete->where(array('question_id'	=>	$result->questionId));
				$statement = $sql->prepareStatementForSqlObject($delete);
				$statement->execute();
			}
		}
		$newcountenter=0;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('section_categories');
		$select->where(array('id'	=>	$data->categoryId, 'delete' => 0));
		$select->columns(array('entered'));
		$selectString = $sql->getSqlStringForSqlObject($select);
		$enteredCount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$enteredCount = $enteredCount->toArray();
		if(count($enteredCount)>0)
		{
			$newcountenter=$enteredCount[0]['entered'];
		}
		
		if($result->questionId == 0) {
			$result->status = 0;
			$result->message = "Some error occured";
			return $result;
		}
		if($data->questionType == 0 || $data->questionType == 6) {
			if(count($data->options) > 0) {
				/*$insertString = "INSERT INTO options_mcq (questionId, parentId, `option`, correct) VALUES";
				foreach($data->options as $option) {
					$insertString .= " ('{$result->questionId}','{$data->parentId}','{$option->opt}','{$option->cState}'),";
				}
				$insertString = substr($insertString, 0, strlen($insertString)-1);
				$adapter->query($insertString, $adapter::QUERY_MODE_EXECUTE);*/
				foreach($data->options as $option) {
					$options_mcq = new TableGateway('options_mcq', $adapter, null,new HydratingResultSet());
					$insert = array(
									'questionId'	=>  $result->questionId,
									'parentId'		=>	$data->parentId,
									'option'		=>	$option->opt,
									'correct'		=>	$option->cState
								);
					$options_mcq->insert($insert);
				}
			}
		}
		else if($data->questionType == 1) {
			if($data->answer !== '') {
				$insertString = "INSERT INTO true_false (questionId, parentId, answer) VALUES('{$result->questionId}','{$data->parentId}','{$data->answer}')";
				$adapter->query($insertString, $adapter::QUERY_MODE_EXECUTE);
			}
		}
		else if($data->questionType == 2) {
			if(count($data->blanks) > 0) {
				$insertString = "INSERT INTO ftb (questionId, parentId, blanks) VALUES";
				foreach($data->blanks as $blank) {
					$insertString .= "('{$result->questionId}','{$data->parentId}','{$blank->ftb}'),";
				}
				$insertString = substr($insertString, 0, strlen($insertString)-1);
				$adapter->query($insertString, $adapter::QUERY_MODE_EXECUTE);
			}
		}
		else if($data->questionType == 3) {
			if(count($data->matches) > 0) {
				/*$insertString = "INSERT INTO options_mtf (questionId, columnA, columnB) VALUES";
				foreach($data->matches as $match) {
					$insertString .= "('{$result->questionId}','{$match->optA}','{$match->optB}'),";
				}
				$insertString = substr($insertString, 0, strlen($insertString)-1);
				$adapter->query($insertString, $adapter::QUERY_MODE_EXECUTE);*/
				foreach($data->matches as $match) {
					$options_mtf = new TableGateway('options_mtf', $adapter, null,new HydratingResultSet());
					$insert = array(
									'questionId'	=>  $result->questionId,
									'columnA'		=>	$match->optA,
									'columnB'		=>	$match->optB
								);
					$options_mtf->insert($insert);
				}
			}
		}
		if($data->questionType == 7) {
			if(count($data->options) > 0) {
				foreach($data->options as $option) {
					$options_mcq = new TableGateway('options_mcq', $adapter, null,new HydratingResultSet());
					$insert = array(
									'questionId'	=>  $result->questionId,
									'parentId'		=>	$data->parentId,
									'option'		=>	$option->opt,
									'correct'		=>	$option->cState
								);
					$options_mcq->insert($insert);
				}
			}
			$question_files = new TableGateway('question_files', $adapter, null,new HydratingResultSet());
			$insert = array(
							'questionId'	=>  $result->questionId,
							'stuff'			=>	$data->audio,
							'duration'		=>	123456
						);
			$question_files->insert($insert);
		}
		if($data->status == 1 && $data->questionType != 4 && $data->parentId == 0) {
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('section_categories');
			$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered + 1")));
			$update->where(array('id' => $data->categoryId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$statement->execute();
		}
		else if($data->questionType == 4 && $data->status == 1) {
			$amount = 0;
			foreach($data->childQuestions as $newQuestion) {
				if($newQuestion->status == 1)
					$amount++;
			}
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('section_categories');
			$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered + ".$amount)));
			$update->where(array('id' => $data->categoryId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$statement->execute();
		}
		if($data->questionType == 4 || $data->questionType == 5) {
			foreach($data->childQuestions as $newQuestion) {
				$newQuestion->parentId = $result->questionId;
				$newQuestion->categoryId = $data->categoryId;
				$e = new Exam();
				$e->saveQuestion($newQuestion);
			}
		}
		/*$e = new Exam();
		
		$e->updateCategoryStatus($data);*/
		if($data->questionType == 4 ) {
			
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('questions');
			$select->where(array('parentId'	=>	$questionidarr[0],'status' => 1,'delete' => 0));
			$select->columns(array('id'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$envalues = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$envalues = $envalues->toArray();
			$yovalue=count($envalues);
						
			$finalvalue=$newcountenter+$yovalue;
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('section_categories');
			$update->set(array('entered' => $finalvalue));
			$update->where(array('id' => $data->categoryId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$statement->execute();
		}
		
		$sql = new Sql($adapter);
		$selectString = "UPDATE section_categories SET entered=(select COUNT(*) from questions where categoryId={$data->categoryId} and parentId=0 ) WHERE id={$data->categoryId}";
		$marks = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$e = new Exam();
		$e->updateCategoryStatus($data);
		$result->status = 1;
		return $result;
	} catch(Exception $e) {
		$result->status = 0;
		$result->message = $e->getMessage();
		return $result;
	}
}
public function editQuestion($data) {
		
	$result =new stdClass();
	try {
		$adapter = $this->adapter;
		//if category has been switched
		if($data->categorySwitch == true) {
			//if question is other than cmp
			if($data->questionType != 4 && $data->parentId == 0) {
				//decrease 1 from old category
				//need to check if question status is 0 then don't decrease the count
				if($data->status == 1) {
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('section_categories');
					$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered - 1")));
					$update->where(array('id' => $data->oldCategory));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
					//increase 1 to new category if status is complete
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('section_categories');
					$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered + 1")));
					$update->where(array('id' => $data->categoryId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
				}
			}//if question type is cmp
			else if($data->questionType == 4) {
				//decrease from old category such many question were complete inside it
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('section_categories');
				$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered - (SELECT COUNT(*) FROM questions WHERE `delete`=0 AND `status`=1 AND parentId=".$data->questionId.")")));
				$update->where(array('id' => $data->oldCategory));

				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
				$amount = 0;
				//increse into new category such many question as are not new and status is complete
				foreach($data->childQuestions as $newQuestion) {
					if($newQuestion->questionId != 0 && $newQuestion->status == 1)
						$amount++;
				}
				if($amount != 0) {
				//print_r("amount !0");
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('section_categories');
					$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered + ".$amount)));
					$update->where(array('id' => $data->categoryId));
					//print_r($update);
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
				}
			}
		}//if category is not switched
		else {
			if($data->questionType != 4 && $data->parentId == 0) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('questions');
				$select->where(array('id'	=>	$data->questionId));
				$select->columns(array('status'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$oldStatus = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$oldStatus = $oldStatus->toArray();
				if($oldStatus[0]['status'] != 1 && $data->status == 1) {
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('section_categories');
					$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered + 1")));
					$update->where(array('id' => $data->categoryId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
				}
				else if($oldStatus[0]['status'] == 1 && ($data->status == 0 || $data->status == 2)) {
					$sql = new Sql($adapter);
					$sql = new Sql($adapter);
					$update = $sql->update();
					$update->table('section_categories');
					$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered - 1")));
					$update->where(array('id' => $data->categoryId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
				}
			}
			else if($data->questionType == 4) {
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('section_categories');
				$select->where(array('id'	=>	$data->categoryId, 'delete' => 0));
				$select->columns(array('entered'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$enteredCount = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$enteredCount = $enteredCount->toArray();
				$newcountenter=$enteredCount[0]['entered'];
				$yovalue=0;									
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('questions');
				$select->where(array('parentId'	=>	$data->questionId,'status' => 1,'delete' => 0));
				$select->columns(array('id'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$envalues = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$envalues = $envalues->toArray();
				$yovalue=count($envalues);
				$counter=0;
				$kill=0;
				$amount = 0;//need to change							
			
				foreach($data->childQuestions as $newQuestion) {
					//print_r($newQuestion);
					$counter=$counter+1;
					if( ($newQuestion->status == 0 || $newQuestion->status == 2)){
						$kill++;
					}
				}						
				$counter=$counter-$kill;
				if($newcountenter==0)
				{
					$newenteredvalue=	(($yovalue));
				} else {
					$newenteredvalue=	(($newcountenter) - ($yovalue) +($counter));	
				}		
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('section_categories');
				$update->set(array('entered' => $newenteredvalue));
					
				$update->where(array('id' => $data->categoryId));
				//print_r($update);
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
			}
		}
		$sql = new Sql($this->adapter);
		$update = $sql->update();
		$update->table('questions');
		$update->set(array('categoryId'		=>	$data->categoryId,
							'questionType' 	=>	$data->questionType,
							'question'		=>	$data->question,
							'description' 	=>	$data->desc,
							'difficulty'	=>	((isset($data->difficulty))?$data->difficulty:0),
							'status'		=>	$data->status
					));
		$update->where(array('id' => $data->questionId));
		$statement = $sql->prepareStatementForSqlObject($update);
		$statement->execute();
		$reqTags = array();
		if (!empty($data->tags)) {
			$reqTags = $data->tags;
			foreach ($data->tags as $key => $tag) {
				$query="SELECT *
						FROM `question_tags`
						WHERE `tag`= '$tag'";
				$tagCheck = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tagCheck = $tagCheck->toArray();
				if (count($tagCheck)) {
					$tagId = $tagCheck[0]['id'];
					$query="SELECT *
							FROM `question_tag_links`
							WHERE `question_id`={$data->questionId} AND `tag_id`= '$tagId'";
					$tagValue = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$tagValue = $tagValue->toArray();
					if (count($tagValue)) {
						unset($data->tags[$key]);
					} else {
						$questionTags = new TableGateway('question_tag_links', $adapter, null,new HydratingResultSet());
						$insert = array(
										'question_id' => $data->questionId,
										'tag_id' => $tagId
									);
						$questionTags->insert($insert);
					}
				} else {
					$questionTags = new TableGateway('question_tags', $adapter, null,new HydratingResultSet());
					$insert = array(
									'tag'	=>  $tag
								);
					$tagId = $questionTags->insert($insert);
					$questionTags = new TableGateway('question_tag_links', $adapter, null,new HydratingResultSet());
					$insert = array(
									'question_id' => $data->questionId,
									'tag_id' => $tagId
								);
					$questionTags->insert($insert);
				}
			}
		}
		$query="SELECT sqtl.id, qt.tag
				FROM `question_tag_links` AS sqtl
				INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
				WHERE sqtl.question_id= '$data->questionId'";
		$tagsInDb = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
		$tagsInDb = $tagsInDb->toArray();
		if (count($tagsInDb)>0){
			if(!empty($reqTags)) {
				$deleteTagIds = array();
				foreach ($tagsInDb as $key => $tag) {
					if (in_array($tag['tag'], $reqTags)) {
						unset($tagsInDb[$key]);
					} else {
						$deleteTagIds[] = $tag['id'];
					}
				}
				if (!empty($deleteTagIds)) {
					$delete = $sql->delete();
					$delete->from('question_tag_links');
					$delete->where(array('id'	=>	$deleteTagIds));
					$statement = $sql->prepareStatementForSqlObject($delete);
					$statement->execute();
				}
			} else {
				$delete = $sql->delete();
				$delete->from('question_tag_links');
				$delete->where(array('question_id'	=>	$data->questionId));
				$statement = $sql->prepareStatementForSqlObject($delete);
				$statement->execute();
			}
		}
		if($data->questionType == 0 || $data->questionType == 6) {
			$sql = new Sql($this->adapter);
			$delete = $sql->delete();
			$delete->from('options_mcq');
			$delete->where(array('questionId' => $data->questionId));
			$statement = $sql->prepareStatementForSqlObject($delete);
			$statement->execute();
			if(count($data->options) > 0) {
				for($i=0; $i<count($data->options); $i++) {
					$options_mcq = new TableGateway('options_mcq', $adapter, null,new HydratingResultSet());
					$insert = array(
									'questionId'	=>  $data->questionId,
									'parentId'		=>	$data->parentId,
									'option'		=>	$data->options[$i]->opt,
									'correct'		=>	$data->options[$i]->cState
								);
					$options_mcq->insert($insert);
				}
				//$adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);
			}
		}
		else if($data->questionType == 1) {
			$sql = new Sql($this->adapter);
			$delete = $sql->delete();
			$delete->from('true_false');
			$delete->where(array('questionId' => $data->questionId));
			$statement = $sql->prepareStatementForSqlObject($delete);
			$statement->execute();
			if($data->answer !== '') {
				$insertString = "INSERT INTO true_false (questionId, parentId, answer) VALUES('{$data->questionId}','{$data->parentId}',{$data->answer})";
				$adapter->query($insertString, $adapter::QUERY_MODE_EXECUTE);
			}
		}
		else if($data->questionType == 2) {
			$sql = new Sql($this->adapter);
			$delete = $sql->delete();
			$delete->from('ftb');
			$delete->where(array('questionId' => $data->questionId));
			$statement = $sql->prepareStatementForSqlObject($delete);
			$statement->execute();
			if(count($data->blanks) > 0) {
				$insert = "INSERT INTO ftb (questionId, parentId, blanks) VALUES";
				for($i=0; $i<count($data->blanks); $i++) {
					$insert .= "('{$data->questionId}','{$data->parentId}','{$data->blanks[$i]->ftb}'),";
				}
				$insert = substr($insert, 0, strlen($insert)-1);
				$adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);
			}
		}
		else if($data->questionType == 3) {
			$sql = new Sql($this->adapter);
			$delete = $sql->delete();
			$delete->from('options_mtf');
			$delete->where(array('questionId' => $data->questionId));
			$statement = $sql->prepareStatementForSqlObject($delete);
			$statement->execute();
			if(count($data->matches) > 0) {
				/*$insert = "INSERT INTO options_mtf (questionId, columnA, columnB) VALUES";
				for($i=0; $i<count($data->matches); $i++) {
					$insert .= "('{$data->questionId}','{$data->matches[$i]->optA}','{$data->matches[$i]->optB}'),";
				}
				$insert = substr($insert, 0, strlen($insert)-1);
				$adapter->query($insert, $adapter::QUERY_MODE_EXECUTE);*/
				for($i=0; $i<count($data->matches); $i++) {
					$options_mtf = new TableGateway('options_mtf', $adapter, null,new HydratingResultSet());
					$insert = array(
									'questionId'	=>  $data->questionId,
									'columnA'		=>	$data->matches[$i]->optA,
									'columnB'		=>	$data->matches[$i]->optB
								);
					$options_mtf->insert($insert);
				}
			}
		}
		else if($data->questionType == 4 || $data->questionType == 5) {
			$delIds = '';
			foreach($data->childQuestions as $newQuestion) {
				$newQuestion->parentId = $data->questionId;
				$newQuestion->categoryId = $data->categoryId;
				$e = new Exam();
				if($newQuestion->questionId == 0) {
					$temp = $e->saveQuestion($newQuestion);
					$delIds .= $temp->questionId.',';
				}
				else {
					$e->editQuestion($newQuestion);
					$delIds .= $newQuestion->questionId.',';
				}
			}
			$delIds = substr($delIds, 0, strlen($delIds) - 1);
			$deleteQuestions = "DELETE FROM questions WHERE id NOT IN (".$delIds.") AND parentId=".$data->questionId.";";
			$deleteTF = "DELETE FROM true_false WHERE questionId NOT IN (".$delIds.") AND parentId=".$data->questionId.";";
			$deleteMCQ = "DELETE FROM options_mcq WHERE questionId NOT IN (".$delIds.") AND parentId=".$data->questionId.";";
			$deleteFTB = "DELETE FROM ftb WHERE questionId NOT IN (".$delIds.") AND parentId=".$data->questionId.";";
			$adapter->query($deleteQuestions, $adapter::QUERY_MODE_EXECUTE);
			$adapter->query($deleteTF, $adapter::QUERY_MODE_EXECUTE);
			$adapter->query($deleteMCQ, $adapter::QUERY_MODE_EXECUTE);
			$adapter->query($deleteFTB, $adapter::QUERY_MODE_EXECUTE);
		}
		else if($data->questionType == 7) {
			$sql = new Sql($this->adapter);
			$delete = $sql->delete();
			$delete->from('options_mcq');
			$delete->where(array('questionId' => $data->questionId));
			$statement = $sql->prepareStatementForSqlObject($delete);
			$statement->execute();
			if(count($data->options) > 0) {
				for($i=0; $i<count($data->options); $i++) {
					$options_mcq = new TableGateway('options_mcq', $adapter, null,new HydratingResultSet());
					$insert = array(
									'questionId'	=>  $data->questionId,
									'parentId'		=>	$data->parentId,
									'option'		=>	$data->options[$i]->opt,
									'correct'		=>	$data->options[$i]->cState
								);
					$options_mcq->insert($insert);
				}
			}
			$select = $sql->select();
			$select->from('question_files');
			$select->where(array('questionId' => $data->questionId));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$qFiles = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$qFiles = $qFiles->toArray();
			if (count($qFiles)>0) {
				$select = $sql->update();
				$select->table('question_files');
				$select->set(array(
								'questionId'	=>  $data->questionId,
								'stuff'			=>	$data->audio,
								'duration'		=>	123456
							));
				$select->where(array('questionId'=>$data->questionId));
				$statement = $sql->prepareStatementForSqlObject($select);
				$statement->execute();
			} else {
				/*$sql = new Sql($this->adapter);
				$delete = $sql->delete();
				$delete->from('question_files');
				$delete->where(array('questionId' => $data->questionId));
				$statement = $sql->prepareStatementForSqlObject($delete);
				$statement->execute();*/
				$question_files = new TableGateway('question_files', $adapter, null,new HydratingResultSet());
				$insert = array(
								'questionId'	=>  $data->questionId,
								'stuff'			=>	$data->audio,
								'duration'		=>	123456
							);
				$question_files->insert($insert);
			}
		}
		$sql = new Sql($adapter);
		$selectString = "UPDATE section_categories SET entered=(select COUNT(*) from questions where categoryId={$data->categoryId} and parentId=0 ) WHERE id={$data->categoryId}";
		$marks = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$e = new Exam();
		$e->updateCategoryStatus($data);
		$result->status = 1;
		return $result;
	} catch(Exception $e) {
		$result->status = 0;
		$result->message = $e->getMessage();
		return $result;
	}
}
public function deleteQuestion($data) {
	$count=0;
	$result = new stdClass();
	try {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$update = $sql->update();
		$update->table('questions');
		$update->set(array('delete'	=>	1));
		$update->where(array('id'	=>	$data->questionId , 'delete' => 0));
		$statement = $sql->prepareStatementForSqlObject($update);
		$count = $statement->execute()->getAffectedRows();
		if($count >0){		
			if($data->questionType != 5 && $data->questionType != 4) {
				if($data->status == 1) {
					$update = $sql->update();
					$update->table('section_categories');
					$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered - 1")));
					$update->where(array('id' => $data->categoryId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
				}
				$sql = new Sql($adapter);
				$update = $sql->update();
				if($data->questionType == 0 || $data->questionType == 6 || $data->questionType == 7)
					$update->table('options_mcq');
				else if($data->questionType == 1)
					$update->table('true_false');	
				else if($data->questionType == 2)
					$update->table('ftb');
				else if($data->questionType == 3)
					$update->table('options_mtf');
				$update->set(array('delete'	=>	1));
				$update->where(array('questionId'	=>	$data->questionId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
			}
			else {
				if($data->questionType == 4) {
					$update = $sql->update();
					$update->table('section_categories');
					$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered -".$data->amount)));
					$update->where(array('id' => $data->categoryId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
				}
				else if($data->questionType == 5) {
					$update = $sql->update();
					$update->table('section_categories');
					$update->set(array('entered' => new \Zend\Db\Sql\Expression("entered - 1")));
					$update->where(array('id' => $data->categoryId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$statement->execute();
				}
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('questions');
				$update->set(array('delete'	=>	1));
				$update->where(array('parentId'	=>	$data->questionId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$statement->execute();
			}
			$e = new Exam();
			$e->updateCategoryStatus($data);
		}
		$result->status = 1;
		return $result;
	}
	catch(Exception $e) {
		$result->status = 0;
		$result->message = $e->getMessage();
		return $result;
	}
}
public function insertSubjectiveQuestion($data) {
	//print_r($data);
	$questionArrays= json_decode($data->questions);
	//print_r($questionArrays);
	$fileLocation='';
	$marks		=	0;
	$parentId	=	0;
	$questionId	=	0;
	$result = new stdClass();
	try {
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$question = $questionArrays->question;
		$answer   = $questionArrays->answer;
		$noOfSubquestionRequired=0;
		$questionsonPage	= 0;
		$difficulty			= 0;
		if($data->parent !='')
		{
			$parentId  =  $data->parent;
		}
					
		if($questionArrays->questionType == 'questionGroup')
		{
			$noOfSubquestionRequired=$questionArrays->answer;
			$questionsonPage=$questionArrays->questionsonPage;
			$marks=$questionArrays->subquestionMrks;
		}else{
			$marks=$questionArrays->marks;
			$difficulty=$questionArrays->difficulty;
		}
		$questions_subjective = new TableGateway('questions_subjective', $adapter, null,new HydratingResultSet());
		$insert = array(
						'examId'				=>  $data->subjectiveExamId,
						'question'				=>	$question,
						'answer'				=>	$answer,
						'questionsonPage'		=>  $questionsonPage,
						'deleted'				=>	0,
						'parentId'				=>	$parentId,
						'marks'					=>  $marks,
						'difficulty'			=>  $difficulty,
						'noOfSubquestionRequired'=> $noOfSubquestionRequired,
						'status'				=>	1
					);
		$questions_subjective->insert($insert);
		$result->questionId = $questions_subjective->getLastInsertValue();

		$reqTags = array();
		if (!empty($data->tags)) {
			$reqTags = $data->tags;
			foreach ($data->tags as $key => $tag) {
				$query="SELECT *
						FROM `question_tags`
						WHERE `tag`= '$tag'";
				$tagCheck = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$tagCheck = $tagCheck->toArray();
				if (count($tagCheck)) {
					$tagId = $tagCheck[0]['id'];
					$questionTags = new TableGateway('subjective_question_tag_links', $adapter, null,new HydratingResultSet());
					$insert = array(
									'question_id' => $result->questionId,
									'tag_id' => $tagId
								);
					$questionTags->insert($insert);
				} else {
					$questionTags = new TableGateway('question_tags', $adapter, null,new HydratingResultSet());
					$insert = array(
									'tag'	=>  $tag
								);
					$questionTags->insert($insert);
					$tagId = $questionTags->getLastInsertValue();
					//var_dump('2: '.$tagId);
					$questionTags = new TableGateway('subjective_question_tag_links', $adapter, null,new HydratingResultSet());
					$insert = array(
									'question_id' => $result->questionId,
									'tag_id' => $tagId
								);
					$questionTags->insert($insert);
				}
			}
		}
		$query="SELECT sqtl.id, qt.tag
				FROM `subjective_question_tag_links` AS sqtl
				INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
				WHERE sqtl.question_id= '$result->questionId'";
		$tagsInDb = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
		$tagsInDb = $tagsInDb->toArray();
		if (count($tagsInDb)>0){
			if(!empty($reqTags)) {
				$deleteTagIds = array();
				foreach ($tagsInDb as $key => $tag) {
					if (in_array($tag['tag'], $reqTags)) {
						unset($tagsInDb[$key]);
					} else {
						$deleteTagIds[] = $tag['id'];
					}
				}
				if (!empty($deleteTagIds)) {
					$delete = $sql->delete();
					$delete->from('subjective_question_tag_links');
					$delete->where(array('id'	=>	$deleteTagIds));
					$statement = $sql->prepareStatementForSqlObject($delete);
					$statement->execute();
				}
			} else {
				$delete = $sql->delete();
				$delete->from('subjective_question_tag_links');
				$delete->where(array('question_id'	=>	$result->questionId));
				$statement = $sql->prepareStatementForSqlObject($delete);
				$statement->execute();
			}
		}
		$query="SELECT qt.id, qt.tag AS name
				FROM `subjective_question_tag_links` AS sqtl
				INNER JOIN `question_tags` AS qt ON sqtl.tag_id=qt.id
				WHERE sqtl.question_id= '$result->questionId'";
		$tags = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
		$tags = $tags->toArray();

		$result->questionBrief = $this->displayString($question);
		$result->answerBrief = $this->displayString($answer);
		$result->tags = $tags;
		$result->status = 1;
		return $result;
	} catch(Exception $e) {
		$result->status = 0;
		$result->message = $e->getMessage();
		return $result;
	}
}
?>