<?php

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "Base.php";
//$adapter = require "adapter.php";
class CourseDetail extends Base {
	public function getCourseDetails($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('c' => 'courses'));
			$arrDetails = array(
				'c.deleted'				=>	0,
				'c.approved'			=>	1,
				'c.availStudentMarket'	=>	1,
				'c.liveForStudent'		=>	1
			);
			if (isset($data->courseId) && !empty($data->courseId)) {
				$arrDetails['c.id'] = $data->courseId;
			} else {
				$arrDetails['c.slug'] = $data->slug;
			}
			$select->where($arrDetails);
			$select->join(array('ur' => 'user_roles'), 'ur.userId = c.ownerId', array('roleId'));
			$statement = $sql->prepareStatementForSqlObject($select);
			$courseDetails = $statement->execute();
			if($courseDetails->count() !== 0) {
				$temp = $courseDetails->next();
				$result->courseDetails = $temp;
				if($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courseDetails['image'] = getSignedURL($result->courseDetails['image']);
				} else {
					$result->courseDetails['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
				}
				$data->courseId = $result->courseDetails['id'];
				//adding rating with course details
				require_once 'Student.php';
				$s = new Student();
				$result->courseDetails['rating'] = $s->getCourseRatings($data->courseId);
				
				//fetching from discount table. null if no discount
				require_once 'Course.php';
				$req = new stdClass();
				$req->courseId = $data->courseId;
				$discount = new Course();
				$result->courseDetails['discount'] = $discount->getCourseDiscount($req);
				
			} else {
				$result->status = 404;
				$result->exception = "Course not found";
				return $result;
			}
			$query="SELECT categoryId, category, slug FROM course_categories ct JOIN categories c on ct.categoryId=c.id WHERE courseId={$data->courseId}";
			$courseCategories = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courseCategories = $courseCategories->toArray();
			require_once 'Tags.php';
			$tg = new Tags();
			$cTags = $tg->getCourseTags($data);
			$result->courseTags = array();
			if ($cTags->status == 1) {
				$result->courseTags = $cTags->tagData;
			}
			$table = 'institute_details';
			switch($result->courseDetails['roleId']) {
				case 1:
					$table = 'institute_details';
					break;
				case 2:
					$table = 'professor_details';
					break;
				case 3:
					$table = 'publisher_details';
					break;
			}
			$select = $sql->select();
			$select->from($table);
			$select->where(array('userId' =>  $result->courseDetails['ownerId']));
			/*$selectString = "SELECT ur.roleId, CASE WHEN ur.roleId=1 THEN (SELECT name, profilePic FROM institute_details WHERE userId = {$result->courseDetails['ownerId']}) WHEN ur.roleId=2 THEN (SELECT CONCAT(firstName, ' ', lastName) as name, profilePic FROM professor_details WHERE userId = {$result->courseDetails['ownerId']}) WHEN ur.roleId=3 (SELECT CONCAT(firstName, ' ', lastName) as name, profilePic FROM publisher_details WHERE userId = {$result->courseDetails['ownerId']}) FROM user_roles ur";*/
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courseCategories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->institute = $courseCategories->toArray();
			$result->institute = $result->institute[0];
			if($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
				require_once 'amazonRead.php';
				$result->institute['profilePic'] = getSignedURL($result->institute['profilePic']);
			} else {
				$result->institute['profilePic'] = $this->sitePath.'manage/assets/pages/media/profile/profile_user.jpg';
			}
			$result->selfCreated = 0;
			if(isset($data->userId)) {
				if($result->courseDetails['ownerId'] == $data->userId) {
					$result->selfCreated = 1;
				}
			}
			$result->studentCount = $this->getStudentCount($data);
			$result->currentDate = time() . '000';
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getPackageCourseDetails($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('c' => 'courses'));
			$select->where(array(
				'c.id'					=>	$data->courseId,
				'c.deleted'				=>	0,
				'c.approved'			=>	1
			));
			$select->join(array('ur' => 'user_roles'), 'ur.userId = c.ownerId', array('roleId'));
			$statement = $sql->prepareStatementForSqlObject($select);
			$courseDetails = $statement->execute();
			if($courseDetails->count() !== 0) {
				$temp = $courseDetails->next();
				/*if ($temp['liveDate'] != "") {
					$temp['liveDate'] = $temp['liveDate'] / 1000;
					$temp['liveDate'] = date('j-m-Y', $temp['liveDate']);
				}
				if ($temp['endDate'] != "") {
					$temp['endDate'] = $temp['endDate'] / 1000;
					$temp['endDate'] = date('j-m-Y', $temp['endDate']);
				}*/
				$result->courseDetails = $temp;
				if($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					require_once 'amazonRead.php';
					$result->courseDetails['image'] = getSignedURL($result->courseDetails['image']);
				} else {
					$result->courseDetails['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
				}

				//adding rating with course details
				require_once 'Student.php';
				$s = new Student();
				$result->courseDetails['rating'] = $s->getCourseRatings($data->courseId);
				
				//fetching from discount table. null if no discount
				require_once 'Course.php';
				$req = new stdClass();
				$req->courseId = $data->courseId;
				$discount = new Course();
				$result->courseDetails['discount'] = $discount->getCourseDiscount($req);
				
			} else {
				$result->status = 0;
				$result->exception = "Course not found";
				return $result;
			}
			$query="SELECT categoryId, category FROM course_categories ct JOIN categories c on ct.categoryId=c.id WHERE courseId={$data->courseId}";
			$courseCategories = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->courseCategories = $courseCategories->toArray();
			$table = 'institute_details';
			switch($result->courseDetails['roleId']) {
				case 1:
					$table = 'institute_details';
					break;
				case 2:
					$table = 'professor_details';
					break;
				case 3:
					$table = 'publisher_details';
					break;
			}
			$select = $sql->select();
			$select->from($table);
			$select->where(array('userId' =>  $result->courseDetails['ownerId']));
			/*$selectString = "SELECT ur.roleId, CASE WHEN ur.roleId=1 THEN (SELECT name, profilePic FROM institute_details WHERE userId = {$result->courseDetails['ownerId']}) WHEN ur.roleId=2 THEN (SELECT CONCAT(firstName, ' ', lastName) as name, profilePic FROM professor_details WHERE userId = {$result->courseDetails['ownerId']}) WHEN ur.roleId=3 (SELECT CONCAT(firstName, ' ', lastName) as name, profilePic FROM publisher_details WHERE userId = {$result->courseDetails['ownerId']}) FROM user_roles ur";*/
			$selectString = $sql->getSqlStringForSqlObject($select);
			$courseCategories = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->institute = $courseCategories->toArray();
			$result->institute = $result->institute[0];
			if($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
				require_once 'amazonRead.php';
				$result->institute['profilePic'] = getSignedURL($result->institute['profilePic']);
			} else {
				$result->institute['profilePic'] = $this->sitePath.'manage/assets/pages/media/profile/profile_user.jpg';
			}
			$result->selfCreated = 0;
			if(isset($data->userId)) {
				if($result->courseDetails['ownerId'] == $data->userId) {
					$result->selfCreated = 1;
				}
			}
			$result->studentCount = $this->getStudentCount($data);
			$result->currentDate = time() . '000';
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getCourseSubjects($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('s'	=>	'subjects'));
			$where = array(
							's.deleted'   =>  0
						);
			if (isset($data->courseId) && !empty($data->courseId)) {
				$where['courseId'] = $data->courseId;
			} else {
				$where['slug'] = $data->slug;
				$select->join(array('c'	=>	'courses'),
					'c.id=s.courseId',
					array('cid'	=>	'id')
				);
			}
			$select->where($where)->order('s.weight ASC')
					->order('s.id ASC');
			$selectString = $sql->getSqlStringForSqlObject($select);
			$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->tempSubjects = $subjects->toArray();
			if (count($result->tempSubjects) == 0) {
				$result->status = 0;
				$result->message = "No subject Found ";
			}
			$result->subjects = array();
			$result->totalQuestions = 0;
			foreach ($result->tempSubjects as $sub) {
				//according to latest design there is no need of professors
				/*$select = $sql->select();
				$select->from(array('sp' => 'subject_professors'))
						->join(array('p' => 'professor_details'), 'sp.professorId = p.userId', array('firstName', 'lastName', 'userId'));
				$select->where(array('subjectId' => $sub['id']));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$prof = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$sub['professors'] = $prof->toArray();*/
				// $result->subjects[] = $sub;
				$select = $sql->select();
				$select->from('chapters');
				$select->where(array(
					'subjectId' =>  $sub['id'],
					'deleted'   =>  0
				));
				$select->order('weight ASC, id ASC');
				$selectString = $sql->getSqlStringForSqlObject($select);
				$chapters = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$sub['chapters'] = $chapters->toArray();
				$duration = 0;
				foreach ($sub['chapters'] as $key => $chapter) {
					$exams=$this->getExamDetailsLong($sub['id'], $chapter['id']);
					$sub['chapters'][$key]['Exam'] = $exams['Exam'];
					$sub['chapters'][$key]['Assignment'] = $exams['Assignment'];
					$sub['chapters'][$key]['content'] = $this->getContentName($sub['id'], $chapter['id']);
					$questions=$this->getExamQuestions($sub['id'], $chapter['id']);
					$result->totalQuestions = $result->totalQuestions + $questions['ExamQuestions'] + $questions['AssQuestions'];
					foreach ($sub['chapters'][$key]['content'] as $key1 => $content) {
						if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
							require_once 'amazonRead.php';
							$sub['chapters'][$key]['content'][$key1]['metastuff'] = getSignedURL($sub['chapters'][$key]['content'][$key1]['metastuff']);
							$duration = $duration + $sub['chapters'][$key]['content'][$key1]['duration'];
						}
					}
					//content not required according to the latest design
					//$sub['chapters'][$key]['content'] = $this->getContentDetails($chapter['id']);
				}
				/*$temp = $this->getExamDetails($sub['id'], 0);
				$sub['independentExam'] = $temp[0]['Exam'];
				$temp = $this->getExamDetails($sub['id'], 0);
				$sub['independentAssignment'] = $temp[0]['Assignment'];*/
				$temp = $this->getExamDetailsLong($sub['id'], 0);
				$sub['independentExam'] = $temp['Exam'];
				$sub['independentAssignment'] = $temp['Assignment'];
				$questions=$this->getExamQuestions($sub['id'], 0);
				$result->totalQuestions = $result->totalQuestions + $questions['ExamQuestions'] + $questions['AssQuestions'];
				$sub['totalDuration'] = $duration;
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$sub['image'] = getSignedURL($sub['image']);
				} else {
					$sub['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
				}
				$result->subjects[] = $sub;
			}
			unset($result->tempSubjects);
			$result->status = 1;
			$result->currentDate = time() . '000';
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function getExamQuestions($subjectId, $chapterId) {
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$query = "SELECT COUNT(*) AS qCount FROM `exams` AS e
			INNER JOIN `exam_sections` AS es ON es.examId=e.id
			INNER JOIN `section_categories` AS sc ON sc.sectionId=es.id
			INNER JOIN `questions` AS q ON q.categoryId=sc.id
			WHERE (e.`delete`=0 AND e.`status`=2) AND (es.`delete`=0 AND es.`status`=1) AND (sc.`delete`=0 AND sc.`status`=1) AND (q.`delete`=0 AND q.`status`=1)
			AND e.`id` IN (SELECT `id` FROM `exams` WHERE subjectId=$subjectId and  chapterId=$chapterId and `type`='Assignment' and `delete` = 0 AND (`status` = 1 OR `status` = 2))";
			$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$qCount = $exams->toArray();
			$retExams['AssQuestions'] = $qCount[0]['qCount'];
			
			$query = "SELECT COUNT(*) AS qCount FROM `exams` AS e
			INNER JOIN `exam_sections` AS es ON es.examId=e.id
			INNER JOIN `section_categories` AS sc ON sc.sectionId=es.id
			INNER JOIN `questions` AS q ON q.categoryId=sc.id
			WHERE (e.`delete`=0 AND e.`status`=2) AND (es.`delete`=0 AND es.`status`=1) AND (sc.`delete`=0 AND sc.`status`=1) AND (q.`delete`=0 AND q.`status`=1)
			AND e.`id` IN (SELECT `id` FROM `exams` WHERE subjectId=$subjectId and  chapterId=$chapterId and `type`='Exam' and `delete` = 0 AND (`status` = 1 OR `status` = 2))";
			$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$qCount = $exams->toArray();
			$retExams['ExamQuestions'] = $qCount[0]['qCount'];
			
			return $retExams;
		} catch (Exception $e) {
			// $result->status = 0;
			// $result->exception = $e->getMessage();
			//SELECT sum(case when type='Assignment' then 1 else 0 end )Assignment,sum(case when type='Exam' then 1 else 0 end )Exam  FROM `exams` WHERE subjectId=5 and  chapterId=4
			//  return $result;
		}
	}

	public function getExamDetailsLong($subjectId, $chapterId) {
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$query = "SELECT `id`, `name`, `startDate`, `endDate` FROM `exams` WHERE subjectId=$subjectId and  chapterId=$chapterId and `type`='Assignment' and `delete` = 0 AND (`status` = 1 OR `status` = 2)";
			$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$retExams['Assignment'] = $exams->toArray();
			
			$query = "SELECT `id`, `name`, `startDate`, `endDate` FROM `exams` WHERE subjectId=$subjectId and  chapterId=$chapterId and `type`='Exam' and `delete` = 0 AND (`status` = 1 OR `status` = 2)";
			$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$retExams['Exam'] = $exams->toArray();
			
			return $retExams;
		} catch (Exception $e) {
			// $result->status = 0;
			// $result->exception = $e->getMessage();
			//SELECT sum(case when type='Assignment' then 1 else 0 end )Assignment,sum(case when type='Exam' then 1 else 0 end )Exam  FROM `exams` WHERE subjectId=5 and  chapterId=4
			//  return $result;
		}
	}

	public function getExamDetails($subjectId, $chapterId) {
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$query = "SELECT sum(case when type='Assignment' then 1 else 0 end ) Assignment, sum(case when type='Exam' then 1 else 0 end ) Exam  FROM `exams` WHERE subjectId=$subjectId and  chapterId=$chapterId and `delete` = 0 AND `status` = 2";
			$exams = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			return $exams->toArray();
		} catch (Exception $e) {
			// $result->status = 0;
			// $result->exception = $e->getMessage();
			//SELECT sum(case when type='Assignment' then 1 else 0 end )Assignment,sum(case when type='Exam' then 1 else 0 end )Exam  FROM `exams` WHERE subjectId=5 and  chapterId=4
			//  return $result;
		}
	}

	public function getContentDetails($chapterId) {
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('headings')
					->order('weight ASC');
			$where = new \Zend\Db\Sql\Where();
			$where->equalTo('chapterId', $chapterId);
			$select->where($where);
			$select->columns(array('id', 'title'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$content = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			return $content->toArray();
		} catch (Exception $e) {
			// $result->status = 0;
			// $result->exception = $e->getMessage();
			// return $result;
		}
	}
	public function getStudentCount($data) {
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('student_course');
			$select->where(array('course_id'	=>	$data->courseId));
			$select->columns(array('studentCount'	=>	new \Zend\Db\Sql\Expression("COUNT(user_id)")));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$count = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$count = $count->toArray();
			return $count[0]['studentCount'];
		}catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getContentName($subjectId, $chapterId) {
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('c' => 'content'))->where(array('chapterId' => $chapterId, 'published' => 1))->order('c.weight ASC, c.id ASC')
			->columns(array('lectureName' => 'title', 'demo'));
			$select->join(array('f' => 'files'), 'c.id=f.contentId', array('lectureType' => 'type', 'metastuff' => 'stuff', 'meta' => 'metadata', 'duration' => 'duration'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$content = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$content = $content->toArray();
			if(count($content) > 0)
				return $content;
			else
				return false;
		}catch(Exception $e) {
			return false;
		}
	}

	public function otherCoursesbyProfessor($data) {
		$result = new stdClass();
		try {
			$start = $data->start;
			$startQry = "";
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			if (isset($data->courseId) && !empty($data->courseId)) {
				$query = "SELECT * FROM courses AS c WHERE c.`ownerId`=$data->instituteId AND c.`id`!=$data->courseId AND c.`deleted`=0 AND c.`approved`=1 AND c.`availStudentMarket`=1 AND c.`liveForStudent`=1";
			} else {
				$query = "SELECT * FROM courses AS c WHERE c.`ownerId`=$data->instituteId AND c.`slug`!='{$data->slug}' AND c.`deleted`=0 AND c.`approved`=1 AND c.`availStudentMarket`=1 AND c.`liveForStudent`=1";
			}
			$courseDetails = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			if($courseDetails->count() !== 0) {
				$result->noOfCourses = $courseDetails->count();
			}
			if(!empty($start)) {$startQry = "AND c.`id`>$start";}
			if (isset($data->courseId) && !empty($data->courseId)) {
				$query = "SELECT * FROM courses AS c WHERE c.`ownerId`=$data->instituteId AND c.`id`!=$data->courseId AND c.`deleted`=0 AND c.`approved`=1 AND c.`availStudentMarket`=1 AND c.`liveForStudent`=1 $startQry ORDER BY c.`id` LIMIT 10";
			} else {
				$query = "SELECT * FROM courses AS c WHERE c.`ownerId`=$data->instituteId AND c.`slug`!='{$data->slug}' AND c.`deleted`=0 AND c.`approved`=1 AND c.`availStudentMarket`=1 AND c.`liveForStudent`=1 $startQry ORDER BY c.`id` LIMIT 10";
			}
			$courseDetails = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			if($courseDetails->count() > 0) {

				//adding rating with course details
				require_once 'Student.php';
				$s = new Student();

				$courses = $courseDetails->toArray();
				foreach ($courses as $key => $course) {
					if($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
						require_once 'amazonRead.php';
						$courses[$key]['image'] = getSignedURL($courses[$key]['image']);
					} else {
						$courses[$key]['image'] = $this->sitePath."manage/assets/pages/img/background/32.jpg";
					}
					$courses[$key]['rating'] = $s->getCourseRatings($courses[$key]['id']);
				}
			} else {
				$result->status = 0;
				$result->exception = "Courses not found";
				return $result;
			}
			$result->courses = $courses;
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

}

?>