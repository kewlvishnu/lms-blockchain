<?php

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "Base.php";

//$adapter = require "adapter.php";
class Parents extends Base {

	public function getParentNotifications($data) {
		$result = new stdClass();
		try {
			//var_dump($data);
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$query="SELECT pn.parentId,pn.message, CONCAT( pd.firstName,' ',pd.lastName) as instructor, pd.profilePic, c.name as courseName, s.name as subjectName
					FROM `parent_notifications` AS pn
					INNER JOIN `professor_details` AS pd ON pd.userId=pn.professorId
					INNER JOIN `courses` AS c ON c.id=pn.courseId
					INNER JOIN `subjects` AS s ON s.id=pn.subjectId
					WHERE pn.parentId={$data->userId} AND pn.status=0";
			$parentNotifications = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$parentNotifications = $parentNotifications->toArray();
			foreach ($parentNotifications as $key => $parentNotification) {
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){
					require_once 'amazonRead.php';
					$parentNotifications[$key]['profilePic'] = stripcslashes(getSignedURL($parentNotifications[$key]['profilePic']));
				} else {
					$parentNotifications[$key]['profilePic'] = 'assets/layouts/layout2/img/avatar3_small.jpg';
				}
			}
			$result->status = 1;
			$result->parentNotifications = $parentNotifications;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
}

?>
