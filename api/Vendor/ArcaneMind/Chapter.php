<?php
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "Base.php";


class Chapter extends Base {
	
	public function createChapter($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			// Need to check if subject is owned by user
			
			//checking if chapter name is duplicate
			$result = $this->checkDuplicateChapter($data);
			if($result->available == false) {
				$result->status = 2;
				$result->message = 'Please select a different name as you have already created a chapter by this name';
				return $result;
			}
			//checking if course is not expired
			require_once 'Subject.php';
			$s = new Subject();
			if($s->subjectCourseExpired($data)) {
				$result->status = 0;
				$result->message = 'Your course has expired. You can not add more Chapters';
				return $result;
			}
			//selecting maximum weight for chapter numbering
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('chapters');
			$select->where(array(
				'subjectId'	=>	$data->subjectId,
				'deleted'	=>	0
			));
			$select->columns(array(
					'max'	=>	new \Zend\Db\Sql\Expression("MAX(weight)")
			));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$max = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$max = $max->toArray();
			if(count($max) >= 0) {
				if($max[0]['max']!=NULL ){
				$max = $max[0]['max'];
				
				}else{
				   $max = -1; 
				}
			}
			else
				$max = -1;
			$chapters = new TableGateway('chapters', $adapter, null,new HydratingResultSet());
			// checking that
			// chapter is created by proffesor or not.
			// professor request for crating chapter is coming from add chapter page in  professor folder.
			$createdby=0;
			if(isset($data->createdby)){
				$createdby=$data->userId;
			}
			if(!isset($data->chapterDesc))
				$data->chapterDesc = '';
			$insert = array(
				'subjectId'			=> 	$data->subjectId,
				'name' 				=>	$data->chapterName,
				'description'		=>	'',
				'demo'		 		=>	$data->demo,
				'weight'			=>	$max + 1,
				'deleted'			=>	0,
				'createdBy'			=>	$createdby
			);
			$chapters->insert($insert);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$data->chapterId = $chapters->getLastInsertValue();
			if($data->chapterId == 0) {
				$result->status = 0;
				$result->message = "Some error occured";
				return $result;
			}

			$result->chapterId = $data->chapterId;
			$result->status = 1;
			$result->message = 'Chapter created!';
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function userOwnsChapter($data){
		$result = new stdClass();
		$adapter = $this->adapter;
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('chapters');
		$where = new \Zend\Db\Sql\Where();
		$where->equalTo('id', $data->chapterId);
		$select->where($where);
		$selectString = $sql->getSqlStringForSqlObject($select);
		$chapter = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$chapter = $chapter->toArray();
		if(count($chapter) == 0)
			return false;
		return true;	
	}
	
	public function getChapterDetails($data){
		try {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('chapters');
			$select->where(array('subjectId' => $data->subjectId, 'id' => $data->chapterId, 'deleted' => 0));
			$statement = $sql->prepareStatementForSqlObject($select);
			$chapters = $statement->execute();
			if($chapters->count() == 0) {
				$result->status = 0;
				$result->message = "Chapter not found";
			}
			$result->tempChapterDetails = $chapters->next();
			$result->status  = 1;
						  $result->chapterDetails = new stdClass();
			$result->chapterDetails->id = $data->chapterId;
			$result->chapterDetails->name = $result->tempChapterDetails['name'];
			//$result->chapterDetails->number = $this->getChapterNumber($data);
			
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function getChapterDetailsForView($data){
		try {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('c' => 'chapters'))
			->join(array('s'	=>	'subjects'),
					'c.subjectId = s.id',
					array(
						'courseId'	=>	'courseId'
						)
			);
			$select->where(array('c.subjectId' => $data->subjectId, 'c.deleted' => 0));
			$select->columns(array('id', 'name'));
			$statement = $sql->getSqlStringForSqlObject($select);
			$chapters = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
			if($chapters->count() == 0) {
				$result->status = 0;
				$result->message = "Chapter not found";
			}
			$result->chapters = $chapters->toArray();
			$result->status  = 1;				
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	
	/*public function getChapterNumber($data) {
		try {
			$result = new stdClass();
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('chapters');
			$select->where(array('subjectId' => $data->subjectId))
					->order('weight ASC')
					->order('id DESC');
			$selectString = $sql->getSqlStringForSqlObject($select);
			$chapters = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$chapters = $chapters->toArray();
			$chapterNum = 1;
			foreach($chapters as $c){
				if($c['id'] == $data->chapterId){
					return $chapterNum;
				}
				$chapterNum++;
			}
			return 0;
		} catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}*/
	public function getChaptersForExam($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			if(!isset($data->subjectId)) {
				$select = $sql->select();
				$select->from('exams');
				$select->where(array('id' => $data->examId));
				$select->columns(array('subjectId'));
				$string = $sql->getSqlStringForSqlObject($select);
				$subjectId = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
				$subjectId = $subjectId->toArray();
				if(count($subjectId) > 0) {
					$data->subjectId = $subjectId[0]['subjectId'];
				}
			}
			$select = $sql->select();
			$select->from('chapters');
			if(isset($data->import) && $data->import == true)
				$select->where(array('subjectId'	=>	$data->subjectId))
						->order('weight ASC')
						->order('id DESC');
			else
				$select->where(array('subjectId'	=>	$data->subjectId,
							  'deleted'	=>	0))
						->order('weight ASC')
						->order('id DESC');
			$select->columns(array('id','name'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$chapters = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result->chapters = $chapters->toArray();
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	
	//function to get breadcrumbs for the chapter content view page
	public function getBreadcrumbForChapter($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = "SELECT c.id AS chapterId, s.id AS subjectId, co.id AS courseId, c.name AS chapterName, s.name AS subjectName, co.name AS courseName FROM chapters AS c JOIN subjects AS s ON s.id=c.subjectId JOIN courses AS co ON co.id = s.courseId WHERE c.subjectId={$data->subjectId}";
			$breadcrumb = $adapter->query($select, $adapter::QUERY_MODE_EXECUTE);
			$breadcrumb = $breadcrumb->toArray();
			$result->breadcrumb = $breadcrumb[0];
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	//function to delete chapter
	public function deleteChapter($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			$subject = $this->getCourseDetailsbyChapter($data->chapterId);
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('chapters');
			$update->set(array('deleted'	=>	1));
			$update->where(array('id'	=>	$data->chapterId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$statement->execute();
			$sql = new Sql($adapter);
			$update = $sql->update();
			$update->table('exams');
			$update->set(array('chapterId'	=>	0));
			$update->where(array('chapterId'	=>	$data->chapterId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$statement->execute();
			$course = $subject->subject;
			$chapterId = $data->chapterId;
			$chapter = $course[0]['chapter'];
			$courseId = $course[0]['courseId'];
			$coursename = $course[0]['course'];
			$subjectname = $course[0]['subject'];
			$subjectId = $course[0]['subjectId'];
			$institute = $course[0]['institute'];
			$institutemail = $course[0]['institutemail'];
			$instituteId = $course[0]['instituteId'];

			// insert notification
			$notification="chapter ".$this->safeString($chapter)." (chapted id : $chapterId ) belonging to subject ".$this->safeString($subjectname)." (subject id : $subjectId ) of Course ".$this->safeString($coursename)." ( course id : $courseId ) has been deleted. Please contact admin@integro.io for restoring the Chapter.";
			$query = "INSERT into notifications (userId,message) values ($instituteId,'$notification')";
			$notification=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();

			require_once "User.php";
			$c = new User();
			$instituteSubject = "Chapter deletion Notification";
			$emailInstitute = "Dear $institute, <br> <br>";
			$emailInstitute .= "There has been a request for deletion of the Chapter $chapter (Chapter ID: $chapterId) belonging to  Subject $subjectname (Subject ID: $subjectId) of Course  $coursename ( Course ID: $courseId ) on <a href=integro.io >Integro.io <a>  <br> <br>";

			 $emailInstitute .= "Please write to us at <a href=admin@integro.io >admin@integro.io <a> if you wish to restore the Chapter  or if you did not initiate the request.<br> <br> "
					. "Please mention your Institute ID, Chapter Name & Chapter ID in the email <br> <br>";

			$emailInstitute .= "<table border=1 ><thead><tr><th> Institute Name</th> <th>Institute ID</th> <th>Deleted Course Name</th> <th> Deleted Course Id</th><th>Deleted Chapter  Name</th> <th>Deleted Chapter Id</th></tr></thead>";
			$emailInstitute .= "<tbody><tr><td> $institute</td><td>$instituteId</td> <td>$coursename</td><td> $courseId</td><td> $chapter</td><td> $chapterId</td> </tr></tbody></table> ";

			if($_SERVER['REMOTE_ADDR'] <> '127.0.0.1' && $_SERVER['REMOTE_ADDR'] <> '::1') {
				$c->sendMail($institutemail, $instituteSubject, $emailInstitute);
			}
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function getAlldeletedChapter($data) {
		$result = new stdClass();
		$adapter = $this->adapter;
		//$sql = new Sql($adapter);
		try {
			$query = "SELECT cp.id chapterid,cp.name chapter, s.id subjectid ,s.name subject,CONCAT( c.name,'(C00',c.id,')') course,l.id instituteid  ,l.email,u.contactMobile ,
			   	case 
				when ur.roleId= 1 then (select name from institute_details where userId= l.id)
				when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
				when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
				end as username
				from chapters cp JOIN subjects s on s.id=cp.subjectId
				join courses c on c.id=s.courseId
				JOIN login_details l on l.id=c.ownerId   Join user_roles  ur on ur.userId=l.id
				JOIN user_details  u on l.id=u.userId
				where cp.deleted=1 and  ur.roleId not in (4,5) order by l.id ";
			$chapters = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->chapters = $chapters->toArray();
			if (count($result->chapters) == 0) {
			$result->status = 0;
			$result->exception = "No deleted chapters Found";
			return $result;
		}
		$result->status = 1;
		return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function restoreChapterByAdmin($req) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();

		$result = new stdClass();

        try {
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('chapters');
			$update->set(array('deleted' => 0));
			$update->where(array('id' => $req->chapterId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$result = $statement->execute();
						$subject = $this->getCourseDetailsbyChapter($req->chapterId);

			$course = $subject->subject;
			$chapterId = $data->chapterId;
			$chapter = $course[0]['chapter'];
			$courseId = $course[0]['courseId'];
			$coursename = $course[0]['course'];
			$subjectname = $course[0]['subject'];
			$subjectId = $course[0]['subjectId'];
			$institute = $course[0]['institute'];
			$institutemail = $course[0]['institutemail'];
			$instituteId = $course[0]['instituteId'];


			// insert notification
			$notification="chapter ".$this->safeString($chapter)." (chapted id : $chapterId ) belonging to subject ".$this->safeString($subjectname)." (subject id : $subjectId ) of Course ".$this->safeString($coursename)." ( course id : $courseId )  has been restored.";
			$query = "Insert into notifications (userId,message) values ($instituteId,'$notification')";
			$notification=$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = "chapter is restored";
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function updateChapterDetails($req) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();

		$result = new stdClass();

        try {
			$adapter = $this->adapter;
			$sql = new Sql($this->adapter);
			$update = $sql->update();
			$update->table('chapters');
			$update->set(array('name' => $req->chapterName, 'demo' => $req->demo));
			$update->where(array('id' => $req->chapterId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$result = $statement->execute();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			$result->status = 1;
			$result->message = "chapter is updated";
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}

	public function getCourseDetailsbyChapter($chapterId) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$query = "SELECT ch.name chapter ,s.name subject,s.id subjectId, c.name course, c.id courseId, l.email institutemail,l.id instituteId,
				case 
				when ur.roleId= 1 then (select name from institute_details where userId= l.id)
					when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId= l.id)
					when ur.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId= l.id)
					end as institute
				from chapters ch join  subjects s on s.id=ch.subjectId
				JOIN courses c on c.id=s.courseId
				JOIN login_details l on c.ownerId=l.id
				JOIN user_roles  ur on ur.userId=l.id where ch.id=$chapterId and ur.roleId not in (4,5)";
			$subject = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->subject = $subject->toArray();

			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function checkAssigenedSubjectContent($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$query = "SELECT id from subjects s left join subject_professors sp on sp.subjectId=s.id where ( s.ownerId=$data->userId or sp.professorId=$data->userId) and s.id=$data->subjectId";
			//die("dggg");
			$subject = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$result->subject = $subject->toArray();
			// die();
			if (count($result->subject) > 0) {
				$query = "SELECT s.id from subjects s join courses c  on c.id=s.courseId where  s.id=$data->subjectId and c.id=$data->courseId";
				$checkCourse = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$result->course = $checkCourse->toArray();
					if (count($result->course) > 0) {
					$query = "SELECT c.id from chapters c join subjects s  on c.subjectId=s.id where  s.id=$data->subjectId and c.id=$data->chapterId";
					$checkChapter = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$result->chapter = $checkChapter->toArray();
					 if (count($result->chapter) > 0) {
						  return true;
					   }
						return false;
				}
				return false;
		   }
			return false;
		} catch (Exception $e) {
			$result->status = 0;
			$result->exception = $e->getMessage();
			return false;
		}
	}
	public function chapterIsFromSubject($data) {
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('chapters');
			$select->where(array(
				'subjectId'	=>	$data->subjectId,
				'id'		=>	$data->chapterId
			));
			$select->columns(array('id'));
			$string = $sql->getSqlStringForSqlObject($select);
			$owns = $adapter->query($string, $adapter::QUERY_MODE_EXECUTE);
			$owns = $owns->toArray();
			if(count($owns) == 0)
				return false;
			else
				return true;
		} catch(Exception $e) {
			return false;
		}
	}
	
	public function checkDuplicateChapter($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$name = trim(strtolower($data->chapterName));
			if(isset($data->chapterId)) {
				$selectString = "SELECT name FROM chapters WHERE subjectId={$data->subjectId} AND TRIM(LCASE(name))='".$this->safeString($name)."' AND id!='{$data->chapterId}' and deleted=0";
			}
			else {
				$selectString = "SELECT name FROM chapters WHERE subjectId={$data->subjectId} AND TRIM(LCASE(name))='".$this->safeString($name)."' AND deleted=0";
			}
			$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$temp = $temp->toArray();
			if(count($temp) == 0)
				$result->available = true;
			else
				$result->available = false;
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	
	public function isFreeChapter($data) {
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from('chapters');
			@$select->where(array('id'	=>	$data->chapterId,'demo'	=>	1));
			$select->columns(array('id'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$temp = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$temp = $temp->toArray();
			if(count($temp) > 0)
				return true;
			else
				return false;
		}catch(Exception $e) {
			return false;
		}
	}
}
?>