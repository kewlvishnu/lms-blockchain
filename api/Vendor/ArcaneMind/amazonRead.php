<?php
	function rsa_sha1_sign($policy, $private_key_filename) {
		$signature = "";
	
		// load the private key
		$fp = fopen($private_key_filename, "r");
		$priv_key = fread($fp, 8192);
		fclose($fp);
		$pkeyid = openssl_get_privatekey($priv_key);
	
		// compute signature
		openssl_sign($policy, $signature, $pkeyid);
	
		// free the key from memory
		openssl_free_key($pkeyid);
	
		return $signature;
	}
	
	function url_safe_base64_encode($value) {
		$encoded = base64_encode($value);
		// replace unsafe characters +, = and / with the safe characters -, _ and ~
		return str_replace(
			array('+', '=', '/'),
			array('-', '_', '~'),
			$encoded);
	}
	
	function create_stream_name($stream, $policy, $signature, $key_pair_id, $expires) {
		$result = $stream;
		// if the stream already contains query parameters, attach the new query parameters to the end
		// otherwise, add the query parameters
		$separator = strpos($stream, '?') == FALSE ? '?' : '&';
		// the presence of an expires time means we're using a canned policy
		if($expires) {
			if (isset($path) && !empty($path)) {
				$result .= $path . $separator . "Expires=" . $expires . "&Signature=" . $signature . "&Key-Pair-Id=" . $key_pair_id;
			} else {
				$result .= $separator . "Expires=" . $expires . "&Signature=" . $signature . "&Key-Pair-Id=" . $key_pair_id;
			}
		} 
		// not using a canned policy, include the policy itself in the stream name
		else {
			if (isset($path) && !empty($path)) {
				$result .= $path . $separator . "Policy=" . $policy . "&Signature=" . $signature . "&Key-Pair-Id=" . $key_pair_id;
			} else {
				$result .= $separator . "Policy=" . $policy . "&Signature=" . $signature . "&Key-Pair-Id=" . $key_pair_id;
			}
		}
	
		// new lines would break us, so remove them
		return str_replace('\n', '', $result);
	}
	
	function encode_query_params($stream_name) {
		// the adobe flash player has trouble with query parameters being passed into it, 
		// so replace the bad characters with their url-encoded forms
		return str_replace(
			array('?', '=', '&'),
			array('%3F', '%3D', '%26'),
			$stream_name);
	}
	
	function get_canned_policy_stream_name($video_path, $private_key_filename, $key_pair_id, $expires) {
		// this policy is well known by CloudFront, but you still need to sign it, since it contains your parameters
		$canned_policy = '{"Statement":[{"Resource":"' . $video_path . '","Condition":{"DateLessThan":{"AWS:EpochTime":'. $expires . '}}}]}';
		// the policy contains characters that cannot be part of a URL, so we base64 encode it
		$encoded_policy = url_safe_base64_encode($canned_policy);
		// sign the original policy, not the encoded version
		$signature = rsa_sha1_sign($canned_policy, $private_key_filename);
		// make the signature safe to be included in a url
		$encoded_signature = url_safe_base64_encode($signature);
	
		// combine the above into a stream name
		$stream_name = create_stream_name($video_path, null, $encoded_signature, $key_pair_id, $expires);
		// url-encode the query string characters to work around a flash player bug
		//return encode_query_params($stream_name);
		return $stream_name;
	}
	
	function get_custom_policy_stream_name($video_path, $private_key_filename, $key_pair_id, $policy) {
		// the policy contains characters that cannot be part of a URL, so we base64 encode it
		$encoded_policy = url_safe_base64_encode($policy);
		// sign the original policy, not the encoded version
		$signature = rsa_sha1_sign($policy, $private_key_filename);
		// make the signature safe to be included in a url
		$encoded_signature = url_safe_base64_encode($signature);
	
		// combine the above into a stream name
		$stream_name = create_stream_name($video_path, $encoded_policy, $encoded_signature, $key_pair_id, null);
		// url-encode the query string characters to work around a flash player bug
		return encode_query_params($stream_name);
	}
	
	function getSignedURL($file, $time = 300) {
		// Path to your private key.  Be very careful that this file is not accessible
		// from the web!
		//$time = $time || 300;
		if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1' && $_SERVER['REMOTE_ADDR']<>'172.18.0.1'){
			$private_key_filename = '/var/www/keys/pk-APKAJZKNEI52OD3GWI4Q.pem';
			//$private_key_filename = 'pk-APKAJZKNEI52OD3GWI4Q.pem';
			$key_pair_id = 'APKAJZKNEI52OD3GWI4Q';
			
			$fileURL = $file;
			
			$expires = time() + $time; // 5 min from now
			return $canned_policy_stream_name = get_canned_policy_stream_name($fileURL, $private_key_filename, $key_pair_id, $expires);
		} else {
			return 'https://picsum.photos/200/300';
		}
	
	}
?>