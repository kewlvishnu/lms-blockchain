<?php

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Mail;
use Zend\Mime\Message;
use Zend\Mime\Part;

class Portfolio {

	public $id;
	public $role;
	public $adapter;

	function __construct() {
		$config = require "Conf.php";
		$this->adapter = new Zend\Db\Adapter\Adapter($config);
	}

	public function portfolioLogin($req) {

		$db = $this->adapter->getDriver()->getConnection();

		$result = new stdClass();
		try {
			$req->username=  strtolower($req->username);
	        if (!isset($req->remember)) {
	            $req->remember = 0;
	        }
	        if (!isset($req->portfolio)) {
	            $req->portfolio = 0;
	        }
	        $adapter = $this->adapter;
	        $sql = new Sql($adapter);
	        $select = $sql->select();
	        //Login Details
	        $select->from('login_details');
	        $select->columns(array('id', 'validated', 'active', 'temp'));
	          $select->where
	                ->nest
	                ->equalTo('username', $req->username)
	                ->or
	                ->equalTo('email', $req->username)
	                ->unnest
	                ->and
	                ->equalTo('password', $req->hashedPassword);
	        $selectString = $sql->getSqlStringForSqlObject($select);
	        $loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
	        $result = new stdClass();
	        if ($loginDetails->count() == 0) {
	            $result->status = 0;
	            $result->valid = false;
				$result->portfolioValid=false;
	            $result->reason = "Invalid Credentials.";
	            return $result;
	        }
	        $result->userIds = $loginDetails->toArray();
	        $temp = array();
	        $validationStatus = array();
	        foreach ($result->userIds as $user) {
	            $temp[] = $user['id'];
	            $validationStatus[$user['id']] = $user['validated'];
	            $activeStatus[$user['id']] = $user['active'];
	            $tempStatus[$user['id']] = $user['temp'];
	        }

	        $result->userIds = $temp;
	        $select = $sql->select();
	        //user roles
	        $select->from('user_roles');
	        $select->columns(array('userId', 'roleId'));
	        $select->where(array('roleId' => $req->userRole));
	        $select->where
	                ->equalTo('roleId', $req->userRole)
	                ->and
	                ->in('userId', $result->userIds);
	        $statement = $sql->prepareStatementForSqlObject($select);
	        $loginDetails = $statement->execute();
	        if ($loginDetails->count() == 0) {
	            $result->status = 0;
	            $result->valid = false;
	            $result->reason = "Invalid Role.";
	            return $result;
	        }
	        $loginDetails->userRole = $loginDetails->next();

	        $result->userRole = $loginDetails->userRole['roleId'];
	        $result->userId = $loginDetails->userRole['userId'];
	        unset($result->userIds);
	        if ($tempStatus[$result->userId] == 1) {
	            $result->temp = 1;
	        } else {
	            $result->temp = 0;
	        }
	        if ($validationStatus[$result->userId] == 0) {
	            /*unset($result->userRole);
	            unset($result->userId);*/
	            $result->valid = false;
				$result->portfolioValid=false;
	            $result->reason = "Account not verified.";
				$result->reset = 1;
	            return $result;
	        }
	        if ($activeStatus[$result->userId] == 0) {
	            unset($result->userRole);
	            unset($result->userId);
	            $result->valid = false;
				$result->portfolioValid=false;
	            $result->reason = "You are banned from our website.";
	            return $result;
	        }

			//now setting username in session for performance improvement ARC-360
			$result->username = $req->username;
			$result->valid = true;
			// return $result;
			
			if($result->valid == false)
				return $result;
			$req->userId = $result->userId;
			$req->userRole = $result->userRole;
			// if response is true till here
			if($req->userRole == 1){
				$selectString ="SELECT status
								FROM  pf_portfolio
								WHERE subdomain ='{$req->slug}' AND userId = '{$req->userId}'";
				$subdomains = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				if ($subdomains->count() > 0) {
					$subdomains = $subdomains->toArray();
					$status     = $subdomains[0]['status'];
					$result->portfolioStatus=$status;
					$result->portfolioValid=true;
				} else {
					$result->portfolioValid=false;
					$result->reason = "Only portfolio owners can log in ";
					return $result;
				}
			}
			elseif($req->userRole == 2){
				$selectString ="SELECT pp.status
								FROM  pf_portfolio as pp
								INNER JOIN pf_main_courses as pc ON pc.portfolioId=pp.id
								INNER JOIN subjects as s ON s.courseId=pc.courseId AND s.deleted=0
								INNER JOIN subject_professors AS sp ON sp.subjectid=s.id
								WHERE subdomain ='{$req->slug}' AND sp.professorId IN ({$req->userId})
								GROUP BY sp.professorId";
				$subdomains = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				if ($subdomains->count() > 0) {
					$subdomains = $subdomains->toArray();
					$status     = $subdomains[0]['status'];
					$result->portfolioStatus=$status;
					$result->portfolioValid=true;
					$result->query = $selectString;
				} else {
					$result->portfolioValid=false;
					$result->reason = "Only portfolio owners can log in ";
					return $result;
				}
			}
			elseif($req->userRole == 4)
			{
				//fetching out courses of Portfolio
				$selectString ="SELECT pc.courseId
								FROM pf_main_courses pc
								LEFT JOIN pf_portfolio pp ON pp.id=pc.portfolioId
								WHERE pp.subdomain='{$req->slug}' AND pp.status='Published'";
				$subdomainsCourses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				if($subdomainsCourses->count() > 0)
				{
					$subdomainsCourses = $subdomainsCourses->toArray();
					$pCourses = array();
					foreach ($subdomainsCourses as $key => $course) {
					    $pCourses[] = $course['courseId'];
					}
					//$pCourses= $subdomainsCourses[0]['courses'];
					$pCourses = implode(",", $pCourses);
					$selectString1 ="SELECT id FROM student_course WHERE user_id='{$req->userId}' AND course_id IN ({$pCourses}) ";
					$subdomainsEnrolled = $adapter->query($selectString1, $adapter::QUERY_MODE_EXECUTE);
					if($subdomainsEnrolled->count() > 0)
					{
						$result->portfolioValid=true;
					} else {
						$result->portfolioValid=false;
						$result->reason = "Not Authorized as no active courses in subdomain";
						return $result;
					}
					 
				}else{
					$result->portfolioValid=false;
					$result->reason = "Not Authorized as no active courses in subdomain";
					return $result;
				}
			}
			else{
				//for super Admin not needed right now
				
			}

			if ($req->userRole == 4) {
				require_once("Student.php");
				$s = new Student();
				$data = new stdClass();
				$data->userId = $req->userId;
				$data->userRole = $req->userRole;
				$studentDetails = $s->getBasicStudentDetails($data);
				if ($studentDetails->status == 1) {
					$result->details = $studentDetails->studentDetails;
				}
			} else {
				$result->details = array();
			}

			$userUpdateDetails = array('lastLogin' => time());
			if($req->remember == 1) {
				//$ip     = $_SERVER[REMOTE_ADDR];
				//$random = md5(uniqid(rand(), true));
				$cookie = serialize(array(base64_encode($result->userId)));
				setcookie('mtwebLogin', $cookie, time() + 31104000, '/');
				$userUpdateDetails['cookie'] = $cookie;
			}
			$db->beginTransaction();

			$update = $sql->update();
			$update->table('login_details');
			$update->set($userUpdateDetails);
			$update->where(array('id' => $result->userId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();
			return $result;
	    } catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
    }

	public function getPortfolioApprovals() {
		$adapter = $this->adapter;
		$selectString ="SELECT pf.id, pf.userId, ld.email, ud.contactMobile AS `mobile`, urd.name AS `userType`, pf.subdomain, pf.name AS `portfolioName`, pf.description AS `portfolioDesc`, pf.status,
						case
							when ur.roleId= 1 then (select name from institute_details where userId=ld.id)
							when ur.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId=ld.id)
						else 'NA' end as name
						FROM `pf_portfolio` AS pf
						INNER JOIN `login_details` AS ld ON ld.id=pf.userId
						INNER JOIN `user_details` AS ud ON ud.userId=pf.userId
						INNER JOIN `user_roles` AS ur ON ur.userId=pf.userId
						INNER JOIN `user_role_details` AS urd ON urd.id=ur.roleId
						WHERE pf.`status`='Waiting for Approval'
						ORDER BY pf.id";
		$portfolioDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$result = new stdClass();
		$result->portfolios = $portfolioDetails->toArray();
		$result->valid = true;
		return $result;
	}
	public function savePortfolioApproval($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$result = new stdClass();
		try {
			$adapter 	 = $this->adapter;
			$sql 		 = new Sql($adapter);
			$adapter 	 = $this->adapter;
			$portfolioId = $data->pfId;
			$reason 	 = '';
			if(isset($data->inputReason) && !empty($data->inputReason)) {
				$reason  = $data->inputReason;
			}
			$approval	 = (($data->selectApproval=='approve')?'Draft':(($data->selectApproval=='reject')?'Disapproved':''));
			if (empty($portfolioId)) {
				$result->status = 0;
				$result->message = "Invalid portfolio";
				return $result;
			} else if (empty($approval)) {
				$result->status = 0;
				$result->message = "Invalid approval";
				return $result;
			} else {
				$db->beginTransaction();
				//updating new cover pic in database
				$update = $sql->update();
				$update->table('pf_portfolio');
				$update->set(
					array('status' => $approval, 'reason' => $reason)
				);
				$update->where(array('id' => $portfolioId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$count = $statement->execute()->getAffectedRows();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
			}
			$result->status = 1;
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function savePortfolioCourse($data) {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$selectString ="SELECT * FROM `pf_portfolio` WHERE `subdomain`='{$data->subdomain}' AND `userId`={$data->userId}";
			$portfolioDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			if($portfolioDetails->count() > 0) {
				$portfolios	 = $portfolioDetails->toArray();
				$portfolio	 = $portfolios[0];
				$portfolioId = $portfolio['id'];
				$courseId	 = $data->course;
				$optionCourse= $data->optionCourse;
				if ($optionCourse>0) {
					$selectString ="SELECT * FROM `pf_main_courses` WHERE `portfolioId`='{$portfolioId}' AND `courseId`={$courseId}";
					$portfolioCourseDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					if($portfolioCourseDetails->count() == 0) {
						$pf_main_courses = new TableGateway('pf_main_courses', $this->adapter, null, new HydratingResultSet());
						$pf_main_courses->insert(array(
							'portfolioId' => $portfolioId,
							'courseId' => $courseId
						));
					}
				} else {
					$selectString ="SELECT * FROM `pf_main_courses` WHERE `portfolioId`='{$portfolioId}' AND `courseId`={$courseId}";
					$portfolioCourseDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					if($portfolioCourseDetails->count() > 0) {
						$sql = new Sql($adapter);
						$delete = $sql->delete();
						//Login Details
						$delete->from('pf_main_courses');
						$delete->where(array('portfolioId' => $portfolioId, 'courseId' => $courseId));
						$statement = $sql->prepareStatementForSqlObject($delete);
						$portfolioCourses = $statement->execute();
					}
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
			} else {
				$result->status = 0;
			}
			return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
}

?>