<?php
	use Zend\Db\TableGateway\TableGateway;
	use Zend\Db\Sql\Select;
	use Zend\Db\Sql\Sql;
	use Zend\Db\ResultSet\HydratingResultSet;
	
	require_once "User.php";
	
	
	
	class Publisher extends User {
		public $id;
		public $role;
		
		function __construct() {
			parent::__construct();
			$this->id = 0;
			$this->role = NULL;
		}
		
		public function register($data) {
			$result = parent::register($data);
			if($result->status == 0) {
				return $result;
			}
			$data->userId = $result->userId;
			$result = parent::addBasicDetails($data);
			if($result->status == 0) {
				return $result;
			}
			
			$this->addPublisherProfileDetails($data);
			
			$this->addUserInstituteTypes($data);
			
			
			$result = parent::sendVerificationEmail($data);
			if($result->status == 0 ){
				return $result;
			}
			
			$result = new stdClass();
			$result->status = 1;
			$result->userId = $data->userId;
			return $result;
		}
		
		public function addPublisherProfileDetails($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			try {
				$instituteDetails = new TableGateway('publisher_details', $this->adapter, null,new HydratingResultSet());
				$instituteDetails->insert(array(
					'userId' 					=> 	$data->userId,
					'firstName' 			=>	$data->firstName,
					'lastName' 			=>	$data->lastName,
					'gender' => $data->gender,
					'dob' => $data->dob,
					// 'coverPic'			 	=>	$data->coverPic,
					// 'profilePic' 			=>	$data->profilePic,
					// 'altContact' 			=>	$data->altContact,
					// 'altEmail' 				=>	$data->altEmail,
					// 'foundedIn' 			=>	$data->foundedIn,
					// 'description' 		=>	$data->description
				));
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
			} catch (Exception $e) {
				$db->rollBack();
			}
		}
		
		/*
		*	$data 										: Object
		* $data->userId 						:	Int
		*	$data->newInstituteTypes	: Array
		*/
		
		public function addUserInstituteTypes($data) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			try {
				$userInstituteTypes = new TableGateway('user_institute_types', $this->adapter, null,new HydratingResultSet());
				foreach($data->stucturedInsTypes as $id=>$instituteData){
					if($id == -1)
						continue;
					
					$instituteJsonData = Zend\Json\Json::encode($instituteData, true);
					$userInstituteTypes->insert(array(
						'userId' 					=> $data->userId,
						'instituteTypeId'	=> $id,
						'data' => $instituteJsonData
					));
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
			} catch (Exception $e) {
				$db->rollBack();
			}
		}
		
		public function addNewInstituteSubtypes($data){

			$db = $this->adapter->getDriver()->getConnection();

			//$db->beginTransaction();
			try {
				$instituteSubtypes = new TableGateway('institute_subtypes', $this->adapter, null,new HydratingResultSet());
				$result->newInstituteSubtypes = array();
				foreach($data->newInstituteSubtypes as $instituteSubtype){
					$instituteSubtypes->insert(array(
						'name' 						=> $instituteSubtype->name,
						'instituteTypeId' => $instituteSubtype->parentId,
						'addedBy' 				=> $data->userId,
						'createdAt' 			=> time(),
					));
					
					$newInstituteSubtype = new stdClass();
					$newInstituteSubtype->id = $instituteSubtypes->getLastInsertValue();
					$newInstituteSubtype->data = $instituteSubtype->data;
					
					$result->newInstituteSubtypes[] = $instituteSubtype;
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				//$db->commit();
				$result->status = 1;
				return $result;
			}catch(Exception $e){
				//$db->rollBack();
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
		
		public function addUserInstituteSubtypes($data){

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();
			try {
				$userInstituteSubtypes = new TableGateway('user_institute_subtypes', $this->adapter, null,new HydratingResultSet());
				foreach($data->userInstituteSubtypes as $instituteSubtype){
					$userInstituteSubtypes->insert(array(
						'userId' 							=> $data->userId,
						'instituteSubtypeId'	=> $instituteSubtype->id,
						'data'								=> $instituteSubtype->data,
					));
				}
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
			}catch(Exception $e){
				$db->rollBack();
			}
		}
	
		public function getProfileDetails($data) {
			try {
				$result = new stdClass();
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				//Login Details
				$select->from('login_details');
				$select->columns(array('id','email', 'username'));
				
				$select->where(array('id' => $data->userId));
				$statement = $sql->prepareStatementForSqlObject($select);
				$loginDetails = $statement->execute();
				
				if($loginDetails->count() !== 0) {
					$result->loginDetails = $loginDetails->next();
				} else {
					$result->status = 0;
					$result->exception = "Login details not found";
					return $result;
				}
				
				// Basic details
				$select = $sql->select();
				$select->from(array('u' => 'user_details'))
						->join(array('c' => 'countries'),
								'u.addressCountry = c.id',
								array('addressCountry' => 'name'));
				$select->columns(array('contactMobilePrefix' => 'contactMobilePrefix', 
										'contactMobile' => 'contactMobile', 
										'contactLandlinePrefix' => 'contactLandlinePrefix',
										'contactLandline' => 'contactLandline', 
										'addressStreet' => 'addressStreet', 
										'addressCity' => 'addressCity', 
										'addressState' => 'addressState', 
										'addressPin' => 'addressPin'));
				
				$select->where(array('u.userId' => $data->userId));
				$statement = $sql->prepareStatementForSqlObject($select);
				$userDetails = $statement->execute();
				
				if($userDetails->count() !== 0) {
					$result->userDetails = $userDetails->next();
				} else {
					$result->status = 0;
					$result->exception = "User details not found";
					return $result;
				}
				
				// Fetching institute categories
				$select = $sql->select();
				$select->from(array('uit' => 'user_institute_types'))
						->join(array('it' => 'institute_types'),
								'it.id = uit.instituteTypeId',
								array('id','name','parentId'));
				$select->columns(array('data'));
				$select->where(array('uit.userId' => $data->userId));
				// $statement = $sql->prepareStatementForSqlObject($select);
				// $userInstituteTypes = $statement->execute();
				$selectString = $sql->getSqlStringForSqlObject($select);
				$userInstituteTypes = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				
				if($userInstituteTypes->count() !== 0) {
					$result->userInstituteTypes = $userInstituteTypes->toArray();
				} else {
					$result->status = 0;
					$result->exception = "User Institutes not found";
					return $result;
				}
				// Fetching Profile Details
				$select = $sql->select();
				$select->from('publisher_details');
				$select->columns(array('Id'=>'id','firstName', 
					'lastName', 'coverPic', 'profilePic', 
					'description', 'foundingYear',
					'gender'));
				
				$select->where(array('userId' => $data->userId));
				$statement = $sql->prepareStatementForSqlObject($select);
				$profileDetails = $statement->execute();
				
				if($profileDetails->count() !== 0) {
					$result->profileDetails = $profileDetails->next();
					if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR'] <> '::1'){
						require_once 'amazonRead.php';
						$result->profileDetails['coverPic'] = getSignedURL($result->profileDetails['coverPic']);
						$result->profileDetails['profilePic'] = getSignedURL($result->profileDetails['profilePic']);
					}
				} else {
					$result->status = 0;
					$result->exception = "Profile not found";
					return $result;
				}
				
				//fetching course details for arcanemind experience
				$sql = new Sql($adapter);
				$select = $sql->select();
				$select->from('courses');
				$select->where(array(
					'ownerId'				=>	$data->userId,
					'deleted'				=>	0,
					'approved'				=>	1,
					'availStudentMarket'	=>	1,
					'liveForStudent'		=>	1
				));
				$select->columns(array('id', 'name'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$courses = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$courses = $courses->toArray();
				foreach($courses as $key=>$course) {
					$sql = new Sql($adapter);
					$select = $sql->select();
					$select->from('subjects');
					$select->where(array(
							'courseId'	=>	$course['id'],
							'deleted'	=>	0
					));
					$select->columns(array('id', 'name'));
					$selectString = $sql->getSqlStringForSqlObject($select);
					$subjects = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
					$subjects = $subjects->toArray();
					foreach($subjects as $key1=>$subject) {
						$sql = new Sql($adapter);
						$select = $sql->select();
						$select->from(array('sp'	=>	'subject_professors'))
						->join(array('pd'	=>	'professor_details'),
									'sp.professorId = pd.userId',
									array('name'	=>	new \Zend\Db\Sql\Expression("CONCAT(firstName, ' ', lastName)"))
						);
						$select->where(array('subjectId'	=>	$subject['id']));
						$select->columns(array());
						$selectString = $sql->getSqlStringForSqlObject($select);
						$professors = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						$professors = $professors->toArray();
						$subjects[$key1]['professors'] = $professors;
					}
					$courses[$key]['subjects'] = $subjects;
				}
				$result->courses = $courses;
				
				$result->status = 1;
				return $result;
			} catch(Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
	
		public function saveCoverImage($filePath, $userId) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$where = array('userId' => $userId);
				$sql = new Sql($adapter);
				
				//deleting previous image from cloud
				$select = $sql->select();
				$select->from('publisher_details');
				$select->where($where);
				$select->columns(array('coverPic'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$coverPic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$coverPic = $coverPic->toArray();
				$coverPic = $coverPic[0]['coverPic'];
				$coverPic = substr($coverPic, 37);
				if($coverPic != '') {
					require_once 'amazonDelete.php';
					deleteFile($coverPic);
				}
				
				$update = $sql->update();
				$update->table('publisher_details');
				$update->set(array('coverPic' => $filePath));
				$update->where($where);
				$statement = $sql->prepareStatementForSqlObject($update);
				$count = $statement->execute()->getAffectedRows();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = 'Cover Image Uploaded!';
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function saveProfileImage($filePath, $userId) {

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$where = array('userId' => $userId);
				$sql = new Sql($adapter);
				
				//deleting previous image from cloud
				$select = $sql->select();
				$select->from('publisher_details');
				$select->where($where);
				$select->columns(array('profilePic'));
				$selectString = $sql->getSqlStringForSqlObject($select);
				$profilePic = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				$profilePic = $profilePic->toArray();
				$profilePic = $profilePic[0]['profilePic'];
				$profilePic = substr($profilePic, 37);
				if($profilePic != '') {
					require_once 'amazonDelete.php';
					deleteFile($profilePic);
				}
				
				$update = $sql->update();
				$update->table('publisher_details');
				$update->set(array('profilePic' => $filePath));
				$update->where($where);
				$statement = $sql->prepareStatementForSqlObject($update);
				$count = $statement->execute()->getAffectedRows();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = 'Profile Image Uploaded!';
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
	
		public function updateProfileDescription($req){

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$where = array('userId' => $req->userId);
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('publisher_details');
				$update->set(array('description' => $req->description));
				$update->where($where);
				$statement = $sql->prepareStatementForSqlObject($update);
				$count = $statement->execute()->getAffectedRows();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();			
				$result->status = 1;
				$result->message = 'Profile Updated!';
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function updateProfileGeneral($req){

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				
				$adapter = $this->adapter;
				$where = array('userId' => $req->userId);
				$sql = new Sql($this->adapter);
				$update = $sql->update();
				$update->table('publisher_details');
				$update->set(array('firstName' => $req->firstName,
									'lastName' => $req->lastName,
									'foundingYear' => $req->foundedIn,
									'gender' => $req->gender));
				$update->where($where);
				$statement = $sql->prepareStatementForSqlObject($update);
				$count = $statement->execute()->getAffectedRows();			
				$update = $sql->update();
				$update->table('user_details');
				$update->set(array('contactMobilePrefix' => $req->phnPrefix,
									'contactMobile' => $req->phnNum,
									'contactLandlinePrefix' => $req->llPrefix,
									'contactLandline' => $req->llNum,
									'addressStreet' => $req->street,
									'addressCity' => $req->city,
									'addressState' => $req->state,
									'addressPin' => $req->pin));
				$update->where($where);
				$statement = $sql->prepareStatementForSqlObject($update);
				// $selectString = $sql->getSqlStringForSqlObject($update);
				// var_dump($selectString); die();
				$count = $statement->execute()->getAffectedRows();
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$result->status = 1;
				$result->message = 'Profile Updated!';
				return $result;
			} catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		
		public function updateUserInstituteType($data){

			$db = $this->adapter->getDriver()->getConnection();

			$db->beginTransaction();

			$result = new stdClass();
			try {
				$result = new stdClass();
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$delete = $sql->delete();
				//Login Details
				$delete->from('user_institute_types');
				$delete->where(array('userId' => $data->userId));
				$statement = $sql->prepareStatementForSqlObject($delete);
				$user_institute_types = $statement->execute();
				$userInstituteTypes = new TableGateway('user_institute_types', $adapter, null,new HydratingResultSet());
				foreach($data->newStructuredInstitutes as $id=>$instituteData) {
					if($id == -1)
						continue;
					$instituteJsonData = Zend\Json\Json::encode($instituteData, true);
					$userInstituteTypes->insert( array(
						'userId' 					=> $data->userId,
						'instituteTypeId'	=> $id,
						'data' => $instituteJsonData
					));
				}
				$result->status = 1;
				$result->message = 'Profile Updated!';
				return $result;
			}catch(Exception $e) {
				$db->rollBack();
				$result->status = 0;
				$result->message = $e->getMessage();
				return $result;
			}
		}
		public function getPortfolios($data) {
			$result = new stdClass();
			try {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select->from(array('p' => 'pf_portfolio'));
				$select->columns(array('subdomain' => 'subdomain',
					'name' => 'name',
					'status' => 'status'));

				$select->where(array('p.userId' => $data->userId, 'p.active' => 'active'));
				$statement = $sql->prepareStatementForSqlObject($select);
				$portfolios = $statement->execute();
				$portfolios = $portfolios->toArray();

				if ($portfolios->count() > 0) {
					$result->portfolios = $portfolios;
				} else {
					$result->portfolios = '';
				}
				$result->status = 1;
				return $result;
			} catch (Exception $e) {
				$result->status = 0;
				$result->exception = $e->getMessage();
				return $result;
			}
		}
	
	}
	
?>