<?php 

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\HydratingResultSet;

require_once "Base.php";

class UserTracking extends Base {

	/**
	* Constructs an array of institutes with their respective tax rates if specified in tax_rates table
	* @return Array institute_taxes
	*/
	public function saveUserTrackinDetails() {

		$db = $this->adapter->getDriver()->getConnection();

		$db->beginTransaction();
		$result = new stdClass();
		try {
			$data = new stdClass();
			$data->userId = 0;
			$data->comments='';
			if(isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
				$data->userId = $_SESSION['userId'];
			}else{
				$data->comments="Anonymous";
			}
			$data->ip='';
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED'];
			} elseif (!empty($_SERVER['HTTP_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_FORWARDED_FOR'];
			} elseif (!empty($_SERVER['HTTP_FORWARDED'])) {
			$ip = $_SERVER['HTTP_FORWARDED'];
			} else {
			$ip = $_SERVER['REMOTE_ADDR'];
			}
			$data->ip			= $ip;
			$data->pageUrl		= $_SERVER['REQUEST_URI'];
			$data->userAgent	= "";
			if (isset($_SERVER['HTTP_USER_AGENT']) && !empty($_SERVER['HTTP_USER_AGENT'])) {
				$data->userAgent	= $_SERVER['HTTP_USER_AGENT'];
			}
			if ($data->userAgent == 'ELB-HealthChecker/1.0') {
				$result->status = 0;
				return $result;
			} else {
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				//$trackingUser = new TableGateway('user_tracking', $adapter, null,new HydratingResultSet());
				$insert = array(
					'pageurl' 			=>  $data->pageUrl,
					'visitorIp' 		=>	'',//$data->ip,
					'visitorId' 		=>	$data->userId,
					'userAgent' 		=>	$data->userAgent,
					'comments'			=>	$data->comments
				);
				//$trackingUser->insert($insert);
				//$trackingId = $trackingUser->getLastInsertValue();
				$query="INSERT INTO user_tracking (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
				$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$adapter->query("SET @trackingId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
				
				$result->status = 1;

				$deleteUserTracks = "DELETE FROM user_tracking WHERE `time`<'".date('Y-m-d H:i:s', strtotime("-15 days"))."'";
				$adapter->query($deleteUserTracks, $adapter::QUERY_MODE_EXECUTE);
				// If all succeed, commit the transaction and all changes are committed at once.
				$db->commit();
				$query = "SELECT @trackingId as trackingID";
				$trackingID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
				$trackingID = $trackingID->toArray();
				$result->trackingId = $trackingID[0]["trackingID"];
				//$result->trackingId = $trackingId;

				return $result;
			}
		}
		catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
			//url,ip,userid or naonmys,time ,comments
		}
	}
}