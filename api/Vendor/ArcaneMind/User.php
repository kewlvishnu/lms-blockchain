<?php

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Mail;
use Zend\Mime\Message ;
use Zend\Mime\Part ;

class User {

    public $id;
    public $role;
    public $adapter;

    function __construct() {
        $config = require "Conf.php";
        $this->adapter = new Zend\Db\Adapter\Adapter($config);
        require "global.php";
        $this->ogPath = $ogCourseImage;
        $this->pbTypes= $publicationTypes;
        $this->sitePath= $sitePath;
        $this->urlDomains= $urlDomains;
    }
	
    public function login($req) {

        $db = $this->adapter->getDriver()->getConnection();
        
        //$db->beginTransaction();

        try {
			$req->username=  strtolower($req->username);
	        if (!isset($req->remember)) {
	            $req->remember = 0;
	        }
	        if (!isset($req->portfolio)) {
	            $req->portfolio = 0;
	        }
	        $adapter = $this->adapter;
	        $sql = new Sql($adapter);
	        $select = $sql->select();
	        //Login Details
			$select->from('login_details');
			$select->columns(array('id', 'validated', 'active', 'temp'));
			$select->where
					->nest
					->equalTo('username', $req->username)
					->or
					->equalTo('email', $req->username)
					->unnest
					->and
					->equalTo('password', $req->hashedPassword);
			$msgInvalidCred = "Invalid Credentials.";
			$selectString = $sql->getSqlStringForSqlObject($select);
			$loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result = new stdClass();
			if ($loginDetails->count() == 0) {
				$result->status = 0;
				$result->valid = false;
				$result->reason = $msgInvalidCred;
				return $result;
			}
	        $result->userIds = $loginDetails->toArray();
	        $temp = array();
	        $validationStatus = array();
	        foreach ($result->userIds as $user) {
	            $temp[] = $user['id'];
	            $validationStatus[$user['id']] = $user['validated'];
	            $activeStatus[$user['id']] = $user['active'];
	            $tempStatus[$user['id']] = $user['temp'];
	        }

	        $result->userIds = $temp;
	        $select = $sql->select();
	        //user roles
	        $select->from('user_roles');
	        $select->columns(array('userId', 'roleId'));
	        $select->where(array('roleId' => $req->userRole));
	        $select->where
	                ->equalTo('roleId', $req->userRole)
	                ->and
	                ->in('userId', $result->userIds);
	        $statement = $sql->prepareStatementForSqlObject($select);
	        $loginDetails = $statement->execute();
	        if ($loginDetails->count() == 0) {
	            $result->status = 0;
	            $result->valid = false;
	            $result->reason = "Invalid Role.";
	            return $result;
	        }
	        $loginDetails->userRole = $loginDetails->next();

	        $result->userRole = $loginDetails->userRole['roleId'];
	        $result->userId = $loginDetails->userRole['userId'];
	        unset($result->userIds);
	        if ($tempStatus[$result->userId] == 1) {
	            $result->temp = 1;
	        } else {
	        	$result->temp = 0;
	        }
	        if ($validationStatus[$result->userId] == 0) {
	            /*unset($result->userRole);
	            unset($result->userId);*/
	            $result->valid = false;
	            $result->reason = "Account not verified.";
				$result->reset = 1;
	            return $result;
	        }
	        if ($activeStatus[$result->userId] == 0) {
	            unset($result->userRole);
	            unset($result->userId);
	            $result->valid = false;
	            $result->reason = "You are banned from our website.";
	            return $result;
	        }
			
	        if ($req->portfolio>0) {
	            $selectString ="SELECT `userId`,`status`
	                            FROM  `pf_portfolio`
	                            WHERE `subdomain`='{$req->slug}'";
	            $subdomains = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
	            if ($subdomains->count() > 0) {
	                $subdomains = $subdomains->toArray();
	                $userId     = $subdomains[0]['userId'];
	                $status     = $subdomains[0]['status'];
	                if (($status == 'Draft') && ($userId!=$result->userId)) {
	                    unset($result->userRole);
	                    unset($result->userId);
	                    $result->valid = false;
	                    $result->reason = "Portfolio not live yet, so only portfolio owners can log in.";
	                    return $result;
	                }
	            }
	        }

			if ($result->userRole == 4) {
				require_once("Student.php");
				$s = new Student();
				$data = new stdClass();
				$data->userId = $result->userId;
				$data->userRole = $result->userRole;
				$studentDetails = $s->getBasicStudentDetails($data);
				if ($studentDetails->status == 1) {
					$result->details = $studentDetails->studentDetails;
				}
				if (isset($req->courseId) && !empty($req->courseId)) {
					$data->courseId = $req->courseId;
					require_once("StudentInvitation.php");
					$si = new StudentInvitation();
					$si->linkStudentCourse($data,'');
				}
			} else {
				$result->details = array();
			}

			//login successful updating time of login
	        $userUpdateDetails = array('lastLogin' => time());
	        if($req->remember == 1) {
	            //$ip     = $_SERVER[REMOTE_ADDR];
	            //$random = md5(uniqid(rand(), true));
	            $cookie = serialize(array(base64_encode($result->userId)));
	            setcookie('mtwebLogin', $cookie, time() + 31104000, '/');
	            $userUpdateDetails['cookie'] = $cookie;
	        }
			$update = $sql->update();
	        $update->table('login_details');
	        $update->set($userUpdateDetails);
	        $update->where(array('id' => $result->userId));
	        $statement = $sql->prepareStatementForSqlObject($update);
	        $count = $statement->execute()->getAffectedRows();
	        
		    // If all succeed, commit the transaction and all changes are committed at once.
		    //$db->commit();

			$userEmail = $this->getUserEmail($result);
			$result->email = "";
			//var_dump($result->email);
			if ($userEmail->status == 1) {
				$result->email = $userEmail->email;
			}

			//now setting username in session for performance improvement ARC-360
			$result->username = $req->username;
	        $result->valid = true;
	        return $result;
		 
        } catch (Exception $e) {
			//$db->rollBack();
            $result = new stdClass();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function googleLogin($req) {

		$db = $this->adapter->getDriver()->getConnection();

		//$db->beginTransaction();

		try {
			$req->username=  strtolower($req->username);
			if (!isset($req->googleId) || empty($req->googleId)) {
				$result->status = 0;
	            $result->message = "Please refresh and try again!";
	            return $result;
			}
			if (!isset($req->remember)) {
				$req->remember = 0;
			}
			if (!isset($req->portfolio)) {
				$req->portfolio = 0;
			}
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('ld'  => 'login_details'));
	        $select->join(array('ur'  => 'user_roles'),'ur.userId=ld.id', array('userRole'=> 'roleId'));
			$select->where
					->equalTo('email', $req->email);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result = new stdClass();
			$result->roleIds = array(
				1 => 0,
				2 => 0,
				4 => 0
			);
			if ($loginDetails->count() == 0) {
			} else {
	        	$logins = $loginDetails->toArray();
	        	foreach ($logins as $key => $userLogin) {
	        		$result->roleIds[$userLogin["userRole"]] = 1;
	        		if (empty($userLogin["googleid"])) {
						$userUpdateDetails = array('googleid' => $req->googleId);
						$update = $sql->update();
						$update->table('login_details');
						$update->set($userUpdateDetails);
						$update->where(array('id' => $userLogin["id"]));
						$statement = $sql->prepareStatementForSqlObject($update);
						$count = $statement->execute()->getAffectedRows();
	        		} else if ($userLogin["googleid"] != $req->googleId) {
						$result->status = 0;
						$result->message = "The Google Login Id doesn't match please refresh and try again!";
						return $result;
	        		}
	        	}
			}
			$result->status = 1;
	        return $result;
		 
        } catch (Exception $e) {
			//$db->rollBack();
            $result = new stdClass();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function googleRoleLogin($req) {

        $db = $this->adapter->getDriver()->getConnection();
        
        //$db->beginTransaction();

        try {
			$req->username=  strtolower($req->username);
	        if (!isset($req->remember)) {
	            $req->remember = 0;
	        }
	        if (!isset($req->portfolio)) {
	            $req->portfolio = 0;
	        }
	        $adapter = $this->adapter;
	        $sql = new Sql($adapter);
	        $select = $sql->select();
	        //Login Details
			$select->from(array('ld'  => 'login_details'));
	        $select->join(array('ur'  => 'user_roles'),'ur.userId=ld.id', array('userRole'=> 'roleId'));
			$select->columns(array('id', 'validated', 'active', 'temp'));
			$select->where
					->equalTo('email', $req->email)
					->and
					->equalTo('googleid', $req->googleId)
					->and
					->equalTo('roleId', $req->userRole);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$result = new stdClass();
			if ($loginDetails->count() == 0) {
				$result->status = 0;
				$result->valid = false;
				$result->reason = "Invalid Credentials.";
				return $result;
			}
	        $userDetails = $loginDetails->toArray();
	        $userDetails = $userDetails[0];

	        $result->userRole = $userDetails['userRole'];
	        $result->userId = $userDetails['id'];

	            /*$validationStatus[$user['id']] = $user['validated'];
	            $activeStatus[$user['id']] = $user['active'];
	            $tempStatus[$user['id']] = $user['temp'];*/

	        if ($userDetails['temp'] == 1) {
	            $result->temp = 1;
	        } else {
	        	$result->temp = 0;
	        }
	        if ($userDetails['validated'] == 0) {
	            /*unset($result->userRole);
	            unset($result->userId);*/
	            $result->valid = false;
	            $result->reason = "Account not verified.";
				$result->reset = 1;
	            return $result;
	        }
	        if ($userDetails['active'] == 0) {
	            unset($result->userRole);
	            unset($result->userId);
	            $result->valid = false;
	            $result->reason = "You are banned from our website.";
	            return $result;
	        }
			
	        if ($req->portfolio>0) {
	            $selectString ="SELECT `userId`,`status`
	                            FROM  `pf_portfolio`
	                            WHERE `subdomain`='{$req->slug}'";
	            $subdomains = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
	            if ($subdomains->count() > 0) {
	                $subdomains = $subdomains->toArray();
	                $userId     = $subdomains[0]['userId'];
	                $status     = $subdomains[0]['status'];
	                if (($status == 'Draft') && ($userId!=$result->userId)) {
	                    unset($result->userRole);
	                    unset($result->userId);
	                    $result->valid = false;
	                    $result->reason = "Portfolio not live yet, so only portfolio owners can log in.";
	                    return $result;
	                }
	            }
	        }

			if ($result->userRole == 4) {
				require_once("Student.php");
				$s = new Student();
				$data = new stdClass();
				$data->userId = $result->userId;
				$data->userRole = $result->userRole;
				$studentDetails = $s->getBasicStudentDetails($data);
				if ($studentDetails->status == 1) {
					$result->details = $studentDetails->studentDetails;
				}
			} else {
				$result->details = array();
			}

			//login successful updating time of login
	        $userUpdateDetails = array('lastLogin' => time());
	        if($req->remember == 1) {
	            //$ip     = $_SERVER[REMOTE_ADDR];
	            //$random = md5(uniqid(rand(), true));
	            $cookie = serialize(array(base64_encode($result->userId)));
	            setcookie('mtwebLogin', $cookie, time() + 31104000, '/');
	            $userUpdateDetails['cookie'] = $cookie;
	        }
			$update = $sql->update();
	        $update->table('login_details');
	        $update->set($userUpdateDetails);
	        $update->where(array('id' => $result->userId));
	        $statement = $sql->prepareStatementForSqlObject($update);
	        $count = $statement->execute()->getAffectedRows();
	        
		    // If all succeed, commit the transaction and all changes are committed at once.
		    //$db->commit();

			$userEmail = $this->getUserEmail($result);
			$result->email = "";
			//var_dump($result->email);
			if ($userEmail->status == 1) {
				$result->email = $userEmail->email;
			}

			//now setting username in session for performance improvement ARC-360
			$result->username = $req->username;
	        $result->valid = true;
	        return $result;
		 
        } catch (Exception $e) {
			//$db->rollBack();
            $result = new stdClass();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function googleRegisteredEmailCheck($req)
    {
    	$result = new stdClass();
    	try {
    		$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$select = $sql->select();
			$select->from(array('ld'  => 'login_details'));
	        $select->join(array('ur'  => 'user_roles'),'ur.userId=ld.id', array('userRole'=> 'roleId'));
			$select->where
					->equalTo('email', $req->email)
	                ->and
	                ->equalTo('roleId', $req->userRole);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			if ($loginDetails->count() == 0) {
				//echo $selectString;
		        $result->status = 0;
				$result->exception = "User not found";
				return $result;
			}
			$userUpdateDetails = array('googleid' => $req->googleId);
			$update = $sql->update();
			$update->table('login_details');
			$update->set($userUpdateDetails);
			$update->where(array('id' => $result->userId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
			$result->status = 1;
			return $result;
    	} catch(Exception $e) {
			//$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
    }
	public function facebookLogin($req) {

        $db = $this->adapter->getDriver()->getConnection();
        
        //$db->beginTransaction();

        $result = new stdClass();
		$req->username=  strtolower($req->username);
		try{
				
			if(isset($req->facebookId))
			{
				$adapter = $this->adapter;
				$sql = new Sql($adapter);
				$select = $sql->select();
				//Login Details
				$select->from('login_details');
				$select->columns(array('id', 'validated', 'active','email','username'));
				$select->where
							->equalTo('facebookid', $req->facebookId);
				$selectString = $sql->getSqlStringForSqlObject($select);
				$loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
				if ($loginDetails->count() > 0) {     
					$result->userIds = $loginDetails->toArray();
					$temp = array();
					$validationStatus = array();
					foreach ($result->userIds as $user) {
						$temp[] = $user['id'];
						$validationStatus[$user['id']] = $user['validated'];
						$activeStatus[$user['id']] = $user['active'];
						$emailCheck[$user['id']] = $user['email'];
						$usernameCheck[$user['id']] = $user['username'];
					}
			
					$result->userIds = $temp;
					$select = $sql->select();
					//user roles
					$select->from('user_roles');
					$select->columns(array('userId', 'roleId'));
					//$select->where(array('roleId' => $req->userRole));
					$select->where
								->equalTo('roleId', $req->userRole)
								->and
								->in('userId', $result->userIds);
					$statement = $sql->prepareStatementForSqlObject($select);
					$loginDetails = $statement->execute();
					if ($loginDetails->count() == 0) {
						/*$result->status = 0;
						$result->valid = false;
						$result->reason = "Invalid Role.";
						return $result;*/

						//need to create new user
						$req->username = $this->generateUsername($req->email);
						$loginDetails = new TableGateway('login_details', $this->adapter, null, new HydratingResultSet());
						$loginDetails->insert(array(
							'email' => $req->email,
							'username' => $req->username,
							'facebookid' => $req->facebookId,
							'createdAt' => 	time(),
							'validated' 	=>	1,
							'active'	=>	1
						));
						$userId = $loginDetails->getLastInsertValue();
						$user_roles = new TableGateway('user_roles', $this->adapter, null, new HydratingResultSet());
						$user_roles->insert(array(
							'userId' => $userId,
							'roleId' => $req->userRole
						));
						$data = new stdClass();
						$data->username	= $req->username;
						$data->email	= $req->email;
						$data->userId	= $userId;
						$data->firstName= $req->firstName;
						$data->lastName	= $req->lastName;
						$data->gender	= 1;
						$data->dob		= '';
						$resBasic = $this->addBasicDetailsShort($data);

						require_once 'Student.php';
						$s = new Student();
						$resBasic = $s->addStudentDetails($data);
						
						// $result = new stdClass();
						//$result->status = 1;

						$userUpdateDetails = array('lastLogin' => time());
						
						// sending welcome email
						$data = new stdClass();
						$data->userId = $result->userId;
						$this->sendWelcomeEmail($data);

						$update = $sql->update();
						$update->table('login_details');
						$update->set($userUpdateDetails);
						$update->where(array('id' => $result->userId));
						$statement = $sql->prepareStatementForSqlObject($update);
						$count = $statement->execute()->getAffectedRows();
						// print_r($userUpdateDetails);
						//now setting username in session for performance improvement ARC-360
						$result->userId = $userId;
						$result->userRole = $req->userRole;
						$result->username = $req->email;
						$result->valid = true;
						return $result;
					}
					$loginDetails->userRole = $loginDetails->next();

					$result->userRole = $loginDetails->userRole['roleId'];
					$result->userId	  = $loginDetails->userRole['userId'];
					$result->username = $req->email;
					unset($result->userIds);
					if ($validationStatus[$result->userId] == 0) {
						/*unset($result->userRole);
						unset($result->userId);*/
						$result->valid = false;
						$result->reason = "Account not verified.";
						$result->reset = 1;
						return $result;
					}
					if ($activeStatus[$result->userId] == 0) {
						unset($result->userRole);
						unset($result->userId);
						$result->valid = false;
						$result->reason = "You are banned from our website.";
						return $result;
					}
					if ($emailCheck[$result->userId] != $req->email) {
						/*unset($result->userRole);
						unset($result->userId);*/
						$result->valid = false;
						$result->reason = "Account already Registered with differnt email/username.";
						$result->reset = 0;
						return $result;
					}
				}
				else{
					//print_r('inside else');
					if( isset($req->email) && !empty($req->email) )
					{
						$sql = new Sql($adapter);
						$select = $sql->select();
						//Login Details
						$select->from('login_details');
						$select->columns(array('id','validated', 'active','facebookid'));
						$select->where
									->equalTo('email', $req->email);
						$selectString = $sql->getSqlStringForSqlObject($select);
						$loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
						if ($loginDetails->count() > 0) {
							//need to update new fb  id
							
							$result->userIds = $loginDetails->toArray();
							$temp = array();
							$validationStatus = array();
							foreach ($result->userIds as $user) {
								$temp[] = $user['id'];
								$validationStatus[$user['id']] = $user['validated'];
								$activeStatus[$user['id']] = $user['active'];
								//$facebookCheck[$user['id']] = $user['facebookid'];
								//$usernameCheck[$user['id']] = $user['username'];
							}
					
							$result->userIds = $temp;
							$select = $sql->select();
							//user roles
							$select->from('user_roles');
							$select->columns(array('userId', 'roleId'));
							$select->where(array('roleId' => $req->userRole));
							$select->where
										->equalTo('roleId', $req->userRole)
										->and
										->in('userId', $result->userIds);
							$statement = $sql->prepareStatementForSqlObject($select);
							$loginDetails = $statement->execute();
							if ($loginDetails->count() == 0) {
								$result->status = 0;
								$result->valid = false;
								$result->reason = "Invalid Role.";
								return $result;
							}
							$loginDetails->userRole = $loginDetails->next();
					
							$result->userRole = $loginDetails->userRole['roleId'];
							$result->userId	  = $loginDetails->userRole['userId'];
							$result->username = $req->email;
							unset($result->userIds);
							if ($validationStatus[$result->userId] == 0) {
								/*unset($result->userRole);
								unset($result->userId);*/
								$result->valid = false;
								$result->reason = "Account not verified.";
								$result->reset = 1;
								return $result;
							}
							if ($activeStatus[$result->userId] == 0) {
								unset($result->userRole);
								unset($result->userId);
								$result->valid = false;
								$result->reason = "You are banned from our website.";
								return $result;
							}
							/*if ($facebookCheck[$result->userId] != '' ) {
								$result->valid = false;
								$result->reason = "Account already Registered with differnt email/username.";
								$result->reset = 1;
								return $result;
							}
							*/
							
							//checking if role exists
							//print_r($loginDetails);
							$update = $sql->update();
							$update->table('login_details');
							$update->set(array('facebookid' => $req->facebookId));
							$update->where
										->equalTo('email', $req->email);
							$statement = $sql->prepareStatementForSqlObject($update);
							$count = $statement->execute()->getAffectedRows();
						
						}else{
							//need to create new user
							$req->username = $this->generateUsername($req->email);
							$loginDetails = new TableGateway('login_details', $this->adapter, null, new HydratingResultSet());
							$loginDetails->insert(array(
								'email' => $req->email,
								'username' => $req->username,
								'facebookid' => $req->facebookId,
								'createdAt' => 	time(),
								'validated' 	=>	1,
								'active'	=>	1
							));
							$userId = $loginDetails->getLastInsertValue();
							$user_roles = new TableGateway('user_roles', $this->adapter, null, new HydratingResultSet());
							$user_roles->insert(array(
								'userId' => $userId,
								'roleId' => $req->userRole
							));
							$data = new stdClass();
							$data->username	= $req->username;
							$data->email	= $req->email;
							$data->userId	= $userId;
							$data->firstName= $req->firstName;
							$data->lastName	= $req->lastName;
							$data->gender	= 1;
							$data->dob		= '';
							$resBasic = $this->addBasicDetailsShort($data);

							require_once 'Student.php';
							$s = new Student();
							$resBasic = $s->addStudentDetails($data);
							
							// $result = new stdClass();
							//$result->status = 1;
							$result->userId = $userId;
							$result->userRole = $req->userRole;
							$result->username = $req->email;
							
							// sending welcome email
							$data = new stdClass();
							$data->userId = $result->userId;
							$this->sendWelcomeEmail($data);
						}
					}else{
						$result->valid = false;
						$result->reason = "Please allow email access to our facebook app.";
						$result->reset = 0;
						return $result;
					}
				}
				
				
			} else {
				$result->valid = false;
				$result->reason = "There was some error. Please refresh and try again.";
				$result->reset = 0;
	            return $result;
			}
			
			if ($result->userRole == 4) {
				require_once("Student.php");
				$s = new Student();
				$data = new stdClass();
				$data->userId = $result->userId;
				$data->userRole = $result->userRole;
				$studentDetails = $s->getBasicStudentDetails($data);
				if ($studentDetails->status == 1) {
					$result->details = $studentDetails->studentDetails;
				}
			} else {
				$result->details = array();
			}
			
			$userUpdateDetails = array('lastLogin' => time());
	        if($req->remember == 1) {
				$cookie = serialize(array(base64_encode($result->userId)));
				setcookie('mtwebLogin', $cookie, time() + 31104000, '/');
				$userUpdateDetails['cookie'] = $cookie;
			}
			// updating login info
			$update = $sql->update();
			$update->table('login_details');
			$update->set($userUpdateDetails);
			$update->where(array('id' => $result->userId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();

			$userEmail = $this->getUserEmail($result);
			$result->email = "";
			//var_dump($result->email);
			if ($userEmail->status == 1) {
				$result->email = $userEmail->email;
			}

		    // If all succeed, commit the transaction and all changes are committed at once.
		    //$db->commit();
			//now setting username in session for performance improvement ARC-360
			$result->username = $req->email;
			$result->valid = true;
			return $result;
		}
		catch(Exception $e){
			//$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
	}
	public function generateUsername($email)
	{
		$username = explode("@", $email);
        $username = $username[0];
        $filler   = "";
        do {
            $usernameDb = $username.$filler;
            $sql = "SELECT id from login_details WHERE username=?";
            $res = $this->adapter->query($sql, array($usernameDb));
            if (empty($filler)) {
                $filler = 1;
            } else {
                $filler++;
            }
        } while ($res->count() > 0);
        return $usernameDb;
	}
    public function parentLogin($req) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();

        try {
	        $req->username = strtolower($req->username);
	        if (!isset($req->remember)) {
	            $req->remember = 0;
	        }
	        $adapter = $this->adapter;
	        $sql = new Sql($adapter);
	        $select = $sql->select();
	        //Login Details
	        $select->from('parent_login_short');
	        $select->columns(array('id', 'student_id', 'course_id', 'active'));
	        $select->where
	                ->equalTo('userid', $req->username)
	                ->and
	                ->equalTo('passwd', $req->hashedPassword);
	        $selectString = $sql->getSqlStringForSqlObject($select);
	        $loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
	        $result = new stdClass();
	        if ($loginDetails->count() == 0) {
	            $result->status = 0;
	            $result->valid = false;
	            $result->reason = "Invalid Credentials.";
	            return $result;
	        }
	        $loginDetails = $loginDetails->toArray();
	        $loginDetails = $loginDetails[0];
	        if($loginDetails['active'] == 0) {
	            $result->status = 0;
	            $result->valid = false;
	            $result->reason = "You are banned from our website.";
	            return $result;
	        }
	        $result->userId = $loginDetails['id'];
	        //login successful updating time of login
	        $userUpdateDetails = array('lastLogin' => time());
	        if($req->remember == 1) {
	            //$ip     = $_SERVER[REMOTE_ADDR];
	            //$random = md5(uniqid(rand(), true));
	            $cookie = serialize(array(base64_encode($result->userId)));
	            setcookie('mtwebLogin', $cookie, time() + 31104000, '/');
	            $userUpdateDetails['cookie'] = $cookie;
	        }
	        $update = $sql->update();
	        $update->table('parent_login_short');
	        $update->set($userUpdateDetails);
	        $update->where(array('id' => $result->userId));
	        $statement = $sql->prepareStatementForSqlObject($update);
	        $count = $statement->execute()->getAffectedRows();
	        
		    // If all succeed, commit the transaction and all changes are committed at once.
		    $db->commit();

	        //Login Details
	        $select = $sql->select();
	        $select->from('student_details');
	        $select->columns(array('id', 'firstName', 'lastName', 'gender', 'profilePic'));
	        $select->where
	                ->equalTo('userId', $loginDetails['student_id']);
	        $selectString = $sql->getSqlStringForSqlObject($select);
	        $studentDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
	        if ($studentDetails->count() == 0) {
	            $result->status = 0;
	            $result->valid = false;
	            $result->reason = "Invalid Student.";
	            return $result;
	        }
	        $studentDetails = $studentDetails->toArray();
	        $studentDetails = $studentDetails[0];

	        require_once('Course.php');
	        $c = new Course();
			$data = new stdClass();
			$data->courseId = $loginDetails['course_id'];
	        $courseDetails = $c->getCourseDetails($data);
	        if ($courseDetails->status == 0) {
	            $result->status = 0;
	            $result->valid = false;
	            $result->reason = $courseDetails->exception;
	            return $result;
	        }
	        $result->courseDetails = $courseDetails->courseDetails;

	        //Login Details
	        $select = $sql->select();
	        $select->from(array('ld'  => 'login_details'));
	        $select->join(array('ur'  => 'user_roles'),'ur.userId=ld.id', array('userRole'=> 'roleId'));
	        $select->columns(array('id'));
	        $select->where
	                ->equalTo('userId', $result->courseDetails['ownerId']);
	        $selectString = $sql->getSqlStringForSqlObject($select);
	        $ownerDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
	        if ($ownerDetails->count() == 0) {
	            $result->status = 0;
	            $result->valid = false;
	            $result->reason = "Invalid Course Owner.";
	            return $result;
	        }
	        $ownerDetails = $ownerDetails->toArray();
	        $data = new stdClass();
	        $data->userId = $result->courseDetails['ownerId'];
	        $data->userRole = $ownerDetails[0]['userRole'];
	        $ownerDetails = Api::getProfileDetails($data);

	        $result->ownerDetails = $ownerDetails->profileDetails;
	        
	        //now setting username in session for performance improvement ARC-360
	        $result->username = $req->username;
	        $result->userRole = $req->userRole;
	        $result->student_id = $loginDetails['student_id'];
	        $result->course_id = $loginDetails['course_id'];
	        $result->name  = ucfirst($studentDetails['firstName']).' '.ucfirst($studentDetails['lastName']);
	        $result->gender = $studentDetails['gender'];
	        $result->valid = true;
	        return $result;
	    }
		catch(Exception $e){
			$db->rollBack();
			$result->status = 0;
			$result->exception = $e->getMessage();
			return $result;
		}
    }
    public function checkUserRemembered($cookie) {
        $result = new stdClass();
        list($userid) = @unserialize($cookie);
        $userid = base64_decode($userid);
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);

            //Login Details
            $select = $sql->select();
            $select->from('login_details');
            $select->columns(array('id', 'email', 'temp'));
            $select->where(array('id' => $userid,'cookie' => $cookie,'validated' => '1','active' => '1'));
            $selectString = $sql->getSqlStringForSqlObject($select);
            $loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            if ($loginDetails->count() == 0) {
                $result->status = 0;
                $result->valid = false;
                setcookie('mtwebLogin', "", time() - 3600, '/');
                unset($_COOKIE['mtwebLogin']);
                return $result;
            }
            $result->status = 1;
            $user = $loginDetails->toArray();
            $result->user = $user[0];
            
            //user roles
            $select = $sql->select();
            $select->from('user_roles');
            $select->columns(array('userId', 'roleId'));
            $select->where(array('userId' => $userid));
            $selectString = $sql->getSqlStringForSqlObject($select);
            $roleDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $roles = $roleDetails->toArray();
            $result->user['roleId'] = $roles[0]['roleId'];

            if ($result->user['roleId'] == 4) {
				require_once("Student.php");
				$s = new Student();
				$data = new stdClass();
				$data->userId = $userid;
				$data->userRole = $result->user['roleId'];
				$studentDetails = $s->getBasicStudentDetails($data);
				if ($studentDetails->status == 1) {
					$result->user['details'] = $studentDetails->studentDetails;
				}
			} else {
				$result->user['details'] = array();
			}

            $result->status = 1;
            $result->valid = true;
            return $result;
        } catch (Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
    public function sendResetPasswordLink($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();

        $result = new stdClass();
        $data->username=  strtolower($data->username);
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            $select = $sql->select();
            //Login Details
            $select->from('login_details');
            $select->columns(array('id', 'email'));
            $select->where
                    ->equalTo('username', $data->username)
                    ->or
                    ->equalTo('email', $data->username);
            $selectString = $sql->getSqlStringForSqlObject($select);
            $loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $result = new stdClass();
            if ($loginDetails->count() == 0) {
                $result->status = 0;
                $result->message = "Invalid Username or Email.";
                return $result;
            }
            $data->userIds = $loginDetails->toArray();
            $temp = array();
            foreach ($data->userIds as $user) {
                $temp[] = $user['id'];
            }

            $data->userIds = $temp;
            $select = $sql->select();
            //user roles
            $select->from('user_roles');
            $select->columns(array('userId', 'roleId'));
            $select->where(array('roleId' => $data->userRole));
            $select->where
                    ->equalTo('roleId', $data->userRole)
                    ->and
                    ->in('userId', $data->userIds);
            $statement = $sql->prepareStatementForSqlObject($select);
            $loginDetails = $statement->execute();
            if ($loginDetails->count() == 0) {
                $result->status = 0;
                $result->message = "Invalid Role.";
                return $result;
            }
            $loginDetails->userRole = $loginDetails->next();
            $data->userRole = $loginDetails->userRole['roleId'];
            $data->userId = $loginDetails->userRole['userId'];

            unset($result->userIds);
            // $select = $sql->select();
            // //Login Details
            // $select->from('login_details');
            // $select->columns(array('email'));
            // $select->where->equalTo('id', $data->userId);
            // $selectString = $sql->getSqlStringForSqlObject($select);
            
            $selectString = "SELECT L.email,L.username,
            case 
        	when UR.roleId= 1 then (select name from institute_details where userId=L.id)
                when UR.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId=L.id)
                when UR.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId=L.id)
                when UR.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId=L.id)
                else 'NA' end as Name
                From login_details as L 
                Join user_roles as UR on UR.userId=L.id
                Where L.id=$data->userId ";

            $loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $result = new stdClass();
            if ($loginDetails->count() == 0) {
                $result->status = 0;
                $result->message = "Invalid Username or Email.";
                return $result;
            }
            $loginDetails->email = $loginDetails->toArray();
            $data->email = $loginDetails->email[0]['email'];
            $data->loginUserName=$loginDetails->email[0]['username'];
            $data->name=$loginDetails->email[0]['Name'];

            // Need to make sure that token is unique
            $token = md5(uniqid($data->email . rand(), true));
            $resetPassword = new TableGateway('reset_password_tokens', $adapter, null, new HydratingResultSet());
            $resetPassword->insert(array(
                'userId' => $data->userId,
                'token' => $token,
                'createdAt' => $data->createdAt,
            ));
		    
		    // If all succeed, commit the transaction and all changes are committed at once.
		    $db->commit();

            global $siteBase;
            
            $emailBody = "Dear {$data->name},<br><br>";
            
            $emailBody .= "We received a request for password change for username <b> $data->username </b> at Integro.io <br>";
            
            $emailBody .= "<br><br>Please click on the following link to set your new password.<br> ";
            if ($data->userRole == 4) {
                $link=$siteBase . "marketplace/reset-password/{$data->userId}/{$token}";
            } else if ($data->userRole == 6) {
                $link=$siteBase . "marketplace/parent/reset-password/{$data->userId}/{$token}";
            } else {
                $link=$siteBase . "marketplace/teach/reset-password/{$data->userId}/{$token}";
            }
            
            $emailBody.= '<a href="'.$link.'">Click here to Reset your Password </a> <br>';
           
            $emailBody.= "<br><br>If you did not initiate the request then please ignore the email.'
                    If you get repeated such emails then please contact us at <a href=admin@integro.io>admin@integro.io</a>  with
                   the Subject : Username : <b>{$data->email} </b> – Frequent Password Requests, explaining to us the issue in detail. <br>";
           
            $subject = 'Password change request';
            $to = $data->email;
        
            $this->sendMail($to, $subject, $emailBody);
           
            $result->status = 1;
            $result->message = "A verification mail has been sent to your account.";
            return $result;
        } catch (Exception $e) {
			$db->rollBack();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function resetPassword($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        //$db->beginTransaction();
        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $where = array('userId' => $data->userId, 'token' => $data->token, 'used' => 0);
            $sql = new Sql($adapter);
            $update = $sql->update();
            $update->table('reset_password_tokens');
            $update->set(array('used' => 1));
            $update->where($where);
            $statement = $sql->prepareStatementForSqlObject($update);
            $count = $statement->execute()->getAffectedRows();
            if ($count == 0) {
                $result->status = 0;
                $result->message = 'This url has expired or it never existed.';
                return $result;
            }
            $where = array('id' => $data->userId);
            $sql = new Sql($adapter);
            $update = $sql->update();
            $update->table('login_details');
            $update->set(array('password' => $data->hashedPassword));
            $update->where($where);
            $statement = $sql->prepareStatementForSqlObject($update);
            $count = $statement->execute();
		    
		    // If all succeed, commit the transaction and all changes are committed at once.
		    //$db->commit();

            $result->status = 1;
            $result->message = "Your password has been updated.";
            return $result;
        } catch (Exception $e) {
			//$db->rollBack();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function validateEmail($req) {
        $adapter = $this->adapter;
        $sql = "SELECT * FROM user_roles WHERE roleId=? AND userId IN (SELECT id from login_details WHERE email=?)";
        $loginDetails = $adapter->query($sql, array($req->role, $req->email));
        $result = new stdClass();
        $result->status = 1;
        if ($loginDetails->count() == 0)
            $result->valid = true;
        else
            $result->valid = false;
        return $result;
    }

    public function getUserEmail($req) {
        $adapter = $this->adapter;
        $sql = "SELECT `email` FROM login_details WHERE id=?";
        $loginDetails = $adapter->query($sql, array($req->userId));
        $result = new stdClass();
        if ($loginDetails->count() > 0) {
        	$result->status = 1;
            $result->email = $loginDetails->toArray();
            $result->email = $result->email[0]['email'];
        } else {
        	$result->status = 0;
        }
        return $result;
    }

    public function getPortfolioBySlug($slug,$userId) {
        try {
            $adapter = $this->adapter;
            $sql = "SELECT pf.id AS portfolioId,ud.userId,ur.roleId, pf.name, pf.affiliation, pf.description,
                            pf.img, pf.favicon, pf.facebook, pf.twitter, pf.linkedin, pf.reason, pf.status
                    FROM user_details AS ud
                    LEFT JOIN user_roles AS ur ON ur.userId=ud.userId
                    LEFT JOIN pf_portfolio AS pf ON pf.userId=ud.userId
                    WHERE pf.subdomain=? and pf.active='active'";
            $portfolio = $adapter->query($sql, array($slug));
            $result = new stdClass();
            $result->status = 1;
            if ($portfolio->count() > 0) {
                $result->valid = true;
                $result->portfolio = $portfolio->toArray();
                $result->portfolio = $result->portfolio[0];
                if (empty($result->portfolio['img'])) {
                    if($result->portfolio['roleId'] == 1) {
                        $sql1 = "SELECT `profilePic`
                                FROM institute_details
                                WHERE userId=?";
                        $profile = $adapter->query($sql1, array($result->portfolio['userId']));
                        if ($profile->count() > 0) {
                            $profile = $profile->toArray();
                            $profile = $profile[0];
                            $result->portfolio['img'] = $profile['profilePic'];
                        }
                    } else {
                        $sql1 = "SELECT `profilePic`
                                FROM professor_details
                                WHERE userId=?";
                        $profile = $adapter->query($sql1, array($result->portfolio['userId']));
                        if ($profile->count() > 0) {
                            $profile = $profile->toArray();
                            $profile = $profile[0];
                            $result->portfolio['img'] = $profile['profilePic'];
                        }
                    }
                }
                $sql1 = "SELECT *
                        FROM pf_menus
                        WHERE portfolioId=? AND status='active'";
                $menus = $adapter->query($sql1, array($result->portfolio['portfolioId']));
                $result->portfolio['menus'] = 0;
                if ($menus->count() > 0) {
                    $menus = $menus->toArray();
                    $result->portfolio['menus'] = count($menus);
                }
                $sql1 = "SELECT *
                        FROM pf_pages
                        WHERE portfolioId=? AND status='active'";
                $pages = $adapter->query($sql1, array($result->portfolio['portfolioId']));
                $result->portfolio['pages'] = 0;
                if ($pages->count() > 0) {
                    $pages = $pages->toArray();
                    $result->portfolio['pages'] = count($pages);
                }
                if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
                    require_once 'amazonRead.php';
                    $result->portfolio['img'] = getSignedURL($result->portfolio['img']);
                } else {
                	$result->portfolio['img'] = 'assets/pages/media/profile/profile_user.jpg';
                }
                $result->userId  = $result->portfolio['userId'];
                $result->userRole= $result->portfolio['roleId'];
                if($result->userId==$userId) {
                    $result->owner  = 1;
                } else {
                    $result->owner  = 0;
                }
                $enumList = $this->get_enum_values('pf_portfolio', 'status');
                if ($enumList != false) {
                    $result->portfolioStatus = $enumList;
                }
            } else {
                $result->valid = false;
            }
            return $result;
        } catch (Exception $e) {
            $result = new stdClass();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
    
    public function get_enum_values( $table, $field ) {
        $adapter = $this->adapter;
        $selectString = "SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'";
        $enums = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        if ($enums->count() > 0) {
            $enums = $enums->toArray();
            $type = $enums[0]['Type'];
            preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
            $enum = explode("','", $matches[1]);
            return $enum;
        } else {
            return false;
        }
        /**/
    }

    public function getUserBySlug($slug,$userId) {
        try {
            $adapter = $this->adapter;
            $sql = "SELECT ud.userId,ur.roleId FROM user_details AS ud
                    LEFT JOIN user_roles AS ur ON ur.userId=ud.userId
                    WHERE ud.slug=? and ud.slug_status=1";
            $userDetails = $adapter->query($sql, array($slug));
            $result = new stdClass();
            $result->status = 1;
            if ($userDetails->count() == 1) {
                $result->valid = true;
                $userDetails->user = $userDetails->toArray();
                $result->userId  = $userDetails->user[0]['userId'];
                $result->userRole= $userDetails->user[0]['roleId'];
                if($result->userRole==1 && $result->userId==$userId) {
                    $result->access  = true;
                } else {
                    $result->access  = false;
                }
            } else {
                $result->valid = false;
            }
            return $result;
        } catch (Exception $e) {
            $result = new stdClass();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
    public function getPortfolios($data) {
        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            $selectString ="SELECT `id`, `subdomain`, `name`, `status`,reason,description
                            FROM  `pf_portfolio`
                            WHERE `userId`={$data->userId} AND `active`='active'";
            $portfolios = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $result->portfolios = $portfolios->toArray();

            $data->instituteId = $data->userId;
            foreach ($result->portfolios as $key => $value) {
                $portfolioId = $result->portfolios[$key]['id'];
                //$query = "SELECT `id` AS value, `name` AS label FROM courses AS c WHERE c.`ownerId`=$data->instituteId AND c.`deleted`=0 AND c.`approved`=1 AND c.`availStudentMarket`=1 AND c.`liveForStudent`=1 ORDER BY c.`id`";
                $query = "SELECT c.`id` AS value, c.`name` AS label, IFNULL(pmc.id,0) AS optionCourse
                            FROM courses AS c
                            LEFT JOIN pf_main_courses AS pmc ON c.id=pmc.courseId AND pmc.portfolioId=$portfolioId
                            WHERE c.`ownerId`=$data->instituteId AND c.`deleted`=0 ORDER BY c.`id`";
                $courseDetails = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
                if($courseDetails->count() > 0) {
                    require_once 'Student.php';
                    $s = new Student();

                    $result->portfolios[$key]['courses'] = $courses = $courseDetails->toArray();
                } else {
                    $result->portfolios[$key]['courses'] = $courses = array();
                }
            }
            //$result->courses = $courses;
            $result->status = 1;
            return $result;
        } catch (Exception $e) {
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }
    public function newPortfolio($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            $portfolioName = $data->portfolioName;
            $portfolioDesc = $data->portfolioDesc;
            $subdomain     = $data->subdomain;
            $portfolioPic      = $data->portfolioPic;
            if(empty($subdomain)) {
                $result->status = 0;
                $result->message = 'Subdomain is compulsory';
                return $result;
            } else if(empty($portfolioName)) {
                $result->status = 0;
                $result->message = 'Portfolio Name is compulsory';
                return $result;
            } else {
                $selectString ="SELECT `subdomain`
                                FROM  `pf_portfolio`
                                WHERE `subdomain`='$subdomain'";
                $subdomains = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                $subdomains = $subdomains->toArray();
                if(count($subdomains)>0) {
                    $result->status = 0;
                    $result->message = 'Subdomain already used';
                    return $result;
                }
                $portfolios = new TableGateway('pf_portfolio', $this->adapter, null, new HydratingResultSet());
                $result->portfolios = array();
                $portfolios->insert(array(
                    'subdomain' => $subdomain,
                    'userId' => $data->userId,
                    'name' => $portfolioName,
                    'description' => $portfolioDesc,
                    'img' => $portfolioPic,
                    'status' => 'Waiting for approval',
                    'createdAt' => time(),
                ));
            }
		    // If all succeed, commit the transaction and all changes are committed at once.
		    $db->commit();
            $result->status = 1;
            $result->message = 'New portfolio submitted for approval';
            return $result;
        } catch (Exception $e) {
			$db->rollBack();
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }
    public function getPortfolioCourses($data) {
        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            $subdomain     = $data->slug;
            if(empty($subdomain)) {
                $result->status = 0;
                $result->message = 'There is some error';
                return $result;
            } else {
                $selectString ="SELECT `id`, `userId`
                                FROM  `pf_portfolio`
                                WHERE `subdomain`='$subdomain' AND `userId`={$data->userId}";
                $subdomains = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                $subdomains = $subdomains->toArray();
                if(count($subdomains)>0) {
                    $subdomains = $subdomains[0];
                    $portfolioId  = $subdomains['id'];
                    $data->instituteId = $subdomains['userId'];
                    $query = "SELECT c.`id` AS value, c.`name` AS label
                              FROM pf_main_courses AS pmc
                              INNER JOIN `courses` AS c ON c.id=pmc.courseId
                              WHERE pmc.`portfolioId`=$portfolioId AND c.`ownerId`=$data->instituteId AND c.`deleted`=0 ORDER BY c.`id`";
                    $courseDetails = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
                    if($courseDetails->count() !== 0) {

                        //adding rating with course details
                        require_once 'Student.php';
                        $s = new Student();

                        $courses = $courseDetails->toArray();
                        /*$resCourses = array();
                        foreach ($courses as $key => $course) {
                            $resCourses[$course['id']] = $course['name'];
                        }*/
                        $result->status = 1;
                        $result->courses = $courses;
                        return $result;
                    } else {
                        $courses = array();
                        $result->status = 1;
                        $result->courses = $courses;
                        return $result;
                    }
                } else {
                    $result->status = 0;
                    $result->message = 'Invalid access';
                    return $result;
                }
            }
            return $result;
        } catch (Exception $e) {
            $result = new stdClass();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
    public function getPortfolioMenus($data) {
        try {
            $adapter = $this->adapter;
            $subdomain = $data->slug;
            $sql = "SELECT pm.`id` AS menuId, pm.`portfolioId`, pm.`name`, pm.`description`, pm.`type`, pm.`pageId`, pg.`pageTitle`, pg.`content`, pg.`status` AS `pageStatus`, ppt.`type` AS pageType, pm.`url`, pm.`order`, pm.`status`
                    FROM `pf_menus` AS pm
                    LEFT JOIN `pf_portfolio` AS pf ON pf.`id`=pm.`portfolioId`
                    LEFT JOIN `pf_pages`     AS pg ON pg.`id`=pm.`pageId`
                    LEFT JOIN `pf_page_types` AS ppt ON ppt.`id`=pg.`pageType`
                    WHERE pf.`subdomain`=? ORDER BY pm.`order`";
            $portfolioMenus = $adapter->query($sql, array($subdomain));
            $result = new stdClass();
            $result->status = 1;
            $result->valid = true;
            if ($portfolioMenus->count() > 0) {
                $result->portfolioMenus = $portfolioMenus->toArray();
            } else {
                $result->portfolioMenus = array();
            }
            return $result;
        } catch (Exception $e) {
            $result = new stdClass();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
    public function savePortfolioMenu($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            $menuType   = $data->menuType;
            $menuName   = $data->menuName;
            $menuDescription = $data->menuDescription;
            $menuOrder  = $data->menuOrder;
            $menuStatus = $data->menuStatus;
            $subdomain  = $data->slug;
            $menuId = 0;
            if (isset($data->menuId) && !empty($data->menuId)) {
                $menuId = $data->menuId;
            }
            if(empty($menuType)) {
                $result->status = 0;
                $result->message = 'Please select Menu Type';
                return $result;
            } else if(empty($menuName)) {
                $result->status = 0;
                $result->message = 'Menu Name can\'t be left empty';
                return $result;
            } else if(empty($menuDescription)) {
                $result->status = 0;
                $result->message = 'Menu Description can\'t be left empty';
                return $result;
            } else if(empty($menuOrder) && ($menuOrder!=0)) {
                $result->status = 0;
                $result->message = 'Menu Order can\'t be left empty';
                return $result;
            } else if(empty($menuStatus)) {
                $result->status = 0;
                $result->message = 'Menu Status can\'t be left empty';
                return $result;
            } else if($menuType == 'page' && empty($data->selectPortfolioPage)) {
                $result->status = 0;
                $result->message = 'Page can\'t be left empty';
                return $result;
            } else if($menuType == 'url' && empty($data->inputMenuUrl)) {
                $result->status = 0;
                $result->message = 'Custom URL can\'t be left empty';
                return $result;
            } else {
                $selectString ="SELECT `id`
                                FROM  `pf_portfolio`
                                WHERE `subdomain`='$subdomain' AND `userId`={$data->userId}";
                $subdomains = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                $subdomains = $subdomains->toArray();
                if(count($subdomains)>0) {
                    $subdomains = $subdomains[0];
                    $portfolioId  = $subdomains['id'];
                    if (empty($menuId)) {
                        //$portfolios = new TableGateway('pf_menus', $this->adapter, null, new HydratingResultSet());
                        $result->portfolios = array();
                        $insertPage = array(
                            'portfolioId' => $portfolioId,
                            'name' => $menuName,
                            'description' => $menuDescription,
                            'order' => $menuOrder,
                            'type' => $menuType,
                            'pageId' => 0,
                            'url' => '',
                            'status' => $menuStatus
                        );
                        if ($menuType == 'page') {
                            $insertPage['pageId'] = $data->selectPortfolioPage;
                        } elseif ($menuType == 'url') {
                            $insertPage['url'] = $data->inputMenuUrl;
                        }
                        //$portfolios->insert($insertPage);
                        //$menuId = $portfolios->getLastInsertValue();
						$query="INSERT INTO pf_menus (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
						$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$adapter->query("SET @menuId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);
						// If all succeed, commit the transaction and all changes are committed at once.
					    $db->commit();

						$query = "SELECT @menuId as menuID";
						$menuID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
						$menuID = $menuID->toArray();
						$menuId = $menuID[0]["menuID"];

                        $result->status = 1;
                        $result->message = 'Portfolio menu added';
                    } else {
                    	//$adapter->query("SET @menuId = {$menuId}", $adapter::QUERY_MODE_EXECUTE);
                        /*if ($menuType == 'page') {
                            $updatePage['pageId'] = $data->selectPortfolioPage;
                            $updatePage['url']    = '';
                        } elseif ($menuType == 'url') {
                            $updatePage['pageId'] = '';
                            $updatePage['url']    = $data->inputMenuUrl;
                        }

						$query="UPDATE pf_menus SET 
							`portfolioId` = $portfolioId,
                            `name` = $menuName,
                            `description` = $menuDescription,
                            `order` = $menuOrder,
                            `type` = '".$menuType."',
                            `pageId` = '".$updatePage['pageId']."',
                            `url` = '".$updatePage['url']."',
                            `status` = '".$menuStatus."'
						WHERE id=@menuId";
						$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);*/
                        $where = array('portfolioId' => $portfolioId,'id' => $menuId);
                        $update = $sql->update();
                        $update->table('pf_menus');
                        $updatePage = array(
                            'portfolioId' => $portfolioId,
                            'name' => $menuName,
                            'description' => $menuDescription,
                            'order' => $menuOrder,
                            'type' => $menuType,
                            'url' => '',
                            'status' => $menuStatus
                        );
                        if ($menuType == 'page') {
                            $updatePage['pageId'] = $data->selectPortfolioPage;
                            $updatePage['url']    = '';
                        } elseif ($menuType == 'url') {
                            $updatePage['pageId'] = '';
                            $updatePage['url']    = $data->inputMenuUrl;
                        }
                        $update->set($updatePage);
                        $update->where($where);
                        $statement = $sql->prepareStatementForSqlObject($update);
                        $count = $statement->execute()->getAffectedRows();
					    // If all succeed, commit the transaction and all changes are committed at once.
					    $db->commit();
                        $result->status = 1;
                        $result->message = 'Portfolio menu updated';
                    }

                    $query = "SELECT pm.`id` AS menuId, pm.`order`, pm.`status`
                            FROM `pf_menus` AS pm
                            LEFT JOIN `pf_portfolio` AS pf ON pf.`id`=pm.`portfolioId`
                            WHERE pf.`subdomain`=? ORDER BY pm.`order`";
                    $portfolioMenus = $adapter->query($query, array($subdomain));
                    if ($portfolioMenus->count() > 0) {
                        $portfolioMenus = $portfolioMenus->toArray();
                        foreach ($portfolioMenus as $key => $value) {
                            //var_dump($value);
                            $NewMenuOrder = $value['order'];
                            if ($value['menuId'] != $menuId && $value['order']>=$menuOrder) {
                                $menuOrder++;
                                $NewMenuOrder = $menuOrder;
                            } 

                            $where = array('portfolioId' => $portfolioId,'id' => $value['menuId']);
                            $update = $sql->update();
                            $update->table('pf_menus');
                            $updatePage = array(
                                'order' => $NewMenuOrder
                            );
                            $update->set($updatePage);
                            $update->where($where);
                            $statement = $sql->prepareStatementForSqlObject($update);
                            $count = $statement->execute()->getAffectedRows();
                        }
                        //var_dump($portfolioMenus);
                        $query1 = "SELECT pm.`id` AS menuId, pm.`order`, pm.`status`
                                FROM `pf_menus` AS pm
                                LEFT JOIN `pf_portfolio` AS pf ON pf.`id`=pm.`portfolioId`
                                WHERE pf.`subdomain`=? ORDER BY pm.`order`";
                        $portfolioMenus1 = $adapter->query($query1, array($subdomain));
                        if ($portfolioMenus1->count() > 0) {
                            $portfolioMenus1 = $portfolioMenus1->toArray();
                            $i=0;
                            foreach ($portfolioMenus1 as $key => $value) {
                                $i++;
                                $NewMenuOrder = $i;

                                $where = array('portfolioId' => $portfolioId,'id' => $value['menuId']);
                                $update = $sql->update();
                                $update->table('pf_menus');
                                $updatePage = array(
                                    'order' => $NewMenuOrder
                                );
                                $update->set($updatePage);
                                $update->where($where);
                                $statement = $sql->prepareStatementForSqlObject($update);
                                $count = $statement->execute()->getAffectedRows();
                            }
                            //var_dump($portfolioMenus1);
                        }
                    }
                    $query = "SELECT pm.`id` AS menuId, pm.`portfolioId`, pm.`name`, pm.`description`, pm.`type`, pm.`pageId`, pg.`pageTitle`, pg.`content`, pg.`status` AS `pageStatus`, ppt.`type` AS pageType, pm.`url`, pm.`order`, pm.`status`
                            FROM `pf_menus` AS pm
                            LEFT JOIN `pf_portfolio` AS pf ON pf.`id`=pm.`portfolioId`
                            LEFT JOIN `pf_pages`     AS pg ON pg.`id`=pm.`pageId`
                            LEFT JOIN `pf_page_types` AS ppt ON ppt.`id`=pg.`pageType`
                            WHERE pf.`subdomain`=? ORDER BY pm.`order`";
                    $portfolioMenus = $adapter->query($query, array($subdomain));
                    if ($portfolioMenus->count() > 0) {
                        $result->portfolioMenus = $portfolioMenus->toArray();
                    } else {
                        $result->portfolioMenus = array();
                    }
                    $result->menuId = $menuId;
                } else {
                    $result->status = 0;
                    $result->message = 'Invalid access';
                }
            }
            return $result;
        } catch (Exception $e) {
			$db->rollBack();
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }
    public function getPortfolioDetail($data) {
        try {
            $adapter = $this->adapter;
            $sql = "SELECT ud.userId,ur.roleId, pf.name, pf.affiliation, pf.description,
                            pf.img, pf.favicon, pf.facebook, pf.twitter, pf.linkedin, pf.status
                    FROM user_details AS ud
                    LEFT JOIN user_roles AS ur ON ur.userId=ud.userId
                    LEFT JOIN pf_portfolio AS pf ON pf.userId=ud.userId
                    WHERE pf.subdomain=? and pf.active='active'";
            $portfolio = $adapter->query($sql, array($data->slug));
            $result = new stdClass();
            $result->status = 1;
            if ($portfolio->count() > 0) {
                $result->valid = true;
                $result->portfolio = $portfolio->toArray();
                $result->portfolio = $result->portfolio[0];
                if (empty($result->portfolio['img'])) {
                    if($result->portfolio['roleId'] == 1) {
                        $sql1 = "SELECT `profilePic`
                                FROM institute_details
                                WHERE userId=?";
                        $profile = $adapter->query($sql1, array($result->portfolio['userId']));
                        if ($profile->count() > 0) {
                            $profile = $profile->toArray();
                            $profile = $profile[0];
                            $result->portfolio['img'] = $profile['profilePic'];
                        }
                    } else {
                        $sql1 = "SELECT `profilePic`
                                FROM professor_details
                                WHERE userId=?";
                        $profile = $adapter->query($sql1, array($result->portfolio['userId']));
                        if ($profile->count() > 0) {
                            $profile = $profile->toArray();
                            $profile = $profile[0];
                            $result->portfolio['img'] = $profile['profilePic'];
                        }
                    }
                }
                if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
                    require_once 'amazonRead.php';
                    $result->portfolio['img'] = getSignedURL($result->portfolio['img']);
                }
                $result->portfolio['facebook'] = ((!empty($result->portfolio['facebook']))?$result->portfolio['facebook']:'javascript:void(0)');
                $result->portfolio['twitter']  = ((!empty($result->portfolio['twitter']))?$result->portfolio['twitter']:'javascript:void(0)');
                $result->portfolio['linkedin'] = ((!empty($result->portfolio['linkedin']))?$result->portfolio['linkedin']:'javascript:void(0)');
                $result->userId  = $result->portfolio['userId'];
                $result->userRole= $result->portfolio['roleId'];
                if($result->userId==$data->userId) {
                    $result->owner  = 1;
                } else {
                    $result->owner  = 0;
                }
                if($result->userRole == 1) {
                    require_once "Institute.php";
                    $i = new Institute();
                    $result->portfolio['portfolio'] = $i->getPortfolioDetail($data);
                } else if($result->userRole == 2) {
                    require_once "Professor.php";
                    $p = new Professor();
                    $result->portfolio['portfolio'] = $p->getPortfolioDetail($data);
                }
            } else {
                $result->valid = false;
            }
            return $result;
        } catch (Exception $e) {
            $result = new stdClass();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
   
    public function reSubmitPortfolio($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
        $result = new stdClass();
        try {
            $adapter = $this->adapter;
			$sql = new Sql($adapter);
           	$update = $sql->update();
			$update->table('pf_portfolio');
			$update->set(array(
						'status'			=>	'Waiting for approval',
						'subdomain' 		=> $data->subdomain,
	                    'name' 				=> $data->portfolioName,
	                    'description' 		=> $data->portfolioDesc
						
			));
			$update->where(array('id'	=>	$data->pfId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$statement->execute();
		    // If all succeed, commit the transaction and all changes are committed at once.
		    $db->commit();
            $result->status = 1;
            $result->message = 'Portfolio again submitted for approval';
            return $result;
        } catch (Exception $e) {
			$db->rollBack();
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }
   
    public function validateUsername($req) {
        $req->username = strtolower($req->username);
        $adapter = $this->adapter;
        $sql = new Sql($adapter);
        $select = $sql->select();
        //Login Details
        $select->from('login_details');
        $select->columns(array('id'));
        $select->where(array('username' => $req->username));
        $statement = $sql->prepareStatementForSqlObject($select);
        $loginDetails = $statement->execute();
        $result = new stdClass();
        $result->status = 1;
        if ($loginDetails->count() == 0)
            $result->valid = true;
        else
            $result->valid = false;
        return $result;
    }
    public function verifyUserID($req) {
        $adapter = $this->adapter;
        $sql = new Sql($adapter);
        $select = $sql->select();
        //Login Details
        $select->from('login_details');
        $select->columns(array('id', 'validated'));
        $select->where(array('id' => $req->userid));
        $statement = $sql->getSqlStringForSqlObject($select);
        //$loginDetails = $statement->execute();
        $loginDetails = $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);
        $result = new stdClass();
        $result->status = 1;
        if ($loginDetails->count() > 0) {
            $result->valid = true;
            $loginDetails = $loginDetails->toArray();
            if ($loginDetails[0]['validated'] == 1) {
                $result->verified = true;
            } else {
                $result->verified = false;
            }
        } else {
            $result->valid = false;
            $result->verified = false;
        }
        return $result;
    }

    public function registerShort($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();

        $adapter = $this->adapter;

        try {
            //$loginDetails = new TableGateway('login_details', $this->adapter, null, new HydratingResultSet());
            //$data->username=  strtolower($data->username);
            $username = explode("@", $data->email);
            $username = $username[0];
            $filler   = "";
				
            do {
                $usernameDb = $username.$filler;
                $sql = "SELECT id from login_details WHERE username=?";
                $res = $this->adapter->query($sql, array($usernameDb));
                if (empty($filler)) {
                    $filler = 1;
                } else {
                    $filler++;
                }
            } while ($res->count() > 0);
            $data->username = $usernameDb;
            $insert = array(
                'email' => $data->email,
                'username' => $data->username,
                'password' => $data->hashedPassword,
                'createdAt' => $data->createdAt
            );
            if (isset($data->googleId) && !empty($data->googleId)) {
            	$insert['googleid'] = $data->googleId;
            	$insert['validated'] = 1;
            }
            //$loginDetails->insert($insert);
            //$userId = $loginDetails->getLastInsertValue();
			
			$query="INSERT INTO login_details (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
			$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$adapter->query("SET @userId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);

            //$user_roles = new TableGateway('user_roles', $this->adapter, null, new HydratingResultSet());
            $insert = array(
                //'userId' => $userId,
                'roleId' => $data->role
            );
            //$user_roles->insert($insert);
			$query="INSERT INTO user_roles (`".implode("`,`", array_keys($insert))."`,`userId`) VALUES ('".implode("','", array_values($insert))."',@userId)";
			$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
		    // If all succeed, commit the transaction and all changes are committed at once.
		    $db->commit();
			$query = "SELECT @userId as userID";
			$userID = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$userID = $userID->toArray();
			$userId = $userID[0]["userID"];
            $result = new stdClass();
            $result->status = 1;
            $result->userId = $userId;
            return $result;
		 
        } catch (Exception $e) {
			$db->rollBack();
            $result = new stdClass();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
    
    public function register($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();

        try {

        	$adapter = $this->adapter;
            //$loginDetails = new TableGateway('login_details', $this->adapter, null, new HydratingResultSet());
            $data->username=  strtolower($data->username);
            $insert = array(
                'email' => $data->email,
                'username' => $data->username,
                'password' => $data->hashedPassword,
                'createdAt' => $data->createdAt
            );
            //$loginDetails->insert($insert);
            //$userId = $loginDetails->getLastInsertValue();
			$query="INSERT INTO login_details (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
			$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$adapter->query("SET @userId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);

            //$user_roles = new TableGateway('user_roles', $this->adapter, null, new HydratingResultSet());
            $insert = array(
                'userId' => $userId,
                'roleId' => $data->role
            );
            //$user_roles->insert($insert);
			$query="INSERT INTO user_roles (`".implode("`,`", array_keys($insert))."`,`userId`) VALUES ('".implode("','", array_values($insert))."',@userId)";
			$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
		    // If all succeed, commit the transaction and all changes are committed at once.
		    $db->commit();
            $result = new stdClass();
            $result->status = 1;
            $result->userId = $userId;
            return $result;
        } catch (Exception $e) {
			$db->rollBack();
            $result = new stdClass();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function addBasicDetailsShort($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
        try {
	        $result = new stdClass();
	        $data->addressCountryId = 1;
	        $data->addressStreet = '';
	        $data->addressCity = '';
	        $data->addressState = '';
	        $data->addressPin = '';
	        if (!isset($data->contactLandlinePrefix)) {
	            $data->contactLandlinePrefix = '';
	        }
	        if (!isset($data->contactLandline)) {
	            $data->contactLandline = '';
	        }
	        if (!isset($data->contactMobile)) {
	            $data->contactMobile = '';
	        }
	        if (!isset($data->contactMobilePrefix)) {
	            $data->contactMobilePrefix = '';
	        }
	        try {
	            $userDetails = new TableGateway('user_details', $this->adapter, null, new HydratingResultSet());
	            $userDetails->insert(array(
	                'userId' => $data->userId,
	                'contactMobilePrefix' => $data->contactMobilePrefix,
	                'contactMobile' => $data->contactMobile,
	                'contactLandlinePrefix' => $data->contactLandlinePrefix,
	                'contactLandline' => $data->contactLandline,
	                'addressCountry' => $data->addressCountryId,
	                'addressStreet' => $data->addressStreet,
	                'addressCity' => $data->addressCity,
	                'addressState' => $data->addressState,
	                'addressPin' => $data->addressPin
	            ));
			    // If all succeed, commit the transaction and all changes are committed at once.
			    $db->commit();
	            $result->status = 1;
	            return $result;
	        } catch (Exception $e) {
	            $result->status = 0;
	            $result->exception = $e->getMessage();
	            return $result;
	        }
        } catch (Exception $e) {
			$db->rollBack();
            $result = new stdClass();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
    
    public function addBasicDetails($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
        $result = new stdClass();
        if ($data->addressCountryId != '1') {
            $data->addressStreet = '';
            $data->addressCity = '';
            $data->addressState = '';
            $data->addressPin = '';
        }
        if (!isset($data->contactLandlinePrefix)) {
            $data->contactLandlinePrefix = '';
        }
        if (!isset($data->contactLandline)) {
            $data->contactLandline = '';
        }
        if (!isset($data->contactMobile)) {
            $data->contactMobile = '';
        }
        if (!isset($data->contactMobilePrefix)) {
            $data->contactMobilePrefix = '';
        }
        try {
            $userDetails = new TableGateway('user_details', $this->adapter, null, new HydratingResultSet());
            $userDetails->insert(array(
                'userId' => $data->userId,
                'contactMobilePrefix' => $data->contactMobilePrefix,
                'contactMobile' => $data->contactMobile,
                'contactLandlinePrefix' => $data->contactLandlinePrefix,
                'contactLandline' => $data->contactLandline,
                'addressCountry' => $data->addressCountryId,
                'addressStreet' => $data->addressStreet,
                'addressCity' => $data->addressCity,
                'addressState' => $data->addressState,
                'addressPin' => $data->addressPin
            ));
		    // If all succeed, commit the transaction and all changes are committed at once.
		    $db->commit();
            $result->status = 1;
            return $result;
        } catch (Exception $e) {
			$db->rollBack();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function completeRegistration($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
        $result = new stdClass();
        try {
            $loginDetails = new TableGateway('login_details', $this->adapter, null, new HydratingResultSet());
            $loginDetails->update(array(
                'id' => $data->userId,
                'registrationComplete' => 1,
            ));
		    // If all succeed, commit the transaction and all changes are committed at once.
		    $db->commit();
            $result->status = 1;
            return $result;
        } catch (Exception $e) {
			$db->rollBack();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function sendWelcomeEmail($data) {
        $result = new stdClass();
        try {
			$adapter = $this->adapter;
			$selectString = "SELECT L.email,L.username,
				case 
				when UR.roleId= 1 then (SELECT name FROM institute_details WHERE userId=L.id)
				when UR.roleId= 2 then (SELECT CONCAT(firstname, ' ', lastname)  FROM professor_details WHERE userId=L.id)
				when UR.roleId= 3 then (SELECT CONCAT(firstname, ' ', lastname)  FROM publisher_details WHERE userId=L.id)
				when UR.roleId= 4 then (SELECT CONCAT(firstname, ' ', lastname)  FROM student_details WHERE userId=L.id)
				else 'NA' end as Name,
				(SELECT name FROM user_role_details WHERE id=UR.roleId) as Role
				From login_details as L 
				Join user_roles as UR on UR.userId=L.id
				Where L.id=$data->userId";

            $loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $result = new stdClass();
            if ($loginDetails->count() == 0) {
                $result->status = 0;
                $result->message = "Invalid Username or Email.";
                return $result;
            }
            $loginDetails->email = $loginDetails->toArray();
            $data->email = $loginDetails->email[0]['email'];
            $data->loginUserName=$loginDetails->email[0]['username'];
            $data->name=$loginDetails->email[0]['Name'];
            $data->role=$loginDetails->email[0]['Role'];

            global $siteBase;
            
            $emailBody = "Dear  $data->name,<br><br>";

            $emailBody .= "Thank you for registering  a new account on Integro.io<br><br>";

            $emailBody .= "Your Username: $data->loginUserName <br>";

            $emailBody .= "$data->role Id : $data->userId <br>";
            
            $subject = 'Integro  Account creation email';
            $to = $data->email;
             
            $this->sendMail($to, $subject, $emailBody);
            
            $result->status = 1;
            return $result;
        } catch (Exception $e) {
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function sendVerificationEmail($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
        $result = new stdClass();
        try {
			// Need to make sure that token is unique
			$token = md5(uniqid($data->email . rand(), true));
			$accountVerification = new TableGateway('account_verification_tokens', $this->adapter, null, new HydratingResultSet());
			$accountVerification->insert(array(
				'userId' => $data->userId,
				'token' => $token,
				'createdAt' => $data->createdAt,
			));
		    // If all succeed, commit the transaction and all changes are committed at once.
		    $db->commit();
		    
			$adapter = $this->adapter;
			$selectString = "SELECT L.email,L.username,
				case 
				when UR.roleId= 1 then (SELECT name FROM institute_details WHERE userId=L.id)
				when UR.roleId= 2 then (SELECT CONCAT(firstname, ' ', lastname)  FROM professor_details WHERE userId=L.id)
				when UR.roleId= 3 then (SELECT CONCAT(firstname, ' ', lastname)  FROM publisher_details WHERE userId=L.id)
				when UR.roleId= 4 then (SELECT CONCAT(firstname, ' ', lastname)  FROM student_details WHERE userId=L.id)
				else 'NA' end as Name,
				(SELECT name FROM user_role_details WHERE id=UR.roleId) as Role
				From login_details as L 
				Join user_roles as UR on UR.userId=L.id
				Where L.id=$data->userId";

            $loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $result = new stdClass();
            if ($loginDetails->count() == 0) {
                $result->status = 0;
                $result->message = "Invalid Username or Email.";
                return $result;
            }
            $loginDetails->email = $loginDetails->toArray();
            $data->email = $loginDetails->email[0]['email'];
            $data->loginUserName=$loginDetails->email[0]['username'];
            $data->name=$loginDetails->email[0]['Name'];
            $data->role=$loginDetails->email[0]['Role'];

            global $siteBase;
            
            $emailBody = "Dear  $data->name,<br><br>";

            $emailBody .= "Thank you for registering  a new account on Integro.io<br><br>";

            $emailBody .= "To activate that account you can click on the link below"
                 . " or copy and paste the link in your browser's address bar<br><br>";

            $link = $siteBase . "verify/{$data->userId}/{$token}";

            $emailBody .= '<a href="'.$link.'">Click here to Verify your Account<a><br><br>';

            $emailBody .= "Your email: $data->email <br>";

            $emailBody .= "$data->role Id : $data->userId <br>";
            
            $subject = 'Integro  Account creation email';
            $to = $data->email;
             
            $this->sendMail($to, $subject, $emailBody);
            
            $result->status = 1;
            return $result;
        } catch (Exception $e) {
			$db->rollBack();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }
	
	public function resendVerificationLink($data) {
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			//fetching the token from database
			$select = $sql->select();
			$select->from('account_verification_tokens');
			$select->where(array(
				'userId'	=>	$data->id,
				//'used'		=>	0
			));
			$select->columns(array('token','used'));
			$selectString = $sql->getSqlStringForSqlObject($select);
			$token = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$token = $token->toArray();
			if(count($token) == 0) {
				$result->status = 0;
				$result->message = 'Your account is not found.';
				return $result;
			}
			$used = $token[0]['used'];
			if($used == 1) {
				$result->status = 0;
				$result->message = 'Your account is already verified.';
				return $result;
			}
			$token = $token[0]['token'];
			//fetching other information required
			$selectString = "SELECT L.email,L.username,
            case 
        	when UR.roleId= 1 then (select name from institute_details where userId=L.id)
                when UR.roleId= 2 then (select CONCAT(firstname, ' ', lastname)  from professor_details where userId=L.id)
                when UR.roleId= 3 then (select CONCAT(firstname, ' ', lastname)  from publisher_details where userId=L.id)
                when UR.roleId= 4 then (select CONCAT(firstname, ' ', lastname)  from student_details where userId=L.id)
                else 'NA' end as Name,
                (select name from user_role_details where id=UR.roleId) as Role
                From login_details as L 
                Join user_roles as UR on UR.userId=L.id
                Where L.id={$data->id}";

            $loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $result = new stdClass();
            if ($loginDetails->count() == 0) {
                $result->status = 0;
                $result->message = "Invalid Username or Email.";
                return $result;
            }
            $loginDetails->email = $loginDetails->toArray();
            $data->email = $loginDetails->email[0]['email'];
            $data->loginUserName=$loginDetails->email[0]['username'];
            $data->name=$loginDetails->email[0]['Name'];
            $data->role=$loginDetails->email[0]['Role'];
			
			//creating and sending email
			global $siteBase;
			$emailBody = "Dear  $data->name,<br><br>";
			$emailBody .= "Thank you for registering  a new account on Integro.io<br><br>";	
			$emailBody .= "To activate that account you can click on the link below or copy and paste the link in your browser's address bar<br><br>";
			$link = $siteBase . "marketplace/verify/{$data->id}/{$token}";
			$emailBody .= '<a href="'.$link.'">Click here to Verify your Account<a><br><br>';
			$emailBody .= "Your Username: {$data->loginUserName} <br>";
			$emailBody .= "{$data->role} Id : {$data->id} <br>";
			$subject = 'Integro  Account creation email';
			$to = $data->email;
			$this->sendMail($to, $subject, $emailBody);
            
            $result->status = 1;
            return $result;
		} catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

    public function validateAccount($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $where = array('userId' => $data->userId, 'token' => $data->token, 'used' => 0);
            $sql = new Sql($adapter);
            $update = $sql->update();
            $update->table('account_verification_tokens');
            $update->set(array('used' => 1));
            $update->where($where);
            $statement = $sql->prepareStatementForSqlObject($update);
            //var_dump($statement);
            $count = $statement->execute()->getAffectedRows();
            if ($count == 1) {
            	$db->beginTransaction();
                $update = $sql->update();
                $where = array('id' => $data->userId);
                $update->table('login_details');
                $update->set(array('validated' 	=>	1,
						'active'	=>	1
				));
                $update->where($where);
                $statement = $sql->prepareStatementForSqlObject($update);
                $count = $statement->execute()->getAffectedRows();
                
            	// setting default rate
                $query="SELECT roleId FROM user_roles WHERE userId = $data->userId AND roleId IN (1,2,3,4)";
                $roleId = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
                $roleId = $roleId->toArray();
                if(count($roleId) > 0) {
                    //$query='';
                    $roleId = $roleId[0]['roleId'];
                    if($roleId!=4){
                      $query="INSERT INTO course_key_rate (userId,key_rate) SELECT $data->userId,key_rate FROM course_key_default_rate ORDER BY id DESC LIMIT 1;";
                      $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
                    }
                    $result->userRole = $roleId;
                }
			    // If all succeed, commit the transaction and all changes are committed at once.
			    $db->commit();
                
                $result->status = 1;
                return $result;
            } else {
                $result->status = 0;
                $result->message = 'This url has expired or it never existed.';
                return $result;
            }
        } catch (Exception $e) {
			$db->rollBack();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function validateAccountShort($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $result = new stdClass();
        try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);
			$db->beginTransaction();
			$update = $sql->update();
			$where = array('id' => $data->userId);
			$update->table('login_details');
			$update->set(array('validated' 	=>	1,
					'active'	=>	1
			));
			$update->where($where);
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();

			// setting default rate
			$query="SELECT roleId FROM user_roles WHERE userId = $data->userId AND roleId IN (1,2,3,4)";
			$roleId = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$roleId = $roleId->toArray();
			if(count($roleId) > 0) {
			    //$query='';
			    $roleId = $roleId[0]['roleId'];
			    if($roleId!=4){
			      $query="INSERT INTO course_key_rate (userId,key_rate) SELECT $data->userId,key_rate FROM course_key_default_rate ORDER BY id DESC LIMIT 1;";
			      $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			    }
			    $result->userRole = $roleId;
			}
			// If all succeed, commit the transaction and all changes are committed at once.
			$db->commit();

			$result->status = 1;
			return $result;
        } catch (Exception $e) {
			$db->rollBack();
            $result->status = 0;
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function getRoleId($userId) {
        $adapter = $this->adapter;
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('user_roles');
        $select->columns(array('roleId'));
        $select->where(array('userId' => $userId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $userRole = $statement->execute();

        if ($userRole->count() !== 0) {
            return $userRole->next();
        } else {
            return 0;
        }
    }

    public function sendMail($to, $subject, $body) {
        //$bcc = "fraaz.ahmad786@gmail.com";
		$bcc = 'noreply.arcanemind.in@gmail.com';
        $bodyPart = new \Zend\Mime\Message();
        $emailTemplate=$this->emailtemplate($body);
        
        $bodyMessage = new \Zend\Mime\Part($emailTemplate);
        $bodyMessage->type = 'text/html';
        $bodyPart->setParts(array($bodyMessage));
        
		// $mail->setBodyText('Activation Link');
		// $mail->setBodyHtml($body);
        
        $mail = new Mail\Message();
        $mail->setFrom('no-reply@integro.io', 'Integro');
        $mail->addTo($to);
        $mail->addBcc($bcc);
        $mail->setBody($bodyPart);
        $mail->setSubject($subject);
        $mail->setEncoding("UTF-8");
        $transport = new Mail\Transport\Sendmail();
		if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
        	$transport->send($mail);
        }

        // Setup SMTP transport using LOGIN authentication
		/*$transport = new Mail\Transport\SmtpTransport();
		$options   = new Mail\Transport\SmtpOptions(array(
		    'name'              => 'email-smtp.us-west-2.amazonaws.com',
		    'host'              => '127.0.0.1',
    		'port'              => 587,
		    'connection_class'  => 'plain',
		    'connection_config' => array(
		        'username' => 'AKIAJ7EK5FKYGID3KDEQ',
		        'password' => 'AnmKWM7A/oI7lWNjPHIQKhyWVoVKSApkp43NxOsYMm/p',
        		'ssl'      => 'tls',
		    ),
		));
		$transport->setOptions($options);
		$transport->send($message);*/
    }

    public function sendMail2($to, $subject, $body, $sender) {
        //$bcc = "fraaz.ahmad786@gmail.com";
		$bcc = 'noreply.arcanemind.in@gmail.com';
        $bodyPart = new \Zend\Mime\Message();
        $emailTemplate=$this->emailtemplate2($sender, $body);
        
        $bodyMessage = new \Zend\Mime\Part($emailTemplate);
        $bodyMessage->type = 'text/html';
        $bodyPart->setParts(array($bodyMessage));
        
		// $mail->setBodyText('Activation Link');
		// $mail->setBodyHtml($body);
        
        $mail = new Mail\Message();
        $mail->setFrom('no-reply@integro.io', 'Integro');
        $mail->addTo($to);
        $mail->addBcc($bcc);
        $mail->setBody($bodyPart);
        $mail->setSubject($subject);
        $mail->setEncoding("UTF-8");
        $transport = new Mail\Transport\Sendmail();
		if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
        	$transport->send($mail);
        }

        // Setup SMTP transport using LOGIN authentication
		/*$transport = new Mail\Transport\SmtpTransport();
		$options   = new Mail\Transport\SmtpOptions(array(
		    'name'              => 'email-smtp.us-west-2.amazonaws.com',
		    'host'              => '127.0.0.1',
    		'port'              => 587,
		    'connection_class'  => 'plain',
		    'connection_config' => array(
		        'username' => 'AKIAJ7EK5FKYGID3KDEQ',
		        'password' => 'AnmKWM7A/oI7lWNjPHIQKhyWVoVKSApkp43NxOsYMm/p',
        		'ssl'      => 'tls',
		    ),
		));
		$transport->setOptions($options);
		$transport->send($message);*/
    }
	public function sendMailFrom($from, $to, $subject, $body) {
        //$bcc = "fraaz.ahmad786@gmail.com";
		$bcc = 'noreply.arcanemind.in@gmail.com';
        $bodyPart = new \Zend\Mime\Message();
        $emailTemplate=$this->emailtemplate($body);
        
        $bodyMessage = new \Zend\Mime\Part($emailTemplate);
        $bodyMessage->type = 'text/html';
        $bodyPart->setParts(array($bodyMessage));
        
        // $mail->setBodyText('Activation Link');
       // $mail->setBodyHtml($body);
        
        $mail = new Mail\Message();
        $mail->setFrom($from);
        $mail->addTo($to);
        $mail->addBcc($bcc);
        $mail->setBody($bodyPart);
        $mail->setSubject($subject);
        $mail->setEncoding("UTF-8");
        $transport = new Mail\Transport\Sendmail();
        $transport->send($mail);
    }
  
    public function changePassword($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
        try {
            $adapter = $this->adapter;
            $result = new stdClass();
            $oldPwd = sha1($data->oldPwd);
            $newPwd = sha1($data->newPwd);
            $sql = new Sql($adapter);
            $select = $sql->select();
            //Login Details
            $select->from('login_details');
            $select->columns(array('id'));
            $select->where
                    ->equalTo('id', $data->userId)
                    ->and
                    ->equalTo('password', $oldPwd);
            $selectString = $sql->getSqlStringForSqlObject($select);
            $loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $result->userId = $loginDetails->toArray();
            if (Count($result->userId) == 0) {
                $result->status = 0;
                $result->message = "You've entered wrong password";
                return $result;
            }
            $update = $sql->update();
            $update->table('login_details');
            $update->set(array('password' => $newPwd));
            $update->where(array('id' => $data->userId));
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
		    // If all succeed, commit the transaction and all changes are committed at once.
		    $db->commit();
            $result->status = 1;
            $result->message = "Password changed successfully";
            return $result;
        } catch (Exception $e) {
			$db->rollBack();
            $result->status = 0;
            $result->msg = 'You are not authorized ';
            $result->exception = $e->getMessage();
            return $result;
        }
    }

    public function getemailUsername($data) {
        $adapter = $this->adapter;
        $result = new stdClass();
        $sql = new Sql($adapter);
        $select = $sql->select();
        //Login Details
        $select->from('login_details');
        $select->columns(array('email', 'username'));
        $select->where
                ->equalTo('id', $data->userId);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        $result->email = $loginDetails->toArray();
        return $result;
    }

	public function getBasicUserDetails($data) {
		$adapter = $this->adapter;
		$result = new stdClass();
		$sql = new Sql($adapter);
		$select = $sql->select();
		//Login Details
		$select->from('login_details');
		$select->columns(array('email', 'username'));
		$select->where
		        ->equalTo('id', $data->userId);
		$selectString = $sql->getSqlStringForSqlObject($select);
		$loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
		$result->email = $loginDetails->toArray();
		$result->email = $result->email[0];
		$userRole = $this->getUserRole($data);
		if ($userRole->status == 1) {
			$result->userRole = $userRole->role;
			if ($result->userRole == 4) {
				require_once("Student.php");
				$s = new Student();
				$dataD = new stdClass();
				$dataD->userId = $data->userId;
				$dataD->userRole = $result->userRole;
				$studentDetails = $s->getBasicStudentDetails($dataD);
				if ($studentDetails->status == 1) {
					$result->details = $studentDetails->studentDetails;
				}
			} else {
				$result->details = array();
			}
		}
		return $result;
	}

    public function getUserRole($data) {
        $adapter = $this->adapter;
        $result = new stdClass();
        $sql = new Sql($adapter);
        $select = $sql->select();
        //Login Details
        $select->from('user_roles');
        $select->columns(array('roleId'));
        $select->where
                ->equalTo('userId', $data->userId);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        $loginDetails = $loginDetails->toArray();
        if (count($loginDetails) == 0) {
			$result->status = 0;
			$result->message = "No Role found!";
		} else {
			$result->status = 1;
			$result->role = $loginDetails[0]['roleId'];
		}
        return $result;
    }

    public function getSocialConnections($data)
    {
        $adapter = $this->adapter;
        $result = new stdClass();
        $sql = new Sql($adapter);
        $select = $sql->select();
        //Login Details
        $select->from('login_details');
        $select->columns(array('facebookid'));
        $select->where
                ->equalTo('id', $data->userId);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
        $result->socials = $loginDetails->toArray();
        $result->socials = $result->socials[0];
        return $result;
    }

	public function setSocialConnection($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$where = array();
			if ($data->socialAction == 'disconnect') {
				$values = array('facebookid' => 0);
			} else if ($data->socialAction == 'connect') {
				$values = array('facebookid' => $data->facebookid);
			}
			if (!empty($where)) {
				$sql = new Sql($adapter);
				$update = $sql->update();
				$update->table('login_details');
				$update->set($values);
				$update->where(array('id' => $data->userId));
				$statement = $sql->prepareStatementForSqlObject($update);
				$result = $statement->execute();
			}
		    // If all succeed, commit the transaction and all changes are committed at once.
		    $db->commit();
			$result->status = 1;
		    return $result;
		} catch (Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = 'You are not authorized';
			$result->exception = $e->getMessage();
			return $result;
		}
	}

    public function emailtemplate($emaildata) {
        global $siteBase;
        $html = '<table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tbody><tr><td height="30"></td></tr>
        <tr bgcolor="#F1F2F7">
        <td width="100%" valign="top" bgcolor="#F1F2F7" align="center">
        <!-- main content -->
        <table cellspacing="0" cellpadding="0" width="600" border="0" bgcolor="ffffff" align="center" class="container">


        <!--Header-->
        <tbody><tr><td height="40"></td></tr>

        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" width="560" border="0" align="center" class="container-middle">
                    <tbody><tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" border="0" align="center" class="logo">
                                <tbody><tr>
                                    <td align="center">
                                        <a href="" style="display: block;"><img width="155" style="display: block;" src="'.$siteBase.'img/email-img/vector-lab.png" alt="logo"></a>
                                    </td>
                                </tr>
                                </tbody></table>

                        </td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        <!-- end header -->


        <!-- main section -->
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" width="560" border="0" align="center" class="container-middle">
                    <tbody>
                    <!--<tr><td align="center"><img width="538" height="auto" alt="large image" src="'.$siteBase.'img/email-img/main-img.png" class="main-image" style="display: block;"></td></tr>-->
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" width="528" border="0" align="center" class="mainContent">
                                <tbody><tr>
                                    <td class="main-header" style="color: #484848; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr><td height="20"></td></tr>
                                <tr>
                                    <td class="main-subheader" style="color: #333; font-size: 16px; font-weight: normal; line-height:16px; font-family: Helvetica, Arial, sans-serif;">
                                    '.$emaildata.'
        							</td>
                                </tr>
                                <tr><td height="20"></td></tr>
        <tr><td height="20"></td></tr>
        <tr><td height="20" style="color: #333; font-size: 16px; line-height:16px;"><b>Best Regards,</b></td></tr>
        <tr><td height="20" style="color: #333; font-size: 16px; line-height:16px;">Integro  Team</td></tr>
        <tr><td height="20" style="color: #333; font-size: 16px; line-height:16px;"><a href="'.$siteBase.'">Integro.io</a></td></tr>
                                </tbody></table>
                        </td>
                    </tr>

                    <tr><td height="25"></td></tr>


                   </tbody></table>
            </td>
        </tr>
        <!-- end main section -->


        <tr><td height="35"></td></tr>

        <!-- prefooter -->

        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" width="560" border="0" align="center" class="container-middle">
                    <tbody><tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" border="0" align="center" class="nav">
                                <tbody><tr><td height="10"></td></tr>
                                <tr>
                                    <td align="center" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;">
                                        <table cellspacing="0" cellpadding="0" border="0" align="center">
                                            <tbody><tr>
                                                <td>
                                                    <a style="display: block; width: 16px;" href="https://twitter.com/integro_io"><img width="16" style="display: block;" src="'.$siteBase.'img/email-img/social-twitter.png" alt="twitter"></a>
                                                </td>
                                                <td>&nbsp;&nbsp;&nbsp;</td>
                                                <td>
                                                    <a style="display: block; width: 16px;" href="https://www.linkedin.com/company/integro-io/"><img width="16" style="display: block;" src="'.$siteBase.'img/email-img/social-linkedin.png" alt="linkedin"></a>
                                                </td>
                                            </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        <!-- end prefooter  -->

        <tr><td height="20"></td></tr>
        <!--<tr>
            <td align="center" style="color: #939393; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;" class="prefooter-header">

                You are currently signed up to Company’s newsletters as: <span style="color: #f68731">arcanemind@gmail.com</span> to unsubscribe <a style="text-decoration: none; color: #f68731;" href="#">click here</a>

            </td>
        </tr>-->

        <tr><td height="30"></td></tr>

        <tr>
            <td align="center" style="color: #939393; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;" class="prefooter-subheader">

                <span style="color: ##045597">Address :</span> 393, Mangaldas House, Naaz Cinema Compound, Below I.O. Bank, Mumbai-400004, INDIA &nbsp;&nbsp;&nbsp;   <span style="color: ##045597">Phone :</span> +91-​9741436024​  &nbsp;&nbsp;&nbsp;<span style="color: #f68731">Email :</span> <a href="mailto:contactus@integro.io">contactus@integro.io</a>


            </td>
        </tr>

        <tr><td height="30"></td></tr>
        </tbody></table>
        <!--end main Content -->


        <!-- footer -->
        <table cellspacing="0" cellpadding="0" width="600" border="0" class="container">
            <tbody>
            <tr bgcolor="#045597"><td height="15"></td></tr>
            <tr bgcolor="#045597">
                <td align="center" style="color: #fff; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">

                    Integro &copy; Copyright 2016 . All Rights Reserved

                </td>
            </tr>

            <tr>
                <td height="14" bgcolor="#045597"></td>
            </tr>
            </tbody></table>
        <!-- end footer-->
        </td>
        </tr>

        <tr><td height="30"></td></tr>

        </tbody>
        </table>';
        
       return $html; 
        
    }

    public function emailtemplate2($emailsender,$emaildata) {
        global $siteBase;
        $html = '<table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tbody><tr><td height="30"></td></tr>
        <tr bgcolor="#F1F2F7">
        <td width="100%" valign="top" bgcolor="#F1F2F7" align="center">
        <!-- main content -->
        <table cellspacing="0" cellpadding="0" width="600" border="0" bgcolor="ffffff" align="center" class="container">


        <!--Header-->
        <tbody><tr><td height="40"></td></tr>

        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" width="560" border="0" align="center" class="container-middle">
                    <tbody><tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" border="0" align="center" class="logo">
                                <tbody><tr>
                                    <td align="center">
                                        <a href="" style="display: block;"><img width="155" style="display: block;" src="'.$siteBase.'img/email-img/vector-lab.png" alt="logo"></a>
                                    </td>
                                </tr>
                                </tbody></table>

                        </td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        <!-- end header -->


        <!-- main section -->
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" width="560" border="0" align="center" class="container-middle">
                    <tbody>
                    <!--<tr><td align="center"><img width="538" height="auto" alt="large image" src="'.$siteBase.'img/email-img/main-img.png" class="main-image" style="display: block;"></td></tr>-->
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" width="528" border="0" align="center" class="mainContent">
                                <tbody><tr>
                                    <td class="main-header" style="color: #484848; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr><td height="20"></td></tr>
                                <tr>
                                    <td class="main-subheader" style="color: #333; font-size: 16px; font-weight: normal; line-height:16px; font-family: Helvetica, Arial, sans-serif;">
                                    '.$emaildata.'
        							</td>
                                </tr>
                                <tr><td height="20"></td></tr>
        <tr><td height="20"></td></tr>
        <tr><td height="20" style="color: #333; font-size: 12px; line-height:12px;">Note: This mail has been sent by '.$emailsender.', and is powered by <a href="'.$siteBase.'">Integro.io</a></td></tr>
                                </tbody></table>
                        </td>
                    </tr>

                    <tr><td height="25"></td></tr>


                   </tbody></table>
            </td>
        </tr>
        <!-- end main section -->


        <tr><td height="35"></td></tr>

        <!-- prefooter -->

        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" width="560" border="0" align="center" class="container-middle">
                    <tbody><tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" border="0" align="center" class="nav">
                                <tbody><tr><td height="10"></td></tr>
                                <tr>
                                    <td align="center" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;">
                                        <table cellspacing="0" cellpadding="0" border="0" align="center">
                                            <tbody><tr>
                                                <td>
                                                    <a style="display: block; width: 16px;" href="http://www.twitter.com/ArcanemindLive"><img width="16" style="display: block;" src="'.$siteBase.'img/email-img/social-twitter.png" alt="twitter"></a>
                                                </td>
                                                <td>&nbsp;&nbsp;&nbsp;</td>
                                                <td>
                                                    <a style="display: block; width: 16px;" href="https://www.linkedin.com/company/arcanemind-com"><img width="16" style="display: block;" src="'.$siteBase.'img/email-img/social-linkedin.png" alt="linkedin"></a>
                                                </td>
                                            </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        <!-- end prefooter  -->

        <tr><td height="20"></td></tr>
        <!--<tr>
            <td align="center" style="color: #939393; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;" class="prefooter-header">

                You are currently signed up to Company’s newsletters as: <span style="color: #f68731">arcanemind@gmail.com</span> to unsubscribe <a style="text-decoration: none; color: #f68731;" href="#">click here</a>

            </td>
        </tr>-->

        <tr><td height="30"></td></tr>

        <tr>
            <td align="center" style="color: #939393; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;" class="prefooter-subheader">

                <span style="color: ##045597">Address :</span> 393, Mangaldas House, Naaz Cinema Compound, Below I.O. Bank, Mumbai-400004, INDIA &nbsp;&nbsp;&nbsp;   <span style="color: ##045597">Phone :</span> +91-​9741436024​  &nbsp;&nbsp;&nbsp;<span style="color: #f68731">Email :</span> <a href="mailto:contactus@integro.io">contactus@integro.io</a>


            </td>
        </tr>

        <tr><td height="30"></td></tr>
        </tbody></table>
        <!--end main Content -->


        <!-- footer -->
        <table cellspacing="0" cellpadding="0" width="600" border="0" class="container">
            <tbody>
            <tr bgcolor="#045597"><td height="15"></td></tr>
            <tr bgcolor="#045597">
                <td align="center" style="color: #fff; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">

                    Integro &copy; Copyright 2016 . All Rights Reserved

                </td>
            </tr>

            <tr>
                <td height="14" bgcolor="#045597"></td>
            </tr>
            </tbody></table>
        <!-- end footer-->
        </td>
        </tr>

        <tr><td height="30"></td></tr>

        </tbody>
        </table>';
        
       return $html; 
        
    }
	
	public function contactUsEmail($data) {
		$result = new stdClass();
		try {
			//sending contact us email to arcanemind
			$subject = 'Enquiry from website';
			$to = 'contactus@integro.io';
			$body = "Hello admin, <br>
					We have received a new enquiry from website, details are<br>
					Name : {$data->name}<br>
					Email : {$data->email}<br>
					Phone : {$data->number}<br>
					Subject : {$data->subject}<br>
					Message : {$data->message}<br>";
			$this->sendMail($to, $subject, $body);
			
			//now sending confirmation mail to user
			$subject = 'Acknowledgement email - Integro';
			$to = $data->email;
			$body = "Hello {$data->name},<br><br>
					Thanks for enquiring with Integro. We will get back to you soon regarding your query.";
			$this->sendMail($to, $subject, $body);
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

    public function getNotificationCount($data) {
        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('notifications');
            $select->where(array(
                'userId' => $data->userId,
                'status' => 0
            ));
            $select->columns(array('notifications' => new \Zend\Db\Sql\Expression("COUNT(*)")));
            $selectString = $sql->getSqlStringForSqlObject($select);
            $count = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $count = $count->toArray();

            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('chat_notifications');
            $select->where(array(
                'userId' => $data->userId,
                'status' => 0
            ));
            $select->columns(array('notifications' => new \Zend\Db\Sql\Expression("COUNT(*)")));
            $selectString = $sql->getSqlStringForSqlObject($select);
            $count1 = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $count1 = $count1->toArray();

            if(count($count) > 0)
                $result->count = $count[0]['notifications']+$count1[0]['notifications'];
            else
                $result->count = 0;
            $result->status = 1;
            return $result;
        }catch(Exception $e) {
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }

    public function completePaypalPaymentUsingIPN($invoice, $amount, $txid, $productinfo = NULL) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('order_details')->where(array('orderId' => $invoice, 'payUId' => 0));
            $selectString = $sql->getSqlStringForSqlObject($select);
            $order = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $order = $order->toArray();
            if(count($order) <= 0) {
                $result->status = 0;
                $result->message = 'No such order found';
                return $result;
            }
            $order = $order[0];

            //now insert a row in paypal_details
            //$paypal = new TableGateway('paypal_details', $adapter, null, new HydratingResultSet());
            $insert = array(
                'paypalId'  =>  $txid,
                'amount'    =>  $amount,
                'timestamp' =>  time()
            );
            //$paypal->insert($insert);
            //$paypalId = $paypal->getLastInsertValue();

			$query="INSERT INTO paypal_details (`".implode("`,`", array_keys($insert))."`) VALUES ('".implode("','", array_values($insert))."')";
			$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
			$adapter->query("SET @paypalId = LAST_INSERT_ID();", $adapter::QUERY_MODE_EXECUTE);

            //updating the value of order with paypal details mapping
            /*$update = $sql->update();
            $update->table('order_details');
            $update->set(array(
                //'payUId'    =>  $paypalId,
                'status'    =>  'E000'
            ));
            $update->where(array('orderId' => $invoice));
            $statement = $sql->getSqlStringForSqlObject($update);
            $adapter->query($statement, $adapter::QUERY_MODE_EXECUTE);*/
            $query="UPDATE order_details SET `payUId`=@paypalId,`status`='E000' WHERE orderId=$invoice";
			$adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
		    // If all succeed, commit the transaction and all changes are committed at once.
		    $db->commit();

            if($amount == $order['amount']){
                if($order['type'] == 1) {
                    //actually purchase the course key
                    $send = new stdClass();
                    $send->key_user = $order['userId'];
                    $send->keys = $order['purchase_detail'];
                    require_once 'Coursekeys.php';
                    $c = new Coursekeys();
                    $c->purchase_key($send);
                }
                else if($order['type'] == 2){
                    //actuall purchase the course
                    $send = new stdClass();
                    $send->courseId = $order['purchase_detail'];
                    $send->price = $order['amount'];
                    $send->userRole = '4';
                    $send->userId = $order['userId'];
                    require_once 'StudentInvitation.php';
                    $s = new StudentInvitation();
                    $s->linkStudentCourse($send, $invoice);
                }
            }
            //updating coupons  
            
            /*$userData1 = json_decode($productinfo);
            
            
            if(  $userData1->couponCode != ''  && $userData1->courseId != ''){
                require_once('Course.php');
                $co = new Course();
                $redeemdata = new stdClass();
                $redeemdata->courseId  = $userData1->courseId;
                $redeemdata->couponCode = $userData1->couponCode;
                $countervalue=$co->redeemCoupon($redeemdata);
                $noCoupons=$countervalue['couponsUsed'];
                $redeemdata->couponsUsed=$noCoupons+1;
                $update=$co->updateCoupon($redeemdata);
            }*/
            $result->status = 1;
            return $result;
        }catch(Exception $e) {
			$db->rollBack();
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }

    public function orderUser($data) {
        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            $select = $sql->select();
            /*$selectString ="SELECT od.`userId`, urd.name AS `userRole`
                            FROM  `order_details` AS od
                            INNER JOIN `user_roles` AS ur ON ur.userId=od.userId
                            INNER JOIN `user_role_details` AS urd ON urd.id=ur.roleId
                            WHERE `orderId`={$data->orderId}";*/
            $select->from(array('od'  =>  'order_details'))->where(array('orderId' => $data->orderId));
            $select->join(array('ur'  =>  'user_roles'),'ur.userId=od.userId', array('userRole'=> 'roleId'));
            $select->join(array('ld'  =>  'login_details'),'ld.id=od.userId', array('email'=> 'email'));
            $select->columns(array('userId','type'));
            $selectString = $sql->getSqlStringForSqlObject($select);
            $order = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $order = $order->toArray();
            if(count($order) <= 0) {
                $result->status = 0;
                $result->message = 'No such order found';
                return $result;
            }
            $order = $order[0];

            //$result->order    = $order;
            $_SESSION["userId"]   = $order["userId"];
            $_SESSION["userRole"] = $order["userRole"];
            $_SESSION["username"] = $order["email"];
			$_SESSION["type"] = $order["type"];
            $result->userId   = $order["userId"];
            $result->userRole = $order["userRole"];
			$result->type = $order["type"];
            $result->status = 1;
            return $result;
        }catch(Exception $e) {
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }
    public function generateParents($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            if (!empty($data->students)) {
                $students = explode(",", $data->students);
                $parents  = array();
                $result->students = array();
                foreach ($students as $key => $value) {
                    $select = $sql->select();
                    $select->from('login_details');
                    $select->where
                            ->equalTo('id', $value);
                    $selectString = $sql->getSqlStringForSqlObject($select);
                    $studentCheck = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                    $studentCheck = $studentCheck->toArray();
                    if (count($studentCheck)>0) {
                        $select = $sql->select();
                        $select->from('parent_login_short');
                        $select->where
                                ->equalTo('student_id', $value)
                                ->and
                                ->equalTo('course_id', $data->course_id);
                        $selectString = $sql->getSqlStringForSqlObject($select);
                        $userCheck = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                        $userCheck = $userCheck->toArray();
                        if (count($userCheck) == 0) {
                            do {
                                $userid = mt_rand(10000, 99999);
                                $select = $sql->select();
                                $select->from('parent_login_short');
                                $select->where->equalTo('userid', $userid);
                                $selectString = $sql->getSqlStringForSqlObject($select);
                                $userDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                                $userDetails = $userDetails->toArray();
                            } while(count($userDetails)>0);
                            // Need to make sure that token is unique
                            $passwd = base64_encode(mt_rand(10000, 99999));
                            $setParent = new TableGateway('parent_login_short', $adapter, null, new HydratingResultSet());
                            $parent = array(
                                'userid' => $userid,
                                'passwd' => $passwd,
                                'student_id' => $value,
                                'course_id' => $data->course_id,
                                'active' => 1,
                                'createdAt' => time()
                            );
                            $setParent->insert($parent);
                            $result->students[] = array(
                                                    'student_id'=>$value,
                                                    'userid'=>$userid,
                                                    'passwd'=>base64_decode($passwd)
                                                    );
                        }
                    }
                }
            }
		    // If all succeed, commit the transaction and all changes are committed at once.
		    $db->commit();

            $result->status = 1;
            return $result;
        }catch(Exception $e) {
			$db->rollBack();
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }

    public function getCountriesList() {
        $result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            
            $select = $sql->select();
            $select->from('countries');
            $select->order('name');
            $selectString = $sql->getSqlStringForSqlObject($select);
            $countries = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $countries = $countries->toArray();
            foreach ($countries as $key=>$country) {
            	$countries[$key]['name'] = addslashes($countries[$key]['name']);
            }
            $result->countries = $countries;
            /*var_dump(json_last_error(), json_last_error_msg());
            die();*/

            $result->status = 1;
            return $result;
        }catch(Exception $e) {
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
    }

	public function exitCTA($data) {

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			if (empty($data->email) && empty($data->phone)) {
				$result->status = 0;
			} else if (!empty($data->email) && !filter_var($data->email, FILTER_VALIDATE_EMAIL)) {
				$result->status = 0;
			} else {
				$remote = new Zend\Http\PhpEnvironment\RemoteAddress;
				$exitDetails = new TableGateway('exit_cta', $this->adapter, null, new HydratingResultSet());
				$exitDetails->insert(array(
					'email' => $data->email,
					'phone' => $data->phone,
					'ip_address' => $remote->getIpAddress(),
					'created' => date("Y-m-d H:i:s")
				));
			    // If all succeed, commit the transaction and all changes are committed at once.
			    $db->commit();
				if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
					//sending contact us email to arcanemind
					$subject = 'Enquiry from website';
					$to = 'contactus@integro.io';
					$body = "Hello admin,<br>
							We have received a new enquiry from website, details are<br>
							Email : {$data->email}<br>
							Phone : {$data->phone}<br>";
					$this->sendMail($to, $subject, $body);
					
					if (!empty($data->email)) {
						//now sending confirmation mail to user
						$subject = 'Acknowledgement email - Integro';
						$to = $data->email;
						$body = "Hello,<br><br>
								Thanks for enquiring with Integro. We will get back to you soon.";
						$this->sendMail($to, $subject, $body);
					}
				}
			}
			$result->status = 1;
			return $result;
		}catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function getSmileys()
	{
		$result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            
            $select = $sql->select();
            $select->from('smileys');
            $selectString = $sql->getSqlStringForSqlObject($select);
            $smileys = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $smileys = $smileys->toArray();
            $result->smileys = $smileys;

            $result->status = 1;
            return $result;
        }catch(Exception $e) {
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
	}
	public function getAccountTerminology($data)
	{
		$result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            
            $select = $sql->select();
            $select->from('account_terminologies');
            $select->where(array('userId'=>$data->userId));
            $selectString = $sql->getSqlStringForSqlObject($select);
            $accData = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $accData = $accData->toArray();
            if (count($accData) == 0) {
            	/*$result->status = 0;
            	return $result;*/
            	$result->terminology = array(
            						"userId" => $data->userId,
            						"account_type" => 'institute',            						
            						"institute_single" => 'Institute',
            						"instructor_single" => 'Instructor',
            						"student_single" => 'Student',
            						"parent_single" => 'Guardian',
            						"course_single" => 'Course',
            						"subject_single" => 'Subject',
            						"syllabus_single" => 'Syllabus',            						
            						"institute_plural" => 'Institutes',
            						"instructor_plural" => 'Instructors',
            						"student_plural" => 'Students',
            						"parent_plural" => 'Guardians',
            						"course_plural" => 'Courses',
            						"subject_plural" => 'Subjects',
            						"syllabus_plural" => 'Syllabus',
            						);
            } else {
            	$result->terminology = $accData[0];
            }
            $result->status = 1;
            return $result;
        }catch(Exception $e) {
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
	}
	public function setAccountTerminology($data)
	{

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
        try {
            $adapter = $this->adapter;
            $sql = new Sql($adapter);
            
            $select = $sql->select();
            $select->from('account_terminologies');
            $select->where(array('userId'=>$data->userId));
            $selectString = $sql->getSqlStringForSqlObject($select);
            $accData = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
            $accData = $accData->toArray();
            if (count($accData) == 0) {
            	$terminology = new TableGateway('account_terminologies', $this->adapter, null, new HydratingResultSet());
            	$insert = array(
					'userId' => $data->userId,
					'account_type' => $data->account_type,
					'institute_single' => $data->institute_single,
					'instructor_single' => $data->instructor_single,
					'student_single' => $data->student_single,
					'parent_single' => $data->parent_single,
					'course_single' => $data->course_single,
					'subject_single' => $data->subject_single,
					'syllabus_single' => $data->syllabus_single,
					'institute_plural' => $data->institute_plural,
					'instructor_plural' => $data->instructor_plural,
					'student_plural' => $data->student_plural,
					'parent_plural' => $data->parent_plural,
					'course_plural' => $data->course_plural,
					'subject_plural' => $data->subject_plural,
					'syllabus_plural' => $data->syllabus_plural
				);
				$terminology->insert($insert);
            } else {
            	$update = $sql->update();
		        $update->table('account_terminologies');
		        $accUpdate = array(
					'userId' => $data->userId,
					'account_type' => $data->account_type,
					'institute_single' => $data->institute_single,
					'instructor_single' => $data->instructor_single,
					'student_single' => $data->student_single,
					'parent_single' => $data->parent_single,
					'course_single' => $data->course_single,
					'subject_single' => $data->subject_single,
					'syllabus_single' => $data->syllabus_single,
					'institute_plural' => $data->institute_plural,
					'instructor_plural' => $data->instructor_plural,
					'student_plural' => $data->student_plural,
					'parent_plural' => $data->parent_plural,
					'course_plural' => $data->course_plural,
					'subject_plural' => $data->subject_plural,
					'syllabus_plural' => $data->syllabus_plural
				);
		        $update->set($accUpdate);
		        $update->where(array('userId' => $data->userId));
		        $statement = $sql->prepareStatementForSqlObject($update);
		        $statement->execute();
            }
		    // If all succeed, commit the transaction and all changes are committed at once.
		    $db->commit();

            $result->status = 1;
            return $result;
        }catch(Exception $e) {
			$db->rollBack();
            $result->status = 0;
            $result->message = $e->getMessage();
            return $result;
        }
	}
	public function deletePersonalData($data)
	{

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			switch ($data->userRole) {
				case '1':

					$coverPic = $profilePic = $tagline = "";

					$query = "EXPLAIN `institute_details`";
					$userDetails = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$userDetails = $userDetails->toArray();
					foreach ($userDetails as $key => $userDetail) {
						if ($userDetail["Field"] == "coverPic") {
							$coverPic = $userDetail["Default"];
						}
						if ($userDetail["Field"] == "profilePic") {
							$profilePic = $userDetail["Default"];
						}
						if ($userDetail["Field"] == "tagline") {
							$tagline = $userDetail["Default"];
						}
					}
					$userUpdateDetails = array(
						'coverPic' => $coverPic,
						'profilePic' => $profilePic,
						'name' => '',
						'ownerFirstName' => '',
						'ownerLastName' => '',
						'contactFirstName' => '',
						'contactLastName' => '',
						'altContact' => '',
						'altEmail' => '',
						'foundingYear' => 0,
						'description' => '',
						'studentCount' => '',
						'tagline' => $tagline
					);
					$update = $sql->update();
					$update->table('institute_details');
					$update->set($userUpdateDetails);
					$update->where(array('userId' => $data->userId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$count = $statement->execute()->getAffectedRows();
					break;
				case '2':

					$coverPic = $profilePic = "";

					$query = "EXPLAIN `professor_details`";
					$userDetails = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$userDetails = $userDetails->toArray();
					foreach ($userDetails as $key => $userDetail) {
						if ($userDetail["Field"] == "coverPic") {
							$coverPic = $userDetail["Default"];
						}
						if ($userDetail["Field"] == "profilePic") {
							$profilePic = $userDetail["Default"];
						}
					}
					$userUpdateDetails = array(
						'coverPic' => $coverPic,
						'profilePic' => $profilePic,
						'firstName' => '',
						'lastName' => '',
						'gender' => '0',
						'dob' => '',
						'description' => ''
					);
					$update = $sql->update();
					$update->table('professor_details');
					$update->set($userUpdateDetails);
					$update->where(array('userId' => $data->userId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$count = $statement->execute()->getAffectedRows();
					break;
				case '4':

					$coverPic = $profilePic = "";

					$query = "EXPLAIN `student_details`";
					$userDetails = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
					$userDetails = $userDetails->toArray();
					foreach ($userDetails as $key => $userDetail) {
						if ($userDetail["Field"] == "coverPic") {
							$coverPic = $userDetail["Default"];
						}
						if ($userDetail["Field"] == "profilePic") {
							$profilePic = $userDetail["Default"];
						}
					}
					$userUpdateDetails = array(
						'coverPic' => $coverPic,
						'profilePic' => $profilePic,
						'firstName' => '',
						'lastName' => '',
						'gender' => '0',
						'dob' => '',
						'background' => '',
						'achievements' => '',
						'interests' => ''
					);
					$update = $sql->update();
					$update->table('student_details');
					$update->set($userUpdateDetails);
					$update->where(array('userId' => $data->userId));
					$statement = $sql->prepareStatementForSqlObject($update);
					$count = $statement->execute()->getAffectedRows();
					break;
				default:
					break;
			}
			
			$userUpdateDetails = array(
				'contactMobilePrefix' => '',
				'contactMobile' => '',
				'contactLandlinePrefix' => '',
				'contactLandline' => '',
				'addressCountry' => '',
				'addressState' => '',
				'addressCity' => '',
				'addressStreet' => '',
				'addressPin' => '',
				'slug' => '',
				'slug_status' => '',
				'website' => '',
				'facebook' => '',
				'twitter' => ''
			);
			$update = $sql->update();
			$update->table('user_details');
			$update->set($userUpdateDetails);
			$update->where(array('userId' => $data->userId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
		    // If all succeed, commit the transaction and all changes are committed at once.
		    $db->commit();

			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
	public function deleteAccount($data)
	{

        $db = $this->adapter->getDriver()->getConnection();
        
        $db->beginTransaction();
		$result = new stdClass();
		try {
			$adapter = $this->adapter;
			$sql = new Sql($adapter);

			$this->deletePersonalData($data);

			$userUpdateDetails = array(
				'email' => '',
				'facebookid' => '',
				'googleid' => '',
				'linkedinid' => '',
				'active' => 0
			);
			$update = $sql->update();
			$update->table('login_details');
			$update->set($userUpdateDetails);
			$update->where(array('id' => $data->userId));
			$statement = $sql->prepareStatementForSqlObject($update);
			$count = $statement->execute()->getAffectedRows();
		    // If all succeed, commit the transaction and all changes are committed at once.
		    $db->commit();

			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}

	public function getVerifiedStatus($data) {
		try {
			$adapter = $this->adapter;
			$result = new stdClass();
			$sql = new Sql($adapter);
			$select = $sql->select();
			//Login Details
			$select->from('login_details');
			$select->columns(array('validated'));
			$select->where
			        ->equalTo('id', $data->userId);
			$selectString = $sql->getSqlStringForSqlObject($select);
			$loginDetails = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
			$loginDetails = $loginDetails->toArray();
			$result->validated = $loginDetails[0]['validated'];
			$userRole = $this->getUserRole($data);
			if ($userRole->status == 1) {
				$result->userRole = $userRole->role;
			}
			$result->status = 1;
			return $result;
		} catch(Exception $e) {
			$db->rollBack();
			$result->status = 0;
			$result->message = $e->getMessage();
			return $result;
		}
	}
}

?>