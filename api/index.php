<?php
	try{
		/**
		* This makes our life easier when dealing with paths. Everything is relative
		* to the application root now.
		*/
		//~ ini_set('display_errors',1);
		//~ ini_set('display_startup_errors',1);
		//~ error_reporting(-1);
		chdir(dirname(__DIR__));

		$libPath = "Vendor";

		// Loading Zend
		include $libPath . '/Zend/Loader/AutoloaderFactory.php';
		Zend\Loader\AutoloaderFactory::factory(array(
				'Zend\Loader\StandardAutoloader' => array(
						'autoregister_zf' => true,
						'db' => 'Zend\Db\Sql'
				)
		));

		// Get the JSON request
		$req = json_decode(file_get_contents("php://input"));

		//Kill the request if action is not specified
		if(!isset($req->action)) {
			$res = new stdClass();
			$res->status = 0;
			$res->message = "No action specified";
			echo json_encode($res);
			die();
		}

		require $libPath . "/ArcaneMind/Api.php";

		//Used for verification email
		$siteBase = "http://localhost/arcanemind/";


		$action = $req->action;
		switch($action) {
			case 'search-page':
				$res = Api::searchCourseForPage($req);
				break;
			case 'search-course':
				$res = Api::searchCourse($req);
				break;
			case 'featured-template-courses':
				$res = Api::getFeaturedTemplateCourse($req);
				break;
			case 'template-courses':
				$res = Api::getTemplateCourse($req);
				break;
			case 'student-template-courses':
				$res = Api::getStudentTemplateCourse($req);
				break;
			case 'student-template-free':
				$res = Api::getStudentFreeTemplateCourse($req);
				break;
			case 'student-template-EntranceExamcourses':
				$res = Api::getStudentEntranceExamTemplateCourse($req);
				break;
			case 'student-template-schoolPrepcourses':
				$res = Api::getStudentschoolPrepTemplateCourse($req);
				break;
			case 'student-template-hobbies_skills':
				$res = Api::getstudentHobbies_skills($req);
				break;
			case 'student-template-Business_Management':
				$res = Api::getstudentBusiness_Management($req);
				break;				
			case 'student-template-IT_Software':
				$res = Api::getStudentIT_Software($req);
				break;
			case 'student-templateEntrepreneurship':
				$res = Api::getstudentEntrepreneurship($req);
				break;
			case 'student-templateMiscellaneous':
				$res = Api::getstudentMiscellaneous($req);
				break;				
			case 'content-template-courses':
				$res = Api::getContentTemplateCourse($req);
				break;
			case 'set-content-watched':
				session_start();
				$req->userId   = 	$_SESSION["userId"];
				$req->userRole = 	$_SESSION["userRole"];
				$res = Api::setContentWatched($req);
				break;
			case 'page-template-courses':
				$res = Api::getPageTemplateCourse($req);
				break;
			case 'get-verified-status':
				$res = Api::getVerifiedStatus($req);
				break;
			case 'register-short':
				$res = Api::registerUserShort($req);
				break;
			case 'register-social':
				$res = Api::registerUserSocial($req);
				break;
			case 'register':
				$res = Api::registerUser($req);
				break;
			case 'resend-verification-link':
				$res = Api::resendVerificationLink($req);
				break;
			case 'validate-email':
				$res = Api::validateEmail($req);
				break;
			case 'validate-username':
				$res = Api::validateUsername($req);
				break;
			case 'account-validation':
				$res = Api::validateAccount($req);
				break;
			case 'verify-userid':
				$res = Api::verifyUserID($req);
				break;
			case 'get-profile-details':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION["userRole"];
				if(isset($req->instituteId)){
					$req->userId = $req->instituteId;
					$req->userRole = 1;
				}
				else if(isset($req->professorId)){
					$req->userId = $req->professorId;
					$req->userRole = 2;
				}
				$res = Api::getProfileDetails($req);
				$res->userRole = $req->userRole;
				break;
			case 'get-profile-stats':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION["userRole"];
				if(isset($req->instituteId)){
					$req->userId = $req->instituteId;
					$req->userRole = 1;
				}
				else if(isset($req->professorId)){
					$req->userId = $req->professorId;
					$req->userRole = 2;
				}
				$res = Api::getProfileStats($req);
				$res->userRole = $req->userRole;
				break;					
			case 'edit-tagline':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::saveTagline($req);
				break;
			case 'get-notification-count':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION["userRole"];
				$res = Api::getNotificationCount($req);
				break;
			case 'user-tracking':
				$data = new stdClass();
				$data->userId = 0;
				$data->comments='';
				if(isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
					$data->userId = $_SESSION['userId'];
				}else{
					$data->comments="anonymous user";
				}
				$data->ip='';
				if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			    $ip = $_SERVER['HTTP_CLIENT_IP'];
				} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				} else {
				    $ip = $_SERVER['REMOTE_ADDR'];
				}
				$data->ip=$ip;
				//print_r($pageUrl);
				$pageUrl='index';
				$data->pageUrl=$_SERVER['REQUEST_URI'];
				$res = Api::trackUser($data);
				break;
			case 'login':
				$res = Api::login($req);
				if($res->valid == false)
					break;
				session_start();
				$_SESSION["userId"] = $res->userId;
				$_SESSION["userRole"] = $res->userRole;
				$_SESSION['username'] = $res->username;
				$_SESSION['userEmail'] = $res->email;
				$_SESSION['temp'] = $res->temp;
				$_SESSION["details"] = $res->details;
				break;
			case 'google-login':
				$res = Api::googleLogin($req);
				break;
			case 'google-role-login':
				$res = Api::googleRoleLogin($req);
				if($res->valid == false)
					break;
				session_start();
				$_SESSION["userId"] = $res->userId;
				$_SESSION["userRole"] = $res->userRole;
				$_SESSION['username'] = $res->username;
				$_SESSION['userEmail'] = $res->email;
				$_SESSION['temp'] = 0;
				$_SESSION["details"] = $res->details;
				break;
			case 'facebook-login':
				$res = Api::facebookLogin($req);
				if($res->valid == false)
					break;
				session_start();
				$_SESSION["userId"] = $res->userId;
				$_SESSION["userRole"] = $res->userRole;
				$_SESSION['username'] = $res->username;
				$_SESSION['userEmail'] = $res->email;
				$_SESSION['temp'] = 0;
				$_SESSION["details"] = $res->details;
				break;
			case 'portfolio-login':
				$res = Api::portfolioLogin($req);
				if($res->valid == false || $res->portfolioValid == false)
					break;
				session_start();
				$_SESSION["userId"] = $res->userId;
				$_SESSION["userRole"] = $res->userRole;
				//performance improvement ARC-360
				$_SESSION['username'] = $res->username;
				$_SESSION['temp'] = $res->temp;
				$_SESSION["details"] = $res->details;
				break;
			case 'parent-login':
				$res = Api::parentLogin($req);
				if($res->valid == false)
					break;
				session_start();
				$_SESSION["userId"] = $res->userId;
				$_SESSION["userRole"] = 6;
				$_SESSION['username'] = $res->username;
				$_SESSION['student_id'] = $res->student_id;
				$_SESSION['course_id'] = $res->course_id;
				$_SESSION['name'] = $res->name;
				$_SESSION['gender'] = $res->gender;
				break;
			case 'back-login':
				$res = Api::backLogin($req);
				if($res->valid == false)
					break;
				session_start();
				$_SESSION["userId"] = $res->userId;
				$_SESSION["userRole"] = $res->userRole;
				$_SESSION['username'] = $res->username;
				break;
			case 'send-reset-password-link':
				$res = Api::sendResetPasswordLink($req);
				break;
			case 'reset-password':
				$res = Api::resetPassword($req);
				break;
			case 'update-profile-desc':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateProfileDescription($req);
				break;
			case 'update-profile-general':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateProfileGeneral($req);
				break;
			case 'update-user-institute-type':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateUserInstituteType($req);
				break;
			case 'create-course':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::createCourse($req);
				break;
			case 'get-course-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getCourseDetails($req);
				break;
			case 'update-course':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateCourse($req);
				break;
			case 'update-course-licensing':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateCourseLicensing($req);
				break;
			case 'create-subject':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::createSubject($req);
				break;
			case 'get-course-subjects':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getCourseSubjects($req);
				break;
			case 'get-subject-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectDetails($req);
				break;
			case 'get-subject-progress-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectProgressDetails($req);
				break;
			case 'get-subject-questions':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectQuestions($req);
				break;
			case 'get-subject-questions-category':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectQuestionsByCategory($req);
				break;
			case 'get-subjective-questions-import':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectiveQuestionsImport($req);
				break;			
			case 'get-subjective-questions':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectiveQuestions($req);
				break;				
			case 'get-student-progress-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getStudentProgressDetails($req);
				break;
			//to assign chapters to student
			case 'get-student-subject-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getStudentSubjectDetails($req);
				break;
			//to assign chapters to student
			case 'set-student-subject-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::setStudentSubjectDetails($req);
				break;
			case 'update-subject':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateSubject($req);
				break;
			case 'get-all-courses':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getAllCourses($req);
				break;

			case 'create-chapter':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::createChapter($req);
				break;
			case 'get-subject-chapters':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectChapters($req);
				break;
			case 'get-subject-chapters-new':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectChaptersNew($req);
				break;
			case 'get-subject-students':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectStudents($req);
				break;
			case 'get-subject-parents':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectParents($req);
				break;
			case 'get-courses-for-import':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getCoursesForImport($req);
				break;
			case 'get-purchased-courses-for-import':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getPurchasedCoursesForImport($req);
				break;    
							
			case 'import-subjects-to-course':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::importSubjectsToCourse($req);
				break;
			case 'get-all-courses-admin':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				if($req->userRole != 5) {
					$res->status = 0;
					$res->message = "Invalid Action";
					break;
				}
				$res = Api::getAllCoursesForApproval($req);
				break;
			case 'get-list-courses-admin':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				if($req->userRole != 5) {
					$res->status = 0;
					$res->message = "Invalid Action";
					break;
				}
				$res = Api::getListCoursesForAdmin($req);
				break;
			case 'get-all-approved-courses-admin':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				if($req->userRole != 5) {
					$res = new stdClass();
					$res->status = 0;
					$res->message = "Invalid Action";
					break;
				}
				$res = Api::getAllApprovedCourses($req);
				break;
			case 'get-all-deleted-course':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				if($req->userRole != 5) {
					$res = new stdClass();
					$res->status = 0;
					$res->message = "Invalid Action";
					break;
				}
				$res = Api::getAlldeletedCourse($req);
				break;
							   
						  case 'restore-course-admin':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				if($req->userRole != 5) {
					$res = new stdClass();
					$res->status = 0;
					$res->message = "Invalid Action";
					break;
				}
				$res = Api::restoreCourseAdmin($req);
				break;
								
			case 'approve-course':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				if($req->userRole != 5) {
					$res = new stdClass();
					$res->status = 0;
					$res->message = "Invalid Action";
					break;
				}
				$res = Api::approveCourse($req);
				break;
			case 'reject-course':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				if($req->userRole != 5) {
					$res = new stdClass();
					$res->status = 0;
					$res->message = "Invalid Action";
					break;
				}
				$res = Api::rejectCourse($req);
				break;
			case 'delete-course':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteCourse($req);
				break;
			case 'restore-course':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::restoreCourse($req);
				break;
			case 'delete-subject':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteSubject($req);
				break;
			case 'restore-subject':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::restoreSubject($req);
				break;
			case 'update-chapter-order':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateChapterOrder($req);
				break;
			case 'get-institute-professors':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getInstituteProfessors($req);
				break;
			case 'checkCourseAcess':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::checkCourseAcess($req);
				break;		
			case 'getcoursesByowner':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getCoursesByOwner($req);
				break;
			case 'getcoursesByownerADMIN':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getCoursesByADMIN($req);
				break;
				//
			case 'getEditcoursesByowner':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getEditcoursesByowner($req);
				break;
				//
			case 'getstudent-package':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getStudentforPackage($req);
				break;
			case 'geteditstudent-package':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getEditStudentforPackage($req);
				break;
			case 'created-package':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getCreatedPackage($req);
				break;
			case 'view-package':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getViewPackage($req);
				break;
			case 'save-package':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::savePackage($req);
				break;
			case 'edit-package':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::editPackage($req);
				break;
			case 'can-create-package':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::canCreatePackage($req);
				break;
			case 'get-package-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getPackagestudent($req);
				break;
			case 'get-package-price':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getPackageprice($req);
				break;
			case 'update-subject-professors':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateSubjectProfessors($req);
				break;
				
			case 'delete-single-professor':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteSubjectProfessor($req);
				break;
			
			case 'delete-institute-professor':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteInstituteProfessor($req);
				break;
			
			case 'get-institute-prof-with-sub':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getInstituteProfessorsWithSubjects($req);
				break;
			
			case 'delete-subject-professor':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteSubjectProfessor($req);
				break;

			case 'get-subject-professors-chat':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectProfessorsChat($req);
				break;

			case 'get-student-chat-link':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getStudentChatLink($req);
				break;

			case 'get-professor-chat-link':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getProfessorChatLink($req);
				break;
			
			case 'request-course-approval':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::requestCourseApproval($req);
				break;
			
			case 'update-course-order':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateCourseOrder($req);
				break;
			
			case 'update-subject-order':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateSubjectOrder($req);
				break;
			
			case 'save-heading':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::saveHeading($req);
				break;
			case 'delete-heading':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteHeading($req);
				break;
			case 'save-content':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::saveContent($req);
				break;
			case 'delete-content':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteContent($req);
				break;
			case 'get-chapter-content':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getAllContent($req);
				break;
			case 'save-text-stuff':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$req->contentType = 'text';
				$req->fileRealName = 'Text';
				$req->metadata = '';
				$res = Api::saveContentStuff($req);
				break;
			case 'save-link-stuff':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$req->contentType = $req->contentType;
				$req->fileRealName = 'Link';
				$req->metadata = '';
				$res = Api::saveContentStuff($req);
				break;
			case 'save-quiz-stuff':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$req->contentType = 'quiz';
				$req->fileRealName = $req->title;
				$req->metadata = '';
				$res = Api::saveContentStuff($req);
				break;
			case 'save-survey-stuff':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$req->contentType = 'survey';
				$req->fileRealName = $req->title;
				$req->metadata = '';
				$res = Api::saveContentStuff($req);
				break;
			case 'update-published-status':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$req->status = $req->status == "1" ? '1' : '0';
				$res = Api::updatePublishedStatus($req);
				break;
			case 'update-downloadable-status':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$req->status = $req->status == "1" ? '1' : '0';
				$res = Api::updateDownloadableStatus($req);
				break;
			case 'save-stuff-desc':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::saveStuffDesc($req);
				break;
			case 'update-content-order':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::updateContentOrder($req);
				break;
			case 'get-chapter-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getChapterDetails($req);
				break;
			case 'get-chapter-details-for-view':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getChapterDetailsForView($req);
				break;
			case 'delete-content-stuff':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$req->publicAccess = true;
				$res = Api::deleteContentStuff($req);
				break;
			case 'delete-supp-stuff':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteSupplementaryStuff($req);
				break;
			case 'get-all-institute-courses':
				session_start();
				$req->userId = $_SESSION["userId"];
				$res = Api::getAllInstituteCoursesForTree($req);
				break;
			case 'get-all-institute-subjects':
				session_start();
				$req->userId = $_SESSION["userId"];
				$res = Api::getAllInstituteSubjectsForTree($req);
				break;
			case 'get-all-institute-chapters':
				session_start();
				$req->userId = $_SESSION["userId"];
				$res = Api::getAllInstituteChaptersForTree($req);
				break;
			case 'get-courses-for-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getCoursesForExam($req);
				break;
			case 'get-subjects-for-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectsForExam($req);
				break;
			case 'get-exam-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getExamDetails($req);
				break;
			case 'get-chapters-for-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getChaptersForExam($req);
				break;
			case 'get-exams-of-subject':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectExams($req);
				break;
			case 'get-assignments-for-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getAssignmentsForExam($req);
				break;
			case 'get-subjective-exams-of-subject':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectSubjectiveExams($req);
				break;
			case 'add-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::addExam($req);
				break;
			case 'add-subejctive-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::addSubejctiveExam($req);
				break;
			case 'edit-subejctive-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::editSubjectiveExam($req);
				break;
			case 'add-template':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::addTemplate($req);
				break;
			case 'get-exam-sections-for-export':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getExamSectionsForExport($req);
				break;
			case 'export-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::exportExam($req);
				break;
			case 'get-exam-sections':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getExamSections($req);
				break;
			case 'get-exam-name':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getExamName($req);
				break;
			case 'get-section-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSectionDetails($req);
				break;
			case 'save-categories':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::saveCategories($req);
				break;
			case 'viewCourseCategory':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::viewCourseCategory($req);
				break;
			case 'save-categories-desc':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::saveCategoriesDesc($req);
				break;
			case 'save-question':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::saveQuestion($req);
				break;
			case 'save-question-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::saveQuestionExam($req);
				break;
			case 'save-subjective-questions':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::saveSubjectiveQuestion($req);
				break;
			case 'insert-subjective-questions':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::insertSubjectiveQuestion($req);
				break;
			case 'edit-subjective-questions':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::editSubjectiveQuestion($req);
				break;
			case 'delete-subjective-questions':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteSubjectiveQuestion($req);
				break;
			case 'insert-subjective-questions-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::insertSubjectiveQuestionExam($req);
				break;
			case 'edit-subjective-questions-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::editSubjectiveQuestionExam($req);
				break;
			case 'delete-subjective-questions-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteSubjectiveQuestionExam($req);
				break;
			case 'live-subjective-questions':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::liveSubjectiveQuestion($req);
				break;
			case 'unlive-subjective-questions':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::unliveSubjectiveQuestion($req);
				break;
			case 'get-check-subjective-exam-students':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getCheckSubjectiveExamStudents($req);
				break;
			case 'get-check-subjective-exam-questions':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getCheckSubjectiveExamQuestions($req);
				break;
			case 'get-subjective-exam-Attempt':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectiveExamAttempt($req);
				break;
			case 'get-check-subjective-exam-studentId':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getCheckSubjectiveExamStudentId($req);
				break;
			case 'mark-subjective-exam-checked':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::markSubjectiveExamChecked($req);
				break;
			case 'get-Attemptlist-subjective-exam-students':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getAttemptlistSubjectiveExamStudents($req);
				break;
			case 'delete-subjective-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteSubjectiveExam($req);
				break;
			case 'get-question-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getQuestionDetails($req);
				break;
			case 'edit-question':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::editQuestion($req);
				break;
			case 'edit-question-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::editQuestionExam($req);
				break;
			case 'get-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getExam($req);
				break;
			case 'check-category-status':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::checkCategoryStatus($req);
				break;
			case 'edit-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::editExam($req);
				break;
			case 'delete-question':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteQuestion($req);
				break;
			case 'delete-question-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteQuestionExam($req);
				break;
			case 'delete-category':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteCategory($req);
				break;
			case 'delete-section':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteSection($req);
				break;
			case 'delete-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteExam($req);
				break;
			case 'copy-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::copyExam($req);
				break;
			case 'copy-subjective-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::copySubjectiveExam($req);
				break;
			case 'copy-submission':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::copySubmission($req);
				break;
			case 'get-section-status':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSectionStatus($req);
				break;
			case 'make-live':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::makeLive($req);
				break;
			case 'make-unlive':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::makeUnlive($req);
				break;
			case 'make-exam-end':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::makeExamEnd($req);
				break;
			case 'get-templates-for-exam':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getTemplateForExam($req);
				break;
			case 'get-template-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getTemplateDetails($req);
				break;
			case 'get-template':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getTemplate($req);
				break;
			case 'get-individual-details':
				session_start();
				$req->userId = $req->uid;
				$req->userRole = $req->userRole;
				$res = Api::getProfileDetails($req);
				break;
			case 'update-invitation':
				$req->sno = $req->sno;
				$res = Api::updateInvitation($req);
				break;
			case 'get-role':
				session_start();
				$res = Api::getRoleId($req->uid);
				break;
			case 'get-invited-professors-details':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getInvitedProfessors($req);
				break;
			case 'fetch-professor-details':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::fetchProfessorDetails($req);
				break;
			case 'invite-professor':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::inviteProfessor($req);
				break;
			case 'edit-template':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::editTemplate($req);
				break;
			case 'delete-template':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::deleteTemplate($req);
				break;
			case 'get-questions-for-import':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getQuestionsForImport($req);
				break;
			case 'import-questions':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::importQuestions($req);
				break;
			case 'import-subject-questions':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::importSubjectQuestions($req);
				break;
			case 'import-subjective-questions':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::importSubjectiveQuestions($req);
				break;
			case 'restore-section':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::restoreSection($req);
				break;
			case 'restore-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::restoreExam($req);
				break;
			case 'restore-subjective-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::restoreSubjectiveExam($req);
				break;
			case 'restore-manual-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::restoreManualExam($req);
				break;
			case 'restore-submission':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::restoreSubmission($req);
				break;
			case 'get-breadcrumb-for-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getBreadcrumbForExam($req);
				break;
			case 'get-breadcrumb-for-manual-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getBreadcrumbForManualExam($req);
				break;
			case 'get-breadcrumb-for-add':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getBreadcrumbForAdd($req);
				break;
			case 'get-course-date':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getCourseDate($req);
				break;
			case 'check-name-for-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkNameForExam($req);
				break;
			case 'check-name-for-subjectiveexam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkNameForsubjectiveexam($req);
				break;
			case 'check-name-for-package':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkNameForPackage($req);
				break;
			case 'check-name-for-Editpackage':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkNameForEditPackage($req);
				break;
			case 'check-name-for-template':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkNameForTemplate($req);
				break;
			/* course key invitation module */
			case 'get-course-keys-details':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getCoursekeyDetails($req);
				break;
			case 'purchase_key':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::purchase_key($req);
				break;
					   
			case 'keys-purchase_invitation':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api:: keysPurchaseInvitation($req);
				break;    
			case 'back_end_course_keys':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::backendCoursekeys($req);
				break;
			case 'free-tier':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::freetier($req);
				break;
			case 'save_free_tier':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::savefreetier($req);
				break;
			case 'change_key_rate':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::change_key_rate($req);
				break;
			case 'setInstitute_rate_back':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::setInstitute_rate_back($req);
				break;
			case 'add_keys':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::add_keys($req);
				break;
			case 'set_default_key_rate':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::set_default_key_rate($req);
				break;
			case 'send_activation_link_student':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::send_activation_link_student($req);
				break;
			case 'student-package':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api:: getPackageForStudent($req);
				break;
			case 'all-package':
				session_start();
				if(isset($_SESSION['userId'])&&isset($_SESSION['userRole'])){
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
				}
				$res = Api:: getAllPackageForStudent($req);
				break;
			case 'loadpackageDetails':
				session_start();
				if(isset($_SESSION['userId'])&&isset($_SESSION['userRole'])){
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
				}
				$res = Api:: getPackageDetails($req);
				break;	
			case 'package-course-enroll':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api:: setstudentPackagecourse($req);
				break;	
			case 'get-packageCourses':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api:: getpackageCourses($req);
				break;	
			//for marketplace and student will pay			
			case 'get-AllpackageCourses':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api:: getAllpackageCourses($req);
				break;
			case 'show-AllpackageCourses':
				$res = Api:: showAllpackageCourses($req);
				break;
			case 'verify_activation_link_student':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::verify_activation_link_student($req);
				break;
			case 'student_invitation_details':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::student_invitation_details($req);
				break;
			case 'resend_invitation_link':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::resend_invitation_link($req);
				break;
			case 'load_student_notification':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::load_student_notification($req);
				break;
			case 'accepted_student_invitation':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::accepted_student_invitation($req);
				break;
			case 'rejected_student_invitation':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::reject_student_invitation($req);
				break;
				;
			case 'reject_link_by_admin':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::reject_link_by_admin($req);
				break;
			case 'keys_avilable':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::keys_avilable($req);
				break;
			case 'get_students_of_course':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::get_students_of_course($req);
				break;
			case 'get_course_student_details':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::get_course_student_details($req);
				break;
			case 'get_students_details_of_course':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::get_students_details_of_course($req);
				break;
			case 'generate-parents':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::generateParents($req);
				break;
			case 'view_institutes_info':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::view_institutes_info($req);
				break;

			case 'fetch-applicable-licenses':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::fetchApplicableLicenses($req);
				break;
							
						case 'fetch-applicable-licenses-FrontPage':
				session_start();
				$res = Api::getLicensesONCourse($req);
				break;
						
			case 'modify-licenses':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::modify_licenses($req);
				break;

			/*Taxes*/
			case 'fetch-all-taxes': 
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::fetchAllTaxes($req);
				break;
			case 'fetch-default-taxes': 
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::fetchDefaultTaxes($req);
				break;
			case 'update-institute-taxes':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::updateInstituteTaxes($req);
				break;
			case 'update-default-tax':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::updateDefaultTax($req);
				break;
			case 'delete-default-tax':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::deleteDefaultTax($req);
				break;
			case 'insert-default-tax':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::insertDefaultTax($req);
				break;

			case 'Student_Details_WithCourses':
					session_start();
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
					$res = Api::StudentDetailsWithCourses($req);
					break; 
			case 'Courses_for_student':
					session_start();
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
					$res = Api::CourseforStudent($req);
					break;  
			case 'viewInstitutesLoginInfo':
					session_start();
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
					$res = Api::viewInstitutesLoginInfo($req);
					break; 
		    case 'ChangeLoginstatus':
					session_start();
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
					$res = Api::ChangeLoginstatus($req);
					break;     
			case 'changePassword':
					session_start();
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
					$res = Api::changePassword($req);
					break;     
			case 'getemailUsername':
					session_start();
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
					$res = Api::getemailUsername($req);
					break;
			case 'loadCourseDetails':
				session_start();
				if(isset($_SESSION['userId'])&&isset($_SESSION['userRole'])){
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
				}
				$res = Api::loadCourseDetails($req);
				break;
			case 'loadPackageCourseDetails':
				$res = Api::loadPackageCourseDetails($req);
				break;
			case 'loadSubjectDetails':
				session_start();
				if(isset($_SESSION['userId'])&&isset($_SESSION['userRole'])){
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
				}
				$res = Api::loadSubjectDetails($req);
				break;
			case 'loadOtherCoursesInstitute':
				session_start();
				if(isset($_SESSION['userId'])&&isset($_SESSION['userRole'])){
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
				}
				$res = Api::otherCoursesbyProfessor($req);
				break;

			/*admin section modules*/
		 	case 'get-all-deleted-subject':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getAlldeletedSubject($req);
				break; 
		 	case 'restore-subject-admin':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				 $res = Api::restoreSubjectByAdmin($req);
				break;
		  	case 'get-all-deleted-chapter':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getAlldeletedChapter($req);
				break;
		   	case 'restore-chapter-admin':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::restoreChapterByAdmin($req);
				break;
		   	case 'get-all-deleted-exams':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getAlldeletedExam($req);
				break;
		   	case 'restore-exam-admin':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::restoreExamByAdmin($req);
				break;
			case 'update-chapter-details':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::updateChapterDetails($req);
				break;
							
			/*student section*/
			//to fetch student name
			case 'get-student-name':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentName($req);
				break;
			//get exam details for student
			case 'get-exam-for-student':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getExamForStudent($req);
				break;
			case 'get-subjective-exam-for-student':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectiveExamForStudent($req);
				break;
			//to get only questions for giving exam
			case 'get-questions-for-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getQuestionsForExam($req);
				break;
			case 'get-questions-for-subjectiveExam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getQuestionsForSubjectiveExam($req);
				break;
			case 'get-questions-for-subjectiveExam-in-resume':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getQuestionsForSubjectiveExamResumeMode($req);
				break;
			case 'save-marks-for-subjectiveExam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveMarksForSubjectiveExam($req);
				break;
			case 'save-review-for-subjectiveExam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveReviewForSubjectiveExam($req);
				break;
			//to get exam attempts for graph creation
			case 'get-subjective-attempt-graph':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectiveAttemptGraph($req);
				break;
			case 'getsubjectiveTimeLineGraph':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				if (isset($_SESSION['student_id']) && !empty($_SESSION['student_id'])) {
					$req->student_id = $_SESSION['student_id'];
				}
				$res = Api::getsubjectiveTimeLine($req);
				break;
			case 'get-subjective-topper-of-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectiveTopperDetails($req);
				break;
			case 'get-SubjectiveNormalization-graph':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectiveNormalizationGraph($req);
				break;
			case 'get-subjective-maximum-time-for-question':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectiveMaxiumTimeForQuestion($req);
				break;
			//to get question and answers to solve assignment
			case 'get-questions-for-assignment':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getQuestionsForAssignment($req);
				break;
				
			//to get questions and answers to solve assignment but in resume mode
			case 'get-questions-for-assignment-in-resume':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getQuestionsForAssignmentInResume($req);
				break;
				
			//to get questions and answers to give exam but in resume mode
			case 'get-questions-for-exam-in-resume':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getQuestionsForExamInResume($req);
				break;
				
			//to get resume mode of the exam
			case 'get-exam-resume-status':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getExamResumeStatus($req);
				break;
			case 'get-subjective-exam-resume-status':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectiveExamResumeStatus($req);
				break;
			case 'get-exam-courseId':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getExamCourseId($req);
				break;
			case 'check-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkExam($req);
				break;
				
			//to get time of exam/assignment
			case 'get-time-and-section':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getTimeAndSection($req);
				break;

			//to get time of exam/assignment
			case 'get-subjective-time':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectiveTime($req);
				break;
				
			//to get time of exam/assignment
			case 'set-time-subjectiveExam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::setTimeSubjectiveExam($req);
				break;
				
			//to increase the attempt of exam
			case 'increase-attempt':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::increaseAttempt($req);
				break;
			case 'increase-subjectiveExam-attempt':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::increaseSubjectiveExamAttempt($req);
				break;
			//to check and save exam attempt
			case 'check-and-save':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkAndSave($req);
				break;
			//to check and save subjective exam attempt
			case 'save-subjective-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveSubjectiveExam($req);
				break;
			case 'check-result-show':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkResultShow($req);
				break;
			//to save all questions for the auto save feature
			case 'save-all-questions':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveAllQuestions($req);
				break;
				
			//to update each answer as it will be given by user
			case 'update-user-answer':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::updateUserAnswer($req);
				break;
				
			//to update each answer as it will be given by user
			case 'update-subjective-Answer':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::updateSubjectiveAnswer($req);
				break;
			/* cases for chapter content view page */
			//to fetch breadcrumb of chapter
			case 'get-breadcrumb-for-chapter':
				session_start();
				@$req->userId = $_SESSION['userId'];
				@$req->userRole = $_SESSION['userRole'];
				$res = Api::getBreadcrumbForChapter($req);
				break;
			//to get headings of a chapter
			case 'get-headings':
				session_start();
				@$req->userId = $_SESSION['userId'];
				@$req->userRole = $_SESSION['userRole'];
				$res = Api::getHeadings($req);
				break;
			//to get stuff of content
			case 'get-content-stuff':
				session_start();
				@$req->userId = $_SESSION['userId'];
				@$req->userRole = $_SESSION['userRole'];
				$res = Api::getContentStuff($req);
				break;
			//to get student profile details
			case 'get-student-details':
				session_start();
				if(!isset($req->userId) && empty($req->userId)) {
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
				}
				$res = Api::getStudentDetails($req);
				break;
			//to get student profile details
			case 'get-student-details-new':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentDetailsNew($req);
				break;
			//to save student background details
			case 'save-student-bio':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveBio($req);
				break;
			//to save student achievements details
			case 'save-student-achievement':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveAchievement($req);
				break;
			//to save student interest details
			case 'save-student-interest':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveInterest($req);
				break;
			//to get student details for setting page
			case 'get-student-details-for-setting':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentDetailsForSetting($req);
				break;
			//to save student setting data
			case 'save-student-setting':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveStudentSetting($req);
				break;
			//to get student details for change password page
			case 'get-student-details-for-changePassword':
				session_start();
				@$req->userId = $_SESSION['userId'];
				@$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentDetailsForChangePassword($req);
				break;
			//to get student notifications
			case 'get-student-notifications':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentNotifications($req);
				break;
			//to get student notifications
			case 'get-student-notifications-activity':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentNotificationsActivity($req);
				break;
			//to get student notifications
			case 'get-student-notifications-chat':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentNotificationsChat($req);
				break;
					
			//to get joined courses @amit
			case 'student-joined-courses':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentJoinedCourses($req);
				break;
			case 'student-subs-courses':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentSubsCourses($req);
				break;
			// to get exam details and count no of attempts of students
			case 'student-exam-attempts':
				session_start();
				if (!isset($req->userId) && empty($req->userId)) {
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
				}
				$res = Api::getStudentExamAttemps($req);
				break;
			// to get exam details and count no of attempts of students
			case 'student-exam-attempts-new':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentExamAttemptsNew($req);
				break;
			// to get subjective exam details and count no of attempts of students
			case 'student-exam-subjective-attempts':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentSubjectiveExamAttemps($req);
				break;
			// to get subjective exam details and count no of attempts of students
			case 'student-exam-manual-exams':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentManualExams($req);
				break;
			//get courseDetails for student
			case 'courseDetails-for-student':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getCourseSubjectsForStudent($req);
				break;
			//get courseDetails for student
			case 'courseDetails-for-student-new':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getCourseSubjectsForStudentNew($req);
				break;
			case 'exam-attempts-chapterwise':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::studentExamsChapterWise($req);
				break;
			case 'subjective-exam-attempts-chapterwise':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::studentSubjectiveExamsChapterWise($req);
				break;
			//to delete chapter by institute roles
			case 'delete-chapter':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::deleteChapter($req);
				break;
			//to fetch all independent exams in a subject
			case 'get-independent-exams':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getIndependentExams($req);
				break;
			//to fetch all independent exams in a subject
			case 'get-independent-exams-new':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getIndependentExamsNew($req);
				break;
			//to fetch all subjective exams in a subject
			case 'get-subjective-exams':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getsubjectiveExams($req);
				break;
			case 'get-subjectiveExam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectiveExamAll($req);
				break;
			//to delete chapter by institute roles
			case 'student-session-check':
				$r = new stdClass();
				session_start();
				$r->status = 0;
				if(isset($_SESSION['userId']) && isset($_SESSION['userRole'])) {
					if($_SESSION['userRole'] == 4)
						$r->status = 1;
				}
				$res = $r;
				break;
				
			/* result creation section for given exams */
			//to fetch result details according to exam student and attempt
			case 'get-result':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getResult($req);
				break;
			//to fetch the questions and answers of each section for result
			case 'get-section-questions':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSectionQuestions($req);
				break;
			//to get topper details
			case 'get-topper-of-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getTopperDetails($req);
				break;
			case 'get-topper-of-examAll':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getTopperDetailsAll($req);
				break;
			case 'getCompleteQuestionAnalysis':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getCompleteQuestionAnalysis($req);
				break;
			case 'getAllStudentTimeLineGraph':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				if (isset($_SESSION['student_id']) && !empty($_SESSION['student_id'])) {
					$req->student_id = $_SESSION['student_id'];
				}
				$res = Api::getStudentTimeLine($req);
				break;
			case 'getAllStudentTimeLineGraphAll':
				$res = Api::getStudentTimeLineAll($req);
				break;
				//
			//to get maximum time for question
			case 'get-maximum-time-for-question':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getMaxiumTimeForQuestion($req);
				break;
			//to get exam attempts for subject
			case 'get-subject-attempts':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectAttempts($req);
				break;
			//to get exam attempts for graph creation
			case 'get-attempt-graph':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getAttemptGraph($req);
				break;
			// to get the graph for the highest comparison on student noramlization
			case 'get-allStudentNormalization-graph':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getallStudentNormalizationGraph($req);
				break;
			//to get exam reports for institute
			case 'get-exam-report':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getExamReport($req);
				break;
			//to get student list who have given exams
			case 'get-student-list':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentList($req);
				break;
			case 'get-question-list':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getQuestionList($req);
				break;
			case 'get-student-list-download':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentListDownload($req);
				break;
			//to create instructions for exams and assignments
			case 'create-instructions':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::createInstructions($req);
				break;
			/* bug fixes */
			//to get course name availability
			case 'check-duplicate-course':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkDuplicateCourse($req);
				break;
			//to get course name availability
			case 'check-duplicate-subject':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkDuplicateSubject($req);
				break;
			case 'check-duplicate-chapter':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkDuplicateChapter($req);
				break;
			//to create student left navigation tree
			case 'get-courses-for-student-tree':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION['userRole'];
				$res = Api::getCoursesForStudentTree($req);
				break;
			//to get counter values for institute dashboard
			case 'get-institute-counters':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getInstituteCounters($req);
				break;
			//to get graph details for institute dashboard
			case 'get-institute-graphs':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getInstituteGraphs($req);
				break;
			case 'get-recent-course-view':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getRecentCourseView($req);
				break;
			//@ayush to get detailed viewing of student
			case 'get-detail-recent-course-view':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getDeatilRecentCourseView($req);
				break;
			// to get institute end notifications
			case 'get-institute-notifications':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getInstituteNotifications($req);
				break;
			case 'get-institute-notifications-activity':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getInstituteNotificationsActivity($req);
				break;
			case 'get-institute-notifications-chat':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getInstituteNotificationsChat($req);
				break;
			// to get instructor end notifications
			case 'get-instructor-notifications':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getInstructorNotifications($req);
				break;
			case 'get-instructor-notifications-activity':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getInstructorNotificationsActivity($req);
				break;
			case 'get-instructor-notifications-chat':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getInstructorNotificationsChat($req);
				break;
			case 'mark-notification':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::markNotification($req);
				break;
			case 'mark-chat-notification':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::markChatNotification($req);
				break;
			case 'count-notification':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::countNotification($req);
				break;
			case 'set-chat-message-notification':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::setChatMessageNotification($req);
				break;
			// get professor invitations
			case 'get-subject-invitation':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectInvitation($req);
				break; 
			case 'accept-subject-invitation':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::acceptSubjectInvitation($req);
				break;
			case 'reject-subject-invitation':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::rejectSubjectInvitation($req);
				break;
			case 'get-subject-assigned':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectAssigned($req);
				break;
			case 'get-subject-assigned-new':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectAssignedNew($req);
				break;
			/* student rate saved for course */
			case 'save-course-rate-student':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveCourseRateStudent($req);
				break;
			case 'live-course-student':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::liveCourseStudent($req);
				break;    
			case 'live-course-content-market':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::liveCourseOnContentMarket($req);
				break;
			case 'unlive-course-student':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::unliveCourseStudent($req);
				break;
			case 'unlive-course-content-market';
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::unliveCourseOnContentMarket($req);
				break;
			case 'fetch-licensing-status':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::fetchCourseLicense($req);
				break;
				//check-courseEnrolled	@Ayush for checking student is enrolled in particular course.
			case 'check-courseEnrolled':
				session_start();
				$res = new stdClass();
				if(isset($_SESSION['userId'])) {
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
				}
				$res->sitebase=$siteBase;
				$res = Api::getcourseEnrollStatus($req);
				//$siteBase   $req->courseId
				break;	
			case 'check-login':
				session_start();
				$res = new stdClass();
				if(isset($_SESSION['userId']))
				{
					$res->status = 1;
					$res->userId = $_SESSION['userId'];
					$res->userRole = $_SESSION['userRole'];
					$res->message = " user log in";
				} else {
					$res->status = 0;
					$res->message = " User not log in";
				}
				break;
			case 'get-currency':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getCurrency($req);
				break;
			case 'get-course-details-studentEnd':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getCourseDetailsStudentEnd($req);
				break;
			case 'get-course-details-instituteEnd':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getCourseDetailsInstituteEnd($req);
				break;
			case 'link-student-course':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::linkStudentCourse($req);
				break;
			case 'puchase-copy-course-Institute':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::CopyCourseandPurchase($req);
				break;

			case 'fetch-purchased-courses-details':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::fetchPurchasedCoursesDetails($req);
				break;

			//case to download the downloadable stuff by student
			case 'download-content':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::downloadContent($req);
				break;
			case 'send-renew request':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::sendRenewRequest($req);
				break;
			//case for breadcrumb on result page
			case 'get-breadcrumb-result':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getBreadcrumbResult($req);
				break;
			case 'resend-pending-invitation':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::resendPendingInvitation($req);
				break;
			case 'contact-us-email':
				$res = Api::contactUsEmail($req);
				break;
			case 'purchaseCourseKeys':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::purchaseCourseKeys($req);
				break;
			case 'purchaseCourseViaPayU':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::purchaseCourseViaPayU($req);
				break;
			case 'purchasePackage':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::purchasePackage($req);
				break;
			case 'purchaseStudentPackage':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::purchaseStudentPackage($req);
				break;			
			case 'ghost-login':
				@session_start();
				if(isset($_SESSION['userRole']) && !empty($_SESSION['userRole'])) {
					if($_SESSION['userRole'] == 5) {
						$_SESSION['userId'] = $req->userId;
						$_SESSION['userRole'] = $req->roleId;
						$_SESSION['userEmail'] = $req->email;
						@$res->status = 1;
					}
					else {
						$res->status = 0;
						$res->message = 'You are not the admin so you better stop threating our website';
					}
				}
				else {
					$res->status = 0;
					$res->message = 'You are not the admin so you better stop threating our website';
				}
				break;
			case 'paypal-return':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::paypalReturn($req);
				break;
			case 'save-rating':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveRating($req);
				break;
			case 'fetch-user-rating':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::fetchUserRating($req);
				break;
			case 'remove-review':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::removeReview($req);
				break;
			case 'get-all-reviews-for-course':
				session_start();
				if(isset($_SESSION['userId']) && isset($_SESSION['userRole'])) {
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
				}
				$res = Api::getAllReviewsForCourse($req);
				break;
			case 'get-all-reviews-for-subject':
				session_start();
				if(isset($_SESSION['userId']) && isset($_SESSION['userRole'])) {
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
				}
				$res = Api::getAllReviewsForSubject($req);
				break;
			case 'get-subject-rating':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectRating($req);
				break;
			case 'delete-demo-video':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::deleteDemoVideo($req);
				break;
			case 'admin_courseCoupons':
				$res = Api::adminCourseCoupons($req);
				break;
			case 'admin_courseCouponsInfo':
				$res = Api::adminCourseCouponsInfo($req);
				break;
			case 'admin_courseDiscount':
				$res = Api::adminCourseDiscount($req);
				break;
			case 'get-all-coupons':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getAllCoupons($req);
				break;
			case 'course-coupons':
				$res = Api::courseCoupons($req);
				break;
			case 'edit-courseCoupons':			
				$res = Api::editcourseCoupons($req);
				break;
			case 'changeupdateCoupon':
				$res = Api::changeupdateCoupon($req);
				break;
			case 'redeemCoupon':
				$res = Api::redeemCoupon($req);
				break;
			case 'course-discount':
				$res = Api::courseDiscount($req);
				break;
			case 'discount-courseId':
				$res = Api::discountCourseId($req);
				break;
			// get list of portfolios
			case 'get-portfolios':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION["userRole"];
				$res = Api::getPortfolios($req);
				$res->userId = $req->userId;
				$res->userRole = $req->userRole;
				break;
			// save portfolio course
			case 'save-portfolio-course':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION["userRole"];
				$res = Api::savePortfolioCourse($req);
				$res->userId = $req->userId;
				$res->userRole = $req->userRole;
				break;
			// set new portfolio
			case 'new-portfolio':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION["userRole"];
				$res = Api::newPortfolio($req);
				break;
			case 're-submit-Portfolio':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION["userRole"];
				$res = Api::reSubmitPortfolio($req);
				break;
			// get portfolio in detail
			case 'get-portfolio-detail':
				session_start();
				$res = Api::getPortfolioDetail($req);
				break;
			// update portfolio information
			case 'update-portfolio':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION["userRole"];
				$res = Api::updatePortfolio($req);
				break;
			// get portfolio page types (available ones only)
			case 'get-page-types':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION["userRole"];
				$res = Api::getPageTypes($req);
				break;
			// get portfolio pages list
			case 'get-portfolio-pages':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION["userRole"];
				$res = Api::getPortfolioPages($req);
				break;
			// set new/update portfolio page
			case 'save-portfolio-page':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION["userRole"];
				$res = Api::savePortfolioPage($req);
				break;
			// get portfolio menus list
			case 'get-portfolio-menus':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION["userRole"];
				$res = Api::getPortfolioMenus($req);
				break;
			// set new/update portfolio menu
			case 'save-portfolio-menu':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION["userRole"];
				$res = Api::savePortfolioMenu($req);
				break;
			case 'get-portfolio-courses':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION["userRole"];
				$res = Api::getPortfolioCourses($req);
				break;
			case 'get-portfolio-approvals':
				session_start();
				$res = Api::getPortfolioApprovals();
				break;
			case 'save-portfolio-approval':
				session_start();
				$res = Api::savePortfolioApproval($req);
				break;
			case 'get-contest-coupon':
				$res = Api::getContestCoupon($req);
				break;
			case 'generate-manual-exam-csv':
				$res = Api::generateManualExamCSV($req);
				break;
			case 'generate-manual-exam-form':
				$res = Api::generateManualExamForm($req);
				break;
			case 'save-manual-exam-form':
				session_start();
				@$req->userId = $_SESSION["userId"];
				@$req->userRole = $_SESSION["userRole"];
				$res = Api::saveManualExamForm($req);
				break;
			//to fetch all manual exams in a subject
			case 'get-manual-exams':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getManualExams($req);
				break;
			//to fetch manual exam details by examId
			case 'get-manual-exam-details':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getManualExamDetails($req);
				break;
			//to save manual exam marks
			case 'save-manual-exam-marks':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveManualExamMarks($req);
				break;
			//to save manual exam
			case 'save-manual-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveManualExam($req);
				break;
			//to delete manual exam name
			case 'delete-manual-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::deleteManualExam($req);
				break;
			//to delete manual exam name
			case 'get-manual-exam-student':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentManualExamDetails($req);
				break;
			//to delete submission
			case 'delete-submission':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::deleteSubmission($req);
				break;
			//to generate students for course
			case 'generate-students-for-course':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::generateStudentsForCourse($req);
				break;
			//to inititate student after login (generated students)
			case 'init-student':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::initStudent($req);
				break;
			//to verify and merge duplicate student
			case 'verify-merge-student':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::verifyMergeStudent($req);
				break;
			case 'back_end_course_chats':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::backendCoursechats($req);
				break;
			case 'course-chat-feature-update':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::backendCoursechatUpdate($req);
				break;
			case 'get-institute-students':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getInstituteStudents($req);
				break;
			case 'get-institute-temp-students':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getInstituteTempStudents($req);
				break;
			case 'transfer-student-course':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::transferStudentCourse($req);
				break;
			case 'delete-students':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::deleteStudentCourse($req);
				break;
			//to get joined courses @amit
			case 'student-joined-courses-new':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentJoinedCoursesNew($req);
				break;
			case 'student-subs-courses-new':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentSubsCoursesNew($req);
				break;
			case 'save-profile-new':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveProfileNew($req);
				break;
			case 'get-countries-list':
				$res = Api::getCountriesList();
				break;
			case 'remove-profile-image':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::removeProfileImage($req);
				break;
			case 'get-social-connections':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSocialConnections($req);
				break;
			case 'set-social-connection':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::setSocialConnection($req);
				break;
			case 'set-content-notes':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::setContentNotes($req);
				break;
			case 'get-content-notes':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getContentNotes($req);
				break;
			case 'get-tags':
				$res = Api::queryTags($req);
				break;
			case 'tag-exam-analysis':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::tagExamAnalysis($req);
				break;
			case 'get-exam-tags':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getExamTags($req);
				break;
			case 'exit-cta':
				$res = Api::exitCTA($req);
				break;
			case 'notify-parents':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::notifyParents($req);
				break;
			case 'get-parent-notifications':
				session_start();
				if(!isset($req->userId) && empty($req->userId)) {
					$req->userId = $_SESSION['userId'];
					$req->userRole = $_SESSION['userRole'];
				}
				$res = Api::getParentNotifications($req);
				break;
			case 'get-smileys':
				$res = Api::getSmileys();
				break;
			case 'save-percent-bracket':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::savePercentBracket($req);
				break;
			case 'save-percent-bracket-edit':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::savePercentBracketEdit($req);
				break;
			case 'delete-percent-bracket':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::deletePercentBracket($req);
				break;
			case 'get-percent-ranges':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getPercentRanges($req);
				break;
			case 'get-subject-stuff':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectAllStuff($req);
				break;
			case 'save-subject-grading':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveSubjectGrading($req);
				break;
			case 'save-subject-reward':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveSubjectReward($req);
				break;
			case 'get-student-performance':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getStudentPerformance($req);
				break;
			case 'check-name-for-submission':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::checkNameForSubmission($req);
				break;
			case 'add-submission':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::addSubmission($req);
				break;
			case 'edit-submission':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::editSubmission($req);
				break;
			case 'get-submission':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubmissionDetail($req);
				break;
			case 'save-submission-question':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::saveSubmissionQuestion($req);
				break;
			case 'edit-submission-question':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::editSubmissionQuestion($req);
				break;
			case 'delete-submission-question':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::deleteSubmissionQuestion($req);
				break;
			case 'get-subject-submission-questions':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectSubmissionQuestions($req);
				break;
			case 'get-submission-question-detail':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubmissionQuestionDetail($req);
				break;
			case 'get-submission-questions':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubmissionQuestions($req);
				break;
			case 'live-submission':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::liveSubmission($req);
				break;
			case 'unlive-submission':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::unliveSubmission($req);
				break;
			case 'get-submissions-of-subject':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubjectSubmissions($req);
				break;
			case 'get-submission-questions-import':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubmissionQuestionsImport($req);
				break;
			case 'import-submission-questions':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::importSubmissionQuestions($req);
				break;
			case 'attempt-submission':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::attemptSubmission($req);
				break;
			case 'submission-answer':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::submissionAnswer($req);
				break;
			case 'submit-submission-attempt':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::submitSubmissionAttempt($req);
				break;
			case 'get-check-submission-students':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getCheckSubmissionStudents($req);
				break;
			case 'get-check-submission-questions':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getCheckSubmissionQuestions($req);
				break;
			case 'save-review-for-submission':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveReviewForSubmission($req);
				break;
			case 'save-marks-for-submission':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveMarksForSubmission($req);
				break;
			case 'mark-submission-checked':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::markSubmissionChecked($req);
				break;
			case 'get-submission-attempt':
				session_start();
				$req->userId = $_SESSION["userId"];
				$req->userRole = $_SESSION["userRole"];
				$res = Api::getSubmissionAttempt($req);
				break;
			//to get exam attempts for graph creation
			case 'get-submission-attempt-graph':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubmissionAttemptGraph($req);
				break;
			case 'get-submission-timeline-graph':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				if (isset($_SESSION['student_id']) && !empty($_SESSION['student_id'])) {
					$req->student_id = $_SESSION['student_id'];
				}
				$res = Api::getSubmissionTimeLine($req);
				break;
			case 'get-submission-topper-of-exam':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubmissionTopperDetails($req);
				break;
			case 'get-submission-normalization-graph':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubmissionNormalizationGraph($req);
				break;
			case 'get-subject-student-groups':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getSubjectStudentGroups($req);
				break;
			case 'gen-subject-student-groups':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::generateSubjectStudentGroups($req);
				break;
			case 'add-student-subject-student-group':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::addStudentSubjectStudentGroup($req);
				break;
			case 'remove-student-subject-student-group':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::removeStudentSubjectStudentGroup($req);
				break;
			case 'add-subject-student-group':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::addSubjectStudentGroup($req);
				break;
			case 'remove-subject-student-groups':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::removeSubjectStudentGroups($req);
				break;
			case 'favorite-course':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::favoriteCourse($req);
				break;
			case 'unfavorite-course':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::unfavoriteCourse($req);
				break;
			case 'get-account-terminology':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getAccountTerminology($req);
				break;
			case 'set-account-terminology':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::setAccountTerminology($req);
				break;
			case 'get-deleted-stuff':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getDeletedStuff($req);
				break;
			case 'save-wallet-address':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveWalletAddress($req);
				break;
			case 'get-IGRO-contract':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::getIGROContractData($req);
				break;
			case 'save-crypto-transaction':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::saveCryptoTransaction($req);
				break;
			case 'check-igro-transactions':
				$res = Api::checkIGROTransactions();
				break;
			case 'delete-personal-data':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::deletePersonalData($req);
				break;
			case 'delete-account':
				session_start();
				$req->userId = $_SESSION['userId'];
				$req->userRole = $_SESSION['userRole'];
				$res = Api::deleteAccount($req);
				break;
			default:
				$res = new stdClass();
				$res->status = 0;
				$res->message = "Invalid Action";
				$res->action = $req->action;
				break;
		}
		if(isset($res->exception)) {
			$res->message = $res->exception;
		}
		echo json_encode($res);
	}catch(Exception $ex) {
		$res = new stdClass();
		$res->status = 0;
		$res->message = $ex->getMessage();
		echo json_encode($res);
	}
?>