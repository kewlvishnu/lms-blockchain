<?php
	header('Content-Type: text/plain; charset=utf-8');
	
	// Include the AWS SDK using the Composer autoloader
	require_once 'api/Vendor/aws/aws-autoloader.php';
	
	use Aws\S3\S3Client;
	
	
	function uploadFile($path, $file, $type) {
		try {
			/*$s3Client = S3Client::factory(array(
				'key'    => 'AKIAI6EW5HWPVDY5IJRA',
				'secret' => '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',
				'region' => 'us-west-2'
			));*/
			$s3Client = S3Client::factory(array(
				'key'    => 'AKIAJ6Y7CVXSZQYOSXDQ',
				'secret' => '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',
				'region' => 'us-west-2'
			));
			$result = $s3Client->putObject(array(
				'Bucket'     => 'arcanemind',
				'Key'        => $path,
				'SourceFile' => $file,
				'ContentType' => $type
				/*'Metadata'   => array(
					'Foo' => 'abc',
					'Baz' => '123'
				)*/
			));
			
			// We can poll the object until it is accessible
			$s3Client->waitUntil('ObjectExists', array(
				'Bucket' => 'arcanemind',
				'Key'    => $path
			));
			
			return $result;
		} catch (Exception $e) {
		    echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	//to upload a cropped image from server to s3 and then delete it from server.
	function uploadImage($path, $type) {
		$s3Client = S3Client::factory(array(
			'key'    => 'AKIAI6EW5HWPVDY5IJRA',
			'secret' => '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',
		));
		$result = $s3Client->putObject(array(
			'Bucket'     	=>	'arcanemind',
			'Key'        	=>	$path,
			'Body'		 	=>	fopen($path, 'r+'),
			'ContentType' 	=>	$type
		));
		
		// We can poll the object until it is accessible
		$s3Client->waitUntil('ObjectExists', array(
			'Bucket' => 'arcanemind',
			'Key'    => $path
		));
		
		//deleting the file from our server
		unlink($path);
		
		return $result;
	}
	
?>