<?php
	try{
		/**
		* This makes our life easier when dealing with paths. Everything is relative
		* to the application root now.
		*/
		//~ ini_set('display_errors',1);
		//~ ini_set('display_startup_errors',1);
		//~ error_reporting(-1);
		chdir(dirname(__DIR__));

		$libPath = "Vendor";

		// Loading Zend
		include $libPath . '/Zend/Loader/AutoloaderFactory.php';
		Zend\Loader\AutoloaderFactory::factory(array(
				'Zend\Loader\StandardAutoloader' => array(
						'autoregister_zf' => true,
						'db' => 'Zend\Db\Sql'
				)
		));

		// Get the JSON request
		$req = $_POST;

		//Kill the request if action is not specified
		if(!isset($req["action"])) {
			$res = new stdClass();
			$res->status = 0;
			$res->message = "No action specified";
			echo json_encode($res);
			die();
		}

		require $libPath . "/ArcaneMind/Api.php";

		$action = $req["action"];
		switch($action) {
			case 'tags':
				$res = Api::queryTags($req);
				break;
			case 'course-tags':
				$res = Api::queryCourseTags($req);
				break;
			default:
				$res = new stdClass();
				$res->status = 0;
				$res->message = "Invalid Action";
				$res->action = $req->action;
				break;
		}
		if(isset($res->exception)) {
			$res->message = $res->exception;
		}
		echo json_encode($res);
	}catch(Exception $ex) {
		$res = new stdClass();
		$res->status = 0;
		$res->message = $ex->getMessage();
		echo json_encode($res);
	}
?>