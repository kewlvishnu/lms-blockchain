<?php

require_once dirname(__FILE__) . '/../../Vendor\ArcaneMind\Api.php';
$libPath = dirname(__FILE__) ."/../../Vendor";
        
        // Loading Zend
        include $libPath . '/Zend/Loader/AutoloaderFactory.php';
        Zend\Loader\AutoloaderFactory::factory(array(
                'Zend\Loader\StandardAutoloader' => array(
                        'autoregister_zf' => true,
                        'db' => 'Zend\Db\Sql'
                )
        ));
 
class CourseTests extends PHPUnit_Framework_TestCase
{
    private $user;
 
    protected function setUp()
    {
        print_r(__DIR__.'api/Vendor/ArcaneMind/Api.php');
        $this->user = new Api();
    }
	
 
    protected function tearDown()
    {
        $this->user = NULL;
    }
 	//name not exists
 	
    //checking status ==1 as status =1 means aviabale
 	public function testSubjectCreateP(){
    	$req = new stdClass();
        $req->userId=378;
		$req->userRole=1;
        $req->courseId='119';
        $req->subjectName='His is phpunit subjct';
        $req->subjectDesc="THis is phpunit test";
		$result = $this->user->createSubject($req);
        $this->assertEquals('Subject created!', $result->message);
    }
    
    //checking if subject already created means aviabale
    public function testSubjectSectionCreatep(){
        $req = new stdClass();
        $req->userId=378;
        $req->userRole=1;
        $req->courseId='119';
        $req->subjectId='180';
        $req->title="New PhpUnit section";
        $req->chapterName='New PhpUnit section';
        $req->demo =0;
        $req->headingId =0;
        $result = $this->user->createChapter($req);
        $this->assertEquals('Chapter created!', $result->message);

    }

    //lecture Test Case Postive Case
    public function testSubjectLectureCreateP(){
        $req = new stdClass();
        $req->userId=378;
        $req->userRole=1;
        $req->title="New PhpUnit NewLect";       
        $req->chapterId=1;
        $req->demo =0;
        $req->headingId =595;
        $req->contentId=0;
        $result = $this->user->saveContent($req);
        $this->assertEquals('Content created!',$result->message);
    }

    

    //checking if subject already created means aviabale
     public function testSubjectCreateN(){
        $req = new stdClass();
        $req->name="New PhpUnit Subject";
        $req->userId=378;
        $req->userRole=1;
        $req->courseId='119';
        $req->subjectName='subjct45';
        $req->subjectDesc="THis is phpunit test";
        $result = $this->user->createSubject($req);
        $this->assertEquals('Please select a different name as you have already created a subject by this name', $result->message);
    }

    
    //Negative case already created subject name
    public function testSubjectSectionCreateN(){
        $req = new stdClass();
        $req->userId=378;
        $req->userRole=1;
        $req->courseId='119';
        $req->subjectId='180';
        $req->title="New PhpUnit section";
        $req->chapterName='New PhpUnit section';
        $req->demo =0;
        $req->headingId =0;
        $result = $this->user->createChapter($req);
        $this->assertEquals('Please select a different name as you have already created a chapter by this name', $result->message);
    }

    //Negative case already created subject name
    public function testEditSubjectSection(){
        $req = new stdClass();
        $req->userId=378;
        $req->userRole=1;
        $req->courseId='119';
        $req->subjectId='180';
        $req->sectionId="556"
        $req->title="Change PHP unit section";
        $req->chapterName='New PhpUnit section';
        $req->demo =0;
       // $req->headingId =0;
       // $result = $this->user->editChapter($req);
        //$this->assertEquals('Please select a different name as you have already created a chapter by this name', $result->message);
    }


    


 

 	
   
}

