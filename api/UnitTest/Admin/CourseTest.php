<?php

require_once dirname(__FILE__) . '/../../Vendor\ArcaneMind\Api.php';
$libPath = dirname(__FILE__) ."/../../Vendor";
        
        // Loading Zend
        include $libPath . '/Zend/Loader/AutoloaderFactory.php';
        Zend\Loader\AutoloaderFactory::factory(array(
                'Zend\Loader\StandardAutoloader' => array(
                        'autoregister_zf' => true,
                        'db' => 'Zend\Db\Sql'
                )
        ));
 
class CourseTests extends PHPUnit_Framework_TestCase
{
    private $user;
 
    protected function setUp()
    {
        print_r(__DIR__.'api/Vendor/ArcaneMind/Api.php');
        $this->user = new Api();
    }
	
 
    protected function tearDown()
    {
        $this->user = NULL;
    }
 	//name not exists
 	//checking status ==1 as status =1 means aviabale
 	 public function testDuplicateCourseNameP(){
    	$req = new stdClass();
		$req->name="New PhpUnitCHECkCOurse";
        $req->userId=378;
		$req->userRole=1;
		$result = $this->user->checkDuplicateCourse($req);
        $this->assertEquals('1', $result->available);
    }

   	//name exists negative Test Case
   	public function testDuplicateCourseNameN(){
    	$req = new stdClass();
		$req->name="IAS - 2016";
        $req->userId=378;
		$req->userRole=1;
		$result = $this->user->checkDuplicateCourse($req);
        $this->assertEquals('', $result->available);
    }
 	

 	////name exists negative Test Case
   	public function testDuplicateCreateCourseP(){
    	$req = new stdClass();
		$req->courseName="Check123";
        $req->userId=378;
		$req->userRole=1;
   		$req->courseSubtitle ='eih hf8ihf8he8if';
    	$req->courseDesc = 'wuh gge3eghgdbuwhd uhduhdhduehuehcdhcb';
    	$req->liveDate = 1455042600000;
    	$req->setEndDate = 0;
    	$req->endDate ='';
    	$courseCat=array();
    	array_push($courseCat, 2);
    	$req->courseCateg = $courseCat;
   		$req->targetAudience = 'students';
   		$req->tags ='';    
		$result = $this->user->createCourse($req);
        $this->assertEquals('Course created!', $result->message);
    }

    ////name exists negative Test Case
   	public function testDuplicateCreateCourseN(){
    	$req = new stdClass();
		$req->courseName="Check123";
        $req->userId=378;
		$req->userRole=4;
   		$req->courseSubtitle ='eih hf8ihf8he8if';
    	$req->courseDesc = 'wuh gge3eghgdbuwhd uhduhdhduehuehcdhcb';
    	$req->liveDate = 1455042600000;
    	$req->setEndDate = 0;
    	$req->endDate ='';
    	$courseCat=array();
    	array_push($courseCat, 2);
    	$req->courseCateg = $courseCat;
   		$req->targetAudience = 'students';
   		$req->tags ='';    
		$result = $this->user->createCourse($req);
        $this->assertEquals('Unspecified error.', $result->message);
    }

}
