<?php

require_once dirname(__FILE__) . '/../../Vendor\ArcaneMind\Api.php';
$libPath = dirname(__FILE__) ."/../../Vendor";
        
        // Loading Zend
        include $libPath . '/Zend/Loader/AutoloaderFactory.php';
        Zend\Loader\AutoloaderFactory::factory(array(
                'Zend\Loader\StandardAutoloader' => array(
                        'autoregister_zf' => true,
                        'db' => 'Zend\Db\Sql'
                )
        ));
 
class CourseTests extends PHPUnit_Framework_TestCase
{
    private $user;
 
    protected function setUp()
    {
        print_r(__DIR__.'api/Vendor/ArcaneMind/Api.php');
        $this->user = new Api();
    }
	
 
    protected function tearDown()
    {
        $this->user = NULL;
    }
 	//name not exists
 	//checking status ==1 as status =1 means aviabale
 	public function testDuplicateCourseNameP(){
    	$req = new stdClass();
		$req->name="Exam PhpUnit2";
        $req->courseId=99;
        $req->type='Exam';
        $req->chapterId='563';
        $req->subjectId=160;
        $req->status=0;
        $req->showResult='immediately';
        $req->showResultTill='1';
        $req->showResultTillDate='';
        $req->startDate='1455100080000';
        $req->endDate='1456568880000';
        $req->attempts='1';
        $req->tt_status='0';
        $req->totalTime='60';
        $req->gapTime='0';
        $req->time=[0,0];
        $req->sectionOrder='0';
        $req->endBeforeTime='0';
        $req->powerOption='0';
        $req->sections=["Section 1","Section 2"];
        $req->weights=[0,1];
        
        $req->userId=378;
		$req->userRole=1;
		$result = $this->user->addExam($req);
        $this->assertEquals('1', $result->status);
    }

    public function testDuplicateCourseNameN(){
        $req = new stdClass();
        $req->name="Exam PhpUnit2";
        $req->courseId=99;
        $req->type='Exam';
        $req->chapterId='563';
        $req->subjectId=160;
        $req->status=0;
        $req->showResult='immediately';
        $req->showResultTill='1';
        $req->showResultTillDate='';
        $req->startDate='1455100080000';
        $req->endDate='1456568880000';
        $req->attempts='1';
        $req->tt_status='0';
        $req->totalTime='60';
        $req->gapTime='0';
        $req->time=[0,0];
        $req->sectionOrder='0';
        $req->endBeforeTime='0';
        $req->powerOption='0';
        $req->sections=["Section 1","Section 2"];
        $req->weights=[0,1];
        
        $req->userId=378;
        $req->userRole=1;
        $result = $this->user->addExam($req);
        print_r($result);
        $this->assertEquals('0', $result->status);
    }
    public function testDuplicateCourseNameP(){
        $req = new stdClass();
        $req->name="Exam PhpUnit2";
        $req->examId='206';
        $req->sectionId=253;
        $req->totalMarks=10;
        $req->randomQuestions=0;
        $req->categories='1';
        $req->showResultTillDate='';
        $req->startDate='1455100080000';
        $req->endDate='1456568880000';
        $req->attempts='1';
        $req->tt_status='0';
        $req->totalTime='60';
        $req->gapTime='0';
        $req->time=[0,0];
        $req->sectionOrder='0';
        $req->endBeforeTime='0';
        $req->powerOption='0';
        $req->sections=["Section 1","Section 2"];
        $req->weights=[0,1];
        
        $req->userId=378;
        $req->userRole=1;
        $result = $this->user->addExam($req);
        $this->assertEquals('1', $result->status);
    }

    public function testSaveQuestionsP(){
        $req = new stdClass();
        $req->name="Exam PhpUnit2";
        $req->examId='206';
        $req->sectionId=253;
        $req->categoryId=235;
        $req->parentId=0;
        $req->desc='';
        $req->question='<p>sjhush</p>\n';
        $req->questionType='0';
        $req->categorySwitch='false';
        $req->status='1';
        $obj= new stdClass();
        $obj->opt='jjajaja';
        $obj->cState='1';
        $req->options=[$obj];      

        $req->userId=378;
        $req->userRole=1;
        $result = $this->user->saveQuestion($req);
        $this->assertEquals('1', $result->status);
    }

    public function testSaveQuestionsN1(){
        $req = new stdClass();
        $req->name="Exam PhpUnit2";
        $req->examId='';
        $req->sectionId='';
        $req->categoryId=235;
        $req->parentId=0;
        $req->desc='';
        $req->question='<p>sjhush</p>\n';
        $req->questionType='0';
        $req->categorySwitch='false';
        $req->status='1';
        $obj= new stdClass();
        $obj->opt='jjajaja';
        $obj->cState='1';
        $req->options=[$obj];      

        $req->userId=378;
        $req->userRole=1;
        $result = $this->user->saveQuestion($req);
        $this->assertEquals('1', $result->status);
    }
    //userid changed negative test case
    public function testSaveQuestionsN2(){
        $req = new stdClass();
        $req->name="Exam PhpUnit2";
        $req->examId='206';
        $req->sectionId=253;
        $req->categoryId=235;
        $req->parentId=0;
        $req->desc='';
        $req->question='<p>sjhush</p>\n';
        $req->questionType='0';
        $req->categorySwitch='false';
        $req->status='1';
        $obj= new stdClass();
        $obj->opt='jjajaja';
        $obj->cState='1';
        $req->options=[$obj];      

        $req->userId=379;
        $req->userRole=1;
        $result = $this->user->saveQuestion($req);
        $this->assertEquals('1', $result->status);
    }

    public function testCategoryP(){
        $req = new stdClass();
        $req->name="Exam PhpUnit2";
        $req->examId='206';
        $req->sectionId=253;
        $req->totalMarks=10;
        $req->randomQuestions=0;
        $obj=new stdClass();
        $obj->correct=2;
        $obj->questionType=0;
        $obj->wrong=1;
        $obj->required=5;
        $obj->categoryId=0;
        $obj->sectionId=253;
        $obj->examId=206;
        $obj->weight=0;

        $req->categories=[$obj];
        
        
        $req->userId=378;
        $req->userRole=1;
        $result = $this->user->saveCategories($req);
        $this->assertEquals('1', $result->status);
    }

    public function testCategoryn1(){
        $req = new stdClass();
        $req->name="Exam PhpUnit2";
        $req->examId='';
        $req->sectionId='';
        $req->totalMarks=10;
        $req->randomQuestions=0;
        $obj=new stdClass();
        $obj->correct=2;
        $obj->questionType=0;
        $obj->wrong=1;
        $obj->required=5;
        $obj->categoryId=0;
        $obj->sectionId=253;
        $obj->examId=206;
        $obj->weight=0;

        $req->categories=[$obj];
        
        
        $req->userId=378;
        $req->userRole=1;
        $result = $this->user->saveCategories($req);
        $this->assertEquals('1', $result->status);
    }
    public function testCategoryN2(){
        $req = new stdClass();
        $req->name="Exam PhpUnit2";
        $req->examId='206';
        $req->sectionId=253;
        $req->totalMarks=10;
        $req->randomQuestions=0;
        $obj=new stdClass();
        $obj->correct=2;
        $obj->questionType=0;
        $obj->wrong=1;
        $obj->required=5;
        $obj->categoryId=0;
        $obj->sectionId=253;
        $obj->examId=206;
        $obj->weight=0;

        $req->categories=[$obj];
        
        
        $req->userId=379;
        $req->userRole=1;
        $result = $this->user->saveCategories($req);
        $this->assertEquals('1', $result->status);
    }



    public function testEditCategoryP(){
        $req = new stdClass();
        $req->name="Exam PhpUnit2";
        $req->examId='206';
        $req->sectionId=253;
        $req->totalMarks=20;
        $req->randomQuestions=0;
        $obj=new stdClass();
        $obj->correct=2;
        $obj->questionType=1;
        $obj->wrong=1;
        $obj->required=5;
        $obj->categoryId=236;
        $obj->sectionId=253;
        $obj->examId=206;
        $obj->weight=0;
        $ob1=new stdClass();
        $obj1->correct=2;
        $obj1->questionType=0;
        $obj1->wrong=1;
        $obj1->required=5;
        $obj1->categoryId=235;
        $obj1->sectionId=253;
        $obj1->examId=206;
        $obj1->weight=1;
        $req->categories=[$obj,$obj1];
        
        
        $req->userId=378;
        $req->userRole=1;
        $result = $this->user->saveCategories($req);
        $this->assertEquals('1', $result->status);
    }


    public function testEditQuestionsP(){
        $req = new stdClass();
        $req->name="Exam PhpUnit2";
        $req->examId='206';
        $req->questionId='5793';
        $req->sectionId=253;
        $req->categoryId=235;
        $req->parentId=0;
        $req->desc='';
        $req->question='<p>NEw edited questions/p>\n';
        $req->questionType='0';
        $req->categorySwitch='false';
        $req->status='1';
        $obj= new stdClass();
        $obj->opt='jjajaja';
        $obj->cState='1';
        $req->options=[$obj];      

        $req->userId=379;
        $req->userRole=1;
        $req->status=1
        $result = $this->user->editQuestion($req);
        $this->assertEquals('1', $result->status);
    }

     public function testEditQuestionsN2(){
        $req = new stdClass();
        $req->name="Exam PhpUnit2";
        $req->examId='206';
        $req->questionId='';
        $req->sectionId=253;
        $req->categoryId=235;
        $req->parentId=0;
        $req->desc='';
        $req->question='<p>NEw edited questions/p>\n';
        $req->questionType='0';
        $req->categorySwitch='false';
        $req->status='1';
        $obj= new stdClass();
        $obj->opt='jjajaja';
        $obj->cState='1';
        $req->options=[$obj];      

        $req->userId=379;
        $req->userRole=1;
        $req->status=1
        $result = $this->user->editQuestion($req);
        $this->assertEquals('1', $result->status);
    }

    
}




