<?php

require_once dirname(__FILE__) . '/../../Vendor\ArcaneMind\Api.php';
$libPath = dirname(__FILE__) ."/../../Vendor";
        
        // Loading Zend
        include $libPath . '/Zend/Loader/AutoloaderFactory.php';
        Zend\Loader\AutoloaderFactory::factory(array(
                'Zend\Loader\StandardAutoloader' => array(
                        'autoregister_zf' => true,
                        'db' => 'Zend\Db\Sql'
                )
        ));
 
class ExamTests extends PHPUnit_Framework_TestCase
{
    private $user;
 
    protected function setUp()
    {
       
        $this->user = new Api();
    }
	
 
    protected function tearDown()
    {
        $this->user = NULL;
    }
	
	public function testStudentResultP(){
    	$req = new stdClass();
        $req->userId=381;
		$req->userRole=4;
		$req->examId=187;
		$result = $this->user->getResult($req);
        $this->assertEquals('1', $result->status);
    }
	//wrong result ID
	public function testStudentResultP(){
    	$req = new stdClass();
        $req->userId=381;
		$req->userRole=4;
		$req->examId=1187;
		$result = $this->user->getResult($req);
        $this->assertEquals('0', $result->status);
    }
	//{"action":"get-section-questions","sectionId":"221","attemptId":"173"
	
	
	public function testSectionQuestionsP(){
    	$req = new stdClass();
        $req->userId=381;
		$req->userRole=4;
		$req->sectionId=221;
		$req->attemptId=173;
		$result = $this->user->getSectionQuestions($req);
        $this->assertEquals('1', $result->status);
    }
	
	
	//section attempt are of differnt ids
	public function testSectionQuestionsN(){
    	$req = new stdClass();
        $req->userId=381;
		$req->userRole=4;
		$req->sectionId=221;
		$req->attemptId=13;
		$result = $this->user->getSectionQuestions($req);
        $this->assertEquals('0', $result->status);
    }
	//{"action":"get-topper-of-exam","examId":"187","attemptId":"173"}
	public function testTopperDetailsP(){
    	$req = new stdClass();
        $req->userId=381;
		$req->userRole=4;
		$req->examId=187;
		$req->attemptId=173;
		$result = $this->user->getTopperDetails($req);
        $this->assertEquals('1', $result->status);
    }
	
	
	//section attempt are of differnt ids
	public function testTopperDetailsN(){
    	$req = new stdClass();
        $req->userId=381;
		$req->userRole=4;
		$req->examId=187;
		$req->attemptId=1;
		$result = $this->user->getTopperDetails($req);
        $this->assertEquals('0', $result->status);
    }
	
}


