<?php
require '../Vendor/ArcaneMind/Api.php';
chdir(dirname(__DIR__));

		$libPath = "../Vendor";
		
		// Loading Zend
		include $libPath . '/Zend/Loader/AutoloaderFactory.php';
		Zend\Loader\AutoloaderFactory::factory(array(
				'Zend\Loader\StandardAutoloader' => array(
						'autoregister_zf' => true,
						'db' => 'Zend\Db\Sql'
				)
		));
 
class CommonTests extends PHPUnit_Framework_TestCase
{
    private $user;
 
    protected function setUp()
    {
        $this->user = new Api();
    }
	
 
    protected function tearDown()
    {
        $this->user = NULL;
    }
 
    public function testcheckLogin()
    { 
		$req = new stdClass();
		$req->password="ayush005";
        $req->username="mackayush@gmail.com";
		$req->userRole=4;
		$result = $this->user->login($req);
        $this->assertEquals('1', $result->valid);

		$req = new stdClass();
		$req->password="ayush006";
        $req->username="mackayush@gmail.com";
		$req->userRole=4;
		$result = $this->user->login($req);
        $this->assertEquals('0', $result->status);
		
		$req = new stdClass();
		$req->password="ayush005";
        $req->username="mackyush@gmail.com";
		$req->userRole=4;
		$result = $this->user->login($req);
        $this->assertEquals('0', $result->status);
		
		$req = new stdClass();
		$req->password="ayush005";
        $req->username="mackayush@gmail.com";
		$req->userRole=1;
		$result = $this->user->login($req);
        $this->assertEquals('0', $result->status);
		
		
		
		/* $response = $this->client->post( [
	        'json' => [
	            'action'   		 => 'login',
	            'username'   	 => 'mackayush@gmil.com',
	            'password'   	 => 'ayush005'
	        ]
	    ]);
	
	    $this->assertEquals(200, $response->getStatusCode());
	
	    $data = json_decode($response->getBody(), true);
	
	    $this->assertEquals('true', $data->valid);
		*/
    }
 
}
