<?php

require_once dirname(__FILE__) . '/../../Vendor\ArcaneMind\Api.php';
$libPath = dirname(__FILE__) ."/../../Vendor";
        
        // Loading Zend
        include $libPath . '/Zend/Loader/AutoloaderFactory.php';
        Zend\Loader\AutoloaderFactory::factory(array(
                'Zend\Loader\StandardAutoloader' => array(
                        'autoregister_zf' => true,
                        'db' => 'Zend\Db\Sql'
                )
        ));
 
class SubjectTests extends PHPUnit_Framework_TestCase
{
    private $user;
 
    protected function setUp()
    {
       
        $this->user = new Api();
    }
	
 
    protected function tearDown()
    {
        $this->user = NULL;
    }
	//positve test
	public function testStudentCoursesListP(){
    	$req = new stdClass();
        $req->userId=381;
		$req->userRole=4;
		$result = $this->user->getStudentJoinedCourses($req);
        $this->assertEquals('0', $result->status);
    }
	//testing wrong user
	public function testStudentCoursesListN(){
    	$req = new stdClass();
        $req->userId=378;
		$req->userRole=1;
		$result = $this->user->getStudentJoinedCourses($req);
		//print_r($result);
      	$this->assertEquals('0', $result->status);
    }
	public function testStudentCoursesP(){
    	$req = new stdClass();
        $req->userId=381;
		$req->userRole=4;
		$req->courseId=99;
		$result = $this->user->getCourseSubjectsForStudent($req);
        $this->assertEquals('1', $result->status);
    }
	//correct  user but course not enrolled 
	public function testStudentCoursesN1(){
    	$req = new stdClass();
        $req->userId=381;
		$req->userRole=4;
		$req->courseId=100;
		$result = $this->user->getCourseSubjectsForStudent($req);
        $this->assertEquals('0', $result->status);
    }
	//testing wrong user for wrong course
	public function testStudentCoursesn2(){
    	$req = new stdClass();
        $req->userId=378;
		$req->userRole=1;
		$req->courseId=99;
		$result = $this->user->getCourseSubjectsForStudent($req);
        $this->assertEquals('0', $result->status);
    }
	
	
	//testing wrong user for wrong course
	public function testStudentCoursesAcessP(){
    	$req = new stdClass();
        $req->userId=381;
		$req->userRole=4;
		$req->courseId=99;
		$req->subjectId=159;
		$result = $this->user->checkCourseAcess($req);
        $this->assertEquals('1', $result->status);
    }
	
	//testing wrong user for wrong course
	public function testStudentCoursesAcessN1(){
    	$req = new stdClass();
        $req->userId=381;
		$req->userRole=4;
		$req->courseId=154;
		$req->subjectId=163;
		$result = $this->user->checkCourseAcess($req);
        $this->assertEquals('0', $result->status);
    }
	
	//testing wrong user for wrong course
	public function testStudentCoursesAcessN2(){
    	$req = new stdClass();
        $req->userId=381;
		$req->userRole=4;
		$req->courseId=99;
		$req->subjectId=163;
		$result = $this->user->checkCourseAcess($req);
        $this->assertEquals('0', $result->status);
    }
	
	//testing wrong user for wrong course
	public function testStudentCoursesAcessN3(){
    	$req = new stdClass();
        $req->userId=148;
		$req->userRole=4;
		$req->courseId=99;
		$req->subjectId=163;
		$result = $this->user->checkCourseAcess($req);
       	$this->assertEquals('0', $result->status);
    }
	
	//studentExamsChapterWise
	public function testStudenttExamsChapterP(){
    	$req = new stdClass();
        $req->userId=381;
		$req->userRole=4;
		$req->courseId=99;
		$req->subjectId=159;
		$result = $this->user->studentExamsChapterWise($req);
        $this->assertEquals('1', $result->status);
    }
	
	//testing wrong user for wrong course
	public function testStudenttExamsChapterN1(){
    	$req = new stdClass();
        $req->userId=381;
		$req->userRole=4;
		$req->courseId=154;
		$req->subjectId=163;
		$result = $this->user->studentExamsChapterWise($req);
        $this->assertEquals('0', $result->status);
    }
	//testing wrong user for wrong course
	public function testStudenttExamsChapterN2(){
    	$req = new stdClass();
        $req->userId=148;
		$req->userRole=4;
		$req->courseId=154;
		$req->subjectId=163;
		$result = $this->user->studentExamsChapterWise($req);
        $this->assertEquals('0', $result->status);
    }
	//studentExamsChapterWise
	public function testgetContentStuffP(){
    	$req = new stdClass();
        $req->userId=381;
		$req->userRole=4;
		$req->contentId=2166;
		$result = $this->user->getContentStuff($req);
        $this->assertEquals('1', $result->status);
    }
	
	//testing wrong user for wrong course
	public function testgetContentStuffN1(){
    	$req = new stdClass();
        $req->userId=381;
		$req->userRole=4;
		$req->contentId=1166;
		$result = $this->user->getContentStuff($req);
        $this->assertEquals('0', $result->status);
    }
	//testing wrong user for wrong course
	public function testgetContentStuffN2(){
    	$req = new stdClass();
        $req->userId=148;
		$req->userRole=4;
		$req->contentId=2166;
		$result = $this->user->getContentStuff($req);
       	$this->assertEquals('0', $result->status);
    }
	
	/*
	///check here
	//
	checkCourseAcess
 	//name not exists
 	//checking status ==1 as status =1 means aviabale
 	 public function testDuplicateCourseNameP(){
    	$req = new stdClass();
		$req->name="New PhpUnitCHECkCOurse";
        $req->userId=378;
		$req->userRole=1;
		$result = $this->user->checkDuplicateCourse($req);
        $this->assertEquals('1', $result->available);
    }

   	//name exists negative Test Case
   	public function testDuplicateCourseNameN(){
    	$req = new stdClass();
		$req->name="IAS - 2016";
        $req->userId=378;
		$req->userRole=1;
		$result = $this->user->checkDuplicateCourse($req);
        $this->assertEquals('', $result->available);
    }
 	

 	////name exists negative Test Case
   	public function testDuplicateCreateCourseP(){
    	$req = new stdClass();
		$req->courseName="Check123";
        $req->userId=378;
		$req->userRole=1;
   		$req->courseSubtitle ='eih hf8ihf8he8if';
    	$req->courseDesc = 'wuh gge3eghgdbuwhd uhduhdhduehuehcdhcb';
    	$req->liveDate = 1455042600000;
    	$req->setEndDate = 0;
    	$req->endDate ='';
    	$courseCat=array();
    	array_push($courseCat, 2);
    	$req->courseCateg = $courseCat;
   		$req->targetAudience = 'students';
   		$req->tags ='';    
		$result = $this->user->createCourse($req);
        $this->assertEquals('Course created!', $result->message);
    }

    ////name exists negative Test Case
   	public function testDuplicateCreateCourseN(){
    	$req = new stdClass();
		$req->courseName="Check123";
        $req->userId=378;
		$req->userRole=4;
   		$req->courseSubtitle ='eih hf8ihf8he8if';
    	$req->courseDesc = 'wuh gge3eghgdbuwhd uhduhdhduehuehcdhcb';
    	$req->liveDate = 1455042600000;
    	$req->setEndDate = 0;
    	$req->endDate ='';
    	$courseCat=array();
    	array_push($courseCat, 2);
    	$req->courseCateg = $courseCat;
   		$req->targetAudience = 'students';
   		$req->tags ='';    
		$result = $this->user->createCourse($req);
        $this->assertEquals('Unspecified error.', $result->message);
    }
	*/

}
