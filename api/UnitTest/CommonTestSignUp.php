<?php
require_once dirname(__FILE__) . '/../Vendor\ArcaneMind\Api.php';
 
$libPath = dirname(__FILE__) ."/../Vendor";
		
		// Loading Zend
		include $libPath . '/Zend/Loader/AutoloaderFactory.php';
		Zend\Loader\AutoloaderFactory::factory(array(
				'Zend\Loader\StandardAutoloader' => array(
						'autoregister_zf' => true,
						'db' => 'Zend\Db\Sql'
				)
		));


class CommonTests extends PHPUnit_Framework_TestCase
{
    private $user;
 
    protected function setUp()
    {
        $this->user = new Api();
    }
	
 
    protected function tearDown()
    {
        $this->user = NULL;
    }
 
    public function testRegisterShort()
    { 
		//student
		//check for already present user email registered
		$req = new stdClass();
		$req->password="qwerty123";
        $req->firstName="ayush";
		$req->lastName="pandey";
		$req->username="mackayush";
		$req->email="mackayush@gmail.com";
		$req->userType="student";
		$req->userRole=4;
		$result = $this->user->registerUserShort($req);
		print_r($result);
        $this->assertEquals('0', $result->status);

		//check for valid user registration
		$req = new stdClass();
		$req->password="qwerty123";
        $req->firstName="ayush";
		$req->lastName="pandey";
		$req->username="mackayush1";
		$req->email="mackayush1@yopmail.com";
		$req->userType="student";
		$req->userRole=4;
		$result = $this->user->registerUserShort($req);
		print_r($result);
        $this->assertEquals('0', $result->status);
		
		//prof
		//check for already present user email registered
		$req = new stdClass();
		$req->password="qwerty123";
        $req->firstName="ayush";
		$req->lastName="pandey";
		$req->username="professor1";
		$req->email="professor@yopmil.com";
		$req->userType="professor";
		$req->userRole=2;
		$result = $this->user->registerUserShort($req);
        $this->assertEquals('0', $result->status);

		//check for valid user registration
		$req = new stdClass();
		$req->password="qwerty123";
        $req->firstName="prof";
		$req->lastName="prof1";
		$req->username="professor1";
		$req->email="professor1@yopmil.com";
		$req->userType="professor";
		$req->userRole=2;
		$result = $this->user->registerUserShort($req);
        $this->assertEquals('1', $result->status);
		//check for already present user email registered
		$req = new stdClass();
		$req->password="qwerty123";
        $req->firstName="institute";
		$req->lastName="institute";
		$req->username="institute1";
		$req->email="institute@yopmail.com";
		$req->userType="institute";
		$req->userRole=1;
		$result = $this->user->registerUserShort($req);
        $this->assertEquals('0', $result->status);

		//check for valid user registration
		$req = new stdClass();
		$req->password="qwerty123";
        $req->firstName="yopmail";
		$req->lastName="intstitute";
		$req->username="institute1";
		$req->email="institute1@yopmail.com";
		$req->userType="institute";
		$req->userRole=1;
		$result = $this->user->registerUserShort($req);
        $this->assertEquals('1', $result->status);
		
		
    }

	public function testRegisterComplete()
    { 
		//student
		//check for already present user email registered
		$req = new stdClass();
		$req->password="qwerty123";
        $req->firstName="ayush";
		$req->lastName="pandey";
		$req->username="mackayush";
		$req->email="mackayush@gmail.com";
		$req->userType="student";
		$req->userRole=4;
		$req->contactMobilePrefix='91';
        $req->contactMobile='7406616834';
        $req->contactLandlinePrefix='';
        $req->contactLandline='';
        $req->addressCountryId='';
       	$req->addressStreet='';
        $req->addressCity='';
        $req->addressState='';
		$req->addressPin='';
		$req->gender="Male";
		$req->dob='';
                
		$result = $this->user->registerUser($req);
        $this->assertEquals('0', $result->status);
		
		
		$req = new stdClass();
		$req->password="qwerty123";
        $req->firstName="ayush";
		$req->lastName="pandey";
		$req->username="mackprof";
		$req->email="mackprof@yopmail.com";
		$req->userType="professor";
		$req->userRole=2;
		$req->contactMobilePrefix='91';
        $req->contactMobile='7406616834';
        $req->contactLandlinePrefix='';
        $req->contactLandline='';
        $req->addressCountryId='';
       	$req->addressStreet='';
        $req->addressCity='';
        $req->addressState='';
		$req->addressPin='';
		$req->gender="Male";
		$req->dob='';
                
		$result = $this->user->registerUser($req);
		
        $this->assertEquals('0', $result->status);
		$req = new stdClass();
		$req->password="qwerty123";
        $req->firstName="ayush";
		$req->lastName="pandey";
		$req->username="mackinsti";
		$req->email="mackinsti@yopmail.com";
		$req->userType="institute";
		$req->userRole=2;
		$req->contactMobilePrefix='91';
        $req->contactMobile='7406616834';
        $req->contactLandlinePrefix='';
        $req->contactLandline='';
        $req->addressCountryId='';
       	$req->addressStreet='';
        $req->addressCity='';
        $req->addressState='';
		$req->addressPin='';
		$req->gender="Male";
		$req->dob='';
                
		$result = $this->user->registerUser($req);
        $this->assertEquals('0', $result->status);
		
		
    }
 
}
