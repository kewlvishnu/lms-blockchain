<?php
    // Loading Zend
    @session_start();
    //var_dump($_SESSION);
    $global = new stdClass();
    if(!(isset($_SESSION['userId']) && !empty($_SESSION['userId']))) {
        header('location:../');
    }
    $libPath = "../api/Vendor";
    include $libPath . '/Zend/Loader/AutoloaderFactory.php';
    require $libPath . "/ArcaneMind/Api.php";
    Zend\Loader\AutoloaderFactory::factory(array(
            'Zend\Loader\StandardAutoloader' => array(
                    'autoregister_zf' => true,
                    'db' => 'Zend\Db\Sql'
            )
    ));
    require_once '../config/url.functions.php';
    $pageArr = explode('/', $pageUrl);
    $chatRoomId = end($pageArr);
    $chatRoomId = (int)$chatRoomId;
    
    require_once '../api/Vendor/ArcaneMind/Coursechat.php';
    $data = new stdClass();
    $data->chatRoomId = $chatRoomId;
    $data->userId = $_SESSION['userId'];
    $data->userRole = $_SESSION['userRole'];
    $cc = new Coursechat();
    $res = $cc->chatValidity($data);
    if ($res->status == 0) {
        header("location:".$sitepath404);
    } else {
        $_SESSION['name'] = $res->chatRoom['userName'];
        $_SESSION['avatar'] = $res->chatRoom['avatar'];
        $chatRoom = $res->chatRoom;
    }

    $messengerRoom = $chatPrefix.$chatRoomId;

    $favicon = $sitepath."img/favicon.ico";

    if(isset($_GET['pageDetailId']) && !empty($_GET['pageDetailId'])) {
        $actual_link  = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $temp_link    = explode("/", $actual_link);
        $sub_link = explode(".", $temp_link[2]);
        $final_link = str_replace($sub_link[0], 'www', $actual_link);
        $subdomain = trim($_GET['pageDetailId']);
        $res = Api::getPortfolioBySlug($subdomain);
        if($res->valid) {
            $portfolioDetails = $res;
            $portfolioName    = $portfolioDetails->portfolio['name'];
            $logoPath = $res->portfolio['img'];
            if(!empty($res->portfolio['favicon'])) {
                $favicon  = $res->portfolio['favicon'];
            }
        } else {
            //header('location:'.$final_link);
        }
    }
    //$title = 'ArcaneMind | Online Courses | Entrance Exam Preparation';
    $description = 'Arcanemind is a platform & marketplace for online courses & entrance exam preparation. We have the best instructors and institutes in the world, we ensure that the content and questions are of the highest quality.';
    $image = 'http://dhzkq6drovqw5.cloudfront.net/fb-arcanemind.jpg';

    // setting link for logo
    $logolink = $sitepath;

    if (isset($_SESSION["userId"]) && !empty($_SESSION["userId"])) {
        $logolink = $sitepathCourses;
    }
    if (isset($_SESSION["temp"]) && $_SESSION["temp"]==1) {
        header('location:../init-student/');
    }

    if(isset($logoPath) && !empty($logoPath)) {
        $logoBlock = '<a class="brand" href="'.$logolink.'">
                        <span class="logo-portfolio">
                            <img src="'.$logoPath.'" alt="'.$portfolioName.'" class="img-logo" />'.$portfolioName.'
                        </span>
                    </a>';
    } else {
        $logoBlock = '<a class="brand" href="'.$logolink.'"><img src="'.$sitepath.'img/arcanemind-logo.png" width="200" height="53" class="img-responsive" alt="Arcanemind"></a>';
    }

    require_once '../api/Vendor/ArcaneMind/Course.php';
    $c = new Course();
    $res = $c->getCourseCategories();
    if ($res->status == 1) {
        $global->courseCategories = $res->courseCategories;
    }
    $courseCategories = "";
    if (isset($global->courseCategories) && (count($global->courseCategories)>0)) {
        $courseCategories = '<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Course Categories <span class="caret"></span></a>
                                <ul class="dropdown-menu">';
        foreach ($global->courseCategories as $key => $value) {
            $courseCategories.= "<li><a href='{$sitepathCourses}{$value['slug']}/'>".htmlentities($value['category'])."</a></li>";
        }
        $courseCategories.= '       <li role="separator" class="divider"></li>
                                    <li><a href="'.$sitepathCourses.'">All Courses</a></li>
                                </ul>
                            </li>';
    }

    if(isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
        if($_SESSION['userRole'] == 4) {
            $dashBoard = '<a href=\"'.$sitepath.'student/\" class=\"btn btn-warning\">Back to Dashboard</a>';
        }
        else if($_SESSION['userRole'] == 1){
            $dashBoard = '<a href=\"'.$sitepath.'admin/dashboard.php\" class=\"btn btn-warning\">Back to Dashboard</a>';
        }
        else if($_SESSION['userRole'] == 2){
            $dashBoard = '<a href=\"'.$sitepath.'admin/dashboard.php\" class=\"btn btn-warning\">Back to Dashboard</a>';
        }
        else if($_SESSION['userRole'] == 3){
            $dashBoard = '<a href=\"'.$sitepath.'admin/dashboard.php\" class=\"btn btn-warning\">Back to Dashboard</a>';
        }
        else if($_SESSION['userRole'] == 5){
            $dashBoard = '<a href=\"'.$sitepath.'admin/approvals.php\" class=\"btn btn-warning\">Back to Dashboard</a>';
        }
        else if($_SESSION['userRole'] == 6){
            $dashBoard = '<a href=\"'.$sitepath.'parent/\" class=\"btn btn-warning\">Back to Dashboard</a>';
        }
    }

    $trackData = Api::trackUser();
?>
<!DOCTYPE html>
<html lang="en" debug="true">
    <head>
    
        <!-- <script type="text/javascript" src="https://getfirebug.com/firebug-lite.js"></script> -->
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />    
        
        <link rel="icon" href="<?php echo $favicon; ?>" type="image/x-icon" >
        <title>Arcanemind Messenger</title>
    
        <!-- Scripts --> 
    
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link href="<?php echo $sitepath; ?>marketplace/app/css/style.css" rel="stylesheet">
        <link href="<?php echo $sitepath; ?>messenger/css/style.css?v=11" rel="stylesheet">
        
    </head>
  
    <body>
        <?php
        /*<header class="header">
            <nav class="navbar navbar-default top-nav">
                <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <?php echo $logoBlock ?>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <?php echo $courseCategories; ?>
                            <li><a href="<?php echo $sitepath; ?>packages">Packages</a></li>
                            <li><a href="http://blog.arcanemind.com" target="_blank">Blog</a></li>
                        </ul>
                        <form class="navbar-form navbar-left" id="frmTopSearch" role="search" action="<?php echo $sitepath; ?>search/">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search Courses" id="searchBox" name="q">
                                    <span class="input-group-btn">
                                        <button class="btn btn-color1" type="submit"><i class="fa fa-search"></i></button>
                                    </span>
                                </div><!-- /input-group -->
                            </div>
                        </form>
                        <ul class="nav navbar-nav navbar-right">
                            <?php
                                if(isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
                                    if($_SESSION['userRole'] == 4) {
                                        ?>
                                        <li><a href="<?php echo $sitepath; ?>student/">Dashboard</a></li>
                                        <li><a href="<?php echo $sitepath; ?>signout">Logout</a></li>
                                        <?php
                                    }
                                    else if($_SESSION['userRole'] == 1){
                                        ?>
                                        <li><a href="<?php echo $sitepath; ?>admin/dashboard.php">Dashboard</a></li>
                                        <li><a href="<?php echo $sitepath; ?>signout">Logout</a></li>
                                        <?php
                                    }
                                    else if($_SESSION['userRole'] == 2){
                                        ?>
                                        <li><a href="<?php echo $sitepath; ?>admin/dashboard.php">Dashboard</a></li>
                                        <li><a href="<?php echo $sitepath; ?>signout">Logout</a></li>
                                        <?php
                                    }
                                    else if($_SESSION['userRole'] == 3){
                                        ?>
                                        <li><a href="<?php echo $sitepath; ?>admin/dashboard.php">Dashboard</a></li>
                                        <li><a href="<?php echo $sitepath; ?>signout">Logout</a></li>
                                        <?php
                                    }
                                    else if($_SESSION['userRole'] == 5){
                                        ?>
                                        <li><a href="<?php echo $sitepath; ?>admin/approvals.php">Dashboard</a></li>
                                        <li><a href="<?php echo $sitepath; ?>signout">Logout</a></li>
                                        <?php
                                    }
                                    else if($_SESSION['userRole'] == 6){
                                        ?>
                                        <li><a href="<?php echo $sitepath; ?>parent/">Dashboard</a></li>
                                        <li><a href="<?php echo $sitepath; ?>signout">Logout</a></li>
                                        <?php
                                    }
                                }
                            ?>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </header>*/
        ?>
        <div id="spika-container">

            <div id="initial-screen" style="position:absolute;width:100%;text-align:center;top:45%">
                Loading Arcanemind Messenger... <br />
                <img src="img/loader.gif" />
            </div>
        
        </div>
        
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->     
    
    <script  type="text/javascript">
        
        document.getElementById("initial-screen").style.height = document.documentElement.clientHeight + "px";
        
    </script>
    
    </body>
    
    <?php
        //<script type="text/javascript" src="js/bundle.js"></script>
        require_once("inc/bundle.js.php");
    ?>
    

</html>