CREATE DEFINER=`root`@`localhost` PROCEDURE `copySection`(IN `isectionId` INT, IN `iexamId` INT)
    MODIFIES SQL DATA
begin
declare newSectionId int;

INSERT INTO exam_sections (`name`, `time`, `examId`, `weight`, `delete`, `status`, `deleteTime`, `totalMarks`, `randomQuestions`)
SELECT `name`, `time`, iexamId, `weight`, 0, `status`, 0, `totalMarks`, `randomQuestions` from exam_sections where id=isectionId; 

SET newSectionId = LAST_INSERT_ID();
begin 
declare newCId int;
declare notfound int;
declare  category cursor FOR  
select id from section_categories where sectionId=isectionId and `delete`=0;
declare continue handler
  FOR NOT FOUND SET notfound = 1;
  OPEN category;

cq: LOOP
 
 FETCH category INTO newCId;
 IF notfound = 1 THEN 
 LEAVE cq;
 END IF;
call copyCatgory(newCId,newSectionId) ;
 
END LOOP cq;
 
CLOSE category; 
end;
end