DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `copySubmissionQuestions`(IN `iexamId` INT, IN `iquestionId` INT, IN `imarks` INT)
    MODIFIES SQL DATA
BEGIN
	INSERT INTO submission_question_links (`questionId`, `examId`, `marks`) VALUES (iquestionId, iexamId, imarks);
END$$
DELIMITER ;