CREATE DEFINER=`root`@`localhost` PROCEDURE `copySubjectiveQuestions`(IN `iexamId` INT, IN `iquestionId` INT, IN `imarks` INT)
    MODIFIES SQL DATA
BEGIN
	INSERT INTO question_subjective_exam_links (`questionId`, `examId`, `marks`) VALUES (iquestionId, iexamId, imarks);
END