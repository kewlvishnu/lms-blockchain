CREATE DEFINER=`root`@`localhost` PROCEDURE `copySubmission`(IN `iexamId` INT, IN `iname` VARCHAR(200))
    MODIFIES SQL DATA
BEGIN
	declare newExamId int;
	declare examStatus int;
	SET examStatus=(SELECT `status` FROM submissions WHERE id=iexamId);
	IF examStatus=2 THEN
		SET examStatus=1;
	END IF;
	INSERT INTO `submissions` (`name`, `ownerId`, `chapterId`, `subjectId`, `status`, `startDate`, `endDate`, `attempts`, `deleted`)
	SELECT iname, `ownerId`, `chapterId`, `subjectId`, examStatus, `startDate`, `endDate`, `attempts`, 0 FROM submissions
	WHERE id=iexamId;
	SET newExamId = LAST_INSERT_ID();

	BEGIN 
		declare newQId int;
		declare newMarks int;
		declare notfound int;
		declare  examQuestions cursor FOR  
		SELECT questionId,marks FROM submission_question_links WHERE examId=iexamId;
		declare continue handler
		FOR NOT FOUND SET notfound = 1;
		OPEN examQuestions;

			eq: LOOP

			FETCH examQuestions INTO newQId,newMarks;
			IF notfound = 1 THEN 
			LEAVE eq;
			END IF;
			call copySubmissionQuestions(newExamId,newQId,newMarks) ;

			END LOOP eq;

		CLOSE examQuestions; 
	END;

END