CREATE DEFINER=`root`@`localhost` PROCEDURE `copySubjectiveQuestions`(IN `iexamId` INT, IN `iquestionId` INT, IN `iparentId` INT)
    MODIFIES SQL DATA
BEGIN
	declare newQId int;
	declare newstatus tinyint;

	INSERT INTO questions_subjective (`questionId`, `exam_subjectiveId`, `questionfileUrl`, `answerFileUrl`, `deleted`, `status`, `parentId`, `questionsonPage`, `noOfSubquestionRequired`, `marks`) 
	SELECT `questionId`, iexamId, `questionfileUrl`, `answerFileUrl`, `deleted`, `status`, iparentId, `questionsonPage`, `noOfSubquestionRequired`, `marks` FROM questions_subjective WHERE id=iquestionId;
	SET newQId = LAST_INSERT_ID();

	BEGIN
		declare cqId int;
		declare notfound int;
		declare  childQuestion cursor FOR  
		select id from questions_subjective where parentId=iquestionId and `deleted`=0;
		declare continue handler
		FOR NOT FOUND SET notfound = 1;

		OPEN childQuestion;

			cq: LOOP

			FETCH childQuestion INTO cqId;
			IF notfound = 1 THEN 
				LEAVE cq;
			END IF;
			BEGIN
				declare childId int;

				INSERT INTO questions_subjective (`questionId`, `exam_subjectiveId`, `questionfileUrl`, `answerFileUrl`, `deleted`, `status`, `parentId`, `questionsonPage`, `noOfSubquestionRequired`, `marks`) 
				SELECT `questionId`, iexamId, `questionfileUrl`, `answerFileUrl`, `deleted`, `status`, newQId, `questionsonPage`, `noOfSubquestionRequired`, `marks` FROM questions_subjective WHERE id=cqId;
				SET childId = LAST_INSERT_ID();

			END;
			 
			END LOOP cq;
		 
		CLOSE childQuestion; 

	END;
END