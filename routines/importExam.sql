CREATE DEFINER=`root`@`localhost` PROCEDURE `importExam`(IN `iexamId` INT, IN `iownerId` INT, IN `isubjectId` INT, IN `ichapterId` INT)
    MODIFIES SQL DATA
begin
declare newExamId int;
declare examStatus int;
declare istartDate varchar(15);
declare iendDate varchar(15);
SET istartDate = (SELECT liveDate FROM courses WHERE id=(SELECT courseId FROM subjects WHERE id=isubjectId));
SET iendDate = (SELECT endDate FROM courses WHERE id=(SELECT courseId FROM subjects WHERE id=isubjectId));
set examStatus=(SELECT `status` from exams where id=iexamId);
IF examStatus=2 THEN
set examStatus=1;
END IF;
INSERT INTO `exams` (`name`, `type`, `ownerId`, `chapterId`, `subjectId`, `status`, `startDate`, `endDate`, `attempts`, `tt_status`, `totalTime`, `gapTime`, `sectionOrder`, `powerOption`, `endBeforeTime`, `association`, `delete`)
SELECT `name`, `type`, iownerId, ichapterId, isubjectId, examStatus, istartDate, iendDate, `attempts`, `tt_status`, `totalTime`, `gapTime`, `sectionOrder`, `powerOption`, `endBeforeTime`,0,0 from exams where id=iexamId; 
SET newExamId = LAST_INSERT_ID();
begin 
declare newSectionId int;
declare notfound int;
declare  sections cursor FOR  
select id from exam_sections where examId=iexamId and `delete`=0;
declare continue handler
  FOR NOT FOUND SET notfound = 1;
  OPEN sections;
cq: LOOP
 
 FETCH sections INTO newSectionId;
 IF notfound = 1 THEN 
 LEAVE cq;
 END IF;
call copySection(newSectionId,newExamId) ;
 
END LOOP cq;
 
CLOSE sections; 
end;
end