DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `importSubmission`(IN `iexamId` INT, IN `iownerId` INT, IN `isubjectId` INT, IN `ichapterId` INT)
    MODIFIES SQL DATA
BEGIN
	declare newExamId int;
	declare examStatus int;
	declare istartDate varchar(15);
	declare iendDate varchar(15);
	SET istartDate = (SELECT liveDate FROM courses WHERE id=(SELECT courseId FROM subjects WHERE id=isubjectId));
	SET iendDate = (SELECT endDate FROM courses WHERE id=(SELECT courseId FROM subjects WHERE id=isubjectId));
	SET examStatus=(SELECT `status` FROM submissions WHERE id=iexamId);
	IF examStatus=2 THEN
		SET examStatus=1;
	END IF;
	INSERT INTO `submissions` (`name`, `ownerId`, `chapterId`, `subjectId`, `status`, `startDate`, `endDate`, `attempts`, `deleted`)
	SELECT `name`, iownerId, ichapterId, isubjectId, examStatus, istartDate, iendDate, `attempts`, 0 FROM submissions
	WHERE id=iexamId;
	SET newExamId = LAST_INSERT_ID();

	BEGIN 
		declare newQId int;
		declare newMarks int;
		declare notfound int;
		declare  examQuestions cursor FOR  
		SELECT questionId,marks FROM submission_question_links WHERE examId=iexamId;
		declare continue handler
		FOR NOT FOUND SET notfound = 1;
		OPEN examQuestions;

			eq: LOOP

			FETCH examQuestions INTO newQId,newMarks;
			IF notfound = 1 THEN 
			LEAVE eq;
			END IF;
			call copySubmissionQuestions(newExamId,newQId,newMarks) ;

			END LOOP eq;

		CLOSE examQuestions; 
	END;

END$$
DELIMITER ;