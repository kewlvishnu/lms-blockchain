CREATE DEFINER=`root`@`localhost` PROCEDURE `importContent`(IN `icontentId` INT, IN `ichapterId` INT)
    MODIFIES SQL DATA
begin
declare newContentId int;
INSERT INTO `content` (`headingId`, `chapterId`, `title`, `published`, `downloadable`, `weight`, `demo`)
SELECT `headingId`, ichapterId, `title`, `published`, `downloadable`, `weight`, `demo` from content where id=icontentId; 
SET newContentId = LAST_INSERT_ID();
INSERT INTO `files` (`contentId`, `type`, `stuff`, `fileRealName`, `description`, `metadata`, `duration`)
SELECT newContentId, `type`, `stuff`, `fileRealName`, `description`, `metadata`, `duration` from files where contentId=icontentId; 
INSERT INTO `supplementary_files` (`contentId`, `fileRealName`)
SELECT newContentId, `fileRealName` from supplementary_files where contentId=icontentId; 
end