CREATE DEFINER=`root`@`localhost` PROCEDURE `copySubjectiveExam`(IN `iexamId` INT, IN `iname` VARCHAR(100))
    MODIFIES SQL DATA
BEGIN
	declare newExamId int;
	declare examStatus int;
	SET examStatus=(SELECT `status` FROM exam_subjective WHERE id=iexamId);
	IF examStatus=2 THEN
		SET examStatus=1;
	END IF;
	INSERT INTO `exam_subjective` (`name`, `type`, `ownerId`, `chapterId`, `subjectId`, `status`, `startDate`, `endDate`, `totalTime`, `attempts`, `shuffle`, `totalQuestions`, `totalRequiredQuestions`, `submissiontype`, `Active`)
	SELECT iname, `type`, `ownerId`, `chapterId`, `subjectId`, examStatus, `startDate`, `endDate`, `totalTime`, `attempts`, `shuffle`, `totalQuestions`, `totalRequiredQuestions`, `submissiontype`, 1 FROM exam_subjective
	WHERE id=iexamId;
	SET newExamId = LAST_INSERT_ID();

	BEGIN 
		declare newQId int;
		declare newMarks int;
		declare notfound int;
		declare  examQuestions cursor FOR  
		SELECT questionId,marks FROM question_subjective_exam_links WHERE examId=iexamId;
		declare continue handler
		FOR NOT FOUND SET notfound = 1;
		OPEN examQuestions;

			eq: LOOP

			FETCH examQuestions INTO newQId,newMarks;
			IF notfound = 1 THEN 
			LEAVE eq;
			END IF;
			call copySubjectiveQuestions(newExamId,newQId,newMarks) ;

			END LOOP eq;

		CLOSE examQuestions; 
	END;

END