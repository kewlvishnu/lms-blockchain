CREATE DEFINER=`root`@`localhost` PROCEDURE `copyCatgory`(IN `icategoryId` INT, IN `isectionId` INT)
    MODIFIES SQL DATA
begin
declare newCId int;
INSERT INTO section_categories (sectionId, correct, wrong, required, entered,questionType, `delete`, `status`, weight)
SELECT isectionId, correct, wrong, required, entered,questionType, 0, `status`, weight from section_categories where id=icategoryId;
SET newCId = LAST_INSERT_ID();
begin 
declare newQId int;
declare newQType int;
declare notfound int;
declare  categoryQuestion cursor FOR  
select id,questionType from questions where categoryId=icategoryId and `delete`=0 and parentId=0;
declare continue handler
  FOR NOT FOUND SET notfound = 1;
  OPEN categoryQuestion;
 
 cq: LOOP
 
 FETCH categoryQuestion INTO newQId,newQType;
 IF notfound = 1 THEN 
 LEAVE cq;
 END IF;
call copyQuestions(newCId,newQId,newQType,0) ;
 
END LOOP cq;
 
CLOSE categoryQuestion; 
end;
end