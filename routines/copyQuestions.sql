CREATE DEFINER=`root`@`localhost` PROCEDURE `copyquestion_category_links`(IN `icategoryId` INT, IN `iquestionId` INT)
    MODIFIES SQL DATA
begin
declare newQId int;
declare newstatus tinyint;
INSERT INTO question_category_links (categoryId, questionId)
SELECT icategoryId, iquestionId FROM question_category_links WHERE id=iquestionId;
SET newQId = LAST_INSERT_ID();
end