CREATE DEFINER=`root`@`localhost` PROCEDURE `copyQuestions`(IN `icategoryId` INT, IN `iquestionId` INT, IN `iquestionType` INT, IN `iparentId` INT)
    MODIFIES SQL DATA
begin
declare newQId int;
declare newstatus tinyint;
INSERT INTO questions (categoryId, questionType, question, description, status, parentId) 
SELECT icategoryId, iquestionType, question, description,status,iparentId FROM questions WHERE id=iquestionId;
SET newQId = LAST_INSERT_ID();
IF(iquestionType=0 OR iquestionType=6 )THEN
INSERT INTO options_mcq (questionId, parentId, `option`, correct)
SELECT newQId, iparentId, `option`,correct FROM options_mcq WHERE questionId=iquestionId and `delete`=0;
ELSEIF (iquestionType=1)THEN
INSERT INTO true_false (questionId, parentId, answer)
SELECT newQId, iparentId, answer FROM true_false WHERE questionId=iquestionId and `delete`=0;
ELSEIF (iquestionType=2)THEN 
INSERT INTO ftb (questionId, parentId, blanks) SELECT newQId, iparentId, blanks FROM ftb WHERE questionId=iquestionId and `delete`=0;
ELSEIF (iquestionType=3)THEN 
INSERT INTO options_mtf (questionId, columnA, columnB) SELECT newQId, columnA, columnB FROM options_mtf WHERE questionId=iquestionId and `delete`=0;
ELSEIF (iquestionType=4 OR iquestionType=5 )THEN
begin
declare cqId int;
declare cqtype int;
declare notfound int;
declare  childQuestion cursor FOR  
select id,questionType from questions where parentId=iquestionId and `delete`=0;
declare continue handler
  FOR NOT FOUND SET notfound = 1;
 
 OPEN childQuestion;
 
 cq: LOOP
 
 FETCH childQuestion INTO cqId,cqtype;
 IF notfound = 1 THEN 
 LEAVE cq;
 END IF;
BEGIN
declare childId int;
INSERT INTO questions (categoryId, questionType, question, description, status, parentId) 
SELECT icategoryId, cqtype, question, description,status,newQId FROM questions WHERE id=cqId  and `delete`=0;
SET childId = LAST_INSERT_ID();
IF(cqtype=0 OR cqtype=6 )THEN
INSERT INTO options_mcq (questionId, parentId, `option`, correct)
SELECT childId, newQId, `option`,correct FROM options_mcq WHERE questionId=cqId and `delete`=0;
ELSEIF (cqtype=1)THEN
INSERT INTO true_false (questionId, parentId, answer)
 SELECT childId, newQId, answer FROM true_false WHERE questionId=cqId and `delete`=0;
ELSEIF (cqtype=2)THEN 
INSERT INTO ftb (questionId, parentId, blanks)
 SELECT childId, newQId, blanks FROM ftb WHERE questionId=cqId and `delete`=0;
ELSEIF (cqtype=3)THEN 
INSERT INTO options_mtf (questionId, columnA, columnB) 
SELECT childId, columnA, columnB FROM options_mtf WHERE questionId=cqId and `delete`=0;
END IF;
END;
 
END LOOP cq;
 
CLOSE childQuestion; 

end;
END IF;
end