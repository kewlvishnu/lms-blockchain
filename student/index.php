<?php
	//get the last-modified-date of this very file
	$lastModified=filemtime(__FILE__);
	//get a unique hash of this file (etag)
	$etagFile = md5_file(__FILE__);
	//get the HTTP_IF_MODIFIED_SINCE header if set
	$ifModifiedSince=(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
	//get the HTTP_IF_NONE_MATCH header if set (etag: unique file hash)
	$etagHeader=(isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

	//set last-modified header
	header("Last-Modified: ".gmdate("D, d M Y H:i:s", $lastModified)." GMT");
	//set etag-header
	header("Etag: $etagFile");
	//make sure caching is turned on
	header('Cache-Control: max-age=290304000, public');

	//check if page has changed. If not, send 304 and exit
	if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])==$lastModified || $etagHeader == $etagFile)
	{
		header("HTTP/1.1 304 Not Modified");
		exit;
	}

	//  url functions and routes
	require_once('../config/url.functions.php');
	$pageArr = explode('/', $pageUrl);

	//$page = "student";
	/*
	**  flagInvalid : true = valid urls, false = invalid urls
	*/
	$flagInvalid = false;
	/*
	**  flagSession :
	**  0 = neutral URLs which opens regardless of user login status
	**  1 = these urls open only for non logged in users
	**  2 = these urls open only for logged in users
	*/
	$flagSession = 2;
	$pages = array('','index.php','courses','subjects','profile','settings','notifications','content','exams','subjective-exams','manual-exams','submissions');

	if ($pageArr[0] == 'student') {
		$pageType = "panel";
		$page	  = "home";
		if (isset($pageArr[1]) && !empty($pageArr[1])) {
			if (in_array($pageArr[1], $pages)) {
				if ($pageArr[1] == "courses") {
					$page = "courses";
					if (isset($pageArr[2]) && !empty($pageArr[2])) {
						$page = "courseDetail";
						$courseId = $pageArr[2];
					}
				} else if ($pageArr[1] == "subjects") {
					if (isset($pageArr[2]) && !empty($pageArr[2])) {
						$page = "subjectDetail";
						$subjectId = $pageArr[2];
					} else {
						$flagInvalid = true;
					}
				} else if ($pageArr[1] == "profile") {
					$page = "profile";
					$subPage = "profileMe";
					if (isset($pageArr[2]) && !empty($pageArr[2])) {
						if ($pageArr[2] == "edit") {
							$subPage = "profileEdit";
						}
					}
				} else if ($pageArr[1] == "settings") {
					$page = "settings";
				} else if ($pageArr[1] == "notifications") {
					if ($pageArr[2] == "activity") {
						$page = "notificationsActivity";
					} else if ($pageArr[2] == "chat") {
						$page = "notificationsChat";
					} else {
						$flagInvalid = true;
					}
				} else if ($pageArr[1] == "content") {
					$pageType = "content";
					if ((isset($pageArr[2]) && !empty($pageArr[2])) && (isset($pageArr[3]) && !empty($pageArr[3]))) {
						if ($pageArr[2] == "subjects") {
							$page = "content";
							$subjectId = $pageArr[3];
							if (isset($pageArr[4]) && !empty($pageArr[4])) {
								if ( ($pageArr[4] == 'chapters') && (isset($pageArr[5]) && !empty($pageArr[5])) ) {
									$chapterId = $pageArr[5];
									if (isset($pageArr[6]) && !empty($pageArr[6])) {
										if ( ($pageArr[6] == 'content') && (isset($pageArr[7]) && !empty($pageArr[7])) ) {
											$contentId = $pageArr[7];
										} else {
											$contentId = 0;
										}
									} else {
										$contentId = 0;
									}
								} else {
									$chapterId = 0;
								}
							} else {
								$chapterId = 0;
							}
						} else {
							$flagInvalid = true;
						}
					} else {
						$flagInvalid = true;
					}
				} else if ($pageArr[1] == "exams") {
					if (isset($pageArr[2]) && !empty($pageArr[2])) {
						$page = "exams";
						$examId = $pageArr[2];
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							if ($pageArr[3] == 'result') {
								$page = "examResult";
								$attemptId = 0;
								if (isset($pageArr[4]) && !empty($pageArr[4])) {
									if ( ($pageArr[4] == 'attempt') && (isset($pageArr[5]) && !empty($pageArr[5])) ) {
										$attemptId = $pageArr[5];
									} else {
										$flagInvalid = true;
									}
								}
							} else {
								$flagInvalid = true;
							}
						} else {
							$flagInvalid = true;
						}
					} else {
						$flagInvalid = true;
					}
				} else if ($pageArr[1] == "subjective-exams") {
					if (isset($pageArr[2]) && !empty($pageArr[2])) {
						$page = "subjectiveExams";
						$examId = $pageArr[2];
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							if ($pageArr[3] == 'result') {
								$page = "subjectiveExamResult";
								$attemptId = 0;
								if (isset($pageArr[4]) && !empty($pageArr[4])) {
									if ( ($pageArr[4] == 'attempt') && (isset($pageArr[5]) && !empty($pageArr[5])) ) {
										$attemptId = $pageArr[5];
									} else {
										$flagInvalid = true;
									}
								}
							} else {
								$flagInvalid = true;
							}
						} else {
							$flagInvalid = true;
						}
					} else {
						$flagInvalid = true;
					}
				} else if ($pageArr[1] == "manual-exams") {
					if (isset($pageArr[2]) && !empty($pageArr[2])) {
						$page = "manualExams";
						$examId = $pageArr[2];
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							if ($pageArr[3] == 'result') {
								$page = "manualExamResult";
								$attemptId = 0;
								if (isset($pageArr[4]) && !empty($pageArr[4])) {
									if ( ($pageArr[4] == 'attempt') && (isset($pageArr[5]) && !empty($pageArr[5])) ) {
										$attemptId = $pageArr[5];
									} else {
										$flagInvalid = true;
									}
								}
							} else {
								$flagInvalid = true;
							}
						} else {
							$flagInvalid = true;
						}
					} else {
						$flagInvalid = true;
					}
				} else if ($pageArr[1] == "submissions") {
					if (isset($pageArr[2]) && !empty($pageArr[2])) {
						$page = "submission";
						$examId = $pageArr[2];
						if (isset($pageArr[3]) && !empty($pageArr[3])) {
							if ($pageArr[3] == 'result') {
								$page = "submissionResult";
								$attemptId = 0;
								if (isset($pageArr[4]) && !empty($pageArr[4])) {
									if ( ($pageArr[4] == 'attempt') && (isset($pageArr[5]) && !empty($pageArr[5])) ) {
										$attemptId = $pageArr[5];
									} else {
										$flagInvalid = true;
									}
								}
							} else {
								$flagInvalid = true;
							}
						}
					} else {
						$flagInvalid = true;
					}
				} else {
					$flagInvalid = true;
				}
			} else {
				$flagInvalid = true;
			}
		}
	} else {
		$flagInvalid = true;
	}
	if ($flagInvalid) {
		//header('location:'.$sitepath404);
	} else {
		@session_start();
		if (!$flagInvalid && ($flagSession == 2) && (!isset($_SESSION['userId']) || empty($_SESSION['userId']) || ($_SESSION['userRole'] != 4))) {

			header('location:'.$sitepath);

		} else {
			/**
			* header checks for metas, page specific validation and portfolio subdomain validations
			**/
			require_once('inc/header.functions.php');
			
			require_once('html-template/index.html.php');

			require_once('../config/tracking.functions.php');
		}
	}
?>