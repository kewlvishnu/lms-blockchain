<?php
    require_once('sidebar.php');
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-white"></i>
                    <span class="caption-subject bold font-white uppercase" id="contentTitle"></span>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body no-space">
                <div class="bg-white" id="pageContent"></div>
            </div>
        </div>
        <div class="row margin-bottom-20 attempt-block hide">
            <div class="col-md-6">
                <a href="javascript:void(0)" class="btn green btn-block h-nav-link previous-attempt" disabled><i class="fa fa-arrow-left"></i> Previous</a>
            </div>
            <div class="col-md-6">
                <a href="javascript:void(0)" class="btn green btn-block h-nav-link next-attempt" disabled><i class="fa fa-arrow-right"></i> Next</a>
            </div>
        </div>
        <div id="description"></div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->
<a href="javascript:;" class="page-quick-sidebar-toggler">
    <i class="icon-login"></i>
</a>
<div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">
    <div class="page-quick-sidebar">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="javascript:;" data-target="#quick_sidebar_tab_1" data-toggle="tab"> NOTES
                    <i class="fa fa-edit"></i>
                </a>
            </li>
            <li>
                <a href="javascript:;" data-target="#quick_sidebar_tab_2" data-toggle="tab"> DOWNLOADS
                    <i class="fa fa-download"></i>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active page-quick-sidebar-alerts" id="quick_sidebar_tab_1">
                <div class="page-quick-sidebar-alerts-list">
                    <form action="" class="form">
                        <div class="form-body">
                            <h4>Type your notes :</h4>
                            <div class="form-group">
                                <div name="notes" id="notes"> </div>
                                <div id="timeNotes"></div>
                            </div>
                            <div class="form-group">
                                <button type="button" id="btnSubmitNotes" class="btn green">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-pane page-quick-sidebar-chat" id="quick_sidebar_tab_2">
                <div class="page-quick-sidebar-chat-users" data-rail-color="#ddd" data-wrapper-class="page-quick-sidebar-list">
                    <div id="contentDownload"></div>
                    <h3 class="list-heading">SUPPLEMENTARY FILES</h3>
                    <div id="downloads">
                        <!-- <ul class="media-list list-items">
                            <li class="media">
                                <div class="media-status">
                                    <span class="badge badge-success">8</span>
                                </div>
                                <img class="media-object" src="assets/layouts/layout/img/avatar3.jpg" alt="...">
                                <div class="media-body">
                                    <h4 class="media-heading">Bob Nilson</h4>
                                    <div class="media-heading-sub"> Project Manager </div>
                                </div>
                            </li>
                            <li class="media">
                                <img class="media-object" src="assets/layouts/layout/img/avatar1.jpg" alt="...">
                                <div class="media-body">
                                    <h4 class="media-heading">Nick Larson</h4>
                                    <div class="media-heading-sub"> Art Director </div>
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-status">
                                    <span class="badge badge-danger">3</span>
                                </div>
                                <img class="media-object" src="assets/layouts/layout/img/avatar4.jpg" alt="...">
                                <div class="media-body">
                                    <h4 class="media-heading">Deon Hubert</h4>
                                    <div class="media-heading-sub"> CTO </div>
                                </div>
                            </li>
                            <li class="media">
                                <img class="media-object" src="assets/layouts/layout/img/avatar2.jpg" alt="...">
                                <div class="media-body">
                                    <h4 class="media-heading">Ella Wong</h4>
                                    <div class="media-heading-sub"> CEO </div>
                                </div>
                            </li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="activityModal" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title" id="activityModalLabel">No activity</h4>
            </div>
            <div class="modal-body">
                <p>Are you here?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Yes</button>
            </div>
        </div>
    </div>
</div>
<!-- END QUICK SIDEBAR -->