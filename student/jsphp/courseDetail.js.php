<script type="text/javascript">
    function fetchCourseDetails() {
        var req = {};
        var res;
        req.action = 'courseDetails-for-student-new';
        req.courseId = courseId;
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 0)
                toastr.error(res.message);
            else {
                fillCourseSubjectDetails(res);
            }
        });
    }
    function fillCourseSubjectDetails(data) {
        //console.log(data);
        /*var courseDoc= data.coursesDoc;
        if(courseDoc !='' && courseDoc != undefined)
        {   var urlAbsolute='assets/ViewerJS/#'+courseDoc;
            $('#viewCourseDoc #viewerCourseDocument').attr("src", urlAbsolute);
            $('#viewcoursedoc1').show();
        }
        
        if(data.certificationStatus ==1 && data.certification!=null)
        {
            certificate=data.certification;
            $('#certification-download').attr("href", data.certification);
            $('#certification-download').show();
            $('#coursecertification').html('Congratulations, You have Sucessfully completed the course');
        }*/
        $('.course-name').text(data.course[0].course);
        $('.institute-name').text(data.course[0].Institute);
        var html = "";
        $.each(data.subjects, function (i, subject) {
            html+='<div class="col-lg-4 col-md-6">'+
                        '<div class="mt-widget-4 margin-bottom-20">'+
                            '<div class="mt-img-container">'+
                                '<img src="' + subject.image + '" /> </div>'+
                            '<div class="mt-container bg-'+darkThemeColors[getRandomInt(0, darkThemeColors.length-1)]+'-opacity">'+
                                '<div class="mt-head-title"> ' + subject.name + ' </div>'+
                                '<div class="mt-body-icons">'+
                                    '<a href="'+sitepathStudentSubjects+subject.id+'" title="Lectures">'+
                                        '<i class="icon-notebook"></i><span class="count">' + subject.chapters + '</span>'+
                                    '</a>'+
                                    '<a href="'+sitepathStudentSubjects+subject.id+'" title="Exams">'+
                                        '<i class="icon-note"></i><span class="count">' + subject.exams + '</span>'+
                                    '</a>'+
                                '</div>'+
                                '<a href="'+sitepathStudentSubjects+subject.id+'">'+
                                    '<div class="mt-footer-button">'+
                                        '<button type="button" class="btn btn-circle btn-danger btn-sm">Start Learning</button>'+
                                    '</div>'+
                                '</a>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';
            //html += '<div class="text-center"> <a href="chapter.php?courseId='+courseId+'&subjectId='+subject.id+'"><img  alt="" src="' + subject.image + '" style="width: 150px;margin: auto;"><br/><strong> <i style="font-size:16px;color:orange;" class=" fa fa-angle-right" ></i><br/>' + subject.name + '</strong></a></div>';
        });
        $('#listSubjects').append(html);
    }
    function getStudentExams() {
        var req = {};
        var res;
        req.action = 'student-exam-attempts-new';
        req.courseId = courseId;
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 0){
                $('#listExams').hide();
                $('#listAssignments').hide();
            }
            else {
                fillStudentExams(res);
            }
        });
    }

    //for subjective Exams
    function getStudentSubjectiveExams() {
        var req = {};
        var res;
        req.action = 'student-exam-subjective-attempts';
        req.courseId = courseId;
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            //console.log(res);
            if (res.status == 0){
                $('#listSubjectiveExams').hide();
            }
            else {
                fillStudentSubjectiveExams(res);
                subjectiveExams = res.exams;
            }
        });
    }

    //for subjective Exams
    function getStudentManualExams() {
        var req = {};
        var res;
        req.action = 'student-exam-manual-exams';
        req.courseId = courseId;
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            //console.log(res);
            if (res.status == 0){
                $('#listManualExams').hide();
            }
            else {
                fillStudentManualExams(res);
            }
        });
    }

    function fillStudentExams(data) {
        var examRow = '', AssignmentRow = '';
        var disabled = '', startnow = '';
        var startButton = 'disabled=disabled';
        var examMore=false;
        var assignmentMore=false;
        var examCount=0;
        var AssignmentCount=0;
        
        $.each(data.exams, function (i, exam) {
            var startdate = new Date(parseInt(exam.startDate));
            var today= $.now();
            //console.log(exam);
            // var today = new Date();
            if(exam.showResult=='immediately' || exam.endDate=='')
            {
                check=1;
            }
            else{
                if(today>exam.endDate)
                {
                    check=1;
                }
                else{
                    check=2;
                    showFrom=2;
                    var tillDate = new Date(parseInt(exam.endDate));
                    if(tillDate!='' || tillDate!=null)
                    {
                        var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
                        if(tillDate.getHours() < 10)
                            tsd += '0' + tillDate.getHours();
                        else
                            tsd += tillDate.getHours();
                        tsd += ':';
                        if(tillDate.getMinutes() < 10)
                            tsd += '0' + tillDate.getMinutes();
                        else
                            tsd += tillDate.getMinutes();
                    }
                    showdateFrom=tsd;
                }
            }
            var display_startdate = startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + ' ' + ((startdate.getHours()<10)?('0'+startdate.getHours()):(startdate.getHours())) + ':' + ((startdate.getMinutes()<10)?('0'+startdate.getMinutes()):(startdate.getMinutes()));
            if (exam.chapter == null) {
                diaplay_chapter = '';
            } else {
                diaplay_chapter = ((exam.chapter == "Independent")?(' independently'):(' under the section ' + exam.chapter));
            }
            if((exam.showResultTill == 2 && today< exam.showResultTillDate) || exam.showResultTill == 1)
            {
                check=1;
            }
            else{
                check=2;
                showTill=2;
                showdatetill=exam.showResultTillDate;
                var tillDate = new Date(parseInt(showdatetill));
                if(tillDate!='' || tillDate!=null)
                {
                    var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
                    if(tillDate.getHours() < 10)
                        tsd += '0' + tillDate.getHours();
                    else
                        tsd += tillDate.getHours();
                    tsd += ':';
                    if(tillDate.getMinutes() < 10)
                        tsd += '0' + tillDate.getMinutes();
                    else
                        tsd += tillDate.getMinutes();
                }
                showdatetill=tsd;
            }

            var show='';

            var diaplay_chapter;
            var enddate, display_enddate;
            var actionButton;

            if (exam.endDate == null || exam.endDate == '') {
                display_enddate = '';
            } else {
                enddate = new Date(parseInt(exam.endDate));
                display_enddate = "This exam ends on "+enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + ' ' + ((enddate.getHours()<10)?('0'+enddate.getHours()):(enddate.getHours())) + ':' + ((enddate.getMinutes()<10)?('0'+enddate.getMinutes()):(enddate.getMinutes())) + '. ';
            }
            if (exam.chapter == null) {
                diaplay_chapter = '';
            } else {
                diaplay_chapter = ((exam.chapter == "Independent")?(' independently'):(' under the section ' + exam.chapter));
            }
            var examgiven = ((exam.examgiven>0)?('You have attempted the exam '+exam.examgiven+' times. '):(''));
            var attempsLeft = exam.attempts - exam.examgiven;
            var attemptsLeftText = '';
            disabledStartTest = '';
            if(attempsLeft<=0)
            {
                attempsLeft         =   0;
                disabledStartTest   = 'disabled="disabled"';
            } else {
                attemptsLeftText    = 'You have ' + attempsLeft + ' attempts left. ';
            }
            if (exam.examgiven == 0) {
                disabled = 'disabled="disabled"';
            } else {
                disabled = '';
            }
            if(exam.paused == 0)
                buttonText = 'Start Test';
            else
                buttonText = 'Resume';

            if(check == 1)
            {
                if (disabled=='') {
                    show = '<a ' + disabled + ' href="'+sitePathStudent+'exams/' + exam.id + '/result/">Click here to see results</a>';
                }
            }
            else{
                if(showTill==2 && showFrom==2)
                {   
                    show= 'Result will be shown only from'+showdateFrom+ 'to' +showdatetill;
                }
                else if(showTill==2 && showFrom==1)
                {
                    show= 'Result was shown till '+showdatetill;
                }
                else
                {
                    show= 'Result will be shown after '+showdateFrom;
                }
            }
            actionButton = '<div class="timeline-body-head-actions">'+
                                '<a href="'+sitepathStudentOld+'examModule.php?examId=' + exam.id + '" target="_blank" class="btn btn-circle red btn-sm start-exam" data-startdate="' + exam.startDate + '" data-endDate="' + exam.endDate + '" data-attempts=' + attempsLeft + '>' + buttonText + '</a>'+
                            '</div>';
                        '</tr>';
            //var iconType = ((exam.type=='Exam')?('fa fa-book'):('fa fa-edit'));
            var iconType = ((exam.type=='Exam')?('E'):('A'));
            tr =    '<!-- TIMELINE ITEM -->'+
                        '<div class="timeline-item">'+
                            '<div class="timeline-badge">'+
                                '<div class="timeline-icon" title="'+exam.type+'">'+
                                    '<span class="font-green-haze">'+iconType+'</span>'+
                                '</div>'+
                            '</div>'+
                            '<div class="timeline-body">'+
                                '<div class="timeline-body-arrow"> </div>'+
                                '<div class="timeline-body-head">'+
                                    '<div class="timeline-body-head-caption">'+
                                        '<a href="'+sitepathStudentOld+'examModule.php?examId=' + exam.id + '" target="_blank" class="timeline-body-title font-blue-madison">' + exam.exam + '</a>'+
                                        '<span class="timeline-body-time font-grey-cascade">Started at ' + display_startdate + '</span>'+
                                    '</div>'+actionButton+
                                '</div>'+
                                '<div class="timeline-body-content">'+
                                    '<span class="font-grey-cascade"> ' + 'This exam was created in the subject <a href="'+sitepathStudentSubjects+exam.subjectId+'">' + exam.subject + '</a>'+ diaplay_chapter +'. ' + display_enddate + examgiven + attemptsLeftText +  show + ' </span>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '<!-- END TIMELINE ITEM -->';
            if (exam.type == 'Exam') {
                examRow += tr;
                examCount++;
            } else {
                AssignmentRow += tr;
                AssignmentCount++;
            }
        });
        if(AssignmentRow==''){
            $('#listAssignments').hide();
        }
        if(examRow==''){
            $('#listExams').hide();
        }
        //console.log(examRow);
        $('#listExams .portlet-body .timeline').html(examRow);

        $('#listAssignments .portlet-body .timeline').html(AssignmentRow);
        
    }

    function examCheckTimer (currentDate) {
        //console.log(currentDate);
        setInterval(function(){
            var todayTime = new Date(currentDate * 1000);
            $('.start-exam').each(function () {
                if ($(this).attr('data-attempts') > 0)
                {   
                    var startdate = new Date(parseInt($(this).attr('data-startdate')));
                    if (startdate.valueOf() <= todayTime.valueOf() )
                    {   
                        $(this).attr('disabled', false);
                    }
                    var endDate = $(this).attr('data-endDate');
                    if (endDate != '') {
                        var examendDate = new Date(parseInt(endDate));
                        if (examendDate.valueOf() <= todayTime.valueOf())
                        {
                            $(this).attr('disabled', 'disabled');
                        }
                    }
                }
            });
            $('.start-subjective-exam').each(function () {
                if ($(this).attr('data-attempts') > 0)
                {   
                    var startdate = new Date(parseInt($(this).attr('data-startdate')));
                    if (startdate.valueOf() <= todayTime.valueOf() )
                    {   
                        $(this).attr('disabled', false);
                    }
                    var endDate = $(this).attr('data-endDate');
                    if (endDate != '') {
                        var examendDate = new Date(parseInt(endDate));
                        if (examendDate.valueOf() <= todayTime.valueOf())
                        {
                            $(this).attr('disabled', 'disabled');
                        }
                    }
                }
            });
            currentDate++;
        }, 1000);
    }

    function fillStudentSubjectiveExams(data) {
        //console.log(data);
        var examRow = '';
        var disabled = '', startnow = '';
        var startButton = 'disabled=disabled';
        var examMore=false;
        var assignmentMore=false;
        var examCount=0;
        var AssignmentCount=0;
        $.each(data.exams, function (i, exam) {
            var startdate = new Date(parseInt(exam.startDate));
            var today= $.now();
            //toastr.error(startdate);
            // var today = new Date();
            showFrom=2;
            var endDate  = parseInt(exam.endDate);
            var tillDate = new Date(endDate);
            if (today>endDate) {
                var tr = '';
            } else {
                //console.log(today);
                //console.log(parseInt(exam.endDate));
                if(tillDate!='' || tillDate!=null)
                {
                    var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
                    if(tillDate.getHours() < 10)
                        tsd += '0' + tillDate.getHours();
                    else
                        tsd += tillDate.getHours();
                    tsd += ':';
                    if(tillDate.getMinutes() < 10)
                        tsd += '0' + tillDate.getMinutes();
                    else
                        tsd += tillDate.getMinutes();
                }
                showdateFrom=tsd;                                                                       
                
                var show='';        
                
                var diaplay_chapter;
                var display_startdate, enddate, display_enddate,actionButton;

                if (exam.endDate == null || exam.endDate == '') {
                    display_enddate = '';
                } else {
                    enddate = new Date(parseInt(exam.endDate));
                    display_enddate = "This exam ends on "+enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + ' ' + ((enddate.getHours()<10)?('0'+enddate.getHours()):(enddate.getHours())) + ':' + ((enddate.getMinutes()<10)?('0'+enddate.getMinutes()):(enddate.getMinutes()))+'. ';
                }
                display_startdate = startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + ' ' + ((startdate.getHours()<10)?('0'+startdate.getHours()):(startdate.getHours())) + ':' + ((startdate.getMinutes()<10)?('0'+startdate.getMinutes()):(startdate.getMinutes()));
                if (exam.chapter == null) {
                    diaplay_chapter = '';
                } else {
                    diaplay_chapter = ' under ' + exam.chapter;
                }
                var examgiven = ((exam.examgiven>0)?('You have attempted the exam '+exam.examgiven+' times. '):(''));
                var attempsLeft = exam.attempts - exam.examgiven;
                disabledStartTest = '';
                if(attempsLeft<=0)
                {
                    attempsLeft         =   0;
                    disabledStartTest   = 'disabled="disabled"';
                } else {
                    attemptsLeftText    = 'You have ' + attempsLeft + ' attempts left. ';
                }
                diaplay_chapter = ((exam.chapter == "Independent")?(' independently'):(' under the section ' + exam.chapter));
                //disabledStartTest   = 'disabled="disabled"';
                if (exam.listAttempts > 0) {
                    show = ' <a href="javascript:void(0)" class="js-check-attempts" data-examid="' + exam.id + '">Click here to see results</a>';
                }
                if(exam.paused == 0)
                    buttonText = 'Start Test';
                else
                    buttonText = 'Resume';
                actionButton = '<div class="timeline-body-head-actions">'+
                                    '<a href="'+sitepathStudentOld+'subjectiveExamModule.php?courseId=' + courseId + '&examId=' + exam.id + '" target="_blank" class="btn btn-circle red btn-sm start-subjective-exam" data-startdate="' + exam.startDate + '" data-endDate="' + exam.endDate + '" data-attempts=' + attempsLeft + ' ' + disabledStartTest+'>' + buttonText + '</a>'+
                                '</div>';
            tr =    '<!-- TIMELINE ITEM -->'+
                        '<div class="timeline-item">'+
                            '<div class="timeline-badge">'+
                                '<div class="timeline-icon" title="Subjective Exam">'+
                                    '<span class="font-green-haze">S</span>'+
                                '</div>'+
                            '</div>'+
                            '<div class="timeline-body">'+
                                '<div class="timeline-body-arrow"> </div>'+
                                '<div class="timeline-body-head">'+
                                    '<div class="timeline-body-head-caption">'+
                                        '<a href="'+sitepathStudentOld+'subjectiveExamModule.php?courseId=' + courseId + '&examId=' + exam.id + '" target="_blank" class="timeline-body-title font-blue-madison" '+disabledStartTest+'>' + exam.exam + '</a>'+
                                        '<span class="timeline-body-time font-grey-cascade">Started at ' + display_startdate + '</span>'+
                                    '</div>'+actionButton+
                                '</div>'+
                                '<div class="timeline-body-content">'+
                                    '<span class="font-grey-cascade"> ' + 'This exam was created in the subject <a href="'+sitepathStudentSubjects+exam.subjectId+'">' + exam.subject + '</a>'+ diaplay_chapter +'. ' + examgiven + attemptsLeftText +  show + ' </span>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '<!-- END TIMELINE ITEM -->';
            }
            if (exam.type == 'Exam') {
                examRow += tr;
                examCount++;
            }
        });
        //console.log(examRow);
        if(examRow==''){
            $('#listSubjectiveExams').hide();
        }

        
        $('#listSubjectiveExams .portlet-body .timeline').html(examRow);

        examCheckTimer(data.currentDate);
        
    }
    
    function fillStudentManualExams(data) {
        //console.log(data);
        var examHtml = '';
        var examCount = 0;
        $.each(data.exams, function (i, exam) {
            //console.log(exam);
            examHtml+= '<!-- TIMELINE ITEM -->'+
                            '<div class="timeline-item">'+
                                '<div class="timeline-badge">'+
                                    '<div class="timeline-icon" title="Manual Exam">'+
                                        '<span class="font-green-haze">M</span>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="timeline-body">'+
                                    '<div class="timeline-body-arrow"> </div>'+
                                    '<div class="timeline-body-head">'+
                                        '<div class="timeline-body-head-caption">'+
                                            '<a href="'+sitePathStudent+'manual-exams/'+exam.id+'/result/" class="timeline-body-title font-blue-madison">'+exam.name+'</a>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="timeline-body-content">'+
                                        '<span class="font-grey-cascade"> ' + 'This exam was created in the subject <a href="'+sitepathStudentSubjects+exam.subjectId+'">' + exam.subjectName + '</a>'+ '. It provides '+exam.type.toUpperCase()+' ANALYTICS for this exam.' + ' <a href="'+sitePathStudent+'manual-exams/'+exam.id+'/result/">Click here to see results</a>.</span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '<!-- END TIMELINE ITEM -->';
            examCount++;
        });
        if(examCount==0){
            $('#listManualExams').hide();
        } else {
            $('#listManualExams .portlet-body .timeline').append(examHtml);
        }
    }

    $('#listSubjectiveExams').on("click", ".js-check-attempts", function(){
        var examId = $(this).attr("data-examid");
        $.each(subjectiveExams, function (i, exam) {
            if (examId==exam.id) {
                $('#subjectiveCheckedModal .modal-title').html(exam.exam);
                if (exam.listAttempts.length>0) {
                    $('#tblSubjectiveChecked').html('<tr><th>Attempts</th><th>Start Date</th><th>End Date</th><th>Checked</th></tr>');
                    $.each(exam.listAttempts, function (j, attempt) {
                        //console.log(attempt);
                        $('#tblSubjectiveChecked').append(
                            '<tr>'+
                                '<td>Attempt '+(j+1)+'</td>'+
                                '<td>'+formatTime(attempt.startDate)+'</td>'+
                                '<td>'+formatTime(attempt.endDate)+'</td>'+
                                '<td>'+((attempt.checked==1)?('<a href="'+sitePathStudent+'subjective-exams/'+examId+'/result/attempt/'+attempt.attemptId+'">See Result</a>'):('Not Checked'))+'</td>'+
                            '</tr>');
                    });
                    $('#subjectiveCheckedModal').modal("show");
                }
            };
        });
    });
</script>