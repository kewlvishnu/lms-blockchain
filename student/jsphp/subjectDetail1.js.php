<script type="text/javascript">
var details;
var rating;
var certificate;
var check=1;
var showdatetill='';
var showdateFrom='';
var showTill=1;
var showFrom=1;
var subjectiveExams = [];
var manualExams = [];
var submissions = [];

function getStudentExams() {
	var req = {};
	var res;
	req.action = 'exam-attempts-chapterwise';
	req.courseId = courseId;
	req.subjectId = subjectId;
	ajaxRequest({
		'type': 'post',
		'url': ApiEndPoint,
		'data': JSON.stringify(req),
		success:function(res) {
			res = $.parseJSON(res);
			
			if (res.status == 0)
				toastr.error(res.message);
			else {
				fillStudentExams(res);
			}
		}
	});
}
function getContentNotes() {
	var req = {};
	var res;
	req.action = 'get-content-notes';
	req.courseId = courseId;
	req.subjectId = subjectId;
	ajaxRequest({
		'type': 'post',
		'url': ApiEndPoint,
		'data': JSON.stringify(req),
		success:function(res) {
			res = $.parseJSON(res);
			if (res.status == 0)
				toastr.error(res.message);
			else {
				fillContentNotes(res);
			}
		}
	});
}
function fillContentNotes(data) {
	var html = '';
	if (data.notes.length>0) {
		for (var i = data.notes.length - 1; i >= 0; i--) {
			html+= '<h4 class="block">'+data.notes[i].contentTitle+'</h4>'+
					'<div class="well">'+data.notes[i].notes+'</div><hr>';

		}
		html = html.substr(0, html.length-4);
	} else {
		html = '<div class="note note-info">'+
                    '<p> No notes </p>'+
                '</div>';
	}
	$('#tabNotes').html(html);
}
function fetchStudentPerformance() {
	var req = {};
	var res;
	req.action = 'get-student-performance';
	req.courseId = courseId;
	req.subjectId = subjectId;
	ajaxRequest({
		'type': 'post',
		'url': ApiEndPoint,
		'data': JSON.stringify(req),
		success:function(res) {
			res = $.parseJSON(res);
			if (res.status == 0)
				toastr.error(res.message);
			else {
				fillStudentPerformance(res);
			}
		}
	});
}
function fillStudentPerformance(data) {
	//console.log(data);
	var html = '';
	$('#subjectScore').html(data.subjectScore+'%');
	if (data.notes) {
		$('#gradingNotes').html(
			'<div class="form-group">'+
				'<label for="">Notes :</label>'+
				'<div class="well">'+data.notes+'</div>'+
			'</div>'
			);
	}
    if (data.contents) {
        var contents = data.contents;
        for (var i = 0; i < contents.length; i++) {
            var section = contents[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            html+='<tr data-id="'+contents[i].id+'" data-type="exam">'+
                    '<td>'+contents[i].title+'</td>'+
                    '<td>Content</td>'+
                    '<td>'+section+'</td>'+
                    '<td>-</td>'+
                    '<td>-</td>'+
                    '<td>-</td>'+
                    '<td>-</td>'+
                    '<td>-</td>'+
                    '<td>'+contents[i].weight_grading+'</td>'+
                    '<td>'+contents[i].performance+'</td>'+
                '</tr>';
        }
    }
    if (data.exams) {
        var exams = data.exams;
        for (var i = 0; i < exams.length; i++) {
            var endDate = '-';
            if (exams[i].endDate) {
                endDate = formatDate(parseInt(exams[i].endDate), '-');
            }
            var section = exams[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            html+='<tr data-id="'+exams[i].id+'" data-type="exam">'+
                    '<td>'+exams[i].name+'</td>'+
                    '<td>'+exams[i].type+'</td>'+
                    '<td>'+section+'</td>'+
                    '<td>'+formatDate(parseInt(exams[i].startDate), '-')+'</td>'+
                    '<td>'+endDate+'</td>'+
                    '<td>'+exams[i].user_attempts+'/'+exams[i].attempts+'</td>'+
                    '<td>'+exams[i].grade_type+'</td>'+
                    '<td>'+exams[i].avg_attempts+'</td>'+
                    '<td>'+exams[i].weight+'</td>'+
                    '<td>'+exams[i].performance+'</td>'+
                '</tr>';
        }
    }
    if (data.subjective_exams) {
        var exams = data.subjective_exams;
        for (var i = 0; i < exams.length; i++) {
            var endDate = '-';
            if (exams[i].endDate) {
                endDate = formatDate(parseInt(exams[i].endDate), '-');
            }
            var section = exams[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            html+='<tr data-id="'+exams[i].id+'" data-type="subjective-exam">'+
                    '<td>'+exams[i].name+'</td>'+
                    '<td>Subjective Exam</td>'+
                    '<td>'+section+'</td>'+
                    '<td>'+formatDate(parseInt(exams[i].startDate), '-')+'</td>'+
                    '<td>'+endDate+'</td>'+
                    '<td>'+exams[i].user_attempts+'/'+exams[i].attempts+'</td>'+
                    '<td>'+exams[i].grade_type+'</td>'+
                    '<td>'+exams[i].avg_attempts+'</td>'+
                    '<td>'+exams[i].weight+'</td>'+
                    '<td>'+exams[i].performance+'</td>'+
                '</tr>';
        }
    }
    if (data.manual_exams) {
        var exams = data.manual_exams;
        for (var i = 0; i < exams.length; i++) {
            var section = exams[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            html+='<tr data-id="'+exams[i].id+'" data-type="manual-exam">'+
                    '<td>'+exams[i].name+'</td>'+
                    '<td>Manual Exam</td>'+
                    '<td>'+section+'</td>'+
                    '<td>NA</td>'+
                    '<td>NA</td>'+
                    '<td>'+exams[i].user_attempts+'/1</td>'+
                    '<td>'+exams[i].grade_type+'</td>'+
                    '<td><span class="badge">N.A.</span></td>'+
                    '<td>'+exams[i].weight+'</td>'+
                    '<td>'+exams[i].performance+'</td>'+
                '</tr>';
        }
    }
    if (data.submissions) {
        var exams = data.submissions;
        for (var i = 0; i < exams.length; i++) {
            var endDate = '-';
            if (exams[i].endDate) {
                endDate = formatDate(parseInt(exams[i].endDate), '-');
            }
            var section = exams[i].chapterName;
            if (section==null) {
                var section = '<span class="badge">Independent</span>';
            }
            html+='<tr data-id="'+exams[i].id+'" data-type="submission">'+
                    '<td>'+exams[i].name+'</td>'+
                    '<td>Submission</td>'+
                    '<td>'+section+'</td>'+
                    '<td>'+formatDate(parseInt(exams[i].startDate), '-')+'</td>'+
                    '<td>'+endDate+'</td>'+
                    '<td>'+exams[i].user_attempts+'/'+exams[i].attempts+'</td>'+
                    '<td>'+exams[i].grade_type+'</td>'+
                    '<td>'+exams[i].avg_attempts+'</td>'+
                    '<td>'+exams[i].weight+'</td>'+
                    '<td>'+exams[i].performance+'</td>'+
                '</tr>';
        }
    }
    $('#listExams').html(html);
}
function fillStudentExams(data) {
	var sectionCoverStart = sectionCoverEnd = divLectures = divExams = tagsExams = itagsExams = tagsHtml = '', tr = '';
	var contentLink = '';
	var count = 1;
	var disabled = '', startnow = '';
	var syllabus= data.syllabus;
	if(!syllabus) {
		$('a[href="#tabSyllabus"]').parent().remove();
		$('#tabSyllabus').remove();
	} else {
		$('#tabSyllabus').html(
			'<iframe src="'+sitepathStudentIncludes+'assets/custom/js/plugins/pdfjs/web/viewer.html?‌​file='+syllabus+'" class="btn-block min-height-700" frameborder="0"></iframe>'
			);
	}
	$.each(data.chapters, function (i, chapter) {
	
		contentLink = sitePathStudent+'content/subjects/'+subjectId+'/chapters/' + chapter.id;

		sectionCoverStart = '<div class="portlet box grey margin-bottom-10">'+
							'<div class="portlet-title">'+
								'<div class="caption">'+
									'<i class="fa fa-book"></i>Section ' + (i + 1) + ': <strong>' + chapter.name + '</strong></div>'+
								'<div class="tools">'+
									'<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
								'</div>'+
							'</div>';

		sectionCoverEnd = '</div>'+
						'</div>'+
					'</div>';

		if(chapter.content) {
			var coursePercentage=Math.round((chapter.seen/chapter.content.length) *100);
			/*divLectures += '<div class="portlet box grey margin-bottom-10">'+
							'<div class="portlet-title">'+
								'<div class="caption">'+
									'<i class="fa fa-book"></i>Section ' + (i + 1) + ': <strong>' + chapter.name + '</strong></div>'+
								'<div class="tools">'+
									'<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
								'</div>'+
							'</div>';*/
			divLectures += sectionCoverStart;
			if (coursePercentage>0) {
				divLectures += '<div class="progress progress-striped">'+
									'<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="'+coursePercentage+'" aria-valuemin="0" aria-valuemax="100" style="width: '+coursePercentage+'%">'+
										'<span class="sr-only"> '+coursePercentage+'% Complete </span>'+
									'</div>'+
								'</div>';
			}

			divLectures += '<div class="portlet-body no-space">'+
								'<div class="table-responsive no-space">'+
									'<table class="table table-striped table-bordered table-advance table-hover">'+
										'<thead>'+
											'<tr>'+
												'<th width="70%">'+
												'<i class="fa fa-briefcase"></i> Title </th>'+
												'<th width="30%"> </th>'+
											'</tr>'+
										'</thead>'+
										'<tbody>';
			$.each(chapter.content, function (j, content) {
				divLectures += '<tr>'+
									'<td class="highlight">'+
										'<div class="'+bootstrapColors[getRandomInt(0, bootstrapColors.length-1)]+'"></div>'+
										'<a href="'+contentLink+'/content/'+content.id+'"> '+content.title+((content.published==0)?' <span class="text-danger">(Not published)</span>':'')+' </a>'+
									'</td>'+
									'<td>'+
										'<a href="'+contentLink+'/content/'+content.id+'" class="btn btn-outline btn-circle btn-sm purple">'+
											'<i class="fa fa-eye"></i> View </a>'+
									'</td>'+
								'</tr>';
			});
			/*divLectures +=			'</tbody>'+
								'</table>'+
							'</div>'+
						'</div>'+
					'</div>';*/
			divLectures +=			'</tbody>'+
								'</table>'+sectionCoverEnd;
		}

		if (chapter.exams.length > 0) {
			divExams+= '<div class="portlet box grey margin-bottom-10">'+
							'<div class="portlet-title">'+
								'<div class="caption">'+
									'<i class="fa fa-edit"></i><strong>Assignment/Exams</strong></div>'+
								'<div class="tools">'+
									'<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
								'</div>'+
							'</div>'+
							'<div class="portlet-body no-space">'+
								'<div class="table-responsive no-space">'+
									'<table class="table table-striped table-bordered table-advance table-hover">'+
										'<thead>'+
											'<tr>'+
											'<th width="30%">Exam<br></th>'+
											'<th width="10%">Type<br></th>'+
											'<th width="10%">Start Date</th>'+
											'<th width="10%">End Date</th>'+
											'<th width="10%" class="text-center">Attempts Given</th>'+
											'<th width="10%" class="text-center">Attempts Left</th>'+
											'<th width="10%" class="text-center">Results</th>'+
											'<th width="10%"></th>'+
											'</tr>'+
										'</thead>'+
										'<tbody>';

			tr = '';
			$.each(chapter.exams, function (i, exam) {
				check=1;
				showdatetill='';
				showdateFrom='';
				showTill=1;
				showFrom=1;
				var today= $.now();
				
				if(exam.showResult=='immediately' || exam.endDate=='')
				{
					check=1;
				}
				else{
					if(today> exam.endDate)
					{
						check=1;
					}			
					else{
						check=2;
						showFrom=2;
						var tillDate = new Date(parseInt(exam.endDate));
						if(tillDate!='' || tillDate!=null)
						{
							var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
							if(tillDate.getHours() < 10)
								tsd += '0' + tillDate.getHours();
							else
								tsd += tillDate.getHours();
							tsd += ':';
							if(tillDate.getMinutes() < 10)
								tsd += '0' + tillDate.getMinutes();
							else
								tsd += tillDate.getMinutes();
						}
						showdateFrom=tsd;
					}
				}
				if(((exam.showResultTill == 2 && today < exam.showResultTillDate) || ((exam.showResultTill == 1 && today < exam.courseEndDate) || (exam.showResultTill == 1 && exam.courseEndDate == ''))) && check==1)
				{	
					check=1;
							
				}			
				else{
					check=2;
					showTill=2;
					if (exam.showResultTill == 2) {
						showdatetill=exam.showResultTillDate;
					} else {
						showdatetill=exam.courseEndDate;
					}
					if (showdatetill != '') {
						var tillDate = new Date(parseInt(showdatetill));
						if(tillDate!='' || tillDate!=null)
						{
							var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
							if(tillDate.getHours() < 10)
								tsd += '0' + tillDate.getHours();
							else
								tsd += tillDate.getHours();
							tsd += ':';
							if(tillDate.getMinutes() < 10)
								tsd += '0' + tillDate.getMinutes();
							else
								tsd += tillDate.getMinutes();
						}
					} else {
						showTill=1;
					}
					showdatetill=tsd;
				}
		
				var show='';
				var startdate = new Date(parseInt(exam.startDate));
				var display_chapter;
				var enddate, display_enddate;
				if (exam.endDate == null || exam.endDate == '') {
					display_enddate = '--';
				} else {
					enddate = new Date(parseInt(exam.endDate));
					display_enddate = enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + '<br/>' + formatTime2Digits(enddate.getHours()) + ':' + formatTime2Digits(enddate.getMinutes());
				}
				if (exam.examgiven == 0) {
					disabled = 'disabled="disabled" href="javascript:void(0);"';
				} else {
					disabled = 'href="'+sitePathStudent+'exams/' + exam.id + '/result"';
				}
				var attempsLeft = parseInt(exam.attempts - exam.examgiven);
				if (attempsLeft<0) {attempsLeft = 0};
				var buttonText = '';
				if(exam.paused == 0)
					buttonText = 'Start Test';
				else
					buttonText = 'Resume';
				tr += '<tr>'+
						'<td class="highlight">'+
							'<div class="'+bootstrapColors[getRandomInt(0, bootstrapColors.length-1)]+'"></div>'+
							'<a data-href="'+sitepathStudentOld+'examModule.php?examId=' + exam.id + '" class="start-exam" data-startdate="' + exam.startDate + '" data-endDate=' + exam.endDate + ' data-attempts=' + attempsLeft + ' DISABLED>' + exam.exam + '</a>'+
						'</td>'+
						'<td>' + exam.type + '</td>'+
						'<td>' + startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + '<br>' + formatTime2Digits(startdate.getHours()) + ':' + formatTime2Digits(startdate.getMinutes()) + '</td>'+
						'<td>' + display_enddate + '</td>'+
						'<td class="text-center">' + exam.examgiven + '</td>'+
						'<td class="text-center">' + attempsLeft + '</td>';
				if(check == 1)
				{
					tr+= '<td class="text-center"><a ' + disabled + ' data-href="'+sitePathStudent+'exams/' + exam.id + '/result" class="btn btn-md myHover">Show</a></td>';
				}
				else{
					if(showTill==2 && showFrom==2)
					{	
						show= 'Result will be shown only from '+showdateFrom+ ' to ' +showdatetill;
						tr+= '<td class="text-center">'+show+'</td>';
						
					}
					//("Submitted sucessfully.Result will be shown only from "+''+showdateFrom+'To '+showdatetill);
					else if(showTill==2 && showFrom==1)
					{
						show= 'Result was shown till '+showdatetill;
						tr+= '<td class="text-center">'+show+'</td>';
					}
					//toastr.error("Submitted sucessfully.Result will be shown till "+' '+showdatetill);
					else
					{
						show= 'Result will be shown after '+showdateFrom;
						tr+= '<td class="text-center">'+show+'</td>';
				
					}
					//toastr.error("Submitted sucessfully.Result will be shown after "+' '+showdateFrom);
				}
				
				tr+= '<td class="text-center"><a data-href="'+sitepathStudentOld+'examModule.php?examId=' + exam.id + '" class="btn btn-success btn-sm btn-block start-exam" data-startdate="' + exam.startDate + '" data-endDate=' + exam.endDate + ' data-attempts=' + attempsLeft + ' DISABLED>' + buttonText + '</a></td>'
				+ '</tr>';

			});
			divExams+= tr + '</tbody>'+
								'</table>'+
							'</div>'+
						'</div>'+
					'</div>';

			itagsExams = '<div class="portlet-body no-space">'+
							'<div class="table-responsive no-space">';

			tagsHtml = '';
			tagsLength = 0;
			$.each(chapter.exams, function (i, exam) {
				tagsHtml +=	'<table class="table table-striped table-bordered table-advance table-hover">'+
							'<thead>'+
								'<tr>'+
								'<th width="70%">' + exam.exam + '</th>'+
								'<th width="20%">Percentage</th>'+
								'<th width="10%"></th>'
								'</tr>'+
							'</thead>'+
							'<tbody>';
				if(exam.tags != undefined) {
					if (exam.tags.length>0) {
						//console.log(exam.tags);
						for (var i = 0; i < exam.tags.length; i++) {
							tagsHtml += '<tr data-exam="'+exam.id+'" data-exam-type="exam" data-tag="'+exam.tags[i].id+'">'+
											'<td class="highlight">'+
												'<div class="'+bootstrapColors[getRandomInt(0, bootstrapColors.length-1)]+'"></div>'+
												'<span class="dib tag-name">'+exam.tags[i].tag+'</span>'+
											'</td>'+
											'<td>'+getPercentage(exam.tags[i].analysis,'exam',exam.tags[i].ranges)+'</td>'+
											'<td>'+
												'<a href="javascript:;" class="btn btn-outline btn-circle btn-sm purple btn-tag-detail"><i class="fa fa-eye"></i> View </a>'+
											'</td>'+
										'</tr>';
							tagsLength++;
						}
					}
				}
				tagsHtml +=		'</tbody>'+
						'</table>';

			});
			itagsExams+= tagsHtml + '</div>' +
						'</div>';
			if (tagsLength>0) {
				tagsExams+= itagsExams;
			}
		}
		if (chapter.subjectiveExams.length > 0) {
			divExams+= '<div class="portlet box grey margin-bottom-10">'+
							'<div class="portlet-title">'+
								'<div class="caption">'+
									'<i class="fa fa-edit"></i><strong>Subjective Exams</strong></div>'+
								'<div class="tools">'+
									'<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
								'</div>'+
							'</div>'+
							'<div class="portlet-body no-space">'+
								'<div class="table-responsive no-space">'+
									'<table class="table table-striped table-bordered table-advance table-hover">'+
										'<thead>'+
											'<tr>'+
											'<th width="30%">Exam<br></th>'+
											'<th width="10%">Type<br></th>'+
											'<th width="10%">Start Date</th>'+
											'<th width="10%">End Date</th>'+
											'<th width="10%" class="text-center">Attempts Given</th>'+
											'<th width="10%" class="text-center">Attempts Left</th>'+
											'<th width="10%" class="text-center">Results</th>'+
											'<th width="10%"></th>'+
											'</tr>'+
										'</thead>'+
										'<tbody>';

			tr = '';
			$.each(chapter.subjectiveExams, function (i, exam) {
				subjectiveExams.push(exam);
				check=1;
				showdatetill='';
				showdateFrom='';
				showTill=1;
				showFrom=1;
				var today= $.now();
				
				if(exam.endDate=='')
				{
					check=1;
				}
				else{
					if(today> exam.endDate)
					{
						check=1;
					}			
					else{
						check=2;
						showFrom=2;
						var tillDate = new Date(parseInt(exam.endDate));
						if(tillDate!='' || tillDate!=null)
						{
							var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
							if(tillDate.getHours() < 10)
								tsd += '0' + tillDate.getHours();
							else
								tsd += tillDate.getHours();
							tsd += ':';
							if(tillDate.getMinutes() < 10)
								tsd += '0' + tillDate.getMinutes();
							else
								tsd += tillDate.getMinutes();
						}
						showdateFrom=tsd;
					}
				}
		
				var show='';
				var startdate = new Date(parseInt(exam.startDate));
				var display_chapter;
				var enddate, display_enddate;
				if (exam.endDate == null || exam.endDate == '') {
					display_enddate = '--';
				} else {
					enddate = new Date(parseInt(exam.endDate));
					display_enddate = enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + '<br/>' + formatTime2Digits(enddate.getHours()) + ':' + formatTime2Digits(enddate.getMinutes());
				}
				if (exam.listAttempts == 0) {
					disabled = 'disabled="disabled"';
				} else {
					disabled = '';
				}
				var attempsLeft = exam.attempts - exam.examgiven;
				if(attempsLeft<=0)
				{
					attempsLeft			=	0;
				}
				var buttonText = '';
				if(exam.paused == 0)
					buttonText = 'Start Test';
				else
					buttonText = 'Resume';
				tr += '<tr>'+
						'<td class="highlight">'+
							'<div class="'+bootstrapColors[getRandomInt(0, bootstrapColors.length-1)]+'"></div>'+
							'<a data-href="'+sitepathStudentOld+'subjectiveExamModule.php?courseId=' + courseId + '&examId=' + exam.id + '" class="start-exam" data-startdate="' + exam.startDate + '" data-endDate="' + exam.endDate + '" data-attempts=' + attempsLeft + ' disabled>' + exam.exam + '</a>'+
						'</td>'+
						'<td>' + exam.type + '</td>'+
						'<td>' + startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + '<br>' + formatTime2Digits(startdate.getHours()) + ':' + formatTime2Digits(startdate.getMinutes()) + '</td>'+
						'<td>' + display_enddate + '</td>'+
						'<td class="text-center">' + exam.examgiven + '</td>'+
						'<td class="text-center">' + (exam.attempts - exam.examgiven) + '</td>';
				if(check == 1)
				{
					tr+= '<td class="text-center"><a ' + disabled + ' href="javascript:void(0)" class="btn btn-md js-check-attempts" data-examid="' + exam.id + '">Show</a></td>';
				}
				else{
					tr+= '<td class="text-center">Result will be shown after '+showdateFrom+'</td>';
				}
				tr += '<td class="text-center"><a data-href="'+sitepathStudentOld+'subjectiveExamModule.php?courseId=' + courseId + '&examId=' + exam.id + '" class="btn btn-success btn-sm btn-block start-exam" data-startdate="' + exam.startDate + '" data-endDate="' + exam.endDate + '" data-attempts=' + attempsLeft + ' disabled>' + buttonText + '</a></td>'+
					'</tr>';
			});
			divExams+= tr + '</tbody>'+
								'</table>'+
							'</div>'+
						'</div>'+
					'</div>';

			itagsExams = '<div class="portlet-body no-space">'+
							'<div class="table-responsive no-space">';

			tagsHtml = '';
			tagsLength = 0;
			$.each(chapter.subjectiveExams, function (i, exam) {
				tagsHtml +=	'<table class="table table-striped table-bordered table-advance table-hover">'+
							'<thead>'+
								'<tr>'+
								'<th width="70%">' + exam.exam + '</th>'+
								'<th width="20%">Percentage</th>'+
								'<th width="10%"></th>'
								'</tr>'+
							'</thead>'+
							'<tbody>';
				if(exam.tags != undefined) {
					if (exam.tags.length>0) {
						//console.log(exam.tags);
						for (var i = 0; i < exam.tags.length; i++) {
							tagsHtml += '<tr data-exam="'+exam.id+'" data-exam-type="subjective" data-tag="'+exam.tags[i].id+'">'+
											'<td class="highlight">'+
												'<div class="'+bootstrapColors[getRandomInt(0, bootstrapColors.length-1)]+'"></div>'+
												'<span class="dib tag-name">'+exam.tags[i].tag+'</span>'+
											'</td>'+
											'<td>'+getPercentage(exam.tags[i].analysis,'subjective',exam.tags[i].ranges)+'</td>'+
											'<td>'+
												'<a href="javascript:;" class="btn btn-outline btn-circle btn-sm purple btn-tag-detail"><i class="fa fa-eye"></i> View </a>'+
											'</td>'+
										'</tr>';
							tagsLength++;
						}
					}
				}
				tagsHtml +=		'</tbody>'+
						'</table>';
			});
			itagsExams+= tagsHtml + '</div>'+
								'</div>';
			if (tagsLength>0) {
				tagsExams+= itagsExams;
			}
		}
		if (chapter.manualExams.length > 0) {
			divExams+= '<div class="portlet box grey margin-bottom-10">'+
							'<div class="portlet-title">'+
								'<div class="caption">'+
									'<i class="fa fa-edit"></i><strong>Manual Exams</strong></div>'+
								'<div class="tools">'+
									'<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
								'</div>'+
							'</div>'+
							'<div class="portlet-body no-space">'+
								'<div class="table-responsive no-space">'+
									'<table class="table table-striped table-bordered table-advance table-hover">'+
										'<thead>'+
											'<tr>'+
												'<th width="30%">Exam<br></th>'+
												'<th width="40%">Type<br></th>'+
												'<th width="30%" class="text-center">Results</th>'+
											'</tr>'+
										'</thead>'+
										'<tbody>';
			tr = '';
			$.each(chapter.manualExams, function (i, exam) {
				manualExams.push(exam);
				var today= $.now();

				var show='';

				tr += '<tr>'+
						'<td class="highlight">'+
							'<div class="'+bootstrapColors[getRandomInt(0, bootstrapColors.length-1)]+'"></div>'+
							'<a href="'+sitePathStudent+'manual-exams/'+exam.id+'/result/" data-examid="' + exam.id + '">'+exam.name +'</a>'+
						'</td>'+
						'<td>' + exam.type + ' Analytics</td>'+
						'<td class="text-center"><a href="'+sitePathStudent+'manual-exams/'+exam.id+'/result/" class="btn btn-md" data-examid="' + exam.id + '">Show</a></td>'+
					'</tr>';
			});
			divExams+= tr + '</tbody>'+
								'</table>'+
							'</div>'+
						'</div>'+
					'</div>';
		}
		if (chapter.submissions.length > 0) {
			divExams+= '<div class="portlet box grey margin-bottom-10">'+
							'<div class="portlet-title">'+
								'<div class="caption">'+
									'<i class="fa fa-edit"></i><strong>Submissions</strong></div>'+
								'<div class="tools">'+
									'<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
								'</div>'+
							'</div>'+
							'<div class="portlet-body no-space">'+
								'<div class="table-responsive no-space">'+
									'<table class="table table-striped table-bordered table-advance table-hover">'+
										'<thead>'+
											'<tr>'+
											'<th width="40%">Submission<br></th>'+
											'<th width="10%">Start Date</th>'+
											'<th width="10%">End Date</th>'+
											'<th width="10%" class="text-center">Attempts Given</th>'+
											'<th width="10%" class="text-center">Attempts Left</th>'+
											'<th width="10%" class="text-center">Results</th>'+
											'<th width="10%"></th>'+
											'</tr>'+
										'</thead>'+
										'<tbody>';

			tr = '';
			$.each(chapter.submissions, function (i, exam) {
				submissions.push(exam);
				check=1;
				showdatetill='';
				showdateFrom='';
				showTill=1;
				showFrom=1;
				var today= $.now();
				
				if(exam.endDate=='')
				{
					check=1;
				}
				else{
					if(today> exam.endDate)
					{
						check=1;
					}			
					else{
						check=2;
						showFrom=2;
						var tillDate = new Date(parseInt(exam.endDate));
						if(tillDate!='' || tillDate!=null)
						{
							var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
							if(tillDate.getHours() < 10)
								tsd += '0' + tillDate.getHours();
							else
								tsd += tillDate.getHours();
							tsd += ':';
							if(tillDate.getMinutes() < 10)
								tsd += '0' + tillDate.getMinutes();
							else
								tsd += tillDate.getMinutes();
						}
						showdateFrom=tsd;
					}
				}
		
				var show='';
				var startdate = new Date(parseInt(exam.startDate));
				var display_chapter;
				var enddate, display_enddate;
				if (exam.endDate == null || exam.endDate == '') {
					display_enddate = '--';
				} else {
					enddate = new Date(parseInt(exam.endDate));
					display_enddate = enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + '<br/>' + formatTime2Digits(enddate.getHours()) + ':' + formatTime2Digits(enddate.getMinutes());
				}
				if (exam.listAttempts == 0) {
					disabled = 'disabled="disabled"';
				} else {
					disabled = '';
				}
				var attempsLeft = exam.attempts - exam.examgiven;
				if(attempsLeft<=0)
				{
					attempsLeft			=	0;
				}
				var buttonText = '';
				if(exam.paused == 0)
					buttonText = 'Start Test';
				else
					buttonText = 'Resume';
				tr += '<tr>'+
						'<td class="highlight">'+
							'<div class="'+bootstrapColors[getRandomInt(0, bootstrapColors.length-1)]+'"></div>'+
							'<a data-href="'+sitePathStudent+'submissions/'+exam.id+'" class="start-exam" data-startdate="' + exam.startDate + '" data-endDate="' + exam.endDate + '" data-attempts=' + attempsLeft + ' disabled>' + exam.exam + '</a>'+
						'</td>'+
						'<td>' + startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + '<br>' + formatTime2Digits(startdate.getHours()) + ':' + formatTime2Digits(startdate.getMinutes()) + '</td>'+
						'<td>' + display_enddate + '</td>'+
						'<td class="text-center">' + exam.examgiven + '</td>'+
						'<td class="text-center">' + (exam.attempts - exam.examgiven) + '</td>';
				if(check == 1)
				{
					tr+= '<td class="text-center"><a ' + disabled + ' href="javascript:void(0)" class="btn btn-md js-check-submission-attempts" data-examid="' + exam.id + '">Show</a></td>';
				}
				else{
					tr+= '<td class="text-center">Result will be shown after '+showdateFrom+'</td>';
				}
				tr += '<td class="text-center"><a data-href="'+sitePathStudent+'submissions/'+exam.id+'" class="btn btn-success btn-sm btn-block start-exam" data-startdate="' + exam.startDate + '" data-endDate="' + exam.endDate + '" data-attempts=' + attempsLeft + ' disabled>' + buttonText + '</a></td>'+
					'</tr>';
			});
			divExams+= tr + '</tbody>'+
								'</table>'+
							'</div>'+
						'</div>'+
					'</div>';

			itagsExams = '<div class="portlet-body no-space">'+
							'<div class="table-responsive no-space">';

			tagsHtml = '';
			tagsLength = 0;
			$.each(chapter.submissions, function (i, exam) {
				tagsHtml +=	'<table class="table table-striped table-bordered table-advance table-hover">'+
							'<thead>'+
								'<tr>'+
								'<th width="70%">' + exam.exam + '</th>'+
								'<th width="20%">Percentage</th>'+
								'<th width="10%"></th>'
								'</tr>'+
							'</thead>'+
							'<tbody>';
				if(exam.tags != undefined) {
					if (exam.tags.length>0) {
						//console.log(exam.tags);
						for (var i = 0; i < exam.tags.length; i++) {
							tagsHtml += '<tr data-exam="'+exam.id+'" data-exam-type="submission" data-tag="'+exam.tags[i].id+'">'+
											'<td class="highlight">'+
												'<div class="'+bootstrapColors[getRandomInt(0, bootstrapColors.length-1)]+'"></div>'+
												'<span class="dib tag-name">'+exam.tags[i].tag+'</span>'+
											'</td>'+
											'<td>'+getPercentage(exam.tags[i].analysis,'submission',exam.tags[i].ranges)+'</td>'+
											'<td>'+
												'<a href="javascript:;" class="btn btn-outline btn-circle btn-sm purple btn-tag-detail"><i class="fa fa-eye"></i> View </a>'+
											'</td>'+
										'</tr>';
							tagsLength++;
						}
					}
				}
				tagsHtml +=		'</tbody>'+
						'</table>';
			});
			itagsExams+= tagsHtml + '</div>'+
								'</div>';
			if (tagsLength>0) {
				tagsExams+= itagsExams;
			}
		}
	});

	if (tagsExams != '') {
		tagsExams = sectionCoverStart+tagsExams+sectionCoverEnd;
	}

	if (data.indepdentExams.length > 0) {
		divExams+= '<div class="portlet box grey margin-bottom-10">'+
						'<div class="portlet-title">'+
							'<div class="caption">'+
								'<i class="fa fa-edit"></i><strong>Independent Assignments/Exams</strong></div>'+
							'<div class="tools">'+
								'<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
							'</div>'+
						'</div>'+
						'<div class="portlet-body no-space">'+
							'<div class="table-responsive no-space">'+
								'<table class="table table-striped table-bordered table-advance table-hover">'+
									'<thead>'+
										'<tr>'+
										'<th width="30%">Exam<br></th>'+
										'<th width="10%">Type<br></th>'+
										'<th width="10%">Start Date</th>'+
										'<th width="10%">End Date</th>'+
										'<th width="10%" class="text-center">Attempts Given</th>'+
										'<th width="10%" class="text-center">Attempts Left</th>'+
										'<th width="10%" class="text-center">Results</th>'+
										'<th width="10%"></th>'+
										'</tr>'+
									'</thead>'+
									'<tbody>';
		tr = '';
		$.each(data.indepdentExams, function (i, exam) {
			check=1;
			showdatetill='';
			showdateFrom='';
			showTill=1;
			showFrom=1;
			var today= $.now();

			if(exam.showResult=='immediately' || exam.endDate=='')
			{
				check=1;
			}		
			else{
				if(today> exam.endDate)
				{
					check=1;
				}			
				else{
					check=2;
					showFrom=2;
					var tillDate = new Date(parseInt(exam.endDate));
					if(tillDate!='' || tillDate!=null)
					{
						var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
						if(tillDate.getHours() < 10)
							tsd += '0' + tillDate.getHours();
						else
							tsd += tillDate.getHours();
						tsd += ':';
						if(tillDate.getMinutes() < 10)
							tsd += '0' + tillDate.getMinutes();
						else
							tsd += tillDate.getMinutes();
					}
					showdateFrom=tsd;
				}																		
			}
			if(((exam.showResultTill == 2 && today < exam.showResultTillDate) || ((exam.showResultTill == 1 && today < exam.courseEndDate) || (exam.showResultTill == 1 && exam.courseEndDate == ''))) && check==1)
			{	
				check=1;		
			}						
			else{
				check=2;
				showTill=2;
				if (exam.showResultTill == 2) {
					showdatetill=exam.showResultTillDate;
				} else {
					showdatetill=exam.courseEndDate;
				}
				var tillDate = new Date(parseInt(showdatetill));
				if(tillDate!='' || tillDate!=null)
				{
					var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
					if(tillDate.getHours() < 10)
						tsd += '0' + tillDate.getHours();
					else
						tsd += tillDate.getHours();
					tsd += ':';
					if(tillDate.getMinutes() < 10)
						tsd += '0' + tillDate.getMinutes();
					else
						tsd += tillDate.getMinutes();
				}											
				showdatetill=tsd;
			}
			//console.log("check: "+check);
		
			var show='';

			var startdate = new Date(parseInt(exam.startDate));
			var display_chapter;
			var enddate, display_enddate;
			if (exam.endDate == null || exam.endDate == '') {
				display_enddate = '--';
			} else {
				enddate = new Date(parseInt(exam.endDate));
				display_enddate = enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + '<br/>' + formatTime2Digits(enddate.getHours()) + ':' + formatTime2Digits(enddate.getMinutes());
			}
			if (exam.examgiven == 0) {
				disabled = 'disabled="disabled" href="javascript:void(0);"';
			} else {
				disabled = 'href="'+sitePathStudent+'exams/' + exam.id + '/result/"';
			}
			var attempsLeft = exam.attempts - exam.examgiven;
			if(attempsLeft<0)
			{
				attempsLeft			=	0;
			}
			if(exam.paused == 0)
					buttonText = 'Start Test';
				else
					buttonText = 'Resume';
			tr += '<tr>'+
					'<td class="highlight">' +
							'<div class="'+bootstrapColors[getRandomInt(0, bootstrapColors.length-1)]+'"></div>'+
							'<a data-href="'+sitepathStudentOld+'examModule.php?examId=' + exam.id + '" class="start-exam" data-startdate="' + exam.startDate + '" data-endDate=' + exam.endDate + ' data-attempts=' + attempsLeft + ' DISABLED>' + exam.exam + '</a>'+
					'</td>'+
					'<td>' + exam.type + '</td>'+
					'<td>' + startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + '</br>' + formatTime2Digits(startdate.getHours()) + ':' + formatTime2Digits(startdate.getMinutes()) + '</td>'+
					'<td>' + display_enddate + '</td>'+
					'<td class="text-center">' + exam.examgiven + '</td>'+
					'<td class="text-center">' + attempsLeft + '</td>';
					if(check == 1)
					{
						tr+= '<td class="text-center"><a ' + disabled + ' data-href="'+sitePathStudent+'exams/' + exam.id + '/result/" class="btn btn-md myHover">Show</a></td>';
					}
					else{
							if(showTill==2 && showFrom==2)
							{	
								show= 'Result will be shown only from '+showdateFrom+ ' to ' +showdatetill;
								tr+= '<td class="text-center">'+show+'</td>';
								
							}
							//("Submitted sucessfully.Result will be shown only from "+''+showdateFrom+'To '+showdatetill);
							else if(showTill==2 && showFrom==1)
							{
								show= 'Result was shown till '+showdatetill;
								tr+= '<td class="text-center">'+show+'</td>';
							}
							//toastr.error("Submitted sucessfully.Result will be shown till "+' '+showdatetill);
							else
							{
								show= 'Result will be shown after '+showdateFrom;
								tr+= '<td class="text-center">'+show+'</td>';
						
							}
					//toastr.error("Submitted sucessfully.Result will be shown after "+' '+showdateFrom);
					}
					tr+= '<td class="text-center"><a data-href="'+sitepathStudentOld+'examModule.php?examId=' + exam.id + '" class="btn btn-success btn-sm btn-block start-exam" data-startdate="' + exam.startDate + '" data-endDate=' + exam.endDate + ' data-attempts=' + attempsLeft + ' DISABLED>' + buttonText + '</a></td>'
					+ '</tr>';
		});
		divExams+= tr + '</tbody>'+
							'</table>'+
						'</div>'+
					'</div>'+
				'</div>';



		itagsExams = '<div class="portlet box grey margin-bottom-10">'+
						'<div class="portlet-title">'+
							'<div class="caption">'+
								'<i class="fa fa-edit"></i><strong>Independent Assignments/Exams</strong></div>'+
							'<div class="tools">'+
								'<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
							'</div>'+
						'</div>'+
						'<div class="portlet-body no-space">'+
							'<div class="table-responsive no-space">';
		tagsHtml = '';
		tagsLength = 0;
		$.each(data.indepdentExams, function (i, exam) {
			tagsHtml +=	'<table class="table table-striped table-bordered table-advance table-hover">'+
							'<thead>'+
								'<tr>'+
								'<th width="70%">' + exam.exam + '</th>'+
								'<th width="20%">Percentage</th>'+
								'<th width="10%"></th>'
								'</tr>'+
							'</thead>'+
							'<tbody>';
			if(exam.tags != undefined) {
				if (exam.tags.length>0) {
					//console.log(exam.tags);
					for (var i = 0; i < exam.tags.length; i++) {
						tagsHtml += '<tr data-exam="'+exam.id+'" data-exam-type="exam" data-tag="'+exam.tags[i].id+'">'+
										'<td class="highlight">'+
											'<div class="'+bootstrapColors[getRandomInt(0, bootstrapColors.length-1)]+'"></div>'+
											'<span class="dib tag-name">'+exam.tags[i].tag+'</span>'+
										'</td>'+
										'<td>'+getPercentage(exam.tags[i].analysis,'exam',exam.tags[i].ranges)+'</td>'+
										'<td>'+
											'<a href="javascript:;" class="btn btn-outline btn-circle btn-sm purple btn-tag-detail"><i class="fa fa-eye"></i> View </a>'+
										'</td>'+
									'</tr>';
						tagsLength++;
					}
				}
			}
			tagsHtml +=		'</tbody>'+
					'</table>';
		});
		itagsExams+= tagsHtml + '</div>'+
							'</div>'+
						'</div>';
		if (tagsLength>0) {
			tagsExams+= itagsExams;
		}
	}

	if (data.independentSubjectiveExams.length > 0) {
		divExams+= '<div class="portlet box grey margin-bottom-10">'+
						'<div class="portlet-title">'+
							'<div class="caption">'+
								'<i class="fa fa-edit"></i><strong>Independent Subjective Exams</strong></div>'+
							'<div class="tools">'+
								'<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
							'</div>'+
						'</div>'+
						'<div class="portlet-body no-space">'+
							'<div class="table-responsive no-space">'+
								'<table class="table table-striped table-bordered table-advance table-hover">'+
									'<thead>'+
										'<tr>'+
										'<th width="30%">Exam<br></th>'+
										'<th width="10%">Type<br></th>'+
										'<th width="10%">Start Date</th>'+
										'<th width="10%">End Date</th>'+
										'<th width="10%" class="text-center">Attempts Given</th>'+
										'<th width="10%" class="text-center">Attempts Left</th>'+
										'<th width="10%" class="text-center">Results</th>'+
										'<th width="10%"></th>'+
										'</tr>'+
									'</thead>'+
									'<tbody>';
		tr = '';
		$.each(data.independentSubjectiveExams, function (i, exam) {
			subjectiveExams.push(exam);
			check=1;
			showdatetill='';
			showdateFrom='';
			showTill=1;
			showFrom=1;
			var today= $.now();
				
			if(exam.endDate=='')
			{
				check=1;
			}
			else{
				if(today> exam.endDate)
				{
					check=1;
				}			
				else{
					check=2;
					showFrom=2;
					var tillDate = new Date(parseInt(exam.endDate));
					if(tillDate!='' || tillDate!=null)
					{
						var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
						if(tillDate.getHours() < 10)
							tsd += '0' + tillDate.getHours();
						else
							tsd += tillDate.getHours();
						tsd += ':';
						if(tillDate.getMinutes() < 10)
							tsd += '0' + tillDate.getMinutes();
						else
							tsd += tillDate.getMinutes();
					}
					showdateFrom=tsd;
				}
			}
		
			var show='';

			var startdate = new Date(parseInt(exam.startDate));
			var display_chapter;
			var enddate, display_enddate;
			if (exam.endDate == null || exam.endDate == '') {
				display_enddate = '--';
			} else {
				enddate = new Date(parseInt(exam.endDate));
				display_enddate = enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + '<br/>' + formatTime2Digits(enddate.getHours()) + ':' + formatTime2Digits(enddate.getMinutes());
			}
			if (exam.listAttempts == 0) {
				disabled = 'disabled="disabled"';
			} else {
				disabled = '';
			}
			var attempsLeft = exam.attempts - exam.examgiven;
			if(attempsLeft<=0)
			{
				attempsLeft			=	0;
			}
			if(exam.paused == 0)
					buttonText = 'Start Test';
				else
					buttonText = 'Resume';
			tr += '<tr>'+
					'<td class="highlight">'+
							'<div class="'+bootstrapColors[getRandomInt(0, bootstrapColors.length-1)]+'"></div>'+
							'<a data-href="'+sitepathStudentOld+'subjectiveExamModule.php?courseId=' + courseId + '&examId=' + exam.id + '" class="start-exam" data-startdate="' + exam.startDate + '" data-endDate="' + exam.endDate + '" data-attempts=' + attempsLeft + ' disabled>'+exam.exam +'</a>'+
					'</td>'+
					'<td>' + exam.type + '</td>'+
					'<td>' + startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + '</br>' + formatTime2Digits(startdate.getHours()) + ':' + formatTime2Digits(startdate.getMinutes()) + '</td>'+
					'<td>' + display_enddate + '</td>'+
					'<td class="text-center">' + exam.examgiven + '</td>'+
					'<td class="text-center">' + attempsLeft + '</td>';
				if(check == 1)
				{
					tr+= '<td class="text-center"><a ' + disabled + ' href="javascript:void(0)" class="btn btn-md js-check-attempts" data-examid="' + exam.id + '">Show</a></td>';
				}
				else{
					tr+= '<td class="text-center">Result will be shown after '+showdateFrom+'</td>';
				}
				tr+= '<td class="text-center"><a data-href="'+sitepathStudentOld+'subjectiveExamModule.php?courseId=' + courseId + '&examId=' + exam.id + '" class="btn btn-success btn-block btn-sm start-exam" data-startdate="' + exam.startDate + '" data-endDate="' + exam.endDate + '" data-attempts=' + attempsLeft + ' disabled>' + buttonText + '</a></td>'+
				'</tr>';
		});
		divExams+= tr + '</tbody>'+
							'</table>'+
						'</div>'+
					'</div>'+
				'</div>';



		itagsExams = '<div class="portlet box grey margin-bottom-10">'+
						'<div class="portlet-title">'+
							'<div class="caption">'+
								'<i class="fa fa-edit"></i><strong>Independent Subjective Exams</strong></div>'+
							'<div class="tools">'+
								'<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
							'</div>'+
						'</div>'+
						'<div class="portlet-body no-space">'+
							'<div class="table-responsive no-space">';
		tagsHtml = '';
		tagsLength = 0;
		$.each(data.independentSubjectiveExams, function (i, exam) {
			tagsHtml +=	'<table class="table table-striped table-bordered table-advance table-hover">'+
							'<thead>'+
								'<tr>'+
								'<th width="70%">' + exam.exam + '</th>'+
								'<th width="20%">Percentage</th>'+
								'<th width="10%"></th>'
								'</tr>'+
							'</thead>'+
							'<tbody>';
			if(exam.tags != undefined) {
				if (exam.tags.length>0) {
					//console.log(exam.tags);
					for (var i = 0; i < exam.tags.length; i++) {
						tagsHtml += '<tr data-exam="'+exam.id+'" data-exam-type="subjective" data-tag="'+exam.tags[i].id+'">'+
										'<td class="highlight">'+
											'<div class="'+bootstrapColors[getRandomInt(0, bootstrapColors.length-1)]+'"></div>'+
											'<span class="dib tag-name">'+exam.tags[i].tag+'</span>'+
										'</td>'+
										'<td>'+getPercentage(exam.tags[i].analysis,'subjective',exam.tags[i].ranges)+'</td>'+
										'<td>'+
											'<a href="javascript:;" class="btn btn-outline btn-circle btn-sm purple btn-tag-detail"><i class="fa fa-eye"></i> View </a>'+
										'</td>'+
									'</tr>';
						tagsLength++;
					}
				}
			}
			tagsHtml +=		'</tbody>'+
					'</table>';
		});
		itagsExams+= tagsHtml + '</div>'+
							'</div>'+
						'</div>';
		if (tagsLength>0) {
			tagsExams+= itagsExams;
		}
	}

	if (data.independentManualExams.length > 0) {
		divExams+= '<div class="portlet box grey margin-bottom-10">'+
						'<div class="portlet-title">'+
							'<div class="caption">'+
								'<i class="fa fa-edit"></i><strong>Independent Manual Exams</strong></div>'+
							'<div class="tools">'+
								'<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
							'</div>'+
						'</div>'+
						'<div class="portlet-body no-space">'+
							'<div class="table-responsive no-space">'+
								'<table class="table table-striped table-bordered table-advance table-hover">'+
									'<thead>'+
										'<tr>'+
											'<th width="30%">Exam<br></th>'+
											'<th width="40%">Type<br></th>'+
											'<th width="30%" class="text-center">Results</th>'+
										'</tr>'+
									'</thead>'+
									'<tbody>';
		tr = '';
		$.each(data.independentManualExams, function (i, exam) {
			manualExams.push(exam);
			var today= $.now();

			var show='';

			tr += '<tr>'+
					'<td class="highlight">'+
						'<div class="'+bootstrapColors[getRandomInt(0, bootstrapColors.length-1)]+'"></div>'+
						'<a href="'+sitePathStudent+'manual-exams/'+exam.id+'/result/" data-examid="' + exam.id + '">'+exam.name +'</a>'+
					'</td>'+
					'<td>' + exam.type + ' Analytics</td>'+
					'<td class="text-center"><a href="'+sitePathStudent+'manual-exams/'+exam.id+'/result/" class="btn btn-md" data-examid="' + exam.id + '">Show</a></td>'+
				'</tr>';
		});
		divExams+= tr + '</tbody>'+
							'</table>'+
						'</div>'+
					'</div>'+
				'</div>';
	}

	if (data.independentSubmissions.length > 0) {
		divExams+= '<div class="portlet box grey margin-bottom-10">'+
						'<div class="portlet-title">'+
							'<div class="caption">'+
								'<i class="fa fa-edit"></i><strong>Independent Submissions</strong></div>'+
							'<div class="tools">'+
								'<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
							'</div>'+
						'</div>'+
						'<div class="portlet-body no-space">'+
							'<div class="table-responsive no-space">'+
								'<table class="table table-striped table-bordered table-advance table-hover">'+
									'<thead>'+
										'<tr>'+
										'<th width="30%">Exam<br></th>'+
										'<th width="10%">Start Date</th>'+
										'<th width="10%">End Date</th>'+
										'<th width="10%" class="text-center">Attempts Given</th>'+
										'<th width="10%" class="text-center">Attempts Left</th>'+
										'<th width="10%" class="text-center">Results</th>'+
										'<th width="10%"></th>'+
										'</tr>'+
									'</thead>'+
									'<tbody>';
		tr = '';
		$.each(data.independentSubmissions, function (i, exam) {
			submissions.push(exam);
			check=1;
			showdatetill='';
			showdateFrom='';
			showTill=1;
			showFrom=1;
			var today= $.now();
				
			if(exam.endDate=='')
			{
				check=1;
			}
			else{
				if(today> exam.endDate)
				{
					check=1;
				}			
				else{
					check=2;
					showFrom=2;
					var tillDate = new Date(parseInt(exam.endDate));
					if(tillDate!='' || tillDate!=null)
					{
						var tsd = tillDate.getDate() + ' ' + month[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
						if(tillDate.getHours() < 10)
							tsd += '0' + tillDate.getHours();
						else
							tsd += tillDate.getHours();
						tsd += ':';
						if(tillDate.getMinutes() < 10)
							tsd += '0' + tillDate.getMinutes();
						else
							tsd += tillDate.getMinutes();
					}
					showdateFrom=tsd;
				}
			}
		
			var show='';

			var startdate = new Date(parseInt(exam.startDate));
			var display_chapter;
			var enddate, display_enddate;
			if (exam.endDate == null || exam.endDate == '') {
				display_enddate = '--';
			} else {
				enddate = new Date(parseInt(exam.endDate));
				display_enddate = enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear() + '<br/>' + formatTime2Digits(enddate.getHours()) + ':' + formatTime2Digits(enddate.getMinutes());
			}
			if (exam.listAttempts == 0) {
				disabled = 'disabled="disabled"';
			} else {
				disabled = '';
			}
			var attempsLeft = exam.attempts - exam.examgiven;
			if(attempsLeft<=0)
			{
				attempsLeft			=	0;
			}
			if(exam.paused == 0)
					buttonText = 'Start Test';
				else
					buttonText = 'Resume';
			tr += '<tr>'+
					'<td class="highlight">'+
							'<div class="'+bootstrapColors[getRandomInt(0, bootstrapColors.length-1)]+'"></div>'+
							'<a data-href="'+sitePathStudent+'submissions/'+exam.id+'" class="start-exam" data-startdate="' + exam.startDate + '" data-endDate="' + exam.endDate + '" data-attempts=' + attempsLeft + ' disabled>'+exam.exam +'</a>'+
					'</td>'+
					'<td>' + startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear() + '</br>' + formatTime2Digits(startdate.getHours()) + ':' + formatTime2Digits(startdate.getMinutes()) + '</td>'+
					'<td>' + display_enddate + '</td>'+
					'<td class="text-center">' + exam.examgiven + '</td>'+
					'<td class="text-center">' + (exam.attempts - exam.examgiven) + '</td>';
				if(check == 1)
				{
					tr+= '<td class="text-center"><a ' + disabled + ' href="javascript:void(0)" class="btn btn-md js-check-submission-attempts" data-examid="' + exam.id + '">Show</a></td>';
				}
				else{
					tr+= '<td class="text-center">Result will be shown after '+showdateFrom+'</td>';
				}
				tr+= '<td class="text-center"><a data-href="'+sitePathStudent+'submissions/'+exam.id+'" class="btn btn-success btn-block btn-sm start-exam" data-startdate="' + exam.startDate + '" data-endDate="' + exam.endDate + '" data-attempts=' + attempsLeft + ' disabled>' + buttonText + '</a></td>'+
				'</tr>';
		});
		divExams+= tr + '</tbody>'+
							'</table>'+
						'</div>'+
					'</div>'+
				'</div>';



		itagsExams = '<div class="portlet box grey margin-bottom-10">'+
						'<div class="portlet-title">'+
							'<div class="caption">'+
								'<i class="fa fa-edit"></i><strong>Independent Submissions</strong></div>'+
							'<div class="tools">'+
								'<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>'+
							'</div>'+
						'</div>'+
						'<div class="portlet-body no-space">'+
							'<div class="table-responsive no-space">';
		tagsHtml = '';
		tagsLength = 0;
		$.each(data.independentSubmissions, function (i, exam) {
			tagsHtml +=	'<table class="table table-striped table-bordered table-advance table-hover">'+
							'<thead>'+
								'<tr>'+
								'<th width="70%">' + exam.exam + '</th>'+
								'<th width="20%">Percentage</th>'+
								'<th width="10%"></th>'
								'</tr>'+
							'</thead>'+
							'<tbody>';
			if(exam.tags != undefined) {
				if (exam.tags.length>0) {
					//console.log(exam.tags);
					for (var i = 0; i < exam.tags.length; i++) {
						tagsHtml += '<tr data-exam="'+exam.id+'" data-exam-type="submission" data-tag="'+exam.tags[i].id+'">'+
										'<td class="highlight">'+
											'<div class="'+bootstrapColors[getRandomInt(0, bootstrapColors.length-1)]+'"></div>'+
											'<span class="dib tag-name">'+exam.tags[i].tag+'</span>'+
										'</td>'+
										'<td>'+getPercentage(exam.tags[i].analysis,'submission',exam.tags[i].ranges)+'</td>'+
										'<td>'+
											'<a href="javascript:;" class="btn btn-outline btn-circle btn-sm purple btn-tag-detail"><i class="fa fa-eye"></i> View </a>'+
										'</td>'+
									'</tr>';
						tagsLength++;
					}
				}
			}
			tagsHtml +=		'</tbody>'+
					'</table>';
		});
		itagsExams+= tagsHtml + '</div>'+
							'</div>'+
						'</div>';
		if (tagsLength>0) {
			tagsExams+= itagsExams;
		}
	}

	if (divLectures != "") {
		$('#tabLectures').append(divLectures);
	} else {
		$('a[href="#tabLectures"]').parent().remove();
		$('#tabLectures').remove();
	}
	if (divExams != "") {
		$('#tabExams').append(divExams);
	} else {
		$('a[href="#tabExams"]').parent().remove();
		$('#tabExams').remove();
		$('a[href="#tabPerformance"]').parent().remove();
		$('#tabPerformance').remove();
	}
	if (tagsExams != "") {
		$('#tabLearnObj').append(tagsExams);
	} else {
		$('a[href="#tabLearnObj"]').parent().remove();
		$('#tabLearnObj').remove();
	}

	var currentDate = data.currentDate;
	setInterval(function () {
		var todayTime = new Date(currentDate * 1000);
		$('.start-exam').each(function () {
			if ($(this).attr('data-attempts') != 0)
			{
				var startdate = new Date(parseInt($(this).attr('data-startdate')));
				if (startdate.valueOf() <= todayTime.valueOf())
				{
					$(this).attr('disabled', false);
					if ($(this).attr('data-href').length>0) {
						$(this).attr('href', $(this).attr('data-href'));
					};
				}
				var endDate = $(this).attr('data-endDate');
				if (endDate != '') {
					var examendDate = new Date(parseInt(endDate));
					if (examendDate.valueOf() <= todayTime.valueOf())
					{
						$(this).attr('disabled', 'disabled');
						var href = $(this).attr('href');
						$(this).attr('href', 'javascript:;');
						$(this).attr('data-href', href);
					}
			  }
			}
		});
		currentDate++;
	}, 1000);

	$(".subject-sidebar li:first-child>a").trigger('click');
}
function getSubjectProfessors() {
	var req = {};
	var res;
	req.action = 'get-subject-professors-chat';
	req.courseId = courseId;
	req.subjectId = subjectId;
	ajaxRequest({
		'type': 'post',
		'url': ApiEndPoint,
		'data': JSON.stringify(req),
		success:function(res) {
			res = $.parseJSON(res);
			if (res.status == 0) {
				$('a[href="#tabChats"]').parent().remove();
				$('#tabChats').remove();
			}
			else {
				fillSubjectProfessors(res.professors);
			}
		}
	});
}
function fillSubjectProfessors (data) {
	if (data.length>0) {
		$('#listProfessors').html(
			'<div class="col-md-3">'+
				'<div class="mt-widget-1 mt-special bg-green-jungle margin-bottom-20">'+
					'<div class="mt-img">'+
						'<img src="'+subjectDetails.image+'"> </div>'+
					'<div class="mt-body">'+
						'<h3 class="mt-username font-white">Public Shoutout</h3>'+
						'<div class="mt-stats">'+
							'<div class="btn-group btn-group-justified">'+
								'<a href="javascript:;" data-professor="0" class="btn btn-block yellow font-white chat-initiate">'+
									'<i class="icon-bubbles"></i> Start Chat </a>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>');
		for (var i = 0; i < data.length; i++) {
			$('#listProfessors').append(
				'<div class="col-md-3">'+
					'<div class="mt-widget-1 margin-bottom-20">'+
						'<div class="mt-img">'+
							'<img src="'+data[i].profilePic+'"> </div>'+
						'<div class="mt-body">'+
							'<h3 class="mt-username">'+data[i].firstName+' '+data[i].lastName+'</h3>'+
							'<div class="mt-stats">'+
								'<div class="btn-group btn-group-justified">'+
									'<a href="javascript:;" data-professor="'+data[i].professorId+'" class="btn btn-block font-blue chat-initiate">'+
										'<i class="icon-bubbles"></i> Start Chat </a>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>');
		};
	} else {
		$('a[href="#tabChats"]').parent().remove();
		$('#tabChats').remove();
	}
}
function fetchUserRating() {
	var req = {};
	var res;
	req.action = 'fetch-user-rating';
	req.subjectId = subjectId;
	req.courseId = courseId;
	$.ajax({
		type: "post",
		url: ApiEndPoint,
		data: JSON.stringify(req) 
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else {
			rating = res;
			initRatingSystem(res);
		}
	});
}
function initRatingSystem(data) {

	//rating initilisation
	$('#rating').rating({
		extendSymbol: function (rate) {
			title = '';
			switch(rate) {
				case 1:
					title = 'Very Bad';
					break;
				case 2:
					title = 'Bad';
					break;
				case 3:
					title = 'OK';
					break;
				case 4:
					title = 'Good';
					break;
				case 5:
					title = 'Very Good';
					break;
			}
			$(this).tooltip({
				placement: 'bottom',
				title: title
			});
		}
	});

	//event listener for submit review button
	$('#submitReview').on('click', function() {
		if($('#titleReview').val() != '' && $('#titleReview').val().length > 200) {
			toastr.error('Please input a smaller title');
			return;
		}
		if($('#rating').val() <= 0) {
			toastr.error('Please select a rate');
			return;
		}

		//now sending ajax to save rating
		var req = {};
		var res;
		req.action = 'save-rating';
		req.rating = $('#rating').val();
		req.title = $('#titleReview').val();
		req.review = $('#review').val();
		req.courseId = courseId;
		req.subjectId = subjectId;
		req.reviewId = $('#reviewId').val();
		$.ajax({
			type: "post",
			url: ApiEndPoint,
			data: JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			$('#reviewModal').modal('hide');
			if(res.status == 1)
				location.reload();
			else
				toastr.error(res.message);
		});
	});

	//modal close event listener
	$('#reviewModal').on('hidden.bs.modal', function() {
		fillRating(rating);
	});

	//remove review event listener
	$('#removeReview').on('click', function() {
		if(confirm('Are you sure you want to remove your Rating and Review?')) {
			var req = {};
			var res;
			req.action = 'remove-review';
			req.reviewId = $('#reviewId').val();
			$.ajax({
				type: "post",
				url: ApiEndPoint,
				data: JSON.stringify(req)   
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else
					location.reload();
			});
		}
	});

	fillRating(data);
}
function fillRating(data) {
	if(data.rating == 0) {
		$('#reviewButton').text('Write Review').removeClass('hide');
		//$('#rating').rating('rate', 0);
		$("#rating").rating('update', 0);
		$("#displayRating").rating({displayOnly: true});
		$("#displayRating").rating('update', 0);
		$(".show-rating").removeClass('hide');
		$('#titleReview').val('');
		$('#review').val('');
		$('#reviewId').val(0);
		$('#removeReview').addClass('hide');
	}
	else {
		$('#reviewButton').text('Edit Review').removeClass('hide');
		//$('#rating').rating('rate', data.rating.rating);
		$("#rating").rating('update', data.rating.rating);
		$("#displayRating").rating({displayOnly: true});
		$("#displayRating").rating('update', data.rating.rating);
		$(".show-rating").removeClass('hide');
		$('#titleReview').val(data.rating.title);
		$('#review').val(data.rating.review);
		$('#reviewId').val(data.rating.id);
		$('#removeReview').removeClass('hide');
	}
}
$('#listProfessors').on("click", "a", function(){
	var username = $(this).closest('.mt-body').find('.mt-username').text();
	var req = {};
	var res;
	req.action = 'get-student-chat-link';
	req.courseId = courseId;
	req.subjectId = subjectId;
	req.professorId = $(this).attr("data-professor");
	$.ajax({
		'type': 'post',
		'url': ApiEndPoint,
		'data': JSON.stringify(req)
	}).done(function (res) {
		res = $.parseJSON(res);
		if (res.status == 0)
			toastr.error(res.message);
		else {
			$('#confirmChatModal .person-name').text(username);
			$('#confirmChatModal .chat-link').attr("href", sitePath+'messenger/'+res.roomId);
			$('#confirmChatModal').modal("show");
			//window.open('../messenger/'+res.roomId);
		}
	});
});
$('#tabExams').on("click", ".js-check-attempts", function(){
	var examId = $(this).attr("data-examid");
	$.each(subjectiveExams, function (i, exam) {
		if (examId==exam.id) {
			$('#subjectiveCheckedModal .modal-title').html(exam.exam);
			if (exam.listAttempts.length>0) {
				$('#tblSubjectiveChecked').html('<tr><th>Attempts</th><th>Start Date</th><th>End Date</th><th>Checked</th></tr>');
				$.each(exam.listAttempts, function (j, attempt) {
					$('#tblSubjectiveChecked').append(
						'<tr>'+
							'<td>Attempt '+(j+1)+'</td>'+
							'<td>'+formatTime(attempt.startDate)+'</td>'+
							'<td>'+((attempt.endDate==0)?'-':formatTime(attempt.endDate))+'</td>'+
							'<td>'+((attempt.checked==1)?('<a href="'+sitePathStudent+'subjective-exams/'+examId+'/result/attempt/'+attempt.attemptId+'/">See Result</a>'):('Not Checked'))+'</td>'+
						'</tr>');
				});
				$('#subjectiveCheckedModal').modal("show");
			}
		};
	});
});
$('#tabExams').on("click", ".js-check-submission-attempts", function(){
	var examId = $(this).attr("data-examid");
	$.each(submissions, function (i, exam) {
		if (examId==exam.id) {
			$('#subjectiveCheckedModal .modal-title').html(exam.exam);
			if (exam.listAttempts.length>0) {
				$('#tblSubjectiveChecked').html('<tr><th>Attempts</th><th>Start Date</th><th>End Date</th><th>Checked</th></tr>');
				$.each(exam.listAttempts, function (j, attempt) {
					$('#tblSubjectiveChecked').append(
						'<tr>'+
							'<td>Attempt '+(j+1)+'</td>'+
							'<td>'+formatTime(attempt.startDate)+'</td>'+
							'<td>'+((attempt.endDate==0)?'-':formatTime(attempt.endDate))+'</td>'+
							'<td>'+((attempt.checked==1)?('<a href="'+sitePathStudent+'submissions/'+examId+'/result/attempt/'+attempt.attemptId+'/">See Result</a>'):('Not Checked'))+'</td>'+
						'</tr>');
				});
				$('#subjectiveCheckedModal').modal("show");
			}
		};
	});
});
var AppCalendar = function() {

	return {
		//main function to initiate the module
		init: function() {
			this.initCalendar();
		},

		initCalendar: function() {

			if (!jQuery().fullCalendar) {
				return;
			}

			var date = new Date();
			var d = date.getDate();
			var m = date.getMonth();
			var y = date.getFullYear();

			var h = {};
			
			if ($('#subjectBox').width() <= 720) {
				$('#calendar').addClass("mobile");
				h = {
					left: 'title, prev, next',
					center: '',
					right: 'today,month,agendaWeek,agendaDay'
				};
			} else {
				$('#calendar').removeClass("mobile");
				h = {
					left: 'title',
					center: '',
					right: 'prev,next,today,month,agendaWeek,agendaDay'
				};
			}

			var initDrag = function(el) {
				// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
				// it doesn't need to have a start or end
				var eventObject = {
					title: $.trim(el.text()) // use the element's text as the event title
				};
				// store the Event Object in the DOM element so we can get to it later
				el.data('eventObject', eventObject);
				// make the event draggable using jQuery UI
				el.draggable({
					zIndex: 999,
					revert: true, // will cause the event to go back to its
					revertDuration: 0 //  original position after the drag
				});
			};

			$('#external-events div.external-event').each(function() {
			    initDrag($(this));
			});

			//predefined events
			$('#event_box').html("");

			$('#calendar').fullCalendar('destroy'); // destroy the calendar
			$('#calendar').fullCalendar({ //re-initialize the calendar
				header: h,
				defaultView: 'month', // change default view with available options from http://arshaw.com/fullcalendar/docs/views/Available_Views/ 
				slotMinutes: 15,
				editable: true,
				droppable: true, // this allows things to be dropped onto the calendar !!!
				drop: function(date, allDay) { // this function is called when something is dropped

					// retrieve the dropped element's stored Event Object
					var originalEventObject = $(this).data('eventObject');
					// we need to copy it, so that multiple events don't have a reference to the same object
					var copiedEventObject = $.extend({}, originalEventObject);

					// assign it the date that was reported
					copiedEventObject.start = date;
					copiedEventObject.allDay = allDay;
					copiedEventObject.className = $(this).attr("data-class");

					// render the event on the calendar
					// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
					$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

					// is the "remove after drop" checkbox checked?
					if ($('#drop-remove').is(':checked')) {
						// if so, remove the element from the "Draggable Events" list
						$(this).remove();
					}
				},
				events: []
			});

		}

	};

}();
function getPercentage(tagData,examType, ranges) {
	if (tagData.length>0) {
		// last 3 attempts
		var limit = tagData.length-3;
		if (limit<0) {
			limit = 0;
		}
		var strAttempts = '';
		if (examType == 'exam') {
			var correctCount = 0;
			var appearCount = 0;
			var percentage = 0;
			for (var i = 0; i < tagData.length; i++) {
				if (limit == i) {
					strAttempts += parseInt(i+1)+',';
					correctCount+= parseInt(tagData[i].correct_count);
					appearCount += parseInt(tagData[i].appear_count);
					limit++;
				}
			}
			if (appearCount>0) {
				percentage = round2(parseFloat(correctCount/appearCount)*100)+'%';
				if (ranges) {
					for (var i = 0; i < ranges.length; i++) {
						if (percentCheck(percentage,ranges[i].percentLow,ranges[i].percentHigh)) {
							percentage+= ' <img src="'+sitepathStudentIncludes+'assets/custom/img/smileys/'+ranges[i].file+'" class="img-smiley" alt="" />';
						}
						
					}
				}
			}
		} else {
			var correctScore = 0;
			var maxScore = 0;
			var percentage = 0;
			for (var i = 0; i < tagData.length; i++) {
				if (limit == i) {
					strAttempts += parseInt(i+1)+',';
					correctScore+= parseFloat(tagData[i].score);
					maxScore += parseFloat(tagData[i].maxScore);
					limit++;
				}
			}
			//console.log(correctScore+ ' : ' +maxScore);
			if (maxScore>0) {
				percentage = round2(parseFloat(correctScore/maxScore)*100)+'%';
				if (ranges) {
					for (var i = 0; i < ranges.length; i++) {
						if (percentCheck(percentage,ranges[i].percentLow,ranges[i].percentHigh)) {
							percentage+= ' <img src="'+sitepathStudentIncludes+'assets/custom/img/smileys/'+ranges[i].file+'" class="img-smiley" alt="" />';
						}
						
					}
				}
			}
		}
	} else {
		percentage = 0;
	}
	return percentage;
}

function percentCheck(percentage,percentLow,percentHigh) {
	if (percentage>=percentLow && percentage<percentHigh && percentHigh!=100) {
		return true;
	} else if (percentage>=percentLow && percentage<=percentHigh && percentHigh==100) {
		return true;
	}
	return false;
}

jQuery(document).ready(function() {
	$('#tabLearnObj').on('click','.btn-tag-detail',function(){
		var tagRow = $(this).closest('tr');
		var tagName = tagRow.find('.tag-name').text();
		var tagId = tagRow.attr('data-tag');
		var examId = tagRow.attr('data-exam');
		if (tagId != undefined) {
			var req = {};
			var res;
			req.action = 'tag-exam-analysis';
			req.tagId = tagId;
			req.examId = examId;
			req.examType = tagRow.attr('data-exam-type');
			ajaxRequest({
				'type': 'post',
				'url': ApiEndPoint,
				'data': JSON.stringify(req),
				success:function(res) {
					res = $.parseJSON(res);
					
					if (res.status == 0)
						toastr.error(res.message);
					else {
						//console.log(res);
						$('#tagName').text(tagName);
						$('#tableTagAnalysis tbody').html('');
						$('#tagsModal').modal('show');
						if (res.tagData.length>0) {
							var tagData = res.tagData;
							// last 3 attempts
							var limit = tagData.length-3;
							if (limit<0) {
								limit = 0;
							}
							var strAttempts = '';
							if (req.examType == 'exam') {
								var correctCount = 0;
								var appearCount = 0;
								var percentage = 0;
								$('#tableTagAnalysis tbody').append(
									'<tr>'+
										'<td></td>'+
										'<td>(correctly solved)</td>'+
										'<td>(appeared in exam)</td>'+
									'</tr>');
								for (var i = 0; i < tagData.length; i++) {
									$('#tableTagAnalysis tbody').append(
										'<tr>'+
											'<td>Attempt '+(i+1)+'</td>'+
											'<td>'+tagData[i].correct_count+'</td>'+
											'<td>'+tagData[i].appear_count+'</td>'+
										'</tr>');
									if (limit == i) {
										strAttempts += parseInt(i+1)+',';
										correctCount+= parseInt(tagData[i].correct_count);
										appearCount += parseInt(tagData[i].appear_count);
										limit++;
									}
								}
								if (appearCount>0) {
									percentage = round2(parseFloat(correctCount/appearCount)*100);
									$('#tableTagAnalysis tbody').append(
										'<tr>'+
											'<td>Sum of attempts '+strAttempts.substr(0,strAttempts.length-1)+'</td>'+
											'<td>'+correctCount+'</td>'+
											'<td>'+appearCount+'</td>'+
										'</tr>'+
										'<tr class="info">'+
											'<td colspan="3">Percentage = '+percentage+'%</td>'+
										'</tr>');
									if (res.ranges) {
										var ranges = res.ranges;
										for (var i = 0; i < ranges.length; i++) {
											if (percentCheck(percentage,ranges[i].percentLow,ranges[i].percentHigh)) {
												$('#tableTagAnalysis tbody').append(
													'<tr class="success">'+
														'<td colspan="3">Instructor Comment = '+ranges[i].comment+' <img src="'+sitepathStudentIncludes+'assets/custom/img/smileys/'+ranges[i].file+'" class="img-smiley" alt="" /></td>'+
													'</tr>');
											}
											
										}
									}
								}
							} else {
								var correctScore = 0;
								var maxScore = 0;
								var percentage = 0;
								$('#tableTagAnalysis tbody').append(
									'<tr>'+
										'<td></td>'+
										'<td>(score received)</td>'+
										'<td>(total score)</td>'+
									'</tr>');
								for (var i = 0; i < tagData.length; i++) {
									$('#tableTagAnalysis tbody').append(
										'<tr>'+
											'<td>Attempt '+(i+1)+'</td>'+
											'<td>'+tagData[i].score+'</td>'+
											'<td>'+tagData[i].maxScore+'</td>'+
										'</tr>');
									if (limit == i) {
										strAttempts += parseInt(i+1)+',';
										correctScore+= parseFloat(tagData[i].score);
										maxScore += parseFloat(tagData[i].maxScore);
										limit++;
									}
								}
								if (maxScore>0) {
									percentage = round2(parseFloat(correctScore/maxScore)*100);
									$('#tableTagAnalysis tbody').append(
										'<tr>'+
											'<td>Sum of attempts '+strAttempts.substr(0,strAttempts.length-1)+'</td>'+
											'<td>'+correctScore+'</td>'+
											'<td>'+maxScore+'</td>'+
										'</tr>'+
										'<tr class="info">'+
											'<td colspan="3">Percentage = '+percentage+'%</td>'+
										'</tr>');
									if (res.ranges) {
										var ranges = res.ranges;
										for (var i = 0; i < ranges.length; i++) {
											if (percentCheck(percentage,ranges[i].percentLow,ranges[i].percentHigh)) {
												$('#tableTagAnalysis tbody').append(
													'<tr class="success">'+
														'<td colspan="3">Instructor Comment = '+ranges[i].comment+' <img src="'+sitepathStudentIncludes+'assets/custom/img/smileys/'+ranges[i].file+'" class="img-smiley" alt="" /></td>'+
													'</tr>');
											}
											
										}
									}
								}
							}
						} else {
							$('#tableTagAnalysis tbody').append(
									'<tr>'+
										'<td colspan="3">No data found</td>'+
									'</tr>');
						}
					}
				}
			});
		}
		/*$('#tagName').text(tagName);
		$('#tagsModal').modal('show');*/
	});
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		if($(e.target).attr('href') == '#tabCalendar')
			AppCalendar.init();
	});
	$('a[data-toggle="sub-tab"]').on('click', function (e) {
		e.preventDefault();
		$('a[data-toggle="tab"][href="'+$(this).attr('href')+'"]').trigger('click');
	});
});
</script>