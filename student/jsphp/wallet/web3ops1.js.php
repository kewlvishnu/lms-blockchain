<script type="text/javascript">
var web3network = '';
var currAddress = '<?php echo $global->walletAddress; ?>';
var IGROdecimals = parseInt(<?php echo $global->decimalPrecision; ?>);
var IGROBalanceDivisor = parseInt(<?php echo $global->balanceDivisor; ?>);
$(document).ready(function() {
	if (page == "home") {
		window.addEventListener('load', function() {
			// Checking if Web3 has been injected by the browser (Mist/MetaMask)
			if (typeof web3 !== 'undefined') {
				// Use Mist/MetaMask's provider
				window.web3 = new Web3(web3.currentProvider);
				startCryptoApp();
			} else {
				if (currAddress == "") {
					$("#modalMetamaskNotInstalled").modal("show");
				}
				//toastr.error('We have detected that you do not have MetaMask installed. Please install it to associate your crypto address with your account.');
				// fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
				//window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
				//window.web3 = new Web3(new Web3.providers.HttpProvider("https://proxy.mobilefish.com:9070"));
			}
			
		});
	}
	/*if (currAddress != '' && page == "home") {
		toastr.success('Your associated address is '+currAddress);
	}*/
	$("#btnMMAssociateAddress").click(function(event) {
		/* Act on the event */
		var address = $("#caAddress").html();
		saveAddress(address);
	});
	$("#btnMMChangeAddress").click(function(event) {
		/* Act on the event */
		var address = $("#ccAddress").html();
		saveAddress(address);
	});
});
function startCryptoApp(){
	showNetwork(0);
	monitorAccountChanges();
}
function checkCryptoApp(){
	if (typeof web3 !== 'undefined') {
		// Use Mist/MetaMask's provider
		window.web3 = new Web3(web3.currentProvider);
		showNetwork(1);
		monitorAccountChanges();
	} else {
		$("#modalMetamaskNotInstalled").modal("show");
	}
}
function showNetwork(flag) {
  web3.version.getNetwork((err, res) => {
    var output = "";

    if (!err) {
      if(res > 1000000000000) {
        output = "testrpc";
      } else {
        switch (res) {
          case "1":
            output = "mainnet";
            break
          case "2":
            output = "morden";
            break
          case "3":
            output = "ropsten";
            break
          case "4":
            output = "rinkeby";
            break
          default:
            output = "unknown network = "+res;
        }
      }
    } else {
      output = "Error";
    }
    web3network = output;
    showAccounts(flag);
  });
}
function showAccounts(flag) {
  if (web3network != 'rinkeby') {
  	//toastr.error('Please switch to main network on MetaMask!');
  	toastr.error('Please switch to rinkeby network on MetaMask!');
  } else {
	web3.eth.getAccounts((err, res) => {
		var output = "";

		if (!err) {
		  output = res.join("<br />");
		} else {
		  output = "Error";
		}
		if (output == '') {
			toastr.error('Please log into MetaMask!');
		} else if ((currAddress != output) || (currAddress == '')) {
			//alert(flag);
			if (currAddress == "") {
				callPopupAddress(output,0);
			} else {
				callPopupAddress(output,1);
			}
		}
	});
  }
}
function callPopupAddress(address,flag) {
	if (flag == 0) {
		$("#caAddress").html(address);
		$("#modalMetamaskAssociateAddress").modal("show");
	} else {
		$("#ccAddress").html(address);
		$("#modalMetamaskChangeAddress").modal("show");
	}
}
function saveAddress(address) {
	var req = {};
	var res;
	req.action = 'save-wallet-address';
	req.network = web3network;
	req.address = address;
	$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else {
			$("#modalMetamaskAssociateAddress").modal("hide");
			$("#modalMetamaskChangeAddress").modal("hide");
			toastr.success(res.message);
			window.location = window.location;
		}
	});
}
function saveTransaction(txnHash,IGROTxnID) {
	var req = {};
	var res;
	req.action = 'save-crypto-transaction';
	req.network = web3network;
	req.address = currAddress;
	req.txnHash = txnHash;
	req.IGROTxnID = IGROTxnID;
	$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else {
			toastr.success(res.message);
		}
	});
}
function monitorAccountChanges() {
  // Declare accountInterval here! Clear the variable if there is no Ethereum node found.
  let accountInterval;

  // Check if an Ethereum node is found.
  if(web3.isConnected()){

    // If a coinbase account is found, automatically update the fromAddress form field with the coinbase account
    getCoinbasePromise()
    .then(function(fromAddress){
      //document.getElementById('fromAddress').value = fromAddress;
    })
    .catch(function(err){
      toastr.error(err);
    });

    let account = $.trim(web3.eth.accounts[0]);

    // At a time interval of 1 sec monitor account changes
    accountInterval = setInterval(function() {

      // Monitor account changes. If you switch account, for example in MetaMask, it will detect this.
      // See: https://github.com/MetaMask/faq/blob/master/DEVELOPERS.md
      //console.log(web3.eth.accounts[0]);
      //console.log(account);
      if ($.trim(web3.eth.accounts[0]) !== account) {
        account = web3.eth.accounts[0];
        //document.getElementById('fromAddress').value = account;
        toastr.success("Account changed!");
        //window.location = window.location;
      }

    }, 1000); // end of accountInterval = setInterval(function()

  } else {
    // Stop the accountInterval
    clearInterval(accountInterval);
    toastr.error("No Ethereum node found");
  }
}
function createContract(){
  // Each time you modify the DemoContract.sol and deploy it on the blockchain, you need to get the abi value.
  // Paste the abi value in web3.eth.contract(PASTE_ABI_VALUE);
  // When the contract is deployed, do not forget to change the contract address, see
  // formfield id 'contractAddress'
  // Replace contract address: 0xf1d2e0b8e09f4dda7f3fd6db26496f74079faeeb with your own.
  //
  const contractSpec = web3.eth.contract(<?php echo $global->contractCode; ?>);

  return contractSpec.at($("#contractAddress").val());
}
// ===================================================
// Promises
// ===================================================
const getCoinbasePromise = function(){
  return new Promise(function(resolve, reject){
    web3.eth.getCoinbase(function(err, res){
      if (!res) {
        reject("No accounts found");
      } else {
        resolve(res);
      }
    });
  });
}
const checkAddressPromise = function(address, addressType) {
  return new Promise(function(resolve, reject){
    if (address != null && web3.isAddress(address)) {
      resolve();
    } else {
      reject(addressType);
    }
  });
}
function balanceOf(balAddress) {
  if (!$.isNumeric(balAddress) || balAddress=='') {
    toastr.error("The address is invalid, please enter a valid address");
  } else {
    // Check if Ethereum node is found
    if(web3.isConnected()){
      const contractAddress = $("#contractAddress").val();
      checkAddressPromise(contractAddress, "contract address").then(function(){
        return checkAddressPromise(balAddress, "to account")
      }).then(function(){
        
        const contract = createContract();
        contract.balanceOf(balAddress, function (err, res) {
          if(err) {
          	return "error 1";
          } else {
          	return res.c;
          }
        });

      }).catch(function(message){
        return "error 2";
      });
    } else {
    	return "error 3";
    }
  }
}
function transferIGRO(tokens,orderId) {
  var toAddress = $("#ownerAddress").val();
  if (!$.isNumeric(toAddress) || toAddress=='') {
    toastr.error("The address is invalid, please enter a valid address");
  } else {
    // Check if Ethereum node is found
    if(web3.isConnected()){
      const contractAddress = $("#contractAddress").val();
      //const fromAddress = $("#ownerAddress").val();
      checkAddressPromise(contractAddress, "contract address").then(function(){
        return checkAddressPromise(toAddress, "to account")
      }).then(function(){
        
        const contract = createContract();
        contract.balanceOf(currAddress, function (err, res) {
          if(err) {
          	toastr.error(err);
          } else {
      			var balance = res.c[0]/IGROBalanceDivisor;
      			if (balance>tokens) {
      				contract.transfer(toAddress, tokens * 10**IGROdecimals, function (err, res) {
      					if(res) {
      						//toastr.success("Transaction successful. Transaction Hash: " + res);
      						saveTransaction(res,orderId);
                  $("#modalPurchaseNowCourseKeys").modal("hide");
      					} else {
      						//toastr.error("showResult: "+err);
      						toastr.error("Transaction Failed!");
      					}
      				});
      			} else {
      				toastr.error("Your don't have sufficient balance!");
      			}
          }
        });
        /*contract.transfer(toAddress, tokens * 10**IGROdecimals, function (err, res) {
          if(res) {
		    toastr.success("Transaction successful. Transaction Hash: " + res);
		  } else {
		    //toastr.error("showResult: "+err);
		    toastr.error("Transaction Failed!");
		  }
        });*/

      }).catch(function(message){
        toastr.error("Not a valid "+message+".");
      });
    }
  }
}
</script>