<script type="text/javascript">
var web3network = '';
var currAddress = '<?php echo $global->walletAddress; ?>';
var IGROdecimals = parseInt(<?php echo $global->decimalPrecision; ?>);
var IGROBalanceDivisor = parseInt(<?php echo $global->balanceDivisor; ?>);
$(document).ready(function() {
  $("#btnMMAssociateAddress").click(function(event) {
    /* Act on the event */
    var address = $("#caAddress").text();
    saveAddress(address);
  });
  $("#btnMMChangeAddress").click(function(event) {
    /* Act on the event */
    var address = $("#ccAddress").text();
    saveAddress(address);
  });
  $("#btnWalletSetAddress").click(function(event) {
    /* Act on the event */
    setAddress();
  });
});
function initWeb3() {
  if (typeof web3 !== 'undefined') {
    //console.log(web3.currentProvider);
    // Use Mist/MetaMask's provider
    setMM();
    async function setMM() {
      if (window.ethereum) {
          window.web3 = new Web3(ethereum);
          try {
              // Request account access if needed
              await ethereum.enable();
              // Acccounts now exposed
              web3network = getETHNetwork();
              // doTransaction(tokens,orderId);
          } catch (error) {
              // User denied account access...
              toastr.error("User denied account access");
          }
      }// Legacy dapp browsers...
      else if (window.web3) {
          window.web3 = new Web3(web3.currentProvider);
          // Acccounts always exposed
          web3network = getETHNetwork();
          // doTransaction(tokens,orderId);
      }
      // Non-dapp browsers...
      else {
          toastr.error("Non-Ethereum browser detected. You should consider trying MetaMask!");
          console.log('Non-Ethereum browser detected. You should consider trying MetaMask!');
      }
    };
  } else {
    $(".modal").modal("hide");
    $("#modalMetamaskNotInstalled").modal("show");
  }
}
function getETHNetwork() {
  var output = "";
  web3.eth.net.getNetworkType((err, res) => {

    if (!err) {
      if(res > 1000000000000) {
        output = "testrpc";
      } else {
        //toastr.info(res);
        output = res;
      }
    } else {
      output = "Error";
    }
    web3network = output;
    return output;
  });
  //toastr.info(output);
  web3network = output;
  return output;
}
function setAddress() {
  // body...
  web3.eth.getAccounts(function(err, res){
    if (res.length>0) {
      var account = res[0];
      checkAddressPromise(account, "Your address").then(function(){
        $(".modal").modal("hide");
        $("#caAddress").html(account);
        $("#modalMetamaskAssociateAddress").modal("show");
      });
    } else {
      toastr.error("Please login into MetaMask!");
    }
  });
}
function saveAddress(address) {
  var req = {};
  var res;
  req.action = 'save-wallet-address';
  req.network = web3network;
  req.address = address;
  req.blockchain = 'm';
  $.ajax({
      'type'  : 'post',
      'url' : ApiEndpoint,
      'data'  : JSON.stringify(req)
  }).done(function(res) {
    res = $.parseJSON(res);
    if(res.status == 0)
      toastr.error(res.message);
    else {
      $("#modalMetamaskAssociateAddress").modal("hide");
      $("#modalMetamaskChangeAddress").modal("hide");
      toastr.success(res.message);
      window.location = window.location;
    }
  });
}
function transferIGRO(tokens,orderId) {
  doTransaction(tokens,orderId);
}
function doTransaction(tokens,orderId) {
  var toAddress = $("#ownerAddress").val();
  //console.log(web3.eth.accounts);
  if (!$.isNumeric(currAddress) || currAddress=='') {
    //toastr.error("Your address is not set, please set your address");
    setAddress();
  } else if (!$.isNumeric(toAddress) || toAddress=='') {
    toastr.error("The address is invalid, please enter a valid address");
  } else {
    // Check if Ethereum node is found
    if(web3.isConnected()){
      const contractAddress = $("#contractAddress").val();
      //const fromAddress = $("#ownerAddress").val();
      checkAddressPromise(contractAddress, "Contract address").then(function(){
        return checkAddressPromise(toAddress, "To account")
      }).then(function(){
        web3.eth.getAccounts(function(err, res){
          if (res.length>0) {
            var blockchainAddress = res[0];
            checkAddressPromise(blockchainAddress, "Your address").then(function(){
              if (blockchainAddress != currAddress) {
                toastr.error("The address you saved and the one you are using are different! Please use your saved wallet.");
              } else {
                const contract = createContract();
                contract.balanceOf(currAddress, function (err, res) {
                  if(err) {
                    toastr.error(err);
                  } else {
                    var balance = res.c[0]/IGROBalanceDivisor;
                    //toastr.info('Balance: '+balance);

                    // set the default account
                    web3.eth.defaultAccount = currAddress;
                    var defaultAccount = web3.eth.defaultAccount;
                    //console.log(defaultAccount); // ''
                    //web3.eth.defaultAccount=$.trim(web3.eth.accounts[0]);
                    if (balance>tokens) {
                      contract.transfer(toAddress, tokens * 10**IGROdecimals, function (err, res) {
                        if(res) {
                          //toastr.success("Transaction successful. Transaction Hash: " + res);
                          saveTransaction(res,orderId);
                          $("#modalPurchaseNowCourseKeys").modal("hide");
                        } else {
                          //toastr.error("showResult: "+err);
                          toastr.error("Transaction Failed!");
                        }
                      });
                    } else {
                      toastr.error("Your don't have sufficient balance!");
                    }
                  }
                });
              }
            });
          } else {
            toastr.error("Please login into MetaMask!");
          }
        });

      }).catch(function(message){
        toastr.error("Not a valid "+message+".");
      });
    }
  }
}
function saveTransaction(txnHash,IGROTxnID) {
  var req = {};
  var res;
  req.action = 'save-crypto-transaction';
  req.network = web3network;
  req.address = currAddress;
  req.txnHash = txnHash;
  req.IGROTxnID = IGROTxnID;
  $.ajax({
      'type'  : 'post',
      'url' : ApiEndpoint,
      'data'  : JSON.stringify(req)
  }).done(function(res) {
    res = $.parseJSON(res);
    if(res.status == 0)
      toastr.error(res.message);
    else {
      toastr.success(res.message,'',
      {
        "timeOut": "0",
        "extendedTimeOut": "0",
      });
      if (page == 'course' || page == 'package') {
        $("#purchaseNowModal").modal("hide");
      }
    }
  });
}
function createContract(){
  // Each time you modify the DemoContract.sol and deploy it on the blockchain, you need to get the abi value.
  // Paste the abi value in web3.eth.contract(PASTE_ABI_VALUE);
  // When the contract is deployed, do not forget to change the contract address, see
  // formfield id 'contractAddress'
  // Replace contract address: 0xf1d2e0b8e09f4dda7f3fd6db26496f74079faeeb with your own.
  //
  const contractSpec = web3.eth.contract(<?php echo $global->contractCode; ?>);

  return contractSpec.at($("#contractAddress").val());
}
const checkAddressPromise = function(address, addressType) {
  return new Promise(function(resolve, reject){
    if (address != null && web3.utils.isAddress(address)) {
      resolve();
    } else {
      //reject(addressType);
      toastr.error(addressType+" is invalid");
    }
  });
}

function createProfileContract(){
  // Each time you modify the DemoContract.sol and deploy it on the blockchain, you need to get the abi value.
  // Paste the abi value in web3.eth.contract(PASTE_ABI_VALUE);
  // When the contract is deployed, do not forget to change the contract address, see
  // formfield id 'contractAddress'
  // Replace contract address: 0xf1d2e0b8e09f4dda7f3fd6db26496f74079faeeb with your own.
  //
  const contractSpec = web3.eth.contract([
    {
      "constant": false,
      "inputs": [
        {
          "name": "_interests",
          "type": "string"
        }
      ],
      "name": "addInterests",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_firstname",
          "type": "string"
        },
        {
          "name": "_lastname",
          "type": "string"
        }
      ],
      "name": "registerStudent",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_info",
          "type": "string"
        }
      ],
      "name": "addInfo",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_info",
          "type": "string"
        },
        {
          "name": "_interests",
          "type": "string"
        },
        {
          "name": "_achievements",
          "type": "string"
        }
      ],
      "name": "addInfoInterestsAchievements",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_interests",
          "type": "string"
        },
        {
          "name": "_achievements",
          "type": "string"
        }
      ],
      "name": "addInterestsAchievements",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_achievements",
          "type": "string"
        }
      ],
      "name": "addAchievements",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_info",
          "type": "string"
        },
        {
          "name": "_interests",
          "type": "string"
        }
      ],
      "name": "addInfoInterests",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_country",
          "type": "string"
        },
        {
          "name": "_info",
          "type": "string"
        },
        {
          "name": "_interests",
          "type": "string"
        }
      ],
      "name": "addCountryInfoInterests",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "checkRegistered",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_achievements",
          "type": "string"
        },
        {
          "name": "_country",
          "type": "string"
        },
        {
          "name": "_info",
          "type": "string"
        }
      ],
      "name": "addAchievementsCountryInfo",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_achievements",
          "type": "string"
        },
        {
          "name": "_country",
          "type": "string"
        }
      ],
      "name": "addAchievementsCountry",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_interests",
          "type": "string"
        },
        {
          "name": "_achievements",
          "type": "string"
        },
        {
          "name": "_country",
          "type": "string"
        }
      ],
      "name": "addInterestsAchievementsCountry",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "",
          "type": "address"
        }
      ],
      "name": "students",
      "outputs": [
        {
          "name": "owner",
          "type": "address"
        },
        {
          "name": "firstName",
          "type": "string"
        },
        {
          "name": "lastName",
          "type": "string"
        },
        {
          "name": "country",
          "type": "string"
        },
        {
          "name": "info",
          "type": "string"
        },
        {
          "name": "interests",
          "type": "string"
        },
        {
          "name": "achievements",
          "type": "string"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_country",
          "type": "string"
        }
      ],
      "name": "addCountry",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_country",
          "type": "string"
        },
        {
          "name": "_interests",
          "type": "string"
        }
      ],
      "name": "addCountryInterests",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_firstname",
          "type": "string"
        },
        {
          "name": "_lastname",
          "type": "string"
        },
        {
          "name": "_country",
          "type": "string"
        },
        {
          "name": "_info",
          "type": "string"
        },
        {
          "name": "_interests",
          "type": "string"
        },
        {
          "name": "_achievements",
          "type": "string"
        }
      ],
      "name": "registerStudentComplete",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_country",
          "type": "string"
        },
        {
          "name": "_info",
          "type": "string"
        }
      ],
      "name": "addCountryInfo",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_country",
          "type": "string"
        },
        {
          "name": "_info",
          "type": "string"
        },
        {
          "name": "_interests",
          "type": "string"
        },
        {
          "name": "_achievements",
          "type": "string"
        }
      ],
      "name": "addCountryInfoInterestsAchievements",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "payable": true,
      "stateMutability": "payable",
      "type": "fallback"
    }
  ]);

  return contractSpec.at($("#rsContractAddress").val());
}
function registerStudentOnBlockchain(_firstname,_lastname,_country, _info, _interests, _achievements, fnBlockchain) {
  //if (typeof web3 !== 'undefined') {
  var toAddress = $("#ownerAddress").val();
  //console.log(web3.eth.accounts);
  if (!$.isNumeric(currAddress) || currAddress=='') {
    //toastr.error("Your address is not set, please set your address");
    setAddress();
  } else if (!$.isNumeric(toAddress) || toAddress=='') {
    toastr.error("The address is invalid, please enter a valid address");
  } else {
    // Check if Ethereum node is found
    if(web3.isConnected()){
      const contractAddress = $("#rsContractAddress").val();
      //const fromAddress = $("#ownerAddress").val();
      checkAddressPromise(contractAddress, "Contract address").then(function(){
        return checkAddressPromise(toAddress, "To account")
      }).then(function(){
        web3.eth.getAccounts(function(err, res){
          if (res.length>0) {
            var blockchainAddress = res[0];
            web3.eth.defaultAccount = currAddress;
            checkAddressPromise(blockchainAddress, "Your address").then(function(){
              if (blockchainAddress != currAddress) {
                toastr.error("The address you saved and the one you are using are different! Please use your saved wallet.");
              } else {
                const contract = createProfileContract();
                contract.checkRegistered(function (err, res) {
                  if(err) {
                    toastr.error("Error: " + err);
                  } else {
                    console.log(subPage);
                    if (!res) {
                      //toastr.info("Not Registered"+res);
                      contract.registerStudentComplete(_firstname, _lastname, _country, _info, _interests, _achievements, function (err, res) {
                        if(res) {
                          //toastr.success("Transaction successful. Transaction Hash: " + res);
                          toastr.success("Transaction successful. <a href=\"https://rinkeby.etherscan.io/tx/"+res+"\" target=\"_blank\"><strong><u>Track Transaction</u></strong></a>");
                          if (subPage == 'profileEdit') {
                            saveProfile();
                          }
                        } else {
                          //toastr.error("showResult: "+err);
                          toastr.error("Transaction Failed!");
                        }
                      });
                    } else {
                      //toastr.info("fnBlockchain: "+fnBlockchain);
                      switch(fnBlockchain) {
                        case 'addCountry':
                              contract.addCountry(_country, function (err, res) {
                                if(res) {
                                  toastr.success("Transaction successful. <a href=\"https://rinkeby.etherscan.io/tx/"+res+"\" target=\"_blank\"><strong><u>Track Transaction</u></strong></a>");
                                  if (subPage == 'profileEdit') {
                                    saveProfile();
                                  }
                                } else {
                                  //toastr.error("showResult: "+err);
                                  toastr.error("Transaction Failed!");
                                }
                              });
                              break;
                        case 'addInfo':
                              contract.addInfo(_info, function (err, res) {
                                if(res) {
                                  toastr.success("Transaction successful. <a href=\"https://rinkeby.etherscan.io/tx/"+res+"\" target=\"_blank\"><strong><u>Track Transaction</u></strong></a>");
                                  if (subPage == 'profileEdit') {
                                    saveProfile();
                                  }
                                } else {
                                  //toastr.error("showResult: "+err);
                                  toastr.error("Transaction Failed!");
                                }
                              });
                              break;
                        case 'addInterests':
                              contract.addInterests(_interests, function (err, res) {
                                if(res) {
                                  toastr.success("Transaction successful. <a href=\"https://rinkeby.etherscan.io/tx/"+res+"\" target=\"_blank\"><strong><u>Track Transaction</u></strong></a>");
                                  if (subPage == 'profileEdit') {
                                    saveProfile();
                                  }
                                } else {
                                  //toastr.error("showResult: "+err);
                                  toastr.error("Transaction Failed!");
                                }
                              });
                              break;
                        case 'addAchievements':
                              contract.addAchievements(_achievements, function (err, res) {
                                if(res) {
                                  toastr.success("Transaction successful. <a href=\"https://rinkeby.etherscan.io/tx/"+res+"\" target=\"_blank\"><strong><u>Track Transaction</u></strong></a>");
                                  if (subPage == 'profileEdit') {
                                    saveProfile();
                                  }
                                } else {
                                  //toastr.error("showResult: "+err);
                                  toastr.error("Transaction Failed!");
                                }
                              });
                              break;
                        case 'addCountryInfo':
                              contract.addCountryInfo(_country, _info, function (err, res) {
                                if(res) {
                                  toastr.success("Transaction successful. <a href=\"https://rinkeby.etherscan.io/tx/"+res+"\" target=\"_blank\"><strong><u>Track Transaction</u></strong></a>");
                                  if (subPage == 'profileEdit') {
                                    saveProfile();
                                  }
                                } else {
                                  //toastr.error("showResult: "+err);
                                  toastr.error("Transaction Failed!");
                                }
                              });
                              break;
                        case 'addInfoInterests':
                              contract.addInfoInterests(_info, _interests, function (err, res) {
                                if(res) {
                                  toastr.success("Transaction successful. <a href=\"https://rinkeby.etherscan.io/tx/"+res+"\" target=\"_blank\"><strong><u>Track Transaction</u></strong></a>");
                                  if (subPage == 'profileEdit') {
                                    saveProfile();
                                  }
                                } else {
                                  //toastr.error("showResult: "+err);
                                  toastr.error("Transaction Failed!");
                                }
                              });
                              break;
                        case 'addInterestsAchievements':
                              contract.addInterestsAchievements(_interests, _achievements, function (err, res) {
                                if(res) {
                                  toastr.success("Transaction successful. <a href=\"https://rinkeby.etherscan.io/tx/"+res+"\" target=\"_blank\"><strong><u>Track Transaction</u></strong></a>");
                                  if (subPage == 'profileEdit') {
                                    saveProfile();
                                  }
                                } else {
                                  //toastr.error("showResult: "+err);
                                  toastr.error("Transaction Failed!");
                                }
                              });
                              break;
                        case 'addAchievementsCountry':
                              contract.addAchievementsCountry(_achievements, _country, function (err, res) {
                                if(res) {
                                  toastr.success("Transaction successful. <a href=\"https://rinkeby.etherscan.io/tx/"+res+"\" target=\"_blank\"><strong><u>Track Transaction</u></strong></a>");
                                  if (subPage == 'profileEdit') {
                                    saveProfile();
                                  }
                                } else {
                                  //toastr.error("showResult: "+err);
                                  toastr.error("Transaction Failed!");
                                }
                              });
                              break;
                        case 'addCountryInterests':
                              contract.addCountryInterests(_country, _interests, function (err, res) {
                                if(res) {
                                  toastr.success("Transaction successful. <a href=\"https://rinkeby.etherscan.io/tx/"+res+"\" target=\"_blank\"><strong><u>Track Transaction</u></strong></a>");
                                  if (subPage == 'profileEdit') {
                                    saveProfile();
                                  }
                                } else {
                                  //toastr.error("showResult: "+err);
                                  toastr.error("Transaction Failed!");
                                }
                              });
                              break;
                        case 'addCountryInfoInterests':
                              contract.addCountryInfoInterests(_country, _info, _interests, function (err, res) {
                                if(res) {
                                  toastr.success("Transaction successful. <a href=\"https://rinkeby.etherscan.io/tx/"+res+"\" target=\"_blank\"><strong><u>Track Transaction</u></strong></a>");
                                  if (subPage == 'profileEdit') {
                                    saveProfile();
                                  }
                                } else {
                                  //toastr.error("showResult: "+err);
                                  toastr.error("Transaction Failed!");
                                }
                              });
                              break;
                        case 'addInfoInterestsAchievements':
                              contract.addInfoInterestsAchievements(_info, _interests, _achievements, function (err, res) {
                                if(res) {
                                  toastr.success("Transaction successful. <a href=\"https://rinkeby.etherscan.io/tx/"+res+"\" target=\"_blank\"><strong><u>Track Transaction</u></strong></a>");
                                  if (subPage == 'profileEdit') {
                                    saveProfile();
                                  }
                                } else {
                                  //toastr.error("showResult: "+err);
                                  toastr.error("Transaction Failed!");
                                }
                              });
                              break;
                        case 'addInterestsAchievementsCountry':
                              contract.addInterestsAchievementsCountry(_interests, _achievements, _country, function (err, res) {
                                if(res) {
                                  toastr.success("Transaction successful. <a href=\"https://rinkeby.etherscan.io/tx/"+res+"\" target=\"_blank\"><strong><u>Track Transaction</u></strong></a>");
                                  if (subPage == 'profileEdit') {
                                    saveProfile();
                                  }
                                } else {
                                  //toastr.error("showResult: "+err);
                                  toastr.error("Transaction Failed!");
                                }
                              });
                              break;
                        case 'addAchievementsCountryInfo':
                              contract.addAchievementsCountryInfo(_achievements, _country, _info, function (err, res) {
                                if(res) {
                                  toastr.success("Transaction successful. <a href=\"https://rinkeby.etherscan.io/tx/"+res+"\" target=\"_blank\"><strong><u>Track Transaction</u></strong></a>");
                                  if (subPage == 'profileEdit') {
                                    saveProfile();
                                  }
                                } else {
                                  //toastr.error("showResult: "+err);
                                  toastr.error("Transaction Failed!");
                                }
                              });
                              break;
                        case 'addCountryInfoInterestsAchievements':
                              contract.addCountryInfoInterestsAchievements(_country, _info, _interests, _achievements, function (err, res) {
                                if(res) {
                                  toastr.success("Transaction successful. <a href=\"https://rinkeby.etherscan.io/tx/"+res+"\" target=\"_blank\"><strong><u>Track Transaction</u></strong></a>");
                                  if (subPage == 'profileEdit') {
                                    saveProfile();
                                  }
                                } else {
                                  //toastr.error("showResult: "+err);
                                  toastr.error("Transaction Failed!");
                                }
                              });
                              break;
                        default:
                              toastr.error("No information was changed!");
                              break;
                      }
                    }
                  }
                });
              }
            });
          } else {
            toastr.error("Please login into MetaMask!");
          }
        });

      }).catch(function(message){
        toastr.error("Not a valid "+message+".");
      });
    }
  }
}
</script>