<script type="text/javascript">
var questions  = {};
var totalScore = 0;
var maxScore = 0;
function fetchSubjectiveQuestions() {
	var req = {};
	var res;
	req.action = 'get-subjective-exam-Attempt';
	req.examId = examId;
	req.attemptId = attemptId;
	$.ajax({
		'type'	: 'post',
		'url'	: ApiEndPoint,
		'data'	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0) {
			//console.log(res.message);
			//window.location.href = sitePath404;
		}
		else {
			//console.log(res);
			$('.attempt-no').text(res.attemptNo);
			questions	= res.questions;
			totalScore	= round2(res.totalScore);
			maxScore	= round2(res.maxScore);
			var percentage = round2((totalScore/maxScore)*100);
			var rank	= round2(res.rank);
			$('#score').text(totalScore + '/' + maxScore);
			$('#percentage').text(percentage).attr("data-value", percentage);
			if (res.ranges) {
				var ranges = res.ranges;
				for (var i = 0; i < ranges.length; i++) {
					if (percentage>=ranges[i].percentLow && percentage<=ranges[i].percentHigh) {
						$('#comment').append(
							'<div>Instructor Comment: '+ranges[i].comment+' <img src="'+sitepathStudentIncludes+'assets/custom/img/smileys/'+ranges[i].file+'" class="img-smiley" alt="" /></div>').removeClass('hide');
					}
					
				}
			}
			$('#rank').text(rank + '/' + res.totalRank).attr("data-value", rank);
			var startDate = new Date(parseInt(res.startDate+'000'));
			var start = startDate.getDate() + '-' + month[startDate.getMonth()].substring(0,3) + '-' + startDate.getFullYear() + ' ' + formatTime2Digits(startDate.getHours()) + ':' + formatTime2Digits(startDate.getMinutes());
			$('#startDate').html(start);
			if(res.endDate == 0)
				var end = 'Not completed';
			else {
				var endDate = new Date(parseInt(res.endDate+'000'));
				var end = endDate.getDate() + '-' + month[endDate.getMonth()].substring(0,3) + '-' + endDate.getFullYear() + ' ' + formatTime2Digits(endDate.getHours()) + ':' + formatTime2Digits(endDate.getMinutes());
			}
			$('#endDate').html(end);
			var time = convertSecondsIntoMinutes(res.totalTime);
			$('#timeTaken').text(time);
			fillQuestions();
			fetchGraph();
			$('.attempt-block').removeClass('hide');
			if (!!res.prevAttemptId) {
				$(".previous-attempt").attr("href", sitePathStudent+'subjective-exams/' + examId + '/result/attempt/' + res.prevAttemptId).removeAttr('disabled');
			}
			if (!!res.nextAttemptId) {
				$(".next-attempt").attr("href", sitePathStudent+'subjective-exams/' + examId + '/result/attempt/' + res.nextAttemptId).removeAttr('disabled');
			}
		}
	});
}
function fetchGraph() {
	var req = {};
	var res;
	req.action = 'get-subjective-attempt-graph';
	req.examId = examId;
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else {
			//console.log(res);
			initGraph(res);
		}
	});
}
function initGraph(data) {
	fetchStudentTimeLine();
	fetchTopperComparison();
	fetchAllStudentGraph();
}
function fetchStudentTimeLine() {
	var req = {};
	var res;
	req.action = 'getsubjectiveTimeLineGraph';
	req.examId = examId;
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		//console.log(res);
		if(res.status == 0)
			toastr.error(res.message);
		else
		{
			initStudentTimeLine(res);
		}
			
	});
}
//function to get graphs vs time graph attempt independent
function initStudentTimeLine(data) {
	/**
	 * Create the chart when all data is loaded
	 * @returns {undefined}
	 */
	var timepoints=[];
	var timepoints2=[];
	var selfMarks=[];
	var dataset = [];
	var avgStudentCount=[];
	var avgStudentMarks=[];
	var toppermarks=[];
	var avgMarks=[];
	var attempts=[];
	var lastday=$.now();//parseInt(data.lastday[0]['endDate'])* parseInt(1000);
	
	$.each(data.self, function (j, self) {
		timepoints[j]=parseInt(self.endDate*1000);
		var element=parseInt(j)+parseInt(1);
		attempts[j]='Attemp NO. '+(element);
		selfMarks[j]=parseInt(self.score);
	});
	$.each(data.avgScore, function (j, avgScore) {
		avgStudentCount[j]=0;
		timepoints2[j]=0;
		avgStudentMarks[j]=0;
		toppermarks[j]=0;
	});
	var lastpoint=timepoints[timepoints.length-1];
	var lastflag=0;
	$.each(data.avgScore, function (j, avgScore) {
		timepoints2[j]=parseInt(avgScore.endDate)* parseInt(1000);
		avgStudentMarks[j]=parseFloat(avgStudentMarks[j])+parseFloat(avgScore.score);
		avgStudentCount[j]=parseInt(avgStudentCount[j])+parseInt(1);
		//calculating for toppermarks
		if(j!=0)
		{
			//to calculate highest from previous
			if(avgScore.score>toppermarks[j-1])
			{
				toppermarks[j]=parseFloat(avgScore.score);
			}else{
				toppermarks[j]=parseFloat(toppermarks[j-1]);
			}
		}else{
		
			toppermarks[j]=parseFloat(avgScore.score);
		}			
		
	});
	for(var i=0;i<data.avgScore.length;i++){
		if(avgStudentCount[i]!=0)
			avgMarks[i]=parseFloat(avgStudentMarks[i])/parseFloat(avgStudentCount[i]);
		else
			avgMarks[i]=0;
	}
	//we got avg as whole it will same for all students	
	//console.log(lastflag);
	//console.log(timepoints2);
		
	timepoints2[timepoints2.length]=parseInt(lastday);

	//console.log(lastpoint+"cjj"+lastday);
	avgMarks[avgMarks.length]=parseFloat(avgMarks[avgMarks.length-1]);
	toppermarks[toppermarks.length]=parseFloat(toppermarks[toppermarks.length-1]);
		
	var jCompare=0;
	var flag=0;

	for(var k=0;k<timepoints.length;)
	{
		for(var i=0;i<timepoints2.length-1;i++)
		{	
			if(timepoints[k]==timepoints2[i])
			{
				break;
			}	
			else if(timepoints[k]>timepoints2[i] &&timepoints[k]<timepoints2[i+1] )
			{		//console.log(timepoints[k]+'check'+timepoints2[i]);
					if(timepoints2[i]>timepoints[k])
					{
						var highmarks=parseFloat(toppermarks[i+1]);
						var avg=(parseFloat(avgMarks[i+1]));
						timepoints2.splice(i, 0,timepoints[k]);
						toppermarks.splice(i, 0,highmarks );
						avgMarks.splice(i, 0,avg );
						break;
					}
					else{
						var highmarks=parseFloat(toppermarks[i+1]);
						var avg=(parseFloat(avgMarks[i+1]));
						timepoints2.splice(i+1, 0,timepoints[k]);
						toppermarks.splice(i+1, 0,highmarks );
						avgMarks.splice(i+1, 0,avg );
						break;
					}
			}
		}
		k++;
	}
	for(var i=0;i<timepoints2.length-1;i++)
	{
		if(timepoints2[i]==timepoints2[i+1])
		{
			timepoints2.splice(i, 1);
			avgMarks.splice(i, 1);
			toppermarks.splice(i,1);
		}
	}

	var scoresOptions = [],
		scoresCounter = 0,
		newnames = ['self','avgScore','topper','attempts'];
	function createChart2() {
		//console.log(scoresOptions);
		var i=0;

		$('#highchart_1').highcharts('StockChart', {
			rangeSelector: {
				selected: 4
			},
			chart : {
				style: {
					fontFamily: 'Open Sans'
				}
			},
			xAxis: {

			},
			yAxis: [{
					labels: {
						align: 'left',
						x: 15,
						formatter: function () {
							return  this.value ;
						}
					},
					//min: 0,
					plotLines: [{
						value: 0,
						width: 2,
						color: 'silver'
					}]
				},
				{ // Secondary yAxis
				labels: {
					formatter: function () {
						return  this.value ;
						//return point.y;
					}
				},
				linkedTo:0,
				opposite: false
			}],
			plotOptions: {
				series: {
					marker : {
						enabled : true,
						radius : 3
					}
				}
			},
			tooltip: {
				pointFormat: '<span >{series.name}</span>: <b>{point.y}</b><br/>'
			},
			legend: {
				enabled: true,
				layout: 'vertical',
				verticalAlign: 'middle',
				borderWidth: 0
			},
			series: scoresOptions
		});
	}
	
	//declaring array for data points calculation
	var elementarray1=[];
	var elementarray2=[];
	var elementarray3=[];
	var elementarray4=[];
	// calculating for self array
	for(var i = 0; i < timepoints.length; i++) {
		elementarray=[];
		elementarray[0] = parseInt(timepoints[i]);
		elementarray[1] = parseInt(selfMarks[i]);
		elementarray1[i]=elementarray;
	}
	//console.log(elementarray1)
	scoresOptions[0] = {
							name: 'Your Score',
							data: elementarray1,
							lineWidth: 3
							//type: 'spline'
						};
	//calculating datapoint for avg 
	for(var i = 0; i < avgMarks.length; i++) {
		elementarray=[];
		elementarray[0] = parseInt(timepoints2[i]);
		elementarray[1] = parseInt(avgMarks[i]);
		elementarray2[i]=elementarray;
		
	}
	scoresOptions[1] = {
							name: 'Class Average',
							data: elementarray2,
							dashStyle: 'longdash',
							type: 'spline'
						};
	for(var i = 0; i < toppermarks.length; i++) {
		elementarray=[];
		elementarray[0] = parseInt(timepoints2[i]);
		elementarray[1] = parseInt(toppermarks[i]);
		elementarray3[i]=elementarray;
	}
	scoresOptions[2] = {
							name: 'Class Highest',
							data: elementarray3,
							type: 'spline',
							dashStyle:'ShortDash'
						};
	for(var i = 0; i < timepoints.length; i++) {
		elementarray=[];
		elementarray[0] = parseInt(timepoints[i]);;
		elementarray[1]= parseInt(i+1);;
		elementarray4[i]=elementarray;
	}
	scoresOptions[3] = {
							name: 'Attempt No ',
							data: elementarray4,
							type: 'spline',
							dashStyle:'ShortDash',
							color: '#FFFFFF',
							lineWidth: 0.4,
							showInLegend: false
						};
	
		
	createChart2();	
				
}
function fetchAllStudentGraph() {
	var req = {};
	var res;
	req.action = 'get-SubjectiveNormalization-graph';
	req.examId = examId;
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else
			initAllStudentGraph(res);
	});
}

//function to initiate attempt graph
function initAllStudentGraph(data) {
	//console.log(data);
	var differnece=totalScore-parseFloat(data.highest);
	var blocksDiff=(parseFloat(totalScore)-parseFloat(differnece+parseFloat(data.lowest)))/10;
	//var marksDis=[];
	var students=[];
	var markPer=[];
	var newlowest=(parseFloat(data.lowest)+parseFloat(differnece)).toFixed(1);
	for(var i=1;i<11;i++)
	{	//marksDis[i-1]=(parseFloat(newlowest)+parseFloat(blocksDiff*i)).toFixed(1);
		students[i-1]=0;
		//checking
		var temp=(parseFloat(newlowest)+parseFloat(blocksDiff*i)).toFixed(1);
		markPer[i-1]=Math.round((temp/totalScore)*100)+'%';
		//markPer[i-1]=markPer[i-1]+'%';
	}
	for(var i=markPer.length-1;i>0;i--)
	{
		markPer[i]= markPer[i-1]+' - '+markPer[i];
	}
	
	markPer[0]=Math.round(newlowest)+'%    -  '+markPer[0];
	$.each( data.graph, function( key, value ) {
		var newscore=(parseFloat(value['score'])+parseFloat(differnece)).toFixed(1);
		var index=Math.round(((newscore-newlowest)/blocksDiff));
		if(index>0)
		{	index=index-1;
		}else{
			index=0;
		}
		students[index]=students[index]+1;
	
		
	});
	//console.log(students);
	$('#hero-bar3').highcharts({
		chart: {
			zoomType: 'xy'
		},
		title: {
			text: 'Percentage distribution of all students'
		},
		subtitle: {
			text: 'This graph shows percentage w.r.t to highest scorer'
		},
		xAxis: [
		{
			title: {
			text: 'Percentage Score',
				style: {
					color: Highcharts.getOptions().colors[1]
				}
			},
			categories: markPer,
			crosshair: true
		}],
		yAxis: [{ // Primary yAxis
			labels: {
				format: '{value}',
				style: {
					color: Highcharts.getOptions().colors[1]
				}
			},
			title: {
				text: 'No. of Students',
				style: {
				color: Highcharts.getOptions().colors[1]
				}
			}
		}, { // Secondary yAxis
		title: {
				text: 'Students',
				style: {
					color: Highcharts.getOptions().colors[0]
				}
			},
			labels: {
			format: '{value}',
				style: {
					color: Highcharts.getOptions().colors[0]
				}
			},
		opposite: true
		}],
		tooltip: {
			shared: true
		},
		legend: {
			layout: 'vertical',
			align: 'left',
			x: 120,
			verticalAlign: 'top',
			y: 100,
			floating: true,
			backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
		},
		series: [{
			name: 'Students',
			type: 'column',
			yAxis: 1,
			data: students,
		tooltip: {
			valueSuffix: ''
			}

		}, {
			name: 'No. of Students',
			type: 'spline',
			yAxis: 1,
			data: students,
			tooltip: {
				valueSuffix: ''
			}
		}]
	});
	//$('#percentGraph').collapse('hide');
}
//function to get topper comparison
function fetchTopperComparison() {
	var req = {};
	var res;
	req.action = 'get-subjective-topper-of-exam';
	req.examId = examId;
	req.attemptId = attemptId;
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else{
			//fetchAllStudentGraph();
			fillTopperComparison(res);
		}	
	});
}

//function to fill the compare graphs of topper
function fillTopperComparison(data) {
	var chart = AmCharts.makeChart("chart_1", {
            "type": "serial",
            "theme": "light",
            "pathToImages": App.getGlobalPluginsPath() + "amcharts/amcharts/images/",
            "autoMargins": false,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,

            "fontFamily": 'Open Sans',            
            "color":    '#888',
            
            "dataProvider": [{
                "device": "You",
                "geekbench": data.self.score,
                "geekpoint": data.self.score
            }, {
                "device": "Topper",
                "geekbench": data.avgMax.max,
                "geekpoint": data.avgMax.max
            }, {
                "device": "Median",
                "geekbench": data.median,
                "geekpoint": data.median
            }, {
                "device": "Average",
                "geekbench": data.avgMax.average,
                "geekpoint": data.avgMax.average
            }],
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left"
            }],
            "startDuration": 1,
            "graphs": [{
                "alphaField": "alpha",
                "balloonText": "<span style='font-size:13px;'>[[title]] of [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "dashLengthField": "dashLengthColumn",
                "fillAlphas": 1,
                "title": "Score",
                "type": "column",
                "valueField": "geekbench"
            }, {
                "balloonText": "<span style='font-size:13px;'>[[title]] of [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "bullet": "round",
                "dashLengthField": "dashLengthLine",
                "lineThickness": 3,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "Score",
                "valueField": "geekpoint"
            }],
            "categoryField": "device",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            }
        });
	var chart = AmCharts.makeChart("chart_2", {
            "type": "serial",
            "theme": "light",
            "pathToImages": App.getGlobalPluginsPath() + "amcharts/amcharts/images/",
            "autoMargins": false,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,

            "fontFamily": 'Open Sans',            
            "color":    '#888',
            
            "dataProvider": [{
                "device": "You",
                "geekbench": data.pself.score,
                "geekpoint": data.pself.score
            }, {
                "device": "Topper",
                "geekbench": data.pavgMax.max,
                "geekpoint": data.pavgMax.max
            }, {
                "device": "Median",
                "geekbench": data.pmedian,
                "geekpoint": data.pmedian
            }, {
                "device": "Average",
                "geekbench": data.pavgMax.average,
                "geekpoint": data.pavgMax.average
            }],
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left"
            }],
            "startDuration": 1,
            "graphs": [{
                "alphaField": "alpha",
                "balloonText": "<span style='font-size:13px;'>[[title]] of [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "dashLengthField": "dashLengthColumn",
                "fillAlphas": 1,
                "title": "Percentage",
                "type": "column",
                "valueField": "geekbench"
            }, {
                "balloonText": "<span style='font-size:13px;'>[[title]] of [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "bullet": "round",
                "dashLengthField": "dashLengthLine",
                "lineThickness": 3,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "Percentage",
                "valueField": "geekpoint"
            }],
            "categoryField": "device",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            }
        });
	/*new Morris.Bar({
		element: 'hero-bar1',
		data: [
		{device: 'You', geekbench: data.self.score},
		{device: 'Topper', geekbench: data.avgMax.max},
		{device: 'Median', geekbench: data.median},
		{device: 'Average', geekbench: data.avgMax.average}
		],
		xkey: 'device',
		ykeys: ['geekbench'],
		labels: ['Score'],
		barRatio: 0.4,
		xLabelAngle: 35,
		hideHover: 'auto',
		barColors: ['#D8574C']
	});
	
	new Morris.Bar({
		element: 'hero-bar2',
		data: [
		{device: 'You', geekbench: data.pself.percentage},
		{device: 'Topper', geekbench: data.pavgMax.max},
		{device: 'Median', geekbench: data.pmedian},
		{device: 'Average', geekbench: data.pavgMax.average}
		],
		xkey: 'device',
		ykeys: ['geekbench'],
		labels: ['Percentage'],
		barRatio: 0.4,
		xLabelAngle: 35,
		hideHover: 'auto',
		barColors: ['#56A738']
	});*/
}
function fillQuestions() {
	$("#questionsContainer").html('');
	//console.log(questions);
	if (questions.length>0) {
		$("#questionsContainer").append('<div class="panel-group questions-panel" id="accQuestions" role="tablist" aria-multiselectable="true"></div>');
		for (var i = 0; i < questions.length; i++) {
			//questions[i]
			$("#accQuestions").append('<div class="panel panel-success panel-result no-space">'+
						'<div class="panel-heading" role="tab" id="headQ'+i+'">'+
							'<h4 class="panel-title">'+
								'<a role="button" data-toggle="collapse" data-parent="#accQuestions" href="#bodyQ'+i+'" aria-expanded="false" aria-controls="bodyQ'+i+'" class="text-uppercase">'+
									'<strong><i class="fa fa-'+((i==0)?'minus':'plus')+'-square"></i> Question #'+questions[i].qno+'</strong>'+
								'</a>'+
							'</h4>'+
						'</div>'+
						'<div id="bodyQ'+i+'" class="panel-collapse collapse '+((i==0)?'in':'')+'" role="tabpanel" aria-labelledby="headQ'+i+'"></div>'+
					'</div>');
			if (questions[i].questionType == "question") {
				$('#bodyQ'+i).append(
					'<div class="panel-body">'+
						'<div class="question"><h4 class="bold">'+questions[i].question+'</h4></div>'+
					'</div>'+
					'<div class="panel-body">'+
						'<div class="answer">'+
							'<div class="row">'+
								'<div class="col-md-12">'+
									'<label for="">Correct Answer :</label>'+
									'<div class="well">'+questions[i].answer+'</div>'+
								'</div>'+
							'</div>'+
						'</div>'+
						'<div class="answer">'+
							'<div class="row">'+
								'<div class="col-md-6">'+
									'<label for="">Student\'s Answer :</label>'+
									'<div class="well">'+((!questions[i].attempted)?('<div class="text-danger">Not Attempted</div>'):(questions[i].student_answer))+'</div>'+
								'</div>'+
								'<div class="col-md-6">'+
									'<label for="">Reviews/Comments :</label>'+
									'<div class="well">'+((!questions[i].review)?('<div class="text-danger">No comments</div>'):(questions[i].review))+'</div>'+
								'</div>'+
							'</div>'+
						'</div>'+
						'<div class="uploadsbox"></div>'+
						'<div class="answer">'+
							'<div class="row">'+
								'<div class="col-md-12">'+
									'<label for="">Topper\'s Answer :</label>'+
									'<div class="well">'+((!questions[i].topper_answer)?('NA'):(questions[i].topper_answer))+'</div>'+
								'</div>'+
							'</div>'+
						'</div>'+
						'<div class="topperuploadsbox"></div>'+
					'</div>'+
					'<div class="panel-body marks-time">'+
						'<div class="row">'+
	                        '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
	                            '<div class="dashboard-stat2 text-center">'+
	                                '<div class="display">'+
	                                    '<div class="number">'+
	                                        '<h3 class="font-green-sharp">'+
	                                            '<span data-counter="counterup" data-value="'+((questions[i].check==0 || questions[i].attempted==0)?'0':questions[i].marks)+'">'+((questions[i].attempted==0)?'NA':((questions[i].check==0)?'NA':questions[i].marks))+'</span>'+
	                                        '</h3>'+
	                                        '<small class="text-uppercase">Your marks</small>'+
	                                    '</div>'+
	                                '</div>'+
	                            '</div>'+
	                        '</div>'+
	                        '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
	                            '<div class="dashboard-stat2 text-center">'+
	                                '<div class="display">'+
	                                    '<div class="number">'+
	                                        '<h3 class="font-red-haze">'+
	                                            '<span data-counter="counterup" data-value="'+questions[i].max_marks+'">'+questions[i].max_marks+'</span>'+
	                                        '</h3>'+
	                                        '<small class="text-uppercase">Maximum Marks</small>'+
	                                    '</div>'+
	                                '</div>'+
	                            '</div>'+
	                        '</div>'+
	                        '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
	                            '<div class="dashboard-stat2 text-center">'+
	                                '<div class="display">'+
	                                    '<div class="number">'+
	                                        '<h3 class="font-blue-sharp">'+
	                                            '<span data-counter="counterup" data-value="'+questions[i].topper_marks+'">'+questions[i].topper_marks+'</span>'+
	                                        '</h3>'+
	                                        '<small class="text-uppercase">Topper Marks</small>'+
	                                    '</div>'+
	                                '</div>'+
	                            '</div>'+
	                        '</div>'+
	                        '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
	                            '<div class="dashboard-stat2 text-center">'+
	                                '<div class="display">'+
	                                    '<div class="number">'+
	                                        '<h3 class="font-purple-soft">'+
	                                            '<span data-counter="counterup" data-value="'+questions[i].avg_marks+'">'+questions[i].avg_marks+'</span>'+
	                                        '</h3>'+
	                                        '<small class="text-uppercase">Average Marks</small>'+
	                                    '</div>'+
	                                '</div>'+
	                            '</div>'+
	                        '</div>'+
						'</div>'+
						'<div class="row">'+
	                        '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
	                            '<div class="dashboard-stat2 text-center">'+
	                                '<div class="display">'+
	                                    '<div class="number">'+
	                                        '<h3 class="font-green-sharp">'+
	                                            '<span data-counter="counterup" data-value="'+questions[i].time+'">'+questions[i].time+'</span>'+
	                                        '</h3>'+
	                                        '<small class="text-uppercase">Time Taken</small>'+
	                                    '</div>'+
	                                '</div>'+
	                            '</div>'+
	                        '</div>'+
	                        '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
	                            '<div class="dashboard-stat2 text-center">'+
	                                '<div class="display">'+
	                                    '<div class="number">'+
	                                        '<h3 class="font-red-haze">'+
	                                            '<span data-counter="counterup" data-value="'+questions[i].topper_time+'">'+questions[i].topper_time+'</span>'+
	                                        '</h3>'+
	                                        '<small class="text-uppercase">Topper Time</small>'+
	                                    '</div>'+
	                                '</div>'+
	                            '</div>'+
	                        '</div>'+
	                        '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
	                            '<div class="dashboard-stat2 text-center">'+
	                                '<div class="display">'+
	                                    '<div class="number">'+
	                                        '<h3 class="font-blue-sharp">'+
	                                            '<span data-counter="counterup" data-value="'+questions[i].avg_time+'">'+questions[i].avg_time+'</span>'+
	                                        '</h3>'+
	                                        '<small class="text-uppercase">Average Time</small>'+
	                                    '</div>'+
	                                '</div>'+
	                            '</div>'+
	                        '</div>'+
	                        '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
	                            '<div class="dashboard-stat2 text-center">'+
	                                '<div class="display">'+
	                                    '<div class="number">'+
	                                        '<h3 class="font-purple-soft">'+
	                                            '<span data-counter="counterup" data-value="'+questions[i].shortest_time+'">'+((questions[i].shortest_time!=0)?(questions[i].shortest_time):('NA'))+'</span>'+
	                                        '</h3>'+
	                                        '<small class="text-uppercase">Shortest Time</small>'+
	                                    '</div>'+
	                                '</div>'+
	                            '</div>'+
	                        '</div>'+
	                    '</div>'+
	                    '<div className="row">'+
							'<div class="col-md-12">'+
								'<button class="btn btn-primary btn-block js-time-compare" data-question="'+questions[i].id+'">Compare your time</button>'+
							'</div>'+
						'</div>'+
					'</div>');
				if (questions[i].attempted) {
					if (questions[i]["uploads"].length>0) {
						var uploads = questions[i]["uploads"];
						for (var j = 0; j < uploads.length; j++) {
							//uploads[i]
							$('#bodyQ'+i+' .uploadsbox').append(
								'<div class="answer">'+
									'<div class="row">'+
										'<div class="col-md-6">'+
											'<label for="">Student\'s Answer :</label>'+
											'<div class="well well-sm">'+
												((uploads[j].type=="pdf")?('<a href="javascript:void(0)" data-path="'+uploads[j].path+'" data-type="'+uploads[j].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (PDF Format)</a>'):((uploads[j].type=="jpg" || uploads[j].type=="jpeg" || uploads[j].type=="png" || uploads[j].type=="gif")?('<a href="javascript:void(0)" data-path="'+uploads[j].path+'" data-type="'+uploads[j].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (Image Format)</a>'):('<a href="'+uploads[j].path+'" class="text-uppercase" target="_blank">Click to download file ('+uploads[j].type+')</a>')))+
											'</div>'+
										'</div>'+
										'<div class="col-md-6">'+
											'<label for="">Reviews/Comments :</label>'+
											'<div class="well well-sm">'+((uploads[j].review=='')?'<div class="text-danger">No comments</div>':uploads[j].review)+'</div>'+
										'</div>'+
									'</div>'+
								'</div>');
						};
					}
				}
				if (questions[i]["topper_uploads"].length>0) {
					var uploads = questions[i]["topper_uploads"];
					for (var j = 0; j < uploads.length; j++) {
						//uploads[i]
						$('#bodyQ'+i+' .topperuploadsbox').append(
							'<div class="topper-answer">'+
								'<div class="row">'+
									'<div class="col-md-12">'+
										'<label for="">Topper\'s Answer :</label>'+
										'<div class="well well-sm">'+
											((uploads[j].type=="pdf")?('<a href="javascript:void(0)" data-path="'+uploads[j].path+'" data-type="'+uploads[j].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (PDF Format)</a>'):((uploads[j].type=="jpg" || uploads[j].type=="jpeg" || uploads[j].type=="png" || uploads[j].type=="gif")?('<a href="javascript:void(0)" data-path="'+uploads[j].path+'" data-type="'+uploads[j].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (Image Format)</a>'):('<a href="'+uploads[j].path+'" class="text-uppercase" target="_blank">Click to download file ('+uploads[j].type+')</a>')))+
										'</div>'+
									'</div>'+
								'</div>'+
							'</div>');
					};
				}
			} else if (questions[i].questionType == "questionGroup") {
				if (questions[i].listType=="one") {
					$('#bodyQ'+i).append(
						'<div class="panel-body">'+
							'<div class="question"><h4 class="bold">'+questions[i].question+'</h4></div>'+
						'</div>'+
						'<div class="panel-body marks-time">'+
							'<div class="row">'+
								'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
									'<div class="dashboard-stat2 text-center">'+
										'<div class="display">'+
											'<div class="number">'+
												'<h3 class="font-green-sharp">'+
													'<span data-counter="counterup" data-value="'+((questions[i].check==0 || questions[i].attempted==0)?'0':questions[i].marks)+'">'+((questions[i].attempted==0)?'NA':((questions[i].check==0)?'NA':questions[i].marks))+'</span>'+
												'</h3>'+
												'<small class="text-uppercase">Your marks</small>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>'+
								'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
									'<div class="dashboard-stat2 text-center">'+
										'<div class="display">'+
											'<div class="number">'+
												'<h3 class="font-red-haze">'+
													'<span data-counter="counterup" data-value="'+questions[i].max_marks+'">'+questions[i].max_marks+'</span>'+
												'</h3>'+
												'<small class="text-uppercase">Maximum Marks</small>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>'+
								'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
									'<div class="dashboard-stat2 text-center">'+
										'<div class="display">'+
											'<div class="number">'+
												'<h3 class="font-blue-sharp">'+
													'<span data-counter="counterup" data-value="'+questions[i].topper_marks+'">'+questions[i].topper_marks+'</span>'+
												'</h3>'+
												'<small class="text-uppercase">Topper Marks</small>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>'+
								'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
									'<div class="dashboard-stat2 text-center">'+
										'<div class="display">'+
											'<div class="number">'+
												'<h3 class="font-purple-soft">'+
													'<span data-counter="counterup" data-value="'+questions[i].avg_marks+'">'+questions[i].avg_marks+'</span>'+
												'</h3>'+
												'<small class="text-uppercase">Average Marks</small>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>');
					if (questions[i]["subQuestions"].length>0) {
						$('#bodyQ'+i+' .marks-time').before('<div class="panel-body"><div class="panel-group" id="accQ'+i+'Subquestions" role="tablist" aria-multiselectable="true"></div></div>');
						var subquestions = questions[i]["subQuestions"];
						for (var j = 0; j < subquestions.length; j++) {
							$('#accQ'+i+'Subquestions').append(
								'<div class="panel panel-default">'+
									'<div class="panel-heading" role="tab" id="headQ'+i+'S'+j+'">'+
										'<h4 class="panel-title">'+
											'<a role="button" data-toggle="collapse" data-parent="#accQ'+i+'Subquestions" href="#bodyQ'+i+'S'+j+'" aria-expanded="true" aria-controls="bodyQ'+i+'S'+j+'">'+
												'Subquestion #'+(j+1)+''+
											'</a>'+
										'</h4>'+
									'</div>'+
									'<div id="bodyQ'+i+'S'+j+'" class="panel-collapse collapse '+((j==0)?'in':'')+'" role="tabpanel" aria-labelledby="headQ'+i+'S'+j+'">'+
										'<div class="panel-body">'+
											'<div class="question"><h4 class="bold">'+subquestions[j].question+'</h4></div>'+
										'</div>'+
										'<div class="panel-body">'+
											'<div class="answer">'+
												'<div class="row">'+
													'<div class="col-md-12">'+
														'<label for="">Correct Answer :</label>'+
														'<div>'+subquestions[j].answer+'</div>'+
													'</div>'+
												'</div>'+
											'</div>'+
											((!subquestions[j].attempted)?('<div class="answer">'+
												'<div class="row">'+
													'<div class="col-md-12">'+
														'<div class="text-danger">Not attempted</div>'+
													'</div>'+
												'</div>'+
											'</div>'):(((!subquestions[j].student_answer)?(''):('<div class="answer">'+
												'<div class="row">'+
													'<div class="col-md-6">'+
														'<label for="">Student\'s Answer :</label>'+
														'<div class="well">'+subquestions[j].student_answer+'</div>'+
													'</div>'+
													'<div class="col-md-6">'+
														'<label for="">Reviews/Comments :</label>'+
														'<div class="well">'+((questions[j].review=='')?'<div class="text-danger">No comments</div>':questions[j].review)+'</div>'+
													'</div>'+
												'</div>'+
											'</div>'))+
											'<div class="uploadsbox"></div>'))+
											((!subquestions[j].topper_answer)?(''):('<div class="topper-answer">'+
												'<div class="row">'+
													'<div class="col-md-12">'+
														'<label for="">Topper\'s Answer :</label>'+
														'<div class="well">'+subquestions[j].topper_answer+'</div>'+
													'</div>'+
												'</div>'+
											'</div>'))+
											'<div class="topperuploadsbox"></div>'+
										'</div>'+
									'</div>'+
								'</div>');
							if (subquestions[j].attempted) {
								if (subquestions[j]["uploads"].length>0) {
									var uploads = subquestions[j]["uploads"];
									for (var k = 0; k < uploads.length; k++) {
										//uploads[i]
										$('#bodyQ'+i+'S'+j+' .uploadsbox').append(
											'<div class="answer">'+
												'<div class="row">'+
													'<div class="col-md-6">'+
														'<label for="">Student\'s Answer :</label>'+
														'<div>'+
															((uploads[k].type=="pdf")?('<a href="javascript:void(0)" data-path="'+uploads[k].path+'" data-type="'+uploads[k].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (PDF Format)</a>'):((uploads[k].type=="jpg" || uploads[k].type=="jpeg" || uploads[k].type=="png" || uploads[k].type=="gif")?('<a href="javascript:void(0)" data-path="'+uploads[k].path+'" data-type="'+uploads[k].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (Image Format)</a>'):('<a href="'+uploads[k].path+'" class="text-uppercase" target="_blank">Click to download file ('+uploads[k].type+')</a>')))+
														'</div>'+
													'</div>'+
													'<div class="col-md-6">'+
														'<label for="">Reviews/Comments :</label>'+
														'<div class="well well-sm">'+((uploads[k].review=='')?'<div class="text-danger">No comments</div>':uploads[k].review)+'</div>'+
													'</div>'+
												'</div>'+
											'</div>');
									};
								}
							}
							if (subquestions[j]["topper_uploads"].length>0) {
								var uploads = subquestions[j]["topper_uploads"];
								for (var k = 0; k < uploads.length; k++) {
									//uploads[i]
									$('#bodyQ'+i+'S'+j+' .topperuploadsbox').append(
										'<div class="topper-answer">'+
											'<div class="row">'+
												'<div class="col-md-12">'+
													'<label for="">Topper\'s Answer :</label>'+
													'<div class="well well-sm">'+
														((uploads[k].type=="pdf")?('<a href="javascript:void(0)" data-path="'+uploads[k].path+'" data-type="'+uploads[k].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (PDF Format)</a>'):((uploads[k].type=="jpg" || uploads[k].type=="jpeg" || uploads[k].type=="png" || uploads[k].type=="gif")?('<a href="javascript:void(0)" data-path="'+uploads[k].path+'" data-type="'+uploads[k].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (Image Format)</a>'):('<a href="'+uploads[k].path+'" class="text-uppercase" target="_blank">Click to download file ('+uploads[k].type+')</a>')))+
													'</div>'+
												'</div>'+
											'</div>'+
										'</div>');
								};
							}
						};
					}
				} else {
					$('#bodyQ'+i).append('<div class="panel-body"></div>');
					if (questions[i]["subQuestions"].length>0) {
						$('#bodyQ'+i+' .panel-body').append('<div class="panel-group" id="accQ'+i+'Subquestions" role="tablist" aria-multiselectable="true"></div>');
						var subquestions = questions[i]["subQuestions"];
						for (var j = 0; j < subquestions.length; j++) {
							console.log(subquestions);
							$('#accQ'+i+'Subquestions').append(
								'<div class="panel panel-default">'+
									'<div class="panel-heading" role="tab" id="headQ'+i+'S'+j+'">'+
										'<h4 class="panel-title">'+
											'<a role="button" data-toggle="collapse" data-parent="#accQ'+i+'Subquestions" href="#bodyQ'+i+'S'+j+'" aria-expanded="true" aria-controls="bodyQ'+i+'S'+j+'">'+
												'Subquestion #'+(j+1)+''+
											'</a>'+
										'</h4>'+
									'</div>'+
									'<div id="bodyQ'+i+'S'+j+'" class="panel-collapse collapse '+((j==0)?'in':'')+'" role="tabpanel" aria-labelledby="headQ'+i+'S'+j+'">'+
										'<div class="panel-body">'+
											'<div class="question"><h4 class="bold">'+subquestions[j].question+'</h4></div>'+
										'</div>'+
										'<div class="panel-body">'+
											'<div class="answer">'+
												'<div class="row">'+
													'<div class="col-md-12">'+
														'<label for="">Correct Answer :</label>'+
														'<div>'+subquestions[j].answer+'</div>'+
													'</div>'+
												'</div>'+
											'</div>'+
											'<div class="answer">'+
												'<div class="row">'+
													'<div class="col-md-6">'+
														'<label for="">Student\'s Answer :</label>'+
														'<div class="well">'+((!subquestions[j].attempted)?('<div class="text-danger">Not Attempted</div>'):(subquestions[j].student_answer))+'</div>'+
													'</div>'+
													'<div class="col-md-6">'+
														'<label for="">Reviews/Comments :</label>'+
														'<div class="well">'+((questions[j].review=='')?'<div class="text-danger">No comments</div>':questions[i].review)+'</div>'+
													'</div>'+
												'</div>'+
											'</div>'+
											'<div class="uploadsbox"></div>'+
											'<div class="topper-answer">'+
												'<div class="row">'+
													'<div class="col-md-12">'+
														'<label for="">Topper\'s Answer :</label>'+
														'<div class="well">'+((!subquestions[j].topper_answer)?(''):(subquestions[j].topper_answer))+'</div>'+
													'</div>'+
												'</div>'+
											'</div>'+
											'<div class="topperuploadsbox"></div>'+
										'</div>'+
										'<div class="panel-body marks-time">'+
											'<div class="row">'+
												'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
													'<div class="dashboard-stat2 text-center">'+
														'<div class="display">'+
															'<div class="number">'+
																'<h3 class="font-green-sharp">'+
																	'<span data-counter="counterup" data-value="'+((subquestions[j].check==0 || subquestions[j].attempted==0)?'0':subquestions[j].marks)+'">'+((subquestions[j].attempted==0)?'NA':((subquestions[j].check==0)?'NA':subquestions[j].marks))+'</span>'+
																'</h3>'+
																'<small class="text-uppercase">Your marks</small>'+
															'</div>'+
														'</div>'+
													'</div>'+
												'</div>'+
												'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
													'<div class="dashboard-stat2 text-center">'+
														'<div class="display">'+
															'<div class="number">'+
																'<h3 class="font-red-haze">'+
																	'<span data-counter="counterup" data-value="'+subquestions[j].max_marks+'">'+subquestions[j].max_marks+'</span>'+
																'</h3>'+
																'<small class="text-uppercase">Maximum Marks</small>'+
															'</div>'+
														'</div>'+
													'</div>'+
												'</div>'+
												'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
													'<div class="dashboard-stat2 text-center">'+
														'<div class="display">'+
															'<div class="number">'+
																'<h3 class="font-blue-sharp">'+
																	'<span data-counter="counterup" data-value="'+subquestions[j].topper_marks+'">'+subquestions[j].topper_marks+'</span>'+
																'</h3>'+
																'<small class="text-uppercase">Topper Marks</small>'+
															'</div>'+
														'</div>'+
													'</div>'+
												'</div>'+
												'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
													'<div class="dashboard-stat2 text-center">'+
														'<div class="display">'+
															'<div class="number">'+
																'<h3 class="font-purple-soft">'+
																	'<span data-counter="counterup" data-value="'+subquestions[j].avg_marks+'">'+subquestions[j].avg_marks+'</span>'+
																'</h3>'+
																'<small class="text-uppercase">Average Marks</small>'+
															'</div>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>'+
											'<div class="row">'+
												'<div class="col-md-12">'+
													'<button class="btn btn-primary btn-block js-time-compare" data-question="'+subquestions[j].id+'">Compare your time</button>'+
												'</div>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>');
							if (subquestions[j].attempted) {
								if (subquestions[j]["uploads"].length>0) {
									var uploads = subquestions[j]["uploads"];
									for (var k = 0; k < uploads.length; k++) {
										//uploads[i]
										$('#bodyQ'+i+'S'+j+' .uploadsbox').append(
											'<div class="answer">'+
												'<div class="row">'+
													'<div class="col-md-6">'+
														'<label for="">Student\'s Answer :</label>'+
														'<div class="well well-sm">'+
															((uploads[k].type=="pdf")?('<a href="javascript:void(0)" data-path="'+uploads[k].path+'" data-type="'+uploads[k].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (PDF Format)</a>'):((uploads[k].type=="jpg" || uploads[k].type=="jpeg" || uploads[k].type=="png" || uploads[k].type=="gif")?('<a href="javascript:void(0)" data-path="'+uploads[k].path+'" data-type="'+uploads[k].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (Image Format)</a>'):('<a href="'+uploads[k].path+'" class="text-uppercase" target="_blank">Click to download file ('+uploads[k].type+')</a>')))+
														'</div>'+
													'</div>'+
													'<div class="col-md-6">'+
														'<label for="">Reviews/Comments :</label>'+
														'<div class="well well-sm">'+((uploads[k].review=='')?'<div class="text-danger">No comments</div>':uploads[k].review)+'</div>'+
													'</div>'+
												'</div>'+
											'</div>');
									};
								}
							}
							if (subquestions[j]["topper_uploads"].length>0) {
								var uploads = subquestions[j]["topper_uploads"];
								for (var k = 0; k < uploads.length; k++) {
									//uploads[i]
									$('#bodyQ'+i+'S'+j+' .topperuploadsbox').append(
										'<div class="topper-answer">'+
											'<div class="row">'+
												'<div class="col-md-12">'+
													'<label for="">Topper\'s Answer :</label>'+
													'<div class="well well-sm">'+
														((uploads[k].type=="pdf")?('<a href="javascript:void(0)" data-path="'+uploads[k].path+'" data-type="'+uploads[k].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (PDF Format)</a>'):((uploads[k].type=="jpg" || uploads[k].type=="jpeg" || uploads[k].type=="png" || uploads[k].type=="gif")?('<a href="javascript:void(0)" data-path="'+uploads[k].path+'" data-type="'+uploads[k].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (Image Format)</a>'):('<a href="'+uploads[k].path+'" class="text-uppercase" target="_blank">Click to download file ('+uploads[k].type+')</a>')))+
													'</div>'+
												'</div>'+
											'</div>'+
										'</div>');
								};
							}
						};
					}
				}
			}
		};
		$('#accQuestions').on('hidden.bs.collapse', function () {
			//$(this).find('.panel-heading .fa').removeClass('fa-minus-square').addClass('fa-plus-square');
			$(this).find('.panel-title>.collapsed .fa').removeClass('fa-minus-square').addClass('fa-plus-square');
			$(this).find('.panel-title>a:not(.collapsed) .fa').removeClass('fa-plus-square').addClass('fa-minus-square');
		})
	};
}
$('#questionsContainer').on('click', '.js-popup-answer', function(){
	var path = $(this).attr("data-path");
	var type = $(this).attr("data-type");
	console.log(path);
	console.log(type);
	if(!path || !type) {
		toastr.error("There is something wrong, please refresh and try again!");
	} else {
		if (type == "pdf") {
			$('#showAnswerDetail').html('<iframe src="'+path+'" frameborder="0" height="500" class="btn-block"></iframe>');
		} else {
			$('#showAnswerDetail').html('<img src="'+path+'" alt="Answer" class="btn-block" />');
		}
		$('#answerModal').modal('show');
	}
});
$('#questionsContainer').on('click', '.js-time-compare', function() {
	var req = {};
	var res;
	req.action = 'get-subjective-maximum-time-for-question';
	req.questionId = $(this).attr('data-question');
	req.attemptId = attemptId;
	req.examId = examId;
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else {
			$('#timeGraph').html('');
			//$('#timeCompareModal').modal('show');
		
			if(res.min == null || res.min == 0)
			res.min = 'NA';
			else
				res.min += ' seconds';
			var sanswer = '';
			if(res.self.check == 0)
				sanswer = '(Unattempted <i class="fa fa-dot-circle-o text-warning"></i>)';
			else if(res.self.check == 1)
				sanswer = '(Correct <i class="fa fa-check-circle-o text-success"></i>)';
			else
				sanswer = '(Incorrect <i class="fa fa-times-circle-o text-danger"></i>)';
			var tanswer = '';
			if(res.topper.check == 0)
				tanswer = '(Unattempted <i class="fa fa-dot-circle-o text-warning"></i>)';
			else if(res.topper.check == 1)
				tanswer = '(Correct <i class="fa fa-check-circle-o text-success"></i>)';
			else
				tanswer = '(Incorrect <i class="fa fa-times-circle-o text-danger"></i>)';
			legendHTML = '<table class="table table-bordered"><tr><td>Time spent by you</td><td>' + res.self.time + ' seconds ' + sanswer + '</td></tr>'
						+ '<tr><td>Average Time spent on this question</td><td>' + res.avg + ' seconds</td></tr>'
						+ '<tr><td>Least time spent on answering the question correctly</td><td>' + res.min + '</td></tr>'
						+ '<tr><td>Time spent by Topper</td><td>' + res.topper.time + ' seconds ' + tanswer + '</td></tr></table>';
			$('#timeGraph').append('<div id="pie-times"></div>' + legendHTML);
			$('#pie-times').highcharts({
				chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false
				},
				title: {
					text: 'Total number of students who have got the question are ' + parseInt(res.studentCount.total)
				},
				tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				},
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
							enabled: true,
							format: '<b>{point.name}</b>: {point.percentage:.1f} %',
							style: {
								color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
						}
						}
					}
				},
				series: [{
					type: 'pie',
					name: 'Number',
					data: [
					['Correct', parseInt(res.studentCount.correct)],
						['Wrong', parseInt(res.studentCount.wrong)],
						['Unattempted', parseInt(res.studentCount.unattempted)]
					]
				}],
				colors: ['green','red','black']
			});
			//this is necessary to resize the pie chart
			//$(window).resize();

			var chart = $('#pie-times').highcharts();
			$('#timeCompareModal').on('show.bs.modal', function() {
				$('#pie-times').css('visibility', 'hidden');
			});
			$('#timeCompareModal').on('shown.bs.modal', function() {
				$('#pie-times').css('visibility', 'initial');
				chart.reflow();
			});
			
			$('#timeCompareModal').modal("show");
		}
	});
});
$(document).ready(function(){
	
});
</script>