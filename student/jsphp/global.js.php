<script type="text/javascript">
var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
var userId = '<?php echo $_SESSION['userId']; ?>';
var userRole = '<?php echo $_SESSION['userRole']; ?>';
var userEmail = '<?php echo $_SESSION['userEmail']; ?>';
var sitePath	= '<?php echo $sitepath; ?>';
var sitePathCourses	= '<?php echo $sitepathCourses; ?>';
var sitePath404 = '<?php echo $sitepath404; ?>';
var sitePathStudent = '<?php echo $sitepathStudent; ?>';
var sitepathStudentOld = '<?php echo $sitepathStudentOld; ?>';
var sitepathStudentIncludes = '<?php echo $sitepathStudentIncludes; ?>';
var sitepathStudentSettings	= '<?php echo $sitepathStudentSettings; ?>';
var sitepathStudentPassword	= '<?php echo $sitepathStudentPassword; ?>';
var sitepathStudentProfile	= '<?php echo $sitepathStudentProfile; ?>';
var sitepathStudentCourses	= '<?php echo $sitepathStudentCourses; ?>';
var sitepathStudentSubjects = '<?php echo $sitepathStudentSubjects; ?>';
var sitepathStudentNotif	= '<?php echo $sitepathStudentNotif; ?>';
var sitepathLogout = '<?php echo $sitepathLogout; ?>';
var page = '<?php echo $page; ?>';
<?php if (isset($subPage) && !empty(isset($subPage))) { ?>
var subPage = '<?php echo $subPage; ?>';
<?php }; ?>
var ApiEndPoint = ApiEndpoint = sitePath+'api/index.php';
var FileApiPoint = sitePath+'api/files1.php';
var slug = '<?php echo ((isset($slug))?($slug):('')); ?>';
var subdomain = '<?php echo ((isset($subdomain))?($subdomain):('')); ?>';
var courses = [];
var subCourses = [];
var notifications;
var month = new Array();
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";
var themeColors = ["white","default","dark","blue","blue-madison","blue-chambray","blue-ebonyclay","blue-hoki","blue-steel","blue-soft","blue-dark","blue-sharp","blue-oleo","green","green-meadow","green-seagreen","green-turquoise","green-haze","green-jungle","green-soft","green-dark","green-sharp","green-steel","grey","grey-steel","grey-cararra","grey-gallery","grey-cascade","grey-silver","grey-salsa","grey-salt","grey-mint","red","red-pink","red-sunglo","red-intense","red-thunderbird","red-flamingo","red-soft","red-haze","red-mint","yellow","yellow-gold","yellow-casablanca","yellow-crusta","yellow-lemon","yellow-saffron","yellow-soft","yellow-haze","yellow-mint","purple","purple-plum","purple-medium","purple-studio","purple-wisteria","purple-seance","purple-intense","purple-sharp","purple-soft"];
var darkThemeColors = ["dark","blue","blue-madison","blue-chambray","blue-ebonyclay","blue-hoki","blue-steel","blue-soft","blue-dark","blue-sharp","blue-oleo","green","green-meadow","green-seagreen","green-turquoise","green-haze","green-jungle","green-soft","green-dark","green-sharp","green-steel","red","red-pink","red-sunglo","red-intense","red-thunderbird","red-flamingo","red-soft","red-haze","red-mint","yellow","yellow-gold","yellow-casablanca","yellow-crusta","yellow-lemon","yellow-saffron","yellow-soft","yellow-haze","yellow-mint","purple","purple-plum","purple-medium","purple-studio","purple-wisteria","purple-seance","purple-intense","purple-sharp","purple-soft"];
var bootstrapColors = ["info", "warning", "danger", "success"]
toastr.options = {
  "closeButton": true,
  "debug": false,
  "positionClass": "toast-top-center",
  "onclick": null,
  "showDuration": "1000",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
var summerShort = {
					height: 300,
					toolbar: [
						['style', ['bold', 'italic', 'underline', 'clear']]
					]
				};
<?php if (isset($courseId) && !empty($courseId)) { ?>
var courseId = "<?php echo $courseId; ?>";
<?php }; ?>
<?php if (isset($subjectId) && !empty($subjectId)) { ?>
var subjectId = "<?php echo $subjectId; ?>";
<?php }; ?>
<?php if (isset($subjectDetails) && !empty($subjectDetails)) { ?>
var subjectDetails = <?php echo json_encode($subjectDetails); ?>;
<?php }; ?>
<?php if (isset($examId) && !empty($examId)) { ?>
var examId = "<?php echo $examId; ?>";
<?php }; ?>
<?php if (isset($attemptId)) { ?>
var attemptId = "<?php echo $attemptId; ?>";
<?php }; ?>
<?php if (isset($chapterId)) { ?>
var chapterId = "<?php echo $chapterId; ?>";
<?php }; ?>
<?php if (isset($contentId)) { ?>
var contentId = "<?php echo $contentId; ?>";
<?php }; ?>
function signOut() {
	gapi.load('auth2', function() {
		gapi.auth2.init().then(() => {
			var auth2 = gapi.auth2.getAuthInstance();
			auth2.signOut().then(function () {
				console.log('User signed out.');
				window.location = sitepathLogout;
			});
		}).catch(function (err) {
			window.location = sitepathLogout;
		});
	});
}

$(document).ready(function(){

	initWeb3();

	fetchUserDetails();
	fetchNotifications();
	getStudentCourses();
	getStudentSubsCourses();
    
    $(".btn-logout").click(function(event) {
        /* Act on the event */
        signOut();
    });

	$('[data-scroll]').click(function(e){
		e.preventDefault();
		var scrollDiv = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(scrollDiv).offset().top - 100
		}, 1000);
	});

	/*$('.btn-crypto-app').click(function(event) {
		checkCryptoApp();
	});*/

	//function to fetch user details for change password page
	function fetchUserDetails() {
		var req = {};
		var res;
		req.action = 'get-student-details-for-changePassword';
		ajaxRequest({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req),
				success:function(res) {
					res = $.parseJSON(res);
					if(res.status == 0)
						toastr.error(res.message);
					else {
						//details = res;
						fillUserDetails(res);
						//console.log(res);
						if (page == 'notificationsActivity') {
							fetchNotificationsActivity(1);
						} else if (page == 'notificationsChat') {
							fetchNotificationsChat(1);
						} else if (page == 'profile') {
							fetchStudentDetails();
						} else if (page == 'settings') {
							checkSocialConnections();
						} else if (page == 'courseDetail') {
							fetchCourseDetails();
							getStudentExams();
							getStudentSubjectiveExams();
							getStudentManualExams();
						} else if (page == 'subjectDetail') {
							getStudentExams();
							getSubjectProfessors();
							getContentNotes();
							fetchUserRating();
							fetchStudentPerformance();
						} else if (page == 'content') {
							fetchContentStuff();
						} else if (page == 'examResult') {
							fetchexamShow();
						} else if (page == 'subjectiveExamResult') {
							fetchSubjectiveQuestions();
						} else if (page == 'manualExamResult') {
							fetchManualExam();
						} else if (page == 'submission') {
							fetchSubmission();
						} else if (page == 'submissionResult') {
							fetchSubmissionQuestions();
						}
					}
				}
		});
	}
	
	//function to fill user details
	function fillUserDetails(data) {
		//console.log(data);
		if (!!data.details.profilePic) {
			$('.dropdown-user .img-circle').attr("src",data.details.profilePic);
		};
		if (!data.details.firstName || !data.details.lastName) {
			$('.dropdown-user .username').text(data.details.username);
		} else {
			$('.dropdown-user .username').text(data.details.firstName+' '+data.details.lastName);
		}
		$('.dropdown-user').removeClass('invisible');
	}
	
	//function to fetch notifications
	function fetchNotifications() {
		var req = {};
		var res;
		req.action = 'get-student-notifications';
		ajaxRequest({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req),
			success:function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else {
					notifications = res;
					fillNotifications(res);
				}
			}
		});
	}
	//function to fill notifications table
	function fillNotifications(data) {
		$('#header_notification_bar .noti-count').html(data.sitenotifications.length);
		$('#header_inbox_bar .noti-count').html(data.chatnotifications.length);
		if (data.sitenotifications.length>0) {
			var html = '';
			for(var i = 0; i < data.sitenotifications.length; i++) {
				var d = new Date(data.sitenotifications[i].timestamp);
				d = timeSince(d.getTime()); //returns 1340220044000
				html += '<li>'+
							'<a href="'+sitePathStudent+'notifications/activity">'+
								'<span class="time">'+d+'</span>'+
								'<span class="details">'+
								'<span class="label label-sm label-icon label-warning">'+
									'<i class="fa fa-bell-o"></i>'+
								'</span>'+
								'<span class="message">Join '+data.sitenotifications[i].courseName+' by '+data.sitenotifications[i].inviter+'</span> </span>'+
							'</a>'+
						'</li>';
			}
			$('#header_notification_bar .dropdown-menu-list').html(html);
		} else {
			$('#header_notification_bar .dropdown-menu-list').remove();
		}
		if (page == "home") {
			fillDashboardNotifications(data);
		};
		if (data.chatnotifications.length>0) {
			var html = '';
			for(var i = 0; i < data.chatnotifications.length; i++) {
				var message = "";
				var d = new Date(data.chatnotifications[i].timestamp);
				d = timeSince(d.getTime()); //returns 1340220044000
				switch(data.chatnotifications[i].type) {
					case '0':
						if (data.chatnotifications[i].room_type == "public") {
							message = data.chatnotifications[i].senderName+' just started conversation in \'Subject : '+data.chatnotifications[i].subjectName+'\' of \'Course : '+data.chatnotifications[i].courseName+'\'</span>';
						} else {
							message = data.chatnotifications[i].senderName+' just started conversation with you in \'Subject : '+data.chatnotifications[i].subjectName+'\' of \'Course : '+data.chatnotifications[i].courseName+'\'</span>';
						}
						break;
					case '1':
						if (data.chatnotifications[i].room_type == "public") {
							message = data.chatnotifications[i].senderName+' just sent a message in \'Subject : '+data.chatnotifications[i].subjectName+'\' of \'Course : '+data.chatnotifications[i].courseName+'\'</span>';
						} else {
							message = data.chatnotifications[i].senderName+' just messaged you in \'Subject : '+data.chatnotifications[i].subjectName+'\' of \'Course : '+data.chatnotifications[i].courseName+'\'</span>';
						}
						break;
				}
				html += '<li class="chat-parent">'+
							'<a href="javascript:void(0)" class="chat-button" data-id="'+data.chatnotifications[i].id+'" data-chat="1" target="_blank">'+
								'<span class="photo">'+
									'<img src="'+data.chatnotifications[i].profilePic+'" class="img-circle" alt=""> </span>'+
								'<span class="subject">'+
									'<span class="from"> '+data.chatnotifications[i].senderName+' </span>'+
									'<span class="time"> '+d+' </span>'+
								'</span>'+
								'<span class="message"> '+message+' </span>'+
							'</a>'+
						'</li>';
			}
			$('#header_inbox_bar .dropdown-menu-list').html(html);
		} else {
			$('#header_inbox_bar .dropdown-menu-list').remove();
		}
	}

	function getStudentCourses() {
        var req = {};
        var res;
        req.portfolioSlug = $('#slug').val();
        req.pageUserId = $('#inputUserId').val();
        req.pageUserRole = $('#inputUserRole').val();
        req.action = 'student-joined-courses-new';
        ajaxRequest({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req),
        	success:function(res) {
		
	            res = $.parseJSON(res);
	            if (res.status == 0)
	                toastr.error(res.message);
	            else {
	            	courses = res.courses;
	                fillStudentCoursesMenu(res);
	                if(page == 'home' || page == 'courses') {
						fillStudentCourses();
					}
	            }
	        }
        });
    }

    function getStudentSubsCourses() {
        var req = {};
        var res;
        req.pageUserId = $('#inputUserId').val();
        req.pageUserRole = $('#inputUserRole').val();
        req.action = 'student-subs-courses-new';
        ajaxRequest({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req),
			success:function(res) {
	            res = $.parseJSON(res);
	            if (res.status == 0)
	                toastr.error(res.message);
	            else {
	                subCourses = res.courses;
	                //fillStudentSubsCoursesMenu(res);
	                if(page == 'home' || page == 'courses') {
	                	fillStudentSubsCourses();
					}
	            }
	        }
        });
    }

    function fillStudentCoursesMenu () {
        if (courses.length>0) {
            $.each(courses, function (i, course) {
                if (i==0) {$('#navCourses .nav-link').after('<ul class="sub-menu"></ul>');}
                $('#navCourses .sub-menu').append(
                    '<li class="nav-item">'+
                        '<a href="'+sitepathStudentCourses+course.course_id+'" class="nav-link">'+
                            '<span class="title">'+course.name+'</span>'+
                        '</a>'+
                    '</li>');
            });
        };
    }

    /*function fillStudentSubsCoursesMenu () {
    	if (subCourses.length>0) {
            $.each(subCourses, function (i, course) {
                if (i==0) {$('#navSubsCourses .nav-link').after('<ul class="sub-menu"></ul>');}
                $('#navSubsCourses .sub-menu').append(
                    '<li class="nav-item">'+
                        '<a href="'+sitepathStudentCourses+course.course_id+'" class="nav-link">'+
                            '<span class="title">'+course.name+'</span>'+
                        '</a>'+
                    '</li>');
            });
        };
    }*/

	/*$('.btn-crypto-info').click(function(event) {
		getPortisInfo();
	});*/
    
    $('.btn-crypto-app').click(function(event) {
        /* Act on the event */
        $("#modalSetAddress").modal("show");
    });

    //adding event handlers for accept button
	$('body').on('click', '.invite-button', function(e){
		var id = $(this).attr('data-id');
		var elem = $('.invite-button[data-id='+id+']').closest('invite-parent');
		var thiz = $(this).closest('.invite-parent-all');
		if (id>0) {
			var action = parseInt($(this).attr('data-action'));
			var req = {};
			var res;
			req.action = ((action==1)?('accepted_student_invitation'):('rejected_student_invitation'));
			req.link_id = id;
			ajaxRequest({
				'type'  :   'post',
				'url'   :   ApiEndPoint,
				'data'  :   JSON.stringify(req),
				success:function(res){
					res = $.parseJSON(res);
					if(res.status == 0) {
						toastr.error(res.message, "Course Invites");
						elem.remove();
						if (thiz.length>0) {
							thiz.addClass('disabled');
							thiz.find('.mt-action-buttons').remove();
						}
					} else {
						var notiCount = parseInt($('#header_notification_bar .noti-count').html());
						notiCount--;
						$('#header_notification_bar .noti-count').html(notiCount);
						var message = ((action==1)?('Invitation Accepted successfully'):('Invitation Rejected successfully'));
						toastr.success(message, "Course Invites");
						elem.remove();
						if (thiz.length>0) {
							thiz.addClass('disabled');
							thiz.find('.mt-action-buttons').remove();
						}
						window.location = window.location;
					}
				}
			});
		}
	});

	//adding event handlers for chat button
	//$('.chat-button').on('click', function() {
	$('body').on('click', '.chat-button', function(){
		var id = $(this).attr('data-id');
		var elem = $('.chat-button[data-id='+id+']').closest('.chat-parent');
		var thiz = $(this).closest('.chat-parent-all');
		if (id>0) {
			var req = {};
			var res;
			req.action = 'mark-chat-notification';
			req.notificationId  = id;
			req.chatAction = $(this).attr('data-chat');
			ajaxRequest({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req),
				success:function(res){
					res = $.parseJSON(res);
					if(res.status == 0)
						toastr.error(res.message, "Chat Notifications");
					else {
						var notiCount = parseInt($('#header_inbox_bar .noti-count').html());
						notiCount--;
						$('#header_inbox_bar .noti-count').html(notiCount);
						if (req.chatAction == 0) {
							toastr.success("Notification marked as Read.", "Chat Notifications");
							elem.remove();
							if (thiz.length>0) {
								thiz.addClass('disabled');
								thiz.find('.mt-comment-actions').remove();
								thiz.find('.chat-button').prop('href',sitePath+'messenger/'+res.roomId);
								thiz.find('.chat-button').removeAttr('data-id');
								thiz.find('.chat-button').removeAttr('data-chat');
							}
						} else {
							elem.remove();
							if (thiz.length>0) {
								thiz.addClass('disabled');
								thiz.find('.mt-comment-actions').remove();
								thiz.find('.chat-button').prop('href',sitePath+'messenger/'+res.roomId);
								thiz.find('.chat-button').removeAttr('data-id');
								thiz.find('.chat-button').removeAttr('data-chat');
							}
							window.open(sitePath+'messenger/'+res.roomId);
						}
					}
				}
			});
		}
	});

});
</script>