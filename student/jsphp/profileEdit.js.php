<script type="text/javascript">
    //function to fetch user details
    function fillUserProfileForEdit() {
        //console.log(userProfile);
        $('#inputFirstName').val(userProfile.details.firstName);
        $('#inputLastName').val(userProfile.details.lastName);
        $('#inputMobilePrefix').val(userProfile.details.contactMobilePrefix);
        $('#inputMobile').val(userProfile.details.contactMobile);
        $('[name=optionGender][value='+userProfile.details.gender+']').prop("checked", true);
        $('#inputAbout').val(userProfile.details.about);
        $('#inputAchievements').val(userProfile.details.achievements);
        $('#inputInterests').val(userProfile.details.interests);
        $('#selectCountry').val(userProfile.details.country);
        $('#inputStreet').val(userProfile.details.street);
        $('#inputCity').val(userProfile.details.city);
        $('#inputState').val(userProfile.details.state);
        $('#inputPin').val(userProfile.details.pin);
        if (userProfile.details.website.length>0) {
            $('#inputWebsite').val(userProfile.details.website);
        };
        if (userProfile.details.facebook.length>0) {
            $('#inputFacebook').val(userProfile.details.facebook);
        };
        if (userProfile.details.twitter.length>0) {
            $('#inputTwitter').val(userProfile.details.twitter);
        };
        $('#userProfilePic').attr('src',userProfile.details.profilePic);
        $('#userProfilePic').attr('old-image',userProfile.details.profilePic);
        //$("#dob").datepicker("setDate", new Date());
        if (userProfile.details.dob.length>0) {
            //console.log(userProfile.details.dob);
            $("#dob").datepicker("setDate", userProfile.details.dob);
        };
    }
    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form1 = $('#formProfile');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        //IMPORTANT: update CKEDITOR textarea with actual content before submit
        form1.on('submit', function() {
            for(var instanceName in CKEDITOR.instances) {
                CKEDITOR.instances[instanceName].updateElement();
            }
        })

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                inputFirstName: {
                    minlength: 2,
                    required: true
                },
                inputLastName: {
                    minlength: 2,
                    required: true
                },  
                selectCountry: {
                    required: true
                },
                inputMobilePrefix: {
                    minlength: 2,
                    required: true
                },
                optionGender: {
                    required: true
                }
            },

            messages: { // custom messages for radio buttons and checkboxes
                optionGender: {
                    required: "Please select a Gender"
                }
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) { 
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) { 
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) { 
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) { 
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            highlight: function (element) { // hightlight error inputs
               $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success1.show();
                error1.hide();
                //form[0].submit(); // submit the form
                saveProfile();
            }

        });
    }
    handleValidation1();

    var handleValidation2 = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form2 = $('#formPassword');
        var error2 = $('.alert-danger', form2);
        var success2 = $('.alert-success', form2);

        //IMPORTANT: update CKEDITOR textarea with actual content before submit
        form2.on('submit', function() {
            for(var instanceName in CKEDITOR.instances) {
                CKEDITOR.instances[instanceName].updateElement();
            }
        })

        form2.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                inputOldPassword: {
                    minlength: 6,
                    required: true
                },
                inputNewPassword: {
                    minlength: 6,
                    required: true
                },  
                inputNewRePassword: {
                    minlength: 6,
                    required: true,
                    equalTo: "#inputNewPassword"
                }
            },

            messages: { // custom messages for radio buttons and checkboxes

            },

            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) { 
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) { 
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) { 
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) { 
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   
                success2.hide();
                error2.show();
                App.scrollTo(error2, -200);
            },

            highlight: function (element) { // hightlight error inputs
               $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();
                //form[0].submit(); // submit the form
                changePassword();
            }

        });
    }
    handleValidation2();

    //event handler to change password
    function changePassword () {
        var oldPwd = $('#inputOldPassword').val();
        var newPwd = $('#inputNewPassword').val();
        
        var req = {};
        var res;
        req.action = 'changePassword';
        req.oldPwd = oldPwd;
        req.newPwd = newPwd;
        ajaxRequest({
            'type'  :   'post',
            'url'   :   ApiEndPoint,
            'data'  :   JSON.stringify(req),
            success:function(res){
                res = $.parseJSON(res);
                if(res.status == 0)
                    toastr.error(res.message);
                else {
                    $('.alert-success', '#formPassword').show();
                    toastr.success("Password changed successfully!", "Change Password");
                }
            }
        });
    };

    function saveProfile () {
        var req = {};
        req.firstName   = $('#inputFirstName').val();
        req.lastName    = $('#inputLastName').val();
        req.mobilePrefix= $('#inputMobilePrefix').val();
        req.mobile      = $('#inputMobile').val();
        req.gender      = $('[name=optionGender]').val();
        req.achievements= $('#inputAchievements').val();
        req.interests   = $('#inputInterests').val();
        req.about       = $('#inputAbout').val();
        req.country     = $('#selectCountry').val();
        req.street      = $('#inputStreet').val();
        req.city        = $('#inputCity').val();
        req.state       = $('#inputState').val();
        req.pin         = $('#inputPin').val();
        req.website     = $('#inputWebsite').val();
        req.facebook    = $('#inputFacebook').val();
        req.twitter     = $('#inputTwitter').val();
        req.dob         = $("#dob").datepicker("getUTCDate");
        req.action      = 'save-profile-new';
        var res;
        ajaxRequest({
            'type'  :   'post',
            'url'   :   ApiEndPoint,
            'data'  :   JSON.stringify(req),
            success:function(res){
                res = $.parseJSON(res);
                if(res.status == 0)
                    toastr.error(res.message);
                else {
                    $('.alert-success', '#formProfile').show();
                    toastr.success("Successfully updated!", "Profile Update");
                }
            }
        });
    };
    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true
            });
        }
    }
    handleDatePickers();

    $('.js-remove').click(function(){
        if ($('#userProfilePic').data('Jcrop')) {
            var JcropAPI = $('#userProfilePic').data('Jcrop');
            JcropAPI.destroy();
            var originalImage = $('#userProfilePic').attr('old-image');
            $('#userProfilePic').prop('src', originalImage);
        } else {
            var req = {};
            req.action      = 'remove-profile-image';
            var res;
            ajaxRequest({
                'type'  :   'post',
                'url'   :   ApiEndPoint,
                'data'  :   JSON.stringify(req),
                success:function(res){
                    res = $.parseJSON(res);
                    if(res.status == 0)
                        toastr.error(res.message);
                    else {
                        $('#userProfilePic').prop('src', res.profilePic);
                        $('#userProfilePic').prop('old-image', res.profilePic);
                    }
                }
            });
        }
    });

    var _URL = window.URL || window.webkitURL;
    var cropCoords,
    file,
    originalImageWidth, originalImageHeight,
    uploadSize = 150,
    previewSize = 500;

    $("#fileProfilePic").on("change", function(){
        file = this.files[0];
        if (!!file) {
            $('.js-upload').removeClass('hide');
            var img;
            img = new Image();
            img.onload = function () {
                //console.log(this.width + " " + this.height);
                originalImageWidth = this.width;
                originalImageHeight = this.height;
                readFile(file, {
                    width: originalImageWidth,
                    height: originalImageHeight
                }).done(function(imgDataUrl, origImage) {
                    //$("input, img, button").toggle();
                    initJCrop(imgDataUrl);
                }).fail(function(msg) {
                    toastr.error(msg);
                });
            };
            img.src = _URL.createObjectURL(file);
        } else {
            $('.js-upload').addClass('hide');
        }
    });

    $("#btnUpload").on("click", function(){
        var thiz = $(this);
        $(this).text("Uploading...").prop("disabled", true);

        readFile(file, {
            width: uploadSize,
            height: uploadSize,
            crop: cropCoords
        }).done(function(imgDataURI) {
            var data = new FormData();
            var blobFile = dataURItoBlob(imgDataURI);
            data.append('profile-pic', blobFile);
            
            ajaxRequest({
                url: FileApiPoint,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                dataType: 'json',
                success: function(res) {
                    if (res.status == 1) {
                        thiz.text('Upload');
                        var JcropAPI = $('#userProfilePic').data('Jcrop');
                        JcropAPI.destroy();
                        $('#userProfilePic').prop('src', res.profilePic);
                        $('#userProfilePic').attr('old-image',res.profilePic);
                        $('.js-upload').addClass('hide');
                    } else {
                        toastr.error(res.msg);
                    }
                },
                error: function(xhr) {
                    toastr.error(blobFile);
                }
            });
        });
    });

    /*****************************
    show local image and init JCrop
    *****************************/
    var initJCrop = function(imgDataUrl){
        var img = $("img#userProfilePic").attr("src", imgDataUrl);
        
        var storeCoords = function(c) {
            //console.log(c);
            cropCoords = c;
        };
        
        var w = img.width();
        var h = img.height();
        var s = uploadSize;
        img.Jcrop({
            onChange: storeCoords,
            onSelect: storeCoords,
            aspectRatio: 1,
            setSelect: [(w - s) / 2, (h - s) / 2, (w - s) / 2 + s, (h - s) / 2 + s],
            trueSize: [originalImageWidth, originalImageHeight]
        });
    };

    $(document).ready(function() {
        $(".bc-control").change(function(event) {
            /* Act on the event */
            $(this).attr('data-changed', 1);
        });

        $("#btnSaveBlockchain").click(function(event) {
            /* Act on the event */
            /*var firstName   = $('#inputFirstName').val();
            var lastName    = $('#inputLastName').val();
            var country     = $('#selectCountry').find("option:selected").text();
            var interests   = $('#inputInterests').val();
            var achievements   = $('#inputAchievements').val();
            registerStudentOnBlockchain(firstName,lastName,country,'',interests,achievements);*/
            var firstName   = $('#inputFirstName').val();
            var lastName    = $('#inputLastName').val();
            var country     = $('#selectCountry').find("option:selected").text();
            var info        = $('#inputAbout').val();
            var interests   = $('#inputInterests').val();
            var achievements   = $('#inputAchievements').val();
            if (!firstName) {
                toastr.error("First Name is compulsory");
            } else if (!lastName) {
                toastr.error("Last Name is compulsory");
            } else if (!country) {
                toastr.error("Country is compulsory");
            } else {
                var countryControl = infoControl = interestsControl = achievementsControl = controls = 0;
                $(".bc-control[data-changed=1]").each(function(index, el) {
                    switch ($(this).attr('id')) {
                        case 'selectCountry':countryControl++;controls++;break;
                        case 'inputAbout':infoControl++;controls++;break;
                        case 'inputInterests':interestsControl++;controls++;break;
                        case 'inputAchievements':achievementsControl++;controls++;break;
                    };
                });
                var fnBlockchain = "";
                if (controls>0) {
                    switch(controls) {
                        case 1:
                            if (countryControl>0) {
                                fnBlockchain = "addCountry";
                            } else if (infoControl>0) {
                                fnBlockchain = "addInfo";
                            } else if (interestsControl>0) {
                                fnBlockchain = "addInterests";
                            } else if (achievementsControl>0) {
                                fnBlockchain = "addAchievements";
                            }
                            break;
                        case 2:
                            if (countryControl>0 && infoControl>0) {
                                fnBlockchain = "addCountryInfo";
                            } else if (infoControl>0 && interestsControl>0) {
                                fnBlockchain = "addInfoInterests";
                            } else if (interestsControl>0 && achievementsControl>0) {
                                fnBlockchain = "addInterestsAchievements";
                            } else if (achievementsControl>0 && countryControl>0) {
                                fnBlockchain = "addAchievementsCountry";
                            } else if (countryControl>0 && interestsControl>0) {
                                fnBlockchain = "addCountryInterests";
                            }
                            break;
                        case 3:
                            if (countryControl>0 && infoControl>0 && interestsControl>0) {
                                fnBlockchain = "addCountryInfoInterests";
                            } else if (infoControl>0 && interestsControl>0 && achievementsControl>0) {
                                fnBlockchain = "addInfoInterestsAchievements";
                            } else if (interestsControl>0 && achievementsControl>0 && countryControl>0) {
                                fnBlockchain = "addInterestsAchievementsCountry";
                            } else if (achievementsControl>0 && countryControl>0 && infoControl>0) {
                                fnBlockchain = "addAchievementsCountryInfo";
                            }
                            break;
                        case 4:
                            if (countryControl>0 && infoControl>0 && interestsControl>0 && achievementsControl>0) {
                                fnBlockchain = "addCountryInfoInterestsAchievements";
                            }
                            break;
                    }
                }
                registerStudentOnBlockchain(firstName,lastName,country,info,interests,achievements,fnBlockchain);
            }
        });
    });
</script>