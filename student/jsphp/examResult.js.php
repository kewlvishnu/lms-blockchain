<script type="text/javascript">
var details;
var sections;
var count = 0;
var sectionCount = 0;
var ch =['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
var totalScore = 0;
var sectionTotal = [];
var sectionScore = [];
var show=1;
var stuname='';
var heroBar1,heroBar2;

//function to fetch breadcrumb
function fetchexamShow() {
	var req = {};
	var res;
	req.action = 'check-result-show';
	req.examId = examId;
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else {
				var today= $.now();
				if((res.exam[0].showResultTill == 2 && today< res.exam[0].showResultTillDate)||res.exam[0].showResultTill == 1)
				{
					show=1;
				}
				else{
					show=2;
					toastr.error("Result cannot be shown this time as last day passed");
					window.location = sitePathStudent;
					return false;
				}
				if(res.exam[0].showResult == 'after' && res.exam[0].endDate!="" )
				{
					// if(today > res.exam[0].endDate && show==1) changed this
					//console.log(today +'<'+ res.exam[0].endDate +'&&'+ show);
					if(today > res.exam[0].endDate && show==1)
					{
						fetchAttemptDetails();
					}
					else{
						toastr.error("Submitted sucessfully. Result will be shown after Exams/Assignments end date.");
						//window.location = sitePathStudent;
						return false;
					}
				}else{
					fetchAttemptDetails();
				}
			}
	});
}

//function to get attempt details
function fetchAttemptDetails() {
	var req = {};
	var res;
	var attemptNo = attemptId;
	if(examId != undefined) {
		req.action = 'get-result';
		req.examId = examId;
		if(attemptNo == undefined)
			req.attemptNo = 0;
		else
			req.attemptNo = attemptNo;
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				toastr.error(res.message);
			else {
				$('.attempt-no').text(res.attemptNo);
				attemptId = res.result.id;
				fillAttemptDetails(res);
				fetchTopperComparison();
				initGraph(res);
				//fetchAllStudentGraph();
				if(res.totalAttempts > 1) {
					$('.attempt-block').removeClass('hide');
					//fetchGraph();
					//initGraph(res);
					
					if(res.attemptNo > 1) {
						$('.previous-attempt').attr('href', sitePathStudent+'exams/' + examId + '/result/attempt/' + (res.totalAttempts - res.attemptNo + 1)).removeAttr('disabled');
					}
					if(res.attemptNo != res.totalAttempts) {
						if ((res.totalAttempts - res.attemptNo) >1 ) {
							$('.next-attempt').attr('href', sitePathStudent+'exams/' + examId + '/result/attempt/' + (res.totalAttempts - res.attemptNo - 1)).removeAttr('disabled');
						} else {
							$('.next-attempt').attr('href', sitePathStudent+'exams/' + examId + '/result/').removeAttr('disabled');
						}
					}
				}
			}
		});
	}
	else
		window.location = 'profile.php';
}

//function to fill attempt details
function fillAttemptDetails(data) {
	sections = data.sections;
	var percentage = round2(data.result.percentage);
	$('#percentage').text(percentage).attr("data-value", percentage);
	if (data.ranges) {
		var ranges = data.ranges;
		for (var i = 0; i < ranges.length; i++) {
			if (percentage>=ranges[i].percentLow && ((ranges[i].percentHigh!=100 && percentage<ranges[i].percentHigh) || (ranges[i].percentHigh==100 && percentage<=ranges[i].percentHigh))) {
				$('#comment').append(
					'<div>Instructor Comment: '+ranges[i].comment+' <img src="'+sitepathStudentIncludes+'assets/custom/img/smileys/'+ranges[i].file+'" class="img-smiley" alt="" /></div>').removeClass('hide');
			}
			
		}
	}
	$('#rank').text(data.rank + '/' + data.totalRank).attr("data-value", data.rank);
	$('#score').text(data.result.score + '/');
	var startDate = new Date(parseInt(data.result.startDate+'000'));
	var start = startDate.getDate() + '-' + month[startDate.getMonth()].substring(0,3) + '-' + startDate.getFullYear() + ' ' + formatTime2Digits(startDate.getHours()) + ':' + formatTime2Digits(startDate.getMinutes());
	$('#startDate').html(start);
	if(data.result.endDate == 0)
		var end = 'Not completed';
	else {
		var endDate = new Date(parseInt(data.result.endDate+'000'));
		var end = endDate.getDate() + '-' + month[endDate.getMonth()].substring(0,3) + '-' + endDate.getFullYear() + ' ' + formatTime2Digits(endDate.getHours()) + ':' + formatTime2Digits(endDate.getMinutes());
	}
	$('#endDate').html(end);
	var time = convertSecondsIntoMinutes(data.result.time);
	$('#timeTaken').text(time);
	var html = '';
	for(var i = 0; i < data.sections.length; i++) {
		var pie = [];
		html = '<tr>'+
				'<td>'+data.sections[i].name+'</td>'+
				'<td class="text-center">'+data.sections[i].questions+'</td>'+
				'<td class="text-center"></td>'+
				'<td>'+convertSecondsIntoMinutes(data.sections[i].time)+'</td>'+
				'<td>'+
					'<div class="progress">'+
                        '<div class="progress-bar bg-green-jungle" style="width: 30%">'+
                            '<span> 3 </span>'+
                            '<span class="sr-only"> 30% Complete (success) </span>'+
                        '</div>'+
                        '<div class="progress-bar bg-yellow-casablanca" style="width: 20%">'+
                            '<span> 2 </span>'+
                            '<span class="sr-only"> 20% Complete (danger) </span>'+
                        '</div>'+
                        '<div class="progress-bar bg-dark" style="width: 10%">'+
                            '<span> 1 </span>'+
                            '<span class="sr-only"> 10% Complete (black) </span>'+
                        '</div>'+
                    '</div>'+
					//+ '<div class="pie-marks"><canvas height="20" width="47" style="display: inline-block; width: 47px; height: 20px; vertical-align: top;"></canvas></div>'
				'</td>'+
			'</tr>';
		$('#sectionTable').append(html);
		var count = 0;
		for(var j = 0; j < 3; j++) {
			var n = 0;
			if(data.sections[i].checks[count] != undefined) {
				if(data.sections[i].checks[count].check == j) {
					n = data.sections[i].checks[count].number;
					count++;
				}
				else {
					n = 0;
				}
			}
			pie.push(n);
		}
		//pie contains counts of unattempted, right, wrong (in same order)
		//calculating percentage
		var total = parseInt(pie[0]) + parseInt(pie[1]) + parseInt(pie[2]);
		var up = (pie[0] / total) * 100;
		var rp = (pie[1] / total) * 100;
		var wp = (pie[2] / total) * 100;
		$('#sectionTable .progress:eq(-1) .bg-green-jungle').css('width', rp+'%');
		$('#sectionTable .progress:eq(-1) .bg-green-jungle').find('span').text(pie[1]);
		$('#sectionTable .progress:eq(-1) .bg-yellow-casablanca').css('width', wp+'%');
		$('#sectionTable .progress:eq(-1) .bg-yellow-casablanca').find('span').text(pie[2]);
		$('#sectionTable .progress:eq(-1) .bg-dark').css('width', up+'%');
		$('#sectionTable .progress:eq(-1) .bg-dark').find('span').text(pie[0]);

		//to generate category table
		var catHtml = '<div class="table-reponsive">'+
						'<table class="table table-bordered no-margin">'+
							'<thead>'+
								'<tr class="bg-green-turquoise font-white">'+
									'<th colspan="4">' + data.sections[i].name + ': </th>'+
								'</tr>'+
								'<tr class="bg-green font-white">'+
									'<th width="24%">No of questions</th>'+
									'<th width="56%">Question Type</th>'+
									'<th width="10%">Correct</th>'+
									'<th width="10%">Wrong</th>'+
								'</tr>'+
							'</thead>'+
							'<tbody>';
		for(var j = 0; j < data.sections[i].categories.length; j++) {
			catHtml += '<tr>'
						+ '<td>'+data.sections[i].categories[j].required+'</td>';
			if(data.sections[i].categories[j].questionType == 0)
				catHtml += '<td>Multiple Choice questions</td>';
			else if(data.sections[i].categories[j].questionType == 1)
				catHtml += '<td>True/False</td>';
			else if(data.sections[i].categories[j].questionType == 2)
				catHtml += '<td>Fill in the blanks</td>';
			else if(data.sections[i].categories[j].questionType == 3)
				catHtml += '<td>Match the following</td>';
			else if(data.sections[i].categories[j].questionType == 4)
				catHtml += '<td>Comprehensive Questions</td>';
			else if(data.sections[i].categories[j].questionType == 5)
				catHtml += '<td>Dependent Questions</td>';
			else if(data.sections[i].categories[j].questionType == 6)
				catHtml += '<td>Multiple Answer Questions</td>';
			catHtml += '<td>+'+data.sections[i].categories[j].correct+'</td>'
						+ '<td>-'+data.sections[i].categories[j].wrong+'</td>'
					+ '</tr>';
		}
		catHtml +=		'</tbody>'+
					'</table>'+
				'</div>';
		$('#categories .category-table').append(catHtml);
	}
	fetchSectionDetails();
	//fetchAllStudentGraph();
}

//function to fetch data for graph
function fetchGraph() {
	var req = {};
	var res;
	req.action = 'get-attempt-graph';
	req.examId = examId;
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else
			initGraph(res);
	});
}

//function to initiate attempt graph
function initGraph(data) {
	fetchStudentTimeLine();
	/*var filler = {};
	var filler1={};
	filler.labels = [];
	filler.datasets = [];
	filler.datasets[0] = {};
	filler1.labels = [];
	filler1.datasets = [];
	filler.datasets[0].label = 'Score';
	filler.datasets[0].fillColor = 'rgba(25, 113, 184, 0)';
	filler.datasets[0].strokeColor = "rgba(25, 113, 184, 1)";
	filler.datasets[0].pointColor = "rgba(25, 113, 184, 1)";
	filler.datasets[0].pointStrokeColor = "#fff";
	filler.datasets[0].pointHighlightFill = "#fff";
	filler.datasets[0].pointHighlightStroke = "rgba(25, 113, 184, 1)";
	filler.datasets[0].data = [];
	filler1.datasets[0] = {};
	filler1.datasets[0].label = 'Percentage';
	filler1.datasets[0].fillColor = 'rgba(25, 184, 75, 0)';
	filler1.datasets[0].strokeColor = "rgba(25, 184, 75, 1)";
	filler1.datasets[0].pointColor = "rgba(25, 184, 75, 1)";
	filler1.datasets[0].pointStrokeColor = "#fff";
	filler1.datasets[0].pointHighlightFill = "#fff";
	filler1.datasets[0].pointHighlightStroke = "rgba(151,187,205,1)";
	filler1.datasets[0].data = [];
	for(var i = 0; i < data.graph.length; i++) {
		filler.labels.push("Attempt No " + (i+1));
                    filler1.labels.push("Attempt No " + (i+1));
		filler.datasets[0].data.push(data.graph[i].score);
		filler1.datasets[0].data.push(data.graph[i].percentage);
	}*/
}

function fetchAllStudentGraph() {
	var req = {};
	var res;
	req.action = 'get-allStudentNormalization-graph';
	req.examId = examId;
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else
			initAllStudentGraph(res);
	});
}

//function to initiate attempt graph
function initAllStudentGraph(data) {
	var differnece=totalScore-parseFloat(data.highest);
	var blocksDiff=(parseFloat(totalScore)-parseFloat(differnece+parseFloat(data.lowest)))/10;
	//var marksDis=[];
	var students=[];
	var markPer=[];
	var newlowest=(parseFloat(data.lowest)+parseFloat(differnece)).toFixed(1);
	for(var i=1;i<11;i++)
	{	//marksDis[i-1]=(parseFloat(newlowest)+parseFloat(blocksDiff*i)).toFixed(1);
		students[i-1]=0;
		//checking
		var temp=(parseFloat(newlowest)+parseFloat(blocksDiff*i)).toFixed(1);
		markPer[i-1]=Math.round((temp/totalScore)*100)+'%';
		//markPer[i-1]=markPer[i-1]+'%';
	}
	for(var i=markPer.length-1;i>0;i--)
	{
		markPer[i]= markPer[i-1]+' - '+markPer[i];
	}
	
	markPer[0]=Math.round(newlowest)+'% - '+markPer[0];
	$.each( data.graph, function( key, value ) {
		var newscore=(parseFloat(value['score'])+parseFloat(differnece)).toFixed(1);
		var index=Math.round(((newscore-newlowest)/blocksDiff));
		if(index>0)
		{	index=index-1;
		}else{
			index=0;
		}
		students[index]=students[index]+1;
		
		
	});
	$('#hero-bar3').highcharts({
		chart: {
			zoomType: 'xy'
		},
		title: {
			text: 'Percentage distribution of all students'
		},
		subtitle: {
			text: 'This graph shows percentage w.r.t to highest scorer'
		},
		xAxis: [
		{
			title: {
				text: 'Percentage Score',
				style: {
					color: Highcharts.getOptions().colors[1]
				}
			},
			categories: markPer,
			crosshair: true
		}],
		yAxis: [{ // Primary yAxis
			labels: {
				format: '{value}',
				style: {
					color: Highcharts.getOptions().colors[1]
				}
			},
			title: {
				text: 'No. of Students',
				style: {
					color: Highcharts.getOptions().colors[1]
				}
			}
		}, { // Secondary yAxis
			title: {
				text: 'Students',
				style: {
					color: Highcharts.getOptions().colors[0]
				}
			},
			labels: {
				format: '{value}',
				style: {
					color: Highcharts.getOptions().colors[0]
				}
			},
			opposite: true
		}],
		tooltip: {
			shared: true
		},
		legend: {
			layout: 'vertical',
			align: 'left',
			x: 120,
			verticalAlign: 'top',
			y: 100,
			floating: true,
			backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
		},
		series: [{
			name: 'Students',
			type: 'column',
			yAxis: 1,
			data: students,
			tooltip: {
				valueSuffix: ''
			}

		}, {
			name: 'No. of Students',
			type: 'spline',
			yAxis: 1,
			data: students,
			tooltip: {
				valueSuffix: ''
			}
		}]
	});
}

function fetchStudentTimeLine() {
	var req = {};
	var res;
	req.action = 'getAllStudentTimeLineGraph';
	req.examId = examId;
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else
			initStudentTimeLine(res);
	});
}

//function to get graphs vs time graph attempt independent
function initStudentTimeLine(data) {
	/**
	 * Create the chart when all data is loaded
	 * @returns {undefined}
	 */
	var timepoints=[];
	var timepoints2=[];
	var selfMarks=[];
	var dataset = [];
	var avgStudentCount=[];
	var avgStudentMarks=[];
	var toppermarks=[];
	var avgMarks=[];
	var attempts=[];
	var lastday=$.now();//parseInt(data.lastday[0]['endDate'])* parseInt(1000);
	
	$.each(data.self, function (j, self) {
		timepoints[j]=parseInt(self.endDate*1000);
		var element=parseInt(j)+parseInt(1);
		attempts[j]='Attemp NO. '+(element);
		selfMarks[j]=parseInt(self.score);
	});
	$.each(data.avgScore, function (j, avgScore) {
		avgStudentCount[j]=0;
		timepoints2[j]=0;
		avgStudentMarks[j]=0;
		toppermarks[j]=0;
	});
	var lastpoint=timepoints[timepoints.length-1];
	var lastflag=0;
	$.each(data.avgScore, function (j, avgScore) {
		timepoints2[j]=parseInt(avgScore.endDate)* parseInt(1000);
		avgStudentMarks[j]=parseFloat(avgStudentMarks[j])+parseFloat(avgScore.score);
		avgStudentCount[j]=parseInt(avgStudentCount[j])+parseInt(1);
		//calculating for toppermarks
		if(j!=0)
		{
			//to calculate highest from previous
			if(avgScore.score>toppermarks[j-1])
			{
				toppermarks[j]=parseFloat(avgScore.score);
			}else{
				toppermarks[j]=parseFloat(toppermarks[j-1]);
			}
		}else{
		
			toppermarks[j]=parseFloat(avgScore.score);
		}			
		
	});
	for(var i=0;i<data.avgScore.length;i++){
		if(avgStudentCount[i]!=0)
			avgMarks[i]=parseFloat(avgStudentMarks[i])/parseFloat(avgStudentCount[i]);
		else
			avgMarks[i]=0;				
	}
	//we got avg as whole it will same for all students	
		
	timepoints2[timepoints2.length]=parseInt(lastday);
	
	avgMarks[avgMarks.length]=parseFloat(avgMarks[avgMarks.length-1]);
	toppermarks[toppermarks.length]=parseFloat(toppermarks[toppermarks.length-1]);
		
	var jCompare=0;
	var flag=0;

	for(var k=0;k<timepoints.length;)
	{
		for(var i=0;i<timepoints2.length-1;i++)
		{	
			if(timepoints[k]==timepoints2[i])
			{
				break;
			}	
			else if(timepoints[k]>timepoints2[i] && timepoints[k]<timepoints2[i+1] )
			{		
				if(timepoints2[i]>timepoints[k])
				{
					var highmarks=parseFloat(toppermarks[i+1]);
					var avg=(parseFloat(avgMarks[i+1]));
					timepoints2.splice(i, 0,timepoints[k]);
					toppermarks.splice(i, 0,highmarks );
					avgMarks.splice(i, 0,avg );
					break;
				}
				else{
					var highmarks=parseFloat(toppermarks[i+1]);
					var avg=(parseFloat(avgMarks[i+1]));
					timepoints2.splice(i+1, 0,timepoints[k]);
					toppermarks.splice(i+1, 0,highmarks );
					avgMarks.splice(i+1, 0,avg );
					break;
				}
			}
			else{
			}
		}
		k++;
	}
	for(var i=0;i<timepoints2.length-1;i++)
	{
		if(timepoints2[i]==timepoints2[i+1])
		{
			timepoints2.splice(i, 1);
			avgMarks.splice(i, 1);
			toppermarks.splice(i,1);
		}
		
	}

	var scoresOptions = [],
		scoresCounter = 0,
		newnames = ['self','avgScore','topper','attempts'];
	function createChart2() {
		var i=0;
		
		$('#highchart_1').highcharts('StockChart', {
			rangeSelector: {
				selected: 4
			},
			chart : {
				style: {
					fontFamily: 'Open Sans'
				}
			},
			xAxis: {

			},
			yAxis: [{
					labels: {
						align: 'left',
						x: 15,
						formatter: function () {
							return  this.value ;
						}
					},
					//min: 0,
					plotLines: [{
						value: 0,
						width: 2,
						color: 'silver'
					}]
				},
				{ // Secondary yAxis
				labels: {
					formatter: function () {
						return  this.value ;
						//return point.y;
					}
				},
				linkedTo:0,
				opposite: false
			}],
			plotOptions: {
				series: {
					marker : {
						enabled : true,
						radius : 3
					}
				}
			},
			tooltip: {
				pointFormat: '<span >{series.name}</span>: <b>{point.y}</b><br/>'
			},
			legend: {
				enabled: true,
				layout: 'vertical',
				verticalAlign: 'middle',
				borderWidth: 0
			},
			series: scoresOptions
		});
	}
	
	//declaring array for data points calculation
	var elementarray1=[];
	var elementarray2=[];
	var elementarray3=[];
	var elementarray4=[];
	// console.log(timepoints2);
	// calculating for self array
	for(var i = 0; i < timepoints.length; i++) {
		elementarray=[];
		elementarray[0] = parseInt(timepoints[i]);
		elementarray[1] = parseInt(selfMarks[i]);
		elementarray1[i]=elementarray;
	}
	scoresOptions[0] = {
							name: 'Your Score',
							data: elementarray1,
							lineWidth: 3
							//type: 'spline'
						};
	//calculating datapoint for avg 
	for(var i = 0; i < avgMarks.length; i++) {
		elementarray=[];
		elementarray[0] = parseInt(timepoints2[i]);
		elementarray[1] = parseInt(avgMarks[i]);
		elementarray2[i]=elementarray;
		
	}
	scoresOptions[1] = {
							name: 'Class Average',
							data: elementarray2,
							dashStyle: 'longdash',
							type: 'spline'
						};
	for(var i = 0; i < toppermarks.length; i++) {
		elementarray=[];
		elementarray[0] = parseInt(timepoints2[i]);
		elementarray[1] = parseInt(toppermarks[i]);
		elementarray3[i]=elementarray;
	}
	scoresOptions[2] = {
							name: 'Class Highest',
							data: elementarray3,
							type: 'spline',
							dashStyle:'ShortDash'
						};
	for(var i = 0; i < timepoints.length; i++) {
		elementarray=[];
		elementarray[0] = parseInt(timepoints[i]);;
		elementarray[1]= parseInt(i+1);;
		elementarray4[i]=elementarray;
	}
	scoresOptions[3] = {
							name: 'Attempt No ',
							data: elementarray4,
							type: 'spline',
							dashStyle:'ShortDash',
							color: '#FFFFFF',
							lineWidth: 0.4,
							showInLegend: false
						};
	
		
	createChart2();	
				
}

//function to fetch each section questions and answers
function fetchSectionDetails() {
	var req = {};
	var res;
	req.action = 'get-section-questions';
	req.sectionId = sections[count].sectionId;
	req.attemptId = attemptId;
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else {
			fillSectionDetails(res);
			if(count == sections.length - 1) {
				$('#score').text($('#score').text() + totalScore);
				MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
			}
			else {
				count++;
				fetchSectionDetails();
			}
		}
	});
}

//function to create section view
function fillSectionDetails(data) {
	var parentId = 0;
	html = '<section class="panel panel-success panel-result no-space">'
			+ '<header class="panel-heading">'
				+ '<a href="javascript:void(0)" class="hide-section text-uppercase"><strong><i class="fa fa-minus-square"></i> '+sections[sectionCount].name+'</strong></a>'
			+ '</header>'
			+ '<div class="panel-body no-space">'
				+ '<section class="panel">'
					+ '<table class="table no-margin">'
						+ '<tbody>';
	sectionTotal[sectionCount] = 0;
	sectionScore[sectionCount] = 0;
	for(var i = 0; i < data.questions.length; i++) {
		//calculting section marks
		sectionTotal[sectionCount] += parseFloat(data.questions[i].correct);
		var tempType = '';
		var buttonText = '';
		if(data.questions[i].questionType == 0)
			tempType = ' <strong> ( Multiple Choice Questions) </strong>';
		else if(data.questions[i].questionType == 1)
			tempType = ' <strong> ( True and False ) </strong>';
		else if(data.questions[i].questionType == 2)
			tempType = ' <strong> ( Fill in the blanks ) </strong>';
		else if(data.questions[i].questionType == 3)
			tempType = ' <strong> ( Match the following ) </strong>';
		else if(data.questions[i].questionType == 5) {
			tempType = ' <strong> ( Dependent type questions ) </strong>';
			buttonText = 'View Sub-Parts';
		}
		else if(data.questions[i].questionType == 6)
			tempType = ' <strong> ( Multiple Answer Questions ) </strong>';
		//paragraph adding for CMP questions
		if(data.questions[i].parentId != 0 && data.questions[i].parentId != parentId) {
			parentId = data.questions[i].parentId;
			html += '<tr><td colspan="4">Q.' + data.questions[i].parent + ' <strong> ( Comprehensive type questions ) </strong></td></tr>';
		}
		html += '<tr data-id="'+data.questions[i].questionId+'">';
		if(data.questions[i].parentId != 0) {
			html += '<td width="5%"></td>';
		}
		html += '<td width="5%">Q.' + (i+1) + '<div class="answer-status">';
		if(data.questions[i].check == 1) {
			html += '<i class="fa fa-check-circle-o text-success"></i>';
			sectionScore[sectionCount] += parseFloat(data.questions[i].correct);
		}
		else if(data.questions[i].check == 0)
			html += '<i class="fa fa-dot-circle-o text-warning"></i>';
		else {
			html += '<i class="fa fa-times-circle-o text-danger"></i>';
			sectionScore[sectionCount] -= parseFloat(data.questions[i].wrong);
		}
		//console.log(data.questions[i].question);
		//console.log(data.questions[i].question.substring(0, data.questions[i].question.length - 5));
		//var question = data.questions[i].question.substring(0, data.questions[i].question.length - 5) + tempType + '</p>';
		var question = data.questions[i].question + '<p>' + tempType + '</p>';
		if(buttonText == '')
			buttonText = 'View Solution';
		html += '</h4></td>';
		if(data.questions[i].parentId == 0)
			html += '<td colspan="2">';
		else
			html += '<td>';
		html += '<div class="question">' + question + '</div>';
		if(data.questions[i].questionType == 3) {
			html += '<div>';
			var temp = [];
			for(var j = 0; j < data.questions[i].origAnswer.length; j++) {
				temp.push(data.questions[i].origAnswer[j].columnB);
			}
			temp = shuffle(temp);
			for(var j = 0; j < data.questions[i].origAnswer.length; j++) {
				html += ch[j] + ') ' + data.questions[i].origAnswer[j].columnA + ' -> ' + temp[j] + '<br>';
			}
			html += '</div>';
		}
		html += '<div class="form-group">'+
					'<button class="btn btn-info btn-sm view-solution" data-hidden="0"  data-text="' + buttonText + '">' + buttonText +'</button>'+
				'</div>'+
				'<div class="answer well well-sm" style="display: none;">';
		totalScore += parseFloat(data.questions[i].correct);
		if(data.questions[i].questionType == 0) {
			if (data.questions[i].origAnswer.length>0) {
				html+='<ul class="list-unstyled">';
				for(var j = 0; j < data.questions[i].origAnswer.length; j++) {
					var origAnswer = data.questions[i].origAnswer[j].option;
					if(data.questions[i].check == 0)
						var userAnswer = '';
					else {
						var userAnswer = '';
						if (data.questions[i].userAnswer.length>0) {
							var userAnswer = data.questions[i].userAnswer[0].answer;
						}
					}
					html += '<li>'+ch[j] + ') ' + data.questions[i].origAnswer[j].option;
					if(data.questions[i].origAnswer[j].correct == 1)
						html += '<i class="fa fa-check-circle-o text-success"></i>';
					else if(origAnswer == userAnswer && data.questions[i].check != 0) {
						html += '<i class="fa fa-times-circle-o text-danger"></i>';
					}
					html += '</li>';
				}
				html+='</ul>';
			};
		}
		else if(data.questions[i].questionType == 1) {
			var origAnswer = data.questions[i].origAnswer[0].answer;
			if(data.questions[i].check == 0)
				var userAnswer = '';
			else {
				var userAnswer = '';
				if (data.questions[i].userAnswer.length>0) {
					var userAnswer = data.questions[i].userAnswer[0].answer;
				}
			}
			html += 'True';
			if(origAnswer == 1) {
				html += ' <i class="fa fa-check-circle-o text-success"></i>';
			}
			else if(userAnswer == 1 && data.questions[i].check != 0)
				html += ' <i class="fa fa-times-circle-o text-danger"></i>';
			html += '<br/>False';
			if(origAnswer == 0) {
				html += ' <i class="fa fa-check-circle-o text-success"></i>';
			}
			else if(userAnswer == 0 && data.questions[i].check != 0)
				html += ' <i class="fa fa-times-circle-o text-danger"></i>';
		}
		else if(data.questions[i].questionType == 2) {
			if (data.questions[i].origAnswer.length>0) {
				html+='<ul class="list-unstyled">';
				for(var j = 0; j < data.questions[i].origAnswer.length; j++) {
					var origAnswer = data.questions[i].origAnswer[j].blanks.toLowerCase().trim();
					if(data.questions[i].check == 0)
						var userAnswer = '';
					else {
						var userAnswer = '';
						if (data.questions[i].userAnswer.length>0) {
							var userAnswer = data.questions[i].userAnswer[j].answer.toLowerCase().trim();
						}
					}
					html += '<li>' + userAnswer;
					if(origAnswer == userAnswer)
						html += '<i class="fa fa-check-circle-o text-success"></i>&emsp;' + data.questions[i].origAnswer[j].blanks;
					else if(data.questions[i].check != 0) {
						html += '<i class="fa fa-times-circle-o text-danger"></i>&emsp;' + data.questions[i].origAnswer[j].blanks;
					}
					html += '</li>';
				}
				html+='</ul>';
			};
		}
		else if(data.questions[i].questionType == 3) {
			if (data.questions[i].origAnswer.length>0) {
				html+='<ul class="list-unstyled">';
				for(var j = 0; j < data.questions[i].origAnswer.length; j++) {
					var origAnswer = data.questions[i].origAnswer[j].columnB;
					if(data.questions[i].check == 0)
						var userAnswer = '';
					else {
						var userAnswer = '';
						if (data.questions[i].userAnswer.length>0) {
							var userAnswer = data.questions[i].userAnswer[j].answer;
						}
					}
					html += '<li>' + ch[j] + ') ' + data.questions[i].origAnswer[j].columnA + ' -> ' + origAnswer + '&emsp;';
					if(origAnswer == userAnswer)
						html += '<i class="fa fa-check-circle-o text-success"></i>';
					else if(data.questions[i].check != 0){
						html += '<i class="fa fa-times-circle-o text-danger"></i>&emsp;' + userAnswer;
					}
					html += '</li>';
				}
				html+='</ul>';
			};
		}
		else if(data.questions[i].questionType == 6) {
			if (data.questions[i].origAnswer.length>0) {
				html+='<ul class="list-unstyled">';
				for(var j = 0; j < data.questions[i].origAnswer.length; j++) {
					var origAnswer = data.questions[i].origAnswer[j].option;
					html += '<li>' + ch[j] + ') ' + data.questions[i].origAnswer[j].option + '&emsp;';
					if(data.questions[i].origAnswer[j].correct == 1)
						html += '<i class="fa fa-check-circle-o text-success"></i>';
					for(var k = 0; k < data.questions[i].userAnswer.length; k++) {
						if(data.questions[i].check == 0)
							var userAnswer = '';
						else {
							var userAnswer = '';
							if (data.questions[i].userAnswer.length>0) {
								var userAnswer = data.questions[i].userAnswer[k].answer;
							}
						}
						if(origAnswer == userAnswer && data.questions[i].origAnswer[j].correct == 0) {
							html += '<i class="fa fa-times-circle-o text-danger"></i>';
						}
					}
					html += '</li>';
				}
				html+='</ul>';
			};
		}
		else if(data.questions[i].questionType == 5) {
			for(var j = 0; j < data.questions[i].childs.length; j++) {
				var tempType = '';
				if(data.questions[i].childs[j].questionType == 0)
					tempType = ' <strong> ( Multiple Choice Questions) </strong>';
				else if(data.questions[i].childs[j].questionType == 1)
					tempType = ' <strong> ( True and False ) </strong>';
				else if(data.questions[i].childs[j].questionType == 2)
					tempType = ' <strong> ( Fill in the blanks ) </strong>';
				else if(data.questions[i].childs[j].questionType == 6)
					tempType = ' <strong> ( Multiple Answer Questions ) </strong>';
				var question = data.questions[i].childs[j].question.substring(0, data.questions[i].childs[j].question.length - 5) + tempType + '</p>';
				html += '<div class="question"><hr><strong>Part ' + (j+1) + '</strong>' + question + '</div>'+
						'<div class="answer-container">'+
							'<div class="form-group">'+
								'<button class="btn btn-info btn-xs view-sol" data-hidden="0">View Solution</button>'+
							'</div>'+
							'<div class="sub-answer well well-sm" style="display: none;">'+
								'<p><strong>Answers</strong></p>';
				if(data.questions[i].childs[j].questionType == 0) {
					if (data.questions[i].childs[j].origAnswer.length>0) {
						html+='<ul class="list-unstyled">';
						for(var k = 0; k < data.questions[i].childs[j].origAnswer.length; k++) {
							var origAnswer = data.questions[i].childs[j].origAnswer[k].option;
							if(data.questions[i].check == 0)
								var userAnswer = '';
							else {
								if(data.questions[i].childs[j].userAnswer.length > 0)
									var userAnswer = data.questions[i].childs[j].userAnswer[0].answer;
								else
									var userAnswer = '';
							}
							html += '<li>' + ch[k] + ') ' + data.questions[i].childs[j].origAnswer[k].option + '&emsp;';
							if(data.questions[i].childs[j].origAnswer[k].correct == 1)
								html += '<i class="fa fa-check-circle-o text-success"></i>';
							else if(origAnswer == userAnswer && data.questions[i].check != 0) {
								html += '<i class="fa fa-times-circle-o text-danger"></i>';
							}
							html += '</li>';
						}
						html+='</ul>';
					}
				}
				else if(data.questions[i].childs[j].questionType == 1) {
					var origAnswer = data.questions[i].childs[j].origAnswer[0].answer;
					if(data.questions[i].check == 0)
						var userAnswer = '';
					else {
						if(data.questions[i].childs[j].userAnswer.length > 0)
							var userAnswer = data.questions[i].childs[j].userAnswer[0].answer;
						else
							var userAnswer = '';
					}
					html += 'True';
					if(origAnswer == 1) {
						html += ' <i class="fa fa-check-circle-o text-success"></i>';
					}
					else if(userAnswer == 1 && data.questions[i].check != 0) {
						html += ' <i class="fa fa-times-circle-o text-danger"></i>';
					}
					html += '<br/>False';
					if(origAnswer == 0) {
						html += ' <i class="fa fa-check-circle-o text-success"></i>';
					}
					else if(userAnswer == 0 && data.questions[i].check != 0)
						html += ' <i class="fa fa-times-circle-o text-danger"></i>';
				}
				else if(data.questions[i].childs[j].questionType == 2) {
					if (data.questions[i].childs[j].origAnswer.length>0) {
						html+='<ul class="list-unstyled">';
						for(var k = 0; k < data.questions[i].childs[j].origAnswer.length; k++) {
							var origAnswer = data.questions[i].childs[j].origAnswer[k].blanks.toLowerCase().trim();
							if(data.questions[i].check == 0)
								var userAnswer = '';
							else {
								if(data.questions[i].childs[j].userAnswer.length > 0)
									var userAnswer = data.questions[i].childs[j].userAnswer[0].answer.toLowerCase().trim();
								else
									var userAnswer = '';
							}
							html += '<li>' + origAnswer + '&emsp;';
							if(origAnswer == userAnswer)
								html += '<i class="fa fa-check-circle-o text-success"></i>';
							else if(data.questions[i].check != 0){
								html += '<i class="fa fa-times-circle-o text-danger"></i>&emsp;' + origAnswer;
							}
							html += '</li>';
						}
						html+='</ul>';
					};
				}
				else if(data.questions[i].childs[j].questionType == 6) {
					if (data.questions[i].childs[j].origAnswer.length>0) {
						html+='<ul class="list-unstyled">';
						for(var k = 0; k < data.questions[i].childs[j].origAnswer.length; k++) {
							var origAnswer = data.questions[i].childs[j].origAnswer[k].option;
							html += '<li>' + ch[k] + ') ' + data.questions[i].childs[j].origAnswer[k].option + '&emsp;';
							if(data.questions[i].childs[j].origAnswer[k].correct == 1)
								html += '<i class="fa fa-check-circle-o text-success"></i>';
							for(var l = 0; l < data.questions[i].childs[j].userAnswer.length; l++) {
								if(data.questions[i].check == 0)
									var userAnswer = '';
								else {
									if(data.questions[i].childs[j].userAnswer.length > 0)
										var userAnswer = data.questions[i].childs[j].userAnswer[0].answer;
									else
										var userAnswer = '';
								}
								if(origAnswer == userAnswer && data.questions[i].childs[j].origAnswer[k].correct == 0) {
									html += '<i class="fa fa-times-circle-o text-danger"></i>';
								}
							}
							html += '</li>';
						}
						html+='</ul>';
					};
				}
				if(data.questions[i].childs[j].description == '')
					data.questions[i].childs[j].description = '<p>No explanation provided</p>';
				html += '<p><strong>Explanation</strong>' + data.questions[i].childs[j].description + '</p></div></div>';
			}
		}
		if(data.questions[i].questionType != 5)
			html += '<p><strong>Explanation</strong>' + data.questions[i].description + '</p></div>';
		html += '</td>'
				+ '<td class="text-right" width="12%">'
				+ '<strong>Marks</strong> +'+data.questions[i].correct+' / -'+data.questions[i].wrong+'<br>'
					+ '<strong>Time</strong> '+data.questions[i].time+'s<br><a class="time-compare"><strong>Compare</strong></a>'
				+ '</td>'
			+ '</tr>';
	}
					html += '</tbody>'
					+ '</table>'
				+ '</section>'
			+ '</div>'  
		+ '</section>';
	$('#sectionDetails').append(html);
	$('#sectionDetails .hide-section:eq(-1)').on('click', function() {
		var thiz = $(this).find('.fa');
		if(thiz.hasClass('fa-minus-square'))
			thiz.removeClass('fa-minus-square').addClass('fa-plus-square');
		else
			thiz.removeClass('fa-plus-square').addClass('fa-minus-square');
		thiz.parents('.panel:eq(0)').find('.panel-body').slideToggle();
	});
	$("#sectionDetails .table.table:eq(-1) .view-solution").on('click', function() {
		var flag = false;
		if($(this).attr('data-hidden') == 1)
			flag = true;
		$.each($('.view-solution'), function() {
			$(this).text($(this).attr('data-text')).attr('data-hidden', 0);
		});
		$('.answer').slideUp();
		if(flag)
			return;
		$(this).parents('td:eq(0)').find('.answer').slideToggle();
		if($(this).attr('data-hidden') == 0) {
			$(this).attr('data-hidden', '1');
			if($(this).attr('data-text') == 'View Sub-Parts')
				$(this).text('Hide Sub-Parts');
			else
				$(this).text('Hide Solution');
		}
		else {
			$(this).attr('data-hidden', '0');
			$(this).text($(this).attr('data-text'));
		}
	});
	$(".view-sol").off('click');
	$(".view-sol").on('click', function() {
		var flag = false;
		if($(this).attr('data-hidden') == 1)
			flag = true;
		$('.view-sol').text('View Solution').attr('data-hidden', 0);
		$('.sub-answer').slideUp();
		if(flag)
			return;
		$(this).parents('.answer-container:eq(0)').find('.sub-answer').slideToggle();
		if($(this).attr('data-hidden') == 0) {
			$(this).attr('data-hidden', '1');
			$(this).html('Hide Solution');
		}
		else {
			$(this).attr('data-hidden', '0');
			$(this).html('View Solution');
		}
	});
	//event handler for compare time click
	$('#sectionDetails .table.table:eq(-1) .time-compare').on('click', function() {
		var req = {};
		var res;
		req.action = 'get-maximum-time-for-question';
		req.questionId = $(this).parents('tr').attr('data-id');
		req.attemptId = attemptId;
		req.examId = examId;
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			console.log(res);
			if(res.status == 0)
				toastr.error(res.message);
			else {

				$('#timeGraph').find('*').remove();
				
				if(res.min == null || res.min == 0)
					res.min = 'NA';
				else
					res.min += ' seconds';
				var sanswer = '';
				if(res.self.check == 0)
					sanswer = '(Unattempted <i class="fa fa-dot-circle-o text-warning"></i>)';
				else if(res.self.check == 1)
					sanswer = '(Correct <i class="fa fa-check-circle-o text-success"></i>)';
				else
					sanswer = '(Incorrect <i class="fa fa-times-circle-o text-danger"></i>)';
				var tanswer = '';
				if(res.topper.check == 0)
					tanswer = '(Unattempted <i class="fa fa-dot-circle-o text-warning"></i>)';
				else if(res.topper.check == 1)
					tanswer = '(Correct <i class="fa fa-check-circle-o text-success"></i>)';
				else
					tanswer = '(Incorrect <i class="fa fa-times-circle-o text-danger"></i>)';
				legendHTML = '<table class="table table-bordered">'+
								'<tr><th class="bg-green font-white">Time spent by you</th><td>' + res.self.time + ' seconds ' + sanswer + '</td></tr>'+
								'<tr><th class="bg-green font-white">Average Time spent on this question</th><td>' + res.avg + ' seconds</td></tr>'+
								'<tr><th class="bg-green font-white">Least time spent on answering the question correctly</th><td>' + res.min + '</td></tr>'+
								'<tr><th class="bg-green font-white">Time spent by Topper</th><td>' + res.topper.time + ' seconds ' + tanswer + '</td></tr>'+
							'</table>';
				$('#timeGraph').append('<div id="pie-times"></div><br>' + legendHTML);
				var timeChart = $('#pie-times').highcharts({
					chart: {
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {
						text: 'Total number of students who have got the question are ' + parseInt(res.studentCount.total)
					},
					tooltip: {
						pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								format: '<b>{point.name}</b>: {point.percentage:.1f} %',
								style: {
									color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
								}
							}
						}
					},
					series: [{
						type: 'pie',
						name: 'Number',
						data: [
							['Correct', parseInt(res.studentCount.correct)],
							['Wrong', parseInt(res.studentCount.wrong)],
							['Unattempted', parseInt(res.studentCount.unattempted)]
						]
					}],
					colors: ['green','red','black']
				});

				//this is necessary to resize the pie chart
				//$(window).resize();

				var chart = $('#pie-times').highcharts();
				$('#timeCompareModal').on('show.bs.modal', function() {
					$('#pie-times').css('visibility', 'hidden');
				});
				$('#timeCompareModal').on('shown.bs.modal', function() {
					$('#pie-times').css('visibility', 'initial');
					chart.reflow();
				});
				
				$('#timeCompareModal').modal("show");
			}
		});
	});
	/*$('#timeCompareModal').on('show.bs.modal', function() {
	    $('#pie-times').css('visibility', 'hidden');
	});
	$('#timeCompareModal').on('shown.bs.modal', function() {
	    $('#pie-times').css('visibility', 'initial');
	    $(window).resize();
	});*/
	//adding sectional score
	$('#sectionTable tbody tr:eq(' + sectionCount + ')').find('.custom-section-score').text(parseFloat(sectionScore[sectionCount]).toFixed(2) + '/' + sectionTotal[sectionCount]);
	sectionCount++;
	fetchAllStudentGraph();
}

//function to get topper comparison
function fetchTopperComparison() {
	var req = {};
	var res;
	req.action = 'get-topper-of-exam';
	req.examId = examId;
	req.attemptId = attemptId;
	$.ajax({
		'type'	:	'post',
		'url'	:	ApiEndPoint,
		'data'	:	JSON.stringify(req)
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.status == 0)
			toastr.error(res.message);
		else{
			//fetchAllStudentGraph();
			fillTopperComparison(res);
		}
			
			
	});
}

//function to fill the compare graphs of topper
function fillTopperComparison(data) {
	var chart = AmCharts.makeChart("chart_1", {
            "type": "serial",
            "theme": "light",
            "pathToImages": App.getGlobalPluginsPath() + "amcharts/amcharts/images/",
            "autoMargins": false,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,

            "fontFamily": 'Open Sans',            
            "color":    '#888',
            
            "dataProvider": [{
                "device": "You",
                "geekbench": data.self.score,
                "geekpoint": data.self.score
            }, {
                "device": "Topper",
                "geekbench": data.avgMax.max,
                "geekpoint": data.avgMax.max
            }, {
                "device": "Median",
                "geekbench": data.median,
                "geekpoint": data.median
            }, {
                "device": "Average",
                "geekbench": data.avgMax.average,
                "geekpoint": data.avgMax.average
            }],
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left"
            }],
            "startDuration": 1,
            "graphs": [{
                "alphaField": "alpha",
                "balloonText": "<span style='font-size:13px;'>[[title]] of [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "dashLengthField": "dashLengthColumn",
                "fillAlphas": 1,
                "title": "Score",
                "type": "column",
                "valueField": "geekbench"
            }, {
                "balloonText": "<span style='font-size:13px;'>[[title]] of [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "bullet": "round",
                "dashLengthField": "dashLengthLine",
                "lineThickness": 3,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "Score",
                "valueField": "geekpoint"
            }],
            "categoryField": "device",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            }
        });
	var chart = AmCharts.makeChart("chart_2", {
            "type": "serial",
            "theme": "light",
            "pathToImages": App.getGlobalPluginsPath() + "amcharts/amcharts/images/",
            "autoMargins": false,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,

            "fontFamily": 'Open Sans',            
            "color":    '#888',
            
            "dataProvider": [{
                "device": "You",
                "geekbench": data.pself.score,
                "geekpoint": data.pself.score
            }, {
                "device": "Topper",
                "geekbench": data.pavgMax.max,
                "geekpoint": data.pavgMax.max
            }, {
                "device": "Median",
                "geekbench": data.pmedian,
                "geekpoint": data.pmedian
            }, {
                "device": "Average",
                "geekbench": data.pavgMax.average,
                "geekpoint": data.pavgMax.average
            }],
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left"
            }],
            "startDuration": 1,
            "graphs": [{
                "alphaField": "alpha",
                "balloonText": "<span style='font-size:13px;'>[[title]] of [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "dashLengthField": "dashLengthColumn",
                "fillAlphas": 1,
                "title": "Percentage",
                "type": "column",
                "valueField": "geekbench"
            }, {
                "balloonText": "<span style='font-size:13px;'>[[title]] of [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "bullet": "round",
                "dashLengthField": "dashLengthLine",
                "lineThickness": 3,
                "bulletSize": 7,
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "useLineColorForBulletBorder": true,
                "bulletBorderThickness": 3,
                "fillAlphas": 0,
                "lineAlpha": 1,
                "title": "Percentage",
                "valueField": "geekpoint"
            }],
            "categoryField": "device",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            }
        });
	/*heroBar1 = Morris.Bar({
		element: 'hero-bar1',
		data: [
		{device: 'You', geekbench: data.self.score},
		{device: 'Topper', geekbench: data.avgMax.max},
		{device: 'Median', geekbench: data.median},
		{device: 'Average', geekbench: data.avgMax.average}
		],
		xkey: 'device',
		ykeys: ['geekbench'],
		labels: ['Score'],
		barRatio: 0.4,
		xLabelAngle: 35,
		hideHover: 'auto',
		barColors: ['#D8574C']
	});
	
	heroBar2 = Morris.Bar({
		element: 'hero-bar2',
		data: [
		{device: 'You', geekbench: data.pself.percentage},
		{device: 'Topper', geekbench: data.pavgMax.max},
		{device: 'Median', geekbench: data.pmedian},
		{device: 'Average', geekbench: data.pavgMax.average}
		],
		xkey: 'device',
		ykeys: ['geekbench'],
		labels: ['Percentage'],
		barRatio: 0.4,
		xLabelAngle: 35,
		hideHover: 'auto',
		barColors: ['#56A738']
	});
    $('#topperCompare').on('show.bs.modal', function() {
    });
    $('#topperCompare').on('shown.bs.modal', function() {
        heroBar1.redraw();
        heroBar2.redraw();
    });*/
	//$("#topperCompare").modal('show');
}

//function to unset error
function unsetError(where) {
	if(where.parents('div:eq(0)').hasClass('has-error')) {
		where.parents('div:eq(0)').removeClass('has-error');
		where.parents('div:eq(0)').find('.help-block').remove();
	}
}

//function to set error
function setError(where, what) {
	where.parents('div:eq(0)').addClass('has-error');
	where.parents('div:eq(0)').append('<span class="help-block">'+what+'</span>');
}
</script>