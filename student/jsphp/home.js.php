<script type="text/javascript">
    function fillStudentCourses () {
        //console.log(courses);
        if (courses.length>0) {
            $('#listCourses').html('');
            $.each(courses, function (i, course) {
                var startDate = new Date(course.startDate);
                startDate = timeSince(startDate.getTime()); //returns 1340220044000
                $('#listCourses').append(
                    '<div class="col-lg-3 col-md-4 col-sm-6">'+
                        '<div class="mt-widget-2 bg-'+themeColors[getRandomInt(0, themeColors.length-1)]+'">'+
                            '<div class="mt-head" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(' + course.coverPic + ');">'+
                                '<div class="mt-head-user">'+
                                    '<div class="mt-head-user-img">'+
                                        '<img src="' + course.profilePic + '"> </div>'+
                                    '<div class="mt-head-user-info">'+
                                        '<span class="mt-user-name">' + course.inviter + '</span>'+
                                        '<span class="mt-user-time">'+
                                            '<i class="icon-clock"></i> '+startDate+' </span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="mt-body">'+
                                '<h3 class="mt-body-title"> ' + course.name + ' </h3>'+
                                '<p class="mt-body-description"> ' + course.subtitle + ' </p>'+
                                '<div class="mt-body-actions">'+
                                    '<div class="btn-group btn-group btn-group-justified">'+
                                        '<a href="javascript:;" class="btn btn-bookmark">'+
                                            '<i class="fa fa-bookmark"></i> Bookmark </a>'+
                                        '<a href="'+sitepathStudentCourses+course.course_id+'" class="btn">'+
                                            '<i class="fa fa-eye"></i> Start Learning </a>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>');
            });
        } else {
            $('#listCourses').removeClass('row').html('Sorry :( You don\'t have any courses. You can checkout our courses <a href="'+sitePathCourses+'">here</a>.');
        }
    }
    function fillStudentSubsCourses () {
        //console.log(courses);
        if (subCourses.length>0) {
            $('#listSubsCourses').html('');
            $.each(subCourses, function (i, course) {
                var startDate = new Date(course.startDate);
                startDate = timeSince(startDate.getTime()); //returns 1340220044000
                $('#listSubsCourses').append(
                    '<div class="col-lg-3 col-md-4 col-sm-6">'+
                        '<div class="mt-widget-2 bg-'+themeColors[getRandomInt(0, themeColors.length)]+'">'+
                            '<div class="mt-head" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(' + course.coverPic + ');">'+
                                '<div class="mt-head-user">'+
                                    '<div class="mt-head-user-img">'+
                                        '<img src="' + course.profilePic + '"> </div>'+
                                    '<div class="mt-head-user-info">'+
                                        '<span class="mt-user-name">' + course.inviter + '</span>'+
                                        '<span class="mt-user-time">'+
                                            '<i class="icon-clock"></i> '+startDate+' </span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="mt-body">'+
                                '<h3 class="mt-body-title"> ' + course.name + ' </h3>'+
                                '<p class="mt-body-description"> ' + course.subtitle + ' </p>'+
                                '<div class="mt-body-actions">'+
                                    '<div class="btn-group btn-group btn-group-justified">'+
                                        '<a href="javascript:;" class="btn btn-bookmark">'+
                                            '<i class="fa fa-bookmark"></i> Bookmark </a>'+
                                        '<a href="'+sitepathStudentCourses+course.course_id+'" class="btn">'+
                                            '<i class="fa fa-eye"></i> Start Learning </a>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>');
            });
        } else {
            $('#listSubsCourses').removeClass('row').html('No subscribed courses');
        }
    }
    function fillDashboardNotifications (data) {
        //console.log(data);
        if (data.sitenotifications.length>0) {
            var html = '';
            for(var i = 0; i < data.sitenotifications.length; i++) {
                var d = new Date(data.sitenotifications[i].timestamp);
                //d = timeSince(d.getTime()); //returns 1340220044000
                var notiDate = d.getDate();
                var notiMonth = month[d.getMonth()];
                var notiHours = d.getHours();
                var notiMinutes = d.getMinutes();

                html += '<div class="mt-action">'+
                            '<div class="mt-action-img">'+
                                '<img src="'+data.sitenotifications[i].courseImage+'" /> </div>'+
                            '<div class="mt-action-body">'+
                                '<div class="mt-action-row">'+
                                    '<div class="mt-action-info">'+
                                        '<div class="mt-action-icon ">'+
                                            '<i class="icon-bell"></i>'+
                                        '</div>'+
                                        '<div class="mt-action-details ">'+
                                            '<span class="mt-action-author">'+data.sitenotifications[i].inviter+'</span>'+
                                            '<p class="mt-action-desc">Join '+data.sitenotifications[i].courseName+'</p>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="mt-action-datetime ">'+
                                        '<span class="mt-action-date">'+notiDate+' '+notiMonth+'</span>'+
                                        '<span class="mt-action-dot bg-green"></span>'+
                                        '<span class="mt=action-time">'+notiHours+':'+notiMinutes+'</span>'+
                                    '</div>'+
                                    '<div class="mt-action-buttons ">'+
                                        '<div class="btn-group btn-group-circle">'+
                                            '<button type="button" class="btn btn-outline green btn-sm invite-button" data-id="'+data.sitenotifications[i].id+'" data-action="1">Approve</button>'+
                                            '<button type="button" class="btn btn-outline red btn-sm invite-button" data-id="'+data.sitenotifications[i].id+'" data-action="0">Reject</button>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
            }
            $('.portlet-invitations .mt-actions').html(html);
        } else {
            $('.portlet-invitations .mt-actions').html('No course invites.');
        }

        if (data.chatnotifications.length>0) {
            var html = '';
            for(var i = 0; i < data.chatnotifications.length; i++) {
                var message = "";
                var d = new Date(data.chatnotifications[i].timestamp);
                //d = timeSince(d.getTime()); //returns 1340220044000
                var notiDate = d.getDate();
                var notiMonth = month[d.getMonth()];
                var notiHours = d.getHours();
                var notiMinutes = d.getMinutes();
                var strTime = getTime12Hour(notiHours,notiMinutes);
                switch(data.chatnotifications[i].type) {
                    case '0':
                        if (data.chatnotifications[i].room_type == "public") {
                            message = data.chatnotifications[i].senderName+' just started conversation in <strong>Subject : '+data.chatnotifications[i].courseName+'</strong> of <strong>Course : '+data.chatnotifications[i].subjectName+'</strong></span>';
                        } else {
                            message = data.chatnotifications[i].senderName+' just started conversation with you in <strong>Subject : '+data.chatnotifications[i].courseName+'</strong> of <strong>Course : '+data.chatnotifications[i].subjectName+'</strong></span>';
                        }
                        break;
                    case '1':
                        if (data.chatnotifications[i].room_type == "public") {
                            message = data.chatnotifications[i].senderName+' just sent a message in <strong>Subject : '+data.chatnotifications[i].courseName+'</strong> of <strong>Course : '+data.chatnotifications[i].subjectName+'</strong></span>';
                        } else {
                            message = data.chatnotifications[i].senderName+' just messaged you in <strong>Subject : '+data.chatnotifications[i].courseName+'</strong> of <strong>Course : '+data.chatnotifications[i].subjectName+'</strong></span>';
                        }
                        break;
                }
                html += '<div class="mt-comment chat-parent">'+
                            '<div class="mt-comment-img">'+
                                '<img src="'+data.chatnotifications[i].profilePic+'" /> </div>'+
                            '<div class="mt-comment-body">'+
                                '<div class="mt-comment-info">'+
                                    '<span class="mt-comment-author">'+data.chatnotifications[i].senderName+'</span>'+
                                    '<span class="mt-comment-date">'+notiDate+' '+notiMonth+', '+strTime+'</span>'+
                                '</div>'+
                                '<div class="mt-comment-text"> '+message+' </div>'+
                                '<div class="mt-comment-details">'+
                                    '<span class="mt-comment-status mt-comment-status-pending">'+
                                        '<a href="javascript:void(0)" class="chat-button" data-id="'+data.chatnotifications[i].id+'" data-chat="1" target="_blank">Chat</a>'+
                                    '</span>'+
                                    '<ul class="mt-comment-actions">'+
                                        '<li>'+
                                            '<a href="javascript:void(0)" class="chat-button" data-id="'+data.chatnotifications[i].id+'" data-chat="0">Mark as read</a>'+
                                        '</li>'+
                                    '</ul>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
            }
            $('.portlet-chats .mt-comments').html(html);
        } else {
            $('.portlet-chats .mt-comments').html('No chat notifications.');
        }
    }
    $(document).ready(function() {        
        if (currAddress == "") {
            $("#modalSetAddress").modal("show");
        }
        $('body').on('click', '.btn-bookmark', function(e){
            bookmarkThisPage(e);
        });
    });
</script>