<script type="text/javascript">
    //function to fetch notifications
    var pageLimit = 10;
    function fetchNotificationsChat(pageNumber) {
        var req = {};
        var res;
        req.pageNumber = parseInt(pageNumber)-1;
        req.pageLimit  = pageLimit;
        req.action = 'get-student-notifications-chat';
        ajaxRequest({
            'type'  :   'post',
            'url'   :   ApiEndPoint,
            'data'  :   JSON.stringify(req),
            success:function(res){
                res = $.parseJSON(res);
                if(res.status == 0)
                    toastr.error(res.message);
                else {
                    notifications = res;
                    fillNotificationsChat(res);
                }
            }
        });
    }
    function fillNotificationsChat (data) {
        console.log(data);
        if (data.notiCount>0) {
            var html = '';
            for(var i = 0; i < data.chatnotifications.length; i++) {
                var message = "";
                var d = new Date(data.chatnotifications[i].timestamp+'  UTC');
                //d = timeSince(d.getTime()); //returns 1340220044000
                var notiDate = d.getDate();
                var notiMonth = month[d.getMonth()].substring(0,3);
                var notiHours = d.getHours();
                var notiMinutes = d.getMinutes();
                var strTime = getTime12Hour(notiHours,notiMinutes);
                switch(data.chatnotifications[i].type) {
                    case '0':
                        if (data.chatnotifications[i].room_type == "public") {
                            message = data.chatnotifications[i].senderName+' just started conversation in <strong>Subject : '+data.chatnotifications[i].subjectName+'</strong> of <strong>Course : '+data.chatnotifications[i].courseName+'</strong></span>';
                        } else {
                            message = data.chatnotifications[i].senderName+' just started conversation with you in <strong>Subject : '+data.chatnotifications[i].subjectName+'</strong> of <strong>Course : '+data.chatnotifications[i].courseName+'</strong></span>';
                        }
                        break;
                    case '1':
                        if (data.chatnotifications[i].room_type == "public") {
                            message = data.chatnotifications[i].senderName+' just sent a message in <strong>Subject : '+data.chatnotifications[i].subjectName+'</strong> of <strong>Course : '+data.chatnotifications[i].courseName+'</strong></span>';
                        } else {
                            message = data.chatnotifications[i].senderName+' just messaged you in <strong>Subject : '+data.chatnotifications[i].subjectName+'</strong> of <strong>Course : '+data.chatnotifications[i].courseName+'</strong></span>';
                        }
                        break;
                }
                html += '<div class="mt-comment chat-parent-all '+((data.chatnotifications[i].status==0)?(''):('disabled'))+'">'+
                            '<div class="mt-comment-img">'+
                                '<img src="'+data.chatnotifications[i].profilePic+'" /> </div>'+
                            '<div class="mt-comment-body">'+
                                '<div class="mt-comment-info">'+
                                    '<span class="mt-comment-author">'+data.chatnotifications[i].senderName+'</span>'+
                                    '<span class="mt-comment-date">'+notiDate+' '+notiMonth+', '+strTime+'</span>'+
                                '</div>'+
                                '<div class="mt-comment-text"> '+message+' </div>'+
                                '<div class="mt-comment-details">'+
                                    ((data.chatnotifications[i].status==0)?(
                                    '<span class="mt-comment-status mt-comment-status-pending">'+
                                        '<a href="javascript:void(0)" class="chat-button" data-id="'+data.chatnotifications[i].id+'" data-chat="1" target="_blank">Chat</a>'+
                                    '</span>'+
                                    '<ul class="mt-comment-actions">'+
                                        '<li>'+
                                            '<a href="javascript:void(0)" class="chat-button" data-id="'+data.chatnotifications[i].id+'" data-chat="0">Mark as read</a>'+
                                        '</li>'+
                                    '</ul>'
                                    ):(
                                    '<span class="mt-comment-status mt-comment-status-pending">'+
                                        '<a href="'+sitePath+'messenger/'+data.chatnotifications[i].roomId+'" class="chat-button" target="_blank">Chat</a>'+
                                    '</span>'
                                    ))+
                                '</div>'+
                            '</div>'+
                        '</div>';
            }
            if ($('#pagerChatsContent').html() != '') {
                var totalNotifications = data.notiCount;
                var noOfPages = parseInt(totalNotifications/pageLimit);
                var handleDynamicPagination = function() {
                    $('#pagerChats').bootpag({
                        paginationClass: 'pagination',
                        next: '<i class="fa fa-angle-right"></i>',
                        prev: '<i class="fa fa-angle-left"></i>',
                        total: noOfPages,
                        page: 1,
                    }).on("page", function(event, num){
                        fetchNotificationsChat(num);
                    });
                }
                handleDynamicPagination();
            }
            $('#pagerChatsContent').html(html);
        } else {

            $('#pagerChatsContent').html('No course invites.');
        }

    }
    $('body').on('click', '.btn-bookmark', function(e){
        bookmarkThisPage(e);
    });
</script>