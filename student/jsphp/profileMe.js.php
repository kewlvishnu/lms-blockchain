<script type="text/javascript">
    //function to fetch user details
    function fillUserProfile() {
        //console.log(userProfile);
        if (userProfile.details.achievements.length>0) {
            $('#achievements').html(userProfile.details.achievements);
        }
        if (userProfile.details.interests) {
            $('#interests').html(userProfile.details.interests);
        }
        if (userProfile.details.about.length>0) {
            $('#about').html(userProfile.details.about);
        }

        var html = '';
        if (userProfile.courses.length>0) {
            $.each(userProfile.courses, function(objKey,objVal) {
                console.log(objVal);
                html+= '<tr>'+
                            '<td class="fit">'+
                                '<img class="user-pic" src="'+objVal.image+'"> </td>'+
                            '<td>'+
                                '<a href="'+sitepathStudentCourses+objVal.id+'" class="primary-link">'+objVal.name+'</a>'+
                            '</td>'+
                            '<td> '+objVal.subjects.length+' </td>'+
                            '<td> '+objVal.chapters+' </td>'+
                            '<td> '+objVal.exams+' </td>'+
                            '<td>'+
                                '<span class="bold theme-font">'+objVal.progress+'%</span>'+
                            '</td>'+
                        '</tr>';
            });
        } else {
            html = '<tr><td colspan="6">- No Courses -</td></tr>'
        }
        $('#tblCourses tbody').html(html);
    }

    $("#btnSaveBlockchain").click(function(event) {
        /* Act on the event */
        var firstName   = userProfile.details.firstName;
        var lastName    = userProfile.details.lastName;
        var country     = userProfile.details.country;
        var interests   = userProfile.details.interests;
        var achievements   = userProfile.details.achievements;
        //registerStudentOnBlockchain(firstName,lastName,country,'',interests,achievements);
        registerStudentOnBlockchain(firstName,lastName,country,'',interests,achievements);
    });
</script>