<script type="text/javascript">
$(document).ready(function(){
    $('#btnSetAddress').click(function(event) {
        /* Act on the event */
        $("#modalSetAddress").modal("show");
    });
    $("#btnDeletePersonalData").click(function(event) {
      /* Act on the event */
      var req = {};
      req.action      = 'delete-personal-data';
      var res;
      ajaxRequest({
          'type'  :   'post',
          'url'   :   ApiEndPoint,
          'data'  :   JSON.stringify(req),
          success:function(res){
              res = $.parseJSON(res);
              if(res.status == 0)
                  toastr.error(res.message);
              else {
                  toastr.success("Successfully deleted personal data!", "Personal Data");
              }
          }
      });
    });
    $("#btnDeleteAccount").click(function(event) {
      /* Act on the event */
      var req = {};
      req.action      = 'delete-account';
      var res;
      ajaxRequest({
          'type'  :   'post',
          'url'   :   ApiEndPoint,
          'data'  :   JSON.stringify(req),
          success:function(res){
              res = $.parseJSON(res);
              if(res.status == 0)
                  toastr.error(res.message);
              else {
                  toastr.success("Successfully deleted your account!", "Delete Account");
                  window.location = sitepathLogout;
              }
          }
      });
    });
    $('.btn-facebook').click(function(){
        var req = {};
        if($(this).text() == "Disconnect") {
            req.socialAction = "disconnect";
            var res;
            req.action = 'set-social-connection';
            ajaxRequest({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req),
                success:function(res){
                    var data = $.parseJSON(res);
                    if (data.status == 1) {
                        toastr.success("Successfully disconnected");
                        $('.btn-facebook').text('Connect').removeClass('btn-danger').addClass('btn-success');
                    } else {
                        toastr.error(data.message);
                    }
                }
            });
        } else {
            FBLogin();
        }
    });
    
    $.ajaxSetup({ cache: true });
    $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
    FB.init({
      appId: '877235672348652',
      version: 'v2.7' // or v2.1, v2.2, v2.3, ...
    });     
    /*$('#loginbutton,#feedbutton').removeAttr('disabled');
    FB.getLoginStatus(updateStatusCallback);*/
    });

    // This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response) {

      if (response.status === 'connected') {
          // Logged into your app and Facebook.
          // we need to hide FB login button
          $('#fblogin').hide();
          //fetch data from facebook
          getUserInfo();
      } else if (response.status === 'not_authorized') {
          // The person is logged into Facebook, but not your app.
          $('#status').html('Please log into this app.');
      } else {
          // The person is not logged into Facebook, so we're not sure if
          // they are logged into this app or not.
          $('#status').html('Please log into facebook');
      }
    }

    function FBLogin()
    {
          FB.login(function(response) {
                if (response.authResponse) 
                {
                    getUserInfo(); //Get User Information.
                } else
                {
                    toastr.error('Authorization failed.');
                }
           },{scope: 'public_profile,email'});
    }

    function getUserInfo() {
        FB.api('/me', function(response) {
            console.log(response);
            var req = {};
            req.socialAction = "connect";
            var res;
            req.facebookid = response.id;
            req.action = 'set-social-connection';
            ajaxRequest({
                'type': 'post',
                'url': ApiEndPoint,
                'data': JSON.stringify(req),
                success:function(res){
                    var data = $.parseJSON(res);
                    if (data.status == 1) {
                        toastr.success("Successfully connected");
                        $('.btn-facebook').text('Disconnect').removeClass('btn-success').addClass('btn-danger');
                    } else {
                        toastr.error(data.message);
                    }
                }
            });
          /*$.ajax({
                type: "POST",
                dataType: 'json',
                data: response,
                url: 'check_user.php',
                success: function(msg) {
                 if(msg.error== 1)
                 {
                  toastr.error('Something Went Wrong!');
                 } else {
                  $('#fbstatus').show();
                  $('#fblogin').hide();
                  $('#fbname').text("Name : "+msg.name);
                  $('#fbemail').text("Email : "+msg.email);
                  $('#fbfname').text("First Name : "+msg.first_name);
                  $('#fblname').text("Last Name : "+msg.last_name);
                  $('#fbid').text("Facebook ID : "+msg.id);
                  $('#fbimg').html("<img src='http://graph.facebook.com/"+msg.id+"/picture'>");
                 }
                }
          });*/

        });
    }

    function FBLogout()
    {
      FB.logout(function(response) {
           $('#fblogin').show(); //showing login button again
           $('#fbstatus').hide(); //hiding the status
      });
    }

    // This function is called when someone finishes with the Login
    // Button.  See the onlogin handler attached to it in the sample code below.
    function checkLoginState() {
      FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
      });
    }

});
function checkSocialConnections() {
    var req = {};
    var res;
    req.action = 'get-social-connections';
    ajaxRequest({
        'type': 'post',
        'url': ApiEndPoint,
        'data': JSON.stringify(req),
        success:function(res){
            var data = $.parseJSON(res);
            if (data.socials.facebookid>0) {
                $('.btn-facebook').text('Disconnect').removeClass('btn-success').addClass('btn-danger');
            } else {
                $('.btn-facebook').text('Connect').removeClass('btn-danger').addClass('btn-success');
            }
        }
    });

}
</script>