<script type="text/javascript">
function fetchSubmission() {
	var req = {};
	var res;
	req.action = 'attempt-submission';
	//req.subjectId = subjectId;
	//req.courseId = courseId;
	req.examId =   examId;
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndPoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
		{
			toastr.error(res.message);
			//window.location.href =sitePath+'404';
		}
		else{
			//console.log(res);
			fillSubmissionQuestions(res.submission);
		}
	});
}
function fillSubmissionQuestions(questions) {

	var html = '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
	for (var i = 0; i < questions.length; i++) {
		var answerText = ((!questions[i]["answerText"])?'':questions[i]["answerText"]);
		var answerFile = answerFileClass = '';
		if (questions[i]["answerFile"]) {
			answerFile = '<a href="'+questions[i]["answerFile"]+'" class="btn green-jungle" target="_blank">Click here to check the upload</a><a href="javascript:void(0)" class="btn red js-upload-remove"><i class="fa fa-times"></i></a>';
			answerFileClass = 'hide';
		}
		html+=	'<div class="panel panel-success panel-result" data-status="'+questions[i]["status"]+'">'+
					'<div class="panel-heading" role="tab" id="heading'+i+'">'+
						'<h4 class="panel-title">'+
							'<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+i+'" aria-expanded="true" aria-controls="collapse'+i+'">'+
								'Question #'+(i+1)+''+
							'</a>'+
						'</h4>'+
					'</div>'+
					'<div id="collapse'+i+'" class="panel-collapse collapse '+((i==0)?'in':'')+'" role="tabpanel" aria-labelledby="heading'+i+'">'+
						'<div class="panel-body no-space">'+
							'<table class="table no-margin">'+
								'<tbody>'+
									'<tr>'+
										'<td>'+questions[i]["question"]+'</td>'+
									'</tr>'+
									'<tr>'+
										'<td>'+
											'<div class="form-group js-answer-text">'+
												'<label for="">Answer Note:</label>'+
												'<textarea name="" id="" cols="30" rows="10" class="form-control">'+answerText+'</textarea>'+
											'</div>'+
											'<div class="form-group js-answer-file">'+
												'<label for="">Answer Upload:</label>'+
												'<form action="'+FileApiPoint+'" class="form-answer" method="post" enctype="multipart/form-data">'+
													'<button type="button" class="btn btn-warning btn-upload '+answerFileClass+'">Upload Answer</button>'+
													'<input type="file" name="submission-upload" class="js-uploader hide">'+
													'<div class="js-file-path">'+answerFile+'</div>'+
												'</form>'+
											'</div>'+
											'<div class="form-group">'+
												'<button class="btn green js-submit-answer" data-ai="'+questions[i]["attemptId"]+'" data-aqi="'+questions[i]["id"]+'" data-q="'+questions[i]["questionId"]+'">Submit Answer</button>'+
											'</div>'+
										'</td>'+
									'</tr>'+
								'</tbody>'+
							'</table>'+
						'</div>'+
					'</div>'+
				'</div>';
		if (i == 0) {
			$("#btnSubmitSubmission").attr('data-attempt', questions[i]["attemptId"]);
		}
	}
	html += '</div>';
	$('#jsQuestions').append(html);
	allAnswersCheck();
}
$(document).ready(function(){
	$('#jsQuestions').on('click', '.js-submit-answer', function() {
		/* Act on the event */
    	var thiz = $(this);
		var answerText = thiz.closest('td').find('.js-answer-text textarea').val();
		var answerFile = thiz.closest('td').find('.js-answer-file .js-file-path a').attr('href');
		if (!answerText) {
			toastr.error("Please enter answer text!");
		} else if (!answerFile) {
			toastr.error("Please select an image or PDF to upload!");
		} else {
			var req = {};
			req.answerText = answerText;
			req.answerFile = answerFile;
			req.attemptId = thiz.attr('data-ai');
			req.attemptQuestionId = thiz.attr('data-aqi');
			req.questionId = thiz.attr('data-q');
			var res;
			req.action = 'submission-answer';
			//req.subjectId = subjectId;
			//req.courseId = courseId;
			req.examId =   examId;
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndPoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0)
				{
					toastr.error(res.message);
					//window.location.href =sitePath+'404';
				}
				else{
					thiz.closest('.panel').attr('data-status', 1);
					toastr.success('Successfully saved the answer.');
					allAnswersCheck();
				}
			});
		}
		return false;
	});
	$('#jsQuestions').on('click', '.btn-upload', function(){
		$(this).next().click();
	});
	$('#jsQuestions').on('change', '.js-uploader', function(){
		var file = $(this).val();
		if (!!file) {
	    	var maxFileSize = 30720000;
	        var fileSize;

	        // If current browser is IE < 10, use ActiveX
	        if (isIE() && isIE() < 10) {
	            var filePath = this.value;
	            if (filePath != '') {
	                var AxFSObj = new ActiveXObject("Scripting.FileSystemObject");
	                var AxFSObjFile = AxFSObj.getFile(filePath);
	                fileSize = AxFSObjFile.size;
	            }
	        } else {
	            // IE >= 10 or not IE

	            if (this.value != '') {
	                fileSize = this.files[0].size;
	            }
	        }

	        if (fileSize > maxFileSize) {
	            toastr.error("File size is greater than 30MB, please upload a smaller file.");
	        } else {
				$(this).closest('form').submit();
			}
		};
	});
	$('#jsQuestions').on('click', '.js-upload-remove', function(event){
		event.preventDefault();
		/* Act on the event */
    	$(this).closest('td').find('.btn-upload').removeClass('hide');
    	$(this).closest('td').find('.js-file-path').html('');
	});
    $('#jsQuestions').on('submit', '.form-answer', function(){
    	var thiz = $(this);
    	thiz.find('.btn-upload').attr('disabled', true);
    	thiz.find('.btn-upload').after('<span class="upload-loader"><img src="'+sitePath+'img/ajax-loader.gif" alt="loader" /></span>');
	    var options = { 
	        //target:        '#output2',   // target element(s) to be updated with server response 
	        data: { examId: examId },
	        dataType: 'json',
	        beforeSubmit:  showRequest,  // pre-submit callback 
	        success: function(msg) {
	        	thiz.find('.js-uploader').val('');
	        	thiz.find('.upload-loader').remove();
    			thiz.find('.btn-upload').attr('disabled', false);
	        	if (msg.status == 0) {
	        		toastr.error(msg.message);
	        	} else {
    				thiz.find('.btn-upload').addClass('hide');
	        		thiz.closest('.form-answer').find('.js-file-path').append('<a href="'+msg.fileName+'" class="btn green-jungle" target="_blank">Click here to check the upload</a><a href="javascript:void(0)" class="btn red js-upload-remove"><i class="fa fa-times"></i></a>');
					thiz.closest('td').find('.js-submit-answer').trigger('click');
	        	}
	        }  // post-submit callback
	    }; 
        // inside event callbacks 'this' is the DOM element so we first 
        // wrap it in a jQuery object and then invoke ajaxSubmit 
        $(this).ajaxSubmit(options); 
 
        // !!! Important !!! 
        // always return false to prevent standard browser submit and page navigation 
        return false;
    });
    $('#btnSubmitSubmission').click(function(){
    	if (allAnswersCheck()) {
    		var req = {};
			var res;
			req.action = 'submit-submission-attempt';
			//req.subjectId = subjectId;
			//req.courseId = courseId;
			req.attemptId	=	$(this).attr('data-attempt');
			req.examId		=	examId;
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndPoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0)
				{
					toastr.error(res.message);
					//window.location.href =sitePath+'404';
				}
				else{
					toastr.success('Successfully submitted.');
					//console.log(sitepathStudentSubjects+subjectId);
					window.location = sitepathStudentSubjects+subjectId;
				}
			});
    	}
    });
});
function allAnswersCheck() {
	var flag = 0;
	$("#jsQuestions .panel").each(function(index, el) {
		if($(el).attr("data-status") == 0) {
			flag++;
		}
	});
	if (flag == 0) {
		$('#btnSubmitSubmission').attr('disabled', false);
		return true;
	}
	$('#btnSubmitSubmission').attr('disabled', true);
	return false;
}
// pre-submit callback 
function showRequest(formData, jqForm, options) { 
    // formData is an array; here we use $.param to convert it to a string to display it 
    // but the form plugin does this for you automatically when it submits the data 
    var queryString = $.param(formData); 
 
    // jqForm is a jQuery object encapsulating the form element.  To access the 
    // DOM element for the form do this: 
    // var formElement = jqForm[0]; 
 
    //toastr.error('About to submit: \n\n' + queryString); 
 
    // here we could return false to prevent the form from being submitted; 
    // returning anything other than false will allow the form submit to continue 
    return true; 
}
</script>