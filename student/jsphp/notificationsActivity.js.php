<script type="text/javascript">
    //function to fetch notifications
    var pageLimit = 10;
    var pageNumber = 1;
    function fetchNotificationsActivity() {
        var req = {};
        var res;
        req.pageNumber = parseInt(pageNumber)-1;
        req.pageLimit  = pageLimit;
        req.action = 'get-student-notifications-activity';
        ajaxRequest({
            'type'  :   'post',
            'url'   :   ApiEndPoint,
            'data'  :   JSON.stringify(req),
            success:function(res){
                res = $.parseJSON(res);
                if(res.status == 0)
                    toastr.error(res.message);
                else {
                    notifications = res;
                    fillNotificationsActivity(res);
                }
            }
        });
    }
    function fillNotificationsActivity (data) {
        console.log(data);
        if (data.notiCount>0) {
            var html = '';
            for(var i = 0; i < data.sitenotifications.length; i++) {
                var d = new Date(data.sitenotifications[i].timestamp);
                //d = timeSince(d.getTime()); //returns 1340220044000
                var notiDate = d.getDate();
                var notiMonth = month[d.getMonth()];
                var notiHours = d.getHours();
                var notiMinutes = d.getMinutes();

                html += '<div class="mt-action invite-parent-all '+((data.sitenotifications[i].status==0)?(''):('disabled'))+'">'+
                            '<div class="mt-action-img">'+
                                '<img src="'+data.sitenotifications[i].courseImage+'" /> </div>'+
                            '<div class="mt-action-body">'+
                                '<div class="mt-action-row">'+
                                    '<div class="mt-action-info">'+
                                        '<div class="mt-action-icon ">'+
                                            '<i class="icon-bell"></i>'+
                                        '</div>'+
                                        '<div class="mt-action-details ">'+
                                            '<span class="mt-action-author">'+data.sitenotifications[i].inviter+'</span>'+
                                            '<p class="mt-action-desc">Join '+data.sitenotifications[i].courseName+'</p>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="mt-action-datetime ">'+
                                        '<span class="mt-action-date">'+notiDate+' '+notiMonth+'</span>'+
                                        '<span class="mt-action-dot bg-green"></span>'+
                                        '<span class="mt=action-time">'+notiHours+':'+notiMinutes+'</span>'+
                                    '</div>'+
                                    ((data.sitenotifications[i].status==0)?(
                                    '<div class="mt-action-buttons ">'+
                                        '<div class="btn-group btn-group-circle">'+
                                            '<button type="button" class="btn btn-outline green btn-sm invite-button" data-id="'+data.sitenotifications[i].id+'" data-action="1">Approve</button>'+
                                            '<button type="button" class="btn btn-outline red btn-sm invite-button" data-id="'+data.sitenotifications[i].id+'" data-action="0">Reject</button>'+
                                        '</div>'
                                    ):(''))+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
            }
            if ($('#pagerInvitesContent').html() == '') {
                var totalNotifications = data.notiCount;
                var noOfPages = parseInt(totalNotifications/pageLimit);
                var handleDynamicPagination = function() {
                    $('#pagerInvites').bootpag({
                        paginationClass: 'pagination',
                        next: '<i class="fa fa-angle-right"></i>',
                        prev: '<i class="fa fa-angle-left"></i>',
                        total: noOfPages,
                        page: pageNumber,
                    }).on("page", function(event, num){
                        pageNumber = num;
                        fetchNotificationsActivity(num);
                    });
                }
                handleDynamicPagination();
            }
            $('#pagerInvitesContent').html(html);
        } else {

            $('#pagerInvitesContent').html('No course invites.');
        }

    }
    $('body').on('click', '.btn-bookmark', function(e){
        bookmarkThisPage(e);
    });
</script>