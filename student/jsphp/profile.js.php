<script type="text/javascript">
    var userProfile;
    //function to fetch user details
    function fetchStudentDetails() {
        var req = {};
        var res;
        req.action = 'get-student-details-new';
        ajaxRequest({
                'type'  :   'post',
                'url'   :   ApiEndPoint,
                'data'  :   JSON.stringify(req),
            success:function(res){
                res = $.parseJSON(res);
                if(res.status == 0)
                    toastr.error(res.message);
                else {
                    //console.log(res);
                    fillStudentDetails(res);
                    if (subPage == 'profileEdit') {
                        fillUserProfileForEdit();
                    } else if (subPage == 'profileMe') {
                        fillUserProfile();
                    };
                }
            }
        });
    }
    function fillStudentDetails (data) {
        //console.log(data);
        userProfile = data;
        if (!!data.details) {
            $('.profile-userpic img').attr('src',data.details.profilePic);
            $('.profile-userpic').removeClass('invisible');
            $('.profile-usertitle-name').html(data.details.name);
            $('.profile-desc-title').html('About '+data.details.name);
            if (data.details.about.length>0) {
                $('.profile-desc-text').html(data.details.about);
            }
        };
        $('.js-courses').html(data.courses.length);
        $('.js-subjects').html(data.subjects);
        $('.js-exams').html(data.exams);
        if (userProfile.details.website.length>0) {
            $('.js-website').html('<i class="fa fa-globe"></i><a href="'+userProfile.details.website+'" target="_blank">'+userProfile.details.website+'</a>');
        };
        if (userProfile.details.facebook.length>0) {
            $('.js-facebook').html('<i class="fa fa-facebook"></i><a href="https://www.facebook.com/'+userProfile.details.facebook+'" target="_blank">'+userProfile.details.facebook+'</a>');
        };
        if (userProfile.details.twitter.length>0) {
            $('.js-twitter').html('<i class="fa fa-twitter"></i><a href="https://www.twitter.com/'+userProfile.details.twitter+'" target="_blank">@'+userProfile.details.twitter+'</a>');
        };
    }
</script>