<script type="text/javascript">
    function fillStudentCourses () {
        //console.log(courses);
        if (courses.length>0) {
            $('#listCourses').html('');
            $.each(courses, function (i, course) {
                var startDate = new Date(course.startDate);
                startDate = timeSince(startDate.getTime()); //returns 1340220044000
                $('#listCourses').append(
                    '<div class="col-lg-3 col-md-4 col-sm-6">'+
                        '<div class="mt-widget-2 bg-'+themeColors[getRandomInt(0, themeColors.length-1)]+'">'+
                            '<div class="mt-head" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(' + course.coverPic + ');">'+
                                '<div class="mt-head-user">'+
                                    '<div class="mt-head-user-img">'+
                                        '<img src="' + course.profilePic + '"> </div>'+
                                    '<div class="mt-head-user-info">'+
                                        '<span class="mt-user-name">' + course.inviter + '</span>'+
                                        '<span class="mt-user-time">'+
                                            '<i class="icon-clock"></i> '+startDate+' </span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="mt-body">'+
                                '<h3 class="mt-body-title"> ' + course.name + ' </h3>'+
                                '<p class="mt-body-description"> ' + course.subtitle + ' </p>'+
                                '<div class="mt-body-actions">'+
                                    '<div class="btn-group btn-group btn-group-justified">'+
                                        '<a href="javascript:;" class="btn btn-bookmark">'+
                                            '<i class="fa fa-bookmark"></i> Bookmark </a>'+
                                        '<a href="'+sitepathStudentCourses+course.course_id+'" class="btn">'+
                                            '<i class="fa fa-eye"></i> Start Learning </a>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>');
            });
        } else {
            $('#listCourses').removeClass('row').html('Sorry :( You don\'t have any courses. You can checkout our courses <a href="'+sitePathCourses+'">here</a>.');
        }
    }
    function fillStudentSubsCourses () {
        //console.log(courses);
        if (subCourses.length>0) {
            $('#listSubsCourses').html('');
            $.each(subCourses, function (i, course) {
                var startDate = new Date(course.startDate);
                startDate = timeSince(startDate.getTime()); //returns 1340220044000
                $('#listSubsCourses').append(
                    '<div class="col-lg-3 col-md-4 col-sm-6">'+
                        '<div class="mt-widget-2 bg-'+themeColors[getRandomInt(0, themeColors.length-1)]+'">'+
                            '<div class="mt-head" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(' + course.coverPic + ');">'+
                                '<div class="mt-head-user">'+
                                    '<div class="mt-head-user-img">'+
                                        '<img src="' + course.profilePic + '"> </div>'+
                                    '<div class="mt-head-user-info">'+
                                        '<span class="mt-user-name">' + course.inviter + '</span>'+
                                        '<span class="mt-user-time">'+
                                            '<i class="icon-clock"></i> '+startDate+' </span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="mt-body">'+
                                '<h3 class="mt-body-title"> ' + course.name + ' </h3>'+
                                '<p class="mt-body-description"> ' + course.subtitle + ' </p>'+
                                '<div class="mt-body-actions">'+
                                    '<div class="btn-group btn-group btn-group-justified">'+
                                        '<a href="javascript:;" class="btn btn-bookmark">'+
                                            '<i class="fa fa-bookmark"></i> Bookmark </a>'+
                                        '<a href="'+sitepathStudentCourses+course.course_id+'" class="btn">'+
                                            '<i class="fa fa-eye"></i> Start Learning </a>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>');
            });
        } else {
            $('#listSubsCourses').removeClass('row').html('No subscribed courses');
        }
    }
    $('body').on('click', '.btn-bookmark', function(e){
        bookmarkThisPage(e);
    });
</script>