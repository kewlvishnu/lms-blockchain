        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer text-center">
            <div class="page-footer-inner clearfix"> 2016 &copy; Copyrights. All Rights Reserved.
                <a target="_blank" href="<?php echo $sitepath; ?>">Integro.io, Inc.</a>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
        </div>
        <!-- DATA HOLDERS START -->
        <input type="hidden" id="contractCode" value="<?php echo $global->contractCode; ?>">
        <input type="hidden" id="contractAddress" value="<?php echo $global->contractAddress; ?>">
        <input type="hidden" id="ownerAddress" value="<?php echo $global->toAddress; ?>">
        <input type="hidden" id="rsContractAddress" value="0x661d04efde622b50cd746d4cce395e909286301e">
        <!-- DATA HOLDERS END --><?php
        require_once 'html-template/pages/modals/metamaskNotInstalled.modal.php';
        require_once 'html-template/pages/modals/metamaskAssociateAddress.modal.php';
        require_once 'html-template/pages/modals/metamaskChangeAddress.modal.php';
        require_once 'html-template/pages/modals/portisAssociateAddress.modal.php';
        require_once 'html-template/pages/modals/setAddress.modal.php';
        ?>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
        <script src="assets/global/plugins/respond.min.js"></script>
        <script src="assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-bootpag/jquery.bootpag.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/holder.js" type="text/javascript"></script>
        <!-- <script src="assets/global/plugins/web3/web3.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/web3/ethjs-util.min.js" type="text/javascript"></script> -->
        <script src="https://cdn.jsdelivr.net/gh/ethereum/web3.js/dist/web3.min.js"></script>
        <!-- <script src="https://cdn.jsdelivr.net/npm/portis/dist/bundle.min.js"></script> -->
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <?php
            if (isset($page) && !empty($page)) {
                $customIncludes = '';
                switch ($page) {
                    case 'home':
                        $customIncludes.=
                        '<script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/horizontal-timeline/horozontal-timeline.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>';
                        break;
                    case 'profile':
                        $customIncludes.=
                        '<script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>';
                        /*<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
                        <script src="assets/global/plugins/gmaps/gmaps.min.js" type="text/javascript"></script>'*/;
                        if ($subPage == 'profileEdit') {
                            $customIncludes.=
                            '<script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/jcrop/js/jquery.color.js" type="text/javascript"></script>
                            <script src="assets/global/plugins/jcrop/js/jquery.Jcrop.min.js" type="text/javascript"></script>';
                        }
                        break;
                    case 'settings':
                        $customIncludes.=
                        '<script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>';
                        break;
                    case 'subjectDetail':
                        $customIncludes.=
                        '<script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/bootstrap-star-rating-master/js/star-rating.min.js" type="text/javascript"></script>';
                        break;
                    case 'examResult':
                        $customIncludes.=
                        '<script src="assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
                        <script src="https://code.highcharts.com/stock/highstock.js"></script>
                        <script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
                        <script src="https://code.highcharts.com/stock/modules/heatmap.js"></script>
                        <script src="https://code.highcharts.com/stock/modules/data.js"></script>
                        <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript"></script>';
                        break;
                    case 'subjectiveExamResult':
                    case 'submissionResult':
                        $customIncludes.=
                        '<script src="assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
                        <script src="https://code.highcharts.com/stock/highstock.js"></script>
                        <script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
                        <script src="https://code.highcharts.com/stock/modules/heatmap.js"></script>
                        <script src="https://code.highcharts.com/stock/modules/data.js"></script>
                        <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript"></script>';
                        break;
                    case 'manualExamResult':
                        $customIncludes.=
                        '<script src="assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
                        <script src="assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
                        <script src="https://code.highcharts.com/stock/highstock.js"></script>
                        <script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
                        <script src="https://code.highcharts.com/stock/modules/heatmap.js"></script>
                        <script src="https://code.highcharts.com/stock/modules/data.js"></script>
                        <script src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript"></script>';
                        break;
                    case 'submission':
                        $customIncludes.=
                        '<script src="//malsup.github.io/jquery.form.js" type="text/javascript"></script>';
                        break;
                    default:break;
                }
                echo $customIncludes;
            }
        ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <script src="assets/global/scripts/functions.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <!-- <script src="assets/pages/scripts/ui-general.min.js" type="text/javascript"></script> -->
        <!-- <script src="assets/pages/scripts/ui-toastr.min.js" type="text/javascript"></script> -->
        <!-- <script src="assets/pages/scripts/dashboard.min.js" type="text/javascript"></script> -->
        <!-- END PAGE LEVEL SCRIPTS -->
        <script src="assets/layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
        <!-- <script src="assets/layouts/layout2/scripts/demo.min.js" type="text/javascript"></script> -->
        <?php
            require_once('jsphp/global.js.php');
            require_once('jsphp/wallet/web3ops.js.php');
            if (isset($page) && !empty($page)) {
                $customIncludes = '';
                switch ($page) {
                    case 'home':
                        require_once('jsphp/home.js.php');
                        break;
                    case 'courses':
                        require_once('jsphp/courses.js.php');
                        break;
                    case 'courseDetail':
                        require_once('jsphp/courseDetail.js.php');
                        break;
                    case 'subjectDetail':
                        require_once('jsphp/subjectDetail.js.php');
                        break;
                    case 'profile':
                        require_once('jsphp/profile.js.php');
                        if ($subPage == 'profileEdit') {
                            require_once('jsphp/profileEdit.js.php');
                        }
                        if ($subPage == 'profileMe') {
                            require_once('jsphp/profileMe.js.php');
                        }
                        break;
                    case 'settings':
                        require_once('jsphp/settings.js.php');
                        break;
                    case 'notificationsActivity':
                        require_once('jsphp/notificationsActivity.js.php');
                        break;
                    case 'notificationsChat':
                        require_once('jsphp/notificationsChat.js.php');
                        break;
                    case 'examResult':
                        require_once('jsphp/examResult.js.php');
                        break;
                    case 'subjectiveExamResult':
                        require_once('jsphp/subjectiveExamResult.js.php');
                        break;
                    case 'manualExamResult':
                        require_once('jsphp/manualExamResult.js.php');
                        break;
                    case 'submission':
                        require_once('jsphp/submission.js.php');
                        break;
                    case 'submissionResult':
                        require_once('jsphp/submissionResult.js.php');
                        break;
                    default:break;
                }
                echo $customIncludes;
            }
        ?>
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/58f86bb530ab263079b60876/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
        <!--End of Tawk.to Script-->
    </body>

</html>