<!-- BEGIN PROFILE SIDEBAR -->
<div class="profile-sidebar">
    <!-- PORTLET MAIN -->
    <div class="portlet light profile-sidebar-portlet ">
        <!-- SIDEBAR USERPIC -->
        <div class="profile-userpic invisible">
            <img src="" class="img-responsive" alt=""> </div>
        <!-- END SIDEBAR USERPIC -->
        <!-- SIDEBAR USER TITLE -->
        <div class="profile-usertitle">
            <div class="profile-usertitle-name"> </div>
            <div class="profile-usertitle-job"> Student </div>
        </div>
        <!-- END SIDEBAR USER TITLE -->
        <?php /*
        <!-- SIDEBAR BUTTONS -->
        <div class="profile-userbuttons">
            <button type="button" class="btn btn-circle green btn-sm">Follow</button>
            <button type="button" class="btn btn-circle red btn-sm">Message</button>
        </div> */ ?>
        <!-- END SIDEBAR BUTTONS -->
        <!-- SIDEBAR MENU -->
        <div class="profile-usermenu">
            <ul class="nav">
                <li class="<?php echo (($page == 'profile' && $subPage == 'profileMe')?'active':'') ; ?>">
                    <a href="<?php echo $sitepathStudent; ?>profile">
                        <i class="icon-home"></i> Overview </a>
                </li>
                <li class="<?php echo (($page == 'profile' && $subPage == 'profileEdit')?'active':'') ; ?>">
                    <a href="<?php echo $sitepathStudent; ?>profile/edit">
                        <i class="icon-pencil"></i> Edit Profile </a>
                </li>
            </ul>
        </div>
        <!-- END MENU -->
    </div>
    <!-- END PORTLET MAIN -->
    <!-- PORTLET MAIN -->
    <div class="portlet light ">
        <!-- STAT -->
        <div class="row list-separated profile-stat">
            <div class="col-md-4 col-sm-4 col-xs-6">
                <div class="uppercase profile-stat-title js-courses"> 0 </div>
                <div class="uppercase profile-stat-text"> Courses </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6">
                <div class="uppercase profile-stat-title js-subjects"> 0 </div>
                <div class="uppercase profile-stat-text"> Subjects </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6">
                <div class="uppercase profile-stat-title js-exams"> 0 </div>
                <div class="uppercase profile-stat-text"> Exams </div>
            </div>
        </div>
        <!-- END STAT -->
        <div>
            <h4 class="profile-desc-title">About Marcus Doe</h4>
            <span class="profile-desc-text"> --- </span>
            <div class="margin-top-20 profile-desc-link js-website">
                <!-- <i class="fa fa-globe"></i>
                <a href="http://www.integro.io">www.integro.io</a> -->
            </div>
            <div class="margin-top-20 profile-desc-link js-twitter">
                <!-- <i class="fa fa-twitter"></i>
                <a href="http://www.twitter.com/arcanemind/">@arcanemind</a> -->
            </div>
            <div class="margin-top-20 profile-desc-link js-facebook">
                <!-- <i class="fa fa-facebook"></i>
                <a href="http://www.facebook.com/arcanemind/">arcanemind</a> -->
            </div>
        </div>
    </div>
    <!-- END PORTLET MAIN -->
</div>
<!-- END BEGIN PROFILE SIDEBAR -->