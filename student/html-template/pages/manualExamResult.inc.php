<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h1 class="page-title"> <span class="exam-name"><?php echo $examDetails['examName']; ?></span></h1>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo $sitepathStudent; ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo $sitepathStudentCourses; ?>">Courses</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <i class="icon-note"></i>
                    <a href="<?php echo $sitepathStudentCourses.$examDetails['courseId']; ?>"><?php echo $examDetails['courseName']; ?></a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <i class="icon-note"></i>
                    <a href="<?php echo $sitepathStudentSubjects.$examDetails['subjectId']; ?>"><?php echo $examDetails['subjectName']; ?></a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="<?php echo $sitepathStudentSubjects.$examDetails['subjectId']; ?>"><?php echo $examDetails['examName']; ?></a>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <div class="clearfix well well-sm bg-green-jungle font-white text-center hide" id="comment"></div>
        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat dashboard-stat-v2 blue">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="rank">0</span>
                        </div>
                        <div class="desc"> Rank </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat dashboard-stat-v2 red">
                    <div class="visual">
                        <i class="fa fa-percent"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="percentage">0</span>% </div>
                        <div class="desc"> Percentage </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat dashboard-stat-v2 green">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="score">0</span>
                        </div>
                        <div class="desc"> Score </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat dashboard-stat-v2 purple">
                    <div class="visual">
                        <i class="fa fa-percent"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="percentile">0</span> </div>
                        <div class="desc"> Percentile </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat dashboard-stat-v2 blue">
                    <div class="visual">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="highestScore">0</span>
                        </div>
                        <div class="desc"> Highest Score </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat dashboard-stat-v2 red">
                    <div class="visual">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="examType"></span> </div>
                        <div class="desc"> Analytics </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BEGIN : HIGHCHARTS -->
        <div class="row">
            <div class="col-md-6">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-white"></i>
                            <span class="caption-subject bold font-white uppercase">Percentage Distribution</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="hero-bar3" style="height:500px;"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-white"></i>
                            <span class="caption-subject bold font-white uppercase">Marks Comparison by Score</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="pieGraph" class="chart" style="height: 500px;"> </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
            if ($examDetails['type'] == "deep") {
        ?>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-white"></i>
                            <span class="caption-subject bold font-white uppercase">Detailed Analysis</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body no-space">
                        <div id="questionMarks"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php
            }
        ?>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<?php
    require_once("modals/timeCompare.modal.php");
    require_once("modals/examQuestionCategories.modal.php");
    require_once("modals/answer.modal.php");
?>