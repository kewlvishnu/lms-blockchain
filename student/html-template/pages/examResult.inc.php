<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h1 class="page-title"> <span class="exam-name"><?php echo $examDetails['name']; ?></span></h1>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo $sitepathStudent; ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo $sitepathStudentCourses; ?>">Courses</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <i class="icon-note"></i>
                    <a href="<?php echo $sitepathStudentCourses.$examDetails['courseId']; ?>"><?php echo $examDetails['courseName']; ?></a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <i class="icon-note"></i>
                    <a href="<?php echo $sitepathStudentSubjects.$examDetails['subjectId']; ?>"><?php echo $examDetails['subjectName']; ?></a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span class="exam-name"><?php echo $examDetails['name']; ?> (Attempt No : <span class="attempt-no"></span>)</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <div class="row margin-bottom-20 attempt-block hide">
            <div class="col-md-6">
                <a href="javascript:void(0)" class="btn green btn-block previous-attempt" disabled><i class="fa fa-arrow-left"></i> Previous Attempt</a>
            </div>
            <div class="col-md-6">
                <a href="javascript:void(0)" class="btn green btn-block next-attempt" disabled><i class="fa fa-arrow-right"></i> Next Attempt</a>
            </div>
        </div>
        <div class="clearfix well well-sm bg-green-jungle font-white text-center hide" id="comment"></div>
        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat dashboard-stat-v2 blue">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="rank">0</span>
                        </div>
                        <div class="desc"> Rank </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat dashboard-stat-v2 red">
                    <div class="visual">
                        <i class="fa fa-percent"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="percentage">0</span>% </div>
                        <div class="desc"> Percentage </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat dashboard-stat-v2 green">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="score">0</span>
                        </div>
                        <div class="desc"> Score </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat stat-sm dashboard-stat-v2 purple">
                    <div class="visual">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="startDate"></span> </div>
                        <div class="desc"> Started On </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat stat-sm dashboard-stat-v2 blue">
                    <div class="visual">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="endDate"></span>
                        </div>
                        <div class="desc"> Completed On </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat stat-sm dashboard-stat-v2 red">
                    <div class="visual">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="0" id="timeTaken">0</span> </div>
                        <div class="desc"> Time taken </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BEGIN : HIGHCHARTS -->
        <div class="row">
            <div class="col-md-6">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-white"></i>
                            <span class="caption-subject bold font-white uppercase">TimeLine Graph</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="highchart_1" style="height:500px;"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-white"></i>
                            <span class="caption-subject bold font-white uppercase">Percentage Distribution</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="hero-bar3" style="height:500px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-microphone font-white hide"></i>
                            <span class="caption-subject bold font-white uppercase"> Attempt Summary</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body no-space">
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-md-6">
                                    <span>
                                        <strong>Legends :</strong>
                                        <span class="label bg-green-jungle"> Correctly answered </span>
                                        <span class="label bg-yellow-casablanca"> Wrongly answered </span>
                                        <span class="label bg-dark"> Unattempted </span>
                                    </span>
                                </div>
                                <div class="col-md-3">
                                    <a class="text-success" href="#categories" data-toggle="modal"><i class="fa fa-bars"></i> See Question Categories</a>
                                </div>
                                <div class="col-md-3">
                                    <a class="text-success" href="#topperCompare" data-scroll><i class="fa fa-search"></i> Compare with toppers</a>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table no-margin" id="sectionTable">
                                <thead>
                                    <tr>
                                        <th>Section</th>
                                        <th class="text-center">Total Questions</th>
                                        <th class="text-center">Your Score</th>
                                        <th>Time Taken</th>
                                        <th>Question Response</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="topperCompare">
            <div class="col-md-6">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-white"></i>
                            <span class="caption-subject bold font-white uppercase">Topper Comparison by Score</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chart_1" class="chart" style="height: 500px;"> </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-white"></i>
                            <span class="caption-subject bold font-white uppercase">Topper Comparison by Percentage</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chart_2" class="chart" style="height: 500px;"> </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div id="sectionDetails"></div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<?php
    require_once("modals/timeCompare.modal.php");
    require_once("modals/examQuestionCategories.modal.php");
    //require_once("modals/topperCompare.modal.php");
?>