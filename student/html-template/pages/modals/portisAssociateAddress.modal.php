<div id="modalPortisAssociateAddress" class="modal modal-notification fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <div class="meta-logo text-center">
                    <img src="<?php echo $sitepathManage; ?>assets/global/img/wallet/associate-address-portis.png" alt="">
                </div>
            </div>
            <div class="modal-footer">
                <p class="notice">Crypto address in Portis is <span id="paAddress"></span>.</p>
                <p class="notice">Do you want to associate this with your account?</p>
                <div class="text-center">
                    <button type="button" class="btn green btn-lg" id="btnPOAssociateAddress">Yes</button>
                    <button type="button" data-dismiss="modal" class="btn red btn-lg">No</button>
                </div>
            </div>
        </div>
    </div>
</div>