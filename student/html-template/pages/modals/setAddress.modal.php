<div id="modalSetAddress" class="modal modal-notification fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Are you ready to connect to the blockchain?</h4>
            </div>
            <div class="modal-body text-center">
                <div class="pad-20 text-center">
                    <div class="row">
                        <!-- <div class="col-md-3">
                            <img src="<?php //echo $sitepath; ?>img/wallet/portis.png" alt="" class="img-responsive dib wd-200">
                        </div>
                        <div class="col-md-3">
                            <div class="mt-50 mb-10">
                                <label class="btn btn-primary active">
                                    <input type="radio" name="optionSetVia" id="optionSetVia1" value="p" autocomplete="off" checked> Set via Portis
                                </label>
                            </div>
                            <small class="note note-warning dib">Preferred!</small>
                        </div>
                        <div class="col-md-3">
                            <img src="<?php //echo $sitepath; ?>img/wallet/metamask.png" alt="" class="img-responsive dib wd-200">
                        </div>
                        <div class="col-md-3">
                            <div class="mt-50 mb-10">
                                <label class="btn btn-primary">
                                    <input type="radio" name="optionSetVia" id="optionSetVia2" value="m" autocomplete="off"> Set via Metamask
                                </label>
                            </div>
                        </div> -->
                        <div class="col-md-3 col-md-offset-3">
                            <img src="<?php echo $sitepath; ?>img/wallet/metamask.png" alt="" class="img-responsive dib wd-200">
                        </div>
                        <div class="col-md-3">
                            <div class="mt-50 mb-10">
                                <button class="btn btn-primary">Powered by Metamask</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Maybe Later</button>
                <button type="button" class="btn green" id="btnWalletSetAddress">Connect</button>
            </div>
        </div>
    </div>
</div>