<div id="reviewModal" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Your Ratings and Review</h4>
            </div>
            <div class="modal-body">
                <div class="login-form clearfix">
                    <form role="form">
                        <div class="form-group">
                            <label>Choose Your Rating: </label>
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- <input type="hidden" id="rating" data-filled="fa fa-star fa-2x gold" data-empty="fa fa-star-o fa-2x gold" value="0"> -->
                                    <input class="star-rating" id="rating" type="number" class="rating" min="0" max="5" step="0.5" data-size="xs">
                                </div>
                                <div class="col-md-6">
                                    <a class="btn red hide" id="removeReview">Remove Rating &amp; Review</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <!-- <h4 style="color: rgb(255, 83, 42);">Please describe why this course is very good?</h4> -->
                            <p><code>Did the course help you achieve what you set out to learn?</code></p>
                            <p><code>How engaging and prepared was the instructor</code></p>
                            <p><code>What was the production quality like?</code></p>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="titleReview" placeholder="Please enter your review title here">
                        </div>
                        <div class="form-group">
                            <textarea name="review" id="review" cols="30" rows="10" class="form-control" placeholder="Please enter your review here"></textarea>
                            <input type="hidden" value="0" id="reviewId">
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="submitReview">Save</button>
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>