<div id="tagsModal" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Tag Analysis</h4>
            </div>
            <div class="modal-body">
                <div class="text-center table-responsive">
                    <table class="table table-striped table-bordered" id="tableTagAnalysis">
                        <thead>
                            <th class="text-center" colspan="3">Tag: <strong id="tagName"></strong></th>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>