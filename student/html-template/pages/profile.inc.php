<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h1 class="page-title"> My Profile
            <small></small>
        </h1>
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <?php
                    require_once('html-template/includes/sidebar-profile.php');
                    switch ($subPage) {
                        case 'profileMe':
                            require_once('html-template/pages/profile/profileMe.html.php');
                            break;
                        case 'profileEdit':
                            require_once('html-template/pages/profile/profileEdit.html.php');
                            break;
                        
                        default:
                            echo "There was some error!";
                            break;
                    }
                ?>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->