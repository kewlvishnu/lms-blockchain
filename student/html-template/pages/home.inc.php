    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <h1 class="page-title"> Dashboard
                <small>dashboard &amp; statistics</small>
            </h1>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo $sitepathStudent; ?>">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <span>Dashboard</span>
                    </li>
                </ul>
                <?php /*
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"> Actions
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a href="#">
                                    <i class="icon-bell"></i> Action</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon-shield"></i> Another action</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon-user"></i> Something else here</a>
                            </li>
                            <li class="divider"> </li>
                            <li>
                                <a href="#">
                                    <i class="icon-bag"></i> Separated link</a>
                            </li>
                        </ul>
                    </div>
                </div>
                */ ?>
            </div>
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-microphone font-dark hide"></i>
                                <span class="caption-subject bold font-dark uppercase"> Courses</span>
                                <span class="caption-helper">Your courses</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row" id="listCourses">
                                <?php /*
                                    <div class="col-md-3">
                                        <div class="mt-widget-2">
                                            <div class="mt-head" style="background-image: url(assets/pages/img/background/32.jpg);">
                                                <div class="mt-head-label">
                                                    <button type="button" class="btn btn-success">Manhattan</button>
                                                </div>
                                                <div class="mt-head-user">
                                                    <div class="mt-head-user-img">
                                                        <img src="assets/pages/img/avatars/team7.jpg"> </div>
                                                    <div class="mt-head-user-info">
                                                        <span class="mt-user-name">Chris Jagers</span>
                                                        <span class="mt-user-time">
                                                            <i class="icon-emoticon-smile"></i> 3 mins ago </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-body">
                                                <h3 class="mt-body-title"> Thomas Clark </h3>
                                                <p class="mt-body-description"> It is a long established fact that a reader will be distracted </p>
                                                <ul class="mt-body-stats">
                                                    <li class="font-green">
                                                        <i class="icon-emoticon-smile"></i> 3,7k</li>
                                                    <li class="font-yellow">
                                                        <i class=" icon-social-twitter"></i> 3,7k</li>
                                                    <li class="font-red">
                                                        <i class="  icon-bubbles"></i> 3,7k</li>
                                                </ul>
                                                <div class="mt-body-actions">
                                                    <div class="btn-group btn-group btn-group-justified">
                                                        <a href="javascript:;" class="btn">
                                                            <i class="icon-bubbles"></i> Bookmark </a>
                                                        <a href="javascript:;" class="btn ">
                                                            <i class="icon-social-twitter"></i> Share </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                */ ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-microphone font-dark hide"></i>
                                <span class="caption-subject bold font-dark uppercase">Subscribed Courses</span>
                                <span class="caption-helper">Your subscribed courses</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row" id="listSubsCourses"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="portlet light portlet-invitations">
                        <div class="portlet-title tabbable-line">
                            <div class="caption">
                                <i class=" icon-social-twitter font-dark hide"></i>
                                <span class="caption-subject font-dark bold uppercase">Course Invitations</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="scroller" style="height: 250px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                <!-- BEGIN: Actions -->
                                <div class="mt-actions">
                                    <!-- <div class="mt-action">
                                        <div class="mt-action-img">
                                            <img src="assets/pages/media/users/avatar10.jpg" /> </div>
                                        <div class="mt-action-body">
                                            <div class="mt-action-row">
                                                <div class="mt-action-info ">
                                                    <div class="mt-action-icon ">
                                                        <i class="icon-magnet"></i>
                                                    </div>
                                                    <div class="mt-action-details ">
                                                        <span class="mt-action-author">Natasha Kim</span>
                                                        <p class="mt-action-desc">Dummy text of the printing</p>
                                                    </div>
                                                </div>
                                                <div class="mt-action-datetime ">
                                                    <span class="mt-action-date">3 jun</span>
                                                    <span class="mt-action-dot bg-green"></span>
                                                    <span class="mt=action-time">9:30-13:00</span>
                                                </div>
                                                <div class="mt-action-buttons ">
                                                    <div class="btn-group btn-group-circle">
                                                        <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                                        <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                                <!-- END: Actions -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="portlet light portlet-chats">
                        <div class="portlet-title tabbable-line">
                            <div class="caption">
                                <i class="icon-bubbles font-dark hide"></i>
                                <span class="caption-subject font-dark bold uppercase">Chat Notifications</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="scroller" style="height: 250px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                <!-- BEGIN: Comments -->
                                <div class="mt-comments">
                                    <!-- <div class="mt-comment">
                                        <div class="mt-comment-img">
                                            <img src="assets/pages/media/users/avatar1.jpg" /> </div>
                                        <div class="mt-comment-body">
                                            <div class="mt-comment-info">
                                                <span class="mt-comment-author">Michael Baker</span>
                                                <span class="mt-comment-date">26 Feb, 10:30AM</span>
                                            </div>
                                            <div class="mt-comment-text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. </div>
                                            <div class="mt-comment-details">
                                                <span class="mt-comment-status mt-comment-status-pending">Pending</span>
                                                <ul class="mt-comment-actions">
                                                    <li>
                                                        <a href="#">Quick Edit</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">View</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                                <!-- END: Comments -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->