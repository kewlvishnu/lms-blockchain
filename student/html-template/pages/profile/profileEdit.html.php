<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                        </li>
                        <li>
                            <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                        </li>
                        <li>
                            <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <!-- PERSONAL INFO TAB -->
                        <div class="tab-pane active" id="tab_1_1">
                            <form role="form" action="#" id="formProfile">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your profile is successfully updated! </div>
                                <div class="form-group">
                                    <label class="control-label">First Name <span class="required" aria-required="true"> * </span></label>
                                    <input type="text" id="inputFirstName" name="inputFirstName" placeholder="e.g. John" class="form-control bc-control" data-changed="0" /> </div>
                                <div class="form-group">
                                    <label class="control-label">Last Name <span class="required" aria-required="true"> * </span></label>
                                    <input type="text" id="inputLastName" name="inputLastName" placeholder="e.g. Doe" class="form-control bc-control" data-changed="0" /> </div>
                                <div class="form-group">
                                    <label class="control-label">Country <span class="required" aria-required="true"> * </span></label>
                                    <select id="selectCountry" name="selectCountry" class="form-control js-countries bc-control" data-changed="0"><?php
                                        if (isset($gCountries) && !empty($gCountries)) {
                                            foreach ($gCountries as $country) {
                                                echo '<option value="'.$country['id'].'">'.$country['name'].'</option>';
                                            }
                                        }
                                    ?></select> </div>
                                <div class="form-group">
                                    <label class="control-label">Mobile Number (Country Code + 10 digit number)</label>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <input type="text" id="inputMobilePrefix" name="inputMobilePrefix" class="form-control" value="+91">
                                        </div>
                                        <div class="col-sm-10">
                                            <input type="text" id="inputMobile" name="inputMobile" placeholder="e.g. 9876543210" class="form-control" />
                                        </div>
                                    </div> </div>
                                <div class="form-group">
                                    <label class="control-label">Select Birthdate</label>
                                    <div class="input-group input-medium date date-picker" id="dob" data-date-format="dd-mm-yyyy">
                                        <input type="text" class="form-control" readonly>
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                    <span class="help-block"> Select date </span>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Gender <span class="required" aria-required="true"> * </span></label>
                                    <div class="mt-radio-inline">
                                        <label class="mt-radio">
                                            <input type="radio" name="optionGender" id="optionGender1" value="1"> Male
                                            <span></span>
                                        </label>
                                        <label class="mt-radio">
                                            <input type="radio" name="optionGender" id="optionGender2" value="2"> Female
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">About</label>
                                    <textarea id="inputAbout" name="inputAbout" class="form-control bc-control" rows="3" placeholder="Enter brief note about you!" data-changed="0"></textarea> </div>
                                <div class="form-group">
                                    <label class="control-label">Achievements</label>
                                    <textarea id="inputAchievements" name="inputAchievements" rows="3" placeholder="Third prize in Olympiad etc." class="form-control bc-control" data-changed="0"></textarea> </div>
                                <div class="form-group">
                                    <label class="control-label">Interests</label>
                                    <textarea id="inputInterests" name="inputInterests" rows="3" placeholder="Design, Web etc." class="form-control bc-control" data-changed="0"></textarea> </div>
                                <div class="form-group">
                                    <label class="control-label">Street</label>
                                    <textarea id="inputStreet" name="inputStreet" rows="3" placeholder="e.g. 21, Cross Road" class="form-control"></textarea> </div>
                                <div class="form-group">
                                    <label class="control-label">City</label>
                                    <input type="text" id="inputCity" name="inputCity" placeholder="e.g. Bangalore" class="form-control" /> </div>
                                <div class="form-group">
                                    <label class="control-label">State</label>
                                    <input type="text" id="inputState" name="inputState" placeholder="e.g. Karnataka" class="form-control" /> </div>
                                <div class="form-group">
                                    <label class="control-label">PIN/ZIP Code</label>
                                    <input type="text" id="inputPin" name="inputPin" placeholder="e.g. 400004" class="form-control" /> </div>
                                <div class="form-group">
                                    <label class="control-label">Website Url</label>
                                    <input type="text" id="inputWebsite" name="inputWebsite" placeholder="http://www.mywebsite.com" class="form-control" /> </div>
                                <div class="form-group">
                                    <label class="control-label">Facebook ID</label>
                                    <input type="text" id="inputFacebook" name="inputFacebook" placeholder="e.g. IntegroLive" class="form-control" /> </div>
                                <div class="form-group">
                                    <label class="control-label">Twitter ID</label>
                                    <input type="text" id="inputTwitter" name="inputTwitter" placeholder="e.g. integro" class="form-control" /> </div>
                                <div class="margin-top-10">
                                    <button type="submit" class="btn green">Submit</button>
                                    <button type="button" class="btn btn-primary" id="btnSaveBlockchain">Save on blockchain</button>
                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                </div>
                            </form>
                        </div>
                        <!-- END PERSONAL INFO TAB -->
                        <!-- CHANGE AVATAR TAB -->
                        <div class="tab-pane" id="tab_1_2">
                            <p> Please select an image for your profile picture. </p>
                            <form action="#" role="form">
                                <!-- <div class="form-group">
                                    <input type="file" />
                                    <img class="crop" style="display:none" />
                                    <button type="submit" style="display:none">Upload</button>
                                </div> -->
                                <div class="form-group">
                                    <div class="profile-pic">
                                        <div class="thumbnail">
                                            <img id="userProfilePic" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span> Change image </span>
                                                <input id="fileProfilePic" type="file" name="..."> </span>
                                            <a href="javascript:;" class="btn default js-remove"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="margin-top-10 hide js-upload">
                                    <a href="javascript:;" class="btn green" id="btnUpload">Upload</a>
                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                </div>
                            </form>
                        </div>
                        <!-- END CHANGE AVATAR TAB -->
                        <!-- CHANGE PASSWORD TAB -->
                        <div class="tab-pane" id="tab_1_3">
                            <form role="form" action="#" id="formPassword">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your password is successfully changed! </div>
                                <div class="form-group">
                                    <label class="control-label">Current Password</label>
                                    <input type="password" class="form-control" id="inputOldPassword" name="inputOldPassword" /> </div>
                                <div class="form-group">
                                    <label class="control-label">New Password</label>
                                    <input type="password" class="form-control" id="inputNewPassword" name="inputNewPassword" /> </div>
                                <div class="form-group">
                                    <label class="control-label">Re-type New Password</label>
                                    <input type="password" class="form-control" id="inputNewRePassword" name="inputNewRePassword" /> </div>
                                <div class="margin-top-10">
                                    <button type="submit" class="btn green">Change Password</button>
                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                </div>
                            </form>
                        </div>
                        <!-- END CHANGE PASSWORD TAB -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PROFILE CONTENT -->