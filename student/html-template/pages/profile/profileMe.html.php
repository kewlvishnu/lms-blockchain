<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET -->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Course Activity</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable table-scrollable-borderless" id="tblCourses">
                        <table class="table table-hover table-light">
                            <thead>
                                <tr class="uppercase">
                                    <th colspan="2"> COURSE </th>
                                    <th> Subjects </th>
                                    <th> Lectures </th>
                                    <th> Exams </th>
                                    <th> Progress </th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END PORTLET -->
        </div>
        <div class="col-md-12">
            <!-- BEGIN PORTLET -->
            <div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">More Information</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div>
                        <h4 class="block">About</h4>
                        <p id="about" class="well"> - No Information - </p>
                    </div>
                    <div>
                        <h4 class="block">Interests</h4>
                        <p id="interests" class="well"> - No Interests - </p>
                    </div>
                    <div>
                        <h4 class="block">Achievements</h4>
                        <p id="achievements" class="well"> - No Achievements - </p>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-primary" id="btnSaveBlockchain">Save on blockchain</button>
                    </div>
                </div>
            </div>
            <!-- END PORTLET -->
        </div>
    </div>
</div>
<!-- END PROFILE CONTENT -->