<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h1 class="page-title"> Account Settings
            <small>modify your account settings</small>
        </h1>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo $sitepathStudent; ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Account Settings</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet light ">
                    <div class="portlet-title tabbable-line">
                        <div class="caption">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Account Settings</span>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_1" data-toggle="tab">CryptoWallet</a>
                            </li>
                            <li>
                                <a href="#tab_1_2" data-toggle="tab">Social Settings</a>
                            </li>
                            <li>
                                <a href="#tab_1_3" data-toggle="tab">Personal Data</a>
                            </li>
                        </ul>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <div class="tab-pane active" id="tab_1_1">
                                <div class="note note-info">
                                    <h4 class="block">Set your address</h4>
                                    <p> The address below is your physical address that will be used for currency transactions</p>
                                </div>
                                <form role="form" action="" id="formCryptoWallet">
                                    <div class="alert alert-danger display-hide">
                                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                    <div class="alert alert-success display-hide">
                                        <button class="close" data-close="alert"></button> Your address is successfully updated! </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Physical Address</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" value="<?php echo $global->walletAddress; ?>" placeholder="Your Wallet Address" readonly>
                                        </div>
                                    </div>
                                    <div class="clearfix form-group margin-top-10">
                                        <button type="button" class="btn green" id="btnSetAddress" <?php echo (!empty($global->walletAddress)?'disabled':''); ?>>Set My Address</button>
                                    </div>
                                </form>
                            </div>
                            <!-- END PERSONAL INFO TAB -->
                            <!-- PRIVACY SETTINGS TAB -->
                            <div class="tab-pane" id="tab_1_2">
                                <div class="note note-info">
                                    <h4 class="block">Social Connections</h4>
                                    <p> Here you can link your social media websites to this account. Currently we support just facebook, twitter and google+ are coming soon. </p>
                                </div>
                                <div class="well">
                                    <form role="form" action="#" id="formSocial">
                                        <div class="alert alert-danger display-hide">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                        <div class="alert alert-success display-hide">
                                            <button class="close" data-close="alert"></button> Your profile is successfully updated! </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="control-label">Facebook Connect</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <button type="button" class="btn btn-success btn-facebook">Connect</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div id="profile_facebook"></div>
                                </div>
                            </div>
                            <!-- END PRIVACY SETTINGS TAB -->
                            <!-- PRIVACY SETTINGS TAB -->
                            <div class="tab-pane" id="tab_1_3">
                                <div class="note note-info">
                                    <h4 class="block">Personal Data</h4>
                                    <p> Delete Personal Data. </p>
                                </div>
                                <div class="well">
                                    <a href="javascript:;" id="btnDeletePersonalData" class="btn red">Delete my personal data</a>
                                    <a href="javascript:;" id="btnDeleteAccount" class="btn red">Delete my account</a>
                                </div>
                            </div>
                            <!-- END PRIVACY SETTINGS TAB -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->