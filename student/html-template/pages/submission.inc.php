<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h1 class="page-title"> <span class="exam-name"><?php echo $examDetails['name']; ?></span></h1>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo $sitepathStudent; ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo $sitepathStudentCourses; ?>">Courses</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <i class="icon-note"></i>
                    <a href="<?php echo $sitepathStudentCourses.$examDetails['courseId']; ?>"><?php echo $examDetails['courseName']; ?></a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <i class="icon-note"></i>
                    <a href="<?php echo $sitepathStudentSubjects.$examDetails['subjectId']; ?>"><?php echo $examDetails['subjectName']; ?></a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="<?php echo $sitepathStudentSubjects.$examDetails['subjectId']; ?>"><?php echo $examDetails['name']; ?></a>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN : HIGHCHARTS -->
        <!-- <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-white"></i>
                    <span class="caption-subject bold font-white uppercase">Submission Tasks</span>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div id="jsQuestions" style="height:500px;"></div>
            </div>
        </div> -->
        <div class="bg-white">
            <div class="note">
                <div class="row">
                    <div class="col-md-8">
                        <p>Please read the questions below and submit your answers. After submitting all your answers, the submission submit button will be active. After submitting your submission you will not be able to change your answers and it will go for check.</p>
                    </div>
                    <div class="col-md-4">
                        <button class="btn green btn-fit-height btn-block" disabled="" id="btnSubmitSubmission">Submit Submission</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="jsQuestions"></div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->