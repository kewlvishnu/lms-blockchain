<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-9">
                <h1 class="page-title"> <span class="subject-name"><?php echo $subjectDetails['name']; ?></span>
                    <small>Following are the list of sections, assignment, exams of subject <?php echo $subjectDetails['name']; ?></small>
                </h1>
            </div>
            <div class="col-md-3">
                <div class="pull-right show-rating hide">
                    <small>
                        <input id="displayRating" type="number" min="0" max="5" step="0.5" data-size="xs" disabled="">
                    </small>
                </div>
            </div>
        </div>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo $sitepathStudent; ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo $sitepathStudentCourses; ?>">Courses</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <i class="icon-note"></i>
                    <a href="<?php echo $sitepathStudentCourses.$courseId; ?>"><?php echo $courseName; ?></a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span class="subject-name"><?php echo $subjectDetails['name']; ?></span>
                </li>
            </ul>
            <div class="page-toolbar">
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-fit-height green" data-toggle="modal" data-target="#reviewModal">Write a Review</button>
                    <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"> Actions
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li>
                            <a href="#tabLectures" data-toggle="sub-tab">
                                <i class="icon-bell"></i> Content &amp; Exams</a>
                        </li>
                        <li>
                            <a href="#tabSyllabus" data-toggle="sub-tab">
                                <i class="icon-shield"></i> Syllabus</a>
                        </li>
                        <!-- <li>
                            <a href="#tabExams" data-toggle="sub-tab">
                                <i class="icon-user"></i> Exam Assignment</a>
                        </li> -->
                        <li>
                            <a href="#tabChats" data-toggle="sub-tab">
                                <i class="icon-bag"></i> Chat</a>
                        </li>
                        <li>
                            <a href="#tabCalendar" data-toggle="sub-tab">
                                <i class="icon-user"></i> Calendar</a>
                        </li>
                        <li>
                            <a href="#tabNotes" data-toggle="sub-tab">
                                <i class="icon-bag"></i> Notes</a>
                        </li>
                        <li>
                            <a href="#tabPerformance" data-toggle="sub-tab">
                                <i class="icon-graph"></i> Performance &amp; Rewards</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue" id="subjectBox">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-note"></i><?php echo $subjectDetails['name']; ?> </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-2 col-sm-3 col-xs-12">
                                <ul class="nav nav-tabs tabs-left subject-sidebar">
                                    <li class="active">
                                        <a href="#tabLectures" data-toggle="tab"> Content &amp; Exams </a>
                                    </li>
                                    <li>
                                        <a href="#tabSyllabus" data-toggle="tab"> Syllabus </a>
                                    </li>
                                    <!-- <li>
                                        <a href="#tabExams" data-toggle="tab"> Exam Assignment </a>
                                    </li> -->
                                    <li>
                                        <a href="#tabLearnObj" data-toggle="tab"> Learning Objectives </a>
                                    </li>
                                    <li>
                                        <a href="#tabChats" data-toggle="tab"> Chat </a>
                                    </li>
                                    <li>
                                        <a href="#tabCalendar" data-toggle="tab"> Calendar <span class="badge badge-warning pull-right"> NEW! </span> </a>
                                    </li>
                                    <li>
                                        <a href="#tabNotes" data-toggle="tab"> Notes <span class="badge badge-warning pull-right"> NEW! </span> </a>
                                    </li>
                                    <li>
                                        <a href="#tabPerformance" data-toggle="tab"> Performance &amp; Rewards<span class="badge badge-warning pull-right"> NEW! </span> </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-10 col-sm-9 col-xs-12">
                                <div class="tab-content min-height-700">
                                    <div class="tab-pane active" id="tabLectures"></div>
                                    <div class="tab-pane fade" id="tabSyllabus"></div>
                                    <!-- <div class="tab-pane fade" id="tabExams"></div> -->
                                    <div class="tab-pane fade" id="tabLearnObj"></div>
                                    <div class="tab-pane fade" id="tabChats">
                                        <div class="row" id="listProfessors"></div>
                                    </div>
                                    <div class="tab-pane fade" id="tabCalendar">
                                        <div class="portlet box green calendar">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-layers"></i>
                                                    <span class="caption-subject sbold uppercase">Calendar</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">
                                                        <div id="calendar" class="has-toolbar"> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tabNotes"></div>
                                    <div class="tab-pane fade" id="tabPerformance">
                                        <div class="note note-success">
                                            <h4 class="block no-margin">Average Subject Score : <strong id="subjectScore"></strong></h4>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover order-column">
                                                <thead>
                                                    <tr>
                                                        <th width="20%"> Title </th>
                                                        <th width="10%"> Type </th>
                                                        <th width="11%"> Section </th>
                                                        <th class="text-center" width="5%"> Your / Total Attempts </th>
                                                        <th width="10%"> Grade Type </th>
                                                        <th width="10%"> Avg of last __ attempts </th>
                                                        <th width="10%"> Weight </th>
                                                        <th width="10%"> Performance </th>
                                                        <th width="10%"> Rewards </th>
                                                    </tr>
                                                </thead>
                                                <tbody id="listExams"></tbody>
                                            </table>
                                        </div>
                                        <div id="gradingNotes"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<div id="confirmChatModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p> Are you sure you want to chat with <span class="person-name"></span>? </p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Cancel</button>
                <a href="javascript:void(0)" target="_blank" class="btn green chat-link">Continue</a>
            </div>
        </div>
    </div>
</div>
<?php
    require_once("modals/subjectiveChecked.modal.php");
    require_once("modals/review.modal.php");
    require_once("modals/tags.modal.php");
?>