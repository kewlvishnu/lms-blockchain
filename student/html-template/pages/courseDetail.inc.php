<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h1 class="page-title"> <span class="course-name"><?php echo $courseDetails->courseDetails['name']; ?></span>
            <small>List of subjects, exams and assignments available in this course created by: <span class="institute-name"></span></small>
        </h1>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo $sitepathStudent; ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo $sitepathStudentCourses; ?>">Courses</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span class="course-name"><?php echo $courseDetails->courseDetails['name']; ?></span>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="portlet light portlet-fit portal-subjects">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-note font-dark hide"></i>
                            <span class="caption-subject bold font-dark uppercase"> Subjects</span>
                            <span class="caption-helper">list of course subjects</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row" id="listSubjects">
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="portlet light portlet-fit" id="listExams">
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-note font-green"></i>
                                <span class="caption-subject bold font-green uppercase"> Active Exams</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="timeline list-timeline">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet light portlet-fit" id="listAssignments">
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-note font-green"></i>
                                <span class="caption-subject bold font-green uppercase"> Active Assignment Exams</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="timeline list-timeline">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet light portlet-fit" id="listSubjectiveExams">
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-note font-green"></i>
                                <span class="caption-subject bold font-green uppercase"> Active Subjective Exams</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="timeline list-timeline"></div>
                        </div>
                    </div>
                </div>
                <div class="portlet light portlet-fit" id="listManualExams">
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-note font-green"></i>
                                <span class="caption-subject bold font-green uppercase"> Active Manual Exams</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="timeline list-timeline"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<?php
    require_once("modals/subjectiveChecked.modal.php");
?>