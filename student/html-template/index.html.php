<?php
	if ($pageType == "panel") {
		require_once('includes/header.php');

		require_once('includes/sidebar.php');
		
		switch ($page) {
			case 'home':
				require_once('pages/home.inc.php');
				break;
			case 'courses':
				require_once('pages/courses.inc.php');
				break;
			case 'courseDetail':
				require_once('pages/courseDetail.inc.php');
				break;
			case 'subjectDetail':
				require_once('pages/subjectDetail.inc.php');
				break;
			case 'profile':
				require_once('pages/profile.inc.php');
				break;
			case 'settings':
				require_once('pages/settings.inc.php');
				break;
			case 'notificationsActivity':
				require_once('pages/notificationsActivity.inc.php');
				break;
			case 'notificationsChat':
				require_once('pages/notificationsChat.inc.php');
				break;
			case 'examResult':
				require_once('pages/examResult.inc.php');
				break;
			case 'subjectiveExamResult':
				require_once('pages/subjectiveExamResult.inc.php');
				break;
			case 'manualExamResult':
				require_once('pages/manualExamResult.inc.php');
				break;
			case 'submission':
				require_once('pages/submission.inc.php');
				break;
			case 'submissionResult':
				require_once('pages/submissionResult.inc.php');
				break;
			default:
				echo "There was some error";
				break;
		}

		require_once('includes/footer.php');
	} else {
		// content page
		require_once('content/includes/header.php');
		require_once('content/includes/content.php');
		require_once('content/includes/footer.php');
	}
?>