function ajaxRequest(data){
    //$("#socialAJXError,#socialAJXSuccess").html('').hide();
    if(!(data.loaderDisable!=undefined && data.loaderDisable=="true"))
    {
        data.complete=function(){
            //hideL();
        }
    }
    $.ajax(data);
}
var readFile = function(file, options) {
    var dfd = new $.Deferred();
    
    // define FileReader object
    var reader = new FileReader();
    var that = this;

    // init reader onload event handlers
    reader.onload = function(e) {
        var image = $('<img/>')
            .load(function() {
                //when image fully loaded
                var newimageurl = getCanvasImage(this, options);
                dfd.resolve(newimageurl, this);
            })
           .attr('src', e.target.result);
    };
    reader.onerror = function(e) {
        dfd.reject("Couldn't read file " + file.name);
    }
    // begin reader read operation
    reader.readAsDataURL(file);
    return dfd.promise();
}
var getCanvasImage = function(image, options) {
    // define canvas
    var canvas = document.createElement("canvas");
    canvas.height = options.height;
    canvas.width = options.width;

    var ctx = canvas.getContext("2d");
	if (!options.crop) {
    	ctx.drawImage(image, 0, 0, image.width, image.height, 0, 0, canvas.width, canvas.height);
	} else {
		// h:196
		// w:196
		// x:0
		// x2:196
		// y:81
		// y2:277
		//console.log(parseInt(options.crop.x) +','+ parseInt(options.crop.y) +','+ parseInt(options.crop.w) +','+ parseInt(options.crop.h) +','+ 0 +','+ 0 +','+ 150 +','+ 150);
		ctx.drawImage(image, parseInt(options.crop.x), parseInt(options.crop.y), parseInt(options.crop.w), parseInt(options.crop.h), 0, 0, 150,150);
	}
    // fit image to canvas
    // convert canvas to jpeg url
    return canvas.toDataURL("image/jpeg");
}
var dataURItoBlob = function(dataURI) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs
    var byteString;
    if (dataURI.split(',')[0].indexOf("base64") >= 0) {
        byteString = atob(dataURI.split(',')[1]);
    } else {
        byteString = unescape(dataURI.split(',')[1]);
    }

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], {
        type: mimeString
    });
}
function compareWidthHeight(width, height) {
	var diff = [];
	if(width > height) {
		diff[0] = width - height;
		diff[1] = 0;
	} else {
		diff[0] = 0;
		diff[1] = height - width;
	}
	return diff;
}
function getUrlParameter(sParam)
{
	sParam = sParam.toLowerCase();
	var sPageURL = window.location.search.substring(1).toLowerCase();
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) 
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) 
		{
			return sParameterName[1];
		}
	}
}
function format(number) {
	if(number < 10)
		return '0' + number;
	else
		return number;
}

function percentage(number, total) {
	if(total == 0)
		return 0;
	return (parseInt(number)/parseInt(total)) * 100;
}

function timeSince(date) {

    var seconds = Math.floor((new Date() - date) / 1000);

    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + " yrs";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hours";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " min";
    }
    return "Just Now";
}

function calcDisplayTimeWords(totalDuration) {
	var hours = parseInt( totalDuration / 3600 ) % 24;
	var minutes = parseInt( totalDuration / 60 ) % 60;
	var seconds = totalDuration % 60;

	if(hours>0) {
		if(minutes>0 && minutes<=15) {
			return hours+".25h";
		} else if(minutes>15 && minutes<=30) {
			return hours+".5h";
		} else if(minutes>30 && minutes<=45) {
			return hours+".75h";
		} else {
			return hours+"h";
		}
	} else if(minutes>0) {
		if(seconds>0 && seconds<=15) {
			return minutes+".25m";
		} else if(seconds>15 && seconds<=30) {
			return minutes+".5m";
		} else if(seconds>30 && seconds<=45) {
			return minutes+".75m";
		} else {
			return minutes+"m";
		}
	} else {
		return seconds+" s";
	}
}

function getTime12Hour (hours, minutes) {
	//var hours = date.getHours();
	//var minutes = date.getMinutes();
	var ampm = hours >= 12 ? 'pm' : 'am';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0'+minutes : minutes;
	var strTime = hours + ':' + minutes + '' + ampm;
	return strTime;
}

function emailValidate(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(regex.test(email))
		return true;
	return false;
}

function bookmarkThisPage(e) {
	var bookmarkURL = window.location.href;
    var bookmarkTitle = document.title;

    if ('addToHomescreen' in window && window.addToHomescreen.isCompatible) {
      // Mobile browsers
      addToHomescreen({ autostart: false, startDelay: 0 }).show(true);
    } else if (window.sidebar && window.sidebar.addPanel) {
      // Firefox version < 23
      window.sidebar.addPanel(bookmarkTitle, bookmarkURL, '');
    } else if ((window.sidebar && /Firefox/i.test(navigator.userAgent)) || (window.opera && window.print)) {
      // Firefox version >= 23 and Opera Hotlist
      $(this).attr({
        href: bookmarkURL,
        title: bookmarkTitle,
        rel: 'sidebar'
      }).off(e);
      return true;
    } else if (window.external && ('AddFavorite' in window.external)) {
      // IE Favorite
      window.external.AddFavorite(bookmarkURL, bookmarkTitle);
    } else {
      // Other browsers (mainly WebKit - Chrome/Safari)
      alert('Press ' + (/Mac/i.test(navigator.userAgent) ? 'Cmd' : 'Ctrl') + '+D to bookmark this page.');
    }

    return false;
}

/**
 * Returns a random number between min (inclusive) and max (exclusive)
 */
function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
//function to format time for 2 digits
function formatTime2Digits(time) {
    time = parseInt(time);
    if (time<10) {
        time = '0'+time;
    };
    return time;
}
//function to format time
function formatTime(time) {
    var a = new Date(time * 1000);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var formattedTime = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    return formattedTime;
}
//function to format date
function formatDate(time,delimiter = ' ') {
    var a = new Date(time);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = formatTime2Digits(a.getDate());
    var formattedDate = date + delimiter + month + delimiter + year;
    return formattedDate;
}
function getToday() {
    return formatDate(Math.floor(Date.now()));
}
function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};
//function to convert given seconds into minute and seconds
function convertSecondsIntoMinutes(sec) {
    var min = 0;
    if(sec - 60 >= 0) {
        min = Math.floor(sec / 60);
        sec = sec - (min * 60);
    }
    var time = min + ' min ' + sec + ' sec';
    return time;
}
function scrollToElem(scrollCon,scrollTime) {
    $('html, body').animate({
        scrollTop: $(scrollCon).offset().top - 100
    }, scrollTime);
}
function round2(number) {
    return Math.round(number*100)/100;
}
// Check if the browser is Internet Explorer
function isIE() {
    var myNav = navigator.userAgent.toLowerCase();
    return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
} 