<?php
	$global = new stdClass();
	//loading zend framework
	$libPath = "../api/Vendor";
	include_once $libPath . "/Zend/Loader/AutoloaderFactory.php";
	require_once $libPath . "/ArcaneMind/Api.php";
	Zend\Loader\AutoloaderFactory::factory(array(
			'Zend\Loader\StandardAutoloader' => array(
					'autoregister_zf' => true,
					'db' => 'Zend\Db\Sql'
			)
	));
	if ($page == 'logout') {
		unset($_SESSION["userId"]);
		unset($_SESSION["userRole"]);
		unset($_SESSION['username']);
		unset($_SESSION['temp']);
		session_destroy();
		setcookie('mtwebLogin', "", time() - 3600, '/');
		unset($_COOKIE['mtwebLogin']);
	}
	if(isset($_COOKIE["mtwebLogin"]) && !empty($_COOKIE["mtwebLogin"])) {
		require_once '../api/Vendor/ArcaneMind/User.php';
		$cookie = $_COOKIE["mtwebLogin"];
		$res = Api::checkUserRemembered($cookie);
		$_SESSION["userId"] = $res->user['id'];
		$_SESSION["userRole"] = $res->user['roleId'];
		$_SESSION["username"] = $res->user['email'];
		$_SESSION["temp"] = $res->user['temp'];
		$_SESSION["details"] = $res->user['details'];
	}
	if(isset($_GET['pageDetailId']) && !empty($_GET['pageDetailId'])) {
		$actual_link  = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$temp_link    = explode("/", $actual_link);
		$sub_link = explode(".", $temp_link[2]);
		$final_link = str_replace($sub_link[0], 'www', $actual_link);
		$subdomain = trim($_GET['pageDetailId']);
		$res = Api::getPortfolioBySlug($subdomain);
		if($res->valid) {
			$portfolioDetails = $res;
			$portfolioName 	  = $portfolioDetails->portfolio['name'];
			$logoPath = $res->portfolio['img'];
			if(!empty($res->portfolio['favicon'])) {
				$favicon  = $res->portfolio['favicon'];
			}
			$sitepathStudentIncludes = str_replace($sub_link[0], 'www', $sitepathStudentIncludes);
		} else {
			header('location:'.$final_link);
		}
	}
	$title = 'Integro | Instructors | Institutes | Online Courses | Entrance Exam Preparation';
	//$description = 'Integro is a platform & marketplace for selling online courses & entrance exam preparation material.';
	$description = 'Integro is a platform & marketplace for online courses & entrance exam preparation. We have the best instructors and institutes in the world, we ensure that the content and questions are of the highest quality.';
	$image = 'http://dhzkq6drovqw5.cloudfront.net/fb-arcanemind.jpg';

	$logolink = $sitepathMarket;

	if(isset($logoPath) && !empty($logoPath)) {
		$logoBlock = '<a href="'.$logolink.'">
                        <img src="'.$logoPath.'" alt="'.$portfolioName.'" alt="logo" class="logo-default" />
                    </a>';
	} else {
		$logoBlock = '<a href="'.$logolink.'">
                        <img src="'.$sitepathStudent.'assets/global/img/logo-default.png" alt="Integro" alt="logo" class="logo-default" />
                    </a>';
	}

	if (isset($_SESSION["temp"])) {
		//var_dump($_SESSION);
		if ($_SESSION["temp"]==1 && $page != "initStudent" && $page != "policiesTerms" && $page != "policiesPrivacy") {
			header('location:'.$sitepathMarket.'init-student/');
		}
		if ($_SESSION["temp"]==0 && $page == "initStudent") {
			header('location:'.$sitepathCourses);
		}
	}

	if ($page == "profile") {
		$gCountries = Api::getCountriesList();
		$gCountries = $gCountries->countries;
	} else if ($page == "courseDetail") {
		$data = new stdClass();
		$data->courseId = $courseId;
		$courseDetails = Api::getCourseDetails($data);
		if ($courseDetails->status == 0) {
			header('location:'.$sitepath404);
		}
	} else if ($page == "subjectDetail") {
		$data = new stdClass();
		$data->subjectId = $subjectId;
		$data->userId = $_SESSION['userId'];
		$data->userRole = $_SESSION['userRole'];
		$subjectDetails = Api::getSubjectDetails($data);
		if ($subjectDetails->status == 0) {
			header('location:'.$sitepath404);
		} else {
			$subjectDetails = $subjectDetails->subjectDetails;
			$subjectId		= $subjectDetails['id'];
			$courseId		= $subjectDetails['courseId'];
			$courseName		= $subjectDetails['courseName'];
			$data = new stdClass();
			$data->courseId = $courseId;
			$courseDetails	= Api::getCourseDetails($data);
			if ($courseDetails->status == 0) {
				header('location:'.$sitepath404);
			}
		}
	} else if ($page == "examResult") {
		$data = new stdClass();
		$data->examId = $examId;
		$data->userId = $_SESSION['userId'];
		$examDetails = Api::getExamDetail($data);
		if ($examDetails->status == 0) {
			header('location:'.$sitepath404);
		}
		$examDetails = $examDetails->exam;
	} else if ($page == "subjectiveExamResult") {
		$data = new stdClass();
		$data->examId = $examId;
		$data->userId = $_SESSION['userId'];
		$examDetails = Api::getSubjectiveExamDetail($data);
		if ($examDetails->status == 0) {
			header('location:'.$sitepath404);
		}
		$examDetails = $examDetails->exam;
	} else if ($page == "manualExamResult") {
		$data = new stdClass();
		$data->examId = $examId;
		$data->userId = $_SESSION['userId'];
		$examDetails = Api::getManualExamDetail($data);
		if ($examDetails->status == 0) {
			header('location:'.$sitepath404);
		}
		$examDetails = $examDetails->exam;
		$courseId = $examDetails['courseId'];
		$subjectId = $examDetails['subjectId'];
	} else if ($page == "submission" || $page == "submissionResult") {
		$data = new stdClass();
		$data->examId = $examId;
		$data->userId = $_SESSION['userId'];
		$examDetails = Api::getSubmissionDetail($data);
		if ($examDetails->status == 0) {
			header('location:'.$sitepath404);
		}
		$examDetails = $examDetails->exam;
		$courseId = $examDetails['courseId'];
		$subjectId = $examDetails['subjectId'];
	} else if ($page == "content") {
		$data = new stdClass();
		$data->subjectId = $subjectId;
		$data->userId = $_SESSION['userId'];
		$content = Api::getHeadings($data);
		if ($content->status == 0) {
			header('location:'.$sitepath404);
		}
		$subjectName= $content->subjectName;
		$content	= $content->headings;
		if ($chapterId == 0) {
			$chapterId = $content[0]['id'];
			$contentId = $content[0]['content'][0]['id'];
		} else {
			if ($contentId == 0) {
				$key = array_search($chapterId, array_column($content, 'id'));
				$contentId = $content[$key]['content'][0]['id'];
			}
		}
		/*var_dump($content);
		die();*/
	}
	require_once '../api/Vendor/ArcaneMind/CryptoWallet.php';
	$w = new CryptoWallet();
	$data = new stdClass();
	$data->userId = $_SESSION['userId'];
	$data->userRole = $_SESSION['userRole'];
	$res = $w->getWalletAddress($data);
	$global->walletAddress = '';
	$global->walletApp = '';
	if ($res->status == 1) {
		if (isset($res->wallet) && !empty($res->wallet)) {
			$global->walletAddress = $res->wallet;
			$global->walletApp = (($res->app=='metamask')?'m':'p');
		}
	}
	$res = $w->getIGROContractData($data);
	if ($res->status == 1) {
		$global->toAddress = $res->ownerAddress;
		$global->contractAddress = $res->contractAddress;
		$global->contractCode = $res->contractCode;
		$global->decimalPrecision = $res->decimalPrecision;
		$global->balanceDivisor = $res->balanceDivisor;
		$global->displayCurrency = $res->displayCurrency;
	}

	//var_dump($_SESSION);
?>