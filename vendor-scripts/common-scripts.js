/*---LEFT BAR ACCORDION----*/
$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
});
/* for navigation to courses.php page after adding tree view feature */
$('.course').click(function(){
	window.location = 'courses.php';
});

var Script = function () {

//    sidebar dropdown menu auto scrolling

    jQuery('#sidebar .sub-menu > a').click(function () {
        var o = ($(this).offset());
        diff = 250 - o.top;
        if(diff>0)
            $("#sidebar").scrollTo("-="+Math.abs(diff),500);
        else
            $("#sidebar").scrollTo("+="+Math.abs(diff),500);
    });

//    sidebar toggle

    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#container').addClass('sidebar-close');
                $('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });
	$('.fa-plus').click(function (e) {
		if ($('#sidebar > ul').is(":visible") === true) {
            // $('#main-content').css({
                // 'margin-left': '-210px'
            // });
            $('#sidebar').css({
                'margin-left': '-410px'
            });
			$('.custom-course').css({
				'margin-left': '0px'
			});
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        } else {
            // $('#main-content').css({
                // 'margin-left': '210px'
            // });
			$('.custom-course').css({
				'margin-left': '17%'
			});
            $('#sidebar > ul').show();
            $('#sidebar').css({
                'margin-left': '0px'
            });
            $("#container").removeClass("sidebar-closed");
        }
	
    });
	
    $('.fa-bars').on('click', function () {
        if ($('#sidebar > ul').is(":visible") === true) {
            $('#main-content').css({
                'margin-left': '0px'
            });
            $('#sidebar').css({
                'margin-left': '-210px'
            });
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        } else {
            $('#main-content').css({
                'margin-left': '210px'
            });
            $('#sidebar > ul').show();
            $('#sidebar').css({
                'margin-left': '0'
            });
            $("#container").removeClass("sidebar-closed");
        }
    });
	
	

// custom scrollbar
     $("#sidebar").niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '3', cursorborderradius: '10px', background: '#404040', spacebarenabled:false, cursorborder: ''});

    // $("html").niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '6', cursorborderradius: '10px', background: '#404040', spacebarenabled:false,  cursorborder: '', zindex: '1000'});

// widget tools

    jQuery('.panel .tools .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".panel").children(".panel-body");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });

    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });


//    tool tips

    $('.tooltips').tooltip();

//    popovers

    $('.popovers').popover();



// custom bar chart

    if ($(".custom-bar-chart")) {
        $(".bar").each(function () {
            var i = $(this).find(".value").html();
            $(this).find(".value").html("");
            $(this).find(".value").animate({
                height: i
            }, 2000)
        })
    }


}();

function getUrlParameter(sParam)
{
	sParam = sParam.toLowerCase();
    var sPageURL = window.location.search.substring(1).toLowerCase();
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

function alertMsg(msg, time){
	time = time || 2000;
	
	var top = '10px';
	if($('.bubble-msg').length > 0)
	  top = parseInt($('.bubble-msg').last().css('top').replace(/px/, '')) + $('.bubble-msg').last().outerHeight() + 10 + 'px';
	var elem = $('<div />').html(msg).attr('title', 'Click to dismiss.').addClass('bubble-msg animate-300')
		.appendTo($('body')).animate({'top': top}, 300);
	if(time != -1) {
		elem.removeAfter(time);
		$('.bubble-msg').on('click', function(){
			$(this).removeAfter(0);
		});
	}	
}


$.fn.removeAfter = function(time) {
	var elem = $(this);
	setTimeout(function() {
		elem.animate({'opacity': 0}, 300);
		setTimeout(function() {
			elem.remove();
		}, 300);
	}, time);
}

function nl2br(str){
	return str.replace(/(?:\r\n|\r|\n)/g, '<br />');
}