var roleuser=0;
$(document).ready(function() {
	addResetListener(getUrlParameter('id'));
	
	$("#signIn").click(function(e) {
		e.preventDefault();
		$('.error-msg').hide();
		var user=$('#user').val();
		var pwd=$('#pwd').val();
		if(user.length<3)
			$('#userError').html("A valid username is required.").show();
		if(pwd.length<6)
			$('#pwdError').html("A valid password is required.").show();
		if(user.length>=3 && pwd.length>=6) {
			req = {
					'action'	: 'portfolio-login',
					'username'	: user,
					'password'	: pwd,
					'userRole'	: $('#userRole').val(),
					'portfolio' : 1,
					'slug'		: p_slug
				};
			req=JSON.stringify(req);
			$.post(ApiEndPoint, req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				console.log(res);
				if(res.valid == false) {
					if(res.reset == 1) {
						$('#signInError').html('<p class="text-danger">'+res.reason + '<a href="#" class="resend-link">Resend email</a></p>').show();
						addResetListener(res.userId);
					}
					else
						$('#signInError').html('<p class="text-danger">'+res.reason+'</p>').show();
				}
				if(res.portfolioValid == false) {
					$('#signInError').html('<p class="text-danger">'+res.reason+'</p>').show();
				}
				if(res.valid == true && res.portfolioValid == true && res.userRole == 4) {
					window.location = actualDomain+"student/";
				}
				else if(res.valid == true && res.portfolioValid == true && (res.userRole == 1 || res.userRole == 2))
				{
					window.location = actualDomain+"manage/";
				}
				else if(res.valid == true && res.portfolioValid == true)
				{
					window.location = actualDomain+"admin/dashboard.php";
				}
			});
		}
	});
    $('#userRole').on('change',function(){
        $('#signInError').html('').hide();
    });
   	$('#resetPassword').click(function() {
		$('.error-msg').hide();
		var fuser=$('#fuser').val();
		if(fuser.length < 6)
			$('#fuserError').html("A valid username is required.").show();
		if(fuser.length >= 6) {
			var req = {};
			req.action = 'send-reset-password-link';
			req.username = fuser;
			req.userRole = $('#fuserRole').val();
			req = JSON.stringify(req);
			$.post(ApiEndPoint, req).success(function(resp) {
				res = jQuery.parseJSON(resp);
				$('#forgetError').html(res.message).show();
			});
		}
	});
	
	function getUrlParameter(sParam)
	{
		sParam = sParam.toLowerCase();
		var sPageURL = window.location.search.substring(1).toLowerCase();
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++) 
		{
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam) 
			{
				return sParameterName[1];
			}
		}
	}
	
	function addResetListener(userId) {
		$('.resend-link').off('click');
		//resend verification link
		$('.resend-link').on('click', function() {
			var req = {};
			var res;
			req.id = userId;
			if(req.id == undefined)
				alert("Some error occurred contact admin.");
			req.action = 'resend-verification-link';
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndpoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else
					alert("Verification mail sent. Please check your mailbox.");
			});
		});
	}
	
});