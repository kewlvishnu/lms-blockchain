<?php
	//code to update any paused exam or assignment
	// Loading Zend
	@session_start();
	$libPath = "../api/Vendor";
	include $libPath . '/Zend/Loader/AutoloaderFactory.php';
	require $libPath . "/ArcaneMind/Api.php";
	Zend\Loader\AutoloaderFactory::factory(array(
			'Zend\Loader\StandardAutoloader' => array(
					'autoregister_zf' => true,
					'db' => 'Zend\Db\Sql'
			)
	));
	require_once '../api/Vendor/ArcaneMind/User.php';
	if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
		$favicon = "http://$_SERVER[HTTP_HOST]/img/favicon.ico";
	} else {
		$favicon = "../favicon.ico";
	}
	if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
		$actual_link  = "http://$_SERVER[HTTP_HOST]";
		$temp_link    = explode("/", $actual_link);
		$sub_link = explode(".", $temp_link[2]);
		$site_link = str_replace($sub_link[0], 'www', $actual_link);
		$portfolio_link = $site_link."/portfolio/";
		$redirect_link = $site_link.$_SERVER["REQUEST_URI"];
	} else {
		$actual_link  = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$site_link = $actual_link;
		$portfolio_link = $site_link."/portfolio/";
	}

	$portfolioName = "";
	$portfolioDetails = '';

	if(isset($_GET['pageDetailId']) && !empty($_GET['pageDetailId'])) {
		$slug = trim($_GET['pageDetailId']);
		$actual_slug = $slug;
		$res = Api::getPortfolioDetail($slug);
		if($res->valid && ($res->portfolio['status'] == 'Draft' || $res->portfolio['status'] == 'Published') ) {
			if($res->portfolio['status'] == 'Published' || ($res->owner == 1)) {
				$portfolio    = $res->portfolio;
				//var_dump($portfolio);
				$logoPath     = 'img/arcanemind-logo.png';
				$portfolioName     = $portfolio['name'];
				$portfolioPic = $portfolio['img'];
				if (!empty($portfolio['description'])) {
					$description = $portfolio['description'];
				} else {
					$description = 'Portfolio Page';
				}
				if(!empty($res->portfolio['favicon'])) {
					$favicon  = $res->portfolio['favicon'];
				}
				$portfolioPages  = $portfolio['portfolio']->portfolioPages;
			}
		} else {
			header('location:404.html');
		}
	} else {
		header('location:404.html');
	}
	if(($res->portfolio['status'] != 'Published') && ($res->owner == 0) && isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
		header('location:404.html');
	}
?>
<!-- saved from url=(0014)about:internet -->
<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?php echo $portfolioName; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="author" content="Arcane">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="description" content="<?php echo $description; ?>" />
		<meta name="keywords" content="" />
		<base href="<?php echo $site_link; ?>">
		<link rel="shortcut icon" href="<?php echo $favicon; ?>">

		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!--CSS styles-->
		<?php
			if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
		?>
		<link rel="stylesheet" href="/portfolio/css/bootstrap.css">
		<link rel="stylesheet" href="/portfolio/css/font-awesome.min.css">
		<link rel="stylesheet" href="/portfolio/css/perfect-scrollbar-0.4.5.min.css">
		<link rel="stylesheet" href="/portfolio/css/magnific-popup.css">
		<link rel="stylesheet" href="/portfolio/css/style.css">
		<link rel="stylesheet" href="/portfolio/css/fac-homepage.css">
		<link rel="stylesheet" href="/portfolio/css/default.css" id="theme-style">
		<!-- <link rel="stylesheet" href="<?php echo $portfolio_link; ?>css/coursevideo1.css" data-turbolinks-track="true"></link> -->
		<!--/CSS styles-->

		<!--Custom Styles for demo only-->
		<link rel="stylesheet" href="/portfolio/css/custom-style.css">
		<!--Custom Styles-->
		<?php
			} else {
		?>
		<link rel="stylesheet" href="/arcanemind/portfolio/css/bootstrap.css">
		<link rel="stylesheet" href="/arcanemind/portfolio/css/font-awesome.min.css">
		<link rel="stylesheet" href="/arcanemind/portfolio/css/perfect-scrollbar-0.4.5.min.css">
		<link rel="stylesheet" href="/arcanemind/portfolio/css/magnific-popup.css">
		<link rel="stylesheet" href="/arcanemind/portfolio/css/style.css">
		<link rel="stylesheet" href="/arcanemind/portfolio/css/fac-homepage.css">
		<link rel="stylesheet" href="/arcanemind/portfolio/css/default.css" id="theme-style">
		<!-- <link rel="stylesheet" href="<?php echo $portfolio_link; ?>css/coursevideo1.css" data-turbolinks-track="true"></link> -->
		<!--/CSS styles-->

		<!--Custom Styles for demo only-->
		<link rel="stylesheet" href="/arcanemind/portfolio/css/custom-style.css">
		<!--Custom Styles-->
		<?php
			}
		?>
	</head>
<body>
<?php
	if(($res->portfolio['status'] == 'Published') || ($res->owner == 1)) {
		if ($res->userRole == 1) {
			require_once('html-template/institute.php');
		} else {
			require_once('html-template/professor.php');
		}
		if(!(isset($_SESSION['userId']) && !empty($_SESSION['userId']))) {
?>
<!-- Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="loginModalLabel">Login Form</h4>
      </div>
      <div class="modal-body">
        <div class="login-form clearfix">
            <form role="form">
                <div class="form-group">
                    <select id="userRole" class="form-control">
                        <option value="4">I am a student</option>
                        <option value="2">I am a professor/tutor</option>
                        <option value="1">I represent an institute</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" placeholder="Enter username or email" id="user" class="form-control">
                    <div id="userError" class="error-msg"></div>
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Password" id="pwd" class="form-control">
                    <div id="pwdError" class="error-msg"></div>
                </div>
                <div class="checkbox pull-right">
                    <label>
                        <input type="checkbox" id="remember"> Remember me on this computer
                    </label>
                </div>
                <div class="pull-left">
                    <div id="signInError" class="error-msg"></div>
                    <button id="signIn" class="btn btn-primary" type="submit">Log In</button>
                </div>
            </form>
        </div>
      </div>
      <div class="modal-footer">
        <div class="pull-left">
            <a data-dismiss="modal" data-toggle="modal" href="#forgetPasswordModal">Forgot Password?</a>
        </div>
        <div class="pull-right">
            <span>Don't have an account?</span>
            <a class="btn btn-danger" href="<?php echo $actual_link; ?>/signup">Register</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="forgetPasswordModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close"  data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Forgot Password?</h3>
			</div>
			<div class="modal-body">
				<div class="login-form clearfix">
					<form role="form">
						<input type="hidden" id="fuserRole" value="4" />
						<div class="form-group">
							<input type="text" class="form-control" id="fuser" placeholder="Enter Email or Username">
							<div class="error-msg" id="fuserError"></div>
						</div>
						<div class="pull-left">
							<div class="error-msg" id="forgetError"></div>
							<a class="btn btn-success" id="resetPassword">Reset</a>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<div class="pull-left">
					<a data-dismiss="modal" data-toggle="modal" href="#loginModal">Login</a>
				</div>
				<div  class="pull-right">
					<span>Don't have an account?</span>
					<a href="signup-student.php#student" class="btn btn-primary">Register</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
        }
    } else if ($res->portfolio['status'] == 'Draft') {
        require_once('html-template/login.php');
    }
    require_once("jsphp/variables.js.php");
?>
<!--Javascript files-->
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script type="text/javascript" src="/portfolio/js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="/portfolio/js/TweenMax.min.js"></script>
<script type="text/javascript" src="/portfolio/js/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" src="/portfolio/js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script type="text/javascript" src="/portfolio/js/modernizr.custom.63321.js"></script>
<script type="text/javascript" src="/portfolio/js/jquery.dropdownit.js"></script>
<script type="text/javascript" src="/portfolio/js/jquery.stellar.min.js"></script>
<script type="text/javascript" src="/portfolio/js/ScrollToPlugin.min.js"></script>
<script type="text/javascript" src="/portfolio/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/portfolio/js/jquery.mixitup.min.js"></script>
<script type="text/javascript" src="/portfolio/js/masonry.min.js"></script>
<script type="text/javascript" src="/portfolio/js/perfect-scrollbar-0.4.5.with-mousewheel.min.js"></script>
<script type="text/javascript" src="/portfolio/js/magnific-popup.js"></script>
<script type="text/javascript" src="/portfolio/js/custom.js"></script>
<script type="text/javascript" src="/portfolio/js/login.js"></script>
<?php if (isset($res->userRole) &&$res->userRole == 1) { ?>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="/portfolio/js/map.js"></script>
<?php } ?>
</body>
</html>