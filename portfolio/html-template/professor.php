<div id="wrapper">
    <a href="#sidebar" class="mobilemenu"><i class="icon-reorder"></i></a>
    <div id="sidebar">
        <div id="main-nav">
            <div id="nav-container">
                <div id="profile" class="clearfix">
                    <div class="portrate hidden-xs" style="background-image:url(<?php echo $portfolioPic; ?>)"></div>
                    <div class="title">
                        <h2><?php echo $portfolioName; ?></h2>
                        <h3>Stanford University</h3>
                    </div>
                </div>
                <ul id="navigation">
                    <?php
                        foreach ($portfolioPages as $key => $page) {
                            if($page['type'] == 'page' && $page['pageType'] == 'courses') {
                                $pfIcon = '<div class="icon glyphicon glyphicon-book"></div>';
                            } else if($page['type'] == 'page' && $page['pageType'] == 'about') {
                                $pfIcon = '<div class="icon glyphicon glyphicon-user"></div>';
                            } else if($page['type'] == 'page' && $page['pageType'] == 'publications') {
                                $pfIcon = '<div class="icon glyphicon glyphicon-edit"></div>';
                            } else if($page['type'] == 'page' && $page['pageType'] == 'teaching') {
                                $pfIcon = '<div class="icon glyphicon glyphicon-time"></div>';
                            } else if($page['type'] == 'page' && $page['pageType'] == 'gallery') {
                                $pfIcon = '<div class="icon glyphicon glyphicon-picture"></div>';
                            } else if($page['type'] == 'page' && $page['pageType'] == 'contact') {
                                $pfIcon = '<div class="icon glyphicon glyphicon-calendar"></div>';
                            } else if($page['type'] == 'page' && $page['pageType'] == 'custom') {
                                $pfIcon = '<div class="icon glyphicon glyphicon-book"></div>';
                            } else if($page['type'] == 'url') {
                                $pfIcon = '<div class="icon glyphicon glyphicon-book"></div>';
                            }
                            echo '<li>
                                    <a href="#slug'.($key+1).'">
                                        '.$pfIcon.'
                                        <div class="text">'.$page['name'].'</div>
                                    </a>
                                </li>';
                        }
                        /*<li>
                            <a href="#biography">
                                <div class="icon glyphicon glyphicon-book"></div>
                                <div class="text">Courses</div>
                            </a>
                        </li>
                        <li>
                            <a href="#aboutme">
                                <div class="icon glyphicon glyphicon-user"></div>
                                <div class="text">About Me</div>
                            </a>
                        </li>
                        <li>
                            <a href="#publications">
                                <div class="icon glyphicon glyphicon-edit"></div>
                                <div class="text">Publications</div>
                            </a>
                        </li>
                        <li>
                            <a href="#teaching">
                                <div class="icon glyphicon glyphicon-time"></div>
                                <div class="text">Teaching</div>
                            </a>
                        </li>
                        <li>
                            <a href="#gallery">
                                <div class="icon glyphicon glyphicon-picture"></div>
                                <div class="text">Gallery</div>
                            </a>
                        </li>
                        <li>
                            <a href="#contact">
                                <div class="icon glyphicon glyphicon-calendar"></div>
                                <div class="text">Contact &amp; Meet Me</div>
                            </a>
                        </li>
                        <li class="external">
                            <a href="#">
                                <div class="icon glyphicon glyphicon-download-alt"></div>
                                <div class="text">Download CV</div>
                            </a>
                        </li>*/
                    ?>
                </ul>
                <div class="login-links">
                <?php
                    @session_start();
                    if(!(isset($_SESSION['userId']) && !empty($_SESSION['userId']))) {
                ?>
                    <a href="javascript:void(0)" data-href="#loginModal" class="js-popup">Login</a> /<a href="<?php echo $actual_link; ?>/signup" class="js-pf-links">Register</a>
                <?php
                    } else {
                        if($_SESSION['userRole'] == 4) {
                            ?>
                            <a href="<?php echo $actualDomain; ?>/student/" class="js-pf-links">Dashboard</a></li> / <a href="<?php echo $actualDomain; ?>signout" class="js-pf-links">Logout</a></li>
                            <?php
                        }
                        else if($_SESSION['userRole'] == 1){
                            ?>
                            <a href="<?php echo $actualDomain; ?>/manage/" class="js-pf-links">Dashboard</a></li> / <a href="<?php echo $actualDomain; ?>signout" class="js-pf-links">Logout</a></li>
                            <?php
                        }
                        else if($_SESSION['userRole'] == 2){
                            ?>
                            <a href="<?php echo $actualDomain; ?>/manage/" class="js-pf-links">Dashboard</a></li> / <a href="<?php echo $actualDomain; ?>signout" class="js-pf-links">Logout</a></li>
                            <?php
                        }
                        else if($_SESSION['userRole'] == 3){
                            ?>
                            <a href="<?php echo $actualDomain; ?>/admin/dashboard.php" class="js-pf-links">Dashboard</a></li> / <a href="<?php echo $actualDomain; ?>signout" class="js-pf-links">Logout</a></li>
                            <?php
                        }
                        else if($_SESSION['userRole'] == 5){
                            ?>
                            <a href="<?php echo $actualDomain; ?>/admin/approvals.php" class="js-pf-links">Dashboard</a></li> / <a href="<?php echo $actualDomain; ?>signout" class="js-pf-links">Logout</a></li>
                            <?php
                        }
                    }
                ?>
                </div>
            </div>        
        </div>
        <div class="social-icons">
            <ul>
                <li><a href="#"><i class="icon-facebook"></i></a></li>
                <li><a href="#"><i class="icon-twitter"></i></a></li>
                <li><a href="#"><i class="icon-linkedin"></i></a></li>
            </ul>
        </div>    
    </div>
    <div id="main">
        <?php
            foreach ($portfolioPages as $key => $page) {
                if($page['type'] == 'page' && $page['pageType'] == 'courses') {
                    require_once('html-template/templates/courses.html.php');
                } else if($page['type'] == 'page' && $page['pageType'] == 'about') {
                    require_once('html-template/templates/about.html.php');
                } else if($page['type'] == 'page' && $page['pageType'] == 'publications') {
                    require_once('html-template/templates/publications.html.php');
                } else if($page['type'] == 'page' && $page['pageType'] == 'teaching') {
                    require_once('html-template/templates/teaching.html.php');
                } else if($page['type'] == 'page' && $page['pageType'] == 'gallery') {
                    require_once('html-template/templates/gallery.html.php');
                } else if($page['type'] == 'page' && $page['pageType'] == 'contact') {
                    require_once('html-template/templates/contact.html.php');
                } else if($page['type'] == 'page' && $page['pageType'] == 'custom') {
                    require_once('html-template/templates/custom.html.php');
                } else if($page['type'] == 'url') {
                    require_once('html-template/templates/external.html.php');
                }
            }
        ?>
        <div id="overlay"></div>
    </div>
</div>