<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="pf-form-login">
                <h2 class="pf-head text-center">Login Form</h2>
                <form role="form text-center">
                    <div class="form-group">
                        <label for="userRole">User Role :</label>
                        <select id="userRole" class="form-control">
                            <option value="2">I am a professor/tutor</option>
                            <option value="1">I represent an institute</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="user">Username or Email :</label>
                        <input type="text" placeholder="Enter username or email" id="user" class="form-control">
                        <div id="userError" class="error-msg"></div>
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password :</label>
                        <input type="password" placeholder="Password" id="pwd" class="form-control">
                        <div id="pwdError" class="error-msg"></div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="remember"> Remember me on this computer
                            </label>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <div id="signInError"></div>
                        <button id="signIn" class="btn btn-default" type="submit">Log In</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>