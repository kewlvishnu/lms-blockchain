<div id="<?php echo 'slug'.($key+1); ?>" class="page <?php echo (($key==0)?'home':''); ?>"  <?php echo (($key==0)?'data-pos="home"':''); ?>>
    <div class="pageheader">
        <div class="headercontent">
            <div class="section-container">
                <div class="row">
                    <div class="col-sm-2 visible-sm"></div>
                    <div class="clearfix visible-sm visible-xs"></div>
                    <div class="col-sm-12">
                        <h3 class="title"><?php echo $page['pageTitle']; ?><?php
                            @session_start();
                            if(!(isset($_SESSION['userId']) && !empty($_SESSION['userId']))) {
                        ?><a href="#loginModal" class="btn btn-primary pull-right" data-toggle="modal">Login</a><?php
                            } else {
                                if($_SESSION['userRole'] == 4) {
                                ?><a href="<?php echo $actual_link; ?>/student/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 1){
                                ?><a href="<?php echo $actual_link; ?>/manage/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 2){
                                ?><a href="<?php echo $actual_link; ?>/manage/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 3){
                                ?><a href="<?php echo $actual_link; ?>/admin/dashboard.php" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 5){
                                ?><a href="<?php echo $actual_link; ?>/admin/approvals.php" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                }
                            }
                        ?></h3>
                        <br />
                        <p><?php echo $page['about']['description']; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pagecontents">
        <?php
            $academic = $page['about']['academic'];
            $education = $page['about']['education'];
            if(count($academic) > 0 && count($education) > 0) {
        ?>
        <div class="section color-1">
            <div class="section-container">
                <div class="row">
                    <?php
                        if(count($academic) > 0) {
                    ?>
                    <div class="col-md-5 col-md-offset-1">
                        <div class="title text-center">
                            <h3><?php echo $page['about']['academic_title']; ?></h3>
                        </div>
                        <?php
                            $academicHtml = '<ul class="ul-dates">';
                            foreach ($academic as $key => $value) {
                                $academicHtml.= '<li>
                                                    <div class="dates">
                                                        <span>'.(($value['toyear'] == '0000')?'Present':$value['toyear']).'</span>
                                                        <span>'.$value['fromyear'].'</span>
                                                    </div>
                                                    <div class="content">
                                                        <h4>'.$value['position'].'</h4>
                                                        <p><em>'.$value['university'].'</em>, '.$value['department'].'</p>
                                                    </div>
                                                </li>';
                            }
                            $academicHtml.= '</ul>';
                            echo $academicHtml;
                        ?>
                        <!-- <ul class="ul-dates">
                            <li>
                                <div class="dates">
                                    <span>Present</span>
                                    <span>2005</span>
                                </div>
                                <div class="content">
                                    <h4>General Atlantic Professor</h4>
                                    <p><em>Stanford University</em>, Graduate School of Business</p>
                                </div>
                            </li>
                            <li>
                                <div class="dates">
                                    <span>2005</span>
                                    <span>2004</span>
                                </div>
                                <div class="content">
                                    <h4>Full Professor</h4>
                                    <p><em>Stanford University</em>, Graduate School of Business</p>
                                </div>
                            </li>
                            <li>
                                <div class="dates">
                                    <span>2004</span>
                                    <span>2001</span>
                                </div>
                                <div class="content">
                                    <h4>Assistant Professor</h4>
                                    <p><em>UCLA</em>, Anderson Graduate School of Management</p>
                                </div>
                            </li>
                            <li>
                            <div class="dates">
                                <span>2001</span>
                                <span>1999</span>
                            </div>
                            <div class="content">
                                <h4>Visiting Assistant Professor</h4>
                                <p><em>Columbia University</em>, Graduate School of Business</p>
                            </div>
                            </li>
                        </ul> -->
                    </div>
                    <?php
                        }
                        if(count($education) > 0) {
                            if(count($academic) > 0) { ?>
                                <div class="col-md-5">
                        <?php } else { ?>
                                <div class="col-md-5 col-md-offset-1">
                        <?php } ?>
                        <div class="title text-center">
                            <h3><?php echo $page['about']['education_title']; ?></h3>
                        </div>
                        <?php
                            $educationHtml = '<ul class="ul-card">';
                            foreach ($education as $key => $value) {
                                $educationHtml.= '<li>
                                                    <div class="dy">
                                                        <span class="degree">'.$value['degree'].'</span>
                                                        <span class="year">'.$value['year'].'</span>
                                                    </div>
                                                    <div class="description">
                                                        <p class="waht">'.$value['title'].'</p>
                                                        <p class="where">'.$value['university'].'</p>
                                                    </div>
                                                </li>';
                            }
                            $educationHtml.= '</ul>';
                            echo $educationHtml;
                        ?>
                        <!-- <ul class="ul-card">
                            <li>
                                <div class="dy">
                                    <span class="degree">Ph.D.</span>
                                    <span class="year">1995</span>
                                </div>
                                <div class="description">
                                    <p class="waht">Ph.D. in Marketing</p>
                                    <p class="where">Stanford University Graduate School of Business</p>
                                </div>
                            </li>
                            <li>
                                <div class="dy">
                                    <span class="degree">M.B.A.</span><span class="year">1993</span>
                                </div>
                                <div class="description">
                                    <p class="waht">Master of Business Administration</p>
                                    <p class="where">Boston University</p>
                                </div>
                            </li>
                            <li>
                                <div class="dy">
                                    <span class="degree">B.A.</span><span class="year">1989</span>
                                </div>
                                <div class="description">
                                    <p class="waht">Bachelor of Arts in Psychology</p>
                                    <p class="where">University of California, Berkeley</p>
                                </div>
                            </li>
                        </ul> -->
                    </div>
                    <?php
                        }
                    ?>
                </div>    
            </div>
        </div>
        <?php
            }
            $honors = $page['about']['honors'];
            if(count($honors) > 0) {
        ?>
        <div class="section color-2">
            <div class="section-container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="title text-center">
                            <h3><?php echo $page['about']['honors_title']; ?></h3>
                        </div>
                        <ul class="timeline">
                            <?php
                                if(count($honors) > 0) {
                                    foreach ($honors as $key => $value) {
                                        echo '<li '.(($key==0)?'class="open"':'').'>
                                                <div class="date">'.$value['year'].'</div>
                                                <div class="circle"></div>
                                                <div class="data">
                                                    <div class="subject">'.$value['title'].'</div>
                                                    <div class="text row">
                                                        <div class="col-md-2">
                                                            <img alt="image" class="thumbnail img-responsive" src="'.$value['image'].'" >
                                                        </div>
                                                        <div class="col-md-10">'.$value['desc'].'</div>
                                                    </div>
                                                </div>
                                            </li>';
                                    }
                                }
                            ?>
                            <!-- <li class="open">
                                <div class="date">SCP 2014</div>
                                <div class="circle"></div>
                                <div class="data">
                                    <div class="subject">Distinguished Scientific Achievement Award</div>
                                    <div class="text row">
                                        <div class="col-md-2">
                                            <img alt="image" class="thumbnail img-responsive" src="<?php echo $portfolio_link; ?>img/Nobel_Prize.png" >
                                        </div>
                                        <div class="col-md-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed egestas dapibus lectus non dignissim. Pellentesque auctor ornare urna, volutpat condimentum quam porttitor at. Vestibulum tincidunt diam in eros aliquam luctus. Donec sagittis a purus a porttitor. Sed non feugiat enim. Donec eget metus erat. Vivamus sed consequat orci. Aenean commodo lectus sed purus auctor ullamcorper.</div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="date">2012-2013</div>
                                <div class="circle"></div>
                                <div class="data">
                                    <div class="subject">Ormond Family Faculty Fellow</div>
                                    <div class="text row">
                                        <div class="col-md-2">
                                            <img alt="image" class="thumbnail img-responsive" src="<?php echo $portfolio_link; ?>img/Nobel_Prize.png" >
                                        </div>
                                        <div class="col-md-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed egestas dapibus lectus non dignissim. Pellentesque auctor ornare urna, volutpat condimentum quam porttitor at. Vestibulum tincidunt diam in eros aliquam luctus. Donec sagittis a purus a porttitor. Sed non feugiat enim. Donec eget metus erat. Vivamus sed consequat orci. Aenean commodo lectus sed purus auctor ullamcorper.</div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="date">June 2011</div>
                                <div class="circle"></div>
                                <div class="data">
                                    <div class="subject">Nautilus Silver Award for Dragonfly Effect</div>
                                    <div class="text row">
                                        <div class="col-md-2">
                                            <img alt="image" class="thumbnail img-responsive" src="<?php echo $portfolio_link; ?>img/Nobel_Prize.png" >
                                        </div>
                                        <div class="col-md-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed egestas dapibus lectus non dignissim. Pellentesque auctor ornare urna, volutpat condimentum quam porttitor at. Vestibulum tincidunt diam in eros aliquam luctus. Donec sagittis a purus a porttitor. Sed non feugiat enim. Donec eget metus erat. Vivamus sed consequat orci. Aenean commodo lectus sed purus auctor ullamcorper.</div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="date">2000 - 2003</div>
                                <div class="circle"></div>
                                <div class="data">
                                    <div class="subject">Hong Kong Science International Research Grant</div>
                                    <div class="text row">
                                        <div class="col-md-2">
                                            <img alt="image" class="thumbnail img-responsive" src="<?php echo $portfolio_link; ?>img/Nobel_Prize.png" >
                                        </div>
                                        <div class="col-md-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed egestas dapibus lectus non dignissim. Pellentesque auctor ornare urna, volutpat condimentum quam porttitor at. Vestibulum tincidunt diam in eros aliquam luctus. Donec sagittis a purus a porttitor. Sed non feugiat enim. Donec eget metus erat. Vivamus sed consequat orci. Aenean commodo lectus sed purus auctor ullamcorper.</div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="date">1999</div>
                                <div class="circle"></div>
                                <div class="data">
                                    <div class="subject">Citibank Best Teacher Award (school-wide award, UCLA)</div>
                                    <div class="text row">
                                        <div class="col-md-2">
                                            <img alt="image" class="thumbnail img-responsive" src="<?php echo $portfolio_link; ?>img/Nobel_Prize.png" >
                                        </div>
                                        <div class="col-md-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed egestas dapibus lectus non dignissim. Pellentesque auctor ornare urna, volutpat condimentum quam porttitor at. Vestibulum tincidunt diam in eros aliquam luctus. Donec sagittis a purus a porttitor. Sed non feugiat enim. Donec eget metus erat. Vivamus sed consequat orci. Aenean commodo lectus sed purus auctor ullamcorper.</div>
                                    </div>
                                </div>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php
            }
        ?>                     
    </div>
</div>