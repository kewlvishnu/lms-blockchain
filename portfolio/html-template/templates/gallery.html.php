<div id="<?php echo 'slug'.($key+1); ?>" class="page <?php echo (($key==0)?'home':''); ?>"  <?php echo (($key==0)?'data-pos="home"':''); ?>>
    <div class="pagecontents">
        <div class="section color-3" id="gallery-header">
            <div class="section-container">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?php echo $page['pageTitle'] ?><?php
                            @session_start();
                            if(!(isset($_SESSION['userId']) && !empty($_SESSION['userId']))) {
                        ?><a href="#loginModal" class="btn btn-primary pull-right" data-toggle="modal">Login</a><?php
                            } else {
                                if($_SESSION['userRole'] == 4) {
                                ?><a href="<?php echo $actual_link; ?>/student/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 1){
                                ?><a href="<?php echo $actual_link; ?>/manage/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 2){
                                ?><a href="<?php echo $actual_link; ?>/manage/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 3){
                                ?><a href="<?php echo $actual_link; ?>/admin/dashboard.php" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 5){
                                ?><a href="<?php echo $actual_link; ?>/admin/approvals.php" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                }
                            }
                        ?></h2>
                        <br />
                        <?php echo $page['gallery']['description'] ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="section color-3" id="gallery-large">
            <div class="section-container">
                <ul id="grid" class="grid">
                    <?php
                        $galleryImages = $page['gallery']['galleryImages'];
                        if(count($galleryImages) > 0) {
                            foreach ($galleryImages as $key => $value) {
                                echo '<li>
                                        <div>
                                            <img alt="image" src="'.$value['image'].'">
                                            <a href="'.$value['image'].'"  class="popup-with-move-anim">
                                                <div class="over">
                                                    <div class="comein">
                                                        <h3>'.$value['title'].'</h3>
                                                        <p>'.$value['description'].'</p>
                                                        <div class="comein-bg"></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </li>';
                            }
                        }
                    /* <li>
                        <div>
                            <img alt="image" src="<?php echo $portfolio_link; ?>img/gallery/06.jpg">
                            <a href="<?php echo $portfolio_link; ?>img/gallery/06.jpg" class="popup-with-move-anim">
                                <div class="over"></div>
                            </a>
                            <div>cnkol</div>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="image" src="<?php echo $portfolio_link; ?>img/gallery/02.jpg">
                            <a href="<?php echo $portfolio_link; ?>img/gallery/02.jpg" class="popup-with-move-anim">
                                <div class="over">
                                    <div class="comein">
                                        <h3>Image Title</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        <div class="comein-bg"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="image" src="<?php echo $portfolio_link; ?>img/gallery/03.jpg">
                            <a href="<?php echo $portfolio_link; ?>img/gallery/03.jpg" class="popup-with-move-anim">
                                <div class="over">
                                    <div class="comein">
                                        <i class="icon-search"></i>
                                        <div class="comein-bg"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="image" src="<?php echo $portfolio_link; ?>img/gallery/04.jpg">
                            <a href="<?php echo $portfolio_link; ?>img/gallery/04.jpg" class="popup-with-move-anim"> 
                                <div class="over">
                                    <div class="comein">
                                        <h3>Image Title</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        <div class="comein-bg"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li>
                    <div>
                        <img alt="image" src="<?php echo $portfolio_link; ?>img/gallery/05.jpg">
                        <a href="<?php echo $portfolio_link; ?>img/gallery/05.jpg" class="popup-with-move-anim">
                            <div class="over">
                                <div class="comein">
                                    <i class="icon-search"></i>
                                    <div class="comein-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    </li>
                    <li>
                        <div>
                            <img alt="image" src="<?php echo $portfolio_link; ?>img/gallery/01.jpg">
                            <a href="<?php echo $portfolio_link; ?>img/gallery/01.jpg" class="popup-with-move-anim">
                                <div class="over">
                                    <div class="comein">
                                        <h3>Image Title</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        <div class="comein-bg"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="image" src="<?php echo $portfolio_link; ?>img/gallery/07.jpg">
                            <a href="<?php echo $portfolio_link; ?>img/gallery/07.jpg" class="popup-with-move-anim">
                                <div class="over">
                                    <div class="comein">
                                        <i class="icon-search"></i>
                                        <div class="comein-bg"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="image" src="<?php echo $portfolio_link; ?>img/gallery/08.jpg">
                            <a href="<?php echo $portfolio_link; ?>img/gallery/08.jpg" class="popup-with-move-anim">
                                <div class="over">
                                    <div class="comein">
                                        <i class="icon-search"></i>
                                        <div class="comein-bg"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="image" src="<?php echo $portfolio_link; ?>img/gallery/09.jpg">
                            <a href="<?php echo $portfolio_link; ?>img/gallery/09.jpg" class="popup-with-move-anim">
                                <div class="over">
                                    <div class="comein">
                                        <h3>Image Title</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <div class="comein-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    </li>
                    <li>
                        <div>
                            <img alt="image" src="<?php echo $portfolio_link; ?>img/gallery/10.jpg">
                            <a href="<?php echo $portfolio_link; ?>img/gallery/10.jpg" class="popup-with-move-anim">
                                <div class="over">
                                    <div class="comein">
                                        <i class="icon-search"></i>
                                        <div class="comein-bg"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="image" src="<?php echo $portfolio_link; ?>img/gallery/11.jpg">
                            <a href="<?php echo $portfolio_link; ?>img/gallery/11.jpg" class="popup-with-move-anim"> 
                                <div class="over">
                                    <div class="comein">
                                        <h3>Image Title</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <div class="comein-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    </li>
                    <li>
                        <div>
                            <img alt="image" src="<?php echo $portfolio_link; ?>img/gallery/12.jpg">
                            <a href="<?php echo $portfolio_link; ?>img/gallery/12.jpg" class="popup-with-move-anim">
                                <div class="over">
                                    <div class="comein">
                                        <i class="icon-search"></i>
                                        <div class="comein-bg"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div>
                            <img alt="image" src="<?php echo $portfolio_link; ?>img/gallery/07.jpg">
                            <a href="<?php echo $portfolio_link; ?>img/gallery/07.jpg" class="popup-with-move-anim">
                                <div class="over">
                                    <div class="comein">
                                        <h3>Image Title</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <div class="comein-bg"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    </li>
                    <li>
                        <div>
                            <img alt="image" src="<?php echo $portfolio_link; ?>img/gallery/02.jpg">
                            <a href="<?php echo $portfolio_link; ?>img/gallery/02.jpg" class="popup-with-move-anim">
                                <div class="over">
                                    <div class="comein">
                                        <i class="icon-search"></i>
                                        <div class="comein-bg"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </li> */
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>