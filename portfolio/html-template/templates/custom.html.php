<div id="<?php echo 'slug'.($key+1); ?>" class="page <?php echo (($key==0)?'home':''); ?>"  <?php echo (($key==0)?'data-pos="home"':''); ?>>
    <div class="pageheader">
        <div class="headercontent">
            <div class="section-container">
                <div class="row">
                    <div class="col-sm-2 visible-sm"></div>
                    <div class="clearfix visible-sm visible-xs"></div>
                    <div class="col-sm-12">
                        <h3 class="title"><?php echo $page['pageTitle'] ?><?php
                            @session_start();
                            if(!(isset($_SESSION['userId']) && !empty($_SESSION['userId']))) {
                        ?><a href="#loginModal" class="btn btn-primary pull-right" data-toggle="modal">Login</a><?php
                            } else {
                                if($_SESSION['userRole'] == 4) {
                                ?><a href="<?php echo $actual_link; ?>/student/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 1){
                                ?><a href="<?php echo $actual_link; ?>/manage/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 2){
                                ?><a href="<?php echo $actual_link; ?>/manage/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 3){
                                ?><a href="<?php echo $actual_link; ?>/admin/dashboard.php" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 5){
                                ?><a href="<?php echo $actual_link; ?>/admin/approvals.php" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                }
                            }
                        ?></h3>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pagecontents">
        <div class="section color-1">
            <div class="section-container">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $page['content']; ?>
                    </div>  
                </div>    
            </div>
        </div>                      
    </div>
</div>