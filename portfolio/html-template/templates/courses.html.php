<div id="<?php echo 'slug'.($key+1); ?>" class="page <?php echo (($key==0)?'home':''); ?>"  <?php echo (($key==0)?'data-pos="home"':''); ?>>
    <div class="pageheader">
        <div class="headercontent">
            <div class="section-container">
                <div class="row">
                    <div class="col-sm-2 visible-sm"></div>
                    <div class="title text-center">
                        <h2><?php echo $page['pageTitle'] ?><?php
                            @session_start();
                            if(!(isset($_SESSION['userId']) && !empty($_SESSION['userId']))) {
                        ?><a href="#loginModal" class="btn btn-primary pull-right" data-toggle="modal">Login</a><?php
                            } else {
                                if($_SESSION['userRole'] == 4) {
                                ?><a href="<?php echo $actual_link; ?>/student/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 1){
                                ?><a href="<?php echo $actual_link; ?>/manage/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 2){
                                ?><a href="<?php echo $actual_link; ?>/manage/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 3){
                                ?><a href="<?php echo $actual_link; ?>/admin/dashboard.php" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 5){
                                ?><a href="<?php echo $actual_link; ?>/admin/approvals.php" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                }
                            }
                        ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pagecontents">
        <div class="section color-2">
            <div class="section-container">
                <div class="row course-img-gallery">
                    <?php
                        //var_dump($page);
                        $courses  = $page['courses'];
                        if(count($courses)>0) {
                            foreach ($courses as $key => $course) {
                                $displayCurrency = $global->displayCurrency;
                                if($course["studentPrice"] == '0' || $course["studentPrice"] == '0.00') {
                                    $displayCurrency = '';
                                    $course["studentPrice"] = 'Free';
                                }
                                $rating = (int)$course["rating"]->rating;
                                $displayRating = "";
                                for ($i=0; $i < 5; $i++) {
                                    if($i<$rating) {
                                        $displayRating.= '<span class="glyphicon glyphicon-star"></span>';
                                    } else {
                                        $displayRating.= '<span class="glyphicon glyphicon-star-empty"></span>';
                                    }
                                }
                                echo '<div class="col-sm-4 col-lg-4 col-md-4">
                                        <div class="thumbnail course-block">
                                            <a href="'.$actual_link.'/course/'.$course["slug"].'" class="js-pf-links"><img src="'.$course["image"].'" alt=""></a>
                                            <div class="caption">
                                                <h4 class="pull-right">'.$displayCurrency.''.$course["studentPrice"].'</h4>
                                                <h4><a href="'.$actual_link.'/course/'.$course["slug"].'" class="course-title js-pf-links">'.$course["name"].'</a></h4>
                                                <p>'.$course["subtitle"].'</p>
                                            </div>
                                            <div class="ratings">
                                                <p class="pull-right">'.$course["studentCount"].' Student Enrolled </p>
                                                <p>'.$displayRating.'</p>
                                            </div>
                                            <div class="sale-box"><span class="on_sale title_shop">New</span></div>
                                        </div>
                                    </div>';
                            }
                        } else {
                            echo '<div class="col-md-12">Login to view your enrolled courses</div>';
                        }
                    ?>
                    <!-- <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="<?php echo $portfolio_link; ?>img/C.png" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$24.99</h4>
                                <h4><a href="#">C programming</a></h4>
                                <p>Become a Super Successful C programmer in Easy Steps with Example Programs</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">15 Student Enrolled </p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </p>
                            </div>
                        </div>
                        <div class="sale-box"><span class="on_sale title_shop">New</span></div>    
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="sale-box1"><span class="on_sale1 title_shop">50% OFF</span></div>    
                        <div class="thumbnail">
                            <img src="<?php echo $portfolio_link; ?>img/hibernate.png" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$64.99</h4>
                                <h4><a href="#">ORM Framework</a></h4>
                                <p> Learn the hottest, most in-demand Java ORM framework Hibernate 4 with easy-to-understand course.</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">12 Student Enrolled</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="<?php echo $portfolio_link; ?>img/oops.jpg" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$74.99</h4>
                                <h4><a href="#">Third Product</a></h4>
                                <p>Learn the hottest, most in-demand Java ORM framework Hibernate 4 with  easy-to-understand course.</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">31 Student Enrolled</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </p>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>      
    </div>
</div>