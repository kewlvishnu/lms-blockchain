<div id="<?php echo 'slug'.($key+1); ?>" class="page <?php echo (($key==0)?'home':''); ?>"  <?php echo (($key==0)?'data-pos="home"':''); ?>>
    <div class="pageheader">
        <div class="headercontent">
            <div class="section-container">
                <h2 class="title"><?php echo $page['pageTitle'] ?><?php
                            @session_start();
                            if(!(isset($_SESSION['userId']) && !empty($_SESSION['userId']))) {
                        ?><a href="#loginModal" class="btn btn-primary pull-right" data-toggle="modal">Login</a><?php
                            } else {
                                if($_SESSION['userRole'] == 4) {
                                ?><a href="<?php echo $actual_link; ?>/student/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 1){
                                ?><a href="<?php echo $actual_link; ?>/manage/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 2){
                                ?><a href="<?php echo $actual_link; ?>/manage/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 3){
                                ?><a href="<?php echo $actual_link; ?>/admin/dashboard.php" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 5){
                                ?><a href="<?php echo $actual_link; ?>/admin/approvals.php" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                }
                            }
                        ?></h2>
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $page['faculty']['description'] ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pagecontents">
        <div class="section color-1">
            <div class="section-container">
                <div class="row">
                    <?php
                            $facultyMembers     = $page['faculty']['facultyMembers'];
                            $noOfFacultyMembers = count($facultyMembers);
                            if($noOfFacultyMembers > 0) {
                                $limit = $noOfFacultyMembers;
                                $limit = $limit/2;
                                $limit = (int)$limit;
                                if($noOfFacultyMembers%2 == 0) {
                                    $limit--;
                                }
                                echo '
                                    <div class="col-md-5  col-md-offset-1">
                                        <ul class="ul-dates">';
                                foreach ($facultyMembers as $key => $value) {
                                    echo '<li>
                                            <div class="dates">
                                                <span><img alt="image" src="'.$value["image"].'"  class="img-responsive"></span>
                                            </div>
                                            <div class="content">
                                                <a href="javascript:void(0)">  <h4>'.$value["name"].'</h4></a>
                                                <p><em>'.$value["university"].'</em>, '.$value["department"].'</p>
                                            </div>
                                        </li>';
                                    if($key == $limit) {
                                        echo '</ul>
                                        </div>
                                        <div class="col-md-5  col-md-offset-1">
                                            <ul class="ul-dates">';
                                    }
                                }
                                echo '
                                    </ul>
                                </div>';
                            }
                    ?>
                    <!-- <div class="col-md-5  col-md-offset-1">
                        <ul class="ul-dates">
                            <li>
                                <div class="dates">
                                    <span><img alt="image" src="<?php echo $portfolio_link; ?>img/personal/06_1.jpg"  class="img-responsive"></span>
                                </div>
                                <div class="content">
                                    <a href="#">  <h4>Doe</h4></a>
                                    <p><em>Stanford University</em>, Graduate School of Business</p>
                                </div>
                            </li>
                            <li>
                                <div class="dates">
                                    <span><img alt="image" src="<?php echo $portfolio_link; ?>img/personal/n.jpg"  class="img-responsive"></span>
                                </div>
                                <div class="content">
                                    <a href="#"><h4>Doe</h4></a>
                                    <p><em>Stanford University</em>, Graduate School of Business</p>
                                </div>
                            </li>
                            <li>
                                <div class="dates">
                                    <span><img alt="image" src="<?php echo $portfolio_link; ?>img/personal/06_1.jpg"  class="img-responsive"></span>
                                </div>
                                <div class="content">
                                    <a href="#">  <h4>Doe</h4></a>
                                    <p><em>Stanford University</em>, Graduate School of Business</p>
                                </div>
                            </li>
                            <li>
                                <div class="dates">
                                    <span><img alt="image" src="<?php echo $portfolio_link; ?>img/personal/n.jpg"  class="img-responsive"></span>
                                </div>
                                <div class="content">
                                    <a href="#">  <h4>Doe</h4></a>
                                    <p><em>Stanford University</em>, Graduate School of Business</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-5  col-md-offset-1">
                        <ul class="ul-dates">
                            <li>
                                <div class="dates">
                                    <span><img alt="image" src="<?php echo $portfolio_link; ?>img/personal/06_1.jpg"  class="img-responsive"></span>
                                </div>
                                <div class="content">
                                    <a href="#">  <h4>Doe</h4></a>
                                    <p><em>Stanford University</em>, Graduate School of Business</p>
                                </div>
                            </li>
                            <li>
                                <div class="dates">
                                    <span><img alt="image" src="<?php echo $portfolio_link; ?>img/personal/n.jpg"  class="img-responsive"></span>
                                </div>
                                <div class="content">
                                    <a href="#">  <h4>Doe</h4></a>
                                    <p><em>Stanford University</em>, Graduate School of Business</p>
                                </div>
                            </li>
                            <li>
                                <div class="dates">
                                    <span><img alt="image" src="<?php echo $portfolio_link; ?>img/personal/06_1.jpg"  class="img-responsive"></span>
                                </div>
                                <div class="content">
                                    <a href="#">  <h4>Doe</h4></a>
                                    <p><em>Stanford University</em>, Graduate School of Business</p>
                                </div>
                            </li>
                            <li>
                                <div class="dates">
                                    <span><img alt="image" src="<?php echo $portfolio_link; ?>img/personal/n.jpg"  class="img-responsive"></span>
                                </div>
                                <div class="content">
                                    <a href="#">  <h4>Doe</h4></a>
                                    <p><em>Stanford University</em>, Graduate School of Business</p>
                                </div>
                            </li>
                        </ul>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>