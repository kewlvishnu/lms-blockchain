<div id="<?php echo 'slug'.($key+1); ?>" class="page stellar">
    <div class="pageheader">
        <div class="headercontent">
            <div class="section-container">
                <h2 class="title"><?php echo $page['pageTitle'] ?><?php
                            @session_start();
                            if(!(isset($_SESSION['userId']) && !empty($_SESSION['userId']))) {
                        ?><a href="#loginModal" class="btn btn-primary pull-right" data-toggle="modal">Login</a><?php
                            } else {
                                if($_SESSION['userRole'] == 4) {
                                ?><a href="<?php echo $actual_link; ?>/student/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 1){
                                ?><a href="<?php echo $actual_link; ?>/manage/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 2){
                                ?><a href="<?php echo $actual_link; ?>/manage/" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 3){
                                ?><a href="<?php echo $actual_link; ?>/admin/dashboard.php" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                } else if($_SESSION['userRole'] == 5){
                                ?><a href="<?php echo $actual_link; ?>/admin/approvals.php" class="btn btn-primary pull-right js-pf-links">Dashboard</a><?php
                                }
                            }
                        ?></h2>
                <div class="row">
                    <div class="col-md-8">
                        <p><?php echo $page['contact']['contact_description'] ?></p>
                    </div>
                    <div class="col-md-4">
                        <?php
                            $contact = $page['contact']['contactInfo'];
                            if(count($contact) > 0) {
                                $contactHtml = '<ul class="list-unstyled">';
                                foreach ($contact as $key => $value) {
                                    switch ($value['type']) {
                                        case 'phone':
                                            $contactHtml.= '<li>
                                                                <strong><i class="glyphicon glyphicon-phone"></i>&nbsp;&nbsp;</strong>
                                                                <span>'.$value['title'].': '.$value['value'].'</span>
                                                            </li>';
                                            break;
                                        case 'email':
                                            $contactHtml.= '<li>
                                                                <strong><i class="glyphicon glyphicon-envelope"></i>&nbsp;&nbsp;</strong>
                                                                <span>'.$value['title'].': '.$value['value'].'</span>
                                                            </li>';
                                            break;
                                        case 'skype':
                                            $contactHtml.= '<li>
                                                                <strong><i class="icon-skype"></i>&nbsp;&nbsp;</strong>
                                                                <span>'.$value['title'].': '.$value['value'].'</span>
                                                            </li>';
                                            break;
                                        case 'facebook':
                                            $dispValue = explode("/", $value['value']);
                                            $dispValue = end($dispValue);
                                            $contactHtml.= '<li>
                                                                <strong><i class="icon-facebook"></i>&nbsp;&nbsp;</strong>
                                                                <span>'.$value['title'].': <a href="'.$value['value'].'">'.$dispValue.'</a></span>
                                                            </li>';
                                            break;
                                        case 'twitter':
                                            $dispValue = explode("/", $value['value']);
                                            $dispValue = end($dispValue);
                                            $contactHtml.= '<li>
                                                                <strong><i class="icon-twitter"></i>&nbsp;&nbsp;</strong>
                                                                <span>'.$value['title'].': <a href="'.$value['value'].'">'.$dispValue.'</a></span>
                                                            </li>';
                                            break;
                                        case 'linkedin':
                                            $dispValue = explode("/", $value['value']);
                                            $dispValue = end($dispValue);
                                            $contactHtml.= '<li>
                                                                <strong><i class="icon-linkedin"></i>&nbsp;&nbsp;</strong>
                                                                <span>'.$value['title'].': <a href="'.$value['value'].'">'.$dispValue.'</a></span>
                                                            </li>';
                                            break;
                                        
                                        default:
                                            $contactHtml.= '<li>
                                                                <span>'.$value['title'].': '.$value['value'].'</span>
                                                            </li>';
                                            break;
                                    }
                                }
                                $contactHtml.= '</ul>';
                            }
                            echo $contactHtml;
                            /*<ul class="list-unstyled">

                                <li>
                                    <strong><i class="glyphicon glyphicon-phone"></i>&nbsp;&nbsp;</strong>
                                    <span>office: 808-808 88 88</span>
                                </li>
                                <li>
                                    <strong><i class="glyphicon glyphicon-phone"></i>&nbsp;&nbsp;</strong>
                                    <span>lab: 808-808 88 88</span>
                                </li>
                                <li>
                                    <strong><i class="glyphicon glyphicon-envelope"></i>&nbsp;&nbsp;</strong>
                                    <span>jdoe@stanford.edu</span>
                                </li>
                                <li>
                                    <strong><i class="glyphicon glyphicon-envelope"></i>&nbsp;&nbsp;</strong>
                                    <span>jdoe@gmail.com</span>
                                </li>
                                <li>
                                    <strong><i class="glyphicon icon-skype"></i>&nbsp;&nbsp;</strong>
                                    <span>jenniferDoe</span>
                                </li>
                                <li>
                                    <strong><i class="glyphicon glyphicon-twitter"></i>&nbsp;&nbsp;</strong>
                                    <span>#jenniferDoe</span>
                                </li>
                                <li>
                                    <strong><i class="icon-linkedin-sign"></i>&nbsp;&nbsp;</strong>
                                    <span><a href="#">us.linkedin.com/in/jdoe</a></span>
                                </li>
                            </ul>*/
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pagecontents">
        <div class="section contact-office" data-stellar-background-ratio="0.1">
            <div class="section-container">
                <div class="row">
                    <div class="col-md-8">
                        <h2 class="title">At My Office</h2>
                        <p><?php echo $page['contact']['office_description'] ?></p>
                    </div>
                    <div id="map-container" class="col-md-8"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="section color-1">
        <div class="section-container">
            <div class="row">
                <div class="col-md-4 text-center hidden-xs hidden-sm">
                    <i class="icon-stethoscope icon-huge"></i>
                </div>
                <div class="col-md-8">
                    <h2 class="title">At My Work</h2>
                    <p><?php echo $page['contact']['work_description'] ?></p>
                </div>
            </div>
        </div>
    </div>
</div>