<div id="<?php echo 'slug'.($key+1); ?>" class="page <?php echo (($key==0)?'home':''); ?>"  <?php echo (($key==0)?'data-pos="home"':''); ?>>
    <div class="pageheader">
        <div class="headercontent">
            <div class="section-container">
                <h2 class="title"><?php echo $page['pageTitle'] ?></h2>
                <div class="row">
                    <div class="col-md-12">
                        <p><?php echo $page['teaching']['description']; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pagecontents">
        <div class="section color-1">
            <div class="section-container">
                <div class="row">
                    <div class="title text-center">
                        <h3>Current Teaching</h3>
                    </div>
                    <?php
                        $noOfCurrent = $page['teaching']['noOfCurrent'];
                        if($noOfCurrent > 0) {
                            $teaching = $page['teaching']['teaching'];
                            $teachingHtml = '<ul class="ul-dates">';
                            foreach ($teaching as $key => $value) {
                                if ($value['toyear'] == '0000') {
                                    $teachingHtml.= '<li>
                                                        <div class="dates">
                                                            <span>Present</span>
                                                            <span>'.$value['fromyear'].'</span>
                                                        </div>
                                                        <div class="content">
                                                            <h4>'.$value['title'].'</h4>
                                                            <p>'.$value['description'].'</p>
                                                        </div>
                                                    </li>';
                                }
                            }
                            $teachingHtml.= '</ul>';
                        } else {
                            $teachingHtml ='<p>No Current Teaching Information</p>';
                        }
                        echo $teachingHtml;
                    ?>
                    <!-- <ul class="ul-dates">
                        <li>
                            <div class="dates">
                                <span>Present</span>
                                <span>1995</span>
                            </div>
                            <div class="content">
                                <h4>Preclinical Endodnotics</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultrices ac elit sit amet porttitor. Suspendisse congue, erat vulputate pharetra mollis, est eros fermentum nibh, vitae rhoncus est arcu vitae elit.</p>
                            </div>
                        </li>
                        <li>
                            <div class="dates">
                                <span>Present</span>
                                <span>2003</span>
                            </div>
                            <div class="content">
                                <h4>SELC 8160 Molar Endodontic Selective</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultrices ac elit sit amet porttitor. Suspendisse congue, erat vulputate pharetra mollis, est eros fermentum nibh, vitae rhoncus est arcu vitae elit.</p>
                            </div>
                        </li>
                        <li>
                            <div class="dates">
                                <span>Present</span>
                                <span>2010</span>
                            </div>
                            <div class="content">
                                <h4>Endodontics Postdoctoral AEGD Program</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultrices ac elit sit amet porttitor. Suspendisse congue, erat vulputate pharetra mollis, est eros fermentum nibh, vitae rhoncus est arcu vitae elit.</p>
                            </div>
                        </li>
                    </ul> -->
                </div>
            </div>
        </div>
        <div class="section color-2">
            <div class="section-container">
                <div class="row">
                    <div class="title text-center">
                        <h3>Teaching History</h3>
                    </div>
                    <?php
                        $noOfPast = $page['teaching']['noOfPast'];
                        if ($noOfPast>0) {
                            $teaching = $page['teaching']['teaching'];
                            $teachingHtml = '<ul class="ul-dates">';
                            foreach ($teaching as $key => $value) {
                                if ($value['toyear'] != '0000') {
                                    $teachingHtml.= '<li>
                                                        <div class="dates">
                                                            <span>'.$value['toyear'].'</span>
                                                            <span>'.$value['fromyear'].'</span>
                                                        </div>
                                                        <div class="content">
                                                            <h4>'.$value['title'].'</h4>
                                                            <p>'.$value['description'].'</p>
                                                        </div>
                                                    </li>';
                                }
                            }
                            $teachingHtml.= '</ul>';
                        } else {
                            $teachingHtml ='<p>No Teaching History</p>';
                        }
                        echo $teachingHtml;
                    ?>
                    <!-- <ul class="ul-dates-gray">
                        <li>
                            <div class="dates">
                                <span>1997</span>
                                <span>1995</span>
                            </div>
                            <div class="content">
                                <h4>Preclinical Endodnotics</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultrices ac elit sit amet porttitor. Suspendisse congue, erat vulputate pharetra mollis, est eros fermentum nibh, vitae rhoncus est arcu vitae elit.</p>
                            </div>
                        </li>
                        <li>
                            <div class="dates">
                                <span>2005</span>
                                <span>2003</span>
                            </div>
                            <div class="content">
                                <h4>SELC 8160 Molar Endodontic Selective</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultrices ac elit sit amet porttitor. Suspendisse congue, erat vulputate pharetra mollis, est eros fermentum nibh, vitae rhoncus est arcu vitae elit.</p>
                            </div>
                        </li>
                        <li>
                            <div class="dates">
                                <span>2011</span>
                                <span>2010</span>
                            </div>
                            <div class="content">
                                <h4>Endodontics Postdoctoral AEGD Program</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultrices ac elit sit amet porttitor. Suspendisse congue, erat vulputate pharetra mollis, est eros fermentum nibh, vitae rhoncus est arcu vitae elit.</p>
                            </div>
                        </li>
                        <li>
                            <div class="dates">
                                <span>2011</span>
                                <span>2010</span>
                            </div>
                            <div class="content">
                                <h4>Endodontics Postdoctoral AEGD Program</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultrices ac elit sit amet porttitor. Suspendisse congue, erat vulputate pharetra mollis, est eros fermentum nibh, vitae rhoncus est arcu vitae elit.</p>
                            </div>
                        </li>
                        <li>
                            <div class="dates">
                                <span>2011</span>
                                <span>2010</span>
                            </div>
                            <div class="content">
                                <h4>Endodontics Postdoctoral AEGD Program</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultrices ac elit sit amet porttitor. Suspendisse congue, erat vulputate pharetra mollis, est eros fermentum nibh, vitae rhoncus est arcu vitae elit.</p>
                            </div>
                        </li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</div>