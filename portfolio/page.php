<?php
    //code to update any paused exam or assignment
    // Loading Zend
    $libPath = "api/Vendor";
    include $libPath . '/Zend/Loader/AutoloaderFactory.php';
    require $libPath . "/ArcaneMind/Api.php";
    Zend\Loader\AutoloaderFactory::factory(array(
            'Zend\Loader\StandardAutoloader' => array(
                    'autoregister_zf' => true,
                    'db' => 'Zend\Db\Sql'
            )
    ));
    require_once 'api/Vendor/ArcaneMind/User.php';
    
    $actual_link  = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $temp_link    = explode("/", $actual_link);
    $sub_link = explode(".", $temp_link[2]);
    $final_link = str_replace($sub_link[0], 'www', $actual_link);

    if(isset($_GET['pageDetailId']) && !empty($_GET['pageDetailId'])) {
        $slug = trim($_GET['pageDetailId']);
        $res = Api::getUserBySlug($slug);
        if($res->valid) {
            $logoPath = $res->profileDetails['profilePic'];
            if ($res->userRole == 1) {
                $portfolioName = $res->profileDetails['name'];
            } else {
                $portfolioName = $res->profileDetails['firstName'].' '.$res->profileDetails['lastName'];
            }
        } else {
            header('location:'.$final_link);
        }
    } else {
        header('location:'.$final_link);
    }
    include 'html-template/header.php';
?>
<div class="min-height padding_top_75">
    <div id="hide-container" style="display:none;">
        <div class="container"> 
            <div class="row self-hidden" style="display: none;">
                <!--feature start-->
                <div class="col-lg-12">
                    <h2 style="text-transform:none;">Featured Courses</h2>
                    <hr>
                </div>
                <!--feature end-->
            </div>
            <div class="row product-list courses-join-hide" id="sample-course"></div>
        </div>
    </div>
</div>
<?php include 'html-template/modal/verify.php'; ?>
<?php include 'html-template/modal/notify.php'; ?>
<?php include 'html-template/footer.php'; ?>
<input type="hidden" name="inputUserId" id="inputUserId" value="<?php echo $res->loginDetails['id']; ?>">
<script src="js/owl.carousel.min.js" type="text/javascript"></script>
<script src="assets/jquery-autocomplete/dist/jquery.autocomplete.min.js" type="text/javascript"></script>
<script src="scripts/accountVerify.js" type="text/javascript"></script>
<script src="scripts/pageCourses.js" type="text/javascript"></script>
<?php
    if ($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1'){ ?>
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/58f86bb530ab263079b60876/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
        <!--End of Tawk.to Script--><?php
    } ?>
</body>
</html>