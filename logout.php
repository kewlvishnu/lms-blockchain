<?php
	session_start();
	unset($_SESSION["userId"]);
	unset($_SESSION["userRole"]);
	unset($_SESSION['username']);
	session_destroy();
    setcookie('mtwebLogin', "", time() - 3600, '/');
    unset($_COOKIE['mtwebLogin']);
	header("Location:http://$_SERVER[HTTP_HOST]");
?>
Please wait.. Clearing your data.