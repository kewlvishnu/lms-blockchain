<!DOCTYPE HTML>
<html>
	<head>
		<title>Content Market Place</title>
		<?php include 'html-template/header.php'; ?>
	</head>
	<body>
		<div class="container">
			<div class="row  margin_top_40">
				<div class="row product-list" id="license-course">
				</div>
			</div>
		</div>
		<?php include 'html-template/modal/verify.php'; ?>
		<?php include 'html-template/modal/notify.php'; ?>
		<?php include 'html-template/footer.php'; ?>
		<script src="scripts/accountVerify.js" type="text/javascript"></script>
		<script src="scripts/contentMarket.js" type="text/javascript"></script>
	</body>
</html>