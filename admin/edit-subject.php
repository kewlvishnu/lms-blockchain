<?php
    require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
    $courseId = $_GET["courseId"];
    $subjectId = $_GET["subjectId"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Subject</title>
    <?php include "html-template/general/head-tags.html.php"; ?>
    <link rel="stylesheet" href="assets/bootstrap-rating/bootstrap-rating.css">
</head>
<body>
    <section id="container" class="">
      <?php include "html-template/general/header.html.php"; ?>
      <?php include "html-template/general/sidebar.html.php"; ?>
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                  <div class="col-md-12">                 
                      <?php include "html-template/edit-subject/edit-subject-form.html.php"; ?>
                  </div>
                  <!--<div class="col-md-3">
                      <?php include "html-template/general/updates-feed.html.php"; ?>
                  </div>-->
              </div>
              <!-- page end-->
          </section>
      </section>
	  <?php include 'html-template/edit-subject/image-upload-modals.html.php'; ?>
      <!--main content end-->
      <?php include "html-template/general/footer.html.php"; ?>
      <?php include "html-template/pageCheckFooter.php"; ?>
  </section>
    <!-- js placed at the end of the document so the pages load faster -->
    <?php include "html-template/general/footer-scripts.html.php"; ?>
	<script src="assets/jquery-multi-select/js/jquery.multi-select.js"></script>
  <script src="assets/bootstrap-rating/bootstrap-rating.min.js" type="text/javascript"></script>
  <script src="js/custom/edit-subject.js"></script>
	<?php include "html-template/edit-subject/edit-subject-modal.html.php"; ?>
	<?php include "html-template/edit-subject/exam-modal.html.php"; ?>
	<?php include "html-template/edit-subject/subjective-exam-modal.html.php"; ?>
	<?php include "html-template/edit-subject/syllabus-modal.html.php"; ?>
	<?php include 'html-template/edit-subject/chapter-click-modal.html.php'; ?>
  <?php include "html-template/edit-subject/copy-exam-modal.html.php"; ?>
</body>
</html>
