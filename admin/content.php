<?php
    require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Content</title>
	<?php include "html-template/general/head-tags.html.php"; ?>
	<link href="assets/bootstrap-switch/css/bootstrap3/bootstrap-switch.css" rel="stylesheet">
	<link href="assets/jquery-toggles/css/toggles.css" rel="stylesheet">
</head>
<body>
	<section id="container" class="">
        <?php include "html-template/general/header.html.php"; ?>
		<?php include "html-template/general/sidebar.html.php"; ?>
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper site-min-height">
				<!-- page start-->
                <div class="row">
                    <div class="col-md-12">
                        <?php include "html-template/content/content.html.php"; ?>
                    </div>
                    <!--<div class="col-md-3">
                        <?php include "html-template/general/updates-feed.html.php"; ?>
                    </div>-->
                </div>
                <!-- page end-->
            </section>
        </section>
        <!--main content end-->
        <?php include "html-template/general/footer.html.php"; ?>
        <?php include "html-template/pageCheckFooter.php"; ?>
    </section>
    <!-- js placed at the end of the document so the pages load faster -->
    <?php include "html-template/general/footer-scripts.html.php"; ?>
    <script src="assets/ckeditor/ckeditor.js"></script>
	<script src="assets/bootstrap-switch/js/bootstrap-switch.js"></script>
	<script src="assets/jquery-toggles/toggles.min.js"></script>
    <script src="js/custom/content.js"></script>
	
    <script>
        jQuery(document).ready(function () {
            // DraggablePortlet.init();
			$("input[name='my-checkbox']").bootstrapSwitch();
        });
  </script>
	<?php include "html-template/content/content-modal.html.php"; ?>
      
</body>
</html>