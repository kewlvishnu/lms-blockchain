<?php
    require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Edit Course</title>
        <?php include "html-template/general/head-tags.html.php"; ?>
        <link rel="stylesheet" href="assets/advanced-datatable/media/css/demo_page.css" />
        <link rel="stylesheet" href="assets/advanced-datatable/media/css/demo_table.css" />
        <link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
        <link rel="stylesheet" href="assets/datetimepicker/jquery.datetimepicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-select/bootstrap-select.css" />
        <link rel="stylesheet" href="assets/bootstrap-rating/bootstrap-rating.css">
        <link rel="stylesheet" href="flowplayer/skin/minimalist.css">
    </head>
    <body>
        <section id="container" class="">
            <?php include "html-template/general/header.html.php"; ?>
            <?php include "html-template/general/sidebar.html.php"; ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper site-min-height">
                    <!-- page start-->
                    <div class="row">
                        <div class="col-md-12">
                            <?php include "html-template/edit-course/edit-course-form.html.php"; ?>
                        </div>
                    </div>
                    <!-- page end-->
                </section>
            </section>
            <?php include 'html-template/edit-course/image-upload-modals.html.php'; ?>
			<?php include 'html-template/edit-course/video-upload-modal.html.php'; ?>
			<?php include "html-template/edit-course/coursedoc-modal.html.php"; ?>
            <!--main content end-->
            <?php include "html-template/general/footer.html.php"; ?>
            <?php include "html-template/pageCheckFooter.php"; ?>
        </section>
        <!-- js placed at the end of the document so the pages load faster -->
        <?php include "html-template/general/footer-scripts.html.php"; ?>
        <script src="assets/datetimepicker/jquery.datetimepicker.js"></script>
        <script type="text/javascript" src="assets/bootstrap-select/bootstrap-select.js"></script>
        <script src="assets/bootstrap-rating/bootstrap-rating.min.js" type="text/javascript"></script>
        <script src="flowplayer/flowplayer.min.js"></script>
        <script src="js/custom/edit-course.js"></script>
        <script src="js/custom/license.js"></script>
        <script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
        <script>
            $(function () {
               // $("#edit-form #course-cat").selectpicker({});
                 $("#edit-form #course-cat").multiselect();
            });
        </script>
        <?php include "html-template/edit-course/edit-course-modal.html.php"; ?>
        <?php include "html-template/edit-course/invite_student_modal.html.php"; ?>
        <?php include "html-template/course-key/purchaseNowModal.html.php"; ?>
        <?php include "html-template/course-key/contact.modal.html.php"; ?>
    </body>
</html>
