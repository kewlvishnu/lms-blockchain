<?php
    require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>View Course Analytics</title>
    <?php include "html-template/general/head-tags.html.php"; ?>
	<link href="assets/morris.js-0.4.3/morris.css" rel="stylesheet" />
	<link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
</head>
<body>
  <section class="" id="container">
		<?php include "html-template/general/header.html.php"; ?>
		<?php include "html-template/general/sidebar.html.php"; ?>
		<!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
          <div class="col-md-12">
					  <?php include "html-template/edit-subject/subject-students.html.php"; ?>
    			</div>
        </div>
        <!-- page end-->
      </section>
    </section>
    <!--main content end-->
    <!--footer start-->
    <?php include "html-template/general/footer.html.php"; ?>
    <?php include "html-template/pageCheckFooter.php"; ?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <?php include "html-template/general/footer-scripts.html.php"; ?>
	
	<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
	<script src="js/custom/subject-students.js"></script>
</body></html>