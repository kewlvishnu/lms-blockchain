<?php
function upload_file($field){  // the $field is the field name in the uploading form
    $target_dir = "upload/";    
    $uploadOk = 1;
    // Check if image file is a actual image or fake image
    $check = getimagesize($_FILES[$field]["tmp_name"]);
    $target_file = $target_dir . "$field." . substr($check["mime"], strpos($check["mime"], '/') + 1);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".\n";
        $uploadOk = 1;
    } else {
        echo "File is not an image.\n";
        $uploadOk = 0;
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.\n";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES[$field]["size"] > 500000) {
        echo "Sorry, your file is too large.\n";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($check["mime"] != "image/jpg" && $check["mime"] != "image/png" && $check["mime"] != "image/jpeg" && $check["mime"] != "image/gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.\n";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.\n";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES[$field]["tmp_name"], $target_file)) {
            echo "The file ".$field. " has been uploaded.\n";
        } else {
            echo "Sorry, there was an error uploading your file $target_file.\n";
        }
    }
}

upload_file('image');
//upload_file('small');

?>