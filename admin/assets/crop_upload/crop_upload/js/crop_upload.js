/** CropUpload.js v1.0.0
 * Javascript Crop & Upload plugin
 * jQuery version 1.0+
 * XMLHttpRequest 2
 * Support Internet Explorer 9+, Firefox, Opera, Chrome, and Safari
 * Author: Tianhu Yang <tianhuyang@gmail.com>
 * Copyright (c) 2014 iNomad LLC
 */
var cropUploadAPI = function() {
    var e = window.FileReader && function(e, t, n) {
        var r = new FileReader;
        r.onload = function(n) {
            var r = new Image;
            r.onload = function(n) {
                if (!t.beforePick.call(e, r)) {
                    e.setImage(r.src, t.afterPick)
                }
            };
            r.onerror = function(n) {
                t.beforePick.call(e, null)
            };
            r.src = n.target.result
        };
        r.readAsDataURL(n)
    };
    var t = function(e, t) {
        var n = e[0].getAttribute("data-jcu-active");
        e.on("dragenter.jcropUpload", function(t) {
            t.stopPropagation();
            t.preventDefault();
            if (n) {
                e.addClass(n)
            }
        });
        e.on("dragover.jcropUpload", function(t) {
            if (n) {
                e.addClass(n)
            }
            t.stopPropagation();
            t.preventDefault()
        });
        e.on("dragleave.jcropUpload", function(t) {
            if (n) {
                e.removeClass(n)
            }
        });
        e.on("drop.jcropUpload", function(r) {
            if (n) {
                e.removeClass(n)
            }
            r.preventDefault();
            var i = r.originalEvent.dataTransfer.files;
            t(i[0])
        })
    };
    var n = function(e, t) {
        var n = e.crop;
        var r = e.image;
        if (n) {
            if (n[0] < 0 || n[1] < 0 || n[2] <= 0 || n[3] <= 0 || n[0] + n[2] > r.width || n[1] + n[3] > r.height) {
                throw new Error("Invalid crop")
            }
        } else {
            e.crop = [0, 0, r.width, r.height]
        }
        var i = t.canvas;
        var s = t.rect;
        if (s) {
            if (s[0] < 0 || s[1] < 0 || s[2] <= 0 || s[3] <= 0 || s[0] + s[2] > i.width || s[1] + s[3] > i.height) {
                throw new Error("Invalid rect")
            }
        } else {
            t.rect = [0, 0, i.width, i.height]
        }
    };
    var r = window.HTMLCanvasElement && function(e, t) {
        n(e, t);
        var r = e.crop;
        var i = e.image;
        var s = t.canvas;
        var o = t.rect;
        var u = s.getContext("2d");
        u.clearRect(0, 0, s.width, s.height);
        u.drawImage(i, r[0], r[1], r[2], r[3], o[0], o[1], o[2], o[3])
    };
    var i = document.createElement("canvas");
    var s = r && function(e, t, n) {
        if (n) {
            if (n[0] <= 0 || n[1] <= 0) {
                throw new Error("Invalid size")
            }
        } else {
            n = [];
            if (t) {
                n[0] = t[2];
                n[1] = t[3]
            } else {
                n[0] = e.width;
                n[1] = e.height
            }
        }
        if (n[0] > 0 && n[1] > 0) {
            i.width = n[0];
            i.height = n[1]
        }
        r({
            image: e,
            crop: t
        }, {
            canvas: i
        });
        return i
    };
    var o = window.HTMLCanvasElement && window.HTMLCanvasElement.prototype;
    var u = window.BlobBuilder || window.MozBlobBuilder || window.WebKitBlobBuilder || window.MSBlobBuilder;
    var a = false;
    try {
        if (window.Blob) {
            a = Boolean(new Blob)
        }
    } catch (f) {}
    var l = false;
    try {
        if (a && window.Uint8Array) {
            l = (new Blob([new Uint8Array(128)])).size === 128
        }
    } catch (f) {}
    var c = (u || a) && window.ArrayBuffer && window.atob && window.Uint8Array;
    if (c) {
        c = function(e) {
            var t = e.split(",");
            var n;
            if (t[0].indexOf("base64") == -1) {
                n = decodeURIComponent(t[1])
            } else {
                n = atob(t[1])
            }
            var r = new ArrayBuffer(n.length);
            var i = new Uint8Array(r);
            for (var s = 0; s < n.length; s += 1) {
                i[s] = n.charCodeAt(s)
            }
            var o = t[0].split(":")[1].split(";")[0];
            var f;
            if (a) {
                f = new Blob([l ? i : r], {
                    type: o
                })
            } else {
                f = new u;
                f.append(r);
                f = f.getBlob(o)
            }
            return f
        }
    }
    var h = o && c && function(e, t, n) {
        var r;
        if (o.mozGetAsFile) {
            if (n && o.toDataURL) {
                r = c(e.toDataURL(t, n))
            } else {
                r = e.mozGetAsFile("blob", t)
            }
        } else if (o.toDataURL) {
            r = c(e.toDataURL(t, n))
        }
        return r
    };
    var p = h && window.FormData && function(e, t) {
        var n;
        if (!t) {
            t = {}
        }
        if (t.data) {
            if (t.data instanceof FormData) {
                n = t.data
            } else {
                n = new FormData;
                $.each(t.data, function(e, t) {
                    n.append(e, t)
                })
            }
        } else {
            n = new FormData
        }
        var r = true;
        $.each(e, function(e, t) {
            if (t.blob) {
                n.append(e, t.blob)
            } else {
                r = false
            }
        });
        if (r) {
            t.data = n;
            t.processData = t.contentType = false;
            if (t.onprogress) {
                if (t.xhr) {
                    var i = t.xhr;
                    t.xhr = function() {
                        var e = i();
                        if (e.upload) {
                            var n = e.upload.onprogress;
                            e.upload.onprogress = function(e) {
                                t.onprogress.call(this, e);
                                if (n) {
                                    n.call(this, e)
                                }
                            }
                        }
                        return e
                    }
                } else {
                    t.xhr = function() {
                        var e = $.ajaxSettings.xhr();
                        if (e.upload) {
                            e.upload.onprogress = t.onprogress
                        }
                        return e
                    }
                }
            }
            if (!t.type) {
                t.type = "POST"
            }
            r = $.ajax(t)
        }
        return r
    };
    var d = h && function(e) {
        var t;
        var n = s(e.image, e.crop, e.size);
        t = h(n, e.type, e.quality | 1);
        return t
    };
    var v = p && function(e, t) {
        for (var n in e) {
            var r = e[n];
            r.blob = d(r)
        }
        return p(e, t)
    };
    var m = {
        imageToBlob: d,
        cropUpload: v,
        drawImageInCanvas: r
    };
    return m;
}();