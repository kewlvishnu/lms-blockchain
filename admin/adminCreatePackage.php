<?php
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Packages & Subscription</title>
    <?php include "html-template/general/head-tags.html.php"; ?>
	<link href="assets/datetimepicker/jquery.datetimepicker.css" rel="stylesheet" />
</head>
<body>
    <section class="" id="container">
         <?php include "html-template/general/header.html.php"; ?>
      <?php include "html-template/general/admin-sidebar.html.php"; ?>
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper site-min-height">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-12">
                        <?php include "html-template/packages_subs/adminPackagesubs02.php"; ?>
                    </div>
                    
                </div>
                <!-- page end-->
            </section>
        </section>
        <!--main content end-->
        <?php include "html-template/general/footer.html.php"; ?>
        <?php include "html-template/pageCheckFooter.php"; ?>
			<?php include 'html-template/packages_subs/image-upload-modals.html.php'; ?>
    </section>
    <!-- js placed at the end of the document so the pages load faster -->
    <?php include "html-template/general/footer-scripts.html.php"; ?>
	<script src="assets/datetimepicker/jquery.datetimepicker.js"></script>
    <script src="js/draggable-portlet.js"></script>
	<script src="assets/jquery-sieve/dist/jquery.sieve.js"></script>
	<script src="assets/jquery-multi-select/js/jquery.multi-select.js"></script>
<script src="js/custom/adminPackagesSubs.js"></script>
	<script>
		$( function() {
		  // $("#course-cat").selectpicker({});
                   $('#course-cat').multiselect();
				   $('#courses').multiselect();
		});
	</script>
   
</body>
</html>
