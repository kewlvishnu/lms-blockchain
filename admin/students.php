<?php
    require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Students</title>
        <?php include "html-template/general/head-tags.html.php"; ?>

        <link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
        <link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
    </head>
    <body>

        <section id="container" class="">
            <?php include "html-template/general/header.html.php"; ?>
            <?php include "html-template/general/sidebar.html.php"; ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper site-min-height">
                    <!-- page start-->
                    <div class="row">
                        <div class="col-md-12">
                            <?php include "html-template/studentDetails/studentDetailsWithCourses.html.php"; ?> 
                        </div>
                        <!--<div class="col-md-3">
                            <?php include "html-template/general/updates-feed.html.php"; ?>
                        </div>-->
                    </div>
                    <!-- page end-->
                </section>
                <?php include "html-template/studentDetails/inviteStudent.modal.html.php"; ?>
            </section>
            <?php include "html-template/general/footer.html.php"; ?>
            <?php include "html-template/pageCheckFooter.php"; ?>
        </section>
		<?php include 'html-template/general/footer-scripts.html.php'; ?>
        <script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
        <script src="js/custom/get-profile-details.js"></script>
        <!--script for this page only-->
        <script src="js/custom/studentDetailsWithCourses.js"></script>

    </body>
</html>
