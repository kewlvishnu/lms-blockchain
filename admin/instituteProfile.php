<?php
	//require_once "../api/Vendor/ArcaneMind/AccessApi.php";
	//AccessApi::checkAccess();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Profile</title>
      <?php include "html-template/general/admin-header.html.php"; ?>
  </head>
  <body>
	<section id="container" class="">
      <?php include "html-template/general/header.html.php"; ?>
      <?php include "html-template/general/sidebar.html.php"; ?>
            
	  <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                <div class="col-md-12">
					  <?php include "html-template/profile/institute-profile-cover.html.php"; ?>
                                         <?php include "html-template/profile/institute-profile-bio.html.php"; ?>
					  <?php include "html-template/profile/institute-profile-am-experience.html.php"; ?>
					  <?php include "html-template/profile/institute-profile-institute-categories.html.php"; ?>
					  <?php include "admin/html-template/profile/institute-profile-general-info.html.php"; ?> 
                  </div>
                  <!-- <div class="col-md-3">
                      <?php include "html-template/general/updates-feed.html.php"; ?>
                  </div>-->
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
	  <?php include "html-template/general/footer.html.php"; ?>
  </section>
	<?php include "html-template/general/admin-footer.html.php"; ?>
	<script src="js/custom/institute-profile.js"></script>
  </body>
</html>