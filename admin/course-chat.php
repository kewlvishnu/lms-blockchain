<?php
	require_once "html-template/cookieCheck.php";
	require_once "../api/Vendor/ArcaneMind/Api.php";
	require_once "../api/Vendor/ArcaneMind/AccessApi.php";
	$access = AccessApi::checkAccess();
	require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Courses Chat Feature</title>
	<?php include "html-template/general/head-tags.html.php"; ?>
	<link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
	<link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
</head>
<body>
	<section id="container" class="">
		<?php include "html-template/general/header.html.php"; ?>
		<?php include "html-template/general/admin-sidebar.html.php"; ?>
		<!--main content start-->
		<?php include "html-template/course-chat/backendcoursechat.html.php"; ?>
		<!--main content end-->
		<?php include "html-template/general/footer.html.php"; ?>
		<?php include "html-template/pageCheckFooter.php"; ?>
	</section>
	<?php include "html-template/general/footer-scripts.html.php"; ?>
	<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
	<script src="js/custom/back_end_course_chat.js"></script>
</body>
</html>