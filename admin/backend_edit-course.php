<?php
    require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Edit Course</title>
        <link href="assets/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-select/bootstrap-select.css" />
        <?php include "html-template/general/head-tags.html.php"; ?>
    </head>
    <body>
        <section id="container" class="">
            <?php include "html-template/general/header.html.php"; ?>
            <?php include "html-template/general/admin-sidebar.html.php"; ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper site-min-height">
                    <!-- page start-->
                    <div class="row">
                        <div class="col-md-12">
                            <?php include "html-template/edit-course/backend_edit-course-form.html.php"; ?>
                        </div>
                        <!--<div class="col-md-3">
                            <?php include "html-template/general/updates-feed.html.php"; ?>
                        </div>-->
                    </div>
                    <!-- page end-->
                </section>
            </section>
            <!--main content end-->
            <?php include "html-template/general/footer.html.php"; ?>
            <?php include "html-template/pageCheckFooter.php"; ?>
        </section>
        <?php include "html-template/general/footer-scripts.html.php"; ?>
        <script src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="assets/bootstrap-select/bootstrap-select.js"></script>
        <script src="js/custom/backend_edit-course.js"></script>
        <script>
            $(function () {
                $("#edit-form #course-cat").selectpicker({});
            });
        </script>
        <?php include "html-template/edit-course/edit-course-modal.html.php"; ?>
        <?php include "html-template/edit-course/invite_student_modal.html.php"; ?>
    </body>
</html>
