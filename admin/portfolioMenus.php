<?php
	require_once "html-template/cookieCheck.php";
	require_once "../api/Vendor/ArcaneMind/Api.php";
	require_once "../api/Vendor/ArcaneMind/AccessApi.php";
	$access = AccessApi::checkAccess();
	require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Portfolio Dashboard</title>
		<?php include 'html-template/general/head-tags.html.php'; ?>
	</head>
	<body>
	<section id="container" >
		<?php include "html-template/general/header.html.php"; ?>
		<?php include "html-template/general/sidebar.html.php"; ?>
		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
				<div class="well well-sm well-white">
					<h1 class="text-center pf-title">Portfolio Dashboard</h1>
				</div>
				<div class="well well-lg well-white">
				<?php
					if(isset($portfolioDetails) && !empty($portfolioDetails)) {
						if($portfolioDetails->owner == 1) {
				?>
					<div>
						<div class="row">
							<div class="col-md-2">
								<h2 class="lead">Portfolio Menu</h2>
								<hr />
								<ul class="nav nav-pills nav-stacked list-pf-mainmenu">
									<li><a href="portfolio.php"><i class="fa fa-edit"></i> Edit Portfolio</a></li>
									<li><a href="portfolioMenus.php" class="selected"><i class="fa fa-bars"></i> Manage Menus</a></li>
									<li><a href="portfolioPages.php"><i class="fa fa-indent"></i> Manage Pages</a></li>
								</ul>
							</div>
							<div class="col-md-6">
								<h2 class="lead">Manage Menus</h2>
								<hr />
								<form id="frmPortfolioMenus">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<div class="help-block"></div>
											</div>
											<div class="form-group">
												<label for="selectMenuType">Menu Type</label>
												<select class="form-control" id="selectMenuType" name="selectMenuType">
													<option value="page">Page</option>
													<option value="url">Custom URL</option>
												</select>
												<input type="hidden" id="menuId" name="menuId" />
												<input type="hidden" id="menuType" name="menuType" />
											</div>
											<div id="pageBlock" class="menu-block">
												<div class="form-group">
													<label for="selectPortfolioPage">Pages</label>
													<select class="form-control" id="selectPortfolioPage" name="selectPortfolioPage"></select>
												</div>
											</div>
											<div id="urlBlock" class="menu-block hide">
												<div class="form-group">
													<label for="inputMenuUrl">URL</label>
													<input type="text" class="form-control" id="inputMenuUrl" name="inputMenuUrl" placeholder="Enter Menu Name">
												</div>
											</div>
											<div class="form-group">
												<label for="inputMenuName">Name</label>
												<input type="text" class="form-control" id="inputMenuName" name="inputMenuName" placeholder="Enter Menu Name">
											</div>
											<div class="form-group">
												<label for="inputMenuDescription">Description</label>
												<textarea class="form-control" id="inputMenuDescription" name="inputMenuDescription" rows="5" placeholder="Enter Menu Description"></textarea>
											</div>
											<div class="form-group">
												<label for="inputMenuOrder">Menu Order</label>
												<input type="text" class="form-control" id="inputMenuOrder" name="inputMenuOrder" placeholder="Enter Menu Name">
											</div>
											<div class="form-group">
												<label for="selectMenuStatus">Status</label>
												<select class="form-control" id="selectMenuStatus" name="selectMenuStatus">
													<option value="active">Active</option>
													<option value="inactive">Inactive</option>
												</select>
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-primary" id="btnSavePortfolioMenu">Save Menu</button>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="col-md-4">
								<h2 class="lead">List of Menus</h2>
								<hr />
								<form action="">
									<div class="js-portfolio-menus">
										<ul class="nav nav-pills nav-stacked list-pf-menu">
											<li><a href="javascript:void" class="js-menu selected"><span class="checkbox"><input type="radio" name="optionMenu" value="new" checked /> New</span></a></li>
										</ul>
									</div>
								</form>
							</div>
						</div>
					</div>
				<?php
						} else {
							header('location:'.$final_link);
						}
					} else {
						header('location:'.$final_link);
					}
				?>
				</div>
			</section>
		<!--main content end-->
		<!--footer start-->
		<!--footer end-->
	</section>
	<?php include "html-template/general/footer.html.php"; ?>
	<?php include "html-template/pageCheckFooter.php"; ?>
	<?php include 'html-template/general/footer-scripts.html.php'; ?>
	<!--script for this page-->
	<?php if(isset($portfolioDetails) && !empty($portfolioDetails)) { ?>
	<script src="js/custom/portfolioMenus.js" type="text/javascript"></script>
	<?php } ?>
	</body>
</html>