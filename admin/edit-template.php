<?php
	require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Edit Template</title>
		<?php include "html-template/general/head-tags.html.php"; ?>
	</head>
	<body>
		<section id="container" class="">
			<?php include "html-template/general/header.html.php"; ?>
			<link type="stylesheet" href="assets/bootstrap-datepicker/css/datepicker.css">
			<?php include "html-template/general/sidebar.html.php"; ?>
			<!--main content start-->
			<section id="main-content">
				<section class="wrapper site-min-height">
					<!-- page start-->
					<div class="row">
						<div class="col-md-12">
							<?php include "html-template/template/edit-template.html.php"; ?>
						</div>
						<!--<div class="col-md-3">
							<?php include "html-template/general/updates-feed.html.php"; ?>
						</div>-->
					</div>
					<!-- page end-->
				</section>
			</section>
			<!--main content end-->
			<!--footer start-->
			<?php include "html-template/general/footer.html.php"; ?>
      		<?php include "html-template/pageCheckFooter.php"; ?>
			<!--footer end-->
		</section>
	<!-- js placed at the end of the document so the pages load faster -->
	<?php include "html-template/general/footer-scripts.html.php"; ?>
	<script src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="js/custom/edit-template.js"></script>
	</body>
</html>
