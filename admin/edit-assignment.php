<?php
    require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Edit Assignment/Exam</title>
    <?php include "html-template/general/head-tags.html.php"; ?>
	<link href="assets/datetimepicker/jquery.datetimepicker.css" rel="stylesheet" />
</head>
<body>
    <section class="" id="container">
		<?php include "html-template/general/header.html.php"; ?>
		<?php include "html-template/general/sidebar.html.php"; ?>
		<!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            <!-- page start-->
            <div class="row">
				<div class="col-md-12">
					<!--breadcrum start-->
					<?php include 'html-template/assignment/assignment-breadcrumb.php'; ?>
					<!--breadcrum ends-->
					<?php include "html-template/assignment/edit-assignment.html.php"; ?>
				</div>
				<!--<div class="col-md-3">
					<?php include "html-template/general/updates-feed.html.php"; ?>
				</div>-->
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!--footer start-->
      <?php include "html-template/general/footer.html.php"; ?>
      <?php include "html-template/pageCheckFooter.php"; ?>
      <!--footer end-->
  </section>
    <!-- js placed at the end of the document so the pages load faster -->
    <?php include "html-template/general/footer-scripts.html.php"; ?>
    <!--common script for all pages-->
	<script src="assets/datetimepicker/jquery.datetimepicker.js"></script>
	<script src="js/custom/edit-assignment.js"></script>
</body></html>
