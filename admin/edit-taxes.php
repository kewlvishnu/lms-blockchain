<?php
    require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Taxes</title>
    <?php include "html-template/general/head-tags.html.php"; ?>
</head>
<body>
    <section id="container" class="">
	  <?php include "html-template/general/header.html.php"; ?>
      <?php include "html-template/general/admin-sidebar.html.php"; ?>
      <!--main content start-->
     <?php include "html-template/taxes/taxes.html.php"; ?>
      <!--main content end-->
      <?php include "html-template/general/footer.html.php"; ?>
      <?php include "html-template/pageCheckFooter.php"; ?>
	   </section>
    <?php include "html-template/general/footer-scripts.html.php"; ?>

    <?php include 'html-template/taxes/taxes_edit_modal.html.php'; ?>
    <?php include 'html-template/taxes/taxes_edit_default_modal.html.php'; ?>
    <script src="js/custom/taxes.js"></script>
</body>
</html>