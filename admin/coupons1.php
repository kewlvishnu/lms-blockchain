

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Discount & Coupons</title>
    <?php include "html-template/general/head-tags.html.php"; ?>
      <link href="css/bootstrap.min.css" rel="stylesheet">
       <link href="css/bootstrap.css" rel="stylesheet">
   
<link href="css/style.css" rel="stylesheet" />
<link href="css/style-responsive.css" rel="stylesheet" />
</head>
<body>
    <section class="" id="container">
        <?php include "html-template/general/header.html.php"; ?>
		<?php include "html-template/general/sidebar.html.php"; ?>
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper site-min-height">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-12">
                        <?php include "html-template/discount-coupons/coupons.html.php"; ?>
                    </div>
                    
                </div>
                <!-- page end-->
            </section>
        </section>
        <!--main content end-->
		
        <?php include "html-template/general/footer.html.php"; ?>
    </section>
    <!-- js placed at the end of the document so the pages load faster -->
	<script src="js/custom/discounts_coupons.js"></script>
    <?php include "html-template/general/footer-scripts.html.php"; ?>
    
   </body>
</html>
