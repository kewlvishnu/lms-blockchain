<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>Key Details</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
	  <header class="header white-bg">
  <div class="sidebar-toggle-box">
	  <div data-original-title="Toggle Navigation" data-placement="right" class="fa fa-bars tooltips"></div>
  </div>
  <!--logo start-->
  <a href="index.html" class="logo"><img src="../img/Arcane_Logo.png"></a>
  <!--logo end-->          
  <div class="top-nav ">
	  <ul class="nav pull-right top-menu">
		  <li class="dropdown" id="header_notification_bar">
			<a data-toggle="dropdown" href="#" class="dropdown-toggle">
			<i class="fa fa-bell-o"></i>
			<span class="badge bg-warning">7</span>
			</a>
			</li>
		  <!-- user login dropdown start-->
		  <li class="dropdown">
			  <a data-toggle="dropdown" class="dropdown-toggle" href="#" id="user-profile-card">
				  <img width="29px" height="29px" alt="" class="user-image" src="#">
				  <span class="username">akgec</span>
				  <b class="caret"></b>
			  </a>
			  <ul class="dropdown-menu extended logout">
				  <div class="log-arrow-up"></div>
				  <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
				  <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
				  <li><a href="#"><i class="fa fa-bell-o"></i> Notification</a></li>
				  <li><a href="../logout.php"><i class="fa fa-key"></i> Log Out</a></li>
			  </ul>
		  </li>
		  <!-- user login dropdown end -->
	  </ul>
  </div>
</header>
      <!--main content start-->
	  <section>
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                  
                  <div class="col-md-12">

                      <section class="panel">
                          <div class="panel-body">
                              
                              <div class="12">
                                  <h4>
                                      <a href="#" class="">Purchase History</a>
                                  </h4>
                                  <p>
                                      Praesent ac condimentum felis. Nulla at nisl orci, at dignissim dolor, The best product descriptions address your ideal buyer directly and personally. The best product descriptions address your ideal buyer directly and personally.
                                  </p>
                                  <div class="product_meta"><table class="table table-striped">
        <thead>
        <tr>
            <td>Title</td>
            <td>Total</td>
            <td>Arcanemind Credit Applied</td>
            <td>Payment</td>
            <td>Type</td>
            <td>Credit/Debit Card</td>
            <td>Date</td>
            <td>Status</td>
            <td>#</td>
        </tr>
        </thead>
                <tbody>
			<tr>
                <td><a target="_self" title="Microsoft Excel 2010 Course Beginners/ Intermediate Training" u_courseid="9287" class="course" href="#">Microsoft Excel 2014 Course Beginners/ Intermediate Training</a></td>
                <td>$10.00</td>
                <td>$0.00</td>
                <td>$10.00</td>
                <td>Credit/Debit Card</td>
                <td>ending in 8589</td>
                <td>2014-01-23 09:48:52</td>
                <td>
                                    </td>
                <td><a href="#">receipt</a></td>
            </tr>
            </tbody></table>                                 
                                      
                                  </div>
                                  <h4>
                                      <a class="" href="#">Refunds</a>
                                  </h4>
                                  <table class="table table-striped">
        <thead>
        <tr>
            <td>Title</td>
            <td>Amount</td>
            <td>Refund Type</td>
            <td>Credit/Debit Card</td>
            <td>Date</td>
        </tr>
        </thead>
            </table>
                                  
                              </div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
	  <!--main content end-->

      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              &copy; 2014 Arcanemind. All Rights Reserved.
              <a class="go-top" href="#">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
	  </section>
	  <?php include 'html-template/general/footer-scripts.html.php'; ?>
</body>
  </html>
