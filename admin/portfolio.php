<?php
	require_once "html-template/cookieCheck.php";
	require_once "../api/Vendor/ArcaneMind/Api.php";
	require_once "../api/Vendor/ArcaneMind/AccessApi.php";
	$access = AccessApi::checkAccess();
	require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Portfolio Dashboard</title>
		<?php include 'html-template/general/head-tags.html.php'; ?>
		<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
		<link rel="stylesheet" href="assets/file-upload/css/jquery.fileupload.css">
	</head>
	<body>
	<section id="container" >
		<?php include "html-template/general/header.html.php"; ?>
		<?php include "html-template/general/sidebar.html.php"; ?>
		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
				<div class="well well-sm well-white">
					<h1 class="text-center pf-title">Portfolio Dashboard</h1>
				</div>
				<div class="well well-lg well-white">
				<?php
					if(isset($portfolioDetails) && !empty($portfolioDetails)) {
						if($portfolioDetails->owner == 1) {
				?>
					<div class="row">
						<div class="col-md-2">
							<div class="pf-avatar">
								<img class="img-responsive" src="<?php echo $portfolioDetails->portfolio['img']; ?>" id="profilePic">
								<input type="hidden" id="portfolioPic" name="portfolioPic">
							</div>
							<div class="text-center mrg-b-20">
								<button type="button" class="btn btn-info" data-target="uploadPortfolioImageModal" data-toggle="modal" id="btnChangeImg">Change</button>
							</div>
							<?php
								$status = $portfolioDetails->portfolio['status'];
								switch ($status) {
									case 'Waiting for approval':
										echo '<p class="jumbotron text-center">Your portfolio is under review for approval.<br>Thank you for you patience!</p>';
										break;
									case 'Disapproved':
										$reason = '';
										if (!empty($portfolioDetails->portfolio['reason'])) {
											$reason = ' due to : '.$portfolioDetails->portfolio['reason'];
										}
										echo '<p class="jumbotron text-center">Your portfolio was disapproved'.$reason.'.<br>Please contact support for further details.<br>Thank you for your patience!';
										break;
									
									default:
							?>
							<ul class="nav nav-pills nav-stacked list-pf-mainmenu">
									<li><a href="portfolio.php" class="selected"><i class="fa fa-edit"></i> Edit Portfolio</a></li>
									<li><a href="portfolioMenus.php"><i class="fa fa-bars"></i> Manage Menus</a></li>
									<li><a href="portfolioPages.php"><i class="fa fa-indent"></i> Manage Pages</a></li>
							</ul>
							<?php
										break;
								}
							?>
						</div>
						<div class="col-md-10">
							<form action="" id="frmPortfolio">
								<div class="form-group">
									<div class="help-block"></div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<p class="lead">Basic Information</p>
										<hr />
										<div class="form-group">
											<label for="">Name :</label>
											<input type="text" class="form-control" id="portfolioName" value="<?php echo $portfolioDetails->portfolio['name']; ?>" />
										</div>
										<div class="form-group">
											<label for="">Description :</label>
											<textarea class="form-control" id="portfolioDescription" rows="5"><?php echo $portfolioDetails->portfolio['description']; ?></textarea>
										</div>
										<div class="form-group">
											<label for="">Subdomain :</label>
											<input type="text" class="form-control" id="portfolioSubdomain" value="<?php echo $slug; ?>" disabled />
										</div>
										<?php
											if ($portfolioDetails->userRole == 2) {
										?>
										<div class="form-group">
											<label for="">Affiliation :</label>
											<input type="text" class="form-control" id="affiliation" value="<?php echo $portfolioDetails->portfolio['affiliation']; ?>" />
										</div>
										<?php
											}
										?>
									</div>
									<div class="col-md-6">
										<p class="lead">Social Information</p>
										<hr />
										<div class="form-group">
											<label for="">Favicon : </label>
											<div class="row">
												<div class="col-xs-2">
													<div class="form-control text-center">
														<img class="dib" src="<?php echo $favicon; ?>" id="favicon" />
													</div>	
												</div>
												<div class="col-xs-8">
													<!-- The fileinput-button span is used to style the file input field as button -->
													<span class="btn btn-success fileinput-button">
														<i class="glyphicon glyphicon-plus"></i>
														<span>Select files for favicon...</span>
														<!-- The file input field used as target for the file upload widget -->
														<input id="fileFavicon" type="file" name="fileFavicon">
													</span>
													<input type="hidden" id="inputFavicon" name="inputFavicon" />
												</div>
											</div>
										</div>
										<div class="form-group">
											<label for="">Facebook :</label>
											<input type="text" class="form-control" id="inputFacebook" value="<?php echo $portfolioDetails->portfolio['facebook']; ?>" />
										</div>
										<div class="form-group">
											<label for="">Twitter :</label>
											<input type="text" class="form-control" id="inputTwitter" value="<?php echo $portfolioDetails->portfolio['twitter']; ?>" />
										</div>
										<div class="form-group">
											<label for="">Linkedin :</label>
											<input type="text" class="form-control" id="inputLinkedin" value="<?php echo $portfolioDetails->portfolio['linkedin']; ?>" />
										</div>
										<div class="form-group">
											<label for="">Status :</label>
											<?php
												if (count($portfolioDetails->listStatus) > 0) {
													echo '<select id="selectStatus" name="selectStatus" class="form-control">';
													foreach ($portfolioDetails->listStatus as $key => $value) {
														if ($key>1) {
															$status = $portfolioDetails->portfolio['status'];
															echo '<option value="'.$value.'" '.(($status==$value)?'selected':'').'>'.$value.'</option>';
														}
													}
													echo '</select>';
												}
											?>
										</div>
										<div class="form-group">
											<button type="submit" class="btn btn-primary pull-right" id="btnUpdatePortfolio">Update Portfolio</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				<?php
						} else {
							//header('location:'.$final_link);
						}
					} else {
				?>
					<div>
						<h2 class="lead">Add New Porfolio</h2>
						<hr />
						<div class="row">
							<div class="col-md-8">
								<form class="form-horizontal invisible" id="frmNewPortfolio">
									<div class="row">
										<div class="col-md-3 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2">
											<div class="pf-avatar">
												<img class="img-responsive" src="#" id="profilePic">
												<input type="hidden" id="portfolioPic" name="portfolioPic">
											</div>
											<div class="text-center">
												<button type="button" class="btn btn-info" data-target="uploadPortfolioImageModal" data-toggle="modal" id="btnChangeImg">Change</button>
											</div>
										</div>
										<div class="col-md-8 col-md-offset-1">
											<div class="form-group">
												<div class="help-block"></div>
											</div>
											<div class="form-group">
												<label for="inputProfileName" class="col-lg-4 control-label">Portfolio Name</label>
												<div class="col-lg-8">
													<input type="text" class="form-control" id="inputProfileName" placeholder="Enter Portfolio Name">
												</div>
											</div>
											<div class="form-group">
												<label for="inputPortfolioDescription" class="col-lg-4 control-label">Portfolio Description</label>
												<div class="col-lg-8">
													<textarea class="form-control" id="inputPortfolioDescription" rows="5" placeholder="Enter Portfolio Description"></textarea>
												</div>
											</div>
											<div class="form-group">
												<label for="inputSubdomain" class="col-lg-4 control-label">Subdomain</label>
												<div class="col-lg-8">
													<input type="text" class="form-control" id="inputSubdomain" placeholder="Enter Subdomain">
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-offset-4 col-lg-8">
													<button type="submit" class="btn btn-primary" id="btnSubmitNewPortfolio">Submit for approval</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="col-md-4">
								<ul class="pf-list-md">
									<li>This is a form for a new portfolio for your institute</li>
									<li>Institute name and description are compulsory</li>
									<li>Be short and precise with your subdomain 4-20 letters are allowed</li>
									<li>After submission it may take 3-4 days for approval.</li>
								</ul>
							</div>
						</div>
						<hr>
						<h2 class="lead">List of Portfolios</h2>
						<hr />
						<div class="js-portfolios"></div>
					</div>
				<?php
					}
				?>
				</div>
			</section>
		<!--main content end-->
		<!--footer start-->
		<!--footer end-->
	</section>
	<?php include "html-template/general/footer.html.php"; ?>
	<?php include "html-template/portfolio/image-upload-modals.html.php"; ?>
	<?php include "html-template/portfolio/editportfoliomodals.html.php"; ?>
	<?php include "html-template/pageCheckFooter.php"; ?>
	<?php include 'html-template/general/footer-scripts.html.php'; ?>
	<!--script for this page-->
	<?php if(isset($portfolioDetails) && !empty($portfolioDetails)) { ?>
	<script src="js/custom/portfolioDashboard.js" type="text/javascript"></script>
	<?php } else { ?>
	<script src="js/custom/portfolio.js" type="text/javascript"></script>
	<?php } ?>
	<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="assets/file-upload/js/jquery.iframe-transport.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="assets/file-upload/js/jquery.fileupload.js"></script>
	</body>
</html>