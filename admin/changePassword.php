<?php
    require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>View Institutes</title>
        <?php include "html-template/general/head-tags.html.php"; ?>

       </head>
    <body>

        <section id="container" class="">
            <?php include "html-template/general/header.html.php"; ?>
            <?php include "html-template/general/sidebar.html.php"; ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper site-min-height">
                    <!-- page start-->
                    <div class="row">
                        <div class="col-md-12">
                            <?php include "html-template/ChangePassword/ChangePassword.html.php"; ?> 
                        </div>
                    </div>
                    <!-- page end-->
                </section>
            </section>
            <?php include "html-template/general/footer.html.php"; ?>
            <?php include "html-template/pageCheckFooter.php"; ?>
        </section>

        <!-- js placed at the end of the document so the pages load faster -->
        <!--<script src="js/jquery.js"></script>-->
        <script type="text/javascript" language="javascript" src="assets/advanced-datatable/media/js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/custom/get-profile-details.js"></script>
       
        <script src="js/custom/ChangePassword.js"></script>
        <!--script for this page only-->

    </body>
</html>
