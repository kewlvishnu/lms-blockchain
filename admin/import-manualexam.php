<?php
	require_once "html-template/cookieCheck.php";
	require_once "../api/Vendor/ArcaneMind/Api.php";
	require_once "../api/Vendor/ArcaneMind/AccessApi.php";
	$access = AccessApi::checkAccess();
	require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Import Manual Exam</title>
	<?php include "html-template/general/head-tags.html.php"; ?>
	<link href="assets/datetimepicker/jquery.datetimepicker.css" rel="stylesheet" />
</head>
<body>
	<section class="" id="container">
		<?php include "html-template/general/header.html.php"; ?>
		<?php include "html-template/general/sidebar.html.php"; ?>
		<!--main content start-->
		<section id="main-content">
			<section class="wrapper site-min-height">
				<!-- page start-->
				<div class="row">
					<div class="col-md-12">
						<?php include "html-template/manual-exam/manual-exam-breadcrumb.php"; ?>
						<?php include "html-template/manual-exam/import-manualexam.html.php"; ?>
					</div>
				</div>
				<!-- page end-->
			</section>
		</section>
	<!--main content end-->
	<!--footer start-->
	<?php include "html-template/general/footer.html.php"; ?>
	<?php include "html-template/pageCheckFooter.php"; ?>
	<!--footer end-->
	</section>
	<!-- js placed at the end of the document so the pages load faster -->
	<?php include "html-template/general/footer-scripts.html.php"; ?>
	<script src="assets/datetimepicker/jquery.datetimepicker.js"></script>
	<script src="js/custom/import-manualexam.js"></script>
</body></html>
