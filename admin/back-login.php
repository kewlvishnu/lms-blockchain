<?php
	$favicon 		  = "img/favicon.ico";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Back Login</title>
    <?php include "html-template/general/head-tags.html.php"; ?>
</head>
<body>
    <section id="container" class="">
	  <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-md-9">
                      <div class="back-login-form clearfix">
							<form role="form">
								<div class="form-group">
									<input type="text" class="form-control" id="username" placeholder="Enter username or email"/>
								</div>
								<div class="form-group">
									<input type="password" class="form-control" id="password" placeholder="Password">
								</div>
								<div class="pull-left">
									<div class="error-msg"></div>
									<button class="btn btn-success back-login">Sign In</button>
								</div>
							</form>
						</div>
                  </div>
                  <div class="col-md-3">
                      
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <?php include "html-template/general/footer.html.php"; ?>
  </section>
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
	<script src="assets/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="js/jquery.scrollTo.min.js"></script>
	<script src="js/jquery.nicescroll.js" type="text/javascript"></script>
	<script src="js/respond.min.js" ></script>
    <script>
		var ApiEndpoint = '../api/index.php';
		$('.back-login').on('click', function(e) {
			e.preventDefault();
			var username = $('#username').val();
			var password = $('#password').val();
			if( username == '' || password == ''){
				$('.error-msg').html('Enter Creds!');
				return;
			}
			
			var req = {};
			
			req.username = username;
			req.password = password;
			req.action = 'back-login';
			
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done( function (res) {
				console.log(res);
				res =  $.parseJSON(res);
				if(res.valid == true) {
					window.location.href = "approvals.php";
					return;
				} else {
					alertMsg(res.reason);
				}
			});	
		});
	</script>
</body>
</html>
