<?php
    require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Coupons</title>
    <?php include "html-template/general/head-tags.html.php"; ?>
      <link href="css/bootstrap.min.css" rel="stylesheet">
       <link href="css/bootstrap.css" rel="stylesheet">
        <link href="assets/datetimepicker/jquery.datetimepicker.css" rel="stylesheet" />
   
<link href="css/style.css" rel="stylesheet" />
<link href="css/style-responsive.css" rel="stylesheet" />
</head>
<body>
    <section class="" id="container">
        <?php include "html-template/general/header.html.php"; ?>
		<?php include "html-template/general/sidebar.html.php"; ?>
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper site-min-height">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-12">
                        <?php include "html-template/discount-coupons/coupons.html.php"; ?>
                    </div>
                    
                </div>
                <!-- page end-->
            </section>
        </section>
        <!--main content end-->
        <?php include "html-template/general/footer.html.php"; ?>
    </section>
    <!-- js placed at the end of the document so the pages load faster -->
    <?php include "html-template/general/footer-scripts.html.php"; ?>

   	<script src="js/custom/coupons.js"></script>

 <script src="assets/datetimepicker/jquery.datetimepicker.js"></script>
  <?php include "html-template/discount-coupons/couponsmodals.html.php"; ?>
 
    
   </body>
</html>
