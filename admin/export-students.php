<?php
	require_once "html-template/cookieCheck.php";
	require_once "../api/Vendor/ArcaneMind/Course.php";
	require_once "../api/Vendor/ArcaneMind/Api.php";
	require_once "../api/Vendor/ArcaneMind/AccessApi.php";
	$access = AccessApi::checkAccess();
	require_once "html-template/pageCheck.php";
	$courseId = $_GET['courseId'];
	if (isset($courseId) && !empty($courseId)) {
		$courseId = (int)$courseId;
		$data = new StdClass();
		$data->courseId = $courseId;
		$data->userId = $_SESSION['userId'];
		$data->userRole = $_SESSION['userRole'];
		$c = new Course();
		$res = $c->exportCourseStudents($data);
		if ($res->status == 0) {
			echo $res->message;
		} else {
			//var_dump($res->students);
			$fp = fopen('php://output', 'w'); 
			if ($fp) 
			{
				$headers = array("ID", "Username", "Password", "Name", "Email", "Parent Username", "Parent Password");
				header('Content-Type: text/csv; charset="UTF-8"');
				header('Content-Disposition: attachment; filename="export.csv"');
				header('Pragma: no-cache');
				header('Expires: 0');
				fputcsv($fp, $headers);
				$students = $res->students;
				foreach ($students as $key => $value) {
					fputcsv($fp, $value); 
				}
			}
		}
	}
?>