<?php
    require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>View Subject Results</title>
    <?php include "html-template/general/head-tags.html.php"; ?>
	<link href="assets/morris.js-0.4.3/morris.css" rel="stylesheet" />
</head>
<body>
    <section class="" id="container">
        <?php include "html-template/general/header.html.php"; ?>
		<?php include "html-template/general/sidebar.html.php"; ?>
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper site-min-height">
                <!-- page start-->
                <div class="row">
                    <div class="col-md-12">
                        <?php include "html-template/assignment/subjectResult.html.php"; ?>
                    </div>
                    <!--<div class="col-md-3">
                        <?php include "html-template/general/updates-feed.html.php"; ?>
                    </div>-->
                </div>
                <!-- page end-->
            </section>
        </section>
        <!--main content end-->
        <?php include "html-template/general/footer.html.php"; ?>
        <?php include "html-template/pageCheckFooter.php"; ?>
    </section>
    <!-- js placed at the end of the document so the pages load faster -->
    <?php include "html-template/general/footer-scripts.html.php"; ?>
    <script src="js/draggable-portlet.js"></script>
	<script src="assets/morris.js-0.4.3/morris.min.js" type="text/javascript"></script>
    <script src="assets/morris.js-0.4.3/raphael-min.js" type="text/javascript"></script>
	<script src="js/custom/subjectResult.js" type="text/javascript"></script>
    <!--common script for all pages ends-->
</body>
</html>
