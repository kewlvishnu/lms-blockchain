var DataSource = function (options) {
this._formatter = options.formatter;
this._columns = options.columns;
this._data = options.data;
};

DataSource.prototype = {
	
	columns: function () {
		return this._columns;
	},

	data: function (options, callback) {

		var self = this;
		if (options.search) {
			callback({ data: self._data, start: start, end: end, count: count, pages: pages, page: page });
        } else if (options.data) {
			callback({ data: options.data, start: 0, end: 0, count: 0, pages: 0, page: 0 });
		}  else {
			callback({ data: self._data, start: 0, end: 0, count: 0, pages: 0, page: 0 });
		}
	}
};
$(document).ready(function(){
	fetchLeftTree();
});
function fetchLeftTree() {
	var req = {};
	var string = '{ "data": [ ';
	req.action = 'get-all-institute-courses';
	req.slug	= $('#slug').val();
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		for(i=0; i<res.courses.length; i++) {
			string+='{ "name": "<span class=\'navigationNodes\' data-target=\'edit-course.php?courseId='+res.courses[i]['id']+'\' id=\'C'+res.courses[i]['id']+'\'>'+res.courses[i]['name']+'</span>", "type": "';
			//if(res.courses[i]['subjects']>0) {
				string+='folder", "data" : [ ';
				for(j=0 ; j<res.subjects.length ; j++) {
					if(res.courses[i]['id'] == res.subjects[j]['courseId']) {
						string+='{ "name" : "<span class=\'navigationNodes\' data-target=\'edit-subject.php?courseId='+res.courses[i]['id']+'&subjectId='+res.subjects[j]['id']+'\' id=\'B'+res.subjects[j]['id']+'\'>'+res.subjects[j]['name']+'</span>", "type" : "';
						//if(res.subjects[j]['chapters']>0) {
							string+='folder", "data" : [ ';
							for(k=0 ; k<res.chapters.length ; k++) {
								if(res.subjects[j]['id'] == res.chapters[k]['subjectId']) {
									string+='{ "name" : "<span class=\'navigationNodes\' data-target=\'content.php?courseId='+res.courses[i]['id']+'&subjectId='+res.subjects[j]['id']+'\'>'+res.chapters[k]['name']+'</span>", "type" : "item", "additionalParameters" : { "id" : "A'+res.chapters[k]['id']+'" } },';
									for(var l = 0; l < res.chapters[k].exams.length; l++) {
										string += '{ "name" : "<span class=\'navigationNodes\' data-target=\'add-assignment-2.php?examId=' + res.chapters[k].exams[l].id + '\'>' + res.chapters[k].exams[l].name;
										if(res.chapters[k].exams[l].type == "Exam")
											string += ' (E)';
										else
											string += ' (A)';
										string += '</span>", "type" : "item" }, ';
									}
								}
							}
							string+='{ "name" : "<span class=\'navigationNodes\' data-target=\'content.php?courseId='+res.courses[i]['id']+'&subjectId='+res.subjects[j]['id']+'\'><i class=\'fa fa-gear\'></i> Add Section</span>", "type" : "item" }, ';
							for(var k = 0; k < res.subjects[j].exams.length; k++) {
								string += '{ "name" : "<span class=\'navigationNodes\' data-target=\'add-assignment-2.php?examId=' + res.subjects[j].exams[k].id + '\'>' + res.subjects[j].exams[k].name;
								if(res.subjects[j].exams[k].type == "Exam")
									string += ' (E)';
								else
									string += ' (A)';
								string += '</span>", "type" : "item" }, ';
							}
							string += '{ "name" : "<span class=\'navigationNodes\' data-target=\'add-assignment.php?courseId=' + res.courses[i].id + '&subjectId=' + res.subjects[j].id + '\'><i class=\'fa fa-gear\'></i> Add Exam/Assignment</span>", "type" : "item" } ],';
						/*}
						else
							string+='item", ';*/
						string+='"additionalParameters" : { "id" : "B'+res.subjects[j]['id']+'" } },';
					}
				}
				string+='{ "name" : "<span class=\'navigationNodes\' data-target=\'add-subject.php?courseId='+res.courses[i]['id']+'\'><i class=\'fa fa-gear\'></i> Add Subject</span>", "type" : "item" } ], ';
			/*}
			else
				string+='item", ';*/
			string+='"additionalParameters" : { "id" : "C'+res.courses[i]['id']+'" } },';
		}
		string+='{ "name" : "<span class=\'navigationNodes\' data-target=\'add-course.php\'><i class=\'fa fa-gear\'></i> Add Course</span>", "type" : "item" } ], "delay": 0 }';
		data = $.parseJSON(string);
		var treeDataSource = new DataSource(data);
		$('#Div1').tree({dataSource: treeDataSource});
		$('.navigationNodes').click(function() {
			if(!$(this).parents('.tree-folder-header').hasClass('clicked')) {
					$(this).parents('.tree-folder-header').addClass('clicked');
					$(this).parents('.tree-folder-header').click();
				}
			window.location = $(this).attr('data-target');
		});
		$('#Div1').on('loaded', function (evt, data) {
			$('.navigationNodes').click(function() {
				if(!$(this).parents('.tree-folder-header').hasClass('clicked')) {
					$(this).parents('.tree-folder-header').addClass('clicked');
					$(this).parents('.tree-folder-header').click();
				}
				window.location = $(this).attr('data-target');
			});
		});
		path = window.location.pathname.toLowerCase();
		if(path.search("courses.php") != -1)
			$('.course').parents('a').click();
		if(path.search("edit-course.php") != -1 || path.search("add-course.php") != -1) {
			$('.course').parents('a').click();
			var courseId = getUrlParameter('courseId');
			$('#C'+courseId).parents('.tree-folder-header').click();
		}
		if((path.search("edit-subject.php") != -1 || path.search("add-subject.php") != -1 || path.search("content.php") != -1 || path.search("add-chapter.php") != -1) && path.search('chapter-content') == -1) {
			$('.course').parents('a').click();
			var courseId = getUrlParameter('courseId');
			var subjectId = getUrlParameter('subjectId');
			$('#C'+courseId).parents('.tree-folder-header').click();
			$('#B'+subjectId).parents('.tree-folder-header').click();
		}
		if(path.search("students.php") != -1 || path.search("invited_students_history.php") != -1)
			$('span.students').parents('a').click();
		$('#Div1').on('opened', function (evt, data) {
			$("#sidebar").getNiceScroll().resize();
		});
		$('#Div1').on('closed', function (evt, data) {
			$("#sidebar").getNiceScroll().resize();
		});
		/*$('#Div1').on('selected', function (evt, data) {
			console.log('item selected: ', data);
		});*/
	});
}