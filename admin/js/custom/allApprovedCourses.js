/*
 Author: Rupesh Pandey.
 Dated: 02/04/2015
 */
var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var userRole;

$(function () {
	fetchCourses();
});

function fetchCourses() {
	var req = {};
	var res;
	req.action = "get-all-approved-courses-admin";
	$.ajax({
		'type': 'post',
		'url': ApiEndpoint,
		'data': JSON.stringify(req)
	}).done(function (res) {
		res = $.parseJSON(res);
		if (res.status == 1)
			fillCourses(res);
		else
			alertMsg(res.message);
	});
}

function fillCourses(data) {
	var html = "";
	var flag = false;
	var editCourseLink, editSubjectLink, addSubjectLink;
	$.each(data.courses, function (i, course) {
		editCourseLink = "backend_edit-course.php?courseId=" + course.id;
		addSubjectLink = "add-subject.php?courseId=" + course.id;
		courseId = "C" + String("000" + course.id).slice(-4);
		html += "<tr data-cid='" + course.id + "'>"
				+ "<td>" + courseId + "</td>"
				+ "<td><h5><a href='" + editCourseLink + "'>" + course.name + "</a></h5>";

		html += "</td>"
		html += "<td>" + course.userid + "</td>";
		html += "<td>" + course.username + "</td>";
		html += "<td class=small-txt>" + course.email + "</td>";
		html += "<td>" + course.contactMobile + "</td>"
				+ "<td><a href='#' class='reject'><span class='label label-danger label-mini' style='display:block;'>Reject</span></a>"
				+ "</tr>";
	});
	$('#all-courses tbody').html('').html(html);
		$('#all-courses').dataTable({
                "aaSorting": [[0, "asc"]]
            });
	addApprovalHandlers();
}

function addApprovalHandlers() {
	$('#all-courses').on( 'click', '.reject', function (e) {
	//$('#all-courses .reject').on('click', function (e) {
		e.preventDefault();
		var con = confirm("Are you sure you want to reject this course?");
		if(con) {
			var course = $(this).parents('tr');
			var cid = course.attr('data-cid');
			var req = {};
			req.action = "reject-course";
			req.courseId = cid;
			$.ajax({
				'type': 'post',
				'url': ApiEndpoint,
				'data': JSON.stringify(req)
			}).done(function (res) {
				console.log(res);
				res = $.parseJSON(res);
				if (res.status == 1) {
					alertMsg("Course removed from Market Place successfully.");
					fetchCourses();
				} else {
					alertMsg(res.message);
				}
			});
		}
	});
}