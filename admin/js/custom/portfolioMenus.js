var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var userRole;
$(document).ajaxStart(function () {
    $('#loader_outer').show();
});

$(document).ajaxComplete(function () {
    $('#loader_outer').hide();
});

$(function () {
    var imageRoot = '/';
    var jcrop_api;
    var portfolioMenus = [];
    var portfolioPages = [];
    $('#loader_outer').hide();
    var fileApiEndpoint = '../api/files1.php';
    fetchPortfolioMenus();
    fetchPortfolioPages();
    $('#selectMenuType').change(function(){
        var menuType = $(this).val();
        $('.menu-block').addClass('hide');
        $('#'+menuType+'Block').removeClass('hide');
    });
    $('.js-portfolio-menus').on('click','.js-menu', function(){
        $('.js-menu').removeClass('selected');
        $(this).addClass('selected');
        var pageChecked = $(this).find('[name=optionMenu]');
        $('.js-portfolio-menus [name=optionMenu]').removeAttr('checked');
        pageChecked.prop('checked', true);
        var menuId = pageChecked.val();
        menuProcess(menuId);
    });
    $('.js-portfolio-menus').on('click','[name=optionMenu]', function(){
        var thiz = $(this);
        $('.js-menu').removeClass('selected');
        thiz.closest('.js-menu').addClass('selected');
        var menuId = thiz.val();
        menuProcess(menuId);
        return true;
    });
    function menuProcess(menuId) {
        if(menuId == 'new') {
            $("#menuId").val('');
            $("#pageType").val('');
            resetForm();
            $('#selectMenuType').closest('.form-group').removeClass('hide');
            $('.menu-block').addClass('hide');
            var pageType = $('#selectMenuType option:selected').text().toLowerCase();
            $('#'+pageType+'Block').removeClass('hide');
            $('#inputPageContent').val('');
        } else {
            resetForm();
            $('#menuId').val(menuId);
            var menu = findObjByValue(portfolioMenus,'menuId',menuId);
            $('#selectMenuType').val(menu.type);
            $('.menu-block').addClass('hide');
            $('#'+menu.type+'Block').removeClass('hide');
            $('#inputMenuName').val(menu.name);
            $('#inputMenuDescription').val(menu.description);
            $('#inputMenuOrder').val(menu.order);
            $('#selectMenuStatus').val(menu.status);
            if(menu.type == 'page') {
                $('#selectPortfolioPage').append('<option value="'+menu.pageId+'" selected> '+menu.pageTitle+' [Type : '+menu.pageType+']</option>');
            } else {
                $('#inputMenuUrl').val(menu.url);
            }
        }
    }
    function resetForm() {
        $('#selectMenuType').val('page');
        $('#pageBlock').removeClass('hide');
        $('#urlBlock').addClass('hide');
        $('#inputMenuUrl').val('');
        $('#inputMenuName').val('');
        $('#inputMenuDescription').val('');
        $('#inputMenuOrder').val('');
        $('#selectMenuStatus').val('active');
        fillPortfolioPages(portfolioPages);
    }
    function fetchPortfolioMenus() {
        var req = {};
        req.slug = trim($('#slug').val());
        req.menu = 1;
        var res;
        req.action = "get-portfolio-menus";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            portfolioMenus = res.portfolioMenus;
            fillPortfolioMenus(res);
            //console.log(res);
        });
    }
    function fillPortfolioMenus(data) {
        // General Details
        if(data.portfolioMenus.length>0) {
            //$('.js-portfolio-menus').html('<ul class="nav nav-pills nav-stacked"></ul>');
            var portfolioMenutype = '';
            for (var i = 0; i < data.portfolioMenus.length; i++) {
                portfolioMenutype = ((data.portfolioMenus[i].type=='page')?'Page : '+data.portfolioMenus[i].pageTitle:'URL : '+data.portfolioMenus[i].url);
                $('.js-portfolio-menus>.nav-pills').append('<li><a href="javascript:void()" class="js-menu"><span class="checkbox"><input type="radio" name="optionMenu" value="'+data.portfolioMenus[i].menuId+'" /> '+data.portfolioMenus[i].name+' ['+portfolioMenutype+'] [Status : '+data.portfolioMenus[i].status+']</span></a></li>');
            };
        }
    }
    function fetchPortfolioPages() {
        var req = {};
        req.slug = trim($('#slug').val());
        req.menu = 1;
        var res;
        req.action = "get-portfolio-pages";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            portfolioPages = res.portfolioPages;
            fillPortfolioPages(portfolioPages);
        });
    }
    function fillPortfolioPages(portfolioPages) {
        $('#selectPortfolioPage').html('');
        // General Details
        if(portfolioPages.length>0) {
            //$('.js-portfolio-pages').html('<ul class="nav nav-pills nav-stacked"></ul>');
            for (var i = 0; i < portfolioPages.length; i++) {
                $('#selectPortfolioPage').append('<option value="'+portfolioPages[i].pageId+'"> '+portfolioPages[i].title+' [Type : '+portfolioPages[i].type+']</option>');
            };
        }
    }
    $('#btnSavePortfolioMenu').click(function(){
        var menuId                  = trim($('#menuId').val());
        var selectMenuType      = trim($('#selectMenuType').val());
        var inputMenuName          = trim($('#inputMenuName').val());
        var inputMenuDescription            = trim($('#inputMenuDescription').val());
        var inputMenuOrder   = trim($('#inputMenuOrder').val());
        var selectMenuStatus      = trim($('#selectMenuStatus').val());
        var selectPortfolioPage     = trim($('#selectPortfolioPage').val());
        var inputMenuUrl        = trim($('#inputMenuUrl').val());
        if(!selectMenuType) {
            $('#frmPortfolioMenus .help-block').html('<p class="text-danger text-center">Please select Menu Type</p>');
        } else if(!inputMenuName) {
            $('#frmPortfolioMenus .help-block').html('<p class="text-danger text-center">Menu Name can\'t be left empty</p>');
        } else if(!inputMenuDescription) {
            $('#frmPortfolioMenus .help-block').html('<p class="text-danger text-center">Menu Description can\'t be left empty</p>');
        } else if(!inputMenuOrder) {
            $('#frmPortfolioMenus .help-block').html('<p class="text-danger text-center">Menu Order can\'t be left empty</p>');
        } else if(!selectMenuStatus) {
            $('#frmPortfolioMenus .help-block').html('<p class="text-danger text-center">Menu Status can\'t be left empty</p>');
        } else if(selectMenuType == 'page' && !selectPortfolioPage) {
            $('#frmPortfolioMenus .help-block').html('<p class="text-danger text-center">Please select a page</p>');
        } else if(selectMenuType == 'url' && !inputMenuUrl) {
            $('#frmPortfolioMenus .help-block').html('<p class="text-danger text-center">Custom URL field can\'t be left empty</p>');
        } else {
            var req       = {};
            req.slug      = $('#slug').val();
            req.menuId    = menuId;
            req.menuType  = selectMenuType;
            req.menuName  = inputMenuName;
            req.menuDescription = inputMenuDescription;
            req.menuOrder  = inputMenuOrder;
            req.menuStatus = selectMenuStatus;
            switch(selectMenuType) {
                case 'page' :  req.selectPortfolioPage = selectPortfolioPage;break;
                case 'url' :  req.inputMenuUrl = inputMenuUrl;break;
            }
            var res;
            req.action = "save-portfolio-menu";
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if(res.status == 1) {
                    portfolioMenus = res.portfolioMenus;
                    $('.js-portfolio-menus').html('<ul class="nav nav-pills nav-stacked list-pf-menu">'+
                                            '<li><a href="javascript:void" class="js-menu selected"><span class="checkbox"><input type="radio" name="optionMenu" value="new" checked /> New</span></a></li>'+
                                        '</ul>');
                    fillPortfolioMenus(res);
                    menuProcess(menuId);
                    $('#frmPortfolioMenus .help-block').html('<p class="text-success text-center">Menus Updated!</p>');
                } else {
                    $('#frmPortfolioMenus .help-block').html('<p class="text-danger text-center">'+res.message+'</p>');
                }
                console.log(res);
            });
        }
        return false;
    });
});