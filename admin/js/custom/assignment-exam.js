$(function() {
	$('a.exams').addClass('active');
	//fetchProfileDetails();
	fetchCourses();
	$('#currentExams').sieve();
	
	$('#courseSelect').on('change', function() {
		$('#tableContainer').hide();
		$('#seeResults').hide();
		$('#addExam').attr('disabled', true);
		if($(this).val() == 0) {
			$("#subjectSelect").attr('disabled', true);
			$('#addSubject').show();
			$('#subjectSelect').find('option').remove()
			$('#subjectSelect').append('<option value="0">Select Subject</option>');
			$('#addSubject').hide();
			$('#warning').html('No Course found in your profile. Please <a href="add-course.php">Add Course</a> to continue with the Assignment Creation.');
			$('#warning').show();
		}
		else {
			$('#addSubject').show();
			$('#addSubject').attr('href', 'add-subject.php?courseId='+$(this).val());
			fetchSubjects($(this).val());
		}
	});
	
	$('#subjectSelect').on('change', function() {
		$('#warning').show();
		$('#currentItem').hide();
		if($(this).val() == 0) {
			$('#tableContainer').hide();
			$('#seeResults').hide();
			$('#addExam').attr('disabled', true);
		}
		else {
			$('#addExam').parent().attr('href', 'add-assignment.php?courseId='+$('#courseSelect').val()+'&subjectId='+$('#subjectSelect').val());
			$('#seeResults').attr('href', 'subjectResult.php?courseId='+$('#courseSelect').val()+'&subjectId='+$('#subjectSelect').val()).show();
			$('#addExam').attr('disabled', false);
			fetchExams($(this).val());
		}
	});
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src',data.profileDetails.profilePic);
		}
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fetchCourses() {
		var req = {};
		var res;
		req.action = 'get-courses-for-exam';
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0) {
				alert(res.message);
			}
			else
				fillCourseSelect(res);
		});
	}
	
	function fillCourseSelect(data) {
		$('#courseSelect').attr('disabled',false);
		for(i=0; i<data.courses.length; i++) {
			$('#courseSelect').append('<option value="'+data.courses[i]['id']+'">'+data.courses[i]['name']+'</option>');
		}
		if(data.courses.length > 0)
			$('#warning').html("Please select a course to continue");
		else
			$('#warning').html('No Course found in your profile. Please <a href="add-course.php">Add Course</a> to continue with the Assignment Creation.');
	}
	
	function fetchSubjects(courseId) {
		var req = {};
		var res;
		req.action = 'get-subjects-for-exam';
		req.courseId = courseId;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0) {
				alert(res.message);
			}
			else
				fillSubjectSelect(res);
		});
	}
	
	function fillSubjectSelect(data) {
		if(data.subjects.length > 0)
			$('#warning').html("Please select a subject to continue.");
		else
			$('#warning').html("You do not have any subjects in this coarse. Please add a <a href='" + $('#addSubject').attr('href') + "'>new subject</a> to continue.");
		$('#subjectSelect').find('option').remove()
		$('#subjectSelect').append('<option value="0">Select Subject</option>');
		$("#subjectSelect").attr('disabled', false);
		for(i=0; i<data.subjects.length; i++) {
			$('#subjectSelect').append('<option value="'+data.subjects[i]['id']+'">'+data.subjects[i]['name']+'</option>');
		}
	}
	
	function fetchExams(subjectId) {
		var req = {};
		var res;
		req.action = 'get-exam-details';
		req.subjectId = subjectId;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0) {
				$('#tableContainer').hide();
				$('#warning').html("You have not created any Assignment/Exam yet.");
				$('#currentExams').find('tbody tr').remove();
				$('#warning').show();
			}
			else
				fillExams(res);
		});
	}
	
	function fillExams(data) {
		$('#warning').hide();
		$('#tableContainer').show();
		$('#currentExams').find('tbody tr').remove();
		var html = "";
		var resultFlag = false;
		for(i=0; i<data.exams.length; i++) {
			var chapter;
			var status;
			var disableState = 'disabled';
			if(data.exams[i].attemptCount > 0) {
				resultFlag = true;
				disableState = '';
			}
			if(data.exams[i]['chapterId'] == 0)
				chapter = "Independent";
			else
				chapter = data.exams[i]['chapterName'];
			if(data.exams[i]['status'] == 0)
				status = "Draft";
			else if(data.exams[i]['status'] == 1)
				status = "Not Live";
			else if(data.exams[i]['status'] == 2)
				status = "Live";
			if(data.exams[i]['delete'] == 0) {
				html += '<tr data-id="' + data.exams[i]['id'] + '">'
						+ '<td width="150">'+data.exams[i]['name']+'</td>'
						+ '<td>'+data.exams[i]['type']+'</td>'
						+ '<td>'+chapter+'</td>'
						+ '<td>'+status+'</td>';
                                var startDate = new Date(parseInt(data.exams[i]['startDate']));
				var endDate = new Date(parseInt(data.exams[i]['endDate']));
				var start = startDate.getDate()+ ' ' +MONTH[startDate.getMonth()]+ ' ' + startDate.getFullYear();
				if(data.exams[i]['endDate'] == '')
					var end = '--';
				else
					var end = endDate.getDate() + ' ' + MONTH[endDate.getMonth()] + ' ' + endDate.getFullYear();
				html += '<td>'+start+'</td>'
						+ '<td>'+end+'</td>'
						+ '<td>'+data.exams[i]['attempts']+'</td>'
						+ '<td>'
							+ '<a href="add-assignment-2.php?examId='+data.exams[i]['id']+'" class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-placement="top" type="button" data-original-title="Edit step 2"><i class="fa fa-edit"></i> Edit Questions</a>'
							+ '<a href="edit-assignment.php?examId='+data.exams[i]['id']+'" type="button" data-placement="top" data-toggle="tooltip" class="btn btn-success btn-xs tooltips" data-original-title="Edit step 1"><i class="fa fa-cog"></i> Settings</a>'
							+ '<a type="button" data-placement="top" data-toggle="tooltip" class="btn btn-info btn-xs tooltips" data-original-title="View Results" href="examResult.php?examId=' + data.exams[i].id +'" ' + disableState + '><i class="fa fa-bullhorn"></i> Result</a>'
							+ '<a class="btn btn-info btn-xs tooltips copy-exam" data-toggle="tooltip" data-placement="top" type="button" data-original-title="Copy"><i class="fa fa-copy"></i> Copy</a>'
							/*+ '<a class="btn btn-success btn-xs tooltips" data-toggle="tooltip" data-placement="top" type="button" data-original-title="Information"><i class="fa fa-info"></i></a>'*/
							+ '<a type="button" data-placement="top" data-toggle="tooltip" class="btn btn-danger btn-xs tooltips delete-button" data-original-title="Delete"><i class="fa fa-trash-o"></i> Delete</a>'
						+ '</td>'
					+ '</tr>';
			}
			/*else {
				html += '<tr data-id="' + data.exams[i]['id'] + '" class="deleted-item">'
						+ '<td width="150">'+data.exams[i]['name']+'</td>'
						+ '<td>'+data.exams[i]['type']+'</td>'
						+ '<td>'+chapter+'</td>'
						+ '<td>Deleted</td>'
						+ '<td>'+data.exams[i]['startDate']+'</td>'
						+ '<td>'+data.exams[i]['endDate']+'</td>'
						+ '<td>'+data.exams[i]['attempts']+'</td>'
						+ '<td>'
							+ '<a href="edit-assignment.php?examId='+data.exams[i]['id']+'" type="button" data-placement="top" data-toggle="tooltip" class="btn btn-success btn-xs tooltips" data-original-title="Edit step 1"><i class="fa fa-cog"></i></a>'
							+ '<a href="add-assignment-2.php?examId='+data.exams[i]['id']+'" class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-placement="top" type="button" data-original-title="Edit step 2"><i class="fa fa-edit"></i></a>'
							+ '<a  class="btn btn-info btn-xs tooltips copy-exam" data-toggle="tooltip" data-placement="top" type="button" data-original-title="Copy"><i class="fa fa-copy"></i></a>'
							+ '<a class="btn btn-success btn-xs tooltips" data-toggle="tooltip" data-placement="top" type="button" data-original-title="Information"><i class="fa fa-info"></i></a>'
							+ '<a type="button" data-placement="top" data-toggle="tooltip" class="btn btn-success btn-xs tooltips restore-button" data-original-title="Restore"><i class="fa fa-refresh"></i></a>'
						+ '</td>'
				+ '</tr>';
			}*/
		}
		$('#currentExams').append(html);
		if(resultFlag)
			$('#seeResults').attr('disabled', false);
		else
			$('#seeResults').attr('disabled', true);
		$('.tooltips').tooltip();
		$('.delete-button').on('click', function() {
			var con = confirm("Are you sure you want to delete this Exam/Assignment.");
			if(con) {
				var req = {};
				req.action = 'delete-exam';
				req.examId = $(this).parents('tr').attr('data-id');
				$.ajax({
					'type'	: 'post',
					'url'	: ApiEndpoint,
					'data'	: JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 0)
						alert(res.message);
					else {
						fetchExams($('#subjectSelect').val());
					}
				});
			}
		});
                var examname='';
                var iexamId='';
                var examType='';
                 $('.copy-exam').on('click', function() {
                     var tr=$(this).parents('tr');
                    iexamId = tr.attr('data-id');
                     examname = 'Copy of ' + tr.find('td:eq(0)').text();
                     $('#new-exam').val(examname);
                    $('#modal-copy-exam').modal('show');
                 });
                 $('#copy-exam').off('click');
                $('#copy-exam').on('click', function() {
                    $(this).attr('disabled',true);
                        var newExamname = $('#new-exam').val();
			if(newExamname!=null) {
				var req = {};
				req.action = 'copy-exam';
				req.iexamId =iexamId;
                                req.iname=newExamname;
                               	$.ajax({
					'type'	: 'post',
					'url'	: ApiEndpoint,
					'data'	: JSON.stringify(req)
				}).done(function(res) {
                                     $('#copy-exam').attr('disabled',false);
					res = $.parseJSON(res);
					if(res.status == 0)
						alert(res.message);
					else {
						fetchExams($('#subjectSelect').val());
                                               $('#modal-copy-exam').modal('hide');
					}
				});
			}
		});
		
		$('.restore-button').on('click', function() {
			var req = {};
			req.action = 'restore-exam';
			req.examId = $(this).parents('tr').attr('data-id');
			$.ajax({
				'type'	: 'post',
				'url'	: ApiEndpoint,
				'data'	: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else
					fetchExams($('#subjectSelect').val());
			});
		});
	}
});