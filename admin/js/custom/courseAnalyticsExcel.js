var ApiEndpoint = '../api/index.php';
var month = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

$(function () {
	var imageRoot = '/';
	$('#loader_outer').hide();
	var fileApiEndpoint = '../api/files1.php';
	//downloadExcel
	$('#downloadExcel').on('click', function() {		
		$('#downloadcustom').modal('show');
		$('#downloadCustom-list').on('click', function () {
			var avg=1;
			var highest=0;
			var attempts=0;
			var lastscore=0;
			if($('#cbox1').is(':checked'))
			{
				avg=1;
			}			
			else{
				avg=0;
			}
			if($('#cbox2').is(':checked'))
			{
				highest=1;
			}			
			if($('#cbox3').is(':checked'))
			{
				attempts=1;
			}			
			if($('#cbox4').is(':checked'))
			{
				lastscore=1;
			}			
				
			var req = {};
			var res;
			req.action = 'get-student-list-download';
			if($("#exam-id").val()!='' || $("#exam-id").val()!=null)
				req.examId = $("#exam-id").val();
			req.avg=avg;
			req.highest=highest;
			req.attempts=attempts;
			req.lastscore=lastscore;
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndpoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				console.log(res);
				if(res.status == 1){
					var url = res.path;
					window.location.href = url;
					$('#downloadcustom').modal('hide');
					//window.open(url, '_blank');
				}				
				else {
					if(res.students.length==0)
					{
						alertMsg("There are no students in the course");
					}else{
						alertMsg(res.message);	
					}
				
				}
			});
			
		});
		
	});
	

});
