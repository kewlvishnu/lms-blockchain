var ApiEndpoint = '../api/index.php';
var courses = [];
var current = 0;
var totalLength = 0;
var courseId=0;
var initdt=0;

$(function() {
	
	$('#next').on('click', function() {
		splitCourseto10(current + 1);
		current++;
	});
	$('#previous').on('click', function() {
		splitCourseto10(current - 1);
		current--;
	});
	//datatablecl
	
	$('#getsummary').on( "click", function() {
		if(courseId==0)
		{
			alert("please select course");
		}else{
	          $('#detailsummary').modal('show');
			var html = "";
			var req = {};
			var res;
			req.action = 'get-detail-recent-course-view';
			req.courseId = courseId;
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndpoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
			//console.log(res);
				res = $.parseJSON(res);
				if(res.status == 1)
				{
					$.each(res.graph, function(i, d) {
					html += "<tr data-cid='" + i + "'" + "" + ">"
					+"<td>" + d.student + "</td>"
					+ "<td>" + d.visdate + "</td>";
				});
				$('#all-visitedStudent tbody').html('').html(html);
				
				$('#all-visitedStudent').show();   
				if(initdt ==0){
				 	$('#all-visitedStudent').dataTable( {
						"aaSorting": [[ 4, "desc" ]]
						
					});  
					initdt=1;
				 }
				
				}
				else{
				alertMsg("please try after some time");
					
				}
				
		
			})
				}

              });
	//fetchProfileDetails();
	fetchCounters();
	fetchGraphs();
	$('a.dashboard').addClass('active');
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	//function to fetch counters
	function fetchCounters() {
		var req = {};
		var res;
		req.portfolioSlug = $('#slug').val();
		req.pageUserId = $('#inputUserId').val();
		req.pageUserRole = $('#inputUserRole').val();
		req.action = 'get-institute-counters';
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
				fillCounters(res);
		});
	}
	
	//to fill the couter values
	function fillCounters(data) {
		countUp($('.courseCount'), data.courseCount);
		countUp($('.studentCount'), data.studentCount);
		countUp($('.professorCount'), data.professorCount);
		if(data.userRole == 2) {
			$('.profOnly').show();
			countUp($('.assignedSubjectsCount'), data.assignedSubjects);
			countUp($('.assignedSubjectsStudent'), data.assignedSubjectsStudent);
		}
	}
	
	//function to fetch graph data
	function fetchGraphs() {
		var req = {};
		var res;
        req.portfolioSlug = $('#slug').val();
        req.pageUserId = $('#inputUserId').val();
        req.pageUserRole = $('#inputUserRole').val();
		req.action = 'get-institute-graphs';
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alertMsg(res.message);
			else {
				var html = '';
				var flag = false;
				$.each(res.graph1, function(i, d) {
					courses.push(d);
					html += '<option value="' + d.courseId + '">' + d.name + '</option>';
					if(d.students != 0)
						flag = true;
					//cutting the course names
					if(d.name.length > 25) {
						var first = d.name.substring(0, 9);
						var second = d.name.substring(d.name.length - 8);
						res.graph1[i].name = first + '...' + second;
					}
				});
				$('#courseSelector').append(html);
				totalLength = Math.floor(res.graph1.length / 10);
				if(flag)
					splitCourseto10(0);
				else {
					$('#hero').append('<p>This graph will only be visible when students are enrolled in any of the courses</p>');
				}
			}
		});
	}
	
	function splitCourseto10(number) {
		var start = number * 10;
		var temp = courses.slice(start, start + 10);
		fillGraphs(temp);
		if(number == 0 && totalLength == 0) {
			$('#next').hide();
			$('#previous').hide();
		}
		else if(number == 0 && totalLength > 0) {
			$('#next').show();
			$('#previous').hide();
		}
		else if(number == totalLength) {
			$('#next').hide();
			$('#previous').show();
		}
		else {
			$('#next').show();
			$('#previous').show();
		}
	}
	
	$('#courseSelector').on('change', function() {
		if($(this).val() != 0) {
			var req = {};
			var res;
			req.action = 'get-recent-course-view';
			req.courseId = $(this).val();
			courseId=$(this).val();
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndpoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else
					fillCourseViewGraph(res);
			});
		}
	});
	
	function fillCourseViewGraph(data) {
		//console.log(data);
		var filler = [];
		var cats = [];
		for(var i = data.graph.length - 1; i >= 0; i--) {
			cats.push(data.graph[i].date);
			filler.push(parseInt(data.graph[i].opened));
		}
		$('#sparkline').highcharts({
			title: {
				text: ''
			},
			xAxis: {
				categories: cats
			},
			yAxis: {
				title: {
					text: '<b style="font-size: 1.5em;font-weight: bold;color: rgb(14, 113, 27);">Unique views (No of students)</b>'
				},
				plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
				}]
			},
			tooltip: {
				pointFormat: '<b>{point.y}</b>student views'
			},
			series: [{
				name: '<b style="font-size: 1.5em;font-weight: bold;color: rgb(14, 113, 27);">Recent views (in last 7 days)</b>',
				data: filler
			}]
		});
	}
	
	//function to fill the graphs
	function fillGraphs(data) {
		var graphdata = [];
		for(var i = 0; i < data.length; i++) {
			var obj = [];
			obj.push('<b>' + data[i].name + '</b>');
			obj.push(parseInt(data[i].students));
			graphdata.push(obj);
		}
		
		$('#hero').highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: ''
			},
			xAxis: {
				type: 'category',
				labels: {
					rotation: -60,
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				},
				title: {
					text: '<b style="font-size: 1.5em;font-weight: bold;color: rgb(14, 113, 27);">Course names</b>'
				}
			},
			yAxis: {
				title: {
					text: '<b style="font-size: 1.5em;font-weight: bold;color: rgb(14, 113, 27);">No of students</b>'
				}
			},
			legend: {
				enabled: false
			},
			tooltip: {
				pointFormat: 'has <b>{point.y}</b> students'
			},
			series: [{
                   name: 'Attempts',
                   data: graphdata,
				colorByPoint: true,
				dataLabels: {
					enabled: true,
					color: '#000',
					align: 'center',
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif',
					}
				}
			}]
		});
	}
	
	//function to count numbers
	function countUp(element, count) {
		var div_by = 100,
			speed = Math.round(count / div_by),
			$display = element,
			run_count = 1,
			int_speed = 170;
	
		var int = setInterval(function() {
			/*if(run_count < div_by){
				$display.text(speed * run_count);
				run_count++;
			} else*/ if(parseInt($display.text()) < count) {
				var curr_count = parseInt($display.text()) + 1;
				$display.text(curr_count);
			} else {
				clearInterval(int);
			}
		}, int_speed);
	}

});