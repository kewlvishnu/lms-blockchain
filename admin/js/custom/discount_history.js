/*
	Author: Ayush Pandey.
	Dated: july, 23/2015
	
	
	********
	1) Notes: JSON.stringify support for IE < 10
*/
var userRole;
var reorderTimer = null;
 var couponsdata= [];
 var couponsdatai="";
 var cid="";
 var k=0;
$(function () {
	var imageRoot = '/';
	$('#loader_outer').hide();
	
	
	function fetchCoupons() {
	var req = {};
		var res;
		req.action = "get-all-coupons";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			console.log(res);
			fillCoupons(res);
		});
	}
	
	function fillCoupons(data) {		
		var html = "";
		var flag = false;
		//var editCourseLink,editSubjectLink,addSubjectLink, deletedClass;
		deletedClass = "";
		var coursesno=data.courses.length;
		if(coursesno > 0) {
			$.each(data.courses, function(i, course) {
				//console.log(course);
				courseName=course.name;
				price=course.studentPrice;
				priceINR=course.studentPriceINR;
				
				courseId = "C" + String("000" + course.id).slice(-4);
				flag = true;
				editCourseLink = "edit-course.php?courseId=" + course.id;
				//addSubjectLink = "add-subject.php?courseId=" + course.id;
				
				deletedClass = "";
				if(course.deleted == 1) {
					deletedClass = " class='deleted-item'";
				}
			$.each(course.Coupons, function(j, coupons) {
				coupons['studentPrice']=price;
				coupons['studentPriceINR']=priceINR;
				var couponCode=coupons.couponCode;
				var noOfCoupons=coupons.noOfCoupons;
				var couponsUsed=coupons.couponsUsed;
				var startDate=coupons.startDate;
				var endDate=coupons.endDate;
				var couponId=coupons.couponId;
				couponsdata[k]=coupons;
				console.log(coupons);
				console.log(k);
				
				//DiscountPer=

				html += "<tr data-cid='" + k + "'" + deletedClass + ">"
					+ "<td>" + courseId + "</td>"
					+ "<td><h5><a href='" + editCourseLink + "'>" + courseName + "</a></h5></td>"
					+ "<td ><p class=coupon>" + couponCode + "</p></td><td><span id = 'discount"+j+"'>";
					console.log(couponCode);
							var discount=parseFloat(coupons.DiscountPer);
							var temp = discount.toFixed(2) +" %";

							//temp=temp.substring(0,temp.length-2);
							html += temp;
					

	
						var varpriceAfterdiscount=price *((100-discount)/100);
						var varpriceAfterdiscountINR=priceINR *((100-discount)/100);	
						varpriceAfterdiscount=parseFloat(varpriceAfterdiscount).toFixed(2);
						varpriceAfterdiscountINR=parseFloat(varpriceAfterdiscountINR).toFixed(2);

				html += "</span></td>"
						+ "<td ><i class='fa fa-dollar'> </i> <span id = 'varpriceAfterdiscount"+k+"'>" + varpriceAfterdiscount+ "</span></td>"
						+ "<td>"+""+"<i class='fa fa-rupee'> </i><span id = 'varpriceAfterdiscountINR"+k+"'> "+ ""+ varpriceAfterdiscountINR+ "</span></td>";
						
						//var noOfCoupons=noOfCoupons;

						var	startDate = new Date(parseInt(startDate));
						var displayStartDate=startDate.getDate()+ ' ' +MONTH[startDate.getMonth()]+ ' ' + startDate.getFullYear();
						var displayendDate='';
							if(endDate!=''){
								var endate=new Date(parseInt(endDate));
									displayendDate=endate.getDate()+ ' ' +MONTH[endate.getMonth()]+ ' ' + endate.getFullYear();    
									}else{
										displayendDate='--';
									}
					html += 
						 "<td>" + couponsUsed+ " /<span id = 'noOfCoupons"+k+"'> "+noOfCoupons +"</span></td>"
						+ "<td><span id = 'displayStartDate"+k+"'>" + displayStartDate+ "</span></td>"
						+ "<td><span id = 'displayendDate"+k+"'>" + displayendDate+ "</span></td>";

					html +=	"<td>"+""+"<button class='btn btn-success btn-xs tooltips coupons-edit' title='' data-placement='top'  type='button' data-original-title='Edit Coupon'><i class='fa fa-edit'></i> Edit Coupon</button>&nbsp;"+""+"</td>";
				 
						//+ "<td>Life Time</td>";
						//
						//+ "<button class='btn btn-success btn-xs tooltips' title='' data-placement='top' data-toggle='tooltip' type='button' data-original-title='Content is eligible to license (sell) to Professors/Institutes/Publishers'><i class='fa fa-check'></i></button>&nbsp;"
						//+ "<a href='#view_details' data-toggle='modal'><button class='btn btn-primary btn-xs tooltips' title='' data-placement='top' data-toggle='tooltip' type='button' data-original-title='View details   about the Course and the Subject Licenses'><i class='fa fa-eye'></i></button></a>&nbsp;";
				k=k+1;
			});
});




		}
		
		if(flag == true) {
			$('#all-courses tbody').html('').html(html);
			$('.tooltips').tooltip();
			addEventHandlers();
			makeCoursesSortable('#all-courses tbody');
		}
		else {
			$('#all-courses').hide();
			$('.panel-heading').after('<span style="margin: 1%;">No courses found. Please create one course <a href="add-course.php">here</a>.</span>');
		}
	}
	
	function makeCoursesSortable(selector) {
		$(selector).sortable({
			items: '> tr',
			forcePlaceholderSize: true,
			placeholder:'sort-placeholder',
			start: function (event, ui) {
				// Build a placeholder cell that spans all the cells in the row
				var cellCount = 0;
				$('td, th', ui.helper).each(function () {
					// For each TD or TH try and get it's colspan attribute, and add that or 1 to the total
					var colspan = 1;
					var colspanAttr = $(this).attr('colspan');
					if (colspanAttr > 1) {
						colspan = colspanAttr;
					}
					cellCount += colspan;
				});

				// Add the placeholder UI - note that this is the item's content, so TD rather than TR
				ui.placeholder.html('<td colspan="' + cellCount + '">&nbsp;</td>');
				//$(this).attr('data-previndex', ui.item.index());
			},
			update: function(event, ui) {
				// gets the new and old index then removes the temporary attribute
				var newOrder = $.map($(this).find('tr'), function(el) {
					return $(el).attr('data-cid')+ "-" +$(el).index();
				});
				if(reorderTimer != null)
					clearTimeout(reorderTimer);
				reorderTimer = setTimeout(function(){updateCourseOrder(newOrder);}, 5000);
			},
			helper: function(e, ui) {
				ui.children().each(function() {
					$(this).width($(this).width());
				});
				return ui;
			}
			
		}).disableSelection();
	}
	
	function updateCourseOrder(newOrder) {
		var order = {};
		$.each(newOrder, function(i, v){
			order[v.split('-')[0]] = v.split('-')[1];
		});
		var req = {};
		var res;
		req.newOrder = order;
		req.action = "update-course-order";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1)
				fetchCourses();
			else
				alertMsg(res.message);
		});
	}
	
	//fetchProfileDetails();
	fetchCoupons();

	
	
});

function addEventHandlers() {
	$('#all-courses .delete-course').on('click', function(e) {
		e.preventDefault();
		var con = confirm("Are you sure to delete this course.");
		if(con) {
			var course = $(this).parents('tr');
			var cid = course.attr('data-cid');
			var req = {};
			req.action = "delete-course";
			req.courseId = cid;
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 1) {
					course.remove();
					alertMsg("Course Deleted. Please contact admin to restore this course.");
				} else {
					alertMsg(res.message);
				}
			});
		}
	});
	
	/*$('#all-courses .restore-course').on('click', function(e) {
		e.preventDefault();
		var course = $(this).parents('tr');
		var cid = course.attr('data-cid');
		var req = {};
		req.action = "restore-course";
		req.courseId = cid;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				course.find('.restore-course').hide();
				course.find('.delete-course').show();
				course.removeClass('deleted-item');
			} else {
				alertMsg(res.message);
			}
		});
	});*/

	$('.coupons-edit').on( "click", function() {
		

		var coupons = $(this).parents('tr');
		 cid = coupons.attr('data-cid');
		couponsdatai=couponsdata[cid];
		console.log(couponsdata);
		console.log(cid);
		$("#originalPriceINR").text(couponsdata[cid].studentPriceINR);
        $("#originalPrice").html(couponsdata[cid].studentPrice);
        $("#couponCode").html(couponsdata[cid].couponCode);
        

		$("#editcoupons").trigger('reset');
        $('#editcoupons').modal('show');
	});


	$('.editCouponbtn').on('click', function() {
            var req="";
            var resp="";
            var newprice = 0;
            var newpriceINR =0;
            var originalPrice =couponsdatai.studentPrice;;
            var originalPriceINR = couponsdatai.studentPriceINR;
            var couponcode=couponsdatai.couponCode;
            var noOfcoupons=$('.noofCouponsC').val();
            var discount = $('.discount11').val();
            var couponEndDate = new Date($('.couponEndDate1').val());
            var couponStartDate= new Date($('.couponStartDate1').val());   
            var error = "";
            $('.help-block').remove();
            $('.has-error').removeClass('has-error');
           
            if (discount == '') {
               $(this).parent().addClass('has-error').append('<span class="help-block">Please enter Discount </span>');
                 return false;
            }

            if ($('.couponStartDate1').val() == '') {
                 unsetError($('.couponStartDate1'));
               $('.couponStartDate').parent().addClass('has-error').append('<span class="help-block">Please enter Start Date </span>');
                return false;
            }
            else if($('.couponEndDate1').val() == ''){
                   unsetError($('.couponEndDate1'));
                  $('.couponEndDate1').parent().addClass('has-error').append('<span class="help-block">Please enter End Date </span>');
                return false;
            }
            else{
                 var couponStartDate=couponStartDate.getTime();
                 var couponEndDate = couponEndDate.getTime();
                 if(couponStartDate>couponEndDate)
                 {
                    unsetError($('.couponEndDate1'));
                  $('.couponEndDate1').parent().addClass('has-error').append('<span class="help-block">End Date should be more than Start Date  </span>');
                return false;
                 }
            }         

                  if(discount > 100 || discount < 0 || !$.isNumeric(discount))
                    {
                            $('.discount11').val(''); 
                            $('.discount11').parent().addClass('has-error').append('<span class="help-block">Please Enter value between 0 and 100 </span>');
                            return false;
                    }
                    else{
                           newpriceINR =(originalPriceINR * ((100 - discount) / 100));
                           newprice = (originalPrice * ((100 - discount) / 100));
                           newpriceINR=parseFloat(newpriceINR).toFixed(2);
                            newprice=parseFloat(newprice).toFixed(2);

                        }
         
                   var today= new Date().getTime();

                var req = {
                        'courseId' : couponsdatai.courseId,
                        'couponCode': couponcode,
                        'noOfCoupons': noOfcoupons ,
						'CouponCodeType': '2',
                        'DiscountPer' : discount,
                        'DiscountPerINR' : discount,
                        'startDate': couponStartDate,
                        'endDate':  couponEndDate,  
                        'today' : today            
                          };
                var res;
                //console.log(req);
                req.action = 'edit-courseCoupons';
                $.ajax({
                    'type'  : 'post',
                    'url'   : ApiEndpoint,
                    'data'  : JSON.stringify(req)
                }).done(function (res) {
                    res =  $.parseJSON(res);
                    if(res.status == 1){
                         alertMsg(res.message);
                        $('#editcoupons').modal('hide');
                    // location.reload();

                        var disreq={
                                        'courseId' : couponsdatai.courseId,
                                        'couponCode': couponcode
                                    };
                        var disres=null;
                        disreq.action = 'changeupdateCoupon';
                         $.ajax({
                                    'type'  : 'post',
                                    'url'   : ApiEndpoint,
                                    'data'  : JSON.stringify(disreq)
                                 }).done(function (disres){
                                  disres =  $.parseJSON(disres);
                                 
                            if(disres.endDate>new Date().getTime()){
                            		

                                                    var newdis= disres.DiscountPer;
                                                    var newdisINR=disres.DiscountPerINR;
                                                    var newprice=originalPrice*((100-newdis)/100).toFixed(2);
                                                    var newpriceINR=originalPriceINR*((100-newdisINR)/100).toFixed(2);
                                                    var dispDis= Math.round(newdis);
                                                    var noOfCoupons=disres.noOfCoupons;
                                                    var StartDate=disres.startDate;
                                                    var endDate=disres.endDate;
                                                    newprice=parseFloat(newprice).toFixed(2);
													newdisINR=parseFloat(newdisINR).toFixed(2);
													newdis=parseFloat(newdis).toFixed(2);
                                                     var	startDate = new Date(parseInt(StartDate));
						var displayStartDate=startDate.getDate()+ ' ' +MONTH[startDate.getMonth()]+ ' ' + startDate.getFullYear();
						var displayendDate='';
							if(endDate!=''){
								var endate=new Date(parseInt(endDate));
									displayendDate=endate.getDate()+ ' ' +MONTH[endate.getMonth()]+ ' ' + endate.getFullYear();    
									}else{
										displayendDate='--';
									}
                        // chnaging values of element using ajax call in parent
                        //displayendDate displayStartDate noOfCoupons  
                        						$("#discount"+cid).html(newdis);
                                                 $("#varpriceAfterdiscount"+cid).html(newprice);
                                                 $("#varpriceAfterdiscountINR"+cid).html(newpriceINR);
                                                 $("#noOfCoupons"+cid).html(noOfCoupons);
                                                 $("#displayStartDate"+cid).html(displayStartDate);
                                                 $("#displayendDate"+cid).html(displayendDate);
                                                
                                                // $("#curPrice"+idParent).html(newprice);
                                                 //$("#curPriceINR"+idParent).html(newpriceINR);
                                                 //$("#discount"+idParent).text("Edit Discount");
                         //curPriceINR

                       }
                });




                     
                  }
                    else
                        alertMsg(res.message);
                });
          
        });
       


        //$("#originalPriceINR").text(coursedata[0].studentPriceINR);
        //$("#originalPrice").html(coursedata[0].studentPrice);

		$('.noofCouponsC').keyup(function () {
         unsetError($('.noofCouponsC'));

             var noofCouponsC = $('.noofCouponsC').val();
            var error = "";
                           
                    if(!$.isNumeric(noofCouponsC))
                    {  
                     
                            $('.noofCouponsC').val('');                     
                             $(this).parent().addClass('has-error').append('<span class="help-block">Please Enter Numeric Value </span>');                    
                            return false;
                    }
           
               
        });

        $('.discount11').keyup(function () {
            unsetError($('.discount11'));
            var newprice = 0;
            var newpriceINR =0; 
            var originalPrice = couponsdatai.studentPrice;
            var originalPriceINR = couponsdatai.studentPriceINR;
            var discount = $('.discount11').val();
            var error = "";
                           
                    if(discount > 100 || discount < 0 || !$.isNumeric(discount))
                    {  
                     
                            $('.discount11').val('');                     
                             $(this).parent().addClass('has-error').append('<span class="help-block">Please Enter value between 0 and 100 </span>');                    
                            return false;
                    }
                    else{
                            newpriceINR = originalPriceINR * ((100 - discount) / 100);
                            newprice = originalPrice * ((100 - discount) / 100);
                            newpriceINR=parseFloat(newpriceINR).toFixed(2);
                            newprice=parseFloat(newprice).toFixed(2);
                       }
         


                      
                $('#price_After_DiscountINR').html('<span class="priceDisp"> <i class="fa fa-rupee"></i> '+newpriceINR+'  </span> ');
                $('#price_After_Disocount').html(' <span class="priceDisp">  <i class="fa fa-dollar"></i> '+newprice+' </span>');
               
        });






        $('#couponStartDate , #couponEndDate').datetimepicker({
                format: 'd F Y H:i',
                timepicker: true,
                closeOnDateSelect: true,
                minDate: 0,
                maxDate: '2050/12/31',
                onSelectDate: function(date) {
                    unsetError($('#couponStartDate, #couponEndDate'));         
                            
                }
            });
			
			
        $('.couponEndDate1 , .couponStartDate1').keyup(function () {
            unsetError($('.couponStartDate1'));
             unsetError($('.couponEndDate1'));
            if($('.couponStartDate1').val()=="")
            {
              $(this).parent().addClass('has-error').append('<span class="help-block">Please Enter Start Date123 </span>');                    
                            return false;
            }
            if($('.couponEndDate1').val()=="")
            {
              $(this).parent().addClass('has-error').append('<span class="help-block">Please Enter End Date123 </span>');                    
                            return false;
            }
        });
		
		$('#couponStartDate , #couponEndDate').on('keypress', function() {
                return false;
        });
       
      
       





      
	$('#all-courses .request-approval-link').on('click', function(e) {
		e.preventDefault();
		var req = {};
		var course = $(this).parents('tr');
		var cid = course.attr('data-cid');
		req.courseId = cid;
		req.action = "request-course-approval";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				alertMsg(res.message);
				$('.request-approval-link').unbind('click').find('span').removeClass('btn-success').removeClass('btn-danger').addClass('btn-warning')
					.html('Pending');
				
			} else {
				alertMsg(res.message);
			}
		});
	});
}