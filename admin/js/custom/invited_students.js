var ApiEndpoint = '../api/index.php';

$(function () {
    $('a.students').click();
    $('a.students').addClass('active');
    var key_rate = "";
    var display_button = '';
    invitation_details();
    function invitation_details() {
        var req = {};
        var res;
        req.action = "student_invitation_details";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            //return false;
            var html = "";
            if (data.invitations.length > 0) {
                for (var i = 0; i < data.invitations.length; i++)
                {
                    var invitation_id = data.invitations[i].id;
                    var course_id = "C" + String("000" + data.invitations[i].course_id).slice(-4);
                    var email = data.invitations[i].email_id;
                    if (data.invitations[i].contactMobile == undefined)
                        var contact_no = '--';
                    else
                        var contact_no = data.invitations[i].contactMobile;
                    if (data.invitations[i].enrollment_type == 0)
                        var enrollment_type = 'Course Key';
                    else if (data.invitations[i].enrollment_type == 1)
                        var enrollment_type = 'Market Place';
                    var status = data.invitations[i].status;
                    var course = data.invitations[i].courseName;
                    if (data.invitations[i].studentName == null)
                        var studentName = '--';
                    else
                        var studentName = data.invitations[i].studentName;
                    var timestamp = data.invitations[i].timestamp;
                    var disable = '';
                    var actionButton = '';
                    switch (status) {
                        case '0':
                            display_button = "<span class='label label-warning label-mini status' style='display:block;'><i class='fa fa-info-circle'></i> Pending</span>";
                            actionButton = '<a class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown"  type="button">Action <span class="caret "></span></a><ul class="dropdown-menu" role="menu"><li><a class=reject_link href="#">Reject</a></li><li><a class="resend_pending" href="#">Resend</a></li></ul>';
                            break;
                        case '1':
                            display_button = "<span class='label label-success label-mini status' style='display:block;'><i class='fa fa-check-circle'></i> Accepted</span>";
                            // disable = "disabled= disabled ";
                            actionButton = '<a disabled= disabled class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown"  type="button">Action <span class="caret "></span></a><ul class="dropdown-menu" role="menu"><li><a class=resend_link href="#" >Resend</a></li><li><a class=reject_link href="#">Reject</a></li></ul>';
                            break;
                        case '2':
                            display_button = "<span class='label label-danger label-mini status' style='display:block;'><i class='fa fa-ban'></i> Rejected</span>";
                            actionButton = '<a class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown"  type="button">Action <span class="caret "></span></a><ul class="dropdown-menu" role="menu"><li><a class="resend_link" href="#" >Resend</a></li></ul>';
                            break;
                        case '3':
                            display_button = "<span class='label label-info label-mini status' style='display:block;'><i class='fa fa-info-circle'></i> Resent</span>";
                            actionButton = '<a class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown"  type="button">Action <span class="caret "></span></a><ul class="dropdown-menu" role="menu"><li><a class=resend_link href="#" >Resend</a></li><li><a class=reject_link href="#">Reject</a></li></ul>';
                            break;
                    }
                    html += '<tr invitation_id= ' + invitation_id + ' class="generated"><td>' + (i + 1) + '</td><td>' + course + '</td><td>' + course_id + '</td><td class=small-txt>' + email + '</td><td>' + studentName + '</td><td> ' + contact_no + '</td><td>' + enrollment_type + '</td><td> ' + display_button + '</td><td class=btn-group> ' + actionButton + '</td></tr>';
                }
                // $('#tbl_student_deatils').find('tr.generated').remove();
                $('#tbl_student_deatils tbody').html(html);
            } else {
                $('#tbl_student_deatils tbody').html(' <tr><td>No any student invited yet</td></tr>');
            }

            $('.resend_link').off('click');
            $('.resend_link').on('click', function () {
                var req = {};
                var link_id = $(this).parents('tr').attr('invitation_id');
                var tr = $(this).parents('tr');
                var res;
                req.action = 'keys_avilable';
                $.ajax({
                    'type': 'post',
                    'url': ApiEndpoint,
                    'data': JSON.stringify(req)
                }).done(function (res) {
                    data = $.parseJSON(res);
                    if (data.status == 1) {
                        if (data.keys > 0) {
                            req.action = "resend_invitation_link";
                            req.link_id = link_id;
                            $.ajax({
                                'type': 'post',
                                'url': ApiEndpoint,
                                'data': JSON.stringify(req)
                            }).done(function (res) {
                                data = $.parseJSON(res);
                                if (data.status == 1)
                                    alertMsg('Invitation has been sent Successfully');
                                invitation_details();
                                //  tr.find('.status').html('<i class="fa fa-info-circle"></i>pending');

                            });
                        }
                        else {
                            alertMsg('You do not have course keys to send invitation.');
                        }
                    }
                    else {
                        alertMsg('You do not have course keys to send invitation.');
                    }
                });
            });
            $('.reject_link').off('click');
            $('.reject_link').on('click', function () {
                var link_id = $(this).parents('tr').attr('invitation_id');
                var tr = $(this).parents('tr');
                var req = {};
                var res;
                req.action = "reject_link_by_admin";
                req.link_id = link_id;
                $.ajax({
                    'type': 'post',
                    'url': ApiEndpoint,
                    'data': JSON.stringify(req)
                }).done(function (res) {
                    data = $.parseJSON(res);
                    if (data.status == 1)
                        alertMsg("Invitation has been rejected Successfully");
                    invitation_details();
                    // tr.find('.status').html('<i class="fa fa-ban"></i>Rejected');
                });
            });
			$('.resend_pending').off('click');
			$('.resend_pending').on('click', function() {
				var parent = $(this).parents('tr:eq(0)');
				var req = {};
				var res;
				req.action = 'resend-pending-invitation';
				req.invitationId = parent.attr('invitation_id');
				$.ajax({
					'type'	:	'post',
					'url'	:	ApiEndpoint,
					'data'	:	JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1)
						alertMsg('Mail sent successfully.');
					else
						alertMsg(res.message);
				});
			});
        });
    }
     $('.search').keyup(function ()
    {
        searchTable($(this).val());
    });

});
function searchTable(inputVal)
{
    var table = $('#tbl_student_deatils');
    table.find('tr').each(function (index, row)
    {
        var allCells = $(row).find('td');
        if (allCells.length > 0)
        {
            var found = false;
            allCells.each(function (index, td)
            {
                var regExp = new RegExp(inputVal, 'i');
                if (regExp.test($(td).text()))
                {
                    found = true;
                    return false;
                }
            });
            if (found == true)
                $(row).show();
            else
                $(row).hide();
        }
    });
}
 