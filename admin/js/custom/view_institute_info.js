
var ApiEndpoint = '../api/index.php';

$(function () {
    var imageRoot = '/';
    $('#loader_outer').hide();
    var fileApiEndpoint = '../api/files1.php';

    function viewInstitutes() {
        var req = {};
        var res;
        req.action = "view_institutes_info";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            var html = "";
            for (var i = 0; i < data.institutes.length; i++)
            {
                var id = data.institutes[i].id;
                var name = data.institutes[i].Name;
                var contactMobile = data.institutes[i].contactMobile;
                // var course_key = data.institutes[i].Key_Id;
                var email = data.institutes[i].email;
                var courses = data.institutes[i].Course;
                
                var students = data.institutes[i].StudentCount;
                if(students==null){
                   students=0; 
                }
                 var keys = data.institutes[i].KeyCount;
                 if(keys==null){
                     keys=0;
                 }
                // var consumed = data.institutes[i].consumed;
                html += '<tr ><td>' + (i + 1) + '</td><td>' + id + '</td><td>' + name + ' </td><td class=small-txt>' + email + '</td><td>' + contactMobile + '</td><td><a href=backend_courses.php?instituteId=' + id + ' class="btn btn-success btn-xs "> C(' + courses + ') </a>\n\
                        </td><td><a href=students.php?instituteId=' + id + ' class="btn btn-primary btn-xs"> S(' + students + ')</a></td><td class="center hidden-phone"> <a href="course-key.php" class="btn btn-info btn-xs">CK(' + keys + ')</td><td \n\
                    class="center hidden-phone" > <a href=CourseKey.php?instituteId='+id+' class="btn btn-warning  btn-xs"> ' + 'Consumption ' + '</a></td></tr>';
            }
            $('#tbl_view_institutes').append(html);
           $('#tbl_view_institutes').dataTable({
                "aaSorting": [[0, "asc"]]
            });

        });

    }
    viewInstitutes();
});
