$(function() {

	fetchSubjectStudents();
	//function to get the students and chapters for the subject
	function fetchSubjectStudents() {
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		if(req.courseId == "" || req.courseId == null || req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action ="get-subject-students";
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			data = $.parseJSON(res);
			console.log(data);
			if(data.status == 0)
				alert(data.message);
			else {
				console.log(data);
				var html = "";
				for (var i = 0; i < data.students.length; i++)
				{
					var student_id = data.students[i].id;
					var student_username = data.students[i].username;
					var name = data.students[i].name;
					var password = ((data.students[i].password == "")?'<small class="text-success">REGISTERED</small>':data.students[i].password);
					// var contactMobile = data.students[i].contactMobile;
					// var course_key = data.students[i].key_id;
					var email = data.students[i].email;
					var date = data.students[i].date;
					var checkbox = ((!data.students[i].parentid)?'<input type="checkbox" class="js-option-student" />':'');
					var parent = ((!data.students[i].parentid)?'<td></td>':'<td><strong>'+data.students[i].userid+'</strong> / <strong>'+data.students[i].passwd+'</strong></td>');
					html += '<tr data-student="'+student_id+'">'+
								/*'<td>' + checkbox + '</td>'+*/
								'<td>' + student_id + '</td>'+
								'<td>' + student_username + '</td>'+
								'<td>' + password + '</td>'+
								'<td>' + name + '</td>'+
								'<td class="small-txt">' + email + '</td>'+
								parent+
								'<td><a href="javascript:void(0)" data-student="' + student_id + '" class="btn btn-warning js-chat">Chat</a></td>'+
							'</tr>';
				}
				$('#tbl_students_details_of_subject').append(html);
				if($('#tbl_students_details_of_subject tbody tr').length==0){
				    $('.existing-students').hide();
				}
			}
		});
	}

	$('.existing-students').on("click", ".js-chat", function(){
		var req = {};
		var res;
		req.action = 'get-professor-chat-link';
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		req.studentId = $(this).attr("data-student");
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			res = $.parseJSON(res);
			//console.log(res);
			if (res.status == 0)
				console.log(res.message);
			else {
				//console.log(res.roomId);
				window.open('../messenger/'+res.roomId);
			}
		});
	});

	function OpenInNewTab(url) {
		var win = window.open(url, '_blank');
		win.focus();
	}
	
});


