
var ApiEndpoint = '../api/index.php';
var instituteId = getUrlParameter('instituteId');

$(function () {
    var imageRoot = '/';
    $('#loader_outer').hide();
    var fileApiEndpoint = '../api/files1.php';

    function StudentDetailsWithCourses() {
        var req = {};
        var res;
        req.action = "Student_Details_WithCourses";
        if (typeof (instituteId) === "undefined") {
                  } else {
            req.instituteId = instituteId;
            $('#invite_student').remove();
        }
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            if(data.status==1){
                  var html = "";
            for (var i = 0; i < data.students.length; i++)
            {
                var studentid = data.students[i].userId;
                var name = data.students[i].name;
                var contactMobile = data.students[i].contactMobile;
                // var course_key = data.institutes[i].Key_Id;
                var email = data.students[i].email;
                var courses = '';
                for (var j = 0; j < data.courses[i].length; j++) {
                    courses += data.courses[i][j].name + ', ';
                }
                courses = courses.substring(0, courses.length - 2);
                html += '<tr><td>' + name + '<br/>' + courses + '</td><td class=small-txt>' + email + '</td><td>' + contactMobile + ' </td><td>' + studentid + '</td></tr>';
            }
            $('#tbl_studentDetails tbody').html(html);
            }else{
                 $('#tbl_studentDetails tbody').html('<tr><td>No student joined yet </td></tr>');
            }
          

        });

    }
    StudentDetailsWithCourses();
    bindCoursesDropdown();
    var keys_avilable = get_keys_remaining();
    function bindCoursesDropdown() {
        var req = {};
        var res;
        req.action = "Courses_for_student";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            for (var i = 0; i < data.courses.length; i++)
            {
                var id = data.courses[i].id;
                var name = data.courses[i].name;
                $("#combo_courses").append("<option value=" + id + " >" + name + "</option>");
            }
        });
    }
    function get_keys_remaining() {
        var req = {};
        req.action = 'keys_avilable';
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            if (data.status == 1) {
                keys_avilable = data.keys;
            }
            else {
                alertMsg(res.msg);
            }
        });
        return keys_avilable;

    }
      $('#send_activation_link').off('click');
    $('#send_activation_link').on('click', function () {
        $('.help-block').remove();
        var invalid = '';
        var correct = true
        var emails = $.trim($('#textarea_email_id').val());
        var email = emails.split(',');
        if (emails == '') {
            alertMsg('Please enter email id ');
            return false;
        }
        if (checkDuplicates(email) == 'false') {
            alertMsg('Please remove duplicates ');
            return false;
        }
        if ((email.length > keys_avilable)) {
            alertMsg('You have only ' + keys_avilable + ' available ');
            return false;
        }
        $.each(email, function (index, value) {
            if (validateEmail($.trim(value)) == false) {
                invalid += value + ',';
                correct = false;
            }
        });
        if (correct == false) {
            $('#textarea_email_id').after('<span class="help-block text-danger "> Correct these email addresses:\n' + invalid+'</span>');
            return false;
        }
        var courseId = $('#combo_courses').val();
        if (courseId == 0) {
            $('#combo_courses').after('<span class="help-block text-danger "> Please select a Course ! </span>');
            return false;
        }
        else {
            var req = {};
            req.courseId = $('#combo_courses').val();
            req.action = 'send_activation_link_student';
            req.email = email;
            req.enrollment_type = 0;
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                data = $.parseJSON(res);
                if (data.status == 1) {
                    alertMsg(data.message);
                }
                else {
                    alertMsg(data.msg);
                }
                $('#myModal').modal('hide');

            });
        }
    });
    $('#cancel-invitation').on('click', function () {
         $('#myModal').modal('hide');
    });
    
    $('#invite_student').on('click', function () {
        $('#textarea_email_id').val('');
        keys_avilable = get_keys_remaining();
        alertMsg("You can send " + keys_avilable + " invitations");
    });
    function checkDuplicates(a) {
        //Check all values in the incoming array and eliminate any duplicates
        var r = new Array(); //Create a new array to be returned with unique values
        //Iterate through all values in the array passed to this function
        o:for (var i = 0, n = a.length; i < n; i++) {
            //Iterate through any values in the array to be returned
            for (var x = 0, y = r.length; x < y; x++) {
                //Compare the current value in the return array with the current value in the incoming array
                if (r[x] == a[i]) {
                    //If they match, then the incoming array value is a duplicate and should be skipped
                    return 'false';
                    //continue o;
                }
            }
            //If the value hasn't already been added to the return array (not a duplicate) then add it
            r[r.length] = a[i];
        }
        //Return the reconstructed array of unique values
        //return r;
        return 'true';
    }
    function validateEmail($email) {
        var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!emailReg.test($email)) {
            return false;
        } else {
            return true;
        }
    }
});
