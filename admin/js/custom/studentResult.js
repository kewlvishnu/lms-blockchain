var ApiEndPoint = '../api/index.php';
var details;
var sections;
var attemptId;
var count = 0;
var sectionCount = 0;
var ch = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
var totalScore = 0;

$(function() {
	$('#back').attr('href', 'examResult.php?examId=' + getUrlParameter('examId'));
	//fetchUserDetails();
	fetchAtemptDeatils();

	//event handler for compare with topper click
	$('#compareTopper').on('click', function() {
		$('#hidden-topper-graph').css('height', '100%');
		$('#hidden-topper-graph').css('width', '100%');
	});

	//event handler for compare with topper close text
	$('#topperClose').on('click', function() {
		$('#hidden-topper-graph').css('height', '0px');
		$('#hidden-topper-graph').css('width', '0px');
	});

	//event handler for compare time close text
	$('#timeClose').on('click', function() {
		$('#hidden-time-graph').css('height', '0px');
		$('#hidden-time-graph').css('width', '0px');
		$('#hero-bar3').remove();
	});

	//function to fetch user details for change password page
	function fetchUserDetails() {
		var req = {};
		var res;
		req.action = 'get-profile-details';
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				details = res;
				fillUserDetails(res);
			}
		});
	}

	//function to fill user details
	function fillUserDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}

	//function to get attempt details
	function fetchAtemptDeatils() {
		var req = {};
		var res;
		var attemptNo = getUrlParameter('attemptNo');
		var examId = getUrlParameter('examId');
		if(examId != undefined) {
			req.action = 'get-result';
			req.examId = examId;
			req.studentId = getUrlParameter('studentId');
			if(attemptNo == undefined)
				req.attemptNo = 0;
			else
				req.attemptNo = attemptNo;
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else {
					$('#attemptNo').text(res.attemptNo);
					attemptId = res.result.id;
					fillAttemptDeatils(res);
					fetchTopperComparison();
					if(res.totalAttempts > 1) {
						//fetchGraph();
						//initGraph(res);
						fetchStudentTimeLine();
						//fetchAllStudentGraph();
						if(res.attemptNo > 1) {
							$('.previous-attempt').attr('href', 'studentResult.php?examId=' + getUrlParameter('examId') + '&studentId=' + getUrlParameter('studentId') + '&attemptNo=' + (res.totalAttempts - res.attemptNo + 1)).removeClass('custom-disabled');
						}
						if(res.attemptNo != res.totalAttempts) {
							$('.next-attempt').attr('href', 'studentResult.php?examId=' + getUrlParameter('examId') + '&studentId=' + getUrlParameter('studentId') + '&attemptNo=' + (res.totalAttempts - res.attemptNo - 1)).removeClass('custom-disabled');
						}
					}
				}
			});
		}
		else
			window.location = 'profile.php';
	}

	//function to fill attempt details
	function fillAttemptDeatils(data) {
		sections = data.sections;
		$('#percentage').text(data.result.percentage);
		$('#rank').text(data.rank + '/' + data.totalRank);
		var startDate = new Date(parseInt(data.result.startDate+'000'));
		var start = startDate.getDate() + '-' + MONTH[startDate.getMonth()] + '-' + startDate.getFullYear() + '&emsp;' + startDate.getHours() + ':' + startDate.getMinutes();
		$('#startDate').html(start);
		if(data.result.endDate == 0)
			var end = 'Not completed';
		else {
			var endDate = new Date(parseInt(data.result.endDate+'000'));
			var end = endDate.getDate() + '-' + MONTH[endDate.getMonth()] + '-' + endDate.getFullYear() + '&emsp;' + endDate.getHours() + ':' + endDate.getMinutes();
		}
		$('#endDate').html(end);
		var time = convertSecondsIntoMinutes(data.result.time);
		$('#timeTaken').text(time);
		$('#score').text(data.result.score + '/');
		var html = '';
		for(var i = 0; i < data.sections.length; i++) {
			var pie = [];
			html = '<tr class="custom-bg">'
					+ '<td>'+data.sections[i].name+'</td>'
					+ '<td>'+data.sections[i].questions+'</td>'
					+ '<td>'+convertSecondsIntoMinutes(data.sections[i].time)+'</td>'
					+ '<td>'
						+ '<div class="progress progress-sm">'
							+ '<div class="progress-bar progress-bar-success" style="width: 30%;">'
								+ '<span>3</span>'
							+ '</div>'
							+ '<div class="progress-bar progress-bar-danger" style="width: 50%">'
								+ '<span>5</span>'
							+ '</div>'
							+ '<div class="progress-bar progress-bar-black" style="width: 20%">'
								+ '<span>2</span>'
							+ '</div>'
						+ '</div>'
						//+ '<div class="pie-marks"><canvas height="20" width="47" style="display: inline-block; width: 47px; height: 20px; vertical-align: top;"></canvas></div>'
					+ '</td>'
				+ '</tr>';
			$('#sectionTable').append(html);
			var count = 0;
			for(var j = 0; j < 3; j++) {
				var n = 0;
				if(data.sections[i].checks[count] != undefined) {
					if(data.sections[i].checks[count].check == j) {
						n = data.sections[i].checks[count].number;
						count++;
					}
					else {
						n = 0;
					}
				}
				pie.push(n);
			}
			//pie contains counts of unattempted, right, wrong (in same order)
			//calculating percentage
			var total = parseInt(pie[0]) + parseInt(pie[1]) + parseInt(pie[2]);
			var up = (pie[0] / total) * 100;
			var rp = (pie[1] / total) * 100;
			var wp = (pie[2] / total) * 100;
			$('#sectionTable .progress:eq(-1) .progress-bar-success').css('width', rp+'%');
			$('#sectionTable .progress:eq(-1) .progress-bar-success').find('span').text(pie[1]);
			$('#sectionTable .progress:eq(-1) .progress-bar-danger').css('width', wp+'%');
			$('#sectionTable .progress:eq(-1) .progress-bar-danger').find('span').text(pie[2]);
			$('#sectionTable .progress:eq(-1) .progress-bar-black').css('width', up+'%');
			$('#sectionTable .progress:eq(-1) .progress-bar-black').find('span').text(pie[0]);
			/*$("#sectionTable .pie-marks:eq(-1)").sparkline(pie, {
				type: 'pie',
				width: '30',
				height: '30',
				sliceColors: ['#201E1E', '#a9d86e', '#f68731']
				//tooltipFormat: '<span style="display:block; padding:0px 10px 12px 0px;">' +
				//'<span style="color: {{color}}">&#9679;</span> {{offset:names}} ({{percent.1}}%)</span>'});
		
			});*/
			//to generate category table
			var catHtml = data.sections[i].name + ' :<br/>'
						+ '<table class="table">'
							+ '<thead>'
								+ '<tr>'
									+ '<th width="24%">No of questions</th>'
									+ '<th width="56%">Question Type</th>'
									+ '<th width="10%">Correct</th>'
									+ '<th width="10%">Wrong</th>'
								+ '</tr>'
							+ '</thead>'
							+ '<tbody>';
			for(var j = 0; j < data.sections[i].categories.length; j++) {
				catHtml += '<tr>'
							+ '<td>'+data.sections[i].categories[j].required+'</td>';
				if(data.sections[i].categories[j].questionType == 0)
					catHtml += '<td>Multiple Choice questions</td>';
				else if(data.sections[i].categories[j].questionType == 1)
					catHtml += '<td>True/False</td>';
				else if(data.sections[i].categories[j].questionType == 2)
					catHtml += '<td>Fill in the blanks</td>';
				else if(data.sections[i].categories[j].questionType == 3)
					catHtml += '<td>Match the following</td>';
				else if(data.sections[i].categories[j].questionType == 4)
					catHtml += '<td>Comprehensive Questions</td>';
				else if(data.sections[i].categories[j].questionType == 5)
					catHtml += '<td>Dependent Questions</td>';
				else if(data.sections[i].categories[j].questionType == 6)
					catHtml += '<td>Multiple Answer Questions</td>';
				else if(data.sections[i].categories[j].questionType == 7)
					catHtml += '<td>Audio Questions</td>';
				catHtml += '<td>+'+data.sections[i].categories[j].correct+'</td>'
							+ '<td>-'+data.sections[i].categories[j].wrong+'</td>'
						+ '</tr>';
			}
			catHtml += '</tbody>'
					+ '</table>';
			$('#categories .category-table').append(catHtml);
		}
		fetchSectionDetails();
	}
	
	//function to fetch data for graph
	function fetchGraph() {
		var req = {};
		var res;
		req.action = 'get-attempt-graph';
		req.examId = getUrlParameter('examId');
		req.studentId = getUrlParameter('studentId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
				initGraph(res);
		});
	}
	
	//function to initiate attempt graph
	function initGraph(data) {
		var filler = {};
		var filler1={};
		filler.labels = [];
		filler.datasets = [];
		filler.datasets[0] = {};
		filler1.labels = [];
		filler1.datasets = [];
		filler.datasets[0].label = 'Score';
		filler.datasets[0].fillColor = 'rgba(25, 113, 184, 0)';
		filler.datasets[0].strokeColor = "rgba(25, 113, 184, 1)";
		filler.datasets[0].pointColor = "rgba(25, 113, 184, 1)";
		filler.datasets[0].pointStrokeColor = "#fff";
		filler.datasets[0].pointHighlightFill = "#fff";
		filler.datasets[0].pointHighlightStroke = "rgba(25, 113, 184, 1)";
		filler.datasets[0].data = [];
		filler1.datasets[0] = {};
		filler1.datasets[0].label = 'Percentage';
		filler1.datasets[0].fillColor = 'rgba(25, 184, 75, 0)';
		filler1.datasets[0].strokeColor = "rgba(25, 184, 75, 1)";
		filler1.datasets[0].pointColor = "rgba(25, 184, 75, 1)";
		filler1.datasets[0].pointStrokeColor = "#fff";
		filler1.datasets[0].pointHighlightFill = "#fff";
		filler1.datasets[0].pointHighlightStroke = "rgba(151,187,205,1)";
		filler1.datasets[0].data = [];
		for(var i = 0; i < data.graph.length; i++) {
			filler.labels.push("Attempt No " + (i+1));
			filler1.labels.push("Attempt No " + (i+1));
			filler.datasets[0].data.push(data.graph[i].score);
			filler1.datasets[0].data.push(data.graph[i].percentage);
		}
		
		var ctx = $('#attemptChart').get(0).getContext("2d");
		var myLineChart = new Chart(ctx).Line(filler, {'bezierCurve' : false});
	//	var ctx = $('#percentageChart').get(0).getContext("2d");
	//	var myLineChart1 = new Chart(ctx).Line(filler1, {'bezierCurve' : false});
	//	$('#graph').show();
	}
	
	
	function fetchAllStudentGraph() {
		var req = {};
		var res;
		req.action = 'get-allStudentNormalization-graph';
		req.examId = getUrlParameter('examId');
		req.studentId= getUrlParameter('studentId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else{
				initAllStudentGraph(res);
			}
				
		});
	}
	
	//function to initiate attempt graph
	function initAllStudentGraph(data) {
		//console.log(totalScore);
		var differnece=totalScore-parseFloat(data.highest);
		var blocksDiff=(parseFloat(totalScore)-parseFloat(differnece+parseFloat(data.lowest)))/10;
		//var marksDis=[];
		var students=[];
		var markPer=[];
		var newlowest=(parseFloat(data.lowest)+parseFloat(differnece)).toFixed(1);
		for(var i=1;i<11;i++)
		{	//marksDis[i-1]=(parseFloat(newlowest)+parseFloat(blocksDiff*i)).toFixed(1);
			students[i-1]=0;
			//checking
			var temp=(parseFloat(newlowest)+parseFloat(blocksDiff*i)).toFixed(1);
			markPer[i-1]=Math.round((temp/totalScore)*100)+'%';
			//markPer[i-1]=markPer[i-1]+'%';
		}
		for(var i=markPer.length-1;i>0;i--)
		{
			markPer[i]= markPer[i-1]+' - '+markPer[i];
		}
		
		markPer[0]=Math.round(newlowest)+'%    -  '+markPer[0];
		$.each( data.graph, function( key, value ) {
			var newscore=(parseFloat(value['score'])+parseFloat(differnece)).toFixed(1);
			var index=Math.round(((newscore-newlowest)/blocksDiff));
			if(index>0)
			{	index=index-1;
			}else{
				index=0;
			}
			students[index]=students[index]+1;
			
			
		});
		$('#hero-bar3').highcharts({
			chart: {
				zoomType: 'xy'
			},
			title: {
				text: 'Percentage distribution of all students'
			},
			subtitle: {
				text: 'This graph shows percentage w.r.t to highest scorer'
			},
			xAxis: [
			{
				title: {
					text: 'Percentage Score',
					style: {
						color: Highcharts.getOptions().colors[1]
					}
				},
				categories: markPer,
				crosshair: true
			}],
			yAxis: [{ // Primary yAxis
				labels: {
					format: '{value}',
					style: {
						color: Highcharts.getOptions().colors[1]
					}
				},
				title: {
					text: 'No. of Students',
					style: {
						color: Highcharts.getOptions().colors[1]
					}
				}
			}, { // Secondary yAxis
				title: {
					text: 'Students',
					style: {
						color: Highcharts.getOptions().colors[0]
					}
				},
				labels: {
					format: '{value}',
					style: {
						color: Highcharts.getOptions().colors[0]
					}
				},
				opposite: true
			}],
			tooltip: {
				shared: true
			},
			legend: {
				layout: 'vertical',
				align: 'left',
				x: 120,
				verticalAlign: 'top',
				y: 100,
				floating: true,
				backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
			},
			series: [{
				name: 'Students',
				type: 'column',
				yAxis: 1,
				data: students,
				tooltip: {
					valueSuffix: ''
				}

			}, {
				name: 'No. of Students',
				type: 'spline',
				yAxis: 1,
				data: students,
				tooltip: {
					valueSuffix: ''
				}
			}]
		});
		$('.percAllStudents').show();
	}


	function fetchStudentTimeLine() {
		var req = {};
		var res;
		req.action = 'getAllStudentTimeLineGraph';
		req.student_id= getUrlParameter('studentId');
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			console.log(res);
			if(res.status == 0)
				alert(res.message);
			else
			{
				//initStudentAttemptLine(res);
				initStudentTimeLine(res);
				
			}
				
		});
	}

	//function to get graphs vs time graph attempt independent
	function initStudentTimeLine(data) {
		/**
		 * Create the chart when all data is loaded
		 * @returns {undefined}
		 */
		 $('#graph').show();
		var timepoints=[];
		var timepoints2=[];
		var selfMarks=[];
		var dataset = [];
		var avgStudentCount=[];
		var avgStudentMarks=[];
		var toppermarks=[];
		var avgMarks=[];
		var attempts=[];
		var lastday=$.now();//parseInt(data.lastday[0]['endDate'])* parseInt(1000);
		//var today=$.now();
		
		$.each(data.self, function (j, self) {
			timepoints[j]=parseInt(self.endDate*1000);
			var element=parseInt(j)+parseInt(1);
			attempts[j]='Attemp NO. '+(element);
			selfMarks[j]=parseInt(self.score);
		});
		$.each(data.avgScore, function (j, avgScore) {
			avgStudentCount[j]=0;
			timepoints2[j]=0;
			avgStudentMarks[j]=0;
			toppermarks[j]=0;
		});
		var lastpoint=timepoints[timepoints.length-1];
		var lastflag=0;
		$.each(data.avgScore, function (j, avgScore) {
			timepoints2[j]=parseInt(avgScore.endDate)* parseInt(1000);
			avgStudentMarks[j]=parseFloat(avgStudentMarks[j])+parseFloat(avgScore.score);
			avgStudentCount[j]=parseInt(avgStudentCount[j])+parseInt(1);
			//calculating for toppermarks
			if(j!=0)
			{
				//to calculate highest from previous
				if(avgScore.score>toppermarks[j-1])
				{
					toppermarks[j]=parseFloat(avgScore.score);
				}else{
					toppermarks[j]=parseFloat(toppermarks[j-1]);
				}
			}else{
			
				toppermarks[j]=parseFloat(avgScore.score);
			}			
			
		});
		for(var i=0;i<data.avgScore.length;i++){
			if(avgStudentCount[i]!=0)
				avgMarks[i]=parseFloat(avgStudentMarks[i])/parseFloat(avgStudentCount[i]);
			else
			avgMarks[i]=0;
						
		}
		//we got avg as whole it will same for all students	
		//console.log(lastflag);
		//console.log(timepoints2);
			
			timepoints2[timepoints2.length]=parseInt(lastday);
			///timepoints2[timepoints2.length]=parseInt(lastpoint);
			//console.log(lastpoint+"cjj"+lastday);
			avgMarks[avgMarks.length]=parseFloat(avgMarks[avgMarks.length-1]);
			toppermarks[toppermarks.length]=parseFloat(toppermarks[toppermarks.length-1]);
			
		var jCompare=0;
		var flag=0;
		
		//console.log(timepoints2);
		//graph to adjust and insert self timestamp
		/*for(var i=0;i<timepoints2.length-1;i++){
			var compareTime=timepoints[jCompare];
			//console.log(compareTime+"---check--"+timepoints2[i]+"--check2--"+timepoints2[i+1]);
			if(jCompare>(timepoints.length-1))
			{
				jCompare=timepoints.length-1;
			}
			if(timepoints2[i]<compareTime && timepoints2[i+1]>compareTime )	
			{	
				jCompare=parseInt(jCompare)+parseInt(1);
				
						var highmarks=parseFloat(toppermarks[i+1]);
						var avg=(parseFloat(avgMarks[i]));
						timepoints2.splice(i, 0,compareTime );
						toppermarks.splice(i, 0,highmarks );
						avgMarks.splice(i, 0,avg );
				
				
			}
			else if(timepoints2[i]==compareTime)
			{
				jCompare=parseInt(jCompare)+parseInt(1);
				
			}else if(timepoints2[i+1]==compareTime){
				
				jCompare=parseInt(jCompare)+parseInt(1);
				// return true;
			}else{
				if(i!=0)
				{
					if(timepoints2[i-1]<compareTime && timepoints2[i]>compareTime )
					{	//console.log('yes');
					
						jCompare=parseInt(jCompare)+parseInt(1);
						//console.log(timepoints2)
					
							var highmarks=parseFloat(toppermarks[i]);
							var avg=(parseFloat(avgMarks[i-1]));
							timepoints2.splice(i-1, 0,compareTime );
							toppermarks.splice(i-1, 0,highmarks );
							avgMarks.splice(i-1, 0,avg );
						
					}
					
				}
			}		
			
		  }
		
		*/
		for(var k=0;k<timepoints.length;)
		{
			for(var i=0;i<timepoints2.length-1;i++)
			{	
				if(timepoints[k]==timepoints2[i])
				{
					//console.log(timepoints[k]);
					//k++;
					break;
				}	
				else if(timepoints[k]>timepoints2[i] &&timepoints[k]<timepoints2[i+1] )
				{		//console.log(timepoints[k]+'check'+timepoints2[i]);
						if(timepoints2[i]>timepoints[k])
						{
							var highmarks=parseFloat(toppermarks[i+1]);
							var avg=(parseFloat(avgMarks[i+1]));
							timepoints2.splice(i, 0,timepoints[k]);
							toppermarks.splice(i, 0,highmarks );
							avgMarks.splice(i, 0,avg );
							break;
						}
						else{
							var highmarks=parseFloat(toppermarks[i+1]);
							var avg=(parseFloat(avgMarks[i+1]));
							timepoints2.splice(i+1, 0,timepoints[k]);
							toppermarks.splice(i+1, 0,highmarks );
							avgMarks.splice(i+1, 0,avg );
							break;
						}
					   
						//k++;
				}
				else{
					//console.log("check");
					//console.log(timepoints[k]+'check'+timepoints2[i]+'check'+timepoints2[i]);
					//k++;
				}
				//k++;
			}
			k++;
		}
		for(var i=0;i<timepoints2.length-1;i++)
		{
			if(timepoints2[i]==timepoints2[i+1])
			{
				timepoints2.splice(i, 1);
				avgMarks.splice(i, 1);
				toppermarks.splice(i,1);
			}
			
		}
	
		var scoresOptions = [],
			scoresCounter = 0,
			newnames = ['self','avgScore','topper','attempts'];
		function createChart2() {
			//console.log(scoresOptions);
			var i=0;
			
			$('#graph12').highcharts('StockChart', {
				rangeSelector: {
					selected: 4
				},
				xAxis: {       
						    
							//ordinal: false
						},
				yAxis:[ {
						labels: {
							  align: 'left',
                			  x: 15,
							formatter: function () {
									return  this.value ;
							}
						},
						//min: 0,
						plotLines: [{
							value: 0,
							width: 2,
							color: 'silver'
						}]
					},
				 { // Secondary yAxis
		            labels: {
		                formatter: function () {
								return  this.value ;
								//return point.y;
							}
		            },
					linkedTo:0,
		            opposite: false
	       		}],
				plotOptions: {
					series: {
						//compare: 'percent'
						//pointStart:timepoints[0],
            			//pointInterval: 3600 * 1000
					marker : {
		                    enabled : true,
		                    radius : 3
		                }
					}
				},legend: {
				            enabled: true,
							layout: 'vertical',
				            verticalAlign: 'middle',
				            borderWidth: 0
				        },
	
				tooltip: {
					pointFormat: '<span >{series.name}</span>: <b>{point.y}</b><br/>'
						
				},
	
				series: scoresOptions
			});
			$('.highcharts-input-group').hide();
		}
		
		//declaring array for data points calculation
		var elementarray1=[];
		var elementarray2=[];
		var elementarray3=[];
		var elementarray4=[];
		// calculating for self array
		for(var i = 0; i < timepoints.length; i++) {
			elementarray=[];
			elementarray[0] = parseInt(timepoints[i]);
			elementarray[1] = parseInt(selfMarks[i]);
			elementarray1[i]=elementarray;
		}
		//console.log(elementarray1)
		scoresOptions[0] = {
								name: 'Your Score',
								data: elementarray1,
								lineWidth: 3
								//type: 'spline'
							};
		//calculating datapoint for avg 
		for(var i = 0; i < avgMarks.length; i++) {
			elementarray=[];
			elementarray[0] = parseInt(timepoints2[i]);
			elementarray[1] = parseInt(avgMarks[i]);
			elementarray2[i]=elementarray;
			
		}
		scoresOptions[1] = {
								name: 'Average',
								data: elementarray2,
								dashStyle: 'longdash',
								type: 'spline'
							};
		for(var i = 0; i < toppermarks.length; i++) {
			elementarray=[];
			elementarray[0] = parseInt(timepoints2[i]);
			elementarray[1] = parseInt(toppermarks[i]);
			elementarray3[i]=elementarray;
		}
		scoresOptions[2] = {
								name: 'Class Highest',
								data: elementarray3,
								type: 'spline',
								dashStyle:'ShortDash'
							};
		for(var i = 0; i < timepoints.length; i++) {
			elementarray=[];
			elementarray[0] = parseInt(timepoints[i]);;
			elementarray[1]= parseInt(i+1);;
			elementarray4[i]=elementarray;
		}
		scoresOptions[3] = {
								name: 'Attempt No ',
								data: elementarray4,
								type: 'spline',
								dashStyle:'ShortDash',
								color: '#FFFFFF',
								lineWidth: 0.4,
								showInLegend: false
							};
		
			
		createChart2();	
		//				
	}
	
	//function to fetch each section questions and answers
	function fetchSectionDetails() {
		var req = {};
		var res;
		req.action = 'get-section-questions';
		req.sectionId = sections[count].sectionId;
		req.attemptId = attemptId;
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				fillSectionDetails(res);
				if(count == sections.length - 1) {
					$('#score').text($('#score').text() + totalScore);
					MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
				}
				else {
					count++;
					fetchSectionDetails();
				}
			}
		});
	}
	
	//function to create section view
	function fillSectionDetails(data) {
		var parentId = 0;
		html = '<section class="panel">'
				+ '<header class="panel-heading">'
					+ '<h4><i class="fa fa-minus-square hide-section" style="cursor:pointer;"></i> '+sections[sectionCount].name+'</h4>'
				+ '</header>'
				+ '<div class="panel-body">'
					+ '<br>'
					+ '<section class="panel">'
						+ '<table class="table">'
							+ '<tbody>';
		for(var i = 0; i < data.questions.length; i++) {
			var tempType = '';
			var buttonText = '';
			if(data.questions[i].questionType == 0)
				tempType = ' <strong> ( Multiple Choice Questions ) </strong>';
			else if(data.questions[i].questionType == 1)
				tempType = ' <strong> ( True and False ) </strong>';
			else if(data.questions[i].questionType == 2)
				tempType = ' <strong> ( Fill in the blanks ) </strong>';
			else if(data.questions[i].questionType == 3)
				tempType = ' <strong> ( Match the following ) </strong>';
			else if(data.questions[i].questionType == 5) {
				tempType = ' <strong> ( Dependent type questions ) </strong>';
				buttonText = 'View Sub-Parts';
			}
			else if(data.questions[i].questionType == 6)
				tempType = ' <strong> ( Multiple Answer Questions ) </strong>';
			else if(data.questions[i].questionType == 7)
				tempType = ' <strong> ( Audio questions ) </strong>';
			//paragraph adding for CMP questions
			if(data.questions[i].parentId != 0 && data.questions[i].parentId != parentId) {
				parentId = data.questions[i].parentId;
				html += '<tr><td colspan=4>' + data.questions[i].parent + ' <strong> ( Comprehensive type questions ) </strong></td></tr>';
			}
			html += '<tr data-id="'+data.questions[i].questionId+'">';
			if(data.questions[i].parentId != 0) {
				html += '<td width="5%"></td>';
			}
			html += '<td width="5%">Q.' + (i+1) + '<h4>';
			if(data.questions[i].check == 1)
				html += '<i class="fa fa-check-circle-o text-success" style="font-size: 2em;"></i>';
			else if(data.questions[i].check == 0)
				html += '<i class="fa fa-dot-circle-o text-warning" style="font-size: 2em;"></i>';
			else
				html += '<i class="fa fa-times-circle-o text-danger" style="font-size: 2em;"></i>';
			var question = data.questions[i].question.substring(0, data.questions[i].question.length - 5) + tempType + '</p>';
			if(buttonText == '')
				buttonText = 'View Solution';
			html += '</h4></td>';
			if(data.questions[i].parentId == 0)
				html += '<td colspan="2">';
			else
				html += '<td>';
			html += '<div class="row">' + question + '</div>';
			if(data.questions[i].questionType == 3) {
				html += '<div>';
				var temp = [];
				for(var j = 0; j < data.questions[i].origAnswer.length; j++) {
					temp.push(data.questions[i].origAnswer[j].columnB);
				}
				temp = shuffle(temp);
				for(var j = 0; j < data.questions[i].origAnswer.length; j++) {
					html += ch[j] + ') ' + data.questions[i].origAnswer[j].columnA + ' -> ' + temp[j] + '<br>';
				}
				html += '</div>';
			}
			html += '<button class="btn btn-info btn-xs view-solution" data-hidden="0"  data-text="' + buttonText + '">' + buttonText +'</button>'
					+ '<div class="answer" style="display: none;">';
			totalScore += parseInt(data.questions[i].correct);
			if(data.questions[i].questionType == 0) {
				for(var j = 0; j < data.questions[i].origAnswer.length; j++) {
					var origAnswer = data.questions[i].origAnswer[j].option;
					if(data.questions[i].check == 0)
						var userAnswer = '';
					else
						var userAnswer = data.questions[i].userAnswer[0].answer;
					html += ch[j] + ') ' + data.questions[i].origAnswer[j].option + '&emsp;';
					if(data.questions[i].origAnswer[j].correct == 1)
						html += '<i class="fa fa-check-circle-o text-success"></i>';
					else if(origAnswer == userAnswer && data.questions[i].check != 0) {
						html += '<i class="fa fa-times-circle-o text-danger"></i>';
					}
					html += '<br/>';
				}
			}
			else if(data.questions[i].questionType == 1) {
				var origAnswer = data.questions[i].origAnswer[0].answer;
				if(data.questions[i].check == 0)
					var userAnswer = '';
				else
					var userAnswer = data.questions[i].userAnswer[0].answer;
				html += 'True';
				if(origAnswer == 1) {
					html += ' <i class="fa fa-check-circle-o text-success"></i>';
				}
				else if(userAnswer == 1 && data.questions[i].check != 0)
					html += ' <i class="fa fa-times-circle-o text-danger"></i>';
				html += '<br/>False';
				if(origAnswer == 0) {
					html += ' <i class="fa fa-check-circle-o text-success"></i>';
				}
				else if(userAnswer == 0 && data.questions[i].check != 0)
					html += ' <i class="fa fa-times-circle-o text-danger"></i>';
			}
			else if(data.questions[i].questionType == 2) {
				for(var j = 0; j < data.questions[i].origAnswer.length; j++) {
					var origAnswer = data.questions[i].origAnswer[j].blanks;
					if(data.questions[i].check == 0)
						var userAnswer = '';
					else
						var userAnswer = data.questions[i].userAnswer[j].answer;
					html += userAnswer + '&emsp;';
					if(origAnswer == userAnswer)
						html += '<i class="fa fa-check-circle-o text-success"></i>';
					else if(data.questions[i].check != 0) {
						html += '<i class="fa fa-times-circle-o text-danger"></i>&emsp;' + data.questions[i].origAnswer[j].blanks;
					}
					html += '<br/>';
				}
			}
			else if(data.questions[i].questionType == 3) {
				for(var j = 0; j < data.questions[i].origAnswer.length; j++) {
					var origAnswer = data.questions[i].origAnswer[j].columnB;
					if(data.questions[i].check == 0)
						var userAnswer = '';
					else
						var userAnswer = data.questions[i].userAnswer[j].answer;
					html += ch[j] + ') ' + data.questions[i].origAnswer[j].columnA + ' -> ' + origAnswer + '&emsp;';
					if(origAnswer == userAnswer)
						html += '<i class="fa fa-check-circle-o text-success"></i>';
					else if(data.questions[i].check != 0){
						html += '<i class="fa fa-times-circle-o text-danger"></i>&emsp;' + userAnswer;
					}
					html += '<br/>';
				}
			}
			else if(data.questions[i].questionType == 6) {
				for(var j = 0; j < data.questions[i].origAnswer.length; j++) {
					var origAnswer = data.questions[i].origAnswer[j].option;
					html += ch[j] + ') ' + data.questions[i].origAnswer[j].option + '&emsp;';
					if(data.questions[i].origAnswer[j].correct == 1)
						html += '<i class="fa fa-check-circle-o text-success"></i>';
					for(var k = 0; k < data.questions[i].userAnswer.length; k++) {
						if(data.questions[i].check == 0)
							var userAnswer = '';
						else
							var userAnswer = data.questions[i].userAnswer[k].answer;
						if(origAnswer == userAnswer && data.questions[i].origAnswer[j].correct == 0) {
							html += '<i class="fa fa-times-circle-o text-danger"></i>';
						}
					}
					html += '<br/>';
				}
			}
			else if(data.questions[i].questionType == 7) {
				for(var j = 0; j < data.questions[i].origAnswer.length; j++) {
					var origAnswer = data.questions[i].origAnswer[j].option;
					if(data.questions[i].check == 0)
						var userAnswer = '';
					else
						var userAnswer = data.questions[i].userAnswer[0].answer;
					html += ch[j] + ') ' + data.questions[i].origAnswer[j].option + '&emsp;';
					if(data.questions[i].origAnswer[j].correct == 1)
						html += '<i class="fa fa-check-circle-o text-success"></i>';
					else if(origAnswer == userAnswer && data.questions[i].check != 0) {
						html += '<i class="fa fa-times-circle-o text-danger"></i>';
					}
					html += '<br/>';
				}
			}
			else if(data.questions[i].questionType == 5) {
				for(var j = 0; j < data.questions[i].childs.length; j++) {
					var tempType = '';
					if(data.questions[i].childs[j].questionType == 0)
						tempType = ' <strong> ( Multiple Choice Questions) </strong>';
					else if(data.questions[i].childs[j].questionType == 1)
						tempType = ' <strong> ( True and False ) </strong>';
					else if(data.questions[i].childs[j].questionType == 2)
						tempType = ' <strong> ( Fill in the blanks ) </strong>';
					else if(data.questions[i].childs[j].questionType == 6)
						tempType = ' <strong> ( Multiple Answer Questions ) </strong>';
					var question = data.questions[i].childs[j].question.substring(0, data.questions[i].childs[j].question.length - 5) + tempType + '</p>';
					html += '<div class="row"><hr><strong>Part ' + (j+1) + '</strong>' + question + '</div>'
					+ '<div class="answer-container"><button class="btn btn-info btn-xs view-sol" data-hidden="0">View Solution</button>'
					+ '<div class="sub-answer" style="display: none;">'
					+ '<strong>Answers</strong><br>';
					if(data.questions[i].childs[j].questionType == 0) {
						for(var k = 0; k < data.questions[i].childs[j].origAnswer.length; k++) {
							var origAnswer = data.questions[i].childs[j].origAnswer[k].option;
							if(data.questions[i].check == 0)
								var userAnswer = '';
							else {
								if(data.questions[i].childs[j].userAnswer.length > 0)
									var userAnswer = data.questions[i].childs[j].userAnswer[0].answer;
								else
									var userAnswer = '';
							}
							html += ch[k] + ') ' + data.questions[i].childs[j].origAnswer[k].option + '&emsp;';
							if(data.questions[i].childs[j].origAnswer[k].correct == 1)
								html += '<i class="fa fa-check-circle-o text-success"></i>';
							else if(origAnswer == userAnswer && data.questions[i].check != 0) {
								html += '<i class="fa fa-times-circle-o text-danger"></i>';
							}
							html += '<br/>';
						}
					}
					else if(data.questions[i].childs[j].questionType == 1) {
						var origAnswer = data.questions[i].childs[j].origAnswer[0].answer;
						if(data.questions[i].check == 0)
							var userAnswer = '';
						else {
							if(data.questions[i].childs[j].userAnswer.length > 0)
								var userAnswer = data.questions[i].childs[j].userAnswer[0].answer;
							else
								var userAnswer = '';
						}
						html += 'True';
						if(origAnswer == 1) {
							html += ' <i class="fa fa-check-circle-o text-success"></i>';
						}
						else if(userAnswer == 1 && data.questions[i].check != 0) {
							html += ' <i class="fa fa-times-circle-o text-danger"></i>';
						}
						html += '<br/>False';
						if(origAnswer == 0) {
							html += ' <i class="fa fa-check-circle-o text-success"></i>';
						}
						else if(userAnswer == 0 && data.questions[i].check != 0)
							html += ' <i class="fa fa-times-circle-o text-danger"></i>';
					}
					else if(data.questions[i].childs[j].questionType == 2) {
						for(var k = 0; k < data.questions[i].childs[j].origAnswer.length; k++) {
							var origAnswer = data.questions[i].childs[j].origAnswer[k].blanks;
							if(data.questions[i].check == 0)
								var userAnswer = '';
							else {
								if(data.questions[i].childs[j].userAnswer.length > 0)
									var userAnswer = data.questions[i].childs[j].userAnswer[0].answer;
								else
									var userAnswer = '';
							}
							html += origAnswer + '&emsp;';
							if(origAnswer == userAnswer)
								html += '<i class="fa fa-check-circle-o text-success"></i>';
							else if(data.questions[i].check != 0){
								html += '<i class="fa fa-times-circle-o text-danger"></i>&emsp;' + origAnswer;
							}
							html += '<br/>';
						}
					}
					else if(data.questions[i].childs[j].questionType == 6) {
						for(var k = 0; k < data.questions[i].childs[j].origAnswer.length; k++) {
							var origAnswer = data.questions[i].childs[j].origAnswer[k].option;
							html += ch[k] + ') ' + data.questions[i].childs[j].origAnswer[k].option + '&emsp;';
							if(data.questions[i].childs[j].origAnswer[k].correct == 1)
								html += '<i class="fa fa-check-circle-o text-success"></i>';
							for(var l = 0; l < data.questions[i].childs[j].userAnswer.length; l++) {
								if(data.questions[i].check == 0)
									var userAnswer = '';
								else {
									if(data.questions[i].childs[j].userAnswer.length > 0)
										var userAnswer = data.questions[i].childs[j].userAnswer[0].answer;
									else
										var userAnswer = '';
								}
								if(origAnswer == userAnswer && data.questions[i].childs[j].origAnswer[k].correct == 0) {
									html += '<i class="fa fa-times-circle-o text-danger"></i>';
								}
							}
							html += '<br/>';
						}
					}
					if(data.questions[i].childs[j].description == '')
						data.questions[i].childs[j].description = '<p>No explanation provided</p>';
					html += '<br/><strong>Explanation</strong>' + data.questions[i].childs[j].description + '<br/></div></div>';
				}
			}
			if(data.questions[i].questionType != 5)
				html += '<br/><strong>Explanation</strong>' + data.questions[i].description + '</div>';
			html += '</td>'
					+ '<td class="text-right">'
					+ '<strong>Marks</strong> +'+data.questions[i].correct+' / -'+data.questions[i].wrong+'<br>'
						+ '<strong>Time</strong> '+data.questions[i].time+'s<br><a class="time-compare"><strong>Compare</strong></a>'
					+ '</td>'
				+ '</tr>';
		}
						html += '</tbody>'
						+ '</table>'
					+ '</section>'
				+ '</div>'  
			+ '</section>';
		$('#sectionDetails').append(html);
		$('#sectionDetails .hide-section:eq(-1)').on('click', function() {
			if($(this).hasClass('fa-minus-square'))
				$(this).removeClass('fa-minus-square').addClass('fa-plus-square');
			else
				$(this).removeClass('fa-plus-square').addClass('fa-minus-square');
			$(this).parents('.panel:eq(0)').find('.panel-body').slideToggle();
		});
		$("#sectionDetails .table.table:eq(-1) .view-solution").on('click', function() {
			var flag = false;
			if($(this).attr('data-hidden') == 1)
				flag = true;
			$.each($('.view-solution'), function() {
				$(this).text($(this).attr('data-text')).attr('data-hidden', 0);
			});
			$('.answer').slideUp();
			if(flag)
				return;
			$(this).parents('td:eq(0)').find('.answer').slideToggle();
			if($(this).attr('data-hidden') == 0) {
				$(this).attr('data-hidden', '1');
				if($(this).attr('data-text') == 'View Sub-Parts')
					$(this).text('Hide Sub-Parts');
				else
					$(this).text('Hide Solution');
			}
			else {
				$(this).attr('data-hidden', '0');
				$(this).text($(this).attr('data-text'));
			}
		});
		$(".view-sol").off('click');
		$(".view-sol").on('click', function() {
			var flag = false;
			if($(this).attr('data-hidden') == 1)
				flag = true;
			$('.view-sol').text('View Solution').attr('data-hidden', 0);
			$('.sub-answer').slideUp();
			if(flag)
				return;
			$(this).parents('.answer-container:eq(0)').find('.sub-answer').slideToggle();
			if($(this).attr('data-hidden') == 0) {
				$(this).attr('data-hidden', '1');
				$(this).html('Hide Solution');
			}
			else {
				$(this).attr('data-hidden', '0');
				$(this).html('View Solution');
			}
		});
		//event handler for compare time click
		$('#sectionDetails .table.table:eq(-1) .time-compare').on('click', function() {
			var req = {};
			var res;
			req.action = 'get-maximum-time-for-question';
			req.questionId = $(this).parents('tr').attr('data-id');
			req.attemptId = attemptId;
			req.studentId=getUrlParameter('studentId');
			req.examId = getUrlParameter('examId');
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				console.log(res);
				if(res.status == 0)
					alert(res.message);
				else {
					/*$('#hidden-time-graph .to-be-added').append('<div id="hero-bar3" class="graph"></div>');
					new Morris.Bar({
						element: 'hero-bar3',
						data: [
						{device: 'You', geekbench: res.self.time},
						{device: 'Topper', geekbench: res.avgMax.min},
						{device: 'Median', geekbench: res.median},
						{device: 'Average', geekbench: res.avgMax.average}
						],
						xkey: 'device',
						ykeys: ['geekbench'],
						labels: ['Time in seconds'],
						barRatio: 0.4,
						xLabelAngle: 35,
						hideHover: 'auto',
						barColors: ['#D8574C']
					});*/
					$('#hidden-time-graph .to-be-added').find('*').remove();
					if(res.min == null || res.min == 0)
						res.min = 'NA';
					else
						res.min += ' seconds';
					var sanswer = '';
					//
					var stuName=res.self.selfName['0'].firstName.concat(' ').concat(res.self.selfName['0'].lastName);
					 
					if(res.self.check == 0)
						sanswer = '(Unattempted <i class="fa fa-dot-circle-o text-warning"></i>)';
					else if(res.self.check == 1)
						sanswer = '(Correct <i class="fa fa-check-circle-o text-success"></i>)';
					else
						sanswer = '(Incorrect <i class="fa fa-times-circle-o text-danger"></i>)';
					var tanswer = '';
					if(res.topper.check == 0)
						tanswer = '(Unattempted <i class="fa fa-dot-circle-o text-warning"></i>)';
					else if(res.topper.check == 1)
						tanswer = '(Correct <i class="fa fa-check-circle-o text-success"></i>)';
					else
						tanswer = '(Incorrect <i class="fa fa-times-circle-o text-danger"></i>)';
						res.avg=parseFloat(res.avg).toFixed(2)
					legendHTML = '<table class="table table-bordered"><tr><td>Time spent by '+stuName+' on this question</td><td>' + res.self.time + ' seconds ' + sanswer + '</td></tr>'
								+ '<tr><td>Average Time spent on this question</td><td>' + res.avg + ' seconds</td></tr>'
								+ '<tr><td>Least time spent on answering the question correctly</td><td>' + res.min + '</td></tr>'
								+ '<tr><td>Time spent by Topper</td><td>' + res.topper.time + ' seconds ' + tanswer + '</td></tr></table>';
					$('#hidden-time-graph .to-be-added').append('<div id="pie-times" style="width: 81%; height: 250px; margin: 0 auto;"></div><br>' + legendHTML);
					$('#hidden-time-graph').css('height', '100%');
					$('#hidden-time-graph').css('width', '100%');
					$('#pie-times').highcharts({
						chart: {
							plotBackgroundColor: null,
							plotBorderWidth: null,
							plotShadow: false
						},
						title: {
							text: 'Total number of students who have got the question are ' + parseInt(res.studentCount.total)
						},
						tooltip: {
							pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions: {
							pie: {
								allowPointSelect: true,
								cursor: 'pointer',
								dataLabels: {
									enabled: true,
									format: '<b>{point.name}</b>: {point.percentage:.1f} %',
									style: {
										color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
									}
								}
							}
						},
						series: [{
							type: 'pie',
							name: 'Number',
							data: [
								['Correct', parseInt(res.studentCount.correct)],
								['Wrong', parseInt(res.studentCount.wrong)],
								['Unattempted', parseInt(res.studentCount.unattempted)]
							]
						}],
						colors: ['green','red','black']
					});
					//this is necessary to resize the pie chart
					$(window).resize();
				}
			});
		});
		sectionCount++;
		fetchAllStudentGraph();
	}
	
	//function to get topper comparison
	function fetchTopperComparison() {
		var req = {};
		var res;
		req.action = 'get-topper-of-exam';
		req.examId = getUrlParameter('examId');
		req.attemptId = attemptId;
		req.studentId= getUrlParameter('studentId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
				fillTopperComparison(res);
		});
	}
	
	//function to fill the compare graphs of topper
	function fillTopperComparison(data) {
		console.log(data);
		var stuName=data.self.selfName['0'].firstName.concat(' ').concat(data.self.selfName['0'].lastName);
		new Morris.Bar({
			element: 'hero-bar1',
			data: [
			{device: stuName, geekbench: data.self.score},
			{device: 'Topper', geekbench: data.avgMax.max},
			{device: 'Median', geekbench: data.median},
			{device: 'Average', geekbench: data.avgMax.average}
			],
			xkey: 'device',
			ykeys: ['geekbench'],
			labels: ['Score'],
			barRatio: 0.4,
			xLabelAngle: 35,
			hideHover: 'auto',
			barColors: ['#D8574C']
		});
		
		new Morris.Bar({
			element: 'hero-bar2',
			data: [
			{device: stuName, geekbench: data.pself.percentage},
			{device: 'Topper', geekbench: data.pavgMax.max},
			{device: 'Median', geekbench: data.pmedian},
			{device: 'Average', geekbench: data.pavgMax.average}
			],
			xkey: 'device',
			ykeys: ['geekbench'],
			labels: ['Percentage'],
			barRatio: 0.4,
			xLabelAngle: 35,
			hideHover: 'auto',
			barColors: ['#56A738']
		});
	}
	
	//function to convert given seconds into minute and seconds
	function convertSecondsIntoMinutes(sec) {
		var min = 0;
		if(sec - 60 >= 0) {
			min = Math.floor(sec / 60);
			sec = sec - (min * 60);
		}
		var time = min + ' minutes ' + sec + ' seconds';
		return time;
	}
	
	
	//function to unset error
	function unsetError(where) {
		if(where.parents('div:eq(0)').hasClass('has-error')) {
			where.parents('div:eq(0)').removeClass('has-error');
			where.parents('div:eq(0)').find('.help-block').remove();
		}
	}
	
	//function to set error
	function setError(where, what) {
		where.parents('div:eq(0)').addClass('has-error');
		where.parents('div:eq(0)').append('<span class="help-block">'+what+'</span>');
	}	
});
function shuffle(o){ //v1.0
	for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
};