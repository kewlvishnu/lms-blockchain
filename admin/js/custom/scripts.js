/*
	Common JS scripts

*/

var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var userRole;
var defaultImagePath = "img/default.jpg";

$( document ).ajaxStart(function() {
  $('#loader_outer').show();
});

$( document ).ajaxComplete(function() {
  $('#loader_outer').hide();
});

function formatContactNumber(user) {
	var num1= "", num2 = "";
	if(user.contactMobilePrefix != '' && user.contactMobile != '') {
		num1 = user.contactMobilePrefix + '-' + user.contactMobile;
	}
	
	if(user.contactLandlinePrefix != '' && user.contactLandline != '') {
		num2 = user.contactLandlinePrefix + '-' + user.contactLandline;
	}
	
	return (num1 + ', ' + num2).replace(/,\s$/,'').replace(/^,\s/,'');
}

function isValidDate(s) {
  var bits = s.split('-'); 
//  if (s.indexOf("/") >= 0){
//    bits = s.split('/');  
//  }else{
//      bits = s.split('-');
//  }
  //alert(bits[0]+ ''+ bits[1]+''+bits[2]+''+bits[3]);
  if(bits.length != 3)
	return false;
  var y = bits[2], m  = bits[1], d = bits[0];
  // Assume not leap year by default (note zero index for Jan)
  var daysInMonth = [31,28,31,30,31,30,31,31,30,31,30,31];

  // If evenly divisible by 4 and not evenly divisible by 100,
  // or is evenly divisible by 400, then a leap year
  if(y < 1970 || y > 2050)
	return false;
  if ( (!(y % 4) && y % 100) || !(y % 400)) {
	daysInMonth[1] = 29;
  }
  return d <= daysInMonth[--m];
}

function isValidPrice(n) {
	var valid = !isNaN(parseFloat(n)) && isFinite(n);
	if(valid == true){
		return n > 0;
	}
	return false;
		
}