var sortOrder = [];
var num = [];
var attempt = [];
var allowed;
var attemptFill = [];
var total = 0;
var questionId=0;
var questions=[];
var qids=[];

$(function() {
	fetchBreadcrumb();
	//fetchProfileDetails();
	fetchQuestionList();
	fetchTimeLine();
	fetchAllStudentGraph();
	fetchTopperComparison();
	//console.log('check');
	
	function fetchBreadcrumb() {
		var req = {};
		var res;
		req.action = "get-breadcrumb-for-exam";
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
				fillBreadcrumb(res);
		});
	}
	
	function fillBreadcrumb(data) {
		$('ul.breadcrumb li:eq(0)').find('a').attr('href', 'edit-course.php?courseId=' + data.course.courseId).text(data.course.courseName);
		$('ul.breadcrumb li:eq(1)').find('a').attr('href', 'edit-subject.php?courseId=' + data.course.courseId + '&subjectId=' + data.subChap.subjectId).text(data.subChap.subjectName);
		if(data.subChap.chapterId == null)
			$('ul.breadcrumb li:eq(2)').find('a').html('<strong>Exam Analysis</strong>');
	}
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	//download-list
	$('#download-list').on('click', function () {
		$('#downloadcustom').modal('show');
		$('#downloadCustom-list').on('click', function () {
			var avg=1;
			var highest=0;
			var attempts=0;
			var lastscore=0;
			if($('#cbox1').is(':checked'))
			{
				avg=1;
			}			
			else{
				avg=0;
			}
			if($('#cbox2').is(':checked'))
			{
				highest=1;
			}			
			if($('#cbox3').is(':checked'))
			{
				attempts=1;
			}			
			if($('#cbox4').is(':checked'))
			{
				lastscore=1;
			}			
				
			var req = {};
			var res;
			req.action = 'get-student-list-download';
			req.examId = getUrlParameter('examId');
			req.avg=avg;
			req.highest=highest;
			req.attempts=attempts;
			req.lastscore=lastscore;
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndpoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				//console.log(res);
				if(res.status == 1){
					var url = res.path;
					window.location.href = url;
					$('#downloadcustom').modal('hide');
					//window.open(url, '_blank');
				}				
				else {
					
					if(res.students.length==0)
					{
						alertMsg("There are no students in the course");
					}else{
						alertMsg(res.message);	
					}
				}
		});
		
	});
		
		
	});
	
	//function to fetch student list who have attempted the exam
	function fetchQuestionList() {
		var req = {};
		var res;
		req.action = 'get-question-list';
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			//console.log(res);
			if(res.status == 0)
				alertMsg(res.message);
			else {
				
				fillQuestionList(res);   //to check the front end
				
			}
		});
	}
	
	
	function fillQuestionList(data) {
		//console.log(data);
		var html = '';
		//detecting the highest and lowest scorers
		//data=data.questions[0];
		var counter=1;
		for(var j = 0; j < data.questions.length; j++){
			for(var i = 0; i < data.questions[j].length; i++) {
				questions.push(data.questions[j][i].question);
				qids.push(data.questions[j][i].id);
				html += '<tr>'
						+ '<td>' + parseInt(counter) + '</td>'
						+ '<td> <a class="detailQuestion" data-cid="'+data.questions[j][i].id+'">' + data.questions[j][i].question + '</a></td>'
						+ '<td>' + ((data.studentGotQues[counter-1] == undefined)?0:data.studentGotQues[counter-1]) + '</td>'
						+ '<td>' + ((data.studentIncorrect[counter-1] == undefined)?0:data.studentIncorrect[counter-1])   + '</td>'
						+ '<td>' + ((data.studentCorrect[counter-1] == undefined)?0:data.studentCorrect[counter-1])   + '</td>'
						+ '<td>' + ((data.studentNA[counter-1] == undefined)?0:data.studentNA[counter-1])  + '</td>'
						+ '<td>' + parseFloat(data.studentavg[counter-1]).toFixed(2) + '</td>'
						+ '<td>' +  ((data.studentTT[counter-1]==0)?'Unattempted':parseFloat(data.studentTT[counter-1]).toFixed(2))  + '</td>'
						+ '<td>' + parseFloat(data.studentfT[counter-1]).toFixed(2)  + '</td>'
						+ '<td>' + ' ' + '</td>'
					+ '</tr>';
					counter++;
			}
		}
		
		
		$('#questionList tbody').append(html);
		$('#questionList').show();
		$('#questionList').dataTable( {
			"aaSorting": [[ 0, "asc" ]]
		});
		
		
		
	}
	$("#questionList").on("click",".detailQuestion",function(){
	  questionId=$(this).attr('data-cid');
	  fetchCompleteQuestionAnalysis();
	});
	$('#detailexamAnalysis').on('hidden.bs.modal', function() {
		//console.log("i-ooooutside");
		$('#detail-questionComplete').dataTable().fnClearTable();
		//$(this).removeData('bs.modal');
		//$(this).data('bs.modal', null);
		
	});

	
	function fetchCompleteQuestionAnalysis() {
		var req = {};
		var res;
		req.action = 'getCompleteQuestionAnalysis';
		req.quesId = questionId;
		req.examId = getUrlParameter('examId');;
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			
			if(res.status == 0)
				alert(res.message);
			else
			{	
				fillCompleteQuestionAnalysis(res);
			}
				
		});
	}
	function fillCompleteQuestionAnalysis(data)
	{
		$("#detail-questionComplete").css("width","100%");
		//console.log(data);
		var html='';
		var index=jQuery.inArray(questionId,qids);
		$('#questioncomplete').html(questions[index]);
		//$('#detail-questionComplete tbody').html('');
		var counter=1;
		for(var j = 0; j < data.studentId.length; j++){
			html += '<tr>'
					+ '<td>' + parseInt(counter) + '</td>'
					+ '<td>' + data.studentNames[counter-1] + '</td>'
					+ '<td>' + data.quesTimes[counter-1]   + '</td>'
					+ '<td>' + data.correct[counter-1]   + '</td>'
					+ '<td>' + data.inCorrect[counter-1]  + '</td>'
					+ '<td>' + data.unAttempted[counter-1]  + '</td>'
					+ '<td>' + ((data.avgTime[counter-1]==0)?('<span class="legend text-danger">NCA</span>'):(parseFloat(data.avgTime[counter-1]).toFixed(2)))  + '</td>'
					+ '<td>' + ((data.avgTime[counter-1]==0)?('<span class="legend text-danger">NCA</span>'):(parseFloat(data.minTime[counter-1]).toFixed(2)))  + '</td>'
					+ '<td>' + ' ' + '</td>'
				+ '</tr>';
				counter++;
		}
		
		$('#detail-questionComplete tbody').html(html);
		$('#detail-questionComplete').show();
		$('#detail-questionComplete').dataTable( {
			"aaSorting": [[ 0, "asc" ]],
			"bDestroy": true
		});
		
		
		$('#detailexamAnalysis').modal('show');
	}
	
	
	function fetchTimeLine() {
		var req = {};
		var res;
		req.action = 'getAllStudentTimeLineGraphAll';
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
			{	
				initTimeLine(res);
				
			}
				
		});
	}

	//function to get graphs vs time graph attempt independent
	function initTimeLine(data) {
		/**
		 * Create the chart when all data is loaded
		 * @returns {undefined}
		 */
		 $('#graph').show();
		var timepoints=[];
		var timepoints2=[];
		var selfMarks=[];
		var dataset = [];
		var avgStudentCount=[];
		var avgStudentMarks=[];
		var toppermarks=[];
		var avgMarks=[];
		var attempts=[];
		var lastday=$.now();//parseInt(data.lastday[0]['endDate'])* parseInt(1000);
		//var today=$.now();
		
		
		$.each(data.avgScore, function (j, avgScore) {
			avgStudentCount[j]=0;
			timepoints2[j]=0;
			avgStudentMarks[j]=0;
			toppermarks[j]=0;
		});
		var lastpoint=timepoints[timepoints.length-1];
		var lastflag=0;
		$.each(data.avgScore, function (j, avgScore) {
			timepoints2[j]=parseInt(avgScore.endDate)* parseInt(1000);
			avgStudentMarks[j]=parseFloat(avgStudentMarks[j])+parseFloat(avgScore.score);
			avgStudentCount[j]=parseInt(avgStudentCount[j])+parseInt(1);
			//calculating for toppermarks
			if(j!=0)
			{
				//to calculate highest from previous
				if(avgScore.score>toppermarks[j-1])
				{
					toppermarks[j]=parseFloat(avgScore.score);
				}else{
					toppermarks[j]=parseFloat(toppermarks[j-1]);
				}
			}else{
			
				toppermarks[j]=parseFloat(avgScore.score);
			}			
			
		});
		for(var i=0;i<data.avgScore.length;i++){
			if(avgStudentCount[i]!=0)
				avgMarks[i]=parseFloat(avgStudentMarks[i])/parseFloat(avgStudentCount[i]);
			else
			avgMarks[i]=0;
						
		}
		//we got avg as whole it will same for all students	
		//console.log(lastflag);
		//console.log(timepoints2);
			
			timepoints2[timepoints2.length]=parseInt(lastday);
			///timepoints2[timepoints2.length]=parseInt(lastpoint);
			//console.log(lastpoint+"cjj"+lastday);
			avgMarks[avgMarks.length]=parseFloat(avgMarks[avgMarks.length-1]);
			toppermarks[toppermarks.length]=parseFloat(toppermarks[toppermarks.length-1]);
			
		var jCompare=0;
		var flag=0;
		
		//console.log(timepoints2);
		//graph to adjust and insert self timestamp
		/*for(var i=0;i<timepoints2.length-1;i++){
			var compareTime=timepoints[jCompare];
			//console.log(compareTime+"---check--"+timepoints2[i]+"--check2--"+timepoints2[i+1]);
			if(jCompare>(timepoints.length-1))
			{
				jCompare=timepoints.length-1;
			}
			if(timepoints2[i]<compareTime && timepoints2[i+1]>compareTime )	
			{	
				jCompare=parseInt(jCompare)+parseInt(1);
				
						var highmarks=parseFloat(toppermarks[i+1]);
						var avg=(parseFloat(avgMarks[i]));
						timepoints2.splice(i, 0,compareTime );
						toppermarks.splice(i, 0,highmarks );
						avgMarks.splice(i, 0,avg );
				
				
			}
			else if(timepoints2[i]==compareTime)
			{
				jCompare=parseInt(jCompare)+parseInt(1);
				
			}else if(timepoints2[i+1]==compareTime){
				
				jCompare=parseInt(jCompare)+parseInt(1);
				// return true;
			}else{
				if(i!=0)
				{
					if(timepoints2[i-1]<compareTime && timepoints2[i]>compareTime )
					{	//console.log('yes');
					
						jCompare=parseInt(jCompare)+parseInt(1);
						//console.log(timepoints2)
					
							var highmarks=parseFloat(toppermarks[i]);
							var avg=(parseFloat(avgMarks[i-1]));
							timepoints2.splice(i-1, 0,compareTime );
							toppermarks.splice(i-1, 0,highmarks );
							avgMarks.splice(i-1, 0,avg );
						
					}
					
				}
			}		
			
		  }
		
		*/
		for(var k=0;k<timepoints.length;)
		{
			for(var i=0;i<timepoints2.length-1;i++)
			{	
				if(timepoints[k]==timepoints2[i])
				{
					//console.log(timepoints[k]);
					//k++;
					break;
				}	
				else if(timepoints[k]>timepoints2[i] &&timepoints[k]<timepoints2[i+1] )
				{		//console.log(timepoints[k]+'check'+timepoints2[i]);
						if(timepoints2[i]>timepoints[k])
						{
							var highmarks=parseFloat(toppermarks[i+1]);
							var avg=(parseFloat(avgMarks[i+1]));
							timepoints2.splice(i, 0,timepoints[k]);
							toppermarks.splice(i, 0,highmarks );
							avgMarks.splice(i, 0,avg );
							break;
						}
						else{
							var highmarks=parseFloat(toppermarks[i+1]);
							var avg=(parseFloat(avgMarks[i+1]));
							timepoints2.splice(i+1, 0,timepoints[k]);
							toppermarks.splice(i+1, 0,highmarks );
							avgMarks.splice(i+1, 0,avg );
							break;
						}
					   
						//k++;
				}
				else{
					//console.log("check");
					//console.log(timepoints[k]+'check'+timepoints2[i]+'check'+timepoints2[i]);
					//k++;
				}
				//k++;
			}
			k++;
		}
		for(var i=0;i<timepoints2.length-1;i++)
		{
			if(timepoints2[i]==timepoints2[i+1])
			{
				timepoints2.splice(i, 1);
				avgMarks.splice(i, 1);
				toppermarks.splice(i,1);
			}
			
		}
	
		var scoresOptions = [],
			scoresCounter = 0,
			newnames = ['avgScore','topper'];
		function createChart2() {
			//console.log(scoresOptions);
			var i=0;
			
			$('#graph12').highcharts('StockChart', {
				rangeSelector: {
					selected: 4
				},
				xAxis: {       
						    
							//ordinal: false
						},
				yAxis:[ {
						labels: {
							  align: 'left',
                			  x: 15,
							formatter: function () {
									return  this.value ;
							}
						},
						//min: 0,
						plotLines: [{
							value: 0,
							width: 2,
							color: 'silver'
						}]
					},
				 { // Secondary yAxis
		            labels: {
		                formatter: function () {
								return  this.value ;
								//return point.y;
							}
		            },
					linkedTo:0,
		            opposite: false
	       		}],
				plotOptions: {
					series: {
						//compare: 'percent'
						//pointStart:timepoints[0],
            			//pointInterval: 3600 * 1000
					marker : {
		                    enabled : true,
		                    radius : 3
		                }
					}
				},legend: {
				            enabled: true,
							layout: 'vertical',
				            verticalAlign: 'middle',
				            borderWidth: 0
				        },
	
				tooltip: {
					pointFormat: '<span >{series.name}</span>: <b>{point.y}</b><br/>'
						
				},
	
				series: scoresOptions
			});
			$('.highcharts-input-group').hide();
		}
		
		//declaring array for data points calculation
		var elementarray1=[];
		var elementarray2=[];
		var elementarray3=[];
		var elementarray4=[];
		
		//calculating datapoint for avg 
		for(var i = 0; i < avgMarks.length; i++) {
			elementarray=[];
			elementarray[0] = parseInt(timepoints2[i]);
			elementarray[1] = parseInt(avgMarks[i]);
			elementarray2[i]=elementarray;
			
		}
		scoresOptions[0] = {
								name: 'Class Average',
								data: elementarray2,
								dashStyle: 'longdash',
								type: 'spline'
							};
		for(var i = 0; i < toppermarks.length; i++) {
			elementarray=[];
			elementarray[0] = parseInt(timepoints2[i]);
			elementarray[1] = parseInt(toppermarks[i]);
			elementarray3[i]=elementarray;
		}
		scoresOptions[1] = {
								name: 'Class Highest',
								data: elementarray3,
								type: 'spline',
								dashStyle:'ShortDash'
							};
		
		
			
		createChart2();	
		//				
	}
	

	function fetchAllStudentGraph() {
		var req = {};
		var res;
		req.action = 'get-allStudentNormalization-graph';
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else{
				initAllStudentGraph(res);
			}
				
		});
	}
	
	//function to initiate attempt graph
	function initAllStudentGraph(data) {
		//console.log(data.total[0]['total']);
		//console.log(data);
		var totalScore=parseFloat(data.total[0]['total']);
		var differnece=totalScore-parseFloat(data.highest);
		var blocksDiff=(totalScore-parseFloat(differnece+parseFloat(data.lowest)))/10;
		//var marksDis=[];
		var students=[];
		var markPer=[];
		var newlowest=(parseFloat(data.lowest)+parseFloat(differnece)).toFixed(1);
		for(var i=1;i<11;i++)
		{	//marksDis[i-1]=(parseFloat(newlowest)+parseFloat(blocksDiff*i)).toFixed(1);
			students[i-1]=0;
			//checking
			var temp=(parseFloat(newlowest)+parseFloat(blocksDiff*i)).toFixed(1);
			markPer[i-1]=Math.round((temp/totalScore)*100)+'%';
			//markPer[i-1]=markPer[i-1]+'%';
		}
		for(var i=markPer.length-1;i>0;i--)
		{
			markPer[i]= markPer[i-1]+' - '+markPer[i];
		}
		
		markPer[0]=Math.round(newlowest)+'% - '+markPer[0];
		//console.log(markPer);
		$.each( data.graph, function( key, value ) {
			var newscore=(parseFloat(value['score'])+parseFloat(differnece)).toFixed(1);
			var index=Math.round(((newscore-newlowest)/blocksDiff));
			if(index>0)
			{	index=index-1;
			}else{
				index=0;
			}
			students[index]=students[index]+1;
		});

		$('#hero-bar3').highcharts({
			chart: {
				zoomType: 'xy'
			},
			title: {
				text: 'Percentage distribution of all students'
			},
			subtitle: {
				text: 'This graph shows percentage w.r.t to highest scorer'
			},
			xAxis: [
			{
				title: {
					text: 'Percentage Score',
					style: {
						color: Highcharts.getOptions().colors[1]
					}
				},
				categories: markPer,
				crosshair: true
			}],
			yAxis: [{ // Primary yAxis
				labels: {
					format: '{value}',
					style: {
						color: Highcharts.getOptions().colors[1]
					}
				},
				title: {
					text: 'No. of Students',
					style: {
						color: Highcharts.getOptions().colors[1]
					}
				}
			}, { // Secondary yAxis
				title: {
					text: 'Students',
					style: {
						color: Highcharts.getOptions().colors[0]
					}
				},
				labels: {
					format: '{value}',
					style: {
						color: Highcharts.getOptions().colors[0]
					}
				},
				opposite: true
			}],
			tooltip: {
				shared: true
			},
			legend: {
				layout: 'vertical',
				align: 'left',
				x: 120,
				verticalAlign: 'top',
				y: 100,
				floating: true,
				backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
			},
			series: [{
				name: 'Students',
				type: 'column',
				yAxis: 1,
				data: students,
				tooltip: {
					valueSuffix: ''
				}

			}, {
				name: 'No. of Students',
				type: 'spline',
				yAxis: 1,
				data: students,
				tooltip: {
					valueSuffix: ''
				}
			}]
		});
		//$('.percAllStudents').show();
	}



		
	//function to get topper comparison
	function fetchTopperComparison() {
		var req = {};
		var res;
		req.action = 'get-topper-of-examAll';
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndPoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
				fillTopperComparison(res);
		});
	}
	
	//function to fill the compare graphs of topper
	function fillTopperComparison(data) {
		//console.log(data);
		//var stuName=data.self.selfName['0'].firstName.concat(' ').concat(data.self.selfName['0'].lastName);
		new Morris.Bar({
			element: 'hero-bar1',
			data: [
			{device: 'Topper', geekbench: data.avgMax.max},
			{device: 'Median', geekbench: data.median},
			{device: 'Average', geekbench: data.avgMax.average}
			],
			xkey: 'device',
			ykeys: ['geekbench'],
			labels: ['Score'],
			barRatio: 0.4,
			xLabelAngle: 35,
			hideHover: 'auto',
			barColors: ['#D8574C']
		});
		
		new Morris.Bar({
			element: 'hero-bar2',
			data: [
			{device: 'Topper', geekbench: data.pavgMax.max},
			{device: 'Median', geekbench: data.pmedian},
			{device: 'Average', geekbench: data.pavgMax.average}
			],
			xkey: 'device',
			ykeys: ['geekbench'],
			labels: ['Percentage'],
			barRatio: 0.4,
			xLabelAngle: 35,
			hideHover: 'auto',
			barColors: ['#56A738']
		});
	}
	
});