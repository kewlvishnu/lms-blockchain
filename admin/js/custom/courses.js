/*
	Author: Fraaz Hashmi.
	Dated: 6/28/2014
	
	
	********
	1) Notes: JSON.stringify support for IE < 10
*/
var userRole;
var reorderTimer = null;
$(function () {
	var imageRoot = '/';
	$('#loader_outer').hide();
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fetchCourses() {
		var req = {};
		var res;
		req.action	= "get-all-courses";
		req.slug	= $('#slug').val();
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillCourses(res);
		});
	}
	
	function fillCourses(data) {
		var html = "";
		var flag = false;
		var editCourseLink,editSubjectLink,addSubjectLink, deletedClass;
		deletedClass = "";
		if(data.courses.length > 0) {
			$.each(data.courses, function(i, course) {
				flag = true;
				editCourseLink = "edit-course.php?courseId=" + course.id;
				addSubjectLink = "add-subject.php?courseId=" + course.id;
				courseId = "C" + String("000" + course.id).slice(-4);
				deletedClass = "";
				if(course.deleted == 1) {
					deletedClass = " class='deleted-item'";
				}
				html += "<tr data-cid='" + course.id + "'" + deletedClass + ">"
					+ "<td>" + courseId + "</td>"
					+ "<td><h5><a href='" + editCourseLink + "'>" + course.name + "</a></h5></td><td>";
							var temp = '';
				$.each(course.subjects, function(i, subject) {
					editSubjectLink = "edit-subject.php?courseId=" + subject.courseId + "&subjectId=" + subject.id;
					temp += "<a href='" + editSubjectLink + "'>" + subject.name + "</a>, ";
				});
							temp=temp.substring(0,temp.length-2);
							html += temp;
					
				html += "<br /><a class='btn-bg btn btn-danger btn-xs' href='" + addSubjectLink + "'><i class='fa fa-plus'></i> Subject</a>";
						var	startDate = new Date(parseInt(course.liveDate));
						var displayStartDate=startDate.getDate()+ ' ' +MONTH[startDate.getMonth()]+ ' ' + startDate.getFullYear();
						var displayendDate='';
							if(course.endDate!=''){
								var endate=new Date(parseInt(course.endDate));
								displayendDate=endate.getDate()+ ' ' +MONTH[endate.getMonth()]+ ' ' + endate.getFullYear();    
							}else{
								displayendDate='--';
							}
							html += "</td>"
						+ "<td>" + displayStartDate+ "</td>"
						+ "<td>" + displayendDate+ "</td>";
						//+ "<td>Life Time</td>";
				if(course.approved == -1)
					html += "<td><a href='#' type='button' data-toggle='tooltip' data-placement='top' title='' class='tooltips request-approval-link'"
							+" data-original-title='Send request to admin'>"
							+ "<span class='label label-success label-mini' style='display:block;'>Send Request</span></a></td>";
				else if(course.approved == 1)
					html += "<td><a href='#' type='button' data-toggle='tooltip' data-placement='top' title='' class='tooltips'"
							+ " data-original-title='Course approved for publishing on  student  & content market place' >"
							+ "<span class='label label-success label-mini' style='display:block;'>Approved</span></a></td>";
				else if(course.approved == 2)
					html += "<td><a href='#' type='button' data-toggle='tooltip' data-placement='top' title='' class='tooltips request-approval-link'"
							+ " data-original-title='Resend'>"
							+ "<span class='label label-danger label-mini' style='display:block;'>Resend</span></a></td>";
				else if(course.approved == 0)
					html += "<td><a href='#' type='button' data-toggle='tooltip' data-placement='top' title='' class='tooltips'"
							+ " data-original-title='Approved pending'>"
							+ "<span class='label label-warning label-mini' style='display:block;'>Pending</span></a></td>";
				html += "<td>";
						//+ "<button class='btn btn-success btn-xs tooltips' title='' data-placement='top' data-toggle='tooltip' type='button' data-original-title='Content is eligible to license (sell) to Professors/Institutes/Publishers'><i class='fa fa-check'></i></button>&nbsp;"
						//+ "<a href='#view_details' data-toggle='modal'><button class='btn btn-primary btn-xs tooltips' title='' data-placement='top' data-toggle='tooltip' type='button' data-original-title='View details   about the Course and the Subject Licenses'><i class='fa fa-eye'></i></button></a>&nbsp;";
				if(course.deleted == 0){
					html +=	"<button class='btn btn-danger btn-xs tooltips delete-course' title='' data-placement='top' data-toggle='tooltip' type='button' data-original-title='Delete the Course'><i class='fa fa-trash-o'></i></button>&nbsp;";
					html +=	"<button class='btn btn-success btn-xs tooltips restore-course hidden-on-load' title='' data-placement='top' data-toggle='tooltip' type='button' data-original-title='Restore the Course'><i class='fa fa-refresh'></i></button>&nbsp;";
				} else {
					html +=	"<button class='btn btn-success btn-xs tooltips restore-course' title='' data-placement='top' data-toggle='tooltip' type='button' data-original-title='Restore the Course'><i class='fa fa-refresh'></i></button>&nbsp;";
					html +=	"<button class='btn btn-danger btn-xs tooltips delete-course hidden-on-load' title='' data-placement='top' data-toggle='tooltip' type='button' data-original-title='Delete the Course'><i class='fa fa-trash-o'></i></button>&nbsp;";
				}
				html += "</td>"
					+	"</tr>";
			});
		}
		
		if(flag == true) {
			$('#all-courses tbody').html('').html(html);
			$('.tooltips').tooltip();
			addEventHandlers();
			makeCoursesSortable('#all-courses tbody');
		}
		else {
			$('#all-courses').hide();
			$('.panel-heading').after('<span style="margin: 1%;">No courses found. Please create one course <a href="add-course.php">here</a>.</span>');
		}
	}
	
	function makeCoursesSortable(selector) {
		$(selector).sortable({
			items: '> tr',
			forcePlaceholderSize: true,
			placeholder:'sort-placeholder',
			start: function (event, ui) {
				// Build a placeholder cell that spans all the cells in the row
				var cellCount = 0;
				$('td, th', ui.helper).each(function () {
					// For each TD or TH try and get it's colspan attribute, and add that or 1 to the total
					var colspan = 1;
					var colspanAttr = $(this).attr('colspan');
					if (colspanAttr > 1) {
						colspan = colspanAttr;
					}
					cellCount += colspan;
				});

				// Add the placeholder UI - note that this is the item's content, so TD rather than TR
				ui.placeholder.html('<td colspan="' + cellCount + '">&nbsp;</td>');
				//$(this).attr('data-previndex', ui.item.index());
			},
			update: function(event, ui) {
				// gets the new and old index then removes the temporary attribute
				var newOrder = $.map($(this).find('tr'), function(el) {
					return $(el).attr('data-cid')+ "-" +$(el).index();
				});
				if(reorderTimer != null)
					clearTimeout(reorderTimer);
				reorderTimer = setTimeout(function(){updateCourseOrder(newOrder);}, 5000);
			},
			helper: function(e, ui) {
				ui.children().each(function() {
					$(this).width($(this).width());
				});
				return ui;
			}
			
		}).disableSelection();
	}
	
	function updateCourseOrder(newOrder) {
		var order = {};
		$.each(newOrder, function(i, v){
			order[v.split('-')[0]] = v.split('-')[1];
		});
		var req = {};
		var res;
		req.newOrder = order;
		req.action = "update-course-order";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1)
				fetchCourses();
			else
				alertMsg(res.message);
		});
	}
	
	//fetchProfileDetails();
	fetchCourses();
	
	
});

function addEventHandlers() {
	$('#all-courses .delete-course').on('click', function(e) {
		e.preventDefault();
		var con = confirm("Are you sure to delete this course.");
		if(con) {
			var course = $(this).parents('tr');
			var cid = course.attr('data-cid');
			var req = {};
			req.action = "delete-course";
			req.courseId = cid;
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 1) {
					course.remove();
					alertMsg("Course Deleted. Please contact admin to restore this course.");
				} else {
					alertMsg(res.message);
				}
			});
		}
	});
	
	/*$('#all-courses .restore-course').on('click', function(e) {
		e.preventDefault();
		var course = $(this).parents('tr');
		var cid = course.attr('data-cid');
		var req = {};
		req.action = "restore-course";
		req.courseId = cid;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				course.find('.restore-course').hide();
				course.find('.delete-course').show();
				course.removeClass('deleted-item');
			} else {
				alertMsg(res.message);
			}
		});
	});*/

	$('#all-courses .request-approval-link').on('click', function(e) {
		e.preventDefault();
		var req = {};
		var course = $(this).parents('tr');
		var cid = course.attr('data-cid');
		req.courseId = cid;
		req.action = "request-course-approval";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				alertMsg(res.message);
				$('.request-approval-link').unbind('click').find('span').removeClass('btn-success').removeClass('btn-danger').addClass('btn-warning')
					.html('Pending');
				
			} else {
				alertMsg(res.message);
			}
		});
	});
}