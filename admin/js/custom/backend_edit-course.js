/*
 Author: Fraaz Hashmi.
 Dated: 6/26/2014
 
 
 ********
 1) Notes: JSON.stringify support for IE < 10
 */

var userRole;
var newSelectedInstitutes = {};
var courseSubject = {};
var courseImageChanged = false;
var reorderTimer = null;
var instituteId = getUrlParameter('instituteId');

$(function () {
    var imageRoot = '/';
    $('#loader_outer').hide();

    function fetchProfileDetails() {
        var req = {};
        var res;
        req.action = "get-profile-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillProfileDetails(res);
        });
    }

    function fillProfileDetails(data) {
        // General Details
        var imageRoot = 'user-data/images/';
        $('#user-profile-card .username').text(data.loginDetails.username);

        if (data.profileDetails.profilePic != "") {
            $('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
        }

        // Role Specific
        userRole = data.userRole;
        if (data.userRole == '1') {
            fillInstituteDetails(data);
        }
        else if (data.userRole == '2') {
            fillProfessorDetails(data);
        }
        else if (data.userRole == '3') {
            fillPublisherDetails(data);
        }
    }

    function fetchCourseDetails() {
        var req = {};
        var res;
        req.courseId = getUrlParameter('courseId');

        if (req.courseId == "" || req.courseId == null) {
            window.location.href = "profile.php";
            return;
        }
        req.action = "get-course-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillCourseDetails(res);
        });
    }

    function fillCourseDetails(data) {
        // General Details
        if (data.courseDetails.deleted == 1)
            alertMsg('This course has been deleted!!', -1);
        var imageRoot = 'user-data/images/';
        var aSM, aCM, licensingLevel;
        var catIds = [], catNames = [];
        var catName=[];
        $('.add-subject-link').attr('href', $('.add-subject-link').attr('href') + '?courseId=' + data.courseDetails.id);
        $('#course-name').html(data.courseDetails.name);
        $('#edit-form #course-name').val(data.courseDetails.name);

        //BCT
        $('.breadcrumbs .course-name').html(data.courseDetails.name).attr('href', 'edit-course.php?courseId=' + data.courseDetails.id);

        // Approval Link
        if (data.courseDetails.approved == -1) {
            $('.request-approval-link').show();
        } else if (data.courseDetails.approved == 0) {
            $('.request-approval-link').removeClass('btn-success').addClass('btn-warning').unbind('click').html('<i class="fa fa-user"></i> Pending');
            $('.request-approval-link').show();
        } else if (data.courseDetails.approved == 1) {
            $('.market-place-link').show();
            //$('#avail-student, #avail-professor').show();

        } else if (data.courseDetails.approved == 2) {
            $('.request-approval-link').removeClass('btn-success').addClass('btn-danger').unbind('click').html('<i class="fa fa-user"></i> Resend');
            $('.request-approval-link').show();
        }
        $('#course-id').html("C" + String("000" + data.courseDetails.id).slice(-4));


        if (data.courseDetails.subtitle != '') {
            $('#course-subtitle').html(data.courseDetails.subtitle);
            $('#edit-form  #course-subtitle').val(data.courseDetails.subtitle);

        }
        if (data.courseDetails.targetAudience != '') {
            $('#target-audience').html(data.courseDetails.targetAudience);
            $('#edit-form #target-audience').val(data.courseDetails.targetAudience);
        }
        if (data.courseDetails.description != '') {
            $('#course-description').html(data.courseDetails.description);
            $('#edit-form #course-description').val(data.courseDetails.description);
        }

        startDate = new Date(parseInt(data.courseDetails.liveDate));
        $('#live-date').html(startDate.getDate() + '/' + (startDate.getMonth() + 1) + '/' + startDate.getFullYear());
        $('#edit-form #live-date').val(startDate.getDate() + '/' + (startDate.getMonth() + 1) + '/' + startDate.getFullYear());

        if (data.courseDetails.endDate != '') {
            $('#end-date').html(data.courseDetails.endDate);
            $('#edit-form #end-date').val(data.courseDetails.endDate);
            $('#edit-form #set-end-date').val(1);
            $('#edit-form #end-date').parents('.form-group').show();
        }

        $.each(data.courseCategories, function (k, cat) {
            //catNames.push(cat.catName);
            catIds.push(cat.categoryId);
            catName.push(cat.category);
        });
        $('#course-categ').html(catName.join(', '));
        $('#edit-form #course-cat').selectpicker('val', catIds)
        if (data.courseDetails.availStudentMarket == 1) {
            aSM = "Yes";
            $('#student-price').parents('.hidden-on-load').show();
            $('#edit-form #avail-student-market').val(1);
            $('#edit-form #student-price').parent().show();
        } else {
            aSM = "No";
        }
        if (data.courseDetails.availContentMarket == 1) {
            aCM = "Yes";
            $('#licensing-level').parents('.hidden-on-load').show();
            $('#edit-form #avail-content-market').val(1);
            $('#edit-form #licensing-level').parent().show();
        } else {
            aCM = "No";
        }
        $('#avail-student-market').html(aSM);
        $('#avail-content-market').html(aCM);
        $('#student-price').html(data.courseDetails.studentPrice);
        $('#edit-form #student-price').val(data.courseDetails.studentPrice);

        if (data.courseDetails.licensingLevel == 1) {
            licensingLevel = 'Subject Level';
        } else if (data.courseDetails.licensingLevel == 2) {
            licensingLevel = 'Course Level';
        }
        $('#licensing-level').html(licensingLevel);
        $('#edit-form #licensing-level').val(data.courseDetails.licensingLevel);

        if (data.courseDetails.licensingLevel == 2) {
            $('#licensing-link').parents('.form-group').show();
        }

        if (data.courseDetails.image == '') {
            var image = "img/course-image-0-uid.jpg";
        } else {
            var image = data.courseDetails.image;
        }

        $('#course-image-preview').attr('src', image);
        $('#edit-form #course-image-preview').attr('src', image);

        // Licensing Options
        //$('#apply-licensing').modal('show');
        $.each(data.courseLicensing, function (i, v) {
            if (v.licenseId == 1) {
                $('#apply-licensing #lic-opt-1').prop('checked', true);
                $('#apply-licensing #lic-opt-1-price').val(v.price).parents('.form-group').show();

            }
            if (v.licenseId == 2) {
                $('#apply-licensing #lic-opt-2').prop('checked', true);
                $('#apply-licensing #lic-opt-2-price').val(v.price).parents('.form-group').show();

            }
            if (v.licenseId == 3) {
                $('#apply-licensing #lic-opt-3').prop('checked', true)
                $('#apply-licensing #lic-opt-3-price').val(v.price).parents('.form-group').show();

            }
            if (v.licenseId == 4) {
                $('#apply-licensing #lic-opt-4').prop('checked', true)
                data = $.parseJSON(v.data);
                $('#apply-licensing #lic-opt-4-comm').val(data.amount);
                $('#apply-licensing #lic-opt-4-comm-per').val(data.percent);
                $('#apply-licensing #lic-opt-4-price').val(v.price).parents('.form-group').show();
            }
        });
        //addEventHandlersForCourse();
    }

    function fillInstituteDetails(data) {
        $('h4#profile-name').text(data.profileDetails.name);
    }

    function fillProfessorDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('a#profile-name').text(name);
    }

    function fillPublisherDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('a#profile-name').text(name);
    }

    function fetchSubjects() {
        var req = {};
        var res;
        req.courseId = getUrlParameter('courseId');
        if (req.courseId == "" || req.courseId == null) {
            window.location.href = "profile.php";
            return;
        }
        req.action = "get-course-subjects";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillSubjects(res);
        });
    }

    function fillSubjects(data) {
        var html = "";
        var flag = false;
        var editLink;
        var deletedClass = "";
        $.each(data.subjects, function (i, subject) {
            if (flag == false)
                flag = true;
            deletedClass = "";
            if (subject.deleted == 1) {
                deletedClass = " deleted-item";
            }

            console.log(deletedClass);

            editLink = "backend_edit-subject.php?courseId=" + subject.courseId + "&subjectId=" + subject.id;
            html += "<tr class='subject" + deletedClass + "' data-sid='" + subject.id + "'>"
                    + "<td><h5><a href='" + editLink + "'>" + subject.name + "</a></h5>";
            $.each(subject.professors, function (i, prof) {
                html += "&nbsp; &nbsp;<i class='fa fa-user'></i> " + prof.firstName + " " + prof.lastName
                        + "&nbsp; &nbsp;<br>";
            });

            html += "</td>"
                    + "<td><br><br></td>"
                    + "<td>S" + String("000" + subject.id).slice(-4) + "</td>";

            if (subject.deleted == 0) {
                html += "<td>&nbsp;</td>";
            } else {
                html += "<td>&nbsp;</td>";
            }
            html += "</tr>";
        });

        if (flag == true) {
            $('#existing-subjects tbody').html(html);
            //addEventHandlersForSubjects();
            //deleteProfessorEventHandler();
            //makeSubjectsSortable('#existing-subjects tbody');
        }
    }

    function fetchCoursesForImport() {
        var req = {};
        var res;
        req.courseId = getUrlParameter('courseId');
        if (req.courseId == "" || req.courseId == null) {
            window.location.href = "profile.php";
            return;
        }
        req.action = "get-courses-for-import";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            console.log(res);
            fillCoursesForImport(res);
        });
    }

    function fillCoursesForImport(data) {
        var html = "";
        var flag = false;
        var editCourseLink, editSubjectLink, addSubjectLink;
        courseSubject = {};
        $.each(data.courses, function (i, course) {
            if (flag == false)
                flag = true;
            courseSubject[course.id] = [];
            html += "<div class='row entry course course-" + course.id + "' data-id='" + course.id + "'>"
                    + "<div class='col-xs-4 checkbox name'>"
                    + "<label>"
                    + "<input type='checkbox' value='1' />"
                    + course.name
                    + "</label>"
                    + "</div>"
                    + "<div class='col-xs-4 expiry-date'>15-06-2014</div>"
                    + "<div class='col-xs-4 license'>Licenced</div>"
                    + "</div>"
                    + "<div class='course-subjects course-" + course.id + "'>";
            $.each(course.subjects, function (i, subject) {
                courseSubject[course.id].push(subject.id);
                html += "<div class='row entry subject subject-" + subject.id + "' data-id='" + subject.id + "'>";
                if (subject.alreadyImported == 0) {
                    html += "<div class='col-xs-4 checkbox name'>"
                            + "<label><input type='checkbox' value='1' />";
                } else {
                    html += "<div class='col-xs-4 checkbox name tooltips' data-placement='top' data-toggle='tooltip' data-original-title='This subject has been already imported in this course.'>"
                            + "<label>"
                            + "<input type='checkbox' value='1' disabled='disabled' readonly='readonly'/>";
                }
                html += subject.name
                        + "</label>"
                        + "</div>"
                        + "<div class='col-xs-4 expiry-date'>15-06-2014</div>"
                        + "<div class='col-xs-4 license'>Licenced</div>"
                        + "</div>";
            });
            html += "</div>";

        });
    if (flag == true) {
            $('#import-existing-course .dyn-data').html(html);
            fitEventHandlersForImportExistingSubjects();
            $('.tooltips').tooltip();
        }
    }

    function fitEventHandlersForImportExistingSubjects() {
        $('#import-existing-course .dyn-data .course input[type=checkbox]').on('click', function () {
            var currCourseId = $(this).parents('.course').attr('data-id');
            if ($(this).prop('checked') == true) {
                $('#import-existing-course .dyn-data .course-subjects.course-' + currCourseId).show();
                $('#import-existing-course .dyn-data .course-subjects.course-' + currCourseId)
                        .find('input[type=checkbox]').prop('checked', true);
            } else {
                $('#import-existing-course .dyn-data .course-subjects.course-' + currCourseId).hide();
                $('#import-existing-course .dyn-data .course-subjects.course-' + currCourseId)
                        .find('input[type=checkbox]').prop('checked', false);
            }

        });
    }

    function formatUserRole(roleId) {
        if (roleId == '1')
            return "Institute";
        if (roleId == '2')
            return "Professor";
        if (roleId == '3')
            return "Publisher";
    }

    function validateForm() {
        $('.form-msg').html('').hide();
        if ($('#edit-form #course-name').val().length < 5 || $('#edit-form #course-name').val().length > 100) {
            $('.form-msg.course-name').html('Invalid course name. Must be between 5 to 100 chars.').show();
            return false;
        }

        if ($('#edit-form #course-subtitle').val().length > 100) {
            $('.form-msg.course-subtitle').html('Must be less than 100 chars.').show();
            return false;
        }
        if ($('#edit-form #course-description').val().length > 1000) {
            $('.form-msg.course-description').html('Can not be more than 1000 chars.').show();
            return false;
        }
        if ($('#edit-form #live-date').val() != "" && !isValidDate($('#edit-form #live-date').val())) {
            $('.form-msg.live-date').html('Live date is invalid.').show();
            return false;
        }
        if ($('#edit-form #set-end-date').val() == 1 && ($('#edit-form #end-date').val() == "" || !isValidDate($('#edit-form #end-date').val()))) {
            $('.form-msg.end-date').html('End date is invalid.').show();
            return false;
        }
        if ($('#edit-form #avail-student-market').val() == 1 && ($('#edit-form #student-price').val() == "" || !isValidPrice($('#edit-form #student-price').val()))) {
            $('.form-msg.student-price').html('Price is invalid.').show();
            return false;
        }
        return true;
    }

    function validateLicensingForm() {
        $('.form-msg').html('').hide();
        var licOpt1 = $('#apply-licensing #lic-opt-1').prop('checked');
        var licOpt2 = $('#apply-licensing #lic-opt-2').prop('checked');
        var licOpt3 = $('#apply-licensing #lic-opt-3').prop('checked');
        var licOpt4 = $('#apply-licensing #lic-opt-4').prop('checked');
        if (licOpt1 == false && licOpt2 == false && licOpt3 == false && licOpt4 == false) {
            $('.form-msg.licensing').html('Please apply licensing').show();
            return false;
        }
        var licVal1 = $('#apply-licensing #lic-opt-1-price').val();
        var licVal2 = $('#apply-licensing #lic-opt-2-price').val();
        var licVal3 = $('#apply-licensing #lic-opt-3-price').val();
        var licVal4 = $('#apply-licensing #lic-opt-4-price').val();
        var licCommAmt4 = $('#apply-licensing #lic-opt-4-comm').val();
        var licCommPer4 = $('#apply-licensing #lic-opt-4-comm-per').val();
        if (licOpt1 == true && (licVal1 == "" || !isValidPrice(licVal1))) {
            $('.form-msg.licensing-op-1').html('Please provide price for one year licensing').show();
            return false;
        }
        if (licOpt2 == true && (licVal2 == "" || !isValidPrice(licVal2))) {
            $('.form-msg.licensing-op-2').html('Please provide price for one time transfer').show();
            return false;
        }
        if (licOpt3 == true && (licVal3 == "" || !isValidPrice(licVal3))) {
            $('.form-msg.licensing-op-3').html('Please provide price for selective IP transfer').show();
            return false;
        }
        if (licOpt4 == true && (licVal4 == "" || !isValidPrice(licVal4))) {
            $('.form-msg.licensing-op-4').html('Please provide price for commission based licensing').show();
            return false;
        }
        if (licOpt4 == true && licCommPer4 == "" && licCommAmt4 == "") {
            $('.form-msg.licensing-op-4').html('Please provide commission').show();
            return false;
        }

        if (licOpt4 == true) {
            if (licCommAmt4 != "" && licCommPer4 != "") {
                $('.form-msg.licensing-op-4').html('You can provide either percentage or amount').show();
                return false;
            }
            if (licCommAmt4 != "" && !isValidPrice(licCommAmt4)) {
                $('.form-msg.licensing-op-4').html('Not a valid amount').show();
                return false;
            }
            if (licCommPer4 != "" && (!isValidPrice(licCommPer4) || licCommPer4 > 100)) {
                $('.form-msg.licensing-op-4').html('Not a valid precentage').show();
                return false;
            }
        }
        return true;
    }

    function makeSubjectsSortable(selector) {
        $(selector).sortable({
            items: '> tr',
            forcePlaceholderSize: true,
            placeholder: 'sort-placeholder',
            start: function (event, ui) {
                // Build a placeholder cell that spans all the cells in the row
                var cellCount = 0;
                $('td, th', ui.helper).each(function () {
                    // For each TD or TH try and get it's colspan attribute, and add that or 1 to the total
                    var colspan = 1;
                    var colspanAttr = $(this).attr('colspan');
                    if (colspanAttr > 1) {
                        colspan = colspanAttr;
                    }
                    cellCount += colspan;
                });

                // Add the placeholder UI - note that this is the item's content, so TD rather than TR
                ui.placeholder.html('<td colspan="' + cellCount + '">&nbsp;</td>');
                //$(this).attr('data-previndex', ui.item.index());
            },
            update: function (event, ui) {
                // gets the new and old index then removes the temporary attribute
                var newOrder = $.map($(this).find('tr'), function (el) {
                    return $(el).attr('data-sid') + "-" + $(el).index();
                });
                if (reorderTimer != null)
                    clearTimeout(reorderTimer);
                reorderTimer = setTimeout(function () {
                    updateSubjectOrder(newOrder);
                }, 5000);
            },
            helper: function (e, ui) {
                ui.children().each(function () {
                    $(this).width($(this).width());
                });
                return ui;
            }

        }).disableSelection();
    }

    function updateSubjectOrder(newOrder) {
        var order = {};
        $.each(newOrder, function (i, v) {
            order[v.split('-')[0]] = v.split('-')[1];
        });
        var req = {};
        var res;
        req.courseId = getUrlParameter('courseId');
        if (req.courseId == "" || req.courseId == null) {
            window.location.href = "profile.php";
            return;
        }
        req.newOrder = order;
        req.action = "update-subject-order";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1)
                fetchSubjects();
            else
                alertMsg(res.message);
        });
    }

    function fetchCourseStudent_details() {
        var req = {};
        var res;
        req.course_id = getUrlParameter('courseId');
        req.action = "get_students_details_of_course";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            var html = "";
            for (var i = 0; i < data.students.length; i++)
            {
                var student_id = data.students[i].id;
                var name = data.students[i].name;
                var contactMobile = data.students[i].contactMobile;
                // var course_key = data.students[i].key_id;
                var email = data.students[i].email;
                var date = data.students[i].date;
                html += '<tr ><td>' + student_id + ' </td><td>' + contactMobile + '</td><td>' + name + '</td><td>' + email + '</td></tr>';
            }
            $('#tbl_students_details_of_course').append(html);

        });
    }

    //fetchProfileDetails();
    fetchCourseDetails();
    fetchSubjects();
    //fetchCoursesForImport();
    fetchCourseStudent_details();
    //get_keys_remaining();

    //Event handlers


    function addEventHandlersForSubjects() {
        $('#existing-subjects .delete-subject').on('click', function (e) {
            e.preventDefault();
            var subject = $(this).parents('tr');
            var sid = subject.attr('data-sid');
            var req = {};
            req.action = "delete-subject";
            req.subjectId = sid;
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if (res.status == 1) {
                    subject.find('.delete-subject').hide();
                    subject.find('.restore-subject').show();
                    subject.addClass('deleted-item');
                } else {
                    alertMsg(res.message);
                }
            });
        });

        $('#existing-subjects .restore-subject').on('click', function (e) {
            e.preventDefault();
            var subject = $(this).parents('tr');
            var sid = subject.attr('data-sid');
            var req = {};
            req.action = "restore-subject";
            req.subjectId = sid;
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if (res.status == 1) {
                    subject.find('.restore-subject').hide();
                    subject.find('.delete-subject').show();
                    subject.removeClass('deleted-item');
                } else {
                    alertMsg(res.message);
                }
            });
        });
    }

    function addEventHandlersForCourse() {
        $('.request-approval-link').on('click', function () {
            var req = {};

            req.courseId = getUrlParameter('courseId');

            if (req.courseId == "" || req.courseId == null) {
                window.location.href = "profile.php";
                return;
            }
            req.action = "request-course-approval";
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if (res.status == 1) {
                    alertMsg(res.message);
                    $('.request-approval-link').removeClass('btn-success').removeClass('btn-danger').addClass('btn-warning')
                            .unbind('click').html('<i class="fa fa-user"></i> Pending');

                } else {
                    alertMsg(res.message);
                }
            });
        });
    }

    $('#course-details .edit-button').click(function () {
        $('#course-details').hide();
        $('#edit-form').fadeIn('fast');
    });

    $('.cancel-edit-button').click(function () {
        $('#edit-form').hide();
        $('#course-details').fadeIn('fast');
    });

    $('#edit-form #set-end-date').on('change', function () {
        if ($('#edit-form #set-end-date').val() == 0)
            $('#edit-form #end-date').parents('.form-group').hide();
        else
            $('#edit-form #end-date').parents('.form-group').show();
    });

    $('#edit-form #avail-student-market').on('change', function () {
        if ($(this).val() == 0)
            $('#edit-form #student-price').parent().hide();
        else
            $('#edit-form #student-price').parent().show();
    });

    $('#edit-form #avail-content-market').on('change', function () {
        if ($(this).val() == 0) {
            $('#edit-form #licensing-level').parent().hide();
            $('#edit-form #licensing-level').val('1');
            $('#edit-form #licensing-link').parents('.form-group').hide();
        } else {
            $('#edit-form #licensing-level').parent().show();
        }
    });

    $('#edit-form #licensing-level').on('change', function () {
        if ($(this).val() == 1)
            $('#edit-form #licensing-link').parents('.form-group').hide();
        else
            $('#edit-form #licensing-link').parents('.form-group').show();
    });

    $('#edit-form .save-button').on('click', function (e) {
        e.preventDefault();
        if (validateForm() == false)
            return;
        var req = {};
        req.courseId = getUrlParameter('courseId');
        req.courseName = $('#edit-form #course-name').val();
        req.courseSubtitle = $('#edit-form #course-subtitle').val();
        req.courseDesc = $('#edit-form #course-description').val();
        req.liveDate = $('#edit-form #live-date').val();
        req.setEndDate = $('#edit-form #set-end-date').val();
        req.endDate = $('#edit-form #end-date').val();
        req.courseCateg = $('#edit-form #course-cat').val();
        req.targetAudience = $('#edit-form #target-audience').val();
        if (req.courseCateg == null)
            req.courseCateg = [];
        req.action = 'update-course';

        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1) {
                alertMsg(res.message);
                $('#edit-form').hide();
                $('#course-details').fadeIn('fast');
                if (courseImageChanged)
                    $('#course-image-form').submit();
                else
                    fetchCourseDetails();

            } else {
                alertMsg(res.msg);
            }
        });
    });

    $('#apply-licensing button.save').on('click', function (e) {
        e.preventDefault();
        if (validateLicensingForm() == false)
            return;
        var req = {};
        req.courseId = getUrlParameter('courseId');
        var licOpt1 = $('#apply-licensing #lic-opt-1').prop('checked');
        var licOpt2 = $('#apply-licensing #lic-opt-2').prop('checked');
        var licOpt3 = $('#apply-licensing #lic-opt-3').prop('checked');
        var licOpt4 = $('#apply-licensing #lic-opt-4').prop('checked');
        var licVal1 = $('#apply-licensing #lic-opt-1-price').val();
        var licVal2 = $('#apply-licensing #lic-opt-2-price').val();
        var licVal3 = $('#apply-licensing #lic-opt-3-price').val();
        var licVal4 = $('#apply-licensing #lic-opt-4-price').val();
        var licCommAmt4 = $('#apply-licensing #lic-opt-4-comm').val();
        var licCommPer4 = $('#apply-licensing #lic-opt-4-comm-per').val();
        var data = {};
        var selectedLicensing = {};
        if (licOpt1 == true) {
            selectedLicensing["1"] = {
                "price": licVal1,
                "data": data
            }
        }
        if (licOpt2 == true) {
            selectedLicensing["2"] = {
                "price": licVal2,
                "data": data
            }
        }
        if (licOpt3 == true) {
            selectedLicensing["3"] = {
                "price": licVal3,
                "data": data
            }
        }
        if (licOpt4 == true) {
            data = {
                "amount": licCommAmt4,
                "percent": licCommPer4
            };

            selectedLicensing["4"] = {
                "price": licVal4,
                "data": data
            }
        }

        req.selectedLicensing = selectedLicensing;

        req.action = 'update-course-licensing';

        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1) {
                alertMsg(res.message);
                $('#apply-licensing').modal('hide');

            } else {
                alertMsg(res.msg);
            }
        });
    });

    $('#apply-licensing button.cancel').on('click', function (e) {
        $('#apply-licensing').modal('hide');
    });

    $('#edit-form #live-date').datepicker({
        format: 'dd-mm-yyyy'
    });

    $('#edit-form #end-date').datepicker({
        format: 'dd-mm-yyyy'
    });

    $('#apply-licensing .lic-opt').on('change', function () {
        var id = $(this).attr('id');
        if ($(this).prop('checked') == true) {
            $('#' + id + '-price').parents('.form-group').show();
        } else {
            $('#' + id + '-price').parents('.form-group').hide();
        }
    });

    $("#course-image").change(function () {
        courseImageChanged = true;
        var input = this;
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            if (typeof jcrop_api != 'undefined' && jcrop_api != null) {
                jcrop_api.destroy();
                jcrop_api = null;
                var pImage = $('.crop');
                pImage.css('height', 'auto');
                pImage.css('width', 'auto');
                var height = pImage.height();
                var width = pImage.width();

                $('.jcrop').width(width);
                $('.jcrop').height(height);
            }
            reader.onload = function (e) {
                $('#edit-form #course-image-preview').attr('src', e.target.result);
                $('#edit-form  .crop').Jcrop({
                    onSelect: updateCoords,
                    bgOpacity: .4,
                    setSelect: [100, 100, 50, 50],
                    aspectRatio: 520 / 200
                }, function () {
                    jcrop_api = this;
                });
            }
            reader.readAsDataURL(input.files[0]);
        }
    });

    function updateCoords(c) {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    }

    function getSelectedFromExistingSubjectImportForm() {
        var selectedSubjects = [];
        $.each($('#import-existing-course .dyn-data .subject'), function (i, subject) {
            var subjectId = $(this).attr('data-id');
            if ($(this).find('input[type=checkbox]').prop('checked') == true)
                selectedSubjects.push(subjectId);
        });
        return selectedSubjects;
    }

    $('#import-existing-course button.import').on('click', function (e) {
        $('.form-msg.existing-subject-import-form').html('').hide();
        e.preventDefault();
        var selectedSubjects = getSelectedFromExistingSubjectImportForm();
        if (selectedSubjects.length == 0) {
            $('.form-msg.existing-subject-import-form').html('Please select a subject to import.').show();
            return;
        }

        var req = {};
        req.courseId = getUrlParameter('courseId');
        req.action = 'import-subjects-to-course';
        req.subjects = selectedSubjects;
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1) {
                alertMsg(res.message);
                $('#import-content-modal').modal('hide');
                fetchSubjects();
                fetchCoursesForImport();
                alertMsg(res.msg);
            } else {
                alertMsg(res.msg);
            }
        });
    });

    var options = {
        beforeSend: function () {
            //alert('sending');
        },
        success: function () {

        },
        complete: function (resp) {
            fetchCourseDetails();
        },
        error: function () {
            alert('Error in uploading file');
        }
    };

    $('#course-image-form').attr('action', fileApiEndpoint);

    $('#course-image-form').ajaxForm(options);

    var keys_avilable;
    $('.invite_students').on('click', function () {
        $('#textarea_email_id').val('');
        get_keys_remaining();
        alertMsg("You can send " + keys_avilable + " invitations");
    });

    function get_keys_remaining() {
        var req = {};
        req.courseId = getUrlParameter('courseId');
        req.action = 'keys_avilable';
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            if (data.status == 1) {
                keys_avilable = data.keys;
                //alertMsg(data.message);
                //$('#modal_invite_student').modal('hide');
            }
            else {
                // alertMsg(res.msg);
            }
            $('#remaining_keys_total').text(keys_avilable);
        });

    }

    $('#send_activation_link').on('click', function () {
        var invalid = '';
        var correct = true
        var emails = $.trim($('#textarea_email_id').val());
        var email = emails.split(',');
        if (emails == '') {
            alertMsg('Please enter email id ');
            return false;
        }
        if (checkDuplicates(email) == 'false') {
            alertMsg('Please remove duplicates ');
            return false;
        }
        if ((email.length > keys_avilable)) {
            alertMsg('You have only ' + keys_avilable + ' available ');
            return false;
        }
        $.each(email, function (index, value) {
            if (validateEmail($.trim(value)) == false) {
                invalid += value + ',';
                correct = false;
            }
        });
        if (correct == false) {
            alert('Correct these email addresses:\n' + invalid);
            return false;
        }
        else {
            var req = {};
            req.courseId = getUrlParameter('courseId');
            req.action = 'send_activation_link_student';
            req.email = email;
            req.enrollment_type = 0;
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                data = $.parseJSON(res);
                if (data.status == 1) {
                    alertMsg(data.message);
                }
                else {
                    alertMsg(data.msg);
                }
                $('#modal_invite_student').modal('hide');

            });
        }
    });
    function checkDuplicates(a) {
        //Check all values in the incoming array and eliminate any duplicates
        var r = new Array(); //Create a new array to be returned with unique values
        //Iterate through all values in the array passed to this function
        o:for (var i = 0, n = a.length; i < n; i++) {
            //Iterate through any values in the array to be returned
            for (var x = 0, y = r.length; x < y; x++) {
                //Compare the current value in the return array with the current value in the incoming array
                if (r[x] == a[i]) {
                    //If they match, then the incoming array value is a duplicate and should be skipped
                    return 'false';
                    //continue o;
                }
            }
            //If the value hasn't already been added to the return array (not a duplicate) then add it
            r[r.length] = a[i];
        }
        //Return the reconstructed array of unique values
        //return r;
        return 'true';
    }

    function validateEmail($email) {
        var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!emailReg.test($email)) {
            return false;
        } else {
            return true;
        }
    }

});