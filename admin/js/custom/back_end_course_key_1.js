var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';

$(function () {
    $('#loader_outer').hide();
    loadbackCoursekeys();
    function loadbackCoursekeys() {
        var req = {};
        var res;
        req.action = "back_end_course_keys";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            var html = "";
            for (var i = 0; i < data.key_details.length; i++) {
                var userId = data.key_details[i].key_details[0].userId;
                var active = data.key_details[i].key_details[0].active;
                var name = data.key_details[i].key_details[0].name;
                var total_key = data.key_details[i].key_details[0].total_key;
                var reamining = total_key - active;
                var edited = data.key_details[i].key_rate[0].is_editable;
                var key_rate = data.key_details[i].key_rate[0].key_rate;
                var key_rate_inr = data.key_details[i].key_rate[0].key_rate_inr;
                var role = data.key_details[i].role[0].name;
                var currency = data.key_details[i].currency[0].currency;
                var style = "";
                if (edited == 1) {
                    style = 'bgcolor=#DDEBF9';
                }
                if (currency == 2)
                {
                    display_currency = '<i class="fa fa-inr"></i>';
                }
                else {
                    display_currency = '$';
                }
                html += '<tr ' + style + ' rowid=' + userId + '><td>' + (i + 1) + '</td><td>' + userId + '</td><td>' + name + '</td><td currency=' + currency + '>' + display_currency + '</td><td>' + role + '</td><td>' + total_key + '</td><td>' + reamining + '</td><td dollar='+key_rate +'>' + key_rate + '</td>';

                html += '<td inr='+key_rate_inr +'>' + key_rate_inr + '</td>' + '<td><a  href="#modal_change_key_rate" data-toggle="modal" class="btn btn-primary btn-xs key_rate_edit" ><i class="fa fa-edit"></i> </a>  <a   class="btn btn-info btn-xs key_rate_default" > <i class="fa fa-refresh"></i> </a> <a href="#modal_purchase_key" data-toggle="modal"   class="btn btn-success btn-xs add_keys" > <i class="fa fa-plus-circle"></i> </a>  </td></tr>';

            }
            $('#all-courses').append(html);
            $('#all-courses').dataTable({
                "aaSorting": [[1, "desc"]]
            });
            $('#default_rate_inr').val(data.default_rate[0].key_rate_inr);
            $('#default_rate_dollar').val(data.default_rate[0].key_rate);
            var rowid = '';

            $('.key_rate_edit').on('click', function () {
                rowid = $(this).parents('td').parent('tr').attr('rowid');
                var td = $(this).parent().prev();
                var dollar = td.prev().attr('dollar');
                var rate = td.attr('inr');
                alert(rate);
                $("#key_rate_indian").val(rate);
                $("#key_rate_dollar").val(dollar);
            });
            var display_currency;
            var key_rate = '';

            $('.add_keys').on('click', function () {
                display_currency = '$';
                $('#totalkeys').val('');
                $('#keyrate').text('');
                $('#totalkeyrate').text('');
                var tr = $(this).parents('td').parent('tr');
                rowid = tr.attr('rowid');
                var currency = tr.find("td:nth-child(4)").attr('currency');
                key_rate = tr.find("td:nth-child(8)").text();
                if (currency == 2) {
                    key_rate = tr.find("td:nth-child(9)").text();
                    display_currency = '<i class="fa fa-inr"></i>';
                }
                $('#keyrate').text(key_rate);
                $('#keyrate').append(display_currency);
            });



            $('#save_default_key_rate').on('click', function () {

                var key_rate = $("#default_rate_dollar").val();
                var key_rate_inr = $("#default_rate_inr").val();
                var req = {};
                var res;
                req.action = "set_default_key_rate";
                req.key_rate = key_rate;
                req.key_rate_inr = key_rate_inr;
                $.ajax({
                    'type': 'post',
                    'url': ApiEndpoint,
                    'data': JSON.stringify(req)
                }).done(function (res) {
                    data = $.parseJSON(res);
                    window.location = self.location;
                    //$(this).hide();
                });

            });

            $('#totalkeys').keyup(function () {

                var total = $(this).val() * key_rate;
                $('#totalkeyrate').text(total);
                $('#totalkeyrate').append(display_currency);

            });

            $('#save_rate').on('click', function () {

                var key_rate = $("#key_rate_dollar").val();
                var key_rate_inr = $("#key_rate_indian").val();
                var req = {};
                var res;
                req.action = "change_key_rate";
                req.key_user = rowid;
                req.key_rate = key_rate;
                req.key_rate_inr = key_rate_inr;
                $.ajax({
                    'type': 'post',
                    'url': ApiEndpoint,
                    'data': JSON.stringify(req)
                }).done(function (res) {
                    data = $.parseJSON(res);
                    window.location = self.location;

                });
            });

            //function to  set back key rate to default for an institute 
            $('.key_rate_default').on('click', function () {
                if (confirm('Are you sure to set default rate ?')) {
                    var td = $(this).parents('td');
                    rowid = td.parent('tr').attr('rowid');
                    var req = {};
                    var res;
                    req.action = "setInstitute_rate_back";
                    req.key_user = rowid;
                    $.ajax({
                        'type': 'post',
                        'url': ApiEndpoint,
                        'data': JSON.stringify(req)
                    }).done(function (res) {
                        data = $.parseJSON(res);
                        window.location = self.location;
                        //alert(data.default_rate[0].key_rate);
                        //td.prev().text(data.default_rate[0].key_rate);
                        //td.prev().prev().text(data.default_rate[0].key_rate_inr);

                    });
                }
            });

            $('#purchase_key').on('click', function () {
                var keys = $('#totalkeys').val();
                var req = {};
                var res;
                req.action = "add_keys";
                req.keys = keys;
                req.key_user = rowid;
                $.ajax({
                    'type': 'post',
                    'url': ApiEndpoint,
                    'data': JSON.stringify(req)
                }).done(function (res) {
                    data = $.parseJSON(res);
                    window.location = self.location;
                    //alert(data.default_rate[0].key_rate);
                    //td.prev().text(data.default_rate[0].key_rate);
                    //td.prev().prev().text(data.default_rate[0].key_rate_inr);

                });

            });

        });



    }

});