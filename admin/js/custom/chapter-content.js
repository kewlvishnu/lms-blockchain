$(function() {
	$('a.chapters').addClass('active');
	//fetchProfileDetails();
	fetchCourses();
	//event handler to handle course drop down change
	$('#courseSelect').on('change', function() {
		$('#currentChapters').hide();
		$('#addChapter').attr('disabled', true);
		if($(this).val() == 0) {
			$("#subjectSelect").attr('disabled', true);
			$('#addSubject').show();
			$('#subjectSelect').find('option').remove()
			$('#subjectSelect').append('<option value="0">Select Subject</option>');
			$('#addSubject').hide();
			$('#warning').html('No Course found in your profile. Please <a href="add-course.php">Add Course</a> to continue with the Assignment Creation.');
			$('#warning').show();
		}
		else {
			$('#addSubject').show();
			$('#addSubject').attr('href', 'add-subject.php?courseId='+$(this).val());
			fetchSubjects($(this).val());
		}
	});
	
	//event handler to handle subject drop down change
	$('#subjectSelect').on('change', function() {
		$('#warning').show();
		$('#currentItem').hide();
		if($(this).val() == 0) {
			$('#currentChapters').hide();
			$('#addChapter').attr('disabled', true);
		}
		else {
			$('#addChapter').attr('href', 'content.php?courseId='+$('#courseSelect').val()+'&subjectId='+$('#subjectSelect').val());
			$('#addChapter').attr('disabled', false);
			fetchChapters();
		}
	});
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		fetchCourses();
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fetchCourses() {
		var req = {};
		var res;
		req.action = 'get-courses-for-exam';
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0) {
				alert(res.message);
			}
			else
				fillCourseSelect(res);
		});
	}
	
	function fillCourseSelect(data) {
		$('#courseSelect').attr('disabled',false);
		for(i=0; i<data.courses.length; i++) {
			$('#courseSelect').append('<option value="'+data.courses[i]['id']+'">'+data.courses[i]['name']+'</option>');
		}
		$('#warning').html("Please select a course to continue");
	}
	
	function fetchSubjects(courseId) {
		var req = {};
		var res;
		req.action = 'get-subjects-for-exam';
		req.courseId = courseId;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0) {
				$("#subjectSelect").attr('disabled', true);
				$('#subjectSelect').find('option').remove();
				$('#subjectSelect').append('<option value="0">No subjects yet.</option>');
				$('#warning').html("You do not have any subjects in this coarse. Please add a new subject to continue.");
			}
			else
				fillSubjectSelect(res);
		});
	}
	
	function fillSubjectSelect(data) {
		$('#warning').html("Please select a subject to continue.");
		$('#subjectSelect').find('option').remove()
		$('#subjectSelect').append('<option value="0">Select Subject</option>');
		$("#subjectSelect").attr('disabled', false);
		for(i=0; i<data.subjects.length; i++) {
			$('#subjectSelect').append('<option value="'+data.subjects[i]['id']+'">'+data.subjects[i]['name']+'</option>');
		}
	}
	
	function fetchChapters() {
		var req = {};
		var res;
		req.action = 'get-chapter-details-for-view';
		req.subjectId = $('#subjectSelect').val();
		//req.courseId = $('#courseSelect').val();
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0) {
				$('#warning').html("You have not created any Chapters yet.");
				$('#warning').show();
			}
			else
				fillChapters(res);
		});
	}
	
	function fillChapters(data) {
		$('#warning').hide();
		$('#currentChapters').show();
		$('#currentChapters').find('tr td').remove();
		var html = "";
		for(i=0; i<data.chapters.length; i++) {
			html += '<tr data-id="' + data.chapters[i]['id'] + '">'
					+ '<td width="150">'+data.chapters[i]['id']+'</td>'
					+ '<td>'+data.chapters[i]['name']+'</td>'
					+ '<td>'
						+ '<a href="chapterPreview.php?courseId='+data.chapters[i]['courseId']+'&subjectId=' + $('#subjectSelect').val() + '&chapterId='+data.chapters[i]['id']+'" type="button" data-placement="top" data-toggle="tooltip" class="btn btn-success btn-xs tooltips btn-space" data-original-title="Preview" target="_blank"><i class="fa fa-info-circle"></i> Preview</a>&emsp;'
						+ '<a href="content.php?courseId='+data.chapters[i]['courseId']+'&subjectId='+$('#subjectSelect').val()+'&chapterId='+data.chapters[i]['id']+'" class="btn btn-primary btn-xs tooltips btn-space" data-toggle="tooltip" data-placement="top" type="button" data-original-title="Add/Edit Content"><i class="fa fa-edit"></i> Add/Edit Content</a>'
					+ '</td>'
				+ '</tr>';
		}
		$('#currentChapters').append(html);
		$('.tooltips').tooltip();
	}
});