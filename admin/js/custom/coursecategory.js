var ApiEndpoint = '../api/index.php';
var month = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
var Cid=0;
$(function () {
	var imageRoot = '/';
	$('#loader_outer').hide();
	var fileApiEndpoint = '../api/files1.php';
	viewcategory();
	function viewcategory() {
		var req = {};
		var res;
		req.action = "viewCourseCategory";
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			console.log(data);
			if(res.status == 0) {
				alertMsg(res.message);
			}else{
				fillcategory(data);
			}
		});
	}
	function fillcategory(data) {
	
		var html = "";
		for (var i = 0; i < data.categories.length; i++)
		{
			var id = data.categories[i].id;
			var name=data.categories[i].category;
			var desc=data.categories[i].description;
			
			html += '<tr catId="' + id + '"><td>' + (i + 1) + '</td><td>' + id + '</td><td>' + name + ' </td><td>' + desc
					+ '</td><td> <button data-description="'+ desc +'" data-name="'+ name +'" class="btn btn-info btn-xs edit-desc">Edit Description</button></tr>';
		}
		$('#courseCatDes tbody').find('tr').remove();
		$('#courseCatDes tbody').append(html);
		$('#courseCatDes').dataTable({
			"aaSorting": [[0, "asc"]]
		});
		$('#courseCatDes').on('click', '.edit-desc', function(){	
			var id = $(this).parents('tr').attr('catId');
			Cid=id;
			var desc      = $(this).attr('data-description');
			var name       = $(this).attr('data-name');

			$('#catName').text(name);
			$('#catDesc').val(desc);
			$('#editcatDesc').modal('show'); 
		
		
		});
		$('.log-in').off('click');
		$('#tbl_view_institutes').on( 'click', '.log-in', function (e) {
		//$('.log-in').on('click', function() {
			var req = {};
			var res;
			req.action = 'ghost-login';
			req.userId = $(this).parents('tr').attr('instituteId');
			req.roleId = $(this).parents('tr').attr('data-roleId');
			$.ajax({
				'type'  : "post",
				'url'   : ApiEndpoint,
				data: JSON.stringify(req)   
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0) {
					alertMsg(res.message);
				}
				else {
					window.location = '../';
				}
			});
		})
	}


	$( "#editCatdesc" ).click(function() {

		var id = Cid;
		var inputDescription = trim(escape($('#catDesc').val()));

		if(!inputDescription) {
			$('#catDesc .help-block').html('<p class="text-danger text-center">Desc Name can\'t be left empty</p>');
		} else {
			var req = {};
			req.categoriesId = id;
			req.description = inputDescription;
			var res;
			req.action = "save-categories-desc";
			$.ajax({
				'type': 'post',
				'url': ApiEndpoint,
				'data': JSON.stringify(req)
			}).done(function (res) {
				res = $.parseJSON(res);
				if(res.status == 1) {
					//$('#frmNewPortfolio').html('<div class="jumbotron text-center"><h3>'+res.message+'</h3></div>');
					window.location = window.location;
				} else {
					$('#editCatdesc .help-block').html('<p class="text-danger text-center">'+res.message+'</p>');
					return false;
				}
				console.log(res);
			});
		}
		
	});
});