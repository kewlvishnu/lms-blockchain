var ApiEndpoint = '../api/index.php';

$(function () {
    $('a.students').click();
    $('a.students').addClass('active');
    var key_rate = "";
    var display_button = '';
    var data;
    var courses = [];
    var students = [];
    get_institute_students();
    function get_institute_students() {
        var req = {};
        var res;
        req.action = "get-institute-temp-students";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            var html = "";
            students = data.students;
            for (var i = 0; i < students.length; i++) {
                html+= '<tr data-id="'+i+'">'+
                            '<td>'+(i+1)+'</td>'+
                            '<td>'+students[i].username+'</td>'+
                            '<td>'+students[i].password+'</td>'+
                            '<td>'+students[i].courseNames+'</td>'+
                            '<td><button class="btn btn-info btn-xs js-transfer">Transfer</button></td>'+
                        '</tr>';
            };
            $('#tbl_student_details tbody').html(html);

            var table = $('#tbl_student_details').dataTable({
                "aaSorting": [[0, "asc"]],
                "bDestroy": true
            });
            
        });
    }
    $('#tbl_student_details').on("click", ".js-transfer", function(){
        $('#transferModal').modal('show');
        $('#transferDropdown').html('');
        var index = $(this).closest('tr').attr("data-id");
        var html = "";
        var iCourses = data.courses;
        var cCourses = students[index].courses;
        
        for (var i = 0; i < iCourses.length; i++) {
            for (var j = 0; j < cCourses.length; j++) {

                if(iCourses[i]["id"] == cCourses[j]["id"]) {
                    $('#transferDropdown').attr("data-course", iCourses[i]['id']);
                    iCourses[i]["used"] = 1;
                    //delete iCourses[i];
                } else {
                    iCourses[i]["used"] = 0;
                }
            };
        };
        
        if(Object.keys(iCourses).length>0) {

            $.each(iCourses, function( index, value ) {
                if (iCourses[index]["used"] == 0) {
                    html+= '<option value="'+iCourses[index]['id']+'">'+iCourses[index]['name']+'</option>';
                }
            });
        };
        $('#transferDropdown').attr("data-student", students[index]["id"]).html(html);
    });
    $('#btnTransfer').on('click', function () {
        var req = {};
        var res;
        req.oldCourseId = $('#transferDropdown').attr("data-course");
        req.studentId = $('#transferDropdown').attr("data-student");
        req.courseId = $('#transferDropdown').val();
        req.action = "transfer-student-course";
        if (!req.courseId) {
            alert("Please select a course!");
        } else {
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                window.location = window.location;
            });
        }
        return false;
    });

});