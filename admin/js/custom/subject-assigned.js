var ApiEndPoint = '../api/index.php';
var details;

$(function() {
	$('a.assignedSubjects').addClass('active');
	var imageRoot = '/';
	$('#loader_outer').hide();
	fetchNotifications();
	
	//function to fetch notifications
	function fetchNotifications() {
		var req = {};
		var res;
		req.action = 'get-subject-assigned';
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			console.log(res);
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				details = res;
				fillNotifications(res);
			}
		});
	}
	//function to fill notifications table
	function fillNotifications(data) {
		var html = '';
		$('#notify-list').find('li').remove();
			$.each(data.subjects, function (i, subject) {
				html += '<li class="task-item">'
							+ '<div class="row"><div class="col-md-3">'
								+ '<a href="edit-subject.php?courseId=' + subject.courseId + '&subjectId=' + subject.subjectId + '"><img style="width: 60%;" class="img-responsive" src="' + subject.subjectImage + '">'
								+ '<h3>' + subject.name + '</h3></a>'
							+ '</div>'
							+ '<div class="col-md-3">'
								+ '<img class="img-responsive" src="' + subject.courseImage + '">'
								+ '<h4>Course: ' + subject.course + '</h4>'
								+ '<a href="../institute/' + subject.instituteId + '" class="institute"><strong>By</strong>: ' + subject.institute + '</a><br></br>'
								+ '<div class="col-xs-2">'
									+ '<i style="font-size:35px; color:orange;margin:5px 10px 0px 0px;" class="fa fa-clock-o"></i>'
								+ '</div>';
				var startDate = new Date(parseInt(subject.liveDate));
				var start = startDate.getDate() + ' ' + MONTH[startDate.getMonth()] + ' ' + startDate.getFullYear();
				var end = 'No Expiry';
				if(subject.endDate != '') {
					var endDate = new Date(parseInt(subject.endDate));
					end = endDate.getDate() + ' ' + MONTH[endDate.getMonth()] + ' ' + endDate.getFullYear();
				}
				html += '<div class="col-xs-10">'
									+ '<span><strong>Start Date</strong>&nbsp;'+ start + '</span><br>'
									+ '<span><strong>End Date</strong>&nbsp;' + end + '</span>'
								+ '</div>'
							+ '</div>'
							+ '<div class="col-md-6" style="padding-top:10px;">'
								+ '<div class="col-xs-1">'
									+ '<i style="font-size:30px; color:orange;margin:5px 15px 0px 0px;" class="fa fa-th-list"></i>'
								+ '</div>'
								+ '<div class="col-xs-11">'
									+ '<strong>Subjects:</strong><br>'
										+ subject.subjects + '<br><br><a href="edit-subject.php?courseId=' + subject.courseId + '&subjectId=' + subject.subjectId + '" class="btn btn-success btn-sm btn-block"><i class="fa fa-pencil"></i> Edit Subject</a>'
								+ '</div>'
							+ '</div>'
						+ '<hr style="color: #CBC8C8;">'
					+ '</div>'
				+'</li>';
		});
		if(html == '')
		html = 'No new invitations found.';
		$('#notify-list').append(html);
	}
});