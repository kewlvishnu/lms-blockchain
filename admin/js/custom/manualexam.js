var sortOrder = [];
var examStart = new Date();
var examEnd = new Date('2099/12/31');
var data;
var mExam = [];
$(function() {
	fetchBreadcrumb();
	fetchManualExam();
	function fetchManualExam() {
		var req = {};
		var res;
		req.action = 'get-manual-exam-details';
		req.examId = getUrlParameter('examId');
		req.subjectId = getUrlParameter('subjectId');
		req.courseId = getUrlParameter('courseId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				data = res;
				//console.log(data);
				setManualExam();
			}
		});
	}
	function setManualExam() {
		var exam1 = data.exam;
		var rank = 1;
		if (exam1.type == "quick") {
			mExam['maxMarks']	= parseFloat(exam1.questions['marks']);
		} else {
			mExam['maxMarks']	= parseFloat(exam1.maxMarks);
		}
		for (var i = 0; i < exam1.students.length; i++) {
			mExam[i] = [];
			mExam[i]['studentId']	= exam1.students[i][0];
			mExam[i]['studentName']	= exam1.students[i][1];
			if (exam1.type == "quick") {
				mExam[i]['marks']		= parseFloat(exam1.students[i][2]['marks']);
				mExam[i]['questionId']	= parseFloat(exam1.students[i][2]['question_id']);
			} else {
				mExam[i]['marks']		= parseFloat(exam1.students[i][2]);
			}
			mExam[i]['percent']		= parseFloat(((mExam[i]['marks']/mExam['maxMarks'])*100).toFixed(2));
			mExam[i]['rank']		= 1;
			if (exam1.type == "quick") {
				mExam[i]['qMarks']	= exam1.students[i][2];
			} else {
				mExam[i]['qMarks']	= exam1.students[i][3];
			}
		};
		var temp = [];
		for (var i = 0; i < mExam.length; i++) {
			for (var j = 0; j < mExam.length; j++) {
				if (mExam[i]['marks']>mExam[j]['marks']) {
					temp = mExam[i];
					mExam[i] = mExam[j];
					mExam[j] = temp;
					temp = [];
				}
			};
		};
		var totalScore = 0;
		var totalPercent = 0;
		for (var i = 0; i < mExam.length; i++) {
			mExam[i]['rank'] = rank;
			rank++;
			//mExam[i]['percentile'] = ((i+1)/mExam.length).toFixed(2);
			//mExam[i]['percentile'] = parseFloat(((((mExam.length-(i+1))/mExam.length))*100).toFixed(2));
			mExam[i]['percentile'] = parseFloat(((((mExam.length-i)/mExam.length))*100).toFixed(2));
			if (i>0) {
				if (mExam[i]['marks']==mExam[i-1]['marks']) {
					//rank--; // line for ascending ranks
					mExam[i]['rank'] = mExam[i-1]['rank'];
					mExam[i]['percentile'] = mExam[i-1]['percentile'];
				};
			};
			totalScore+=mExam[i]['marks'];
			totalPercent+=mExam[i]['percent'];
		};
		mExam.totalScore = totalScore;
		mExam.avgScore = round2(totalScore/mExam.length);
		mExam.avgPercent = round2(totalPercent/mExam.length);

		fillManualExam();
	}
	function drawManualExam() {
		var maxScore = mExam[0]['marks'];
		var minScore = round2(mExam[mExam.length-1]['marks']);
		var avgScore = mExam.avgScore;
		var noOfStudents = mExam.length;
		var ascMExam = [];
		var j = mExam.length-1;
		for (var i = 0; i < mExam.length; i++) {
			ascMExam[j] = mExam[i];
			j--;
		};
		if (noOfStudents%2 == 0) {
			var median2 = (noOfStudents)/2;
			var median1 = median2-1;
			var medianScore = round2((ascMExam[median1]['marks']+ascMExam[median2]['marks'])/2);
		} else {
			var median = (noOfStudents-1)/2;
			var medianScore = round2(ascMExam[median]['marks']);
		}
		$('#scoreGraph').highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: ''
			},
			xAxis: {
				type: 'category',
				labels: {
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			},
			yAxis: {
				title: {
					text: '<b>Scores</b>'
				}
			},
			legend: {
				enabled: false
			},
			tooltip: {
				pointFormat: 'Score: <b>{point.y}</b>'
			},
			series: [{
				name: 'Attempts',
				data: [
					['Max<br>Score', parseFloat(maxScore)],
					['Avg<br>Score', parseFloat(avgScore)],
					['Median<br>Score', parseFloat(medianScore)],
					['Min<br>Score', parseFloat(minScore)]
				],
				colorByPoint: true,
				dataLabels: {
					enabled: true,
					color: '#000',
					align: 'center',
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif',
					}
				}
			}],
			colors: ['green', 'yellow', 'blue', 'red']
		});

		var maxPercent = mExam[0]['percent'];
		var minPercent = mExam[mExam.length-1]['percent'];
		var avgPercent = mExam.avgPercent;
		if (noOfStudents%2 == 0) {
			var median2 = (noOfStudents)/2;
			var median1 = median2-1;
			var medianPercent = (ascMExam[median1]['percent']+ascMExam[median2]['percent'])/2;
		} else {
			var median = (noOfStudents-1)/2;
			var medianPercent = ascMExam[median]['percent'];
		}
		$('#percentGraph').highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: ''
			},
			xAxis: {
				type: 'category',
				labels: {
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			},
			yAxis: {
				title: {
					text: '<b>Percent</b>'
				}
			},
			legend: {
				enabled: false
			},
			tooltip: {
				pointFormat: 'Percent: <b>{point.y}</b>'
			},
			series: [{
				name: 'Attempts',
				data: [
					['Max<br>Percent', parseFloat(maxPercent)],
					['Avg<br>Percent', parseFloat(avgPercent)],
					['Median<br>Percent', parseFloat(medianPercent)],
					['Min<br>Percent', parseFloat(minPercent)]
				],
				colorByPoint: true,
				dataLabels: {
					enabled: true,
					color: '#000',
					align: 'center',
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif',
					}
				}
			}],
			colors: ['green', 'yellow', 'blue', 'red']
		});

		//console.log(data.total[0]['total']);
		var totalScore=mExam.maxMarks;
		var difference=totalScore-maxScore;
		var blocksDiff=(parseFloat(totalScore)-parseFloat(difference+minScore))/10;
		console.log(blocksDiff);
		//var marksDis=[];
		var students=[];
		var markPer=[];
		var newlowest=(parseFloat(minScore)+parseFloat(difference)).toFixed(1);
		for(var i=1;i<11;i++)
		{	//marksDis[i-1]=(parseFloat(newlowest)+parseFloat(blocksDiff*i)).toFixed(1);
			students[i-1]=0;
			//checking
			var temp=(parseFloat(newlowest)+parseFloat(blocksDiff*i)).toFixed(1);
			markPer[i-1]=Math.round((temp/totalScore)*100)+'%';
			//markPer[i-1]=markPer[i-1]+'%';
		}
		for(var i=markPer.length-1;i>0;i--)
		{
			markPer[i]= markPer[i-1]+' - '+markPer[i];
		}
		
		markPer[0]=Math.round(newlowest)+'% - '+markPer[0];
		//console.log(markPer);
		$.each( mExam, function( key, value ) {
			var newscore=(parseFloat(value['marks'])+parseFloat(difference)).toFixed(1);
			var index=Math.round(((newscore-newlowest)/blocksDiff));
			if(index>0)
			{	index=index-1;
			}else{
				index=0;
			}
			students[index]=students[index]+1;
		});
		//console.log(students);
		$('#percentDistGraph').highcharts({
			chart: {
				zoomType: 'xy'
			},
			title: {
				text: 'Percentage distribution of all students'
			},
			subtitle: {
				text: 'This graph shows percentage w.r.t to highest scorer'
			},
			xAxis: [
			{
				title: {
					text: 'Percentage Score',
					style: {
						color: Highcharts.getOptions().colors[1]
					}
				},
				categories: markPer,
				crosshair: true
			}],
			yAxis: [{ // Primary yAxis
				labels: {
					format: '{value}',
					style: {
						color: Highcharts.getOptions().colors[1]
					}
				},
				title: {
					text: 'No. of Students',
					style: {
						color: Highcharts.getOptions().colors[1]
					}
				}
			}, { // Secondary yAxis
				title: {
					text: 'Students',
					style: {
						color: Highcharts.getOptions().colors[0]
					}
				},
				labels: {
					format: '{value}',
					style: {
						color: Highcharts.getOptions().colors[0]
					}
				},
				opposite: true
			}],
			tooltip: {
				shared: true
			},
			legend: {
				layout: 'vertical',
				align: 'left',
				x: 120,
				verticalAlign: 'top',
				y: 100,
				floating: true,
				backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
			},
			series: [{
				name: 'Students',
				type: 'column',
				yAxis: 1,
				data: students,
				tooltip: {
					valueSuffix: ''
				}

			}, {
				name: 'No. of Students',
				type: 'spline',
				yAxis: 1,
				data: students,
				tooltip: {
					valueSuffix: ''
				}
			}]
		});
	}
	/*function drawManualExam() {
		var exam = data.exam;
		var examType = '['+((exam.type=='quick')?'Quick':'Deep')+' Analytics]';
		var dataGraph = [];
		for (var i = 0; i < mExam.length; i++) {
			dataGraph.push({name:mExam[i]['studentName'],y:mExam[i]['marks'],percent:mExam[i]['percent'],rank:mExam[i]['rank']});
		};
		//console.log(dataGraph);
		$('#analytics').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: exam.name
	        },
	        subtitle: {
	            text: examType
	        },
	        xAxis: {
	            type: 'category',
	            labels: {
	                rotation: -45,
	                style: {
	                    fontSize: '13px',
	                    fontFamily: 'Verdana, sans-serif'
	                }
	            }
	        },
	        yAxis: {
	            title: {
	                text: 'Total Marks'
	            }

	        },
	        legend: {
	            enabled: false
	        },
	        plotOptions: {
	            series: {
	                borderWidth: 0,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.y:.1f}'
	                }
	            }
	        },

	        tooltip: {
	            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	            pointFormat: '<span style="color:{point.color}">{point.name}</span>:<br/>Marks - <b>{point.y:.2f}</b> out of '+mExam.maxMarks+'<br/>Percent - <b>{point.percent}%</b><br/>Rank - <b>{point.rank}</b>'
	        },

	        series: [{
	            name: 'Students',
	            colorByPoint: true,
	            data: dataGraph
	        }]
	    });
	}*/
	function fillManualExam() {
		//console.log(data);
		var exam = data.exam;
		var questionsCount = data.questionsCount;
		var questions = exam.questions;
		var filler = '';
		$('#examTitle').html('<h2>Exam : '+exam.name+' <button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button></h2>');
		$('#exam').html('<table class="table table-hover table-bordered '+((exam.type == "deep")?'table-deep':'')+'">'+
							'<thead>'+
								'<tr><th class="text-center" colspan="6">Exam Marks Table ['+((exam.type=='quick')?'Quick':'Deep')+' Analytics]</th></tr>'+
								((exam.type == "deep")?'<tr><th class="text-center" colspan="6"><small class="text-info">Click on any row to get marks by question</small></th></tr>':'')+
								'<tr>'+
									'<th class="text-center" width="10%">Student Id</th>'+
									'<th>Student Name</th>'+
									'<th class="text-center" width="15%">Total Marks (out of '+mExam.maxMarks+')</th>'+
									'<th class="text-center" width="15%">Percent</th>'+
									'<th class="text-center" width="15%">Rank</th>'+
									'<th class="text-center" width="15%">Percentile</th>'+
								'</tr>'+
							'</thead>'+
							'<tbody></tbody>'+
						'</table>');
		for (var i = 0; i < mExam.length; i++) {
			$('#exam tbody').append('<tr class="table-row" data-student="'+i+'" data-max="'+mExam.maxMarks+'">'+
										'<td class="text-center">'+mExam[i]['studentId']+'</td>'+
										'<td>'+mExam[i]['studentName']+'</td>'+
										'<td class="text-center">'+round2(mExam[i]['marks'])+((exam.type=='quick')?'<button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button>':'')+'</td>'+
										'<td class="text-center">'+mExam[i]['percent']+'</td>'+
										'<td class="text-center">'+mExam[i]['rank']+'</td>'+
										'<td class="text-center">'+mExam[i]['percentile']+'</td>'+
									'</tr>');
		};
		drawManualExam();
	}
	$('#exam').on('click', '.table-deep .table-row', function(){
		var studentIndex = $(this).attr('data-student');
		var marks = mExam[studentIndex]['qMarks'];
		//console.log(students[studentIndex]);
		console.log(marks);
		$('#questionMarks').html('<table class="table table-bordered">'+
									'<thead>'+
										'<tr><th colspan="3">Student ID : '+mExam[studentIndex]['studentId']+'</th></tr>'+
										'<tr><th colspan="3">Student Name : '+mExam[studentIndex]['studentName']+'</th></tr>'+
										'<tr><th width="40%">Question</th><th width="30%">Marks</th><th width="30%">Max Marks</th></tr>'+
									'</thead>'+
									'<tbody></tbody></table>');
		for (var i = 0; i < marks.length; i++) {
			$('#questionMarks tbody').append('<tr data-marks="'+marks[i]['id']+'" data-max="'+data.exam.questions[i]['marks']+'">'+
												'<td>Q'+(i+1)+'</td>'+
												'<td>'+marks[i]['marks']+'<button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button></td>'+
												'<td>'+data.exam.questions[i]['marks']+'</td>'+
											'</tr>');
		};
		$('#marksModal').modal('show');
		setTimeout(function(){drawInnerManualExam(studentIndex);}, 500);
		//drawInnerManualExam(studentIndex);
	});
	function drawInnerManualExam(studentIndex) {
		var exam = mExam[studentIndex];
		var dataGraph = [];
		var dataMin = [];
		var dataMax = [];
		var categories = [];
		var qMarks = exam['qMarks'];
		var qName = "";
		for (var i = 0; i < qMarks.length; i++) {
			qname = 'Q'+(i+1);
			categories[i] = qname;
			dataMin[i] = parseFloat(qMarks[i]['marks']);
			dataMax[i] = data.exam.questions[i]['marks']-parseFloat(qMarks[i]['marks']);
		};
		dataGraph.push({name: 'Marks received', data:dataMin});
		dataGraph.push({name: 'Marks lost', data:dataMax});
		//console.log(dataGraph);
		$('#innerAnalytics').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: exam['studentName']
	        },
	        subtitle: {
	            text: 'Student ID : '+exam['studentId']
	        },
	        xAxis: {
	            categories: categories
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Marks'
	            },
	            stackLabels: {
	                enabled: true,
	                style: {
	                    fontWeight: 'bold',
	                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
	                }
	            }
	        },
	        legend: {
	            align: 'right',
	            x: -30,
	            verticalAlign: 'top',
	            y: 25,
	            floating: true,
	            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
	            borderColor: '#CCC',
	            borderWidth: 1,
	            shadow: false
	        },
	        tooltip: {
	            headerFormat: '<b>{point.x}</b><br/>',
	            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
	        },
	        plotOptions: {
	            column: {
	                stacking: 'normal',
	                dataLabels: {
	                    enabled: true,
	                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
	                    style: {
	                        textShadow: '0 0 3px black'
	                    }
	                }
	            }
	        },
	        series: dataGraph
	    });
	}
	/*function drawInnerManualExam(studentIndex) {
		var exam = mExam[studentIndex];
		var dataGraph = [];
		var qMarks = exam['qMarks'];
		for (var i = 0; i < qMarks.length; i++) {
			dataGraph.push({name:'Q'+(i+1),y:parseFloat(qMarks[i]['marks']),max:parseFloat(data.exam.questions[i]['marks'])});
		};
		//console.log(data.exam.questions);
		$('#innerAnalytics').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Student ID : '+exam['studentId']
	        },
	        subtitle: {
	            text: exam['studentName']
	        },
	        xAxis: {
	            type: 'category',
	            labels: {
	                rotation: -45,
	                style: {
	                    fontSize: '13px',
	                    fontFamily: 'Verdana, sans-serif'
	                }
	            }
	        },
	        yAxis: {
	            title: {
	                text: 'Questions'
	            }

	        },
	        legend: {
	            enabled: false
	        },
	        plotOptions: {
	            series: {
	                borderWidth: 0,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.y:.1f}'
	                }
	            }
	        },

	        tooltip: {
	            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	            pointFormat: '<span style="color:{point.color}">{point.name}</span>:<br/>Marks - <b>{point.y:.2f}</b> out of {point.max}<br/>'
	        },

	        series: [{
	            name: 'Students',
	            colorByPoint: true,
	            data: dataGraph
	        }]
	    });
	}*/
	$('#exam,#questionMarks').on('click', '.table tr .btn-edit', function(){
		var marks = $(this).closest('td').text();
		$(this).closest('td').html('<div class="input-group">'+
										'<input type="text" class="form-control" data-oldval="'+marks+'" value="'+marks+'">'+
										'<span class="input-group-btn">'+
											'<button class="btn btn-success btn-save" type="button"><i class="fa fa-save"></i></button>'+
											'<button class="btn btn-danger1 btn-cancel" type="button"><i class="fa fa-times"></i></button>'+
										'</span>'+
									'</div>');
	});
	$('#exam,#questionMarks').on('click', '.table tr .btn-cancel', function(){
		var marks = $(this).closest('td').find('.form-control').attr('data-oldval');
		$(this).closest('td').html(marks+'<button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button>');
	});
	$('#exam,#questionMarks').on('click', '.table tr .btn-save', function(){
		var marks = parseFloat($(this).closest('td').find('.form-control').val());
		if (data.exam.type=="quick") {
			var studentIndex = $(this).closest('tr').attr('data-student');
			var marksId = mExam[studentIndex]['qMarks']['id'];
		} else {
			var marksId = $(this).closest('tr').attr('data-marks');
		}
		var maxMarks = parseFloat($(this).closest('tr').attr('data-max'));
		if (marks=='') {
			alert('Please enter some marks');
		} else if (marks>maxMarks) {
			//console.log(maxMarks);
			alert('Please enter valid marks');
		} else {
			var req = {};
			var res;
			req.action = 'save-manual-exam-marks';
			req.marksId = marksId;
			req.examId = getUrlParameter('examId');
			req.marks = marks;
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else
					window.location = window.location;
			});
		}
	});
	$('#examTitle').on('click', '.btn-edit', function(){
		$('#examTitle').html('<div class="input-group">'+
								'<input type="text" class="form-control" value="'+data.exam.name+'">'+
								'<span class="input-group-btn">'+
									'<button class="btn btn-success btn-save" type="button"><i class="fa fa-save"></i></button>'+
									'<button class="btn btn-danger1 btn-cancel" type="button"><i class="fa fa-times"></i></button>'+
								'</span>'+
							'</div>');
	});
	$('#examTitle').on('click', '.btn-cancel', function(){
		$('.help-block').remove();
		$('#examTitle').html('<h2>Exam : '+data.exam.name+' <button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button></h2>');
	});
	$('#examTitle').on('click', '.btn-save', function(){
		$('.help-block').remove();
		var examName = $(this).closest('.input-group').find('.form-control').val();
		if (!examName) {
			$('#examTitle').find('.input-group').after('<span class="help-block text-danger">Please enter exam title.</span>');
		} else {
			var req = {};
			var res;
			req.action = 'save-manual-exam-name';
			req.examId = getUrlParameter('examId');
			req.examName = examName;
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else {
					data.exam.name = examName;
					$('#examTitle').html('<h2>Exam : '+data.exam.name+' <button class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></button></h2>');
				}
			});
		}
	});
	function fetchBreadcrumb() {
		var req = {};
		var res;
		req.action = 'get-breadcrumb-for-manual-exam';
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				//console.log(res);
				fillBreadcrumb(res);
			}
		});
	}
	
	function fillBreadcrumb(data) {
		$('ul.breadcrumb li:eq(0)').find('a').attr('href', 'edit-course.php?courseId=' + data.course.courseId).text(data.course.courseName);
		$('ul.breadcrumb li:eq(1)').find('a').attr('href', 'edit-subject.php?courseId=' + data.course.courseId + '&subjectId=' + data.subject.subjectId).text(data.subject.subjectName);
		$('ul.breadcrumb li:eq(2)').find('a').attr('href', 'manualexam.php?courseId=' + data.course.courseId + '&subjectId=' + data.subject.subjectId + '&examId=' + data.exam.examId).text(data.exam.examName);
	}

	function round2(number) {
		return Math.round(number*100)/100;
	}
	
	function setError(where, what) {
		unsetError(where);
		where.parent().addClass('has-error');
		where.parent().append('<span class="help-block">'+what+'</span>');
	}
	
	function unsetError(where) {
		if(where.parent().hasClass('has-error')) {
			where.parent().find('.help-block').remove();
			where.parent().removeClass('has-error');
		}
	}
});