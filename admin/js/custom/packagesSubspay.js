var error=0;
var students=0;
var amount=1000;
var currency=2;
$(function(){
		checkCurrency();
		fetchpackageInfo();	
		function fetchpackageInfo() {
		var courses=1;
		var req = {};
		var res;
		req.packId=getUrlParameter('newcase');
		req.action = 'get-package-details';
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
		console.log(res);
			res =  $.parseJSON(res);
			if(res.status == 0) {
				alert(res.message);
			}
			else{
				students=res.packages[0]['count'];
				courses=res.packages[0]['courses'];
				$('#totalStudents').html('<strong>Total number of Students  </strong>'+' '+ students);
				if(courses >10)
				{
					courses=10;
				}
				$('#paynowpack').show();
				amount= 100*courses*students;
				if(currency==1)
				{ 
					amount=5*courses*students;
				}
				amount=parseFloat(amount).toFixed(2);
				if(currency==1)
				{
					$('#totalAmount').html('<strong>Total Amount </strong>'+'<i class="fa fa-dollar"></i>'+'  '+amount);
				}else{
					$('#totalAmount').html('<strong>Total Amount </strong>'+'<i class="fa fa-inr"></i>'+'  '+amount);
				}
				
				console.log(res.packages[0]['count']);
				}
		});
	}																																																																				
	
			
		$('#paynowpack').on('click', function() {
		fetchpackageInfo();
				if(currency==1)
					{
						$('#purchaseNowModal .amount').html('<strong>Total Amount </strong>'+'<i class="fa fa-dollar"></i>'+'  '+amount);
					}else{
						$('#purchaseNowModal .amount').html('<strong>Total Amount </strong>'+'<i class="fa fa-inr"></i>'+'  '+amount);
					}
			$('#purchaseNowModal').modal('show');
		});
		
		
			
			
		$('#payUMoneyButton').on('click', function() {
				var today= new Date().getTime();
				var req = {};
				var res;
				req.action = 'purchasePackage';
				req.packId = getUrlParameter('newcase');
				req.date= today;
				req.couponCode="";
				req.couStatus="";
				$.ajax({
					'type' : 'post',
					'url'  : ApiEndpoint,
					'data' : JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 1) {
						if(res.paymentSkip == 1)
							window.location = 'student/';
						else if(res.method == 2){
							var html = '';
							html = '<form action="'+res.url+'" method="post" id="payUForm">'
								+ '<input type="hidden" name="txnid" value="'+res.txnid+'">'
								+ '<input type="hidden" name="key" value="'+res.key+'">'
								+ '<input type="hidden" name="amount" value="'+res.amount+'">'
								+ "<input type='hidden' name='productinfo' value='"+res.productinfo+"'>"
								+ '<input type="hidden" name="firstname" value="'+res.firstname+'">'
								+ '<input type="hidden" name="email" value="'+res.email+'">'
								+ '<input type="hidden" name="surl" value="'+res.surl+'">'
								+ '<input type="hidden" name="furl" value="'+res.furl+'">'
								+ '<input type="hidden" name="curl" value="'+res.curl+'">'
								+ '<input type="hidden" name="furl" value="'+res.furl+'">'
								+ '<input type="hidden" name="hash" value="'+res.hash+'">'
								+ '<input type="hidden" name="service_provider" value="'+res.service_provider+'">';
								+ '</form>'
							$('body').append(html);
							$('#payUForm').submit();
						}
					else if(res.method == 1) {
						var html = '';
						html += '<form action="' + res.url + '" method="post" id="paypalForm">'
							+ '<!-- Identify your business so that you can collect the payments. -->'
							+ '<input type="hidden" name="business" value="' + res.business + '">'
							+ '<!-- Specify a Buy Now button. -->'
							+ '<input type="hidden" name="cmd" value="_xclick">'
							+ '<!-- Specify details about the item that buyers purchase. -->'
							+ '<input type="hidden" name="item_name" value="' + res.item_name + '">'
							+ '<input type="hidden" name="amount" value="' + res.amount + '">'
							+ '<input type="hidden" name="currency_code" value="USD">'
							+ '<input type="hidden" name="quantity" value="1">'
							+ '<input type="hidden" name="invoice" value="' + res.orderId + '">'
							+ '<input type="hidden" name="item_number" value="' + res.orderId + '">'
							+ '<input type="hidden" name="return" value="' + res.surl + '" />'
							+ '<input type="hidden" name="cancel_return" value="' + res.curl + '" />'
							+ '<input type="hidden" name="notify_url" value="' + res.nurl + '" />'
							+ '<input type="hidden" name="lc" value="US" />'
							+ '<!-- Display the payment button. -->'
							+ '<input type="image" name="submit" border="0" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online">'
							+ '<img alt="" border="0" width="1" height="1"	src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >'
						+ '</form>';
						$('body').append(html);
						$('#paypalForm').submit();
					}
					}else{
						alert(res.message);
						$('#purchaseNowModal').modal('hide');
					}
				});
			});
		
		
		function checkCurrency(){
		
			var req = {};
			var res;
			req.action = 'get-currency';
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status==1){
					currency=res.currency[0].currency;
					
				} 
				
				
				
			});
		}
			
		
		});