/*
	Author: Fraaz Hashmi.
	Dated: 6/28/2014
	********
	1) Notes: JSON.stringify support for IE < 10
*/
var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var userRole;
var reorderTimer = null;
$(function () {
	var imageRoot = '/';
	$('#loader_outer').hide();
	
	fetchIndependentExams();
	fetchBreadcrumb();
	
	$('#newChapter').on('click', function() {
		location.reload();
	});
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
			console.log(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fetchCourseDetails() {
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action = "get-course-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			console.log(res);
			res =  $.parseJSON(res);
			fillCourseDetails(res);
		});
	}
	
	function fillCourseDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#course-name').html(data.courseDetails.name);
	}
	
	function fetchSubjectDetails() {
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		
		req.subjectId = getUrlParameter('subjectId');
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="courses.php";
			return;
		}
		return;
		req.action = "get-subject-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			console.log(res);
			res =  $.parseJSON(res);
			fillCourseDetails(res);
		});
	}
	
	function fillSubjectDetails(data) {
		// General Details
		if(data.subjectDetails.deleted == 1)
			alertMsg('This subject has been deleted!!', 20000)
		var imageRoot = 'user-data/images/';
		$('#course-name').html(data.courseDetails.name);
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fetchSubjectChapters() {
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action = "get-subject-chapters";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1)
				fillSubjectChapters(res);
			else
				alertMsg(res.message);
		});
	}
	
	//function to fill chapter table with associated exams and assignments
	function fillSubjectChapters(data) {
		var html = "";
		var flag = false;
		var editLink;
		var ChapterNum = 1;
		$.each(data.chapters, function(i, chapter) {
			if(flag == false)
				flag = true;
			editLink = "content.php?courseId=" + data.courseId + "&subjectId=" + chapter.subjectId +  "&chapterId=" + chapter.id;
			html += "<tr data-ch-id='" + chapter.id  + "'>"
					+ "<td><a data-href='" + editLink + "' class='modal-pop' data-chapterId='" + chapter.id + "'>Chapter "+ ChapterNum + ": " + chapter.name + "</a>&emsp;&emsp;<a href='#'><i class='fa fa-trash-o delete-subject'></i></a></td>";
			var examhtml = '<td>';
			var assignmenthtml = '<td>';
			for(var j = 0; j < chapter.exams.length; j++) {
				if(chapter.exams[j].type == "Exam")
					examhtml += '<a examId='+chapter.exams[j].id+' independent=1 type=Exam data-startDate="'+chapter.exams[j].startDate+'" data-endDate="'+chapter.exams[j].endDate+'" data-status='+chapter.exams[j].status+' data-attempts='+chapter.exams[j].attempts+'   class="examPopup" >' + chapter.exams[j].name + ' </a><br />';
				else if(chapter.exams[j].type == "Assignment")
					assignmenthtml += '<a examId='+chapter.exams[j].id+' independent=1 type=Assignment data-startDate="'+chapter.exams[j].startDate+'" data-endDate="'+chapter.exams[j].endDate+'"  data-status='+chapter.exams[j].status+' data-attempts='+chapter.exams[j].attempts+'   class="examPopup" >' + chapter.exams[j].name + ' </a><br />';
			}
			examhtml += '</td>';
			assignmenthtml += '</td>';
			html += assignmenthtml + examhtml + "</tr>";
			ChapterNum++;
		});
		
		if(flag == true)
			$('#existing-chapters tbody').html(html);
					else{
						$('#existing-chapters tbody').html('<tr><td>No Chapters added yet</td></tr>');
					}
			
		//event handlers for chapter modal pop up
		$('.modal-pop').on('click', function() {
			$('#chapter-click').modal('show');
			$('#chapter-click .content-link').attr('href', $(this).attr('data-href'));
			$('#chapter-click .exam-link').attr('href', 'add-assignment.php?courseId=' + getUrlParameter('courseId') + '&subjectId=' + getUrlParameter('subjectId') + '&chapterId=' + $(this).attr('data-chapterId'));
		});
		
		
		makeChaptersSortable('#existing-chapters tbody');
		
		//event handlers for chapter delete button click
		$('.delete-subject').on('click', function() {
			var con = confirm("Are you sure to delete this chapter? All associated exams and assignments will become independent.");
			if(con) {
				var chapterId = $(this).parents('tr:eq(0)').attr('data-ch-id');
				var req = {};
				var res;
				req.action = 'delete-chapter';
				req.chapterId = chapterId;
				$.ajax({
					'type'  : 'post',
					'url'   : ApiEndpoint,
					'data' 	: JSON.stringify(req)
				}).done(function (res) {
					res =  $.parseJSON(res);
					if(res.status == 0)
						alert(res.message);
					else
						fetchSubjectChapters();
				});
			}
		});

		$('.examPopup').off('click');
		$('.examPopup').on('click', function () {
			var tr = $(this).parent().parent('tr');
			var data=tr.attr('data-ch-id');
			var chapter='';
			var independent=$(this).attr('independent');
			if(independent==0){
				chapter='Independent';
			}else{
				chapter=tr.attr('data-chapter');
			}
			var type = $(this).attr('type');
			var examName = $(this).text();
			var startDate = new Date(parseInt($(this).attr('data-startDate')));
			var displayEndDate='';
			if(($(this).attr('data-endDate'))==''){
				displayEndDate='--';
			}else{
				displayEndDate=new Date(parseInt($(this).attr('data-endDate')));
			}
			var endDate = displayEndDate;
			var attempts =$(this).attr('data-attempts');
			var status =$(this).attr('data-status');
			var examId=$(this).attr('examId');
			var displayStatus='';
			if(status==0)
				displayStatus='Draft';
			   else if (status==1)
					displayStatus='NOT Live';
				else
					 displayStatus='Live';
			
			$('#examId').val(examId);
			$('#modelin-exam-name').text(examName);
			$('#modal-copy-exam #new-exam').val('Copy of '+examName);
			$('#modelin-chapter-name').text(chapter);
			$('#modelin-exam-startDate').text(startDate.getDate() + '-' +(startDate.getMonth()+1)+'-'+startDate.getFullYear());
			$('#modelin-exam-attempts').text(attempts);
			$('#modelin-exam-status').text(displayStatus);
			if(endDate == '--'){
				  $('#modelin-exam-endDate').text(endDate);
			}else{
				  $('#modelin-exam-endDate').text(endDate.getDate() + '-' +(endDate.getMonth()+1)+'-'+endDate.getFullYear());
			}
		  
			$('#modelin-exam-type').text(type);
			$('#modal-exam .step1').attr('href','edit-assignment.php?examId='+examId);
			$('#modal-exam .step2').attr('href','add-assignment-2.php?examId='+examId);
			$('#modal-exam .report').attr('href','examResult.php?examId='+examId);
			
			$('#modal-exam').modal('show');

			//$('#chapter-click .content-link').attr('href', $(this).attr('data-href'));
			//$('#chapter-click .exam-link').attr('href', 'add-assignment.php?courseId=' + getUrlParameter('courseId') + '&subjectId=' + getUrlParameter('subjectId'));
		});
	}
	
	//to add BCT dynamically
	function fetchBreadcrumb() {
		var req = {};
		var res;
		req.action = 'get-breadcrumb-for-add';
		req.subjectId = getUrlParameter('subjectId');
		req.courseId = getUrlParameter('courseId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
				fillBreadcrumb(res);
		});
	}
	
	function fillBreadcrumb(data) {
		$('ul.breadcrumb li:eq(0)').find('a').attr('href', 'edit-course.php?courseId=' + data.courseId).text(data.courseName);
		$('ul.breadcrumb li:eq(1)').find('a').attr('href', 'edit-subject.php?courseId=' + data.courseId + '&subjectId=' + data.subjectId).text(data.subjectName);
	}
	
	//function to fetch all independent exams and assignments
	function fetchIndependentExams() {
		var req = {};
		var res;
		req.subjectId = getUrlParameter('subjectId');
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action = "get-independent-exams";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1)
				fillIndependentExams(res);
			else
				alertMsg(res.message);
		});
	}
	
	//function to fill independent exams table
	function fillIndependentExams(data) {
		var ehtml = '';
		var ahtml = '';
		for(var i = 0; i < data.exams.length; i++) {
			if(data.exams[i].type == "Exam")
				ehtml += '<a examId='+data.exams[i].id+' independent=0 type=Exam data-startDate="'+data.exams[i].startDate+'" data-endDate="'+data.exams[i].endDate+'"  data-status='+data.exams[i].status+' data-attempts='+data.exams[i].attempts+'   class=examPopup >' + data.exams[i].name + ' </a><br />';
			else if(data.exams[i].type == "Assignment")
				ahtml += '<a examId='+data.exams[i].id+' independent=0 type=Assignment data-startDate="'+data.exams[i].startDate+'" data-endDate="'+data.exams[i].endDate+'"  data-status='+data.exams[i].status+' data-attempts='+data.exams[i].attempts+'   class=examPopup >' + data.exams[i].name + ' </a><br />';
		}
		if(data.exams.length == 0)
			html = '&emsp;No exams or assignments found.';
		else
			html = '<tr><td>' + ahtml + '</td><td>' + ehtml + '</td></tr>';
		$('#independentExams tbody').append(html);
		$('.examPopup').off('click');
		$('.examPopup').on('click', function () {
			var tr = $(this).parent().parent('tr');
			var data=tr.attr('data-ch-id');
			var chapter='';
			var independent=$(this).attr('independent');
			if(independent==0){
				chapter='Independent';
			}else{
				chapter=tr.attr('data-chapter');
			}
			var type = $(this).attr('type');
			var examName = $(this).text();
			var startDate = new Date(parseInt($(this).attr('data-startDate')));
			var displayEndDate='';
			if(($(this).attr('data-endDate'))==''){
				displayEndDate='--';
			}else{
				displayEndDate=new Date(parseInt($(this).attr('data-endDate')));
			}
			var endDate = displayEndDate;
			var attempts =$(this).attr('data-attempts');
			var status =$(this).attr('data-status');
			var examId=$(this).attr('examId');
			var displayStatus='';
			if(status==0)
				displayStatus='Draft';
			   else if (status==1)
					displayStatus='NOT Live';
				else
					 displayStatus='Live';
			
			$('#examId').val(examId);
			$('#modelin-exam-name').text(examName);
			$('#modal-copy-exam #new-exam').val('Copy of '+examName);
			$('#modelin-chapter-name').text(chapter);
			$('#modelin-exam-startDate').text(startDate.getDate() + '-' +(startDate.getMonth()+1)+'-'+startDate.getFullYear());
			$('#modelin-exam-attempts').text(attempts);
			$('#modelin-exam-status').text(displayStatus);
			if(endDate == '--'){
				  $('#modelin-exam-endDate').text(endDate);
			}else{
				  $('#modelin-exam-endDate').text(endDate.getDate() + '-' +(endDate.getMonth()+1)+'-'+endDate.getFullYear());
			}
		  
			$('#modelin-exam-type').text(type);
			$('#modal-exam .step1').attr('href','edit-assignment.php?examId='+examId);
			$('#modal-exam .step2').attr('href','add-assignment-2.php?examId='+examId);
			$('#modal-exam .report').attr('href','examResult.php?examId='+examId);
			
			$('#modal-exam').modal('show');

			//$('#chapter-click .content-link').attr('href', $(this).attr('data-href'));
			//$('#chapter-click .exam-link').attr('href', 'add-assignment.php?courseId=' + getUrlParameter('courseId') + '&subjectId=' + getUrlParameter('subjectId'));
		});
	}

	$('#modal-exam .delete-exam').off('click');
	$('#modal-exam .delete-exam').on('click', function () {
		var con = confirm("Are you sure you want to delete this Exam/Assignment.");
		if (con) {
			var req = {};
			req.action = 'delete-exam';
			req.examId = $('#modal-exam #examId').val();
			$.ajax({
				'type': 'post',
				'url': ApiEndpoint,
				'data': JSON.stringify(req)
			}).done(function (res) {
				res = $.parseJSON(res);
				if (res.status == 0)
					alert(res.message);
				else {
					//fetchExams($('#subjectSelect').val());
					fetchSubjectChapters();
					$('#modal-exam').modal('hide');
				}
			});
		}
	});
	
   $('#modal-copy-exam #copy-exam').off('click');
   $('#modal-copy-exam #copy-exam').on('click', function () {
		$(this).attr('disabled', true);
		var newExamname = $('#modal-copy-exam #new-exam').val();
		if (newExamname != null) {
			var req = {};
			req.action = 'copy-exam';
			req.iexamId = $('#modal-exam #examId').val();
			req.iname = newExamname;
			$.ajax({
				'type': 'post',
				'url': ApiEndpoint,
				'data': JSON.stringify(req)
			}).done(function (res) {
				$('#modal-copy-exam #copy-exam').attr('disabled', false);
				res = $.parseJSON(res);
				if (res.status == 0)
					alert(res.message);
				else {
					//fetchExams($('#subjectSelect').val());
					fetchSubjectChapters();
					$('#modal-copy-exam').modal('hide');
				}
			});
		}
	});
	
	function makeChaptersSortable(selector){
		$(selector).sortable({
			items: '> tr',
			forcePlaceholderSize: true,
			placeholder:'sort-placeholder',
			start: function (event, ui) {
				// Build a placeholder cell that spans all the cells in the row
				var cellCount = 0;
				$('td, th', ui.helper).each(function () {
					// For each TD or TH try and get it's colspan attribute, and add that or 1 to the total
					var colspan = 1;
					var colspanAttr = $(this).attr('colspan');
					if (colspanAttr > 1) {
						colspan = colspanAttr;
					}
					cellCount += colspan;
				});

				// Add the placeholder UI - note that this is the item's content, so TD rather than TR
				ui.placeholder.html('<td colspan="' + cellCount + '">&nbsp;</td>');
				//$(this).attr('data-previndex', ui.item.index());
			},
			update: function(event, ui) {
				// gets the new and old index then removes the temporary attribute
				var newOrder = $.map($(this).find('tr'), function(el) {
					return $(el).attr('data-ch-id')+ "-" +$(el).index();
				});
				if(reorderTimer != null)
					clearTimeout(reorderTimer);
				reorderTimer = setTimeout(function(){updateChapterOrder(newOrder);}, 5000);
			}
		}).disableSelection();
	}
	
	function updateChapterOrder(newOrder) {
		var order = {};
		$.each(newOrder, function(i, v){
			order[v.split('-')[0]] = v.split('-')[1];
		});
		//console.log(order);
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		req.newOrder = order;
		req.action = "update-chapter-order";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			//console.log(res);
			res =  $.parseJSON(res);
			if(res.status == 1)
				fetchSubjectChapters();
			else
				alertMsg(res.message);
		});
	}
	
	$('#chapter-name').on('blur', function() {
		var req = {};
		var res;
		unsetError($('#chapter-name'));
		if($('#chapter-name').val().length < 5 || $('#chapter-name').val().length > 100) {
			setError($('#chapter-name'), 'Please enter a name between 5 to 100 characters.');
			return false;
		}
		//commenting this ar ARC-692
		/*else if(!($('#chapter-name').parents('div:eq(0)').hasClass('has-error'))) {
			unsetError($('#chapter-name'));
		}
		//now checking for duplicate name
		req.action = 'check-duplicate-chapter';
		req.subjectId = getUrlParameter('subjectId');
		req.chapterName = $('#chapter-name').val();
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else if(res.available == false) {
				setError($('#chapter-name'), 'Please select a different name as you have already created a chapter by this name.');
			}
			else
				unsetError($('#chapter-name'));
		});*/
	});
	
	$('#chapter-description').on('blur', function() {
		unsetError($('#chapter-description'));
		if($('#chapter-description').val().length > 1000) {
			setError($('#chapter-description'), 'Please enter a description less than 1000 characters.');
			return false;
		}
	});
	
	//fetchProfileDetails();
	fetchSubjectDetails();
	fetchCourseDetails();
	fetchSubjectChapters();
	//fetchSubjectDetails();
	
	//Event handlers
	
	$('#create-chapter').on('click', function(e) {
		e.preventDefault();
		$('#chapter-description, #chapter-name').blur();
		if($(document).find('.has-error').length > 0)
			return;
		var req = {};
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
				req.chapterName = $('#chapter-name').val();
				
			  if( req.chapterName.length < 5 || req.chapterName.length > 70) {
			 $('#chapter-name').after('<span class="help-block text-danger">Invalid chapter name. Must be between 5 to 70 chars.</span>');
			 return false;
		}
				
		req.chapterDesc = $('#chapter-description').val();
				req.demo =$("#is-demo").prop("checked") ? 1 : 0; 
				req.action = 'create-chapter';
		
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done( function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				//alertMsg(res.message);
				$('#chapter-name').val('');
				$('#chapter-description').val('');
				$('#chapter-created #chapter-name').html(req.subjectName);
				var contentUrl = 'content.php?courseId=' + req.courseId + '&subjectId=' + req.subjectId + '&chapterId=' + res.chapterId;
				$('#chapter-created .content-link').attr('href', contentUrl);
				$('#chapter-created .exam-link').attr('href', 'add-assignment.php?courseId=' + req.courseId + '&subjectId=' + req.subjectId + '&chapterId=' + res.chapterId);
				
				$('#chapter-created').modal('show');
				fetchSubjectChapters();
			} else if(res.status == 2) {
				setError($('#chapter-name'), res.message);
			}
			else {
				alertMsg(res.message);
			}
		});	
	});
	
	
	function setError(where, what) {
		unsetError(where);
		where.parents('div:eq(0)').addClass('has-error');
		where.parents('div:eq(0)').append('<span class="help-block">'+what+'</span>');
	}
	
	function unsetError(where) {
		if(where.parents('div:eq(0)').hasClass('has-error')) {
			where.parents('div:eq(0)').find('.help-block').remove();
			where.parents('div:eq(0)').removeClass('has-error');
		}
	}
});
