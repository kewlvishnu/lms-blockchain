/*
	Author: Fraaz Hashmi.
	Dated: 6/19/2014
	
	
	********
	1) Notes: JSON.stringify support for IE < 10
*/

var jcrop_api;
var userRole;
var ApiEndpoint = '../api/index.php';
var newSelectedInstitutes = {};
var imageC;
var canvasC;
var imageP;
var canvasP;
$(function () {	
   	$('a.profile').addClass('active');
	var imageRoot = '/';
	$('#loader_outer').hide();
	var fileApiEndpoint = '../api/files1.php';
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			
			res =  $.parseJSON(res);
			if(res.status == 0)
				alertMsg(res.message);
			else
				fillProfileDetails(res);
			// }catch(e) {
				// alertMsgMsg(e);
			// }
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
                $('#user-type').text(formatUserRole(data.userRole)+' (ID : '+data.loginDetails.id+ ' )');
		if(data.profileDetails.coverPic != "")
			$('img#cover-pic').attr('src', data.profileDetails.coverPic);
		if(data.profileDetails.profilePic != "") {
			$('img#profile-pic').attr('src', data.profileDetails.profilePic);
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		$('#user-profile-cover').show();
		if(data.profileDetails.description != ""){
                    var showdata=data.profileDetails.description.replace(new RegExp('\r?\n','g'), '<br />');
			$('#user-bio p.text').html(showdata);
			$('#user-bio textarea').val(data.profileDetails.description);
		}
		else
			$('#user-bio p.text').text("A short description.");
		$('#user-bio').show();
		var html = '';
		if(data.courses.length == 0) {
			html = '<p>No Courses Present</p>';
		}
		for(var i = 0; i < data.courses.length; i++) {
			html += '<tr>'
						+ '<td><a href="edit-course.php?courseId='+data.courses[i].id+'">' + data.courses[i].name + '</a></td>'
						+ '<td>';
			for(var j = 0; j < data.courses[i].subjects.length; j++) {
				html += '<a href="edit-subject.php?courseId='+data.courses[i].id+'&subjectId='+ data.courses[i].subjects[j].id + '">'+ data.courses[i].subjects[j].name + '</a><br>';// + ' : ';
				/*for(var k = 0; k < data.courses[i].subjects[j].professors.length; k++) {
					html += data.courses[i].subjects[j].professors[k].name + ',';
				}*/
			}				
			html += '</td>'
					//+ '<td><a href="edit-course.php?courseId='+data.courses[i].id+'">View Course</a></td>'
				+ '</tr>';
		}
		$('#user-am-experience table.xp ').hide();
		$('#user-am-experience table.xp tbody').append(html);
                $('#user-am-experience table.xp tbody tr').hide();
                 $('#user-am-experience table.xp tbody tr:eq(0)').show();
                 $('#user-am-experience table.xp tbody tr:eq(1)').show();
                  $('#user-am-experience table.xp').show();
                  $('#expand-course').attr('show','plus');
                  if( $('#user-am-experience table.xp tbody tr').length<2){
                        $('#expand-course').hide();
                      }
             $('#expand-course').click(function(){
                 if($(this).attr('show')=='plus'){
                     $('#user-am-experience table.xp tbody tr').show();
                      $('#expand-course').attr('show','minus');
                      $('#expand-course').html('<i class="fa fa-minus-circle"></i> View Less');
                       
                 }else{
                 $('#user-am-experience table.xp tbody tr').hide();
                 $('#user-am-experience table.xp tbody tr:eq(0)').show();
                 $('#user-am-experience table.xp tbody tr:eq(1)').show();
                 $('#expand-course').attr('show','plus');
                  $('#expand-course').html('<i class="fa fa-plus-circle"></i> View More');
                 }
               });
                 
                
		$('#user-am-experience').show();
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1'){
			fillInstituteDetails(data);
			fillInstituteFormDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
			fillProfessorFormDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
			fillPublisherFormDetails(data);
		}
		if(data.userInstituteTypes !== false){
			$('#user-institute-types .view-section').html(formatUserInstituteTypes(data.userInstituteTypes));
			$('#user-institute-types').show();
		}
	}
	
	function fillInstituteDetails(data) {
		//taking year of today to generate year of inception
		var time = new Date(data.profileDetails.time * 1000);
		var year = time.getFullYear();
		var html = '<option value="0">Not Specified</option>';
		for(var i = 1990; i <= year; i++) {
			html += '<option value="' + i + '">' + i + '</option>';
		}
		$('#founded-in-inp').find('option').remove();
		$('#founded-in-inp').append(html);
		
		$('#user-institute-types .top-heading').html('Select Institute types');
		$('#user-institute-types .other-categories label').html('Training Institutes');
		
		$('a#profile-name').text(data.profileDetails.name);
                
                if(data.userRole=='1'){
                    $('#tagline').html(data.profileDetails.tagline+'&emsp;');
                    $('#txt-tagline').val(data.profileDetails.tagline);
                    $('#div-tagline').show();
                }
		     
		$('#institute-general-information span#institute-name').text(data.profileDetails.name);
		var ownerName = data.profileDetails.ownerFirstName + " " + data.profileDetails.ownerLastName;
		if(data.profileDetails.ownerFirstName == '' && data.profileDetails.ownerLastName == '')
			ownerName = "Not Specified";
		$('#institute-general-information span#owner-name').text(ownerName);
		$('#institute-general-information span#contact-name').text(data.profileDetails.contactFirstName + " " + data.profileDetails.contactLastName);
		
		$('#institute-general-information span#institute-size').text(data.profileDetails.studentCount);
		if(data.profileDetails.studentCount == '')
			$('#institute-general-information span#institute-size').text('Not Specified.');
		
		var year = data.profileDetails.foundingYear;
		year = year != 0 ? year : 'Not set';
		$('#institute-general-information span#founded-in').text(year);
		$('#institute-general-information span#contact-num').text(formatContactNumber(data.userDetails));
		$('#institute-general-information span#primary-email').text(data.loginDetails.email);
		$('#institute-general-information span#address-country').text(data.userDetails.addressCountry);
		$('#institute-general-information span#address-details').text(formatAddress(data.userDetails));
		$('#institute-general-information').show();
	}
	
	function fillInstituteFormDetails(data) {
		$('#institute-general-information #institute-name-inp').val(data.profileDetails.name);
		
		$('#institute-general-information #owner-first-name-inp').val(data.profileDetails.ownerFirstName);
		$('#institute-general-information #owner-last-name-inp').val(data.profileDetails.ownerLastName);
		
		$('#institute-general-information #contact-first-name-inp').val(data.profileDetails.contactFirstName);
		$('#institute-general-information #contact-last-name-inp').val(data.profileDetails.contactLastName);
		
		$('#institute-general-information #institute-size-inp').val(data.profileDetails.studentCount);
		
		var year = data.profileDetails.foundingYear;
		$('#institute-general-information #founded-in-inp').val(year);
		
		
		$('#institute-general-information #phnNumPrefix').val(data.userDetails.contactMobilePrefix);
		$('#institute-general-information #phnNum').val(data.userDetails.contactMobile);
		$('#institute-general-information #llNumPrefix').val(data.userDetails.contactLandlinePrefix);
		$('#institute-general-information #llNum').val(data.userDetails.contactLandline);
		
		//$('span#address-country').value(data.userDetails.addressCountryId);
		
		$('#institute-general-information #street-inp').val(data.userDetails.addressStreet);
		$('#institute-general-information #city-inp').val(data.userDetails.addressCity);
		$('#institute-general-information #state-inp').val(data.userDetails.addressState);
		$('#institute-general-information #pin-inp').val(data.userDetails.addressPin);
		$('#institute-general-information .country-inp').val(data.userDetails.addressCountry);
		
		//$('#user-institute-types').html(formatUserInstituteTypes(data.userInstituteTypes));
		
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
		var gender = "Not Specified.";
		if(data.profileDetails.gender == 1)
			gender = "Male";
		if(data.profileDetails.gender == 2)
			gender = "Female";
		$('#professor-general-information span#gender').text(gender);
		$('#professor-general-information span#professor-name').text(name);
		
		$('#professor-general-information span#contact-num').text(formatContactNumber(data.userDetails));
		$('#professor-general-information span#primary-email').text(data.loginDetails.email);
		$('#professor-general-information span#address-country').text(data.userDetails.addressCountry);
		$('#professor-general-information span#address-details').text(formatAddress(data.userDetails));
		$('#professor-general-information').show();
	}
	
	function fillProfessorFormDetails(data) {
		
		$('#professor-general-information #first-name-inp').val(data.profileDetails.firstName);
		$('#professor-general-information #last-name-inp').val(data.profileDetails.lastName);
		
		$('#professor-general-information #phnNumPrefix').val(data.userDetails.contactMobilePrefix);
		$('#professor-general-information #phnNum').val(data.userDetails.contactMobile);
		$('#professor-general-information #llNumPrefix').val(data.userDetails.contactLandlinePrefix);
		$('#professor-general-information #llNum').val(data.userDetails.contactLandline);
		
		//$('span#address-country').value(data.userDetails.addressCountryId);
		
		$('#professor-general-information #street-inp').val(data.userDetails.addressStreet);
		$('#professor-general-information #city-inp').val(data.userDetails.addressCity);
		$('#professor-general-information #state-inp').val(data.userDetails.addressState);
		$('#professor-general-information #pin-inp').val(data.userDetails.addressPin);
		$('#professor-general-information .country-inp').val(data.userDetails.addressCountry);
		//for gender selection
		$('#professor-general-information #gender-inp').val(data.profileDetails.gender);
               
		//$('#user-institute-types').html(formatUserInstituteTypes(data.userInstituteTypes));
		
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
		var gender="Not Specified.";
		if(data.profileDetails.gender == 1)
			gender = "Male";
		if(data.profileDetails.gender == 2)
			gender = "Female";
		$('#publisher-general-information span#gender').text(gender);
		$('#publisher-general-information span#publisher-name').text(name);
		$('#publisher-general-information span#owner-name').text(name);
		$('#publisher-general-information span#contact-person').text(name);
		
		var year = data.profileDetails.foundingYear;
		year = year != 0 ? year : 'Not set';
		$('#publisher-general-information span#founded-in').text(year);
		$('#publisher-general-information span#contact-num').text(formatContactNumber(data.userDetails));
		$('#publisher-general-information span#primary-email').text(data.loginDetails.email);
		$('#publisher-general-information span#address-country').text(data.userDetails.addressCountry);
		$('#publisher-general-information span#address-details').text(formatAddress(data.userDetails));
		
		$('#publisher-general-information').show();
	}
	
	function fillPublisherFormDetails(data) {
		$('#publisher-general-information #first-name-inp').val(data.profileDetails.firstName);
		$('#publisher-general-information #last-name-inp').val(data.profileDetails.lastName);
		
		var year = data.profileDetails.foundingYear;
		$('#publisher-general-information #founded-in-inp').val(year);
		
		
		$('#publisher-general-information #phnNumPrefix').val(data.userDetails.contactMobilePrefix);
		$('#publisher-general-information #phnNum').val(data.userDetails.contactMobile);
		$('#publisher-general-information #llNumPrefix').val(data.userDetails.contactLandlinePrefix);
		$('#publisher-general-information #llNum').val(data.userDetails.contactLandline);
		
		//$('span#address-country').value(data.userDetails.addressCountryId);
		
		$('#publisher-general-information #street-inp').val(data.userDetails.addressStreet);
		$('#publisher-general-information #city-inp').val(data.userDetails.addressCity);
		$('#publisher-general-information #state-inp').val(data.userDetails.addressState);
		$('#publisher-general-information #pin-inp').val(data.userDetails.addressPin);
		$('#publisher-general-information .country-inp').val(data.userDetails.addressCountry);
		//for gender selection
		$('#publisher-general-information #gender-inp').val(data.profileDetails.gender);
		
		//$('#user-institute-types').html(formatUserInstituteTypes(data.userInstituteTypes));
		
	}
	
	function formatUserRole(roleId) {
		if(roleId == '1')
			return "Institute";
		if(roleId == '2')
			return "Professor/Tutor";
		if(roleId == '3')
			return "Publisher";
	}
	
	function formatAddress(user) {
		var address = '';
		if(user.addressStreet != '')
			address += user.addressStreet + ', ';
		if(user.addressCity != '')
			address += user.addressCity + ', ';
		if(user.addressState != '')
			address += user.addressState + ', ';
		if(user.addressPin != '')
			address += user.addressPin + ', ';
		ret = address.replace(/,\s$/, '.');
		if(ret == '')
			return 'Not Specified';
		else
			return ret;
	}
	
	function formatUserInstituteTypes(insTypes) {
		var arrangedIns = {};
		$.each(insTypes, function(i,ins) {
			if(ins.parentId == 0 ) {
				if(typeof arrangedIns[ins.id] !== "undefined" ) {
					var children = arrangedIns[ins.id]['children'];
					arrangedIns[ins.id] = {
						'data': ins.data,
						'name': ins.name,
						'children': children
					}
				}else {
					arrangedIns[ins.id] = {
						'data': ins.data,
						'name': ins.name,
						'children': {}
					}
				}
			}else {
				if(typeof arrangedIns[ins.parentId] == "undefined") {
					arrangedIns[ins.parentId] = {
						'children' : {},
					};
				}
				
				arrangedIns[ins.parentId]['children'][ins.id] = {
					'data': ins.data,
					'name': ins.name,
					'children': {}
				}
			}
		});
		console.log(arrangedIns);
		var html = "";
		var data;
		if(typeof arrangedIns['1'] !== "undefined") {
			var ins = arrangedIns['1'];
			data = $.parseJSON(ins.data);
			html += '<div class="row"><div class="panel-body bio-graph-info margin-top-20">'
					+ '<h1>' + ins.name + '</h1>'
					+ '<div class="row">'
					+ '<div class="col-lg-6 three-row">' + formatSchoolBoard(data) + '</div>'
					+ '<div class="col-lg-6 three-row">' + formatSchoolClass(data) + '</div>'
					+ "</div></div></div>";
		}
		
		if(typeof arrangedIns['2'] !== "undefined") {
			var ins = arrangedIns['2'];
			html += '<div class="row"><div class="panel-body bio-graph-info margin-top-20">'
					+ '<h1>' + ins.name + '</h1>'
					+ '<div class="row">'
					+ '<div class="col-lg-12 three-row">' + formatCoaching(ins, arrangedIns) + '</div>'
					+ "</div></div></div>";
		}
		
		if(typeof arrangedIns['3'] !== "undefined") {
			var ins = arrangedIns['3'];
			html += '<div class="row"><div class="panel-body bio-graph-info margin-top-20">'
					+ '<h1>' + ins.name + '</h1>'
					+ '<div class="row">'
					+ '<div class="col-lg-12 three-row">' + formatColleges(ins) + '</div>'
					+ "</div></div></div>";
		}
		
		if(typeof arrangedIns['4'] !== "undefined") {
			var ins = arrangedIns['4'];
			var name =  userRole == 1 ? "Training Institute": "Other Categories";
			html += '<div class="row"><div class="panel-body bio-graph-info margin-top-20">'
					+ '<h1>' + name + '</h1>'
					+ '<div class="row">'
					+ '<div class="col-lg-12 three-row">' + formatTrainingIns(ins) + '</div>'
					+ "</div></div></div>";
		}
		handleInstituteTypeInputs(arrangedIns);
		return html;
	}
	
	function formatSchoolBoard(data) {
		if(data.boardId == '5')
			return "<strong>Board:</strong> CBSE";
		if(data.boardId == '6')
			return "<strong>Board:</strong> ICSE";
		if(data.boardId == '7')
			return "<strong>Board:</strong> State Board(" + data.state + ")";
		if(data.boardId == '0')
			return "<strong>Board:</strong> " + data.other;
		return " ";
	}
	
	function formatSchoolClass(data) {
		return "<strong>Classes Upto: </strong> " + data.classes + "th" ;
	}
	
	function formatColleges(ins) {
		var html = "";
		$.each(ins.children, function(k,v) {
			html += v.name + ', ';
		});
		data = $.parseJSON(ins.data);
		var others = data.other;
		if( others != "" && others != undefined)
			html += others;
		return html.replace(/,\s$/,'.');
	}
	
	function formatTrainingIns(ins) {
		var html = "";
		$.each(ins.children, function(k,v) {
			html += v.name + ', ';
		});
		data = $.parseJSON(ins.data);
		var others = data.other;
		if( others != "" && others != undefined)
			html += others;
		return html.replace(/,\s$/,'.');
	}
	
	function formatCoaching(ins, arrangedIns) {
		var html = "";
		var others;
		$.each(ins.children, function(k,v) {
			html += "<div><b>" + v.name + ': </b>';
			if(typeof arrangedIns[k] !== "undefined"){
				$.each(arrangedIns[k].children, function(subK,subC) {
					html += subC.name + ", ";
				});
			}
			data = $.parseJSON(v.data);
			others = data.other;
			if( others != "" && others != undefined)
				html += others;
			html = html.replace(/,\s$/, '.') + "</div>";
		});
		data = $.parseJSON(ins.data);
		others = data.other;
		if( others != "" && others != undefined)
			html += "<div> <b>" + others + "</b></div>";
		return html;
	}
	
	function prepareInsDataKey(k) {
		if(k == 'other')
			return '';
		else
			return k + ': ';
	}
	
	fetchProfileDetails();
	
	$('.cover-photo').hover(function() {
		$(this).find('.upload-cover-image').show();
	},function(){
		$(this).find('.upload-cover-image').hide();
	});

	$("#cover-image-form #cover-image-uploader").change(function() {
		$('#uploaded-image1').show();
		$('#cover-image-form input[type="submit"]').show();
		var _URL = window.URL || window.webkitURL;
		var img = new Image();
		var file = this;
		img.onload = function() {
			if(!(img.width < 280 || img.height < 200))
				readURL1(file);
			else {
				$('#cover-image-form input[type=submit]').prop('disabled',true);
				$('#cover-image-form .error').remove();
				$('#cover-image-form').append('<span style="color:red;" class="error">Please select a larger image than 280 x 200.</span>');
			}
		}
		img.src = _URL.createObjectURL(this.files[0]);
	});

	$('#cover-image-form').attr('action', fileApiEndpoint);

	// $('#cover-image-form .submitButton').on('click', function() {
		// $('#cover-image-form').submit();
	// });
	
	$('#cover-image-form').on('submit', function(e) {
		e.preventDefault();
		c = jcrop_api.tellSelect();
		crop = [c.x, c.y, c.w, c.h];
		imageC = document.getElementById('uploaded-image1');
		var sources = {
            'cover-image': {
                image: imageC,
                type: 'image/jpeg',
                crop: crop,
                size: [634, 150],
                quality: 1.0
            }
        }
        //settings for $.ajax function
        var settings = {
			url: fileApiEndpoint,
			data: {'text': 'This is the text!'}, //three fields (medium, small, text) to upload
			beforeSend: function()
			{
				$("#cover-image-form .progress").show();
			},
			complete: function (resp) {
				console.log(resp.responseText);
				$("#cover-image-form .progress").hide();
				$("#cover-image-form .msg").html("<font color='green'>"+resp.responseText+"</font>").show();
				$('#cover-image-form .jcrop-holder').hide();
				$('#cover-image-form input[type="file"]').val('');
				$('#cover-image-form input[type="submit"]').hide();
				fetchProfileDetails();
				$('#upload-cover-image').modal('hide');
			}
        }
        cropUploadAPI.cropUpload(sources, settings);
	});
	
	$('#upload-cover-image').on('hiddn.bs.modal', function() {
		$("#cover-image-form .progress").hide();
		$('#cover-image-form .jcrop-holder').hide();
		$('#cover-image-form input[type="file"]').val('');
		$('#cover-image-form input[type="submit"]').hide();
	});
	
	
	$('.profile-photo').hover(function() {
		$(this).find('.upload-profile-image').show();
	},function(){
		$(this).find('.upload-profile-image').hide();
	});
	
	$("#profile-image-form #profile-image-uploader").change(function() {
        $('#profile-image-form input[type="submit"]').show();
		var _URL = window.URL || window.webkitURL;
		var img = new Image();
		var file = this;
		img.onload = function() {
			if(!(img.width < 200 || img.height < 200))
				readURL2(file);
			else {
				$('#profile-image-form input[type=submit]').prop('disabled',true);
				$('#profile-image-form .error').remove();
				$('#profile-image-form').append('<span style="color:red;" class="error">Please select a larger image than 280 x 200.</span>');
			}
		}
		img.src = _URL.createObjectURL(this.files[0]);
    });
	
	
	$('#profile-image-form').attr('action', fileApiEndpoint);
	
	//uploading profile image
	$('#profile-image-form').on('submit', function(e) {
		e.preventDefault();
		c = jcrop_api.tellSelect();
		crop = [c.x, c.y, c.w, c.h];
		imageP = document.getElementById('uploaded-image2');
		var sources = {
            'profile-image': {
                image: imageP,
                type: 'image/jpeg',
                crop: crop,
                size: [200, 200],
                quality: 1.0
            }
        }
        //settings for $.ajax function
        var settings = {
			url: fileApiEndpoint,
			data: {'text': 'This is the text!'}, //three fields (medium, small, text) to upload
			beforeSend: function()
			{
				$("#profile-image-form .progress").show();
			},
			complete: function (resp) {
				console.log(resp.responseText);
				$("#profile-image-form .progress").hide();
				$("#profile-image-form .msg").html("<font color='green'>"+resp.responseText+"</font>").show();
				$('#profile-image-form .jcrop-holder').hide();
				$('#profile-image-form input[type="file"]').val('');
				$('#profile-image-form input[type="submit"]').hide();
				$('#upload-profile-image').modal('hide');
				fetchProfileDetails();
			}
        }
        cropUploadAPI.cropUpload(sources, settings);
	});
	
	$('#upload-profile-image').on('hiddn.bs.modal', function() {
		$("#profile-image-form .progress").hide();
		$('#profile-image-form .jcrop-holder').hide();
		$('#profile-image-form input[type="file"]').val('');
		$('#profile-image-form input[type="submit"]').hide();
	});
	$('#upload-cover-image').on('show.bs.modal', function () {
		$('#cover-image-form .msg').hide();
	});
	
	$('#upload-profile-image').on('show.bs.modal', function () {
		$('#profile-image-form .msg').hide();
	});
	
	function handleInstituteTypeInputs(arrangedIns) {
		var ins, schoolBoard, schoolClasses;
		if(typeof arrangedIns['1'] !== "undefined") {
			ins = arrangedIns['1'];
			$('#ins-1').prop('checked', true);
			$('#school').show();
			data = $.parseJSON(ins.data);
			newSelectedInstitutes[1] = data;
			schoolBoard = data.boardId;
			$('#school-board').val(schoolBoard);
			if(schoolBoard == 7){
				$('#school-board-state input[type=text]').val(data.state);
				$('#school-board-state').show();
				
			}
			if(schoolBoard == 0){
				$('#school-board').val(-1);
				$('#school-board-other input[type=text]').val(data.other);
				$('#school-board-other').show();
			}
			schoolClasses = data.classes;
			$('#school-classes').val(schoolClasses);
		}
		if(typeof arrangedIns['2'] !== "undefined") {
			ins = arrangedIns['2'];
			$('#ins-2').prop('checked', true);
			$('#coaching').show();
			newSelectedInstitutes[2] = {};
			$.each(ins.children, function(k,v) {
				$('#ins-' + k).prop('checked', true);
				if(k == 8)
					$('#management').show();
				if(k == 9)
					$('#engineering').show();
				if(k == 10)
					$('#medical').show();
				if(k == 11)
					$('#schoolLevel').show();
				if(k == 12)
					$('#misc').show();
				data = $.parseJSON(v.data);
				newSelectedInstitutes[k] = data;
				if(typeof data.other !== "undefined") {
					$('#ins-' + k + '-other input[type=text]').val(data.other);
					$('#ins-' + k + '-other').show();
					$('#ins-' + k + '-subcat').multiSelect('select', '-1');
				}
				if(typeof arrangedIns[k] !== "undefined") {
					$.each(arrangedIns[k].children, function(subK,subC) {
						$('#ins-' + k + '-subcat').multiSelect('select', subK);
					});
				}
			});
		}
		if(typeof arrangedIns['3'] !== "undefined") {
			ins = arrangedIns['3'];
			$('#ins-3').prop('checked', true);
			$('#college').show();
			data = $.parseJSON(ins.data);
			newSelectedInstitutes[3] = data;
			if(typeof data.other !== "undefined") {
				$('#ins-3-other input[type=text]').val(data.other);
				$('#ins-3-other').show();
				$('#ins-3-subcat').multiSelect('select', '-1');
			}
			$.each(ins.children, function(k,v) {
				$('#ins-3-subcat').multiSelect('select', k);
			});
		}
		if(typeof arrangedIns['4'] !== "undefined") {
			ins = arrangedIns['4'];
			$('#ins-4').prop('checked', true);
			$('#other').show();
			data = $.parseJSON(ins.data);
			newSelectedInstitutes[4] = data;
			if(typeof data.other !== "undefined") {
				$('#ins-4-other input[type=text]').val(data.other);
				$('#ins-4-other').show();
				$('#ins-4-subcat').multiSelect('select', '-1');
			}
			$.each(ins.children, function(k,v) {
				$('#ins-4-subcat').multiSelect('select', k);
			});
		}

		if(typeof arrangedIns['4'] !== "undefined") {
			var ins = arrangedIns['4'];
			var name =  userRole == 1 ? "Training Institute": "Other Categories";
			
		}
	}

	function updateInstituteTypes(elem) {
		$('.form-msg').html('').hide();
		var newStructuredInstitutes = {};
		var board;
		if(typeof newSelectedInstitutes[1] == "undefined"
			&& typeof newSelectedInstitutes[2] == "undefined" 
			&& typeof newSelectedInstitutes[3] == "undefined" 
			&& typeof newSelectedInstitutes[4] == "undefined") {
				$('.form-msg.overall').html('Please select a category').show();
				return ;
		}
		if(typeof newSelectedInstitutes[1] !== "undefined"){
			board = $('#school-board').val();
			state = $('#school-board-state input[type=text]').val();
			otherBoard = $('#school-board-other input[type=text]').val();
			classes = $('#school-classes').val();
			if(board == 0){
				$('.form-msg.board').html('Please select a board').show();
				return;
			}
			if(board == 7 && state.length < 2) {
				$('.form-msg.board-state').html('Invalid state name.').show();
				return;
			}
			if(board == -1 && otherBoard == "") {
				$('.form-msg.board-other').html('Please specify a board.').show();
				return;
			}
			if(classes == 0) {
				$('.form-msg.classes').html('Please select classes.').show();
				return;
			}
			//Validated
			if(board == 7) {
				data = {
					"boardId" : "7",
					"state" : state
				};
			} else if(board == -1) {
				data = {
					"boardId" : "0",
					"other" : otherBoard
				};
			} else {
				data = {
					"boardId" : board
				};
			}
			data["classes"] = classes;
			newStructuredInstitutes[1] = data;
		}
		if(typeof newSelectedInstitutes[2] !== "undefined") {
			selectedSubcat = getSelectedSubcat(2);
			if(countDictKeys(selectedSubcat) == 0) {
				$('.form-msg.coaching').html('Please select a coaching type.').show();
				return;
			}
			newStructuredInstitutes[2] = {};
			inner = true;
			$.each(selectedSubcat, function(k,v) {
				selectedSubcat2 = getSelectedSubcat(k);
				if(countDictKeys(selectedSubcat2) == 0
					&& typeof newSelectedInstitutes[k]["other"] == "undefined") {
					$('.form-msg.' + k + '-msg').html('Please select a type.').show();
					inner = false;
				}
				if(typeof newSelectedInstitutes[k]["other"] !== "undefined"
					&& $('#ins-' + k + '-other input[type=text]').val() == "") {
					$('.form-msg.' + k + '-msg').html('Please specify a new type.').show();
					inner = false;
				}
				if(typeof newSelectedInstitutes[k]["other"] === "undefined") {
					newStructuredInstitutes[k] = {};
				} else {
					newStructuredInstitutes[k] = {
						"other" : $('#ins-' + k + '-other input[type=text]').val()
					};
				}
				$.each(selectedSubcat2, function(subK,subV) {
					newStructuredInstitutes[subK] = subV;
				});
			});
			if(!inner)
				return;
		}
		if(typeof newSelectedInstitutes[3] !== "undefined") {
			selectedSubcat = getSelectedSubcat(3);
			if(countDictKeys(selectedSubcat) == 0 && typeof newSelectedInstitutes[3]["other"] == "undefined") {
				$('.form-msg.college').html('Please select a college type.').show();
				return;
			}
			if(typeof newSelectedInstitutes[3]["other"] !== "undefined"
				&& $('#ins-3-other input[type=text]').val() == "") {
				$('.form-msg.college-other').html('Please specify new college type.').show();
				return;
			}
			if(typeof newSelectedInstitutes[3]["other"] === "undefined") {
				newStructuredInstitutes[3] = {};
			} else {
				newStructuredInstitutes[3] = {
					"other" : $('#ins-3-other input[type=text]').val()
				};
			}
			$.each(selectedSubcat, function(k,v){
				newStructuredInstitutes[k] = v;
			});
		}
		if(typeof newSelectedInstitutes[4] !== "undefined") {
			selectedSubcat = getSelectedSubcat(4);
			if(countDictKeys(selectedSubcat) == 0 && typeof newSelectedInstitutes[4]["other"] == "undefined") {
				$('.form-msg.training').html('Please select a training type.').show();
				return;
			}
			if(typeof newSelectedInstitutes[4]["other"] !== "undefined"
				&& $('#ins-4-other input[type=text]').val() == "") {
				$('.form-msg.training-other').html('Please specify new training type.').show();
				return;
			}
			if(typeof newSelectedInstitutes[4]["other"] === "undefined") {
				newStructuredInstitutes[4] = {};
			} else {
				newStructuredInstitutes[4] = {
					"other" : $('#ins-4-other input[type=text]').val()
				};
			}
			$.each(selectedSubcat, function(k,v){
				newStructuredInstitutes[k] = v;
			});
		}
		console.log(newStructuredInstitutes);
		var req = {};
		req.newStructuredInstitutes = newStructuredInstitutes;
		req.action = "update-user-institute-type";
		elem.attr('disabled',true);
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				fetchProfileDetails();
				alertMsgMsg(res.message);
				elem.parents('.panel').find('.edit-section').hide();
				elem.parents('.panel').find('.view-section').fadeIn('fast');
			}else {
				alertMsgMsg(e);
			}
			elem.attr('disabled',false);
			elem.parents('section.panel').find('.fa-times').removeClass('fa-times').addClass('fa-pencil');
		});
	}
	
	$('.panel .edit-button').click(function() {
		if($(this).find('i.fa').hasClass('fa-pencil')){
			$(this).find('i.fa').removeClass('fa-pencil').addClass('fa-times');
			$(this).parents('.panel').find('.view-section').hide();
			$(this).parents('.panel').find('.edit-section').fadeIn('fast');
		} else{
			$(this).find('i.fa').removeClass('fa-times').addClass('fa-pencil');
			$(this).parents('.panel').find('.edit-section').hide();
			$(this).parents('.panel').find('.view-section').fadeIn('fast');
			fetchProfileDetails();
		}
	});
    $('#edit-tagline').click(function() {
		$('.help-block').remove();
		$('#tagline').hide();
		$('#edit-tagline').hide();
		$('#txt-tagline').show();
		$('#save-tagline').show();
		$('#cancel-tagline').show();
	});
    $('#cancel-tagline').click(function() {
		$('.help-block').remove();
		$('#tagline').show();
		$('#edit-tagline').show();
		$('#txt-tagline').hide();
		$('#save-tagline').hide();
		$('#cancel-tagline').hide();
	});
        
	$('#save-tagline').click(function() {
		var req = {};
        var tagline=$('#txt-tagline').val();
        $('.help-block').remove();
        if(tagline==''){
              $('#txt-tagline').after('<span class="help-block" style="color:red;"> Tagline can not be empty .</span>');
            return false;
        }
        if(tagline.length>80){
           $('#txt-tagline').after('<span class="help-block" style="color:red;float: left;">&nbsp;&nbsp;Tagline text should not be more than 80 characters. </span>');
            return false;
        }
		req.tagline = tagline;
		req.action = "edit-tagline";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				$('#tagline').text(tagline);
                                $('#edit-tagline').show();
                                $('#tagline').show();
                                $('#save-tagline').hide();
                                $('#cancel-tagline').hide();
				$('#txt-tagline').hide();
			}else {
				alertMsgMsg(e);
			}
			
		});
                
	});
	
	$('.panel .cancel-button').click(function() {
        $('.help-block').remove();
		$(this).parents('.panel').find('.edit-button').click();
		$(this).parents('.panel').find('.edit-section').hide();
		$(this).parents('.panel').find('.view-section').fadeIn('fast');
	});
	
	$('#user-bio .save-button').click(function() {
        $('.help-block').remove();
		var req = {};
		var res;
		var elem = $(this);
		req.action = "update-profile-desc";
		req.description = $('#user-bio textarea').val();
		if(req.description.length>1000){
                  $('#user-bio textarea').after('<span class="help-block" style="color:red;">Background Information should be less than 1000 characters.</span>');
                    return false;
                }
                $(this).attr('disabled',true);
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			try {
				res =  $.parseJSON(res);
				if(res.status == 1) {
					fetchProfileDetails();
					alertMsgMsg(res.message);
					elem.parents('.panel').find('.edit-section').hide();
					elem.parents('.panel').find('.view-section').fadeIn('fast');
				}
				console.log(res);
				
			}catch(e) {
				alertMsgMsg(e);
			}
			finally {
				elem.attr('disabled',false);
				elem.parents('section.panel').find('.fa-times').removeClass('fa-times').addClass('fa-pencil');
			}
		});
	});
	
	$('#institute-general-information .save-button').click(function() {
		$('#institute-name-inp, #contact-first-name-inp, #contact-last-name-inp, #institute-general-information #phnNum, #institute-general-information #llNum').blur();
		if($('.has-error')[0] == undefined) {
			var req = {};
			var res;
			var elem = $(this);
			req.action = "update-profile-general";
			
			req.instituteName = $('#institute-general-information #institute-name-inp').val();
			req.ownerFirstName = $('#institute-general-information #owner-first-name-inp').val();
			req.ownerLastName = $('#institute-general-information #owner-last-name-inp').val();
			req.contactFirstName = $('#institute-general-information #contact-first-name-inp').val();
			req.contactLastName = $('#institute-general-information #contact-last-name-inp').val();
			
			req.studentCount = $('#institute-general-information #institute-size-inp').val();
			
			req.foundedIn = $('#institute-general-information #founded-in-inp').val();
			
			req.phnPrefix = $('#institute-general-information #phnNumPrefix').val();
			req.phnNum = $('#institute-general-information #phnNum').val();
			req.llPrefix = $('#institute-general-information #llNumPrefix').val();
			req.llNum = $('#institute-general-information #llNum').val();
			
			//$('span#address-country').value(data.userDetails.addressCountryId);
			
			req.street = $('#institute-general-information #street-inp').val();
			req.city = $('#institute-general-information #city-inp').val();
			req.state = $('#institute-general-information #state-inp').val();
			req.pin = $('#institute-general-information #pin-inp').val();
			$(this).attr('disabled',true);
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				try {
					res =  $.parseJSON(res);
					if(res.status == 1) {
						fetchProfileDetails();
						alertMsgMsg(res.message);
						elem.parents('.panel').find('.edit-section').hide();
						elem.parents('.panel').find('.view-section').fadeIn('fast');
					}
					console.log(res);
				}catch(e) {
					alertMsgMsg(e);
				}
				finally {
					elem.attr('disabled',false);
					elem.parents('section.panel').find('.fa-times').removeClass('fa-times').addClass('fa-pencil');
				}
			});
		}
	});
	
	$('#professor-general-information .save-button').click(function() {
		$('#professor-general-information #first-name-inp, #professor-general-information #last-name-inp, #professor-general-information #phnNum, #professor-general-information #llNum').blur();
		if($('.has-error')[0] == undefined) {
			var req = {};
			var res;
			var elem = $(this);
			req.action = "update-profile-general";
			
			req.firstName = $('#professor-general-information #first-name-inp').val();
			req.lastName = $('#professor-general-information #last-name-inp').val();
			
			req.phnPrefix = $('#professor-general-information #phnNumPrefix').val();
			req.phnNum = $('#professor-general-information #phnNum').val();
			req.llPrefix = $('#professor-general-information #llNumPrefix').val();
			req.llNum = $('#professor-general-information #llNum').val();
			
			req.gender = $('#professor-general-information #gender-inp').val();
			
			req.street = $('#professor-general-information #street-inp').val();
			req.city = $('#professor-general-information #city-inp').val();
			req.state = $('#professor-general-information #state-inp').val();
			req.pin = $('#professor-general-information #pin-inp').val();
			$(this).attr('disabled',true);
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				try {
					res =  $.parseJSON(res);
					if(res.status == 1) {
						fetchProfileDetails();
						alertMsgMsg(res.message);
						elem.parents('.panel').find('.edit-section').hide();
						elem.parents('.panel').find('.view-section').fadeIn('fast');
					}
					console.log(res);
				}catch(e) {
					alertMsgMsg(e);
				}
				finally {
					elem.attr('disabled',false);
					elem.parents('section.panel').find('.fa-times').removeClass('fa-times').addClass('fa-pencil');
				}
			});
		}
	});
	
	$('#publisher-general-information .save-button').click(function() {
		$('#publisher-general-information #first-name-inp, #publisher-general-information #last-name-inp, #publisher-general-information #phnNum, #publisher-general-information #llNum').blur();
		if($('.has-error')[0] == undefined) {
			var req = {};
			var res;
			var elem = $(this);
			req.action = "update-profile-general";
			
			req.firstName = $('#publisher-general-information #first-name-inp').val();
			req.lastName = $('#publisher-general-information #last-name-inp').val();
			
			req.foundedIn = $('#publisher-general-information #founded-in-inp').val();
			
			req.phnPrefix = $('#publisher-general-information #phnNumPrefix').val();
			req.phnNum = $('#publisher-general-information #phnNum').val();
			req.llPrefix = $('#publisher-general-information #llNumPrefix').val();
			req.llNum = $('#publisher-general-information #llNum').val();
			
			req.gender = $('#publisher-general-information #gender-inp').val();
			
			req.street = $('#publisher-general-information #street-inp').val();
			req.city = $('#publisher-general-information #city-inp').val();
			req.state = $('#publisher-general-information #state-inp').val();
			req.pin = $('#publisher-general-information #pin-inp').val();
			$(this).attr('disabled',true);
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				try {
					res =  $.parseJSON(res);
					if(res.status == 1) {
						fetchProfileDetails();
						alertMsgMsg(res.message);
						elem.parents('.panel').find('.edit-section').hide();
						elem.parents('.panel').find('.view-section').fadeIn('fast');
					}
					console.log(res);
					$(this).attr('disabled',false);
				}catch(e) {
					alertMsgMsg(e);
				}
				finally {
					elem.attr('disabled',false);
					elem.parents('section.panel').find('.fa-times').removeClass('fa-times').addClass('fa-pencil');
				}
			});
		}
	});
	
	$('#user-institute-types .save-button').click(function() {
		var elem = $(this);
		updateInstituteTypes(elem);
	});
	
	$('select.multi-select').multiSelect({
	  afterSelect: function(value) {
		//alertMsg("Select value: "+values);
		if(value == "-1") {
			console.log($(this)[0].$element.attr('id'));
			var id = $(this)[0].$element.attr('id');
			$('#' + id.replace('-subcat', '-other')).show();
			ins = id.replace('-subcat','').replace('ins-','');
			var other = $('#' + id.replace('-subcat', '-other')).find('input[type=text]').val();
			newSelectedInstitutes[ins] = {
				"other" : other
			};
		}else{
			newSelectedInstitutes[value] = {};
		}
			
	  },
	  afterDeselect: function(value) {
		var id = $(this)[0].$element.attr('id');
		$('#' + id.replace('-subcat', '-other')).hide();
		var ins = id.replace('-subcat','').replace('ins-','');
		if(value == "-1") {
			console.log($(this)[0].$element.attr('id'));
			newSelectedInstitutes[ins] =  {};
			$('#ins-'+ins+'-other input[type=text]').val('');
		}else{
			delete newSelectedInstitutes[value];
		}
	  }
	});
	
	$('#ins-1, #ins-2, #ins-3, #ins-4, #ins-8, #ins-9, #ins-10, #ins-11, #ins-12').on('change', function(){
		if($(this).prop('checked') == true) {
			var ins = $(this).attr('id').replace('ins-', '');
			newSelectedInstitutes[ins] = {
				"data" : {}
			};
		}else {
			var ins = $(this).attr('id').replace('ins-', '');
			delete newSelectedInstitutes[ins];
		}
		
	});

	$('#school-board').on('change', function() {
		board = $('#school-board').val();
		if(board == 7){
			$('#school-board-other').hide();
			$('#school-board-state').show();
		} else if(board == -1){
			$('#school-board-state').hide();
			$('#school-board-other').show();
		} else {
			$('#school-board-state').hide();
			$('#school-board-other').hide();
		}
	});
});

function readURL1(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		console.log(width, height);
		if(!(input.files[0]['type'] == 'image/jpeg' || input.files[0]['type'] == 'image/png' || input.files[0]['type'] == 'image/gif')) {
			$('#cover-image-form input[type=submit]').prop('disabled',true);
			$('#cover-image-form .error').remove();
			$('#cover-image-form').append('<span style="color:red;" class="error">Please select a proper file type.</span>');
			return;
		}
		else if(input.files[0]['size']>819200) {
			$('#cover-image-form input[type=submit]').prop('disabled',true);
			$('#cover-image-form .error').remove();
			$('#cover-image-form').append('<span style="color:red;" class="error">Files larger than 800kb are not allowed.</span>');
			return;
		}
		else {
			$('#cover-image-form .error').remove();
			$('#cover-image-form input[type=submit]').prop('disabled',false);
		}
		if (typeof jcrop_api != 'undefined' && jcrop_api != null) {
			jcrop_api.destroy();
			jcrop_api = null;
			var pImage = $('.crop');
			pImage.css('height', 'auto');
			pImage.css('width', 'auto');
			var height = pImage.height();
			var width = pImage.width();
			
			$('.jcrop').width(width);
			$('.jcrop').height(height);
		}
		reader.onload = function (e) {
			//$('#cover-image-form #uploaded-image1').remove();
			//$('<img id="cover-image-form" class="crop" src="" />').insertAfter($('#cover-image-form #cover-image-uploader'));
			$('#cover-image-form #uploaded-image1').attr('src', e.target.result).show();
			$('#cover-image-form .crop').Jcrop({
				onSelect: updateCoords1,
				bgOpacity:   .4,
				boxWidth: 830,
				minSize: [280, 200],
				setSelect:   [0, 0, 280, 200],
				aspectRatio: 1109/450
			}, function() {
				jcrop_api = this;
				jcrop_api.setSelect([0, 0, 280, 200]);
			});
		}
		reader.readAsDataURL(input.files[0]);
	}
}

function updateCoords1(c) {
	$('#x1').val(c.x);
	$('#y1').val(c.y);
	$('#w1').val(c.w);
	$('#h1').val(c.h);
	var crop = [c.x, c.y, c.w, c.h];
}

function readURL2(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		if(!(input.files[0]['type'] == 'image/jpeg' || input.files[0]['type'] == 'image/png'|| input.files[0]['type'] == 'image/gif')) {
			$('#profile-image-form input[type=submit]').prop('disabled',true);
			$('#profile-image-form .error').remove();
			$('#profile-image-form').append('<span style="color:red;" class="error">Please select a proper file type.</span>');
			return;
		}
		else if(input.files[0]['size']>819200) {
			$('#profile-image-form input[type=submit]').prop('disabled',true);
			$('#profile-image-form .error').remove();
			$('#profile-image-form').append('<span style="color:red;" class="error">Files larger than 800kb are not allowed.</span>');
			return;
		}
		else {
			$('#profile-image-form .error').remove();
			$('#profile-image-form input[type=submit]').prop('disabled',false);
		}
		if (typeof jcrop_api != 'undefined' && jcrop_api != null) {
			jcrop_api.destroy();
			jcrop_api = null;
			var pImage = $('.crop');
			pImage.css('height', 'auto');
			pImage.css('width', 'auto');
			var height = pImage.height();
			var width = pImage.width();
			
			$('.jcrop').width(width);
			$('.jcrop').height(height);
		}
		reader.onload = function (e) {
			$('#profile-image-form #uploaded-image2').attr('src', e.target.result).show();
			$('#profile-image-form .crop').Jcrop({
				onSelect: updateCoords2,
				bgOpacity:   .4,
				boxWidth: 830,
				minSize: [200, 200],
				setSelect:   [ 0, 0, 200, 200],
				aspectRatio: 1
			}, function() {
				jcrop_api = this;
				jcrop_api.setSelect([0, 0, 200, 200]);
			});
		}
		reader.readAsDataURL(input.files[0]);
	}
}

function updateCoords2(c) {
	$('#x2').val(c.x);
	$('#y2').val(c.y);
	$('#w2').val(c.w);
	$('#h2').val(c.h);
	var crop = [c.x, c.y, c.w, c.h];
}

function alertMsgMsg(m) {
	alertMsg(m);
}

function getSelectedSubcat(catId) {
	var catHierarchy = {
		"1": [5,6,7],
		"2": [8,9,10,11,12],
		"3": [46,47,48,49,50,51,52,53],
		"4": [54,55,56,57,58,59,60,61,62,63],
		"8": [13,14,15,16,17],
		"9": [18,19,21,22],
		"10": [23,24,25,26,27],
		"11": [28,29,30,31,32,33,34],
		"12": [35,36,37,38,39,40,41,42,43,44,45]
	};
	var subCats = catHierarchy[catId];
	var selectedSubcat = {};
	$.each(subCats, function(i,v) {
		if(typeof newSelectedInstitutes[v] !== "undefined") {
			selectedSubcat[v] = newSelectedInstitutes[v];
		}
	});
	return selectedSubcat;
}

function countDictKeys(c){
	var count = 0;
	for (var i in c) {
	   if (c.hasOwnProperty(i)) count++;
	}
	return count;
}

/*****************************************************
	functions for validating data before saving
	Author	:	Rupesh
	Date	:	07/08/2014
*****************************************************/
$(document).ready(function() {
	$('#management, #engineering, #medical, #schoolLevel, #misc, #school, #coaching, #college, #other').hide();
	$('#ins-1').on('change', function() {
		$('#school').slideToggle();
		if(!$(this).prop('checked'))
			schoolReset();
	});
	$('#ins-2').on('change', function() {
		$("#management, #engineering, #medical, #schoolLevel, #misc").hide();
		$('#coaching').slideToggle();
		if(!$(this).prop('checked')) {
			coachingReset();
			delete newSelectedInstitutes[2];
		}
	});
	$('#ins-3').on('change', function() {
		$('#college').slideToggle();
		if(!$(this).prop('checked')) {
			releaseMultiselectOptions($(this));
			delete newSelectedInstitutes[3];
		}
	});
	$('#ins-4').on('change', function() {
		$('#other').slideToggle();
		if(!$(this).prop('checked')) {
			releaseMultiselectOptions($(this));
			delete newSelectedInstitutes[4];
		}
	});
	$('#ins-8').on('change', function() {
		$('#management').slideToggle();
		if(!$(this).prop('checked')) {
			$('#management').hide();
			releaseMultiselectOptions($(this));
			delete newSelectedInstitutes[8];
		}
	});
	$('#ins-9').on('change', function() {
		$('#engineering').slideToggle();
		if(!$(this).prop('checked')) {
			releaseMultiselectOptions($(this));
			delete newSelectedInstitutes[9];
		}
	});
	$('#ins-10').on('change', function() {
		$('#medical').slideToggle();
		if(!$(this).prop('checked')) {
			releaseMultiselectOptions($(this));
			delete newSelectedInstitutes[10];
		}
	});
	$('#ins-11').on('change', function() {
		$('#schoolLevel').slideToggle();
		if(!$(this).prop('checked')) {
			releaseMultiselectOptions($(this));
			delete newSelectedInstitutes[11];
		}
	});
	$('#ins-12').on('change', function() {
		$('#misc').slideToggle();
		if(!$(this).prop('checked')) {
			releaseMultiselectOptions($(this));
			delete newSelectedInstitutes[12];
		}
	});
	$('#institute-name-inp').blur(function() {
		if(!minCheck($(this),5))
			setError($(this),"Please specify a institute name greater than 5 characters.");
		else
			unsetError($(this));
	});
	$('#contact-first-name-inp, #professor-general-information #first-name-inp, #publisher-general-information #first-name-inp').blur(function() {
		if(!minCheck($(this),3))
			setError($(this),"Please specify a first name greater than 3 characters.");
		else
			unsetError($(this));
	});
	$('#contact-last-name-inp, #professor-general-information #last-name-inp, #publisher-general-information #last-name-inp').blur(function() {
		if(!minCheck($(this),3))
			setError($(this),"Please specify a last name greater than 3 characters.");
		else
			unsetError($(this));
	});
	$('#institute-general-information #phnNum, #institute-general-information #llNum,#publisher-general-information #phnNum, #publisher-general-information #llNum, #professor-general-information #phnNum, #professor-general-information #llNum').blur(function() {
		unsetErrorContact($(this));
		elem = $(this);
		thisId = elem.attr('data-text');
		console.log(thisId);
		if($("#"+thisId+"-general-information #phnNumPrefix").val().length>0 || $("#"+thisId+"-general-information #phnNum").val().length>0) {
			if(!validateMobile(thisId))
				setErrorContact(elem,"Please specify correct mobile number.");
		}
		if($("#"+thisId+"-general-information #llNumPrefix").val().length>0 || $("#"+thisId+"-general-information #llNum").val().length>0) {
			if(!validateLandline(thisId))
				setErrorContact(elem,"Please specify correct landline number.");
		}
		if($("#"+thisId+"-general-information #phnNumPrefix").val() == '' && $("#"+thisId+"-general-information #phnNum").val() == '' && $("#"+thisId+"-general-information #llNumPrefix").val() == '' && $("#"+thisId+"-general-information #llNum").val() == '')
			setErrorContact(elem,"Please specify at least one contact number.");
	});
	//fields where only numbers are allowed
	$("#phnNum, #llNumPrefix, #llNum, #pin-inp").keypress(function (e) {
		//if the letter is not digit then don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	});
});
function schoolReset() {
	$('#school-board').val(0);
	$('#school-classes').val(0);
	$('#school-board-state input[type=text]').val('');
	$('#school-board-other input[type=text]').val('');
}
function coachingReset() {
	$('#ins-8').prop('checked',false);
	delete newSelectedInstitutes[8];
	$('#ins-9').prop('checked',false);
	delete newSelectedInstitutes[9];
	$('#ins-10').prop('checked',false);
	delete newSelectedInstitutes[10];
	$('#ins-11').prop('checked',false);
	delete newSelectedInstitutes[11];
	$('#ins-12').prop('checked',false);
	delete newSelectedInstitutes[12];
}
function releaseMultiselectOptions(element) {
	var catHierarchy = {
		"1": [5,6,7],
		"2": [8,9,10,11,12],
		"3": [46,47,48,49,50,51,52,53],
		"4": [54,55,56,57,58,59,60,61,62,63],
		"8": [13,14,15,16,17],
		"9": [18,19,21,22],
		"10": [23,24,25,26,27],
		"11": [28,29,30,31,32,33,34],
		"12": [35,36,37,38,39,40,41,42,43,44,45]
	};
	var id = element.attr('id');
	$('#'+id+'-subcat').multiSelect('deselect','-1');
	for(i=0;i<catHierarchy[id[id.length-1]].length;i++) {
		$('#'+id+'-subcat').multiSelect('deselect', catHierarchy[id[id.length-1]][i]+'');
	}
	$('#'+id+'-other input[type=text]').val('');
}
function unsetErrorContact(where) {
	where.parents('.contact').removeClass('has-error');
	$('.contactError').hide();
}
function setErrorContact(where, what) {
	where.parents('.contact').addClass('has-error');
	$('.contactError').text(what);
	$('.contactError').show();
}
function minCheck(element,len) {
	if(element.val().length>=len)
		return true;
	else
		return false;
}
function setError(where,what) {
	unsetError(where);
	parent=where.parent();
	parent.addClass('has-error');
	parent.append('<span style="color:red;">'+what+'</span>');
}
function unsetError(where) {
	where.parent().removeClass('has-error');
	where.next().remove();
}
function validateMobile(str) {
	preMobile=Mobile=false;
	pre=$("#"+str+"-general-information #phnNumPrefix").val();
	preN=pre.substring(1);
	regex=/^(\d{1,5})$/;
	if(regex.test(preN) && pre[0]=='+')
		preMobile=true;
	mobile=$("#"+str+"-general-information #phnNum").val();
	regex=/^(\d{10})$/;
	if(regex.test(mobile))
		Mobile=true;
	if(preMobile && Mobile)
		return true;
	if(pre == "+91" && mobile == '' && validateLandline(str))
		return true;
	return false;
}
function validateLandline(str) {
	preLandline=Landline=false;
	pre=$("#"+str+"-general-information #llNumPrefix").val();
	preN=pre.substring(1);
	regex=/^(\d{1,5})$/;
	if(regex.test(preN) && pre[0]=='0')
		preLandline=true;
	landline=$("#"+str+"-general-information #llNum").val();
	regex=/^(\d{6,8})$/;
	if(regex.test(landline))
		Landline=true;
	if(preLandline && Landline)
		return true;
	return false;
}

function formatContactNumber(data) {
	var mobile = (data.contactMobile != '')?(data.contactMobilePrefix + '-' + data.contactMobile):'';
	var landline = (data.contactLandline != '')?(data.contactLandlinePrefix + '-' + data.contactLandline):'';
	if(mobile != '' && landline != '') {
		return mobile + ', ' + landline;
	}
	else if(mobile == '') {
		return landline;
	}
	else
		return mobile;
}