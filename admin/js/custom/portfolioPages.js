var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var userRole;
$(document).ajaxStart(function () {
    $('#loader_outer').show();
});

$(document).ajaxComplete(function () {
    $('#loader_outer').hide();
});

$(function () {
    var imageRoot = '/';
    var jcrop_api;
    var aboutAcademic = [];
    var aa = 1;
    var aboutEducation = [];
    var ae = 1;
    var aboutHonors = [];
    var ah = 1;
    var facultyMembers = [];
    var fm = 1;
    var galleryImages = [];
    var gi = 1;
    var contactInfo = [];
    var ci = 1;
    var publications = [];
    var pb = 1;
    var teaching = [];
    var tc = 1;
    var portfolioPages = [];
    $('#loader_outer').hide();
    var fileApiEndpoint = '../api/files1.php';

    // file upload Our Honors
    $('#fileOHImage').fileupload({
        url: fileApiEndpoint,
        dataType: 'json',
        done: function (e, data) {
            $('#imgOHImage').attr('src', data._response.result.imageName);
            $('#imgOHImage').removeClass('hide');
            $('#inputOHImage').val(data._response.result.imageName);
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    // file upload faculty
    $('#fileFMImage').fileupload({
        url: fileApiEndpoint,
        dataType: 'json',
        done: function (e, data) {
            $('#imgFMImage').attr('src', data._response.result.imageName);
            $('#imgFMImage').removeClass('hide');
            $('#inputFMImage').val(data._response.result.imageName);
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
    
    // file upload gallery
    $('#fileGIImage').fileupload({
        url: fileApiEndpoint,
        dataType: 'json',
        done: function (e, data) {
            $('#imgGIImage').attr('src', data._response.result.imageName);
            $('#imgGIImage').removeClass('hide');
            $('#inputGIImage').val(data._response.result.imageName);
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    $('.dpYears').datetimepicker({
        format: 'yyyy',
        startView: 4,
        minView: 4,
        maxView: 4
    });
    $('#inputPageContent').ckeditor();
    fetchPortfolioCourses();
    fetchPageTypes();
    $('.btn-modal').click(function(e){
        e.preventDefault();
        var modal = $(this).attr('data-target');
        $(modal).find('input').val('');
        $(modal).find('img').attr('src', '').addClass('hide');
        $(modal).find('.js-btn-submit').attr('data-update','');
        $(modal).modal('show');
    });
    $('#frmPortfolioPages').on('click','.js-btn-change', function(){
        var dataid = $(this).closest('tr').attr('data-id');
        var parent = $(this).closest('.table').parent();
        var objSearch = '';
        if(parent.hasClass('js-academic')) {
            objSearch = aboutAcademic;
        } else if(parent.hasClass('js-education')) {
            objSearch = aboutEducation;
        } else if(parent.hasClass('js-honors')) {
            objSearch = aboutHonors;
        } else if(parent.hasClass('js-faculty')) {
            objSearch = facultyMembers;
        } else if(parent.hasClass('js-publication')) {
            objSearch = publications;
        } else if(parent.hasClass('js-teaching')) {
            objSearch = teaching;
        } else if(parent.hasClass('js-gallery')) {
            objSearch = galleryImages;
        } else if(parent.hasClass('js-contact')) {
            objSearch = contactInfo;
        }
        var op = findObjByValue(objSearch,'id',dataid);
        if(parent.hasClass('js-academic')) {
            $("#inputAPFromYear").val(op.fromyear);
            $("#inputAPToYear").val(op.toyear);
            $("#inputAPPosition").val(op.position);
            $("#inputAPUniversity").val(op.university);
            $("#inputAPDepartment").val(op.department);
            $('#academicPositionsModal').find('.js-btn-submit').attr('data-update',dataid);
            $('#academicPositionsModal').modal('show');
        } else if(parent.hasClass('js-education')) {
            $("#inputETDegree").val(op.degree);
            $("#inputETYear").val(op.year);
            $("#inputETTitle").val(op.title);
            $("#inputETUniversity").val(op.university);
            $('#educationModal').find('.js-btn-submit').attr('data-update',dataid);
            $('#educationModal').modal('show');
        } else if(parent.hasClass('js-honors')) {
            $("#imgOHImage").attr('src', op.image).removeClass('hide');
            $("#inputOHImage").val(op.image);
            $("#inputOHYear").val(op.year);
            $("#inputOHTitle").val(op.title);
            $("#inputOHDescription").val(op.description);
            $('#honorsModal').find('.js-btn-submit').attr('data-update',dataid);
            $('#honorsModal').modal('show');
        } else if(parent.hasClass('js-faculty')) {
            $("#imgFMImage").attr('src', op.image).removeClass('hide');
            $("#inputFMImage").val(op.image);
            $("#inputFMName").val(op.name);
            $("#inputFMUniversity").val(op.university);
            $("#inputFMDepartment").val(op.department);
            $('#facultyMembersModal').find('.js-btn-submit').attr('data-update',dataid);
            $('#facultyMembersModal').modal('show');
        } else if(parent.hasClass('js-publication')) {
            $("#selectPBType").val(op.type);
            $("#inputPBName").val(op.name);
            $("#inputPBDescription").val(op.description);
            $("#inputPBYear").val(op.year);
            $("#inputPBISBN").val(op.isbn);
            $('#publicationsModal').find('.js-btn-submit').attr('data-update',dataid);
            $('#publicationsModal').modal('show');
        } else if(parent.hasClass('js-teaching')) {
            $("#inputTCTitle").val(op.title);
            $("#inputTCDescription").val(op.description);
            $("#inputTCFrom").val(op.from);
            $("#inputTCTo").val(op.to);
            $('#teachingModal').find('.js-btn-submit').attr('data-update',dataid);
            $('#teachingModal').modal('show');
        } else if(parent.hasClass('js-gallery')) {
            $("#imgGIImage").attr('src', op.image).removeClass('hide');
            $("#inputGIImage").val(op.image);
            $("#inputGITitle").val(op.title);
            $("#inputGIDescription").val(op.description);
            $('#galleryModal').find('.js-btn-submit').attr('data-update',dataid);
            $('#galleryModal').modal('show');
        } else if(parent.hasClass('js-contact')) {
            $("#selectCIType").val(op.type);
            $("#inputCITitle").val(op.title);
            $("#inputCIValue").val(op.value);
            $('#contactModal').find('.js-btn-submit').attr('data-update',dataid);
            $('#contactModal').modal('show');
        }
        return false;
    });
    $('#frmPortfolioPages').on('click','.js-btn-remove', function(){
        var dataid = $(this).closest('tr').attr('data-id');
        var parent = $(this).closest('.table').parent();
        var objSearch = '';
        if(parent.hasClass('js-academic')) {
            aboutAcademic = findRemoveObjByValue(aboutAcademic).remove('id', dataid);
            if(aboutAcademic.length == 0){
                $('.js-academic .table').remove();
            } else {
                $('.js-academic .table').find('tr[data-id="'+dataid+'"]').remove();
            }
        } else if(parent.hasClass('js-education')) {
            aboutEducation = findRemoveObjByValue(aboutEducation).remove('id', dataid);
            if(aboutEducation.length == 0){
                $('.js-education .table').remove();
            } else {
                $('.js-education .table').find('tr[data-id="'+dataid+'"]').remove();
            }
            delete aboutEducation[key];
        } else if(parent.hasClass('js-honors')) {
            aboutHonors = findRemoveObjByValue(aboutHonors).remove('id', dataid);
            if(aboutHonors.length == 0){
                $('.js-honors .table').remove();
            } else {
                $('.js-honors .table').find('tr[data-id="'+dataid+'"]').remove();
            }
            delete aboutHonors[key];
        } else if(parent.hasClass('js-faculty')) {
            facultyMembers = findRemoveObjByValue(facultyMembers).remove('id', dataid);
            if(facultyMembers.length == 0){
                $('.js-faculty .table').remove();
            } else {
                $('.js-faculty .table').find('tr[data-id="'+dataid+'"]').remove();
            }
            delete facultyMembers[key];
        } else if(parent.hasClass('js-publication')) {
            publications = findRemoveObjByValue(publications).remove('id', dataid);
            if(publications.length == 0){
                $('.js-publication .table').remove();
            } else {
                $('.js-publication .table').find('tr[data-id="'+dataid+'"]').remove();
            }
            delete publications[key];
        } else if(parent.hasClass('js-teaching')) {
            teaching = findRemoveObjByValue(teaching).remove('id', dataid);
            if(teaching.length == 0){
                $('.js-teaching .table').remove();
            } else {
                $('.js-teaching .table').find('tr[data-id="'+dataid+'"]').remove();
            }
            delete teaching[key];
        } else if(parent.hasClass('js-gallery')) {
            galleryImages = findRemoveObjByValue(galleryImages).remove('id', dataid);
            if(galleryImages.length == 0){
                $('.js-gallery .table').remove();
            } else {
                $('.js-gallery .table').find('tr[data-id="'+dataid+'"]').remove();
            }
            delete galleryImages[key];
        } else if(parent.hasClass('js-contact')) {
            contactInfo = findRemoveObjByValue(contactInfo).remove('id', dataid);
            if(contactInfo.length == 0){
                $('.js-contact .table').remove();
            } else {
                $('.js-contact .table').find('tr[data-id="'+dataid+'"]').remove();
            }
            delete contactInfo[key];
        }
        return false;
    });
    $('#btnAcademicPosition').click(function(){
        var dataid            = trim($(this).attr('data-update'));
        var inputAPFromYear   = trim($("#inputAPFromYear").val());
        var inputAPToYear     = trim($("#inputAPToYear").val());
        var inputAPPosition   = trim($("#inputAPPosition").val());
        var inputAPUniversity = trim($("#inputAPUniversity").val());
        var inputAPDepartment = trim($("#inputAPDepartment").val());
        var helpBlock         = $(this).closest('form').find('.help-block');
        helpBlock.html('');
        if (!inputAPFromYear) {
            helpBlock.html('From Year field is compulsory');
        } else if (!inputAPPosition) {
            helpBlock.html('Position field is compulsory');
        } else if (!inputAPUniversity) {
            helpBlock.html('University field  is compulsory');
        } else if (!inputAPDepartment) {
            helpBlock.html('Department field is compulsory');
        } else {
            if(!dataid) {
                if($('.js-academic').html().length == 0) {
                    $('.js-academic').html('<table class="table table-bordered">'+
                                                '<tr>'+
                                                    '<th>From Year</th>'+
                                                    '<th>To Year</th>'+
                                                    '<th>Position</th>'+
                                                    '<th>University</th>'+
                                                    '<th>Department</th>'+
                                                    '<th></th>'+
                                                    '<th></th>'+
                                                '</tr>'+
                                            '</table>');
                }
                $('.js-academic .table tr:last-child').after('<tr data-id="n'+aa+'">'+
                                                    '<td>'+inputAPFromYear+'</td>'+
                                                    '<td>'+inputAPToYear+'</td>'+
                                                    '<td>'+inputAPPosition+'</td>'+
                                                    '<td>'+inputAPUniversity+'</td>'+
                                                    '<td>'+inputAPDepartment+'</td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                                '</tr>');
                aboutAcademic.push(
                    {id: 'n'+aa,fromyear:inputAPFromYear,toyear:inputAPToYear,position:inputAPPosition,university:inputAPUniversity,department:inputAPDepartment,status:'new'}
                );
                aa++;
            } else {
                $('.js-academic .table').find('tr[data-id="'+dataid+'"]').html(
                                                    '<td>'+inputAPFromYear+'</td>'+
                                                    '<td>'+inputAPToYear+'</td>'+
                                                    '<td>'+inputAPPosition+'</td>'+
                                                    '<td>'+inputAPUniversity+'</td>'+
                                                    '<td>'+inputAPDepartment+'</td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>');
                var key = findKeyByValue(aboutAcademic,dataid);
                aboutAcademic[key].fromyear   = inputAPFromYear;
                aboutAcademic[key].toyear     = inputAPToYear;
                aboutAcademic[key].position   = inputAPPosition;
                aboutAcademic[key].university = inputAPUniversity;
                aboutAcademic[key].department = inputAPDepartment;
                $(this).attr('data-update', '');
            }
            $('#academicPositionsModal form')[0].reset();
            $('#academicPositionsModal').modal('hide');
        }
    });
    $('#btnEducation').click(function(){
        var dataid            = trim($(this).attr('data-update'));
        var inputETDegree     = trim($("#inputETDegree").val());
        var inputETYear       = trim($("#inputETYear").val());
        var inputETTitle      = trim($("#inputETTitle").val());
        var inputETUniversity = trim($("#inputETUniversity").val());
        var helpBlock         = $(this).closest('form').find('.help-block');
        helpBlock.html('');
        if (!inputETDegree) {
            helpBlock.html('Degree field is compulsory');
        } else if (!inputETYear) {
            helpBlock.html('Year field is compulsory');
        } else if (!inputETTitle) {
            helpBlock.html('Title field  is compulsory');
        } else if (!inputETUniversity) {
            helpBlock.html('University field is compulsory');
        } else {
            if(!dataid) {
                if($('.js-education').html().length == 0) {
                    $('.js-education').html('<table class="table table-bordered">'+
                                                '<tr>'+
                                                    '<th>Degree</th>'+
                                                    '<th>Year</th>'+
                                                    '<th>Title</th>'+
                                                    '<th>University</th>'+
                                                    '<th></th>'+
                                                    '<th></th>'+
                                                '</tr>'+
                                            '</table>');
                }
                $('.js-education .table tr:last-child').after('<tr data-id="n'+ae+'">'+
                                                    '<td>'+inputETDegree+'</td>'+
                                                    '<td>'+inputETYear+'</td>'+
                                                    '<td>'+inputETTitle+'</td>'+
                                                    '<td>'+inputETUniversity+'</td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                                '</tr>');
                aboutEducation.push(
                    {id: 'n'+ae,degree:inputETDegree,year:inputETYear,title:inputETTitle,university:inputETUniversity,status:'new'}
                );
                ae++;
            } else {
                $('.js-education .table').find('tr[data-id="'+dataid+'"]').html(
                                                    '<td>'+inputETDegree+'</td>'+
                                                    '<td>'+inputETYear+'</td>'+
                                                    '<td>'+inputETTitle+'</td>'+
                                                    '<td>'+inputETUniversity+'</td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>');
                var key = findKeyByValue(aboutEducation,dataid);
                aboutEducation[key].degree     = inputETDegree;
                aboutEducation[key].year       = inputETYear;
                aboutEducation[key].title      = inputETTitle;
                aboutEducation[key].university = inputETUniversity;
                $(this).attr('data-update', '');
            }
            $('#educationModal form')[0].reset();
            $('#educationModal').modal('hide');
        }
    });
    $('#btnHonors').click(function(){
        var dataid             = trim($(this).attr('data-update'));
        var inputOHImage       = trim($("#inputOHImage").val());
        var inputOHYear        = trim($("#inputOHYear").val());
        var inputOHTitle       = trim($("#inputOHTitle").val());
        var inputOHDescription = trim($("#inputOHDescription").val());
        var helpBlock          = $(this).closest('form').find('.help-block');
        helpBlock.html('');
        if (!inputOHImage) {
            helpBlock.html('Image field is compulsory');
        } else if (!inputOHYear) {
            helpBlock.html('Year field is compulsory');
        } else if (!inputOHTitle) {
            helpBlock.html('Title field is compulsory');
        } else if (!inputOHDescription) {
            helpBlock.html('Description field  is compulsory');
        } else {
            if(!dataid) {
                if($('.js-honors').html().length == 0) {
                    $('.js-honors').html('<table class="table table-bordered">'+
                                                '<tr>'+
                                                    '<th>Image</th>'+
                                                    '<th>Year</th>'+
                                                    '<th>Title</th>'+
                                                    '<th>Description</th>'+
                                                    '<th></th>'+
                                                    '<th></th>'+
                                                '</tr>'+
                                            '</table>');
                }
                $('.js-honors .table tr:last-child').after('<tr data-id="n'+ah+'">'+
                                                    '<td><img src="'+inputOHImage+'" height="100px" alt="'+inputOHTitle+'" /></td>'+
                                                    '<td>'+inputOHYear+'</td>'+
                                                    '<td>'+inputOHTitle+'</td>'+
                                                    '<td>'+inputOHDescription+'</td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                                '</tr>');
                aboutHonors.push(
                    {id: 'n'+ah,image:inputOHImage,year:inputOHYear,title:inputOHTitle,description:inputOHDescription,status:'new'}
                );
                ah++;
            } else {
                $('.js-honors .table').find('tr[data-id="'+dataid+'"]').html(
                                                    '<td><img src="'+inputOHImage+'" height="100px" alt="'+inputOHTitle+'" /></td>'+
                                                    '<td>'+inputOHYear+'</td>'+
                                                    '<td>'+inputOHTitle+'</td>'+
                                                    '<td>'+inputOHDescription+'</td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>');
                var key = findKeyByValue(aboutHonors,dataid);
                aboutHonors[key].image       = inputOHImage;
                aboutHonors[key].year        = inputOHYear;
                aboutHonors[key].title       = inputOHTitle;
                aboutHonors[key].description = inputOHDescription;
                $(this).attr('data-update', '');
            }
            $('#honorsModal form')[0].reset();
            $('#honorsModal').modal('hide');
        }
    });
    $('#btnFacultyMember').click(function(){
        var dataid            = trim($(this).attr('data-update'));
        var inputFMImage      = trim($("#inputFMImage").val());
        var inputFMName       = trim($("#inputFMName").val());
        var inputFMUniversity = trim($("#inputFMUniversity").val());
        var inputFMDepartment = trim($("#inputFMDepartment").val());
        var helpBlock         = $(this).closest('form').find('.help-block');
        helpBlock.html('');
        if (!inputFMImage) {
            helpBlock.html('Image field is compulsory');
        } else if (!inputFMName) {
            helpBlock.html('Name field is compulsory');
        } else if (!inputFMUniversity) {
            helpBlock.html('University field is compulsory');
        } else if (!inputFMDepartment) {
            helpBlock.html('Department field  is compulsory');
        } else {
            if(!dataid) {
                if($('.js-faculty').html().length == 0) {
                    $('.js-faculty').html('<table class="table table-bordered">'+
                                                '<tr>'+
                                                    '<th>Image</th>'+
                                                    '<th>Name</th>'+
                                                    '<th>University</th>'+
                                                    '<th>Department</th>'+
                                                    '<th></th>'+
                                                    '<th></th>'+
                                                '</tr>'+
                                            '</table>');
                }
                $('.js-faculty .table tr:last-child').after('<tr data-id="n'+fm+'">'+
                                                    '<td><img src="'+inputFMImage+'" height="100px" alt="'+inputFMName+'" /></td>'+
                                                    '<td>'+inputFMName+'</td>'+
                                                    '<td>'+inputFMUniversity+'</td>'+
                                                    '<td>'+inputFMDepartment+'</td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                                '</tr>');
                facultyMembers.push(
                    {id: 'n'+fm,image:inputFMImage,name:inputFMName,university:inputFMUniversity,department:inputFMDepartment,status:'new'}
                );
                fm++;
            } else {
                $('.js-faculty .table').find('tr[data-id="'+dataid+'"]').html(
                                                    '<td><img src="'+inputFMImage+'" height="100px" alt="'+inputFMName+'" /></td>'+
                                                    '<td>'+inputFMName+'</td>'+
                                                    '<td>'+inputFMUniversity+'</td>'+
                                                    '<td>'+inputFMDepartment+'</td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>');
                var key = findKeyByValue(facultyMembers,dataid);
                facultyMembers[key].image      = inputFMImage;
                facultyMembers[key].name       = inputFMName;
                facultyMembers[key].university = inputFMUniversity;
                facultyMembers[key].department = inputFMDepartment;
                $(this).attr('data-update', '');
            }
            $('#facultyMembersModal form')[0].reset();
            $('#facultyMembersModal').modal('hide');
        }
    });
    $('#btnPublication').click(function(){
        var dataid             = trim($(this).attr('data-update'));
        var selectPBType       = trim($("#selectPBType").val());
        var textPBType         = trim($("#selectPBType option:selected").text());
        var inputPBName        = trim($("#inputPBName").val());
        var inputPBDescription = trim($("#inputPBDescription").val());
        var inputPBYear        = trim($("#inputPBYear").val());
        var inputPBISBN        = trim($("#inputPBISBN").val());
        var helpBlock          = $(this).closest('form').find('.help-block');
        helpBlock.html('');
        if (!selectPBType) {
            helpBlock.html('Publication Type field is compulsory');
        } else if (!inputPBName) {
            helpBlock.html('Name field is compulsory');
        } else if (!inputPBDescription) {
            helpBlock.html('Description field is compulsory');
        } else if (!inputPBYear) {
            helpBlock.html('Year field  is compulsory');
        } else {
            if(!dataid) {
                if($('.js-publication').html().length == 0) {
                    $('.js-publication').html('<table class="table table-bordered">'+
                                                '<tr>'+
                                                    '<th>Publication Type</th>'+
                                                    '<th>Name</th>'+
                                                    '<th>Description</th>'+
                                                    '<th>Year</th>'+
                                                    '<th>ISBN</th>'+
                                                    '<th></th>'+
                                                    '<th></th>'+
                                                '</tr>'+
                                            '</table>');
                }
                $('.js-publication .table tr:last-child').after('<tr data-id="n'+pb+'">'+
                                                    '<td>'+textPBType+'</td>'+
                                                    '<td>'+inputPBName+'</td>'+
                                                    '<td>'+inputPBDescription+'</td>'+
                                                    '<td>'+inputPBYear+'</td>'+
                                                    '<td>'+inputPBISBN+'</td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                                '</tr>');
                publications.push(
                    {id: 'n'+pb,type:selectPBType,name:inputPBName,description:inputPBDescription,year:inputPBYear,isbn:inputPBISBN,status:'new'}
                );
                pb++;
            } else {
                $('.js-publication .table').find('tr[data-id="'+dataid+'"]').html(
                                                    '<td>'+textPBType+'</td>'+
                                                    '<td>'+inputPBName+'</td>'+
                                                    '<td>'+inputPBDescription+'</td>'+
                                                    '<td>'+inputPBYear+'</td>'+
                                                    '<td>'+inputPBISBN+'</td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>');
                var key = findKeyByValue(publications,dataid);
                publications[key].type        = selectPBType;
                publications[key].name        = inputPBName;
                publications[key].description = inputPBDescription;
                publications[key].year        = inputPBYear;
                publications[key].isbn        = inputPBISBN;
                $(this).attr('data-update', '');
            }
            $('#publicationsModal form')[0].reset();
            $('#publicationsModal').modal('hide');
        }
    });
    $('#btnTeaching').click(function(){
        var dataid             = trim($(this).attr('data-update'));
        var inputTCTitle       = trim($("#inputTCTitle").val());
        var inputTCDescription = trim($("#inputTCDescription").val());
        var inputTCFrom        = trim($("#inputTCFrom").val());
        var inputTCTo          = trim($("#inputTCTo").val());
        var helpBlock          = $(this).closest('form').find('.help-block');
        helpBlock.html('');
        if (!inputTCTitle) {
            helpBlock.html('Title field is compulsory');
        } else if (!inputTCDescription) {
            helpBlock.html('Description field is compulsory');
        } else if (!inputTCFrom) {
            helpBlock.html('From field is compulsory');
        } else {
            if(!dataid) {
                if($('.js-teaching').html().length == 0) {
                    $('.js-teaching').html('<table class="table table-bordered">'+
                                                '<tr>'+
                                                    '<th>Title</th>'+
                                                    '<th>Description</th>'+
                                                    '<th>From</th>'+
                                                    '<th>To</th>'+
                                                    '<th></th>'+
                                                    '<th></th>'+
                                                '</tr>'+
                                            '</table>');
                }
                $('.js-teaching .table tr:last-child').after('<tr data-id="n'+tc+'">'+
                                                    '<td>'+inputTCTitle+'</td>'+
                                                    '<td>'+inputTCDescription+'</td>'+
                                                    '<td>'+inputTCFrom+'</td>'+
                                                    '<td>'+inputTCTo+'</td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                                '</tr>');
                teaching.push(
                    {id: 'n'+tc,title:inputTCTitle,description:inputTCDescription,from:inputTCFrom,to:inputTCTo,status:'new'}
                );
                tc++;
            } else {
                $('.js-teaching .table').find('tr[data-id="'+dataid+'"]').html(
                                                    '<td>'+inputTCTitle+'</td>'+
                                                    '<td>'+inputTCDescription+'</td>'+
                                                    '<td>'+inputTCFrom+'</td>'+
                                                    '<td>'+inputTCTo+'</td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>');
                var key = findKeyByValue(teaching,dataid);
                teaching[key].title       = inputTCTitle;
                teaching[key].description = inputTCDescription;
                teaching[key].from        = inputTCFrom;
                teaching[key].to          = inputTCTo;
                $(this).attr('data-update', '');
            }
            $('#teachingModal form')[0].reset();
            $('#teachingModal').modal('hide');
        }
    });
    $('#btnGalleryImage').click(function(){
        var dataid             = trim($(this).attr('data-update'));
        var inputGIImage       = trim($("#inputGIImage").val());
        var inputGITitle       = trim($("#inputGITitle").val());
        var inputGIDescription = trim($("#inputGIDescription").val());
        var helpBlock          = $(this).closest('form').find('.help-block');
        helpBlock.html('');
        if (!inputGIImage) {
            helpBlock.html('Image field is compulsory');
        } else if (!inputGITitle) {
            helpBlock.html('Title field is compulsory');
        } else if (!inputGIDescription) {
            helpBlock.html('Description field is compulsory');
        } else {
            if(!dataid) {
                if($('.js-gallery').html().length == 0) {
                    $('.js-gallery').html('<table class="table table-bordered">'+
                                                '<tr>'+
                                                    '<th>Image</th>'+
                                                    '<th>Title</th>'+
                                                    '<th>Description</th>'+
                                                    '<th></th>'+
                                                    '<th></th>'+
                                                '</tr>'+
                                            '</table>');
                }
                $('.js-gallery .table tr:last-child').after('<tr data-id="n'+gi+'">'+
                                                    '<td><img src="'+inputGIImage+'" height="100px" alt="'+inputGITitle+'" /></td>'+
                                                    '<td>'+inputGITitle+'</td>'+
                                                    '<td>'+inputGIDescription+'</td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                                '</tr>');
                galleryImages.push(
                    {id: 'n'+gi,image:inputGIImage,title:inputGITitle,description:inputGIDescription,status:'new'}
                );
                gi++;
            } else {
                $('.js-gallery .table').find('tr[data-id="'+dataid+'"]').html(
                                                    '<td><img src="'+inputGIImage+'" height="100px" alt="'+inputGITitle+'" /></td>'+
                                                    '<td>'+inputGITitle+'</td>'+
                                                    '<td>'+inputGIDescription+'</td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>');
                var key = findKeyByValue(galleryImages,dataid);
                galleryImages[key].image       = inputGIImage;
                galleryImages[key].title       = inputGITitle;
                galleryImages[key].description = inputGIDescription;
                $(this).attr('data-update', '');
            }
            $('#galleryModal form')[0].reset();
            $('#galleryModal').modal('hide');
        }
    });
    $('#btnContact').click(function(){
        var dataid         = trim($(this).attr('data-update'));
        var selectCIType   = trim($("#selectCIType").val());
        var inputCITitle   = trim($("#inputCITitle").val());
        var inputCIValue   = trim($("#inputCIValue").val());
        var helpBlock      = $(this).closest('form').find('.help-block');
        helpBlock.html('');
        if (!selectCIType) {
            helpBlock.html('Contact Type field is compulsory');
        } else if (!inputCITitle) {
            helpBlock.html('Title field is compulsory');
        } else if (!inputCIValue) {
            helpBlock.html('Value field  is compulsory');
        } else {
            if(!dataid) {
                if($('.js-contact').html().length == 0) {
                    $('.js-contact').html('<table class="table table-bordered">'+
                                                '<tr>'+
                                                    '<th>Type</th>'+
                                                    '<th>Title</th>'+
                                                    '<th>Value</th>'+
                                                    '<th></th>'+
                                                    '<th></th>'+
                                                '</tr>'+
                                            '</table>');
                }
                $('.js-contact .table tr:last-child').after('<tr data-id="n'+ci+'">'+
                                                    '<td>'+selectCIType+'</td>'+
                                                    '<td>'+inputCITitle+'</td>'+
                                                    '<td>'+inputCIValue+'</td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                                '</tr>');
                contactInfo.push(
                    {id: 'n'+ci,type:selectCIType,title:inputCITitle,value:inputCIValue,status:'new'}
                );
                ci++;
            } else {
                $('.js-contact .table').find('tr[data-id="'+dataid+'"]').html(
                                                    '<td>'+selectCIType+'</td>'+
                                                    '<td>'+inputCITitle+'</td>'+
                                                    '<td>'+inputCIValue+'</td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                    '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>');
                var key = findKeyByValue(contactInfo,dataid);
                contactInfo[key].type   = selectCIType;
                contactInfo[key].title  = inputCITitle;
                contactInfo[key].value  = inputCIValue;
                $(this).attr('data-update', '');
            }
            $('#contactModal form')[0].reset();
            $('#contactModal').modal('hide');
        }
    });
    $('#selectPageType').change(function(){
        var pageType = $(this).val();
        $('.page-block').addClass('hide');
        switch(pageType) {
            case '1': $('#coursesBlock').removeClass('hide');break;
            case '2': $('#aboutBlock').removeClass('hide');break;
            case '3': $('#facultyBlock').removeClass('hide');break;
            case '4': $('#publicationsBlock').removeClass('hide');break;
            case '5': $('#teachingBlock').removeClass('hide');break;
            case '6': $('#galleryBlock').removeClass('hide');break;
            case '7': $('#contactBlock').removeClass('hide');break;
            default: $('#customBlock').removeClass('hide');break;
        }
    });
    $('.js-portfolio-pages').on('click','.js-page', function(){
        $('.js-page').removeClass('selected');
        $(this).addClass('selected');
        var pageChecked = $(this).find('[name=optionPage]');
        $('.js-portfolio-pages [name=optionPage]').removeAttr('checked');
        pageChecked.prop('checked', true);
        var pageId = pageChecked.val();
        pageProcess(pageId);
    });
    $('.js-portfolio-pages').on('click','[name=optionPage]', function(){
        var thiz = $(this);
        $('.js-page').removeClass('selected');
        thiz.closest('.js-page').addClass('selected');
        var pageId = thiz.val();
        pageProcess(pageId);
        return true;
    });
    function pageProcess(pageId) {
        if(pageId == 'new') {
            $("#pageId").val('');
            $("#pageType").val('');
            resetForm();
            $('#selectPageType').closest('.form-group').removeClass('hide');
            $('.page-block').addClass('hide');
            var pageType = $('#selectPageType option:selected').text().toLowerCase();
            $('#'+pageType+'Block').removeClass('hide');
            $('#inputPageContent').val('');
        } else {
            $('#pageId').val(pageId);
            var page = findObjByValue(portfolioPages,'pageId',pageId);
            $("#pageType").val(page.typeid);
            switch(page.type) {
                case 'courses' :
                                resetForm();
                                $('#inputPageTitle').val(page.title);
                                //$('#inputCourses').val(page.courses.courses);
                                if(page.courses.length>0) {
                                    for (var i = 0; i < page.courses.length; i++) {
                                        $('.js-courses [value='+page.courses[i]+']').prop('checked', true);
                                    }
                                }
                                $('#selectPageType').closest('.form-group').addClass('hide');
                                $('.page-block').addClass('hide');
                                $('#coursesBlock').removeClass('hide');
                                break;
                case 'about' : 
                                resetForm();
                                $('#inputPageTitle').val(page.title);
                                $('#inputAboutDescription').val(page.about.description);
                                $('#inputAcademicTitle').val(page.about.academic_title);
                                $('#inputEducationTitle').val(page.about.education_title);
                                $('#inputHonorsTitle').val(page.about.honors_title);
                                if(page.about.academic.length>0) {
                                    aboutAcademic = page.about.academic;
                                    $('.js-academic').html('<table class="table table-bordered">'+
                                            '<tr>'+
                                                '<th>From Year</th>'+
                                                '<th>To Year</th>'+
                                                '<th>Position</th>'+
                                                '<th>University</th>'+
                                                '<th>Department</th>'+
                                                '<th></th>'+
                                                '<th></th>'+
                                            '</tr>'+
                                        '</table>');
                                    for (var i = 0; i < page.about.academic.length; i++) {
                                        $('.js-academic .table tr:last-child').after('<tr data-id="'+page.about.academic[i].id+'">'+
                                                '<td>'+page.about.academic[i].fromyear+'</td>'+
                                                '<td>'+page.about.academic[i].toyear+'</td>'+
                                                '<td>'+page.about.academic[i].position+'</td>'+
                                                '<td>'+page.about.academic[i].university+'</td>'+
                                                '<td>'+page.about.academic[i].department+'</td>'+
                                                '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                            '</tr>');
                                    };
                                    $('.js-academic').val('');
                                }
                                if(page.about.education.length>0) {
                                    aboutEducation = page.about.education;
                                    $('.js-education').html('<table class="table table-bordered">'+
                                            '<tr>'+
                                                '<th>Degree</th>'+
                                                '<th>Year</th>'+
                                                '<th>Title</th>'+
                                                '<th>University</th>'+
                                                '<th></th>'+
                                                '<th></th>'+
                                            '</tr>'+
                                        '</table>');
                                    for (var i = 0; i < page.about.education.length; i++) {
                                        $('.js-education .table tr:last-child').after('<tr data-id="'+page.about.education[i].id+'">'+
                                                '<td>'+page.about.education[i].degree+'</td>'+
                                                '<td>'+page.about.education[i].year+'</td>'+
                                                '<td>'+page.about.education[i].title+'</td>'+
                                                '<td>'+page.about.education[i].university+'</td>'+
                                                '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                            '</tr>');
                                    };
                                    $('.js-education').val('');
                                }
                                if(page.about.honors.length>0) {
                                    aboutHonors = page.about.honors;
                                    $('.js-honors').html('<table class="table table-bordered">'+
                                            '<tr>'+
                                                '<th>Image</th>'+
                                                '<th>Year</th>'+
                                                '<th>Title</th>'+
                                                '<th>Description</th>'+
                                                '<th></th>'+
                                                '<th></th>'+
                                            '</tr>'+
                                        '</table>');
                                    for (var i = 0; i < page.about.honors.length; i++) {
                                        $('.js-honors .table tr:last-child').after('<tr data-id="'+page.about.honors[i].id+'">'+
                                                '<td><img src="'+page.about.honors[i].image+'" height="100px" alt="'+page.about.honors[i].title+'" /></td>'+
                                                '<td>'+page.about.honors[i].year+'</td>'+
                                                '<td>'+page.about.honors[i].title+'</td>'+
                                                '<td>'+page.about.honors[i].description+'</td>'+
                                                '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                            '</tr>');
                                    };
                                    $('.js-honors').val('');
                                }
                                $('#selectPageType').closest('.form-group').addClass('hide');
                                $('.page-block').addClass('hide');
                                $('#aboutBlock').removeClass('hide');
                                break;
                case 'faculty' : 
                                resetForm();
                                $('#inputPageTitle').val(page.title);
                                $('#inputFacultyDescription').val(page.faculty.description);
                                if(page.faculty.facultyMembers.length>0) {
                                    facultyMembers = page.faculty.facultyMembers;
                                    $('.js-faculty').html('<table class="table table-bordered">'+
                                            '<tr>'+
                                                '<th>Image</th>'+
                                                '<th>Name</th>'+
                                                '<th>University</th>'+
                                                '<th>Department</th>'+
                                                '<th></th>'+
                                                '<th></th>'+
                                            '</tr>'+
                                        '</table>');
                                    for (var i = 0; i < page.faculty.facultyMembers.length; i++) {
                                        $('.js-faculty .table tr:last-child').after('<tr data-id="'+page.faculty.facultyMembers[i].id+'">'+
                                                '<td><img src="'+page.faculty.facultyMembers[i].image+'" height="100px" alt="'+page.faculty.facultyMembers[i].name+'" /></td>'+
                                                '<td>'+page.faculty.facultyMembers[i].name+'</td>'+
                                                '<td>'+page.faculty.facultyMembers[i].university+'</td>'+
                                                '<td>'+page.faculty.facultyMembers[i].department+'</td>'+
                                                '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                            '</tr>');
                                    };
                                    $('.js-faculty').val('');
                                }
                                $('#selectPageType').closest('.form-group').addClass('hide');
                                $('.page-block').addClass('hide');
                                $('#facultyBlock').removeClass('hide');
                                break;
                case 'publications' : 
                                resetForm();
                                $('#inputPageTitle').val(page.title);
                                $('#inputPublicationsDescription').val(page.publications.description);
                                if(page.publications.publications.length>0) {
                                    publications = page.publications.publications;
                                    $('.js-publication').html('<table class="table table-bordered">'+
                                            '<tr>'+
                                                '<th>Publication Type</th>'+
                                                '<th>Name</th>'+
                                                '<th>Description</th>'+
                                                '<th>Year</th>'+
                                                '<th>ISBN</th>'+
                                                '<th></th>'+
                                                '<th></th>'+
                                            '</tr>'+
                                        '</table>');
                                    for (var i = 0; i < page.publications.publications.length; i++) {
                                        $('.js-publication .table tr:last-child').after('<tr data-id="'+page.publications.publications[i].id+'">'+
                                                '<td>'+page.publications.publications[i].txtType+'</td>'+
                                                '<td>'+page.publications.publications[i].name+'</td>'+
                                                '<td>'+page.publications.publications[i].description+'</td>'+
                                                '<td>'+page.publications.publications[i].year+'</td>'+
                                                '<td>'+page.publications.publications[i].isbn+'</td>'+
                                                '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                            '</tr>');
                                    };
                                    $('.js-publication').val('');
                                }
                                $('#selectPageType').closest('.form-group').addClass('hide');
                                $('.page-block').addClass('hide');
                                $('#publicationsBlock').removeClass('hide');
                                break;
                case 'teaching' : 
                                resetForm();
                                $('#inputPageTitle').val(page.title);
                                $('#inputTeachingDescription').val(page.teaching.description);
                                if(page.teaching.teaching.length>0) {
                                    teaching = page.teaching.teaching;
                                    $('.js-teaching').html('<table class="table table-bordered">'+
                                            '<tr>'+
                                                '<th>Title</th>'+
                                                '<th>Description</th>'+
                                                '<th>From</th>'+
                                                '<th>To</th>'+
                                                '<th></th>'+
                                                '<th></th>'+
                                            '</tr>'+
                                        '</table>');
                                    for (var i = 0; i < page.teaching.teaching.length; i++) {
                                        $('.js-teaching .table tr:last-child').after('<tr data-id="'+page.teaching.teaching[i].id+'">'+
                                                '<td>'+page.teaching.teaching[i].title+'</td>'+
                                                '<td>'+page.teaching.teaching[i].description+'</td>'+
                                                '<td>'+page.teaching.teaching[i].from+'</td>'+
                                                '<td>'+page.teaching.teaching[i].to+'</td>'+
                                                '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                            '</tr>');
                                    };
                                    $('.js-teaching').val('');
                                }
                                $('#selectPageType').closest('.form-group').addClass('hide');
                                $('.page-block').addClass('hide');
                                $('#teachingBlock').removeClass('hide');
                                break;
                case 'gallery' : 
                                resetForm();
                                $('#inputPageTitle').val(page.title);
                                $('#inputGalleryDescription').val(page.gallery.description);
                                if(page.gallery.galleryImages.length>0) {
                                    galleryImages = page.gallery.galleryImages;
                                    $('.js-gallery').html('<table class="table table-bordered">'+
                                            '<tr>'+
                                                '<th>Image</th>'+
                                                '<th>Title</th>'+
                                                '<th>Description</th>'+
                                                '<th></th>'+
                                                '<th></th>'+
                                            '</tr>'+
                                        '</table>');
                                    for (var i = 0; i < page.gallery.galleryImages.length; i++) {
                                        $('.js-gallery .table tr:last-child').after('<tr data-id="'+page.gallery.galleryImages[i].id+'">'+
                                                '<td><img src="'+page.gallery.galleryImages[i].image+'" height="100px" alt="'+page.gallery.galleryImages[i].title+'" /></td>'+
                                                '<td>'+page.gallery.galleryImages[i].title+'</td>'+
                                                '<td>'+page.gallery.galleryImages[i].description+'</td>'+
                                                '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                            '</tr>');
                                    };
                                    $('.js-gallery').val('');
                                }
                                $('#selectPageType').closest('.form-group').addClass('hide');
                                $('.page-block').addClass('hide');
                                $('#galleryBlock').removeClass('hide');
                                break;
                case 'contact' : 
                                resetForm();
                                $('#inputPageTitle').val(page.title);
                                $('#inputContactDescription').val(page.contact.contact_description);
                                $('#inputOfficeDescription').val(page.contact.office_description);
                                $('#inputWorkDescription').val(page.contact.work_description);
                                if($('#inputLabDescription').length>0 && !!page.contact.lab_description) {
                                    $('#inputLabDescription').val(page.contact.lab_description);
                                }
                                if(page.contact.contactInfo.length>0) {
                                    contactInfo = page.contact.contactInfo;
                                    $('.js-contact').html('<table class="table table-bordered">'+
                                            '<tr>'+
                                                '<th>Type</th>'+
                                                '<th>Title</th>'+
                                                '<th>Value</th>'+
                                                '<th></th>'+
                                                '<th></th>'+
                                            '</tr>'+
                                        '</table>');
                                    for (var i = 0; i < page.contact.contactInfo.length; i++) {
                                        $('.js-contact .table tr:last-child').after('<tr data-id="'+page.contact.contactInfo[i].id+'">'+
                                                '<td>'+page.contact.contactInfo[i].type+'</td>'+
                                                '<td>'+page.contact.contactInfo[i].title+'</td>'+
                                                '<td>'+page.contact.contactInfo[i].value+'</td>'+
                                                '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                                '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                            '</tr>');
                                    };
                                    $('.js-contact').val('');
                                }
                                $('#selectPageType').closest('.form-group').addClass('hide');
                                $('.page-block').addClass('hide');
                                $('#contactBlock').removeClass('hide');
                                break;
                case 'custom' :
                                resetForm();
                                $('#inputPageTitle').val(page.title);
                                $('#inputPageContent').val(page.content);
                                $('#selectPageType').closest('.form-group').addClass('hide');
                                $('.page-block').addClass('hide');
                                $('#customBlock').removeClass('hide');
                                break;
            }
        }
    }
    function resetForm() {
        $('#inputPageTitle').val('');
        $('#inputCourses').val('');
        $('#inputAboutDescription').val('');
        $('#inputAcademicTitle').val('Academic Position');
        $('#inputEducationTitle').val('Education &amp; Training');
        $('#inputHonorsTitle').val('Our Honors');
        $('#inputFacultyDescription').val('');
        $('#inputGalleryDescription').val('');
        $('#inputContactDescription').val('');
        $('#inputOfficeDescription').val('');
        $('#inputWorkDescription').val('');
        if($('#inputLabDescription').length>0) {
            $('#inputLabDescription').val('');
        }
        $('.js-academic').val('');
        $('.js-education').val('');
        $('.js-honors').val('');
        $('.js-faculty').val('');
        $('.js-gallery').val('');
        $('.js-contact').val('');
        var aboutAcademic = [];
        var aa = 1;
        var aboutEducation = [];
        var ae = 1;
        var aboutHonors = [];
        var ah = 1;
        var facultyMembers = [];
        var fm = 1;
        var galleryImages = [];
        var gi = 1;
        var contactInfo = [];
        var ci = 1;
        var portfolioPages = [];
    }
    function fetchPortfolioCourses() {
        var req = {};
        req.slug = trim($('#slug').val());
        req.action = "get-portfolio-courses";
        var res;
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            portfolioCourses = res.courses;
            fillPortfolioCourses(portfolioCourses);
            fetchPortfolioPages();
            console.log(res);
        });
    }
    function fillPortfolioCourses(portfolioCourses) {
        // General Details
        if(portfolioCourses.length>0) {
            $('.js-courses').html('<ul class="nav nav-pills nav-stacked"></ul>');
            for (var i = 0; i < portfolioCourses.length; i++) {
                $('.js-courses>.nav-pills').append('<li><a href="javascript:void(0)"><input type="checkbox" value="'+portfolioCourses[i].value+'" />'+portfolioCourses[i].label+'</a></li>');
            };
        } else {
            $('.js-courses').html('No courses posted by you!');
        }
    }
    function fetchPageTypes() {
        var req = {};
        req.slug = trim($('#slug').val());
        var res;
        req.action = "get-page-types";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillPageTypes(res);
        });
    }
    function fillPageTypes(data) {
        // General Details
        if(data.pageTypes.length>0) {
            for (var i = 0; i < data.pageTypes.length; i++) {
                $('#selectPageType').append('<option value="'+data.pageTypes[i].value+'">'+capitalizeFirstLetter(data.pageTypes[i].type)+'</option>');
            };
            var pageType = $('#selectPageType option:selected').text().toLowerCase();
            //alert('#'+pageType+'Block');
            $('#'+pageType+'Block').removeClass('hide');
        } else {
            $('#selectPageType').html('<option>No Page Types</option>');
        }
    }
    function fetchPortfolioPages() {
        var req = {};
        req.slug = trim($('#slug').val());
        var res;
        req.action = "get-portfolio-pages";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            portfolioPages = res.portfolioPages;
            fillPortfolioPages(res);
        });
    }
    function fillPortfolioPages(data) {
        // General Details
        if(data.portfolioPages.length>0) {
            //$('.js-portfolio-pages').html('<ul class="nav nav-pills nav-stacked"></ul>');
            for (var i = 0; i < data.portfolioPages.length; i++) {
                $('.js-portfolio-pages>.nav-pills').append('<li><a href="javascript:void(0)" class="js-page"><span class="checkbox" name="optionCourses[]"><input type="radio" name="optionPage" value="'+data.portfolioPages[i].pageId+'" /> '+data.portfolioPages[i].title+' [Type : '+data.portfolioPages[i].type+']</span></a></li>');
            };
        }
    }
    $('#btnSavePortfolioPage').click(function(){
        var pageId                  = trim($('#pageId').val());
        if(!pageId) {
            var selectPageType      = trim($('#selectPageType').val());
        } else {
            var selectPageType      = trim($('#pageType').val());
        }
        var inputPageTitle          = trim($('#inputPageTitle').val());

        var inputCourses            = '';
        $('.js-courses input:checked').each(function(i,elem){
            inputCourses+= $(elem).val()+',';
        });
        inputCourses = inputCourses.substr(0, inputCourses.length-1);
        var inputAboutDescription        = trim($('#inputAboutDescription').val());
        var inputAcademicTitle           = trim($('#inputAcademicTitle').val());
        var inputEducationTitle          = trim($('#inputEducationTitle').val());
        var inputHonorsTitle             = trim($('#inputHonorsTitle').val());
        var inputFacultyDescription      = trim($('#inputFacultyDescription').val());
        var inputPublicationsDescription = trim($('#inputPublicationsDescription').val());
        var inputTeachingDescription     = trim($('#inputTeachingDescription').val());
        var inputGalleryDescription      = trim($('#inputGalleryDescription').val());
        var inputContactDescription      = trim($('#inputContactDescription').val());
        var inputOfficeDescription       = trim($('#inputOfficeDescription').val());
        var inputWorkDescription         = trim($('#inputWorkDescription').val());
        if($('#inputLabDescription').length>0) {
            var inputLabDescription         = trim($('#inputLabDescription').val());
        }
        var inputPageContent             = trim($('#inputPageContent').val());
        if(!selectPageType) {
            $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">Please select Page Type</p>');
        } else if(!inputPageTitle) {
            $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">Page Title can\'t be left empty</p>');
        } else if(selectPageType == '1' && !inputCourses) {
            $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">Courses field can\'t be left empty</p>');
        } else if(selectPageType == '2' && !inputAboutDescription) {
            $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">About Description can\'t be left empty</p>');
        } else if(selectPageType == '2' && !inputAcademicTitle) {
            $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">Academic Position Title can\'t be left empty</p>');
        } else if(selectPageType == '2' && !inputEducationTitle) {
            $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">Education Title can\'t be left empty</p>');
        } else if(selectPageType == '2' && !inputHonorsTitle) {
            $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">Our Honors Title can\'t be left empty</p>');
        } else if(selectPageType == '3' && !inputFacultyDescription) {
            $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">Faculty Description can\'t be left empty</p>');
        } else if(selectPageType == '4' && !inputPublicationsDescription) {
            $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">Publications Description can\'t be left empty</p>');
        } else if(selectPageType == '5' && !inputTeachingDescription) {
            $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">Teaching Description can\'t be left empty</p>');
        } else if(selectPageType == '6' && !inputGalleryDescription) {
            $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">Gallery Description can\'t be left empty</p>');
        } else if(selectPageType == '7' && !inputContactDescription) {
            $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">Contact Description can\'t be left empty</p>');
        } else if(selectPageType == '7' && !inputOfficeDescription) {
            $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">Office Description can\'t be left empty</p>');
        } else if(selectPageType == '7' && !inputWorkDescription) {
            $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">Work Description can\'t be left empty</p>');
        } else if(selectPageType == '7' && $('#inputLabDescription').length>0 && !inputLabDescription) {
            $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">Lab Description can\'t be left empty</p>');
        } else if(selectPageType == '8' && !inputPageContent) {
            $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">Page Content can\'t be left empty</p>');
        } else {
            var req       = {};
            req.slug      = $('#slug').val();
            req.pageId    = pageId;
            req.pageType  = selectPageType;
            req.pageTitle = inputPageTitle;
            switch(selectPageType) {
                case '1' :  req.inputCourses                 = inputCourses;break;
                case '2' :  req.inputAboutDescription        = inputAboutDescription;
                            req.inputAcademicTitle           = inputAcademicTitle;
                            req.aboutAcademic                = aboutAcademic;
                            req.inputEducationTitle          = inputEducationTitle;
                            req.aboutEducation               = aboutEducation;
                            req.inputHonorsTitle             = inputHonorsTitle;
                            req.aboutHonors                  = aboutHonors;
                            break;
                case '3' :  req.inputFacultyDescription      = inputFacultyDescription;
                            req.facultyMembers               = facultyMembers;
                            break;
                case '4' :  req.inputPublicationsDescription = inputPublicationsDescription;
                            req.publications                 = publications;
                            break;
                case '5' :  req.inputTeachingDescription     = inputTeachingDescription;
                            req.teaching                     = teaching;
                            break;
                case '6' :  req.inputGalleryDescription      = inputGalleryDescription;
                            req.galleryImages                = galleryImages;
                            break;
                case '7' :  req.inputContactDescription      = inputContactDescription;
                            req.inputOfficeDescription       = inputOfficeDescription;
                            req.inputWorkDescription         = inputWorkDescription;
                            if($('#inputLabDescription').length>0) {
                                req.inputLabDescription      = inputLabDescription;
                            }
                            req.contactInfo                  = contactInfo;
                            break;
                case '8' :  req.pageContent = inputPageContent;
                            break;
            }
            var res;
            req.action = "save-portfolio-page";
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if(res.status == 1) {
                    var page = res;
                    switch(selectPageType) {
                        case '2' :
                            resetForm();
                            $('#inputPageTitle').val(page.title);
                            $('#inputAboutDescription').val(page.about.description);
                            $('#inputAcademicTitle').val(page.about.academic_title);
                            $('#inputEducationTitle').val(page.about.education_title);
                            $('#inputHonorsTitle').val(page.about.honors_title);
                            if(page.about.academic.length>0) {
                                aboutAcademic = page.about.academic;
                                $('.js-academic').html('<table class="table table-bordered">'+
                                        '<tr>'+
                                            '<th>From Year</th>'+
                                            '<th>To Year</th>'+
                                            '<th>Position</th>'+
                                            '<th>University</th>'+
                                            '<th>Department</th>'+
                                            '<th></th>'+
                                            '<th></th>'+
                                        '</tr>'+
                                    '</table>');
                                for (var i = 0; i < page.about.academic.length; i++) {
                                    $('.js-academic .table tr:last-child').after('<tr data-id="'+page.about.academic[i].id+'">'+
                                            '<td>'+page.about.academic[i].fromyear+'</td>'+
                                            '<td>'+page.about.academic[i].toyear+'</td>'+
                                            '<td>'+page.about.academic[i].position+'</td>'+
                                            '<td>'+page.about.academic[i].university+'</td>'+
                                            '<td>'+page.about.academic[i].department+'</td>'+
                                            '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                            '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                        '</tr>');
                                };
                                $('.js-academic').val('');
                            }
                            if(page.about.education.length>0) {
                                aboutEducation = page.about.education;
                                $('.js-education').html('<table class="table table-bordered">'+
                                        '<tr>'+
                                            '<th>Degree</th>'+
                                            '<th>Year</th>'+
                                            '<th>Title</th>'+
                                            '<th>University</th>'+
                                            '<th></th>'+
                                            '<th></th>'+
                                        '</tr>'+
                                    '</table>');
                                for (var i = 0; i < page.about.education.length; i++) {
                                    $('.js-education .table tr:last-child').after('<tr data-id="'+page.about.education[i].id+'">'+
                                            '<td>'+page.about.education[i].degree+'</td>'+
                                            '<td>'+page.about.education[i].year+'</td>'+
                                            '<td>'+page.about.education[i].title+'</td>'+
                                            '<td>'+page.about.education[i].university+'</td>'+
                                            '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                            '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                        '</tr>');
                                };
                                $('.js-education').val('');
                            }
                            if(page.about.honors.length>0) {
                                aboutHonors = page.about.honors;
                                $('.js-honors').html('<table class="table table-bordered">'+
                                        '<tr>'+
                                            '<th>Image</th>'+
                                            '<th>Year</th>'+
                                            '<th>Title</th>'+
                                            '<th>Description</th>'+
                                            '<th></th>'+
                                            '<th></th>'+
                                        '</tr>'+
                                    '</table>');
                                for (var i = 0; i < page.about.honors.length; i++) {
                                    $('.js-honors .table tr:last-child').after('<tr data-id="'+page.about.honors[i].id+'">'+
                                            '<td><img src="'+page.about.honors[i].image+'" height="100px" alt="'+page.about.honors[i].title+'" /></td>'+
                                            '<td>'+page.about.honors[i].year+'</td>'+
                                            '<td>'+page.about.honors[i].title+'</td>'+
                                            '<td>'+page.about.honors[i].description+'</td>'+
                                            '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                            '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                        '</tr>');
                                };
                                $('.js-honors').val('');
                            }
                            break;
                        case '3' :
                            resetForm();
                            $('#inputPageTitle').val(page.title);
                            $('#inputFacultyDescription').val(page.faculty.description);
                            if(page.faculty.facultyMembers.length>0) {
                                facultyMembers = page.faculty.facultyMembers;
                                $('.js-faculty').html('<table class="table table-bordered">'+
                                        '<tr>'+
                                            '<th>Image</th>'+
                                            '<th>Name</th>'+
                                            '<th>University</th>'+
                                            '<th>Department</th>'+
                                            '<th></th>'+
                                            '<th></th>'+
                                        '</tr>'+
                                    '</table>');
                                for (var i = 0; i < page.faculty.facultyMembers.length; i++) {
                                    $('.js-faculty .table tr:last-child').after('<tr data-id="'+page.faculty.facultyMembers[i].id+'">'+
                                            '<td><img src="'+page.faculty.facultyMembers[i].image+'" height="100px" alt="'+page.faculty.facultyMembers[i].name+'" /></td>'+
                                            '<td>'+page.faculty.facultyMembers[i].name+'</td>'+
                                            '<td>'+page.faculty.facultyMembers[i].university+'</td>'+
                                            '<td>'+page.faculty.facultyMembers[i].department+'</td>'+
                                            '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                            '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                        '</tr>');
                                };
                                $('.js-faculty').val('');
                            }
                            break;
                        case '4' :
                            resetForm();
                            $('#inputPageTitle').val(page.title);
                            $('#inputPublicationsDescription').val(page.publications.description);
                            if(page.publications.publications.length>0) {
                                publications = page.publications.publications;
                                $('.js-publication').html('<table class="table table-bordered">'+
                                        '<tr>'+
                                            '<th>Publication Type</th>'+
                                            '<th>Name</th>'+
                                            '<th>Description</th>'+
                                            '<th>Year</th>'+
                                            '<th>ISBN</th>'+
                                            '<th></th>'+
                                            '<th></th>'+
                                        '</tr>'+
                                    '</table>');
                                for (var i = 0; i < page.publications.publications.length; i++) {
                                    $('.js-publication .table tr:last-child').after('<tr data-id="'+page.publications.publications[i].id+'">'+
                                            '<td>'+page.publications.publications[i].txtType+'</td>'+
                                            '<td>'+page.publications.publications[i].name+'</td>'+
                                            '<td>'+page.publications.publications[i].description+'</td>'+
                                            '<td>'+page.publications.publications[i].year+'</td>'+
                                            '<td>'+page.publications.publications[i].isbn+'</td>'+
                                            '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                            '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                        '</tr>');
                                };
                                $('.js-publication').val('');
                            }
                            break;
                        case '5' :
                            resetForm();
                            $('#inputPageTitle').val(page.title);
                            $('#inputTeachingDescription').val(page.teaching.description);
                            if(page.teaching.teaching.length>0) {
                                teaching = page.teaching.teaching;
                                $('.js-teaching').html('<table class="table table-bordered">'+
                                        '<tr>'+
                                            '<th>Title</th>'+
                                            '<th>Description</th>'+
                                            '<th>From</th>'+
                                            '<th>To</th>'+
                                            '<th></th>'+
                                            '<th></th>'+
                                        '</tr>'+
                                    '</table>');
                                for (var i = 0; i < page.teaching.teaching.length; i++) {
                                    $('.js-teaching .table tr:last-child').after('<tr data-id="'+page.teaching.teaching[i].id+'">'+
                                            '<td>'+page.teaching.teaching[i].title+'</td>'+
                                            '<td>'+page.teaching.teaching[i].description+'</td>'+
                                            '<td>'+page.teaching.teaching[i].from+'</td>'+
                                            '<td>'+page.teaching.teaching[i].to+'</td>'+
                                            '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                            '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                        '</tr>');
                                };
                                $('.js-teaching').val('');
                            }
                            break;
                        case '6' :
                            resetForm();
                            $('#inputPageTitle').val(page.title);
                            $('#inputGalleryDescription').val(page.gallery.description);
                            if(page.gallery.galleryImages.length>0) {
                                galleryImages = page.gallery.galleryImages;
                                $('.js-gallery').html('<table class="table table-bordered">'+
                                        '<tr>'+
                                            '<th>Image</th>'+
                                            '<th>Title</th>'+
                                            '<th>Description</th>'+
                                            '<th></th>'+
                                            '<th></th>'+
                                        '</tr>'+
                                    '</table>');
                                for (var i = 0; i < page.gallery.galleryImages.length; i++) {
                                    $('.js-gallery .table tr:last-child').after('<tr data-id="'+page.gallery.galleryImages[i].id+'">'+
                                            '<td><img src="'+page.gallery.galleryImages[i].image+'" height="100px" alt="'+page.gallery.galleryImages[i].title+'" /></td>'+
                                            '<td>'+page.gallery.galleryImages[i].title+'</td>'+
                                            '<td>'+page.gallery.galleryImages[i].description+'</td>'+
                                            '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                            '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                        '</tr>');
                                };
                                $('.js-gallery').val('');
                            }
                            break;
                        case '7' :
                            resetForm();
                            $('#inputPageTitle').val(page.title);
                            $('#inputContactDescription').val(page.contact.contact_description);
                            $('#inputOfficeDescription').val(page.contact.office_description);
                            $('#inputWorkDescription').val(page.contact.work_description);
                            if($('#inputLabDescription').length>0 && !!page.contact.lab_description) {
                                $('#inputLabDescription').val(page.contact.lab_description);
                            }
                            if(page.contact.contactInfo.length>0) {
                                contactInfo = page.contact.contactInfo;
                                $('.js-contact').html('<table class="table table-bordered">'+
                                        '<tr>'+
                                            '<th>Type</th>'+
                                            '<th>Title</th>'+
                                            '<th>Value</th>'+
                                            '<th></th>'+
                                            '<th></th>'+
                                        '</tr>'+
                                    '</table>');
                                for (var i = 0; i < page.contact.contactInfo.length; i++) {
                                    $('.js-contact .table tr:last-child').after('<tr data-id="'+page.contact.contactInfo[i].id+'">'+
                                            '<td>'+page.contact.contactInfo[i].type+'</td>'+
                                            '<td>'+page.contact.contactInfo[i].title+'</td>'+
                                            '<td>'+page.contact.contactInfo[i].value+'</td>'+
                                            '<td><button class="btn btn-warning btn-xs js-btn-change">Change</button></td>'+
                                            '<td><button class="btn btn-warning btn-xs js-btn-remove">Remove</button></td>'+
                                        '</tr>');
                                };
                                $('.js-contact').val('');
                            }
                            break;
                    }
                    //$('#frmPortfolioPages .help-block').html('<p class="text-success text-center">Pages Updated!</p>');
                    alert("Pages Updated!");
                    window.location = window.location;
                } else {
                    $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">'+res.message+'</p>');
                }
                console.log(res);
            });
        }
        return false;
    });
});