        /*
            Author: Ayush Pandey
            Dated: 06/07/2014
            
            
            ********
            1) Notes: JSON.stringify support for IE < 10
        */

        var userRole;
        var reorderTimer = null;
        var coursedata= [];  
        var coursedatei=null;
        var idParent=null;
        $(function () {
           
           
            var imageRoot = '/';
            $('#loader_outer').hide();
             
            
            function fetchProfileDetails() {
               
                req.action = "get-profile-details";
                $.ajax({
                    'type'  : 'post',
                    'url'   : ApiEndpoint,
                    'data'  : JSON.stringify(req)
                }).done(function (res) {
                    res =  $.parseJSON(res);
                    fillProfileDetails(res);
                });
            }
            
            function fillProfileDetails(data) {
                // General Details
                var imageRoot = 'user-data/images/';
                $('#user-profile-card .username').text(data.loginDetails.username);
                
                if(data.profileDetails.profilePic != "") {
                    $('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
                }
                
                // Role Specific
                userRole = data.userRole;
                if(data.userRole == '1') {
                    fillInstituteDetails(data);
                }
                else if(data.userRole == '2'){
                    fillProfessorDetails(data);
                }
                else if(data.userRole == '3') {
                    fillPublisherDetails(data);
                }
            }
            
            function fillInstituteDetails(data) {
                $('h4#profile-name').text(data.profileDetails.name);
            }
            
            function fillProfessorDetails(data) {
                var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
                $('a#profile-name').text(name);
            }
            
            function fillPublisherDetails(data) {
                var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
                $('a#profile-name').text(name);
            }
            
            function fetchCourses() {
                var req = {};
                var res;
                req.action = "get-all-courses";
                $.ajax({
                    'type'  : 'post',
                    'url'   : ApiEndpoint,
                    'data'  : JSON.stringify(req)
                }).done(function (res) {
                    res =  $.parseJSON(res);
                     fillCourses(res);
                });
            }


            function fillCourses(data) {
                var subjectHtml = '', html = '';
                var courseStartDate = '';
                $.each(data.courses, function (i, course) {
                    //variable to detect payment can be made or not
                    coursedata.push( course);
                    var paymentStatus = 1;
                    var startdate = new Date(parseInt(course.liveDate));
                    var today = new Date();
                    var enddate="";
                    var couEnddate =[];
                    if(course.endDate!=""){
                       enddate  =new Date(parseInt(course.endDate));
                       enddate.setFullYear(enddate.getFullYear() + 1);
                       couEnddate.push(enddate.getDate());
                       couEnddate.push(MONTH[enddate.getMonth()]);
                       couEnddate.push(enddate.getFullYear());
                    }
                    else{
                        var enddate="";
                        couEnddate.push("");
                        couEnddate.push("");
                        couEnddate.push("");
                    }
                   
                    var currentprice=course.studentPrice;
                    var currentpriceINR=course.studentPriceINR;
                    var discountside=0;
                    var buttonname= "Generate Coupon";
                    var flag=0;
                    if(!(currentprice>0 || currentpriceINR>0))
                    {   flag=1;
                        buttonname="Free Course";
                    }
                    if(course.discount.endDate>today.getTime())
                    {   buttonname="Generate Coupon";

                        discountside=Math.round(course.discount.discount);
                        currentprice=course.studentPrice*((100 - course.discount.discount) / 100);
                        currentpriceINR=course.studentPriceINR*((100 - course.discount.discountINR) / 100);
                    }
               
                   
                    if (course.endDate != null && course.endDate != '') {
                        var enddatedb = new Date(parseInt(course.endDate));
                        var origStart = new Date(parseInt(course.origStartDate));
                        if (enddatedb.valueOf() < enddate.valueOf()) {
                            //enddate = new Date(enddatedb);
                            enddate = enddatedb;
                            //disabling self payment
                            paymentStatus = 0;
                        }
                        //this will show the course starting date
                        //courseStartDate = '<br><i class="fa fa-clock-o"></i> <strong>Start Date</strong> ' + origStart.getDate() + ' ' + month[origStart.getMonth()] + ' ' + origStart.getFullYear();
                    }
                    var percentage = (today.valueOf() - startdate.valueOf()) / (enddate.valueOf() - startdate.valueOf()) * 100;
                    var percent = parseFloat(percentage);
                    var display = percent.toFixed(2) + '%';
                    var expired="";
                    
                    
                    
                    if(enddate.valueOf()<today.valueOf()){
                        expired="disable-mycourse";
                    }
                    // alert(display);
                    html += 
                    '<div class="col-md-12 col-sm-12 grid-box-course">'
                        + '<div class="row">' 
                          + '<div class="col-md-3">'
                                + '<a href="../courseDetails.php?courseId=' + course.id + '"><img onerror=""  class="img-responsive course-img" src=" '+course.image+'" alt=""></a>'
                            + '</div>'
                            + '<div class="col-md-9">'
                                + '<div class="col-md-12">'
                                    + '<div> '
                                        + '<h3> <a href="../courseDetails.php?courseId=' + course.id + '"> ' + course.name + '<span style="font-size:17px;font-weight:600;"> (ID: C00' + course.id + ')</span> </a></h3>'
                                            + '<div class="price-row">'
                                                + '<strong>Original Price</strong>:&nbsp;&nbsp; <b><i class="fa fa-rupee"></i></b>'
                                                + '<b id="curPriceINR'+i+'">'+course.studentPriceINR+'&nbsp;&nbsp;&nbsp;&nbsp;</b> <b><i class="fa fa-dollar"></i></b>'
                                                + '<b id="curPrice'+i+'">'+course.studentPrice+'</b>'
                                            + '</div>'
                                + '</div>'
                            + '</div>'
                            + '<div class="col-sm-4" style="padding-top:10px;">'
                                + '<div class="col-xs-2"><i style="font-size:35px; color:orange;margin:5px 10px 0px 0px;" class="fa fa-clock-o"></i></div>'
                                + '<div class="col-xs-10">'                 
                                    + '<span><strong>Start Date:</strong> ' + startdate.getDate() + ' ' +MONTH[startdate.getMonth()] + ' ' + startdate.getFullYear() + '</span><br/><span><strong>End Date:</strong> ' + couEnddate[0] + ' ' + couEnddate[1] + ' ' + couEnddate[2] + '</span>'
                                    + '<div style="margin-top: 6px;" class="progress progress-xs">'
                                        + '<div style="width: ' + display + '" aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar progress-bar-danger">'
                                            + '<span class="sr-only">' + display + ' Complete</span>'
                                        + '</div>'
                                    + '</div>'
                                + '</div>'                   
                            + '</div>'
                            + '<div class="col-sm-4" style="padding-top:10px;">'
                                   
                            + '</div>'                 
                            + '<div class="text-center">'
                                + '<div class="col-md-4" style="padding-top:10px;">'
                                    + '<div class="col-xs-11" id ="discount" >' 
                                    // ((course.discount.endDate>today.getTime())?"Edit Discount":"Give Discount")
                                        + '<a  class="btn btn-md btn-success course" '+((flag>0)?"disabled":"")+' id="discount'+i+'" > '+ buttonname +'</a>'                      
                                    + '</div>'
                                + '</div>'
                            + '</div>'
                        + '</div>'
                    + '</div>'
                + '</div>'
                });
         
                $('#allcourses-Coupons').append(html);
                 
        // action when discount button to pop up form
        $('.course').on( "click", function() {
				var getId=$(this).attr('id');
                getId=getId.replace('discount','');
                idParent=getId;
                coursedatei=coursedata[getId];
                $("#originalPriceINR").text(coursedata[getId].studentPriceINR);
                $("#originalPrice").html(coursedata[getId].studentPrice);
                //$('#Give_Discount').modal('show')
                $("#coupons").trigger('reset');
                $('#coupons').modal('show');
				

              });
        // action when discount button is clicked of the pop up form
        $('.courseCouponbtn').on('click', function() {
            
            var req="";
            var resp="";
            var newprice = 0;
            var newpriceINR =0;
            var originalPrice =coursedatei.studentPrice;;
            var originalPriceINR = coursedatei.studentPriceINR;
            var couponcode=$('.couponCodeC').val().toUpperCase();
            var noOfcoupons=$('.noofCouponsC').val();
            var discount = $('.discount11').val();
            var couponEndDate = new Date($('.couponEndDate1').val());
            var couponStartDate= new Date($('.couponStartDate1').val());   

            var error = "";
            $('.help-block').remove();
            $('.has-error').removeClass('has-error');
           
            if (couponcode == '') {
               $('.couponCodeC').parent().addClass('has-error').append('<span class="help-block">Please enter Coupon Code </span>');
                 return false;
            }

            if ((noOfcoupons <= 0)) {
               $('.noofCouponsC').parent().addClass('has-error').append('<span class="help-block">Please enter Value greater than 0 </span>');
                 return false;
            }
            if (discount == '') {
               $('.discount11').parent().addClass('has-error').append('<span class="help-block">Please enter Discount </span>');
                 return false;
            }

            if ($('.couponStartDate1').val() == '') {
                 unsetError($('.couponStartDate1'));
               $('.couponStartDate').parent().addClass('has-error').append('<span class="help-block">Please enter Start Date </span>');
                return false;
            }
            else if($('.couponEndDate1').val() == ''){
                   unsetError($('.couponEndDate1'));
                  $('.couponEndDate1').parent().addClass('has-error').append('<span class="help-block">Please enter End Date </span>');
                return false;
            }
            else{
                 var couponStartDate=couponStartDate.getTime();
                 var couponEndDate = couponEndDate.getTime();
                 //console.log(couponEndDate);
                // console.log(couponStartDate);

                 if(couponStartDate > couponEndDate || !(couponStartDate < couponEndDate) )
                 {
                    unsetError($('.couponEndDate1'));
                  $('.couponEndDate1').parent().addClass('has-error').append('<span class="help-block">End Date should be greater than Start Date  </span>');
                return false;
                 }
            }         

                  if(discount > 100 || discount < 0 || !$.isNumeric(discount))
                    {
                            $('.discount11').val(''); 
                            $('.discount11').parent().addClass('has-error').append('<span class="help-block">Please Enter value between 0 and 100 </span>');
                            return false;
                    }
                    else{
                           newpriceINR =(originalPriceINR * ((100 - discount) / 100));
                           newprice = (originalPrice * ((100 - discount) / 100));
                           newpriceINR=parseFloat(newpriceINR).toFixed(2);

                        }
         
                   var today= new Date().getTime();

                var req = {
                        'courseId' : coursedatei.id,
                        'couponCode': couponcode,
                        'noOfCoupons': noOfcoupons ,
                        'DiscountPer' : discount,
                        'DiscountPerINR' : discount,
                        'startDate': couponStartDate,
                        'endDate':  couponEndDate,  
                        'today' : today            
                          };
                var res;
                req.action = 'course-coupons';
                $.ajax({
                    'type'  : 'post',
                    'url'   : ApiEndpoint,
                    'data'  : JSON.stringify(req)
                }).done(function (res) {
                    res =  $.parseJSON(res);
                    if(res.status == 1){
                         alertMsg(res.message);
                        $('#coupons').modal('hide');
                    // location.reload();

                        var disreq={
                                        'courseId' : coursedatei.id,
                                    };
                        var disres=null;
                        disreq.action = 'discount-courseId3';
                         $.ajax({
                                    'type'  : 'post',
                                    'url'   : ApiEndpoint,
                                    'data'  : JSON.stringify(disreq)
                                 }).done(function (disres){
                                  disres =  $.parseJSON(disres);
                                       
                            if(disres.endDate>new Date().getTime()){
                                                     var newdis= disres.discount;
                                                     var newdisINR=disres.discountINR;
                                                    var newprice=originalPrice*((100-newdis)/100).toFixed(2);
                                                    var newpriceINR=originalPriceINR*((100-newdisINR)/100).toFixed(2);
                                                    var dispDis= Math.round(newdis);
                        // chnaging values of element using ajax call in parent
                                                 $("#discountside"+idParent).html(dispDis+"% OFF");
                                                 $("#curPrice"+idParent).html(newprice);
                                                 $("#curPriceINR"+idParent).html(newpriceINR);
                                                 $("#discount"+idParent).text("Edit Discount");
                         //curPriceINR

                       }
                });




                     
                  }
                    else
                        alertMsg(res.message);
                });
          
        });

     


        //$("#originalPriceINR").text(coursedata[0].studentPriceINR);
        //$("#originalPrice").html(coursedata[0].studentPrice);

        $('.discountActive').ready(function (){
            var countDiscount=coursedata.length;
            var count=0;
            var today = new Date();
            for (index = 0; index < countDiscount; index++) 
                {
                 var newelement= coursedata[index];
                 if(newelement.discount.endDate>today.getTime())
                    {
                     count++;
                    }
                 }
                 
            $('.discountActive').html(count);

        });



		
        $('.couponCodeC').keyup(function () {
         unsetError($('.couponCodeC'));
		  var coupon = $('.couponCodeC').val();
		 if(!(coupon.length>0))
		 {
		 	 $('.couponCodeC').val('');                     
                             $(this).parent().addClass('has-error').append('<span class="help-block">Please Enter coupon code </span>');                    
                            return false;
			
		 }
		/* if(coupon.length<6 && coupon.length>8)
		 {
		 	 $('.couponCodeC').val('');                     
                             $(this).parent().addClass('has-error').append('<span class="help-block">Please Enter coupon code of 6 to 8 characters </span>');                    
                            return false;
			
		 }*/
		 });
		 
		// noofCouponsC
		
		$('.noofCouponsC').keyup(function () {
         unsetError($('.noofCouponsC'));

             var noofCouponsC = $('.noofCouponsC').val();
            var error = "";
                           
                    if( noofCouponsC <= 0 || !($.isNumeric(noofCouponsC)) )
                    {  
                     
                            $('.noofCouponsC').val('');                     
                             $(this).parent().addClass('has-error').append('<span class="help-block">Please Enter value greater than 0</span>');                    
                            return false;
                    }
           
               
        });

        $('.discount11').keyup(function () {
            unsetError($('.discount11'));
            var newprice = 0;
            var newpriceINR =0; 
            var originalPrice = coursedatei.studentPrice;
            var originalPriceINR = coursedatei.studentPriceINR;
            var discount = $('.discount11').val();
            var error = "";
                           
                    if(discount > 100 || discount < 0 || !$.isNumeric(discount))
                    {  
                     
                            $('.discount11').val('');                     
                             $(this).parent().addClass('has-error').append('<span class="help-block">Please Enter value between 0 and 100 </span>');                    
                            return false;
                    }
                    else{
                            newpriceINR = originalPriceINR * ((100 - discount) / 100);
                            newprice = originalPrice * ((100 - discount) / 100);
                            newpriceINR=parseFloat(newpriceINR).toFixed(2);
                            newprice=parseFloat(newprice).toFixed(2);
                       }
         


                      
                $('#price_After_DiscountINR').html('<span class="priceDisp"> <i class="fa fa-rupee"></i> '+newpriceINR+'  </span> ');
                $('#price_After_Disocount').html(' <span class="priceDisp">  <i class="fa fa-dollar"></i> '+newprice+' </span>');
               
        });






        $('#couponStartDate , #couponEndDate').datetimepicker({
                format: 'd F Y H:i',
                timepicker: true,
                closeOnDateSelect: true,
                minDate: 0,
                maxDate: '2050/12/31',
                onSelectDate: function(date) {
                    unsetError($('#couponStartDate, #couponEndDate'));         
                            
                }
            });
			
			
        $('.couponEndDate1 , .couponStartDate1').keyup(function () {
            unsetError($('.couponStartDate1'));
             unsetError($('.couponEndDate1'));
             var couponEndDate = new Date($('.couponEndDate1').val());
            var couponStartDate= new Date($('.couponStartDate1').val()); 
            if($('.couponStartDate1').val()=="")
            {
              $(this).parent().addClass('has-error').append('<span class="help-block">Please Enter Start Date </span>');                    
                            return false;
            }
            if($('.couponEndDate1').val()=="")
            {
              $(this).parent().addClass('has-error').append('<span class="help-block">Please Enter End Date </span>');                    
                            return false;
            }


            if(couponStartDate > couponEndDate || !(couponStartDate < couponEndDate) )
                 {
                    unsetError($('.couponEndDate1'));
                  $('.couponEndDate1').parent().addClass('has-error').append('<span class="help-block">End Date should be greater than Start Date  </span>');
                return false;
                 }
        });
		
		$('#couponStartDate , #couponEndDate').on('keypress', function() {
                return false;
        });







                     //event listener for handling expired courses
                $('.disable-mycourse a:not(.institute)').on('click', function(e) {
                    e.preventDefault();
                    var instituteId = $(this).parents('.disable-mycourse').find('.institute').attr('data-instituteId');
                    var courseId = $(this).parents('.disable-mycourse').find('.course').attr('data-courseId');
                    $('#instituteId').val(instituteId);
                    $('#courseId').val(courseId);
                    $('#payYourself').attr('href', 'paymentDetails.php?courseId=' + courseId);
                    if($(this).parents('.disable-mycourse').attr('data-paymentStatus') == 0) {
                        //now removing self pay button and changing the text of other button
                        $('#payYourself').hide();
                        $('#showContact').text('Contact Institute/Professor for extending the course');
                        $('#payment .message').text('This course has expired. Contact respective Institute/Professor to extend this course.');
                    }
                    $('#payment').modal('show');
                });

            }
            
            function makeCoursesSortable(selector) {
                $(selector).sortable({
                    items: '> tr',
                    forcePlaceholderSize: true,
                    placeholder:'sort-placeholder',
                    start: function (event, ui) {
                        // Build a placeholder cell that spans all the cells in the row
                        var cellCount = 0;
                        $('td, th', ui.helper).each(function () {
                            // For each TD or TH try and get it's colspan attribute, and add that or 1 to the total
                            var colspan = 1;
                            var colspanAttr = $(this).attr('colspan');
                            if (colspanAttr > 1) {
                                colspan = colspanAttr;
                            }
                            cellCount += colspan;
                        });

                        // Add the placeholder UI - note that this is the item's content, so TD rather than TR
                        ui.placeholder.html('<td colspan="' + cellCount + '">&nbsp;</td>');
                        //$(this).attr('data-previndex', ui.item.index());
                    },
                    update: function(event, ui) {
                        // gets the new and old index then removes the temporary attribute
                        var newOrder = $.map($(this).find('tr'), function(el) {
                            return $(el).attr('data-cid')+ "-" +$(el).index();
                        });
                        if(reorderTimer != null)
                            clearTimeout(reorderTimer);
                        reorderTimer = setTimeout(function(){updateCourseOrder(newOrder);}, 5000);
                    },
                    helper: function(e, ui) {
                        ui.children().each(function() {
                            $(this).width($(this).width());
                        });
                        return ui;
                    }
                    
                }).disableSelection();
            }
            
           
           
            //fetchProfileDetails();
            fetchCourses();
           
            
        });

        function addEventHandlers() {

        /*



            $('#all-courses .delete-course').on('click', function(e) {
                e.preventDefault();
                var con = confirm("Are you sure to delete this course.");
                if(con) {
                    var course = $(this).parents('tr');
                    var cid = course.attr('data-cid');
                    var req = {};
                    req.action = "delete-course";
                    req.courseId = cid;
                    $.ajax({
                        'type'  : 'post',
                        'url'   : ApiEndpoint,
                        'data'  : JSON.stringify(req)
                    }).done(function (res) {
                        res =  $.parseJSON(res);
                        if(res.status == 1) {
                            course.remove();
                            alertMsg("Course Deleted. Please contact admin to restore this course.");
                        } else {
                            alertMsg(res.message);
                        }
                    });
                }
            });
            
            $('#all-courses .restore-course').on('click', function(e) {
                e.preventDefault();
                var course = $(this).parents('tr');
                var cid = course.attr('data-cid');
                var req = {};
                req.action = "restore-course";
                req.courseId = cid;
                $.ajax({
                    'type'  : 'post',
                    'url'   : ApiEndpoint,
                    'data'  : JSON.stringify(req)
                }).done(function (res) {
                    res =  $.parseJSON(res);
                    if(res.status == 1) {
                        course.find('.restore-course').hide();
                        course.find('.delete-course').show();
                    `    course.removeClass('deleted-item');
                    } else {
                        alertMsg(res.message);
                    }
                });
            });

            $('#all-courses .request-approval-link').on('click', function(e) {
                e.preventDefault();
                var req = {};
                var course = $(this).parents('tr');
                var cid = course.attr('data-cid');
                req.courseId = cid;
                req.action = "request-course-approval";
                $.ajax({
                    'type'  : 'post',
                    'url'   : ApiEndpoint,
                    'data'  : JSON.stringify(req)
                }).done(function (res) {
                    res =  $.parseJSON(res);
                    if(res.status == 1) {
                        alertMsg(res.message);
                        $('.request-approval-link').unbind('click').find('span').removeClass('btn-success').removeClass('btn-danger').addClass('btn-warning')
                            .html('Pending');
                        
                    } else {
                        alertMsg(res.message);
                    }
                });
            });

            $('#discount').on('click', function() {
                 console.log("abc");
                alert('button clicked');
                if($('#totalkeys').val() != '' && parseInt($('#totalkeys').val()) > 0) {
                    $('#Purchase_More').modal('hide');
                    var keys = $('#totalkeys').val();
                    var amount = keys * key_rate;
                    //$('#totalkeys').val('');
                    $('#purchaseNowModal').modal('show');
                    $('#purchaseNowModal .amount').html(display_current_currency + amount);
                    $('#purchaseNowModal .price-mul').html(keys + ' * ' + display_current_currency + key_rate);
                }else {
                    setError($('#totalkeys'), 'Invalid entry')
                }
            });


        */
        }

