var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var pacCat = ['All Own Courses', 'Courses From Category', 'Custom Course'];
var pactype = ['Market Place', 'Student will pay', 'Institute will pay'];
var error=0;
var packageEdit=[];
var subjectProfessors =[];
var packagedata= [];  
var packagedatai=null;
var packend=[];
var packendi=null;
var courseId=[];
var courseIdi=null;
var previousId=null;
var coursesArray=[];
var studentArray=[];
var uploadimg=0;
var imgpath=""; 
var category=0;
var catID=0;
var price=0;
var priceINR=0;
$(function() {
	getPackages();

	/*
	Function : to get the conetnt of the view package information of the  view_editPackage.php 
	req.packId  is the packageId extracted from the url
	*/

	function getPackages() {
		var req = {};
		var res;
		req.packId=getUrlParameter('editpackage');
		req.action = 'view-package';
		$.ajax({
			'type': 'post',
			'url': ApiEndPoint,
			'data': JSON.stringify(req)
		}).done(function (res) {

			res = $.parseJSON(res);
			if (res.status == 0)
			{	//alert(res.message);
				$('#warning').show();
			}
			else {
				fillPackages(res);
			}
		});
	}

	/*
	Function : to fill the conetnt of the view package information of the  view_editPackage.php 
	*/
	function fillPackages(data) {
		var subjectHtml = '', html = '';
		var courseStartDate = '';
		$.each(data.packages, function (i, packages) {
			packageEdit[i]=packages;
			var startdate = new Date(parseInt(packages.startDate));
			var enddate = new Date(parseInt(packages.endDate));
			$('#package-image-preview').attr("src",packages.image);
			$('#pack-name').html(' '+packages.packName);
			$('#pacD').html(' '+packages.packDescription);
			$('#pactype').html(' '+pactype[packages.packType-1]);
			$('#paccat').html(' '+pacCat[packages.packCategory-2]);
			$('#price').html(' <i class="fa fa-dollar"></i> '+packages.price);
			$('#priceINR').html('<i class="fa fa-inr"></i>  '+packages.priceINR);
			$('#startdate').html(' '+startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear());
			$('#endDate').html(' '+enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear());
			$.each(packages.course, function (i, course) {
				$('#packCourses').append(' '+'<a href="courses.php">'+course.name +' '+'</a>' +' ');
			});
		});
	}
	
	/*
	Function : to fill the conetnt of the edit package information of the  view_editPackage.php 
	*/
	function fillPackagesForm() {
	
		$('#package-name').val(' '+packageEdit[0]['packName']);
		$('#package-description').text(' '+packageEdit[0]['packDescription']);
		var startdate = new Date(parseInt(packageEdit[0]['startDate']));
		var enddate = new Date(parseInt(packageEdit[0]['endDate']));
		$('#pstartDate').val(' '+startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear()+' '+startdate.getHours()+':'+ startdate.getMinutes());
		$('#pendDate').val(' '+enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear()+' '+enddate.getHours()+':'+enddate.getMinutes());
		uploadimg=1;
		imgpath=packageEdit[0]['image1'];
		category=packageEdit[0]['packCategory'];
		catID=packageEdit[0]['catID'];
		price=packageEdit[0]['price'];
		priceINR=packageEdit[0]['priceINR'];
		if(packageEdit[0]['packType']==1)
		{
			$('#marketplace').prop("checked", true);
			$('.mprice').val(price);
			$('.mpriceINR').val(priceINR);
			$('#marketselect').show();
			$('#internalselect').hide();
			$('#paynow-savepack').hide();
			$('#savepack').show();
		}
		else if(packageEdit[0]['packType'] ==2)
		{
			fetchStudent();
			$('#internal').prop("checked", true);
			$('#internalselect').show();	
			$('#stupay').prop("checked", true);
			$('.mprice').val(price);
			$('.mpriceINR').val(priceINR);
			$('#marketselect').show();
			$('#paynow-savepack').hide();
			$('#savepack').show();
		}
		else{
			fetchStudent();
			$('#internal').prop("checked", true);
			$('#internalselect').show();
			$('#paynow-savepack').show();
			$('#savepack').hide();
			$('#inspay').prop("checked", true);
		}

		if(packageEdit[0]['packCategory']==4){
			fetchEditOwnerCourses();
			$('#allcourses').hide();
			$('#catCourses').hide();
			$('#custom-course').hide();
			$('#catSelect').show();
			$('#catSelect2').show();	
			//$('#inspay').prop("checked", true);
		}
		else{
			$('#packCat').hide();
			$('#allcourses').hide();
			$('#catCourses').hide();
			$('#custom-course').hide();
		}
	}

	/*
	Function : To check if new name is avialable in edit package information of the view_editPackage.php 
	*/
	
	function nameAvailable() {
		if($('#package-name').val().trim().length != '') {
			var req = {};
			var res;
			req.action = 'check-name-for-package';
			req.name = $('#package-name').val().trim();
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				console.log(res);
				if(res.status == 0)
					alert(res.message);
				else {
					unsetError($('#package-name'));
					if(!res.available){
						setError($('#package-name'),'Please select a different name.This name is already used');
						error=1;
					}
					
				}
			});
		}
	}
	
	/*
	Function : to get the Courses  of the  owner in edit-package information of the  view_editPackage.php 	
	*/
	function fetchEditOwnerCourses() {
		console.log("111fillInstituteProfessorscheck");
		var req = {};
		var res;
		req.packId=getUrlParameter('editpackage');
		req.action = "getEditcoursesByowner";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			console.log(res);
			res = $.parseJSON(res);
			if(res.status == 1) {
				fillOwnerCourses(res);
				//$('#manageProfessorSection').show();
			}
			else if(res.status == 0)
				alertMsg(res.message);
		});
	}
	function fillOwnerCourses(data) {
		console.log("fillOwnerCourses");
		var option = "";
		var selectElem = '#courses-list';
		var select = $(selectElem);
		var tempProf = {};
		$.each(data.courses, function(i,v) {
			var pName = v.name;
			option = "<option value='" + v.id + "'>" + pName + "</option>";
			select.append(option);
			tempProf[v.professorId] = pName;
		});
		applyMultiselectForCourses(selectElem);
	}				
	function applyMultiselectForCourses(selector) {		
		$(selector).multiSelect({
			afterSelect: function(value) {
				//alert("Select value: "+values);
				var index = coursesArray.indexOf(value[0]);
				if(index === -1)
					coursesArray.push(value[0]);
			},
			afterDeselect: function(value) {
				var index = coursesArray.indexOf(value[0]);
				coursesArray.splice(index, 1);
			}
		});
	}
	
	/*
	Function : to get the Students  of the  owner in edit-package information of the  view_editPackage.php 	
	*/

	function fetchStudent() {
		var req = {};
		var res;
		req.packId=getUrlParameter('editpackage');
		req.action = "geteditstudent-package";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			//console.log(res);
			res =  $.parseJSON(res);
			if(res.status == 1) {
				fillStudent(res);
				//$('#manageProfessorSection').show();
			}
			else if(res.status == 0)
				alertMsg(res.message);
		});
	}
	
	function fillStudent(data) {
		var option = "";
		var selectElem = '#students-list';
		var select = $(selectElem);
		var tempProf = {};
		$.each(data.students, function(i,v) {
			var selected = '';
			$.each(data.pstudents, function(j,w) {
				if (v.id == w.id) {
					selected = "selected";
				}
			});
			var pName = v.name;
			option = "<option value='" + v.id + "' "+selected+">" + pName + "</option>";
			select.append(option);
			tempProf[v.professorId] = pName;
		});
		applyMultiselectForStudent(selectElem);
		$('#noof-student').text(studentArray.length);
	}
	function applyMultiselectForStudent(selector) {	
		$(selector).multiSelect({
			afterSelect: function(value) {
				//alert("Select value: "+values);
				var index = studentArray.indexOf(value[0]);
				if(index === -1)
					studentArray.push(value[0]);
				$('#noof-student').text(studentArray.length);
				console.log(studentArray.length);
			},
			afterDeselect: function(value) {
				var index = studentArray.indexOf(value[0]);
				studentArray.splice(index, 1);
				$('#noof-student').text(studentArray.length);
			}
		});
	}		
															
																													
	/*
		Function : Event handlers for edit package information of the  view_editPackage.php 
	*/
	$('#package-details .edit-button').click(function () {
		fillPackagesForm();
		$('#package-details').hide();
		$('#edit-package').fadeIn('fast');
	});

	$('.cancel-edit-button').click(function () {
		$('.has-error').each(function() {
			$(this).removeClass('has-error');
		});
		$('.help-block').remove();
		$('#edit-package').hide();
		$('#package-details').fadeIn('fast');
	});
	
	$('#pstartDate').datetimepicker({
					format: 'd F Y H:i',
					minDate: 0,
					maxDate: '2050/12/31',
					step: 30,
					onSelectTime: function(date) {
						unsetError($('#startDate'));
						if (date.valueOf() > tempEnd.valueOf()) {
							setError($('#startDate'), 'The start date can not be greater than the end date.');
						} else {
							tempStart = date;
						}
					}
	});
	$('#pendDate').datetimepicker({
					format: 'd F Y H:i',
					minDate: 0,
					maxDate: '2050/12/31',
					step: 30,
					onSelectTime: function(date) {
						unsetError($('#endDate'));
						if (date.valueOf() < tempStart.valueOf()) {
							setError($('#pendDate'), 'The end date can not be less than the start date.');
						} else {
							tempEnd = date;
						}
					}
	});
	$('#pstartDate').bind("cut copy paste",function(e) {
		e.preventDefault();
	});
	$('#pendDate').bind("cut copy paste",function(e) {
		e.preventDefault();
	});

	$('#package-image-form').attr('action', fileApiEndpoint);

	$('#package-image-form').on('submit', function(e) {
		e.preventDefault();
		c = jcrop_api.tellSelect();
		crop = [c.x, c.y, c.w, c.h];
		imageA = document.getElementById('image-preview');
		var sources = {
			'package-image': {
				image: imageA,
				type: 'image/jpeg',
				crop: crop,
				/*size: [634, 150],*/
				quality: 1.0
			}
		}
		//settings for $.ajax function
		var settings = {
			url: fileApiEndpoint,
			data: {'packId': 3, 'x': $('#x').val(),'y': $('#y').val(),'w': $('#w').val(),'h': $('#h').val()}, //three fields (medium, small, text) to upload
			beforeSend: function()
			{
				console.log(settings);
				//console.log('sending image');
			},
			complete: function (resp) {
				console.log(resp);
				$('#upload-image').modal('hide');
				res = $.parseJSON(resp.responseText);
				if(res.status == 1)
				{
					uploadimg=1;
					imgpath=res.imageName;
					$('#imgPack').parent().append("Image Uploaded Sucessfully !!! ");
				}
				//alertMsg(res.message);
				$('#package-image-form .jcrop-holder').hide();
				$('#package-image-form input[type="file"]').val('');
				
			}
		}
		cropUploadAPI.cropUpload(sources, settings);
	});
	
	$("#package-image").change(function () {
		console.log("inside 2");
		$('.help-block').remove();
		//$('#edit-form .save-button').prop('disabled',false);
		courseImageChanged = true;
		var _URL = window.URL || window.webkitURL;
		var img = new Image();
		var file = this;
		img.onload = function() {
			if(!(img.width < 280 || img.height < 200))
				readURL(file);
			else {
				$('#package-image-form input[type=submit]').prop('disabled',true);
				$('#package-image-form .error').remove();
				$('#package-image-form').append('<span style="color:red;" class="error">Please select a larger image than 280 x 200.</span>');
			}
		}
		img.src = _URL.createObjectURL(this.files[0]);
    });
	
	$('#category-course').on('click', function() {
		$('#catSelect').show();
		$('#catSelect1').show();
		$('#catSelect2').hide();
	});

	$('#custom-course').on('click', function() {
		fetchOwnerCourses();
		$('#catSelect1').hide();
		$('#catSelect').show();
		$('#catSelect2').show();
	});
	
	$('#allowncourses').on('click', function() {
		$('#catSelect1').hide();
		$('#catSelect2').hide();
		$('#catSelect').hide();
	});
	
	$('#allarcanemindcourses').on('click', function() {
		$('#catSelect1').hide();
		$('#catSelect2').hide();
		$('#catSelect').hide();
	});
	
	$('#internal').on('click', function() {
		fetchStudent();
		$('#marketselect').hide();
		$('#internalselect').show();
		$('#stupay').prop("checked", true);
		$('#marketselect').show();
	});
	
	$('#marketplace').on('click', function() {
		$('#internalselect').hide();
		$('#marketselect').show();
		$('#paynow-savepack').hide();
		$('#savepack').show();
	});	
	
	$('#inspay').on('click', function() {
		fetchStudent();
		$('#marketselect').hide();
		$('#student-pay').show();
		$('#select-students').show();
		$('#savepack').hide();
		$('#paynow-savepack').show();
				
	});
	$('#stupay').on('click', function() {
		fetchStudent();
		$('#student-pay').hide();
		$('#marketselect').show();
		$('#select-students').show();
		$('#paynow-savepack').hide();
		$('#savepack').show();
	});
	$('#pstartDate').on('keypress', function() {
		return false;
	});
	$('#pendDate').on('keypress', function() {
		return false;
	});
	$('#noof-student').on('keypress', function() {
		return false;
	});
	$('#package-name').on('blur', function() {
		unsetError($(this));
		if(min($(this), 3)) {
			if(!max($(this), 50))
				setError($(this), "Please give a shorter package name. Maximum allowed limit is 50 characters.");
		}
		else
			setError($(this), "Please give a longer package name. Minimum allowed limit is 3 characters.");

		if($('#package-name').val().trim()!=(packageEdit[0]['packName']))
		{	console.log("yes");
			nameAvailable();
		}
	});
	$('#price').on('blur', function() {
		unsetError($(this));
	});
	$('#priceINR').on('blur', function() {
		unsetError($(this));
	});
	$('#package-description').on('blur', function() {
		unsetError($(this));
		if(min($(this), 3)) {
			if(!max($(this), 1000))
				setError($(this), "Please give a shorter Description.");
		}
		else
			setError($(this), "Please give a longer Description name. Minimum allowed limit is 3 characters.");
	});
	
	/*
	this event handler is called if  Package type = Market Place or Student will pay  then owner dont need to pay for package
	so just save button is visible and save-pay-button gets hide.
	*/
	$('.save-button').on('click', function() {
		if(error == 0){
			var ierror		= 0;
			var pacType		= 0;
			var paytype		= 0;
			var pacType		= 0;
			var courseCateg	= catID;
			elem			= $(this);
			var packName=$('#package-name').val();
			if(packName == '' || packName==null )
			{
				ierror=1;
				setError($('#package-name'), "Please Enter Package Name here.");
				return;
			}
			var packageDesc=$('#package-description').val();
			if(packageDesc == '' ||packageDesc==null )
			{
				ierror=1;
				setError($('#package-description'), "Please Enter Package Description here.");
				return;
			}
			if($('#pstartDate').val() == '' )
			{
				ierror=1;
				unsetError($('#pstartDate'));
				unsetError($('#pendDate'));
				$('#startDate').parent().addClass('has-error').append('<span class="help-block">Please Enter Start Date  </span>');
				return false;
			}
			if($('#pendDate').val() == '' )
			{
				ierror=1;
				unsetError($('#pstartDate'));
				unsetError($('#pendDate'));
				$('#endDate').parent().addClass('has-error').append('<span class="help-block">Please Enter End Date  </span>');
				return false;
			}

			var pstartDate = new Date($('#pstartDate').val());
			var pendDate = new Date($('#pendDate').val());		
			if(pstartDate > pendDate || !(pstartDate < pendDate) )
			{
				ierror=1;
				unsetError($('#pstartDate'));
				unsetError($('#pendDate'));
				$('#pendDate').parent().addClass('has-error').append('<span class="help-block">End Date should be greater than Start Date  </span>');
				return false;
			}

			if($('#marketplace').prop('checked') || $('#stupay').prop('checked') ) {
				if($('#marketplace').prop('checked')  && $('.mprice').val().trim()!='' &&  $('.mpriceINR').val().trim()!='' && $.isNumeric($('.mpriceINR').val().trim()) && $.isNumeric($('.mprice').val().trim()) ){
					pacType=1;
					priceINR=$('.mpriceINR').val().trim();
					price=$('.mprice').val().trim();
				}					
				else if($('#stupay').prop('checked')  && $('.mprice').val().trim()!='' &&  $('.mpriceINR').val().trim()!='' && $.isNumeric($('.mpriceINR').val().trim()) && $.isNumeric($('.mprice').val().trim()) ){
					pacType=2;
					priceINR=$('.mpriceINR').val().trim();
					price=$('.mprice').val().trim();
				}
				else{
					ierror=1;
					setError($('#price'), "please enter value .");
					return;
				}																																																					
			}
			else {
				ierror=1;
				setError($('#pactype'), "Only marketplace Allowed.");
				console.log("error here");
				return;
			}
			if(uploadimg !=0  || imgpath !='')
			{}
			else{
				ierror=1;
				//$('#imgPack').parent().append("Image Uploaded Sucessfully !!! ");
				setError($('#imgPack'), "Please Upload Image here.");
			}

			if(pacType == 1)
			{
				studentArray=[];
			}
		
			console.log(category);
			console.log(courseCateg);
			if(ierror==0)
			{
				console.log("kcnc");
				var req = {};
				var res;
				req.action = 'edit-package';
				req.packId=getUrlParameter('editpackage');
				req.name = packName.trim();
				req.packDescription=packageDesc.trim();
				req.packType = pacType;
				req.packCategory=category;
				req.catID=courseCateg;
				req.image=imgpath.trim();
				req.price=price;
				req.priceINR=priceINR;
				req.startDate=pstartDate.getTime();
				req.endDate=pendDate.getTime();
				req.courseArray=coursesArray;
				req.studentArray=studentArray;
				console.log(req);
				$.ajax({
					'type'  : 'post',
					'url'   : ApiEndpoint,
					'data' 	: JSON.stringify(req)
				}).done(function (res) {
					res =  $.parseJSON(res);
					console.log(res);
					if(res.status == 0)
						alert(res.message);
					else {
						alert(res.message);
						window.location = 'packages_subs.php';
					}
				});
			}
		}
		else{
			setError($('.save-button'), "Please review your forms. It contains errors.");
			return false;
		}
	});
	/*
	this event handler is called if  Package type = Institute will pay  then owner  needs to pay for package
	so just save button is visible and save-pay-button gets hide.
	*/
	$('.save-pay-button').on('click', function() {
		if(error == 0){
			var ierror=0;
			var pacType=0;
			var paytype=0;
			var price=0;
			var priceINR=0;
			var pacType=0;
			var courseCateg=catID;
			elem = $(this);
			
			var packName=$('#package-name').val();
			if(packName == '' || packName==null )
			{
				ierror=1;
				setError($('#package-name'), "Please Enter Package Name here.");
				return;
			}
		
			var packageDesc=$('#package-description').val();
			if(packageDesc == '' ||packageDesc==null )
			{
				ierror=1;
				setError($('#package-description'), "Please Enter Package Description here.");
				return;
			}	
		 
			if($('#pstartDate').val() == '' )
			{
				ierror=1;
				unsetError($('#pstartDate'));
				unsetError($('#epndDate'));
				$('#pstartDate').parent().addClass('has-error').append('<span class="help-block">Please Enter Start Date  </span>');
				return false;
			}
			if($('#pendDate').val() == '' )
			{
				ierror=1;
				unsetError($('#pstartDate'));
				unsetError($('#pendDate'));
				$('#pendDate').parent().addClass('has-error').append('<span class="help-block">Please Enter End Date  </span>');
				return false;
			}

			var startDate = new Date($('#pstartDate').val());
			var endDate = new Date($('#pendDate').val());
		
			if(startDate > endDate || !(startDate < endDate) )
			{
				ierror=1;
				unsetError($('#pstartDate'));
				unsetError($('#pendDate'));
				$('#pendDate').parent().addClass('has-error').append('<span class="help-block">End Date should be greater than Start Date  </span>');
				return false;
			}

			console.log(packageDesc);
		
			if($('#internal').prop('checked') && $('#inspay').prop('checked') ) {
				pacType=3;																																										
			}
			else{			
				ierror=1;
				setError($('#pactype'), "Only please select Internal and Institute will pay option.");
				return;
			}
			if(uploadimg !=0  || imgpath !='')
			{
				console.log("uimhkkk");
			}
			else{
				ierror=1;
				//$('#imgPack').parent().append("Image Uploaded Sucessfully !!! ");
				setError($('#imgPack'), "Please Upload Image here.");
			}
		
			if(pacType == 1)
			{
				studentArray=[];
			}
			console.log(category);
			if(ierror==0)
			{
				console.log("kcnc");
				var req = {};
				var res;
				req.action = 'edit-package';
				req.packId=getUrlParameter('editpackage');
				req.name = packName.trim();
				req.packDescription=packageDesc.trim();
				req.packType = pacType;
				req.packCategory=category;
				req.catID=courseCateg;
				req.image=imgpath.trim();
				req.price=0;
				req.priceINR=0;
				req.startDate=startDate.getTime();
				req.endDate=endDate.getTime();
				req.courseArray=coursesArray;
				req.studentArray=studentArray;
				console.log(req);
				$.ajax({
					'type'  : 'post',
					'url'   : ApiEndpoint,
					'data' 	: JSON.stringify(req)
				}).done(function (res) {
					res =  $.parseJSON(res);
					console.log(res);
					if(res.status == 0)
						alert(res.message);
					else {
						window.location = 'createPackagePay.php?newcase='+ res.id;
						alert(res.message);
					}
				});
			}
		}
		else{
			setError($('.save-button'), "Please review your forms. It contains errors.");
			return false;
		}
	});
	
	function min(what, length) {
		if(what.val().length < length)
			return false;
		else
			return true;
	}
	
	function max(what, length) {
		if(what.val().length > length)
			return false;
		else
			return true;
	}
	
	function setError(where, what) {
		unsetError(where);
		where.parent().addClass('has-error');
		where.parent().append('<span class="help-block">'+what+'</span>');
	}
	
	function unsetError(where) {
		if(where.parent().hasClass('has-error')) {
			where.parent().find('.help-block').remove();
			where.parent().removeClass('has-error');
		}
	}
	
	function updateCoords(c) {
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
		var crop = [c.x, c.y, c.w, c.h];
	}
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src',data.profileDetails.profilePic);
		}
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function readURL(input) {

		if(input.files && input.files[0]) {
			if(!(input.files[0]['type'] == 'image/jpeg' || input.files[0]['type'] == 'image/png' || input.files[0]['type'] == 'image/gif')) {
				$('#package-image-form input[type=submit]').prop('disabled',true);
				$('#package-image-form .error').remove();
				$('#package-image-form').append('<span class="help-block" style="color:red;" class="error">Please select a proper file type.</span>');
				return;
			}else if(input.files[0]['size']>819200) {
				$('#package-image-form input[type=submit]').prop('disabled',true);
				$('#package-image-form .error').remove();
				$('#package-image-form').append('<span style="color:red;" class="error">Files larger than 800kb are not allowed.</span>');
				return;
			}else {
				$('#package-image-form .error').remove();
				$('#package-image-form input[type=submit]').prop('disabled',false);
			}
			var reader = new FileReader();
			if (typeof jcrop_api != 'undefined' && jcrop_api != null) {
				jcrop_api.destroy();
				jcrop_api = null;
				var pImage = $('.crop');
				pImage.css('height', 'auto');
				pImage.css('width', 'auto');
				var height = pImage.height();
				var width = pImage.width();
				$('.jcrop').width(width);
				$('.jcrop').height(height);
			}
			reader.onload = function (e) {
				$('#image-preview').attr('src', e.target.result);
				$('#upload-image .crop').Jcrop({
					onSelect: updateCoords,
					bgOpacity:   .4,
					boxWidth: 830,
					minSize: [280, 200],
					setSelect:   [0, 0, 280, 200],
					aspectRatio: 804/440
				}, function () {
					jcrop_api = this;
					jcrop_api.setSelect([0, 0, 280, 200]);
				});
			}
			reader.readAsDataURL(input.files[0]);
		}
	}

});