/*
 Author: Fraaz Hashmi.
 Dated: 7/2/2014
 
 
 ********
 1) Notes: JSON.stringify support for IE < 10
 */
var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var userRole;

$(function () {
    var imageRoot = '/';
    $('#loader_outer').hide();

    function fetchProfileDetails() {
        var req = {};
        var res;
        req.action = "get-profile-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillProfileDetails(res);
            console.log(res);
        });
    }

    function fillProfileDetails(data) {
        // General Details
        var imageRoot = 'user-data/images/';
        $('#user-profile-card .username').text(data.loginDetails.username);

        if (data.profileDetails.profilePic != "") {
            $('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
        }

        // Role Specific
        userRole = data.userRole;
        if (data.userRole == '1') {
            fillInstituteDetails(data);
        }
        else if (data.userRole == '2') {
            fillProfessorDetails(data);
        }
        else if (data.userRole == '3') {
            fillPublisherDetails(data);
        }
    }

    function fillInstituteDetails(data) {
        $('h4#profile-name').text(data.profileDetails.name);
    }

    function fillProfessorDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('a#profile-name').text(name);
    }

    function fillPublisherDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('a#profile-name').text(name);
    }

    function fetchExams() {
        var req = {};
        var res;
        req.action = "get-all-deleted-exams";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1)
                fillExams(res);
            else
                alertMsg(res.message);
        });
    }

    function fillExams(data) {
        var html = "";
        var editCourseLink, editSubjectLink, addSubjectLink;
        var institutenameId,coursenameId,subjectnameId,chapternameId;
        $.each(data.exams, function (i, exam) {
           
          //  editCourseLink = "backend_edit-course.php?courseId=" + course.id;
            coursenameId = exam.course;
            subjectnameId=exam.subject+' (S00'+exam.subjectid+')';
            institutenameId = exam.username+' ('+exam.instituteid+')';
            chapternameId=exam.chapter+' (CH00'+exam.chapterid+')';
           // chapternameId=''
            html += "<tr data-cid='" + exam.examid + "'>"
                    + "<td>" + exam.examid + "</td>"
                    + "<td>" +exam.examname+ "</td>"
                    + "<td>" +exam.examtype+ "</td>"
                    + "<td>" +chapternameId+ "</td>"
                    + "<td><h5><a href='#'>" + subjectnameId + "</a></h5>";
            html += "</td>";
            html += "<td>" + coursenameId + "</td>";
            html += "<td>" + institutenameId + "</td>";
            html += "<td class=small-txt>" + exam.email + "</td>";
            html += "<td>" + exam.contactMobile + "</td>"
                    + "<td><a href='#' class='restore'><span class='label label-success label-mini' style='display:block;'>Restore</span></a></td>"
                    + "</td></tr>";
    });
         
            $('#all-exams tbody').html(html);
//            $('#all-exams').dataTable({
//            "aaSorting": [[0, "asc"]]
//         });
            addApprovalHandlers();
       
       

    }
    //fetchProfileDetails();
    fetchExams();
});

function addApprovalHandlers() {
    $('#all-exams .restore').on('click', function (e) {
        e.preventDefault();
        var exam = $(this).parents('tr');
        var cid = exam.attr('data-cid');
        var req = {};
        req.action = "restore-exam-admin";
        req.examId = cid;
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
             res = $.parseJSON(res);
            if (res.status == 1) {
                exam.remove();
                // course.find('.approved').show();
            } else {
                alertMsg(res.message);
            }
        });
    });

}