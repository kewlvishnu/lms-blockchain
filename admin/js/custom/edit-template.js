var sortOrder = [];
$(function() {
	//fetchProfileDetails();
	fetchTemplateDetails();
	
	$('#orderRandom').on('click', function() {
		$('#sortOrder').slideUp();
	});
	
	$('#orderStart').on('click', function() {
		$('#sortOrder').find('li').remove();
		var html = '';
		for(var i=0; i<$('#sections').find('input[type="text"]').length; i++) {
			var value = $('#sections input[type="text"]:eq('+i+')').val();
			html += '<li data-original="'+i+'" class="sort-li">' + value + '</li>';
			sortOrder[i] = i+'';
		}
		$('#sortOrder').append(html);
		$('#sortOrder').slideDown();
	});
	
	$('#sortOrder').sortable({revert : true, 
							placeholder : 'sort-placeholder',
							cursor : 'move',
							forcePlaceholderSize : true,
							tolerance : 'pointer',
							opacity : 0.6,
							update : function() {
								for(var i=0; i< $('#sortOrder').find('li').length; i++) {
									sortOrder[i] = $('#sortOrder li:eq('+i+')').attr('data-original');
								}
							}
						});
	
	$('#addSection').on('click', function() {
		$('#tempSection').parent().removeClass('has-error');
		$('#tempSection').next().remove();
		if($('#tempSection').val() != '') {
			var html='<div class="form-group">'
						+ '<div class="row">'
							+ '<div class="col-lg-9">'
								+ '<input type="text" class="form-control" value="'+$('#tempSection').val()+'">'
							+ '</div>'
							+ '<div class="col-lg-3">'
								+ '<button class="btn btn-danger btn-xs delete-button" type="button"><i class="fa fa-trash-o "></i></button>'
							+ '</div>'
						+ '</div>'
					+ '</div>';
			$('#sections').append(html);
			$('#tempSection').val('');
			$('.delete-button').on('click', function() {
				$(this).parents('.form-group').remove();
				$('#sectionTimes .row:eq(0)').find('*').remove();
				$('#switchNo').click();
			});
		}
		else {
			$('#tempSection').parent().addClass('has-error');
			$('#tempSection').parent().append('<span class="help-block">Please provide a section name.</span>');
		}
	});
	
	$('#switchYes').on('click', function() {
		unsetError($('#totalTimeHour').parent());
		unsetError($('#sectionTimes .row:eq(0)'));
		unsetError($('#orderStart').parent());
		$('#sectionTimes').hide();
		$('#totalTime').show();
	});
	
	$('#switchNo').on('click', function() {
		unsetError($('#totalTimeHour').parent());
		unsetError($('#orderStart').parent());
		unsetError($('#sectionTimes .row:eq(0)'));
		$('#totalTime').hide();
		var html = '';
		$('#sectionTimes .row:eq(0)').find('*').remove();
		if($('#sections').find('input').length == 0) {
			html = '<span style="color: red;">Please Add a section first.</span>';
		}
		for(i=0; i<$('#sections').find('input').length; i++) {
			html += '<div class="col-lg-6">'
					+ '<label for="coursename">Total Time for '+$('#sections').find('input:eq('+i+')').val()+'</label><br>'
					+ '<div class="col-md-4">'
						+ '<div class="input-group input-small">'
							+ '<input type="text" maxlength="3" class="spinner-input form-control time" value="0h" disabled=true>'
							+ '<div class="spinner-buttons input-group-btn btn-group-vertical">'
								+ '<button type="button" class="btn spinner-up btn-xs btn-default generated">'
									+ '<i class="fa fa-angle-up"></i>'
								+ '</button>'
								+ '<button type="button" class="btn spinner-down btn-xs btn-default generated">'
									+ '<i class="fa fa-angle-down"></i>'
								+ '</button>'
							+ '</div>'
						+ '</div>'
					+ '</div> &nbsp;&nbsp;&nbsp;&nbsp;'
					+ '<div class="col-md-4">'
						+ '<div class="input-group input-small">'
							+ '<input type="text" class="spinner-input form-control time" maxlength="3" value="0m" disabled=true>'
							+ '<div class="spinner-buttons input-group-btn btn-group-vertical">'
								+ '<button class="btn spinner-up btn-xs btn-default generated" type="button">'
									+ '<i class="fa fa-angle-up"></i>'
								+ '</button>'
								+ '<button class="btn spinner-down btn-xs btn-default generated" type="button">'
									+ '<i class="fa fa-angle-down"></i>'
								+ '</button>'
							+ '</div>'
						+ '</div>'
					+ '</div>'
				+ '</div>';
		}
		$('#sectionTimes .row:eq(0)').append(html);
		$('#sectionTimes').show();
		$('.spinner-up.generated').on('click', function() {
			var input = $(this).parents('.input-group').find('input');
			var value = input.val();
			var postfix = value.substring(value.length-1);
			var num = value.substring(0, value.length-1);
			num++;
			input.val(num+postfix);
		});
		$('.spinner-down.generated').on('click', function() {
			var input = $(this).parents('.input-group').find('input');
			var value = input.val();
			var postfix = value.substring(value.length-1);
			var num = value.substring(0, value.length-1);
			if(num-1 >= 0)
				num--;
			input.val(num+postfix);
		});
	});
	
	$('.spinner-up').on('click', function() {
		var input = $(this).parents('.input-group').find('input');
		var value = input.val();
		var postfix = value.substring(value.length-1);
		var num = value.substring(0, value.length-1);
		num++;
		input.val(num+postfix);
	});
	
	$('.spinner-down').on('click', function() {
		var input = $(this).parents('.input-group').find('input');
		var value = input.val();
		var postfix = value.substring(value.length-1);
		var num = value.substring(0, value.length-1);
		if(num-1 >= 0)
			num--;
		input.val(num+postfix);
	});
		
	$('#titleName').on('blur', function() {
		unsetError($(this));
		if(min($(this), 3)) {
			if(!max($(this), 50))
				setError($(this), "Please give a shorter Template name. Maximum allowed limit is 50 characters.");
		}
		else
			setError($(this), "Please give a longer Template name. Minimum allowed limit is 3 characters.");
		nameAvailable();
	});
	
	$('.save-button').on('click', function() {
		$('#titleName').blur();
		unsetError($('#noOfAttempts'));
		if($('#noOfAttempts').val() != '') {
			unsetError($('#examCat').parent());
			if($('#assignmentCat').prop('checked') || $('#examCat').prop('checked')) {
				unsetError($('#tempSection').next());
				if($('#sections').find('input').length != 0) {	
					unsetError($('#switchYes').parent());
					if($('#switchYes').prop('checked') || $('#switchNo').prop('checked')) {
						unsetError($('#endYes').parent());
						if($('#endYes').prop('checked') || $('#endNo').prop('checked')) {
							unsetError($('#noTimeLost').parent());
							if($('#noTimeLost').prop('checked') || $('#timeLost').prop('checked')) {
								unsetError($('.save-button'));
								unsetError($('#totalTimeHour').parent());
								unsetError($('#sectionTimes .row:eq(0)'));
								unsetError($('#orderStart').parent());
								if($('.has-error').length == 0 ) {
									var req = {};
									req.action = 'edit-template';
									req.templateId = getUrlParameter('templateId');
									req.name = $('#titleName').val();
									if($('#assignmentCat').prop('checked'))
										req.type = 'Assignment';
									else if($('#examCat').prop('checked'))
										req.type = 'Exam';
									if($('#chapterSelect').val() == -1)
										req.chapterId = 0;
									else
										req.chapterId = $('#chapterSelect').val();
									req.subjectId = getUrlParameter('subjectId');
									req.status = 0;
									req.attempts = $('#noOfAttempts').val();
									if($('#switchYes').prop('checked'))
										req.tt_status = 0;
									else if($('#switchNo').prop('checked'))
										req.tt_status = 1;
									req.totalTime = 6000;
									req.gapTime = 0;
									req.time = [];
									if(req.tt_status == 0) {
										req.sectionOrder = 2;
										var tth = $('#totalTimeHour').val();
										tth = tth.substring(0, tth.length-1);
										tth = parseInt(tth);
										var ttm = $('#totalTimeMinute').val();
										ttm = ttm.substring(0, ttm.length-1);
										ttm = parseInt(ttm);
										req.totalTime = (tth * 60) + ttm;
										if(req.totalTime <= 0) {
											setError($('#totalTimeHour').parent(), "Please specify a correct time limit.");
											return;
										}
									}
									if(req.tt_status == 1) {
										var gap = $('#gapTime').val();
										req.gapTime = gap.substring(0, gap.length-1);
										var sectionTimes = $('#sectionTimes').find('input.time');
										for(i=0; i<sectionTimes.length; i = i + 2) {
											var sth = sectionTimes[i]['value'];
											sth = sth.substring(0, sth.length-1);
											sth = parseInt(sth);
											var stm = sectionTimes[i+1]['value'];
											stm = stm.substring(0, stm.length-1);
											stm = parseInt(stm);
											var st = (sth * 60) +stm;
											if(st <= 0) {
												setError($('#sectionTimes .row:eq(0)'), "Please select correct timings for every section.");
												return;
											}
											req.time.push(st);
										}
										if(!($('#orderStart').prop('checked') || $('#orderRandom').prop('checked'))) {
											setError($('#orderStart').parent(), "Please select ordering of the sections.");
											return;
										}
									}
									req.powerOption = 0;
									if($('#endYes').prop('checked'))
										req.endBeforeTime = 1;
									else if($('#endNo').prop('checked'))
										req.endBeforeTime = 0;
									if($('#noTimeLost').prop('checked'))
										req.powerOption = 0;
									else if($('#timeLost').prop('checked'))
										req.powerOption = 1;
									req.sections = [];
									req.weights = [];
									var sections = $('#sections').find('input');
									for(i=0; i<sections.length; i++) {
										req.sections.push(sections[i]['value']);
										req.weights.push(sortOrder.indexOf(i+''));
									}
									if(req.tt_status == 0) {
										for(i=0; i<sections.length; i++) {
											req.time.push(0);
										}
									}
									if($('#orderStart').prop('checked'))
										req.sectionOrder = 0;
									else if($('#orderRandom').prop('checked'))
										req.sectionOrder = 1;
									$.ajax({
										'type'  : 'post',
										'url'   : ApiEndpoint,
										'data' 	: JSON.stringify(req)
									}).done(function (res) {
										res =  $.parseJSON(res);
										if(res.status == 1) {
											window.location = 'template.php';
										}
										else
											alert(res.message);
									});
								}
								else
									setError($('.save-button'), "Please review your forms. It contains errors.");
							}
							else
								setError($('#noTimeLost').parent(), "Please select an option to continue.");
						}
						else
							setError($('#endYes').parent(), "Please select one option.");
					}
					else
						setError($('#switchYes').parent(), "Please select a option to switch between sections or not.");
				}
				else
					setError($('#tempSection'), "Please add atleast one section.");
			}
			else
				setError($('#examCat').parent(), "Please select a test type.")
		}
		else
			setError($('#noOfAttempts'), "Please specify number of attempts allowed.");
	});
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fetchTemplateDetails() {
		var req = {};
		var res;
		req.action = 'get-template-details';
		req.templateId = getUrlParameter('templateId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
				fillTemplateDetails(res);
		});
	}
	
	function fillTemplateDetails(data) {
		$('#titleName').val(data.template.name);
		$('#noOfAttempts').val(data.template.attempts);
		if(data.template.type == 'Assignment')
			$('#assignmentCat').prop('checked', true);
		else if(data.template.type == 'Exam')
			$('#examCat').prop('checked', true);
		if(data.template.endBeforeTime == 1)
			$('#endYes').prop('checked', true);
		else
			$('#endNo').prop('checked', true);
		if(data.template.powerOption == 0)
			$('#noTimeLost').prop('checked', true);
		else
			$('#timeLost').prop('checked', true);
		if(data.template.totalTime == 0) {
			$('#switchNo').prop('checked', true);
			$('#sectionTimes').show();
			$('#totalTime').hide();
			$('#gapTime').val(data.template.gapTime+'m');
			$('#sections .form-group').remove();
			$('#sectionTimes .row:eq(0)').find('*').remove();
			for(i=0; i<data.sections.length; i++) {
				var html='<div class="form-group">'
						+ '<div class="row">'
							+ '<div class="col-lg-9">'
								+ '<input type="text" class="form-control" value="'+data.sections[i].name+'" id="'+data.sections[i].id+'">'
							+ '</div>'
							+ '<div class="col-lg-3">'
								+ '<button class="btn btn-danger btn-xs delete-button" type="button" style="margin-left:5px;"><i class="fa fa-trash-o "></i></button>'
							+ '</div>'
						+ '</div>'
					+ '</div>';
				$('#sections').append(html);
				$('.delete-button').on('click', function() {
					$(this).parents('.form-group').remove();
					$('#sectionTimes .row:eq(0)').find('*').remove();
					$('#switchNo').click();
				});
				var htmlTime = '<div class="col-lg-6">'
					+ '<label for="coursename">Total Time for '+data.sections[i].name+'</label><br>'
					+ '<div class="col-md-4">'
						+ '<div class="input-group input-small">'
							+ '<input type="text" maxlength="3" class="spinner-input form-control time hour" value="0h" disabled=true>'
							+ '<div class="spinner-buttons input-group-btn btn-group-vertical">'
								+ '<button type="button" class="btn spinner-up btn-xs btn-default generated">'
									+ '<i class="fa fa-angle-up"></i>'
								+ '</button>'
								+ '<button type="button" class="btn spinner-down btn-xs btn-default generated">'
									+ '<i class="fa fa-angle-down"></i>'
								+ '</button>'
							+ '</div>'
						+ '</div>'
					+ '</div> &nbsp;&nbsp;&nbsp;&nbsp;'
					+ '<div class="col-md-4">'
						+ '<div class="input-group input-small">'
							+ '<input type="text" class="spinner-input form-control time minute" maxlength="3" value="0m" disabled=true>'
							+ '<div class="spinner-buttons input-group-btn btn-group-vertical">'
								+ '<button class="btn spinner-up btn-xs btn-default generated" type="button">'
									+ '<i class="fa fa-angle-up"></i>'
								+ '</button>'
								+ '<button class="btn spinner-down btn-xs btn-default generated" type="button">'
									+ '<i class="fa fa-angle-down"></i>'
								+ '</button>'
							+ '</div>'
						+ '</div>'
					+ '</div>'
				+ '</div>';
				$('#sectionTimes .row:eq(0)').append(htmlTime);
				hour = data.sections[i].time / 60;
				minute = data.sections[i].time - (hour * 60);
				$('#sectionTimes input[type="text"].hour:eq('+i+')').val(hour+'h');
				$('#sectionTimes input[type="text"].minute:eq('+i+')').val(minute+'m');
			}
		}
		else {
			$('#sections .form-group').remove();
			$('#sectionTimes .row:eq(0)').find('*').remove();
			for(i=0; i<data.sections.length; i++) {
				var html='<div class="form-group">'
						+ '<div class="row">'
							+ '<div class="col-lg-9">'
								+ '<input type="text" class="form-control" value="'+data.sections[i].name+'" id="'+data.sections[i].id+'">'
							+ '</div>'
							+ '<div class="col-lg-3">'
								+ '<button class="btn btn-danger btn-xs delete-button" type="button" style="margin-left:5px;"><i class="fa fa-trash-o "></i></button>'
							+ '</div>'
						+ '</div>'
					+ '</div>';
				$('#sections').append(html);
				$('.delete-button').on('click', function() {
					$(this).parents('.form-group').remove();
					$('#sectionTimes .row:eq(0)').find('*').remove();
					$('#switchNo').click();
				});
			}
			$('#switchYes').prop('checked', true);
			$('#sectionTimes').hide();
			$('#totalTime').show();
			hour = data.template.totalTime / 60;
			minute = data.template.totalTime - (hour * 60);
			$('#totalTimeHour').val(hour+'h');
			$('#totalTimeMinute').val(minute+'m')
		}
		if(data.template.sectionOrder == 0) {
			$('#orderStart').prop('checked', true);
			$('#sortOrder').find('li').remove();
			var htm = '';
			for(var a=0; a<$('#sections').find('input[type="text"]').length; a++) {
				var value = $('#sections input[type="text"]:eq('+a+')').val();
				htm += '<li data-original="'+a+'">' + value + '</li>';
				sortOrder[a] = a+'';
			}
			$('#sortOrder').append(htm);
			$('#sortOrder').show();
		}
		else
			$('#orderRandom').prop('checked', true);
	}
	
	function min(what, length) {
		if(what.val().length < length)
			return false;
		else
			return true;
	}
	
	function max(what, length) {
		if(what.val().length > length)
			return false;
		else
			return true;
	}
	
	function setError(where, what) {
		unsetError(where);
		where.parent().addClass('has-error');
		where.parent().append('<span class="help-block">'+what+'</span>');
	}
	
	function unsetError(where) {
		if(where.parent().hasClass('has-error')) {
			where.parent().find('.help-block').remove();
			where.parent().removeClass('has-error');
		}
	}
	
	function nameAvailable() {
		if($('#titleName').val().length != '' && ($('#examCat').prop('checked') || $('#assignmentCat').prop('checked'))) {
			var req = {};
			var res;
			req.action = 'check-name-for-template';
			req.name = $('#titleName').val();
			req.templateId = getUrlParameter('templateId');
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else {
					unsetError($('#titleName'));
					if(!res.available)
						setError($('#titleName'),'Please select a different name as you have already used this name.');
				}
			});
		}
	}
});