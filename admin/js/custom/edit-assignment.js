var sortOrder = [];
var tempStart = new Date();
var tempEnd = new Date('2099/12/31');
$(function() {
	fetchBreadcrumb();
	//fetchProfileDetails();
	fetchCourseDates();
	fetchChapters();

	function fetchChapters() {
		var req = {};
		req.action = "get-chapters-for-exam";
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillChaptersSelect(res);
		});
	}
	
	function fillChaptersSelect(data) {
		if(data.status == 0) {
			alertMsg(data.message);
			return;
		}
		var opts = '';
		for(i=0; i<data.chapters.length; i++) {
			opts += '<option value="'+data.chapters[i]['id']+'">'+data.chapters[i]['name']+'</option>';
		}
		opts += '<option value="-1">Independent</option>';
		$('#chapterSelect').append(opts);
		fetchExamDetails();
	}
	
	//disabling user input in dates
	$('#settime').on('keypress', function() {
		return false;
	});
	
	$('#custom-time').on('click', function() {
		$('#set-exam-date').show();
	});
	
	$('#course-end').on('click', function() {
		$('#set-exam-date').hide();
	});	
				
	$('#startDate, #endDate').on('keypress', function() {
		return false;
	});
	
	$('#tempSection').on('blur', function() {
		if($(this).val() != '') {
			$('#addSection').click();
		}
	});
	
	$('#sections input[type="text"]').on('blur', function() {
		if($('#orderStart').prop('checked')) {
			$('#orderStart').click();
		} else if($('#orderRandom').prop('checked')) {
			$('#orderRandom').click();
		}
	});
	
	//disabling user input in dates
	$('#startDate, #endDate').on('keypress', function() {
		return false;
	});
	
	$('#orderRandom').on('click', function() {
		$('#sortOrder').slideUp();
	});
	
	$('#orderStart').on('click', function() {
		$('#sortOrder').find('li').remove();
		var html = '';
		for(var i=0; i<$('#sections').find('input[type="text"]').length; i++) {
			var value = $('#sections input[type="text"]:eq('+i+')').val();
			html += '<li data-original="'+i+'" class="sort-li">' + value + '</li>';
			sortOrder[i] = i+'';
		}
		$('#sortOrder').append(html);
		$('#sortOrder').slideDown();
	});
	
	$('#sortOrder').sortable({revert : true, 
		placeholder : 'sort-placeholder',
		cursor : 'move',
		forcePlaceholderSize : true,
		tolerance : 'pointer',
		opacity : 0.6,
		update : function() {
			for(var i=0; i< $('#sortOrder').find('li').length; i++) {
				sortOrder[i] = $('#sortOrder li:eq('+i+')').attr('data-original');
			}
		}
	});
	
	$('#titleName').on('blur', function() {
		unsetError($(this));
		if(min($(this), 3)) {
			if(!max($(this), 50))
				setError($(this), "Please give a shorter assignment/exam name. Maximum allowed limit is 50 characters.");
		}
		else
			setError($(this), "Please give a longer assignment/exam name. Minimum allowed limit is 3 characters.");
		nameAvailable();
	});
	
	$('.save-button').on('click', function() {
		elem = $(this);
		$('#titleName').blur();
		unsetError($('#noOfAttempts'));
		if($('#noOfAttempts').val() != '') {
			unsetError($('#examCat').parent());
			if($('#assignmentCat').prop('checked') || $('#examCat').prop('checked')) {
				unsetError($('#tempSection').next());
				if($('#sections').find('input[type="text"]').length != 0) {
					unsetError($('#startDate'));
					if($('#startDate').val() != '') {
						unsetError($('#endDate'));
						if(($('#endDate').val() == '' && $('#courseEndDate').val() == '') || $('#endDate').val() != '') {
							unsetError($('#switchYes').parent());
						if($('#course-end').prop('checked') || ($('#custom-time').prop('checked')  && $('#settime').val() != '')) {
									unsetError($('#course-end').parent());

							if($('#immediately').prop('checked') || $('#after-examend').prop('checked')  || !$('#examCat').prop('checked')) {
									unsetError($('#immediately').parent());
							if(($('#switchYes').prop('checked') || $('#switchNo').prop('checked')) || !$('#examCat').prop('checked')) {
								unsetError($('#endYes').parent());
								if(($('#endYes').prop('checked') || $('#endNo').prop('checked')) || !$('#examCat').prop('checked')) {
									unsetError($('#noTimeLost').parent());
									if(($('#noTimeLost').prop('checked') || $('#timeLost').prop('checked')) || !$('#examCat').prop('checked')) {
										unsetError($('.save-button'));
										unsetError($('#totalTimeHour').parent());
										unsetError($('#sectionTimes .row:eq(0)'));
										unsetError($('#orderStart').parent());
										unsetError($('#chapterSelect'));
										if($('#chapterSelect').val() == 0) {
											setError($('#chapterSelect'), 'Please select any chapter or Independent');
											return;
										}
										if($('.has-error').length == 0 ) {
											var req = {};
											req.action = 'edit-exam';
											req.chapterId = $('#chapterSelect').val();
											req.examId = getUrlParameter('examId');
											req.name = $('#titleName').val();
											if($('#assignmentCat').prop('checked'))
												req.type = 'Assignment';
											else if($('#examCat').prop('checked'))
												req.type = 'Exam';
											req.status = 0;
											var showResult='immediately';
												if($('#after-examend').prop('checked'))
													showResult='after';
												req.showResult=showResult;
												var showResultTill=1;
												var showResultTillDate="";
												if($('#custom-time').prop('checked'))
												{
													showResultTill=2;
													var timestamp = new Date($('#settime').val());
													showResultTillDate = timestamp.getTime();
													
												}
												req.showResultTill=showResultTill;
												req.showResultTillDate=showResultTillDate;
											var timestamp = new Date($('#startDate').val());
											req.startDate = timestamp.getTime();
											req.endDate = '';
											if($('#endDate').val() != '') {
												timestamp = new Date($('#endDate').val());
												req.endDate = timestamp.getTime();
											}
											req.attempts = $('#noOfAttempts').val();
											req.tt_status = 0;
											if($('#switchYes').prop('checked'))
												req.tt_status = 0;
											else if($('#switchNo').prop('checked'))
												req.tt_status = 1;
											req.totalTime = 6000;
											req.gapTime = 0;
											if(req.tt_status == 0 || !$('#examCat').prop('checked')) {
												req.sectionOrder = 2;
												var tth = $('#totalTimeHour').val();
												tth = tth.substring(0, tth.length-1);
												tth = parseInt(tth);
												var ttm = $('#totalTimeMinute').val();
												ttm = ttm.substring(0, ttm.length-1);
												ttm = parseInt(ttm);
												req.totalTime = (tth * 60) + ttm;
												if(req.totalTime <= 0 && $('#examCat').prop('checked')) {
													setError($('#totalTimeHour').parent(), "Please specify a correct time limit.");
													return;
												}
											}
											if(req.tt_status == 1 || !$('#examCat').prop('checked')) {
												req.totalTime=0;
												var gap = $('#gapTime').val();
												req.gapTime = gap.substring(0, gap.length-1);
												if(!($('#orderStart').prop('checked') || $('#orderRandom').prop('checked'))) {
													if($('#examCat').prop('checked')) {
														setError($('#orderStart').parent(), "Please select ordering of the sections.");
														return;
													}
												}
											}
											req.endBeforeTime = 0;
											req.powerOption = 0;
											if($('#endYes').prop('checked'))
												req.endBeforeTime = 1;
											else if($('#endNo').prop('checked'))
												req.endBeforeTime = 0;
											if($('#noTimeLost').prop('checked'))
												req.powerOption = 0;
											else if($('#timeLost').prop('checked'))
												req.powerOption = 1;
											req.sections = [];
											var sections = $('#sections').find('input[type="text"]');
											var sectionTimesHour = $('#sectionTimes').find('input.hour');
											var sectionTimesMinute = $('#sectionTimes').find('input.minute');
											for(i=0; i<sections.length; i++) {
												var obj = {};
												obj.section = sections[i]['value'];
												obj.id = sections[i]['id'];
												obj.time = 0;
												obj.weight = sortOrder.indexOf(i+'');
												if(req.tt_status == 1 && $('#examCat').prop('checked')) {
													var sth = sectionTimesHour[i]['value'];
													sth = sth.substring(0, sth.length-1);
													sth = parseInt(sth);
													var stm = sectionTimesMinute[i]['value'];
													stm = stm.substring(0, stm.length-1);
													stm = parseInt(stm);
													var st = (sth * 60) +stm;
													if(st <= 0 && $('#examCat').prop('checked')) {
														setError($('#sectionTimes .row:eq(0)'), "Please select correct timings for every section.");
														return;
													}
													obj.time = st;
												}
												req.sections.push(obj);
											}
											req.sectionOrder = 1;
											if($('#orderStart').prop('checked'))
												req.sectionOrder = 0;
											else if($('#orderRandom').prop('checked'))
												req.sectionOrder = 1;
												//console.log(req);
											$.ajax({
												'type'  : 'post',
												'url'   : ApiEndpoint,
												'data' 	: JSON.stringify(req)
											}).done(function (res) {
											res =  $.parseJSON(res);
												if(res.status == 0)
													alert(res.message);
												else {
													if(elem.hasClass('proceed'))
														window.location = 'add-assignment-2.php?examId='+res.examId;
													else
														fetchExamDetails();
												}
											});
										}
										else
											setError($('.save-button'), "Please review your forms. It contains errors.");
									}
									else
										setError($('#noTimeLost').parent(), "Please select an option to continue.");
								}
								else
									setError($('#endYes').parent(), "Please select one option.");
							}
							else
								setError($('#switchYes').parent(), "Please select a option to switch between sections or not.");
						
						}
							else
									setError($('#immediately').parent(), "Please select a option when student can see the result.");
							}
							else
									setError($('#course-end').parent(), "Please select a option/End date till when student can see the result.");					}
						else
							setError($('#endDate'), "Please specify a correct end date.");
					}
					else
						setError($('#startDate'), "Please specify a correct start date.");
				}
				else
					setError($('#tempSection'), "Please add atleast one section.");
			}
			else
				setError($('#examCat').parent(), "Please select a test type.")
		}
		else
			setError($('#noOfAttempts'), "Please specify number of attempts allowed.");
	});
	
	$('#switchYes').on('click', function() {
		unsetError($('#totalTimeHour').parent());
		unsetError($('#sectionTimes .row:eq(0)'));
		unsetError($('#orderStart').parent());
		$('#sectionTimes').hide();
		$('#totalTime').show();
	});
	
	$('#switchNo').on('click', function() {
		unsetError($('#totalTimeHour').parent());
		unsetError($('#orderStart').parent());
		unsetError($('#sectionTimes .row:eq(0)'));
		$('#totalTime').hide();
		var html = '';
		$('#sectionTimes .row:eq(0)').find('*').remove();
		if($('#sections').find('input[type="text"]').length == 0) {
			html = '<span style="color: red;">Please Add a section first.</span>';
		}
		for(i=0; i<$('#sections').find('input[type="text"]').length; i++) {
			html += '<div class="col-lg-6">'
					+ '<label for="coursename">Total Time for '+$('#sections').find('input[type="text"]:eq('+i+')').val()+'</label><br>'
					+ '<div class="col-md-4">'
						+ '<div class="input-group input-small">'
							+ '<input type="text" maxlength="3" class="spinner-input form-control time hour" value="0h" disabled=true>'
							+ '<div class="spinner-buttons input-group-btn btn-group-vertical">'
								+ '<button type="button" class="btn spinner-up btn-xs btn-default generated">'
									+ '<i class="fa fa-angle-up"></i>'
								+ '</button>'
								+ '<button type="button" class="btn spinner-down btn-xs btn-default generated">'
									+ '<i class="fa fa-angle-down"></i>'
								+ '</button>'
							+ '</div>'
						+ '</div>'
					+ '</div> &nbsp;&nbsp;&nbsp;&nbsp;'
					+ '<div class="col-md-4">'
						+ '<div class="input-group input-small">'
							+ '<input type="text" class="spinner-input form-control time minute" maxlength="3" value="0m" disabled=true>'
							+ '<div class="spinner-buttons input-group-btn btn-group-vertical">'
								+ '<button class="btn spinner-up btn-xs btn-default generated" type="button">'
									+ '<i class="fa fa-angle-up"></i>'
								+ '</button>'
								+ '<button class="btn spinner-down btn-xs btn-default generated" type="button">'
									+ '<i class="fa fa-angle-down"></i>'
								+ '</button>'
							+ '</div>'
						+ '</div>'
					+ '</div>'
				+ '</div>';
		}
		$('#sectionTimes .row:eq(0)').append(html);
		$('#sectionTimes').show();
		$('.spinner-up.generated').on('click', function() {
			var input = $(this).parents('.input-group').find('input[type="text"]');
			var value = input.val();
			var postfix = value.substring(value.length-1);
			var num = value.substring(0, value.length-1);
			num++;
			input.val(num+postfix);
		});
		$('.spinner-down.generated').on('click', function() {
			var input = $(this).parents('.input-group').find('input[type="text"]');
			var value = input.val();
			var postfix = value.substring(value.length-1);
			var num = value.substring(0, value.length-1);
			if(num-1 >= 0)
				num--;
			input.val(num+postfix);
		});
	});
	
	$('#addSection').on('click', function() {
		$('#tempSection').parent().removeClass('has-error');
		$('#tempSection').next().remove();
		if($('#tempSection').val() != '') {
			var html='<div class="form-group">'
						+ '<div class="row">'
							+ '<div class="col-lg-9">'
								+ '<input type="text" class="form-control" value="'+$('#tempSection').val()+'" id=0>'
							+ '</div>'
							+ '<div class="col-lg-3">'
								+ '<button class="btn btn-primary btn-xs">Draft</button>'
								+ '<button class="btn btn-danger btn-xs delete-button" style="margin-left:5px;"><i class="fa fa-trash-o "></i></button>'
							+ '</div>'
						+ '</div>'
					+ '</div>';
			$('#sections').append(html);
			$('#tempSection').val('');
			$('.delete-button').on('click', function() {
				$(this).parents('.form-group').remove();
				$('#sectionTimes .row:eq(0)').find('*').remove();
				if($('#switchNo').prop('checked'))
					$('#switchNo').click();
				if($('#sections').find('input[type="text"]').length <= 1) {
					$('#switchYes').prop('checked', true);
					$('#orderRandom').prop('checked', true);
					$('#switchYes').parents('div:eq(0)').hide();
					$('#orderStart').parents('div:eq(0)').hide();
					$('#sectionTimes').hide();
					$('#totalTime').show();
				}
			});
			if($('#sections').find('input[type="text"]').length > 1) {
				$('#switchYes').prop('checked', false);
				$('#orderRandom').prop('checked', false);
				$('#switchYes').parents('div:eq(0)').show();
				$('#orderStart').parents('div:eq(0)').show();
			}
		}
		else {
			$('#tempSection').parent().addClass('has-error');
			$('#tempSection').parent().append('<span class="help-block">Please provide a section name.</span>');
		}
		//event listener for bluring sections name
		$('#sections input[type="text"]').off('blur');
		$('#sections input[type="text"]').on('blur', function() {
			if($('#orderStart').prop('checked')) {
				$('#orderStart').click();
			} else if($('#orderRandom').prop('checked')) {
				$('#orderRandom').click();
			}
		});
		$('#sections input[type="text"]:eq(0)').blur();
	});
	
	
	$('.spinner-up').on('click', function() {
		var input = $(this).parents('.input-group').find('input[type="text"]');
		var value = input.val();
		var postfix = value.substring(value.length-1);
		var num = value.substring(0, value.length-1);
		num++;
		input.val(num+postfix);
	});
	
	$('.spinner-down').on('click', function() {
		var input = $(this).parents('.input-group').find('input[type="text"]');
		var value = input.val();
		var postfix = value.substring(value.length-1);
		var num = value.substring(0, value.length-1);
		if(num-1 >= 0)
			num--;
		input.val(num+postfix);
	});
	
	function fetchCourseDates() {
		var req = {};
		var res;
		req.action = 'get-course-date';
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				$('#courseStartDate').val(res.dates.liveDate);
				$('#courseEndDate').val(res.dates.endDate);
				
				
				//adding handlers for datetimepicker of start and end date
				var now = new Date();
				var start = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0); //imp as to get exact 00:00 time value
				var courseStart = new Date(parseInt(res.dates.liveDate));
				examStart = new Date(courseStart.getTime());
				/*if(start.valueOf() < courseStart.valueOf())
					examStart = new Date(courseStart.getTime());
				else
					examStart = new Date(start.getTime());*/
				var courseEnd = new Date(parseInt(res.dates.endDate));
				var end = new Date('2099/12/31');
				if(end.valueOf() > courseEnd.valueOf())
					examEnd = new Date(courseEnd.getTime());
				else
					examEnd = new Date(end.getTime());
				if(res.dates.endDate == '') {
					$('#endDate').parent().hide();
					$('#result-show').hide();
				//	$('#set-exam-date').hide();
					examEnd = new Date(end.getTime());
				}
				$('#startDate').datetimepicker({
					format: 'd F Y H:i',
					minDate: examStart,
					maxDate: examEnd,
					step: 30,
					onSelectTime: function(date) {
						unsetError($('#startDate'));
						if (date.valueOf() > tempEnd.valueOf()) {
							setError($('#startDate'), 'The start date can not be greater than the end date.');
						} else {
							tempStart = date;
						}
					}
				});
				var today=new Date($.now());
				$('#settime').datetimepicker({
					format: 'd F Y H:i',
					minDate: today,
					step: 30						
				});
				$('#endDate').datetimepicker({
					format: 'd F Y H:i',
					minDate: examStart,
					maxDate: examEnd,
					step: 30,
					onSelectTime: function(date) {
						unsetError($('#endDate'));
						if (date.valueOf() < tempStart.valueOf()) {
							setError($('#endDate'), 'The end date can not be less than the start date.');
						} else {
							tempEnd = date;
						}
					}
				});
			}
		});
	}
	
	function fetchBreadcrumb() {
		var req = {};
		var res;
		req.action = "get-breadcrumb-for-exam";
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
				fillBreadcrumb(res);
		});
	}
	
	function fillBreadcrumb(data) {
		$('ul.breadcrumb li:eq(0)').find('a').attr('href', 'edit-course.php?courseId=' + data.course.courseId).text(data.course.courseName);
		$('ul.breadcrumb li:eq(1)').find('a').attr('href', 'edit-subject.php?courseId=' + data.course.courseId + '&subjectId=' + data.subChap.subjectId).text(data.subChap.subjectName);
		if(data.subChap.chapterId == null)
			$('ul.breadcrumb li:eq(2)').find('a').text('Independent');
		else
			$('ul.breadcrumb li:eq(2)').find('a').attr('href', 'content.php?courseId=' + data.course.courseId + '&subjectId=' + data.subChap.subjectId + '&chapterId=' + data.subChap.chapterId).text(data.subChap.chapterName);
	}
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fetchExamDetails() {
		var req = {};
		req.action = 'get-exam';
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			//console.log(res);
			fillExamDetails(res);
		});
	}
	
	function fillExamDetails(data) {
		$('#examName').text(data.exam.name);
		var status = '';
		if(data.exam.status == 0)
			status = 'Draft';
		else if(data.exam.status == 1)
			status = 'Not Live';
		else if(data.exam.status == 2)
			status = 'Live';
		$('#examStatus').text(status);
		$('#noOfAttempts').val(data.exam.attempts);
		if(data.exam.showResultTill== 1)
		{
			$('#course-end').prop('checked', true);
			
		}
		else{
			$('#custom-time').prop('checked', true);
			var tillDate = new Date(parseInt(data.exam.showResultTillDate));
			if(tillDate!='' || tillDate!=null)
			{
				var tsd = tillDate.getDate() + ' ' + MONTH[tillDate.getMonth()]  + ' ' + tillDate.getFullYear() + ' ';
				if(tillDate.getHours() < 10)
					tsd += '0' + tillDate.getHours();
				else
					tsd += tillDate.getHours();
				tsd += ':';
				if(tillDate.getMinutes() < 10)
					tsd += '0' + tillDate.getMinutes();
				else
					tsd += tillDate.getMinutes();
					
				$('#settime').val(tsd);
				$('#set-exam-date').show();
			}
			
		}
		if(data.exam.showResult=='immediately')
		{
			$('#immediately').prop('checked', true);
			
		}
		else{
			$('#after-examend').prop('checked', true);	
			
		}
		//$('#noOfAttempts').val(data.exam.attempts);
		if(data.exam.chapterId == 0)
			$('#chapterSelect').val('-1');
		else
			$('#chapterSelect').val(data.exam.chapterId);
		$('#titleName').val(data.exam.name);
		if(data.exam.type == 'Assignment')
			$('#assignmentCat').prop('checked', true);
		else if(data.exam.type == 'Exam') {
			$('#examCat').prop('checked', true);
			$('#assExm').show();
		}
		var startDate = new Date(parseInt(data.exam.startDate));
		var endDate = new Date(parseInt(data.exam.endDate));
		tempStart = startDate;
		tempEnd = endDate;
		var sd = startDate.getDate() + ' ' + MONTH[startDate.getMonth()]  + ' ' + startDate.getFullYear() + ' ';
		if(startDate.getHours() < 10)
			sd += '0' + startDate.getHours();
		else
			sd += startDate.getHours();
		sd += ':';
		if(startDate.getMinutes() < 10)
			sd += '0' + startDate.getMinutes();
		else
			sd += startDate.getMinutes();
		var ed = endDate.getDate()+' '+MONTH[endDate.getMonth()] +' '+endDate.getFullYear()+' ';
		if(endDate.getHours() < 10)
			ed += '0' + endDate.getHours();
		else
			ed += endDate.getHours();
		ed += ':';
		if(endDate.getMinutes() < 10)
			ed += '0' + endDate.getMinutes();
		else
			ed += endDate.getMinutes();
		$('#startDate').val(sd);
		$('#endDate').val(ed);
		//$('#startDate, #endDate').blur();
		if(data.exam.endBeforeTime == 1)
			$('#endYes').prop('checked', true);
		else
			$('#endNo').prop('checked', true);
		if(data.exam.powerOption == 0)
			$('#noTimeLost').prop('checked', true);
		else
			$('#timeLost').prop('checked', true);
		$('#sections').find('*').remove();
		$('#sectionTimes .row:eq(0)').find('*').remove();
		if(data.exam.totalTime == 0 || data.exam.totalTime == 6000) {//changed to checked total time
			$('#switchNo').prop('checked', true);
			$('#sectionTimes').show();
			$('#totalTime').hide();
			$('#gapTime').val(data.exam.gapTime+'m');
			for(i=0; i<data.sections.length; i++) {
				var sectionStatus = '';
				if(data.sections[i].status == 0)
					sectionStatus = 'Draft';
				else if(data.sections[i].status == 1)
					sectionStatus = 'Completed';
				var html='<div class="form-group">'
						+ '<div class="row">'
							+ '<div class="col-lg-9">'
								+ '<input type="text" class="form-control" value="'+data.sections[i].name+'" id="'+data.sections[i].id+'">'
							+ '</div>'
							+ '<div class="col-lg-3">'
								+ '<input type="button" class="btn btn-xs btn-primary" value="' + sectionStatus + '">'
								+ '<button class="btn btn-danger btn-xs delete-button" type="button" style="margin-left:5px;"><i class="fa fa-trash-o "></i></button>'
							+ '</div>'
						+ '</div>'
					+ '</div>';
				$('#sections').append(html);
				$('.delete-button').on('click', function() {
				$(this).parents('.form-group').remove();
				$('#sectionTimes .row:eq(0)').find('*').remove();
				if($('#switchNo').prop('checked'))
					$('#switchNo').click();
				if($('#sections').find('input[type="text"]').length <= 1) {
					$('#switchYes').prop('checked', true);
					$('#orderRandom').prop('checked', true);
					$('#switchYes').parents('div:eq(0)').hide();
					$('#orderStart').parents('div:eq(0)').hide();
					$('#sectionTimes').hide();
					$('#totalTime').show();
				}
			});
				var htmlTime = '<div class="col-lg-6">'
					+ '<label for="coursename">Total Time for ' + data.sections[i].name + '</label><br>'
					+ '<div class="col-md-4">'
						+ '<div class="input-group input-small">'
							+ '<input type="text" maxlength="3" class="spinner-input form-control time hour" value="0h" disabled=true>'
							+ '<div class="spinner-buttons input-group-btn btn-group-vertical">'
								+ '<button type="button" class="btn spinner-up btn-xs btn-default generated">'
									+ '<i class="fa fa-angle-up"></i>'
								+ '</button>'
								+ '<button type="button" class="btn spinner-down btn-xs btn-default generated">'
									+ '<i class="fa fa-angle-down"></i>'
								+ '</button>'
							+ '</div>'
						+ '</div>'
					+ '</div> &nbsp;&nbsp;&nbsp;&nbsp;'
					+ '<div class="col-md-4">'
						+ '<div class="input-group input-small">'
							+ '<input type="text" class="spinner-input form-control time minute" maxlength="3" value="0m" disabled=true>'
							+ '<div class="spinner-buttons input-group-btn btn-group-vertical">'
								+ '<button class="btn spinner-up btn-xs btn-default generated" type="button">'
									+ '<i class="fa fa-angle-up"></i>'
								+ '</button>'
								+ '<button class="btn spinner-down btn-xs btn-default generated" type="button">'
									+ '<i class="fa fa-angle-down"></i>'
								+ '</button>'
							+ '</div>'
						+ '</div>'
					+ '</div>'
				+ '</div>';
				$('#sectionTimes .row:eq(0)').append(htmlTime);
				hour = Math.floor(data.sections[i].time / 60);
				minute = data.sections[i].time - (hour * 60);
				$('#sectionTimes input[type="text"].hour:eq('+i+')').val(hour+'h');
				$('#sectionTimes input[type="text"].minute:eq('+i+')').val(minute+'m');
			}
		}
		else {
			$('#switchYes').prop('checked', true);
			$('#sectionTimes').hide();
			$('#totalTime').show();
			for(i=0; i<data.sections.length; i++) {
				var sectionStatus = '';
				if(data.sections[i].status == 0)
					sectionStatus = 'Draft';
				else if(data.sections[i].status == 1)
					sectionStatus = 'Completed';
				var html='<div class="form-group">'
						+ '<div class="row">'
							+ '<div class="col-lg-9">'
								+ '<input type="text" class="form-control" value="'+data.sections[i].name+'" id="'+data.sections[i].id+'">'
							+ '</div>'
							+ '<div class="col-lg-3">'
								+ '<input type="button" class="btn btn-xs btn-primary" value="' + sectionStatus + '">'
								+ '<button class="btn btn-danger btn-xs delete-button" type="button" style="margin-left:5px;"><i class="fa fa-trash-o "></i></button>'
							+ '</div>'
						+ '</div>'
					+ '</div>';
				$('#sections').append(html);
				$('.delete-button').on('click', function() {
				$(this).parents('.form-group').remove();
				$('#sectionTimes .row:eq(0)').find('*').remove();
				if($('#switchNo').prop('checked'))
					$('#switchNo').click();
				if($('#sections').find('input[type="text"]').length <= 1) {
					$('#switchYes').prop('checked', true);
					$('#orderRandom').prop('checked', true);
					$('#switchYes').parents('div:eq(0)').hide();
					$('#orderStart').parents('div:eq(0)').hide();
					$('#sectionTimes').hide();
					$('#totalTime').show();
				}
			});
			}
			hour = Math.floor(data.exam.totalTime / 60);
			minute = data.exam.totalTime - (hour * 60);
			$('#totalTimeHour').val(hour+'h');
			$('#totalTimeMinute').val(minute+'m')
		}
		if($('#sections').find('input[type="text"]').length > 1) {
			//$('#switchYes').prop('checked', false);
			//$('#orderRandom').prop('checked', false);
			$('#switchYes').parents('div:eq(0)').show();
			$('#orderStart').parents('div:eq(0)').show();
		}
		if(data.exam.sectionOrder == 0) {
			$('#orderStart').prop('checked', true);
			$('#sortOrder').find('li').remove();
			var htm = '';
			for(var a=0; a<$('#sections').find('input[type="text"]').length; a++) {
				var value = $('#sections input[type="text"]:eq('+a+')').val();
				htm += '<li data-original="'+a+'">' + value + '</li>';
				sortOrder[a] = a+'';
			}
			$('#sortOrder').append(htm);
			$('#sortOrder').show();
		}
		else
			$('#orderRandom').prop('checked', true);
		$('#sections input[type="text"]').on('blur', function() {
			if($('#orderStart').prop('checked')) {
				$('#orderStart').click();
			} else if($('#orderRandom').prop('checked')) {
				$('#orderRandom').click();
			}
		});
	}
	//check
	function min(what, length) {
		if(what.val().length < length)
			return false;
		else
			return true;
	}
	
	function max(what, length) {
		if(what.val().length > length)
			return false;
		else
			return true;
	}
	
	function nameAvailable() {
		if($('#titleName').val().length != '' && ($('#examCat').prop('checked') || $('#assignmentCat').prop('checked'))) {
			var req = {};
			var res;
			req.action = 'check-name-for-exam';
			req.name = $('#titleName').val();
			req.examId = getUrlParameter('examId');
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else {
					unsetError($('#titleName'));
					if(!res.available)
						setError($('#titleName'),'Please select a different name as you have already used this name.');
				}
			});
		}
	}
	$("#noOfAttempts").keypress(function (e) {
		$('.help-block').remove();
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			$('#noOfAttempts').after('<span class="help-block text-danger">Enter Only Numbers</span>');
			//$('.help-block').fadeOut("slow");
			// ("#errmsg").html("Digits Only").show().fadeOut("slow");
			return false;
		}
	});
});