/*
 Author: Fraaz Hashmi.
 Dated: 7/2/2014
 
 
 ********
 1) Notes: JSON.stringify support for IE < 10
 */
var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var userRole;

$(function () {
    var imageRoot = '/';
    $('#loader_outer').hide();

    function fetchProfileDetails() {
        var req = {};
        var res;
        req.action = "get-profile-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillProfileDetails(res);
            console.log(res);
        });
    }

    function fillProfileDetails(data) {
        // General Details
        var imageRoot = 'user-data/images/';
        $('#user-profile-card .username').text(data.loginDetails.username);

        if (data.profileDetails.profilePic != "") {
            $('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
        }

        // Role Specific
        userRole = data.userRole;
        if (data.userRole == '1') {
            fillInstituteDetails(data);
        }
        else if (data.userRole == '2') {
            fillProfessorDetails(data);
        }
        else if (data.userRole == '3') {
            fillPublisherDetails(data);
        }
    }

    function fillInstituteDetails(data) {
        $('h4#profile-name').text(data.profileDetails.name);
    }

    function fillProfessorDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('a#profile-name').text(name);
    }

    function fillPublisherDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('a#profile-name').text(name);
    }

    function fetchChapters() {
        var req = {};
        var res;
        req.action = "get-all-deleted-chapter";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1)
                fillChapters(res);
            else
                alertMsg(res.message);
        });
    }

    function fillChapters(data) {
        var html = "";
        var editCourseLink, editSubjectLink, addSubjectLink;
        var chapterId,institutenameId,coursenameId,subjectnameId;
        $.each(data.chapters, function (i, chapter) {
           
          //  editCourseLink = "backend_edit-course.php?courseId=" + course.id;
            coursenameId = chapter.course;
            chapterId='CH00'+chapter.chapterid;
            subjectnameId=chapter.subject+' (S00'+chapter.subjectid+')';
            institutenameId = chapter.username+' ('+chapter.instituteid+')';
            html += "<tr data-cid='" + chapter.chapterid + "'>"
                    + "<td>" +chapterId+ "</td>"
                   + "<td>" +chapter.chapter+ "</td>"
                    + "<td><h5><a href='#'>" + subjectnameId + "</a></h5>";
            html += "</td>";
            html += "<td>" + coursenameId + "</td>";
            html += "<td>" + institutenameId + "</td>";
            html += "<td class=small-txt>" + chapter.email + "</td>";
            html += "<td>" + chapter.contactMobile + "</td>"
                    + "<td><a href='#' class='restore'><span class='label label-success label-mini' style='display:block;'>Restore</span></a></td>"
                    + "</td></tr>";
    });
         
            $('#all-chapters tbody').html(html);
//            $('#all-chapters').dataTable({
//            "aaSorting": [[0, "asc"]]
//         });
            addApprovalHandlers();
       
       

    }
    //fetchProfileDetails();
    fetchChapters();
});

function addApprovalHandlers() {
    $('#all-chapters .restore').on('click', function (e) {
        e.preventDefault();
        var chapter = $(this).parents('tr');
        var cid = chapter.attr('data-cid');
        var req = {};
        req.action = "restore-chapter-admin";
        req.chapterId = cid;
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
             res = $.parseJSON(res);
            if (res.status == 1) {
                chapter.remove();
                // course.find('.approved').show();
            } else {
                alertMsg(res.message);
            }
        });
    });

}