var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var ids=[];
var names=[];
var freetiereligibility1=[];
var courselevelprice=[];
var courselevelcheck=[];
var examlevelprice=[];
var examlevelcheck=[];

$(function () {
    
	
	
    loadbackCoursekeys();
    function loadbackCoursekeys() {
        var req = {};
        var res;
        req.action = "free-tier";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
			//console.log(data.users);
            var html = "";
            for (var i = 0; i < data.users.length; i++) {
			//	console.log(data.users[i]);
				
                var userId = data.users[i].userId;
                var name = data.users[i].name;
				var role = data.users[i].role;
				var eligibility=0;
				var ecostprice=0;
				var ccostprice=0;
				var ecostpricecheck=0;
				var ccostpricecheck=0;
				if(data.users[i].freeTierInfo != undefined)
				{
					var free_tier=(data.users[i].freeTierInfo);
				//	console.log(free_tier);
					eligibility=free_tier.free_tiereligibility;
					ecostprice=free_tier.examlevelCost;
					ccostprice=free_tier.courselevelCost;
					ecostpricecheck=free_tier.examlevelCheck;
					ccostpricecheck=free_tier.courselevelCheck;
				}
				if(role==1)
				{
					role='Institute';
				}
				else{
					role='Professor/Tutor';
				}
				ids.push(userId);
				names.push(name);
			    freetiereligibility1.push(eligibility);
				courselevelprice.push(ccostprice);
				courselevelcheck.push(ccostpricecheck);
				examlevelprice.push(ecostprice);  
				examlevelcheck.push(ecostpricecheck);
				var style = "";
                var ele="<input  type='checkbox' >";
				if(eligibility==1)
				{
					  ele="<input  type='checkbox' checked='checked'>"	;
				}
				var elevel=' Price: '+ecostprice+'  <input  type="checkbox" >';
				if(ecostpricecheck==1)
				{
					  elevel=' Price: '+ecostprice +'  <input  type="checkbox" checked="checked" >';
				}
				var clevel=' Price: '+ccostprice+'  <input  type="checkbox" >';
				if(ccostpricecheck==1)
				{
					  clevel=' Price: '+ccostprice +'  <input  type="checkbox" checked="checked" >';
				}
                html += '<tr ' + style + ' rowid=' + userId + '><td>' + (i + 1) + '</td><td>' + userId + '</td><td>' + name + '</td><td>' + role + '</td><td>' +ele + '</td><td>' + elevel + '</td><td>' + clevel + '</td>';

                html += '<td><a  href="#modal_edit_free_tier" data-toggle="modal" class="btn btn-primary btn-md edit_modal" ><i class="fa fa-edit"></i> </a>   </td></tr>';

            }
            
			$('#free-tier1 tbody').html(html);
			$('#free-tier1').show();
			$('#free-tier1').dataTable( {
				"aaSorting": [[ 0, "asc" ]],
				"bDestroy": true
			});
		
            //$('#default_rate_inr').val(data.default_rate[0].key_rate_inr);
            //$('#default_rate_dollar').val(data.default_rate[0].key_rate);
            var rowid = '';
			var id=''
			var freeeligibility='';
			var clevel=[];
			var clevelcheck=[];
			var evelprice=[];
			var evelcheck=[];

            $('.edit_modal').on('click', function () {
                rowid = $(this).parents('td').parent('tr').attr('rowid');
				var index=jQuery.inArray( rowid, ids );
				id=ids[index];
				var name=names[index];
				//console.log(name);
                $("#nameI").text(name);
				if(freetiereligibility1[index]==1)
				{
					$('#freetier-eligibility').prop('checked',true);
				}else{
					$('#freetier-eligibility').prop('checked',false);
				}
				if(courselevelcheck[index]==1)
				{
					$('#course-level-box').prop('checked',true);
				}else{
					$('#course-level-box').prop('checked',false);
				}
				if(examlevelcheck[index]==1)
				{
					$('#exam-level-box').prop('checked',true);
				}else{
					$('#exam-level-box').prop('checked',false);
				}
				
				 
				$("#course-level").val(courselevelprice[index]);
				$("#exam-level").val(examlevelprice[index]);
				
            });
            

            $('#save_free_tier').on('click', function () {
                var req = {};
                var res;
				
				
				
				
				
				
				
				
				freeeligibility=0;
				clevelcheck=0;
				evelcheck=0;
				
				if($('#freetier-eligibility').is(":checked"))
					freeeligibility=1;
				if($('#course-level-box').is(":checked"))
					clevelcheck=1;
				if($('#exam-level-box').is(":checked"))
					evelcheck=1;
				
				clevel=$('#course-level').val();
				evelprice=$('#exam-level').val();
				req.action = "save_free_tier";
                req.instiId = id;				
                req.freeeligibility = parseInt(freeeligibility);
                req.clevelprice = parseFloat(clevel);
				req.clevelcheck = clevelcheck;
                req.elevelprice =  parseFloat(evelprice);
				req.elevelcheck = evelcheck;
				console.log(req);
                $.ajax({
                    'type': 'post',
                    'url': ApiEndpoint,
                    'data': JSON.stringify(req)
                }).done(function (res) {
                    data = $.parseJSON(res);
                   window.location = self.location;

                });
            });

            
        });



    }

});