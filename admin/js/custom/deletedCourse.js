/*
 Author: Fraaz Hashmi.
 Dated: 7/2/2014
 
 
 ********
 1) Notes: JSON.stringify support for IE < 10
 */
var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var userRole;

$(function () {
    var imageRoot = '/';
    $('#loader_outer').hide();

    function fetchProfileDetails() {
        var req = {};
        var res;
        req.action = "get-profile-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillProfileDetails(res);
            console.log(res);
        });
    }

    function fillProfileDetails(data) {
        // General Details
        var imageRoot = 'user-data/images/';
        $('#user-profile-card .username').text(data.loginDetails.username);

        if (data.profileDetails.profilePic != "") {
            $('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic );
        }

        // Role Specific
        userRole = data.userRole;
        if (data.userRole == '1') {
            fillInstituteDetails(data);
        }
        else if (data.userRole == '2') {
            fillProfessorDetails(data);
        }
        else if (data.userRole == '3') {
            fillPublisherDetails(data);
        }
    }

    function fillInstituteDetails(data) {
        $('h4#profile-name').text(data.profileDetails.name);
    }

    function fillProfessorDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('a#profile-name').text(name);
    }

    function fillPublisherDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('a#profile-name').text(name);
    }

    function fetchCourses() {
        var req = {};
        var res;
        req.action = "get-all-deleted-course";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            //console.log(res);
            res = $.parseJSON(res);
            if (res.status == 1)
                fillCourses(res);
            else
                alertMsg(res.message);
        });
    }

    function fillCourses(data) {
        var html = "";
        var editCourseLink, editSubjectLink, addSubjectLink;
        $.each(data.courses, function (i, course) {
           
            editCourseLink = "backend_edit-course.php?courseId=" + course.id;
            courseId = "C" + String("000" + course.courseid).slice(-4);
            html += "<tr data-cid='" + course.courseid + "'>"
                    + "<td>" + courseId + "</td>"
                    + "<td><h5><a href='" + editCourseLink + "'>" + course.name + "</a></h5>";

            html += "</td>"
            html += "<td>" + course.id + "</td>";
            html += "<td>" + course.username + "</td>";
            html += "<td class=small-txt>" + course.email + "</td>";
            html += "<td>" + course.contactMobile + "</td>"
                    + "<td><a href='#' class='restore'><span class='label label-success label-mini' style='display:block;'>Restore</span></a></td>"
                    + "</td></tr>";
    });
         $('#all-courses tbody').html(html);
//            $('#all-courses').dataTable({
//            "aaSorting": [[0, "asc"]]
//         });
            addApprovalHandlers();
     
    }
    //fetchProfileDetails();
    fetchCourses();
});

function addApprovalHandlers() {
    $('#all-courses .restore').on('click', function (e) {
        e.preventDefault();
        var course = $(this).parents('tr');
        var cid = course.attr('data-cid');
        var req = {};
        req.action = "restore-course-admin";
        req.courseId = cid;
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            console.log(res);
            res = $.parseJSON(res);
            if (res.status == 1) {
                course.remove();
                // course.find('.approved').show();
            } else {
                alertMsg(res.message);
            }
        });
    });

    $('#all-courses .reject').on('click', function (e) {
        e.preventDefault();
        var course = $(this).parents('tr');
        var cid = course.attr('data-cid');
        var req = {};
        req.action = "reject-course";
        req.courseId = cid;
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            console.log(res);
            res = $.parseJSON(res);
            if (res.status == 1) {
                course.find('.approve, .reject').hide();
                course.find('.rejected').show();
            } else {
                alertMsg(res.message);
            }
        });
    });
}