var ApiEndPoint = '../api/index.php';
var questions;
/*var questions = [
					{"id":"133","qno":1,"questionType":"question","question":"<p>What is the capital of Maharashtra?<\/p>\n","time":0,"topper_time":0,"avg_time":0,"marks":10,"max_marks":10,"topper_marks":10,"avg_marks":10,"uploads":[
						{"path":"../user-data/test.pdf", "type":"pdf"},
						{"path":"../user-data/fb-img.jpg", "type":"jpg"},
						{"path":"http://dmsd6g597imqb.cloudfront.net/user-data/files/subjective-upload-14-1452465501.jpg", "type":"docx"}
					],"answer":"Mumbai","student_answer":"Bombay","subQuestions":[]},
					{"id":"143","qno":2,"questionType":"questionGroup","question":"<p>Rapid Fire<\/p>\n","time":0,"topper_time":0,"avg_time":0,"marks":10,"max_marks":10,"topper_marks":10,"avg_marks":10,"uploads":[],"answer":"","subquestionMrks":"2.00","listType":"one","subQuestions":[
						{"id":"144","questionType":"question","question":"<p>Capital of Karnataka?<\/p>\n","time":0,"topper_time":0,"avg_time":0,"marks":10,"max_marks":10,"topper_marks":10,"avg_marks":10,"uploads":[
							{"path":"../user-data/test.pdf", "type":"pdf"},
							{"path":"../user-data/fb-img.jpg", "type":"jpg"},
							{"path":"http://dmsd6g597imqb.cloudfront.net/user-data/files/subjective-upload-14-1452465501.jpg", "type":"docx"}
						],"answer":"Bangalore","student_answer":"Mangalore"},
						{"id":"145","questionType":"question","question":"<p>Capital of Telangana<\/p>\n","time":0,"topper_time":0,"avg_time":0,"marks":10,"max_marks":10,"topper_marks":10,"avg_marks":10,"uploads":[],"answer":"Hyderabad","student_answer":"Vijaywada"}
					]},
					{"id":"146","qno":3,"questionType":"questionGroup","question":false,"time":0,"topper_time":0,"avg_time":0,"marks":10,"max_marks":10,"topper_marks":10,"avg_marks":10,"uploads":[],"answer":"","subquestionMrks":"2.00","listType":"multi","subQuestions":[
						{"id":"147","qno":1,"questionType":"question","question":"<p>Capital of Madhya Pradesh?<\/p>\n","time":0,"topper_time":0,"avg_time":0,"marks":10,"max_marks":10,"topper_marks":10,"avg_marks":10,"uploads":[],"answer":"Bhopal","student_answer":"Indore"},
						{"id":"148","qno":2,"questionType":"question","question":"<p>Capital of Gujarat?<\/p>\n","time":0,"topper_time":0,"avg_time":0,"marks":10,"max_marks":10,"topper_marks":10,"avg_marks":10,"uploads":[],"answer":"Gandhinagar","student_answer":"Ahmedabad"}
					]}
				];*/
$(function(){
	fetchSubjectiveQuestions();
	$('#questionsContainer').on("click", ".js-assign-marks", function() {
		var thiz		= $(this);
		var marks		= parseFloat($(this).closest('.input-group').find('.form-control').val());
		var questionId	= $(this).attr("data-question");
		var maxMarks	= parseFloat($(this).attr("data-max"));
		if (marks>maxMarks) {
			alert("Enter valid marks please!");
		} else if(!questionId) {
			alert("Please, refresh and try again!");
		} else {
			var req = {};
			var res;
			req.action		= 'save-marks-for-subjectiveExam';
			req.attemptId	= getUrlParameter('attemptId');
			req.questionId	= questionId;
			req.marks		= marks;
			thiz.attr("disabled", true);
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				thiz.attr("disabled", false);
				res =  $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else {
					//alert("Marks saved!");
				}
			});
		}
	});
	$('#questionsContainer').on("click", ".js-answer-review", function() {
		var thiz		= $(this);
		var review		= $(this).closest('.col-md-6').find('.form-control').val();
		var questionId	= $(this).attr("data-question");
		var answerId	= $(this).attr("data-answer");
		if (!review) {
			alert("Enter review/comment please!");
		} else if(!questionId && !answerId) {
			alert("Please, refresh and try again!");
		} else {
			var req = {};
			var res;
			req.action		= 'save-review-for-subjectiveExam';
			req.attemptId	= getUrlParameter('attemptId');
			req.review		= review;
			if (!!questionId) {
				req.questionId	= questionId;
			} else {
				req.answerId	= answerId;
			}
			thiz.attr("disabled", true);
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				thiz.attr("disabled", false);
				res =  $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else {
					//alert("Review/comment saved!");
				}
			});
		}
	});
	$("#btnAttemptChecked").click(function() {
		var thiz		= $(this);
		var req = {};
		var res;
		req.action		= 'mark-subjective-exam-checked';
		req.attemptId	= getUrlParameter('attemptId');
		thiz.attr("disabled", true);
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			thiz.attr("disabled", false);
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				//alert("Review/comment saved!");
			}
		});
	});
	$('#questionsContainer').on('click', '.js-popup-answer', function(){
		var path = $(this).attr("data-path");
		var type = $(this).attr("data-type");
		if(!path || !type) {
			alert("There is something wrong, please refresh and try again!");
		} else {
			if (type == "pdf") {
				$('#showAnswerDetail').html('<iframe src="'+path+'" frameborder="0" height="500" class="btn-block"></iframe>');
			} else {
				$('#showAnswerDetail').html('<img src="'+path+'" alt="Answer" class="btn-block" />');
			}
			$('#answerModal').modal('show');
		}
	});
});
function fetchSubjectiveQuestions() {
	var req = {};
	var res;
	req.action = 'get-check-subjective-exam-questions';
	req.attemptId = getUrlParameter('attemptId');
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
			console.log(res.message);
		else {
			//console.log(res);
			$("#studentName").html(res.student);
			$("#attemptNo").html(res.attemptNo);
			var checked = res.checked;
			if (checked==1) {
				$("#btnAttemptChecked").attr("disabled", true);
			};
			questions = res.questions;
			fillQuestions();
		}
	});
}
function fillQuestions() {
	$("#questionsContainer").html('');
	console.log(questions);
	if (questions.length>0) {
		$("#questionsContainer").append('<div class="panel-group questions-panel" id="accQuestions" role="tablist" aria-multiselectable="true"></div>');
		for (var i = 0; i < questions.length; i++) {
			//questions[i]
			$("#accQuestions").append('<div class="panel panel-default">'+
						'<div class="panel-heading" role="tab" id="headQ'+i+'">'+
							'<h4 class="panel-title">'+
								'<a role="button" data-toggle="collapse" data-parent="#" href="#bodyQ'+i+'" aria-expanded="true" aria-controls="bodyQ'+i+'">'+
									'Question #'+questions[i].qno+''+
								'</a>'+
							'</h4>'+
						'</div>'+
						'<div id="bodyQ'+i+'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headQ'+i+'"></div>'+
					'</div>');
			if (questions[i].questionType == "question") {
				$('#bodyQ'+i).append('<div class="panel-body">'+
					'<div class="question">'+questions[i].question+'</div>'+
					'<div class="answer">'+
						'<div class="row">'+
							'<div class="col-md-12">'+
								'<label for="">Correct Answer :</label>'+
								'<div>'+questions[i].answer+'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
					((!questions[i].attempted)?('<div class="answer">'+
						'<div class="row">'+
							'<div class="col-md-12">'+
								'<div class="text-danger">Not attempted</div>'+
							'</div>'+
						'</div>'+
					'</div>'):(((!questions[i].student_answer)?(''):('<div class="answer">'+
						'<div class="row">'+
							'<div class="col-md-6">'+
								'<label for="">Student\'s Answer :</label>'+
								'<div>'+questions[i].student_answer+'</div>'+
							'</div>'+
							'<div class="col-md-6">'+
								'<label for="">Reviews/Comments :</label>'+
								'<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control">'+questions[i].review+'</textarea></div>'+
								'<div class="form-group">'+
									'<button class="btn btn-info js-answer-review" data-question="'+questions[i].id+'">Save Comments</button>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'))+
					'<div class="uploadsbox"></div>'))+
					'<div class="marks-time">'+
						'<div class="row">'+
							'<div class="col-md-4">'+
								'<div><label for="">Time</label></div>'+
								'<div><i class="fa fa-clock-o"></i> Time Taken: <span>'+questions[i].time+'</span></div>'+
								'<div><i class="fa fa-clock-o"></i> Topper Time: <span>'+questions[i].topper_time+'</span></div>'+
								'<div><i class="fa fa-clock-o"></i> Average Time: <span>'+questions[i].avg_time+'</span></div>'+
								((questions[i].shortest_time!=0)?('<div><i class="fa fa-clock-o"></i> Shortest Time: <span>'+questions[i].shortest_time+'</span></div>'):(''))+
							'</div>'+
							'<div class="col-md-4">'+
								'<div><label for="">Marks</label></div>'+
								'<div><i class="fa fa-check-square-o"></i> Maximum Marks: <span>'+questions[i].max_marks+'</span></div>'+
								'<div><i class="fa fa-check-square-o"></i> Topper Marks: <span>'+questions[i].topper_marks+'</span></div>'+
								'<div><i class="fa fa-check-square-o"></i> Average Marks: <span>'+questions[i].avg_marks+'</span></div>'+
							'</div>'+
							'<div class="col-md-4">'+
								'<label for="">Assign Marks:</label>'+
								((questions[i].attempted==0)?('<div class="text-danger">Not attempted</div>'):(
								'<div class="input-group">'+
									'<input type="text" class="form-control" value="'+((questions[i].check==0)?'':questions[i].marks)+'">'+
									'<span class="input-group-btn">'+
										'<button class="btn btn-primary js-assign-marks" data-question="'+questions[i].id+'" data-max="'+questions[i].max_marks+'">Set</button>'+
									'</span>'+
								'</div>'))+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>');
				if (questions[i].attempted) {
					if (questions[i]["uploads"].length>0) {
						var uploads = questions[i]["uploads"];
						for (var j = 0; j < uploads.length; j++) {
							//uploads[i]
							$('#bodyQ'+i+' .uploadsbox').append(
								'<div class="answer">'+
									'<div class="row">'+
										'<div class="col-md-6">'+
											'<label for="">Student\'s Answer :</label>'+
											'<div>'+
												((uploads[j].type=="pdf")?('<a href="javascript:void(0)" data-path="'+uploads[j].path+'" data-type="'+uploads[j].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (PDF Format)</a>'):((uploads[j].type=="jpg" || uploads[j].type=="jpeg" || uploads[j].type=="png" || uploads[j].type=="gif")?('<a href="javascript:void(0)" data-path="'+uploads[j].path+'" data-type="'+uploads[j].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (Image Format)</a>'):('<a href="'+uploads[j].path+'" class="text-uppercase" target="_blank">Click to download file ('+uploads[j].type+')</a>')))+
											'</div>'+
										'</div>'+
										'<div class="col-md-6">'+
											'<label for="">Reviews/Comments :</label>'+
											'<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control">'+uploads[j].review+'</textarea></div>'+
											'<div class="form-group">'+
												'<button class="btn btn-info js-answer-review" data-answer="'+uploads[j].id+'">Save Comments</button>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>');
						};
					}
				}
			} else if (questions[i].questionType == "questionGroup") {
				if (questions[i].listType=="one") {
					$('#bodyQ'+i).append('<div class="panel-body">'+
						'<div class="question">'+questions[i].question+'</div>'+
						'<div class="marks-time">'+
							'<div class="row">'+
								'<div class="col-md-4">'+
									'<div><label for="">Time</label></div>'+
									'<div><i class="fa fa-clock-o"></i> Time Taken: <span>'+questions[i].time+'</span></div>'+
									'<div><i class="fa fa-clock-o"></i> Topper Time: <span>'+questions[i].topper_time+'</span></div>'+
									'<div><i class="fa fa-clock-o"></i> Average Time: <span>'+questions[i].avg_time+'</span></div>'+
									((questions[i].shortest_time!=0)?('<div><i class="fa fa-clock-o"></i> Shortest Time: <span>'+questions[i].shortest_time+'</span></div>'):(''))+
								'</div>'+
								'<div class="col-md-4">'+
									'<div><label for="">Marks</label></div>'+
									'<div><i class="fa fa-check-square-o"></i> Maximum Marks: <span>'+questions[i].max_marks+'</span></div>'+
									'<div><i class="fa fa-check-square-o"></i> Topper Marks: <span>'+questions[i].topper_marks+'</span></div>'+
									'<div><i class="fa fa-check-square-o"></i> Average Marks: <span>'+questions[i].avg_marks+'</span></div>'+
								'</div>'+
								'<div class="col-md-4">'+
									'<label for="">Assign Marks:</label>'+
									((questions[i].attempted==0)?('<div class="text-danger">Not attempted</div>'):(
									'<div class="input-group">'+
										'<input type="text" class="form-control" value="'+((questions[i].check==0)?'':questions[i].marks)+'">'+
										'<span class="input-group-btn">'+
											'<button class="btn btn-primary js-assign-marks" data-question="'+questions[i].id+'" data-max="'+questions[i].max_marks+'">Set</button>'+
										'</span>'+
									'</div>'))+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>');
					if (questions[i]["subQuestions"].length>0) {
						$('#bodyQ'+i+' .marks-time').before('<div class="panel-group" id="accQ'+i+'Subquestions" role="tablist" aria-multiselectable="true"></div>');
						var subquestions = questions[i]["subQuestions"];
						for (var j = 0; j < subquestions.length; j++) {
							$('#accQ'+i+'Subquestions').append(
								'<div class="panel panel-default">'+
									'<div class="panel-heading" role="tab" id="headQ'+i+'S'+j+'">'+
										'<h4 class="panel-title">'+
											'<a role="button" data-toggle="collapse" data-parent="#" href="#bodyQ'+i+'S'+j+'" aria-expanded="true" aria-controls="bodyQ'+i+'S'+j+'">'+
												'Subquestion #'+(j+1)+''+
											'</a>'+
										'</h4>'+
									'</div>'+
									'<div id="bodyQ'+i+'S'+j+'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headQ'+i+'S'+j+'">'+
										'<div class="panel-body">'+
											'<div class="question">'+subquestions[j].question+'</div>'+
											'<div class="answer">'+
												'<div class="row">'+
													'<div class="col-md-12">'+
														'<label for="">Correct Answer :</label>'+
														'<div>'+subquestions[j].answer+'</div>'+
													'</div>'+
												'</div>'+
											'</div>'+
											((!subquestions[j].attempted)?('<div class="answer">'+
												'<div class="row">'+
													'<div class="col-md-12">'+
														'<div class="text-danger">Not attempted</div>'+
													'</div>'+
												'</div>'+
											'</div>'):(((!subquestions[j].student_answer)?(''):('<div class="answer">'+
												'<div class="row">'+
													'<div class="col-md-6">'+
														'<label for="">Student\'s Answer :</label>'+
														'<div>'+subquestions[j].student_answer+'</div>'+
													'</div>'+
													'<div class="col-md-6">'+
														'<label for="">Reviews/Comments :</label>'+
														'<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control">'+subquestions[j].review+'</textarea></div>'+
														'<div class="form-group">'+
															'<button class="btn btn-info js-answer-review" data-question="'+subquestions[j].id+'">Save Comments</button>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>'))+
											'<div class="uploadsbox"></div>'))+
										'</div>'+
									'</div>'+
								'</div>');
							if (subquestions[j].attempted) {
								if (subquestions[j]["uploads"].length>0) {
									var uploads = subquestions[j]["uploads"];
									for (var k = 0; k < uploads.length; k++) {
										//uploads[i]
										$('#bodyQ'+i+'S'+j+' .uploadsbox').append(
											'<div class="answer">'+
												'<div class="row">'+
													'<div class="col-md-6">'+
														'<label for="">Student\'s Answer :</label>'+
														'<div>'+
															((uploads[k].type=="pdf")?('<a href="javascript:void(0)" data-path="'+uploads[k].path+'" data-type="'+uploads[k].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (PDF Format)</a>'):((uploads[k].type=="jpg" || uploads[k].type=="jpeg" || uploads[k].type=="png" || uploads[k].type=="gif")?('<a href="javascript:void(0)" data-path="'+uploads[k].path+'" data-type="'+uploads[k].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (Image Format)</a>'):('<a href="'+uploads[k].path+'" class="text-uppercase" target="_blank">Click to download file ('+uploads[k].type+')</a>')))+
														'</div>'+
													'</div>'+
													'<div class="col-md-6">'+
														'<label for="">Reviews/Comments :</label>'+
														'<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control">'+uploads[k].review+'</textarea></div>'+
														'<div class="form-group">'+
															'<button class="btn btn-info js-answer-review" data-answer="'+uploads[k].id+'">Save Comments</button>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>');
									};
								}
							}
						};
					}
				} else {
					$('#bodyQ'+i).append('<div class="panel-body"></div>');
					if (questions[i]["subQuestions"].length>0) {
						$('#bodyQ'+i+' .panel-body').append('<div class="panel-group" id="accQ'+i+'Subquestions" role="tablist" aria-multiselectable="true"></div>');
						var subquestions = questions[i]["subQuestions"];
						for (var j = 0; j < subquestions.length; j++) {
							$('#accQ'+i+'Subquestions').append(
								'<div class="panel panel-default">'+
									'<div class="panel-heading" role="tab" id="headQ'+i+'S'+j+'">'+
										'<h4 class="panel-title">'+
											'<a role="button" data-toggle="collapse" data-parent="#" href="#bodyQ'+i+'S'+j+'" aria-expanded="true" aria-controls="bodyQ'+i+'S'+j+'">'+
												'Subquestion #'+(j+1)+''+
											'</a>'+
										'</h4>'+
									'</div>'+
									'<div id="bodyQ'+i+'S'+j+'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headQ'+i+'S'+j+'">'+
										'<div class="panel-body">'+
											'<div class="question">'+subquestions[j].question+'</div>'+
											'<div class="answer">'+
												'<div class="row">'+
													'<div class="col-md-12">'+
														'<label for="">Correct Answer :</label>'+
														'<div>'+subquestions[j].answer+'</div>'+
													'</div>'+
												'</div>'+
											'</div>'+
											((!subquestions[j].attempted)?('<div class="answer">'+
												'<div class="row">'+
													'<div class="col-md-12">'+
														'<div class="text-danger">Not attempted</div>'+
													'</div>'+
												'</div>'+
											'</div>'):(((!subquestions[j].student_answer)?(''):('<div class="answer">'+
												'<div class="row">'+
													'<div class="col-md-6">'+
														'<label for="">Student\'s Answer :</label>'+
														'<div>'+subquestions[j].student_answer+'</div>'+
													'</div>'+
													'<div class="col-md-6">'+
														'<label for="">Reviews/Comments :</label>'+
														'<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control">'+subquestions[j].review+'</textarea></div>'+
														'<div class="form-group">'+
															'<button class="btn btn-info js-answer-review" data-question="'+subquestions[j].id+'">Save Comments</button>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>'))+
											'<div class="uploadsbox"></div>'))+
											'<div class="marks-time">'+
												'<div class="row">'+
													'<div class="col-md-4">'+
														'<div><label for="">Time</label></div>'+
														'<div><i class="fa fa-clock-o"></i> Time Taken: <span>'+subquestions[j].time+'</span></div>'+
														'<div><i class="fa fa-clock-o"></i> Topper Time: <span>'+subquestions[j].topper_time+'</span></div>'+
														'<div><i class="fa fa-clock-o"></i> Average Time: <span>'+subquestions[j].avg_time+'</span></div>'+
														((subquestions[j].shortest_time!=0)?('<div><i class="fa fa-clock-o"></i> Shortest Time: <span>'+subquestions[j].shortest_time+'</span></div>'):(''))+
													'</div>'+
													'<div class="col-md-4">'+
														'<div><label for="">Marks</label></div>'+
														'<div><i class="fa fa-check-square-o"></i> Maximum Marks: <span>'+subquestions[j].max_marks+'</span></div>'+
														'<div><i class="fa fa-check-square-o"></i> Topper Marks: <span>'+subquestions[j].topper_marks+'</span></div>'+
														'<div><i class="fa fa-check-square-o"></i> Average Marks: <span>'+subquestions[j].avg_marks+'</span></div>'+
													'</div>'+
													'<div class="col-md-4">'+
														'<label for="">Assign Marks:</label>'+
														((subquestions[j].attempted==0)?('<div class="text-danger">Not attempted</div>'):(
														'<div class="input-group">'+
															'<input type="text" class="form-control" value="'+((subquestions[j].check==0)?'':subquestions[j].marks)+'">'+
															'<span class="input-group-btn">'+
																'<button class="btn btn-primary js-assign-marks" data-question="'+subquestions[j].id+'" data-max="'+subquestions[j].max_marks+'">Set</button>'+
															'</span>'+
														'</div>'))+
													'</div>'+
												'</div>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>');
							if (subquestions[j].attempted) {
								if (subquestions[j]["uploads"].length>0) {
									var uploads = subquestions[j]["uploads"];
									for (var k = 0; k < uploads.length; k++) {
										//uploads[i]
										$('#bodyQ'+i+'S'+j+' .uploadsbox').append(
											'<div class="answer">'+
												'<div class="row">'+
													'<div class="col-md-6">'+
														'<label for="">Student\'s Answer :</label>'+
														'<div>'+
															((uploads[k].type=="pdf")?('<a href="javascript:void(0)" data-path="'+uploads[k].path+'" data-type="'+uploads[k].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (PDF Format)</a>'):((uploads[k].type=="jpg" || uploads[k].type=="jpeg" || uploads[k].type=="png" || uploads[k].type=="gif")?('<a href="javascript:void(0)" data-path="'+uploads[k].path+'" data-type="'+uploads[k].type+'" class="btn btn-warning js-popup-answer">Checkout the answer (Image Format)</a>'):('<a href="'+uploads[k].path+'" class="text-uppercase" target="_blank">Click to download file ('+uploads[k].type+')</a>')))+
														'</div>'+
													'</div>'+
													'<div class="col-md-6">'+
														'<label for="">Reviews/Comments :</label>'+
														'<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control">'+uploads[k].review+'</textarea></div>'+
														'<div class="form-group">'+
															'<button class="btn btn-info js-answer-review" data-answer="'+uploads[k].id+'">Save Comments</button>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>');
									};
								}
							}
						};
					}
				}
			}
		};
	};
}