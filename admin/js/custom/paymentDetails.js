var ApiEndPoint = '../api/index.php';
var details;
var currency=1;
var price=0.00;
var courseId= getUrlParameter('courseId');
var licenseLevel=getUrlParameter('license');
var licenseType=["One Year Licensing","One Time Transfer","IP Transfer","Commission based licensing"];
$(function() {
	
	
        checkCurrency();
        
	//function to fetch notifications
	function fetchPurchasingDetails() {
		var req = {};
		var res;
		req.action = 'get-course-details-instituteEnd';
		req.courseId = courseId;
		req.licensingLevel = getUrlParameter('license');
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alertMsg(res.message);
			else {
				details = res;
				fillPurchasingDetails(res);
			}
		});
	}
	
	//function to fill notifications table
	function fillPurchasingDetails(data) {
		var html = '';
                 var displayCurrency='<i class="fa fa-dollar"></i>';
                 var flag=0;
                 	$.each(data.courseLicensing, function(i, license1) {
                           if(license1.licensingType==licenseLevel) {
                              if(currency==2){
                              displayCurrency='<i class="fa fa-rupee"></i>';
                               price=license1.priceINR;
                           }else{
                                 price=license1.priceUSD;
                            }   
                      }
                });
                    $('#notify-list').find('li').remove();
                   html += '<li data-id="'+data.courseDetails.id+'">'
					+ '<div class="task-title">'
						+ '<span class="task-title-sp">The Course  <b> '+data.courseDetails.name+' of price   '+displayCurrency+price+' of license type '+licenseType[licenseLevel-1]+'</b> </span>'
						+ '<div class="pull-right hidden-phone">'
							+ '<a id="purchase-button" class="btn btn-success btn-xs mark-notification"><i class=" fa fa-shopping-cart"></i>&nbsp;Purchase</a>&emsp;'
                                        	+ '</div>'
					+ '</div>'
				+ '</li>';
		if(html == '')
		html = 'No new invitations found.';
		$('#notify-list').append(html);

		//$('#notify-list').append(html);
		
              //adding event handlers for accept button
		$('#purchase-button').on('click', function() {
			var id = $(this).parents('li').attr('data-id');
			var req = {};
			var res;
			req.action = 'puchase-copy-course-Institute';
			req.courseId = id;
			req.LicensingLevel=licenseLevel;
			$.ajax({
					'type'	:	'post',
					'url'	:	ApiEndPoint,
					'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				console.log(res);
				window.location = 'courses.php';
			});
		});
		
		//adding event handlers for reject button
		$('.reject-button').on('click', function() {
			var id = $(this).parents('li').attr('data-id');
			var req = {};
			var res;
			req.action = 'rejected_student_invitation';
			req.link_id = id;
			$.ajax({
					'type'	:	'post',
					'url'	:	ApiEndPoint,
					'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else {
					alertMsg("Invitation rejected successfully.");
					fetchNotifications();
				}
			});
		});
	}
          function checkCurrency(){
                 var req = {};
                 var res;
        	req.action = 'get-currency';
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status==1){
                           currency=res.currency[0].currency;//'<i class="fa fa-rupee"></i>';
                       } 
                      fetchPurchasingDetails();
		});
  
}

});