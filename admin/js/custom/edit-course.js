/*
 Author: Fraaz Hashmi.
 Dated: 6/26/2014
 ********
 1) Notes: JSON.stringify support for IE < 10
 */
var UserCurrency = 2;
var key_rate = "";
var display_current_currency = '$';
var userRole;
var newSelectedInstitutes = {};
var courseSubject = {};
var courseImageChanged = false;
var reorderTimer = null;
var  details= {};
//to check student association
var assoc = false;
var startDate = new Date(1990, 1, 01);
var endDate = new Date(2099, 1, 25);
var flagForImport = false;
var importCourse;

$(function () {
    //events for demo video
    $('#video-upload-modal .courseId').val(getUrlParameter('courseId'));
    $('#video-upload-modal .upload-file').on('click', function() {
        $('#video-upload-modal .inputFile').click();
    });

    $('#video-upload-modal .inputFile').on('change', function() {
        $('#video-upload-modal .upload-options').hide();
        $('#video-upload-modal .upload-working').show();
        $('#video-upload-modal').find('form').submit();
    });

    $('#btnGenStudents').click(function(){
        $('#frmGenStudents .help-block').html("");
        var inputGenStudents = $('#inputGenStudents').val();
        if (!$.isNumeric(inputGenStudents)) {
            $('#frmGenStudents .help-block').html("Please enter a number!");
        } else if (inputGenStudents<1) {
            $('#frmGenStudents .help-block').html("Please enter a number greater than 0!");
        } else {
            var req = {};
            var res;
            req.action = 'generate-students-for-course';
            req.students = inputGenStudents;
            req.courseId = getUrlParameter('courseId');
            $.ajax({
                'type'  :   'post',
                'url'   :   ApiEndpoint,
                'data'  :   JSON.stringify(req)
            }).done(function(res) {
                res = $.parseJSON(res);
                if(res.status == 0)
                    alert(res.message);
                else {
                    console.log(res);
                    window.location = window.location;
                }
            });
        }
    });

    $('#btnExportStudents').attr("href", "export-students.php?courseId="+getUrlParameter('courseId'));

    $('.btn-add-students').click(function() {
        keys_available = get_keys_remaining();
        //console.log(keys_avilable);
        //console.log(keys_pending);
        $('#availableKeys').html("<big>"+keys_avilable+"</big>");
        $('#addStudentsModal').modal("show");
        return false;
    });

    $('#video-upload-modal').find('form').attr('action', fileApiEndpoint).ajaxForm({
        beforeSend: function(arr, $form, data) {
            console.log('starting');
        },
        uploadProgress: function(event, position, total, percentComplete) {
            //Progress bar
            var progress = $('#video-upload-modal').find('.progress')
            progress.find('.progress-bar').width(percentComplete + '%') //update progressbar percent complete
            progress.find('.sr-only').html(percentComplete + '%') //update progressbar percent complete
        },
        success: function() {
            
        },
        complete: function(resp) {
            res = $.parseJSON(resp.responseText);
            if(res.status == 1)
                location.reload();
            else
                alertMsg('An error occured. Please refresh the page and try again. Error Details: ' + res.message);
        },
        error: function() {
            console.log('Error');
        }
    });

    $('#video-upload-modal .delete-file').on('click', function() {
        var req = {};
        var res;
        req.action = 'delete-demo-video';
        req.courseId = getUrlParameter('courseId');
        $.ajax({
            type: 'post',
            url: ApiEndpoint,
            data: JSON.stringify(req)   
        }).done(function(res) {
            res = $.parseJSON(res);
            if(res.status == 1)
                location.reload();
            else
                alert(res.message);
        });
    });

	$('#viewAssignSubs').on('click', function() {
		window.location = 'assignSubs.php?courseId=' + getUrlParameter('courseId');
	});

    //view review button listener
    $('#viewReview').on('click', function() {
        window.location = 'rating.php?courseId=' + getUrlParameter('courseId');
    });

	//disabling user input in dates
	$('#live-date, #end-date').on('keypress', function() {
		return false;
	});
    var imageRoot = '/';
    $('#loader_outer').hide();
        //even handler to check duplicate course name
	$('#edit-form #course-name').on('blur', function() {
		var req = {};
		var res;
		if($($('#edit-form #course-name').parents('div:eq(0)')).hasClass('has-error'))
			unsetError($('#edit-form #course-name'));
		req.action = 'check-duplicate-course';
		req.name = $('#edit-form #course-name').val();
		req.id = getUrlParameter('courseId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else if(res.available == false) {
				setError($('#edit-form #course-name'), 'Please select a different name as you have already created a course by this name.');
			}
		});
	});
	
	$('.custom_img').hover(function() {
		$(this).find('.upload').show();
        $(this).css('margin-bottom', '-15px');
	},function(){
		$(this).find('.upload').hide();
        $(this).css('margin-bottom', '20px');
	});
	
	$('#upload-courseDoc-modal .courseId').val(getUrlParameter('courseId'));
    $('#upload-courseDoc-modal .upload-file').on('click', function() {
        $('#upload-courseDoc-modal .inputFile').click();
    });

    $('#upload-courseDoc-modal .inputFile').on('change', function() {
        $('#upload-courseDoc-modal .upload-options').hide();
        $('#upload-courseDoc-modal .upload-working').show();
		var file = this.files[0];
        //console.log(file.type);
		if(file.type =='application/pdf'){
			$('#upload-courseDoc-modal').find('form').submit();
		}
		else{
			alert("Please Uplaod Pdf files only");
			$('#upload-courseDoc-modal .upload-options').show();
        	$('#upload-courseDoc-modal .upload-working').hide();
		}
    });  

    $('#upload-courseDoc-modal').find('form').attr('action', fileApiEndpoint).ajaxForm({
        beforeSend: function(arr, $form, data) {
            console.log('starting');
        },
        uploadProgress: function(event, position, total, percentComplete) {
            //Progress bar
            var progress = $('#upload-courseDoc-modal').find('.progress')
            progress.find('.progress-bar').width(percentComplete + '%') //update progressbar percent complete
            progress.find('.sr-only').html(percentComplete + '%') //update progressbar percent complete
        },
        success: function() {
            
        },
        complete: function(resp) {
            res = $.parseJSON(resp.responseText);
            if(res.status == 1)
                location.reload();
            else
                alertMsg('An error occured. Please refresh the page and try again. Error Details: ' + res.message);
        },
        error: function() {
            console.log('Error');
        }
    });
	
    function fetchProfileDetails() {
        var req = {};
        var res;
        req.action = "get-profile-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillProfileDetails(res);
        });
    }

    function fillProfileDetails(data) {
        // General Details
        var imageRoot = 'user-data/images/';
        $('#user-profile-card .username').text(data.loginDetails.username);

        if (data.profileDetails.profilePic != "") {
            $('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
        }
         // Role Specific
        userRole = data.userRole;
        if (data.userRole == '1') {
            fillInstituteDetails(data);
        }
        else if (data.userRole == '2') {
            fillProfessorDetails(data);
        }
        else if (data.userRole == '3') {
            fillPublisherDetails(data);
        }
    }

    function fetchCourseDetails() {
        var req = {};
        var res;
        req.courseId = getUrlParameter('courseId');

        if (req.courseId == "" || req.courseId == null) {
            window.location.href = "profile.php";
            return;
        }
        req.action = "get-course-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillCourseDetails(res);
            details=res;
            
        });
    }

    function fillCourseDetails(data) {
		$('#resultResend').hide();
        //filling demo video
        if(data.courseDetails.demoVideo != '') {
            $('[href="#video-upload-modal"]').text('Edit Demo Video').show();
            $('#video-upload-modal .delete-file').show();
            $('.hiddenDemoFlag').val(data.courseDetails.demoVideo);
            $('.flow source').attr('src', data.courseDetails.demoVideo);
            $('.flow').show();
            $('.flow .player').flowplayer();
        }
        $('[href="#video-upload-modal"]').css('display', 'inline-block');

        //filling ratings
        if(data.courseDetails.rating.total == 0)
            rating = 0;
        else
            rating = parseFloat(data.courseDetails.rating.rating).toFixed(1);
        $('#ratingTotal').text(data.courseDetails.rating.total);
        $('#rating').rating('rate', rating);

        // General Details
        if (data.courseDetails.deleted == 1)
            alertMsg('This course has been deleted!!', -1);
        var imageRoot = 'user-data/images/';
        var aSM, aCM, licensingLevel;
        var catIds = [], catNames = [];
        var catName=[];
        $('.add-subject-link').attr('href', 'add-subject.php?courseId=' + data.courseDetails.id);
        $('#course-name').html(data.courseDetails.name);
        $('#edit-form #course-name').val(data.courseDetails.name);

        //BCT
        $('.breadcrumbs .course-name').html(data.courseDetails.name).attr('href', 'edit-course.php?courseId=' + data.courseDetails.id);
        // Approval Link
        if (data.courseDetails.approved == -1) {
            $('.request-approval-link').show();
        } else if (data.courseDetails.approved == 0) {
            $('.request-approval-link').removeClass('btn-success').addClass('btn-warning').unbind('click').html('<i class="fa fa-user"></i> Pending');
            $('.request-approval-link').show();
            $('.request-approval-link').removeClass('request-approval-link');
           
        } else if (data.courseDetails.approved == 1) {
            $('.market-place-link').show();
            //$('#avail-student, #avail-professor').show();

        } else if (data.courseDetails.approved == 2) {
            $('.request-approval-link').removeClass('btn-success').addClass('btn-danger').unbind('click').html('<i class="fa fa-user"></i> Resend');
            $('.request-approval-link').show();
        	$('#resultResend').show();
        }
        $('#course-id').html("C" + String("000" + data.courseDetails.id).slice(-4));


        if (data.courseDetails.subtitle != '') {
            $('#course-subtitle').html(data.courseDetails.subtitle);
            $('#edit-form  #course-subtitle').val(data.courseDetails.subtitle);
        }

        if (data.courseDetails.targetAudience != '') {
            $('#target-audience').html(data.courseDetails.targetAudience);
            $('#edit-form #target-audience').val(data.courseDetails.targetAudience);
        }
		$('#edit-form #tags').val(data.courseDetails.tags);
        if (data.courseDetails.description != '') {
              var showdata=data.courseDetails.description.replace(new RegExp('\r?\n','g'), '<br />');
            $('#course-description').html(showdata);
            $('#edit-form #course-description').val(data.courseDetails.description);
        }
        startDate = new Date(parseInt(data.courseDetails.liveDate));
        $('#live-date').html(startDate.getDate()+ ' ' +MONTH[startDate.getMonth()]+ ' ' + startDate.getFullYear());
        $('#edit-form #live-date').val(startDate.getDate()+ ' ' +MONTH[startDate.getMonth()]+ ' ' + startDate.getFullYear());
       
        $('#end-date').html('- - - - ');
        if (data.courseDetails.endDate != '') {
			endDate = new Date(parseInt(data.courseDetails.endDate));
            $('#end-date').html(endDate.getDate()+ ' ' +MONTH[endDate.getMonth()]+ ' ' + endDate.getFullYear());
            $('#edit-form #end-date').val(endDate.getDate()+ ' ' +MONTH[endDate.getMonth()]+ ' ' + endDate.getFullYear());
            $('#edit-form #set-end-date').val(1);
            $('#edit-form #end-date').parents('.form-group').show();
        }

        if (data.courseDetails.image == '') {
            var image = "img/course-image-0-uid.jpg";
        } else {
            var image = data.courseDetails.image;
        }

        $('#course-image-preview').attr('src', image);
        $('#image-preview').attr('src', image);
		
		
        $.each(data.courseCategories, function (k, cat) {
            //catNames.push(cat.catName);
            catIds.push(cat.categoryId);
            catName.push(cat.category);
        });
        $('#course-categ').html(catName.join(', '));
        //$('#edit-form #course-cat').selectpicker('val', catIds)
        $('#edit-form #course-cat').multiselect('select', catIds)
        if (data.courseDetails.availStudentMarket == 1) {
            aSM = "Yes";
            $('#student-price').parents('.hidden-on-load').show();
            $('#edit-form #avail-student-market').val(1);
            $('#edit-form #student-price').parent().show();
			$('#availStudentMarket').prop('checked', true);
			$('#live-course-student').attr('disabled', false);
        } else {
            aSM = "No";
			$('#availStudentMarket').prop('checked',false);
        }
        if (data.courseDetails.availContentMarket == 1) {
            aCM = "Yes";
            $('#licensing-level').parents('.hidden-on-load').show();
            $('#edit-form #avail-content-market').val(1);
            $('#edit-form #licensing-level').parent().show();
			$('#availContentMarket').prop('checked', true);
			$('#liveForContentMarket').attr('disabled', false);
        } else {
			$('#availContentMarket').prop('checked', false);
            aCM = "No";
        }
        $('#avail-student-market').html(aSM);
        $('#avail-content-market').html(aCM);
        $('#student-price').html(data.courseDetails.studentPrice);
        $('#edit-form #student-price').val(data.courseDetails.studentPrice);

        if (data.courseDetails.licensingLevel == 1) {
            licensingLevel = 'Subject Level';
        } else if (data.courseDetails.licensingLevel == 2) {
            licensingLevel = 'Course Level';
        }
        $('#licensing-level').html(licensingLevel);
        $('#edit-form #licensing-level').val(data.courseDetails.licensingLevel);

        if (data.courseDetails.licensingLevel == 2) {
            $('#licensing-link').parents('.form-group').show();
        }
        $('#student-price-inr').val( data.courseDetails.studentPriceINR);
        $('#student-price-dollar').val(data.courseDetails.studentPrice);
        
        // Licensing Options
        //$('#apply-licensing').modal('show');
        $.each(data.courseLicensing, function (i, v) {
            if (v.licenseId == 1) {
                $('#apply-licensing #lic-opt-1').prop('checked', true);
                $('#apply-licensing #lic-opt-1-price').val(v.price).parents('.form-group').show();

            }
            if (v.licenseId == 2) {
                $('#apply-licensing #lic-opt-2').prop('checked', true);
                $('#apply-licensing #lic-opt-2-price').val(v.price).parents('.form-group').show();

            }
            if (v.licenseId == 3) {
                $('#apply-licensing #lic-opt-3').prop('checked', true)
                $('#apply-licensing #lic-opt-3-price').val(v.price).parents('.form-group').show();

            }
            if (v.licenseId == 4) {
                $('#apply-licensing #lic-opt-4').prop('checked', true)
                data = $.parseJSON(v.data);
                $('#apply-licensing #lic-opt-4-comm').val(data.amount);
                $('#apply-licensing #lic-opt-4-comm-per').val(data.percent);
                $('#apply-licensing #lic-opt-4-price').val(v.price).parents('.form-group').show();
            }
        });
        addEventHandlersForCourse();
    }

    function fillInstituteDetails(data) {
        $('h4#profile-name').text(data.profileDetails.name);
    }

    function fillProfessorDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('a#profile-name').text(name);
    }

    function fillPublisherDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('a#profile-name').text(name);
    }

    function fetchSubjects() {
        var req = {};
        var res;
        req.courseId = getUrlParameter('courseId');
        if (req.courseId == "" || req.courseId == null) {
            window.location.href = "profile.php";
            return;
        }
        req.action = "get-course-subjects";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillSubjects(res);
        });
    }

    function fillSubjects(data) {
        var html = "";
        var flag = false;
        var editLink;
        var deletedClass = "";
		if(data.subjects.length >1)
		{	
			$('.assignSub').show();			
		}
        $.each(data.subjects, function (i, subject) {
            if (flag == false)
                flag = true;
            deletedClass = "";
            if (subject.deleted == 1) {
                deletedClass = " deleted-item";
            }

            //console.log(deletedClass);
            editLink = "edit-subject.php?courseId=" + subject.courseId + "&subjectId=" + subject.id;
            html += "<tr class='subject" + deletedClass + "' data-sid='" + subject.id + "'>"
                    + "<td><a href='" + editLink + "'>" + subject.name + "</a>";
            /*$.each(subject.professors, function (i, prof) {
                html += "&nbsp; &nbsp;<i class='fa fa-user'></i> " + prof.firstName + " " + prof.lastName
                        + "&nbsp; &nbsp;<a href='#' class='remove-professor' data-pid='" + prof.userId + "'><i class='fa fa-trash-o'></i></a><br>";
            });

            html += "<a href='" + editLink + "#manage'>&nbsp; &nbsp;Assign New Professor</a></td>"*/
            html += "<td><br></td>"
                    + "<td>S" + String("000" + subject.id).slice(-4) + "</td>";

            if (subject.deleted == 0) {
                html += "<td><a href='#' class='delete-subject'><i class='fa fa-trash-o'></i></a>"
                        + "<a href='#' class='restore-subject hidden-on-load'><i class='fa fa-refresh'></i></a></td>";
            } else {
                html += "<td><a href='#' class='delete-subject hidden-on-load'><i class='fa fa-trash-o'></i></a>"
                        + "<a href='#' class='restore-subject'><i class='fa fa-refresh'></i></a></td>";
            }
            html += "</tr>";
        });

        if (flag == true) {
            $('#existing-subjects tbody').html(html);
            addEventHandlersForSubjects();
            deleteProfessorEventHandler();
            makeSubjectsSortable('#existing-subjects tbody');
        }else{
             $('#existing-subjects tbody').html('<tr><td>No Subjects added yet</td></tr>');
        }
    }
	
	//event handler for cancel button
	$('#import-content-modal').on('hidden.bs.modal', function() {
		$('.course-subjects').hide();
		$('input[type="checkbox"]').prop('checked', false);
		$('.existing-subject-import-form').hide();
	});

    function fetchCoursesForImport() {
        var req = {};
        var res;
        req.courseId = getUrlParameter('courseId');
        if (req.courseId == "" || req.courseId == null) {
            window.location.href = "profile.php";
            return;
        }
        req.action = "get-courses-for-import";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
           // console.log(res);
		   flagForImport = true;
		   importCourse = res;
           fillCoursesForImport(importCourse);
        });
    }

    function fillCoursesForImport(data) {
        var html = "";
        var flag = false;
        var editCourseLink, editSubjectLink, addSubjectLink;
        var courseSubject = {};
        if (typeof data.courses == "undefined" ){
            $('#import-existing-course .dyn-data').html("There are no courses available to import");
            $('#import-existing-course .dyn-data').prev('div').hide();
            $('#import-existing-course .dyn-data').next('div').hide();
            return false;
        }
         if (data.courses.length == 0 ){
            $('#import-existing-course .dyn-data').html("There are no courses available to import");
            $('#import-existing-course .dyn-data').prev('div').hide();
            $('#import-existing-course .dyn-data').next('div').hide();
            return false;
        }
        $.each(data.courses, function (i, course) {
            if (flag == false)
                flag = true;
            courseSubject[course.id] = [];
            var expirydate='No Expiry';
             if(course.endDate!=""){
            var endDate = new Date(parseInt(course.endDate));
             expirydate=endDate.getDate()+ ' ' +MONTH[endDate.getMonth()]+ ' ' + endDate.getFullYear();
             }
             html += "<div class='row entry course course-" + course.id + "' data-id='" + course.id + "'>"
                    + "<div class='col-xs-4 checkbox name'>"
                    + "<label>"
                    + "<input type='checkbox' value='1' />"
                    + course.name
                    + "</label>"
                    + "</div>"
                    + "<div class='col-xs-4 expiry-date'>"+expirydate+"</div>"
                    + "<div class='col-xs-4 license'>Licenced</div>"
                    + "</div>"
                    + "<div class='course-subjects course-" + course.id + "'>";
            $.each(course.subjects, function (i, subject) {
                courseSubject[course.id].push(subject.id);
                html += "<div class='row entry subject subject-" + subject.id + "' data-id='" + subject.id + "'>";
                if (subject.alreadyImported == 0) {
                    html += "<div class='col-xs-4 checkbox name'>"
                            + "<label><input type='checkbox' value='1' />";
                } else {
                    html += "<div class='col-xs-4 checkbox name tooltips' data-placement='top' data-toggle='tooltip' data-original-title='This subject has been already imported in this course.'>"
                            + "<label>"
                            + "<input type='checkbox' value='1' disabled='disabled' readonly='readonly'/>";
                }
                html += subject.name
                        + "</label>"
                        + "</div>"
                        + "<div class='col-xs-4 expiry-date'>"+expirydate+"</div>"
                        + "<div class='col-xs-4 license'>Licenced</div>"
                        + "</div>";
            });
            html += "</div>";

        });

        if (flag == true) {
            $('#import-existing-course .dyn-data').html(html);
            fitEventHandlersForImportExistingSubjects();
            $('.tooltips').tooltip();
        }
    }

    function fitEventHandlersForImportExistingSubjects() {
        $('#import-existing-course .dyn-data .course input[type=checkbox]').on('click', function () {
            var currCourseId = $(this).parents('.course').attr('data-id');
            if ($(this).prop('checked') == true) {
                $('#import-existing-course .dyn-data .course-subjects.course-' + currCourseId).show();
                $('#import-existing-course .dyn-data .course-subjects.course-' + currCourseId)
                        .find('input[type=checkbox]').prop('checked', true);
            } else {
                $('#import-existing-course .dyn-data .course-subjects.course-' + currCourseId).hide();
                $('#import-existing-course .dyn-data .course-subjects.course-' + currCourseId)
                        .find('input[type=checkbox]').prop('checked', false);
            }

        });
    }
    
    function fetchPurchasedCoursesForImport() {
        var req = {};
        var res;
        req.courseId = getUrlParameter('courseId');
        if (req.courseId == "" || req.courseId == null) {
            window.location.href = "profile.php";
            return;
        }
        req.action = "get-purchased-courses-for-import";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
           // console.log(res);
            fillPurchasedCoursesForImport(res);
        });
    }

    function fillPurchasedCoursesForImport(data) {
        var html = "";
        var flag = false;
        var editCourseLink, editSubjectLink, addSubjectLink;
        var courseSubject = {};
        if (typeof data.courses == "undefined" ){
            $('#import-purchased-course .dyn-data').html("There are no courses available to import");
            $('#import-purchased-course .dyn-data').prev('div').hide();
            $('#import-purchased-course .dyn-data').next('div').hide();
            return false;
        }
         if (data.courses.length == 0 ){
            $('#import-purchased-course .dyn-data').html("There are no courses available to import");
            $('#import-purchased-course .dyn-data').prev('div').hide();
            $('#import-purchased-course .dyn-data').next('div').hide();
            return false;
        }
        $.each(data.courses, function (i, course) {
            if (flag == false)
                flag = true;
			var dis = '';
			if(new Date(parseInt(course.endDate)).valueOf() <= new Date().valueOf())
				dis = 'DISABLED';
            courseSubject[course.id] = [];
            var expirydate='No Expiry';
             if(course.endDate!=""){
            var endDate = new Date(parseInt(course.endDate));
             expirydate=endDate.getDate()+ ' ' +MONTH[endDate.getMonth()]+ ' ' + endDate.getFullYear();
             }
             html += "<div class='row entry course course-" + course.id + "' data-id='" + course.id + "'>"
                    + "<div class='col-xs-4 checkbox name'>"
                    + "<label>"
                    + "<input type='checkbox' value='1' "+dis+"/>"
                    + course.name
                    + "</label>"
                    + "</div>"
                    + "<div class='col-xs-4 expiry-date'>"+expirydate+"</div>"
                    + "<div class='col-xs-4 license'>Licenced</div>"
                    + "</div>"
                    + "<div class='course-subjects course-" + course.id + "'>";
            $.each(course.subjects, function (i, subject) {
                courseSubject[course.id].push(subject.id);
                html += "<div class='row entry subject subject-" + subject.id + "' data-id='" + subject.id + "'>";
                if (subject.alreadyImported == 0) {
                    html += "<div class='col-xs-4 checkbox name'>"
                            + "<label><input type='checkbox' value='1' "+dis+"/>";
                } else {
                    html += "<div class='col-xs-4 checkbox name tooltips' data-placement='top' data-toggle='tooltip' data-original-title='This subject has been already imported in this course.'>"
                            + "<label>"
                            + "<input type='checkbox' value='1' disabled='disabled' readonly='readonly'/>";
                }
                html += subject.name
                        + "</label>"
                        + "</div>"
                        + "<div class='col-xs-4 expiry-date'>"+expirydate+"</div>"
                        + "<div class='col-xs-4 license'>Licenced</div>"
                        + "</div>";
            });
            html += "</div>";

        });

        if (flag == true) {
            $('#import-purchased-course .dyn-data').html(html);
            fitEventHandlersForImportPurchasedSubjects();
            $('.tooltips').tooltip();
        }
    }

    function fitEventHandlersForImportPurchasedSubjects() {
        $('#import-purchased-course .dyn-data .course input[type=checkbox]').on('click', function () {
            var currCourseId = $(this).parents('.course').attr('data-id');
            if ($(this).prop('checked') == true) {
                $('#import-purchased-course .dyn-data .course-subjects.course-' + currCourseId).show();
                $('#import-purchased-course .dyn-data .course-subjects.course-' + currCourseId)
                        .find('input[type=checkbox]').prop('checked', true);
            } else {
                $('#import-purchased-course .dyn-data .course-subjects.course-' + currCourseId).hide();
                $('#import-purchased-course .dyn-data .course-subjects.course-' + currCourseId)
                        .find('input[type=checkbox]').prop('checked', false);
            }

        });
    }

    function formatUserRole(roleId) {
        if (roleId == '1')
            return "Institute";
        if (roleId == '2')
            return "Professor";
        if (roleId == '3')
            return "Publisher";
    }

    function validateForm() {
        unsetError($('#edit-form #course-name'));
        if ($('#edit-form #course-name').val().length < 5 || $('#edit-form #course-name').val().length > 70) {
            setError($('#edit-form #course-name'), 'Invalid course name. Must be between 5 to 70 chars.');
            return false;
        }

        unsetError($('#edit-form #course-subtitle'));
        if ($('#edit-form #course-subtitle').val() == '' || $('#edit-form #course-subtitle').val().length > 140) {
            setError($('#edit-form #course-subtitle'), 'Must be less than 140 chars.');
            return false;
        }

        unsetError($('#edit-form #course-description'));
        if ($('#edit-form #course-description').val() == '' || $('#edit-form #course-description').val().length > 1000) {
            setError($('#edit-form #course-description'), 'Course description should not contain more than 1000 chars');
            return false;
        }
        unsetError($('#target-audience'));
        if ($('#target-audience').val().length < 0 && $('#target-audience').val().length > 200) {
            setError($('#target-audience'), 'Can not be more than 200  chars.');
            return false;
        }
        unsetError($('#edit-form #live-date'));
        if ($('#edit-form #live-date').val() == "") {// || !isValidDate($('#live-date').val())) {
            setError($('#edit-form #live-date'), 'Live date is invalid.');
            return false;
        }

        unsetError($('#edit-form #end-date'));
        if ($('#edit-form #set-end-date').val() == 1) {
			if($('#edit-form #end-date').val() == "") {
				setError($('#edit-form #end-date'), 'End date is invalid.');
				return false;
			}
			else if(assoc == true){
				var end = new Date();
				end.setFullYear(end.getFullYear() + 1);
				endDate = new Date($('#edit-form #end-date').val());
				if(endDate.valueOf() < end.valueOf()) {
					//this is needed to be implemented using the last student's joining date instead of today's date.
					setError($('#edit-form #end-date'), 'Please select a end date after 1 year as their are associtated students.');
					return false;
				}
			}
        }
        if ($('#edit-form #avail-student-market').val() == 1 && ($('#edit-form #student-price').val() == "" || !isValidPrice($('#edit-form #student-price').val()))) {
            $('.form-msg.student-price').html('Price is invalid.').show();
            return false;
        }
        return true;
    }

    function validateLicensingForm() {
        $('.form-msg').html('').hide();
        var licOpt1 = $('#apply-licensing #lic-opt-1').prop('checked');
        var licOpt2 = $('#apply-licensing #lic-opt-2').prop('checked');
        var licOpt3 = $('#apply-licensing #lic-opt-3').prop('checked');
        var licOpt4 = $('#apply-licensing #lic-opt-4').prop('checked');
        if (licOpt1 == false && licOpt2 == false && licOpt3 == false && licOpt4 == false) {
            $('.form-msg.licensing').html('Please apply licensing').show();
            return false;
        }
        var licVal1 = $('#apply-licensing #lic-opt-1-price').val();
        var licVal2 = $('#apply-licensing #lic-opt-2-price').val();
        var licVal3 = $('#apply-licensing #lic-opt-3-price').val();
        var licVal4 = $('#apply-licensing #lic-opt-4-price').val();
        var licCommAmt4 = $('#apply-licensing #lic-opt-4-comm').val();
        var licCommPer4 = $('#apply-licensing #lic-opt-4-comm-per').val();
        if (licOpt1 == true && (licVal1 == "" || !isValidPrice(licVal1))) {
            $('.form-msg.licensing-op-1').html('Please provide price for one year licensing').show();
            return false;
        }
        if (licOpt2 == true && (licVal2 == "" || !isValidPrice(licVal2))) {
            $('.form-msg.licensing-op-2').html('Please provide price for one time transfer').show();
            return false;
        }
        if (licOpt3 == true && (licVal3 == "" || !isValidPrice(licVal3))) {
            $('.form-msg.licensing-op-3').html('Please provide price for selective IP transfer').show();
            return false;
        }
        if (licOpt4 == true && (licVal4 == "" || !isValidPrice(licVal4))) {
            $('.form-msg.licensing-op-4').html('Please provide price for commission based licensing').show();
            return false;
        }
        if (licOpt4 == true && licCommPer4 == "" && licCommAmt4 == "") {
            $('.form-msg.licensing-op-4').html('Please provide commission').show();
            return false;
        }

        if (licOpt4 == true) {
            if (licCommAmt4 != "" && licCommPer4 != "") {
                $('.form-msg.licensing-op-4').html('You can provide either percentage or amount').show();
                return false;
            }
            if (licCommAmt4 != "" && !isValidPrice(licCommAmt4)) {
                $('.form-msg.licensing-op-4').html('Not a valid amount').show();
                return false;
            }
            if (licCommPer4 != "" && (!isValidPrice(licCommPer4) || licCommPer4 > 100)) {
                $('.form-msg.licensing-op-4').html('Not a valid precentage').show();
                return false;
            }
        }
        return true;
    }

    function deleteProfessorEventHandler() {
        $('a.remove-professor').on('click', function (e) {
            e.preventDefault();
            var req = {};
            req.professorId = $(this).attr('data-pid');
            req.subjectId = $(this).parents('tr.subject').attr('data-sid');
            req.action = "delete-subject-professor";
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                fetchSubjects(res);
            });

        });
    }

    function makeSubjectsSortable(selector) {
        $(selector).sortable({
            items: '> tr',
            forcePlaceholderSize: true,
            placeholder: 'sort-placeholder',
            start: function (event, ui) {
                // Build a placeholder cell that spans all the cells in the row
                var cellCount = 0;
                $('td, th', ui.helper).each(function () {
                    // For each TD or TH try and get it's colspan attribute, and add that or 1 to the total
                    var colspan = 1;
                    var colspanAttr = $(this).attr('colspan');
                    if (colspanAttr > 1) {
                        colspan = colspanAttr;
                    }
                    cellCount += colspan;
                });

                // Add the placeholder UI - note that this is the item's content, so TD rather than TR
                ui.placeholder.html('<td colspan="' + cellCount + '">&nbsp;</td>');
                //$(this).attr('data-previndex', ui.item.index());
            },
            update: function (event, ui) {
                // gets the new and old index then removes the temporary attribute
                var newOrder = $.map($(this).find('tr'), function (el) {
                    return $(el).attr('data-sid') + "-" + $(el).index();
                });
                if (reorderTimer != null)
                    clearTimeout(reorderTimer);
                reorderTimer = setTimeout(function () {
                    updateSubjectOrder(newOrder);
                }, 5000);
            },
            helper: function (e, ui) {
                ui.children().each(function () {
                    $(this).width($(this).width());
                });
                return ui;
            }

        }).disableSelection();
    }

    function updateSubjectOrder(newOrder) {
        var order = {};
        $.each(newOrder, function (i, v) {
            order[v.split('-')[0]] = v.split('-')[1];
        });
        var req = {};
        var res;
        req.courseId = getUrlParameter('courseId');
        if (req.courseId == "" || req.courseId == null) {
            window.location.href = "profile.php";
            return;
        }
        req.newOrder = order;
        req.action = "update-subject-order";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1)
                fetchSubjects();
            else
                alertMsg(res.message);
        });
    }

    function fetchCourseStudent_details() {
        var req = {};
        var res;
        req.course_id = getUrlParameter('courseId');
        req.action = "get_students_details_of_course";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            var html = "";
			if(data.students.length == 0) {
				$('#edit-form #live-date').datetimepicker({
					format: 'd F Y',
					timepicker: false,
					closeOnDateSelect: true,
					minDate: 0,
					maxDate: '2050/12/31',
					onSelectDate: function(date) {
						unsetError($('#edit-form #live-date'));
						if(date.valueOf() > endDate.valueOf()) {
							setError($('#edit-form #live-date'), 'The start date can not be greater than the end date.');
						}
						else {
							startDate = date;
						}
					}
				});
			}
			else {
				assoc = true;
				$('#edit-form #live-date').attr('disabled', true);
				$('#edit-form #live-date').parents('div:eq(0)').append('Students are associated with this course hencce you can\'t edit start date of this course');
			}
            for (var i = 0; i < data.students.length; i++)
            {
                var student_id = data.students[i].id;
                var student_username = data.students[i].username;
                var name = data.students[i].name;
                var password = ((data.students[i].password == "")?'<small class="text-success">REGISTERED</small>':data.students[i].password);
                // var contactMobile = data.students[i].contactMobile;
                // var course_key = data.students[i].key_id;
                var email = data.students[i].email;
                var date = data.students[i].date;
                var checkbox = ((!data.students[i].parentid)?'<input type="checkbox" class="js-option-student" />':'<input type="checkbox" class="js-option-student" />');
                var parent = ((!data.students[i].parentid)?'<td></td>':'<td><strong>'+data.students[i].userid+'</strong> / <strong>'+data.students[i].passwd+'</strong></td>');
                html += '<tr data-student="'+student_id+'" data-temp="'+data.students[i].temp+'"><td>' + checkbox + '</td><td>' + student_id + '</td><td>' + student_username + '</td><td>' + password + '</td><td>' + name + '</td><td class="small-txt">' + email + '</td>' + parent + '</tr>';
            }
            $('#tbl_students_details_of_course').append(html);
            if($('#tbl_students_details_of_course tbody tr').length==0){
                $('.existing-students').hide();
            }
            var table = $('#tbl_students_details_of_course').dataTable({
                "aaSorting": [[0, "asc"]],
                "bDestroy": true
            });

        });
    }

    $('#jsRemoveStudents').click(function(){
        tempStudents = "";
        permStudents = "";
        var noOfStudents = 0;
        $('.js-option-student').each(function(key,obj){
            if ($(obj).is(":checked")) {
                if ($(obj).closest('tr').attr("data-temp") == "1") {
                    tempStudents+= $(obj).closest('tr').find('td:nth-child(2)').text()+',';
                } else {
                    permStudents+= $(obj).closest('tr').find('td:nth-child(2)').text()+',';
                }
                noOfStudents++;
            }
        });
        tempStudents = ((tempStudents!="")?(tempStudents.substr(0,tempStudents.length-1)):(""));
        permStudents = ((permStudents!="")?(permStudents.substr(0,permStudents.length-1)):(""));
        
        if (noOfStudents==0) {
            alert("Please select at least 1 student");
        } else {
            var req = {};
            var res;
            req.course_id = getUrlParameter('courseId');
            req.tempStudents  = tempStudents;
            req.permStudents  = permStudents;
            req.action    = "delete-students";
            
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                data = $.parseJSON(res);
                if (data.status == 0) {
                    alert(data.exception);
                } else {
                    for (var i = 0; i < data.students.length; i++) {
                        $('[data-student="'+data.students[i]+'"]').remove();
                        keys_avilable++;
                    };
                    $('#remaining_keys_total').html(keys_avilable);
                }
                console.log(data);
            });
        };
    });

    $('#jsParent').click(function(){
        var students = "";
        $('.js-option-student').each(function(key,obj){
            if ($(obj).is(":checked")) {
                if ($(obj).closest('tr').attr("data-temp") == "0") {
                    students+= $(obj).closest('tr').find('td:nth-child(2)').text()+',';
                }
            }
        });
        students = students.substr(0,students.length-1);
        
        if (!students) {
            alert("Please select at least 1 registered student");
        } else {
            var req = {};
            var res;
            req.course_id = getUrlParameter('courseId');
            req.students  = students;
            req.action    = "generate-parents";
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                data = $.parseJSON(res);
                for (var i = 0; i < data.students.length; i++) {
                    $('[data-student="'+data.students[i].student_id+'"]').find('td:first-child').html('');
                    //$('[data-student="'+data.students[i].student_id+'"]').find('td:nth-child(6)').html(data.students[i].userid);
                    $('[data-student="'+data.students[i].student_id+'"]').find('td:nth-child(7)').html('<strong>'+data.students[i].userid+'</strong> / <strong>'+data.students[i].passwd+'</strong>');
                };
                console.log(data);
            });
        };
    });

    $('.js-select-all').change(function(){
        $(".js-option-student").prop('checked', $(this).prop("checked"));
    });

    //fetchProfileDetails();
    fetchCourseDetails();
    fetchSubjects();
    fetchPurchasedCoursesForImport();
    fetchCourseStudent_details();
    get_keys_remaining();
    fetchCoursekeys();
	fetchLicensingStatus();

	$('#import-content-modal').on('show.bs.modal', function() {
		if(flagForImport)
			fillCoursesForImport(importCourse);
		else
			fetchCoursesForImport();
	});
	function fetchLicensingStatus() {
		var req = {};
		var res;
		req.action = 'fetch-licensing-status';
		req.courseId = getUrlParameter('courseId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alertMsg(res.message);
			else {
				if(res.course.liveForStudent == 0) {
					$('#unlive-course-student').parents('span:eq(0)').hide();
					$('#availStudentMarket').prop('checked', false);
				}
				else {
					$('#live-course-student').hide();
				}
				if(res.course.liveForContentMarket == 0) {
					$('#unliveForContentMarket').parents('span:eq(0)').hide();
					$('#availContentMarket').prop('checked', false);
				}
				else {
					$('#availContentMarket').prop('checked', true);
					$('#liveForContentMarket').hide();
				}
			}
		});
	}
    //Event handlers
    function addEventHandlersForSubjects() {
        $('#existing-subjects .delete-subject').on('click', function (e) {
            e.preventDefault();
            var con = confirm("Are you sure you want to delete this subject?");
            if(con) {
            var subject = $(this).parents('tr');
            var sid = subject.attr('data-sid');
            var req = {};
            req.action = "delete-subject";
            req.subjectId = sid;
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if (res.status == 1) {
                    subject.remove();
                    // subject.find('.delete-subject').hide();
                    // subject.find('.restore-subject').show();
                    // subject.addClass('deleted-item');
                    if ($('#existing-subjects tbody tr').length == 0) {
                        $('#existing-subjects tbody').html('<tr><td> No Subjects added yet </td></tr>');
                        $('.request-approval-link').show();
                        $('.request-approval-link').show();
                        $('.market-place-link').hide();
                        // $('.invite_students').hide();
                    }
                } else {
                    alertMsg(res.message);
                }
            });
        }
        });

        $('#existing-subjects .restore-subject').on('click', function (e) {
            e.preventDefault();
            var subject = $(this).parents('tr');
            var sid = subject.attr('data-sid');
            var req = {};
            req.action = "restore-subject";
            req.subjectId = sid;
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if (res.status == 1) {
                    subject.find('.restore-subject').hide();
                    subject.find('.delete-subject').show();
                    subject.removeClass('deleted-item');
                } else {
                    alertMsg(res.message);
                }
            });
        });
    }

    function addEventHandlersForCourse() {
        $('.request-approval-link').on('click', function () {
            var req = {};
            req.courseId = getUrlParameter('courseId');
            if (req.courseId == "" || req.courseId == null) {
                window.location.href = "profile.php";
                return;
            }
            req.action = "request-course-approval";
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if (res.status == 1) {
                    alertMsg(res.message);
                    $('.request-approval-link').removeClass('btn-success').removeClass('btn-danger').addClass('btn-warning').unbind('click').html('<i class="fa fa-user"></i> Pending');
					$('#resultResend').hide();
                } else {
                    alertMsg(res.message);
                }
            });
        });
    }

    $('#course-details .edit-button').click(function () {
        $('#course-details').hide();
        $('#edit-form').fadeIn('fast');
    });

    $('.cancel-edit-button').click(function () {
		$('.has-error').each(function() {
			$(this).removeClass('has-error');
		});
        $('.help-block').remove();
        $('#edit-form').hide();
        $('#course-details').fadeIn('fast');
        fillCourseDetails(details);
    });
	
	$('#availStudentMarket').on('click', function() {
		if($(this).prop('checked')) {
			$('#live-course-student').attr('disabled', false);
		}
		else
			$('#live-course-student').attr('disabled', true);
	});
	
	$('#availContentMarket').on('click', function() {
		if($(this).prop('checked')) {
			$('#liveForContentMarket').attr('disabled', false);
		}
		else
			$('#liveForContentMarket').attr('disabled', true);
	});

    $('#edit-form #set-end-date').on('change', function () {
        if ($('#edit-form #set-end-date').val() == 0)
            $('#edit-form #end-date').parents('.form-group').hide();
        else
            $('#edit-form #end-date').parents('.form-group').show();
    });

    $('#edit-form #avail-student-market').on('change', function () {
        if ($(this).val() == 0)
            $('#edit-form #student-price').parent().hide();
        else
            $('#edit-form #student-price').parent().show();
    });

    $('#edit-form #avail-content-market').on('change', function () {
        if ($(this).val() == 0) {
            $('#edit-form #licensing-level').parent().hide();
            $('#edit-form #licensing-level').val('1');
            $('#edit-form #licensing-link').parents('.form-group').hide();
        } else {
            $('#edit-form #licensing-level').parent().show();
        }
    });

    $('#edit-form #licensing-level').on('change', function () {
        if ($(this).val() == 1)
            $('#edit-form #licensing-link').parents('.form-group').hide();
        else
            $('#edit-form #licensing-link').parents('.form-group').show();
    });

    $('#edit-form .save-button').on('click', function (e) {
        e.preventDefault();
        if (validateForm() == false)
            return;
        var req = {};
        req.courseId = getUrlParameter('courseId');
        req.courseName = $('#edit-form #course-name').val();
        req.courseSubtitle = $('#edit-form #course-subtitle').val();
        req.courseDesc = $('#edit-form #course-description').val();
        startDate = new Date($('#edit-form #live-date').val());
        req.liveDate = startDate.getTime();
        req.setEndDate = $('#edit-form #set-end-date').val();
        endDate = new Date($('#edit-form #end-date').val());
        req.endDate = endDate.getTime();
        req.courseCateg = $('#edit-form #course-cat').val();
        req.targetAudience = $('#edit-form #target-audience').val();
        req.tags = $('#edit-form #tags').val();
        if (req.courseCateg == null)
            req.courseCateg = [];
        req.action = 'update-course';

        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1) {
                alertMsg(res.message);
                $('#edit-form').hide();
                $('#course-details').fadeIn('fast');
                // if (courseImageChanged)
                    // $('#course-image-form').submit();
                // else
                    fetchCourseDetails();

            } else {
                alertMsg(res.msg);
            }
        });
    });

    $('#apply-licensing button.save').on('click', function (e) {
        e.preventDefault();
        if (validateLicensingForm() == false)
            return;
        var req = {};
        req.courseId = getUrlParameter('courseId');
        var licOpt1 = $('#apply-licensing #lic-opt-1').prop('checked');
        var licOpt2 = $('#apply-licensing #lic-opt-2').prop('checked');
        var licOpt3 = $('#apply-licensing #lic-opt-3').prop('checked');
        var licOpt4 = $('#apply-licensing #lic-opt-4').prop('checked');
        var licVal1 = $('#apply-licensing #lic-opt-1-price').val();
        var licVal2 = $('#apply-licensing #lic-opt-2-price').val();
        var licVal3 = $('#apply-licensing #lic-opt-3-price').val();
        var licVal4 = $('#apply-licensing #lic-opt-4-price').val();
        var licCommAmt4 = $('#apply-licensing #lic-opt-4-comm').val();
        var licCommPer4 = $('#apply-licensing #lic-opt-4-comm-per').val();
        var data = {};
        var selectedLicensing = {};
        if (licOpt1 == true) {
            selectedLicensing["1"] = {
                "price": licVal1,
                "data": data
            }
        }
        if (licOpt2 == true) {
            selectedLicensing["2"] = {
                "price": licVal2,
                "data": data
            }
        }
        if (licOpt3 == true) {
            selectedLicensing["3"] = {
                "price": licVal3,
                "data": data
            }
        }
        if (licOpt4 == true) {
            data = {
                "amount": licCommAmt4,
                "percent": licCommPer4
            };

            selectedLicensing["4"] = {
                "price": licVal4,
                "data": data
            }
        }

        req.selectedLicensing = selectedLicensing;

        req.action = 'update-course-licensing';

        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1) {
                alertMsg(res.message);
                $('#apply-licensing').modal('hide');

            } else {
                alertMsg(res.msg);
            }
        });
    });

    $('#apply-licensing button.cancel').on('click', function (e) {
        $('#apply-licensing').modal('hide');
    });

    $('#edit-form #end-date').datetimepicker({
		format: 'd F Y',
		timepicker: false,
		closeOnDateSelect: true,
        minDate: 0,
		maxDate: '2050/12/31',
		onSelectDate: function(date) {
			unsetError($('#edit-form #end-date'));
			if (date.valueOf() < startDate.valueOf()) {
				setError($('#edit-form #end-date'), 'The end date can not be less than the start date.');
			} else {
				endDate = date;
			}
		}
    });

    /*$('#edit-form #live-date').on('changeDate', function (ev) {
        unsetError($('#edit-form #live-date'));
        if (ev.date.valueOf() > endDate.valueOf()) {
            setError($('#edit-form #live-date'), 'The start date can not be greater than the end date.');

        } else {
            startDate = new Date(ev.date);
        }
    });*/

    /*$('#edit-form #end-date').on('changeDate', function (ev) {
        unsetError($('#edit-form #end-date'));
        if (ev.date.valueOf() < startDate.valueOf()) {
           setError($('#edit-form #end-date'), 'The end date can not be less then the start date.');
        } else {
            endDate = new Date(ev.date);
        }
	});*/

    $('#apply-licensing .lic-opt').on('change', function () {
        var id = $(this).attr('id');
        if ($(this).prop('checked') == true) {
            $('#' + id + '-price').parents('.form-group').show();
        } else {
            $('#' + id + '-price').parents('.form-group').hide();
        }
    });

    $("#course-image").change(function () {
        $('.help-block').remove();
        //$('#edit-form .save-button').prop('disabled',false);
		courseImageChanged = true;
        var _URL = window.URL || window.webkitURL;
		var img = new Image();
		var file = this;
		img.onload = function() {
			if(!(img.width < 280 || img.height < 200))
				readURL(file);
			else {
				$('#course-image-form input[type=submit]').prop('disabled',true);
				$('#course-image-form .error').remove();
				$('#course-image-form').append('<span style="color:red;" class="error">Please select a larger image than 280 x 200.</span>');
			}
		}
		img.src = _URL.createObjectURL(this.files[0]);
    });
	
	function readURL(input) {
		if(input.files && input.files[0]) {
            if(!(input.files[0]['type'] == 'image/jpeg' || input.files[0]['type'] == 'image/png' || input.files[0]['type'] == 'image/gif')) {
				$('#course-image-form input[type=submit]').prop('disabled',true);
				$('#course-image-form .error').remove();
				$('#course-image-form').append('<span class="help-block" style="color:red;" class="error">Please select a proper file type.</span>');
				return;
			}else if(input.files[0]['size']>819200) {
				$('#course-image-form input[type=submit]').prop('disabled',true);
				$('#course-image-form .error').remove();
				$('#course-image-form').append('<span style="color:red;" class="error">Files larger than 800kb are not allowed.</span>');
				return;
			}else {
				$('#course-image-form .error').remove();
				$('#course-image-form input[type=submit]').prop('disabled',false);
			}
            var reader = new FileReader();
            if (typeof jcrop_api != 'undefined' && jcrop_api != null) {
                jcrop_api.destroy();
                jcrop_api = null;
                var pImage = $('.crop');
                pImage.css('height', 'auto');
                pImage.css('width', 'auto');
                var height = pImage.height();
                var width = pImage.width();
                $('.jcrop').width(width);
                $('.jcrop').height(height);
            }
            reader.onload = function (e) {
                $('#image-preview').attr('src', e.target.result);
                $('#upload-image .crop').Jcrop({
                    onSelect: updateCoords,
                    bgOpacity:   .4,
					boxWidth: 830,
					minSize: [280, 200],
					setSelect:   [0, 0, 280, 200],
					aspectRatio: 804/440
                }, function () {
                    jcrop_api = this;
					jcrop_api.setSelect([0, 0, 280, 200]);
                });
            }
            reader.readAsDataURL(input.files[0]);
        }
	}

    function updateCoords(c) {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
		var crop = [c.x, c.y, c.w, c.h];
    }

    function getSelectedFromExistingSubjectImportForm() {
        var selectedSubjects = [];
        $.each($('#import-existing-course .dyn-data .subject'), function (i, subject) {
            var subjectId = $(this).attr('data-id');
            if ($(this).find('input[type=checkbox]').prop('checked') == true)
                selectedSubjects.push(subjectId);
        });
        return selectedSubjects;
    }

    $('#import-existing-course button.import').on('click', function (e) {
        $('.form-msg.existing-subject-import-form').html('').hide();
        e.preventDefault();
        var selectedSubjects = getSelectedFromExistingSubjectImportForm();
        if (selectedSubjects.length == 0) {
            $('.form-msg.existing-subject-import-form').html('Please select a subject to import.').show();
            return;
        }

        var req = {};
        req.courseId = getUrlParameter('courseId');
        req.action = 'import-subjects-to-course';
        req.subjects = selectedSubjects;
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1) {
                alertMsg(res.message);
               // $('#import-content-modal').modal('hide');
               // fetchSubjects();
               // fetchCoursesForImport();
              //  alertMsg(res.msg);
             location.reload();
            } else {
                alertMsg(res.msg);
            }
        });
    });
    
    function getSelectedFromPurchasedSubjectImportForm() {
        var selectedSubjects = [];
        $.each($('#import-purchased-course .dyn-data .subject'), function (i, subject) {
            var subjectId = $(this).attr('data-id');
            if ($(this).find('input[type=checkbox]').prop('checked') == true)
                selectedSubjects.push(subjectId);
        });
        return selectedSubjects;
    }

    $('#import-purchased-course button.import').on('click', function (e) {
        $('.form-msg.purchased-subject-import-form').html('').hide();
        e.preventDefault();
        var selectedSubjects = getSelectedFromPurchasedSubjectImportForm();
        if (selectedSubjects.length == 0) {
            $('.form-msg.purchased-subject-import-form').html('Please select a subject to import.').show();
            return;
        }

        var req = {};
        req.courseId = getUrlParameter('courseId');
        req.action = 'import-subjects-to-course';
        req.subjects = selectedSubjects;
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1) {
                alertMsg(res.message);
                $('#import-content-modal').modal('hide');
               location.reload();
            } else {
                alertMsg(res.msg);
            }
        });
    });

    $('#course-image-form').attr('action', fileApiEndpoint);

	$('#course-image-form').on('submit', function(e) {
		e.preventDefault();
		c = jcrop_api.tellSelect();
		crop = [c.x, c.y, c.w, c.h];
		imageA = document.getElementById('image-preview');
		var sources = {
            'course-image': {
                image: imageA,
                type: 'image/jpeg',
                crop: crop,
                size: [634, 150],
                quality: 1.0
            }
        }
        //settings for $.ajax function
        var settings = {
			url: fileApiEndpoint,
			data: {'courseId': getUrlParameter('courseId')}, //three fields (medium, small, text) to upload
			beforeSend: function()
			{
				//console.log('sending image');
			},
			complete: function (resp) {
				$('#upload-image').modal('hide');
                res = $.parseJSON(resp.responseText);
				alertMsg(res.message);
				$('#course-image-form .jcrop-holder').hide();
				$('#course-image-form input[type="file"]').val('');
				fetchCourseDetails();
			}
        }
        cropUploadAPI.cropUpload(sources, settings);
	});

    var keys_avilable;
    var keys_pending;

    $('.invite_students').on('click', function () {
        $('.help-block').remove();
        $('.has-error').removeClass('has-error');
        $('#textarea_email_id').val('');
        keys_avilable = get_keys_remaining();
        // $('#textarea_email_id').parent().addClass('has-error').append('<span class="help-block">You can send '+keys_avilable+' invitations</span>');
        // alertMsg("You can send "+keys_avilable+" invitations");
    });

    function get_keys_remaining() {
        var req = {};
        //  req.courseId = getUrlParameter('courseId');
        req.action = 'keys_avilable';
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            //console.log(data);
            if (data.status == 1) {
                keys_avilable = data.keys;
                keys_pending = data.invitation;
                //alertMsg(data.message);
                //$('#modal_invite_student').modal('hide');
            }
            else {
                // alertMsg(res.msg);
            }
            $('#remaining_keys_total, .keys-remaining.badge').text(keys_avilable);
            $('#pending_keys_total, .keys-pending.badge').text(keys_pending);
        });
    }

    $('#modal_invite_student').on('show.bs.modal', function() {
        alertMsg('You have ' + $('#remaining_keys_total').text() + ' course keys left.');
    });

    /*  Purchase more scipts starts */
    //purchaseNow button click event listener
    $('#purchaseNow').on('click', function() {
        if($('#totalkeys').val() != '' && parseInt($('#totalkeys').val()) > 0) {
            $('#Purchase_More').modal('hide');
            var keys = $('#totalkeys').val();
            var amount = keys * key_rate;
            //$('#totalkeys').val('');
            $('#purchaseNowModal').modal('show');
            $('#purchaseNowModal .amount').html(display_current_currency + amount);
            $('#purchaseNowModal .price-mul').html(keys + ' * ' + display_current_currency + key_rate);
        }else {
            setError($('#totalkeys'), 'Invalid entry')
        }
    });

    //event handler for PayU Money
    $('#payUMoneyButton').on('click', function() {
        var req = {};
        var res;
        req.action = 'purchaseCourseKeys';
        req.quantity = $('#totalkeys').val();
        req.currency = UserCurrency;
        $.ajax({
            'type'  : 'post',
            'url'   : ApiEndpoint,
            'data'  : JSON.stringify(req), 
        }).done(function(res) {
            res = $.parseJSON(res);
            if(res.status == 1) {
                if(res.paymentSkip == 1)
                    location.reload();
               else if(res.method == 2){
                    var html = '';
                    html = '<form action="'+res.url+'" method="post" id="payUForm">'
                        + '<input type="hidden" name="txnid" value="' + res.txnid + '">'
                        + '<input type="hidden" name="key" value="'+res.key+'">'
                        + '<input type="hidden" name="amount" value="'+res.amount+'">'
                        + "<input type='hidden' name='productinfo' value='"+res.productinfo+"'>"
                        + '<input type="hidden" name="firstname" value="'+res.firstname+'">'
                        + '<input type="hidden" name="email" value="'+res.email+'">'
                        + '<input type="hidden" name="surl" value="'+res.surl+'">'
                        + '<input type="hidden" name="furl" value="'+res.furl+'">'
                        + '<input type="hidden" name="curl" value="'+res.curl+'">'
                        + '<input type="hidden" name="furl" value="'+res.furl+'">'
                        + '<input type="hidden" name="hash" value="'+res.hash+'">'
                        + '<input type="hidden" name="service_provider" value="'+res.service_provider+'">'
                    + '</form>';
                    $('body').append(html);
                    $('#payUForm').submit();
                }
                else if(res.method == 1) {
                    var html = '';
                    html += '<form action="' + res.url + '" method="post" id="paypalForm">'
                        + '<!-- Identify your business so that you can collect the payments. -->'
                        + '<input type="hidden" name="business" value="' + res.business + '">'
                        + '<!-- Specify a Buy Now button. -->'
                        + '<input type="hidden" name="cmd" value="_xclick">'
                        + '<!-- Specify details about the item that buyers purchase. -->'
                        + '<input type="hidden" name="item_name" value="Course Keys">'
                        + '<input type="hidden" name="amount" value="' + res.rate + '">'
                        + '<input type="hidden" name="currency_code" value="USD">'
                        + '<input type="hidden" name="quantity" value="' + res.quantity + '">'
                        + '<input type="hidden" name="invoice" value="' + res.orderId + '">'
                        + '<input type="hidden" name="item_number" value="' + res.orderId + '">'
                        + '<input type="hidden" name="return" value="' + res.surl + '" />'
                        + '<input type="hidden" name="notify_url" value="' + res.nurl + '" />'
                        + '<input type="hidden" name="cancel_return" value="' + res.curl + '" />'
                        + '<input type="hidden" name="lc" value="US" />'
                        + '<!-- Display the payment button. -->'
                        + '<input type="image" name="submit" border="0" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online">'
                        + '<img alt="" border="0" width="1" height="1"  src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >'
                    + '</form>';
                    $('body').append(html);
                    $('#paypalForm').submit();
                }
            }else{
                alertMsg(res.message);
            }
        });
    });

    $('#totalkeys').keyup(function () {
        unsetError($('#totalkeys'));
        var total = $(this).val() * key_rate;
        $('#totalkeyrate').text(total);
        $('#totalkeyrate').append(display_current_currency);
    });

    $('#purchase_key').click(function () {
        var req = {};
        var res;
        var keys = $('#totalkeys').val();
        if (confirm('Are you sure to purchase?')) {
            req.action = "purchase_key";
            req.keys = keys;
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                data = $.parseJSON(res);
                window.location = self.location;
            });

        }
    });
    $('#keys-purchase').click(function () {
        total_puchased_keys = $.trim($('#totalkeys').val());
        $('.help-block').remove();
        if (total_puchased_keys == '' || total_puchased_keys == 0) {
            $('#totalkeys').after('<span class="help-block text-danger">Invalid entry</span>');
            return false;
        }
        $('#Purchase_More').modal('hide');
    });
    $("#totalkeys").keypress(function (e) {
        $('.help-block').remove();
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $('#totalkeys').after('<span class="help-block text-danger">Enter Only Numbers</span>');
            //$('.help-block').fadeOut("slow");
            // ("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
    $('#keys-purchaseInvitation').click(function () {
        $('.help-block').remove();
        var req = {};
        var res;
        var email = $.trim($('#inviteon_email').val());
        if (email != '' && validateEmail(email) == false && isNaN(email)) {
            $('#inviteon_email').after('<span class="help-block text-danger">Please enter Correct email address/ Phone no.</span>')
            return false;
        } else {
            if (validateMobile(email) && !(isNaN(email))) {
                $('#inviteon_email').after('<span class="help-block text-danger">Please enter Correct email address/ Phone no.</span>')
                return false;
            }
        }
        req.action = "keys-purchase_invitation";
        req.keys = total_puchased_keys;
        req.email = email;
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            alertMsg(data.message);
            $('#inviteon_email').next().append('<span class="help-block text-success">' + data.message + '</span>');
            $('#Purchase_More').modal('hide');
            $('#modal_keys_purchaseInvitation').modal('hide');
            // window.location.reload();
        });
    });

    function validateEmail(email) {
        var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!emailReg.test(email)) {
            return false;
        } else {
            return true;
        }
    }
    function validateMobile(mobile) {
        regex = /^(\d{10})$/;
        if (regex.test(mobile))
            return false;
        else
            return true;
    }
    function fetchCoursekeys() {
        var req = {};
        var res;
        req.action = "get-course-keys-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            console.log(data);
            var current_currency = data.currency[0].currency;
            UserCurrency = data.currency[0].currency;
            key_rate = data.key_rate[0].key_rate;
            if (current_currency == 2) {
                display_current_currency = '<i class="fa fa-inr"></i>';
                key_rate = data.key_rate[0].key_rate_inr;
            }
            $('#keyrate').html(display_current_currency + key_rate);
        });
    }
    /*  Purchase more scipts ends */

    $('#send_activation_link').on('click', function () {
        var invalid = '';
        var correct = true
        var emails = $.trim($('#textarea_email_id').val());
        var email = emails.split(',');
        $('.help-block').remove();
        $('.has-error').removeClass('has-error');
        if (emails == '') {
            $('#textarea_email_id').parent().addClass('has-error').append('<span class="help-block">Please enter email id </span>');
            // alertMsg('Please enter email id ');
            return false;
        }
        if (checkDuplicates(email) == 'false') {
            $('#textarea_email_id').parent().addClass('has-error').append('<span class="help-block">Please remove duplicates</span>');
            //alertMsg('Please remove duplicates ');
            return false;
        }
        if ((email.length > keys_avilable)) {
            $('#textarea_email_id').parent().addClass('has-error').append('<span class="help-block">You have ' + keys_avilable + ' course keys.Please add course key in your account to assign students. </span>');
            // alertMsg('You have only ' + keys_avilable + ' available ');
            return false;
        }
        $.each(email, function (index, value) {
            if (validateEmail($.trim(value)) == false) {
                invalid += value + ',';
                correct = false;
            }
        });
        if (correct == false) {
            $('#textarea_email_id').parent().addClass('has-error').append('<span class="help-block">Correct these email addresses:\n' + invalid + ' </span>');
            //alert('Correct these email addresses:\n' + invalid);
            return false;
        }
        else {
            var req = {};
            req.courseId = getUrlParameter('courseId');
            req.action = 'send_activation_link_student';
            req.email = email;
            req.enrollment_type = 0;
			//req.ignoreWarning = false;
            inviteStudent(req);
        }
    });
    function checkDuplicates(a) {
        //Check all values in the incoming array and eliminate any duplicates
        var r = new Array(); //Create a new array to be returned with unique values
        //Iterate through all values in the array passed to this function
        o:for (var i = 0, n = a.length; i < n; i++) {
            //Iterate through any values in the array to be returned
            for (var x = 0, y = r.length; x < y; x++) {
                //Compare the current value in the return array with the current value in the incoming array
                if (r[x] == a[i]) {
                    //If they match, then the incoming array value is a duplicate and should be skipped
                    return 'false';
                    //continue o;
                }
            }
            //If the value hasn't already been added to the return array (not a duplicate) then add it
            r[r.length] = a[i];
        }
        //Return the reconstructed array of unique values
        //return r;
        return 'true';
    }

    function validateEmail($email) {
        var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!emailReg.test($email)) {
            return false;
        } else {
            return true;
        }
    }
	
	//for allowing decimal number
	$('#student-price-dollar, #student-price-inr').keypress(function (event) {
		return allowDecimal(event, $(this));
	});
	function allowDecimal(event, obj) {
		if ((event.which != 46 || obj.val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) && event.which != 8) {
			return false;
		}
		var text = obj.val();
		if ((text.indexOf('.') != -1) && (text.substring(text.indexOf('.')).length > 2)) {
			if (event.which != 8)
				return false;
		}
		return true;
	}

    function setError(where, what) {
        unsetError(where);
        where.parents('div:eq(0)').addClass('has-error');
        where.parents('div:eq(0)').append('<span class="help-block">' + what + '</span>');
    }

    function unsetError(where) {
        if (where.parents('div:eq(0)').hasClass('has-error')) {
            where.parents('div:eq(0)').find('.help-block').remove();
            where.parents('div:eq(0)').removeClass('has-error');
        }
    }

    function inviteStudent(req) {
    	$.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            if (data.status == 1) {
                // $('#textarea_email_id').parent().append('<span class="help-block text-success">'+data.message+' </span>');  
                alertMsg(data.message);
              $('#modal_invite_student').modal('hide');
              get_keys_remaining();
            }
    		/*else if(data.warning == true) {
    			con = confirm(data.message);
    			if(con) {
    				req.ignoreWarning = true;
    				inviteStudent(req);
    			}
    		}*/
            else {
                $('#textarea_email_id').parent().addClass('has-error').append('<span class="help-block text-danger">' + data.message + ' </span>');
                //$('#textarea_email_id').parent().addClass('has-success').append('<span class="help-block">'+data.message+' </span>');  
                // alertMsg(data.msg);
            }    
        });
    }

});