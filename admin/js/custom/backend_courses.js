/*
 Author: Fraaz Hashmi.
 Dated: 6/28/2014
 
 
 ********
 1) Notes: JSON.stringify support for IE < 10
 */
var userRole;
var reorderTimer = null;
var instituteId = getUrlParameter("instituteId");
if(typeof(instituteId)==="undefined"){
   instituteId=1;
}
$(function () {
    var imageRoot = '/';
    $('#loader_outer').hide();

    function fetchProfileDetails() {
        var req = {};
        var res;
        req.action = "get-profile-details";
         $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillProfileDetails(res);
            console.log(res);
        });
    }

    function fillProfileDetails(data) {
        // General Details
        var imageRoot = 'user-data/images/';
        $('#user-profile-card .username').text(data.loginDetails.username);

        if (data.profileDetails.profilePic != "") {
            $('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
        }

        // Role Specific
        userRole = data.userRole;
        if (data.userRole == '1') {
            fillInstituteDetails(data);
        }
        else if (data.userRole == '2') {
            fillProfessorDetails(data);
        }
        else if (data.userRole == '3') {
            fillPublisherDetails(data);
        }
    }

    function fillInstituteDetails(data) {
        $('h4#profile-name').text(data.profileDetails.name);
    }

    function fillProfessorDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('a#profile-name').text(name);
    }

    function fillPublisherDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('a#profile-name').text(name);
    }

    function fetchCourses() { 
        var req = {};
        var res;
        req.action = "get-all-courses";
        req.instituteId = instituteId;
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            console.log(res);
            res = $.parseJSON(res);
            fillCourses(res);
        });
    }

    function fillCourses(data) {
        var html = "";
        var flag = false;
        var editCourseLink, editSubjectLink, addSubjectLink, deletedClass;
        deletedClass = "";
        $.each(data.courses, function (i, course) {
            if (flag == false)
                flag = true;
            editCourseLink = "backend_edit-course.php?courseId=" + course.id;
            addSubjectLink = "add-subject.php?courseId=" + course.id;
            courseId = "C" + String("000" + course.id).slice(-4);
            deletedClass = "";
            if (course.deleted == 1) {
                deletedClass = " class='deleted-item'";
            }
            html += "<tr data-cid='" + course.id + "'" + deletedClass + ">"
                    + "<td>" + courseId + "</td>"
                    + "<td><h5><a href='" + editCourseLink + "'>" + course.name + "</a></h5>";
            $.each(course.subjects, function (i, subject) {
                editSubjectLink = "backend_edit-subject.php?courseId=" + subject.courseId + "&subjectId=" + subject.id;
                html += "<a href='" + editSubjectLink + "'>" + subject.name + "</a>, ";
            });

            html += "<br />";
            html += "</td>"
                    + "<td>" + course.liveDate + " to " + course.endDate + "</td>"
                    + "<td>Life Time</td>";

            +"</tr>";
        });

        if (flag == true) {
            $('#all-courses tbody').html('').html(html);
            $('.tooltips').tooltip();
        }
    }

    //fetchProfileDetails();
    fetchCourses();
});
