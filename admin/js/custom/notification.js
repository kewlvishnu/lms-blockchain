var ApiEndPoint = '../api/index.php';
var details;

$(function() {
     var imageRoot = '/';
	$('#loader_outer').hide();
	
	//fetchProfileDetails();
	fetchNotifications();
	 
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
       
		

	//function to fetch notifications
	function fetchNotifications() {
		var req = {};
		var res;
		req.action = 'get-institute-notifications';
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				details = res;
				fillNotifications(res);
			}
		});
	}
	
	//function to fill notifications table
	function fillNotifications(data) {
		var html = '';
		$('#notify-list').find('li').remove();
                 $.each(data.sitenotifications, function (i, notification) {
			html += '<li data-id="'+notification.id+'">'
					+ '<div class="task-title">'
						+ '<span class="task-title-sp">'+notification.message+'</span>'
						+ '<div class="pull-right hidden-phone">';
					if(notification.status == 0)
							html += '<button class="btn btn-success btn-xs mark-notification"><i class=" fa fa-check"></i> Mark Read </button>&emsp;';
						html += '</div>'
					+ '</div>'
				+ '</li>';
		});
        if (!!data.chatnotifications) {
	        for(var i = 0; i < data.chatnotifications.length; i++) {
				var link = '';
				switch(data.chatnotifications[i].roleId) {
					case '1':
						link = 'institute/' + data.chatnotifications[i].userId;
						link = "<a href='"+link+"' target='_blank'><strong>"+data.chatnotifications[i].senderName+"</strong></a>";
						break;
					case '2':
						link = 'instructor/' + data.chatnotifications[i].userId;
						link = "<a href='"+link+"' target='_blank'><strong>"+data.chatnotifications[i].senderName+"</strong></a>";
						break;
					case '3':
						link = 'institute/' + data.chatnotifications[i].userId;
						link = "<a href='"+link+"' target='_blank'><strong>"+data.chatnotifications[i].senderName+"</strong></a>";
						break;
					case '4':
						link = "<strong>"+data.chatnotifications[i].senderName+"</strong>";
						break;
				}
				var message = "";
				switch(data.chatnotifications[i].type) {
					case '0':
						if (data.chatnotifications[i].room_type == "public") {
							message = '<span class="task-title-sp"> '+link+' just started conversation in <strong>Subject : '+data.chatnotifications[i].courseName+'</strong> of <strong>Course : '+data.chatnotifications[i].subjectName+'</strong></span>';
						} else {
							message = '<span class="task-title-sp"> '+link+' just started conversation with you in <strong>Subject : '+data.chatnotifications[i].courseName+'</strong> of <strong>Course : '+data.chatnotifications[i].subjectName+'</strong></span>';
						}
						break;
					case '1':
						if (data.chatnotifications[i].room_type == "public") {
							message = '<span class="task-title-sp"> '+link+' just sent a message in <strong>Subject : '+data.chatnotifications[i].courseName+'</strong> of <strong>Course : '+data.chatnotifications[i].subjectName+'</strong></span>';
						} else {
							message = '<span class="task-title-sp"> '+link+' just messaged you in <strong>Subject : '+data.chatnotifications[i].courseName+'</strong> of <strong>Course : '+data.chatnotifications[i].subjectName+'</strong></span>';
						}
						break;
				}
				html += '<li data-id="'+data.chatnotifications[i].id+'">'
						+ '<div class="task-title">'
							+ message
							+ '<div class="pull-right hidden-phone">'
								+ '<button class="btn btn-success btn-xs chat-button" data-chat="1"><i class=" fa fa-check"></i> Start Chat</button>&emsp;'
								+ '<button class="btn btn-danger btn-xs chat-button" data-chat="0"><i class="fa fa-times"></i> Mark read</button>'
							+ '</div>'
						+ '</div>'
					+ '</li>';
			}
		}
		if(html == '')
			html = 'No new notification found.';
		$('#notify-list').append(html);

		//adding event handlers for accept button
		$('.chat-button').on('click', function() {
			var id = $(this).parents('li').attr('data-id');
			var req = {};
			var li = $(this).parents('li');
			var res;
			req.action = 'mark-chat-notification';
			req.notificationId  = id;
			req.chatAction = $(this).attr('data-chat');
			$.ajax({
					'type'	:	'post',
					'url'	:	ApiEndPoint,
					'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else {
					if (!res.roomId) {
						alertMsg("Notification marked as Read.");
						li.remove();
						fetchNotificationCount();
					} else {
						li.remove();
						fetchNotificationCount();
						window.open('../messenger/'+res.roomId);
					}
				}
			});
		});
		
		//adding event handlers for accept button
		$('.mark-notification').on('click', function() {
			var id = $(this).parents('li').attr('data-id');
			var req = {};
			var li = $(this).parents('li');
			var res;
			req.action = 'mark-notification';
			req.notificationId  = id;
			$.ajax({
					'type'	:	'post',
					'url'	:	ApiEndPoint,
					'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else {
					alertMsg("Notification marked as Read. ");
					li.find('.mark-notification').remove();
					fetchNotificationCount();
				}
			});
		});

	}
});