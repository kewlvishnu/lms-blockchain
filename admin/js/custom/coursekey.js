var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var instituteId = getUrlParameter('instituteId');
var total_puchased_keys = '';
var UserCurrency = 2;
var key_rate = "";
var display_current_currency = '$';
$(function () {
	//purchaseNow button click event listener
	$('#purchaseNow').on('click', function() {
		if($('#totalkeys').val() != '' && parseInt($('#totalkeys').val()) > 0) {
			$('#Purchase_More').modal('hide');
			var keys = $('#totalkeys').val();
			var amount = keys * key_rate;
			//$('#totalkeys').val('');
			$('#purchaseNowModal').modal('show');
			$('#purchaseNowModal .amount').html(display_current_currency + amount);
			$('#purchaseNowModal .price-mul').html(keys + ' * ' + display_current_currency + key_rate);
		} else {
			setError($('#totalkeys'), 'Invalid entry')
		}
	});

	//event handler for PayU Money
	$('#payUMoneyButton').on('click', function() {
		var req = {};
		var res;
		req.action = 'purchaseCourseKeys';
		req.quantity = $('#totalkeys').val();
		req.currency = UserCurrency;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data'  : JSON.stringify(req), 
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 1) {
				if(res.paymentSkip == 1)
					location.reload();
				else if(res.method == 2){
					var html = '';
					html = '<form action="'+res.url+'" method="post" id="payUForm">'
						+ '<input type="hidden" name="txnid" value="' + res.txnid + '">'
						+ '<input type="hidden" name="key" value="'+res.key+'">'
						+ '<input type="hidden" name="amount" value="'+res.amount+'">'
						+ "<input type='hidden' name='productinfo' value='"+res.productinfo+"'>"
						+ '<input type="hidden" name="firstname" value="'+res.firstname+'">'
						+ '<input type="hidden" name="email" value="'+res.email+'">'
						+ '<input type="hidden" name="surl" value="'+res.surl+'">'
						+ '<input type="hidden" name="furl" value="'+res.furl+'">'
						+ '<input type="hidden" name="curl" value="'+res.curl+'">'
						+ '<input type="hidden" name="furl" value="'+res.furl+'">'
						+ '<input type="hidden" name="hash" value="'+res.hash+'">'
						+ '<input type="hidden" name="service_provider" value="'+res.service_provider+'">';
					+ '</form>'
					$('body').append(html);
					$('#payUForm').submit();
				}
				else if(res.method == 1) {
					var html = '';
					html += '<form action="' + res.url + '" method="post" id="paypalForm">'
						+ '<!-- Identify your business so that you can collect the payments. -->'
						+ '<input type="hidden" name="business" value="' + res.business + '">'
						+ '<!-- Specify a Buy Now button. -->'
						+ '<input type="hidden" name="cmd" value="_xclick">'
						+ '<!-- Specify details about the item that buyers purchase. -->'
						+ '<input type="hidden" name="item_name" value="Course Keys">'
						+ '<input type="hidden" name="amount" value="' + res.rate + '">'
						+ '<input type="hidden" name="currency_code" value="USD">'
						+ '<input type="hidden" name="quantity" value="' + res.quantity + '">'
						+ '<input type="hidden" name="invoice" value="' + res.orderId + '">'
						+ '<input type="hidden" name="item_number" value="' + res.orderId + '">'
						+ '<input type="hidden" name="return" value="' + res.surl + '" />'
						+ '<input type="hidden" name="notify_url" value="' + res.nurl + '" />'
						+ '<input type="hidden" name="cancel_return" value="' + res.curl + '" />'
						+ '<input type="hidden" name="lc" value="US" />'
						+ '<!-- Display the payment button. -->'
						+ '<input type="image" name="submit" border="0" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online">'
						+ '<img alt="" border="0" width="1" height="1"	src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >'
					+ '</form>';
					$('body').append(html);
					$('#paypalForm').submit();
				}
			}else{
				alertMsg(res.message);
			}
		});
	});

	$('a.history').addClass('active');
	fetchCoursekeys();
	get_students_of_course();
	fetchPurchasedCourses();
	function fetchCoursekeys() {
		var req = {};
		var res;
		req.action = "get-course-keys-details";
		if (typeof (instituteId) === "undefined") {
		} else {
			req.instituteId = instituteId;
			$('#btn_Purchase_More').remove();
		}
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			var html = "";
			for (var i = 0; i < data.courses.length; i++)
			{
				var keys = data.courses[i].keys;
				var rate = data.courses[i].key_rate;
				var amount = keys * rate;
				var date = data.courses[i].date;
				var time = data.courses[i].time;
				var currency = data.courses[i].currency;
				if (currency == 2)
				{
					display_currency = '<i class="fa fa-inr"></i>';
				}
				else {
					display_currency = '$';
				}
				html += '<tr><td>' + data.courses[i].date + '&nbsp;&nbsp;' + time + ' GMT </td><td>' + keys + '</td><td>' + rate + ' ' + display_currency + '</td><td>' + amount + ' ' + display_currency + '</td></tr>';

			}
			$('#tabl_Totalkeys tbody').html(html);
			if ($('#tabl_Totalkeys tbody tr').length == 0) {
				$('#tabl_Totalkeys tbody').html('<tr><td>No any purchase done yet </td></tr>');
			}
			var current_currency = data.currency[0].currency;
			UserCurrency=data.currency[0].currency;
			key_rate = data.key_rate[0].key_rate;
			if (current_currency == 2) {
				display_current_currency = '<i class="fa fa-inr"></i>';
				key_rate = data.key_rate[0].key_rate_inr;
			}
			//key_rate=data.key_rate[0].key_rate;
			$('#keyrate').text(key_rate);
			$('#keyrate').append(display_current_currency);
			var active_key = data.key_consume[0].active_key;
			var total_keys = data.key_consume[0].total_keys;
			var remaining_keys = total_keys - active_key;
			$('#active_key').text(active_key);
			$('#total_keys').text(total_keys);
			$('#remaining_key').text(remaining_keys);
			$('#totalkeyrate').append(display_current_currency);
		});
	}

	function get_students_of_course() {
		var req = {};
		var res;
		req.action = "get_students_of_course";
		if (typeof (instituteId) === "undefined") {
		} else {
			req.instituteId = instituteId;

		}
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			var html = "";
			for (var i = 0; i < data.students.length; i++)
			{
				var key_student = data.students[i].key_student;
				var course_id = data.students[i].course_id;
				var name = data.students[i].name;
				var stand_alone = data.students[i].stand_student;
				html += '<tr course_id=' + course_id + '><td>' + course_id + ' </td><td>' + name + '</td><td><a class="showcourse" data-toggle=modal href="#modal_student">' + key_student + '</a></td><td> <a href="#modal_student" data-toggle="modal" class="shownonkey">' + stand_alone + ' </a></td></tr>';
			}
			$('#tbl_course_student_count').append(html);

			$('.showcourse').click(function () {
				$("#tbl_course_details > tbody").html("");
				var course_id = $(this).parents('tr').attr('course_id');
				var req = {};
				var res;
				req.action = "get_course_student_details";
				req.keyId = true;
				if (typeof (instituteId) !== "undefined") {
					req.instituteId = instituteId;
					$('#btn_Purchase_More').remove();
				}
				req.course_id = course_id;
				$.ajax({
					'type': 'post',
					'url': ApiEndpoint,
					'data': JSON.stringify(req)
				}).done(function (res) {
					data = $.parseJSON(res);
					var html = "";
					for (var i = 0; i < data.students.length; i++)
					{
						var name = data.students[i].name;
						var contactMobile = data.students[i].contactMobile;
						var course_key = 'CK00' + data.students[i].key_id;
						var email = data.students[i].email;
						var date = data.students[i].date;
						html += '<tr ><td>' + name + ' </td><td>' + email + '</td><td>' + contactMobile + '</td><td>' + course_key + '</td><td> ' + date + '</td></tr>';
					}
					$('#tbl_course_details').append(html);
				});

			});
			$('.shownonkey').click(function () {
				$("#tbl_course_details > tbody").html("");
				var course_id = $(this).parents('tr').attr('course_id');
				var req = {};
				var res;
				req.action = "get_course_student_details";
				req.keyId = false;
				if (typeof (instituteId) !== "undefined") {
					req.instituteId = instituteId;
					$('#btn_Purchase_More').remove();
				}
				req.course_id = course_id;
				$.ajax({
					'type': 'post',
					'url': ApiEndpoint,
					'data': JSON.stringify(req)
				}).done(function (res) {
					data = $.parseJSON(res);
					var html = "";
					for (var i = 0; i < data.students.length; i++)
					{
						var name = data.students[i].name;
						var contactMobile = data.students[i].contactMobile;
						var course_key = 'CK00' + data.students[i].key_id;
						var email = data.students[i].email;
						var date = data.students[i].date;
						html += '<tr ><td>' + name + ' </td><td>' + email + '</td><td>' + contactMobile + '</td><td>' + course_key + '</td><td> ' + date + '</td></tr>';
					}
					$('#tbl_course_details').append(html);
				});

			});
		
		});
	}

	function fetchPurchasedCourses() {
		var req = {};
		var res;
		req.action = "fetch-purchased-courses-details";
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			var html = "";
			if(data.purchasedCourses.length > 0) {
				$.each(data.purchasedCourses, function (c, pcourse) {
					var coursePrice="";
					html += '<tr courseId=' + pcourse.parentCourseId + '><td>' + pcourse.parentCourseId + ' </td><td>' + pcourse.name + '</td><td>' + pcourse.purchasedAt + '</td><td>' + pcourse.ownerName + ' </td><td>' + pcourse.priceUSD + '</td> </tr>';
				});
				$('#tbl-purchased-courses').append(html);
			}
			else
				$('#tbl-purchased-courses').html('No courses purchased');
		 });
	}
	$('#totalkeys').keyup(function () {
		unsetError($('#totalkeys'));
		var total = $(this).val() * key_rate;
		$('#totalkeyrate').text(total);
		$('#totalkeyrate').append(display_current_currency);
	});
	$('#purchase_key').click(function () {
		var req = {};
		var res;
		var keys = $('#totalkeys').val();
		if (confirm('Are you sure to purchase?')) {
			req.action = "purchase_key";
			req.keys = keys;
			$.ajax({
				'type': 'post',
				'url': ApiEndpoint,
				'data': JSON.stringify(req)
			}).done(function (res) {
				data = $.parseJSON(res);
				window.location = self.location;
			});

		}
	});
	$('#keys-purchase').click(function () {
		total_puchased_keys = $.trim($('#totalkeys').val());
		$('.help-block').remove();
		if (total_puchased_keys == '' || total_puchased_keys == 0) {
			$('#totalkeys').after('<span class="help-block text-danger">Invalid entry</span>');
			return false;
		}
		$('#Purchase_More').modal('hide');
	});
	$("#totalkeys").keypress(function (e) {
		$('.help-block').remove();
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			$('#totalkeys').after('<span class="help-block text-danger">Enter Only Numbers</span>');
			//$('.help-block').fadeOut("slow");
			// ("#errmsg").html("Digits Only").show().fadeOut("slow");
			return false;
		}
	});
	$('#keys-purchaseInvitation').click(function () {
		$('.help-block').remove();
		var req = {};
		var res;
		var email = $.trim($('#inviteon_email').val());
		if (email != '' && validateEmail(email) == false && isNaN(email)) {
			$('#inviteon_email').after('<span class="help-block text-danger">Please enter Correct email address/ Phone no.</span>')
			return false;
		} else {
			if (validateMobile(email) && !(isNaN(email))) {
				$('#inviteon_email').after('<span class="help-block text-danger">Please enter Correct email address/ Phone no.</span>')
				return false;
			}
		}
		req.action = "keys-purchase_invitation";
		req.keys = total_puchased_keys;
		req.email = email;
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			alertMsg(data.message);
			$('#inviteon_email').next().append('<span class="help-block text-success">' + data.message + '</span>');
			$('#Purchase_More').modal('hide');
			$('#modal_keys_purchaseInvitation').modal('hide');
			// window.location.reload();
		});
	});

	function validateEmail(email) {
		var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!emailReg.test(email)) {
			return false;
		} else {
			return true;
		}
	}
	function validateMobile(mobile) {
		regex = /^(\d{10})$/;
		if (regex.test(mobile))
			return false;
		else
			return true;
	}
});