/*
 Author: Fraaz Hashmi.
 Dated: 6/28/2014
 
 
 ********
 1) Notes: JSON.stringify support for IE < 10
 */

var userRole;
var reorderTimer = null;
var subjectProfessors = [];
var popupShown = false;
var subjectImageChanged = false;
var instituteId = getUrlParameter('instituteId');

$(function () {
    var imageRoot = '/';
    $('#loader_outer').hide();

    function fetchProfileDetails() {
        var req = {};
        var res;
        req.action = "get-profile-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillProfileDetails(res);
            console.log(res);
        });
    }

    function fillProfileDetails(data) {
        // General Details
        var imageRoot = 'user-data/images/';
        $('#user-profile-card .username').text(data.loginDetails.username);

        if (data.profileDetails.profilePic != "") {
            $('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
        }

        // Role Specific
        userRole = data.userRole;
        if (data.userRole == '1') {
            fillInstituteDetails(data);
        }
        else if (data.userRole == '2') {
            fillProfessorDetails(data);
        }
        else if (data.userRole == '3') {
            fillPublisherDetails(data);
        }
    }

    function fetchCourseDetails() {
        var req = {};
        var res;
        req.courseId = getUrlParameter('courseId');
        if (req.courseId == "" || req.courseId == null) {
            window.location.href = "profile.php";
            return;
        }
        req.action = "get-course-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            console.log(res);
            res = $.parseJSON(res);
            fillCourseDetails(res);
        });
    }

    function fillCourseDetails(data) {
        // General Details
        var imageRoot = 'user-data/images/';
        $('#course-name').html(data.courseDetails.name);
        if (data.courseDetails.image == '') {
            var image = defaultImagePath;
        } else {
            var image = data.courseDetails.image;
        }
        $('#course-image-preview').attr('src', image + "?" + new Date().getTime());
    }

    function fillInstituteDetails(data) {
        $('h4#profile-name').text(data.profileDetails.name);
    }

    function fillProfessorDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('a#profile-name').text(name);
    }

    function fillPublisherDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('a#profile-name').text(name);
    }

    function fetchSubjectDetails(data) {
        var req = {};
        var res;
        req.courseId = getUrlParameter('courseId');
        req.subjectId = getUrlParameter('subjectId');
        if (req.courseId == "" || req.courseId == null) {
            window.location.href = "profile.php";
            return;
        }
        if (req.subjectId == "" || req.subjectId == null) {
            window.location.href = "profile.php";
            return;
        }
        req.action = "get-subject-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            console.log(res);
            res = $.parseJSON(res);
            fillSubjectDetails(res);
        });
    }

    function fillSubjectDetails(data) {
        if (data.subjectDetails.deleted == 1)
            alertMsg('This subject has been deleted!!', -1);
        var courseId = getUrlParameter('courseId');
        var subjectId = getUrlParameter('subjectId');
        $('#subject-name-title').html(data.subjectDetails.name);
        $('#subject-name').html(data.subjectDetails.name);
        $('#edit-form #subject-name').val(data.subjectDetails.name);

        $('#subject-id').html('S' + String("000" + data.subjectDetails.id).slice(-4));

        $('#subject-desc').html(data.subjectDetails.description);
        $('#edit-form #subject-desc').val(data.subjectDetails.description);
        $('.add-chapter-link').attr('href', 'add-chapter.php?courseId=' + courseId + '&subjectId=' + subjectId);

        var imageRoot = 'user-data/images/';
        if (data.subjectDetails.image == '') {
            var image = defaultImagePath;
        } else {
            var image = data.subjectDetails.image;
        }

        $('#subject-image-preview').attr('src', image + "?" + new Date().getTime());
        $('#edit-form #subject-image-preview').attr('src', image + "?" + new Date().getTime());
    }

    function fetchSubjectChapters() {
        var req = {};
        var res;
        req.courseId = getUrlParameter('courseId');
        req.subjectId = getUrlParameter('subjectId');
        if (req.courseId == "" || req.courseId == null) {
            window.location.href = "profile.php";
            return;
        }
        if (req.subjectId == "" || req.subjectId == null) {
            window.location.href = "profile.php";
            return;
        }
        req.action = "get-subject-chapters";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            console.log(res);
            res = $.parseJSON(res);
            if (res.status == 1)
                fillSubjectChapters(res);
            else
                alertMsg(res.message);
        });
    }

    function fillSubjectChapters(data) {
        var html = "";
        var flag = false;
        var editLink;
        var ChapterNum = 1;
        $.each(data.chapters, function (i, chapter) {
            if (flag == false)
                flag = true;
            console.log(chapter);
            editLink = "content.php?courseId=" + data.courseId + "&subjectId=" + chapter.subjectId + "&chapterId=" + chapter.id;
            html += "<tr data-ch-id='" + chapter.id + "'>"
                    + "<td><a href=#>Chapter " + ChapterNum + ": " + chapter.name + "</a></td>"
                    + "<td><a href='#'>First Assignment</a><br /><a href='#'>Second Assignment</a></td>"
                    + "<td><a href='#'>Exam 1</a></td>"
                    + "</tr>";
            ChapterNum++;
        });

        if (flag == true)
            $('#existing-chapters tbody').html(html);
        //makeChaptersSortable('#existing-chapters tbody');
    }

    function makeChaptersSortable(selector) {
        $(selector).sortable({
            items: '> tr',
            forcePlaceholderSize: true,
            placeholder: 'sort-placeholder',
            start: function (event, ui) {
                // Build a placeholder cell that spans all the cells in the row
                var cellCount = 0;
                $('td, th', ui.helper).each(function () {
                    // For each TD or TH try and get it's colspan attribute, and add that or 1 to the total
                    var colspan = 1;
                    var colspanAttr = $(this).attr('colspan');
                    if (colspanAttr > 1) {
                        colspan = colspanAttr;
                    }
                    cellCount += colspan;
                });

                // Add the placeholder UI - note that this is the item's content, so TD rather than TR
                ui.placeholder.html('<td colspan="' + cellCount + '">&nbsp;</td>');
                //$(this).attr('data-previndex', ui.item.index());
            },
            update: function (event, ui) {
                // gets the new and old index then removes the temporary attribute
                var newOrder = $.map($(this).find('tr'), function (el) {
                    return $(el).attr('data-ch-id') + "-" + $(el).index();
                });
                if (reorderTimer != null)
                    clearTimeout(reorderTimer);
                reorderTimer = setTimeout(function () {
                    updateChapterOrder(newOrder);
                }, 5000);
            },
            helper: function (e, ui) {
                ui.children().each(function () {
                    $(this).width($(this).width());
                });
                return ui;
            }

        }).disableSelection();
    }

    function updateChapterOrder(newOrder) {
        var order = {};
        $.each(newOrder, function (i, v) {
            order[v.split('-')[0]] = v.split('-')[1];
        });
        console.log(order);
        var req = {};
        var res;
        req.courseId = getUrlParameter('courseId');
        req.subjectId = getUrlParameter('subjectId');
        if (req.courseId == "" || req.courseId == null) {
            window.location.href = "profile.php";
            return;
        }
        if (req.subjectId == "" || req.subjectId == null) {
            window.location.href = "profile.php";
            return;
        }
        req.newOrder = order;
        req.action = "update-chapter-order";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            console.log(res);
            res = $.parseJSON(res);
            if (res.status == 1)
                fetchSubjectChapters();
            else
                alertMsg(res.message);
        });
    }

    function fetchInstituteProfessors() {
        var req = {};
        var res;
        req.courseId = getUrlParameter('courseId');
        req.subjectId = getUrlParameter('subjectId');
        req.instituteId = instituteId;
        if (req.courseId == "" || req.courseId == null) {
            window.location.href = "profile.php";
            return;
        }
        if (req.subjectId == "" || req.subjectId == null) {
            window.location.href = "profile.php";
            return;
        }
        req.action = "get-institute-professors";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            console.log(res);
            res = $.parseJSON(res);
            if (res.status == 1)
                fillInstituteProfessors(res);
            else
                alertMsg(res.message);
        });
    }

    function fillInstituteProfessors(data) {
        var option = "";
        var selectElem = '#subject-professors';
        var select = $(selectElem);
        var tempProf = {};
        $.each(data.instituteProfessors, function (i, v) {
            var pName = v.firstName + ' ' + v.lastName;
            option = "<option value='" + v.professorId + "'>" + pName + "</option>";
            select.append(option);
            tempProf[v.professorId] = pName;
        });
        applyMultiselectForProf(selectElem);

        //Html for subject Profs.
        var html = '';
        $.each(data.subjectProfessors, function (i, v) {
            $(selectElem).multiSelect('select', v.professorId);
            html += '<li><a>' + tempProf[v.professorId] + '</a></li>';
        });
        if (html != '')
            $('#subject-professors-teaser').html(html);
        else
            $('#subject-professors-teaser').html('<li><a>No Professors</a></li>');
        // Show Manage  popup if #manage is present in url
        if (typeof window.location.hash !== "undefined" && window.location.hash == "#manage" && popupShown == false) {
            $('#subject-professor-modal').modal('show');
            popupShown = true;
        }
    }

    function formatUserRole(roleId) {
        if (roleId == '1')
            return "Institute";
        if (roleId == '2')
            return "Professor";
        if (roleId == '3')
            return "Publisher";
    }

    function validateForm() {
        $('.form-msg').html('').hide();
        if ($('#edit-form #subject-name').val().length < 5 || $('#edit-form #subject-name').val().length > 100) {
            $('.form-msg.subject-name').html('Invalid subject name. Must be between 5 to 100 chars.').show();
            return false;
        }

        if ($('#edit-form #subject-description').val().length > 1000) {
            $('.form-msg.subject-desc').html('Can not be more than 1000 chars.').show();
            return false;
        }

        return true;
    }

    function validateLicensingForm() {
        $('.form-msg').html('').hide();
        var licOpt1 = $('#apply-licensing #lic-opt-1').prop('checked');
        var licOpt2 = $('#apply-licensing #lic-opt-2').prop('checked');
        var licOpt3 = $('#apply-licensing #lic-opt-3').prop('checked');
        var licOpt4 = $('#apply-licensing #lic-opt-4').prop('checked');
        if (licOpt1 == false && licOpt2 == false && licOpt3 == false && licOpt4 == false) {
            $('.form-msg.licensing').html('Please apply licensing').show();
            return false;
        }
        var licVal1 = $('#apply-licensing #lic-opt-1-price').val();
        var licVal2 = $('#apply-licensing #lic-opt-2-price').val();
        var licVal3 = $('#apply-licensing #lic-opt-3-price').val();
        var licVal4 = $('#apply-licensing #lic-opt-4-price').val();
        var licCommAmt4 = $('#apply-licensing #lic-opt-4-comm').val();
        var licCommPer4 = $('#apply-licensing #lic-opt-4-comm-per').val();
        if (licOpt1 == true && (licVal1 == "" || !isValidPrice(licVal1))) {
            $('.form-msg.licensing-op-1').html('Please provide price for one year licensing').show();
            return false;
        }
        if (licOpt2 == true && (licVal2 == "" || !isValidPrice(licVal2))) {
            $('.form-msg.licensing-op-2').html('Please provide price for one time transfer').show();
            return false;
        }
        if (licOpt3 == true && (licVal3 == "" || !isValidPrice(licVal3))) {
            $('.form-msg.licensing-op-3').html('Please provide price for selective IP transfer').show();
            return false;
        }
        if (licOpt4 == true && (licVal4 == "" || !isValidPrice(licVal4))) {
            $('.form-msg.licensing-op-4').html('Please provide price for commission based licensing').show();
            return false;
        }
        if (licOpt4 == true && licCommPer4 == "" && licCommAmt4 == "") {
            $('.form-msg.licensing-op-4').html('Please provide commission').show();
            return false;
        }

        if (licOpt4 == true) {
            if (licCommAmt4 != "" && licCommPer4 != "") {
                $('.form-msg.licensing-op-4').html('You can provide either percentage or amount').show();
                return false;
            }
            if (licCommAmt4 != "" && !isValidPrice(licCommAmt4)) {
                $('.form-msg.licensing-op-4').html('Not a valid amount').show();
                return false;
            }
            if (licCommPer4 != "" && (!isValidPrice(licCommPer4) || licCommPer4 > 100)) {
                $('.form-msg.licensing-op-4').html('Not a valid precentage').show();
                return false;
            }
        }
        return true;
    }

    //fetchProfileDetails();
    fetchCourseDetails();
    fetchSubjectDetails();
    fetchSubjectChapters();
    fetchInstituteProfessors();

    //Event handlers


    $('#subject-details .edit-button').click(function () {
        $('#subject-details').hide();
        $('#edit-form').fadeIn('fast');
    });

    $('.cancel-edit-button').click(function () {
        $('#edit-form').hide();
        $('#subject-details').fadeIn('fast');
    });

    $('#edit-form  .save-button').on('click', function (e) {
        e.preventDefault();
        if (validateForm() == false)
            return;
        var req = {};
        req.courseId = getUrlParameter('courseId');
        req.subjectId = getUrlParameter('subjectId');
        req.courseName = $('#edit-form #subject-name').val();
        req.courseDesc = $('#edit-form #subject-description').val();
        req.action = 'update-subject';

        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1) {
                alertMsg(res.message);
                $('#edit-form').hide();
                $('#subject-details').fadeIn('fast');
                if (subjectImageChanged)
                    $('#subject-image-form').submit();
                else
                    fetchSubjectDetails();
            } else {
                alertMsg(res.msg);
            }
        });
    });

    $('#create-subject').on('click', function (e) {
        e.preventDefault();
        if (validateForm() == false)
            return;
        var req = {};
        req.courseId = getUrlParameter('courseId');
        req.subjectName = $('#subject-name').val();
        req.subjectDesc = $('#subject-description').val();
        req.action = 'create-subject';

        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1) {
                //alertMsg(res.message);
                $('#subject-name').val('');
                $('#subject-description').val('');
                $('#subject-created #subject-name').html(req.subjectName);
                $('#subject-created').modal('show');
            } else {
                alertMsg(res.msg);
            }
        });
    });

    $('#subject-created #add-subject').on('click', function (e) {
        $('#subject-created').modal('hide');
    });

    $('#apply-licensing button.save').on('click', function (e) {
        e.preventDefault();
        if (validateLicensingForm() == false)
            return;
        var req = {};
        req.courseId = getUrlParameter('courseId');
        var licOpt1 = $('#apply-licensing #lic-opt-1').prop('checked');
        var licOpt2 = $('#apply-licensing #lic-opt-2').prop('checked');
        var licOpt3 = $('#apply-licensing #lic-opt-3').prop('checked');
        var licOpt4 = $('#apply-licensing #lic-opt-4').prop('checked');
        var licVal1 = $('#apply-licensing #lic-opt-1-price').val();
        var licVal2 = $('#apply-licensing #lic-opt-2-price').val();
        var licVal3 = $('#apply-licensing #lic-opt-3-price').val();
        var licVal4 = $('#apply-licensing #lic-opt-4-price').val();
        var licCommAmt4 = $('#apply-licensing #lic-opt-4-comm').val();
        var licCommPer4 = $('#apply-licensing #lic-opt-4-comm-per').val();
        var data = {};
        var selectedLicensing = {};
        if (licOpt1 == true) {
            selectedLicensing["1"] = {
                "price": licVal1,
                "data": data
            }
        }
        if (licOpt2 == true) {
            selectedLicensing["2"] = {
                "price": licVal2,
                "data": data
            }
        }
        if (licOpt3 == true) {
            selectedLicensing["3"] = {
                "price": licVal3,
                "data": data
            }
        }
        if (licOpt4 == true) {
            data = {
                "amount": licCommAmt4,
                "percent": licCommPer4
            };

            selectedLicensing["4"] = {
                "price": licVal4,
                "data": data
            }
        }

        req.selectedLicensing = selectedLicensing;

        req.action = 'update-course-licensing';

        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1) {
                alertMsg(res.message);
                $('#apply-licensing').modal('hide');

            } else {
                alertMsg(res.msg);
            }
        });
    });

    $('#apply-licensing button.cancel').on('click', function (e) {
        $('#apply-licensing').modal('hide');
    });

    $('#update-subject-professors').on('click', function (e) {
        e.preventDefault();
        var req = {};
        req.courseId = getUrlParameter('courseId');
        req.subjectId = getUrlParameter('subjectId');
        req.subjectProfessors = subjectProfessors;

        req.action = 'update-subject-professors';

        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1) {
                alertMsg(res.message);
                $('#subject-professor-modal').modal('hide');
                fetchInstituteProfessors();
            } else {
                alertMsg(res.msg);
            }
        });
    });

    $("#subject-image").change(function () {
        subjectImageChanged = true;
        var input = this;
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            if (typeof jcrop_api != 'undefined' && jcrop_api != null) {
                jcrop_api.destroy();
                jcrop_api = null;
                var pImage = $('.crop');
                pImage.css('height', 'auto');
                pImage.css('width', 'auto');
                var height = pImage.height();
                var width = pImage.width();

                $('.jcrop').width(width);
                $('.jcrop').height(height);
            }
            reader.onload = function (e) {
                $('#edit-form #subject-image-preview').attr('src', e.target.result);
                $('#edit-form  .crop').Jcrop({
                    onSelect: updateCoords,
                    bgOpacity: .4,
                    setSelect: [100, 100, 50, 50],
                    aspectRatio: 1
                }, function () {
                    jcrop_api = this;
                });
            }
            reader.readAsDataURL(input.files[0]);
        }
    });

    function updateCoords(c) {
        console.log(c);
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    }

    var options = {
        beforeSend: function () {
            //alert('sending');
        },
        success: function () {

        },
        complete: function (resp) {
            fetchSubjectDetails();
        },
        error: function () {
            alert('Error in uploading file');
        }
    };

    $('#subject-image-form').attr('action', fileApiEndpoint);

    $('#subject-image-form').ajaxForm(options);

});

function applyMultiselectForProf(selector) {
    $(selector).multiSelect({
        afterSelect: function (value) {
            //alert("Select value: "+values);
            var index = subjectProfessors.indexOf(value[0]);
            if (index === -1)
                subjectProfessors.push(value[0]);

        },
        afterDeselect: function (value) {
            var index = subjectProfessors.indexOf(value[0]);
            subjectProfessors.splice(index, 1);
        }
    });
}
