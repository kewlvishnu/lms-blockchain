var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var r;
$(document).ready(function() {
	fetchinvitedprofessors();
});

function fetchinvitedprofessors() {
	var req = {};
	var res;
	req.action = 'get-invited-professors-details';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		fillinvitedprofessors(res);
		//console.log(res);
	
	});
}
function fillinvitedprofessors(data) {
	htm='';
	var details;
	$.each(data.check, function(idx, obj) {
		name = obj.firstName+' '+obj.lastName;
		uid = obj.userId;
		if(obj.status == 0)
		{
				btn='<td>';
                btn+='<span class="label label-warning label-mini" style="display:block;"> <i class="fa fa-info-circle"></i> Pending</span></td>';
                btn+='<td><div class="btn-group">';
                btn+='<button data-toggle="dropdown" class="btn btn-primary  dropdown-toggle btn-xs" type="button">Action <span class="caret"></span></button>';
                btn+='<ul role="menu" class="dropdown-menu">';
                btn+='<li><a href="#" class="resend-pending">Resend</a></li>';
                btn+='</ul></div></td>';
		}
		else  if(obj.status == 1) {
				btn='<td>';
                btn+='<span class="label label-success label-mini" style="display:block;"> <i class="fa fa-check-circle"></i> Accepted</span></td>';
                btn+='<td></td>';
		}

		else if(obj.status == 2) {
				btn='<td>';
                btn+='<span class="label label-danger  label-mini" style="display:block;"> <i class="fa fa-ban"></i> Rejected</span></td>';
                btn+='<td><div class="btn-group">';
                btn+='<button data-toggle="dropdown" class="btn btn-primary  dropdown-toggle btn-xs" type="button">Action <span class="caret"></span></button>';
                btn+='<ul role="menu" class="dropdown-menu">';
                btn+='<li><a href="#" class="resend-rejected">Resend</a></li>';
                btn+='</ul></div></td>';
		}
		htm+="<tr data-id='" + obj.id + "'><td>"+name+"</td><td>" + obj.email + "</td><td>" + obj.contactMobilePrefix + '-' + obj.contactMobile + "</td>"+btn+"</tr>";
	});
	$('#tab tbody').find('tr').remove();
	$('#tab tbody').html(htm);
	addEventListeners();
}

function updateInvitation(elem) {
	var req = {};
	var res;
	req.action = "update-invitation";
	req.sno = elem.parents('tr').attr('data-id');
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'async': false,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		fetchinvitedprofessors();
	});
}
function addEventListeners() {
	$('.resend-pending, .resend-rejected').on('click', function() {
		updateInvitation($(this));
	});
}