var sortOrder = [];
var examStart = new Date();
var examEnd = new Date('2099/12/31');
$(function() {
	//fetchProfileDetails();
	fetchChapters();
	fetchTemplates();
	fetchBreadcrumb();
	fetchCourseDates();
	
	
	
	
	$('#tempSection').on('blur', function() {
		if ($(this).val() != "") {
			$('#addSection').click();
		}
	});
	
	$('#sections input[type="text"]').on('blur', function() {
		if($('#orderStart').prop('checked')) {
			$('#orderStart').click();
		} else if($('#orderRandom').prop('checked')) {
			$('#orderRandom').click();
		}
	});
	
	$('#course-end').on('click', function() {
		$('#set-exam-date').hide();
	});	
	//disabling user input in dates
	$('#startDate, #endDate').on('keypress', function() {
		return false;
	});
	$('#settime').on('keypress', function() {
		return false;
	});
	
	
	$('#custom-time').on('click', function() {
		$('#set-exam-date').show();
	});
	$('#settime').datetimepicker({
					format: 'd F Y H:i',
					minDate: examStart,
					step: 30,
					
				});
	
	$('#orderRandom').on('click', function() {
		$('#sortOrder').slideUp();
	});
	
	$('#orderStart').on('click', function() {
		$('#sortOrder').find('li').remove();
		var html = '';
		for(var i=0; i<$('#sections').find('input[type="text"]').length; i++) {
			var value = $('#sections input[type="text"]:eq('+i+')').val();
			html += '<li data-original="'+i+'" class="sort-li">' + value + '</li>';
			sortOrder[i] = i+'';
		}
		$('#sortOrder').append(html);
		$('#sortOrder').slideDown();
	});
	
	$('#sortOrder').sortable({revert : true, 
							placeholder : 'sort-placeholder',
							cursor : 'move',
							forcePlaceholderSize : true,
							tolerance : 'pointer',
							opacity : 0.6,
							update : function() {
								for(var i=0; i< $('#sortOrder').find('li').length; i++) {
									sortOrder[i] = $('#sortOrder li:eq('+i+')').attr('data-original');
								}
							}
						});

	$('#templateSelect').on('change', function() {
		if($(this).val() != 0) {
			unsetError($(this));
			var req  = {};
			req.action = 'get-template-details';
			req.templateId = $(this).val();
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 1)
					fillTemplateDetails(res);
				else
					alert(res.message);
			});
		}
	});
		
	$('#fromTemplate').on('change', function() {
		$('#extraFields').slideUp();
		$('#templateSelect').parent().show();
		$('#sections').find('div.form-group').remove();
		$('#noOfAttempts').val('');
		$('.time.hour').val('0h');
		$('.time.minute').val('0m');
		$('#totalTimeHour').val('0h');
		$('#totalTimeMinute').val('0m');
	});
	
	$('#ownSettings').on('change', function() {
		$('#templateSelect').parent().hide();
		$('#extraFields').slideDown();
		$('#sections').find('div.form-group').remove();
		$('#noOfAttempts').val('');
		$('.time.hour').val('0h');
		$('.time.minute').val('0m');
		$('#totalTimeHour').val('0h');
		$('#totalTimeMinute').val('0m');
	});
	
	$('#addSection').on('click', function() {
		$('#tempSection').parent().removeClass('has-error');
		$('#tempSection').next().remove();
		if($('#tempSection').val() != '') {
			var html='<div class="form-group">'
						+ '<div class="row">'
							+ '<div class="col-lg-9">'
								+ '<input type="text" class="form-control" value="'+$('#tempSection').val()+'">'
							+ '</div>'
							+ '<div class="col-lg-3">'
								+ '<button class="btn btn-danger btn-xs delete-button" type="button"><i class="fa fa-trash-o "></i></button>'
							+ '</div>'
						+ '</div>'
					+ '</div>';
			$('#sections').append(html);
			$('#tempSection').val('');
			if($('#switchNo').prop('checked'))
				$('#switchNo').click();
			$('.delete-button').on('click', function() {
				$(this).parents('.form-group').remove();
				$('#sectionTimes .row:eq(0)').find('*').remove();
				if($('#switchNo').prop('checked'))
					$('#switchNo').click();
				if($('#sections').find('input[type="text"]').length <= 1) {
					$('#switchYes').prop('checked', true);
					$('#orderRandom').prop('checked', true);
					$('#switchYes').parents('div:eq(0)').hide();
					$('#orderStart').parents('div:eq(0)').hide();
					$('#sectionTimes').hide();
					$('#totalTime').show();
				}
				if($('#orderStart').prop('checked')) {
					$('#orderStart').click();
				} else if($('#orderRandom').prop('checked')) {
					$('#orderRandom').click();
				}
			});
			if($('#sections').find('input[type="text"]').length > 1) {
				$('#switchYes').prop('checked', false);
				$('#orderRandom').prop('checked', false);
				$('#switchYes').parents('div:eq(0)').show();
				$('#orderStart').parents('div:eq(0)').show();
			}
		}
		else {
			$('#tempSection').parent().addClass('has-error');
			$('#tempSection').parent().append('<span class="help-block">Please provide a section name.</span>');
		}
		
		//event listener for bluring sections name
		$('#sections input[type="text"]').off('blur');
		$('#sections input[type="text"]').on('blur', function() {
			if($('#orderStart').prop('checked')) {
				$('#orderStart').click();
			} else if($('#orderRandom').prop('checked')) {
				$('#orderRandom').click();
			}
		});
		$('#sections input[type="text"]:eq(0)').blur();
	});
	
	$('#switchYes').on('click', function() {
		unsetError($('#totalTimeHour').parent());
		unsetError($('#sectionTimes .row:eq(0)'));
		unsetError($('#orderStart').parent());
		$('#sectionTimes').hide();
		$('#totalTime').show();
	});
	
	$('#switchNo').on('click', function() {
		unsetError($('#totalTimeHour').parent());
		unsetError($('#orderStart').parent());
		unsetError($('#sectionTimes .row:eq(0)'));
		$('#totalTime').hide();
		var html = '';
		$('#sectionTimes .row:eq(0)').find('*').remove();
		if($('#sections').find('input[type="text"]').length == 0) {
			html = '<span style="color: red;">Please Add a section first.</span>';
		}
		for(i=0; i<$('#sections').find('input[type="text"]').length; i++) {
			html += '<div class="col-lg-6">'
					+ '<label for="coursename">Total Time for '+$('#sections').find('input[type="text"]:eq('+i+')').val()+'</label><br>'
					+ '<div class="col-md-4">'
						+ '<div class="input-group input-small">'
							+ '<input type="text" maxlength="3" class="spinner-input form-control time" value="0h" disabled=true>'
							+ '<div class="spinner-buttons input-group-btn btn-group-vertical">'
								+ '<button type="button" class="btn spinner-up btn-xs btn-default generated">'
									+ '<i class="fa fa-angle-up"></i>'
								+ '</button>'
								+ '<button type="button" class="btn spinner-down btn-xs btn-default generated">'
									+ '<i class="fa fa-angle-down"></i>'
								+ '</button>'
							+ '</div>'
						+ '</div>'
					+ '</div> &nbsp;&nbsp;&nbsp;&nbsp;'
					+ '<div class="col-md-4">'
						+ '<div class="input-group input-small">'
							+ '<input type="text" class="spinner-input form-control time" maxlength="3" value="0m" disabled=true>'
							+ '<div class="spinner-buttons input-group-btn btn-group-vertical">'
								+ '<button class="btn spinner-up btn-xs btn-default generated" type="button">'
									+ '<i class="fa fa-angle-up"></i>'
								+ '</button>'
								+ '<button class="btn spinner-down btn-xs btn-default generated" type="button">'
									+ '<i class="fa fa-angle-down"></i>'
								+ '</button>'
							+ '</div>'
						+ '</div>'
					+ '</div>'
				+ '</div>';
		}
		$('#sectionTimes .row:eq(0)').append(html);
		$('#sectionTimes').show();
		$('.spinner-up.generated').on('click', function() {
			var input = $(this).parents('.input-group').find('input[type="text"]');
			var value = input.val();
			var postfix = value.substring(value.length-1);
			var num = value.substring(0, value.length-1);
			num++;
			input.val(num+postfix);
		});
		$('.spinner-down.generated').on('click', function() {
			var input = $(this).parents('.input-group').find('input[type="text"]');
			var value = input.val();
			var postfix = value.substring(value.length-1);
			var num = value.substring(0, value.length-1);
			if(num-1 >= 0)
				num--;
			input.val(num+postfix);
		});
	});
	
	$('.spinner-up').on('click', function() {
		var input = $(this).parents('.input-group').find('input[type="text"]');
		var value = input.val();
		var postfix = value.substring(value.length-1);
		var num = value.substring(0, value.length-1);
		num++;
		input.val(num+postfix);
	});
	
	$('.spinner-down').on('click', function() {
		var input = $(this).parents('.input-group').find('input[type="text"]');
		var value = input.val();
		var postfix = value.substring(value.length-1);
		var num = value.substring(0, value.length-1);
		if(num-1 >= 0)
			num--;
		input.val(num+postfix);
	});
	
	//for assignment and exam dependent timing sections
	$('#examCat').on('click', function() {
		$('#assExm').slideDown();
		nameAvailable();
	});
	$('#assignmentCat').on('click', function() {
		$('#assExm').slideUp();
		nameAvailable();
	});
	
	$('#titleName').on('blur', function() {
		unsetError($(this));
		if(min($(this), 3)) {
			if(!max($(this), 50))
				setError($(this), "Please give a shorter assignment/exam name. Maximum allowed limit is 50 characters.");
		}
		else
			setError($(this), "Please give a longer assignment/exam name. Minimum allowed limit is 3 characters.");
		nameAvailable();
	});
	
	$('#chapterSelect').on('blur', function() {
		unsetError($(this));
		if($(this).val() == 0)
			setError($(this), "Please select a chapter or select independent.");
	});
	
	$('.save-button').on('click', function(){
		var resultshown='immediately';
		var resulttill=1;
		var resulttilldate='';
		$('#tempSection').parent().removeClass('has-error');
		$('#tempSection').next().remove();
		if($('#custom-time').prop('checked') )
		{
			resulttill=2;
		}
		if($('#after-examend').prop('checked'))
		{
			resultshown='after'
		}
		elem = $(this);
		$('#titleName, #chapterSelect').blur();
		unsetError($('#fromTemplate').parent());
		if($('#fromTemplate').prop('checked') || $('#ownSettings').prop('checked')) {
			if($('#fromTemplate').prop('checked')) {
				unsetError($('#templateSelect'));
				if($('#templateSelect').val() == 0) {
					setError($('#templateSelect'), "Please select a template from here.");
					return;
				}
			}
			unsetError($('#noOfAttempts'));
			if($('#noOfAttempts').val() != '') {
				unsetError($('#examCat').parent());
				if($('#assignmentCat').prop('checked') || $('#examCat').prop('checked')) {
					unsetError($('#tempSection').next());
					if($('#sections').find('input[type="text"]').length != 0) {
						unsetError($('#startDate'));
						if($('#startDate').val() != '') {
                                                    if($('#courseEndDate').val()!= '' && $('#endDate').val()!='' ) {
                                                        var setStart = new Date($('#startDate').val());
                                                        var setEnd = new Date($('#endDate').val());
                                                        unsetError($('#startDate'));
                                                        if(setEnd.valueOf() <= setStart.valueOf())
                                                                setError($('#startDate'), 'Plese select proper dates.');
                                                    }
							unsetError($('#endDate'));
							if(($('#endDate').val() == '' && $('#courseEndDate').val() == '') || $('#endDate').val() != '') {
								unsetError($('#switchYes').parent());
							if(resulttill==1 || (resulttill==2  && $('#settime').val() != '')) {
									unsetError($('#course-end').parent());

								if(resultshown =='immediately' || resultshown=='after'  || !$('#examCat').prop('checked')) {
									unsetError($('#immediately').parent());
								if($('#switchYes').prop('checked') || $('#switchNo').prop('checked')  || !$('#examCat').prop('checked')) {
									unsetError($('#endYes').parent());
									if($('#endYes').prop('checked') || $('#endNo').prop('checked')  || !$('#examCat').prop('checked')) {
										unsetError($('#noTimeLost').parent());
										if($('#noTimeLost').prop('checked') || $('#timeLost').prop('checked')  || !$('#examCat').prop('checked')) {
											unsetError($('.save-button'));
											unsetError($('#totalTimeHour').parent());
											unsetError($('#sectionTimes .row:eq(0)'));
											unsetError($('#orderStart').parent());
											if($('.has-error').length == 0 ) {
												var req = {};
												req.action = 'add-exam';
												req.name = $('#titleName').val();
												req.courseId = getUrlParameter('courseId');
												if($('#assignmentCat').prop('checked'))
													req.type = 'Assignment';
												else if($('#examCat').prop('checked'))
													req.type = 'Exam';
												if($('#chapterSelect').val() == -1)
													req.chapterId = 0;
												else
													req.chapterId = $('#chapterSelect').val();
												req.subjectId = getUrlParameter('subjectId');
												req.status = 0;
												var showResult='immediately';
												if($('#after-examend').prop('checked'))
													showResult='after';
												req.showResult=showResult;
												var showResultTill=1;
												var showResultTillDate="";
												if($('#custom-time').prop('checked'))
												{
													showResultTill=2;
													var timestamp = new Date($('#settime').val());
													showResultTillDate = timestamp.getTime();
													
												}
												req.showResultTill=showResultTill;
												req.showResultTillDate=showResultTillDate;
												var timestamp = new Date($('#startDate').val());
												req.startDate = timestamp.getTime();
												req.endDate = '';
												if($('#endDate').val() != '' && $('#courseEndDate').val() != '') {
													timestamp = new Date($('#endDate').val());
													req.endDate = timestamp.getTime();
												}
												req.attempts = $('#noOfAttempts').val();
												req.tt_status = 0;
												if($('#switchYes').prop('checked'))
													req.tt_status = 0;
												else if($('#switchNo').prop('checked'))
													req.tt_status = 1;
												req.totalTime = 6000;
												req.gapTime = 0;
												req.time = [];
												if(req.tt_status == 0  || !$('#examCat').prop('checked')) {
													req.sectionOrder = 2;
													var tth = $('#totalTimeHour').val();
													tth = tth.substring(0, tth.length-1);
													tth = parseInt(tth);
													var ttm = $('#totalTimeMinute').val();
													ttm = ttm.substring(0, ttm.length-1);
													ttm = parseInt(ttm);
													req.totalTime = (tth * 60) + ttm;
													if(req.totalTime <= 0 && $('#examCat').prop('checked')) {
														setError($('#totalTimeHour').parent(), "Please specify a correct time limit.");
														return;
													}
												}
												if(req.tt_status == 1  || !$('#examCat').prop('checked')) {
													var gap = $('#gapTime').val();
													req.gapTime = gap.substring(0, gap.length-1);
													var sectionTimes = $('#sectionTimes').find('input.time');
													for(i=0; i<sectionTimes.length; i = i + 2) {
														var sth = sectionTimes[i]['value'];
														sth = sth.substring(0, sth.length-1);
														sth = parseInt(sth);
														var stm = sectionTimes[i+1]['value'];
														stm = stm.substring(0, stm.length-1);
														stm = parseInt(stm);
														var st = (sth * 60) +stm;
														if(st <= 0 && $('#examCat').prop('checked')) {
															setError($('#sectionTimes .row:eq(0)'), "Please select correct timings for every section.");
															return;
														}
														req.time.push(st);
													}
													if(!($('#orderStart').prop('checked') || $('#orderRandom').prop('checked'))) {
														if($('#examCat').prop('checked')) {
															setError($('#orderStart').parent(), "Please select ordering of the sections.");
															return;
														}
													}
												}
												req.endBeforeTime = 0;
												req.powerOption = 0;
												if($('#endYes').prop('checked'))
													req.endBeforeTime = 1;
												else if($('#endNo').prop('checked'))
													req.endBeforeTime = 0;
												if($('#noTimeLost').prop('checked'))
													req.powerOption = 0;
												else if($('#timeLost').prop('checked'))
													req.powerOption = 1;
												req.sections = [];
												req.weights = [];
												var sections = $('#sections').find('input[type="text"]');
												for(i=0; i<sections.length; i++) {
													req.sections.push(sections[i]['value']);
													req.weights.push(sortOrder.indexOf(i+''));
												}
												if(req.tt_status == 0) {
													for(i=0; i<sections.length; i++) {
														req.time.push(0);
													}
												}
												req.sectionOrder = 0;
												if($('#orderStart').prop('checked'))
													req.sectionOrder = 0;
												else if($('#orderRandom').prop('checked'))
													req.sectionOrder = 1;
												if($('#ownSettings').prop('checked')) {
													var con = confirm("Do you want to save this as a template.");
													if(con) {
														var templateName = prompt("Please give your template a name.");
														if(templateName == '')
															templateName = 'Untitled template';
														var treq = {};
														treq.action = 'add-template';
														treq.name = templateName;
														treq.type = req.type;
														treq.attempts = req.attempts;
														treq.tt_status = req.tt_status;
														treq.totalTime = req.totalTime;
														treq.gapTime = req.gapTime;
														treq.sectionOrder = req.sectionOrder;
														treq.powerOption = req.powerOption;
														treq.endBeforeTime = req.endBeforeTime;
														treq.sections = req.sections;
														treq.time = req.time;
														treq.weights = req.weights;
														$.ajax({
															'type'  : 'post',
															'url'   : ApiEndpoint,
															'data' 	: JSON.stringify(treq)
														});
													}
												}
												$.ajax({
													'type'  : 'post',
													'url'   : ApiEndpoint,
													'data' 	: JSON.stringify(req)
												}).done(function (res) {
													res =  $.parseJSON(res);
													if(res.status == 1) {
														/*if(elem.hasClass('proceed'))
															window.location = 'add-assignment-2.php?examId='+res.examId;
														else
															window.location = 'assignment-exam.php';*/
													}
													else
														alertMsg(res.message);
												});
											}
											else
												setError($('.save-button'), "Please review your forms. It contains errors.");
										}
										else
											setError($('#noTimeLost').parent(), "Please select an option to continue.");
									}
									else
										setError($('#endYes').parent(), "Please select one option.");
								}
								else
									setError($('#switchYes').parent(), "Please select a option to switch between sections or not.");
							}
							else
									setError($('#immediately').parent(), "Please select a option when student can see the result.");
							}
							else
									setError($('#course-end').parent(), "Please select a option/End date till when student can see the result.");
							}
							else
								setError($('#endDate'), "Please specify a correct end date.");
						}
						else
							setError($('#startDate'), "Please specify a correct start date.");
					}
					else
						setError($('#tempSection'), "Please add atleast one section.");
				}
				else
					setError($('#examCat').parent(), "Please select a test type.")
			}
			else
				setError($('#noOfAttempts'), "Please specify number of attempts allowed.");
		}
		else
			setError($('#fromTemplate').parent(), "Please select any one import setting.");
	});
	
	function fetchBreadcrumb() {
		var req = {};
		var res;
		req.action = 'get-breadcrumb-for-add';
		req.subjectId = getUrlParameter('subjectId');
		req.courseId = getUrlParameter('courseId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
				fillBreadcrumb(res);
		});
	}
	
	function fillBreadcrumb(data) {
		$('ul.breadcrumb li:eq(0)').find('a').attr('href', 'edit-course.php?courseId=' + data.courseId).text(data.courseName);
		$('ul.breadcrumb li:eq(1)').find('a').attr('href', 'edit-subject.php?courseId=' + data.courseId + '&subjectId=' + data.subjectId).text(data.subjectName);
	}
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src',data.profileDetails.profilePic);
		}
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fetchChapters() {
		var req = {};
		req.action = "get-chapters-for-exam";
		req.subjectId = getUrlParameter('subjectId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillChaptersSelect(res);
		});
	}
	
	function fillChaptersSelect(data) {
		if(data.status == 0) {
			alert(data.message);
			return;
		}
		var opts = '';
		for(i=0; i<data.chapters.length; i++) {
			opts += '<option value="'+data.chapters[i]['id']+'">'+data.chapters[i]['name']+'</option>';
		}
		opts += '<option value="-1">Independent</option>';
		$('#chapterSelect').append(opts);
		var xx = getUrlParameter('chapterId');
		if(xx)
			$('#chapterSelect').val(xx);
	}
	
	function fetchCourseDates() {
		var req = {};
		var res;
		req.action = 'get-course-date';
		req.courseId = getUrlParameter('courseId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				$('#courseStartDate').val(res.dates.liveDate);
				$('#courseEndDate').val(res.dates.endDate);
				
				//adding handlers for datetimepicker of start and end date
				var now = new Date();
				var start = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
				var courseStart = new Date(parseInt(res.dates.liveDate));
				if(start.valueOf() < courseStart.valueOf())
					examStart = new Date(courseStart.getTime());
				else
					examStart = new Date(start.getTime());
				
				var courseEnd = new Date(parseInt(res.dates.endDate));
				var end = new Date('2099/12/31');
				if(end.valueOf() < courseEnd.valueOf())
					examEnd = new Date(courseEnd.getTime());
				else
					examEnd = new Date(end.getTime());
				if(res.dates.endDate == '') {
					//alert('hello');
					$('#endDate').parent().hide();
					$('#result-show').hide();
					$('#set-exam-date').hide();					
					examEnd = new Date(end.getTime());
				}
				
				$('#startDate').datetimepicker({
					format: 'd F Y H:i',
					minDate: examStart,
					step: 30,
					onSelectTime: function(date) {
						unsetError($('#startDate'));
						if (date.valueOf() > examEnd.valueOf()) {
							setError($('#startDate'), 'The start date can not be greater than the end date.');
						} else {
							examStart = date;
						}
					}
				});
				$('#endDate').datetimepicker({
					format: 'd F Y H:i',
					minDate: examStart,
					maxDate: examEnd,
					step: 30,
					onSelectTime: function(date) {
						unsetError($('#endDate'));
						if (date.valueOf() < examStart.valueOf()) {
							setError($('#endDate'), 'The end date can not be less than the start date.');
						} else {
							examEnd = date;
						}
					}
				});
			}
		});
	}
	
	function fetchTemplates() {
		var req = {};
		req.action = "get-templates-for-exam";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillTemplateSelect(res);
		});
	}
	
	function fillTemplateSelect(data) {
		if(data.status == 0) {
			alert(data.message);
			return;
		}
		var opts = '';
		for(i=0; i<data.templates.length; i++) {
			opts += '<option value="'+data.templates[i]['id']+'">'+data.templates[i]['name']+'</option>';
		}
		$('#templateSelect').append(opts);
	}
	
	function fillTemplateDetails(data) {    
		if(data.sections.length > 1) {
			$('#switchYes').prop('checked', false);
			$('#orderRandom').prop('checked', false);
			$('#switchYes').parents('div:eq(0)').show();
			$('#orderStart').parents('div:eq(0)').show();
		}
		$('#extraFields').slideDown();
		$('#noOfAttempts').val(data.template.attempts);
		if(data.template.type == 'Assignment') {
			$('#assignmentCat').prop('checked', true);
			$('#assignmentCat').click();
		}
		else if(data.template.type == 'Exam') {
			$('#examCat').prop('checked', true);
			//$('#immediately').prop('checked', true);
			$('#examCat').click();
		}
		$('#startDate').val(data.template.startDate);
		$('#endDate').val(data.template.endDate);
		/*$('.delete-button').on('click', function() {
				alert("hello17889");
					$(this).parents('.form-group').remove();
					$('#sectionTimes .row:eq(0)').find('*').remove();
					$('#switchNo').click();
					$('#sectionTimes').hide();
					$('#totalTime').show();
					if($('#orderStart').prop('checked')) {
						$('#orderStart').click();
					} else if($('#orderRandom').prop('checked')) {
						$('#orderRandom').click();
					}
				});*/
		if(data.template.endBeforeTime == 0)
			$('#endYes').prop('checked', true);
		else
			$('#endNo').prop('checked', true);
		if(data.template.powerOption == 0)
			$('#noTimeLost').prop('checked', true);
		else
			$('#timeLost').prop('checked', true);
		if(data.template.totalTime == 0 || data.template.totalTime == 6000) {
			$('#switchNo').prop('checked', true);
			$('#sectionTimes').show();
			$('#totalTime').hide();
			$('#gapTime').val(data.template.gapTime+'m');
			$('#sections .form-group').remove();
			$('#sectionTimes .row:eq(0)').find('*').remove();
			$('.delete-button').on('click', function() {
				//alert("hello17889");
					$(this).parents('.form-group').remove();
					$('#sectionTimes .row:eq(0)').find('*').remove();
					$('#switchNo').click();
					$('#sectionTimes').hide();
					$('#totalTime').show();
					if($('#orderStart').prop('checked')) {
						$('#orderStart').click();
					} else if($('#orderRandom').prop('checked')) {
						$('#orderRandom').click();
					}
				});
				
			for(i=0; i<data.sections.length; i++) {
				var html='<div class="form-group">'
						+ '<div class="row">'
							+ '<div class="col-lg-9">'
								+ '<input type="text" class="form-control" value="'+data.sections[i].name+'" id="'+data.sections[i].id+'">'
							+ '</div>'
							+ '<div class="col-lg-3">'
								+ '<button class="btn btn-danger btn-xs delete-button" type="button" style="margin-left:5px;"><i class="fa fa-trash-o "></i></button>'
							+ '</div>'
						+ '</div>'
					+ '</div>';
				$('#sections').append(html);
				
				var htmlTime = '<div class="col-lg-6">'
					+ '<label for="coursename">Total Time for '+data.sections[i].name+'</label><br>'
					+ '<div class="col-md-4">'
						+ '<div class="input-group input-small">'
							+ '<input type="text" maxlength="3" class="spinner-input form-control time hour" value="0h" disabled=true>'
							+ '<div class="spinner-buttons input-group-btn btn-group-vertical">'
								+ '<button type="button" class="btn spinner-up btn-xs btn-default generated">'
									+ '<i class="fa fa-angle-up"></i>'
								+ '</button>'
								+ '<button type="button" class="btn spinner-down btn-xs btn-default generated">'
									+ '<i class="fa fa-angle-down"></i>'
								+ '</button>'
							+ '</div>'
						+ '</div>'
					+ '</div> &nbsp;&nbsp;&nbsp;&nbsp;'
					+ '<div class="col-md-4">'
						+ '<div class="input-group input-small">'
							+ '<input type="text" class="spinner-input form-control time minute" maxlength="3" value="0m" disabled=true>'
							+ '<div class="spinner-buttons input-group-btn btn-group-vertical">'
								+ '<button class="btn spinner-up btn-xs btn-default generated" type="button">'
									+ '<i class="fa fa-angle-up"></i>'
								+ '</button>'
								+ '<button class="btn spinner-down btn-xs btn-default generated" type="button">'
									+ '<i class="fa fa-angle-down"></i>'
								+ '</button>'
							+ '</div>'
						+ '</div>'
					+ '</div>'
				+ '</div>';
				$('#sectionTimes .row:eq(0)').append(htmlTime);
				hour = Math.floor(data.sections[i].time / 60);
				minute = data.sections[i].time - (hour * 60);
				$('#sectionTimes input[type="text"].hour:eq('+i+')').val(hour+'h');
				$('#sectionTimes input[type="text"].minute:eq('+i+')').val(minute+'m');
			}
		}
		else {
			$('#sections .form-group').remove();
			$('#sectionTimes .row:eq(0)').find('*').remove();
			$('.delete-button').on('click', function() {
					$(this).parents('.form-group').remove();
					$('#sectionTimes .row:eq(0)').find('*').remove();
					$('#switchNo').click();
					$('#sectionTimes').hide();
					$('#totalTime').show();
					if($('#orderStart').prop('checked')) {
						$('#orderStart').click();
					} else if($('#orderRandom').prop('checked')) {
						$('#orderRandom').click();
					}
				});
			for(i=0; i<data.sections.length; i++) {
				var html='<div class="form-group">'
						+ '<div class="row">'
							+ '<div class="col-lg-9">'
								+ '<input type="text" class="form-control" value="'+data.sections[i].name+'" id="'+data.sections[i].id+'">'
							+ '</div>'
							+ '<div class="col-lg-3">'
								+ '<button class="btn btn-danger btn-xs delete-button" type="button" style="margin-left:5px;"><i class="fa fa-trash-o "></i></button>'
							+ '</div>'
						+ '</div>'
					+ '</div>';
				$('#sections').append(html);
				$('.delete-button').on('click', function() {
				$(this).parents('.form-group').remove();
				$('#sectionTimes .row:eq(0)').find('*').remove();
				if($('#switchNo').prop('checked'))
					$('#switchNo').click();
				if($('#sections').find('input[type="text"]').length <= 1) {
					$('#switchYes').prop('checked', true);
					$('#orderRandom').prop('checked', true);
					$('#switchYes').parents('div:eq(0)').hide();
					$('#orderStart').parents('div:eq(0)').hide();
					$('#sectionTimes').hide();
					$('#totalTime').show();
				}
				if($('#orderStart').prop('checked')) {
					$('#orderStart').click();
				} else if($('#orderRandom').prop('checked')) {
					$('#orderRandom').click();
				}
			});
			
				if($('#orderStart').prop('checked')) {
					//$('#orderStart').click();
				} else if($('#orderRandom').prop('checked')) {
					//$('#orderRandom').click();
				}
			}
			$('#switchYes').prop('checked', true);
			$('#sectionTimes').hide();
			$('#totalTime').show();
			hour = Math.floor(data.template.totalTime / 60);
			minute = data.template.totalTime - (hour * 60);
			$('#totalTimeHour').val(hour+'h');
			$('#totalTimeMinute').val(minute+'m');
		}
		if(data.template.sectionOrder == 0) {
			$('#orderStart').prop('checked', true);
			$('#orderStart').click();
		}
		else
			$('#orderRandom').prop('checked', true);
		$('#sections input[type="text"]').on('blur', function() {
			if($('#orderStart').prop('checked')) {
				$('#orderStart').click();
			} else if($('#orderRandom').prop('checked')) {
				$('#orderRandom').click();
			}
		});
	}
	
	function min(what, length) {
		if(what.val().length < length)
			return false;
		else
			return true;
	}
	
	function max(what, length) {
		if(what.val().length > length)
			return false;
		else
			return true;
	}
	
	function setError(where, what) {
		unsetError(where);
		where.parent().addClass('has-error');
		where.parent().append('<span class="help-block">'+what+'</span>');
	}
	
	function unsetError(where) {
		if(where.parent().hasClass('has-error')) {
			where.parent().find('.help-block').remove();
			where.parent().removeClass('has-error');
		}
	}
//        function unsetErrorBefore(where) {
//		if(where.parent().hasClass('has-error')) {
//			where.parent().parent().find('.help-block').remove();
//			where.parent().removeClass('has-error');
//		}
//	}
	
	function nameAvailable() {
		if($('#titleName').val().length != '' && ($('#examCat').prop('checked') || $('#assignmentCat').prop('checked'))) {
			var req = {};
			var res;
			req.action = 'check-name-for-exam';
			req.name = $('#titleName').val();
			if($('examCat').prop('checked'))
				req.name += '_E';
			else
				req.name += '_A';
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else {
					unsetError($('#titleName'));
					if(!res.available)
						setError($('#titleName'),'Please select a different name as you have already used this name.');
				}
			});
		}
	}
   $("#noOfAttempts").keypress(function (e) {
        $('.help-block').remove();
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $('#noOfAttempts').after('<span class="help-block text-danger">Enter Only Numbers</span>');
            //$('.help-block').fadeOut("slow");
            // ("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
});