var sortOrder = [];
var num = [];
var attempt = [];
var allowed;
var attemptFill = [];
var total = 0;

$(function() {
	fetchBreadcrumb();
	//fetchProfileDetails();
	fetchStudentList();
	$('#examAnalysis').attr("href","examAnalysis.php?examId="+getUrlParameter('examId'));
	
	
	function fetchBreadcrumb() {
		var req = {};
		var res;
		req.action = "get-breadcrumb-for-exam";
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
				fillBreadcrumb(res);
		});
	}
	
	function fillBreadcrumb(data) {
		$('ul.breadcrumb li:eq(0)').find('a').attr('href', 'edit-course.php?courseId=' + data.course.courseId).text(data.course.courseName);
		$('ul.breadcrumb li:eq(1)').find('a').attr('href', 'edit-subject.php?courseId=' + data.course.courseId + '&subjectId=' + data.subChap.subjectId).text(data.subChap.subjectName);
		if(data.subChap.chapterId == null)
			$('ul.breadcrumb li:eq(2)').find('a').html('<strong>Independent</strong>');
		else
			$('ul.breadcrumb li:eq(2)').find('a').attr('href', 'content.php?courseId=' + data.course.courseId + '&subjectId=' + data.subChap.subjectId + '&chapterId=' + data.subChap.chapterId).html('<strong>' + data.subChap.chapterName + '</strong>');
	}
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	//download-list
	$('#download-list').on('click', function () {
		$('#downloadcustom').modal('show');
		$('#downloadCustom-list').on('click', function () {
			var avg=1;
			var highest=0;
			var attempts=0;
			var lastscore=0;
			if($('#cbox1').is(':checked'))
			{
				avg=1;
			}			
			else{
				avg=0;
			}
			if($('#cbox2').is(':checked'))
			{
				highest=1;
			}			
			if($('#cbox3').is(':checked'))
			{
				attempts=1;
			}			
			if($('#cbox4').is(':checked'))
			{
				lastscore=1;
			}			
				
			var req = {};
			var res;
			req.action = 'get-student-list-download';
			req.examId = getUrlParameter('examId');
			req.avg=avg;
			req.highest=highest;
			req.attempts=attempts;
			req.lastscore=lastscore;
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndpoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				console.log(res);
				if(res.status == 1){
					var url = res.path;
					window.location.href = url;
					$('#downloadcustom').modal('hide');
					//window.open(url, '_blank');
				}				
				else {
					
					if(res.students.length==0)
					{
						alertMsg("There are no students in the course");
					}else{
						alertMsg(res.message);	
					}
				}
		});
		
	});
		
		
	});
	
	//function to get exam report
	function fetchExamReport() {
		var req = {};
		var res;
		req.action = 'get-exam-report';
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				initGraphs(res);
			}
		});
	}
	
	//function to initiate all graphs
	function initGraphs(data) {
		$('#hero-donut').highcharts({
			chart: {
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false
			},
			title: {
				text: ''
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						format: '<b>{point.name}</b>: {point.percentage:.1f} %',
						style: {
							color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
						}
					}
				}
			},
			series: [{
				type: 'pie',
				name: 'Number',
				data: [
					['Attempted', parseInt(data.attempted)],
					['Unattempted', parseInt(total - data.attempted)]
				]
			}],
			colors: ['green','red']
		});
		
		$.each(data.graph, function(i, d) {
			num.push(d.num);
			attempt.push(d.attempt);
		});
		allowed = data.allowed;
		for(var i = 0; i <= allowed; i++) {
			var temp = [];
			temp.push(i + '');
			needle = attempt.indexOf(temp[0]);
			if(needle == -1)
				temp.push(0);
			else
				temp.push(parseInt(num[needle]));
			attemptFill.push(temp);
		}
		var html = '';
		var count = 0;
		for(var i = 0; i <= allowed; i=i+6) {
			html += '<option value="'+count+'">'+i+'-'+(i+6)+'</option>';
			count++;
		}
		$('#attSelector').append(html);
		initStudentGraph(0);
		$('#attSelector').on('change', function() {
			initStudentGraph($(this).val());
		})
		$('#expand').on('click', function() {
			if($(this).attr('data-hidden') == 0) {
				$(this).parents('.col-lg-4').removeClass('col-lg-4').addClass('col-lg-12');
				$(this).html('<<');
				$(this).attr('data-hidden', 1);
			} else {
				$(this).parents('.col-lg-12').removeClass('col-lg-12').addClass('col-lg-4');
				$(this).html('>>');
				$(this).attr('data-hidden', 0);
			}
			initStudentGraph($('#attSelector').val());
		});
		
		$('#hero-bar').highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: ''
			},
			xAxis: {
				type: 'category',
				labels: {
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			},
			yAxis: {
				title: {
					text: '<b>Scores</b>'
				}
			},
			legend: {
				enabled: false
			},
			tooltip: {
				pointFormat: 'Score: <b>{point.y}</b>'
			},
			series: [{
				name: 'Attempts',
				data: [
					['Max<br>Score', parseInt(data.mma.max)],
					['Avg<br>Score', parseInt(data.mma.avg)],
					['Median<br>Score', parseInt(data.mma.median)],
					['Min<br>Score', parseInt(data.mma.min)]
				],
				colorByPoint: true,
				dataLabels: {
					enabled: true,
					color: '#000',
					align: 'center',
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif',
					}
				}
			}],
			colors: ['green', 'yellow', 'blue', 'red']
		});
	}
	
	function initStudentGraph(number) {
		var start = number * 6;
		var temp = attemptFill.slice(start, start+6);
		//temp =temp.splice(start, 6);
		$('#studentGraph').highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: ''
			},
			xAxis: {
				type: 'category',
				labels: {
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				},
				title: {
					text: '<b>Attempts</b>'
				}
			},
			yAxis: {
				min: 0,
				title: {
					text: '<b>Number of Students</b>'
				}
			},
			legend: {
				enabled: false
			},
			tooltip: {
				pointFormat: 'attempts by <b>{point.y}</b> Students'
			},
			series: [{
                    name: 'Attempts',
                    data: temp,
					dataLabels: {
						enabled: true,
						color: '#000',
						align: 'center',
						style: {
							fontSize: '13px',
							fontFamily: 'Verdana, sans-serif',
						}
					}
                }]
		});
	}
	
	//function to fetch student list who have attempted the exam
	function fetchStudentList() {
		var req = {};
		var res;
		req.action = 'get-student-list';
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alertMsg(res.message);
			else {
				total = res.students.length;
				fetchExamReport();
				fillStudentList(res);
			}
		});
	}
	
	//function to fill student list table
	function fillStudentList(data) {
		console.log(data);
		var html = '';
		//detecting the highest and lowest scorers
		if(data.toppers.length > 0) {
			var highest = [];
			var lowest = [];
			var max = data.toppers[0].score;
			var min = data.toppers[0].score;

			for(var i = 0; i < data.toppers.length; i++) {
				if(data.toppers[i].score == max)
					highest.push(data.toppers[i].studentId);
				else if(highest.indexOf(data.toppers[i].studentId) == -1)
					lowest.push(data.toppers[i].studentId);
			}
		}
		for(var i = 0; i < data.students.length; i++) {
			if(data.students[i].details.max == null)
				data.students[i].details.max = '--';
			if(data.students[i].details.avg == null)
				data.students[i].details.avg = '--';
			else
				data.students[i].details.avg=parseFloat(data.students[i].details.avg).toFixed(2);
			
			var lastscore="";
			if(data.students[i].lastScore == null  || data.students[i].lastScore == undefined)
			{
				lastscore='--';
			}				
			else{
				lastscore= data.students[i].lastScore.score;
			}
			html += '<tr>'
					+ '<td>' + data.students[i].userId + '</td>'
					+ '<td><a href="studentResult.php?examId=' + getUrlParameter('examId') + '&studentId=' + data.students[i].userId + '">' + data.students[i].firstName + ' ' + data.students[i].lastName;
			if(highest.indexOf(data.students[i].userId) != -1)
				html += '&emsp;<i class="fa fa-thumbs-o-up text-success" title="Highest Scorer"></i>';
			else if(lowest.indexOf(data.students[i].userId) != -1)
				html += '&emsp;<i class="fa fa-thumbs-o-down text-danger" title="Lowest Scorer"></i>';
			html += '</a></td>'
					+ '<td>' + data.students[i].email + '</td>'
					+ '<td>' + data.students[i].details.total + '</td>'
					+ '<td>' + lastscore + '</td>'
					+ '<td>' + data.students[i].details.max + '</td>'
					+ '<td>' + data.students[i].details.avg + '</td>'
				+ '</tr>';
		}
		$('#studentList tbody').append(html);
		$('#studentList').show();
		$('#studentList').dataTable( {
			"aaSorting": [[ 4, "desc" ]]
		});
	}
	
});