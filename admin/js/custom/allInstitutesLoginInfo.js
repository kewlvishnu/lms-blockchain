var ApiEndpoint = '../api/index.php';
var month = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

$(function () {
	var imageRoot = '/';
	$('#loader_outer').hide();
	var fileApiEndpoint = '../api/files1.php';

	function viewInstitutes() {
		var req = {};
		var res;
		req.action = "viewInstitutesLoginInfo";
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			var html = "";
			for (var i = 0; i < data.institutes.length; i++)
			{
				var id = data.institutes[i].Id;
				var roleId = data.institutes[i].roleId;
				var name = data.institutes[i].Name;
				var contactMobile = data.institutes[i].contactMobile;
				var Role = data.institutes[i].Role;
				var Username = data.institutes[i].Username;
				// var course_key = data.institutes[i].Key_Id;
				var email = data.institutes[i].email;
				if(data.institutes[i].LastLogin != '') {
					var LastLogin = parseInt(data.institutes[i].LastLogin) * 1000;
					var date = new Date(LastLogin);
					var display_date = date.getDate() + ' ' + month[date.getMonth()] + ' ' + date.getFullYear() + '<br/>' + date.getHours() + ':' + date.getMinutes();
				}
				else
					var display_date = '--';
				var Active = data.institutes[i].Active;
				var display = '';
				if (Active == 1) {
					display = '<span class="btn-success btn-xs ">Active</span>';
				} else {
					display = '<span class="btn-danger btn-xs ">Deactive</span>';
				}
				html += '<tr instituteId=' + id + ' data-roleId="' + roleId + '" data-email="' + email + '"><td>' + (i + 1) + '</td><td>' + id + '</td><td>' + name + ' </td><td>' + Role
						+ '</td><td>' + Username + '</td><td class=small-txt>' + email + ' </td><td>' + contactMobile + ' </td><td >' + display_date
						+ '</td><td status=' + Active + '>' + display + '<br><a class="btn btn-primary btn-xs changeStatus" type="button" >Change</a></td>'
						+ '<td> <button class="btn btn-info btn-xs log-in">Log in As</button></tr>';
			}
			$('#tbl_view_institutes tbody').find('tr').remove();
			$('#tbl_view_institutes tbody').append(html);
          	$('#tbl_view_institutes').dataTable({
                "aaSorting": [[0, "asc"]]
            });
		$('.changeStatus').off('click');
		$('.changeStatus').on('click', function () {
			var req = {};
			var res;
			var status = $(this).parents('td').attr('status');
			var instituteId = $(this).parents('tr').attr('instituteId');
			var changedstatus = '';
			if (status == 0) {
				changedstatus = 1;
			} else {
				changedstatus = 0;
			}
			if (confirm("Are you sure To Change status ? ")) {
				req.action = "ChangeLoginstatus";
				req.instituteId = instituteId;
				req.status = changedstatus;
				$.ajax({
					'type': 'post',
					'url': ApiEndpoint,
					'data': JSON.stringify(req)
				}).done(function (res) {
					viewInstitutes();
				});
			}
		});
		$('.log-in').off('click');
		$('#tbl_view_institutes').on( 'click', '.log-in', function (e) {
		//$('.log-in').on('click', function() {
			var req = {};
			var res;
			req.action = 'ghost-login';
			req.userId = $(this).parents('tr').attr('instituteId');
			req.roleId = $(this).parents('tr').attr('data-roleId');
			req.email = $(this).parents('tr').attr('data-email');
			$.ajax({
				'type'  : "post",
				'url'   : ApiEndpoint,
				data: JSON.stringify(req)   
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0) {
					alertMsg(res.message);
				}
				else {
					window.location = '../';
				}
			});
		})
   });
}
viewInstitutes();

});
