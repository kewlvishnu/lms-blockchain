var ApiEndPoint = '../api/index.php';
$(document).ready(function(){
	fetchSubjectiveExamAttempts();
});
//to add BCT dynamically
function fetchSubjectiveExamAttempts() {
	var req = {};
	var res;
	req.action = 'get-check-subjective-exam-students';
	req.examId = getUrlParameter('examId');
	req.subjectId = getUrlParameter('subjectId');
	req.courseId = getUrlParameter('courseId');
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status == 0)
			console.log(res.message);
		else
			console.log(res);
			fullSubjectiveExamAttempts(res.students);
	});
}
function fullSubjectiveExamAttempts(data) {
	for (var i = 0; i < data.length; i++) {
		$('#subjectiveExams').append(
			'<div class="panel panel-default">'+
				'<div class="panel-heading" role="tab" id="heading'+data[i].userId+'">'+
					'<h4 class="panel-title">'+
						'<a class="'+((i==0)?'':'collapsed')+'" role="button" data-toggle="collapse" data-parent="#" href="#collapse'+data[i].userId+'" aria-expanded="'+((i==0)?'true':'false')+'" aria-controls="collapse'+data[i].userId+'">'+
							data[i].name+
						'</a>'+
					'</h4>'+
				'</div>'+
				'<div id="collapse'+data[i].userId+'" class="panel-collapse collapse '+((i==0)?'in':'')+'" role="tabpanel" aria-labelledby="heading'+data[i].userId+'">'+
					'<table class="table"><tr><th>Attempt</th><th>Start Date</th><th>End Date</th><th>Checked</th></tr></table>'+
				'</div>'+
			'</div>');
			for (var j = 0; j < data[i]["attempts"].length; j++) {
				//$('#subjectiveExams .panel-default:last-child .list-group').append('<li class="list-group-item"><a href="checkSubjectiveExam.php?attemptId='+data[i]["attempts"][j].attemptId+'">Attempt '+(j+1)+' ['+formatTime(data[i]["attempts"][j].startDate)+' - '+formatTime(data[i]["attempts"][j].endDate)+']</a></li>');
				$('#subjectiveExams .panel-default:last-child .table').append(
					'<tr>'+
						'<td>'+
							'<a href="checkSubjectiveExam.php?examId='+getUrlParameter('examId')+'&subjectId='+getUrlParameter('subjectId')+'&courseId='+getUrlParameter('courseId')+'&attemptId='+data[i]["attempts"][j].attemptId+'">Attempt '+(j+1)+'</a>'+
						'</td>'+
						'<td>'+formatTime(data[i]["attempts"][j].startDate)+'</td>'+
						'<td>'+formatTime(data[i]["attempts"][j].endDate)+'</td>'+
						'<td><span class="badge">'+((data[i]["attempts"][j].checked==1)?'Yes':'No')+'</span></td>'+
					'</tr>');
			};
	};
}
//function to format time
function formatTime(time) {
	var a = new Date(time * 1000);
	var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var hour = a.getHours();
	var min = a.getMinutes();
	var sec = a.getSeconds();
	var formattedTime = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
	return formattedTime;
}