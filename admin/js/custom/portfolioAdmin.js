
var ApiEndpoint = '../api/index.php';

$(function () {
    var imageRoot = '/';
    $('#loader_outer').hide();
    var fileApiEndpoint = '../api/files1.php';
    
    $('#tblPortfolioApprovals').on('click', '.btn-modal', function(e){
        e.preventDefault();
        var pfId = $(this).closest('tr').attr('data-id');
        var modal = '#approveModal';
        $('#selectApproval').val('approve');
        $('#inputReason').val('');
        $('.js-reason').addClass('invisible');
        $(modal).find('.js-btn-submit').attr('data-update',pfId);
        $(modal).modal('show');
    });

    $('#selectApproval').change(function(){
        if($(this).val() == 'reject') {
            $('.js-reason').removeClass('invisible');
        } else {
            $('.js-reason').addClass('invisible');
        }
    });

    function portfolioApprovals() {
        var req = {};
        var res;
        req.action = "get-portfolio-approvals";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            var html = "";
            for (var i = 0; i < data.portfolios.length; i++) {
                html+=
                    '<tr data-id="'+data.portfolios[i].id+'">'+
                        '<td>'+(i+1)+'</td>'+
                        '<td>'+data.portfolios[i].userId+'</td>'+
                        '<td>'+data.portfolios[i].name+'</td>'+
                        '<td>'+data.portfolios[i].email+'</td>'+
                        '<td>'+data.portfolios[i].mobile+'</td>'+
                        '<td>'+data.portfolios[i].userType+'</td>'+
                        '<td>'+data.portfolios[i].subdomain+'</td>'+
                        '<td>'+data.portfolios[i].portfolioName+'</td>'+
                        '<td>'+data.portfolios[i].portfolioDesc+'</td>'+
                        '<td>'+data.portfolios[i].status+'</td>'+
                        '<td><button class="btn btn-warning btn-sm btn-modal" data-target="#approveModal">Process</button></td>'+
                    '</tr>';
            };
            $('#tblPortfolioApprovals').append(html);
        });
    }
    portfolioApprovals();
    $('#btnApprove').click(function(){
        var pfId           = $(this).attr('data-update');
        var selectApproval = $('#selectApproval').val();
        var inputReason    = $('#inputReason').val();
        if(!pfId) {
            $('#frmApproval .help-block').html('<p class="text-danger text-center">Please refresh and try again</p>');
        } else if(!selectApproval) {
            $('#frmApproval .help-block').html('<p class="text-danger text-center">Please select approval type</p>');
        } else {
            var req            = {};
            req.pfId           = pfId;
            req.selectApproval = selectApproval;
            if(selectApproval == 'reject') {
                req.inputReason = inputReason;
            }
            var res;
            req.action = "save-portfolio-approval";
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if(res.status == 1) {
                    $('#tblPortfolioApprovals').find('tr[data-id='+pfId+']').remove();
                    $('#approveModal').modal('hide');
                } else {
                    $('#frmPortfolioPages .help-block').html('<p class="text-danger text-center">'+res.message+'</p>');
                }
                console.log(res);
            });
        }
    });
});
