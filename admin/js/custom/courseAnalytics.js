var filler_00 = [];
var filler_25 = [];
var filler_50 = [];
var filler_75 = [];
var filler_76 = [];
var cats = [];
var contentId=[];
var students=[];
var contentProgress=[];
var contentTimeSpent=[];
var initdt=0;
var contentName=[];
$(function() {
	fetchCourseAnalytics();
	fetchCourseStudentAnalytics();
		
	//function to set the courses progress byt students
	function fetchCourseStudentAnalytics() {
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action ="get-student-progress-details";
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			console.log(res);
			//console.log('Hello');
			if(res.status == 0){
				alert(res.message);
				window.location.href ='edit-subject.php?courseId='+getUrlParameter('courseId')+'&subjectId='+getUrlParameter('subjectId');
			} else {
				students=res.studentEnrolled;
				contentProgress=res.content;
			}
		});
	}
	//function to get number bar for course progress
	function fetchCourseAnalytics() {
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action ="get-subject-progress-details";
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			console.log(res);
			if(res.status == 0)
				alert(res.message);
			else {
				initGraphs(res);
			}
		});
	}
	
	//function to initiate all graphs
	function initGraphs(data) {
		console.log(data);
		$.each(data.content, function(i, d) {
			//console.log(d);
			contentName.push(d.title);
			if(d.title.length > 20) {
						var first = d.title.substring(0, 20);
						//var second = d.title.substring(d.title.length - 8);
						d.title = first + '...';// + second;
					}
			cats.push(d.title);			
			
			
			contentId.push(d.id);
			filler_76.push(parseInt(d.content_76));
			filler_75.push(parseInt(d.content_75));
			filler_50.push(parseInt(d.content_50));
			filler_25.push(parseInt(d.content_25));
			filler_00.push(parseInt(d.content_00));
		});
		$('#courseAnalytics').highcharts({
			colors: ['#2ecc71','#5C97BF','#4ECDC4','#F39C12','#bdc3c7'],
			chart: {
				type: 'bar'
			},
			title: {
				text: 'Content Viewed By Students'
			},
			tooltip: {
				followPointer: true,
				pointFormat: '<b>{point.y}</b> students {series.name}'
			},
			xAxis: {
				categories: cats,
				type: 'category',
				labels: {
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				},
				title: {
					text: '<b>Contents</b>'
				}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Total Students'
			}
		},
		legend: {
			reversed: true,
			// align: 'right',
		},
			plotOptions: {
			series: {
				stacking: 'normal',
				cursor: 'pointer',
				point: {
					
					events: {  
						click: function () {
							//console.log(contentProgress);
							var html="";
							var id=this.x;
							var totalTime=0;
							var progress=contentProgress[id].studentprogress;
							if(contentProgress[id].totalTime !=undefined){
								///	console.log(contentProgress[id].totalTime);
								for (var key in contentProgress[id].totalTime) {
									if(contentProgress[id].totalTime[key]!='') {
										//totalTime=parseFloat(contentProgress[id].totalTime[key]).toFixed(2);
										totalTime=((contentProgress[id].totalTime[key]=='0')?'-':contentProgress[id].totalTime[key]);
									}
									else{
										totalTime=0;
									}
								}
							}
							//console.log(totalTime);
							//console.log(contentProgress);							
							//console.log(contentProgress[id]);		
							$('#detailAnalytics').modal('show');
							$('#content-name').html(' '+contentName[id] +' ');
							//$('#detail-StudentProgress tbody').html('').html('');

							console.log(progress);
							$.each(progress, function(i, d) {
									d=Math.ceil(d);
									if(d>100){
										d=100;
									}
									//console.log(i);
									//console.log(contentProgress[id].totalTime);
									if(contentProgress[id].totalTime!='' && contentProgress[id].totalTime!=undefined){														//	console.log(contentProgress[id].totalTime[i]);
										if(contentProgress[id].totalTime[i] != undefined){
											//totalTime=parseFloat(contentProgress[id].totalTime[i]).toFixed(2);
											totalTime=((contentProgress[id].totalTime[i]=='0')?'-':contentProgress[id].totalTime[i]);
										}else{
											totalTime=0;
										}
										//console.log(totalTime);
									}	  
									else{
										totalTime=0;
									}
									//console.log(contentProgress[i].totalTime);
									//console.log(contentProgress[i]);
									//console.log(i);
									html += "<tr data-cid='" + i + "'" + "" + ">"
									+"<td>" + students[i] + "</td>"
									+ "<td class='text-center'>" + d+ "</td>"
									+ "<td class='text-center'>" + totalTime+ "</td>";
							});
							$('#detail-StudentProgress tbody').html('').html(html);		
							$('#detail-StudentProgress').show(); 
							$("#detail-StudentProgress").css("width","100%")  
							$('#detail-StudentProgress').dataTable({
									"aaSorting": [[ 4, "desc" ]]
							});
						}
					}//clik ended
				}//event ended
			}//points ended
		},
		series: [
		{
			name: 'More than 75 % watched',
			data: filler_76
		},{
			name: 'Between 50 -75 % Watched',
			data: filler_75
		},{
			name: 'Between 25 -50 % Watched',
			data: filler_50
		},{
			name: 'Below 25 % Watched',
			data: filler_25
		},{
			name: 'Not Watched',
			data: filler_00
		}]
		});
	}
	
	// function to destroy data table each time it close button is clicked
	$('.datatablecll').on('click', function() {
			var table = $('#detail-StudentProgress').DataTable();
			table.fnDestroy();
	});
	
});