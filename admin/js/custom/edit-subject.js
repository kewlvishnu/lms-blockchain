/*
	Author: Fraaz Hashmi.
	Dated: 6/28/2014
	
	
	********
	1) Notes: JSON.stringify support for IE < 10
*/

var userRole;
var reorderTimer = null;
var subjectProfessors = [];
var popupShown = false;
var subjectImageChanged = false;
var subjectpdfChanged = false;
var reloadFlag=false;
var filePdf;

$(function () {
	var imageRoot = '/';
	$('#loader_outer').hide();
	
	fetchIndependentExams();
	fetchSubjectiveExams();
	fetchSubjectRating();
	fetchManualExams();

	//event listener for view review
	$('#viewReview').on('click', function() {
		window.location = 'subjectRating.php?courseId=' + getUrlParameter('courseId') + '&subjectId=' + getUrlParameter('subjectId');
	});
	$('#viewCourseAnalytics').on('click', function() {
		window.location = 'viewCourseAnalytics.php?courseId=' + getUrlParameter('courseId') + '&subjectId=' + getUrlParameter('subjectId');
	});
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
			//console.log(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fetchCourseDetails() {
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action = "get-course-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			//console.log(res);
			res =  $.parseJSON(res);
			fillCourseDetails(res);
		});
	}
	
	function fillCourseDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#course-name').html('<a href="edit-course.php?courseId=' + data.courseDetails.id + '">' + data.courseDetails.name + '</a>');
		if(data.courseDetails.image == '') {
			var image = defaultImagePath;
		} else {
			var image = data.courseDetails.image;
		}
		$('#course-image-preview').attr('src', image);
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fetchSubjectDetails(data) {
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action = "get-subject-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			//console.log(res);
			res =  $.parseJSON(res);
			if (res.status == 0) {
				alert(res.message);
				window.location = "edit-course.php?courseId="+getUrlParameter('courseId');
			}
			fillSubjectDetails(res);
		});
	}
	
	function fillSubjectDetails(data) {
		if(data.subjectDetails.deleted == 1)
			alertMsg('This subject has been deleted!!', -1);
		var courseId = getUrlParameter('courseId');
		var subjectId = getUrlParameter('subjectId');
		$('#subject-name-title').html(data.subjectDetails.name);
		$('#subject-name-span').html(data.subjectDetails.name);
		$('#edit-form #subject-name').val(data.subjectDetails.name);
		$('#subject-id').html('S' + String("000" + data.subjectDetails.id).slice(-4));
		var showdata='';
		if (data.subjectDetails.description) {
			showdata=data.subjectDetails.description.replace(new RegExp('\r?\n','g'), '<br />');	
		};
		$('#subject-desc').html(showdata);
				
		$('#edit-form #subject-description').val(data.subjectDetails.description);
		$('.add-chapter-link').attr('href', 'content.php?courseId=' + courseId +'&subjectId=' + subjectId);
		$('.add-exam-link').attr('href', 'add-assignment.php?courseId=' + courseId +'&subjectId=' + subjectId);
		$('.add-subjective-exam-link').attr('href', 'add-subjectiveassignment.php?courseId=' + courseId +'&subjectId=' + subjectId);
		$('.add-manual-exam-link').attr('href', 'add-manualexam.php?courseId=' + courseId +'&subjectId=' + subjectId);
		
		var imageRoot = 'user-data/images/';
		if(data.subjectDetails.image == '') {
			var image = defaultImagePath;
		} else {
			var image = data.subjectDetails.image;
		}
		
		$('#subjectImage').attr('src', image);
		$('#subject-image-preview').attr('src', image);
	}

	function fetchSubjectChapters() {
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		//req.action ="get-subject-progress-details"; 
		req.action ="get-subject-chapters";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1)
				fillSubjectChapters(res);
			else
				alertMsg(res.message);
		});
	}
	
	function fillSubjectChapters(data) {
		var html = "";
		var flag = false;
		var editLink;
		var ChapterNum = 1;
		$.each(data.chapters, function(i, chapter) {
			if(flag == false)
				flag = true;
			editLink = "content.php?courseId=" + data.courseId + "&subjectId=" + chapter.subjectId;
			html += "<tr data-chapter='"+ chapter.name+"' data-ch-id='" + chapter.id  + "'>"
					+ "<td><a data-href='" + editLink + "' class='modal-pop' data-chapterId='" + chapter.id + "'>Section "+ ChapterNum + ": " + chapter.name + "</a>&emsp;&emsp;<a href='" + editLink + "&edit=1'><i class='fa fa-pencil'></i></a>&emsp;<a href='#'><i class='fa fa-trash-o delete-subject'></i></a></td><td><a href='" + editLink + "' class='btn btn-info btn-xs'>Content</a></td>";
			var examhtml = '<td>';
			var assignmenthtml = '<td>';
			for(var j = 0; j < chapter.exams.length; j++) {
				if(chapter.exams[j].type == "Exam")
					examhtml += '<a examId='+chapter.exams[j].id+' independent=1 type=Exam data-startDate="'+chapter.exams[j].startDate+'" data-endDate="'+chapter.exams[j].endDate+'" data-status='+chapter.exams[j].status+' data-attempts='+chapter.exams[j].attempts+'   class="examPopup" >' + chapter.exams[j].name + ' </a><br />';
				else if(chapter.exams[j].type == "Assignment")
					  assignmenthtml += '<a examId='+chapter.exams[j].id+' independent=1 type=Assignment data-startDate="'+chapter.exams[j].startDate+'" data-endDate="'+chapter.exams[j].endDate+'"  data-status='+chapter.exams[j].status+' data-attempts='+chapter.exams[j].attempts+'   class="examPopup" >' + chapter.exams[j].name + ' </a><br />';
			}
			examhtml += '</td>';
			assignmenthtml += '</td>';
			html += assignmenthtml + examhtml + "</tr>";
			ChapterNum++;
		});
		
		if(flag == true)
			$('#existing-chapters tbody').html(html);
		else
			$('#existing-chapters tbody').html('No Section added yet '); 
			makeChaptersSortable('#existing-chapters tbody');
		
		//event handlers for chapter delete button click
				$('.delete-subject').off('click');
		$('.delete-subject').on('click', function() {
			var con = confirm("Are you sure to delete this Section? All associated exams and assignments will become independent.");
			if(con) {
				var chapterId = $(this).parents('tr:eq(0)').attr('data-ch-id');
				var req = {};
				var res;
				req.action = 'delete-chapter';
				req.chapterId = chapterId;
				$.ajax({
					'type'  : 'post',
					'url'   : ApiEndpoint,
					'data' 	: JSON.stringify(req)
				}).done(function (res) {
					res =  $.parseJSON(res);
					if(res.status == 0)
						alert(res.message);
					else {
						alertMsg('Section deleted successfully');
						fetchSubjectChapters();
					}
				});
			}
		});
		//event handlers for chapter modal pop up
		$('.modal-pop').on('click', function() {
			$('#chapter-click').modal('show');
			$('#chapter-click .content-link').attr('href', $(this).attr('data-href'));
			$('#chapter-click .exam-link').attr('href', 'add-assignment.php?courseId=' + getUrlParameter('courseId') + '&subjectId=' + getUrlParameter('subjectId') + '&chapterId=' + $(this).attr('data-chapterId'));
		});
		$('.examPopup').off('click');
		$('.examPopup').on('click', function () {
			var tr = $(this).parent().parent('tr');
			var data=tr.attr('data-ch-id');
			var chapter='';
			var independent=$(this).attr('independent');
			if(independent==0){
				chapter='Independent';
			}else{
				chapter=tr.attr('data-chapter');
			}
			var type = $(this).attr('type');
			var examName = $(this).text();
			var startDate = new Date(parseInt($(this).attr('data-startDate')));
			var displayEndDate='';
			if(($(this).attr('data-endDate'))==''){
				displayEndDate='--';
			}else{
				displayEndDate=new Date(parseInt($(this).attr('data-endDate')));
			}
			var endDate = displayEndDate;
			var attempts =$(this).attr('data-attempts');
			var status =$(this).attr('data-status');
			var examId=$(this).attr('examId');
			var displayStatus='';
			if(status==0)
				displayStatus='Draft';
			else if (status==1)
				displayStatus='NOT Live';
			else
				displayStatus='Live';

			$('#examId').val(examId);
			$('#modelin-exam-name').text(examName);
			$('#modal-copy-exam #new-exam').val('Copy of '+examName);
			$('#modelin-chapter-name').text(chapter);
			$('#modelin-exam-startDate').text(startDate.getDate() + ' ' + MONTH[startDate.getMonth()] + ' ' + startDate.getFullYear());
			$('#modelin-exam-attempts').text(attempts);
			$('#modelin-exam-status').text(displayStatus);
			if(endDate == '--'){
				  $('#modelin-exam-endDate').text(endDate);
			}else{
				  $('#modelin-exam-endDate').text(endDate.getDate() + ' ' + MONTH[endDate.getMonth()] + ' ' + endDate.getFullYear());
			}

			$('#modelin-exam-type').text(type);
			$('#modal-exam .step1').attr('href','edit-assignment.php?examId='+examId);
			$('#modal-exam .step2').attr('href','add-assignment-2.php?examId='+examId);
			$('#modal-exam .report').attr('disabled', false);
			if(status != 0)
				$('#modal-exam .report').attr('href','examResult.php?examId='+examId);
			else
				$('#modal-exam .report').attr('disabled', true);
			//$('#modal-exam .report').attr('href','examResult.php?examId='+examId);
			
			$('#modal-exam').modal('show');

			//$('#chapter-click .content-link').attr('href', $(this).attr('data-href'));
			//$('#chapter-click .exam-link').attr('href', 'add-assignment.php?courseId=' + getUrlParameter('courseId') + '&subjectId=' + getUrlParameter('subjectId'));
		});
	}
	$('#modal-exam .delete-exam').off('click');
	$('#modal-exam .delete-exam').on('click', function () {
		var con = confirm("Are you sure you want to delete this Exam/Assignment.");
		if (con) {
			var req = {};
			req.action = 'delete-exam';
			req.examId = $('#modal-exam #examId').val();
			$.ajax({
				'type': 'post',
				'url': ApiEndpoint,
				'data': JSON.stringify(req)
			}).done(function (res) {
				res = $.parseJSON(res);
				if (res.status == 0)
					alert(res.message);
				else {
					//fetchExams($('#subjectSelect').val());
					fetchSubjectChapters();
					$('#modal-exam').modal('hide');
				}
			});
		}
	});
	
	$('#modal-copy-exam #copy-exam').off('click');
	$('#modal-copy-exam #copy-exam').on('click', function () {
		$(this).attr('disabled', true);
		var newExamname = $('#modal-copy-exam #new-exam').val();
		if (newExamname != null) {
			var req = {};
			req.action = 'copy-exam';
			req.iexamId = $('#modal-exam #examId').val();
			req.iname = newExamname;
			$.ajax({
				'type': 'post',
				'url': ApiEndpoint,
				'data': JSON.stringify(req)
			}).done(function (res) {
				$('#modal-copy-exam #copy-exam').attr('disabled', false);
				res = $.parseJSON(res);
				if (res.status == 0)
					alert(res.message);
				else {
					//fetchExams($('#subjectSelect').val());
					fetchSubjectChapters();
					$('#modal-copy-exam').modal('hide');
				}
			});
		}
	});
	
	function makeChaptersSortable(selector) {
		$(selector).sortable({
			items: '> tr',
			forcePlaceholderSize: true,
			placeholder:'sort-placeholder',
			start: function (event, ui) {
				// Build a placeholder cell that spans all the cells in the row
				var cellCount = 0;
				$('td, th', ui.helper).each(function () {
					// For each TD or TH try and get it's colspan attribute, and add that or 1 to the total
					var colspan = 1;
					var colspanAttr = $(this).attr('colspan');
					if (colspanAttr > 1) {
						colspan = colspanAttr;
					}
					cellCount += colspan;
				});

				// Add the placeholder UI - note that this is the item's content, so TD rather than TR
				ui.placeholder.html('<td colspan="' + cellCount + '">&nbsp;</td>');
				//$(this).attr('data-previndex', ui.item.index());
			},
			update: function(event, ui) {
				// gets the new and old index then removes the temporary attribute
				var newOrder = $.map($(this).find('tr'), function(el) {
					return $(el).attr('data-ch-id')+ "-" +$(el).index();
				});
				if(reorderTimer != null)
					clearTimeout(reorderTimer);
				reorderTimer = setTimeout(function(){updateChapterOrder(newOrder);}, 5000);
			},
			helper: function(e, ui) {
				ui.children().each(function() {
					$(this).width($(this).width());
				});
				return ui;
			}
			
		}).disableSelection();
	}
	
	//function to fetch all independent exams and assignments
	function fetchIndependentExams() {
		var req = {};
		var res;
		req.subjectId = getUrlParameter('subjectId');
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action = "get-independent-exams";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1)
				fillIndependentExams(res);
			else
				alertMsg(res.message);
		});
	}
	
	//function to fill independent exams table
	function fillIndependentExams(data) {
		var ehtml = [];
		var ahtml = [];
		var html='';
		var ne = 0;
		var na = 0;
		if (data.exams.length>0) {
			for(var i = 0; i < data.exams.length; i++) {
				if(data.exams[i].type == "Exam") {
					ehtml[ne]= '<td><a examId='+data.exams[i].id+' independent=0 type=Exam data-startDate="'+data.exams[i].startDate+'" data-endDate="'+data.exams[i].endDate+'"  data-status='+data.exams[i].status+' data-attempts='+data.exams[i].attempts+'   class=examPopup >' + data.exams[i].name + ' </a></td>';
					ne++;
				}
				else if(data.exams[i].type == "Assignment") {
					ahtml[na]= '<td><a examId='+data.exams[i].id+' independent=0 type=Assignment data-startDate="'+data.exams[i].startDate+'" data-endDate="'+data.exams[i].endDate+'"  data-status='+data.exams[i].status+' data-attempts='+data.exams[i].attempts+'   class=examPopup >' + data.exams[i].name + ' </a></td>';
					na++;
				}
			}
		}
		if(data.exams.length == 0)
			html = 'No exams or assignments found';
		else {
			if (ne>na) {
				for (var i = 0; i < ne-na; i++) {
					ahtml[na+i]= '<td></td>';
				};
			} else if(na>ne) {
				for (var i = 0; i < na-ne; i++) {
					ehtml[ne+i]= '<td></td>';
				};
			}
			html = "";
			for (var i = 0; i < ahtml.length; i++) {
				html+= '<tr>' + ahtml[i] + '' + ehtml[i] + '</tr>';	
			};
			//console.log(html);
		}
		$('#independentExams tbody').append(html);
		$('.examPopup').off('click');
		$('.examPopup').on('click', function () {
			var tr = $(this).parent().parent('tr');
			var data=tr.attr('data-ch-id');
			var chapter='';
			var independent=$(this).attr('independent');
			if(independent==0){
				chapter='Independent';
			}else{
				chapter=tr.attr('data-chapter');
			}
			var type = $(this).attr('type');
			var examName = $(this).text();
			var startDate = new Date(parseInt($(this).attr('data-startDate')));
			var displayEndDate='';
			if(($(this).attr('data-endDate'))==''){
				displayEndDate='--';
			}else{
				displayEndDate=new Date(parseInt($(this).attr('data-endDate')));
			}
			var endDate = displayEndDate;
			var attempts =$(this).attr('data-attempts');
			var status =$(this).attr('data-status');
			var examId=$(this).attr('examId');
			var displayStatus='';
			if(status==0)
				displayStatus='Draft';
			   else if (status==1)
					displayStatus='NOT Live';
				else
					 displayStatus='Live';
			
			$('#examId').val(examId);
			$('#modelin-exam-name').text(examName);
			$('#modal-copy-exam #new-exam').val('Copy of '+examName);
			$('#modelin-chapter-name').text(chapter);
			$('#modelin-exam-startDate').text(startDate.getDate() + '-' +(startDate.getMonth()+1)+'-'+startDate.getFullYear());
			$('#modelin-exam-attempts').text(attempts);
			$('#modelin-exam-status').text(displayStatus);
			if(endDate == '--'){
				  $('#modelin-exam-endDate').text(endDate);
			}else{
				  $('#modelin-exam-endDate').text(endDate.getDate() + '-' +(endDate.getMonth()+1)+'-'+endDate.getFullYear());
			}
		  
			$('#modelin-exam-type').text(type);
			$('#modal-exam .step1').attr('href','edit-assignment.php?examId='+examId);
			$('#modal-exam .step2').attr('href','add-assignment-2.php?examId='+examId);
			$('#modal-exam .report').attr('href','examResult.php?examId='+examId);
			
			$('#modal-exam').modal('show');

			//$('#chapter-click .content-link').attr('href', $(this).attr('data-href'));
			//$('#chapter-click .exam-link').attr('href', 'add-assignment.php?courseId=' + getUrlParameter('courseId') + '&subjectId=' + getUrlParameter('subjectId'));
		});
	}


	//function to fetch all independent exams and assignments
	function fetchSubjectiveExams() {
		var req = {};
		var res;
		req.subjectId = getUrlParameter('subjectId');
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action = "get-subjective-exams";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1)
				fillSubjectiveExams(res);
			else
				alertMsg(res.message);
		});
	}
	
	//function to fill subjective exams table
	function fillSubjectiveExams(data) {
		//console.log(data);
		var ehtml = '';
		var ahtml = '';
		var html='';
		var courseId = getUrlParameter('courseId');
		var subjectId = getUrlParameter('subjectId');
		for(var i = 0; i < data.exams.length; i++) {
			if(data.exams[i].type == "Exam")
				ehtml += '<tr data-exam-id="'+data.exams[i].id+'"><td><a examId='+data.exams[i].id+' independent=0 type=Exam data-startDate="'+data.exams[i].startDate+'" data-endDate="'+data.exams[i].endDate+'"  data-status='+data.exams[i].status+' data-attempts='+data.exams[i].attempts+'   class=examPopup1 >' + data.exams[i].name + ' </a></td><td><a href="checkSubjectiveExams.php?examId='+data.exams[i].id+'&courseId='+courseId+'&amp;subjectId='+subjectId+'" class="btn btn-info btn-xs">Check</a></td><td><a href="javascript:void(0)" class="btn btn-xs btn-primary js-delete-subjective-exam"><i class="fa fa-trash-o"></i> Delete</a></tr>';
			else if(data.exams[i].type == "Assignment")
				ahtml += '<tr data-exam-id="'+data.exams[i].id+'"><td><a examId='+data.exams[i].id+' independent=0 type=Assignment data-startDate="'+data.exams[i].startDate+'" data-endDate="'+data.exams[i].endDate+'"  data-status='+data.exams[i].status+' data-attempts='+data.exams[i].attempts+'   class=examPopup1 >' + data.exams[i].name + ' </a></td><td><a href="checkSubjectiveExams.php?examId='+data.exams[i].id+'&courseId='+courseId+'&amp;subjectId='+subjectId+'" class="btn btn-info btn-xs">Check</a></td><td><a href="javascript:void(0)" class="btn btn-xs btn-primary js-delete-subjective-exam"><i class="fa fa-trash-o"></i> Delete</a></tr>';
		}
		if(data.exams.length == 0)
			html = 'No subjective exams found';
		else
			html = ehtml;
		$('#subjectiveExams').html(
									'<thead>'+
										'<tr>'+
											'<th width="50%">Subjective Exams</th>'+
											'<th width="25%">Check</th>'+
											'<th width="25%"></th>'+
										'</tr>'+
									'</thead>'+
									'<tbody></tbody>');
		$('#subjectiveExams tbody').append(html);
		$('#subjectiveExams').on("click", ".js-delete-subjective-exam", function(){
			var con = confirm("Are you sure you want to delete this Subjective Exam?");
			if(con) {
				var examId = $(this).closest('tr').attr('data-exam-id');
				var req = {};
				var res;
				req.action = 'delete-subjective-exam';
				req.examId = examId;
				$.ajax({
					'type'  : 'post',
					'url'   : ApiEndpoint,
					'data' 	: JSON.stringify(req)
				}).done(function (res) {
					res =  $.parseJSON(res);
					if(res.status == 0) {
						alert(res.message);
						fetchSubjectiveExams();
					} else {
						alertMsg('Subjective Exam deleted successfully');
						fetchSubjectiveExams();
					}
				});
			}
		});
		$('.examPopup1').off('click');
		$('.examPopup1').on('click', function () {
			var tr = $(this).parent().parent('tr');
			var data=tr.attr('data-ch-id');
			var chapter='';
			var independent=$(this).attr('independent');
			var type = $(this).attr('type');
			var examName = $(this).text();
			var startDate = new Date(parseInt($(this).attr('data-startDate')));
			var displayEndDate='';
			if(($(this).attr('data-endDate'))==''){
				displayEndDate='--';
			}else{
				displayEndDate=new Date(parseInt($(this).attr('data-endDate')));
			}
			var endDate = displayEndDate;
			var attempts =$(this).attr('data-attempts');
			var status =$(this).attr('data-status');
			var examId=$(this).attr('examId');
			var displayStatus='';
			if(status==0)
				displayStatus='NOT Live';
				else
					 displayStatus='Live';
			
			$('#examId').val(examId);
			var subjectId = getUrlParameter('subjectId');
		    var  courseId = getUrlParameter('courseId');
			$('#modelin-exam-name1').text(examName);
			//$('#modal-copy-exam #new-exam').val('Copy of '+examName);
			//$('#modelin-chapter-name1').text(chapter);
			$('#modelin-exam-startDate1').text(startDate.getDate() + '-' +(startDate.getMonth()+1)+'-'+startDate.getFullYear());
			$('#modelin-exam-attempts1').text(attempts);
			$('#modelin-exam-status1').text(displayStatus);
			if(endDate == '--'){
				  $('#modelin-exam-endDate1').text(endDate);
			}else{
				  $('#modelin-exam-endDate1').text(endDate.getDate() + '-' +(endDate.getMonth()+1)+'-'+endDate.getFullYear());
			}
		  
			$('#modelin-exam-type1').text(type);
			$('#modal-subjectiv-exam .editsubjectiveExam').attr('href','edit-subjectiveassignment.php?examId='+examId+'&courseId='+courseId+'&subjectId='+subjectId);
			//$('#modal-exam .step2').attr('href','add-assignment-2.php?examId='+examId);
			//$('#modal-exam .report').attr('href','examResult.php?examId='+examId);
			
			$('#modal-subjectiv-exam').modal('show');

			//$('#chapter-click .content-link').attr('href', $(this).attr('data-href'));
			//$('#chapter-click .exam-link').attr('href', 'add-assignment.php?courseId=' + getUrlParameter('courseId') + '&subjectId=' + getUrlParameter('subjectId'));
		});
	}

	//function to fetch all independent exams and assignments
	function fetchManualExams() {
		var req = {};
		var res;
		req.subjectId = getUrlParameter('subjectId');
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action = "get-manual-exams";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1)
				fillManualExams(res);
			else
				alertMsg(res.message);
		});
	}
	
	//function to fill subjective exams table
	function fillManualExams(data) {
		console.log(data);
		var ehtml = '';
		var html='';
		var courseId = getUrlParameter('courseId');
		var subjectId = getUrlParameter('subjectId');
		if(data.exams.length == 0)
			html = 'No manual exams found';
		else {
			for(var i = 0; i < data.exams.length; i++) {
				if(data.exams[i].type == "quick")
					html += '<tr data-mexam-id='+data.exams[i].id+'><td><a href="manualexam.php?courseId='+courseId+'&subjectId='+subjectId+'&examId='+data.exams[i].id+'" data-status='+data.exams[i].status+'>' + data.exams[i].name + ' </a></td><td>Quick Analytics</td><td><a href="javascript:void(0)" class="btn btn-xs btn-primary js-delete-manual-exam"><i class="fa fa-trash-o"></i> Delete</a></td></tr>';
				else if(data.exams[i].type == "deep")
					html += '<tr data-mexam-id='+data.exams[i].id+'><td><a href="manualexam.php?courseId='+courseId+'&subjectId='+subjectId+'&examId='+data.exams[i].id+'" data-status='+data.exams[i].status+'>' + data.exams[i].name + ' </a></td><td>Deep Analytics</td><td><a href="javascript:void(0)" class="btn btn-xs btn-primary js-delete-manual-exam"><i class="fa fa-trash-o"></i> Delete</a></td></tr>';
			}
		}
		$('#manualExams').html(
								'<thead>'+
									'<tr>'+
										'<th width="50%">Manual Exams</th>'+
										'<th width="25%">Type</th>'+
										'<th width="25%"></th>'+
									'</tr>'+
									'</thead><tbody>'+
								'</tbody>');
		$('#manualExams tbody').append(html);
		$('#manualExams').on("click", ".js-delete-manual-exam", function(){
			var con = confirm("Are you sure you want to delete this Manual Exam?");
			if(con) {
				var examId = $(this).closest('tr').attr('data-mexam-id');
				var req = {};
				var res;
				req.action = 'delete-manual-exam';
				req.examId = examId;
				$.ajax({
					'type'  : 'post',
					'url'   : ApiEndpoint,
					'data' 	: JSON.stringify(req)
				}).done(function (res) {
					res =  $.parseJSON(res);
					if(res.status == 0) {
						alert(res.message);
					} else {
						alertMsg('Manual Exam deleted successfully');
						fetchManualExams();
					}
				});
			}
		});
	}
	
	function updateChapterOrder(newOrder) {
		var order = {};
		$.each(newOrder, function(i, v){
			order[v.split('-')[0]] = v.split('-')[1];
		});
		//console.log(order);
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		req.newOrder = order;
		req.action = "update-chapter-order";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			//console.log(res);
			res =  $.parseJSON(res);
			if(res.status == 1)
				fetchSubjectChapters();
			else
				alertMsg(res.message);
		});
	}
	
	function fetchInstituteProfessors() {
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action = "get-institute-professors";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			//console.log(res);
			res =  $.parseJSON(res);
			if(res.status == 1) {
				fillInstituteProfessors(res);
				$('#manageProfessorSection').show();
			}
			else if(res.status == 0)
				alertMsg(res.message);
		});
	}
	
	function fillInstituteProfessors(data) {
		var option = "";
		var selectElem = '#subject-professors';
		var select = $(selectElem);
		var tempProf = {};
		$.each(data.instituteProfessors, function(i,v) {
			var pName = v.firstName + ' ' + v.lastName;
			option = "<option value='" + v.professorId + "'>" + pName + "</option>";
			select.append(option);
			tempProf[v.professorId] = pName;
		});
		applyMultiselectForProf(selectElem);
		
		//Html for subject Profs.
		var html = '';
		$.each(data.subjectProfessors, function(i,v) {
			$(selectElem).multiSelect('select', v.professorId);
			html += '<li data-professor="' + v.professorId + '"><a>' + tempProf[v.professorId] + '<span class="delete-professors pull-right"><i class="fa fa-trash-o" style="cursor: pointer;"></i></span></a></li>';
		});
		if(html != '') {
			$('#subject-professors-teaser').html(html);
			$('.delete-professors').on('click', function() {
				var req = {};
				var res;
				var elem = $(this);
				var professor = elem.parents('li').attr('data-professor');
				con = confirm("Are you sure to delete this professor?");
				if(con) {
					req = {};
					req.professorId = professor;
					req.subjectId = getUrlParameter('subjectId');
					req.action = 'delete-single-professor';
					$.ajax({
						'type'	:	'post',
						'url'	:	ApiEndpoint,
						'data'	:	JSON.stringify(req)
					}).done(function(res) {
						res = $.parseJSON(res);
						if(res.status == 0) {
							alert(res.message);
						}
						else {
							subjectProfessors = subjectProfessors.splice(subjectProfessors.indexOf(professor));
							alertMsg('Professor removed successfully');
							elem.parents('li').remove();
							$('#subject-professors').multiselect('deselect_all');
							$('#subject-professors').multiselect('select', subjectProfessors);
						}
					});
				}
			});
		}
		else {
			$('#subject-professors-teaser').html('<li><a>No Professors</a></li>');
		}
		// Show Manage  popup if #manage is present in url
		if(typeof window.location.hash !== "undefined" && window.location.hash == "#manage" && popupShown == false) {
			$('#subject-professor-modal').modal('show');
			popupShown = true;
		}
	}
	
	function formatUserRole(roleId) {
		if(roleId == '1')
			return "Institute";
		if(roleId == '2')
			return "Professor";
		if(roleId == '3')
			return "Publisher";
	}
	
	function validateLicensingForm() {
		$('.form-msg').html('').hide();
		var licOpt1 = $('#apply-licensing #lic-opt-1').prop('checked');
		var licOpt2 = $('#apply-licensing #lic-opt-2').prop('checked');
		var licOpt3 = $('#apply-licensing #lic-opt-3').prop('checked');
		var licOpt4 = $('#apply-licensing #lic-opt-4').prop('checked');
		if(licOpt1 == false && licOpt2 == false && licOpt3 == false && licOpt4 == false){
			$('.form-msg.licensing').html('Please apply licensing').show();
			return false;
		}
		var licVal1 = $('#apply-licensing #lic-opt-1-price').val();
		var licVal2 = $('#apply-licensing #lic-opt-2-price').val();
		var licVal3 = $('#apply-licensing #lic-opt-3-price').val();
		var licVal4 = $('#apply-licensing #lic-opt-4-price').val();
		var licCommAmt4 = $('#apply-licensing #lic-opt-4-comm').val();
		var licCommPer4 = $('#apply-licensing #lic-opt-4-comm-per').val();
		if(licOpt1 == true &&  (licVal1 == "" || !isValidPrice(licVal1))){
			$('.form-msg.licensing-op-1').html('Please provide price for one year licensing').show();
			return false;
		}
		if(licOpt2 == true &&  (licVal2 == "" || !isValidPrice(licVal2))){
			$('.form-msg.licensing-op-2').html('Please provide price for one time transfer').show();
			return false;
		}
		if(licOpt3 == true &&  (licVal3 == "" || !isValidPrice(licVal3))){
			$('.form-msg.licensing-op-3').html('Please provide price for selective IP transfer').show();
			return false;
		}
		if(licOpt4 == true &&  (licVal4 == "" || !isValidPrice(licVal4))) {
			$('.form-msg.licensing-op-4').html('Please provide price for commission based licensing').show();
			return false;
		}
		if(licOpt4 == true &&  licCommPer4 == "" && licCommAmt4 == "") {
			$('.form-msg.licensing-op-4').html('Please provide commission').show();
			return false;
		}
		
		if(licOpt4 == true){
			if(licCommAmt4 != "" && licCommPer4 != "" ) {
				$('.form-msg.licensing-op-4').html('You can provide either percentage or amount').show();
				return false;
			}
			if(licCommAmt4 != "" && !isValidPrice(licCommAmt4)){
				$('.form-msg.licensing-op-4').html('Not a valid amount').show();
				return false;
			}
			if(licCommPer4 != "" && (!isValidPrice(licCommPer4) || licCommPer4 > 100)) {
				$('.form-msg.licensing-op-4').html('Not a valid precentage').show();
				return false;
			}
		}
		return true;
	}
	
	//fetchProfileDetails();
	fetchCourseDetails();
	fetchSubjectDetails();
	fetchSubjectChapters();
	fetchInstituteProfessors();
	
	//Event handlers
	
	$('.subject-image-thumb').hover(function() {
		$(this).find('.upload').show();
	},function(){
		$(this).find('.upload').hide();
	});
	
	$('#subject-details .edit-button').click(function() {		
		$('#subject-details').hide();
		$('#edit-form').fadeIn('fast');
	});
	
	
	
	$('#subject-details .edit-syllabus').click(function() {		
		//$('#subject-details').hide();
		//$('#edit-form').fadeIn('fast');
	});
	
	$('.cancel-edit-button').click(function() {
		$('.help-block').remove();
		$('#edit-form').hide();
		$('#subject-details').fadeIn('fast');
	});
		
	$('#edit-form .save-button').on('click', function(e) {
		e.preventDefault();
		$('#subject-description, #subject-name').blur();
		if($(document).find('.has-error').length > 0)
			return;
		var req = {};
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		req.courseName = $('#edit-form #subject-name').val();
		req.courseDesc = $('#edit-form #subject-description').val();
		req.action = 'update-subject';
		
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done( function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				alertMsg(res.message);
				$('#edit-form').hide();
				$('#subject-details').fadeIn('fast');
				/*if(subjectImageChanged)
					$('#subject-image-form').submit();
				else*/
					fetchSubjectDetails();
			} else {
				alertMsg(res.msg);
			}
		});
	});
	
	$('#create-subject').on('click', function(e) {
		e.preventDefault();
		if(validateForm() == false)
			return;
		var req = {};
		req.courseId = getUrlParameter('courseId');
		req.subjectName = $('#subject-name').val();
		req.subjectDesc = $('#subject-description').val();
		req.action = 'create-subject';
		
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done( function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				//alertMsg(res.message);
				$('#subject-name').val('');
				$('#subject-description').val('');
				$('#subject-created #subject-name-span').html(req.subjectName);
				$('#subject-created').modal('show');
			} else {
				alertMsg(res.msg);
			}
		});	
	});
	
	$('#subject-created #add-subject').on('click', function(e) {
		$('#subject-created').modal('hide');
	});
	
	$('#apply-licensing button.save').on('click', function(e) {
		e.preventDefault();
		if(validateLicensingForm() == false)
			return;
		var req = {};
		req.courseId = getUrlParameter('courseId');
		var licOpt1 = $('#apply-licensing #lic-opt-1').prop('checked');
		var licOpt2 = $('#apply-licensing #lic-opt-2').prop('checked');
		var licOpt3 = $('#apply-licensing #lic-opt-3').prop('checked');
		var licOpt4 = $('#apply-licensing #lic-opt-4').prop('checked');
		var licVal1 = $('#apply-licensing #lic-opt-1-price').val();
		var licVal2 = $('#apply-licensing #lic-opt-2-price').val();
		var licVal3 = $('#apply-licensing #lic-opt-3-price').val();
		var licVal4 = $('#apply-licensing #lic-opt-4-price').val();
		var licCommAmt4 = $('#apply-licensing #lic-opt-4-comm').val();
		var licCommPer4 = $('#apply-licensing #lic-opt-4-comm-per').val();
		var data = {};
		var selectedLicensing = {};
		if(licOpt1 == true){
			selectedLicensing["1"] = {
				"price" : licVal1,
				"data" : data
			}
		}
		if(licOpt2 == true){
			selectedLicensing["2"] = {
				"price" : licVal2,
				"data" : data
			}
		}
		if(licOpt3 == true){
			selectedLicensing["3"] = {
				"price" : licVal3,
				"data" : data
			}
		}
		if(licOpt4 == true) {
			data = {
				"amount" : licCommAmt4,
				"percent" : licCommPer4
			};
				
			selectedLicensing["4"] = {
				"price" : licVal4,
				"data" : data
			}
		}
		
		req.selectedLicensing = selectedLicensing;
		
		req.action = 'update-course-licensing';
		
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done( function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				alertMsg(res.message);
				$('#apply-licensing').modal('hide');
				
			} else {
				alertMsg(res.msg);
			}
		});
	});
	
	$('#apply-licensing button.cancel').on('click', function(e) {
		$('#apply-licensing').modal('hide');
	});
	
	$('#update-subject-professors').on('click', function(e) {
		e.preventDefault();
		var req = {};
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		req.subjectProfessors = subjectProfessors;
		
		req.action = 'update-subject-professors';
		
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done( function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				alertMsg(res.message);
				$('#subject-professor-modal').modal('hide');
				fetchInstituteProfessors();
			} else {
				alertMsg(res.msg);
			}
		});
	});

	$("#subject-image").change( function () {
		$('.help-block').remove();
		subjectImageChanged = true;
		var _URL = window.URL || window.webkitURL;
		var img = new Image();
		var file = this;
		img.onload = function() {
			if(!(img.width < 280 || img.height < 200))
				readURL(file);
			else {
				$('#subject-image-form input[type=submit]').prop('disabled',true);
				$('#subject-image-form .error').remove();
				$('#subject-image-form').append('<span style="color:red;" class="error">Please select a larger image than 200 x 200.</span>');
			}
		}
		img.src = _URL.createObjectURL(this.files[0]);
	});
	
	function readURL(input) {
		if(!(input.files[0]['type'] == 'image/jpeg' || input.files[0]['type'] == 'image/png' || input.files[0]['type'] == 'image/gif')) {
			$('#subject-image-form input[type=submit]').prop('disabled',true);
			$('#subject-image-form .error').remove();
			$('#subject-image-form').append('<span class="help-block" style="color:red;" class="error">Please select a proper file type.</span>');
			return;
		}else if(input.files[0]['size']>819200) {
			$('#subject-image-form input[type=submit]').prop('disabled', true);
			$('#subject-image-form .error').remove();
			$('#subject-image-form').append('<span style="color:red;" class="error">Files larger than 800kb are not allowed.</span>');
			return;
		}
		else {
			$('#subject-image-form .error').remove();
			$('#subject-image-form input[type=submit]').prop('disabled', false);
		}
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			if (typeof jcrop_api != 'undefined' && jcrop_api != null) {
				jcrop_api.destroy();
				jcrop_api = null;
				var pImage = $('.crop');
				pImage.css('height', 'auto');
				pImage.css('width', 'auto');
				var height = pImage.height();
				var width = pImage.width();
				$('.jcrop').width(width);
				$('.jcrop').height(height);
			}
			reader.onload = function (e) {
				$('#subject-image-preview').attr('src', e.target.result);
				$('#upload-image .crop').Jcrop({
					onSelect: updateCoords,
					bgOpacity:   .4,
					boxWidth: 830,
					minSize: [200, 200],
					setSelect:   [ 0, 0, 200, 200 ],
					aspectRatio: 1
				}, function() {
					jcrop_api = this;
					jcrop_api.setSelect([0, 0, 200, 200]);
				});
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	function updateCoords(c) {
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
		var crop = [c.x, c.y, c.w, c.h];
	}
	
	$('#subject-image-form').attr('action', fileApiEndpoint);
	
	$('#subject-image-form').on('submit', function(e) {
		e.preventDefault();
		c = jcrop_api.tellSelect();
		crop = [c.x, c.y, c.w, c.h];
		image = document.getElementById('subject-image-preview');
		var sources = {
			'subject-image': {
				image: image,
				type: 'image/jpeg',
				crop: crop,
				size: [200, 200],
				quality: 1.0
			}
		}
		//settings for $.ajax function
		var settings = {
			url: fileApiEndpoint,
			data: {'subjectId': getUrlParameter('subjectId')}, //three fields (medium, small, text) to upload
			beforeSend: function()
			{
				//console.log('sending image...');
			},
			complete: function (resp) {
				$('#upload-image').modal('hide');
				res = $.parseJSON(resp.responseText);
				alertMsg(res.message);
				$('#subject-image-form .jcrop-holder').hide();
				$('#subject-image-form input[type="file"]').val('');
				fetchSubjectDetails();
			}
		}
		cropUploadAPI.cropUpload(sources, settings);
	});

	
	$('#upload-syllabus-modal .subjectId').val(getUrlParameter('subjectId'));
    $('#upload-syllabus-modal .upload-file').on('click', function() {
        $('#upload-syllabus-modal .inputFile').click();
    });

    $('#upload-syllabus-modal .inputFile').on('change', function() {
        $('#upload-syllabus-modal .upload-options').hide();
        $('#upload-syllabus-modal .upload-working').show();
		var file = this.files[0];
		if(file.type =='application/pdf'){
			$('#upload-syllabus-modal').find('form').submit();
		}
		else{
			alert("Please Uplaod Pdf files only");
			$('#upload-syllabus-modal .upload-options').show();
       		$('#upload-syllabus-modal .upload-working').hide();
		}
        
    });  

    $('#upload-syllabus-modal').find('form').attr('action', fileApiEndpoint).ajaxForm({
        beforeSend: function(arr, $form, data) {
            console.log('starting');
        },
        uploadProgress: function(event, position, total, percentComplete) {
            //Progress bar
            var progress = $('#upload-syllabus-modal').find('.progress')
            progress.find('.progress-bar').width(percentComplete + '%') //update progressbar percent complete
            progress.find('.sr-only').html(percentComplete + '%') //update progressbar percent complete
        },
        success: function() {
            
        },
        complete: function(resp) {
            res = $.parseJSON(resp.responseText);
            if(res.status == 1)
                location.reload();
            else
                alertMsg('An error occured. Please refresh the page and try again. Error Details: ' + res.message);
        },
        error: function() {
            console.log('Error');
        }
    });

   /* $('#upload-syllabus-modal .delete-file').on('click', function() {
        var req = {};
        var res;
        req.action = 'delete-demo-video';
        req.courseId = getUrlParameter('courseId');
        $.ajax({
            type: 'post',
            url: ApiEndpoint,
            data: JSON.stringify(req)   
        }).done(function(res) {
            res = $.parseJSON(res);
            if(res.status == 1)
                location.reload();
            else
                alert(res.message);
        });
    });	
	*/	
	//even handler to check duplicate subject name
	$('#subject-name').on('blur', function() {
		var req = {};
		var res;
		if($('#subject-name').val().length < 5 || $('#subject-name').val().length > 50) {
			setError($('#subject-name'), 'Please enter a name between 5 to 50 characters');
			return false;
		}
		else if(!($('#subject-name').parents('div:eq(0)').hasClass('has-error'))) {
			unsetError($('#subject-name'));
		}
		req.action = 'check-duplicate-subject';
		req.courseId = getUrlParameter('courseId');
		req.subjectId=getUrlParameter('subjectId');
		req.name = $('#subject-name').val();
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else if(res.available == false) {
				setError($('#subject-name'), 'Please select a different name as you have already created a subject by this name');
			}
			else
				unsetError($('#subject-name'));
		});
	});
	
	$('#subject-description').on('blur', function() {
		unsetError($('#subject-description'));
		if($('#subject-description').val().length > 2000) {
			setError($('#subject-description'), 'Please enter a description less than 2000 characters');
			return false;
		}
	});
	
	function setError(where, what) {
		unsetError(where);
		where.parents('div:eq(0)').addClass('has-error');
		where.parents('div:eq(0)').append('<span class="help-block">'+what+'</span>');
	}
	
	function unsetError(where) {
		if(where.parents('div:eq(0)').hasClass('has-error')) {
			where.parents('div:eq(0)').find('.help-block').remove();
			where.parents('div:eq(0)').removeClass('has-error');
		}
	}	
});

function applyMultiselectForProf(selector) {
	$(selector).multiSelect({
		afterSelect: function(value) {
			//alert("Select value: "+values);
			var index = subjectProfessors.indexOf(value[0]);
			if(index === -1)
				subjectProfessors.push(value[0]);
		},
		afterDeselect: function(value) {
				var index = subjectProfessors.indexOf(value[0]);
				subjectProfessors.splice(index, 1);
		}
	});
}


/**
* Function to fetch overall subject rating
*
* @author Rupesh Pandey
* @date 24/05/2015
*/
function fetchSubjectRating() {
	var req = {};
	var res;
	req.action = 'get-subject-rating';
	req.courseId = getUrlParameter('courseId');
	req.subjectId = getUrlParameter('subjectId');
	$.ajax({
		type: 'post',
		url: ApiEndpoint,
		data: JSON.stringify(req)   
	}).done(function(res) {
		res = $.parseJSON(res);
		if(res.total == 0)
			rating = 0;
		else
			rating = parseFloat(res.rating).toFixed(1);
		$('.ratingStars').rating('rate', rating);
		$('#ratingTotal').text(res.total);
	});
}