var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var instituteId = getUrlParameter('instituteId');
var total_puchased_keys = '';
$(function () {
	$('a.history').addClass('active');
    var key_rate = "";
    var display_current_currency = 'IGRO';
    fetchCoursekeys();
    get_students_of_course();
    function fetchCoursekeys() {
        var req = {};
        var res;
        req.action = "get-course-keys-details";
        if (typeof (instituteId) === "undefined") {
        } else {
            req.instituteId = instituteId;
            $('#btn_Purchase_More').remove();
        }
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            var html = "";
            for (var i = 0; i <data.courses.length; i++)
            {
                var keys = data.courses[i].keys;
                var rate = data.courses[i].key_rate;
                var amount = keys * rate;
                var date = data.courses[i].date;
                var time = data.courses[i].time;
                html += '<tr><td>' + data.courses[i].date + '&nbsp;&nbsp;' + time + ' </td><td>' + keys + '</td><td>' + rate + ' ' + display_currency + '</td><td>' + amount + ' ' + display_currency + '</td></tr>';

            }
            $('#tabl_Totalkeys tbody').html(html);
            if($('#tabl_Totalkeys tbody tr').length==0){
                 $('#tabl_Totalkeys tbody').html('<tr><td>No any purchase done yet </td></tr>');
            }
            key_rate = data.key_rate[0].key_rate;
            
            //key_rate=data.key_rate[0].key_rate;
            $('#keyrate').text(key_rate);
            $('#keyrate').append(display_current_currency);
            var active_key = data.key_consume[0].active_key;
            var total_keys = data.key_consume[0].total_keys;
            var remaining_keys = total_keys - active_key;
            $('#active_key').text(active_key);
            $('#total_keys').text(total_keys);
            $('#remaining_key').text(remaining_keys);
            $('#totalkeyrate').append(display_current_currency);
        });
    }

    function get_students_of_course() {
        var req = {};
        var res;
        req.action = "get_students_of_course";
        if (typeof (instituteId) === "undefined") {
        } else {
            req.instituteId = instituteId;

        }
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            var html = "";
            for (var i = 0; i < data.students.length; i++)
            {
                var total_student = data.students[i].total_student;
                var course_id = data.students[i].course_id;
                var name = data.students[i].name;
                var stand_alone = '0';
                html += '<tr course_id=' + course_id + '><td>' + course_id + ' </td><td>' + name + '</td><td><a class=showcourse data-toggle=modal href=#modal_student>' + total_student + '</a></td><td <a href="#Div2" data-toggle="modal">' + stand_alone + ' </a></td></tr>';
            }
            $('#tbl_course_student_count').append(html);

            $('.showcourse').click(function () {
                $("#tbl_course_details > tbody").html("");
                var course_id = $(this).parents('tr').attr('course_id');
                var req = {};
                var res;
                req.action = "get_course_student_details";
                if (typeof (instituteId) === "undefined") {
                } else {
                    req.instituteId = instituteId;
                    $('#btn_Purchase_More').remove();
                }
                req.course_id = course_id;
                $.ajax({
                    'type': 'post',
                    'url': ApiEndpoint,
                    'data': JSON.stringify(req)
                }).done(function (res) {
                    data = $.parseJSON(res);
                    var html = "";
                    for (var i = 0; i < data.students.length; i++)
                    {
                        var name = data.students[i].name;
                        var contactMobile = data.students[i].contactMobile;
                        var course_key = 'CK00' + data.students[i].key_id;
                        var email = data.students[i].email;
                        var date = data.students[i].date;
                        html += '<tr ><td>' + name + ' </td><td>' + email + '</td><td>' + contactMobile + '</td><td>' + course_key + '</td><td> ' + date + '</td></tr>';
                    }
                    $('#tbl_course_details').append(html);
                });

            });
     });
    }
  $('#totalkeys').keyup(function () {
        var total = $(this).val() * key_rate;
        $('#totalkeyrate').text(total);
        $('#totalkeyrate').append(display_current_currency);

    });
    $('#purchase_key').click(function () {
        var req = {};
        var res;
        var keys = $('#totalkeys').val();
        if (confirm('Are you sure to purchase?')) {
            req.action = "purchase_key";
            req.keys = keys;
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                data = $.parseJSON(res);
                window.location = self.location;
            });

        }
    });
    $('#keys-purchase').click(function () {
        total_puchased_keys = $.trim($('#totalkeys').val());
          $('.help-block').remove();
        if(total_puchased_keys=='' || total_puchased_keys==0){
            $('#totalkeys').after('<span class="help-block text-danger"> Please enter keys</span>');
            return false;
        }
        $('#Purchase_More').modal('hide');

    });
    $("#totalkeys").keypress(function (e) {
        $('.help-block').remove();
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $('#totalkeys').after('<span class="help-block text-danger">Enter Only Numbers</span>');
            //$('.help-block').fadeOut("slow");
            // ("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    $('#keys-purchaseInvitation').click(function () {
        $('.help-block').remove();
        var req = {};
        var res;
        var email = $.trim($('#inviteon_email').val());
        if(email!='' && validateEmail(email)==false && isNaN(email)){
          $('#inviteon_email').after('<span class="help-block text-danger">Please enter Correct email address/ Phone no.</span>')
            return false; 
        }else{
           if(validateMobile(email)&& !(isNaN(email)) ){
                $('#inviteon_email').after('<span class="help-block text-danger">Please enter Correct email address/ Phone no.</span>')
                  return false;
           }
       }
        req.action = "keys-purchase_invitation";
        req.keys = total_puchased_keys;
        req.email = email;
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            alertMsg(data.message);
             $('#inviteon_email').next().append('<span class="help-block text-success">' + data.message + '</span>');
            $('#Purchase_More').modal('hide');
            $('#modal_keys_purchaseInvitation').modal('hide');
            
          // window.location.reload();
        });
    });
    
     function validateEmail(email) {
        var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!emailReg.test(email)) {
            return false;
        } else {
            return true;
        }
    }
    function validateMobile(mobile) {
        regex = /^(\d{10})$/;
        if (regex.test(mobile))
            return false;
        else
            return true;
    }
 
});