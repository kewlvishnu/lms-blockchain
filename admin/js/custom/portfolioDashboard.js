var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var userRole;
$(document).ajaxStart(function () {
    $('#loader_outer').show();
});

$(document).ajaxComplete(function () {
    $('#loader_outer').hide();
});

$(function () {
    var imageRoot = '/';
    var jcrop_api;
    $('#loader_outer').hide();
    var fileApiEndpoint = '../api/files1.php';
    // favicon upload
    $('#fileFavicon').fileupload({
        url: fileApiEndpoint,
        dataType: 'json',
        done: function (e, data) {
            $('#favicon').attr('src', data._response.result.imageName);
            $('#inputFavicon').val(data._response.result.imageName);
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    $('#btnUpdatePortfolio').click(function(){
        var portfolioName = trim($('#portfolioName').val());
        var portfolioDescription = trim($('#portfolioDescription').val());
        var portfolioPic = trim($('#portfolioPic').val());
        var selectStatus = trim($('#selectStatus').val());
        if(!portfolioName) {
            $('#frmPortfolio .help-block').html('<p class="text-danger text-center">Portfolio Name can\'t be left empty</p>');
        } else if(!portfolioDescription) {
            $('#frmPortfolio .help-block').html('<p class="text-danger text-center">Portfolio Description can\'t be left empty</p>');
        } else if(!selectStatus) {
            $('#frmPortfolio .help-block').html('<p class="text-danger text-center">Status can\'t be left empty</p>');
        } else {
            updatePortfolio(portfolioName,portfolioDescription,portfolioPic);
        }
        return false;
    });

    function updatePortfolio(portfolioName,portfolioDescription,portfolioPic) {
        var req = {};
        req.slug = $('#slug').val();
        req.portfolioName = portfolioName;
        req.portfolioDesc = portfolioDescription;
        req.portfolioPic  = portfolioPic;
        if ($('#affiliation').length>0) {
            req.affiliation  = $('#affiliation').val();
        };
        req.inputFavicon  = $('#inputFavicon').val();
        req.inputFacebook = $('#inputFacebook').val();
        req.inputTwitter  = $('#inputTwitter').val();
        req.inputLinkedin = $('#inputLinkedin').val();
        req.selectStatus  = $('#selectStatus').val();
        var res;
        req.action = "update-portfolio";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if(res.status == 1) {
                //$('#frmPortfolio').html('<div class="jumbotron text-center"><h3>'+res.message+'</h3></div>');
                window.location = window.location;
            } else {
                $('#frmPortfolio .help-block').html('<p class="text-danger text-center">'+res.message+'</p>');
            }
            console.log(res);
        });
    }

    // Image upload block
    function updateCoords(c) {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    };
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            if(!(input.files[0]['type'] == 'image/jpeg' || input.files[0]['type'] == 'image/png'|| input.files[0]['type'] == 'image/gif')) {
                $('#frmPortfolioImage .error').html('');
                $('#frmPortfolioImage .error').append('<span style="color:red;" class="error">Please select a proper file type.</span>');
                return;
            }
            else if(input.files[0]['size']>819200) {
                $('#frmPortfolioImage .error').html('');
                $('#frmPortfolioImage .error').append('<span style="color:red;" class="error">Files larger than 800kb are not allowed.</span>');
                return;
            }
            else {
                $('#frmPortfolioImage .error').html('');
                $('#frmPortfolioImage input[type="submit"]').removeClass('hide');
            }

            if (typeof jcrop_api != 'undefined' && jcrop_api != null) {
                jcrop_api.destroy();
                jcrop_api = null;
            } else {
                $('#uploadPortfolioPic').removeClass('hide');
                $('#frmPortfolioImage input[type="submit"]').removeClass('hide');
            }
            reader.onload = function (e) {
                $('#uploadPortfolioPic').attr('src', e.target.result);
                $('#uploadPortfolioPic').Jcrop({
                    minSize: [200, 200],
                    setSelect:   [ 0, 0, 200, 200],
                    aspectRatio: 1,
                    onSelect: updateCoords
                },function(){
                    jcrop_api = this;
                });
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgPortfolio").change(function(){
        readURL(this);
    });

    $('#btnChangeImg').click(function(){
       $('#uploadPortfolioImageModal').modal('show');
    });

    $('#frmPortfolioImage').attr('action', fileApiEndpoint);
    
    $('#frmPortfolioImage').on('submit', function(e) {
        e.preventDefault();
        c = jcrop_api.tellSelect();
        crop = [c.x, c.y, c.w, c.h];
        imageC = document.getElementById('uploadPortfolioPic');
        var sources = {
            'portfolio-image': {
                image: imageC,
                type: 'image/jpeg',
                crop: crop,
                quality: 1.0
            }
        }
        //settings for $.ajax function
        var settings = {
            url: fileApiEndpoint,
            data: {'x': $('#x').val(),'y': $('#y').val(),'w': $('#w').val(),'h': $('#h').val()}, //three fields (medium, small, text) to upload
            beforeSend: function()
            {
                $("#cover-image-form .progress").show();
            },
            complete: function (resp) {
                var data = $.parseJSON(resp.responseText);
                $('#profilePic').attr('src', data.imageName);
                $('#portfolioPic').val(data.imageName);
                $('#frmPortfolioImage .jcrop-holder').hide();
                $('#frmPortfolioImage input[type="file"]').val('');
                $('#frmPortfolioImage input[type="submit"]').addClass('hide');
                $('#uploadPortfolioImageModal').modal('hide');
            }
        }
        cropUploadAPI.cropUpload(sources, settings);
    });
});