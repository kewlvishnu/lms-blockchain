﻿var sortOrder = [];
var examStart = new Date();
var examEnd = new Date('2099/12/31');
var totalQuestions=0;
var mainQuestionsId=[];
var subQuestions=[];// holdng main question  refernece for questions
var questions = [];// for questions arrays
var SubQuesTionsMain=[];// holding subquestions to be send to server
var subQuesSettingR=[];//for subquestions required 
var subQuesSettingT=[];// for subquestion total
var questionSubQues=[];
var counter=0;
var subjectiveExamId=0;
var chapterId=0;
var questions = [];
var index=-1;
$(function() {
	//fetchProfileDetails();
	fetchChapters();
	fetchBreadcrumb();
	fetchCourseDates();
	ckeditorOn('inputQuestion');
	ckeditorOn('inputAnswer');
	ckeditorOn('inputQuestionDescription');
	//fetchCKEditor();
	//ckeditorOn(question);
	//			console.log($('#inputQuestion'));

	function CKupdate(){
		console.log("check");
	    for ( instance in CKEDITOR.instances ){
	        CKEDITOR.instances[instance].updateElement();
	        CKEDITOR.instances[instance].setData('');
	    }
	}

		
	$('#addQuestionsButton').click(function(){
		//$("#questionModal").reset();
		$("#questionModal").modal();
	});
	
	$('input[type=radio][name=questionsOnpage]').change(function() {
        console.log(this);
		if (this.id == 'onequestion') {
           $(".desc").show();
        }
        else{
			$(".desc").hide();
		}
    });
	$(".modal").on("hidden.bs.modal", function(){
		//$(".modal-body").html("");
		//$(this).removeData();	 
	});
	$('.js-question-type').change(function(){
		var questionType = $(this).val();
		if (questionType == "question") {
			$('.js-question-block').addClass('hide');
			$('#questionBlock').removeClass('hide');
		} else {
			$('.js-question-block').addClass('hide');
			$('#questionGroupBlock').removeClass('hide');
		}
	});
	$('#btnAddQuestion').click(function(){
		var req={};
		var questionType = $('.js-question-type:checked').val();
		var questionjson=[];
		var parentid='';
		var marks='';
		if (questionType == "question") {
			var question = $('#inputQuestion').val();
			var answer	 = $('#inputAnswer').val();
			var marks	 = $('#inputMarks').val();
			console.log($('#inputMarks').val());
			if($('#inputMarks').val()=='' || $('#inputMarks').val()==undefined)
			{
				marks=0;
			}
			if (!question || !answer || marks <0) {
				$('#questionBlock .help-block').html('<p class="text-danger">Question and Answer both are compulsory and marks should be greater than 0 !</p>');
			} else {
				var questionEdit = $(this).attr('data-question');
				var parent = $(this).attr('data-parent');
				if (!questionEdit) {
					if (!parent) {
						var qno = questions.length+1;
						questions.push({id:'n'+qno,questionType:questionType,question:question,answer:answer,marks:marks,subQuestions:[]});
						questionjson= ({id:'n'+qno,questionType:questionType,question:question,answer:answer,marks:marks,subQuestions:[]});
						parentid='';
					} 
					else {
						var qno = questions[parent]["subQuestions"].length+1;
						questions[parent]["subQuestions"].push({id:'s'+qno,questionType:questionType,question:question,answer:answer,marks:marks});
						console.log(questions[parent]);
						parentid=questions[parent].id;
						marks=questions[parent].subquestionMrks;
						console.log(marks);
						questionjson={id:'s'+qno,questionType:questionType,question:question,answer:answer,marks:marks};
						//console.log({id:'s'+qno,questionType:questionType,question:question,answer:answer});
					}
				
				//saving question in db
				req.questions=JSON.stringify(questionjson);
				req.parent=parentid;
				req.subjectiveExamId=subjectiveExamId;
				req.marks=marks;
				req.subjectId=getUrlParameter('subjectId');
				req.courseId=getUrlParameter('courseId');
				req.action = 'insert-subjective-questions';
				console.log(req);
				$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
				}).done(function (res) {
					res =  $.parseJSON(res);
					if(res.status == 0)
						alert(res.message);
					else
					{
						//alert("Question paper saved sucessfully. Please make exam Live");
						$('.makelive').prop('disabled', false);
					}
				});
				 



				} else {
					if (!parent) {
						questions[questionEdit].questionType = questionType;
						questions[questionEdit].question	 = question;
						questions[questionEdit].answer		 = answer;
						questions[questionEdit].marks		 = marks;						
						questions[questionEdit].subQuestions = [];
						console.log(questions[questionEdit]);
						questionjson=questions[questionEdit];
						//
					} else {
						questions[parent]["subQuestions"][questionEdit].questionType = questionType;
						questions[parent]["subQuestions"][questionEdit].question	 = question;
						questions[parent]["subQuestions"][questionEdit].answer		 = answer;
						questions[parent]["subQuestions"][questionEdit].marks		 = marks;
						console.log(questions[parent]["subQuestions"][questionEdit]);
						console.log(questions[parent].id);
						questionjson=questions[parent]["subQuestions"][questionEdit];
						parentid=questions[parent].id;
						marks=questions[parent].subquestionMrks;
						
					}
					req.questions=JSON.stringify(questionjson);
					req.parent=parentid;
					req.subjectiveExamId=subjectiveExamId;
					req.marks=marks;
					req.subjectId=getUrlParameter('subjectId');
					req.courseId=getUrlParameter('courseId');
					req.action = 'edit-subjective-questions';
					console.log(req);
					$.ajax({
					'type'  : 'post',
					'url'   : ApiEndpoint,
					'data' 	: JSON.stringify(req)
					}).done(function (res) {
						res =  $.parseJSON(res);
						if(res.status == 0)
							alert(res.message);
						else
						{
							//alert("Question paper saved sucessfully. Please make exam Live");
							$('.makelive').prop('disabled', false);
						}
					});
				}
				$('#questionModal').modal('hide');
				resetQuestionModal();
			}
		} 
		else {
			var question = '';
			if($('#inputQuestionDescription').val()!='' || $('#inputQuestionDescription').val()!=undefined)
			{
				question = $('#inputQuestionDescription').val();
			}
			var noOfQuestions = $('#inputNoOfQuestions').val();		
			var subquestionMrks=$('#subquestionsMrks').val();
			var questionsonPage=1;
			var subquestions=[];
			if(!(index<0))
			{
				subquestions=questions[index].subQuestions;
			}
			if($('#onequestion').prop('checked'))
			{
				questionsonPage=2;
			}else{
				question='';
			}
			if ( !noOfQuestions || noOfQuestions < 0 || subquestionMrks < 0) {
				$('#questionGroupBlock .help-block').html('<p class="text-danger">Question Group Description and No of Questions both are compulsory and  No of Questions and subquestions marks should be greater then 0!</p>');
			} else {
				var questionEdit = $(this).attr('data-question');
				if (!questionEdit) {
					var qno = questions.length+1;
					questions.push({id:'n'+qno,questionType:questionType,question:question,answer:noOfQuestions,subquestionMrks:subquestionMrks,questionsonPage:questionsonPage,subQuestions:[]});
					questionjson=({id:'n'+qno,questionType:questionType,question:question,answer:noOfQuestions,subquestionMrks:subquestionMrks,questionsonPage:questionsonPage,subQuestions:[]});
					console.log(questionjson);
					req.questions=JSON.stringify(questionjson);
					req.parent=parentid;
					req.subjectiveExamId=subjectiveExamId;
					req.subjectId=getUrlParameter('subjectId');
					req.courseId=getUrlParameter('courseId');
					req.action = 'insert-subjective-questions';
					//console.log(req);
					$.ajax({
					'type'  : 'post',
					'url'   : ApiEndpoint,
					'data' 	: JSON.stringify(req)
					}).done(function (res) {
						res =  $.parseJSON(res);
						if(res.status == 0)
							alert(res.message);
						else
						{
							//alert("Question paper saved sucessfully. Please make exam Live");
							$('.makelive').prop('disabled', false);
						}
					});
				} else {
					questions[questionEdit].questionType = questionType;
					questions[questionEdit].question	 = question;
					questions[questionEdit].answer		 = noOfQuestions;
					questions[questionEdit].subquestionMrks	 = subquestionMrks;
					questions[questionEdit].questionsonPage		 = questionsonPage;
					questions[questionEdit].subQuestions = subquestions;
					questionjson=questions[questionEdit];
					
					req.questions=JSON.stringify(questionjson);
					req.parent=parentid;
					req.subjectiveExamId=subjectiveExamId;
					req.marks=marks;
					req.subjectId=getUrlParameter('subjectId');
					req.courseId=getUrlParameter('courseId');
					req.action = 'edit-subjective-questions';
					//console.log(req);
					$.ajax({
					'type'  : 'post',
					'url'   : ApiEndpoint,
					'data' 	: JSON.stringify(req)
					}).done(function (res) {
						res =  $.parseJSON(res);
						if(res.status == 0)
							alert(res.message);
						else
						{
							//alert("Question paper saved sucessfully. Please make exam Live");
							$('.makelive').prop('disabled', false);
						}
					});
				}
				$('#questionModal').modal('hide');
				resetQuestionModal();
				//console.log(JSON.stringify(questions));
				
			}
		}
		
	});

	function resetQuestionModal() {
		console.log("check");
		$('#inputQuestion').val('');
		$('#inputAnswer').val('');
		$('#inputQuestionDescription').val('');
		$('#inputNoOfQuestions').val('');
		$('#questionBlock .help-block').html('');
		$('#questionGroupBlock .help-block').html('');
		$('#optionQuestionGroup').prop('disabled', false);
		$('#optionQuestion').trigger('click');
		$('#btnAddQuestion').attr('data-question','');
		$('#btnAddQuestion').attr('data-parent','');
		CKupdate();
		if (questions.length>0) {
			$('#listQuestions').html(
				'<table class="table table-bordered table-sbj" id="tblQuestions">'+
					'<tr class="sbj-header">'+
						'<th width="5%">No.</th>'+
						'<th width="95%" colspan="2">Questions</th>'+
						'<th class="text-center">Edit</th>'+
						'<th class="text-center">Delete</th>'+
					'</tr>'+
				'</table>'
				);
			$.each(questions, function(k,v){
				if(questions[k].questionType == "question") {
					$('#tblQuestions').append('<tr data-question="'+k+'">'+
												'<td rowspan="3" width="5%">'+(k+1)+'</td>'+
												'<td class="sbj-question" colspan="2" width="95%">'+questions[k].question+'</td>'+
												'<td rowspan="3"><button class="btn btn-warning btn-sm js-question-edit">Edit</button></td>'+
												'<td rowspan="3"><button class="btn btn-danger btn-sm js-question-delete">Delete</button></td>'+
											'</tr>'+
											'<tr data-question="'+k+'">'+
												'<td class="sbj-answer" colspan="2" width="95%">'+questions[k].answer+'</td>'+
											'</tr>'+
												'<tr data-question="'+k+'">'+
												'<td class="sbj-marks" colspan="2" width="95%">'+questions[k].marks+'</td>'+
											'</tr>');
				} else {
					$('#tblQuestions').append('<tr data-question="'+k+'">'+
												'<td>'+(k+1)+'</td>'+
												'<td class="sbj-question-grp" colspan="2">'+questions[k].question+' (Questions:'+questions[k].answer+' ,Marks Of each subquestions : '+questions[k].subquestionMrks+')</td>'+
												'<td><button class="btn btn-warning btn-sm js-question-edit">Edit</button></td>'+
												'<td><button class="btn btn-danger btn-sm js-question-delete">Delete</button></td>'+
											'</tr>'+
											'<tr data-question="'+k+'" class="sbj-sub-header">'+
												'<th width="10%" class="text-right" colspan="2">No.</th><th width="90%">Sub Questions</th>'+
												'<th class="text-center">Edit</th>'+
												'<th class="text-center">Delete</th>'+
											'</tr>');
					var subQuestions = questions[k].subQuestions;
					if (subQuestions.length>0) {
						$.each(subQuestions, function(k1,v1){
							if(subQuestions[k1].questionType == "question") {
								$('#tblQuestions').append('<tr data-question="'+k+'" data-subquestion="'+k1+'">'+
															'<td colspan="2" rowspan="3" class="text-right">'+(k1+1)+'</td>'+
															'<td class="sbj-question">'+subQuestions[k1].question+'</td>'+
															'<td rowspan="3"><button class="btn btn-warning btn-sm js-subquestion-edit">Edit</button></td>'+
															'<td rowspan="3"><button class="btn btn-danger btn-sm js-subquestion-delete">Delete</button></td>'+
														'</tr>'+
														'<tr data-question="'+k+'" data-subquestion="'+k1+'">'+
															'<td class="sbj-answer">'+subQuestions[k1].answer+'</td>'+
														'</tr>'+
														'<tr data-question="'+k+'" data-subquestion="'+k1+'">'+
															'<td class="sbj-marks">'+questions[k].subquestionMrks+'</td>'+
														'</tr>');
							}
						});
					};
					$('#tblQuestions').append('<tr data-question="'+k+'"><td width="10%" colspan="2"></td><td width="90%" colspan="3"><button class="btn btn-primary btn-sm js-subQuestions"><i class="fa fa-plus-circle"></i> Add Sub Question</button></td></tr>');
				}
			});
		} else {
			$('#listQuestions').html('');
		}
	}

	$('#questionModal').on('hidden.bs.modal', function () {
		resetQuestionModal();
	})

	$('#listQuestions').on('click', '.js-subQuestions', function(){
		var question = $(this).closest('tr').attr('data-question');
		$('#optionQuestionGroup').prop('disabled', true);
		$('#btnAddQuestion').attr('data-parent',question);
		$('#questionModal').modal('show');
		$('.chooseQuestionType').hide();
		$('.marks').hide();
	});
	$('#listQuestions').on('click', '.js-question-edit', function(){
		var questionEdit = $(this).closest('tr').attr('data-question');
		index=questionEdit;
		var question = questions[questionEdit].question;
		var answer = questions[questionEdit].answer;
		var marks=   questions[questionEdit].marks;	
		console.log(questions[questionEdit]);
		if(questions[questionEdit].questionType == "question") {
			$('#optionQuestion').trigger('click');
			$('#inputQuestion').val(question);
			CKEDITOR.instances['inputQuestion'].setData(question);
			$('#inputAnswer').val(answer);
			$('#inputMarks').val(marks);
		} else {
			console.log(question);
			var subquestionMrks=questions[questionEdit].subquestionMrks;
			var questionsonPage=questions[questionEdit].questionsonPage;
			$('#optionQuestionGroup').trigger('click');
			$('#inputQuestionDescription').val(question);
			$('#inputNoOfQuestions').val(answer);
			$('#subquestionsMrks').val(subquestionMrks);
			if(questionsonPage==2)
			{
				$('#onequestion').prop('checked');
			}else{
				$('#allquestion').prop('checked');
			}
			
		}
		console.log(questionEdit);
		console.log(questions);
		$('#btnAddQuestion').attr('data-question',questionEdit);
		$('#questionModal').modal('show');
	});
	$('#listQuestions').on('click', '.js-subquestion-edit', function(){
		var questionEdit = $(this).closest('tr').attr('data-question');
		var subQuestionEdit = $(this).closest('tr').attr('data-subquestion');
		var question = questions[questionEdit]["subQuestions"][subQuestionEdit].question;
		var answer = questions[questionEdit]["subQuestions"][subQuestionEdit].answer;
		var marks=  questions[questionEdit]["subQuestions"][subQuestionEdit].marks;	
		if(questions[questionEdit]["subQuestions"][subQuestionEdit].questionType == "question") {
			$('#optionQuestion').trigger('click');
			$('#inputQuestion').val(question);
			$('#inputAnswer').val(answer);
			$('#inputMarks').val(marks);			
		}
		$('#btnAddQuestion').attr('data-question',subQuestionEdit);
		$('#btnAddQuestion').attr('data-parent',questionEdit);
		$('#optionQuestionGroup').prop('disabled', true);
		$('#questionModal').modal('show');
	});
	$('#listQuestions').on('click', '.js-question-delete', function(){
		var questionEdit = $(this).closest('tr').attr('data-question');
		console.log(JSON.stringify(questions[questionEdit]));
		var subQuestionsCheck=questions[questionEdit]["subQuestions"];
		if(subQuestionsCheck.length>0)
		{
			alert("Please delete Subquestions before deleting Main questions");
		}
		else{
			var parentid='';
			var req={};
			req.questions=JSON.stringify(questions[questionEdit]);
			req.parent=parentid;
			req.subjectiveExamId=subjectiveExamId;
			req.subjectId=getUrlParameter('subjectId');
			req.courseId=getUrlParameter('courseId');
			req.action = 'delete-subjective-questions';
			console.log(req);
			$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else
				{
					//alert("Question paper saved sucessfully. Please make exam Live");
					//$('.makelive').prop('disabled', false);
				}
			});
			questions.splice(questionEdit,1);
			resetQuestionModal();
		}		
	});
	$('#listQuestions').on('click', '.js-subquestion-delete', function(){
		var questionEdit = $(this).closest('tr').attr('data-question');
		var subQuestionEdit = $(this).closest('tr').attr('data-subquestion');
		var parentid='';
		var req={};
		req.questions=JSON.stringify(questions[questionEdit]["subQuestions"]);
		req.parent=questions[questionEdit].id;
		req.subjectiveExamId=subjectiveExamId;
		req.subjectId=getUrlParameter('subjectId');
		req.courseId=getUrlParameter('courseId');
		req.action = 'delete-subjective-questions';
		console.log(req);
		$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
			{
				//alert("Question paper saved sucessfully. Please make exam Live");
				//$('.makelive').prop('disabled', false);
			}
		});
		questions[questionEdit]["subQuestions"].splice(subQuestionEdit,1);
		resetQuestionModal();
	});
	$('#btnSaveQuestions').click(function(){
		var req={};
		if (questions.length>0) {
			//console.log(JSON.stringify(questions));
			req.questions=JSON.stringify(questions);
			req.chapterId=chapterId;
			req.subjectiveExamId=subjectiveExamId;
			req.subjectId=getUrlParameter('subjectId');
			req.courseId=getUrlParameter('courseId');
			req.action = 'save-subjective-questions';
			//console.log(subjectiveExamId);
			$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
			}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
			{
				alert("Question paper saved sucessfully. Please make exam Live");
				$('.makelive').prop('disabled', false);
			}
		});
		} else {
			alert("Please add at least 1 question!");
		}
	});
	$('.makelive').click(function(){
		if(questions.length){
			var req={};
			req.subjectiveExamId=subjectiveExamId;
			req.action = 'live-subjective-questions';
			//console.log(subjectiveExamId);
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else
				{	
					alert("Exam is Live now");
					location.reload();
				}
			});
		}else{
			alert("Unable to make exam live.Please Add questions.");
		}	
	});
	
	//
	$('#inputNoOfQuestions1').on('blur', function() {
		console.log('cjeck');
		unsetError($(this));
		var noOfQues=$('#inputNoOfQuestions').val().trim();
		totalQuestions=noOfQues;
		if(noOfQues<1 || !($.isNumeric( noOfQues )) ){
			setError($(this), "Please give a no. of questions greater than 1");
			return false;
		}
		
	});
	//disabling user input in dates
	$('#startDate, #endDate').on('keypress', function() {
		return false;
	});
	
	$('#startDate, #endDate').bind("cut copy paste",function(e) {
		e.preventDefault();
	});
	
	$('#titleName').on('blur', function() {
		unsetError($(this));
		if(min($(this), 3)) {
			if(!max($(this), 50))
				setError($(this), "Please give a shorter exam name. Maximum allowed limit is 50 characters.");
		}
		else
			setError($(this), "Please give a longer exam name. Minimum allowed limit is 3 characters.");
		nameAvailable();
	});

	$('#mnsTest').on('blur', function() {
		unsetError($(this));
		var mnsTest=$('#mnsTest').val().trim();
		if(mnsTest<0 || mnsTest>59 || !($.isNumeric( mnsTest ))){
			setError($(this), "Please give a proper time in between 0 to 59 minutes");
		}
		
	});
	$('#hrsTest').on('blur', function() {
		unsetError($(this));
		var hrsTest=$('#hrsTest').val().trim();
		if(hrsTest<0  || !($.isNumeric( hrsTest ))){
			setError($(this), "Please give a proper time greater than 0 hrs");
		}
		
	});
	
	$('#noOfAttempts').on('blur', function() {
		unsetError($(this));
		var noOfAttempts=$('#noOfAttempts').val().trim();
		if(noOfAttempts<1 || !($.isNumeric( noOfAttempts )) ){
			setError($(this), "Please give a no. of attempts greater than 1");
		}
		
	});
	
	$('#noOfQues').on('blur', function() {
		unsetError($(this));
		var noOfQues=$('#noOfQues').val().trim();
		totalQuestions=noOfQues;
		if(noOfQues<1 || !($.isNumeric( noOfQues )) ){
			setError($(this), "Please give a no. of questions greater than 1");
		}
		
	});
		
	$('#noOfRequiredQues1').on('blur', function() {
		unsetError($(this));
		var noOfRQues=$('#noOfRequiredQues1').val().trim();
		if(  noOfRQues<1 || !($.isNumeric( noOfRQues )) ){
			setError($(this), "Please give a no. of required questions greater than 1 and less than total questions");
		}
		console.log(noOfRQues);
		console.log(totalQuestions);
	});
	
	$('.save-button').on('click', function(){
		var error=0;
		var req = {};
		var title=$('#titleName').val().trim();
		var startDate = new Date($('#startDate').val());
        //var endDate= new Date($('#endDate').val());
		if((title.length> 3 && title.length<50)){
			unsetError($('#titleName'));
			if($('#startDate').val()!=''){
				unsetError($('#startDate'));
				var startDate = new Date($('#startDate').val());
				if($('#endDate').val()!='')	{
					unsetError($('#endDate'));
					var enddate=new Date($('#endDate').val());
					if(startDate.getTime()>enddate.getTime())
					{	 error=1;
						setError($('#endDate'), "End date should not be less than start date.");
					}
				}
				if($('#hrsTest').val().trim()!=''){
					unsetError($('#hrsTest'));
					if($('#mnsTest').val().trim()!=''){
						unsetError($('#mnsTest'));
						if($('#noOfAttempts').val().trim()!=''){
							unsetError($('#noOfAttempts'));
								if($('#switchyes').prop('checked') || $('#switchno').prop('checked')){
									unsetError($('#errormsg'));
									if($('#switchtype').is(":checked") || $('#switchdoc').is(":checked")){
										unsetError($('#errormsg1'));
										// all main processing here
										if( error==0){
										totalhrs=0;
										req.title=	title;
										req.startDate=startDate.getTime();
										if($('#endDate').val()!='')	{
											var endDate=new Date($('#endDate').val());
											req.endDate=endDate.getTime();	
										}
										else								
											req.endDate='';
										var totalhrs=$('#hrsTest').val().trim()*60;
										var mins=$('#mnsTest').val().trim();
										req.totalTime=parseInt(totalhrs)+parseInt(mins);
										req.totalAttempts=$('#noOfAttempts').val().trim();
										//req.totalQuestions=$('#noOfQues').val().trim();
										//req.totalRequiredQuestions=$('#noOfRequiredQues').val().trim();
										//noOfRequiredQues
										if($('#switchyes').prop('checked'))
										{
											req.shuffle='yes';	
										}else{
											req.shuffle='no';
										}
										if($('#switchtype').is(":checked") && $('#switchdoc').is(":checked")){
											req.submission=3;
										}
										else if($('#switchdoc').is(":checked")){
											req.submission=2;
										}else{
											req.submission=1;
										}
										req.chapterId=$('#chapterSelect').val();
										chapterId=$('#chapterSelect').val();
										req.subjectId=getUrlParameter('subjectId');
										req.courseId=getUrlParameter('courseId');
										req.action = 'add-subejctive-exam';
										//console.log(req);
										$.ajax({
											'type'  : 'post',
											'url'   : ApiEndpoint,
											'data' 	: JSON.stringify(req)
										}).done(function (res) {
											res =  $.parseJSON(res);
											//console.log(res);
											if(res.status == 0)
												alert(res.data.message);
											else
											{
												//fetchCKEditor(res);
												
												subjectiveExamId=res.data.examId;
												$('.save-button').hide();
												$('#sidepannelBody').hide();
												window.location.href = 'edit-subjectiveassignment.php?examId='+subjectiveExamId+'&courseId='+getUrlParameter('courseId')+'&subjectId='+getUrlParameter('subjectId');
												//$('#titleName').val(req.title);
												//console.log(subjectiveExamId);
												//$('.subjectiveExamopt').show();
											}
										});
										}
									}else{
										 error=1;
										setError($('#errormsg1'), "Please check atleast one choice");
									}
								}else{
									 error=1;
									setError($('#errormsg'), "Please check one choice.");
								}
							
						}else{
						 	error=1;
							setError($('#noOfAttempts'), "Please give a no. of attempts greater than 1.");
						}
					}
					else{
						error=1;
						setError($('#mnsTest'), "Please give a proper time in between 0 to 59 minutes.");	
					}			
				}else{
					 error=1;
					setError($('#hrsTest'), "Please give a proper time greater than 0 hrs.");				}
			}else{
				 error=1;
				setError($('#startDate'), "Please specify start date.");
			}
		}else{
			 error=1;
			setError($('#titleName'), "Please give proper exam name.");
		}
		
	});
	//data
	function fetchCKEditor(data) {
		//save-cancel-div
		$('.save-cancel-div').show();
		var html='';
		subjectiveExamId=data.data.examId;
		totalQuestions=data.data.totalQuestions;
		console.log(subjectiveExamId);
		//data.data.totalQuestions
		for(var i = 0; i < data.data.totalQuestions; i++) {
			html = '<form role="form">'
						+'<div class="col-lg-12 padBot10">'
				  		 	+'<label for="question" id="qq">Question</label>'
				  			 +'<textarea placeholder="Please enter the question text here" rows="5" class="form-control t-text-area questions" id="question'+i+'"></textarea>'
			    		+'</div>'
			    		+'<div class="col-lg-8 col-md-8 add-div add-subQuesButton" id="aquestion'+i+'">'
				  			 +'<button type="button" data-id="question'+i+'" class="btn btn-primary pull-right add-option"><i class="fa fa-plus-circle"></i> Add Sub Question Group</button>'
			    		+'</div>'
						+'<div  class="col-lg-12 col-md-12 dispNone subQuesAdd" id="squestion'+i+'" >'
							+'<div class="form-group row padBot10"id="sTquestion'+i+'">'
								+'<div class="col-md-3 col-sm-3">'
										+'<label  for="courseid">Total SubGroup Questions</label>'
								+'</div>'
								+'<div class="col-md-3 col-sm-3" id="subGRPtotalC">'
				  						 +'<input  type="text" placeholder=" Total Questions" id="subGRPtotal" class="form-control">'
								+'</div>'
								+'<div class="col-md-3 col-sm-3">'
										+'<label  for="courseid">Total SubGroup Questions</label>'
				 				+'</div>'
				 	 			+'<div class="col-md-3 col-sm-3" id="subGRPRequiredC">'
				  						 +'<input  type="text" placeholder="Required Questions" id="subGRPRequired" class="form-control">'
								+'</div>'
								+'<div class="col-lg-8 col-md-8" id="bquestion'+i+'" >'
				  						 +'<button type="button" data-id="question'+i+'" class="btn btn-primary pull-right add-subques"><i class="fa fa-plus-circle"></i> Save Sub Question Settings</button>'
			    				+'</div>'
						    +'</div>'
						+'</div>'
		 		+'</form>';
			$('.subjectiveQues').append(html);
			var question='question'+i;
			ckeditorOn(question);
			
			
			}
		$('.add-option').on('click', function(){
		//which = $(this).parents('.question').attr("id");
		which=$(this).parent().attr('id');
		$('#'+which).hide();
		$('#s'+which.substring(1)).show();
	});
		$('.add-subques').on('click', function(){
		which=$(this).parent().attr('id');
		var sibelements=($(this).parent().siblings("[id]"));
		for(var i = sibelements.length; i--;) {
			var total = $(sibelements[i]).children('#subGRPtotal').val();
			var required = $(sibelements[i]).children('#subGRPRequired').val();
			if(  parseInt(required) > parseInt(total)  || required<1 || !($.isNumeric( required )) ){
				setError($(sibelements[i]).children('#subGRPRequired'), "Please give a no. of required questions greater than 1 and less than total questions");
			}
			if(total<1 || !($.isNumeric( total )) ){
				setError($(sibelements[i]).children('#subGRPtotal'), "Please give a no. of questions greater than 1");
			}
		
			if(total != undefined){
					subQuesSettingT[counter]=total;
			}
			if(required != undefined){
					subQuesSettingR[counter]=required;
			}
		}
		questionSubQues[counter]=(which.substring(1));
		counter++;
		addChildQuestion(total, which);
	});
	}
	
	function ckeditorOn(element) {
		CKEDITOR.inline( element, {
						toolbar: [
								{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ]},
								'/',
								{ name: 'insert', items: [ 'Image', 'Mathjax' ]},
								{ name: 'styles', items: [ 'FontSize' ] },
								{ name: 'colors', items: [ 'TextColor' ] }],
						right: 0,
						filebrowserBrowseUrl: '../api/browse.php',
						filebrowserUploadUrl: '../api/uploader.php',
						filebrowserWindowWidth: '640',
						filebrowserWindowHeight: '480',
						extraPlugins: 'confighelper'
					});
		//for inline ckeditor
		//CKEDITOR.instances["question"].setData('<div contenteditable="true">asxkgsydfb</div>');
		CKEDITOR.instances[element].on("instanceReady", function() {
			//set keyup event
			this.document.on("keyup", function() {
				$("#" + element).val(CKEDITOR.instances[element].getData());
			});
			
			//and paste event
			this.document.on("paste", function() {
				$("#" + element).val(CKEDITOR.instances[element].getData());
			});
			CKEDITOR.instances[element].on('blur', function(e) {
				$("#" + element).val(CKEDITOR.instances[element].getData());
				$("#" + element).blur();
				if($("#question").val() == '') {
					if($('#hiddenQuestionType').val() == 4 || $('#hiddenQuestionType').val() == 5) {
						$('div[aria-describedby="cke_43"]').html('Please enter the passage here. A Passage could be a few paragraphs or a problem statement.');
					}
				}
			});
		});
		CKEDITOR.on( 'dialogDefinition', function( evt ) {
			var dialog = evt.data;
			if ( dialog.name == 'image' ) {
				evt.data.definition.width = "90%";
			}
		});
	}
	
	//save questions feild
	$('.save-questions-button').on('click', function(){
		var error=0;
		var req = {};
		for(var i=0;i<totalQuestions;i++)
		{	
			var quesid='question'+i;
			var ques = document.getElementById(quesid).value;
			if(ques.trim() == null || ques.trim()=='' || ques.length<1)
			{
				
			}else{
				
				questions[i]=ques;
			}	
			console.log(questions);
			// for subarray
			var index=($.inArray( quesid, questionSubQues ));	 
			if(index<0)
			{
				//console.log(quesid+'chhhchch'+questionSubQues);
			}
			else{
			 var totalT=subQuesSettingT[index];
			 var subQuesArray=[];
			 if(totalT>0){
			 	for(var j=0;j<totalT;j++)
			 	{
				 	//console.log('yes');
					var subQues1='QsubQuestion'+j+'-'+i;
					var SubQuestions = document.getElementById(subQues1).value;
					//console.log(SubQuestions);
					subQuesArray[j]=SubQuestions;
				
			 	}
			
			 }else{
			 	
			 }
			 	 SubQuesTionsMain[i]=subQuesArray;
				 console.log(quesid+'fuhr'+questionSubQues);
			}
			

		}
		req.chapterId=$('#chapterSelect').val();
		req.questions=questions;
		req.SubQuesTionsMain=SubQuesTionsMain;
		req.questionSubQues=questionSubQues;
		req.subQuesSettingR=subQuesSettingR;
		req.subQuesSettingT=subQuesSettingT;
		req.subjectId=getUrlParameter('subjectId');
		req.courseId=getUrlParameter('courseId');
		req.action = 'save-subjective-questions';
		req.subjectiveExamId=subjectiveExamId;
		console.log(req);
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			console.log(res.totalQuestions);
			if(res.status == 0)
				alert(res.message);
			else
			{
				alert("Question paper saved sucessfully. Please make exam Live");
				
				//window.location.href = 'edit-subject.php?courseId='+getUrlParameter('courseId')+'&subjectId='+getUrlParameter('subjectId');
				//fetchCKEditor(res);
			}
		});
	
	});
	// this to add show number of subquestion required and total
	$('.add-option').on('click', function(){
		//which = $(this).parents('.question').attr("id");
		which=$(this).parent().attr('id');
		$('#'+which).hide();
		$('#s'+which.substring(1)).show();
	});
	$('.add-subques').on('click', function(){
		which=$(this).parent().attr('id');
		var sibelements=($(this).parent().siblings("[id]"));
		for(var i = sibelements.length; i--;) {
			var total = $(sibelements[i]).children('#subGRPtotal').val();
			var required = $(sibelements[i]).children('#subGRPRequired').val();
			if(total != undefined){
					subQuesSettingT[counter]=total;
			}
			if(required != undefined){
					subQuesSettingR[counter]=required;
			}
		}
		questionSubQues[counter]=(which.substring(1));
		counter++;
		addChildQuestion(total, which);
	});
	
	function addChildQuestion(amount,which){
		var html = '';
		var flag;
		var prentid=which.parent;
		var idNo=parseInt(which.substring(9));
		//console.log(which);
		//console.log(idNo+"idNoidNo");
		$('#sT'+which.substring(1)).hide();
		for(var i=0; i<amount; i++) {
			html = '<div class="col-lg-12 padBot10 " id="subQuestion'+i+'">'
						+'<label for="question" id="qq"> Sub Question <span id="subques">'+(i+1) +'</span></label>'
						+'<textarea placeholder="Please enter the question text here" rows="5" class="form-control t-text-area" id="QsubQuestion'+i+'-'+idNo+'"></textarea>'
			       +'</div>';
			//$(html).insertBefore(which);
			$('#s'+which.substring(1)).append(html);
			var subQues='QsubQuestion'+i+'-'+idNo;
			ckeditorOn(subQues);
		}
		
	}
	
	function fetchBreadcrumb() {
		var req = {};
		var res;
		req.action = 'get-breadcrumb-for-add';
		req.subjectId = getUrlParameter('subjectId');
		req.courseId = getUrlParameter('courseId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
				fillBreadcrumb(res);
		});
	}
	
	function fillBreadcrumb(data) {
		$('ul.breadcrumb li:eq(0)').find('a').attr('href', 'edit-course.php?courseId=' + data.courseId).text(data.courseName);
		$('ul.breadcrumb li:eq(1)').find('a').attr('href', 'edit-subject.php?courseId=' + data.courseId + '&subjectId=' + data.subjectId).text(data.subjectName);
	}
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src',data.profileDetails.profilePic);
		}
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}

	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}

	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}

	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}

	function fetchChapters() {
		var req = {};
		req.action = "get-chapters-for-exam";
		req.subjectId = getUrlParameter('subjectId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			console.log(res);
			fillChaptersSelect(res);
		});
	}

	function fillChaptersSelect(data) {
		if(data.status == 0) {
			alert(data.message);
			return;
		}
		var opts = '';
		for(i=0; i<data.chapters.length; i++) {
			opts += '<option value="'+data.chapters[i]['id']+'">'+data.chapters[i]['name']+'</option>';
		}
		opts += '<option value="-1">Independent</option>';
		$('#chapterSelect').append(opts);
		var xx = getUrlParameter('chapterId');
		if(xx)
			$('#chapterSelect').val(xx);
	}

	function fetchCourseDates() {
		var req = {};
		var res;
		req.action = 'get-course-date';
		req.courseId = getUrlParameter('courseId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				$('#courseStartDate').val(res.dates.liveDate);
				$('#courseEndDate').val(res.dates.endDate);
				
				//adding handlers for datetimepicker of start and end date
				var now = new Date();
				var start = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
				var courseStart = new Date(parseInt(res.dates.liveDate));
				if(start.valueOf() < courseStart.valueOf())
					examStart = new Date(courseStart.getTime());
				else
					examStart = new Date(start.getTime());
				
				var courseEnd = new Date(parseInt(res.dates.endDate));
				var end = new Date('2099/12/31');
				if(end.valueOf() < courseEnd.valueOf())
					examEnd = new Date(courseEnd.getTime());
				else
					examEnd = new Date(end.getTime());
				
				
				$('#startDate').datetimepicker({
					format: 'd F Y H:i',
					minDate: examStart,
					step: 30,
					onSelectTime: function(date) {
						unsetError($('#startDate'));
						if (date.valueOf() > examEnd.valueOf()) {
							setError($('#startDate'), 'The start date can not be greater than the end date.');
						} else {
							examStart = date;
						}
					}
				});
				$('#endDate').datetimepicker({
					format: 'd F Y H:i',
					minDate: examStart,
					maxDate: examEnd,
					step: 30,
					onSelectTime: function(date) {
						unsetError($('#endDate'));
						if (date.valueOf() < examStart.valueOf()) {
							setError($('#endDate'), 'The end date can not be less than the start date.');
						} else {
							examEnd = date;
						}
					}
				});
			}
		});
	}

	function min(what, length) {
		if(what.val().length < length)
			return false;
		else
			return true;
	}

	function max(what, length) {
		if(what.val().length > length)
			return false;
		else
			return true;
	}

	function setError(where, what) {
		unsetError(where);
		where.parent().addClass('has-error');
		where.parent().append('<span class="help-block">'+what+'</span>');
	}

	function unsetError(where) {
		if(where.parent().hasClass('has-error')) {
			where.parent().find('.help-block').remove();
			where.parent().removeClass('has-error');
		}
	}
//  function unsetErrorBefore(where) {
//		if(where.parent().hasClass('has-error')) {
//			where.parent().parent().find('.help-block').remove();
//			where.parent().removeClass('has-error');
//		}
//	}
	
	function nameAvailable() {
		if($('#titleName').val().length != '' ) {
			var req = {};
			var res;
			req.action = 'check-name-for-subjectiveexam';
			req.name = $('#titleName').val();
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				console.log(res);
				if(res.status == 0)
					alert(res.message);
				else {
					unsetError($('#titleName'));
					if(!res.available)
						setError($('#titleName'),'Please select a different name as you have already used this name.');
				}
			});
		}
	}
	$("#noOfAttempts").keypress(function (e) {
		$('.help-block').remove();
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			$('#noOfAttempts').after('<span class="help-block text-danger">Enter Only Numbers</span>');
			//$('.help-block').fadeOut("slow");
			// ("#errmsg").html("Digits Only").show().fadeOut("slow");
			return false;
		}
	});
});