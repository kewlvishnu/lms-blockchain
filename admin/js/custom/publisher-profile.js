/*
	Author: Fraaz Hashmi.
	Dated: 6/20/2014
	
	
*/
$(function () {
	var imageRoot = '/';
	var ApiEndpoint = '../api/index.php';
	var fileApiEndpoint = '../api/files2.php';
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		req.userRole = "publisher";
		
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			try {
				res =  $.parseJSON(res);
				fillProfileDetails(res);
				console.log(res);
			} catch(e) {
				alertMsg(e);
			}
		});
	}
	
	function fillProfileDetails(data) {
		var imageRoot = 'user-data/images/';
		$('#username').text(data.loginDetails.username);
		var publisherName = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#publisher-name').text(publisherName);
		if(data.profileDetails.coverPic != "")
			$('img#cover-pic').attr('src', data.profileDetails.coverPic);
		if(data.profileDetails.profilePic != "")
			$('img#profile-pic').attr('src', data.profileDetails.profilePic);
		if(data.profileDetails.description != "")
			$('#publisher-bio p.text').text(data.profileDetails.description);
		
		$('span#publisher-name').text(publisherName);
		$('span#owner-name').text(publisherName);
		$('span#contact-person').text(publisherName);
		
		var year = data.profileDetails.foundingYear;
		year = year != 0 ? year : '';
		$('span#founded-in').text(year);
		$('span#contact-num').text(formatContactNumber(data.userDetails));
		$('span#primary-email').text(data.loginDetails.email);
		$('span#address-country').text(data.userDetails.addressCountry);
		$('span#address-details').text(formatAddress(data.userDetails));
		
		$('#user-institute-types').html(formatUserInstituteTypes(data.userInstituteTypes));
	}
	
	function formatAddress(user) {
		var address = '';
		if(user.addressStreet != '')
			address += user.addressStreet + ', ';
		if(user.addressCity != '')
			address += user.addressCity + ', ';
		if(user.addressState != '')
			address += user.addressState + ', ';
		if(user.addressPin != '')
			address += user.addressPin + ', '
		return address.replace(/,\s$/, '.');
	}
	
	function formatContactNumber(user) {
		var num1= "", num2 = "";
		if(user.contactMobilePrefix != '' && user.contactMobile != '') {
			num1 = user.contactMobilePrefix + '-' + user.contactMobile;
		}
		
		if(user.contactLandlinePrefix != '' && user.contactLandline != '') {
			num2 = user.contactLandlinePrefix + '-' + user.contactLandline;
		}
		
		return (num1 + ', ' + num2).replace(/,\s$/,'').replace(/^,\s/,'');
	}
	
	function formatUserInstituteTypes(insTypes) {
		var arrangedIns = {};
		$.each(insTypes, function(i,ins) {
			if(ins.parentId == 0 ) {
				arrangedIns[ins.id] = {
					'data': ins.data,
					'name': ins.name,
					'children': {}
				}
			}else {
				if(typeof arrangedIns[ins.parentId] == undefined) {
					arrangedIns[ins.parentId] = {
						'children' : {},
					};
				}
				
				arrangedIns[ins.parentId]['children'][ins.id] = {
					'data': ins.data,
					'name': ins.name,
					'children': {}
				}
			}
		});
		console.log(arrangedIns);
		var html = "";
		var data;
		$.each(arrangedIns, function(i,ins) {
			html += '<div class="panel-body bio-graph-info"><h1>' 
					+ ins.name + '</h1>';
			if(ins.data != '{}'){
				data = $.parseJSON(ins.data);
				html += '<div class="row">';
				$.each(data, function(k,v) {
					html += '<div class="col-lg-6 three-row">' + prepareInsDataKey(k) + v + '</div>';
				});
				html += '</div>';
			}
			$.each(arrangedIns[i].children, function(i,subCat) {
				html += '<div class="panel-body bio-graph-info"><h1>' 
					+ ins.name + '</h1>';
				if(ins.data != '{}') {
					data = $.parseJSON(ins.data);
					html += '<div class="row">';
					$.each(data, function(k,v) {
						html += '<div class="col-lg-6 three-row">' + prepareInsDataKey(k) + ': ' + v + '</div>';
					});
					html += '</div>';
				}
			});
			html += '</div>';
		});
		return html;
	}
	
	function prepareInsDataKey(k){
		if(k == 'other')
			return '';
		else
			return k + ': ';
	}
	
	fetchProfileDetails();
	
	$('.cover-photo').hover(function() {
		$(this).find('.upload-cover-image').show();
	},function(){
		$(this).find('.upload-cover-image').hide();
	});
	
	$("#cover-image-form #cover-image-uploader").change(function() {
        $('#cover-image-form input[type="submit"]').show();
		console.log(this);
        readURL1(this);
    });
	
	var options1 = { 
		beforeSend: function()
		{
			$("#cover-image-form #progress").show();
		},
		success: function() 
		{
			
		},
		complete: function(resp)
		{
			$("#cover-image-form .progress").hide();
			$("#cover-image-form .msg").html("<font color='green'>"+resp.responseText+"</font>");
			fetchProfileDetails();
		},
		error: function()
		{
			$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
	 
		}
	};
	
	$('#cover-image-form').attr('action', fileApiEndpoint);
	
	$('#cover-image-form').ajaxForm(options1);
	
	
	$('.profile-photo').hover(function() {
		$(this).find('.upload-profile-image').show();
	},function(){
		$(this).find('.upload-profile-image').hide();
	});
	
	$("#profile-image-form #profile-image-uploader").change(function() {
        $('#profile-image-form input[type="submit"]').show();
		console.log(this);
        readURL2(this);
    });
	
	var options2 = { 
		beforeSend: function()
		{
			$("#profile-image-form #progress").show();
		},
		success: function() 
		{
			
		},
		complete: function(resp)
		{
			$("#profile-image-form .progress").hide();
			$("#profile-image-form .msg").html("<font color='green'>"+resp.responseText+"</font>");
			fetchProfileDetails();
		},
		error: function()
		{
			$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
	 
		}
	};
	
	$('#profile-image-form').attr('action', fileApiEndpoint);
	
	$('#profile-image-form').ajaxForm(options2);
});

	function readURL1(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			
			reader.onload = function (e) {
				$('#cover-image-form #uploaded-image').attr('src', e.target.result).show();
				$('#cover-image-form .crop').Jcrop({
				onSelect: updateCoords1,
					bgOpacity:   .4,
					setSelect:   [ 100, 100, 50, 50 ],
				});
			}
			reader.readAsDataURL(input.files[0]);
		}
	}

	function updateCoords1(c){
		console.log(c);
		$('#x1').val(c.x);
		$('#y1').val(c.y);
		$('#w1').val(c.w);
		$('#h1').val(c.h);
	}

	function readURL2(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			
			reader.onload = function (e) {
				$('#profile-image-form #uploaded-image').attr('src', e.target.result).show();
				$('#profile-image-form .crop').Jcrop({
				onSelect: updateCoords2,
					bgOpacity:   .4,
					setSelect:   [ 100, 100, 50, 50 ],
					aspectRatio: 1
				});
			}
			reader.readAsDataURL(input.files[0]);
		}
	}

	function updateCoords2(c){
		console.log(c);
		$('#x2').val(c.x);
		$('#y2').val(c.y);
		$('#w2').val(c.w);
		$('#h2').val(c.h);
	}