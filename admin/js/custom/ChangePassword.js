
var ApiEndpoint = '../api/index.php';
var email;
var username;

$(function () {
    var imageRoot = '/';
    $('#loader_outer').hide();
    var fileApiEndpoint = '../api/files1.php';

    $('#changePwd').on('click', function () {
        var oldPwd = $.trim($('#oldPwd').val());
        var newPwd = $.trim($('#newPwd').val());
        var repeatPwd = $.trim($('#repeatPwd').val());
        var valiadte = true
        $('.help-block').remove();
        $('.has-error').removeClass('has-error');
        if (newPwd == '') {
            $('#newPwd').parent().addClass('has-error').append('<span class="help-block">Password can not be empty</span>');
            valiadte = false;

        }
        if (oldPwd == '') {
            $('#oldPwd').parent().addClass('has-error').append('<span class="help-block">Password can not be empty</span>');
            valiadte = false;

        }

        if (repeatPwd == '') {
            $('#repeatPwd').parent().addClass('has-error').append('<span class="help-block">Password can not be empty</span>');
            valiadte = false;

        }
        if (newPwd.length <6) {
            $('#repeatPwd').parent().addClass('has-error').append('<span class="help-block">Password can not be less than 6 characters </span>');
            valiadte = false;

        }
        if (newPwd != repeatPwd && (newPwd != '' && repeatPwd != '')) {
            $('#repeatPwd').parent().addClass('has-error').append('<span class="help-block">Your New password and retype password is not matched</span>');
            valiadte = false;

        }
        if (newPwd == email) {
            $('#repeatPwd').parent().addClass('has-error').append('<span class="help-block">Your New password can not be your email address.</span>');
            valiadte = false;

        }
        if (newPwd == username) {
            $('#repeatPwd').parent().addClass('has-error').append('<span class="help-block">Your New password can not be your username.</span>');
            valiadte = false;
        }
        if (valiadte == false) {
            return false;
        }

        var req = {};
        var res;
        req.oldPwd = oldPwd;
        req.newPwd = newPwd;
        req.action = 'changePassword';

        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            if (data.status == 1) {
                $('#changePwd').parent().addClass('has-success').append('<span class="help-block">' + data.message + '</span>');
                // window.location = 'profile.php';
            }
            else {
                $('#oldPwd').parent().addClass('has-error').append('<span class="help-block">' + data.message + '</span>');

            }
        });


    });
    loadCurrencyDetails();
    //getemailUsername();

    function loadCurrencyDetails() {
        var req = {};
        var res;
        req.action = 'LoadCurrencySetting';
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            if (data.status == 1) {
                if (data.currency[0].currency == 2) {
                    $('#btn_dollar').addClass('btn-white');
                    $('#btn_rupee').addClass('btn-info');
                } else {
                    $('#btn_rupee').addClass('btn-white');
                    $('#btn_dollar').addClass('btn-info');
                }
            }
            else {
                // alert(data.message);
            }

            $('.changeCurrency').on('click', function () {
                var currency = $(this).attr('currency');
                var btnId = '#' + $(this).attr('id');
                var otherbtn = '#' + $(btnId).siblings('.btn').attr('id');
                var req = {};
                var res;
                if ($(this).hasClass('btn-white')) {
                    req.action = 'SetCurrencySetting';
                    req.currency = currency;
                    $.ajax({
                        'type': 'post',
                        'url': ApiEndpoint,
                        'data': JSON.stringify(req)
                    }).done(function (res) {
                        data = $.parseJSON(res);
                        if (data.status == 1) {
                            $(btnId).removeClass('btn-white').addClass('btn-info');
                            $(otherbtn).addClass('btn-white').removeClass('btn-info');

                        } else {

                        }

                    });
                }

            });
        });

    }
    function getemailUsername() {
        var req = {};
        var res;
        req.action = 'getemailUsername';
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            email = data.email[0].email;
            username = data.email[0].username;
        });
    }


});
