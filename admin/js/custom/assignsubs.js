var studentdArray={}; 
var subjectIds={};
var alreadyAssigned=[];
var deleted=[];
var assigndStudents=[];
$(function() {

	fetchCourseSubjectStudent();
	//function to get the students and chapters for the subject
	function fetchCourseSubjectStudent() {
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action ="get-student-subject-details";
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			console.log(res);
			if(res.status == 0)
				alert(res.message);
			else {
				fillStudentsSubjects(res);
			}
		});
	}
	function fillStudentsSubjects(data) {
		console.log(data);
		var headHtml="";
		var bodyHtml="";
		headHtml   = "<tr>"
					+"<td>" + 'Students' + "</td>";
		 $.each(data.Subjects, function (i, Subject) {
		 	subjectIds[i]=Subject.id;
			headHtml += "<td>" + Subject.name + "</td>";	
		
		 });
		 headHtml   += "</tr>"
		 $('#students-subject thead').html('').html(headHtml);	
		 //for setting rows
		 $.each(data.students, function (i, student) {
		 	studentdArray[i]=student.userId ;
			bodyHtml += "<tr data-cid='" + student.userId + "'" + "" + ">"
						+"<td>" + student.name + "</td>";
						$.each(data.Subjects, function (i, Subject) {
						bodyHtml +="<td><input  type='checkbox'   id="+student.userId+'_'+Subject.id+"></td>";
						});
					
			bodyHtml +="</tr>";	
		
		});
		$('#students-subject tbody').html('').html(bodyHtml);	
		$("#students-subject").css("width","100%")  
		$('#students-subject').dataTable({
											"aaSorting": [[ 4, "desc" ]]
										});	
		$.each(data.Student_enrolled, function (i, student_enroll) {
				var index=jQuery.inArray(student_enroll.userId, assigndStudents )
				if(index<0){
					
					assigndStudents.push(student_enroll.userId);
				}
				$('#'+student_enroll.userId+'_'+student_enroll.subjectId).prop('checked', true);
				alreadyAssigned.push(student_enroll.userId+'_'+student_enroll.subjectId);
		  });
		$.each(data.students, function (i, students) {
				var index=jQuery.inArray( students.userId, assigndStudents);
				if(index<0){
					$.each(data.Subjects, function (i, Subjects){
						$('#'+students.userId+'_'+Subjects.id).prop('checked', true);
						alreadyAssigned.push(students.userId+'_'+Subjects.id);
					});
					//console.log(students.userId)
				}else{
						}
				
				//$('#'+student_enroll.userId+'_'+student_enroll.subjectId).prop('checked', true);
				//alreadyAssigned.push(student_enroll.userId+'_'+student_enroll.subjectId);
		  });
	
	}
	//
	$('#save-student-subject').on('click', function() {
		alreadyChecked=$.unique(alreadyAssigned.sort());
		var arrayStudent={};
		var arraySubjectId={};
		var req = {};
		var res;
		
		$('#students-subject').find('input[type="checkbox"]:checked').each(function (i, row) {
			//this is the current checkbox
			var $actualrow = $(row);
			$checkbox = $actualrow.find('input:checked').context.id;
			var index=jQuery.inArray($checkbox, alreadyChecked )
			if(index!=-1)
			{
				//alreadyAssigned[index]='';
				 alreadyChecked.splice(index, 1);
			}
			var splitvalue= $checkbox.split('_');
			arrayStudent[i]=splitvalue[0];
			arraySubjectId[i]=splitvalue[1];
		});
		req.courseId = getUrlParameter('courseId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action ="set-student-subject-details";
		req.students=arrayStudent;
		req.subjects=arraySubjectId;
		req.alreadyChecked=alreadyChecked;
		console.log(req);
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			alert(res.message);
			if(res.status == 1){
				window.location.href="edit-course.php?courseId="+getUrlParameter('courseId');
			}
		});
	
	});
	
});


