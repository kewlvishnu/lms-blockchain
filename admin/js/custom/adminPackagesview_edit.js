var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var pacCat = ['All Arcanemind Courses','All Own Courses', 'Courses From Category', 'Custom Course'];
var pactype = ['Market Place', 'Student will pay', 'Institute will pay'];
var error=0;
var packageEdit=[];
var subjectProfessors =[];
var packagedata= [];  
var packagedatai=null;
var packend=[];
var packendi=null;
var courseId=[];
var courseIdi=null;
var previousId=null;
var coursesArray=[];
var studentArray=[];
var uploadimg=0;
var imgpath=""; 
var category=0;
var catID=0;
var price=0;
var priceINR=0;
$(function() {
	getPackages();
	$('#package-details .edit-button').click(function () {
		fillPackagesForm();
        $('#package-details').hide();
        $('#edit-package').fadeIn('fast');
    });
	
	 $('.cancel-edit-button').click(function () {
		$('.has-error').each(function() {
			$(this).removeClass('has-error');
		});
        $('.help-block').remove();
        $('#edit-package').hide();
        $('#package-details').fadeIn('fast');
       
    });
	
	
	function getPackages() {
        var req = {};
        var res;
		req.packId=getUrlParameter('editpackage');
        req.action = 'view-package';
        $.ajax({
            'type': 'post',
            'url': ApiEndPoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
		
            res = $.parseJSON(res);
			console.log(res);
            if (res.status == 0)
			{	alert(res.message);
				$('#warning').show();
			}
                
            else {
                fillPackages(res);
            }
        });
    }
    function fillPackages(data) {
		console.log(data);
	    var subjectHtml = '', html = '';
		var courseStartDate = '';
        $.each(data.packages, function (i, packages) {
			//variable to detect payment can be made or not
			//
			packageEdit[i]=packages;
			 var startdate = new Date(parseInt(packages.startDate));
			 var enddate = new Date(parseInt(packages.endDate));			
			 $('#pack-name').html(' '+packages.packName);
			 $('#pacD').html(' '+packages.packDescription);
			 $('#pactype').html(' '+pactype[packages.packType-1]);
			 $('#paccat').html(' '+pacCat[packages.packCategory-1]);
			 $('#startdate').html(' '+startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear());
			 $('#package-image-preview').attr("src",packages.image);//image
			  $('#endDate').html(' '+enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear());
			  
			 
			  //courses.php
			  $.each(packages.course, function (i, course) {
			   $('#packCourses').append(' '+'<a href="courses.php">'+course.name +'&nbsp;&nbsp;&nbsp; '+'</a>' +' ');
			  }
			  );
		
        });
      
	
    }

	function fillPackagesForm() {
			 $('#package-name').val(' '+packageEdit[0]['packName']);
			 $('#package-description').text(' '+packageEdit[0]['packDescription']);
			 var startdate = new Date(parseInt(packageEdit[0]['startDate']));
			 var enddate = new Date(parseInt(packageEdit[0]['endDate']));
      		 $('#pstartDate').val(' '+startdate.getDate() + ' ' + month[startdate.getMonth()] + ' ' + startdate.getFullYear());
			  $('#pendDate').val(' '+enddate.getDate() + ' ' + month[enddate.getMonth()] + ' ' + enddate.getFullYear());
			  uploadimg=1;
			  imgpath=packageEdit[0]['image1'];
			  category=packageEdit[0]['packCategory'];
			  catID=packageEdit[0]['catID'];
			  price=packageEdit[0]['price'];
			  priceINR=packageEdit[0]['priceINR'];
			  if(packageEdit[0]['packType']==1)
			  {
				 $('#marketplace').prop("checked", true);
				 $('#marketselect').show();
				 $('#price').val(packageEdit[0]['price']);
				 $('#priceINR').val(packageEdit[0]['priceINR']);
				 $('#internalselect').hide();
				 $('#paynow-savepack').hide();
				 $('#savepack').show();
				
				 
			  }
			  else if(packageEdit[0]['packType'] ==2)
			  { fetchStudent();
			  $('#internal').prop("checked", true);
			  $('#internalselect').show();	
			  $('#stupay').prop("checked", true);
			  $('#marketselect').show();
			  $('#price').val(packageEdit[0]['price']);
			  $('#priceINR').val(packageEdit[0]['priceINR']);
			  $('#paynow-savepack').hide();
			  $('#savepack').show();
				
			  	
			  }
			  else{
			 	 fetchStudent();
			  	$('#internal').prop("checked", true);
				$('#internalselect').show();
				$('#paynow-savepack').show();
				$('#savepack').hide();
				$('#inspay').prop("checked", true);
				
				
			  }
			  
			   if(packageEdit[0]['packCategory']==4){
			  	fetchEditOwnerCourses();
				$('#allarcanemindcourses').hide();
				$('#allcourses').hide();
				$('#catCourses').hide();
			  	$('#custom-course').prop("checked", true);
				$('#catSelect').show();
				$('#catSelect2').show();	
				//$('#inspay').prop("checked", true);
			  }
			  else{
			  
			  	//$('#allarcanemindcourses').hide();
			  	$('#allarcanemindcourses').html(''+pacCat[packageEdit[0]['packCategory']-1]);
				$('#allcourses').hide();
				$('#catCourses').hide();
				$('#cust-courses').hide();
			  }
			  
			  
			  
	
	
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	$('#package-image-form').attr('action', fileApiEndpoint);

	$('#package-image-form').on('submit', function(e) {
		e.preventDefault();
		c = jcrop_api.tellSelect();
		crop = [c.x, c.y, c.w, c.h];
		imageA = document.getElementById('image-preview');
		var sources = {
            'package-image': {
                image: imageA,
                type: 'image/jpeg',
                crop: crop,
                /*size: [634, 150],*/
                quality: 1.0
            }
        }
        //settings for $.ajax function
        var settings = {
			url: fileApiEndpoint,
			data: {'packId': 3, 'x': $('#x').val(),'y': $('#y').val(),'w': $('#w').val(),'h': $('#h').val()}, //three fields (medium, small, text) to upload
			beforeSend: function()
			{ console.log(settings);
				//console.log('sending image');
			},
			complete: function (resp) {
				console.log(resp);
				$('#upload-image').modal('hide');
                res = $.parseJSON(resp.responseText);
				if(res.status == 1)
				{
					uploadimg=1;
					imgpath=res.imageName;
					$('#imgPack').parent().append("Image Uploaded Sucessfully !!! ");
				}
				//alertMsg(res.message);
				$('#package-image-form .jcrop-holder').hide();
				$('#package-image-form input[type="file"]').val('');
				
			}
        }
        cropUploadAPI.cropUpload(sources, settings);
	});
	
	 $("#package-image").change(function () {
	 	console.log("inside 2");
         $('.help-block').remove();
        //$('#edit-form .save-button').prop('disabled',false);
		courseImageChanged = true;
        var _URL = window.URL || window.webkitURL;
		var img = new Image();
		var file = this;
		img.onload = function() {
			if(!(img.width < 280 || img.height < 200))
				readURL(file);
			else {
				$('#package-image-form input[type=submit]').prop('disabled',true);
				$('#package-image-form .error').remove();
				$('#package-image-form').append('<span style="color:red;" class="error">Please select a larger image than 280 x 200.</span>');
			}
		}
		img.src = _URL.createObjectURL(this.files[0]);
    });
	
	
	$('#category-course').on('click', function() {
		$('#catSelect').show();
		$('#catSelect1').show();
		$('#catSelect2').hide();
	});

	$('#custom-course').on('click', function() {
		fetchOwnerCourses();
		$('#catSelect1').hide();
		$('#catSelect').show();
		$('#catSelect2').show();			
	});
	
	$('#allowncourses').on('click', function() {
		$('#catSelect1').hide();
		$('#catSelect2').hide();
		$('#catSelect').hide();		
	});
	
	$('#allarcanemindcourses').on('click', function() {
		$('#catSelect1').hide();
		$('#catSelect2').hide();	
		$('#catSelect').hide();	
	});
	
	$('#internal').on('click', function() {
		fetchStudent();
		$('#marketselect').hide();
		$('#internalselect').show();	
		$('#stupay').prop("checked", true)	;
		$('#marketselect').show();
	});
	
	$('#marketplace').on('click', function() {
		$('#internalselect').hide();
		$('#marketselect').show();
		$('#paynow-savepack').hide();
		$('#savepack').show();	
		
	});	
	
	
	
	$('#inspay').on('click', function() {
		fetchStudent();
		$('#marketselect').hide();
		$('#student-pay').show();
		$('#select-students').show();
		$('#savepack').hide();
		$('#paynow-savepack').show();
				
	});
	$('#stupay').on('click', function() {
		fetchStudent();
		$('#student-pay').hide();
		$('#marketselect').show();	
		$('#select-students').show();
		$('#paynow-savepack').hide();
		$('#savepack').show();	
		
			
	});

	$('#pstartDate').on('keypress', function() {
		return false;
	});
	$('#pendDate').on('keypress', function() {
		return false;
	});
	$('#noof-student').on('keypress', function() {
		return false;
	});
	$('#package-name').on('blur', function() {
		unsetError($(this));
		if(min($(this), 3)) {
			if(!max($(this), 50))
				setError($(this), "Please give a shorter package name. Maximum allowed limit is 50 characters.");
		}
		else
			setError($(this), "Please give a longer package name. Minimum allowed limit is 3 characters.");
		
		if($('#package-name').val().trim()!=(packageEdit[0]['packName']))
		{	console.log("yes");
			nameAvailable();
		}
				
		
	});
	
	$('#price').on('blur', function() {
	unsetError($(this));
	});
	$('#priceINR').on('blur', function() {
	unsetError($(this));
	});
	
	$('#package-description').on('blur', function() {
		unsetError($(this));
		if(min($(this), 3)) {
			if(!max($(this), 1000))
				setError($(this), "Please give a shorter Description.");
		}
		else
			setError($(this), "Please give a longer Description name. Minimum allowed limit is 3 characters.");
	});
	
	 $('#startDate').bind("cut copy paste",function(e) {
          e.preventDefault();
      });
	  $('#endDate').bind("cut copy paste",function(e) {
          e.preventDefault();
      });
	
	
	   $('#pstartDate').datetimepicker({
					format: 'd F Y H:i',
					 minDate: 0,
               		 maxDate: '2050/12/31',
					step: 30,
					onSelectTime: function(date) {
						unsetError($('#startDate'));
						if (date.valueOf() > tempEnd.valueOf()) {
							setError($('#startDate'), 'The start date can not be greater than the end date.');
						} else {
							tempStart = date;
						}
					}
				});
				
		$('#pendDate').datetimepicker({
					format: 'd F Y H:i',
					minDate: 0,
               		 maxDate: '2050/12/31',
					step: 30,
					onSelectTime: function(date) {
						unsetError($('#endDate'));
						if (date.valueOf() < tempStart.valueOf()) {
							setError($('#pendDate'), 'The end date can not be less than the start date.');
						} else {
							tempEnd = date;
						}
					}
				});


	
	
	$('.save-button').on('click', function() {
	console.log(catID);
	if(error == 0){
		var ierror=0;
		var pacType=0;
		
		var paytype=0;
		
		var pacType=0;
		var courseCateg	= catID;
		console.log("courseCategcourseCategcourseCategcourseCateg"+courseCateg);
		elem = $(this);
		
		var packName=$('#package-name').val();
		if(packName == '' || packName==null )
		{	ierror=1;
			setError($('#package-name'), "Please Enter Package Name here.");
			return;
		}
		
		var packageDesc=$('#package-description').val();
		if(packageDesc == '' ||packageDesc==null )
		{	ierror=1;
			setError($('#package-description'), "Please Enter Package Description here.");
			return;
		}
		
		
		 
		if($('#pstartDate').val() == '' )
                 {	ierror=1;
                    unsetError($('#pstartDate'));
					unsetError($('#pendDate'));
                  $('#startDate').parent().addClass('has-error').append('<span class="help-block">Please Enter Start Date  </span>');
                	return false;
                 }
		if($('#pendDate').val() == '' )
                 {ierror=1;
                    unsetError($('#pstartDate'));
					unsetError($('#pendDate'));
                  $('#endDate').parent().addClass('has-error').append('<span class="help-block">Please Enter End Date  </span>');
                	return false;
                 }
				 
		var pstartDate = new Date($('#pstartDate').val());
		var pendDate = new Date($('#pendDate').val());
		
		
		if(pstartDate > pendDate || !(pstartDate < pendDate) )
                 {ierror=1;
                    unsetError($('#pstartDate'));
					unsetError($('#pendDate'));
                  $('#pendDate').parent().addClass('has-error').append('<span class="help-block">End Date should be greater than Start Date  </span>');
                	return false;
                 }
		
		
		
	if($('#marketplace').prop('checked') || $('#stupay').prop('checked') ) {
		
		if($('#marketplace').prop('checked')  && $('#price').val().trim()!='' &&  $('#priceINR').val().trim()!='' && $.isNumeric($('#priceINR').val().trim()) && $.isNumeric($('#price').val().trim()) ){
							pacType=1;
							priceINR=$('#priceINR').val().trim();
							price=$('#price').val().trim();
							
			}
			else if($('#stupay').prop('checked')  && $('#price').val().trim()!='' &&  $('#priceINR').val().trim()!='' && $.isNumeric($('#priceINR').val().trim()) && $.isNumeric($('#price').val().trim()) ){
				pacType=2;
				priceINR=$('#priceINR').val().trim();
				price=$('#price').val().trim();
			}
			else{
						ierror=1;
						setError($('#price'), "please enter value .");
						return;
					}

				
																																																									
			}
	else{	ierror=1;
						setError($('#pactype'), "Only marketplace Allowed.");
				return;
			}
	if(uploadimg !=0  || imgpath !='')
				{
					console.log("uimhkkk");
				}
	else{
				ierror=1;
					//$('#imgPack').parent().append("Image Uploaded Sucessfully !!! ");
					setError($('#imgPack'), "Please Upload Image here.");
				}
		if(pacType == 1)
		{
			studentArray=[];
		}
		
		console.log(category);
		  if(ierror==0)
		  { 
		  	var req = {};
			var res;
			req.action = 'edit-package';
			req.packId=getUrlParameter('editpackage');
			req.name = packName.trim();
			req.packDescription=packageDesc.trim();
			req.packType = pacType;
			req.packCategory=category;
			req.catID=courseCateg;
			req.image=imgpath.trim();
			req.price=price;
			req.priceINR=priceINR;
			req.startDate=pstartDate.getTime();
			req.endDate=pendDate.getTime();
			req.courseArray=coursesArray;
			req.studentArray=studentArray;
			console.log(req);
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				console.log(res);
				if(res.status == 0)
					alert(res.message);
				else {
						alert(res.message);
						window.location = 'admin_packages.php';
					
					
					
				}
			});
		
		
		
		  }
		
			
		
		
		
	}
	else{
		setError($('.save-button'), "Please review your forms. It contains errors.");
		return false;
	}
		
	});
	
	$('.save-pay-button').on('click', function() {
	console.log("sayveand pay buton");
	
	if(error == 0){
		var ierror=0;
		var pacType=0;
		var paytype=0;
		var price=0;
		var priceINR=0;
		var pacType=0;
		var courseCateg= catID;
		elem = $(this);
		
		var packName=$('#package-name').val();
		if(packName == '' || packName==null )
		{	ierror=1;
			setError($('#package-name'), "Please Enter Package Name here.");
			return;
		}
		
		var packageDesc=$('#package-description').val();
		if(packageDesc == '' ||packageDesc==null )
		{	ierror=1;
			setError($('#package-description'), "Please Enter Package Description here.");
			return;
		}
		
		
		 
		if($('#pstartDate').val() == '' )
                 {	ierror=1;
                    unsetError($('#pstartDate'));
					unsetError($('#epndDate'));
                  $('#pstartDate').parent().addClass('has-error').append('<span class="help-block">Please Enter Start Date  </span>');
                	return false;
                 }
		if($('#pendDate').val() == '' )
                 {ierror=1;
                    unsetError($('#pstartDate'));
					unsetError($('#pendDate'));
                  $('#pendDate').parent().addClass('has-error').append('<span class="help-block">Please Enter End Date  </span>');
                	return false;
                 }
				 
		var startDate = new Date($('#pstartDate').val());
		var endDate = new Date($('#pendDate').val());
		
		
		if(startDate > endDate || !(startDate < endDate) )
                 {ierror=1;
                    unsetError($('#pstartDate'));
					unsetError($('#pendDate'));
                  $('#pendDate').parent().addClass('has-error').append('<span class="help-block">End Date should be greater than Start Date  </span>');
                	return false;
                 }
		
		
		
		console.log(packageDesc);
		
	if($('#internal').prop('checked') && $('#inspay').prop('checked') ) {
						pacType=3;																																										
			}
	else{				
						ierror=1;
						setError($('#pactype'), "Only please select Internal and Institute will pay option.");
				return;
			}
	if(uploadimg !=0  || imgpath !='')
				{
					console.log("uimhkkk");
				}
	else{
				ierror=1;
					//$('#imgPack').parent().append("Image Uploaded Sucessfully !!! ");
					setError($('#imgPack'), "Please Upload Image here.");
				}
		
		if(pacType == 1)
		{
			studentArray=[];
		}
		console.log(category);
		  if(ierror==0)
		  { console.log("kcnc");
		  	var req = {};
			var res;
			req.action = 'edit-package';
			req.packId=getUrlParameter('editpackage');
			req.name = packName.trim();
			req.packDescription=packageDesc.trim();
			req.packType = pacType;
			req.packCategory=category;
			req.catID=courseCateg;
			req.image=imgpath.trim();
			req.price=0;
			req.priceINR=0;
			req.startDate=startDate.getTime();
			req.endDate=endDate.getTime();
			req.courseArray=coursesArray;
			req.studentArray=studentArray;
			console.log(req);
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				console.log(res);
				if(res.status == 0)
					alert(res.message);
				else {
						window.location = 'createPackagePay.php?newcase='+ res.id;
						alert(res.message);
					
					
					
				}
			});
		
		
		
		  }
		
			
		
		
		
	}
	else{
		setError($('.save-button'), "Please review your forms. It contains errors.");
		return false;
	}
		
	});
	
	
	
	
	
	function nameAvailable() {

		
		if($('#package-name').val().trim().length != '') {
		//console.log($('#package-name').val().trim());
			var req = {};
			var res;
			//req.packId=getUrlParameter('editpackage');
			req.action = 'check-name-for-package';
			req.name = $('#package-name').val().trim();
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				console.log(res);
				if(res.status == 0)
					alert(res.message);
				else {
					unsetError($('#package-name'));
					
					if(!res.available){
						setError($('#package-name'),'Please select a different name.This name is already used');
						error=1;
						
					}
					
				}
			});
		}
	}
	
	
	
	function min(what, length) {
		if(what.val().length < length)
			return false;
		else
			return true;
	}
	
	function max(what, length) {
		if(what.val().length > length)
			return false;
		else
			return true;
	}
	
	function setError(where, what) {
		unsetError(where);
		where.parent().addClass('has-error');
		where.parent().append('<span class="help-block">'+what+'</span>');
	}
	
	function unsetError(where) {
		if(where.parent().hasClass('has-error')) {
			where.parent().find('.help-block').remove();
			where.parent().removeClass('has-error');
		}
	}
	
	
	
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src',data.profileDetails.profilePic);
		}
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fetchCourses() {
		var req = {};
		var res;
		req.action = 'get-courses-for-exam';
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0) {
				alert(res.message);
			}
			else
				fillCourseSelect(res);
		});
	}
	
	function fillCourseSelect(data) {
		$('#courseSelect').attr('disabled',false);
		for(i=0; i<data.courses.length; i++) {
			$('#courseSelect').append('<option value="'+data.courses[i]['id']+'">'+data.courses[i]['name']+'</option>');
		}
		if(data.courses.length > 0)
			$('#warning').html("Please select a course to continue");
		else
			$('#warning').html('No Course found in your profile. Please <a href="add-course.php">Add Course</a> to continue with the Assignment Creation.');
	}
	
	function fetchSubjects(courseId) {
		var req = {};
		var res;
		req.action = 'get-subjects-for-exam';
		req.courseId = courseId;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0) {
				alert(res.message);
			}
			else
				fillSubjectSelect(res);
		});
	}
	
	function fillSubjectSelect(data) {
		if(data.subjects.length > 0)
			$('#warning').html("Please select a subject to continue.");
		else
			$('#warning').html("You do not have any subjects in this coarse. Please add a <a href='" + $('#addSubject').attr('href') + "'>new subject</a> to continue.");
		$('#subjectSelect').find('option').remove()
		$('#subjectSelect').append('<option value="0">Select Subject</option>');
		$("#subjectSelect").attr('disabled', false);
		for(i=0; i<data.subjects.length; i++) {
			$('#subjectSelect').append('<option value="'+data.subjects[i]['id']+'">'+data.subjects[i]['name']+'</option>');
		}
	}
	
	function fetchExams(subjectId) {
		var req = {};
		var res;
		req.action = 'get-exam-details';
		req.subjectId = subjectId;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0) {
				$('#warning').html("You have not created any Assignment/Exam yet.");
				$('#currentExams').find('tbody tr').remove();
				$('#warning').show();
			}
			else
				fillExams(res);
		});
	}
	
	function fillExams(data) {
		$('#warning').hide();
		$('#tableContainer').show();
		$('#currentExams').find('tbody tr').remove();
		var html = "";
		var resultFlag = false;
		for(i=0; i<data.exams.length; i++) {
			var chapter;
			var status;
			var disableState = 'disabled';
			if(data.exams[i].attemptCount > 0) {
				resultFlag = true;
				disableState = '';
			}
			if(data.exams[i]['chapterId'] == 0)
				chapter = "Independent";
			else
				chapter = data.exams[i]['chapterName'];
			if(data.exams[i]['status'] == 0)
				status = "Draft";
			else if(data.exams[i]['status'] == 1)
				status = "Not Live";
			else if(data.exams[i]['status'] == 2)
				status = "Live";
			if(data.exams[i]['delete'] == 0) {
				html += '<tr data-id="' + data.exams[i]['id'] + '">'
						+ '<td width="150">'+data.exams[i]['name']+'</td>'
						+ '<td>'+data.exams[i]['type']+'</td>'
						+ '<td>'+chapter+'</td>'
						+ '<td>'+status+'</td>';
                                var startDate = new Date(parseInt(data.exams[i]['startDate']));
				var endDate = new Date(parseInt(data.exams[i]['endDate']));
				var start = startDate.getDate()+ ' ' +MONTH[startDate.getMonth()]+ ' ' + startDate.getFullYear();
				if(data.exams[i]['endDate'] == '')
					var end = '--';
				else
					var end = endDate.getDate() + ' ' + MONTH[endDate.getMonth()] + ' ' + endDate.getFullYear();
				html += '<td>'+start+'</td>'
						+ '<td>'+end+'</td>'
						+ '<td>'+data.exams[i]['attempts']+'</td>'
						+ '<td>'
							+ '<a href="add-assignment-2.php?examId='+data.exams[i]['id']+'" class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-placement="top" type="button" data-original-title="Edit step 2"><i class="fa fa-edit"></i> Edit Questions</a>'
							+ '<a href="edit-assignment.php?examId='+data.exams[i]['id']+'" type="button" data-placement="top" data-toggle="tooltip" class="btn btn-success btn-xs tooltips" data-original-title="Edit step 1"><i class="fa fa-cog"></i> Settings</a>'
							+ '<a type="button" data-placement="top" data-toggle="tooltip" class="btn btn-info btn-xs tooltips" data-original-title="View Results" href="examResult.php?examId=' + data.exams[i].id +'" ' + disableState + '><i class="fa fa-bullhorn"></i> Result</a>'
							+ '<a class="btn btn-info btn-xs tooltips copy-exam" data-toggle="tooltip" data-placement="top" type="button" data-original-title="Copy"><i class="fa fa-copy"></i> Copy</a>'
							/*+ '<a class="btn btn-success btn-xs tooltips" data-toggle="tooltip" data-placement="top" type="button" data-original-title="Information"><i class="fa fa-info"></i></a>'*/
							+ '<a type="button" data-placement="top" data-toggle="tooltip" class="btn btn-danger btn-xs tooltips delete-button" data-original-title="Delete"><i class="fa fa-trash-o"></i> Delete</a>'
						+ '</td>'
					+ '</tr>';
			}
			/*else {
				html += '<tr data-id="' + data.exams[i]['id'] + '" class="deleted-item">'
						+ '<td width="150">'+data.exams[i]['name']+'</td>'
						+ '<td>'+data.exams[i]['type']+'</td>'
						+ '<td>'+chapter+'</td>'
						+ '<td>Deleted</td>'
						+ '<td>'+data.exams[i]['startDate']+'</td>'
						+ '<td>'+data.exams[i]['endDate']+'</td>'
						+ '<td>'+data.exams[i]['attempts']+'</td>'
						+ '<td>'
							+ '<a href="edit-assignment.php?examId='+data.exams[i]['id']+'" type="button" data-placement="top" data-toggle="tooltip" class="btn btn-success btn-xs tooltips" data-original-title="Edit step 1"><i class="fa fa-cog"></i></a>'
							+ '<a href="add-assignment-2.php?examId='+data.exams[i]['id']+'" class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" data-placement="top" type="button" data-original-title="Edit step 2"><i class="fa fa-edit"></i></a>'
							+ '<a  class="btn btn-info btn-xs tooltips copy-exam" data-toggle="tooltip" data-placement="top" type="button" data-original-title="Copy"><i class="fa fa-copy"></i></a>'
							+ '<a class="btn btn-success btn-xs tooltips" data-toggle="tooltip" data-placement="top" type="button" data-original-title="Information"><i class="fa fa-info"></i></a>'
							+ '<a type="button" data-placement="top" data-toggle="tooltip" class="btn btn-success btn-xs tooltips restore-button" data-original-title="Restore"><i class="fa fa-refresh"></i></a>'
						+ '</td>'
				+ '</tr>';
			}*/
		}
		$('#currentExams').append(html);
		if(resultFlag)
			$('#seeResults').attr('disabled', false);
		else
			$('#seeResults').attr('disabled', true);
		$('.tooltips').tooltip();
		$('.delete-button').on('click', function() {
			var con = confirm("Are you sure you want to delete this Exam/Assignment.");
			if(con) {
				var req = {};
				req.action = 'delete-exam';
				req.examId = $(this).parents('tr').attr('data-id');
				$.ajax({
					'type'	: 'post',
					'url'	: ApiEndpoint,
					'data'	: JSON.stringify(req)
				}).done(function(res) {
					res = $.parseJSON(res);
					if(res.status == 0)
						alert(res.message);
					else {
						fetchExams($('#subjectSelect').val());
					}
				});
			}
		});
                var examname='';
                var iexamId='';
                var examType='';
                 $('.copy-exam').on('click', function() {
                     var tr=$(this).parents('tr');
                    iexamId = tr.attr('data-id');
                     examname = 'Copy of ' + tr.find('td:eq(0)').text();
                     $('#new-exam').val(examname);
                    $('#modal-copy-exam').modal('show');
                 });
                 $('#copy-exam').off('click');
                $('#copy-exam').on('click', function() {
                    $(this).attr('disabled',true);
                        var newExamname = $('#new-exam').val();
			if(newExamname!=null) {
				var req = {};
				req.action = 'copy-exam';
				req.iexamId =iexamId;
                                req.iname=newExamname;
                               	$.ajax({
					'type'	: 'post',
					'url'	: ApiEndpoint,
					'data'	: JSON.stringify(req)
				}).done(function(res) {
                                     $('#copy-exam').attr('disabled',false);
					res = $.parseJSON(res);
					if(res.status == 0)
						alert(res.message);
					else {
						fetchExams($('#subjectSelect').val());
                                               $('#modal-copy-exam').modal('hide');
					}
				});
			}
		});
		
		$('.restore-button').on('click', function() {
			var req = {};
			req.action = 'restore-exam';
			req.examId = $(this).parents('tr').attr('data-id');
			$.ajax({
				'type'	: 'post',
				'url'	: ApiEndpoint,
				'data'	: JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else
					fetchExams($('#subjectSelect').val());
			});
		});
	}



	function readURL(input) {
		console.log(input);
		if(input.files && input.files[0]) {
            if(!(input.files[0]['type'] == 'image/jpeg' || input.files[0]['type'] == 'image/png' || input.files[0]['type'] == 'image/gif')) {
				$('#package-image-form input[type=submit]').prop('disabled',true);
				$('#package-image-form .error').remove();
				$('#package-image-form').append('<span class="help-block" style="color:red;" class="error">Please select a proper file type.</span>');
				return;
			}else if(input.files[0]['size']>819200) {
				$('#package-image-form input[type=submit]').prop('disabled',true);
				$('#package-image-form .error').remove();
				$('#package-image-form').append('<span style="color:red;" class="error">Files larger than 800kb are not allowed.</span>');
				return;
			}else {
				$('#package-image-form .error').remove();
				$('#package-image-form input[type=submit]').prop('disabled',false);
			}
            var reader = new FileReader();
            if (typeof jcrop_api != 'undefined' && jcrop_api != null) {
                jcrop_api.destroy();
                jcrop_api = null;
                var pImage = $('.crop');
                pImage.css('height', 'auto');
                pImage.css('width', 'auto');
                var height = pImage.height();
                var width = pImage.width();
                $('.jcrop').width(width);
                $('.jcrop').height(height);
            }
            reader.onload = function (e) {
                $('#image-preview').attr('src', e.target.result);
                $('#upload-image .crop').Jcrop({
                    onSelect: updateCoords,
                    bgOpacity:   .4,
					boxWidth: 830,
					minSize: [280, 200],
					setSelect:   [0, 0, 280, 200],
					aspectRatio: 804/440
                }, function () {
                    jcrop_api = this;
					jcrop_api.setSelect([0, 0, 280, 200]);
                });
            }
            reader.readAsDataURL(input.files[0]);
        }
	}




function fetchEditOwnerCourses() {
console.log("111fillInstituteProfessorscheck");
		var req = {};
		var res;
		req.packId=getUrlParameter('editpackage');
		req.action = "getEditcoursesByowner";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			console.log(res);
			res =  $.parseJSON(res);
			if(res.status == 1) {
				fillOwnerCourses(res);
				//$('#manageProfessorSection').show();
			}
			else if(res.status == 0)
				alertMsg(res.message);
		});
	}
function fillOwnerCourses(data) {
	console.log("fillOwnerCourses");
	console.log(data);
		var option = "";
		var selectElem = '#courses-list';
		var select = $(selectElem);
		var tempProf = {};
		$.each(data.courses, function(i,v) {
			var pName = v.name;
			option = "<option value='" + v.id + "'>" + pName + "</option>";
			select.append(option);
			tempProf[v.professorId] = pName;
		});
		applyMultiselectForCourses(selectElem);
		
		
	}

function applyMultiselectForCourses(selector) {
				
	$(selector).multiSelect({
		afterSelect: function(value) {
			//alert("Select value: "+values);
			var index = coursesArray.indexOf(value[0]);
			if(index === -1)
				coursesArray.push(value[0]);
		},
		afterDeselect: function(value) {
				var index = coursesArray.indexOf(value[0]);
				coursesArray.splice(index, 1);
		}
	});
}



function fetchStudent() {
		var req = {};
		var res;
		req.packId=getUrlParameter('editpackage');
		req.action = "geteditstudent-package";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			console.log(res);
			res =  $.parseJSON(res);
			if(res.status == 1) {
				fillStudent(res);
				//$('#manageProfessorSection').show();
			}
			else if(res.status == 0)
				alertMsg(res.message);
		});
	}
function fillStudent(data) {
	
		var option = "";
		var selectElem = '#students-list';
		var select = $(selectElem);
		var tempProf = {};
		$.each(data.students, function(i,v) {

			var pName = v.name;
			option = "<option value='" + v.id + "'>" + pName + "</option>";
			select.append(option);
			tempProf[v.professorId] = pName;
		});
		applyMultiselectForStudent(selectElem);
		$('#noof-student').text(studentArray.length);
		
		//Html for subject Profs.
		/*var html = '';
		$.each(data.subjectProfessors, function(i,v) {
			$(selectElem).multiSelect('select', v.professorId);
			html += '<li data-professor="' + v.professorId + '"><a>' + tempProf[v.professorId] + '<span class="delete-professors pull-right"><i class="fa fa-trash-o" style="cursor: pointer;"></i></span></a></li>';
		});
		if(html != '') {
			$('#subject-professors-teaser').html(html);
			$('.delete-professors').on('click', function() {
				var req = {};
				var res;
				var elem = $(this);
				var professor = elem.parents('li').attr('data-professor');
				con = confirm("Are you sure to delete this professor?");
				if(con) {
					req = {};
					req.professorId = professor;
					req.subjectId = getUrlParameter('subjectId');
					req.action = 'delete-single-professor';
					$.ajax({
						'type'	:	'post',
						'url'	:	ApiEndpoint,
						'data'	:	JSON.stringify(req)
					}).done(function(res) {
						res = $.parseJSON(res);
						if(res.status == 0) {
							alert(res.message);
						}
						else {
							subjectProfessors = subjectProfessors.splice(subjectProfessors.indexOf(professor));
							alertMsg('Professor removed successfully');
							elem.parents('li').remove();
							$('#subject-professors').multiselect('deselect_all');
							$('#subject-professors').multiselect('select', subjectProfessors);
						}
					});
				}
			});
		}
		else {
			$('#subject-professors-teaser').html('<li><a>No Professors</a></li>');
		}
		// Show Manage  popup if #manage is present in url
		if(typeof window.location.hash !== "undefined" && window.location.hash == "#manage" && popupShown == false) {
			$('#subject-professor-modal').modal('show');
			popupShown = true;
		}
	*/
	}

function applyMultiselectForStudent(selector) {
				
	$(selector).multiSelect({
		afterSelect: function(value) {
			//alert("Select value: "+values);
			var index = studentArray.indexOf(value[0]);
			if(index === -1)
				studentArray.push(value[0]);
				$('#noof-student').text(studentArray.length);
				console.log(studentArray.length);
		},
		afterDeselect: function(value) {
				var index = studentArray.indexOf(value[0]);
				studentArray.splice(index, 1);
				$('#noof-student').text(studentArray.length);
		}
	});
}


function updateCoords(c) {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
		var crop = [c.x, c.y, c.w, c.h];
    }





	




/*

function fetchInstituteProfessorscheck() {
console.log("111fillInstituteProfessorscheck");
		var req = {};
		var res;
		req.courseId = 62;
		req.subjectId = 110;
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action = "get-institute-professors";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			console.log(res);
			res =  $.parseJSON(res);
			if(res.status == 1) {
				fillInstituteProfessorscheck(res);
				//$('#manageProfessorSection').show();
			}
			else if(res.status == 0)
				alertMsg(res.message);
		});
	}
	
	function fillInstituteProfessorscheck(data) {
	console.log("fillInstituteProfessorscheck");
	console.log(data);
		var option = "";
		var selectElem = '#subject-professors1';
		var select = $(selectElem);
		var tempProf = {};
		$.each(data.instituteProfessors, function(i,v) {
			var pName = v.firstName + ' ' + v.lastName;
			option = "<option value='" + v.professorId + "'>" + pName + "</option>";
			select.append(option);
			tempProf[v.professorId] = pName;
		});
		applyMultiselectForProf(selectElem);
		
		//Html for subject Profs.
		var html = '';
		$.each(data.subjectProfessors, function(i,v) {
			$(selectElem).multiSelect('select', v.professorId);
			html += '<li data-professor="' + v.professorId + '"><a>' + tempProf[v.professorId] + '<span class="delete-professors pull-right"><i class="fa fa-trash-o" style="cursor: pointer;"></i></span></a></li>';
		});
		if(html != '') {
			$('#subject-professors-teaser').html(html);
			$('.delete-professors').on('click', function() {
				var req = {};
				var res;
				var elem = $(this);
				var professor = elem.parents('li').attr('data-professor');
				con = confirm("Are you sure to delete this professor?");
				if(con) {
					req = {};
					req.professorId = professor;
					req.subjectId = getUrlParameter('subjectId');
					req.action = 'delete-single-professor';
					$.ajax({
						'type'	:	'post',
						'url'	:	ApiEndpoint,
						'data'	:	JSON.stringify(req)
					}).done(function(res) {
						res = $.parseJSON(res);
						if(res.status == 0) {
							alert(res.message);
						}
						else {
							subjectProfessors = subjectProfessors.splice(subjectProfessors.indexOf(professor));
							alertMsg('Professor removed successfully');
							elem.parents('li').remove();
							$('#subject-professors').multiselect('deselect_all');
							$('#subject-professors').multiselect('select', subjectProfessors);
						}
					});
				}
			});
		}
		else {
			$('#subject-professors-teaser').html('<li><a>No Professors</a></li>');
		}
		// Show Manage  popup if #manage is present in url
		if(typeof window.location.hash !== "undefined" && window.location.hash == "#manage" && popupShown == false) {
			$('#subject-professor-modal').modal('show');
			popupShown = true;
		}
	}


function applyMultiselectForProf(selector) {
				
	$(selector).multiSelect({
		afterSelect: function(value) {
			//alert("Select value: "+values);
			var index = subjectProfessors.indexOf(value[0]);
			if(index === -1)
				subjectProfessors.push(value[0]);
		},
		afterDeselect: function(value) {
				var index = subjectProfessors.indexOf(value[0]);
				subjectProfessors.splice(index, 1);
		}
	});
}*/






});