var ApiEndpoint = '../api/index.php';


$(function() {
   var key_rate="";
   var display_button='';

   var link_id = GetURLParameter('link_id');
   
   window.location = "http://www.page-2.com";
  
	invitation_details();
	function invitation_details(){
	    var req = {};
		var res; 
		req.action = "load_student_notification";
		req.link_id = link_id;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			data =  $.parseJSON(res);
			var html="";
			for(var i=0; i<data.invitations.length; i++)
			{
			var invitation_id=data.invitations[i].id;
			var course_id=data.invitations[i].course_id;
			var email=data.invitations[i].email_id;
			var contact_no=data.invitations[i].contact_no;
			var enrollment_type=data.invitations[i].enrollment_type;
			var status=data.invitations[i].status;
			var course=data.invitations[i].courseName;
			var studentName=data.invitations[i].studentName;
			var timestamp=data.invitations[i].timestamp;
			switch(status){
			case '0':display_button="<span class='label label-warning label-mini' style='display:block;'><i class='fa fa-info-circle'></i>Pending</span>";break;
			case '1':display_button="<span class='label label-success label-mini' style='display:block;'><i class='fa fa-check-circle'></i>Accepted</span>";break;
			case '2':display_button="<span class='label label-danger label-mini' style='display:block;'><i class='fa fa-ban'></i>Rejected</span>";break;
			}
			html+='<tr invitation_id= '+invitation_id+'><td>'+(i+1)+' </td><td>'+course+'</td><td>'+display_button+'</td><td><span class="label label-info label-mini accept_invitation" >Accept</span>&nbsp;<span class="label label-danger label-mini reject_invitation" >Reject</span> </td></tr>';
		  }
			$('#tbl_student_deatils').append(html);
			
			
		$('.accept_invitation').on('click',function() {
			 var link_id=$(this).parents('tr').attr('invitation_id');
			    var req = {};
				var res; 
				req.action = "accepted_student_invitation";
				req.link_id =link_id;
	 	      	$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
				}).done(function (res) {
					data =  $.parseJSON(res);
					return false;
		
		   });
				
		   });
		 $('.reject_invitation').on('click',function() {
			 var link_id=$(this).parents('tr').attr('invitation_id');
			    var req = {};
				var res; 
				req.action = "rejected_student_invitation";
				req.link_id =link_id;
	 	   	$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
				}).done(function (res) {
					data =  $.parseJSON(res);
					return false;
		
		   });
	
		});   
	
		});
	

	}

	
	
	   function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}
	
	});