var ApiEndpoint = '../api/index.php';
//var fileApiEndpoint = '../api/files1.php';

$(function () {
	$('#loader_outer').hide();
	loadbackCoursekeys();
	function loadbackCoursekeys() {
		var req = {};
		var res;
		req.action = "back_end_course_keys";
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			var html = "";
			var count=0;
			for (var i = 0; i < data.key_details.length; i++) {
				var userId = data.key_details[i].key_details[0].userId;
				var active = data.key_details[i].key_details[0].active;
				var name = data.key_details[i].key_details[0].name;
				var total_key = data.key_details[i].key_details[0].total_key;
				var reamining = total_key - active;
				var key_rate='';
				if(data.key_details[i].key_rate.length>0){
					var edited = data.key_details[i].key_rate[0].is_editable;
					key_rate = data.key_details[i].key_rate[0].key_rate;
					var role = data.key_details[i].role[0].name;
				}
				else{
					//console.log( data.key_details[i].key_details[0].userId);
				}

				var style = "";
				if (edited == 1) {
					style = 'bgcolor=#DDEBF9';
				}
				display_currency = 'IGRO';

				//if( key_rate ==='' ){
					//html += "";
				//}else{
					html += '<tr ' + style + ' rowid=' + userId + '><td>' + (count + 1) + '</td><td>' + userId + '</td><td>' + name + '</td><td>' + display_currency + '</td><td>' + role + '</td><td>' + total_key + '</td><td>' + reamining + '</td><td>' + key_rate + '</td>';

					html += '<td><a  href="#modal_change_key_rate" data-toggle="modal" class="btn btn-primary btn-xs key_rate_edit" ><i class="fa fa-edit"></i> </a>  <a   class="btn btn-info btn-xs key_rate_default" > <i class="fa fa-refresh"></i> </a> <a href="#modal_purchase_key" data-toggle="modal"   class="btn btn-success btn-xs add_keys" > <i class="fa fa-plus-circle"></i> </a>  </td></tr>';
					count++;
				//}

			}
			$('#all-courses-tbl tbody').html(html);
			
			var table = $('#all-courses-tbl').dataTable({
				"aaSorting": [[0, "asc"]],
  				"bDestroy": true
			});
			// }
			$('#default_rate').val(data.default_rate[0].key_rate);
			var rowid = '';

			$('.key_rate_edit').off('click');
			$('#all-courses-tbl').on('click', '.key_rate_edit', function(){
				rowid = $(this).parents('td').parent('tr').attr('rowid');
				var td = $(this).parent().prev();
				var rate = td.text();
				$("#key_rate_course").val(rate);
				/*console.log(rowid);
				console.log(td);
				console.log(dollar);
				console.log(rate);*/
			});
			var display_currency;
			var key_rate = '';
			$('.add_keys').off('click');
			$('#all-courses-tbl').on('click', '.add_keys', function(){
			//$('.add_keys').on('click', function () {
				display_currency = 'IGRO';
				$('#totalkeys').val('');
				$('#keyrate').text('');
				$('#totalkeyrate').text('');
				var tr = $(this).parents('td').parent('tr');
				rowid = tr.attr('rowid');
				key_rate = tr.find("td:nth-child(8)").text();
				$('#keyrate').text(key_rate);
				$('#keyrate').append(display_currency);
			});

			$('#save_default_key_rate').off('click');
			$('#save_default_key_rate').on('click', function () {

				var key_rate = $("#default_rate").val();
				var req = {};
				var res;
				var validate=true;
				$('.help-block').remove();
				if(key_rate<=0){
					$('#default_rate').after('<span class="help-block text-danger">Enter key rate greater than 0</span>');
					validate= false;
				}
				if(validate== false){
					return false;
				}
				req.action = "set_default_key_rate";
				req.key_rate = key_rate;
				$.ajax({
					'type': 'post',
					'url': ApiEndpoint,
					'data': JSON.stringify(req)
				}).done(function (res) {
					data = $.parseJSON(res);
					window.location = self.location;
					//$(this).hide();
				});

			});
			$("#totalkeys").off("keyup");
			$('#totalkeys').keyup(function () {
				var total = $(this).val() * key_rate;
				$('#totalkeyrate').text(total);
				$('#totalkeyrate').append(display_currency);

			});
			$("#save_rate").off("click");
			$('#save_rate').on('click', function () {
				var key_rate = $("#key_rate_course").val();
				$('.help-block').remove();
				var req = {};
				var res;
				var validate=true;
				if(key_rate<=0){
					$('#key_rate_course').after('<span class="help-block text-danger">Enter key rate greater than 0</span>');
					validate= false;
				}
				if(validate== false){
					return false;
				}
				req.action = "change_key_rate";
				req.key_user = rowid;
				req.key_rate = key_rate;
				$.ajax({
					'type': 'post',
					'url': ApiEndpoint,
					'data': JSON.stringify(req)
				}).done(function (res) {
					data = $.parseJSON(res);
					window.location = self.location;

				});
			});
			$(".key_rate_default").off("click");
			//function to  set back key rate to default for an institute 
			$('#all-courses-tbl').on('click', '.key_rate_default', function(){
			//$('.key_rate_default').on('click', function () {
				if (confirm('Are you sure to set default rate ?')) {
					var td = $(this).parents('td');
					rowid = td.parent('tr').attr('rowid');
					var req = {};
					var res;
					req.action = "setInstitute_rate_back";
					req.key_user = rowid;
					$.ajax({
						'type': 'post',
						'url': ApiEndpoint,
						'data': JSON.stringify(req)
					}).done(function (res) {
						data = $.parseJSON(res);
						window.location = self.location;
					});
				}
			});
			$("#purchase_key").off("click");
			$('#purchase_key').on('click', function () {
				var keys = $('#totalkeys').val();
				var req = {};
				var res;
				req.action = "add_keys";
				req.keys = keys;
				req.key_user = rowid;
				$('.help-block').remove();
				if(keys<=0){
					$('#totalkeys').after('<span class="help-block text-danger">Enter keys greater than 0</span>');
					return false;
				}
				$.ajax({
					'type': 'post',
					'url': ApiEndpoint,
					'data': JSON.stringify(req)
				}).done(function (res) {
					data = $.parseJSON(res);
					alertMsg(keys+' are added to the institute');
					$('#modal_purchase_key').modal('hide');
					$("#all-courses-tbl").dataTable().fnDestroy();
					loadbackCoursekeys();
				});

			});

		});
	}
	$('#default_rate,#key_rate_course').keypress(function (event) {
		return eventNumber(event, $(this));
	});
	function eventNumber(event, obj) {
		if ((event.which != 46 || obj.val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) && event.which != 8) {
			return false;
		}

		var text = obj.val();
		if ((text.indexOf('.') != -1) && (text.substring(text.indexOf('.')).length > 2)) {
			if (event.which != 8)
				return false;
		}
		return true;

	}
	$("#totalkeys").keypress(function (e) {
		$('.help-block').remove();
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			//display error message
			$('#totalkeys').after('<span class="help-block text-danger">Enter Only Numbers</span>');
			return false;
		}
		if (e.which == 13) {
			$('#purchase_key').trigger("click");
			return false;
		}
	});
});