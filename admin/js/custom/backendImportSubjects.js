/*
 Author: Rupesh Pandey.
 Dated: 02/04/2015
 */
var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var userRole;
var courseFrom = 0;
var courseTo = 0;
$(function () {
	fetchCourses();
});

function fetchCourses() {
	var req = {};
	var res;
	req.action = "get-list-courses-admin";
	$.ajax({
		'type': 'post',
		'url': ApiEndpoint,
		'data': JSON.stringify(req)
	}).done(function (res) {
		res = $.parseJSON(res);
		if (res.status == 1)
			fillCourses(res);
		else
			alertMsg(res.message);
	});
}

function fillCourses(data) {
	var html = "";
	var flag = false;
	var editCourseLink, editSubjectLink, addSubjectLink;
	$.each(data.courses, function (i, course) {
		editCourseLink = "backend_edit-course.php?courseId=" + course.id;
		addSubjectLink = "add-subject.php?courseId=" + course.id;
		courseId = "C" + String("000" + course.id).slice(-4);
		html += "<tr data-cid='" + course.id + "'>"
				+ "<td>" + courseId + "</td>"
				+ "<td><h5><a href='" + editCourseLink + "'>" + course.name + "</a></h5>";

		html += "</td>"
		html += "<td>" + course.userid + "</td>";
		html += "<td>" + course.username + "</td>";
		html += "<td class=small-txt>" + course.email + "</td>";
		html += "<td>" + course.contactMobile + "</td>"
				+ "<td><a href='#' class='import'><span class='label label-danger label-mini' style='display:block;'>Select</span></a>"
				+ "</tr>";
	});
	$('.table-courses tbody').html('').html(html);
	$('.table-courses').dataTable({
        "aaSorting": [[0, "asc"]]
    });
	addApprovalHandlers();
}

function addApprovalHandlers() {
	$('#step1-courses').on( 'click', '.import', function (e) {
	//$('#all-courses .reject').on('click', function (e) {
		e.preventDefault();
		courseFrom = $(this).parents('tr').attr("data-cid");
		$("#step1").addClass("hide");
		$("#step2").removeClass("hide");
	});
	$('#step2-courses').on( 'click', '.import', function (e) {
	//$('#all-courses .reject').on('click', function (e) {
		e.preventDefault();
		courseTo = $(this).parents('tr').attr("data-cid");
		var req = {};
		req.action = "get-course-subjects";
		req.courseId = courseFrom;
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			//console.log(res);
			res = $.parseJSON(res);
			if (res.status == 1) {
				var subjects = res.subjects;
				if (subjects.length>0) {
					$("#step3 .subjects").html('');
					for (var i = 0; i < subjects.length; i++) {
						$("#step3 .subjects").append(
							'<li data-sid="'+subjects[i].id+'"><input type="checkbox" />'+subjects[i].name+'</li>');
					};
				}
			} else {
				alertMsg(res.message);
			}
		});
		$("#step2").addClass("hide");
		$("#step3").removeClass("hide");
	});
	$('#step3').on( 'click', '.btn-import', function (e) {
        var selectedSubjects = [];
		$("#step3 .subjects li").each(function(obj){
            var subjectId = $(this).attr('data-sid');
			if($(this).find("input").prop('checked') == true) {
                selectedSubjects.push(subjectId);
			}
		});
		if (selectedSubjects.length==0) {
			alert("Please select at least one subject!");
		} else {
			var req = {};
			req.courseId = courseTo;
			req.courseFrom = courseFrom;
			req.action	 = 'import-subjects-to-course';
			req.subjects = selectedSubjects;
			$.ajax({
				'type': 'post',
				'url': ApiEndpoint,
				'data': JSON.stringify(req)
			}).done(function (res) {
				res = $.parseJSON(res);
				if (res.status == 1) {
					alert(res.message);
					location.reload();
				} else {
					alertMsg(res.msg);
				}
			});
		}
	});
}