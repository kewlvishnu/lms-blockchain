var ApiEndpoint = '../api/index.php';

$(function () {
    $('a.students').click();
    $('a.students').addClass('active');
    var key_rate = "";
    var display_button = '';
    get_institute_students();
    function get_institute_students() {
        var req = {};
        var res;
        req.action = "get-institute-students";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            data = $.parseJSON(res);
            //return false;
            var html = "";
            console.log(data);
            var students = data.students;
            for (var i = 0; i < students.length; i++) {
                html+= '<tr>'+
                            '<td>'+(i+1)+'</td>'+
                            '<td>'+students[i].username+'</td>'+
                            '<td>'+students[i].email+'</td>'+
                            '<td>'+students[i].name+'</td>'+
                            '<td>'+students[i].contactMobile+'</td>'+
                            '<td>'+students[i].courseNames+'</td>'+
                            '<td><button class="btn btn-info btn-xs">Action</button></td>'+
                        '</tr>';
            };
            $('#tbl_student_deatils tbody').html(html);

            var table = $('#tbl_student_deatils').dataTable({
                "aaSorting": [[0, "asc"]],
                "bDestroy": true
            });
            
        });
    }
     $('.search').keyup(function ()
    {
        searchTable($(this).val());
    });

});
function searchTable(inputVal)
{
    var table = $('#tbl_student_deatils');
    table.find('tr').each(function (index, row)
    {
        var allCells = $(row).find('td');
        if (allCells.length > 0)
        {
            var found = false;
            allCells.each(function (index, td)
            {
                var regExp = new RegExp(inputVal, 'i');
                if (regExp.test($(td).text()))
                {
                    found = true;
                    return false;
                }
            });
            if (found == true)
                $(row).show();
            else
                $(row).hide();
        }
    });
}