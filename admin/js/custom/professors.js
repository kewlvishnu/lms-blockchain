var userId = 0;

/**
* Fetches the professor details according to user search query and displays the results as html
*/
$(function () {
    //functions
    var displayProfessorDetails = {},
    fetchProfessorDetails       = {},
    init                        = {},
    inviteProfessor             = {},
    getSearchQuery              = {};

    /**
    * Fetches the professor json
    * @param name string The name being searched for
    * @param callback function The function that displays html data
    *
    * @author Archit Saxena
    */
    fetchProfessorDetails = function (query, callback) {
        var req = {},
        res     = {};

        req.action = "fetch-professor-details";
        req.query   = query;
        $.ajax({
            'type'  : 'post',
            'url'   : ApiEndpoint,
            'data'  : JSON.stringify(req)
        }).done(function (res) {
            res =  $.parseJSON(res);
            userId = res.userId;
            if (typeof callback === 'function') {
                // call displayProfessorDetails
                callback(res, inviteProfessor);
            }
        });
    };

    /**
    * Create required html and add it in the page
    * @param professors Array contains array of professors returned by calling function
    *
    * @author Archit Saxena
    */
    displayProfessorDetails = function (professors, callback) {
        var html = "",
        flag = false,
        count = 0,
        userDetails,
        invitedStatus = false,
        notFound = "No such entries found!";

        userDetails = professors.userDetails;
        if (userDetails) {
            userDetails.forEach(function (professor) {
                invitedStatus = (professor.invitedStatus !== null) ? true: false;
                if(flag == false) {
                    flag = true;
                }
                if(professor)
                {
                    html += '<div class="row" style="margin-left:0px;margin-right:0px"><div class=" pull-left custom-img"><aside><div class="post-info"><span class="arrow-pro right"></span><div class="panel-body custom_prof"><h1><a href="../instructor/' + professor.id + '" target="_blank"><strong style="text-transform: none;">' + professor.firstName + " " + professor.lastName + '</strong></a>';
                    html += '</h1>E-Mail: ' + professor.email + '<br>Country: ' + professor.addressCountry + '<br><br>';
                    if (!invitedStatus || professor.instituteId != userId) {
                        html += '<div class="post-btn"><button onclick="window.location = \'../instructor/' + professor.id + '\'" class="btn btn-primary">Profile</button>&emsp;<button class="btn btn-info send-invitation-btn" type="button" id="' + professor.profId + '">Send Invitation</button></div>';
                    }
                    else {
                        html += '<div class="post-btn"><button onclick="window.location = \'../instructor/' + professor.id + '\'" class="btn btn-primary">Profile</button>&emsp;<button class="btn btn-info invited" type="button">Invited</button></div>';
                    }
                    html += '</div></div></aside></div>';
                    html += '<div class="pull-left custom-right-img"><aside class="post-highlight yellow v-align"><a href="../instructor/' + professor.id + '" target="_blank"><div class="panel-body text-center"><div class="pro-thumb"><img src="' + professor.profilePic + '" alt="Profile Avtar"></div></div></a></aside></div></div><hr>';
                }
            });
        }
        if(flag == true) {
            $('#invite-professor-table').html(html);
            if (typeof callback === 'function') {
                // call inviteProfessor
                callback();
            }
        }
        else {
            $('#invite-professor-table').html(notFound);
        }
    };

    /**
    * Invite professor
    *
    * @author Archit Saxena
    */
    inviteProfessor = function () {
        $('.send-invitation-btn').click(function () {
            var profId = $(this).attr("id"),
            invited;
            var req = {},
            res     = {};

            // if professor already invited, return
            invited = $(this).hasClass('invited');
            if(invited) {
                return;
            }

            //send invitation request
            req.action = "invite-professor";
            req.profId   = profId;
            $.ajax({
                'type'  : 'post',
                'url'   : ApiEndpoint,
                'data'  : JSON.stringify(req)
            }).done(function (res) {
                res =  $.parseJSON(res);
                if (res.status === 1) {
                     $('#'+profId).text(res.message);
					 $('#'+profId).addClass('invited');
                    //$('.send-invitation-btn').addClass('invited');
					//$('.send-invitation-btn').text('invited');
                }
				
            });
        });
    };

    /**
    * Create the query in required form
    * @param string searchQuery The string entered by user
    * @param int type The filter selected by user
    * @returns Object The searched keywords
    */
    getSearchQuery = function () {
        var query = {};
        query.type = parseInt($('#filter-drpDwn :selected').attr("val"));
        query.text = $('#search-professor').val().trim();
        return query;
    };

    /**
    * Initiates the execution process
    *
    * @author Archit Saxena
    */
    init = function () {
        var searchQuery,
        name = {};
        // on query entered in search bar 
        $('#search-professor').on("keypress", function (event) {
            if (event.which === 13) {
                searchQuery = getSearchQuery();
                if (searchQuery.text !== "") {
                    fetchProfessorDetails(searchQuery, displayProfessorDetails);
                }
            };
        });
        $('#search-button').on('click', function() {
            searchQuery = getSearchQuery();
            if (searchQuery.text !== "") {
                fetchProfessorDetails(searchQuery, displayProfessorDetails);
            }
        });
    };

    //initiate function calls
    init();
});