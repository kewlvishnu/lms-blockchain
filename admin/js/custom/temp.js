/*
	Common JS scripts

*/

function formatContactNumber(user) {
	var num1= "", num2 = "";
	if(user.contactMobilePrefix != '' && user.contactMobile != '') {
		num1 = user.contactMobilePrefix + '-' + user.contactMobile;
	}
	
	if(user.contactLandlinePrefix != '' && user.contactLandline != '') {
		num2 = user.contactLandlinePrefix + '-' + user.contactLandline;
	}
	
	return (num1 + ', ' + num2).replace(/,\s$/,'').replace(/^,\s/,'');
}