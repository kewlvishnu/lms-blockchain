/**
* Shows applicable licenses for the course and enables adding or deleting licenses
*/
    var displayLicenses = {},
    fetchLicenses       = {},
    initLicensing       = {},
    licensesList        = [
        'Own Course', 
        'One Year Licensing', 
        'One Time Transfer', 
        'Select IP Transfer', 
        'Commition based licensing'
    ],
    licensesEnum        = {
        'ONE_YEAR'        : 1,
        'ONE_TIME'        : 2,
        'SELECTIVE_IP'    : 3,
        'COMISSION_BASED' : 4
    },
    licenseHandler      = {},
    changeLicenses      = {},
    licenseFormValid = {},
    taxRates = {},
    calculateTaxes = {};
	var licenses;

    /**
    * Fetches the Licenses json
    * @param courseId int The courseId being modified
    * @param callback function The function that displays html data
    *
    * @author Archit Saxena
    */
    fetchLicenses = function (callback) {
        var req = {},
        res     = {};
        req.courseId = getUrlParameter('courseId');
        req.action = "fetch-applicable-licenses";
        $.ajax({
            'type'  : 'post',
            'url'   : ApiEndpoint,
            'data'  : JSON.stringify(req)
        }).done(function (res) {
            res =  $.parseJSON(res);
            //console.log(res);
            if (typeof callback === 'function') {
                // call displayProfessorDetails
				licenses = res;
                callback(res, licenseHandler);
            }
        });
    };
    /**
    * Create required html and add it in the page
    * @param professors Array contains array of professors returned by calling function
    *
    * @author Archit Saxena
    */
    displayLicenses = function (res, callback) {
        var html = "",
			licenseType,
			flag = false,
			i,
			licenseApplicable,
			licenseSaved,
			$elem,
			priceBreakdownINR,
			priceBreakdownUSD;
        //console.log("----------------------------------");
        //console.log(res);
        if(res.status === 0) {
			html += res.message;
			$('#licenses').html(html);
			return;
		}
		if(res.applicableLicenses.length === 0){
			$('.content-marketplace-options').hide();
		}
		taxRates = res.taxes;
		html += 'Apply Licensing';
		for(i = 1; i <= 4; i++) {
			html = '';
			licenseApplicable = false;
			licenseSaved = false;
			license = res.savedLicenses[i];
			if($.inArray(i, res.applicableLicenses) > -1)
				licenseApplicable = true;
			if(typeof res.savedLicenses[i] !== 'undefined')
				licenseSaved = true;
			if(licenseApplicable || licenseSaved) {
				flag = true;
				html += '<div class="license-box" data-license-type="' + i + '"><div class="form-group"><div class="col-lg-12"><div class="checkbox license-checkbox"><label><input type="checkbox"><span class="license-title"></span></label></div></div></div>';
				html += '<div class="license-container" ><div class="form-group"><label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">'
							+ 'Price:</label><div class="col-lg-5"><input type="text" class="form-control price-inr" id="Text2" placeholder=" INR" ></div><div class="col-lg-5"><input type="text" class="form-control price-usd" id="Text8" placeholder=" USD"></div></div>';
				if(i === licensesEnum.COMISSION_BASED) {
					html += '<div class="form-group"><label for="inputEmail1" class="col-lg-3 col-sm-3 control-label">Commission:</label><div class="col-lg-3 col-sm-3">'
						+ '<input type="text" class="form-control comm-amt" id="Text17" placeholder=" " ></div><div class="col-lg-2 col-sm-2">or</div><div class="col-lg-3"><input type="text" class="form-control comm-per" id="Text18" placeholder=" " >' 
						+ '</div><div class="col-lg-1 col-sm-1">%</div></div>';
				}
				html += '<div class="form-group price-breakdown hidden-l"><label for="inputEmail1" class="col-lg-9 col-sm-9 control-label">Your Amount <span class="your-price"></span></label><div class="col-lg-3"><a href="#" data-details-shown="0" class="show-price-breakdown">View Details</a></div></div>';
				html += '<div class="price-breakdown-details hidden-l"></div>';
				html += '</div></div>';
				$elem = $(html);
				$elem.find('.license-checkbox span.license-title').text(licensesList[i]);
				if(licenseSaved) {
					$elem.find('input[type="checkbox"]').attr('id',license.licenseId);
					$elem.find('.price-inr').val(license.priceINR || 0);
					$elem.find('.price-usd').val(license.priceUSD || 0);
					$elem.find('.comm-amt').val(license.comissionAmt || 0);
					$elem.find('.comm-per').val(license.comissionPercent || 0);
					showPriceBreakDown($elem);
				}
				if(!licenseApplicable)
				$elem.addClass('not-applicable').attr('title', 'This license is no longer applicable. Maybe you included a subject that invalidated it.');
				$('#licenses').append($elem);
			}
		}
		html = '<div class="form-group col-lg-12"><a class="btn btn-xs btn-success " id="save-licenses"> Save Licenses </a></div>';
        $('#licenses').append(html);
		//adding listener for decimal inputs only
		//for allowing decimal number
		$('#licenses input[type="text"]').keypress(function (event) {
			return allowDecimal(event, $(this));
		});
		function allowDecimal(event, obj) {
			if ((event.which != 46 || obj.val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) && event.which != 8) {
				return false;
			}
			var text = obj.val();
			if ((text.indexOf('.') != -1) && (text.substring(text.indexOf('.')).length > 2)) {
				if (event.which != 8 || event.which != 46)
					return false;
			}
			return true;	
		}
		if(typeof callback === 'function' && flag === true) {
            // call event Handler
            callback(res.savedLicenses);
        }else{
			$('#licenses').append('No licenses applicable');
	}
    };
    licenseHandler = function (licenses) {
        var modify = [];
        $('.show-price-breakdown').on("click", function () {
            $(this).parents('.license-box').find('.price-breakdown-details').toggle();
            if($(this).attr('data-details-shown') == '0')
				$(this).text('Hide').attr('data-details-shown', 1);
			else
				$(this).text('View Details').attr('data-details-shown', 0);
        });
        
        $('.license-checkbox input[type="checkbox"]').on("click", function () {
            $(this).parents('.license-box').find('.license-container').toggle();
        });
        $.each(licenses, function (licenseType, license) {
            $('#'+license.licenseId).prop('checked', true);
        });
        $('.license-checkbox input[type="checkbox"]').each(function (elem) {
            if($(this).prop('checked') === false) {
                $(this).parents('.license-box').find('.license-container').hide();
            }
        });
        $('.license-checkbox input[type="checkbox"]').on("change", function () {
            ($(this).attr("data-changed")) ? $(this).removeAttr("data-changed") : $(this).attr("data-changed", "true");
        });
        $('.license-container input[type=text]').on("change", function() {
            $(this).parents('.license-box').find('.license-checkbox input[type=checkbox]').attr("data-updated", "true");
            showPriceBreakDown($(this).parents('.license-box')); 
        });
        $('.license-container input[type=text]').on("keyup", function() {
			showPriceBreakDown($(this).parents('.license-box')); 
        });
        $('#save-licenses').on("click", function () {
			//validating the form
			var flag = false;
			$('.license-box').each(function() {
				if($(this).find('input[type="checkbox"]').prop('checked')) {
					var inr = $(this).find('input[type="text"].price-inr').val();
					unsetError($(this).find('input[type="text"].price-inr'));
					if(inr == '') {
						setError($(this).find('input[type="text"].price-inr'), 'Please fill the INR value.');
						flag = true;
					}
					inr = parseFloat(inr);
					if(inr > 100000) {
						setError($(this).find('input[type="text"].price-inr'), 'Please fill a value less than 1 lakh');
						flag = true;
					}
					var usd = $(this).find('input[type="text"].price-usd').val();
					unsetError($(this).find('input[type="text"].price-usd'));
					if(usd == '') {
						setError($(this).find('input[type="text"].price-usd'), 'Please fill the USD value.');
						flag = true;
					}
					usd = parseFloat(usd);
					if(usd > 100000) {
						setError($(this).find('input[type="text"].price-usd'), 'Please fill a value less than 1 lakh');
						flag = true;
					}
				}
			});
			if(flag)
				return;
            var modify = {};
            modify.insert = [];
            modify.delete = [];
            modify.update = [];
            $('.license-checkbox input[type="checkbox"]').each(function () {
                //console.log("==============");
                //console.log($(this));
                if($(this).attr('data-changed') == 'true') {
                    if ($(this).prop('checked') === true && typeof $(this).attr('id') == 'undefined') {
						if(!licenseFormValid($(this).parents('.license-box'))) {
							alert('Well! You didn\'t think this would work. Did you?');
							return;
						}
                        modify.insert.push({
							'type' : $(this).parents('.license-box').attr('data-license-type'),
							'INR' : $(this).parents('.license-box').find('.price-inr').val(),
							'USD' : $(this).parents('.license-box').find('.price-usd').val(),
							'comissionAmt' : ($(this).parents('.license-box').find('.comm-amt')) ? ($(this).parents('.license-box').find('.comm-amt').val()) : undefined,
							'comissionPercent' : ($(this).parents('.license-box').find('.comm-per')) ? ($(this).parents('.license-box').find('.comm-per').val()) : undefined
						});
                    }
                    else {
                        modify.delete.push($(this).attr('id'));
                    }
                }
                if($(this).attr('data-updated') == 'true' && typeof $(this).attr('id') != 'undefined') {
                    modify.update.push({
                        'id' : $(this).attr('id'),
                        'INR' : $(this).parents('.license-box').find('.price-inr').val(),
                        'USD' : $(this).parents('.license-box').find('.price-usd').val(),
                        'comissionAmt' : ($(this).parents('.license-box').find('.comm-amt')) ? ($(this).parents('.license-box').find('.comm-amt').val()) : undefined,
                        'comissionPercent' : ($(this).parents('.license-box').find('.comm-per')) ? ($(this).parents('.license-box').find('.comm-per').val()) : undefined
                    });
                }
                if($(this).attr('data-updated') == 'true' && typeof $(this).attr('id') == 'undefined') {
					
				}
            });
            // console.log(modify);
            changeLicenses(modify);
            
        });
        
            
   $('#save-rate-student').on("click", function () {
		//validations for student rates
		/*unsetError($('#availStudentMarket'));
		if(!$('#availStudentMarket').prop('checked')) {
			setError($('#availStudentMarket'), 'You must enable this option');
			return;
		}*/
		unsetError($('#student-price-inr'));
		var inrRate = $('#student-price-inr').val();
		if(inrRate == '') {
			setError($('#student-price-inr'), 'Please fill the INR value.');
			return;
		}
		inrRate = parseFloat(inrRate);
		if(inrRate < 0 || inrRate > 100000) {
			setError($('#student-price-inr'), 'Please select a value less than 1 lakh.');
			return;
		}
		unsetError($('#student-price-dollar'));
		var usdRate = $('#student-price-dollar').val();
		if(usdRate == '') {
			setError($('#student-price-dollar'), 'Please fill the USD value.');
			return;
		}
		usdRate = parseFloat(usdRate);
		if(usdRate < 0 || usdRate > 100000) {
			setError($('#student-price-dollar'), 'Please select a value less than 1 lakh.');
			return;
		}
        var req = {},
        res     = {};
        req.courseId = getUrlParameter('courseId');
        req.action = "save-course-rate-student";
        req.studentPrice=$('#student-price-dollar').val();
        req.studentPriceINR=$('#student-price-inr').val();
        //req.availStudentMarket=$('#availStudentMarket').prop("checked")?1:0;
        req.availStudentMarket=1;
        $.ajax({
            'type'  : 'post',
            'url'   : ApiEndpoint,
            'data'  : JSON.stringify(req)
        }).done(function (res) {
			//console.log(res);
            res =  $.parseJSON(res);
            if(res.status == 1)
                    //$('#market-place-modal').modal('hide');
					alertMsg("Rates saved successfully.");
			else
			alert(res.message);
        });
    });
    
    
    $('#live-course-student').on("click", function () {
        var req = {},
        res     = {};
        req.courseId = getUrlParameter('courseId');
        req.action = "live-course-student";
		//req.ignoreWarning = false;
        req.liveForStudent = 1;
		makeCourseLiveOnStudentMarketPlace(req);
    });
	
      $('#liveForContentMarket').on("click", function () {
        var req = {},
        res     = {};
        req.courseId = getUrlParameter('courseId');
        req.action = "live-course-content-market";
        req.liveForContentMarket = 1;
		//req.ignoreWarning = false;
        makeCourseLiveOnContentMarketPlace(req);
    });
	
	//unlive handler
	$('#unlive-course-student').on("click", function () {
        var req = {},
        res     = {};
        req.courseId = getUrlParameter('courseId');
        req.action = "unlive-course-student";
         $.ajax({
            'type'  : 'post',
            'url'   : ApiEndpoint,
            'data'  : JSON.stringify(req)
        }).done(function (res) {
			//console.log(res);
            res =  $.parseJSON(res);
            if(res.status == 1) {
				//$('#market-place-modal').modal('hide');
				alertMsg("Your Course removed from Student Market Place.");
				$('#unlive-course-student').parents('span:eq(0)').hide();
				$('#live-course-student').show();
				$('#live-course-student').attr('disabled', true);
				$('#availStudentMarket').prop('checked', false);
			}
			else
				alert(res.message);
        });
    });
      $('#unliveForContentMarket').on("click", function () {
        var req = {},
        res     = {};
        req.courseId = getUrlParameter('courseId');
        req.action = "unlive-course-content-market";
         $.ajax({
            'type'  : 'post',
            'url'   : ApiEndpoint,
            'data'  : JSON.stringify(req)
        }).done(function (res) {
			//console.log(res);
            res =  $.parseJSON(res);
            if(res.status == 1) {
				//$('#market-place-modal').modal('hide');
				alertMsg("Your Course is removed from Content Market Place.");
				$('#unliveForContentMarket').parents('span:eq(0)').hide();
				$('#liveForContentMarket').show();
				$('#liveForContentMarket').attr('disabled', true);
				$('#availContentMarket').prop('checked', false);
			}
			else
				alert(res.message);
        });
    });
  };

    changeLicenses = function (modify) {
        var req = {},
        res     = {};
        req.courseId = getUrlParameter('courseId');
        req.action = "modify-licenses";
        //req.availContentMarket=$('#availContentMarket').prop('checked')?1:0;
        req.availContentMarket=1;
        req.modify = modify;
        $.ajax({
            'type'  : 'post',
            'url'   : ApiEndpoint,
            'data'  : JSON.stringify(req)
        }).done(function (res) {
			//console.log(res);
            res =  $.parseJSON(res);
            if(res.status == 1)
				//$('#market-place-modal').modal('hide');
				alertMsg('Licenses Saved successfully.');
			else
				alert(res.message);
        });
    };
    licenseFormValid = function($licenseDiv) {
		var licenseType = $(this).parents('.license-box').attr('data-license-type');
		if(licenseType != '4') {
			var priceInr = $licenseDiv.find('.price-inr').val(),
				priceUSD = $licenseDiv.find('.price-usd').val();
			if( priceInr == '' || priceUSD == '' ) {
				return false;
			}
		} else {
			var priceInr = $licenseDiv.find('.price-inr').val(),
				priceUSD = $licenseDiv.find('.price-usd').val(),
				commAmt = $licenseDiv.find('.comm-amt').val(),
				commPer = $licenseDiv.find('.price-usd').val();
			if( priceInr == '' || priceUSD == '' || commAmt == '' || commPer == '') {
				return false;
			}	
		}
		return true;
	}
	
	calculateTaxes = function(price) {
		if(price == '')
			price = 0;
		price = parseFloat(price);
		var priceBreakdown = {},
			taxAmts = {},
			totalTax = 0;
		$.each(taxRates, function(taxId, taxDetails){
			if(taxDetails.value == 0)
				return;
			taxAmts[taxDetails.taxname] = price * parseFloat(taxDetails.value) / 100;
			
			totalTax += taxAmts[taxDetails.taxname];
		});
		priceBreakdown['totalTax'] = totalTax;
		priceBreakdown['taxAmts'] = taxAmts;
		priceBreakdown['yourPrice'] = price - totalTax;
		return priceBreakdown;
	}
	
	showPriceBreakDown = function($elem) {
		priceBreakdownINR = calculateTaxes($elem.find('.price-inr').val());
		priceBreakdownUSD = calculateTaxes($elem.find('.price-usd').val());
		$elem.find('.your-price').html( priceBreakdownINR.yourPrice.toFixed(2) + ' INR, ' + priceBreakdownUSD.yourPrice.toFixed(2) + '$' );
		$elem.find('.price-breakdown').show();
		
		var html = '<div><h2>Price INR</h2>';
					+ '<h3>Your Price  : ' + priceBreakdownINR.yourPrice.toFixed(2) + ' INR</h3>';
					+ '<h3>Total Tax : ' + priceBreakdownINR.totalTax.toFixed(2) + ' INR</h3>';
		$.each(priceBreakdownINR.taxAmts, function(taxName, taxAmt) {
			html += '<div><span>' + taxName + ' : </span><span>' + taxAmt.toFixed(2) + ' INR</span></div>'
		});
		html += '</div>';
		html += '<div><h2>Price USD</h2>';
					+ '<h3>Your Price  : ' + priceBreakdownUSD.yourPrice.toFixed(2) + '$</h3>';
					+ '<h3>Total Tax : ' + priceBreakdownUSD.totalTax.toFixed(2) + '$</h3>';
		$.each(priceBreakdownUSD.taxAmts, function(taxName, taxAmt) {
			html += '<div><span>' + taxName + ' : </span><span>' + taxAmt.toFixed(2) + '$</span></div>'
		});
		html += '</div>';
		$elem.find('.price-breakdown-details').html(html);
	}
	
    /**
    * Initiates the execution process
    *
    * @author Archit Saxena
    */
    initLicensing = function () {
        /* --------------------------- event handlers ---------------------------- //
        $(document).ajaxStart(function() {
            $('#loader_outer').show();
        });
        $(document).ajaxComplete(function() {
            $('#loader_outer').hide();
        });*/

        fetchLicenses(displayLicenses);
    };

    //initiate function calls
    initLicensing();

	function makeCourseLiveOnStudentMarketPlace(req) {
		$.ajax({
            'type'  : 'post',
            'url'   : ApiEndpoint,
            'data'  : JSON.stringify(req)
        }).done(function (res) {
			//console.log(res);
            res =  $.parseJSON(res);
            if(res.status == 1) {
				//$('#market-place-modal').modal('hide');
				alertMsg("Your Course is live on Student Market Place.");
				$('#unlive-course-student').parents('span:eq(0)').show();
				$('#live-course-student').hide();
			}
			else {
				/*if(res.warning == true) {
					con = confirm(res.message);
					if(con) {
						req.ignoreWarning = true;
						makeCourseLiveOnStudentMarketPlace(req);
					}
				}
				else*/
					$('#serror').text(res.message).show();
			}
        });
	}
	
	function makeCourseLiveOnContentMarketPlace(req) {
		$.ajax({
            'type'  : 'post',
            'url'   : ApiEndpoint,
            'data'  : JSON.stringify(req)
        }).done(function (res) {
			//console.log(res);
            res =  $.parseJSON(res);
            if(res.status == 1) {
				//$('#market-place-modal').modal('hide');
				alertMsg("Your Course is live on Content Market Place.");
				$('#unliveForContentMarket').parents('span:eq(0)').show();
				$('#liveForContentMarket').hide();
			}
			else {
				/*if(res.warning == true) {
					con = confirm(res.message);
					if(con) {
						req.ignoreWarning = true;
						makeCourseLiveOnContentMarketPlace(req);
					}
				}
				else*/
					$('#cerror').text(res.message).show();
			}
        });
	}
