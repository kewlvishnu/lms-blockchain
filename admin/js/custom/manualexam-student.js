$(function() {
	fetchBreadcrumb();
	function fetchBreadcrumb() {
		var req = {};
		var res;
		req.action = 'get-breadcrumb-for-manual-exam';
		req.examId = getUrlParameter('examId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				console.log(res);
				fillBreadcrumb(res);
			}
		});
	}
	
	function fillBreadcrumb(data) {
		$('ul.breadcrumb li:eq(0)').find('a').attr('href', 'edit-course.php?courseId=' + data.course.courseId).text(data.course.courseName);
		$('ul.breadcrumb li:eq(1)').find('a').attr('href', 'edit-subject.php?courseId=' + data.course.courseId + '&subjectId=' + data.subject.subjectId).text(data.subject.subjectName);
		$('ul.breadcrumb li:eq(2)').find('a').attr('href', 'manualexam.php?courseId=' + data.course.courseId + '&subjectId=' + data.subject.subjectId + '&examId=' + data.exam.examId).text(data.exam.examName);
	}
	
	function setError(where, what) {
		unsetError(where);
		where.parent().addClass('has-error');
		where.parent().append('<span class="help-block">'+what+'</span>');
	}
	
	function unsetError(where) {
		if(where.parent().hasClass('has-error')) {
			where.parent().find('.help-block').remove();
			where.parent().removeClass('has-error');
		}
	}
});