var ApiEndPoint = '../api/index.php';

$(function() {
	//fetchProfileDetails();
	fetchExamResult();
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	//function to fetch subject exam attempt status
	function fetchExamResult() {
		var req = {};
		var res;
		req.action = 'get-subject-attempts';
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message)
			else {
				var data = [];
				var html = '';
				for(var i = 0; i < res.exams.length; i++) {
					var obj = {};
					obj.device = res.exams[i].name;
					obj.geekbench = res.exams[i].count;
					data.push(obj);
					html += '<tr>'
							+ '<td>' + res.exams[i].name + '</td>'
							+ '<td>' + res.exams[i].count + '</td>'
							+ '<td>' + (res.totalStudents - res.exams[i].count) + '</td>'
						+ '</tr>';
				}
				$('#attemptTable tbody').append(html);
				$('#attemptTable').show();
				new Morris.Bar({
					element: 'hero-bar',
					data: data,
					xkey: 'device',
					ykeys: ['geekbench'],
					labels: ['Total Attempts'],
					barRatio: 0.4,
					xLabelAngle: 35,
					hideHover: 'auto',
					barColors: ['#D8574C']
				});
			}
		});
	}
});