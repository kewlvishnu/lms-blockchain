/*
	Author: Fraaz Hashmi.
	Dated: 7/8/2014
	
	
	********
	1) Notes: JSON.stringify support for IE < 10
*/

"use strict";
var headingCount = 1;
var pageLoadRendering = true;
var chapterId = 1;
var chapterNumber = 1;
var reorderTimer = null;
var reloadFlag=false;
var chapterWeight=0;
$(function () {
    
    //    this function is for preventing refresh the page for complete upload of content
       $(window).bind('beforeunload', function () {
		if (reloadFlag)
			return "Your File is still being uploaded. If you leave the page, changes will be lost.";
		});
	var imageRoot = '/';
	$('#loader_outer').hide();
	
	fetchBreadcrumb();
	
	//to add BCT dynamically
	function fetchBreadcrumb() {
		var req = {};
		var res;
		req.action = 'get-breadcrumb-for-add';
		req.subjectId = getUrlParameter('subjectId');
		req.courseId = getUrlParameter('courseId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alertMsg(res.message);
			else
				fillBreadcrumb(res);
		});
	}
	
	function fillBreadcrumb(data) {
		$('ul.breadcrumb li:eq(0)').find('a').attr('href', 'edit-course.php?courseId=' + data.courseId).text(data.courseName);
		$('ul.breadcrumb li:eq(1)').find('a').attr('href', 'edit-subject.php?courseId=' + data.courseId + '&subjectId=' + data.subjectId).text(data.subjectName);
	}
	
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1)
				fillProfileDetails(res);
			else
				alertMsg(res.message);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fetchCourseDetails() {
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action = "get-course-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1)
				fillCourseDetails(res);
			else
				alertMsg(res.message);
		});
	}
	
	function fillCourseDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		if(data.courseDetails.image == '') {
			var image = defaultImagePath;
		} else {
			var image = data.courseDetails.image;
		}
		$('#course-image-preview').attr('src', image + "?" + new Date().getTime());
	}
	
	function fetchSubjectDetails(data) {
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action = "get-subject-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1)
				fillSubjectDetails(res);
			else
				alertMsg(res.message);
		});
	}
	
	function fillSubjectDetails(data) {
		if(data.subjectDetails.deleted == 1)
			alertMsg('This subject has been deleted!!', -1);
		var courseId = getUrlParameter('courseId');
		var subjectId = getUrlParameter('subjectId');
		// $('#subject-name-title').html(data.subjectDetails.name);
		$('#subject-name').html(data.subjectDetails.name);
		// $('#edit-form #subject-name').val(data.subjectDetails.name);
		
		// $('#subject-id').html('S' + String("000" + data.subjectDetails.id).slice(-4));
		
		// $('#subject-desc').html(data.subjectDetails.description);
		// $('#edit-form #subject-desc').val(data.subjectDetails.description);
		// $('.add-chapter-link').attr('href', 'add-chapter.php?courseId=' + courseId +'&subjectId=' + subjectId);
		
		var imageRoot = 'user-data/images/';
		if(data.subjectDetails.image == '') {
			var image = defaultImagePath;
		} else {
			var image = data.subjectDetails.image;
		}
		
		$('#subject-image-preview').attr('src', image + "?" + new Date().getTime());
		$('#edit-form #subject-image-preview').attr('src', image + "?" + new Date().getTime());
	}

	function fetchChapterDetails(data) {
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		req.chapterId = getUrlParameter('chapterId');
		
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action = "get-chapter-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1)
				fillChapterDetails(res);
			else
				alertMsg(res.message);
		});
	}
	
	function fillChapterDetails(data) {
		chapterWeight=parseInt(data.tempChapterDetails.weight) + 1;
		$('#chapter-name').html('Chapter ' + chapterWeight + ' : ' + data.chapterDetails.name);
                if(data.tempChapterDetails.description!=''){
                      $('#chapter-description').html(data.tempChapterDetails.description.replace(new RegExp('\r?\n','g'), '<br />')); 
                }else{
                    $('#chapter-description').prev().hide();
                    $('#chapter-description').html(data.tempChapterDetails.description); 
                }
		$('#edit-chapter-name').val(data.chapterDetails.name);
        $('#edit-chapter-description').val(data.tempChapterDetails.description);
        if(data.tempChapterDetails.demo == 1)
        { 
            $('#demochapter').remove();
            $("#is-demo").prop("checked", true);
            $('#chapter-description').after('<p id=demochapter>This chapter is allowed for free demo. </p>'); 
        } else{
            $("#is-demo").prop("checked", false);
            $('#demochapter').remove();
        }
         //data.tempChapterDetails.demo ? $("#is-demo").prop("checked",true):$("#is-demo").prop("checked",false);
          
        chapterNumber = (parseInt(data.tempChapterDetails.weight) + 1);
        if(getUrlParameter('edit') != undefined && getUrlParameter('edit') == 1)
        	$('.edit-button').click();
		fetchContent();
	}
	fetchContent();
	function fetchContent() {
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		req.chapterId = getUrlParameter('chapterId');
		//chapterId = getUrlParameter('chapterId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		if(req.subjectId == "" || req.subjectId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action = "get-chapter-content";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1)
				initializePanel(res);
			else
				alertMsg(res.message);
		});
	}
	
	function fillContent(data) {
		
	}
	
	function showAddHeadingLink() {
		$('.add-heading').show();
		$('.add-content').show();
	}
	
	function hideAddHeadingLink() {
		$('.add-heading').hide();
		//$('.add-content').hide();
	}
	
	function showAddContentLink() {
		$('.add-content').show();
	}
	
	function hideAddContentLink() {
		$('.add-content').hide();
	}
		
	function setContentWidgetHandles() {
		
	}
	
	function addNewHeading(heading) {
		headingCount ++;
		var headingNum = headingCount;
		headingNum = 'Section ' + headingNum + ':';
		if (typeof heading == "undefined") {
			var heading = {};
			heading.id = "0";
			heading.name = "";
			heading.hideEdit = false;
		} else {
			heading.hideEdit = true;
		}
		var checked = '';
		if(heading.demo == 1)
			checked = 'CHECKED';
		var html = '<div class="section-heading sortable-item temp" data-hid="' + heading.id + '">'
						+ '<div class="view-pane hidden-l">'
							+ '<div class="panel-heading">'
								+ '<label class="heading-number">' + headingNum + ': </label> <span class="heading-title">' + heading.name + '</span>'
								+ '&nbsp;&nbsp;<a href="#" class="edit-heading"><i class="fa fa-pencil"></i></a>'
								+ '<button class="btn btn-xs btn-warning add-content pull-right" style="margin-top:0px;"><i class="fa fa-plus"></i> Add Lecture</button>'
							+ '</div>'
						+ '</div>'
						+ '<div class="edit-pane">'
							+ '<div class="row panel-body">'
								+ '<div class="form-group">'
									+ '<div class="col-lg-2">'
										+ '<label class="heading-number">' + headingNum + ': </label>'
									+ '</div>'
									+ '<div class="col-lg-10">'
										+ '<input type="text" class="form-control heading-title-input" placeholder="Title" value="' + heading.name + '" maxlength="60"><br>'
										+ 'Note : After you click on <button class="btn btn-info" DISABLED>Save</button> please click on <button class="btn btn-xs btn-warning" DISABLED><i class="fa fa-plus"></i> Add Lecture</button>. You can add multiple Lecctures'
									+ '</div>'
								+ '</div>'
							+ '</div>'
							+ '<div class="row panel-body">'
								+ '<div class="col-md-6"><a class="btn btn-info save-title" href="#">Save</a> Or <a href="#" class="cancel-edit">Cancel</a></div>'
								+ '<div class="col-md-6" style="text-align: right;">'
									+ '<a href="#" class="delete-heading">'
										+ '<i class="fa fa-trash-o"></i>'
									+ '</a>'
								+ '</div>'
							+ '</div>'
						+ '</div>'
					+ '</div>';
		var elem = $(html);
		headingEventHandlers(elem);
		if( heading.hideEdit == true ) {
			elem.removeClass('temp');
			elem.find('.edit-pane').hide();
			elem.find('.view-pane').show();
		}
		$('#content .headings').append(elem);
		
		reNumberEverything(false);
	}
	
	function getLastHeadingContentCount() {
		return 0;
		return $('.section-heading').last().find('.section-content').length;
	}
	
	function addNewContent(content, heading) {
		var headingNum = headingCount;
		var contentCount = getLastHeadingContentCount() + 1;
		
		var contentNum = 'Content ' + headingNum  + '.' + contentCount;
		if (typeof content == "undefined" || content == null) {
			var content = {};
			content.id = "0";
			content.title = "";
			content.hideEdit = false;
		} else {
			content.hideEdit = true;
		}
		var checked = parseInt(content.demo) ? 'CHECKED' : '';
		var html = '<div class="section-content indented temp sortable-item" data-conid="' + content.id + '">'
						+ '<div class="view-pane hidden-l">'
							+ '<div class="panel-heading orange">'
								+ '<label class="content-number">' + contentNum + '</label> <span class="content-title">' + content.title + '</span>'
								+ '&nbsp;&nbsp;<a href="#" class="edit-content"><i class="fa fa-pencil"></i></a>'
								+ '<button class="btn btn-xs btn-warning add-stuff pull-right" style="margin-top:0px;"><i class="fa fa-plus"></i> Add Content</button>'
							+ '</div>'
						+ '</div>'
						+ '<div class="edit-pane">'
							+ '<div class="row panel-body">'
								+ '<div class="form-group">'
									+ '<div class="col-lg-2">'
										+ '<label class="content-number">' + contentNum + '</label>'
									+ '</div>'
									+ '<div class="col-lg-10">'
										+ '<input type="text" class="form-control content-title-input" placeholder="Lecture name" value="' + content.title + '" maxlength="60"/><br>'
										+ '<label><input type="checkbox" class="demo" ' + checked + '> Free Demo</label><br>'
										+ 'Note : You can choose the content type after clicking <button class="btn btn-info" DISABLED>Save</button>'
									+ '</div>'
								+ '</div>'
							+ '</div>'
							+ '<div class="row panel-body">'
								+ '<div class="col-md-6"><a class="btn btn-info save-title" href="#">Save</a> Or <a href="#" class="cancel-edit">Cancel</a></div>'
								+ '<div class="col-md-6" style="text-align: right;">'
									+ '<a href="#" class="delete-content">'
										+ '<i class="fa fa-trash-o"></i>'
									+ '</a>'
								+ '</div>'
							+ '</div>'
						+ '</div>';
		html += '</div>';

		var elem = $(html);
		contentEventHandlers(elem);
		if( content.hideEdit == true ) {
			elem.removeClass('temp');
			elem.find('.edit-pane').hide();
			elem.find('.view-pane').show();
		}
		if(typeof content.stuff != "undefined" && content.stuff != null) {
			elem.attr('data-content-type', content.fileType);
			elem.attr('data-file-name', content.fileName);
			elem.attr('data-metadata', content.metadata);
			if(content.fileType == 'text')
				elem.append('<div class="hidden hidden-l text-stuff">' + content.stuff + '</div>');
			else if(content.fileType == 'Youtube' || content.fileType == 'Vimeo')
				elem.append('<div class="hidden hidden-l link-stuff">' + content.stuff + '</div>');
			elem.addClass('has-file');
			elem.find('.add-stuff').hide();
			var stuffDP = createStuffDP(elem);
			//console.log(stuffDP[0].innerHTML);
			if(content.published == '1') {
				stuffDP.find('.change-published').attr('data-published', '1').text('UnPublish');
				stuffDP.find('.change-published').removeClass('btn-warning').addClass('btn-success');
				elem.find('.panel-heading').removeClass('orange');
				stuffDP.find('.downloadable-holder').show();
			}
			
			if(content.downloadable == '1' && content.fileType != 'text') {
				//stuffDP.find('.change-downloadable').attr('data-downloadable', '1').text('Remove Downloadable');
				stuffDP.find('.change-downloadable').attr('data-downloadable', '1').bootstrapSwitch('state', true, true);
			}
			
			elem.append(stuffDP);
			if(content.descText.trim() != '') {
				var stuffDescriptionWidget = createStuffDescriptionWidget(elem);
				var stuffDescDisplay = createStuffDescDisplay(nl2br(content.descText));
				stuffDescriptionWidget.find('.view').html(stuffDescDisplay);
				stuffDescriptionWidget.find('.edit').hide();
				elem.find('.supp-section').prepend(stuffDescriptionWidget);
				stuffDP.find('.add-stuff-desc').hide();
			}
			if(typeof content.supplementaryFiles !== 'undefined' && content.supplementaryFiles !== null) {
				var supplementaryStuffWidget = createSupplementaryStuffWidget(elem, content.supplementaryFiles);
				elem.find('.supp-section').prepend(supplementaryStuffWidget);
			}
		}
		
		if(typeof heading == 'undefined' || heading == null) {
			$('.headings').append(elem);
		} else if(heading.nextAll('.section-heading:eq(0)').length == 0) {
			$('.headings').append(elem);
		} else {
			elem.insertBefore(heading.nextAll('.section-heading:eq(0)'));
		}
		
		if(content.descText != null && content.descText.trim() != '') {
			stuffDescriptionWidget.find('textarea').val(content.descText.trim());
			// CKEDITOR.replace( editorId, {
				// height: 100
			// });
			// CKEDITOR.instances[editorId].setData(content.descText.trim());
		}
		reNumberEverything(false);
	}
	
	function createSupplementaryStuffWidget(elem, suppFiles) {
		var html = '<div class="supplementary-stuff-widget">'
					+ '<b>Supplementary Files</b><br/>';
		$.each(suppFiles, function(i,v) {
			html += createSuppFileDP(v);
		});
		html = html + '</div>';
		var widget = $(html);
		var conid = elem.attr('data-conid');
		
		supplementaryStuffWidgetEventHandlers(widget);
		return widget;
	}
	
	function createSuppFileDP(file) {
		var html = '<div class="supp-file clearfix" data-supp-file="' + file.id + '">'
					+'<span class="supp-file-name">' + file.fileRealName + '</span>'
					+'<a href="#" class="delete-supp-file pull-right"><i class="fa fa-trash-o"></i></a>'
					+ '</div>';
		return html;
	}
	
	function supplementaryStuffWidgetEventHandlers(elem) {
		elem.find('.delete-supp-file').on('click', function(e) {
			e.preventDefault();
			var req = {};
			req.suppFileId = $(this).parents('.supp-file').attr('data-supp-file');
			deleteSuppFileFromDB(req, $(this).parents('.supp-file'));
		});
	}
	
	function headingEventHandlers(elem) {
		elem.find('.delete-heading').off('click');
		elem.find('.delete-heading').on('click', function(e) {
			e.preventDefault();
			if($(this).parents('.section-heading').hasClass('temp'))
				return;
			if($(this).parents('.section-heading').next('.section-content').length > 0){
				alert('The heading has content. Delete them first.')
				return;
			}
			var msg = "Are you sure you want to delete this?";
			if(!confirm(msg))
				return;
			deleteHeadingFromDB($(this).parents('.section-heading'))
			headingCount--;
			reNumberEverything(false);
		});
		elem.find('.edit-heading').off('click');
		elem.find('.edit-heading').on('click', function(e) {
			e.preventDefault();
			$(this).parents('.section-heading').find('.view-pane').hide();
			$(this).parents('.section-heading').find('.edit-pane').show();
		});
		elem.find('.cancel-edit').off('click');
		elem.find('.cancel-edit').on('click', function(e) {
			e.preventDefault();
			if($(this).parents('.section-heading').hasClass('temp')) {
				$(this).parents('.section-heading').remove();
				showAddHeadingLink();
				headingCount--;
			} else {
				$(this).parents('.section-heading').find('.edit-pane').hide();
				$(this).parents('.section-heading').find('.view-pane').show();
			}
		});
		elem.find('.save-title').off('click');
		elem.find('.save-title').on('click', function(e) {
			e.preventDefault();
			var title = elem.find('.heading-title-input').val();
			if(title == '') {
				elem.find('.heading-title-input').addClass('error');
				return;
			}
			$('.heading-title-input').removeClass('error');
			elem.find('.heading-title').text(title);
			if($(this).parents('.section-heading').hasClass('temp')) {
				//Save heading to DB I/U
				showAddHeadingLink();
				$(this).parents('.section-heading').removeClass('temp');
			}
			var req = {};
			req.subjectId = getUrlParameter('subjectId');
			req.title = title;
			req.headingId = $(this).parents('.section-heading').attr('data-hid');
			req.demo = 0;
			if(elem.find('input:checkbox.demo').prop('checked'))
				req.demo = 1;
			saveHeadingToDB(req, $(this).parents('.section-heading'));
			elem.find('.view-pane').show();
			elem.find('.edit-pane').hide();
		});
		
		elem.find('.heading-title-input').off('keyup');
		elem.find('.heading-title-input').on('keyup', function(e){
			if(e.which == 13){
				$(this).parents('.section-heading').find('.save-title').click();
			}
		});
		elem.find('.add-content').off('click');
		elem.find('.add-content').on('click', function() {
		//hideAddContentLink();
			var content = null;
			addNewContent(content, $(this).parents('.section-heading'));
		});
	}
	
	function contentEventHandlers(elem) {
		elem.find('.delete-content').off('click');
		elem.find('.delete-content').on('click', function(e) {
			e.preventDefault();
			if($(this).parents('.section-content').hasClass('temp'))
				return;
			if($(this).parents('.section-content').hasClass('has-file')){
				alert('A file is attached to this content. You will have to delete that first');
				return;
			}
			var msg = "Are you sure you want to delete this?";
			if(!confirm(msg))
				return;
			// Delete from DB
			deleteContentFromDB($(this).parents('.section-content'))
			//$(this).parents('.section-content').remove();
			reNumberEverything(false);
		});
		
		elem.find('.edit-content').off('click');
		elem.find('.edit-content').on('click', function(e) {
			e.preventDefault();
			elem.find('.remove-type-selector').length != 0 ? elem.find('.remove-type-selector').click() : '';
			elem.find('.remove-file-uploader').length != 0 ? elem.find('.remove-file-uploader').click() : '';
			elem.find('.remove-text-widget').length != 0 ? elem.find('.remove-text-widget').click() : '';
			
			$(this).parents('.section-content').find('.view-pane').hide();
			$(this).parents('.section-content').find('.edit-pane').show();
			if(!$(this).parents('.section-content').find('.stuff-dp').hasClass('hidden-l'))
				$(this).parents('.section-content').find('.stuff-dp').addClass('hidden-l');
		});
		
		elem.find('.cancel-edit').off('click');
		elem.find('.cancel-edit').on('click', function(e) {
			e.preventDefault();
			if($(this).parents('.section-content').hasClass('temp')) {
				$(this).parents('.section-content').remove();
				showAddContentLink();
			} else {
				$(this).parents('.section-content').find('.edit-pane').hide();
				$(this).parents('.section-content').find('.view-pane').show();
			}
		});
		
		elem.find('.save-title').off('click');
		elem.find('.save-title').on('click', function(e) {
			var tempFlag = false;
			e.preventDefault();
			var title = elem.find('.content-title-input').val();
			if(title == '') {
				elem.find('.content-title-input').addClass('error');
				return;
			}
			$('.content-title-input').removeClass('error');
			elem.find('.content-title').text(title);
			if($(this).parents('.section-content').hasClass('temp')) {
				showAddContentLink();
				tempFlag = true;
				$(this).parents('.section-content').removeClass('temp');
			}
			var req = {};
			req.chapterId = chapterId;
			var prevHeading = $(this).parents('.section-content').prevAll('.section-heading:first');
			req.demo = ($(this).parents('.edit-pane').find('input:checkbox.demo').prop('checked')) ? 1 : 0;
			if(prevHeading.length == 0)
				req.headingId = 0;
			else
				req.headingId = prevHeading.attr('data-hid');
			req.title = title;
			req.contentId = $(this).parents('.section-content').attr('data-conid');
			saveContentToDB(req, $(this).parents('.section-content'));
			elem.find('.view-pane').show();
			elem.find('.edit-pane').hide();
			if(tempFlag == true){
				elem.find('.add-stuff').hide();
				var stuffTypeSelector = createStuffTypeSelector();
				elem.append(stuffTypeSelector);
			}
		});
		
		elem.find('.content-title-input').off('keyup');
		elem.find('.content-title-input').on('keyup', function(e) {
			if(e.which == 13) {
				$(this).parents('.section-content').find('.save-title').click();
			}
		});
		
		elem.find('.add-stuff').off('click');
		elem.find('.add-stuff').on('click', function() {
			var content = $(this).parents('.section-content');
			if(content.attr('data-conid') == '0') {
				content.find('.save-title').click();
			}
			$(this).hide();
			var stuffTypeSelector = createStuffTypeSelector();
			content.append(stuffTypeSelector);
		});
	}
	
	function stuffTypeSelectEventHandler(elem) {
		elem.find('.remove-type-selector').on('click', function() {
			$(this).parents('.section-content').find('.add-stuff').show();
			$(this).parents('.stuff-type-selector').remove();
		});
		elem.find('.add-file').on('click', function(e) {
			e.preventDefault();
			var parent = elem.parents('.section-content');
			elem.remove();
			var fileUploader = createFileUploader();
			fileUploader.find('.content-id').val(parent.attr('data-conid'));
			
			if($(this).hasClass('ppt')) {
				fileUploader.find('.file-type').html('Presentation');
				fileUploader.find('.stuff-msg').html('Please use a Powerpoint or Keynote Presentation  saved as pdf');
				fileUploader.find('.stuff-desc').html('Tip : A presentation can be a PowerPoint or Keynote saved as  a pdf. Please do not just use text in the presentation, make it as animated as possible<br>Once you have the presentation ready, please save it as a pdf');
				fileUploader.find('.content-type').val('ppt');
			}else if($(this).hasClass('doc')) {
				fileUploader.find('.file-type').html('Document');
				fileUploader.find('.stuff-msg').html('Please upload pdf file here');
				fileUploader.find('.stuff-desc').html('Tip : Documents can be a Word or  Pages file saved as a pdf. Please make it downloadable, if you want the students to access it offline');
				fileUploader.find('.content-type').val('doc');
			}else if($(this).hasClass('dwn')) {
				fileUploader.find('.file-type').html('Downloadable');
				fileUploader.find('.stuff-msg').html('The maximum file size which the student can download  is 1 GB');
				fileUploader.find('.stuff-desc').html('Tip : The files attached here  will be downloaded by the students. There is no restriction on the file type');
				fileUploader.find('.content-type').val('dwn');
			}
			parent.append(fileUploader);
		});
		
		elem.find('.add-link').on('click', function(e) {
			e.preventDefault();
			var parent = elem.parents('.section-content');
			elem.remove();
			var linkView = createLinkWidget(parent);
			if($(this).hasClass('vimeo')) {
				linkView.find('.link-type').html('Vimeo');
				linkView.find('input[type="text"]').attr('placeholder', "Add Vimeo Link");
				linkView.find('.stuff-desc').html('Tip: Add Vimeo Link');
			}
			parent.append(linkView);
		});
		
		elem.find('.add-text').on('click', function(e) {
			e.preventDefault();
			var parent = elem.parents('.section-content');
			elem.remove();
			var textWidget = createTextWidget(parent);
			parent.append(textWidget);
			var editorId = 'editor-' + parent.attr('data-conid');
			CKEDITOR.inline( editorId, {
						toolbar: [
								{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ]},
								'/',
								{ name: 'insert', items: [ 'Image', 'Mathjax' ]},
								{ name: 'styles', items: [ 'FontSize' ] },
								{ name: 'colors', items: [ 'TextColor' ] }],
						right: 0,
						filebrowserBrowseUrl: '../api/browse.php',
						filebrowserUploadUrl: '../api/uploader.php',
						filebrowserWindowWidth: '640',
						filebrowserWindowHeight: '480',
						extraPlugins: 'confighelper'
					});
			//elem.find('.cke_dialog_tab[title="Advanced"], .cke_dialog_tab[title="Link"]').hide();
			//elem.find('.cke_dialog_tab[title="Upload"]').click();
		});
	}
	
	function fileUploaderEventHandler(elem) {
		elem.find('.remove-file-uploader').on('click', function() {
			if(!$(this).parents('.section-content').hasClass('has-file'))
				$(this).parents('.section-content').find('.add-stuff').show();
			if($(this).parents('.stuff-file-uploader').hasClass('supp-files'))
				$(this).parents('.section-content').find('.add-supp-stuff').show();
			$(this).parents('.stuff-file-uploader').remove();
		});
		
		elem.find('.upload-file').on('click', function() {
			$(this).parents('.section-content').find('input[type=file]').click();
		});
		
		elem.find('input[type="file"]').on('change', function() {
			console.log($(this).val());
			//var fileName = $(this).val().split('/').pop();
			var parent = $(this).parents('.section-content');
			var contentType = parent.find('.content-type').val();
			console.log($(this)[0].files);
			var fileName = $(this)[0].files[0].name;
			var ext = fileName.split('.').pop().toLowerCase();
			if(contentType == "video") {
				if( $.inArray(ext, ['mp4','mpeg','mpeg4','asf','mkv ','mov','wmv','flv','avi']) == -1) {
					alert('Invalid file type for video content');
					return;
				}
			} else if(contentType == "ppt") {
				if( ext != 'pdf') {
					alert('You can upload only pdf files');
					return;
				}
			} else if(contentType == "doc") {
				if( ext != 'pdf') {
					alert('You can upload only pdf files');
					return;
				}
			} else if(contentType == "dwn") {
				if( ext == 'exe') {
					alert('This file type is invalid');
					return;
				}
			} else{
				return;
			}
			$(this).parents('.section-content').find('form .file-name').val(fileName);
			$(this).parents('.section-content').attr('data-file-name', fileName);
			elem.find('form').submit();
		});
		
		var options = {
			beforeSend: function(arr, $form, data) {
				// console.log(elem);
				elem.find('.upload-options').hide();
				elem.find('.upload-working').show();
                                reloadFlag=true;
			},
            uploadProgress: function(event, position, total, percentComplete) {
				//Progress bar
				var progress = elem.find('.progress')
				progress.find('.progress-bar').width(percentComplete + '%') //update progressbar percent complete
				progress.find('.sr-only').html(percentComplete + '%') //update progressbar percent complete
			},
			success: function() {
				
			},
			complete: function(resp) {
                             reloadFlag=false;
                            //location.reload(true);
				console.log(resp.responseText);
				var sectionContent = elem.parents('.section-content');
				var file = $.parseJSON(resp.responseText);
                                //file = 'h';
				if(elem.find('input[name="uploaded-content-stuff"]').length > 0) {
					var contentType = elem.find('form .content-type').val();
					sectionContent.attr('data-content-type', contentType);
					sectionContent.find('.stuff-file-uploader').remove();
					sectionContent.find('.stuff-dp').remove();
					if(!sectionContent.hasClass('has-file'))
						sectionContent.addClass('has-file');
					//sectionContent.attr('data-metadata', file.metadata);
					sectionContent.append(createStuffDP(sectionContent));
				}else {
					if(sectionContent.find('.supplementary-stuff-widget').length == 0) {
						var temp = [];
						temp.push(file);
						var supplementaryStuffWidget = createSupplementaryStuffWidget(sectionContent, temp);
						sectionContent.find('.supp-section').prepend(supplementaryStuffWidget);
					} else {
						var suppDp = $(createSuppFileDP(file));
						suppDp.find('.delete-supp-file').on('click', function(e) {
							e.preventDefault();
							var req = {};
							req.suppFileId = $(this).parents('.supp-file').attr('data-supp-file');
							deleteSuppFileFromDB(req, $(this).parents('.supp-file'));
						});
						sectionContent.find('.supplementary-stuff-widget').append(suppDp);
					}
					sectionContent.find('.stuff-file-uploader').remove();
					sectionContent.find('.add-supp-stuff').show();
				}
			},
			error: function() {
				$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
			}
		};
		elem.find('form').attr('action', fileApiEndpoint).ajaxForm(options);
	}
	
	function linkWidgetEventHandler(elem) {
           
		elem.find('.remove-link-widget').on('click', function() {
			if(!$(this).parents('.section-content').hasClass('has-file'))
				$(this).parents('.section-content').find('.add-stuff').show();
			else
				$(this).parents('.section-content').find('.show-stuff-dp i').removeClass('fa-caret-square-o-up').addClass('fa-caret-square-o-down');
			$(this).parents('.stuff-link-widget').remove();
		});
		elem.find('.save-link').on('click', function() {
                        $('.help-block').remove();
			var req = {};
			req.stuff = elem.find('input[type="text"]').val();
            req.contentType = $(this).parents('.stuff-link-widget').find('.link-type').html();
            var matches='';
            if(req.contentType=='Youtube'){
               matches= req.stuff.match(/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11,})(?:\S+)?$/);
            }
            if (req.contentType == 'Vimeo') {
                matches = req.stuff.match(/\/\/(?:www\.)?vimeo.com\/([0-9a-z\-_]+)/i);
            }
            if (matches) {
                // alert('valid');
                req.contentId = $(this).parents('.section-content').attr('data-conid');
                saveLinkToDB(req, $(this).parents('.section-content'));

            } else {
                // alert('Invalid');
                $(this).parent().prev().find('input[type=text]').after('<span class="help-block text-danger">Enter a valid url </span>');
                return false;
            }

        });
	}
	
	function textWidgetEventHandler(elem) {
		elem.find('.remove-text-widget').on('click', function() {
			if(!$(this).parents('.section-content').hasClass('has-file'))
				$(this).parents('.section-content').find('.add-stuff').show();
			else
				$(this).parents('.section-content').find('.show-stuff-dp i').removeClass('fa-caret-square-o-up').addClass('fa-caret-square-o-down');
				
			$(this).parents('.stuff-text-widget').remove();
		});	
		
		elem.find('.save-text').on('click', function() {
			var id = $(this).parents('.stuff-text-widget').find('textarea').attr('id');
			var html = CKEDITOR.instances[id].getData();
			if(html.trim() == ''){
				alert('Empty area');
				return;
			}
			var req = {};
			req.stuff = html;
			req.contentId = $(this).parents('.section-content').attr('data-conid');
			saveTextStuffToDB(req, $(this).parents('.section-content'));
		});
	}
	
	function reNumberEverything(updateOnServer) {
		var headingNum = 1;
		var contentNum = 1;
		var ordering = {'headings' : {}};
		var formattedContentNum, formattedHeadingNum, increaseHeading, headingEncounter;
		headingEncounter = false;
		var  hid = 0;
		var conid = 0;
		//ordering['headings']['0'] = {};
		//ordering['headings']['0']['content'] = {};
		$('.headings > div.sortable-item').each(function() {
			if($(this).hasClass('section-heading')) {
				headingEncounter = true;
				if(increaseHeading == true) {
					headingNum++;
					increaseHeading = false;
				}
				if(updateOnServer == true) {
					hid = $(this).attr('data-hid');
					if(hid != '0')
						ordering['headings'][hid] = {'weight' : headingNum, 'content' : {}};
				}
				formattedHeadingNum = 'Section ' + headingNum + ':';
				$(this).find('.heading-number').html(formattedHeadingNum);
				increaseHeading = true;
				contentNum = 1;
			}
			
			if($(this).hasClass('section-content')) {
				formattedContentNum = 'Lecture '  + headingNum + '.' + contentNum;
				if(headingEncounter == false) {
					formattedContentNum = '';
				}
				if(updateOnServer == true) {
					conid = $(this).attr('data-conid');
					if(conid != '0')
						ordering['headings'][hid]['content'][conid] = contentNum;
				}
				$(this).find('.content-number').html(formattedContentNum);
				contentNum++;
				// if(headingNum == 1)
					// increaseHeading = true;
			}
		});
		if(updateOnServer == true) {
			//console.log(ordering);
			
			updateContentOrderOnServer(ordering);
		}
		headingCount = headingNum - 1;
	}
	
	function createStuffTypeSelector() {
		var html = '<div class="stuff-type-selector">'
						+ '<div class="stuff-type-selector-header clearfix">'
							+ '<span class="pull-right">Select Content Type <button class="btn btn-xs btn-danger remove-type-selector"><i class=" fa fa-times"></i></button></span>'
						+ '</div>'
						+ '<div class="stuff-type-selector-body">'
							+ '<ul class="summary-list-a">'
								+ '<li>'
									+ '<div class="btn-group open video-icon">'
										+ '<a type="button" class="dropdown-toggle" data-toggle="dropdown" class="video-text">'
											+ '<i class=" fa fa-video-camera text-primary"></i>'
											+ 'Video<span class="caret"></span>'
										+ '</a>'
										+ '<ul class="dropdown-menu" role="menu">'
											+ '<li><a href="#" class="add-file video">Add From PC</a></li>'
											+ '<li><a href="#" class="add-link youtube">Import From Youtube</a></li>'
											+ '<li><a href="#" class="add-link vimeo">Import From Vimeo</a></li>'
										+ '</ul>'
									+ '</div>'
								+ '</li>'
								+ '<li><a href="#" class="add-file ppt"><i class=" fa fa-desktop text-info"></i>Presentation</a></li>'
								+ '<li><a href="#" class="add-text"><i class="fa fa-text-width text-success"></i>Text</a></li>'
								+ '<li><a href="#" class="add-file doc"><i class="fa fa-file-text-o text-primary"></i>Document</a></li>'
								+ '<li><a href="#" class="add-file dwn"><i class="fa fa-download text-info"></i>Downloadable</a></li>'
							+ '</ul>'
						+ '</div>'
					+ '</div>';
		var stuffTypeSelector = $(html);
		stuffTypeSelectEventHandler(stuffTypeSelector);
		return stuffTypeSelector;
	}
	
	function createLinkWidget(elem) {
		var html = '<div class="stuff-link-widget">'
						+ '<div class="stuff-link-widget-header clearfix">'
							+ '<span class=" pull-right">Add <span class="link-type">Youtube</span>  Link<button class="btn btn-xs btn-danger remove-link-widget"><i class=" fa fa-times"></i></button></span>'
						+ '</div>'
						+ '<div class="stuff-link-widget-body">'
							+ '<div class="row">'
								+ '<div class="col-sm-10">'
									+ '<label for="url">Your Video Url</label>'
									+ '<input type="text" placeholder="Add Youtube Link" class="form-control">'
								+ '</div>'
								+ '<div class="col-sm-1">'
									+ '<label for="save-button">&nbsp;</label><br/>'
									+ '<button class="btn btn-info btn-sm save-link">Save</button>'
								+ '</div>'
							+ '</div>'
							+ '<div class="stuff-desc">'
								+ 'Tip: Add Youtube Link'
							+ '</div>'
						+ '</div>'
					+ '</div>';
		var linkWidget = $(html);
		linkWidgetEventHandler(linkWidget);
		return linkWidget;
	}
	
	function createTextWidget(elem) {
		var html = '<div class="stuff-text-widget">'
						+ '<div class="stuff-text-widget-header clearfix">'
							+ '<span class=" pull-right">Add <span class="file-type">Text</span> <button class="btn btn-xs btn-danger remove-text-widget"><i class=" fa fa-times"></i></button></span>'
						+ '</div>'
						+ '<div class="stuff-text-widget-body">'
							+ '<div>'
								+ '<textarea id="editor" cols="10" rows="3" placeholder="Please add text here"></textarea>'
								+ '<button class="btn btn-info btn-sm save-text pull-right" style="margin-top: 1%;">Save</button>'
							+ '</div>'
							+ '<div class="stuff-desc">'
								+ 'Tip : You can  also add images and special symbols to the text'
							+ '</div>'
						+ '</div>'
					+ '</div>';
		var textWidget = $(html);
		var editorId = 'editor-' + elem.attr('data-conid');
		textWidget.find('textarea').attr('id', editorId);
		textWidgetEventHandler(textWidget);
		return textWidget;
		
	}
	
	//fetchProfileDetails();
	
	$('.add-heading').click(function() {
		hideAddHeadingLink();
		addNewHeading();
	});
	
	$('.headings').sortable({
		handle: '> .view-pane, > .edit-pane',
		cursor: 'move',
		items: 'div.sortable-item',
		forcePlaceholderSize: true,
		placeholder:'sort-placeholder',
		start: function (event, ui) {
			// Build a placeholder cell that spans all the cells in the row
			var cellCount = 0;
			$('td, th', ui.helper).each(function () {
				// For each TD or TH try and get it's colspan attribute, and add that or 1 to the total
				var colspan = 1;
				var colspanAttr = $(this).attr('colspan');
				if (colspanAttr > 1) {
					colspan = colspanAttr;
				}
				cellCount += colspan;
			});

			// Add the placeholder UI - note that this is the item's content, so TD rather than TR
			ui.placeholder.html('<td colspan="' + cellCount + '">&nbsp;</td>');
			//$(this).attr('data-previndex', ui.item.index());
		},
		update: function(event, ui) {
			// gets the new and old index then removes the temporary attribute
			// var newOrder = $.map($(this).find('tr'), function(el) {
				// return $(el).attr('data-ch-id')+ "-" +$(el).index();
			// });
			
			
			if(reorderTimer != null)
				clearTimeout(reorderTimer);
			reorderTimer = setTimeout(function(){reNumberEverything(true);}, 5000);
			// if(reorderTimer != null)
				// clearTimeout(reorderTimer);
			// reorderTimer = setTimeout(function(){updateChapterOrder(newOrder);}, 5000);
		},
		helper: function(e, ui) {
			ui.children().each(function() {
				//$(this).width($(this).width());
			});
			return ui;
		}
	})
	// .disableSelection();
	// $('.headings').find('input').enableSelection();
	
	function initializePanel(data) {
		var flagHeading = false, flagContent = false;
		if(data.status == 1) {
			/*$.each(data.headlessContent, function(i, content) {
				addNewContent(content);
			});*/
			$.each(data.headings, function(i, heading) {
				addNewHeading(heading);
				flagHeading = false;
				flagContent = false;
				$.each(heading.content, function(i, content) {
					addNewContent(content);
				});
			});
		}
		$('.section-heading').each( function() {
			headingEventHandlers($(this));
		});
		
		$('.section-content').each(function() {
			contentEventHandlers($(this));
		});
		// if(flagHeading == true) {
			// var heading = {};
			// heading.title = "My first heading";
			// heading.id = "0";
			// addNewHeading(heading);
		// }
		// if(flagContent == true) {
			// var content = {};
			// content.title = "My first Content";
			// content.id = "0";
			// addNewContent(content);
		// }
		pageLoadRendering = false;
		
	}

	function saveHeadingToDB(req, elem) {
		var res;
		req.action = "create-chapter";
		if(req.headingId != 0) {
			req.action = 'update-chapter-details';
			req.chapterId = req.headingId;
			req.chapterName = req.title;
		}
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		req.chapterName = req.title;
		//req.chapterId = getUrlParameter('chapterId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1){
				elem.attr('data-hid', res.chapterId);
			}
			else
				alertMsg(res.message);
		});
	}
	
	function createFileUploader() {
		var html = '<div class="stuff-file-uploader">'
						+ '<div class="stuff-file-uploader-header clearfix">'
							+ '<span class=" pull-right">Upload <span class="file-type">Video</span> <button class="btn btn-xs btn-danger remove-file-uploader"><i class=" fa fa-times"></i></button></span>'
						+ '</div>'
						+ '<div class="stuff-file-uploader-body">'
							+ '<div class="upload-options">'
								+ '<label class="stuff-msg">Please use mp4 and mov files only. The maximum file size can be 1 GB</label>'
								+ '<button class="btn btn-info btn-sm upload-file">Upload <span class="file-type">Video</span> File</button>'
								+ '<form method="post" enctype="multipart/form-data">'
									+ '<input type="file" class="hidden" name="uploaded-content-stuff"/>'
									+ '<input type="hidden" name="contentId" class="content-id" value="" />'
									+ '<input type="hidden" name="contentType" class="content-type" value="video" />'
									+ '<input type="hidden" name="fileName" class="file-name" value="none" />'
								+ '</form>'
							+ '</div>'
							+ '<div class="upload-working hidden-l">'
								+ '<div class="progress">'
									+ '<div class="progress-bar progress-bar-striped active"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">'
										+ '<span class="sr-only">45% Complete</span>'
									+ '</div>'
								+ '</div>'
							+ '</div>'
							+ '<div class="stuff-desc">'
								+ 'Tip:  Videos should be of good quality. For best learning, please use as many videos as you can. Ideally a video should not be longer than 20 minutes'
							+ '</div>'
						+ '</div>'
					+ '</div>';
		var fileUploader = $(html);
		fileUploaderEventHandler(fileUploader);
		return fileUploader;
	}
	
	function saveContentToDB(req, elem) {
		var res;
		req.action = "save-content";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				elem.attr('data-conid', res.contentId);
			}
			else
				alertMsg(res.message);
		});
	}

	function createStuffDP(sectionContent) {
		var contentType = sectionContent.attr('data-content-type');
		var fileName = sectionContent.attr('data-file-name');
		var metadata = sectionContent.attr('data-metadata');
		var html = '<div class="stuff-dp">'
						+ '<div class="stuff-dp-header clearfix">'
							+ ''
						+ '</div>'
						+ '<div class="stuff-dp-body">'
							+ '<div class="clearfix">'
								+ '<div class="stuff-icon large"></div>'
								+ '<div class="stuff-details">'
									+ '<span class="file-name">File name</span>'
									//+ '<span class="file-meta">Meta Data</span>'
									+ '<a class="edit-stuff" href="#"><i class=" fa fa-pencil"></i>Edit Stuff</a>'
								+ '</div>'
								+ '<div class="stuff-links">'
									+ '<button class="btn btn-sm btn-success preview-button">Preview</button>&nbsp;&nbsp;'
									+ '<button class="btn btn-sm btn-warning change-published" data-published="0">Publish</button>'
									+ '<br /><br /><div class="downloadable-holder hidden-l"><b>Downloadable:</b> <input type="checkbox" class="change-downloadable" data-downloadable="0"></div>'
									+ '<a href="#" class="delete-stuff pull-right"><i class="fa fa-trash-o"></i></a>'
								+ '</div>'
							+ '</div>'
							+ '<div class="supp-section">'
								+ '<div class="supp-section-links">'
									+ '<button class="btn btn-sm btn-success add-supp-stuff">Add Supplementary Material</button><i class="tooltips fa fa-info-circle" data-original-title="Supplementary materials is basically files that the students can download. You can add more that one files. Each lecture can have its own supplemntary material." data-placement="right"></i>'
									+ '&nbsp; &nbsp;<button class="btn btn-sm btn-success add-stuff-desc">Add Description</button><i class="tooltips fa fa-info-circle" data-original-title="Please add any information or details about the lecture for the student" data-placement="right"></i>'
								+ '</div>'
							+ '</div>'
						+ '</div>'
					+ '</div>';
		var hideButton = $('<span class="pull-right"><span class="show-stuff-dp"><i class=" fa fa-caret-square-o-up"></i></span></span>');
		hideButton.on('click', function() {
			$(this).parents('.section-content').find('.stuff-dp').toggleClass('hidden-l');
			if($(this).parents('.section-content').find('.stuff-dp').hasClass('hidden-l'))
				$(this).find('i').removeClass('fa-caret-square-o-up').addClass('fa-caret-square-o-down');
			else
				$(this).find('i').removeClass('fa-caret-square-o-down').addClass('fa-caret-square-o-up');
		});
		var stuffDP = $(html);
		//event handler for preview button click
		stuffDP.find('.preview-button').off('click');
		stuffDP.find('.preview-button').on('click', function() {
			var contentId = $(this).parents('div.section-content:eq(0)').attr('data-conId');
			var chapterId = $(this).parents('div.section-content:eq(0)').prev().attr('data-hid');
			window.location = "chapterPreview.php?courseId=" + getUrlParameter('courseId') + "&subjectId=" + getUrlParameter('subjectId') + "&chapterId=" + chapterId + '&contentId=' + contentId;
		});
		stuffDP.find('.tooltips').tooltip();
		if(pageLoadRendering == true) {
			stuffDP.addClass('hidden-l');
			hideButton.find('i').removeClass('fa-caret-square-o-up').addClass('fa-caret-square-o-down');
		}
		if(contentType == 'text' || contentType == 'Youtube' || contentType == 'Vimeo')
			stuffDP.find('.downloadable-holder').remove();
		sectionContent.find('.show-stuff-dp').remove();
		sectionContent.find('.view-pane .panel-heading').append(hideButton);
		stuffDP.find('.stuff-icon').html(getStuffImageByType(contentType));
		stuffDP.find('.file-name').html(fileName);
		//stuffDP.find('.file-meta').html(metadata);
		if(contentType ==  'text') {
			var textStuff = sectionContent.find('.text-stuff');
			if(textStuff.length != 0){
				stuffDP.append('<div class="hidden hidden-l text-stuff">' + textStuff.html() + '</div>');
				textStuff.remove();
			}
		}
		if(contentType ==  'Youtube' || contentType == 'Vimeo') {
			var linkStuff = sectionContent.find('.link-stuff');
			if(linkStuff.length != 0){
				stuffDP.append('<div class="hidden hidden-l link-stuff">' + linkStuff.html() + '</div>');
				linkStuff.remove();
			}
		}
		stuffDPEventHandler(stuffDP);
		return stuffDP;
	}
	
	function stuffDPEventHandler(elem) {
		elem.find('.edit-stuff').on('click',function(e) {
			e.preventDefault();
			var parent = $(this).parents('.section-content');
			var type = parent.attr('data-content-type');
			if(!parent.find('.stuff-dp').hasClass('hidden-l'))
				parent.find('.stuff-dp').addClass('hidden-l');
			if(type != 'text' && type != 'Youtube' && type != 'Vimeo') {
				var fileUploader = createFileUploader();
				fileUploader.find('.content-id').val(parent.attr('data-conid'));
				if(type == 'ppt') {
					fileUploader.find('.file-type').html('Presentation');
					fileUploader.find('.stuff-msg').html('Please use a Powerpoint or Keynote Presentation  saved as pdf');
					fileUploader.find('.stuff-desc').html('Tip : A presentation can be a PowerPoint or Keynote saved as  a pdf. Please do not just use text in the presentation, make it as animated as possible<br>Once you have the presentation ready, please save it as a pdf');
					fileUploader.find('.content-type').val('ppt');
				}
				else if(type == 'doc') {
					fileUploader.find('.file-type').html('Document');
					fileUploader.find('.file-type').html('Document');
					fileUploader.find('.stuff-msg').html('Please upload pdf file here');
					fileUploader.find('.stuff-desc').html('Tip : Documents can be a Word or  Pages file saved as a pdf. Please make it downloadable, if you want the students to access it offline');
					fileUploader.find('.content-type').val('doc');
				}
				else if(type == 'dwn') {
					fileUploader.find('.file-type').html('Downloadable');
					fileUploader.find('.stuff-msg').html('The maximum file size which the student can download  is 1 GB');
					fileUploader.find('.stuff-desc').html('Tip : The files attached here  will be downloaded by the students. There is no restriction on the file type');
					fileUploader.find('.content-type').val('dwn');
				}
				parent.append(fileUploader);
			}
			else if (type == 'text') {
				var textWidget = createTextWidget(parent);
				parent.append(textWidget);
				var html = parent.find('.stuff-dp .text-stuff').html();
				var editorId = 'editor-' + parent.attr('data-conid');
				CKEDITOR.replace( editorId, {
					toolbar: [
								{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ]},
								'/',
								{ name: 'insert', items: [ 'Image', 'Mathjax' ]},
								{ name: 'styles', items: [ 'FontSize' ] },
								{ name: 'colors', items: [ 'TextColor' ] }],
					extraPlugins: 'mathjax,image',
					removePlugins : 'elementspath',
					filebrowserBrowseUrl: '../api/browse.php',
					filebrowserUploadUrl: '../api/uploader.php',
					filebrowserWindowWidth: '640',
					filebrowserWindowHeight: '480',
					height: 150
				});
				//$('.cke_dialog_tab[title="Advanced"], .cke_dialog_tab[title="Link"]').hide();
				CKEDITOR.instances[editorId].setData(html);
			} else if(type == 'Youtube' || type == 'Vimeo') {
				var linkWidget = createLinkWidget(parent);
				parent.append(linkWidget);
				if(type == 'Vimeo') {
					linkWidget.find('.link-type').html('Vimeo');
					linkWidget.find('input[type="text"]').attr('placeholder', "Add Vimeo Link");
					linkWidget.find('.stuff-desc').html('Tip: Add Vimeo Link');
				}
				var html = parent.find('.stuff-dp .link-stuff').html();
				parent.find('input[type="text"]').val(html);
			}
		});
		
		elem.find('.change-published').on('click',function() {
			var req = {};
			if($(this).attr('data-published') == '0') {
				req.status = 1;
			}else{
				req.status = 0;
			}
			updatePublishedStatus(req, $(this));
		});
		
		elem.find('.change-downloadable').on('click',function() {
			var req = {};
			if($(this).attr('data-downloadable') == '0') {
				req.status = 1;
			} else {
				req.status = 0;
			}
			updateDownloadableStatus(req, $(this));
		});
		
		elem.find('.add-stuff-desc').on('click',function() {
			var stuffDescriptionWidget = createStuffDescriptionWidget(elem.parents('.section-content'));
			elem.find('.supp-section').prepend(stuffDescriptionWidget);
			// var editorId = stuffDescriptionWidget.find('textarea').attr('id');
			// CKEDITOR.replace( editorId, {
				// extraPlugins: 'image',
				// height: 100
			// });
			$(this).hide();
		});
		
		//elem.find('.change-downloadable').bootstrapSwitch('size', 'mini');
		elem.find('.change-downloadable').on('switchChange.bootstrapSwitch', function(event, state) {
			// console.log(this); // DOM element
			// console.log(event); // jQuery event
			// console.log(state); // true | false
			var req = {};
			if(state == true) {
				req.status = 1;
			} else {
				req.status = 0;
			}
			updateDownloadableStatus(req, $(this));
		});
	
		elem.find('.delete-stuff').on('click', function(e) {
			e.preventDefault();
			var req = {};
	        if(confirm("Are you sure to delete ? ")){
	            deleteContentStuff(req, $(this));
	        }
			
		});
		
		elem.find('.add-supp-stuff').on('click', function() {
			var parent = $(this).parents('.section-content');
			$(this).hide();
			var fileUploader = createFileUploader();
			fileUploader.addClass('supp-files');
			fileUploader.find('.content-id').val(parent.attr('data-conid'));
			fileUploader.find('.file-type').html('Supplementary');
			fileUploader.find('.stuff-msg').html('Use any file no larger than 1.0 GiB.');
			fileUploader.find('.stuff-desc').html('Tip: This is a supplementary material');
			fileUploader.find('.content-type').val('dwn');
			fileUploader.find('input[type="file"]').attr('name','uploaded-supplementary-stuff');
			parent.append(fileUploader);
		});
	}
	
	function createStuffDescriptionWidget (elem) {
		var html = '<div class="stuff-description-widget">'
						+ '<div class="view">'
							+ ''
						+ '</div>'	
						+ '<div class="edit">'
							+ '<textarea></textarea>'
							+ '<div>'
								+ '<button class="btn btn-info btn-sm save-stuff-desc">Save</button>&nbsp;&nbsp;&nbsp;'
								+ '<button class="btn btn-default btn-sm cancel-stuff-desc">Cancel</button>'
							+ '</div>'
						+ '</div>'
					+ '</div>';
		var widget = $(html);
		var conid = elem.attr('data-conid');
		var id = 'desc-editor-' + conid;
		widget.find('textarea').attr('id', id);
		stuffDescriptionWidgetEventHandlers(widget);
		return widget;
	}
	
	function stuffDescriptionWidgetEventHandlers(elem) {
		elem.find('.save-stuff-desc').on('click', function() {
			// var id = elem.find('textarea').attr('id');
			// var html = CKEDITOR.instances[id].getData();
			var html = elem.find('textarea').val();
			var req = {};
			req.descText = html;
			req.contentId = $(this).parents('.section-content').attr('data-conid');
			saveStuffDescToDB(req, $(this).parents('.section-content'));
		});
		elem.find('.cancel-stuff-desc').on('click', function() {
			elem.find('.edit').hide();
			if(elem.find('.view').text().trim() != '') {
				elem.find('.edit').hide();
				elem.find('.view').show();
			}else {
				elem.parents('.section-content').find('.add-stuff-desc').show();
				elem.remove();
			}
		});
		
	}
	
	function saveStuffDescToDB(req, elem) {
		var res;
		req.action = "save-stuff-desc";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				var sectionContent = elem;
				if(req.descText.trim() != '') {
					sectionContent.find('.stuff-description-widget .edit').hide();
					var stuffDescDisplay = createStuffDescDisplay(nl2br(req.descText));
					sectionContent.find('.stuff-description-widget .view').html(stuffDescDisplay).show();
				}else {
					sectionContent.find('.stuff-description-widget').remove();
					sectionContent.find('.add-stuff-desc').show();
				}
			}else
				alertMsg(res.message);
		});
	}
	
	function createStuffDescDisplay(html) {
		var html = '<div class="stuff-description-display">'
					+ '<div><b>Stuff Description</b></div><br/>'
					+ html
					+ '</div>';
		var elem = $(html);
		stuffDescDisplayEventHandler(elem);
		return elem;
	}
	
	function stuffDescDisplayEventHandler(elem) {
		elem.on('click', function() {
			$(this).parents('.stuff-description-widget').find('.view').hide();
			$(this).parents('.stuff-description-widget').find('.edit').show();
		});
	}
	
	function saveTextStuffToDB(req, elem) {
		var res;
		req.action = "save-text-stuff";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				var sectionContent = elem;
				var contentType = 'text';
				if(!sectionContent.hasClass('has-file'))
					sectionContent.addClass('has-file');
				sectionContent.attr('data-content-type', contentType);
				sectionContent.find('.stuff-text-widget').remove();
				sectionContent.attr('data-file-name', 'Text');
				sectionContent.attr('data-metadata', '');
				sectionContent.find('.text-stuff').remove();
				sectionContent.append('<div class="hidden hidden-l text-stuff">' + req.stuff + '</div>');
				var stuffDp = createStuffDP(sectionContent)
				sectionContent.find('.stuff-dp').remove();
				sectionContent.append(stuffDp);
			}else
				alertMsg(res.message);
		});
	}
	
	function saveLinkToDB(req, elem) {
		var res;
		req.action = "save-link-stuff";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				var sectionContent = elem;
				var contentType = req.contentType;
				if(!sectionContent.hasClass('has-file'))
					sectionContent.addClass('has-file');
				sectionContent.attr('data-content-type', contentType);
				sectionContent.find('.stuff-link-widget').remove();
				sectionContent.attr('data-file-name', 'Link');
				sectionContent.attr('data-metadata', '');
				sectionContent.find('.link-stuff').remove();
				sectionContent.append('<div class="hidden hidden-l link-stuff">' + req.stuff + '</div>');
				var stuffDp = createStuffDP(sectionContent)
				sectionContent.find('.stuff-dp').remove();
				sectionContent.append(stuffDp);
			}else
				alertMsg(res.message);
		});
	}
	
	function deleteHeadingFromDB(heading) {
		heading.find('.edit-pane').hide();
		heading.find('.view-pane').show();
		var hid = heading.attr('data-hid');
		heading.addClass('deleted-section');
		
		var req = {};
		var res;
		req.hid = hid;
		req.chapterId = hid;
		req.action = "delete-chapter";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				heading.remove();
				reNumberEverything(false);
			}
			else
				alertMsg(res.message);
		});
	}
	
	function deleteContentFromDB(content) {
		content.find('.edit-pane').hide();
		content.find('.view-pane').show();
		var conid = content.attr('data-conid');
		
		content.addClass('deleted-section');
		var req = {};
		var res;
		req.conid = conid;
		req.action = "delete-content";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				content.remove();
				reNumberEverything(false);
			}else
				alertMsg(res.message);
		});
	}
	
	function updatePublishedStatus(req, elem) {
		req.action = 'update-published-status';
		var res;
		var conid = elem.parents('.section-content').attr('data-conid');
		req.conid = conid;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1){
				elem.attr('data-published', req.status);
				elem.text(req.status == 1 ? 'UnPublish' : 'Publish');
				if(req.status == 1) {
					elem.removeClass('btn-warning').addClass('btn-success');
					elem.parents('.section-content').find('.panel-heading').removeClass('orange');
					elem.parents('.stuff-dp').find('.downloadable-holder').show();
				}
				else {
					elem.removeClass('btn-success').addClass('btn-warning');
					elem.parents('.section-content').find('.panel-heading').addClass('orange');
					elem.parents('.stuff-dp').find('.downloadable-holder').hide();
				}
			}else
				alertMsg(res.message);
		});
	}
	
	function updateDownloadableStatus(req, elem) {
		req.action = 'update-downloadable-status';
		var res;
		var conid = elem.parents('.section-content').attr('data-conid');
		req.conid = conid;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				elem.attr('data-downloadable', req.status);
				elem.text(req.status == 1 ? 'Remove Downloadable' : 'Make Downloadable');
			}else
				alertMsg(res.message);
		});
	}
	
	function deleteContentStuff(req, elem) {
		req.action = 'delete-content-stuff';
		var res;
		var conid = elem.parents('.section-content').attr('data-conid');
		req.contentId = conid;
		elem.parents('.stuff-dp').addClass('deleted');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				console.log(elem);
				elem.parents('.section-content').find('.panel-heading').addClass('orange');
				elem.parents('.section-content').find('.show-stuff-dp').parent('span').remove();
				elem.parents('.section-content').find('.add-stuff').show();
				elem.parents('.section-content').removeClass('has-file');
				elem.parents('.stuff-dp').remove();
				reNumberEverything(false);
			}else
				alertMsg(res.message);
		});
		
		// Todo: Delete Files from structure
	}
	
	function deleteSuppFileFromDB(req, elem) {
		req.action = 'delete-supp-stuff';
		var res;
		var conid = elem.parents('.section-content').attr('data-conid');
		req.contentId = conid;
		//elem.parents('.stuff-dp').addClass('deleted');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				var widget = elem.parents('.supplementary-stuff-widget');
				//console.log(widget.find('.sup'));
				if(widget.find('.supp-file').length == 1){
					widget.remove();
				}else{
					elem.remove();
				}
				reNumberEverything(false);
			}else
				alertMsg(res.message);
		});
		
		// Todo: Delete Files from structure
	}
	
	function updateContentOrderOnServer(order) {
		var req = {};
		req.action = 'update-content-order';
		var res;
		req.order = order;
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status != 1) {
				alertMsg('Some error occured! Whatever you did was not succesfull');
			}
		});
	}
	
	function getStuffImageByType(contentType) {
		if(contentType == 'doc')
			return '<i class="fa fa-file-text-o text-info"></i>';
		else if(contentType == 'ppt')
			return '<i class=" fa fa-desktop text-info"></i>';
		else if(contentType == 'text')
			return '<i class="fa fa-list text-info"></i>';
		else if(contentType == 'dwn')
			return '<i class="fa fa-download text-info"></i>';
		else if(contentType == 'video')
			return '<i class="fa fa-video-camera text-info"></i>';
		else if(contentType == 'Youtube')
			return '<i class="fa fa-youtube text-info"></i>';
		else if(contentType == 'Vimeo')
			return '<i class="fa fa-vimeo-square text-info"></i>';
	}
	
	fetchCourseDetails();
	fetchSubjectDetails();
  $( "#section-edit-chapter" ).hide();
    $('.edit-button').click(function () {
		$('#content').hide();
		$('.subject-image-thumb').hide();
        $( "#section-edit-chapter" ).toggle();
    });
	$('#edit-chapter-description').on('blur', function() {
		unsetError($('#edit-chapter-description'));
		if($('#edit-chapter-description').val().length > 1000) {
			setError($('#edit-chapter-description'), 'Please enter a description less than 1000 characters.');
			return false;
		}
	});
	
	$('#edit-chapter-name').on('blur', function() {
		var req = {};
		var res;
		if($('#edit-chapter-name').val().length < 5 || $('#edit-chapter-name').val().length > 100) {
			setError($('#edit-chapter-name'), 'Please enter a name between 5 to 100 characters.');
			return false;
		}
		else if(!($('#edit-chapter-name').parents('div:eq(0)').hasClass('has-error'))) {
			unsetError($('#edit-chapter-name'));
		}
		//now checking for duplicate name
		req.action = 'check-duplicate-chapter';
		req.subjectId = getUrlParameter('subjectId');
		req.chapterId = getUrlParameter('chapterId');
		req.name = $('#edit-chapter-name').val();
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else if(res.available == false) {
				setError($('#edit-chapter-name'), 'Please select a different name as you have already created a section by this name.');
			}
			else
				unsetError($('#edit-chapter-name'));
		});
	});
	
	$('#edit-chapter').click(function (e) {
		e.preventDefault();
		$('#edit-chapter-name, #edit-chapter-description').blur();
		if($(document).find('.has-error').length > 0)
			return;
		var req = {};
		var res;
		req.name = $.trim($('#edit-chapter-name').val());
		req.description = $.trim($('#edit-chapter-description').val());
		req.demo = $('#is-demo').prop('checked')?1:0;
		req.action = 'update-chapter-details';
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		req.chapterId = getUrlParameter('chapterId');
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				location.reload();
				$('#chapter-name').html('Chapter ' + chapterWeight + ' : ' +  req.name);
				$( "#section-edit-chapter" ).toggle();
				$('#content').show();
				$('.subject-image-thumb').show();
				if( req.description=='') {
					$('#chapter-description').prev().hide();
					$('#chapter-description').html(req.description);
				}else {
					$('#chapter-description').html(req.description.replace(new RegExp('\r?\n','g'), '<br />'));
					$('#chapter-description').prev().show();
				}
                if (req.demo == 1) {
                    $('#demochapter').remove();
                    $('#chapter-description').after('<p id=demochapter>This chapter is allowed for free demo. </p>');
                } else
                    $('#demochapter').remove();
            }else
				alertMsg(res.message);
		});
    });
	
	$('.cancel-edit').on('click', function() {
		$( "#section-edit-chapter" ).toggle();
		$('#content').show();
		$('.subject-image-thumb').show();
	});
	
});
	