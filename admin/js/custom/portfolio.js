var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var userRole;
var pfId=0;
$(document).ajaxStart(function () {
    $('#loader_outer').show();
});

$(document).ajaxComplete(function () {
    $('#loader_outer').hide();
});

$(function () {
    var imageRoot = '/';
    var jcrop_api;
    $('#loader_outer').hide();
    var fileApiEndpoint = '../api/files1.php';

    fetchProfileDetails();
    fetchPortfolios();

    $('#btnSubmitNewPortfolio').click(function(){
        var inputProfileName = trim($('#inputProfileName').val());
        var inputPortfolioDescription = trim($('#inputPortfolioDescription').val());
        var inputSubdomain = trim($('#inputSubdomain').val());
        var portfolioPic = trim($('#portfolioPic').val());
        
        if(!inputProfileName) {
            $('#frmNewPortfolio .help-block').html('<p class="text-danger text-center">Portfolio Name can\'t be left empty</p>');
        } else if(!inputPortfolioDescription) {
            $('#frmNewPortfolio .help-block').html('<p class="text-danger text-center">Portfolio Description can\'t be left empty</p>');
        } else if(!inputSubdomain) {
            $('#frmNewPortfolio .help-block').html('<p class="text-danger text-center">Subdomain Name can\'t be left empty</p>');
        } else {
            var req = {};
            req.portfolioName = inputProfileName;
            req.portfolioDesc = inputPortfolioDescription;
            req.subdomain     = inputSubdomain;
            req.portfolioPic  = portfolioPic;
            var res;
            req.action = "new-portfolio";
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if(res.status == 1) {
                    //$('#frmNewPortfolio').html('<div class="jumbotron text-center"><h3>'+res.message+'</h3></div>');
                    window.location = window.location;
                } else {
                    $('#frmNewPortfolio .help-block').html('<p class="text-danger text-center">'+res.message+'</p>');
                }
                console.log(res);
            });
        }
        return false;
    });

    function updateCoords(c) {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    };
    function readURL(input) {
        var _URL = window.URL || window.webkitURL;
        var imgWidth,imgHeight;
        var img = new Image();
        img.onload = function () {
            imgWidth = this.width;
            imgHeight = this.height;
    	    if (input.files && input.files[0]) {
    	        var reader = new FileReader();
                
                if(!(input.files[0]['type'] == 'image/jpeg' || input.files[0]['type'] == 'image/png'|| input.files[0]['type'] == 'image/gif')) {
                    $('#frmPortfolioImage .error').html('');
                    $('#frmPortfolioImage .error').append('<span style="color:red;" class="error">Please select a proper file type.</span>');
                    return;
                }
                else if(input.files[0]['size']>819200) {
                    $('#frmPortfolioImage .error').html('');
                    $('#frmPortfolioImage .error').append('<span style="color:red;" class="error">Files larger than 800kb are not allowed.</span>');
                    return;
                }
                else if(imgWidth<100 || imgHeight<100) {
                    $('#frmPortfolioImage .error').html('');
                    $('#frmPortfolioImage .error').append('<span style="color:red;" class="error">File\'s width and height needs to be more than 100.</span>');
                    return;
                }
                else if(imgWidth>900 || imgHeight>900) {
                    $('#frmPortfolioImage .error').html('');
                    $('#frmPortfolioImage .error').append('<span style="color:red;" class="error">File\'s width and height can\'t to be more than 900.</span>');
                    return;
                }
                else {
                    $('#frmPortfolioImage .error').html('');
                    $('#frmPortfolioImage input[type="submit"]').removeClass('hide');
                }

                if (typeof jcrop_api != 'undefined' && jcrop_api != null) {
                    jcrop_api.destroy();
                    jcrop_api = null;
                } else {
                    $('#uploadPortfolioPic').removeClass('hide');
                    $('#frmPortfolioImage input[type="submit"]').removeClass('hide');
                }
    	        reader.onload = function (e) {
    	            $('#uploadPortfolioPic').attr('src', e.target.result);
                    $('#uploadPortfolioPic').Jcrop({
                        minSize: [100, 100],
                        setSelect:   [ 0, 0, 100, 100],
                        aspectRatio: 1,
                        onSelect: updateCoords
                    },function(){
                        jcrop_api = this;
                    });
    	        }

    	        reader.readAsDataURL(input.files[0]);
    	    }
        };
        img.src = _URL.createObjectURL(input.files[0]);
	}

	$("#imgPortfolio").change(function(){
        readURL(this);
	});
    /*$('#profilePic').Jcrop();
	$('#btnChangeImg').click(function(){
		$("#imgPortfolio").trigger('click');
	});*/
    $('#btnChangeImg').click(function(){
       $('#uploadPortfolioImageModal').modal('show'); 
    });
    
    $('#frmPortfolioImage').attr('action', fileApiEndpoint);
    
    // $('#cover-image-form .submitButton').on('click', function() {
        // $('#cover-image-form').submit();
    // });
    
    $('#frmPortfolioImage').on('submit', function(e) {
        e.preventDefault();
        c = jcrop_api.tellSelect();
        crop = [c.x, c.y, c.w, c.h];
        imageC = document.getElementById('uploadPortfolioPic');
        var sources = {
            'portfolio-image': {
                image: imageC,
                type: 'image/jpeg',
                crop: crop,
                quality: 1.0
            }
        }
        //settings for $.ajax function
        var settings = {
            url: fileApiEndpoint,
            data: {'x': $('#x').val(),'y': $('#y').val(),'w': $('#w').val(),'h': $('#h').val()}, //three fields (medium, small, text) to upload
            beforeSend: function()
            {
                $("#cover-image-form .progress").show();
            },
            complete: function (resp) {
                var data = $.parseJSON(resp.responseText);
                $('#profilePic').attr('src', data.imageName);
                $('#portfolioPic').val(data.imageName);
                $('#frmPortfolioImage .jcrop-holder').hide();
                $('#frmPortfolioImage input[type="file"]').val('');
                $('#frmPortfolioImage input[type="submit"]').addClass('hide');
                $('#uploadPortfolioImageModal').modal('hide');
            }
        }
        cropUploadAPI.cropUpload(sources, settings);
    });

    function fetchPortfolios() {
        var req = {};
        var res;
        req.action = "get-portfolios";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillPortfoliosList(res);
            console.log(res);
        });
    }
    function fillPortfoliosList(data) {
		console.log(data);
        // General Details
        if(data.portfolios.length>0) {
        	$('.js-portfolios').html('<ul class="nav nav-pills nav-stacked list-pf-mainmenu"></ul>');
        	for (var i = 0; i < data.portfolios.length; i++) {
        		$('.js-portfolios>.list-pf-mainmenu').append('<li data-id="'+data.portfolios[i].subdomain+'"><a href="http://'+data.portfolios[i].subdomain+'.arcanemind.com/admin/portfolio.php">'+data.portfolios[i].subdomain+'.arcanemind.com  [Status : '+data.portfolios[i].status+']</a></li>');
                if(data.portfolios[i].courses.length>0) {
                    $('.js-portfolios>.list-pf-mainmenu li:last-child').append('<div class="pf-courses"></div>');
                    for (var j = 0; j < data.portfolios[i].courses.length; j++) {
                        $('.js-portfolios>.list-pf-mainmenu li:last-child .pf-courses').append('<label class="pf-co-single" for="optionCourse'+i+j+'"><input type="checkbox" id="optionCourse'+i+j+'" class="pf-co-single-check" data-course="'+data.portfolios[i].courses[j].value+'" '+((data.portfolios[i].courses[j].optionCourse>0)?'checked':'')+' />'+data.portfolios[i].courses[j].label+'</label>');
                    }
                }
				if(data.portfolios[i].status == 'Disapproved') {
                    $('.js-portfolios>.list-pf-mainmenu li:last-child').append('<div class="pf-resend" ></div>');
                    $('.js-portfolios>.list-pf-mainmenu li:last-child .pf-resend').append('<div class=" col-md-8 pf-reason"><b>Reason : </b>'+data.portfolios[i].reason+'</div><div class="pf-resendB"><button type="button" data-pid="'+data.portfolios[i].id+'" data-name="'+data.portfolios[i].name+'" data-description="'+data.portfolios[i].description+'" data-subdomain="'+data.portfolios[i].subdomain+'" class="btn btn-success  pf-resubmit">Modify and Re-Submit</button></div>');
                    
                }
        	};
        } else {
        	$('.js-portfolios').html('No portfolios created yet');
        }
    }
	$('.js-portfolios').on('click', '.pf-resubmit', function(){	
		/*var req = {};
        req.pfId       = $(this).attr('data-pid');
        var res;
        req.action = "re-submit-Portfolio";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
             if(res.status == 1) {
                    //$('#frmNewPortfolio').html('<div class="jumbotron text-center"><h3>'+res.message+'</h3></div>');
                    window.location = window.location;
              } else {
                    $('#frmNewPortfolio .help-block').html('<p class="text-danger text-center">'+res.message+'</p>');
               }
            console.log(res);
        });*/
		 pfId       = $(this).attr('data-pid');
		var PortfolioDescription      = $(this).attr('data-description');
		var inputSubdomain       = $(this).attr('data-subdomain');
		var pfname       = $(this).attr('data-name');
		//alert(inputSubdomain);
		$('#pfName').val(pfname);
		$('#pfDesc').val(PortfolioDescription);
		$('#pfsubdomain').val(inputSubdomain);
		$('#editPortfolio').modal('show'); 
		
		
	});
	
	$( "#editportfoliobtn" ).click(function() {
	 
	    var inputProfileName = trim($('#pfName').val());
        var inputPortfolioDescription = trim($('#pfDesc').val());
        var inputSubdomain = trim($('#pfsubdomain').val());        
        if(!inputProfileName) {
            $('#frmNewPortfolio .help-block').html('<p class="text-danger text-center">Portfolio Name can\'t be left empty</p>');
        } else if(!inputPortfolioDescription) {
            $('#frmNewPortfolio .help-block').html('<p class="text-danger text-center">Portfolio Description can\'t be left empty</p>');
        } else if(!inputSubdomain) {
            $('#frmNewPortfolio .help-block').html('<p class="text-danger text-center">Subdomain Name can\'t be left empty</p>');
        } else {
            var req = {};
			req.pfId = pfId;
            req.portfolioName = inputProfileName;
            req.portfolioDesc = inputPortfolioDescription;
            req.subdomain     = inputSubdomain;
            var res;
            req.action = "re-submit-Portfolio";
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if(res.status == 1) {
                    //$('#frmNewPortfolio').html('<div class="jumbotron text-center"><h3>'+res.message+'</h3></div>');
                    window.location = window.location;
                } else {
                    $('#editportfoliobtn .help-block').html('<p class="text-danger text-center">'+res.message+'</p>');
					return false;
                }
                console.log(res);
            });
        }
		
	});
	
    $('.js-portfolios').on('click', '.pf-co-single-check', function(){
        var subdomain = $(this).closest('li').attr('data-id');
        if (!!subdomain) {
            var req = {};
            req.course       = $(this).attr('data-course');
            req.optionCourse = (this.checked)?1:0;
            req.subdomain    = subdomain;
            var res;
            req.action = "save-portfolio-course";
            $.ajax({
                'type': 'post',
                'url': ApiEndpoint,
                'data': JSON.stringify(req)
            }).done(function (res) {
                res = $.parseJSON(res);
                if(res.status == 1) {
                    //alert('voila it works!');
                } else {
                    $('#frmNewPortfolio .help-block').html('<p class="text-danger text-center">'+res.message+'</p>');
                }
                console.log(res);
            });
        };
    });
    function fetchProfileDetails() {
        var req = {};
        var res;
        req.action = "get-profile-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillProfileDetails(res);
            //console.log(res);
        });
    }
    function fillProfileDetails(data) {
        // General Details
        var imageRoot = 'user-data/images/';
        
        if (data.profileDetails.profilePic != "") {
            $('#profilePic').attr('src', data.profileDetails.profilePic);
        }

        // Role Specific
        userRole = data.userRole;
        if (data.userRole == '1') {
            fillInstituteDetails(data);
        }
        else if (data.userRole == '2') {
            fillProfessorDetails(data);
        }
        else if (data.userRole == '3') {
            fillPublisherDetails(data);
        }
    }

    function fillInstituteDetails(data) {
        $('#inputProfileName').val(data.profileDetails.name);
        $('#frmNewPortfolio').removeClass('invisible');
    }

    function fillProfessorDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('#inputProfileName').val(name);
        $('#frmNewPortfolio').removeClass('invisible');
    }

    function fillPublisherDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('#inputProfileName').val(name);
        $('#frmNewPortfolio').removeClass('invisible');
    }
});