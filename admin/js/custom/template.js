$(function() {
	//fetchProfileDetails();
	fetchTemplate();
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fetchTemplate() {
		var req = {};
		var res;
		req.action = 'get-template';
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else
				fillTemplate(res);
		});
	}
	
	function fillTemplate(data) {
		var html = '';
		$('#viewTemplates').find('tbody tr').remove();
		for(var i=0; i<data.templates.length; i++) {
			html += '<tr data-id="' + data.templates[i].id + '">'
					+ '<td>' + data.templates[i].name + '</td>'
					+ '<td>' + data.templates[i].type + '</td>'
					+ '<td>Draft</td>'
					+ '<td>15-04-2014</td>'
					+ '<td>0</td>'
					+ '<td>';
					if(data.templates[i].id != 1 && data.templates[i].id != 2) {
						html += '<a href="edit-template.php?templateId=' + data.templates[i].id + '" title="" class="btn btn-primary btn-xs tooltips" data-original-title="Edit"><i class="fa fa-edit"></i></a>&emsp;'
						+ '<a title="" data-original-title="Delete" class="btn btn-danger btn-xs tooltips delete-button" data-toggle="tooltip"><i class="fa fa-trash-o "></i></a>';
					}
					html += '</td>'
				+ '</tr>';
		}
		$('#viewTemplates tbody').append(html);
		$('.delete-button').on('click', function() {
			var req = {};
			var res;
			req.action = "delete-template";
			req.templateId = $(this).parents('tr').attr('data-id');
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 1)
					fetchTemplate(res);
				else
					alert(res.message);
			});
		});
	}
});