/*	
	Author: Fraaz Hashmi.
	Dated: 6/25/2014
	
	
	********
	1) Notes: JSON.stringify support for IE < 10
*/

var jcrop_api;
var userRole;
var courseId;
$(function () {
	//disabling user input in dates
	$('#live-date, #end-date').on('keypress', function() {
		return false;
	});
	var imageRoot = '/';
	$('#loader_outer').hide();
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1'){
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
		
	function formatUserRole(roleId){
		if(roleId == '1')
			return "Institute";
		if(roleId == '2')
			return "Professor";
		if(roleId == '3')
			return "Publisher";
	}
	
	function validateForm() {
		if($('#course-name').parents('div:eq(0)').hasClass('has-error') && !(($('#course-name').val().length < 5 || $('#course-name').val().length >  70)) )
			return false;
              unsetError($('#course-name'));
               if($('#course-name').val().length==0){
                       setError($('#course-name'), 'Please enter course name.');
			return false; 
                    }
		if($('#course-name').val().length < 5 || $('#course-name').val().length >  70) {
                   setError($('#course-name'), 'Course name must be between 5 to 70 chars.');
			return false;
		}
		
		unsetError($('#course-subtitle'));
               if($('#course-subtitle').val() == '' ){
                   setError($('#course-subtitle'), 'Please enter course subtitle .');
			return false;
                }
		if( $('#course-subtitle').val().length > 140){
			setError($('#course-subtitle'), 'Course subtitle Must be less than 140 chars.');
			return false;
		}
		
		unsetError($('#course-description'));
                if($('#course-description').val() == ''){
                    setError($('#course-description'), 'Please enter course description .');
			return false;
                }
		if($('#course-description').val().length > 1000) {
			setError($('#course-description'), 'Course description can not be more than 1000 chars.');
			return false;
		}
                unsetError($('#target-audience'));
                if($('#target-audience').val() == ''){
                   setError($('#target-audience'),'Please enter target audience .');
			return false;
                }
		if( $('#target-audience').val().length > 200 ) {
			setError($('#target-audience'), 'Target audience description can not be more than 200  chars.');
			return false;
		}
		
		unsetError($('#live-date'));
		if($('#live-date').val() == "") {// || !isValidDate($('#live-date').val())) {
			setError($('#live-date'), 'Please provide a valid start date.');
			return false;
		}
		var today = new Date();
		today.setTime(today.getTime() - 86400000);
		if(new Date($('#live-date').val()).valueOf()< today.valueOf()) {// || !isValidDate($('#live-date').val())) {
			setError($('#live-date'), 'Please provide a valid start date.');
			return false;
		}
		
		unsetError($('#end-date'));
		if($('#set-end-date').val() == 1 && $('#end-date').val() == "") {// || !isValidDate($('#end-date').val()))) {
			setError($('#end-date'), 'Please provide a valid end date.');
			return false;
		}
		return true;
	}
	
	//fetchProfileDetails();
	
	//Event handlers
	$('#set-end-date').on('change', function() {
		if($('#set-end-date').val() == 0)
			$('#end-date').parents('.form-group').hide();
		else
			$('#end-date').parents('.form-group').show();
	});
	

	$('#create-course').on('click', function(e) {
		e.preventDefault();
		unsetError($('#create-course'));
		if(validateForm() == false)
		{
			setError($('#create-course'), 'Please fill the required fields.');
			return false;
		}
		var req = {};
		req.courseName = $('#course-name').val();
		req.courseSubtitle = $('#course-subtitle').val();
		req.courseDesc = $('#course-description').val();
		startDate = new Date($('#live-date').val());
		req.liveDate = startDate.getTime();
		req.setEndDate = $('#set-end-date').val();
		endDate = new Date($('#end-date').val());
		req.endDate = endDate.getTime();
		req.availStudentMarket = $('#avail-student-market').val();
		req.studentPrice = $('#student-price').val();
		req.courseCateg = $('#course-cat').val();
		req.targetAudience = '';
		req.tags = $('#tags').val();
		if($('#target-audience').val() != '')
			req.targetAudience = $('#target-audience').val();
		if(req.courseCateg == null)
			req.courseCateg = [];
		req.availContentMarket = $('#avail-content-market').val();
		req.licensingLevel = $('#licensing-level').val();
		req.portfolioSlug = $('#slug').val();
		req.pageUserId = $('#inputUserId').val();
		req.pageUserRole = $('#inputUserRole').val();
		req.action = 'create-course';
		
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				/*alertMsg(res.message);
				courseId=res.courseId;*/
				window.location = "courses.php";
			} else {
				alertMsg(res.msg);
			}
		});
	});

	//even handler to check duplicate course name
	$('#course-name').on('blur', function() {
		var req = {};
		var res;
		if($($('#course-name').parents('div:eq(0)')).hasClass('has-error'))
			unsetError($('#course-name'));
		req.action = 'check-duplicate-course';
		req.name = $('#course-name').val();
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else if(res.available == false) {
				setError($('#course-name'), 'Please select a different name as you have already created a course by this name.');
			}
		});
	});
        
    var startDate = new Date(1990, 1, 01);
    var endDate = new Date(2099, 1, 25);
    var now = new Date();
    var currentDate = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0);
    
    $('#live-date').datetimepicker({
        format: 'd F Y',
		timepicker: false,
		closeOnDateSelect: true,
        minDate: 0,
		maxDate: '2050/12/31',
		onSelectDate: function(date) {
			unsetError($('#live-date'));
			if(date.valueOf() > endDate.valueOf()) {
				setError($('#live-date'), 'The start date can not be greater than the end date.');
			}
			else {
				startDate = date;
			}
		}
    });
	$('#end-date').datetimepicker({
		format: 'd F Y',
		timepicker: false,
		closeOnDateSelect: true,
        minDate: 0,
		maxDate: '2050/12/31',
		onSelectDate: function(date) {
			unsetError($('#end-date'));
			if (date.valueOf() < startDate.valueOf()) {
				setError($('#end-date'), 'The end date can not be less than the start date.');
			} else {
				endDate = date;
			}
		}
	});
    /*$('#live-date').on('changeDate', function (ev) {
        unsetError($('#live-date'));
        if (ev.date.valueOf() > endDate.valueOf()) {
            setError($('#live-date'), 'The start date can not be greater then the end date.');

        } else {
            startDate = new Date(ev.date);
        }
		$('#end-date').attr('disabled', false);
    });*/

    /*$('#end-date').on('changeDate', function (ev) {
         unsetError($('#end-date'));
        if (ev.date.valueOf() < startDate.valueOf()) {
           setError($('#end-date'), 'The end date can not be less then the start date.');
        } else {
              endDate = new Date(ev.date);
           }
    });*/
	
	/*$("#course-image").change(function() {
           
            $('#create-course').prop('disabled',false);
                var input = this;
                if (input.files && input.files[0]) {
                     if(!(input.files[0]['type'] == 'image/jpeg' || input.files[0]['type'] == 'image/png' || input.files[0]['type'] == 'image/gif')) {
			$('#create-course').prop('disabled',true);
			$('#course-image').after('<span class="help-block" style="color:red;">Please select a proper file type.</span>');
			return;
		}
                    
			var reader = new FileReader();
			if (typeof jcrop_api != 'undefined' && jcrop_api != null) {
				jcrop_api.destroy();
				jcrop_api = null;
				var pImage = $('.crop');
				pImage.css('height', 'auto');
				pImage.css('width', 'auto');
				var height = pImage.height();
				var width = pImage.width();
				
				$('.jcrop').width(width);
				$('.jcrop').height(height);
			}
			reader.onload = function (e) {
				$('#course-image-preview').attr('src', e.target.result);
				$('.crop').Jcrop({
					onSelect: updateCoords,
					bgOpacity:   .4,
					boxWidth: 830,
					minSize: [280, 200],
					setSelect:   [0, 0, 280, 200],
					aspectRatio: 1109/450
                }, function () {
                    jcrop_api = this;
					jcrop_api.setSelect([0, 0, 280, 200]);
                });
			}
			reader.readAsDataURL(input.files[0]);
		}
    });
	
	function updateCoords(c) {
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
		var crop = [c.x, c.y, c.w, c.h];
		console.log(crop);
	}
	
	$('#course-image-form').attr('action', fileApiEndpoint);
	$('#course-image-form').on('submit', function(e) {
		e.preventDefault();
		c = jcrop_api.tellSelect();
		crop = [c.x, c.y, c.w, c.h];
		image = document.getElementById('course-image-preview');
		console.log(crop);
		var sources = {
            'course-image': {
                image: image,
                type: 'image/jpeg',
                crop: crop,
                size: [634, 150],
                quality: 1.0
            }
        }
        //settings for $.ajax function
        var settings = {
			url: fileApiEndpoint,
			data: {'text': 'This is the text!'}, //three fields (medium, small, text) to upload
			beforeSend: function()
			{
				console.log('sending image');
			},
			complete: function (resp) {
				console.log(resp.responseText);
				//window.location.href = "edit-course.php?courseId="+courseId;
			}
        }
        cropUploadAPI.cropUpload(sources, settings);
	});*/
	
	function setError(where, what) {
		unsetError(where);
		where.parents('div:eq(0)').addClass('has-error');
		where.parents('div:eq(0)').append('<span class="help-block">'+what+'</span>');
	}
	
	function unsetError(where) {
		if(where.parents('div:eq(0)').hasClass('has-error')) {
			where.parents('div:eq(0)').find('.help-block').remove();
			where.parents('div:eq(0)').removeClass('has-error');
		}
	}
});
