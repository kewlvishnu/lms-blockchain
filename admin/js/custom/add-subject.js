/*
	Author: Fraaz Hashmi.
	Dated: 6/27/2014
	
	
	********
	1) Notes: JSON.stringify support for IE < 10
*/
var userRole;
var subjectImageChanged = false;
var reorderTimer = null;

$(function () {
	var imageRoot = '/';
	$('#loader_outer').hide();
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
			console.log(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fetchCourseDetails() {
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action = "get-course-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			console.log(res);
			res =  $.parseJSON(res);
			fillCourseDetails(res);
		});
	}
	
	function fillCourseDetails(data) {
		// General Details
		if(data.courseDetails.deleted == 1)
			alertMsg('This course has been deleted!!', 20000);
		var imageRoot = 'user-data/images/';
		$('#course-name').html(data.courseDetails.name);
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fetchSubjects(){
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		req.action = "get-course-subjects";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			console.log(res);
			res =  $.parseJSON(res);
			fillSubjects(res);
		});
	}
	
	function fillSubjects(data) {
		var html = "";
		var flag = false;
		var editLink;
		var deletedClass = "";
		$.each(data.subjects, function(i, subject) {
			if(flag == false)
				flag = true;
			
			deletedClass = "";
			if(subject.deleted == 1) {
				deletedClass = " class='deleted-item'";
			}
			editLink = "edit-subject.php?courseId=" + subject.courseId + "&subjectId=" + subject.id;
			html += "<tr data-sid='" + subject.id + "'" + deletedClass + ">"
					+ "<td><h5><a href='" + editLink + "'>" + subject.name + "</a></h5>"
					//+ "&nbsp; &nbsp;<i class='fa fa-plus'></i> Professor ABC<br><a href='#'>&nbsp; &nbsp;Assign New Professor</a>"
					+ "</td>"
					+ "<td>"/*<a href='#trash-o'>"
					+ "<i class='fa fa-trash-o'></i>"
					+ "</a>*/+"</td>"
					+ "<td>S" + String("000" + subject.id).slice(-4) + "</td>";
					
			if(subject.deleted == 0) {
				html +=	"<td><a href='#' class='delete-subject'><i class='fa fa-trash-o'></i></a>"
						+ "<a href='#' class='restore-subject hidden-on-load'><i class='fa fa-refresh'></i></a></td>";
			} else {
				html +=	"<td><a href='#' class='delete-subject hidden-on-load'><i class='fa fa-trash-o'></i></a>"
						+ "<a href='#' class='restore-subject'><i class='fa fa-refresh'></i></a></td>";
			}
			html += "</tr>";
		});
		if(flag == true){
			$('#existing-subjects tbody').html(html);
                       	makeSubjectsSortable('#existing-subjects tbody');
			addEventHandlersForSubjects();
		}
                 else
                            $('#existing-subjects tbody').html('<tr><td>No subjects added yet </td></tr>');
		
	}
	
	function formatUserRole(roleId) {
		if(roleId == '1')
			return "Institute";
		if(roleId == '2')
			return "Professor";
		if(roleId == '3')
			return "Publisher";
	}
	
	function validateLicensingForm() {
		$('.form-msg').html('').hide();
		var licOpt1 = $('#apply-licensing #lic-opt-1').prop('checked');
		var licOpt2 = $('#apply-licensing #lic-opt-2').prop('checked');
		var licOpt3 = $('#apply-licensing #lic-opt-3').prop('checked');
		var licOpt4 = $('#apply-licensing #lic-opt-4').prop('checked');
		if(licOpt1 == false && licOpt2 == false && licOpt3 == false && licOpt4 == false){
			$('.form-msg.licensing').html('Please apply licensing').show();
			return false;
		}
		var licVal1 = $('#apply-licensing #lic-opt-1-price').val();
		var licVal2 = $('#apply-licensing #lic-opt-2-price').val();
		var licVal3 = $('#apply-licensing #lic-opt-3-price').val();
		var licVal4 = $('#apply-licensing #lic-opt-4-price').val();
		var licCommAmt4 = $('#apply-licensing #lic-opt-4-comm').val();
		var licCommPer4 = $('#apply-licensing #lic-opt-4-comm-per').val();
		if(licOpt1 == true &&  (licVal1 == "" || !isValidPrice(licVal1))){
			$('.form-msg.licensing-op-1').html('Please provide price for one year licensing').show();
			return false;
		}
		if(licOpt2 == true &&  (licVal2 == "" || !isValidPrice(licVal2))){
			$('.form-msg.licensing-op-2').html('Please provide price for one time transfer').show();
			return false;
		}
		if(licOpt3 == true &&  (licVal3 == "" || !isValidPrice(licVal3))){
			$('.form-msg.licensing-op-3').html('Please provide price for selective IP transfer').show();
			return false;
		}
		if(licOpt4 == true &&  (licVal4 == "" || !isValidPrice(licVal4))) {
			$('.form-msg.licensing-op-4').html('Please provide price for commission based licensing').show();
			return false;
		}
		if(licOpt4 == true &&  licCommPer4 == "" && licCommAmt4 == "") {
			$('.form-msg.licensing-op-4').html('Please provide commission').show();
			return false;
		}
		
		if(licOpt4 == true){
			if(licCommAmt4 != "" && licCommPer4 != "" ) {
				$('.form-msg.licensing-op-4').html('You can provide either percentage or amount').show();
				return false;
			}
			if(licCommAmt4 != "" && !isValidPrice(licCommAmt4)){
				$('.form-msg.licensing-op-4').html('Not a valid amount').show();
				return false;
			}
			if(licCommPer4 != "" && (!isValidPrice(licCommPer4) || licCommPer4 > 100)) {
				$('.form-msg.licensing-op-4').html('Not a valid precentage').show();
				return false;
			}
		}
		return true;
	}

	function makeSubjectsSortable(selector) {
		$(selector).sortable({
			items: '> tr',
			forcePlaceholderSize: true,
			placeholder:'sort-placeholder',
			start: function (event, ui) {
				// Build a placeholder cell that spans all the cells in the row
				var cellCount = 0;
				$('td, th', ui.helper).each(function () {
					// For each TD or TH try and get it's colspan attribute, and add that or 1 to the total
					var colspan = 1;
					var colspanAttr = $(this).attr('colspan');
					if (colspanAttr > 1) {
						colspan = colspanAttr;
					}
					cellCount += colspan;
				});

				// Add the placeholder UI - note that this is the item's content, so TD rather than TR
				ui.placeholder.html('<td colspan="' + cellCount + '">&nbsp;</td>');
				//$(this).attr('data-previndex', ui.item.index());
			},
			update: function(event, ui) {
				// gets the new and old index then removes the temporary attribute
				var newOrder = $.map($(this).find('tr'), function(el) {
					return $(el).attr('data-sid')+ "-" +$(el).index();
				});
				if(reorderTimer != null)
					clearTimeout(reorderTimer);
				reorderTimer = setTimeout(function(){updateSubjectOrder(newOrder);}, 5000);
			},
			helper: function(e, ui) {
				ui.children().each(function() {
					$(this).width($(this).width());
				});
				return ui;
			}
			
		}).disableSelection();
	}
	
	function updateSubjectOrder(newOrder) {
		var order = {};
		$.each(newOrder, function(i, v){
			order[v.split('-')[0]] = v.split('-')[1];
		});
		var req = {};
		var res;
		req.courseId = getUrlParameter('courseId');
		if(req.courseId == "" || req.courseId == null) {
			window.location.href="profile.php";
			return;
		}
		req.newOrder = order;
		req.action = "update-subject-order";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1)
				fetchSubjects();
			else
				alertMsg(res.message);
		});
	}
		
	//fetchProfileDetails();
	fetchCourseDetails();
	fetchSubjects();
	//Event handlers
	
	function addEventHandlersForSubjects() {
		$('#existing-subjects .delete-subject').on('click', function(e) {
			e.preventDefault();
			var con = confirm("Are you sure you want to delete this subject?");
			if(con) {
				var subject = $(this).parents('tr');
				var sid = subject.attr('data-sid');
				var req = {};
				req.action = "delete-subject";
				req.subjectId = sid;
				$.ajax({
					'type'  : 'post',
					'url'   : ApiEndpoint,
					'data' 	: JSON.stringify(req)
				}).done(function (res) {
					res =  $.parseJSON(res);
					if(res.status == 1) {
						/*subject.find('.delete-subject').hide();
						subject.find('.restore-subject').show();
						subject.addClass('deleted-item');*/
						subject.remove();
						if($('#existing-subjects tbody tr').length==0) {
							$('#existing-subjects tbody').html('<tr><td> No Subjects added yet </td></tr>');
						}
					} else {
						alertMsg(res.message);
					}
				});
			}
		});
		
		$('#existing-subjects .restore-subject').on('click', function(e) {
			e.preventDefault();
			var subject = $(this).parents('tr');
			var sid = subject.attr('data-sid');
			var req = {};
			req.action = "restore-subject";
			req.subjectId = sid;
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				res =  $.parseJSON(res);
				if(res.status == 1) {
					subject.find('.restore-subject').hide();
					subject.find('.delete-subject').show();
					subject.removeClass('deleted-item');
				} else {
					alertMsg(res.message);
				}
			});
		});
	}
	
	$('#create-subject').on('click', function(e) {
		e.preventDefault();
		$('#subject-description, #subject-name').blur();
		if($(document).find('.has-error').length > 0)
			return;
		var req = {};
		req.courseId = getUrlParameter('courseId');
		req.subjectName = $('#subject-name').val();
		req.subjectDesc = $('#subject-description').val();
		req.action = 'create-subject';
		
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done( function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				//alertMsg(res.message);
				$('#subject-name').val('');
				$('#subject-description').val('');
				$('#subject-created #subject-name').html(req.subjectName);
				$('#subject-created').modal('show');
                $('#add-chapter').on('click', function(e) {
                    window.location="content.php?courseId="+req.courseId+"&subjectId="+res.subjectId;
                });
				/*if(subjectImageChanged)
					$('#subject-image-form').submit();*/
				fetchSubjects();
			} else if(res.status == 2) {
				setError($('#subject-name'), res.message);
			}
			else {
				alertMsg(res.message);
			}
		});	
	});

	$('#subject-created #add-subject').on('click', function(e) {
		location.reload();
	});
	
	$('#apply-licensing button.save').on('click', function(e) {
		e.preventDefault();
		if(validateLicensingForm() == false)
			return;
		var req = {};
		req.courseId = getUrlParameter('courseId');
		var licOpt1 = $('#apply-licensing #lic-opt-1').prop('checked');
		var licOpt2 = $('#apply-licensing #lic-opt-2').prop('checked');
		var licOpt3 = $('#apply-licensing #lic-opt-3').prop('checked');
		var licOpt4 = $('#apply-licensing #lic-opt-4').prop('checked');
		var licVal1 = $('#apply-licensing #lic-opt-1-price').val();
		var licVal2 = $('#apply-licensing #lic-opt-2-price').val();
		var licVal3 = $('#apply-licensing #lic-opt-3-price').val();
		var licVal4 = $('#apply-licensing #lic-opt-4-price').val();
		var licCommAmt4 = $('#apply-licensing #lic-opt-4-comm').val();
		var licCommPer4 = $('#apply-licensing #lic-opt-4-comm-per').val();
		var data = {};
		var selectedLicensing = {};
		if(licOpt1 == true){
			selectedLicensing["1"] = {
				"price" : licVal1,
				"data" : data
			}
		}
		if(licOpt2 == true){
			selectedLicensing["2"] = {
				"price" : licVal2,
				"data" : data
			}
		}
		if(licOpt3 == true){
			selectedLicensing["3"] = {
				"price" : licVal3,
				"data" : data
			}
		}
		if(licOpt4 == true) {
			data = {
				"amount" : licCommAmt4,
				"percent" : licCommPer4
			};
				
			selectedLicensing["4"] = {
				"price" : licVal4,
				"data" : data
			}
		}
		
		req.selectedLicensing = selectedLicensing;
		
		req.action = 'update-course-licensing';
		
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done( function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				alertMsg(res.message);
				$('#apply-licensing').modal('hide');				
			} else {
				alertMsg(res.msg);
			}
		});
	});
	
	$('#apply-licensing button.cancel').on('click', function(e) {
		$('#apply-licensing').modal('hide');
	});
	
	/*$("#subject-image").change( function () {
		subjectImageChanged = true;
		var input = this;
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			if (typeof jcrop_api != 'undefined' && jcrop_api != null) {
				jcrop_api.destroy();
				jcrop_api = null;
				var pImage = $('.crop');
				pImage.css('height', 'auto');
				pImage.css('width', 'auto');
				var height = pImage.height();
				var width = pImage.width();
				
				$('.jcrop').width(width);
				$('.jcrop').height(height);
			}
			reader.onload = function (e) {
				$('#subject-image-preview').attr('src', e.target.result);
				$('.crop').Jcrop({
					onSelect: updateCoords,
					bgOpacity:   .4,
					setSelect:   [ 0, 0, 200, 200 ],
					aspectRatio: 1
				}, function() {
					jcrop_api = this;
					jcrop_api.setSelect([0, 0, 200, 200]);
				});
			}
			reader.readAsDataURL(input.files[0]);
		}
    });
	
	function updateCoords(c) {
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
		var crop = [c.x, c.y, c.w, c.h];
	}
	
	$('#subject-image-form').attr('action', fileApiEndpoint);
	
	$('#subject-image-form').on('submit', function(e) {
		e.preventDefault();
		c = jcrop_api.tellSelect();
		crop = [c.x, c.y, c.w, c.h];
		image = document.getElementById('subject-image-preview');
		var sources = {
            'subject-image': {
                image: image,
                type: 'image/jpeg',
                crop: crop,
                size: [200, 200],
                quality: 2.0
            }
        }
        //settings for $.ajax function
        var settings = {
			url: fileApiEndpoint,
			data: {'text': 'This is the text!'}, //three fields (medium, small, text) to upload
			beforeSend: function()
			{
				console.log('sending image...');
			},
			complete: function (resp) {
				console.log(resp.responseText);
				$('#subject-image').val('');
				$('.jcrop-holder').hide();
				$('#subject-image-preview').attr('src', '');
			}
        }
        cropUploadAPI.cropUpload(sources, settings);
	});*/
    
	//event for subject description
	$('#subject-description').on('blur', function() {
		unsetError($('#subject-description'));
		if($('#subject-description').val().length > 2000) {
			setError($('#subject-description'), 'Please enter a description less than 2000 characters.');
			return false;
		}
	});
	
    //event handler to check duplicate subject name
	$('#subject-name').on('blur', function() {
		var req = {};
		var res;
		unsetError($('#subject-name'));
		if($('#subject-name').val().length < 5 || $('#subject-name').val().length > 50) {
			setError($('#subject-name'), 'Please enter a name between 5 to 50 characters.');
			return false;
		}
		//commenting this code as per ARC-693
		/*else if(!($('#subject-name').parents('div:eq(0)').hasClass('has-error'))) {
			unsetError($('#subject-name'));
		}
		//now checking for duplicate name
		req.action = 'check-duplicate-subject';
		req.courseId = getUrlParameter('courseId');
		req.subjectName = $('#subject-name').val();
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else if(res.available == false) {
				setError($('#subject-name'), 'Please select a different name as you have already created a subject by this name.');
			}
			else
				unsetError($('#subject-name'));
		});*/
	});
	
	
	function setError(where, what) {
		unsetError(where);
		where.parents('div:eq(0)').addClass('has-error');
		where.parents('div:eq(0)').append('<span class="help-block">'+what+'</span>');
	}
	
	function unsetError(where) {
		if(where.parents('div:eq(0)').hasClass('has-error')) {
			where.parents('div:eq(0)').find('.help-block').remove();
			where.parents('div:eq(0)').removeClass('has-error');
		}
	}
});