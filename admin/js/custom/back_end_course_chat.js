var ApiEndpoint = '../api/index.php';
//var fileApiEndpoint = '../api/files1.php';

$(function () {
	$('#loader_outer').hide();
	loadbackCourseChats();
	function loadbackCourseChats() {
		var req = {};
		var res;
		req.action = "back_end_course_chats";
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			if (data.status == 1) {
				console.log(data);
				var html = "";
				for (var i = 0; i < data.chat_data.length; i++) {
					html+= '<tr data-course="'+data.chat_data[i].courseId+'">'+
								'<td>'+(i+1)+'</td>'+
								'<td>'+data.chat_data[i].courseId+'</td>'+
								'<td>'+data.chat_data[i].courseName+'</td>'+
								'<td>'+data.chat_data[i].name+'</td>'+
								'<td>'+
									((data.chat_data[i].chat==1)?'<button class="btn btn-success js-chat" data-value="ON">TURN OFF</button>':'<button class="btn btn-danger js-chat" data-value="OFF">TURN ON</button>')+
								'</td>'+
							'</tr>';
				};
				$('#all-courses-tbl tbody').html(html);
				
				var table = $('#all-courses-tbl').dataTable({
					"aaSorting": [[0, "asc"]],
					"bDestroy": true
				});
			} else {
				console.log(data.message);
			}

		});
	}
	$('#all-courses-tbl').on('click', '.js-chat', function(){
		var thiz = $(this);
		var req = {};
		var res;
		req.chat = $(this).attr("data-value");
		req.courseId = $(this).closest('tr').attr("data-course");
		req.action = "course-chat-feature-update";
		$.ajax({
			'type': 'post',
			'url': ApiEndpoint,
			'data': JSON.stringify(req)
		}).done(function (res) {
			data = $.parseJSON(res);
			if (data.status==1) {
				thiz.text((req.chat=="ON")?"TURN ON":"TURN OFF").removeClass("btn-danger btn-success").addClass((req.chat=="ON")?"btn-danger":"btn-success").attr("data-value",((req.chat=="ON")?"OFF":"ON"));
			} else {
				console.log(data.message);
			}
		});
		return false;
	});
});