/*
 Author: Fraaz Hashmi.
 Dated: 7/2/2014
 
 
 ********
 1) Notes: JSON.stringify support for IE < 10
 */
var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var userRole;

$(function () {
    var imageRoot = '/';
    $('#loader_outer').hide();

    function fetchProfileDetails() {
        var req = {};
        var res;
        req.action = "get-profile-details";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            fillProfileDetails(res);
            console.log(res);
        });
    }

    function fillProfileDetails(data) {
        // General Details
        var imageRoot = 'user-data/images/';
        $('#user-profile-card .username').text(data.loginDetails.username);

        if (data.profileDetails.profilePic != "") {
            $('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
        }

        // Role Specific
        userRole = data.userRole;
        if (data.userRole == '1') {
            fillInstituteDetails(data);
        }
        else if (data.userRole == '2') {
            fillProfessorDetails(data);
        }
        else if (data.userRole == '3') {
            fillPublisherDetails(data);
        }
    }

    function fillInstituteDetails(data) {
        $('h4#profile-name').text(data.profileDetails.name);
    }

    function fillProfessorDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('a#profile-name').text(name);
    }

    function fillPublisherDetails(data) {
        var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
        $('a#profile-name').text(name);
    }

    function fetchSubjects() {
        var req = {};
        var res;
        req.action = "get-all-deleted-subject";
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            res = $.parseJSON(res);
            if (res.status == 1)
                fillSubjects(res);
            else
                alertMsg(res.message);
        });
    }

    function fillSubjects(data) {
        var html = "";
        var editCourseLink, editSubjectLink, addSubjectLink;
        var institutenameId,coursenameId,subjectId;
        $.each(data.subjects, function (i, subject) {
           
          //  editCourseLink = "backend_edit-course.php?courseId=" + course.id;
            coursenameId = subject.course;
            subjectId='S00'+subject.subjectid;
            institutenameId = subject.username+' ('+subject.instituteid+')';
            html += "<tr data-cid='" + subject.subjectid + "'>"
                    + "<td>" +subjectId+ "</td>"
                    + "<td><h5><a href='#'>" + subject.subject + "</a></h5>";
            html += "</td>";
            html += "<td>" + coursenameId + "</td>";
            html += "<td>" + institutenameId + "</td>";
            html += "<td class=small-txt>" + subject.email + "</td>";
            html += "<td>" + subject.contactMobile + "</td>"
                    + "<td><a href='#' class='restore'><span class='label label-success label-mini' style='display:block;'>Restore</span></a></td>"
                    + "</td></tr>";
    });
         
            $('#all-subjects tbody').html(html);
//            $('#all-subjects').dataTable({
//            "aaSorting": [[0, "asc"]]
//         });
            addApprovalHandlers();
       
       

    }
    //fetchProfileDetails();
    fetchSubjects();
});

function addApprovalHandlers() {
    $('#all-subjects .restore').on('click', function (e) {
        e.preventDefault();
        var subject = $(this).parents('tr');
        var cid = subject.attr('data-cid');
        var req = {};
        req.action = "restore-subject-admin";
        req.subjectId = cid;
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
             res = $.parseJSON(res);
            if (res.status == 1) {
                subject.remove();
                // course.find('.approved').show();
            } else {
                alertMsg(res.message);
            }
        });
    });

    $('#all-courses .reject').on('click', function (e) {
        e.preventDefault();
        var course = $(this).parents('tr');
        var cid = course.attr('data-cid');
        var req = {};
        req.action = "reject-course";
        req.courseId = cid;
        $.ajax({
            'type': 'post',
            'url': ApiEndpoint,
            'data': JSON.stringify(req)
        }).done(function (res) {
            console.log(res);
            res = $.parseJSON(res);
            if (res.status == 1) {
                course.find('.approve, .reject').hide();
                course.find('.rejected').show();
            } else {
                alertMsg(res.message);
            }
        });
    });
}