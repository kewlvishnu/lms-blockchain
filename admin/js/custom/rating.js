$(function() {
	fetchCourseReviews();
});

/**
* funtion to fetch all reviews for all subjects of the courseDetails
*
* @author Rupesh Pandey
* @date 23/05/2015
*/
function fetchCourseReviews() {
	var req = {};
	var res;
	req.action = 'get-all-reviews-for-course';
	req.courseId = getUrlParameter('courseId');
	$.ajax({
		type: "post",
		url: ApiEndpoint,
		data: JSON.stringify(req) 
	}).done(function(res) {
		res = $.parseJSON(res);
		var html = '';
		var c1=c2=c3=c4=c5=0;
		var rateTotal = 0;
		$.each(res.reviews, function(i, review) {
			//counting reviews
			switch(review.rating) {
				case '1.00':
					c1++;
					break;
				case '2.00':
					c2++;
					break;
				case '3.00':
					c3++;
					break;
				case '4.00':
					c4++;
					break;
				case '5.00':
					c5++;
					break;
			}
			rateTotal += parseFloat(review.rating);
			var date = '';
			var dateobj = new Date(parseInt(review.time + '000'));
			date = format(dateobj.getDate()) + ' ' + MONTH[dateobj.getMonth()] + ' ' + dateobj.getFullYear() + ' ' + format(dateobj.getHours()) + ':' + format(dateobj.getMinutes());
			html += '<li class="comment-item">'
						+ '<div class="row">'
							+ '<div class="col-sm-3">'
								+ '<div class="user-block">'
									+ '<div class="user-avatar">'
										+ '<img src="' + review.studentImage + '" alt="">'
									+ '</div>'
									+ '<span>' + review.studentName + '</span>'
								+ '</div>'
							+ '</div>'
							+ '<div class="col-sm-9">'
								+ '<div class="rating">'
									+ '<input type="hidden" class="studentRating" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="' + review.rating + '" DISABLED/>'
									+ '<span class="">&emsp;' + date + '</span>';
								//if(subjectCount > 1)
									html += '<span class="subject"> (Subject: ' + review.subjectName + ')</span>';
								html += '</div>'
								+ '<h4>' + review.title + '</h4>'
								+ '<div class="comment">'
									+ '<p>' + review.review + '</p>'
								+ '</div>'
							+ '</div>'
						+ '</div>'
					+ '</li>';
		});
		$('#comments ul.list-comments').append(html);
		$('.studentRating').rating();
		var total = c1 + c2 + c3 + c4 + c5;
		$('.ratingTotal').text(total + ' Ratings');
		if(total == 0) {
			$('#comments ul').html('<li class="comment-item"><div class="user-block"><span style="margin-left: 0px;">No Reviews Found</span></div></li>');
			avg = 0;
		}
		else
			avg = (rateTotal / total).toFixed(1);
		$('.avg-rate').text(avg);
		$('.ratingStars').rating('rate', avg);
		$('.progress1').css('width', percentage(c1, total));
		$('.progress2').css('width', percentage(c2, total));
		$('.progress3').css('width', percentage(c3, total));
		$('.progress4').css('width', percentage(c4, total));
		$('.progress5').css('width', percentage(c5, total));
		$('.count1').text(c1);
		$('.count2').text(c2);
		$('.count3').text(c3);
		$('.count4').text(c4);
		$('.count5').text(c5);
	});
}

/**
* Funtion to format single digit number into double digit i.e. append one before item
*
* @author Rupesh Pandey
* @date 23/05/2015
* @param integer input number
* @return integer two digit number
*/
function format(number) {
	if(number < 10)
		return '0' + number;
	else
		return number;
}

function percentage(number, total) {
	if(total == 0)
		return 0;
	return (parseInt(number)/parseInt(total)) * 100;
}