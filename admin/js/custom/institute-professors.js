/*
	Author: Fraaz Hashmi.
	Dated: 7/3/2014
		
	********
	1) Notes: JSON.stringify support for IE < 10
*/
var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var userRole;

$( document ).ajaxStart(function() {
  $('#loader_outer').show();
});

$( document ).ajaxComplete(function() {
  $('#loader_outer').hide();
});

$(function () {
	$('a.professors').addClass('active');
	var imageRoot = '/';
	$('#loader_outer').hide();
	
	function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			fillProfileDetails(res);
			console.log(res);
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		
		if(data.profileDetails.profilePic != "") {
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1') {
			fillInstituteDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
		}
	}
	
	function fillInstituteDetails(data) {
		$('h4#profile-name').text(data.profileDetails.name);
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
	}
	
	function fetchInstituteProfessors() {
		var req = {};
		var res;
		req.action = "get-institute-prof-with-sub";
		
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			console.log(res);
			res =  $.parseJSON(res);
			if(res.status == 1)
				fillInstituteProfessors(res);
			else
				alertMsg(res.message);
		});
	}
	
	function fillInstituteProfessors(data) {
		var html = "";
		var count = 0;
		if(data.instituteProfessors.length == 0)
			html = '<p>There are no professors currently associated with you</p>';
		$.each(data.instituteProfessors, function(i, prof) {
			html += "<tr>"
					+ "<td><h5><a href='../instructor/" + prof.professorId + "' target='_blank'>" + prof.firstName + " " + prof.lastName + "</a></h5>";
		if(prof.subjects != undefined)
			{			
				$.each(prof.subjects, function(i, subject) {
					if(count++ == 0)
						html += "&nbsp;<b>Subject: </b>" +  subject.subjectName +  " (<b>Course: </b>" + subject.courseName + ")<br />";
					else
						html += "&nbsp;" +  subject.subjectName +  " (" + subject.courseName + ")<br />";
				});
			}
			html += "</td>"
					+ "<td class=small-txt>" + prof.email + "</td>"
					+ "<td>" + formatContactNumber(prof) + "</td>"
					+ "</tr>";
		});
		$('#institute-professors tbody').html(html);
	}
	
	function validateForm() {
		$('.form-msg').html('').hide();
		if($('#chapter-name').val().length < 5 || $('#chapter-name').val().length > 100) {
			$('.form-msg.chapter-name').html('Invalid chapter name. Must be between 5 to 100 chars.').show();
			return false;
		}
		
		if($('#chapter-description').val().length > 1000) {
			$('.form-msg.chapter-desc').html('Can not be more than 1000 chars.').show();
			return false;
		}
		return true;
	}
	
	//fetchProfileDetails();
	fetchInstituteProfessors();
	
	//Event handlers
	
	$('#create-chapter').on('click', function(e) {
		e.preventDefault();
		if(validateForm() == false)
			return;
		var req = {};
		req.courseId = getUrlParameter('courseId');
		req.subjectId = getUrlParameter('subjectId');
		
		req.chapterName = $('#chapter-name').val();
		req.chapterDesc = $('#chapter-description').val();
		req.action = 'create-chapter';
		
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done( function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				//alertMsg(res.message);
				$('#chapter-name').val('');
				$('#chapter-description').val('');
				$('#chapter-created #chapter-name').html(req.subjectName);
				$('#chapter-created').modal('show');
				fetchSubjectChapters();
			} else {
				alertMsg(res.msg);
			}
		});	
	});

	function formatContactNumber(data) {
		var mobile = (data.contactMobile != '')?(data.contactMobilePrefix + '-' + data.contactMobile):'';
		var landline = (data.contactLandline != '')?(data.contactLandlinePrefix + '-' + data.contactLandline):'';
		if(mobile != '' && landline != '') {
			return mobile + ', ' + landline;
		}
		else if(mobile == '') {
			return landline;
		}
		else
			return mobile;
	}
});
