/*
	Author: Fraaz Hashmi.
	Dated: 6/19/2014
	
	
	********
	1) Notes: JSON.stringify support for IE < 10
*/

var jcrop_api;
var userRole;
var ApiEndPoint = '../api/index.php';
var newSelectedInstitutes = {};
$( document ).ajaxStart(function() {
  $('#loader_outer').show();
});

$( document ).ajaxComplete(function() {
  $('#loader_outer').hide();
});
$(function () {
//	var imageRoot = '/';
//	$('#loader_outer').hide();
//	var fileApiEndpoint = '../api/files1.php';
//	
	fetchUserDetails();
	//function to fetch user details for change password page
	function fetchUserDetails() {
		var req = {};
		var res;
		req.action = 'get-student-details-for-changePassword';
		$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndPoint,
				'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				alert(res.message);
			else {
				details = res;
				fillUserDetails(res);
			}
		});
	}
	
	//function to fill user details
	function fillUserDetails(data) {
		$('.fullName').text(data.details.firstName + ' ' + data.details.lastName);
		$('.username').text(data.details.username);
		$('.user-image').attr('src', data.details.profilePic);
		$('#btn_rupee, #btn_dollar').removeClass('btn-white btn-info');
		if(data.details.currency == 1) {
			$('#btn_dollar').addClass('btn-info');
			$('#btn_rupee').addClass('btn-white');
		}
		else if(data.details.currency == 2) {
			$('#btn_dollar').addClass('btn-white');
			$('#btn_rupee').addClass('btn-info');
		}
	}
	/*function fetchProfileDetails() {
		var req = {};
		var res;
		req.action = "get-profile-details";
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			
			res =  $.parseJSON(res);
			fillProfileDetails(res);
			console.log(res);
			// }catch(e) {
				// alertMsg(e);
			// }
		});
	}
	
	function fillProfileDetails(data) {
		// General Details
		var imageRoot = 'user-data/images/';
		$('#user-profile-card .username').text(data.loginDetails.username);
		$('#user-type').text(formatUserRole(data.userRole));
		if(data.profileDetails.coverPic != "")
			$('img#cover-pic').attr('src', data.profileDetails.coverPic);
		if(data.profileDetails.profilePic != "") {
			$('img#profile-pic').attr('src', data.profileDetails.profilePic);
			$('#user-profile-card .user-image').attr('src', data.profileDetails.profilePic);
		}
		$('#user-profile-cover').show();
		if(data.profileDetails.description != ""){
			$('#user-bio p.text').text(data.profileDetails.description);
			$('#user-bio textarea').val(data.profileDetails.description);
		}
		else
			$('#user-bio p.text').text("A short description.");
		$('#user-bio').show();
		var html = '';
		for(var i = 0; i < data.courses.length; i++) {
			html += '<div class="panel-body bio-graph-info">'
					+ '<div class="row">'
						+ '<div class="col-lg-3">'
							+ '<label>Course:</label> ' + data.courses[i].name
						+ '</div>'
						//+ '<div class="col-lg-3"><a href="edit-course.php?courseId='+data.courses[i].id+'">View Details</a></div>'
					+ '</div>'
					+ '<div class="row">'
						+ '<div class="col-lg-3"><label>Subjects</label></div>'
					+ '</div>';
			for(var j = 0; j < data.courses[i].subjects.length; j++) {
				html += '<div class="row">'
						+ '<div class="col-lg-12">'
							+ data.courses[i].subjects[j].name;// + ' : ';
				/*for(var k = 0; k < data.courses[i].subjects[j].professors.length; k++) {
					html += data.courses[i].subjects[j].professors[k].name + ',';
				}
				html += '</div></div>';
			}
			html += '</div>';
		}
		$('#user-am-experience').append(html);
		$('#user-am-experience').show();
		// Role Specific
		userRole = data.userRole;
		if(data.userRole == '1'){
			fillInstituteDetails(data);
			fillInstituteFormDetails(data);
		}
		else if(data.userRole == '2'){
			fillProfessorDetails(data);
			fillProfessorFormDetails(data);
		}
		else if(data.userRole == '3') {
			fillPublisherDetails(data);
			fillPublisherFormDetails(data);
		}
		if(data.userInstituteTypes !== false){
			$('#user-institute-types .view-section').html(formatUserInstituteTypes(data.userInstituteTypes));
			$('#user-institute-types').show();
		}
	}*/
	
	function fillInstituteDetails(data) {
		
		$('#user-institute-types .top-heading').html('Select Institute types');
		$('#user-institute-types .other-categories label').html('Training Institutes');
		
		$('a#profile-name').text(data.profileDetails.name);
		
		$('#institute-general-information span#institute-name').text(data.profileDetails.name);
		var ownerName = data.profileDetails.ownerFirstName + " " + data.profileDetails.ownerLastName;
		if(data.profileDetails.ownerFirstName == '' && data.profileDetails.ownerLastName == '')
			ownerName = "Not Specified";
		$('#institute-general-information span#owner-name').text(ownerName);
		$('#institute-general-information span#contact-name').text(data.profileDetails.contactFirstName + " " + data.profileDetails.contactLastName);
		
		$('#institute-general-information span#institute-size').text(data.profileDetails.studentCount);
		if(data.profileDetails.studentCount == '')
			$('#institute-general-information span#institute-size').text('Not Specified.');
		
		var year = data.profileDetails.foundingYear;
		year = year != 0 ? year : 'Not set';
		$('#institute-general-information span#founded-in').text(year);
		$('#institute-general-information span#contact-num').text(formatContactNumber(data.userDetails));
		$('#institute-general-information span#primary-email').text(data.loginDetails.email);
		$('#institute-general-information span#address-country').text(data.userDetails.addressCountry);
		$('#institute-general-information span#address-details').text(formatAddress(data.userDetails));
		$('#institute-general-information').show();
	}
	
	function fillInstituteFormDetails(data) {
		$('#institute-general-information #institute-name-inp').val(data.profileDetails.name);
		
		$('#institute-general-information #owner-first-name-inp').val(data.profileDetails.ownerFirstName);
		$('#institute-general-information #owner-last-name-inp').val(data.profileDetails.ownerLastName);
		
		$('#institute-general-information #contact-first-name-inp').val(data.profileDetails.contactFirstName);
		$('#institute-general-information #contact-last-name-inp').val(data.profileDetails.contactLastName);
		
		$('#institute-general-information #institute-size-inp').val(data.profileDetails.studentCount);
		
		var year = data.profileDetails.foundingYear;
		$('#institute-general-information #founded-in-inp').val(year);
		
		
		$('#institute-general-information #phnNumPrefix').val(data.userDetails.contactMobilePrefix);
		$('#institute-general-information #phnNum').val(data.userDetails.contactMobile);
		$('#institute-general-information #llNumPrefix').val(data.userDetails.contactLandlinePrefix);
		$('#institute-general-information #llNum').val(data.userDetails.contactLandline);
		
		//$('span#address-country').value(data.userDetails.addressCountryId);
		
		$('#institute-general-information #street-inp').val(data.userDetails.addressStreet);
		$('#institute-general-information #city-inp').val(data.userDetails.addressCity);
		$('#institute-general-information #state-inp').val(data.userDetails.addressState);
		$('#institute-general-information #pin-inp').val(data.userDetails.addressPin);
		
		
		//$('#user-institute-types').html(formatUserInstituteTypes(data.userInstituteTypes));
		
	}
	
	function fillProfessorDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
		var gender = "Not Specified.";
		if(data.profileDetails.gender == 1)
			gender = "Male";
		if(data.profileDetails.gender == 2)
			gender = "Female";
		$('#professor-general-information span#gender').text(gender);
		$('#professor-general-information span#professor-name').text(name);
		
		$('#professor-general-information span#contact-num').text(formatContactNumber(data.userDetails));
		$('#professor-general-information span#primary-email').text(data.loginDetails.email);
		$('#professor-general-information span#address-country').text(data.userDetails.addressCountry);
		$('#professor-general-information span#address-details').text(formatAddress(data.userDetails));
		
		$('#professor-general-information').show();
	}
	
	function fillProfessorFormDetails(data) {
		
		$('#professor-general-information #first-name-inp').val(data.profileDetails.firstName);
		$('#professor-general-information #last-name-inp').val(data.profileDetails.lastName);
		
		$('#professor-general-information #phnNumPrefix').val(data.userDetails.contactMobilePrefix);
		$('#professor-general-information #phnNum').val(data.userDetails.contactMobile);
		$('#professor-general-information #llNumPrefix').val(data.userDetails.contactLandlinePrefix);
		$('#professor-general-information #llNum').val(data.userDetails.contactLandline);
		
		//$('span#address-country').value(data.userDetails.addressCountryId);
		
		$('#professor-general-information #street-inp').val(data.userDetails.addressStreet);
		$('#professor-general-information #city-inp').val(data.userDetails.addressCity);
		$('#professor-general-information #state-inp').val(data.userDetails.addressState);
		$('#professor-general-information #pin-inp').val(data.userDetails.addressPin);
		
		//for gender selection
		$('#professor-general-information #gender-inp').val(data.profileDetails.gender);
		//$('#user-institute-types').html(formatUserInstituteTypes(data.userInstituteTypes));
		
	}
	
	function fillPublisherDetails(data) {
		var name = data.profileDetails.firstName + " " + data.profileDetails.lastName;
		$('a#profile-name').text(name);
		var gender="Not Specified.";
		if(data.profileDetails.gender == 1)
			gender = "Male";
		if(data.profileDetails.gender == 2)
			gender = "Female";
		$('#publisher-general-information span#gender').text(gender);
		$('#publisher-general-information span#publisher-name').text(name);
		$('#publisher-general-information span#owner-name').text(name);
		$('#publisher-general-information span#contact-person').text(name);
		
		var year = data.profileDetails.foundingYear;
		year = year != 0 ? year : 'Not set';
		$('#publisher-general-information span#founded-in').text(year);
		$('#publisher-general-information span#contact-num').text(formatContactNumber(data.userDetails));
		$('#publisher-general-information span#primary-email').text(data.loginDetails.email);
		$('#publisher-general-information span#address-country').text(data.userDetails.addressCountry);
		$('#publisher-general-information span#address-details').text(formatAddress(data.userDetails));
		
		$('#publisher-general-information').show();
	}
	
	function fillPublisherFormDetails(data) {
		$('#publisher-general-information #first-name-inp').val(data.profileDetails.firstName);
		$('#publisher-general-information #last-name-inp').val(data.profileDetails.lastName);
		
		var year = data.profileDetails.foundingYear;
		$('#publisher-general-information #founded-in-inp').val(year);
		
		
		$('#publisher-general-information #phnNumPrefix').val(data.userDetails.contactMobilePrefix);
		$('#publisher-general-information #phnNum').val(data.userDetails.contactMobile);
		$('#publisher-general-information #llNumPrefix').val(data.userDetails.contactLandlinePrefix);
		$('#publisher-general-information #llNum').val(data.userDetails.contactLandline);
		
		//$('span#address-country').value(data.userDetails.addressCountryId);
		
		$('#publisher-general-information #street-inp').val(data.userDetails.addressStreet);
		$('#publisher-general-information #city-inp').val(data.userDetails.addressCity);
		$('#publisher-general-information #state-inp').val(data.userDetails.addressState);
		$('#publisher-general-information #pin-inp').val(data.userDetails.addressPin);
		
		//for gender selection
		$('#publisher-general-information #gender-inp').val(data.profileDetails.gender);
		
		//$('#user-institute-types').html(formatUserInstituteTypes(data.userInstituteTypes));
		
	}
	
	function formatUserRole(roleId) {
		if(roleId == '1')
			return "Institute";
		if(roleId == '2')
			return "Professor";
		if(roleId == '3')
			return "Publisher";
	}
	
	function formatAddress(user) {
		var address = '';
		if(user.addressStreet != '')
			address += user.addressStreet + ', ';
		if(user.addressCity != '')
			address += user.addressCity + ', ';
		if(user.addressState != '')
			address += user.addressState + ', ';
		if(user.addressPin != '')
			address += user.addressPin + ', ';
		ret = address.replace(/,\s$/, '.');
		if(ret == '')
			return 'Not Specified.';
		else
			return ret;
	}
	
	function formatUserInstituteTypes(insTypes) {
		var arrangedIns = {};
		$.each(insTypes, function(i,ins) {
			if(ins.parentId == 0 ) {
				if(typeof arrangedIns[ins.id] !== "undefined" ) {
					var children = arrangedIns[ins.id]['children'];
					arrangedIns[ins.id] = {
						'data': ins.data,
						'name': ins.name,
						'children': children
					}
				}else {
					arrangedIns[ins.id] = {
						'data': ins.data,
						'name': ins.name,
						'children': {}
					}
				}
			}else {
				if(typeof arrangedIns[ins.parentId] == "undefined") {
					arrangedIns[ins.parentId] = {
						'children' : {},
					};
				}
				
				arrangedIns[ins.parentId]['children'][ins.id] = {
					'data': ins.data,
					'name': ins.name,
					'children': {}
				}
			}
		});
		console.log(arrangedIns);
		var html = "";
		var data;
		if(typeof arrangedIns['1'] !== "undefined") {
			var ins = arrangedIns['1'];
			data = $.parseJSON(ins.data);
			html += '<div class="panel-body bio-graph-info margin-top-20">'
					+ '<h1>' + ins.name + '</h1>'
					+ '<div class="row">'
					+ '<div class="col-lg-6 three-row">' + formatSchoolBoard(data) + '</div>'
					+ '<div class="col-lg-6 three-row">' + formatSchoolClass(data) + '</div>'
					+ "</div></div>";
		}
		
		if(typeof arrangedIns['2'] !== "undefined") {
			var ins = arrangedIns['2'];
			html += '<div class="panel-body bio-graph-info margin-top-20">'
					+ '<h1>' + ins.name + '</h1>'
					+ '<div class="row">'
					+ '<div class="col-lg-12 three-row">' + formatCoaching(ins, arrangedIns) + '</div>'
					+ "</div></div>";
		}
		
		if(typeof arrangedIns['3'] !== "undefined") {
			var ins = arrangedIns['3'];
			html += '<div class="panel-body bio-graph-info margin-top-20">'
					+ '<h1>' + ins.name + '</h1>'
					+ '<div class="row">'
					+ '<div class="col-lg-12 three-row">' + formatColleges(ins) + '</div>'
					+ "</div></div>";
		}
		
		if(typeof arrangedIns['4'] !== "undefined") {
			var ins = arrangedIns['4'];
			var name =  userRole == 1 ? "Training Institute": "Other Categories";
			html += '<div class="panel-body bio-graph-info margin-top-20">'
					+ '<h1>' + name + '</h1>'
					+ '<div class="row">'
					+ '<div class="col-lg-12 three-row">' + formatTrainingIns(ins) + '</div>'
					+ "</div></div>";
		}
		handleInstituteTypeInputs(arrangedIns);
		return html;
	}
	
	function formatSchoolBoard(data) {
		if(data.boardId == '5')
			return "Board: CBSE";
		if(data.boardId == '6')
			return "Board: ICSE";
		if(data.boardId == '7')
			return "Board: State Board(" + data.state + ")";
		if(data.boardId == '0')
			return "Board: " + data.other;
		return " ";
	}
	
	function formatSchoolClass(data) {
		return "Classes upto " + data.classes + "th" ;
	}
	
	function formatColleges(ins) {
		var html = "";
		$.each(ins.children, function(k,v) {
			html += v.name + ', ';
		});
		data = $.parseJSON(ins.data);
		var others = data.other;
		if( others != "" && others != undefined)
			html += others;
		return html.replace(/,\s$/,'.');
	}
	
	function formatTrainingIns(ins) {
		var html = "";
		$.each(ins.children, function(k,v) {
			html += v.name + ', ';
		});
		data = $.parseJSON(ins.data);
		var others = data.other;
		if( others != "" && others != undefined)
			html += others;
		return html.replace(/,\s$/,'.');
	}
	
	function formatCoaching(ins, arrangedIns) {
		var html = "";
		var others;
		$.each(ins.children, function(k,v) {
			html += "<div><b>" + v.name + ': </b>';
			if(typeof arrangedIns[k] !== "undefined"){
				$.each(arrangedIns[k].children, function(subK,subC) {
					html += subC.name + ", ";
				});
			}
			data = $.parseJSON(v.data);
			others = data.other;
			if( others != "" && others != undefined)
				html += others;
			html = html.replace(/,\s$/, '.') + "</div>";
		});
		data = $.parseJSON(ins.data);
		others = data.other;
		if( others != "" && others != undefined)
			html += "<div> <b>" + others + "</b></div>";
		return html;
	}
	
	function prepareInsDataKey(k) {
		if(k == 'other')
			return '';
		else
			return k + ': ';
	}
	
	//fetchProfileDetails();
	
	function updateInstituteTypes(elem) {
		$('.form-msg').html('').hide();
		var newStructuredInstitutes = {};
		var board;
		if(typeof newSelectedInstitutes[1] == "undefined"
			&& typeof newSelectedInstitutes[2] == "undefined" 
			&& typeof newSelectedInstitutes[3] == "undefined" 
			&& typeof newSelectedInstitutes[4] == "undefined") {
				$('.form-msg.overall').html('Please select a category').show();
				return ;
		}
		if(typeof newSelectedInstitutes[1] !== "undefined"){
			board = $('#school-board').val();
			state = $('#school-board-state input[type=text]').val();
			otherBoard = $('#school-board-other input[type=text]').val();
			classes = $('#school-classes').val();
			if(board == 0){
				$('.form-msg.board').html('Please select a board').show();
				return;
			}
			if(board == 7 && state.length < 2) {
				$('.form-msg.board-state').html('Invalid state name.').show();
				return;
			}
			if(board == -1 && otherBoard == "") {
				$('.form-msg.board-other').html('Please specify a board.').show();
				return;
			}
			if(classes == 0) {
				$('.form-msg.classes').html('Please select classes.').show();
				return;
			}
			//Validated
			if(board == 7) {
				data = {
					"boardId" : "7",
					"state" : state
				};
			} else if(board == -1) {
				data = {
					"boardId" : "0",
					"other" : otherBoard
				};
			} else {
				data = {
					"boardId" : board
				};
			}
			data["classes"] = classes;
			newStructuredInstitutes[1] = data;
		}
		if(typeof newSelectedInstitutes[2] !== "undefined") {
			selectedSubcat = getSelectedSubcat(2);
			if(countDictKeys(selectedSubcat) == 0) {
				$('.form-msg.coaching').html('Please select a coaching type.').show();
				return;
			}
			newStructuredInstitutes[2] = {};
			inner = true;
			$.each(selectedSubcat, function(k,v) {
				selectedSubcat2 = getSelectedSubcat(k);
				if(countDictKeys(selectedSubcat2) == 0
					&& typeof newSelectedInstitutes[k]["other"] == "undefined") {
					$('.form-msg.' + k + '-msg').html('Please select a type.').show();
					inner = false;
				}
				if(typeof newSelectedInstitutes[k]["other"] !== "undefined"
					&& $('#ins-' + k + '-other input[type=text]').val() == "") {
					$('.form-msg.' + k + '-msg').html('Please specify a new type.').show();
					inner = false;
				}
				if(typeof newSelectedInstitutes[k]["other"] === "undefined") {
					newStructuredInstitutes[k] = {};
				} else {
					newStructuredInstitutes[k] = {
						"other" : $('#ins-' + k + '-other input[type=text]').val()
					};
				}
				$.each(selectedSubcat2, function(subK,subV) {
					newStructuredInstitutes[subK] = subV;
				});
			});
			if(!inner)
				return;
		}
		if(typeof newSelectedInstitutes[3] !== "undefined") {
			selectedSubcat = getSelectedSubcat(3);
			if(countDictKeys(selectedSubcat) == 0 && typeof newSelectedInstitutes[3]["other"] == "undefined") {
				$('.form-msg.college').html('Please select a college type.').show();
				return;
			}
			if(typeof newSelectedInstitutes[3]["other"] !== "undefined"
				&& $('#ins-3-other input[type=text]').val() == "") {
				$('.form-msg.college-other').html('Please specify new college type.').show();
				return;
			}
			if(typeof newSelectedInstitutes[3]["other"] === "undefined") {
				newStructuredInstitutes[3] = {};
			} else {
				newStructuredInstitutes[3] = {
					"other" : $('#ins-3-other input[type=text]').val()
				};
			}
			$.each(selectedSubcat, function(k,v){
				newStructuredInstitutes[k] = v;
			});
		}
		if(typeof newSelectedInstitutes[4] !== "undefined") {
			selectedSubcat = getSelectedSubcat(4);
			if(countDictKeys(selectedSubcat) == 0 && typeof newSelectedInstitutes[4]["other"] == "undefined") {
				$('.form-msg.training').html('Please select a training type.').show();
				return;
			}
			if(typeof newSelectedInstitutes[4]["other"] !== "undefined"
				&& $('#ins-4-other input[type=text]').val() == "") {
				$('.form-msg.training-other').html('Please specify new training type.').show();
				return;
			}
			if(typeof newSelectedInstitutes[4]["other"] === "undefined") {
				newStructuredInstitutes[4] = {};
			} else {
				newStructuredInstitutes[4] = {
					"other" : $('#ins-4-other input[type=text]').val()
				};
			}
			$.each(selectedSubcat, function(k,v){
				newStructuredInstitutes[k] = v;
			});
		}
		console.log(newStructuredInstitutes);
		var req = {};
		req.newStructuredInstitutes = newStructuredInstitutes;
		req.action = "update-user-institute-type";
		elem.attr('disabled',true);
		$.ajax({
			'type'  : 'post',
			'url'   : ApiEndpoint,
			'data' 	: JSON.stringify(req)
		}).done(function (res) {
			res =  $.parseJSON(res);
			if(res.status == 1) {
				//fetchProfileDetails();
				alertMsg(res.message);
				elem.parents('.panel').find('.edit-section').hide();
				elem.parents('.panel').find('.view-section').fadeIn('fast');
			}else {
				alertMsg(e);
			}
			elem.attr('disabled',false);
			elem.parents('section.panel').find('.fa-times').removeClass('fa-times').addClass('fa-pencil');
		});
	}
	
	

function alertMsg(m) {
	alert(m);
}

function getSelectedSubcat(catId) {
	var catHierarchy = {
		"1": [5,6,7],
		"2": [8,9,10,11,12],
		"3": [46,47,48,49,50,51,52,53],
		"4": [54,55,56,57,58,59,60,61,62,63],
		"8": [13,14,15,16,17],
		"9": [18,19,21,22],
		"10": [23,24,25,26,27],
		"11": [28,29,30,31,32,33,34],
		"12": [35,36,37,38,39,40,41,42,43,44,45]
	};
	var subCats = catHierarchy[catId];
	var selectedSubcat = {};
	$.each(subCats, function(i,v) {
		if(typeof newSelectedInstitutes[v] !== "undefined") {
			selectedSubcat[v] = newSelectedInstitutes[v];
		}
	});
	return selectedSubcat;
}

function countDictKeys(c){
	var count = 0;
	for (var i in c) {
	   if (c.hasOwnProperty(i)) count++;
	}
	return count;
}
});