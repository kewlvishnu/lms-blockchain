//dependency variables
var ApiEndPoint = '../api/index.php';
var ApiEndpoint = '../api/index.php';
var fileApiEndpoint = '../api/files1.php';
var MONTH=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
//global available functions
function setError(where, what) {
	unsetError(where);
	where.parents('div:eq(0)').addClass('has-error');
	where.parents('div:eq(0)').append('<span class="help-block">' + what + '</span>');
}

function unsetError(where) {
	if (where.parents('div:eq(0)').hasClass('has-error')) {
		where.parents('div:eq(0)').find('.help-block').remove();
		where.parents('div:eq(0)').removeClass('has-error');
	}
}

//for ajax loader display
$( document ).ajaxStart(function() {
  $('#loader').show();
});

$( document ).ajaxComplete(function() {
  $('#loader').hide();
});

/*---LEFT BAR ACCORDION----*/
$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
        //cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
	$('.dcjq-parent').on('click', function() {
		setTimeout(function() {
			$("#sidebar").getNiceScroll().resize();
		}, 1000);
	});

    //ajax for notification count at institute end
    fetchNotificationCount();
});
function fetchNotificationCount() {
    var req = {};
    req.action = 'get-notification-count';
    $.ajax({
        type: 'post',
        url: ApiEndpoint,
        data: JSON.stringify(req) 
    }).done(function(res) {
        res = $.parseJSON(res);
        if(res.status == 1)
            $('.notification.badge').text(res.count);
    });
}
/* for navigation to courses.php page after adding tree view feature */
$('.course').click(function(){
	window.location = $(this).parents('a:eq(0)').attr('href');
});

/* for navigation to students.php page after adding tree view feature */
$('.students').click(function(){
	window.location = $(this).parents('a:eq(0)').attr('href');
});
/* for navigation to subject-invitation.php page after adding tree view feature */
$('.subject-invitation').on('click', function() {
	window.location = $(this).parents('a:eq(0)').attr('href');
});
var Script = function () {

//    sidebar dropdown menu auto scrolling

    jQuery('#sidebar .sub-menu > a').click(function () {
        var o = ($(this).offset());
        diff = 250 - o.top;
        if(diff>0)
            $("#sidebar").scrollTo("-="+Math.abs(diff),500);
        else
            $("#sidebar").scrollTo("+="+Math.abs(diff),500);
    });

//    sidebar toggle

    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#sidebar').on("click", ".content-view", function(){
                    //console.log($('#container').hasClass('sidebar-closed'));
                    $('.sidebar-toggle .navbar-toggle').trigger("click");
                });
            }
            /*if (wSize <= 768) {
                $('#container').addClass('sidebar-close sidebar-closed');
                $('#sidebar > ul').hide();
                $('#sidebar').on("click", ".content-view", function(){
                    //console.log($('#container').hasClass('sidebar-closed'));
                    if (!$('#container').hasClass('sidebar-closed')) {
                        $('.tooltips').trigger("click");
                    };
                });
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }*/
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });
	
    /*$('.fa-bars').on('click', function () {
        if ($('#sidebar > ul').is(":visible") === true) {
            $('#main-content').css({
                'margin-left': '0px'
            });
            $('#sidebar').css({
                'margin-left': '-210px'
            });
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        } else {
            $('#main-content').css({
                'margin-left': '210px'
            });
            $('#sidebar > ul').show();
            $('#sidebar').css({
                'margin-left': '0'
            });
            $("#container").removeClass("sidebar-closed");
        }
    });*/

	$("#sidebar").niceScroll({
		styler: "fb",
		cursorcolor: "#e8403f",
		cursorwidth: '8px',
		cursorborderradius: '10px',
		background: '#404040',
		spacebarenabled:false,
		cursorborder: '',
		zindex: '1000',
		autohidemode: false
	});

// widget tools

    jQuery('.panel .tools .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".panel").children(".panel-body");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });

    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });


//    tool tips

    $('.tooltips').tooltip({
		html: true
	});

//    popovers

    $('.popovers').popover();



// custom bar chart

    if ($(".custom-bar-chart")) {
        $(".bar").each(function () {
            var i = $(this).find(".value").html();
            $(this).find(".value").html("");
            $(this).find(".value").animate({
                height: i
            }, 2000)
        })
    }


}();

function getUrlParameter(sParam)
{
	sParam = sParam.toLowerCase();
    var sPageURL = window.location.search.substring(1).toLowerCase();
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

function alertMsg(msg, time){
	time = time || 5000;
	$('.bubble-msg').remove();
	var top = '10px';
	if($('.bubble-msg').length > 0)
	  top = parseInt($('.bubble-msg').last().css('top').replace(/px/, '')) + $('.bubble-msg').last().outerHeight() + 10 + 'px';
	var elem = $('<div />').html(msg).attr('title', 'Click to dismiss.').addClass('bubble-msg animate-300')
		.appendTo($('body')).animate({'top': top}, 300);
	if(time != -1) {
		elem.removeAfter(time);
		$('.bubble-msg').on('click', function(){
			$(this).removeAfter(0);
		});
	}	
}


$.fn.removeAfter = function(time) {
	var elem = $(this);
	setTimeout(function() {
		elem.animate({'opacity': 0}, 300);
		setTimeout(function() {
			elem.remove();
		}, 300);
	}, time);
}

function nl2br(str){
	return str.replace(/(?:\r\n|\r|\n)/g, '<br />');
}
function trim(string) {
    return($.trim(string));
}
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function findObjByValue(obj,matches,value) {
    for(var i = 0; i < obj.length; i++)
    {
      if(obj[i][matches] == value)
      {
        return obj[i];
      }
    }
}
function findKeyByValue(obj,value) {
    for(var i = 0; i < obj.length; i++)
    {
      if(obj[i].id == value)
      {
        return i;
      }
    }
}
function findRemoveObjByValue(param) {
    var obj = {};

    //set data
    obj = param;

    //augment the object with a remove function
    obj.remove = function(key, val) {
        var i = 0;
        //loop through data
        while (this[i]) {
            if (this[i][key] === val) {
                //if we have that data, splice it
                //splice changes the array length so we don't increment
                this.splice(i, 1);
            } else {
                //else move on to the next item
                i++;
            }
        }
        //be sure to return the object so that the chain continues
        return this;
    }

    //return object for operation
    return obj
}