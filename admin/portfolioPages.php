<?php
	require_once "html-template/cookieCheck.php";
	require_once "../api/Vendor/ArcaneMind/Api.php";
	require_once "../api/Vendor/ArcaneMind/AccessApi.php";
	$access = AccessApi::checkAccess();
	require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Portfolio Dashboard</title>
		<?php include 'html-template/general/head-tags.html.php'; ?>
		<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
		<link rel="stylesheet" href="assets/file-upload/css/jquery.fileupload.css">
	</head>
	<body>
	<section id="container">
		<?php include "html-template/general/header.html.php"; ?>
		<?php include "html-template/general/sidebar.html.php"; ?>
		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
				<div class="well well-sm well-white">
					<h1 class="text-center pf-title">Portfolio Dashboard</h1>
				</div>
				<div class="well well-lg well-white">
				<?php
					if(isset($portfolioDetails) && !empty($portfolioDetails)) {
						if($portfolioDetails->owner == 1) {
				?>
					<div>
						<div class="row">
							<div class="col-md-2">
								<h2 class="lead">Portfolio Menu</h2>
								<hr />
								<ul class="nav nav-pills nav-stacked list-pf-mainmenu">
									<li><a href="portfolio.php"><i class="fa fa-edit"></i> Edit Portfolio</a></li>
									<li><a href="portfolioMenus.php"><i class="fa fa-bars"></i> Manage Menus</a></li>
									<li><a href="portfolioPages.php" class="selected"><i class="fa fa-indent"></i> Manage Pages</a></li>
								</ul>
							</div>
							<div class="col-md-6">
								<h2 class="lead">Manage Pages</h2>
								<hr />
								<form id="frmPortfolioPages">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<div class="help-block"></div>
											</div>
											<div class="form-group">
												<label for="inputProfileName">Page Type</label>
												<select class="form-control" id="selectPageType" name="selectPageType"></select>
												<input type="hidden" id="pageId" name="pageId" />
												<input type="hidden" id="pageType" name="pageType" />
											</div>
											<div class="form-group">
												<label for="inputPageTitle">Page Title</label>
												<input type="text" class="form-control" id="inputPageTitle" name="inputPageTitle" placeholder="Enter Page Title">
											</div>
											<div id="coursesBlock" class="page-block hide">
												<div class="form-group">
													<label for="inputCourses">Select Courses</label>
													<!-- <input type="text" class="form-control" id="inputCourses" name="inputCourses" placeholder="Select Courses" /> -->
													<div class="js-courses"></div>
												</div>
											</div>
											<div id="aboutBlock" class="page-block hide">
												<div class="form-group">
													<label for="inputAboutDescription">About Description</label>
													<textarea class="form-control" id="inputAboutDescription" rows="5" placeholder="Enter About Description"></textarea>
												</div>
												<div class="form-group">
													<p class="lead">Academic Position</p>
													<hr>
												</div>
												<div class="form-group">
													<label for="inputAcademicTitle">Title</label>
													<input type="text" class="form-control" id="inputAcademicTitle" name="inputAcademicTitle" value="Academic Position" placeholder="Academic Position Title" />
													<button class="btn btn-warning btn-modal" data-target="#academicPositionsModal">Add Academic Position</button>
													<div class="js-academic"></div>
												</div>
												<div class="form-group">
													<p class="lead">Education &amp; Training</p>
													<hr>
												</div>
												<div class="form-group">
													<label for="inputEducationTitle">Title</label>
													<input type="text" class="form-control" id="inputEducationTitle" name="inputEducationTitle" value="Education &amp; Training" placeholder="Education &amp; Training Title" />
													<button class="btn btn-warning btn-modal" data-target="#educationModal">Add Education &amp; Training</button>
													<div class="js-education"></div>
												</div>
												<div class="form-group">
													<p class="lead">Our Honors</p>
													<hr>
												</div>
												<div class="form-group">
													<label for="inputHonorsTitle">Title</label>
													<input type="text" class="form-control" id="inputHonorsTitle" name="inputHonorsTitle" value="Our Honors" placeholder="Honors Section Title" />
													<button class="btn btn-warning btn-modal" data-target="#honorsModal">Add Our Honors Information</button>
													<div class="js-honors"></div>
												</div>
											</div>
											<?php
												if($portfolioDetails->userRole == 1) {
											?>
											<div id="facultyBlock" class="page-block hide">
												<div class="form-group">
													<label for="inputFacultyDescription">Faculty Description</label>
													<textarea class="form-control" id="inputFacultyDescription" rows="5" placeholder="Enter Faculty Description"></textarea>
												</div>
												<div class="form-group">
													<button class="btn btn-warning btn-modal" data-target="#facultyMembersModal">Add Faculty Member</button>
													<div class="js-faculty"></div>
												</div>
											</div>
											<?php
												} elseif($portfolioDetails->userRole == 2) {
											?>
											<div id="publicationsBlock" class="page-block hide">
												<div class="form-group">
													<label for="inputPublicationsDescription">Publications Description</label>
													<textarea class="form-control" id="inputPublicationsDescription" rows="5" placeholder="Enter Publications Description"></textarea>
												</div>
												<div class="form-group">
													<button class="btn btn-warning btn-modal" data-target="#publicationsModal">Add Publication</button>
													<div class="js-publication"></div>
												</div>
											</div>
											<div id="teachingBlock" class="page-block hide">
												<div class="form-group">
													<label for="inputTeachingDescription">Teaching Description</label>
													<textarea class="form-control" id="inputTeachingDescription" rows="5" placeholder="Enter Teaching Description"></textarea>
												</div>
												<div class="form-group">
													<button class="btn btn-warning btn-modal" data-target="#teachingModal">Add Teaching</button>
													<div class="js-teaching"></div>
												</div>
											</div>
											<?php
												}
											?>
											<div id="galleryBlock" class="page-block hide">
												<div class="form-group">
													<label for="inputGalleryDescription">Gallery Description</label>
													<textarea class="form-control" id="inputGalleryDescription" rows="5" placeholder="Enter Gallery Description"></textarea>
												</div>
												<div class="form-group">
													<button class="btn btn-warning btn-modal" data-target="#galleryModal">Add Gallery Image</button>
													<div class="js-gallery"></div>
												</div>
											</div>
											<div id="contactBlock" class="page-block hide">
												<div class="form-group">
													<button class="btn btn-warning btn-modal" data-target="#contactModal">Add Contact</button>
													<div class="js-contact"></div>
												</div>
												<div class="form-group">
													<label for="inputContactDescription">Contact Description</label>
													<textarea class="form-control" id="inputContactDescription" rows="5" placeholder="Enter Contact Description"></textarea>
												</div>
												<div class="form-group">
													<label for="inputOfficeDescription">Office Description</label>
													<textarea class="form-control" id="inputOfficeDescription" rows="5" placeholder="Enter Office Description"></textarea>
												</div>
												<div class="form-group">
													<label for="inputWorkDescription">Work Description</label>
													<textarea class="form-control" id="inputWorkDescription" rows="5" placeholder="Enter Work Description"></textarea>
												</div>
												<?php
													if($portfolioDetails->userRole == 2) {
												?>
												<div class="form-group">
													<label for="inputLabDescription">Lab Description</label>
													<textarea class="form-control" id="inputLabDescription" rows="5" placeholder="Enter Lab Description"></textarea>
												</div>
												<?php
													}
												?>
											</div>
											<div id="customBlock" class="page-block hide">
												<div class="form-group">
													<label for="inputPageContent">Page Content</label>
													<textarea class="form-control" id="inputPageContent" name="inputPageContent" rows="10" placeholder="Enter Page Content"></textarea>
												</div>
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-primary" id="btnSavePortfolioPage">Save Page</button>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="col-md-4">
								<h2 class="lead">List Of Pages</h2>
								<hr />
								<form action="">
									<div class="js-portfolio-pages">
										<ul class="nav nav-pills nav-stacked list-pf-menu">
											<li><a href="javascript:void(0)" class="js-page selected"><span class="checkbox"><input type="radio" name="optionPage" value="new" checked /> New</span></a></li>
										</ul>
									</div>
								</form>
							</div>
						</div>
					</div>
				<?php
						} else {
							header('location:'.$final_link);
						}
					} else {
						header('location:'.$final_link);
					}
				?>
				</div>
			</section>
		<!--main content end-->
		<!--footer start-->
		<!--footer end-->
	</section>
	<!--Modal for academic positions-->
	<div aria-hidden="true" aria-labelledby="academicPositionsModal" role="dialog" tabindex="-1" id="academicPositionsModal" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
					<h4 class="modal-title">
						Add Academic position
					</h4>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="form-group">
							<p class="help-block text-danger"></p>
						</div>
						<div class="form-group">
							<div class="container-fluid">
								<div class="col-sm-6">
									<label for="">From Year</label>
									<input type="text" class="form-control dpYears" id="inputAPFromYear" />
								</div>
								<div class="col-sm-6">
									<label for="">To Year<small>(Leave blank for present)</small></label>
									<input type="text" class="form-control dpYears" id="inputAPToYear" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="">Position</label>
							<input type="text" class="form-control" id="inputAPPosition" />
						</div>
						<div class="form-group">
							<label for="">University</label>
							<input type="text" class="form-control" id="inputAPUniversity" />
						</div>
						<div class="form-group">
							<label for="">Department</label>
							<input type="text" class="form-control" id="inputAPDepartment" />
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnAcademicPosition">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--Modal for education and training-->
	<div aria-hidden="true" aria-labelledby="educationModal" role="dialog" tabindex="-1" id="educationModal" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
					<h4 class="modal-title">
						Add Education &amp; Training Information
					</h4>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="form-group">
							<p class="help-block text-danger"></p>
						</div>
						<div class="form-group">
							<label for="">Degree</label>
							<input type="text" class="form-control" id="inputETDegree" />
						</div>
						<div class="form-group">
							<label for="">Year</label>
							<input type="text" class="form-control dpYears" id="inputETYear" />
						</div>
						<div class="form-group">
							<label for="">Title</label>
							<input type="text" class="form-control" id="inputETTitle" />
						</div>
						<div class="form-group">
							<label for="">University</label>
							<input type="text" class="form-control" id="inputETUniversity" />
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnEducation">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--Modal for Our Honors-->
	<div aria-hidden="true" aria-labelledby="honorsModal" role="dialog" tabindex="-1" id="honorsModal" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
					<h4 class="modal-title">
						Add Our Honors Information
					</h4>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="form-group">
							<p class="help-block text-danger"></p>
						</div>
						<div class="form-group">
							<label for="">Image</label>
							<!-- The fileinput-button span is used to style the file input field as button -->
							<span class="btn btn-success fileinput-button">
								<i class="glyphicon glyphicon-plus"></i>
								<span>Select files...</span>
								<!-- The file input field used as target for the file upload widget -->
								<input id="fileOHImage" type="file" name="fileOHImage">
							</span>
							<img src="" alt="" width="100px" id="imgOHImage" class="hide" />
							<input type="hidden" id="inputOHImage" name="inputOHImage" />
						</div>
						<div class="form-group">
							<label for="">Year</label>
							<input type="text" class="form-control dpYears" id="inputOHYear" />
						</div>
						<div class="form-group">
							<label for="">Title</label>
							<input type="text" class="form-control" id="inputOHTitle" />
						</div>
						<div class="form-group">
							<label for="">Description</label>
							<input type="text" class="form-control" id="inputOHDescription" />
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnHonors">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--Modal for Faculty-->
	<div aria-hidden="true" aria-labelledby="facultyMembersModal" role="dialog" tabindex="-1" id="facultyMembersModal" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
					<h4 class="modal-title">
						Add Faculty Member
					</h4>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="form-group">
							<p class="help-block text-danger"></p>
						</div>
						<div class="form-group">
							<label for="">Image</label>
							<!-- The fileinput-button span is used to style the file input field as button -->
							<span class="btn btn-success fileinput-button">
								<i class="glyphicon glyphicon-plus"></i>
								<span>Select files...</span>
								<!-- The file input field used as target for the file upload widget -->
								<input id="fileFMImage" type="file" name="fileFMImage">
							</span>
							<img src="" alt="" width="100px" id="imgFMImage" class="hide" />
							<input type="hidden" id="inputFMImage" name="inputFMImage" />
						</div>
						<div class="form-group">
							<label for="">Name</label>
							<input type="text" class="form-control" id="inputFMName" />
						</div>
						<div class="form-group">
							<label for="">University</label>
							<input type="text" class="form-control" id="inputFMUniversity" />
						</div>
						<div class="form-group">
							<label for="">Department</label>
							<input type="text" class="form-control" id="inputFMDepartment" />
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnFacultyMember">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--Modal for Publications-->
	<div aria-hidden="true" aria-labelledby="publicationsModal" role="dialog" tabindex="-1" id="publicationsModal" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
					<h4 class="modal-title">
						Add Publication
					</h4>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="form-group">
							<p class="help-block text-danger"></p>
						</div>
						<div class="form-group">
							<label for="selectPBType">Publication Type</label>
							<select name="selectPBType" id="selectPBType" class="form-control">
								<option value="1">Published Papers</option>
								<option value="2">Research Papers</option>
								<option value="3">Books</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">Name</label>
							<input type="text" class="form-control" id="inputPBName" />
						</div>
						<div class="form-group">
							<label for="">Description</label>
							<input type="text" class="form-control" id="inputPBDescription" />
						</div>
						<div class="form-group">
							<label for="">Year</label>
							<input type="text" class="form-control dpYears" id="inputPBYear" />
						</div>
						<div class="form-group">
							<label for="">ISBN</label>
							<input type="text" class="form-control" id="inputPBISBN" />
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnPublication">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--Modal for Teachings-->
	<div aria-hidden="true" aria-labelledby="teachingModal" role="dialog" tabindex="-1" id="teachingModal" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
					<h4 class="modal-title">
						Add Teaching
					</h4>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="form-group">
							<p class="help-block text-danger"></p>
						</div>
						<div class="form-group">
							<label for="">Title</label>
							<input type="text" class="form-control" id="inputTCTitle" />
						</div>
						<div class="form-group">
							<label for="">Description</label>
							<input type="text" class="form-control" id="inputTCDescription" />
						</div>
						<div class="form-group">
							<label for="">From Year</label>
							<input type="text" class="form-control dpYears" id="inputTCFrom" />
						</div>
						<div class="form-group">
							<label for="">To Year<small>(Leave blank for present)</small></label>
							<input type="text" class="form-control dpYears" id="inputTCTo" />
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnTeaching">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--Modal for Gallery-->
	<div aria-hidden="true" aria-labelledby="galleryModal" role="dialog" tabindex="-1" id="galleryModal" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
					<h4 class="modal-title">
						Add Gallery Image
					</h4>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="form-group">
							<p class="help-block text-danger"></p>
						</div>
						<div class="form-group">
							<label for="">Image</label>
							<!-- The fileinput-button span is used to style the file input field as button -->
							<span class="btn btn-success fileinput-button">
								<i class="glyphicon glyphicon-plus"></i>
								<span>Select files...</span>
								<!-- The file input field used as target for the file upload widget -->
								<input id="fileGIImage" type="file" name="fileGIImage">
							</span>
							<img src="" alt="" width="100px" id="imgGIImage" class="hide" />
							<input type="hidden" id="inputGIImage" name="inputGIImage" />
						</div>
						<div class="form-group">
							<label for="">Title</label>
							<input type="text" class="form-control" id="inputGITitle" />
						</div>
						<div class="form-group">
							<label for="">Description</label>
							<input type="text" class="form-control" id="inputGIDescription" />
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnGalleryImage">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--Modal for contacts-->
	<div aria-hidden="true" aria-labelledby="contactModal" role="dialog" tabindex="-1" id="contactModal" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
					<h4 class="modal-title">
						Add Contact
					</h4>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="form-group">
							<p class="help-block text-danger"></p>
						</div>
						<div class="form-group">
							<select class="form-control" id="selectCIType">
								<option value="phone">Phone</option>
								<option value="email">Email</option>
								<option value="skype">Skype</option>
								<option value="facebook">Facebook</option>
								<option value="twitter">Twitter</option>
								<option value="linkedin">Linkedin</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">Title</label>
							<input type="text" class="form-control" id="inputCITitle" />
						</div>
						<div class="form-group">
							<label for="">Value</label>
							<input type="text" class="form-control" id="inputCIValue" />
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnContact">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<?php include "html-template/general/footer.html.php"; ?>
	<?php include "html-template/pageCheckFooter.php"; ?>
	<?php include 'html-template/general/footer-scripts.html.php'; ?>
	<!--script for this page-->
	<?php if(isset($portfolioDetails) && !empty($portfolioDetails)) { ?>
	<!--<script src="//cdn.ckeditor.com/4.5.1/basic/ckeditor.js"></script>-->
	<!--<script src="js/custom/initializeCKEditor.js"></script>-->
	<script src="//cdn.ckeditor.com/4.5.2/full/ckeditor.js"></script>
	<script src="assets/ckeditor/adapters/jquery.js"></script>
	<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="assets/file-upload/js/jquery.iframe-transport.js"></script>
	<!-- The basic File Upload plugin -->
	<script src="assets/file-upload/js/jquery.fileupload.js"></script>
	<script src="js/custom/portfolioPages.js" type="text/javascript"></script>
	<?php } ?>
	</body>
</html>