<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Students Profile</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
	  <header class="header white-bg">
  <div class="sidebar-toggle-box">
	  <div data-original-title="Toggle Navigation" data-placement="right" class="fa fa-bars tooltips"></div>
  </div>
  <!--logo start-->
  <a href="index.html" class="logo"><img src="../img/Arcane_Logo.png"></a>
  <!--logo end-->          
  <div class="top-nav ">
	  <ul class="nav pull-right top-menu">
		  <li><a href="signup-student.php#student">Register</a></li>
						<li><a href="promotion.php">For Institute/ Professor/ Publisher</a></li>
                        <li><a href="#login-modal" data-toggle="modal">Login</a></li>
<li class="dropdown" id="header_notification_bar">
			<a data-toggle="dropdown" href="#" class="dropdown-toggle">
			<i class="fa fa-bell-o"></i>
			<span class="badge bg-warning">7</span>
			</a>
			</li>
		  <!-- user login dropdown start-->
		  <li class="dropdown">
			  <a data-toggle="dropdown" class="dropdown-toggle" href="#" id="user-profile-card">
				  <img width="29px" height="29px" alt="" class="user-image" src="user-data/images/profile-image-1-uid.jpeg?1410762348797">
				  <span class="username">akgec</span>
				  <b class="caret"></b>
			  </a>
			  <ul class="dropdown-menu extended logout">
				  <div class="log-arrow-up"></div>
				  <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
				  <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
				  <li><a href="#"><i class="fa fa-bell-o"></i> Notification</a></li>
				  <li><a href="../logout.php"><i class="fa fa-key"></i> Log Out</a></li>
			  </ul>
		  </li>
		  <!-- user login dropdown end -->
	  </ul>
  </div>
</header>

      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                  <div class="col-md-3">
                      <section class="panel">
                          <div class="panel-body">
                              <input type="text" placeholder="Keyword Search" class="form-control">
                          </div>
                      </section>
                      <section class="panel">
                          <header class="panel-heading">
                              Category
                          </header>
                          <div class="panel-body">
                              <ul class="nav prod-cat">
                                  <li>
                                      <a href="#" class="active"><i class=" fa fa-angle-right"></i> Dress</a>
                                      <ul class="nav">
                                          <li class="active"><a href="#">- Shirt</a></li>
                                          <li><a href="#">- Pant</a></li>
                                          <li><a href="#">- Shoes</a></li>
                                      </ul>
                                  </li>
                                  <li><a href="#"><i class=" fa fa-angle-right"></i> Bags &amp; Purses</a></li>
                                  <li><a href="#"><i class=" fa fa-angle-right"></i> Beauty</a></li>
                                  <li><a href="#"><i class=" fa fa-angle-right"></i> Coat &amp; Jacket</a></li>
                                  <li><a href="#"><i class=" fa fa-angle-right"></i> Jeans</a></li>
                                  <li><a href="#"><i class=" fa fa-angle-right"></i> Jewellery</a></li>
                                  <li><a href="#"><i class=" fa fa-angle-right"></i> Electronics</a></li>
                                  <li><a href="#"><i class=" fa fa-angle-right"></i> Sports</a></li>
                                  <li><a href="#"><i class=" fa fa-angle-right"></i> Technology</a></li>
                                  <li><a href="#"><i class=" fa fa-angle-right"></i> Watches</a></li>
                                  <li><a href="#"><i class=" fa fa-angle-right"></i> Accessories</a></li>
                              </ul>
                          </div>
                      </section>
                      
                      
                  </div>
                  <div class="col-md-9">

                      <section class="panel">
                          <div class="panel-body">
                              
                              <div class="12">
                                  <h4 class="pro-d-title">
                                      <a href="#" class="">
                                          Leopard Shirt Dress
                                      </a>
                                  </h4>
                                  <p>
                                      Praesent ac condimentum felis. Nulla at nisl orci, at dignissim dolor, The best product descriptions address your ideal buyer directly and personally. The best product descriptions address your ideal buyer directly and personally.
                                  </p>
                                  <div class="product_meta">
                                      <span class="posted_in"> <strong>Categories:</strong> <a rel="tag" href="#">Jackets</a>, <a rel="tag" href="#">Men</a>, <a rel="tag" href="#">Shirts</a>, <a rel="tag" href="#">T-shirt</a>.</span>
                                      <span class="tagged_as"><strong>Tags:</strong> <a rel="tag" href="#">mens</a>, <a rel="tag" href="#">womens</a>.</span>
                                  </div>
                                  <div class="m-bot15"> <strong>Price : </strong> <span class="amount-old">$544</span>  <span class="pro-price"> $300.00</span></div>
                                  <div class="form-group">
                                      <label>Quantity</label>
                                      <input type="quantiy" placeholder="1" class="form-control quantity">
                                  </div>
                                  <p>
                                      <button class="btn btn-round btn-danger" type="button"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                                  </p>
                              </div>
                          </div>
                      </section>

                      <section class="panel">
                          <header class="panel-heading tab-bg-dark-navy-blue">
                              <ul class="nav nav-tabs ">
                                  <li class="active">
                                      <a data-toggle="tab" href="#description">
                                          Description
                                      </a>
                                  </li>
                                  <li class="">
                                      <a data-toggle="tab" href="#reviews">
                                          Reviews
                                      </a>
                                  </li>

                              </ul>
                          </header>
                          <div class="panel-body">
                              <div class="tab-content tasi-tab">
                                  <div id="description" class="tab-pane active">
                                      <h4 class="pro-d-head">Product Description</h4>
                                      <p> Praesent ac condimentum felis. Nulla at nisl orci, at dignissim dolor, The best product descriptions address your ideal buyer directly and personally. The best product descriptions address your ideal buyer directly and personally. </p>
                                      <p> Praesent ac condimentum felis. Nulla at nisl orci, at dignissim dolor, The best product descriptions address your ideal buyer directly and personally. The best product descriptions address your ideal buyer directly and personally. The best product descriptions address your ideal buyer directly and personally. The best product descriptions address your ideal buyer directly and personally. </p>
                                  </div>
                                  <div id="reviews" class="tab-pane">
                                      <article class="media">
                                          <a class="pull-left thumb p-thumb">
                                              <img src="img/avatar-mini.jpg">
                                          </a>
                                          <div class="media-body">
                                              <a href="#" class="cmt-head">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a>
                                              <p> <i class="fa fa-time"></i> 1 hours ago</p>
                                          </div>
                                      </article>
                                      <article class="media">
                                          <a class="pull-left thumb p-thumb">
                                              <img src="img/avatar-mini2.jpg">
                                          </a>
                                          <div class="media-body">
                                              <a href="#" class="cmt-head">Nulla vel metus scelerisque ante sollicitudin commodo</a>
                                              <p> <i class="fa fa-time"></i> 23 mins ago</p>
                                          </div>
                                      </article>
                                      <article class="media">
                                          <a class="pull-left thumb p-thumb">
                                              <img src="img/avatar-mini3.jpg">
                                          </a>
                                          <div class="media-body">
                                              <a href="#" class="cmt-head">Donec lacinia congue felis in faucibus. </a>
                                              <p> <i class="fa fa-time"></i> 15 mins ago</p>
                                          </div>
                                      </article>
                                  </div>
                              </div>
                          </div>
                      </section>

                      
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->

      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2013 &copy; FlatLab by VectorLab.
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->



  </section>

	<?php include 'html-template/general/footer-scripts.html.php'; ?>  
</body> </html>
