<?php
  require_once "../api/Vendor/ArcaneMind/Api.php";
  require_once "../api/Vendor/ArcaneMind/AccessApi.php";
  $access = AccessApi::checkAccess();
  require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Courses</title>
    <?php include "html-template/general/head-tags.html.php"; ?>
     
        <link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
        <link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
</head>
<body>
    <section id="container" class="">
	  <?php include "html-template/general/header.html.php"; ?>
      <?php include "html-template/general/admin-sidebar.html.php"; ?>
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-md-12">
                     <?php include "html-template/packages_subs/packagesubs01.php"; ?>
                  </div>
                 </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <?php include "html-template/general/footer.html.php"; ?>
      <?php include "html-template/pageCheckFooter.php"; ?>
  </section>
    <!-- js placed at the end of the document so the pages load faster -->
    <?php include "html-template/general/footer-scripts.html.php"; ?>
	<script src="js/custom/adminPackagesSubs.js"></script>
</body>
</html>
