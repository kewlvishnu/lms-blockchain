<?php
  require_once "html-template/cookieCheck.php";
  require_once "../api/Vendor/ArcaneMind/Api.php";
  require_once "../api/Vendor/ArcaneMind/AccessApi.php";
  $access = AccessApi::checkAccess();
  require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Add new course</title>
    <link href="assets/datetimepicker/jquery.datetimepicker.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-select/bootstrap-select.css" />
	<?php include "html-template/general/head-tags.html.php"; ?>
  </head>
  <body>
	<section id="container" class="">
      <?php include "html-template/general/header.html.php"; ?>
      <?php include "html-template/general/sidebar.html.php"; ?>
	  <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                <div class="col-md-12">
		 <?php include "html-template/add-course/add-course-form.html.php"; ?>
                  </div>
				  <!--
                  <div class="col-md-3">
                      <?php //include "html-template/general/updates-feed.html.php"; ?>
                  </div>-->
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
	  <?php include "html-template/general/footer.html.php"; ?>
    <?php include "html-template/pageCheckFooter.php"; ?>
  </section>
	<?php include "html-template/general/footer-scripts.html.php"; ?>
	<script src="assets/datetimepicker/jquery.datetimepicker.js"></script>
	<script type="text/javascript" src="assets/bootstrap-select/bootstrap-select.js"></script>
        <script src="js/custom/add-course.js"></script>
	<script>
		$( function() {
		  // $("#course-cat").selectpicker({});
                   $('#course-cat').multiselect();
		});
	</script>
	<?php include "html-template/add-course/add-course-licensing-modal.html.php"; ?>
  </body>
</html>