<?php
    require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>View Institutes</title>
        <?php include "html-template/general/head-tags.html.php"; ?>
     
        <link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
        <link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
    </head>
     <body>

        <section id="container" class="">
            <?php include "html-template/general/header.html.php"; ?>
            <?php include "html-template/general/admin-sidebar.html.php"; ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper site-min-height">
                    <!-- page start-->
                    <div class="row">
                        <div class="col-md-12">
                            <?php include "html-template/portfolio-admin/portfolio.php"; ?>
                        </div>
                    </div>
                    <!-- page end-->
                </section>
            </section>
            <?php include "html-template/general/footer.html.php"; ?>
            <?php include "html-template/pageCheckFooter.php"; ?>
        </section>
        <!--Modal for contacts-->
        <div aria-hidden="true" aria-labelledby="approveModal" role="dialog" tabindex="-1" id="approveModal" class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                        <h4 class="modal-title">
                            Approve/Reject Portfolio
                        </h4>
                    </div>
                    <div class="modal-body">
                        <form action="" id="frmApproval">
                            <div class="form-group">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <label for="selectApproval">Approval Type</label>
                                <select class="form-control" id="selectApproval">
                                    <option value="approve">Approve</option>
                                    <option value="reject">Reject</option>
                                </select>
                            </div>
                            <div class="form-group invisible js-reason">
                                <label for="">Reason</label>
                                <textarea class="form-control" rows="5" id="inputReason"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary js-btn-submit" data-update="" id="btnApprove">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
		<?php include 'html-template/general/footer-scripts.html.php'; ?>
        <!--script for this page only-->
		<script src="js/custom/portfolioAdmin.js"></script>
    </body>
</html>
