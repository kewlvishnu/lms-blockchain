<?php
	require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Results</title>
	<?php include 'html-template/general/head-tags.html.php'; ?>
	<link href="assets/morris.js-0.4.3/morris.css" rel="stylesheet" />

</head>
<body>
	<section id="container" class="">
		<!--header start-->
			<?php include 'html-template/general/header.html.php'; ?>
		<!--header end-->
			<?php include "html-template/general/sidebar.html.php"; ?>
		<section id="main-content">
		<section>
			<section class="wrapper">
				<!-- page start-->
				<div class="row">
					<!--<div class="col=ld-4">
						<?php //include "html-template/general/sidebar.html.php"; ?>
					</div>-->
					<div class="col-lg-12 dispNone"  id="graph" >
						<!--<section class="panel">
							<header class="panel-heading">
								<h4><i class="fa fa-minus-square hide-summary"></i> Charts</h4>							  
							</header>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-6 text-center" >  
										<canvas id="attemptChart" width="400" height="400">    </canvas>
								   	</div>
									<div class="col-lg-6 text-center" > 
								  	<canvas  width="400" height="400"  id="graph1" ></canvas>
									</div>
									<div class="col-lg-6 text-center"><h3>Score vs Attempt</h3> </div>
									<div class="col-lg-6 text-center"><h3>Percentage of all students</h3></div>
								</div>
							</div>
						</section>
						-->
						<section class="panel">
							<header class="panel-heading">
								<h4><i class="fa fa-minus-square hide-summary"></i> TimeLine Grph</h4>							  
							</header>
							<div class="panel-body">
								<div class="row">
								  	<div class="panel-body"   id="graph12" ></div>
									<div class=" text-center " ><h3>Comparision graph w.r.t to timeline</h3></div>
								</div>
							</div>
						</section>
					</div>
					<div class="col-md-12">
						<section class="panel">
							<header class="panel-heading">
								<h4><i class="fa fa-minus-square hide-summary"></i> Summary Attempt No: <span id="attemptNo"></span> </h4>
							</header>
							<div class="panel-body"> 
								<div class="row">
									<div class="col-md-3 result-custom">
										<div class="row">
											<div class="col-xs-1 text-danger">
												<i class="fa fa-clock-o" style="font-size: 2.2em;"></i>
											</div>
											<div class="col-xs-10">
												<table style="width:100%">
													<tr>
														<td><strong>Started on:</strong></td>
														<td><span id="startDate"></span></td>
													</tr>
													<tr>
														<td><strong>Completed on:</strong></td>
														<td><span id="endDate"></span></td>
													</tr>
													<tr>
														<td><strong>Time taken:</strong></td>
														<td><span id="timeTaken"></span></td>
													</tr>
												</table>
											</div>
										</div>
									</div>
									<div class="col-md-2 result-custom">
										<table style="width:100%">
											<tr>
												<td><strong>Rank:</strong></td>
												<td><span id="rank"></span></td>
											</tr>
											<tr>
												<td><strong>Percentage:</strong></td>
												<td><span id="percentage"></span> %</td>
											</tr>
											<tr>
												<td><strong>Score:</strong></td>
												<td><span id="score"></span></td>
											</tr>
										</table>
									</div>
									<div class="col-md-3 result-custom-img">
										<div class="row">
											<div class="col-xs-6 result-custom-img">
												<a class="text-danger previous-attempt custom-disabled" href="#">
													Previous Attempt<br/>
													<img src="../admin/img/result/previous_attempt.png">
												</a>
											</div>
											<div class="col-xs-6">
												<a href="#" class="text-danger next-attempt custom-disabled">
													Next Attempt<br/>
													<img src="../admin/img/result/next_attempt.png">
												</a>
											</div>
										</div>
									</div>
									<div class="col-md-2 result-custom-img">
										<a class="text-danger" href="#categories" data-toggle="modal">
											See Question Categories<br/>
											<img src="../admin/img/result/categories.png">
										</a>
									</div>
									<div class="col-md-2 result-custom-img">
										<a class="text-danger" id="compareTopper">
											Compare with toppers<br/>
											<img src="../admin/img/result/compare.png">
										</a>
									</div>
								</div>
								<br>
								<section class="panel">
									<div class="row">
										<div class="col-lg-10">
											<table class="table" id="sectionTable">
												<thead>
												<tr class="custom-bg">
													<th>Section</th>
													<th>Total Question</th>
													<th>Time Taken</th>
													<th>Question Response</th>
												</tr>
												</thead>
												<tbody>
												</tbody>								 
											</table>
										</div>
										<div class="col-lg-2">
											<span>
												Legend<br/>
												<button class="btn btn-success btn-sm"> </button> Correct Answer<br/>
												<button class="btn btn-danger btn-sm"> </button> Wrong Answer<br/>
												<button class="btn btn-black btn-sm"> </button> Unattempted Questions<br/>
											</span>
										</div>
									</div>
									<a class="btn btn-info btn-md pull-right" href="#" id="back">Back</a>
								</section>
							</div>  
						</section>
						<div id="sectionDetails">
						
						</div>
					</div>                      
				</div>
			<!-- page end-->
			</section>
			<div id="hidden-topper-graph">
				<div id="topper-container">
					<span class="pull-right" style="margin-right: -27px;font-size: 1.8em;margin-top: -35px;" id="topperClose"><i class="fa fa-times-circle-o text-danger"></i></span>
					<div class="row percAllStudents dispNone">
						<section class="panel">
								<header class="panel-heading">
									Percentage of all students
								</header>
								<div class="panel-body">
									<div id="hero-bar3" class="graph" style="width: 847px;"></div>
								</div>
						</section>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<section class="panel">
								<header class="panel-heading">
									Score
								</header>
								<div class="panel-body">
									<div id="hero-bar1" class="graph"></div>
								</div>
							</section>
						</div>
						<div class="col-lg-6">
							<section class="panel">
								<header class="panel-heading">
									Percentage
								</header>
								<div class="panel-body">
									<div id="hero-bar2" class="graph"></div>
								</div>
							</section>
						</div>
					</div>
				</div>
			</div>
			<div id="hidden-time-graph">
				<div id="time-container">
					<span class="pull-right" style="margin-right: -27px;font-size: 1.8em;margin-top: -35px;" id="timeClose"><i class="fa fa-times-circle-o text-danger"></i></span>
					<div class="row">
						<div class="col-lg-12">
							<section class="panel">
								<header class="panel-heading">
									Time spent on a question statistics
								</header>
								<div class="panel-body to-be-added">
								</div>
							</section>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--main content end-->
		<?php include 'html-template/general/footer.html.php'; ?>
        <?php include "html-template/pageCheckFooter.php"; ?>
	</section>

	<?php include 'html-template/general/footer-scripts.html.php'; ?>
	<script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
	<script src="js/easy-pie-chart.js"></script>
	<script src="assets/chart-master/Chart.js"></script>
	<!--script for this page-->
	<!--<script src="../admin/js/sparkline-chart.js"></script>-->
	<script src="js/count.js"></script>
	
	<!-- script for this page only
	<script src="../admin/js/all-chartjs.js"></script>-->
	<script>
		$('.hide-summary').on('click', function() {
			if($(this).hasClass('fa-minus-square'))
				$(this).removeClass('fa-minus-square').addClass('fa-plus-square');
			else
				$(this).removeClass('fa-plus-square').addClass('fa-minus-square');
			$(this).parents('.panel:eq(0)').find('.panel-body').slideToggle();
		});
	</script>
	<script src="assets/morris.js-0.4.3/morris.min.js" type="text/javascript"></script>
	<script src="assets/morris.js-0.4.3/raphael-min.js" type="text/javascript"></script>
	<script src="assets/chart/Chart.min.js" type="text/javascript"></script>
	<script src="https://code.highcharts.com/stock/highstock.js"></script>
	<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/stock/modules/heatmap.js"></script>
	<script src="https://code.highcharts.com/stock/modules/data.js"></script>
	<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
	<script src="js/custom/studentResult.js"></script>
	<?php include 'html-template/result/category-modal.html.php'; ?>
  </body>
</html>
