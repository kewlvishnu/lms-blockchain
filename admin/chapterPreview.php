<?php
	require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Preview Chapter</title>
		<?php include 'html-template/general/head-tags.html.php'; ?>
		<link rel="stylesheet" type="text/css" href="flowplayer/skin/minimalist.css">
<link rel="stylesheet" href="mediaelement/build/mediaelementplayer.min.css" />
				<script language="JavaScript">
					//script to disable f12 key
					document.onkeypress = function (event) {
						event = (event || window.event);
						if (event.keyCode == 123 || event.keyCode == 18) {
							//alert('No F-12');
							return false;
						}
					}
						document.onmousedown = function (event) {
								event = (event || window.event);
								if (event.keyCode == 123 || event.keyCode == 18) {
										//alert('No F-keys');
										return false;
								}
						}
						document.onkeydown = function (event) {
								event = (event || window.event);
								if (event.keyCode == 123 || event.keyCode == 18) {
										//alert('No F-keys');
										return false;
								}
						}
				 //script to disable right mouse click key
						function clickIE4() {
								if (event.button == 2) {
										return false;
								}
						}

						function clickNS4(e) {
								if (document.layers || document.getElementById && !document.all) {
										if (e.which == 2 || e.which == 3) {
												return false;
										}
								}
						}

						if (document.layers) {
								document.captureEvents(Event.MOUSEDOWN);
								document.onmousedown = clickNS4;
						}
						else if (document.all && !document.getElementById) {
								document.onmousedown = clickIE4;
						}
						document.oncontextmenu = new Function("return false;");

						//disabling ctrl keys
						var isCtrl = false;
						document.onkeyup = function (e) {
								if (e.which == 17)
										isCtrl = false;
						}

						document.onkeydown = function (e) {
								if (e.which == 17)
										isCtrl = true;
								if (((e.which == 85) || (e.which == 117) || (e.which == 65) || (e.which == 97) || (e.which == 67) || (e.which == 99)) && isCtrl == true) {
										//alert('Keyboard shortcuts are cool!');
										return false;
								}
						}
				</script>
	</head>

	<body class="custom-container">

	<section id="container">
	<!--header start-->
		<?php include "html-template/general/header.html.php"; ?>
			<!--sidebar start-->
			<aside>
					<div id="sidebar"  class="nav-collapse custom-sidebar ">		  
							<!-- sidebar menu start-->
							<ul class="sidebar-menu custom-sidebar-menu" id="nav-accordion">				
				<div class="panel-group m-bot20 nav-accordion" id="accordion">
					
				</div>
							</ul>
							<!-- sidebar menu end-->
					</div>
			</aside>
			<!--sidebar end-->
			<!--main content start-->
		<div class="panel-body custom-course">
			<section class="custom-wrapper">
				<div class="panel-body custom-brad" id="breadcrumb" style="color: #BBBBBB;padding-left: 0%;">
					<span class="btc"></span> /
					<span class="btc"></span> /
					<span class="btc"></span>
				</div>
				<section class="panel" id="content-view">
					<div class="row">
							<header id="contentHeading">
								<span class="custom-panel-heading init"></span>
								<span class="arrow"></span>
								<span class="custom-panel-content cont"></span>
								<a class="pull-right btn btn-success btn-custom btn-md" id="backSubject" href="#" style="margin-right: 2.5%;">Back</a>
							</header>
					</div>
					<section id="block-everything"></section>
					<section id="progress">
						<div class="pcontainer">Loading document please wait<br>
						<div class="progress progress-striped active progress-sm">
																	<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
																	</div>
															</div>
						</div>
					</section>
					<div class="panel-body custom-content-part">
						
					</div>
				</section>
				<div class="panel-body pull-right" style="padding-top: 0px;">
					<button type="button" class="btn btn-success btn-sm btn-custom" id="previous">Previous Lecture</button>
					<button type="button" class="btn btn-success btn-sm btn-custom" id="next">Next Lecture</button>
				</div>
				<div class="description" style="display: none;">
					<div class="heading">Description</div>
					<div id="desc"></div>
				</div><br>
				<div class="description"  style="display: none;">
					<div class="heading">Supplementary Files</div>
					<div id="supp"></div>
				</div>
				<br><br>
			</section>
		</div>
			<!--main content end-->
			<!--footer start
			<footer class="site-footer">
					<div class="text-center">
							2013 &copy; FlatLab by VectorLab.
							<a href="#" class="go-top">
									<i class="fa fa-angle-up"></i>
							</a>
					</div>
			</footer>
			<!--footer end-->
	</section>
      	<?php include "html-template/pageCheckFooter.php"; ?>
		<?php include 'html-template/general/footer-scripts.html.php'; ?>
		<script type="text/javascript" src="flowplayer/flowplayer.min.js"></script>
		<script src="mediaelement/build/mediaelement-and-player.min.js"></script>
		<script src="assets/ViewerJS/viewer.js"></script>
		<script src="../admin/assets/ckeditor/ckeditor.js"></script>
		<script src="js/custom/chapterPreview.js" type="text/javascript"></script>
	</body>
</html>
