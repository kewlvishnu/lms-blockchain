<section class="panel">
	<header class="panel-heading">
		Import Manual Exam
	</header>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="well">
					<form class="frm-upload" action="../api/files1.php" method="post">
						<div class="form-group">
							<label for="">Select file to upload :</label>
							<div class="btn-block">
								<button type="button" class="btn btn-warning btn-upload">Upload Excel Sheet (CSV)</button>
								<input type="file" name="manual-exam-upload" class="js-uploader hide">
								<ul class="list-inline list-files js-file-path"></ul>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-6">
				<div class="text-center">
					<a href="#" class="btn btn-primary btn-lg mt50 import-manual-exam-link">Generate CSV</a>
				</div>
			</div>
		</div>
		<div id="jsForm"></div>
	</div>
</section>