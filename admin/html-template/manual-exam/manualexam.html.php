<section class="panel">
	<header class="panel-heading">
		Manual Exam
	</header>
  <div id="examTitle" class="pd20"></div>
  <div id="percentDistGraph"></div>
  <div class="row">
    <div class="col-md-6">
      <div id="scoreGraph"></div>
    </div>
    <div class="col-md-6">
      <div id="percentGraph"></div>
    </div>
  </div>
	<div id="exam"></div>
</section>
<!-- Modal -->
<div class="modal fade" id="marksModal" tabindex="-1" role="dialog" aria-labelledby="marksModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="marksModalLabel">Student Marks</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div id="innerAnalytics"></div>
            <div id="questionMarks"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>