<section class="panel">
	<header class="panel-heading">
		Add Manual Exam
	</header>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="well">
					<form role="form">
						<div class="form-group">
							<div class="row">
								<div class="col-md-6 text-center">
									<div class="radio dib">
										<input type="radio" value="quick" name="optionAnalytics" id="optionQuickAnalytics" checked="" /> <label for="optionQuickAnalytics">Quick Analytics <i class="tooltips fa fa-info-circle" data-original-title="Simple analytics based on each student's total exam scores. (Need only total exam score)" data-placement="right"></i></label>
									</div>
								</div>
								<div class="col-md-6 text-center">
									<div class="radio dib">
										<input type="radio" value="deep" name="optionAnalytics" id="optionDeepAnalytics" /> <label for="optionDeepAnalytics">Deep Analytics  <i class="tooltips fa fa-info-circle" data-original-title=" Detailed analytics based on each student's score per question. Output will give question level &amp; overall exam level analytics ( Need scores received on each question )" data-placement="right"></i></label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group hide js-quick">
									<label for="inputQuestions">Number of questions :</label>
									<input type="number" class="form-control" name="inputQuestions" id="inputQuestions">
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6 text-center">
											<div class="btn btn-primary" id="btnGenerateCSV">Generate CSV (Excel)</div>
										</div>
										<div class="col-sm-6 text-center">
											<div class="btn btn-primary" id="btnEnterManually">Enter manually</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-6">
				<div class="text-center">
					<a href="#" class="btn btn-primary btn-lg mt50 import-manual-exam-link">Import CSV</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="jsForm" class="oy-auto"></div>
			</div>
		</div>
	</div>
</section>