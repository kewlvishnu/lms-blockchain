<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-10">
		<div id="reviews" class="rate-reviews">
			<h2 class="block-title">Reviews</h2>
			<div class="row">
				<div class="col-sm-3">
					<span class="subblock-title">Average Ratings</span>
					<div class="avg-rate"></div>
					<div class="rating" style="font-size: 1.5em;">
						<input type="hidden" class="rating ratingStars" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="0" DISABLED/>
						&emsp;<span class="rating-count ratingTotal"></span>
					</div>
				</div>
				<div class="col-sm-4">
					<span class="subblock-title">Details</span>
					<ul class="list-unstyled list-rates">
						<li>
							<span class="item-title">5 Stars</span>
							<div class="progress">
								<div class="progress-bar progress5" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
									<span class="sr-only">60% Complete</span>
								</div>
							</div>&nbsp;<span class="count5"></span>
						</li>
						<li>
							<span class="item-title">4 Stars</span>
							<div class="progress">
								<div class="progress-bar  progress4" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
									<span class="sr-only">50% Complete</span>
								</div>
							</div>&nbsp;<span class="count4"></span>
						</li>
						<li>
							<span class="item-title">3 Stars</span>
							<div class="progress">
								<div class="progress-bar progress3" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
									<span class="sr-only">40% Complete</span>
								</div>
							</div>&nbsp;<span class="count3"></span>
						</li>
						<li>
							<span class="item-title">2 Stars</span>
							<div class="progress">
								<div class="progress-bar progress2" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
									<span class="sr-only">30% Complete</span>
								</div>
							</div>&nbsp;<span class="count2"></span>
						</li>
						<li>
							<span class="item-title">1 Stars</span>
							<div class="progress">
								<div class="progress-bar progress1" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
									<span class="sr-only">10% Complete</span>
								</div>
							</div>&nbsp;<span class="count1"></span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div id="comments" class="comments">
			<ul class="list-unstyled list-comments">
			</ul>
		</div>
	</div>
</div>