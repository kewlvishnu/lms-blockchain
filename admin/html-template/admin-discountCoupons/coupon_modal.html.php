<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
     id="coupons" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" color: #fff; class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <h4 class="modal-title">
                   Promote your course with Coupons </h4>
            </div>
    <div class="modal-body" style="padding-right: 35px;">
      <form role="form">
          
		    <div class="row">
             <div class="form-group ">
              <label for="inputEmail1" class="col-md-3 control-label">Coupon Code</label>
              <div class="col-md-7">
                <input type="text"  id = "couponCode" name ="couponCode" class="input-group form-control  couponCodeC" >
               <span class="help-block"></span>
            	</div>
               </div>
           </div>
                    
   <br/>
		  
		   <div class="row">
             <div class="form-group ">
              <label for="inputEmail1" class="col-md-3 control-label">No. Of Coupons</label>
              <div class="col-md-7">
                <input type="text"  id = "noofCoupons" name ="discount" class="input-group form-control  noofCouponsC" >
               <span class="help-block"></span>
            	</div>
               </div>
           </div>
                    
   <br/>
		  
		  
           <div class="row">
             <div class="form-group ">
              <label for="inputEmail1" class="col-md-3 control-label">Discount  Percentage</label>
              <div class="col-md-7">
                <input type="text"  id = "discount1" name ="discount" class="input-group form-control  discount11" >
               <span class="help-block"></span>
            	</div>
               </div>
           </div>
                    
   <br/>
   
    <div class="row">
                     <div class="form-group ">
              <label for="inputEmail1" class="col-md-3 control-label">Start Date </label>
              <div class="col-md-7">
                <input type="text"  id = "couponStartDate" name ="couponStartDate"  todayHighlight = true placeholder="10 Jan 2015" class="form-control couponStartDate1" >
                 <div class="form-msg couponStartDate"></div>
                </div>
                   </div>
                    </div> <br/>
                     <div class="row">
                     <div class="form-group ">
              <label for="inputEmail1" class="col-md-3 control-label">End Date </label>
              <div class="col-md-7">
                <input type="text"  id = "couponEndDate" name ="couponEndDate"  todayHighlight = true placeholder="10 Jan 2015" class="form-control couponEndDate1" >
                 <div class="form-msg couponEndDate"></div>
                </div>
                   </div>
                    </div> <br/>
                     
                   <div class="form-group">
                        <div style="text-align: right;">
                           <a class="btn btn-info courseCouponbtn" name="coursediscount1" id="coursediscount1"><i class="fa fa-shopping-cart"></i> Save Coupon.</a>
                           
                        </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>



