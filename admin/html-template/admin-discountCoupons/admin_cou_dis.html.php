      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-md-12">
                      <section class="panel">
					  <header class="panel-heading">
						 <h4>Create Coupons on All Active Courses</h4>
					  </header>
					 <div class="row panel-body">
	
        <div style="text-align: right" class="col-lg-12">
          <a data-toggle="modal" href="#coupons">
            <button type="submit"  class="btn btn-success btn-xs pull-left">Generate Coupons</button>
          </a>
            
        </div>
    </div> 					  
					  
                 </section>
                  </div>
                  <div class="col-md-3">
                      
                  </div>
              </div>
 			  
				  
				  
			 <div class="row">
                  <div class="col-md-12">
                      <section class="panel">
					  <header class="panel-heading">
						 <h4>Create Discounts on All Active Courses</h4>
					  </header>
					 <div class="row panel-body">
	
        <div style="text-align: right" class="col-lg-12">
          <a data-toggle="modal" href="#Give_Discount">
            <button type="submit" class="btn btn-success btn-xs pull-left">Create Discounts</button>
          </a>
            <label>
                <input type="text" placeholder="Search" class="form-control pull-right" aria-controls="example">
            </label>
        </div>
    </div> 					  
					  
                 </section>
                  </div>
                  <div class="col-md-3">
                      
                  </div>
              </div>
			  
 <div class="row">
 <div class="col-md-12 couponsinfo">
  <?php include "couponsHistory.html.php"; ?>
</div>
</div>	
			  
              <!-- page end-->
			  
          </section>
      </section>