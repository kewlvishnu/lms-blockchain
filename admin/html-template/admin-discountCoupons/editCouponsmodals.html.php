<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
     id="editcoupons" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" color: #fff; class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <h4 class="modal-title">
                   Edit Coupons </h4>
            </div>
    <div class="modal-body" style="padding-right: 35px;">
      <form role="form">
       <div class="row">
                   
          
		    <div class="row">
             <div class="form-group ">
              <label for="inputEmail1" class="col-md-3 control-label">Coupon Code</label>
              <div class="col-md-7">
                <span id='couponCodeE'>     </span><span class="help-block"></span>
            	</div>
               </div>
           </div>
                    
   <br/>
		  
		   <div class="row">
             <div class="form-group ">
              <label for="inputEmail1" class="col-md-3 control-label">No. Of Coupons</label>
              <div class="col-md-7">
                <input type="text"  id = "noofCoupons" name ="discount" class="input-group form-control  noofCouponsCE" >
               <span class="help-block"></span>
            	</div>
               </div>
           </div>
                    
   <br/>
		  
		  
           <div class="row">
             <div class="form-group ">
              <label for="inputEmail1" class="col-md-3 control-label">Discount  Percentage</label>
              <div class="col-md-7">
                <input type="text"  id = "discount1E" name ="discount" class="input-group form-control  discountE" >
               <span class="help-block"></span>
            	</div>
               </div>
           </div>
                    
   <br/>
   
    <div class="row">
                     <div class="form-group ">
              <label for="inputEmail1" class="col-md-3 control-label">Start Date </label>
              <div class="col-md-7">
                <input type="text"  id = "couponStartDateE" name ="couponStartDate"  todayHighlight = true placeholder="10 Jan 2015" class="form-control couponStartDateE" >
                 <div class="form-msg couponStartDateE"></div>
                </div>
                   </div>
                    </div> <br/>
                     <div class="row">
                     <div class="form-group ">
              <label for="inputEmail1" class="col-md-3 control-label">End Date </label>
              <div class="col-md-7">
                <input type="text"  id = "couponEndDateE" name ="couponEndDate"  todayHighlight = true placeholder="10 Jan 2015" class="form-control couponEndDateE" >
                 <div class="form-msg couponEndDateE"></div>
                </div>
                   </div>
                    </div> <br/>
            
                   <div class="form-group">
                        <div style="text-align: right;">
                           <a class="btn btn-info editCouponbtn" name="coursediscount1" id="editcouponbtn"><i class="fa fa-shopping-cart"></i> Edit Coupon.</a>
                           
                        </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


