<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
     id="downloadcustom" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button"  class="close datatablecll buttoncolWhite" id="datableclose" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Please choose Feilds to be imported</h4>
			</div>
			<div class="modal-body">
				<section class="panel">
					<div id ="overflowAuto">
						<div class="row">
							<label><input type="checkbox" id="cbox1" value="first_checkbox" checked="checked"> Average Scores of students</label>
						</div>
					</div>
				</section>
				<section class="panel">
					<div id ="overflowAuto">
						<div class="row">
							<label><input type="checkbox" id="cbox2" value="second_checkbox"> Highest Scores Of students</label>
						</div>
					</div>
				</section>
				<section class="panel">
					<div id ="overflowAuto">
						<div class="row">
							<label><input type="checkbox" id="cbox3" value="third_checkbox"> Number of Attempts by students</label>
						</div>
					</div>
				</section>
				<section class="panel">
					<div id ="overflowAuto">
						<div class="row">
							<label><input type="checkbox" id="cbox4" value="fourth_checkbox"> Score Of Last Attempt of Students</label>
						</div>
					</div>
				</section>
				<section>
					<div id="overflowAuto">
						<div class="pull-right custom-alignment">
							<button type="button" class="btn btn-success btn-md" id="downloadCustom-list">Export</button>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
