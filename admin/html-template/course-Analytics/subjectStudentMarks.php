<section class="panel">
	<header class="panel-heading">
		Download course Analytic in Excel sheet
	</header>
	<div class="panel-body">
		<form role="form">
			<div class="row">
				<div class="row">
					<div class="form-group ">
						<label for="Enter-Exam-Id" class="col-md-3 control-label">Enter Exam Id</label>
						<div class="col-md-7">
							<input type="text"  id = "exam-id" name ="examId" class="input-group form-control  exam-Id" >
							<span class="help-block"></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div style="text-align: right;">
						<a class="btn btn-info download-excel" name="download-excel" id="downloadExcel">Download Excel</a>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>