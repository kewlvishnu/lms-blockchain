<div id="categories" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close"  data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Exam Categories</h3>
			</div>
			<div class="modal-body">
				<div class="category-table clearfix">
				</div>
			</div>
			<!--<div class="modal-footer">
				<div  class="pull-right">
					<span>Don't have an account?</span>
					<a href="signup-student.php#student" class="btn btn-primary">Sign Up</a>
				</div>
			</div>-->
		</div>
	</div>
</div>