<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
        id="chapter-click" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        �</button>
                    <h4 class="modal-title">
                        Chapter Navigation</h4>
                </div>
                <div class="modal-body">
                <div class="row"><h5>Please choose a action to perform.</h5> </div>
                <div class="row">
					<div class="col-md-5">
					<a href="#" class="content-link btn btn-info">Add/Edit Content to Chapter</a>
					</div>
					<div class="col-md-5">
						<a href="#" class="exam-link btn btn-info">Add Exam/Assignment</a>
					</div>
				</div>
                </div>
            </div>
        </div>
    </div>