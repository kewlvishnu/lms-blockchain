<div class="row">
	<div class="col-lg-12">
		<!--breadcrumbs start -->
		<ul class="breadcrumb">
			<li><a href=""></a></li>
			<li><a href=""></a></li>
		</ul>
		<!--breadcrumbs end -->
	</div>
</div>
<div class="row">
                      <div class="col-lg-12 col-md-12"> 
						<section class="panel">
                          <header class="panel-heading">
                              Add Chapter
                          </header>
                          <div class="panel-body">
                              <div role="form">
                                      <div class="form-group">
                                          <b><i class="text-danger">*</i></b>    
                                      <label for="courseid">Chapter Name</label>
                                      <input type="text" placeholder="" id="chapter-name" class="form-control">
									  <div class="form-msg chapter-name"></div>
                                      </div>
                                  <!--<div class="form-group">
                                      <label for="coursedescription">Chapter Description</label>
                                      <textarea class="form-control" rows="5" cols="60" id="chapter-description"></textarea>
									  <div class="form-msg chapter-desc"></div>
                                    </div>-->
                                  <div class="checkbox">
                                      <label>
                                          <input id="is-demo" type="checkbox"> Allow for free demo
                                      </label>
                                  </div>
                                  <div class="form-group">
                                  <div class="row">
                                     <div class="col-lg-6">
										<button type="submit" class="btn btn-info" id="create-chapter">Create Chapter</button>
                                     </div>
                                  </div>
                                  </div>                                  
                              </div>
                          </div>
                      </section>
                      </div>
                      <div class="col-lg-6 col-md-6">
                      <!--<section class="panel">
                          <header class="panel-heading">
                              Add Assignment/Exam
                          </header>
                          <div class="panel-body">
                              <form role="form">
                                  <div class="form-group">
                                  <div class="row">
                                        <div class="col-lg-12">
											<label for="inputSuccess"></label>&nbsp; &nbsp;&nbsp;
                                          <label class="radio-inline" style="margin-top:0px;">
                                              <input type="radio" value="option1" name="optionsRadios" id="inlineradio1"> Assignment
                                          </label>
                                          <label class="radio-inline">
                                              <input type="radio" value="option2" name="optionsRadios" id="inlineradio1"> Exam
                                          </label>
                                      </div>
                                  </div>
                                  </div>                                  
                                   <div class="form-group">
                                      <label for="courseid">Name</label>
                                      <input type="text" placeholder="" id="Text2" class="form-control">
                                      </div>
                                      <div class="form-group">
                                      <label for="courseid">Chapter Name</label>
                                      <select class="form-control">
                                            <option>Chapter 1</option>
                                            <option>Chapter 2</option>
                                            <option>Chapter 3</option>
                                            <option>Chapter 4</option>
                                            <option>Independent</option>
                                            </select>
                                      </div>                                 
                                  <div class="form-group">
                                  <div class="row">
                                      <div class="col-lg-6">
                                      <button class="btn btn-info" type="submit">Add Content</button>
                                  </div>
                                  </div>
                                  </div>
                              </form>
                          </div>
                      </section>-->
                      </div>
                      </div>
                      <section class="panel">                          
							<header class="panel-heading">
                              Chapters
							</header>
                          <table class="table table-hover" id="existing-chapters">
                              <thead>
                              <tr>
                                  <th>Chapter</th>
                                  <th>Assignment</th>
                                  <th>Exam</th>
                              </tr>
                              </thead>
                              <tbody class="sortable">
                              <tr>
                                  <td>No chapters yet</td>
                              </tr>
                              </tbody>
                          </table>
                      </section>
                      <section class="panel">
                      <header class="panel-heading">
                              Independent  Assignments and Exams
                          </header>                          
                          <table class="table table-hover" id="independentExams">
                              <thead>
                              <tr>
                                  <th>Assignment</th>
                                  <th>Exam</th>
                              </tr>
                              </thead>
                              <tbody>
                              </tbody>
                          </table>
                      </section> 