<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
        id="chapter-created" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        x</button>
                    <h4 class="modal-title">
                        Create Chapter</h4>
                </div>
                <div class="modal-body">
                <div class="row"><h5>Chapter <label id="chapter-name"></label> Created Successfully</h5> </div>
                <div class="row">
					<div class="col-md-4">
					<a href="content.php" class="content-link btn btn-info">Add Content to Chapter</a>
					</div>
					<div class="col-md-4">
						<a href="#" class="exam-link btn btn-info">Add Exam/Assignment</a>
					</div>
					<div class="col-md-4">
							<button aria-hidden="true" class="btn btn-info" type="button" id="newChapter">
							Add New Chapter
							</button>               
					</div>
				</div>
                </div>
            </div>
        </div>
    </div>