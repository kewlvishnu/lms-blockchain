<section class="panel">
    <header class="panel-heading">
        <h4>Courses Table</h4>
       <!-- <a href="addNewCourse.html"><button class="btn btn-info pull-right" type="button"><i class="fa fa-plus"></i> New Course</button></a> -->
    </header>
	<div style="overflow: auto;">
    <table class="display table table-bordered table-striped" id="all-courses">
        <thead>
            <tr>
                <th>ID</th>
                <th>Courses Name</th>
                <th>Institute id </th>
                <th>Name</th>
                <th>Email </th>
                <th>Mobile </th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
	</div>
</section>