<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
           <li><a href="instituteDashboard.php"><i class="fa fa-home"></i>Dashboard</a></li>
            <li><a href="students.php">Students</a></li>
            <li><a href="invited_students_history.php">Invited Student Status</a></li>
        </ul>
        <!--breadcrumbs end -->
    </div></div>
<section class="panel">
    <header class="panel-heading">
        Invited Student Status
    </header>
	<input type="text" placeholder="Search" class="form-control search" style="margin-bottom:5px;">
    <div style="overflow: auto;">
        <table id='tbl_student_deatils' class="table">
            <thead>
                <tr>
                    <th>Sr. No.</th>
                    <th>Course</th>
                    <th>CourseId</th>
                    <th>Email Id</th>
                    <th>Name</th>
                    <th>Contact No</th>
                    <th><a class="tooltips" title="" data-placement="top" data-toggle="tooltip" data-original-title="See if students have pad themselves or enrolled through institute">Enrollment Type</a></th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
<!--                <tr><td>No any student invited yet</td></tr>-->
            </tbody>
        </table>
    </div>
</section>

