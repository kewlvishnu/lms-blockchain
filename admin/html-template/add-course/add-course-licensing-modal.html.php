<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="apply-licensing" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					�</button>
				<h4 class="modal-title">
					Apply Licensing
				</h4>
			</div>
			<div class="modal-body">
				Please Select Licensing Option and their details
				<div class="modal-body">
					<form role="form" class="form-horizontal">
					<div class="form-group">
						<div class="col-md-6">
							<div class="checkbox">
								<label>
									<input type="checkbox">
									One Year Licensing
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">
							Price:</label>
						<div class="col-lg-6">
							<input type="text" placeholder="100 " id="inputEmail4" class="form-control">
						</div>
					</div>
					<div class="form-group col-lg-12">
						Total=70+15(AC)+10(ST)+5(PGC)
					</div>
					<div class="form-group">
						<div class="col-lg-6">
							<div class="checkbox">
								<label>
									<input type="checkbox">
									One Time Transfer
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">
							Price:</label>
						<div class="col-lg-6">
							<input type="text" placeholder="100 " id="Text4" class="form-control">
						</div>
					</div>
					<div class="form-group col-lg-12">
						Total=70+15(AC)+10(ST)+5(PGC)
					</div>
					<div class="form-group">
						<div class="col-lg-6">
							<div class="checkbox">
								<label>
									<input type="checkbox">
									Selective IP transfer
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">
							Price:</label>
						<div class="col-lg-6">
							<input type="text" placeholder="100 " id="Text5" class="form-control">
						</div>
					</div>
					<div class="form-group col-lg-12">
						Total=70+15(AC)+10(ST)+5(PGC)
					</div>
					<div class="form-group">
						<div class="col-lg-6">
							<div class="checkbox">
								<label>
									<input type="checkbox">
									Commission based licensing
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">
							Price:</label>
						<div class="col-lg-6">
							<input type="text" placeholder="100 " id="Text6" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-6">
							Total=70+15(AC)+10(ST)+5(PGC)
						</div>
						<div class="col-lg-6" style="float: right; font-weight: bold;">
							*AC=Arcanemind Charges</br> *St=Services Tax</br> *PGC=Payment Gateway Charges
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">
							Commission:</label>
						<div class="col-lg-3">
							<input type="text" placeholder=" " id="Text9" class="form-control">
						</div>
						<div class="col-lg-1">
							Or</div>
						<div class="col-lg-3">
							<input type="text" placeholder=" " id="Text10" class="form-control">
						</div>
						<div class="col-lg-1">
							%</div>
					</div>
					<div class="form-group">
						<div class="col-lg-offset-2 col-lg-10" style="text-align: right;">
							<button class="btn btn-default" type="submit">
								Ok</button>
							<button class="btn btn-default" type="submit">
								Cancel</button>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
