<!--<section class="panel">
  <header class="panel-heading">
	<h4 id="profile-name"></h4>
  </header>
</section>-->
<section class="panel">
  <header class="panel-heading">
	 Add New Courses
  </header>
  <div class="panel-body">
	  <div role="form">
		  <div class="form-group">
			<div class="row">
			  <div class="col-lg-12">
					<label for="coursename"><i class="text-danger">*</i>Course Name</label>
					<input type="text" placeholder="" id="course-name" class="form-control">
				  </div>
			  </div>
		  </div>
		  <div class="form-group">
			<div class="row">
			  <div class="col-lg-12">
					<label for="coursename"><i class="text-danger">*</i>Course Subtitle</label>
					<input type="text" placeholder="Course Subtitle" id="course-subtitle" class="form-control" maxlength="200">
			  </div>                                      
			</div>
		  </div>
		  <div class="form-group">
			<label for="coursedescription"><i class="text-danger">*</i>Course Description</label>
			  <textarea class="form-control" rows="5" cols="60" id="course-description" maxlength="1000"></textarea>
		  </div>
		  <div class="form-group">
			<label for="targetaudience"><i class="text-danger">*</i>Target Audience</label>
			<input type="text" class="form-control" id="target-audience">
		  </div>
		<div class="form-group">
			<label for="tags">Tags</label>
			<input type="text" class="form-control" id="tags">
		</div>
			<div class="form-group">
			<div class="row">
			  <div class="col-md-6">
					<label for="coursename"><i class="text-danger">*</i>Course Start Date</label>
					<input type="text" placeholder="10 Jan 2015" id="live-date" class="form-control">
			  </div>
			  <div class="col-md-6">
				  <label for="coursename">Set End Date</label>
				  <select class="form-control m-bot15 custom-form" id="set-end-date">
					<option value="0">No</option>
					<option value="1">Yes</option>
				  </select> 
			  </div>
			</div>
			</div>
			<div class="form-group hidden-on-load">
			<div class="row">
			  <div class="col-md-6">
				  <label for="courseid">Course End Date</label>
				  <input type="text" placeholder="10 Jan 2015" id="end-date" class="form-control" />
			  </div>
			</div>
				
		  </div>
		  <div class="form-group">
			<div class="row">
		  <!--<div class="col-md-6">
			  <form id="course-image-form"  method="post" enctype="multipart/form-data">
				  <label for="exampleInputFile">Course Images</label>
				  <input type="file" id="course-image" name="course-image" />
				  <img src id="course-image-preview" style="max-width:100%;height:auto;" class="crop"/>
				  <input type="hidden" id="x" name="x" />
					<input type="hidden" id="y" name="y" />
					<input type="hidden" id="w" name="w" />
					<input type="hidden" id="h" name="h" />
				  
			  </form>
		  </div>-->
                  <div class="col-md-6">
				  <label for="courseid">Course Category</label>
                                  <br/>
				  <select id="course-cat" class="form-control" multiple>
					<option value="1">Entrance Exams</option>
					<option value="2">School & College Prep</option>
                    <option value="3">Hobbies & Skills</option>
                    <option value="4">Business & Management</option>
                    <option value="5">IT & Software</option>
                    <option value="6">Entrepreneurship</option>
                    <option value="7">Miscellaneous</option>
                    <option value="8">Finance & Accounting</option>
				  </select>
			</div>
        
		  </div>
		  </div>
              <div class="form-group">
                   <a href="#" target="_self" class="btn btn-info" id="create-course">Create Course</a> 
              </div>
	 </div>

  </div>
</section>