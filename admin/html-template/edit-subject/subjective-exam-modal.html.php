<!--<div id="modal-exam" class="modal fade in" id="chapter-click" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: block;">-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal-subjectiv-exam" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                    x</button>
                <h4 class="modal-title">
                     subjective Exam
                </h4>
            </div>
            <input type="hidden" value="0" id="examId">
            <div class="modal-body">
                <div class="row">
<div class="col-lg-12">
<table class="table table-hover">
				<thead>
					<tr>
						<th>Tittle</th>
						<th>Start Date</th>
						<th>Attempts</th>
						<th>Type</th>
						<th>Status</th>
						<th>End Date</th>
					</tr>
				</thead>
				<tbody>
				      <tr><td><span id="modelin-exam-name1"></span></td>
					  <td><span id="modelin-exam-startDate1"></span></td>
                      <td><span id="modelin-exam-attempts1"></span></td>                 
                      <td><span id="modelin-exam-type1"></span></td> 
                      <td><span id="modelin-exam-status1"></span></td> 
                      <td><span id="modelin-exam-endDate1">--</span></td></tr>
				</tbody>
			</table>
			</div>				
                    <div class="col-md-12">
                        <br>
                        <a class="btn btn-xs btn-success editsubjectiveExam" href="#"><i class="fa fa-edit"></i>  Edit Exam and questions</a>
                      <!--  <a class="btn btn-xs btn-primary delete-subjective-exam" href="#"><i class="fa fa-trash-o"></i> Delete</a>&nbsp;&nbsp;&nbsp;-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>