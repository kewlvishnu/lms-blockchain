<!--<div id="modal-exam" class="modal fade in" id="chapter-click" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: block;">-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal-exam" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                    x</button>
                <h4 class="modal-title">
                    Assignments/Exam
                </h4>
            </div>
            <input type="hidden" value="0" id="examId">
            <div class="modal-body">
                <div class="row">
<div class="col-lg-12">
<table class="table table-hover">
				<thead>
					<tr>
						<th>Tittle</th>
						<th>Chapter</th>
						<th>Start Date</th>
						<th>Attempts</th>
						<th>Type</th>
						<th>Status</th>
						<th>End Date</th>
					</tr>
				</thead>
				<tbody>
				      <tr><td><span id="modelin-exam-name"></span></td>
					  <td><span id="modelin-chapter-name"></span></td>
					  <td><span id="modelin-exam-startDate"></span></td>
                      <td><span id="modelin-exam-attempts"></span></td>                 
                      <td><span id="modelin-exam-type"></span></td> 
                      <td><span id="modelin-exam-status"></span></td> 
                      <td><span id="modelin-exam-endDate">--</span></td></tr>
				</tbody>
			</table>
			</div>				
                    <div class="col-md-12">
                        <br>
                        <a class="btn btn-xs btn-success step2" href="#"><i class="fa fa-edit"></i>  Edit Questions</a>&nbsp;&nbsp;&nbsp;
                        <a class="btn btn-xs btn-danger step1" href="#"><i class="fa fa-cog"></i>  Settings</a>&nbsp;&nbsp;&nbsp;
                        <a class="btn btn-xs btn-primary report" href="#"><i class="fa fa-globe"></i> Result</a>&nbsp;&nbsp;&nbsp;
                        <!--<a class="btn btn-xs btn-primary info" href="#"><i class="fa fa-info"></i> Information</a>&nbsp;&nbsp;&nbsp;-->
                        <a class="btn btn-xs btn-info copy-exam" href="#modal-copy-exam" data-toggle="modal" data-dismiss='modal'><i class="fa fa-copy"></i> Copy </a>&nbsp;&nbsp;&nbsp;
                        <a class="btn btn-xs btn-primary delete-exam" href="#"><i class="fa fa-trash-o"></i> Delete</a>&nbsp;&nbsp;&nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>