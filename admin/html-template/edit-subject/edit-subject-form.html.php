<div id="subject-details" class="clearfix">
	<aside class="col-md-9">
		<section>
			<div class="custom_img">
				<img src="" id="course-image-preview"/>
			</div>
			<div class="subject-image-thumb">
				<img src="" alt="" id="subjectImage"/>
				<a class="btn btn-xs btn-danger upload subStyle" href="#upload-image" data-toggle="modal" ><i class="fa fa-pencil"></i></a>
			</div>
			<header id="clear-both">
				<h3><span id="subject-name-span"></span>
					<!--<button class="btn btn-success btn-xs edit-button1 pull-right"><i class="fa fa-pencil"></i>  syllabus </button>-->
					<button class="btn btn-danger btn-xs edit-button pull-right"><i class="fa fa-pencil"></i> Subject</button>
				</h3>
			</header>
			<strong>Course Name: </strong><span class="mb40" id="course-name"></span>
			<h5><b>Subject Description</b></h5>
			<p id="subject-desc"></p>
		</section>
		<section class="panel">
			<header class="panel-heading">
				<h4>Existing Sections </h4>
				<div class="btn-group pull-right custom-alignment">
					<!--<button type="button" class="btn btn-info btn-xs  dropdown-toggle" data-toggle="dropdown">Add Chapter <span class="caret"></span></button>
					<ul class="dropdown-menu" role="menu">
						<li>-->
						<a href="add-chapter.php" class="btn btn-info btn-xs add-chapter-link">Add Section</a>
						<a href="edit-chapter.php" class="btn btn-warning btn-xs add-exam-link">Add Exam/Assignment</a>
						<!-- <a href="javscript:void(0)" class="btn btn-warning btn-xs add-subjective-exam-link">Add Subjective Exam</a> -->
						<!--<li><a href="#">Import Chapter</a></li>
					</ul>-->
				</div>
			</header>
			<table class="table table-hover" id="existing-chapters">
				<thead>
					<tr>
						<th width="40%">Section</th>
						<th width="10%"></th>
						<th width="25%">Assignment</th>
						<th width="25%">Exam</th>
					</tr>
				</thead>
				<tbody>
					<tr><td> No Section added yet </td></tr>
				</tbody>
			</table>
		</section>
		<section class="panel">
			<header class="panel-heading">
					<h4>Independent Assignments and Exams</h4>
			</header>
			<table class="table table-hover" id="independentExams">
				<thead>
				<tr>
					<th width="50%">Assignment</th>
					<th width="50%">Exam</th>
				</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</section>
		<section class="panel">
			<header class="panel-heading">
					<h4>Subjective Exams</h4>
					<div class="btn-group pull-right custom-alignment">
						<a href="javscript:void(0)" class="btn btn-warning btn-xs add-subjective-exam-link">Add Subjective Exam</a>
					</div>
			</header>
			<table class="table table-hover" id="subjectiveExams">
				<!-- <thead>
				<tr>
					<th>Subjective Exams</th>
					<th>Check</th>
				</tr>
				</thead>
				<tbody>
				</tbody> -->
			</table>
		</section>
		<section class="panel">
			<header class="panel-heading">
					<h4>Manual Exams</h4>
					<div class="btn-group pull-right custom-alignment">
						<a href="javscript:void(0)" class="btn btn-warning btn-xs add-manual-exam-link">Add Manual Exam</a>
					</div>
			</header>
			<table class="table table-hover" id="manualExams">
				<!-- <thead>
				<tr>
					<th>Manual Exams</th>
					<th>Type</th>
					<th></th>
				</tr>
				</thead><tbody>
				</tbody> -->
			</table>
		</section>
	
	</aside>
	<aside class="profile-nav col-md-3">
		<section class="panel">
			<ul class="nav nav-pills nav-stacked">
				<li><a><i class="fa fa-book"></i>Subject Id <span class="label label-primary pull-right r-activity" id="subject-id"></span></a></li>
			</ul>

		</section>
		<section class="panel">
			<div class="flat-carousal">Student Ratings</div>
			<span id="font-size1">
				<input type="hidden" class="rating ratingStars" data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="0" DISABLED/>
				&nbsp;(<span id="ratingTotal"></span>)
				<?php
					@session_start();
					if($_SESSION['userRole'] == 2)
					{
						?>
						<i class="tooltips fa fa-info-circle" data-original-title="This rating and Review is for your subject only." data-placement="top"  id="font-size2"></i>
						<?php
					}
				?>
				<button class="btn btn-xs btn-info pull-right mar3" id="viewReview" >View Reviews</button>
			</span>
		</section>
		<section class="panel" id="height-35">
			<div class="flat-carousal" >Course Analytics</div>
			<span id="font-size1">
				<button class="btn btn-xs btn-info pull-right mar3" id="viewCourseAnalytics" >View Analytics</button>
			</span>
		</section>
		<section class="panel disp-none" id="manageProfessorSection">
			<div class="flat-carousal">
				<span>Assigned Professor</span>
				<a href="#subject-professor-modal" data-toggle="modal" class="btn btn-xs btn-success pull-right">Manage</a>
			</div>
			<ul class="nav nav-pills nav-stacked" id="subject-professors-teaser">
				<li><a>No professors.</a></li>
			</ul>
		</section>
		<section class="panel" id="height-35">
			<div class="flat-carousal" >Subject Syllabus</div>
			<span id="font-size1">
				<div class="col-md-12"><a href="#upload-syllabus-modal" data-toggle="modal" class="btn btn-sm btn-success btn-block mar3 edit-syllabus">Upload Syllabus </a></div>
			</span>
		</section>
		<section class="panel" id="height-35">
			<div class="flat-carousal" >Subject Students</div>
			<span id="font-size1">
				<div class="col-md-12"><a href="subject-students.php?courseId=<?php echo $courseId; ?>&subjectId=<?php echo $subjectId; ?>" class="btn btn-sm btn-success btn-block mar3 view-subject-students">View Students List </a></div>
			</span>
		</section>
	</aside>
</div>

<div class="panel hidden-l minheight515" id="edit-form" >
	<header class="panel-heading">
		Edit Subject
		<button class="btn btn-primary btn-xs cancel-edit-button cross-button"><i class="fa fa-times"></i></button>
	</header>
	<div class="panel-body">
		<div role="form">
			<div class="form-group">
				<div class="row">
					<div class="col-lg-6">
						<b><i class="text-danger">*</i></b>
						<label for="coursename">Subject Name</label>
						<input type="text" placeholder="" id="subject-name" class="form-control" />
						<div class="form-msg subject-name"></div>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-lg-12">
						<label for="coursedescription">Subject Description</label>
						<textarea cols="60" rows="5" class="form-control" id="subject-description"></textarea>
						<div class="form-msg subject-desc"></div>
					</div>
				</div>
			</div>

			<!--<div class="form-group">
				<div class="row">
					<div class="col-lg-6">
						<form id="subject-image-form"  method="post" enctype="multipart/form-data">
							<label for="exampleInputFile">Subject Image</label>
							<input type="file" id="subject-image" name="subject-image" />
							<img src id="subject-image-preview" style="max-width:100%;height:auto;" class="crop"/>
							<input type="hidden" id="x" name="x" />
							<input type="hidden" id="y" name="y" />
							<input type="hidden" id="w" name="w" />
							<input type="hidden" id="h" name="h" />
						</form>
					</div>
				</div>
			</div>-->
			<div class="form-group">
				<div class="row">
					<div class="col-lg-12 test-align-right" >
						<button class="btn btn-primary btn-xs cancel-edit-button"><i class="fa fa-times"></i> Cancel</button>
						<button class="btn btn-primary btn-xs save-button" ><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>