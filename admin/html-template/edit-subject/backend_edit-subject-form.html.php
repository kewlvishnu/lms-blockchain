<div id="subject-details" class="clearfix">
    <aside class="col-md-9">
        <section>
            <div class="custom_img">
                <img src="../img/about_1.jpg" id="course-image-preview" />
            </div>
            <div class="subject-image-thumb">
                <img src="img/profile-avatar.jpg" alt="" id="subject-image-preview" />
            </div>
            <header style="clear:both">
                <h3><span id="subject-name"></span>
                <!--	<button class="btn btn-info btn-xs edit-button pull-right"><i class="fa fa-pencil"></i> Subject</button> -->
                </h3>
            </header>
            <strong>Course Name: </strong><span id="course-name"></span><br />
            <br />
            <h5><b>Subject Description</b></h5>
            <p id="subject-desc"></p>
        </section>
        <section class="panel">
            <header class="panel-heading">
                <h4>Existing Content </h4>

                <!--	<div class="btn-group pull-right">
                                <button type="button" class="btn btn-info btn-xs  dropdown-toggle" data-toggle="dropdown">Add Content <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                        <li><a href="add-chapter.php" class="add-chapter-link">Create New Content</a></li>
                                        <li><a href="#">Import Content</a></li>
                                </ul>
                        </div>
                -->
            </header>
            <table class="table table-hover" id="existing-chapters">
                <thead>
                    <tr>
                        <th>Chapter</th>
                        <th>Assignment</th>
                        <th>Exam</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </section>
    </aside>
    <aside class="profile-nav col-md-3">
        <section class="panel">
            <ul class="nav nav-pills nav-stacked">
                <li><a><i class="fa fa-book"></i>Subject Id <span class="label label-primary pull-right r-activity" id="subject-id"></span></a></li>
            </ul>

        </section>
        <section class="panel">
            <div class="flat-carousal">
                <span>Assigned Professor</span>
                <!--<a href="#subject-professor-modal" data-toggle="modal" class="btn btn-xs btn-success pull-right">Manage</a>
                -->
            </div>
            <ul class="nav nav-pills nav-stacked" id="subject-professors-teaser">
                <li><a>No professors.</a></li>
            </ul>
        </section>
    </aside>
</div>

<div class="panel hidden-l"  id="edit-form">
    <header class="panel-heading">
        Edit Subject
        <button class="btn btn-primary btn-xs cancel-edit-button" style="float: right; border-color: rgb(255, 255, 255); background-color: rgb(10, 49, 81); margin-top: 0px;"><i class="fa fa-times"></i></button>
    </header>
    <div class="panel-body">
        <div role="form">
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6">
                        <label for="coursename">Subject Name</label>
                        <input type="text" placeholder="" id="subject-name" class="form-control" />
                        <div class="form-msg subject-name"></div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6">
                        <form id="subject-image-form"  method="post" enctype="multipart/form-data">
                            <label for="exampleInputFile">Subject Image</label>
                            <input type="file" id="subject-image" name="subject-image" />
                            <img src id="subject-image-preview" style="max-width:100%;height:auto;" class="crop"/>
                            <input type="hidden" id="x" name="x" />
                            <input type="hidden" id="y" name="y" />
                            <input type="hidden" id="w" name="w" />
                            <input type="hidden" id="h" name="h" />
                        </form>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-12">
                        <label for="coursedescription">Subject Description</label>
                        <textarea cols="60" rows="5" class="form-control" id="subject-description"></textarea>
                        <div class="form-msg subject-desc"></div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-12" style="text-align:right;">
                        <button class="btn btn-primary btn-xs cancel-edit-button"><i class="fa fa-times"></i> Cancel</button>
                        <button class="btn btn-primary btn-xs save-button" ><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>