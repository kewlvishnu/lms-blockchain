<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal-copy-exam" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    x</button>
                <h4 class="modal-title">
                    Copy Assignment/Exam </h4>
            </div>
            <div class="modal-body" style="padding-right: 35px;">
                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <label for="inputEmail1">
                            Please Enter a name   </label>
                        <input type="textarea" class="form-control" placeholder="" id="new-exam"> 
                    </div>
                    <div class="form-group">
                        <div style="text-align: right;">
                            <a class="btn btn-info" id="copy-exam" >
                                Copy </a> 
                            <button aria-hidden="true" data-dismiss="modal" class="btn btn-danger" type="button">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>