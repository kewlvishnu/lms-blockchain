<section class="panel existing-students">
	<header class="panel-heading">
		<h4>Existing Students <div class="pull-right"><a href="javascript:void(0)" data-student="0" class="btn btn-xs btn-warning js-chat">Public Chat</a></div></h4>
	</header>
	<table class="table table-striped" id="tbl_students_details_of_subject">
		<thead>
			<tr>
				<!-- <th><input type="checkbox" class="js-select-all" /></th> -->
				<th>ID</th>
				<th>Username</th>
				<th>Password (Temporary)</th>
				<th>Name</th>
				<th>Email</th>
				<th>Parent (UserID/Password)</th>
				<th>Chat</th>
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>
</section>