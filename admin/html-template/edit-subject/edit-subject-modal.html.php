<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
	id="subject-professor-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">
					x</button>
				<h4 class="modal-title">
					Manage Professor</h4>
			</div>
			<div class="modal-body">
				<div role="form">
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-10">
							<label class="control-label">
								Available Professor
							</label>
							<br/>
							<br/>
							<select multiple="multiple" class="multi-select multiple" id="subject-professors" name="my_multi_select1[]">
								
							</select>

						</div>
					</div>
				</div>
				
				<div class="row clearfix panel-body" style="text-align: right;">
                                    <a href="invite-professors.php" class="btn btn-success btn-sm pull-left" >
						Invite Professors
					</a>
					<button class="btn btn-info btn-sm" type="submit" id="update-subject-professors">
						Ok
					</button>
					<button class="btn btn-default btn-sm" type="button" data-dismiss="modal" aria-hidden="true">
						Cancel
					</button>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>