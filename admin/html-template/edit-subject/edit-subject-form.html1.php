<section class="panel">
  <header class="panel-heading">
	  <h4 id="profile-name"></h4>
  </header>
</section>
<section class="panel" id="subject-details">
  <header class="panel-heading">
	<label id="course-name"></label>: <label id="subject-name-title"></label>
	<button class="btn btn-primary btn-xs edit-button" style="float: right; border-color: rgb(255, 255, 255); background-color: rgb(10, 49, 81); margin-top: 0px;"><i class="fa fa-pencil"></i></button>
  </header>
  <div class="panel-body view-section">
	  <div role="form">                             
		  <div class="form-group">
			<div class="row">
			  <div class="col-lg-6 col-md-6">
				  <label for="coursename">Subject Name</label>
				  <label class="custom_label" id="subject-name"></label>
			  </div>
			  <div class="col-lg-6 col-md-6">
				  <label for="courseid">Subject Id</label>
				  <label class="custom_label" id="subject-id"></label>
			  </div>
			</div>
		  </div>
		  <div class="form-group">
			  <label for="coursedescription">Subject Description</label>
			  <label class="custom_label" id="subject-desc"></label>
		   </div>
		  <div class="form-group">
		  <div class="row">
			  <div class="col-lg-6 col-md-6">
				<button type="submit" class="btn btn-info">Import Subject</button>
			  </div>
			  <div class="col-lg-6 col-md-6">
			  <a href="#subject-professor-modal" data-toggle="modal" class="btn btn-info btn-success"> Manage Professor </a>
		  </div>
		  </div>
		  </div> 
		  <a href="add-chapter.php" class="add-chapter-link"><button class="btn btn-info" type="button"><i class="fa fa-plus"></i> New Chapter</button></a>							   
	  </div>
  </div>
  <div class="panel-body edit-section">
	<div role="form" id="edit-form">                             
	  <div class="form-group">
		<div class="row">
		  <div class="col-lg-6 col-md-6">
			  <label for="coursename">Subject Name</label>
			  <input type="text" placeholder="" id="subject-name" class="form-control" />
			  <div class="form-msg subject-name"></div>
		  </div>
		</div>
	  </div>
	  <div class="form-group">
		<div class="row">
		  <div class="col-lg-12">
			  <label for="coursedescription">Subject Description</label>
			  <textarea cols="60" rows="5" class="form-control" id="subject-description"></textarea>
			  <div class="form-msg subject-desc"></div>
		  </div>
		</div>
	  </div>
	  <div class="form-group">
	  <div class="row">
		  <div class="col-lg-12">
		  <button class="btn btn-primary btn-xs cancel-button" style="float:right; border-color:#fff; background-color:#0A3151;"><i class="fa fa-save"></i> Cancel</button>
		<button class="btn btn-primary btn-xs save-button" style="float:right; border-color:#fff; background-color:#0A3151;"><i class="fa fa-save"></i> Save</button>
		  </div>
	  </div>
	  </div>  
	</div>
  </div>
</section>

<section class="panel">
  <header class="panel-heading">
	  Existing Content
  </header>
  <table class="table table-hover" id="existing-chapters">
	  <thead>
	  <tr>
		  <th>Chapter</th>
		  <th>Assignment</th>
		  <th>Exam</th>
	  </tr>
	  </thead>
	  <tbody class='sortable'>
		<tr>
			<td>No Chapters yet!</td>
		</tr>
	  </tbody>
  </table>
</section>                      