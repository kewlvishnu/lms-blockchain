<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
        id="modal_purchase_key" class="modal fade">
		
          <div class="modal-dialog">
		     <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×</button>
                    <h4 class="modal-title">
                         Add Course Key</h4>
                </div>
                <div class="modal-body" style="padding-right: 35px;">
                    <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <label for="inputEmail1">
                            Enter No of Course Key</label>
                        <input type="text" id='totalkeys' placeholder="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1">
                            Price: <span id='keyrate'></span>/Course Key</label>
                    </div>
                        <div class="form-group">
                        <label for="inputEmail1">
                            Total Amount: <span id='totalkeyrate'>0</span></label>
                    </div>
                    <div class="form-group">
                        <div style="text-align: right;">
                            <a class="btn btn-info" id="purchase_key"><i class="fa fa-shopping-cart"></i>
                                    Add Keys </a> </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>