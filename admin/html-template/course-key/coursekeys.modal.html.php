<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
        id="modal_change_key_rate" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×</button>
                    <h4 class="modal-title">
                         Change Rate</h4>
                </div>
                <div class="modal-body" style="padding-right: 35px;">
                    <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <label for="inputEmail1">
                            Enter Rate (in IGRO)</label>
                        <input type="text" placeholder="" id="key_rate_course" class="form-control">
                    </div>
                        
                    <div class="form-group">
                        <div style="text-align: right;">
                            <a id="save_rate" class="btn btn-info" >
                                    Save </a> 
									<button aria-hidden="true" data-dismiss="modal" class="btn btn-danger" type="button">Cancel</button>
												
                                   </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>