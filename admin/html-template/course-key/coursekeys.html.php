<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-12">
                        <!--breadcrumbs start -->
                        <ul class="breadcrumb">
                            <li><a href="dashboard.php"><i class="fa fa-home"></i>Dashboard</a></li>
                            <li><a href="CourseKey.php">History Course Key</a></li>
                        </ul>
                        <!--breadcrumbs end -->
                    </div></div>
				<?php
					if($_SESSION['userRole'] != 3) {
				?>
					<section class="panel">
						<header class="panel-heading">
							Course Key Consumption History
						</header>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-3">Used :&nbsp;<span id="active_key"  class="badge bg-important">0</span></div>
								<div class="col-xs-3">Remaining :&nbsp;<span id="remaining_key" class="badge bg-info">0</span></div>
								<div class="col-xs-3">Total :&nbsp;<span id="total_keys" class="badge bg-success">0</span></div>
								<div class="col-xs-12 col-sm-3"><a id="btn_Purchase_More" onclick="$('#totalkeys').val('');$('#inviteon_email').val(''); " class="btn btn-xs btn-danger" style="margin-top: 5px;" data-toggle="modal" href="#Purchase_More"> Purchase More </a></div>
							</div></div>
					</section>
				<?php
					}
				?>
                <section class="panel">
					<?php
						if($_SESSION['userRole'] == 3) {
					?>
						<header class="panel-heading">
							Purchased Courses
						</header>
					<?php
						}
						else {
					?>
						<header class="panel-heading tab-bg-dark-navy-blue">
							<ul class="nav nav-tabs nav-justified ">
								<li class="active">
									<a data-toggle="tab" href="#popular">
										History                                           
									</a>
								</li>
								<li class="">
									<a data-toggle="tab" href="#comments">
										Course Wise                       
									</a>
								</li>
								<li class="">
									<a data-toggle="tab" href="#purchased-courses">
										Purchased Courses
									</a>
								</li> 
							</ul>
						</header>
					<?php
						}
					?>
                    <div class="panel-body">
						<?php
							if($_SESSION['userRole'] != 3) {
						?>
							<div class="tab-content tasi-tab">
								<div id="popular" class="tab-pane active">
									<article class="media">
										<div class="media-body">
											<table id='tabl_Totalkeys' class="table table-striped">
												<thead>
													<tr>
														<th>Purchased Date</th>
														<th>Course Key Purchased</th>
														<th>Rate</th>
														<th>Total Amount</th>
													</tr>
												</thead>
												<tbody>
	
												</tbody>
											</table>
										</div>
									</article>
								</div>
								<div id="comments" class="tab-pane">
									<article class="media">
										<div class="media-body">
											<table id="tbl_course_student_count" class="table table-striped">
												<thead>
													<tr>
														<th>Course Id</th>
														<th>Course Name</th>
														<th>Student (Course key)</th>
														<th>Student (Stand alone)</th>
													</tr>
												</thead>
											<tbody>
												</tbody>
											</table>
										</div>
									</article> 
								</div>
							<?php
								}
							?>
							<div id="purchased-courses" class="tab-pane">
								<article class="media">
									<div class="media-body">
										<table id="tbl-purchased-courses" class="table table-striped">
											<thead>
												<tr>
													<th>Course Id</th>
													<th>Course Name</th>
													<th>Date</th>
													<th>Institute</th>
													<th>Price</th>
													</tr>
											</thead>
											<tbody>
											
											</tbody>
										</table>
									</div>
								</article>    
							</div> 
                        </div>
                    </div>
                </section>
            </div>
            <!--<div class="col-md-3">
                <div class="fb-timeliner">
                    <h2 class="recent-highlight">Updates</h2>
                    <ul>
                        <li class="active"><a href="#">December</a></li>
                        <li><a href="#">November</a></li>
                        <li><a href="#">October</a></li>
                        <li><a href="#">September</a></li>
                        <li><a href="#">August</a></li>
                        <li><a href="#">July</a></li>
                        <li><a href="#">June</a></li>
                        <li><a href="#">May</a></li>
                        <li><a href="#">April</a></li>
                        <li><a href="#">March</a></li>
                        <li><a href="#">February</a></li>
                        <li><a href="#">January</a></li>
                    </ul>
                </div>
            </div>-->
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
     id="modal_student" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <h4 class="modal-title">
                    Consumption Detail</h4>
            </div>
            <div class="modal-body" style="padding-right: 35px;">
                <table  id="tbl_course_details" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Student Name</th>
                            <th>Email Id</th>
                            <th>Contact No.</th>
                            <th>Course Key</th>
                            <th>Enrollment Date</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
     id="Div2" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <h4 class="modal-title">
                    Consumption Detail</h4>
            </div>
            <div class="modal-body" style="padding-right: 35px;">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Student Name</th>
                            <th>Email Id</th>
                            <th>Contact No.</th>
                            <th>Enrollment Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ABC</td>
                            <td>abc@gmail.com</td>
                            <td>8008008000</td>
                            <td>15-04-2014</td>

                        </tr>
                        <tr>
                            <td>ABC</td>
                            <td>abc@gmail.com</td>
                            <td>8008008000</td>
                            <td>15-04-2014</td>
                        </tr>
                        <tr>
                            <td>ABC</td>
                            <td>abc@gmail.com</td>
                            <td>8008008000</td>
                            <td>15-04-2014</td>
                        </tr>
                        <tr>
                            <td>ABC</td>
                            <td>abc@gmail.com</td>
                            <td>8008008000</td>
                            <td>15-04-2014</td>
                        </tr>
                        <tr>
                            <td>ABC</td>
                            <td>abc@gmail.com</td>
                            <td>8008008000</td>
                            <td>15-04-2014</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>