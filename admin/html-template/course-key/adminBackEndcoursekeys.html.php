<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-12">
                        <!--breadcrumbs start -->
                        <ul class="breadcrumb">
                            <li><a href="instituteDashboard.php"><i class="fa fa-home"></i>Dashboard</a></li>
                            <li><a href="CourseKey.php">History Course Key</a></li>
                        </ul>
                        <!--breadcrumbs end -->
                    </div></div>
                <section class="panel">
                    <header class="panel-heading">
                        Course Key Consumption History
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-3">Used :&nbsp;<span id="active_key"  class="badge bg-important"><a   href="#" data-toggle="tab" style="color:#fff;"></a></span></div>
                            <div class="col-xs-3">Remaining :&nbsp;<span id="remaining_key" class="badge bg-info"></span></div>   
                            <div class="col-xs-3">Total :&nbsp;<span id="total_keys" class="badge bg-success">125</span></div>
<!--                            <div class="col-xs-12 col-sm-3"><a id="btn_Purchase_More" onclick="$('#totalkeys').val('');$('#inviteon_email').val(''); " class="btn btn-xs btn-danger" style="margin-top: 5px;" data-toggle="modal" href="#Purchase_More"> Purchase More </a></div>-->
                        </div></div>
                </section>
                <section class="panel">
                    <header class="panel-heading tab-bg-dark-navy-blue">
                        <ul class="nav nav-tabs nav-justified ">
                            <li class="active">
                                <a data-toggle="tab" href="#popular">
                                    History                                           
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#comments">
                                    Course Wise                       
                                </a>
                            </li>
                    <!--    <li class="">
                                <a data-toggle="tab" href="#recent">
                                    Used Key History
                                </a>
                            </li> -->
                        </ul>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content tasi-tab">
                            <div id="popular" class="tab-pane active">
                                <article class="media">
                                    <div class="media-body">
                                        <table id='tabl_Totalkeys' class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Purchased Date</th>
                                                    <th>Course Key Purchased</th>
                                                    <th>Rate</th>
                                                    <th>Total Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </article>
                            </div>
                            <div id="comments" class="tab-pane">
                                <article class="media">
                                    <div class="media-body">
                                        <table id="tbl_course_student_count" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Course Id</th>
                                                    <th>Course Name</th>
                                                    <th>Student (Course key)</th>
                                                    <th>Student (Stand alone)</th>
                                                </tr>
                                            </thead>
                                        <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </article> 
                            </div>
                                  <div id="recent" class="tab-pane">
                                        <article class="media">
                                            <div class="media-body">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Date</th>
                                                            <th>Course Id</th>
                                                            <th>Course Name</th>
                                                            <th>No of Key Consume</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>15-06-2014</td>
                                                            <td>C10001</td>
                                                            <td>My course1</td>
                                                            <td><a data-toggle="modal" href="#Div1">12</a> </td>
                                                        </tr>
                                                        <tr>
                                                            <td>15-05-2014</td>
                                                            <td>C10002</td>
                                                            <td>My course2</td>
                                                            <td><a data-toggle="modal" href="#Div1">20</a> </td>
                                                        </tr>
                                                        <tr>
                                                            <td>15-04-2014</td>
                                                            <td>C10003</td>
                                                            <td>My course3</td>
                                                            <td><a data-toggle="modal" href="#Div1">10</a> </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </article>    
                                    </div> 
                        </div>
                    </div>
                </section>
            </div>
            <!--<div class="col-md-3">
                <div class="fb-timeliner">
                    <h2 class="recent-highlight">Updates</h2>
                    <ul>
                        <li class="active"><a href="#">December</a></li>
                        <li><a href="#">November</a></li>
                        <li><a href="#">October</a></li>
                        <li><a href="#">September</a></li>
                        <li><a href="#">August</a></li>
                        <li><a href="#">July</a></li>
                        <li><a href="#">June</a></li>
                        <li><a href="#">May</a></li>
                        <li><a href="#">April</a></li>
                        <li><a href="#">March</a></li>
                        <li><a href="#">February</a></li>
                        <li><a href="#">January</a></li>
                    </ul>
                </div>
            </div>-->
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
     id="Purchase_More" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <h4 class="modal-title">
                    Purchase Course Key</h4>
            </div>
            <div class="modal-body" style="padding-right: 35px;">
                <form role="form">
                    <div class="form-group">
                        <label for="inputEmail1">
                            Enter No of Course Key</label>
                        <input type="text" value="" id='totalkeys' placeholder="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1">
                            Price: <span id='keyrate'></span>/Course Key</label>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1">
                            Total Amount: <span id='totalkeyrate'>0</span></label>
                    </div>
                  <!-- <div class="form-group">
                        <div style="text-align: right;">
                            <a class="btn btn-info" id="purchase_key"><i class="fa fa-shopping-cart"></i>
                                Buy Now </a> </div>
                    </div>-->
                   <div class="form-group">
                        <div style="text-align: right;">
                            <a class="btn btn-info" href="#modal_keys_purchaseInvitation" data-toggle="modal" id="keys-purchase"><i class="fa fa-shopping-cart"></i>
                               Get </a> </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
     id="modal_student" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <h4 class="modal-title">
                    Consumption Detail</h4>
            </div>
            <div class="modal-body" style="padding-right: 35px;">
                <table  id="tbl_course_details" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Student Name</th>
                            <th>Email Id</th>
                            <th>Contact No.</th>
                            <th>Course Key</th>
                            <th>Enrollment Date</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
     id="Div2" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <h4 class="modal-title">
                    Consumption Detail</h4>
            </div>
            <div class="modal-body" style="padding-right: 35px;">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Student Name</th>
                            <th>Email Id</th>
                            <th>Contact No.</th>
                            <th>Enrollment Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ABC</td>
                            <td>abc@gmail.com</td>
                            <td>8008008000</td>
                            <td>15-04-2014</td>

                        </tr>
                        <tr>
                            <td>ABC</td>
                            <td>abc@gmail.com</td>
                            <td>8008008000</td>
                            <td>15-04-2014</td>
                        </tr>
                        <tr>
                            <td>ABC</td>
                            <td>abc@gmail.com</td>
                            <td>8008008000</td>
                            <td>15-04-2014</td>
                        </tr>
                        <tr>
                            <td>ABC</td>
                            <td>abc@gmail.com</td>
                            <td>8008008000</td>
                            <td>15-04-2014</td>
                        </tr>
                        <tr>
                            <td>ABC</td>
                            <td>abc@gmail.com</td>
                            <td>8008008000</td>
                            <td>15-04-2014</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>