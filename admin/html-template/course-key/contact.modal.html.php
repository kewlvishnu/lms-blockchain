<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
	 id="modal_keys_purchaseInvitation" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					×</button>
				<h4 class="modal-title">
					Customer Support</h4>
			</div>
			<div class="modal-body" style="padding-right: 35px;">
				<form role="form" class="form-horizontal">
					<div class="form-group">
						<span for="inputEmail1">
						  You will be soon contacted by our Sales Team at your registered Mobile No  &  Email Id. <br/>
						  You can also  get in touch with us at  <a class="small-txt text-danger" href="mailto: sales@arcanemind.com " > sales@arcanemind.com </a>   <br/>
						If you would like to be contacted at some other  mobile number or email id, please enter in the below box.
						</span>
					   
						
					  </div>
					<div class="form-group">
						<label for="inputEmail1">
							Email/Phone no </label>
						<input type="text" id='inviteon_email' placeholder="" class="form-control">
					</div>
					<div class="form-group">
						<div style="text-align: right;">
							<a class="btn btn-info" id="keys-purchaseInvitation">
								Send 
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
     id="Purchase_More" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <h4 class="modal-title">
                    Purchase Course Key</h4>
            </div>
            <div class="modal-body" style="padding-right: 35px;">
                <form role="form">
                    <div class="form-group">
                        <label for="inputEmail1">
                            Enter No of Course Key</label>
                        <input type="text" value="" id='totalkeys' placeholder="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1">
                            Price: <span id='keyrate'></span>/Course Key</label>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1">
                            Total Amount: <span id='totalkeyrate'>0</span></label>
                    </div>
                   <div class="form-group">
                        <div style="text-align: right;">
                            <a class="btn btn-info" id="purchaseNow"><i class="fa fa-shopping-cart"></i>
                                Buy Now
                            </a>
                            <a class="btn btn-info" href="#modal_keys_purchaseInvitation" data-toggle="modal" id="keys-purchase"><i class="fa fa-shopping-cart"></i>
                               Get
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>