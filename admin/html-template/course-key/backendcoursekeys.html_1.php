<section class="panel">
    <header class="panel-heading">
        <h4>Courses Key</h4>

        <a id='change_default_rate_admin' class="btn btn-success btn-xs pull-right" data-toggle="modal" href="#modal_change_default_rate">
            <i class="fa fa-plus-circle"></i>Default Rate</a> 
    </header>
    <!--    <div class="row panel-body">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-6">
                        <select id="courseSelect" class="form-control input-sm m-bot15">
                            <option value="0">Select For Filter</option>
                            <option value="1">Edited Rate</option>
                            <option value="2">Default Rate</option>
                            <option value="3">Institute</option>
                            <option value="1">Professor</option>
                        </select>
                    </div>
    
                </div>
            </div>
    
            <div style="text-align: right" class="col-lg-6">
                <label>
                    <input type="text" placeholder="Search" class="form-control pull-right" aria-controls="example"></label>
            </div>
        </div> 
    -->

    <table  class="display table table-bordered table-striped" id="all-courses">
        <thead>
            <tr>
                <th> Sr. No</th>
                <th>ID</th>
                <th>Institute Name</th>
                <th>Currency </th>
                <th>Role </th>
                <th>Total key  </th>
                <th>Remaining  </th>
                <th>Rate </th>
                <th>Rate INR </th>
                <th>Action </th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</section>
