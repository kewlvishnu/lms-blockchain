<section class="panel">
    <header class="panel-heading">
        Deleted Chapters
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <table id="all-chapters"  class="display table table-bordered table-striped" >
                <thead>
                    <tr>
                        <th>Id</th>
                         <th>Chapter</th>
                          <th>Subject & ID</th>
                        <th>Courses Name & ID</th>
                        <th>Institute Name & ID </th>
                         <th>Email </th>
                         <th>Contact No. </th>
                        <th>Action</th> 
                    </tr>
                </thead>
                <tbody>  </tbody>
            </table>
        </div>
    </div>
</section>