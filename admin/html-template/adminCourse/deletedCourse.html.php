<section class="panel">
    <header class="panel-heading">
        Deleted Courses
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <table id="all-courses"  class="display table table-bordered table-striped" >
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Courses Name</th>
                        <th>Institute id </th>
                        <th>Name</th>
                        <th>Email </th>
                        <th>Contact No. </th>
                        <th>Action</th> 
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</section>