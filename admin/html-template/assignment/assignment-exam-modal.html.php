<div class="modal fade" id="view_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        �</button>
                    <h4 class="modal-title">Course ABCD
                    </h4>
                </div>
                <div class="modal-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Subject Name
                                </th>
                                <th>Licensing/Ownership
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>A
                                </td>
                                <td>1 year license ---- 6 months remaining
                                </td>
                                <td>
                                    <button type="button" class="btn btn-primary">
                                        Renew button</button>
                                </td>
                            </tr>
                            <tr>
                                <td>B
                                </td>
                                <td>1 year license ---- 6 months remaining
                                </td>
                                <td>
                                    <button type="button" class="btn btn-primary">
                                        Renew button</button>
                                </td>
                            </tr>
                            <tr>
                                <td>C
                                </td>
                                <td>Selective Ip Transfer
                                </td>
                                <td>-
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p>
                        <strong>Teach Students </strong>- minimum 6 months , As subjects expire they will
                        be unavailable to students.<br>
                        <strong>License to other prof/inst/publishers </strong>- Yes
                   
                    </p>
                    <p>
                        <strong>Types of licensing allowed</strong>
                        <br>
                        Licensing is not allowed because 1 year licensed subject present in the course disqualifies
                        the course for selling to publishers/institutes and professors.
                   
                    </p>
                    <p>
                        ( to see video or read document on understanding licenses, click here )
                   
                    </p>
                </div>
            </div>
        </div>
    </div>