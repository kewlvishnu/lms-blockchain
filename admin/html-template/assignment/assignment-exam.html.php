<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="dashboard.php"><i class="fa fa-home"></i>Dashboard</a></li>
            <li><a href="assignment-exam.php">Add New Assignment/Exam</a></li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<section class="panel">
    <header class="panel-heading">
        <h4>Existing Exam/Assignment</h4>
        <div class="pull-right custom-alignment">
            <a href="#">
                <button type="button" class="btn btn-info btn-xs" id="addExam" disabled=true><i class="fa fa-plus"></i> Add New Exam/Assignment </button>
            </a>
			<a href="template.php">
                <button type="button" class="btn btn-info btn-xs"> Templates </button>
            </a>
        </div>
    </header>
    <div class="row panel-body">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <select class="form-control input-sm m-bot15" id="courseSelect" disabled="true">
                        <option value="0">Select Course</option>
                    </select>
                </div>
                <div class="col-md-6 col-sm-6">
                    <select class="form-control input-sm m-bot15" id="subjectSelect" disabled="true">
                        <option value="0">Select Subject</option>
                    </select>
                </div>
            </div>
        </div>
		<div class="col-lg-3 col-md-3"> 
			<a href="add-subject.php" id="addSubject" style="display: none;">+ New Subject</a>&emsp;
		</div>
        <div class="col-lg-3 col-md-3" style="text-align: right">
            <!--<label>
                <input type="text" aria-controls="example" class="form-control pull-right" placeholder="Search"></label>-->
        </div>
		<!--<a href="" id="seeResults" style="display: none;margin-right: 8px;" class="btn btn-info btn-xs pull-right" DISABLED>See Results</a>-->
    </div>
	<div id="tableContainer" style="display: none;">
		<table class="table table-hover" id="currentExams">
			<thead>
				<tr>
					<th>Title</th>
					<th>Type</th>
					<th>Section</th>
					<th>Status</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Attempts</th>
					<th>Options</th>
				</tr>
			</thead>
		</table>
	</div>
	<hr>
	<div id="warning" style="padding: 5px;">
		No Course found in your profile. Please <a href="add-course.php">Add Course</a> to continue with the Assignment Creation.
	</div>
</section>