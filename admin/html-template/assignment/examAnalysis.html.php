<!--<section class="panel">
	<header class="panel-heading">
		View Results
	</header>
	<div class="panel-body">-->
		
		<div class="row">
			<div class="col-lg-12 ">
				<section class="panel">
					<header class="panel-heading">
						List Of Exam Questions 
					</header>
					<div class="panel-body">
						<table class="table" id="questionList" style="display: none;">
							<thead>
								<tr>
									<th>Question No. </th>
									<th>Question </th>
									<th>Appeared in exam attempts</th>
									<th>Answered incorrectly</th>
									<th>Answered correctly</th>
									<th>Not attempted</th>
									<th>Average Time(secs) </th>
									<th>Topper time(secs)</th>
									<th>Fastest time(secs)</th>
									<th>Needs Review</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</section>
				<section class="panel">
					<header class="panel-heading">
						<h4>TimeLine Graph</h4>
					</header>
					<div class="panel-body">
						<div class="row">
						  	<div class="panel-body" id="graph12"></div>
							<div class="text-center" ><h3>Comparision graph w.r.t to timeline</h3></div>
						</div>
					</div>
				</section>
				<section class="panel">
					<header class="panel-heading">
						Percentage of all students
					</header>
					<div class="panel-body">
						<div id="hero-bar3" class="graph" ></div>
					</div>
				</section>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<section class="panel">
					<header class="panel-heading">
						Score
					</header>
					<div class="panel-body">
						<div id="hero-bar1" class="graph"></div>
					</div>
				</section>
			</div>
			<div class="col-lg-6">
				<section class="panel">
					<header class="panel-heading">
						Percentage
					</header>
					<div class="panel-body">
						<div id="hero-bar2" class="graph"></div>
					</div>
				</section>
			</div>
		</div>
		
	<!--</div>
</section>-->