<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="view_details" class="modal fade" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">
				&times;</button>
				<h4 class="modal-title">Category</h4>
			</div>
			<div class="modal-body">
				<span class="switch-label">Student will get random Question?</span>
				<div class="switch-wrapper">
					<input type="checkbox" id="randomQuestions" CHECKED>
				</div>
				<table class="table table-hover table-striped">
					<thead>
						<tr>
							<th colspan=3>Question Type</th>
							<th>Correct Answer</th>
							<th>Wrong Answer</th>
							<th>No of Question</th>
							<th>Total</th>

						</tr>
					</thead>
					<tbody>
						<tr data-id="0">
							<td colspan=3>
								<select class="form-control input-sm questionTypeSelect">
									<option value="-1" selected>Select Category</option>
									<option value="0">Multiple Choice Question</option>
									<option value="1">True or False</option>
									<option value="2">Fill in the blanks</option>
									<option value="3">Match the following</option>
									<option value="4">Comprehensive Type Question</option>
									<option value="5">Dependent Type Question</option>
									<option value="6">Multiple Answer Question</option>
									<option value="7">Audio question</option>
								</select>
							</td>
							<td>
								<input type="text" class="form-control input-sm plus" placeholder="+5">
							</td>
							<td>
								<input type="text" class="form-control input-sm minus" placeholder="-3">
							</td>
							<td>
								<input type="text" class="form-control input-sm number" placeholder="5">
							</td>
							<td>
								<strong><span class="total">0</span></strong>
							</td>
						</tr>
					</tbody>
				</table>
				<span style="color: red;" id="catError"></span><br>
				<button type="button" class="btn btn-primary" id="modalAddCategory"><i class="fa fa-plus-circle"></i>&nbsp;Category</button>
				<span style="float: right;margin-right: 35px;"><strong>Total</strong>&emsp;<span id="grandTotal">0</span></span>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="modalSaveCategory">Save</button>
				<button aria-hidden="true" data-dismiss="modal" class="btn btn-danger" type="button">Cancel</button>
			</div>
		</div>
	</div>
</div>