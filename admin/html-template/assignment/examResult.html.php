<!--<section class="panel">
	<header class="panel-heading">
		View Results
	</header>
	<div class="panel-body">-->
		<div class="row">
			<div class="col-lg-4">
				<section class="panel">
					<div class="revenue-head">
						<h3>Students vs Attempts</h3>
						<span class="rev-combo" id="expand" data-hidden="0">
							>>
						</span>
						<span class="rev-combo pull-right">
							<select class="rev-combo form-control" style="height: 18px;padding: 0px;" id="attSelector">
								<option value="0">Attempts</option>
							</select>
						</span>
					</div>
					<div class="panel-body">
						<div id="studentGraph" style="height: 280px;width: 100%;"></div>
					</div>
				</section>
			</div>
			<div class="col-lg-4">
				<section class="panel">
					<div class="revenue-head">
						<h3>Scores</h3>
					</div>
					<div class="panel-body">
						<div id="hero-bar" style="height: 280px;width: 100%;"></div>
					</div>
				</section>
			</div>
			<div class="col-lg-4">
				<section class="panel">
					<div class="revenue-head">
						<h3>Students Attempted vs Unattempted</h3>
					</div>
					<div class="panel-body">
						<div id="hero-donut" style="height: 280px;width: 100%;"></div>
					</div>
				</section>
			</div>
		</div>
		<div class="clearfix">
			<a class="btn btn-info btn-md" href="#" id="examAnalysis">Exam Analysis</a>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading pad60">
						List Of Students
						 <div class="pull-right custom-alignment">
					       <button type="button" class="btn btn-success btn-md" id="download-list"><i class="fa fa-plus"></i> Download List </button>
						        </div>
					</header>
					<div class="panel-body">
						<table class="table" id="studentList" style="display: none;">
							<thead>
								<tr>
									<th>Student ID</th>
									<th>Student Name</th>
									<th>Student Email</th>
									<th>Student Attempts</th>
									<th>Last Attempt Score</th>
									<th>Highest Score</th>
									<th>Average Score</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</section>
			</div>
		</div>
	<!--</div>
</section>-->