<aside class="profile-nav col-lg-3 col-md-3 ">
	<div class="" style="padding: 10px;">
		<section class="panel">
			<ul class="nav nav-pills nav-stacked custom-add-sidebar min" id="sections">
			</ul>
			<div class="panel-body">
				<button class="btn btn-primary btn-sm tooltips" type="button" disabled="true" id="examStatus" data-original-title="For making a Test Live, none of the Sections should  be  in DRAFT state.</br>DRAFT button changes to GO LIVE when all Sections have reached NOT LIVE State.</br>Click on GO LIVE Button to make the test Live" data-placement="right">Draft</button>
				<span class="pull-right"><h5><span id="examTotalMarks">0</span> Marks</h5>
				</span>
			</div>
		</section>
	</div>
</aside>
<aside class="profile-nav col-lg-9 col-md-9">
	<div class="custom-exam">
		<div id="exam" style="display: none;">
			<h3>Step 2 : Adding Questions to the <span id="examType" style="float: none;"></span> : <strong><span id="examName" style="float: none;"></span></strong>
			<span><button class="btn btn-primary btn-sm" type="button" id="setting"><i class="fa fa-cog"></i> Setting</button>
			<button class="btn btn-danger btn-sm " type="button" id="examDelete"><i class="fa fa-trash-o "></i></button></span></h3>
		</div>
		<h4 style="margin-top: 2%;"><span id="sectionName" style="float: none;"></span></h4>
		<span id="extraSections" style="float: none;display: none;">
			<b class="badge bg-warning">Draft</b>
			<button type="button" class="btn btn-danger btn-xs delete-section"><i class="fa fa-trash-o "></i></button>
			<i class="fa fa-info-circle tooltips" data-placement="right" data-original-title="DRAFT State : Questions entered in all categories  <  Questions required in all the categories.<br>NOT LIVE State :  Questions entered in all categories >= Questions required in all the categories<br>The state changes from DRAFT to NOT LIVE automatically"></i>
		</span>
	</div>
	<section class="panel section-table" id="sectionTable" style="display: none;">
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>Category <button data-toggle="modal" href="#view_details" type="button" class="btn btn-info btn-xs" style="border: none;"><i class="fa fa-pencil"></i> Edit</button></th>
					<th>Required</th>
					<th>Entered</th>
					<th>Total</th>
					<th>Remaining</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</section>
	<section class="panel" id="sectionAdd" style="display: none;">
		<div class="custom-exam panel-body">
			<button class="btn btn-info" data-toggle="modal" href="#view_details" type="button"><i class="fa fa-plus-circle"></i>&nbsp;Category</button>&nbsp;&nbsp;
			<button class="btn btn-info" type="button" href="#" id="newQuestion" DISABLED><i class="fa fa-plus-circle"></i>&nbsp;Question</button>
			<button class="btn btn-info" id="importQuestions" DISABLED href="#import_questions" data-toggle="modal">Import Questions</button>
		</div>
	</section>
</aside>