<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="import_questions" class="modal fade" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">
				&times;</button>
				<h4 class="modal-title">Import from Question Bank</h4>
			</div>
			<div class="modal-body">
				<div class="row panel">
					<div class="col-lg-4 col-md-4">
						<select class="form-control input-sm courseSelect" DISABLED>
							<option value=0 SELECTED>Select Course</option>
						</select>
					</div>
					<div class="col-lg-4 col-md-4">
						<select class="form-control input-sm subjectSelect" DISABLED>
							<option value=0 SELECTED>Select Subject</option>
						</select>
					</div>
					<div class="col-lg-4 col-md-4">
						<select class="form-control input-sm chapterSelect" DISABLED>
							<option value=-1 SELECTED>Select Chapter</option>
						</select>
					</div>
				</div>
				<br>
				<div class="row panel">
					<div class="col-lg-4 col-md-4">
						<select class="form-control input-sm assignmentSelect" DISABLED>
							<option value=0 SELECTED>Select Assignment</option>
						</select>
					</div>
					<div class="col-lg-8 col-md-8">
						<select class="form-control input-sm importCategorySelect" DISABLED>
							<option value=0 SELECTED>Select Import Category</option>
						</select>
					</div>
				</div>
				<br>
				<span id="initialView"><hr>Please select above options to view the list of questions.</span>
				<div class="import-question">
					<table class="table table-hover table-striped" style="display: none;">
						<thead>
							<tr>
								<th><input type="checkbox" class="checkbox-inline" id="selectAll"></th>
								<th></th>
								<th colspan="3">Question</th>
								<th>Type</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<div>
					<span style="color: red;" id="questionError"></span>
				</div><br>
				<button type="button" class="btn btn-primary" id="modalImportQuestion">Import</button>
				<button aria-hidden="true" data-dismiss="modal" class="btn btn-danger cancel-button" type="button">Cancel</button>
			</div>
		</div>
	</div>
</div>