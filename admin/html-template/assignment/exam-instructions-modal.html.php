<div class="modal fade" id="instructions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                    <h4 class="modal-title">Instructions
                    </h4>
                </div>
                <div class="modal-body">
					<div class="ins-container">
					</div>
					<br/><br/>
					<div class="edit-ins-container row">
						<div class="col-lg-12">
							<label for="instructions">Your additional Instructions</label>
							<textarea rows="8" class="form-control"></textarea>
						</div>
					</div>
                </div>
				<div class="modal-footer">
					<!--<button type="button" class="btn btn-primary" id="editInstruction">Edit</button>-->
					<button type="button" class="btn btn-primary" id="makeExamLive">Make Live</button>
					<button aria-hidden="true" data-dismiss="modal" class="btn btn-danger" type="button">Cancel</button>
				</div>
            </div>
        </div>
    </div>