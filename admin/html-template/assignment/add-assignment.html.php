<section class="panel">
	<header class="panel-heading">
		Step 1 : Name of Test & Settings
	</header>
	<div class="panel-body">
		<form role="form">
			<input type="hidden" id="courseStartDate">
			<input type="hidden" id="courseEndDate">
			<div class="form-group">
				<div class="row">
					<div class="col-md-6 col-sm-6">
						<label for="coursename">Title of the test</label>
						<input type="text" placeholder="Title" id="titleName" class="form-control">
					</div>
					<div class="col-md-6 col-sm-6">
						<label for="courseid">Section <i class="tooltips fa fa-info-circle" data-original-title="Please select the section name under which the Assignment/ Exam would be listed. You can make the Assignment/ Exam as Independent, if you do not want it to be part of any section" data-placement="right"></i></label>
						<select class="form-control" id="chapterSelect">
							<option value="0" SELECTED>Select Section</option>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-6 col-sm-6">
						<label for="coursename">Select test type <i class="tooltips fa fa-info-circle" data-original-title="Assignment does not have any time restriction. Students can give an assignment in unlimited time but Exam is time bound. You will decide the time for the exam" data-placement="right"></i></label><br>
						<label>
							<input type="radio" name="optionsRadios1" id="assignmentCat" value="option1">Assignment
						</label>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<label>
							<input type="radio" name="optionsRadios1" id="examCat" value="option1">Exam
						</label>
					</div>
					<div class="col-md-6 col-sm-6">
						<label for="coursename">Settings</label>
						<a class="pull-right" href="add-template.php"> Add Template</a><br>
						<label>
							<input type="radio" name="optionsRadios" id="fromTemplate" value="option1">
								Choose settings from template
						</label>&nbsp;&nbsp;&nbsp;&nbsp;
						<label>
							<input type="radio" name="optionsRadios" id="ownSettings" value="option1">
							Choose your Own settings
						</label>
					</div>
					<div class="col-md-6 col-sm-6" style="display: none;">
						<label for="courseid">Select Template</label>
						<select class="form-control" id="templateSelect">
							<option value=0>Select Template</option>
							<option value=1>Assignment_Template</option>
							<option value=2>Exam_Template</option>
						</select>
					</div>
					<div class="col-md-6 col-sm-6">
						<label for="courseid">No of attempts a student is allowed</label>
						<input type="text" placeholder="Total attempts allowed" id="noOfAttempts" class="form-control">
					</div>
				</div>
			</div>
			<div id="extraFields" style="display: none;">
				<div id="sections">
					<label><strong>Sections:</strong></label>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-lg-9 col-md-9">
							<input type="text" placeholder="Enter new section name" id="tempSection" class="form-control">
						</div>
						<div class="col-lg-3 col-md-3">
							<a id="addSection"> Add Section </a>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<label for="coursename">Start date of test</label>
							<input type="text" class="form-control" id="startDate" placeholder="10 Jan 2015 14:30">
						</div>
						<div class="col-lg-6 col-md-6">
							<label for="courseid">End date of test</label>
							<input type="text" class="form-control" id="endDate" placeholder="10 Jan 2015 14:30">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row" id="result-show">
						<div class="col-lg-6 col-md-6">
							<label for="Rsult-show">Students can see result after</label><br>
							<label class="pad-right10">
								<input type="radio" name="optionsshowResult" id="immediately">Immediately after finishing the exam 
							</label>
							<label>
								<input type="radio" name="optionsshowResult" id="after-examend">After the exam end date
							</label>
						</div>
						<div class="col-lg-6 col-md-6">
							<label for="Rsult-time">Time by which student can see result</label><br>
							<label class="pad-right10">
								<input type="radio" name="optionstimeResult" id="course-end" checked="checked">Till course end date 
							</label>
							<label>
								<input type="radio" name="optionstimeResult" id="custom-time">Set time
							</label>
						</div>						
					</div>
				</div>
				<div class="form-group dispNone" id="set-exam-date">
						<div class="row">
							<div class="col-lg-6 col-md-6 pull-right">
							<div class="col-lg-6 col-md-6">
							<label for="coursename">Date till result is shown</label>
							<input type="text" class="form-control" id="settime" placeholder="10 Jan 2015 14:30">
							</div>
							</div>
						</div>
				</div>
				<!-- div for timing section to be shown only in exam -->
				<div id="assExm" style="display: none;">
					<div class="form-group">
						<div class="row">
							<div class="col-lg-6 col-md-6" style="display: none;">
								<label for="coursename"> Can Students Switch  Between Sections?</label><br>
								<label>
									<input type="radio" value="option1" id="switchYes" name="optionsRadios2" CHECKED>Yes
								</label>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<label>
									<input type="radio" value="option1" id="switchNo" name="optionsRadios2">No
								</label>
							</div>
							<div class="col-lg-6 col-md-6" id="totalTime">
								<label for="coursename">Total time of test</label><br>
								<div class="col-md-4">
									<div class="input-group input-small">
										<input type="text" maxlength="3" class="spinner-input form-control" value="0h" disabled=true id="totalTimeHour">
										<div class="spinner-buttons input-group-btn btn-group-vertical">
											<button type="button" class="btn spinner-up btn-xs btn-default">
												<i class="fa fa-angle-up"></i>
											</button>
											<button type="button" class="btn spinner-down btn-xs btn-default">
												<i class="fa fa-angle-down"></i>
											</button>
										</div>
									</div>
								</div> &nbsp;&nbsp;&nbsp;&nbsp;
								<div class="col-md-4">
									<div class="input-group input-small">
										<input type="text" class="spinner-input form-control" maxlength="3" value="0m" disabled=true id="totalTimeMinute">
										<div class="spinner-buttons input-group-btn btn-group-vertical">
											<button class="btn spinner-up btn-xs btn-default" type="button">
												<i class="fa fa-angle-up"></i>
											</button>
											<button class="btn spinner-down btn-xs btn-default" type="button">
												<i class="fa fa-angle-down"></i>
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group" id="sectionTimes" style="display: none;">
						<div class="row">
						</div>
						<div class="row">
							<div class="col-lg-6">
								<label for="coursename">Gap between sections</label><br>
								<div class="col-md-4 pad-right10">
									<div class="input-group input-small">
										<input type="text" class="spinner-input form-control" maxlength="3" value="0m" disabled=true id="gapTime">
										<div class="spinner-buttons input-group-btn btn-group-vertical">
											<button class="btn spinner-up btn-xs btn-default" type="button">
												<i class="fa fa-angle-up"></i>
											</button>
											<button class="btn spinner-down btn-xs btn-default" type="button">
												<i class="fa fa-angle-down"></i>
											</button>
										</div>
									</div>
								</div> 
							</div>
						</div>
					</div>
					<div class="form-group">
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<label for="coursename">Can students end test before time?</label><br>
							<label class="pad-right10">
								<input type="radio" name="optionsRadios3" id="endYes">Yes
							</label> 
							<label>
								<input type="radio" name="optionsRadios3" id="endNo">No
							</label>
						</div>
						<div class="col-lg-6 col-md-6">
							<label for="coursename">If power goes off/on browser closes ( answers are not lost )</label><br>
							<label  class="pad-right10">
								<input type="radio" name="optionsRadios4" id="timeLost">Time Continues
							</label>
							<label>
								<input type="radio" name="optionsRadios4" id="noTimeLost">Timer Stops
							</label>
						</div>
						<div class="col-lg-12" style="display: none;">
							<label for="coursename">Section ordering</label><br>
							<label class="pad-right10">
								<input type="radio" name="optionsRadios5" id="orderStart" value="option1">
								Fixed
							</label> 
							<label>
								<input type="radio" name="optionsRadios5" id="orderRandom" value="option1" CHECKED>
								Random
							</label>
							<div class="row">
								<div class="col-lg-12">
									<ul id="sortOrder" style="display: none;">
									</ul>
								</div>	
							</div>
						</div>
					</div>
				</div>
					
			</div>
			</div>
			<div class="row panel-body" style="text-align: right;">
				<div class="col-lg-12">
					<button class="btn btn-primary save-button" type="button">Save</button>
					<button aria-hidden="true" data-dismiss="modal" type="button" class="btn btn-info save-button proceed">Save &amp; Proceed</button>
					<button onclick="history.back();" class="btn btn-danger">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</section>