﻿<div class="row">
<!-- <div class="col-md-8 col-sm-8">
	<section class="panel">
		<header class="panel-heading">
			Subjective Exams 
		</header>
			<div class="panel-body min-500  ">
			<div class="fillformRight">Fill the exam settings form  <i class="fa fa-arrow-right" id="arrow"></i>
			</div>
		</div>
	</section>
</div> -->
<div class="col-md-12">
	<section class="panel">
		<header class="panel-heading">
		Subjective Exam Settings
		</header>
		<div class="panel-body" id="sidepannelBody">
			<div class="form-group">
				<div class="row padTop10 padBot10">
					<div class="col-md-4 col-sm-4">
						<label  for="courseid">Test Title</label>
					</div>
					<div class="col-md-8 col-sm-8">
						<input  type="text" placeholder="Title of the test" id="titleName" class="form-control">
					</div>
				</div>
				<div class="row padTop10 padrgtlft30">
					<div class="form-group">
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<label for="coursename">Start date</label>
								<input type="text" class="form-control" id="startDate" placeholder="10 Jan 2015 14:30">
							</div>
							<div class="col-lg-6 col-md-6">
								<label for="courseid">End date (optional)</label>
								<input type="text" class="form-control" id="endDate" placeholder="10 Jan 2015 14:30">
							</div>
						</div>
					</div>
				</div>
				<div class="row padTop10 padrgtlft30">
					 <div class="form-group">
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<label for="coursename">Duration Of Test(hrs)</label>
								<input type="text" class="form-control" id="hrsTest" placeholder="Duration in hrs">
							</div>
							<div class="col-lg-6 col-md-6">
								<label for="courseid">Duration Of Test(min)</label>
								<input type="text" class="form-control" id="mnsTest" placeholder="Duration in mins">
							</div>
						</div>
					</div>
				</div>
				<div class="row padTop10 padBot10 ">
					<div class="col-md-4 col-sm-4">
						<label for="courseid">Section <i class="tooltips fa fa-info-circle" data-original-title="Please select the section name under which the  Exam would be listed. You can make the  Exam as Independent, if you do not want it to be part of any section" data-placement="right"></i></label>
					</div>
					<div class="col-md-8 col-sm-8">
						<select class="form-control" id="chapterSelect">
							<option value="0" SELECTED>Select Section</option>
						</select>
					</div>
				</div>
				<div class="row padTop10 padBot10 ">
					<div class="col-md-4 col-sm-4">
						<label  for="courseid">Total Attempts</label>
					</div>
					<div class="col-md-8 col-sm-8">
						<input  type="text" placeholder="no. of Attempts " id="noOfAttempts" class="form-control">
					</div>
				</div>
				<div class="row padTop10 ">
					<div class="col-md-4 col-sm-4">
						<label  for="courseid">Shuffle Questions</label>
					</div>
					<div class="col-md-8 col-sm-8 errormsg">
						<label>
							<input type="radio" value="option1" id="switchyes" name="optionsRadios2" CHECKED>Yes  
						</label>
						<label clas="padrgtlft10">
							<input type="radio" value="option1" id="switchno" name="optionsRadios2">No
						</label>
					</div>
				</div>
				<div class="row padTop10 padBot20 ">
					<div class="col-md-3 col-sm-3">
						<label  for="courseid">Submission</label>
					</div>
					<div class="col-md-9 col-sm-9 errormsg1">
						<label>
							<input type="checkbox" value="option1" id="switchtype" name="optionsRadios2" CHECKED>Type in textbox  
						</label>
						<label clas="padrgtlft10">
							<input type="checkbox" value="option1" id="switchdoc" name="optionsRadios2">Can upload File
						</label>
					</div>			
				</div>
			</div>
		</div>
		<div class="row padTop80 padBot10 padrgtlft30">
			<div class="col-lg-12 col-sm-12 ">
				<button class="btn btn-primary save-button padrgtlft30" type="button">Save Settings</button>
				<button  class="btn btn-success makelive padrgtlft30" disabled="disabled">Make Exam Live</button>
			</div>
		</div>
	</section>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="questionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add question</h4>
			</div>
			<div class="modal-body ">
				<form method="post">
					<div class="row chooseQuestionType">
						<div class="col-md-6 ">
							<div class="form-group">
								<div class="radio">
									<input type="radio" class="js-question-type" name="questionType" id="optionQuestion" value="question" checked=""> <label for="optionQuestion">Question</label>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<div class="radio">
									<input type="radio" class="js-question-type" name="questionType" id="optionQuestionGroup" value="questionGroup"> <label for="optionQuestionGroup">Question Group</label>
								</div>
							</div>
						</div>
					</div>
					<div class="well js-question-block" id="questionBlock">
						<div class="help-block text-center"></div>
						<div class="form-group">
							<label for="">Enter Question</label>
							<textarea name="" id="inputQuestion" cols="30" rows="10" class="form-control question"></textarea>
						</div>
						<div class="form-group">
							<label for="">Enter Answer</label>
							<textarea name="" id="inputAnswer" cols="30" rows="10" class="form-control"></textarea>
						</div>
						<div class="form-group marks">
							<label for="">Enter Maximaum Marks</label>
							<input type="number" name=""  id="inputMarks" cols="30" rows="1" class="form-control" />
						</div>
					</div>
					<div class="well js-question-block hide" id="questionGroupBlock">
						<div class="help-block text-center"></div>
						<div class="form-group">
							<label for="">Questions to show on each page  </label>
							<input type="radio" name="questionsOnpage" id="allquestion" checked>In one page 
							<input type="radio" name="questionsOnpage" id="onequestion">One question per page
						</div>
						<div class="form-group desc dispNone">
							<label for="">Enter Description about Question Group</label>
							<textarea name="" id="inputQuestionDescription" cols="30" rows="5" class="form-control"></textarea>
						</div>
						<div class="form-group">
							<label for="">Number of Questions to show</label>
							<input type="number" name="" id="inputNoOfQuestions" class="form-control" />
						</div>
						<div class="form-group">
							<label for="">Marks of each subquestions</label>
							<input type="number" name="" id="subquestionsMrks" class="form-control" />
						</div>
						
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" data-question="" data-parent="" id="btnAddQuestion">Save changes</button>
			</div>
		</div>
	</div>
</div>