<div class="col-lg-5">
	<section class="panel custom-scroll" id="sectionQuestions" style="display: none;">
	</section>
</div>
<div class="col-lg-7">
	<section class="panel" id="sectionNewQuestion" style="display: none;">
		<form>
			<input type="hidden" id="questionId" value="0">
			<div class="row panel-body">
			<input type="hidden" id="hiddenQuestionType" value="0">
				<div class="col-lg-8 col-md-8">
					<select class="form-control input-sm" id="categorySelect">
					</select>
				</div>
				<div class="col-lg-4 col-md-4">
					<i class="fa fa-info-circle tooltips" data-placement="right" data-original-title="" id="questionInfo"></i>
				</div>
			</div>
			<div class="row panel-body">
				<div class="col-lg-12 form-group">
					<label for="question" id="qq">Question</label>
					<textarea placeholder="Please enter the question text here" rows="10" style="min-height:500px" class="form-control t-text-area" id="question"></textarea>
				</div>
			</div>
			<div class="row panel-body" id="options">
				<div class="col-lg-12"><strong>Only one answer option is correct</strong></div>
				<div class="col-lg-5 col-md-5">
					<div class="row">
						<div class="col-xs-10">
							<span class="radio-inline" style="padding-top: 0px;">
								<input type="radio" class="top-magin" name="options">
								<input type="text" placeholder="Answer Option" class="form-control option" id="option0">
							</span>
						</div>
						<div class="col-xs-2">
						</div>
					</div>
				</div>
				<div class="col-lg-5 col-md-5">
					<div class="row">
						<div class="col-xs-10 col-md-10">
							<span class="radio-inline" style="padding-top: 0px;">
								<input type="radio" class="top-magin" name="options">
								<input type="text" placeholder="Answer Option" class="form-control option" id="option1">
							</span>
						</div>
						<div class="col-xs-2 col-md-2">
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 add-div">
					<button type="button" class="btn btn-primary pull-right add-option"><i class="fa fa-plus-circle"></i></button>
				</div>
			</div>
			<div class="row panel-body" id="ftb">
				<div class="col-lg-5 col-md-5">
					<input type="text" placeholder="Please write the answer" class="form-control ftb" id="ftbOption0">
				</div>
			</div>
			<div class="row panel-body" id="trueFalse">
				<div class="col-lg-5 col-md-5">
					<label class="checkbox-inline" style="padding-top: 0px;">
						<input type="radio" class="top-magin" name="trueFalse" id="true"> True
					</label>
				</div>
				<div class="col-lg-5 col-md-5">
					<label class="checkbox-inline">
						<input type="radio" class="top-magin" name="trueFalse" id="false"> False
					</label>
				</div>
			</div>
			<div class="row panel-body" id="match">
				<div class="col-lg-12"><strong>Student will see column B in jumbled order</strong></div>
				
				<div class="col-lg-10 col-md-10">
					<div class="row">
						<div class="col-lg-5 col-md-5" style="text-align: center;">
							<strong>A</strong>
						</div>
						<div class="col-lg-5 col-md-5" style="text-align: center;">
							<strong>B</strong>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-2"></div>
				<div class="col-lg-10 col-md-10 custom-match">
					<div class="match row">
						<div class="col-lg-5 col-md-5">
							<input type="text" class="form-control a" placeholder="Enter option A here" id="mtfOptionA0">
						</div>
						<div class="col-lg-5 col-md-5">
							<input type="text" class="form-control b" placeholder="Correct answer for column A" id="mtfOptionB0">
						</div>
					</div>
					<div class="row match">
						<div class="col-lg-5 col-md-5">
							<input type="text" class="form-control a" placeholder="Enter option A here" id="mtfOptionA1">
						</div>
						<div class="col-lg-5 col-md-5">
							<input type="text" class="form-control b" placeholder="Correct answer for column A" id="mtfOptionB1">
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-2">
					<button type="button" class="btn btn-primary pull-right add-option"><i class="fa fa-plus-circle"></i></button>
				</div>
			</div>
			<div class="row panel-body" id="optionsMul" style="display: none;">
				<div class="col-lg-12"><strong>More than one answer option can be correct</strong></div>
				<div class="col-lg-5 col-md-5">
					<div class="row">
						<div class="col-xs-10 col-md-10">
							<span class="checkbox-inline" style="padding-top: 0px;">
								<input type="checkbox" class="top-magin">
								<input type="text" placeholder="Answer Option" class="form-control optionMul" id="optionMul0">
							</span>
						</div>
						<div class="col-xs-2 col-md-2">
						</div>
					</div>
				</div>
				<div class="col-lg-5 col-md-5">
					<div class="row">
						<div class="col-xs-10 col-md-10">
							<span class="checkbox-inline" style="padding-top: 0px;">
								<input type="checkbox" class="top-magin">
								<input type="text" placeholder="Answer Option" class="form-control optionMul" id="optionMul1">
							</span>
						</div>
						<div class="col-xs-2 col-md-2">
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 add-div">
					<button type="button" class="btn btn-primary pull-right add-option"><i class="fa fa-plus-circle"></i></button>
				</div>
			</div>
			<div class="row panel-body" id="audio">
				<div class="col-lg-12 form-group">
					<button type="button" class="btn btn-warning btn-audio-upload">Upload Audio Clip (MP3)</button>
				</div>
				<div class="col-lg-12 form-group">
					<div class="js-file-path"></div>
				</div>
				<div class="col-lg-12"><strong>Only one answer option is correct</strong></div>
				<div class="col-lg-5 col-md-5">
					<div class="row">
						<div class="col-xs-10">
							<span class="radio-inline" style="padding-top: 0px;">
								<input type="radio" class="top-magin" name="options">
								<input type="text" placeholder="Answer Option" class="form-control option" id="option0">
							</span>
						</div>
						<div class="col-xs-2">
						</div>
					</div>
				</div>
				<div class="col-lg-5 col-md-5">
					<div class="row">
						<div class="col-xs-10 col-md-10">
							<span class="radio-inline" style="padding-top: 0px;">
								<input type="radio" class="top-magin" name="options">
								<input type="text" placeholder="Answer Option" class="form-control option" id="option1">
							</span>
						</div>
						<div class="col-xs-2 col-md-2">
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 add-div">
					<button type="button" class="btn btn-primary pull-right add-option"><i class="fa fa-plus-circle"></i></button>
				</div>
			</div>
			<div class="row panel-body">
				<div class="col-lg-12 col-md-12 form-group">
					<label for="Explanation" id="eq">Explanation</label>
					<textarea class="form-control  t-text-area" rows="2" placeholder="Please enter the explanation for the question" id="desc"></textarea>
				</div>
			</div>
			<div id="compro">	
				<div class="panel sectionNewQuestion">
					<form>
						<div class="col-lg-12">Please select the question type</div>
						<input type="hidden" class="questionId" value="0">
						<div class="row panel-body">
							<div class="col-lg-1 col-md-1">
								<span class="subQuestionNumbers"></span>
							</div>
							<div class="col-lg-6 col-md-6">
								<select class="form-control input-sm questionTypeSelect">
									<option value=0>Multiple Choice Question</option>
									<option value=1>True or False</option>
									<option value=2>Fill in the blanks</option>
									<option value=6>Multiple Answer Questions</option>
								</select>
							</div>
							<div class="col-lg-5 col-md-5">
								<span style="color: red;display: none;" class="marks"></span>
							</div>
						</div>
						<div class="row panel-body">
							<div class="col-lg-12 col-md-12">
								<textarea placeholder="Please enter the question text here. The passage is already written above." rows="2" class="form-control  t-text-area question" id="childQuestion0"></textarea>
							</div>
						</div>
						<div class="row panel-body options">
							<div class="col-lg-5 col-md-5">
								<div class="row">
									<div class="col-xs-10 col-md-10">
										<span class="radio-inline" style="padding-top: 0px;">
											<input type="radio" class="top-magin" name="childOptions0">
											<input type="text" placeholder="Option" class="form-control option" id="childOption00">
										</span>
									</div>
									<div class="col-xs-2 col-md-2">
									</div>
								</div>
							</div>
							<div class="col-lg-5 col-md-5">
								<div class="row">
									<div class="col-xs-10 col-md-10">
										<span class="radio-inline" style="padding-top: 0px;">
											<input type="radio" class="top-magin" name="childOptions0">
											<input type="text" placeholder="Option" class="form-control option" id="childOption01">
										</span>
									</div>
									<div class="col-xs-2 col-md-2">
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 add-div">
								<button type="button" class="btn btn-primary pull-right add-option"><i class="fa fa-plus-circle"></i></button>
							</div>
						</div>
						<div class="row panel-body ftbs">
							<div class="col-lg-5 col-md-5">
								<input type="text" placeholder="Answer" class="form-control ftb" id="childftb00">
							</div>
						</div>
						<div class="row panel-body trueFalse">
							<div class="col-lg-5 col-md-5">
								<label class="checkbox-inline" style="padding-top: 0px;">
									<input type="radio" class="top-magin true" name="trueFalse"> True
								</label>
							</div>
							<div class="col-lg-5 col-md-5">
								<label class="checkbox-inline">
									<input type="radio" class="top-magin false" name="trueFalse"> False
								</label>
							</div>
						</div>
						<div class="row panel-body optionsMul" style="display: none;">
							<div class="col-lg-5 col-md-5">
								<div class="row">
									<div class="col-xs-10 col-md-10">
										<span class="checkbox-inline" style="padding-top: 0px;">
											<input type="checkbox" class="top-magin">
											<input type="text" class="form-control optionMul" placeholder="Option" id="childOptionMul00">
										</span>
									</div>
									<div class="col-xs-2 col-md-2">
									</div>
								</div>
							</div>
							<div class="col-lg-5 col-md-5">
								<div class="row">
									<div class="col-xs-10 col-md-10">
										<span class="checkbox-inline" style="padding-top: 0px;">
											<input type="checkbox" class="top-magin">
											<input type="text" class="form-control optionMul" placeholder="Option" id="childOptionMul01">
										</span>
									</div>
									<div class="col-xs-2 col-md-2">
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 add-div">
								<button type="button" class="btn btn-primary pull-right add-option"><i class="fa fa-plus-circle"></i></button>
							</div>
						</div>
						<div class="row panel-body">
							<div class="col-lg-12 col-md-12">
								<textarea class="form-control  t-text-area desc" rows="2" placeholder="Please enter the explanation of the question" id="childDesc0"></textarea>
							</div>
						</div>
					</form>
				</div>
				<div class="lastCol row">
					<div class="col-lg-3 col-md-3">
						<button type="button" class="btn btn-primary pull-right add-question"><i class="fa fa-plus-circle"> Question</i></button>
					</div>
				</div>
			</div>
			<div class="row panel-body" style="text-align: right;">
				<div class="col-lg-12">
					<button class="btn btn-primary save-button" type="button">
						Save</button>
					<button class="btn btn-default cancel-button" type="reset">
						Cancel</button>
				</div>
			</div>
		</form>
		<form id="frmAudioUpload" class="frm-upload" action="../api/files1.php" method="post">
			<input type="file" name="audio-upload" class="js-uploader hide">
		</form>
	</section>
</div>