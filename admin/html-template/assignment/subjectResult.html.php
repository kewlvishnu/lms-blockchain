<section class="panel">
    <div class="row">
		<div class="col-lg-6 col-md-6">
			<section class="panel">
				<header class="panel-heading">
					Number of students appeared
				</header>
				<div class="panel-body" style="overflow: auto;">
					<div id="hero-bar" class="graph"></div>
				</div>
			</section>
		</div>
		<div class="col-lg-6 col-md-6">
			<section class="panel">
				<header class="panel-heading">
					Number of students appeared
				</header>
				<div class="panel-body" style="overflow: auto;">
					<table class="table" style="display: none;" id="attemptTable">
						<thead>
							<tr>
								<th>Name</th>
								<th>Attempted</th>
								<th>Unattempted</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</section>
		</div>
	</div>
</section>