<section class="panel hidden-on-load" id="user-am-experience">
	<div class="bio-graph-heading">
		Arcanemind Experience
	</div>
	<div class="panel">
		<table class="table xp">
			<thead>
				<tr>
					<th style="width:30%">Course</th>
					<th style="width:30%" colspan="2">Subjects
                                       
                                        </th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
            <a class="btn btn-xs btn-danger pull-right" id="expand-course" style="margin-right: 10px;"><i class="fa fa-plus-circle"> </i> View More</a>
            &nbsp;<br></br>
        </div>
</section>