<section class="panel hidden-on-load" id="user-institute-types">
  <div class="bio-graph-heading heading">
   Institute Categories                             
<!--	  <button class="btn btn-primary btn-xs edit-button" style="float:right; border-color:#fff; background-color:#0A3151;"><i class="fa fa-pencil"></i></button>-->
  </div>
  
  <div class="panel-body bio-graph-info view-section">
	
  </div>
  <div class="panel-body bio-graph-info edit-section">
		<div class="form-group">
			<label for="Institute_Name" class="top-heading">Please select the categories in which you would like to Teach</label>
			<div class="form-msg overall"></div>
			<h4>
				<input type="checkbox" id="ins-1" value="1">
				School
			</h4>
		</div>
		<div class="form-group" id="school">
			<div class="row">
				<div class="col-lg-6">
					<label for="Institute_Name">Board</label>
					<select class="form-control m-bot15" id="school-board">
						<option value="0">Please Select Board</option>
						<option value="5">CBSE</option>
						<option value="6">ICSE</option>
						<option value="7">State Board</option>
						<option value="-1">Others</option>
					</select>
					<div class="form-msg board"></div>
				</div>
				<div class="col-lg-6 hidden-on-load" id="school-board-state">
					<label for="Institute_Name">.</label>
					<input type="text" placeholder="State" class="form-control">
					<div class="form-msg board-state"></div>
				</div>
				<div class="col-lg-6 hidden-on-load" id="school-board-other">
					<label for="Institute_Name">.</label>
					<input type="text" placeholder="Others"class="form-control">
					<div class="form-msg board-other"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<label for="Institute_Name">Classes Upto</label>
					<select class="form-control m-bot15" id="school-classes">
						<option value="0">Please Select Classes</option>
						<option value="5">5</option>
						<option value="8">8</option>
						<option value="10">10</option>
						<option value="12">12</option>
					</select>
					<div class="form-msg classes"></div>
				</div>
				<div class="col-lg-6">
				</div>	
			</div>
		</div>
		<h4 class="margin_top_40">
			<input type="checkbox" id="ins-2" value="Exams">
			Coaching</h4>
		<div class="form-msg coaching"></div>
		<div class="form-group" id="coaching">
			<div class="form-group">
				<label class="control-label">
					<input type="checkbox" id="ins-8" value="option1">
					Management Exams
				</label>
				<div class="form-group" id="management">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-10">
							<select multiple="multiple" class="multi-select" id="ins-8-subcat" name="my_multi_select1[]">
								<option value="13">CAT</option>
								<option value="14">MAT</option>
								<option value="15">CMAT</option>
								<option value="16">CET</option>
								<option value="17">GMAT</option>
								<option value="-1">Others</option>
							</select>
						</div>
						<div class="form-msg 8-msg"></div>
					</div>
					<div class="row">
						<div class="col-lg-6 hidden-on-load" id="ins-8-other">
							<label for="Institute_Name">.</label>
							<input type="text" placeholder="Others"class="form-control" />
							<div class="form-msg 8-other"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label">
					<input type="checkbox" id="ins-9" value="option1">
					Engineering Entrance Exams
				</label>
				<div class="form-group" id="engineering">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-10">
							<select multiple="multiple" class="multi-select" id="ins-9-subcat" name="my_multi_select1[]">
								<option value="18">JEE Mains</option>
								<option value="19">JEE Advance</option>
								<option value="21">GATE</option>
								<option value="22">SAT</option>
								<option value="-1">Others</option>
							</select>
						</div>
						<div class="form-msg 9-msg"></div>
					</div>
					<div class="row">
						<div class="col-lg-6 hidden-on-load" id="ins-9-other">
							<label for="Institute_Name">.</label>
							<input type="text" placeholder="Others"class="form-control" />
							<div class="form-msg 9-other"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label">
					<input type="checkbox" id="ins-10" value="option1">
					Medical Entrance Exams
				</label>
				<div class="form-group" id="medical">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-10">
							<select multiple="multiple" class="multi-select" id="ins-10-subcat" name="my_multi_select1[]">
								<option value="23">AIIMS</option>
								<option value="24">PMT</option>
								<option value="25">CET</option>
								<option value="26">PG Medical</option>
								<option value="27">AIPG Dental</option>
								<option value="-1">Others</option>
							</select>
						</div>
						<div class="form-msg 10-msg"></div>
					</div>
					<div class="row">
						<div class="col-lg-6 hidden-on-load" id="ins-10-other">
							<label for="Institute_Name">.</label>
							<input type="text" placeholder="Others"class="form-control" />
							<div class="form-msg 10-other"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label">
					<input type="checkbox" id="ins-11" value="option1">
					School Level 
				</label>
				<div class="form-group" id="schoolLevel">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-10">
							<select multiple="multiple" class="multi-select" id="ins-11-subcat" name="my_multi_select1[]">
								<option value="28">Classes 1-5</option>
								<option value="29">Classes 6-8</option>
								<option value="30">Classes 9-10</option>
								<option value="31">Pre-IIT</option>
								<option value="32">Olympiad</option>
								<option value="33">NTSE</option>
								<option value="34">MTSE</option>
								<option value="-1">Others</option>
							</select>
						</div>
						<div class="form-msg 11-msg"></div>
					</div>
					<div class="row">
						<div class="col-lg-6 hidden-on-load" id="ins-11-other">
							<label for="Institute_Name">.</label>
							<input type="text" placeholder="Others"class="form-control" />
							<div class="form-msg 11-other"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label">
					<input type="checkbox" id="ins-12" value="option1">
					Miscellaneous
				</label>
				<div class="form-group" id="misc">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-10">
							<select multiple="multiple" class="multi-select" id="ins-12-subcat" name="my_multi_select1[]">
								<option value="35">Classes XI-XII</option>
								<option value="36">CA-CS-ICWA</option>
								<option value="37">Bank PO</option>
								<option value="38">Railway Entrance</option>
								<option value="39">SSC</option>
								<option value="40">CSAT</option>
								<option value="41">Accounts</option>
								<option value="42">CLAT</option>
								<option value="43">MPSE</option>
								<option value="44">UPSE</option>
								<option value="45">IAS</option>
								<option value="-1">Others</option>
							</select>
						</div>
						<div class="form-msg 12-msg"></div>
					</div>
					<div class="row">
						<div class="col-lg-6 hidden-on-load" id="ins-12-other">
							<label for="Institute_Name">.</label>
							<input type="text" placeholder="Others"class="form-control" />
							<div class="form-msg 12-other"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<h4>
				<input type="checkbox" id="ins-3" value="3">
				College
			</h4>
			<div class="form-group"  id="college">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-10">
						<select multiple="multiple" class="multi-select" id="ins-3-subcat" name="my_multi_select1[]">
							<option value="46">Medical</option>
							<option value="47">LAW</option>
							<option value="48">Engineering</option>
							<option value="49">Commerce</option>
							<option value="50">Arts</option>
							<option value="51">Management</option>
							<option value="52">Science</option>
							<option value="53">Junior College( XI-XII)</option>
							<option value="-1">Others</option>
						</select>
					</div>
					<div class="form-msg college"></div>
				</div>
				<div class="row">
					<div class="col-lg-6 hidden-on-load" id="ins-3-other">
						<label for="Institute_Name">.</label>
						<input type="text" placeholder="Others" class="form-control" />
						<div class="form-msg college-other"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<h4 class="other-categories">
				<input type="checkbox" id="ins-4" value="4">
				Other Categories
			</h4>
			<div class="form-group" id="other">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-10">
						<select multiple="multiple" class="multi-select" id="ins-4-subcat" name="my_multi_select1[]">
							<option value="54">Sports and Fitness</option>
							<option value="55">Music</option>
							<option value="56">Dance</option>
							<option value="57">Vocational Trainig</option>
							<option value="58">Design and Multimedia</option>
							<option value="59">Animation And Games</option>
							<option value="60">Computer Courses</option>
							<option value="61">Foreign Languages</option>
							<option value="62">Photography</option>
							<option value="63">Entrpreneurship</option>
							<option value="-1">Others</option>
						</select>
					</div>
					<div class="form-msg training"></div>
				</div>
				<div class="row">
					<div class="col-lg-6 hidden-on-load" id="ins-4-other">
						<label for="Institute_Name">.</label>
						<input type="text" placeholder="Others"class="form-control" />
						<div class="form-msg training-other"></div>
					</div>
				</div>
			</div>
		</div>
		<p class="text-right">
			<button class="btn btn-primary btn-xs cancel-button"><i class="fa fa-times"></i> Cancel</button>
		<button class="btn btn-primary btn-xs save-button"><i class="fa fa-save"></i> Save</button>
		</p>
	</div>
</section>
