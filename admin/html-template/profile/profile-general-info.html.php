<!-- Institute -->
<section class="panel hidden-on-load" id="institute-general-information">
  <div class="bio-graph-heading">
	  General Information                                                                                         
	  <button class="btn btn-primary btn-xs edit-button" style="float:right; border-color:#fff; background-color:#F6954B;"><i class="fa fa-pencil"></i></button>
  </div>
  <div class="panel-body bio-graph-info view-section">
	  <div class="row">
		  <div class="col-sm-12">
			  <p><span>Institute Name : </span><span id="institute-name"></span></p>
		  </div>
		  <div class="col-sm-12">
			  <p><span>Owner/Trust Name : </span><span id="owner-name"></span></p>
		  </div>
		  <div class="col-sm-6">
			  <p><span>Contact Person Name : </span><span id="contact-name"></span></p>
		  </div>
		  <div class="col-sm-6">
			  <p><span>Contact No : </span><span id="contact-num"></span> </p>
		  </div>
		  <div class="col-sm-6">
			  <p><span>Email : </span><span id="primary-email"></span></p>
		  </div>
		  <div class="col-sm-6">
			  <p><span>Year Of Inception : </span><span id="founded-in"></span></p>
		  </div>
		  <div class="col-sm-6">
			  <p><span>Country : </span><span id="address-country"></span></p>
		  </div>
		  <div class="col-sm-6">
			  <p><span>No. of Student : </span><span id="institute-size"></span></p>
		  </div>
		  <div class="col-sm-12">
			  <p><span>Address : </span><span id="address-details" ></span></p>
		  </div>
	  </div>
  </div>
  <!-- edit section -->
  <div class="contact-form panel-body edit-section">
	<div role="form">
		<div class="form-group">
			<div class="row">
				<div class="col-lg-12">
                                    <label for="Contact_Name"><i class="text-danger">*</i>Institute Name </label>
					<input type="text" class="form-control txt-captial" id="institute-name-inp" placeholder="Institute Name">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<label for="Contact_Name">Owner/Trust Name</label>
					<input type="text" class="form-control txt-captial" id="owner-first-name-inp" placeholder="First Name" style="margin-top: 5px;">
				</div>
				<div class="col-lg-6">
					<label for="Contact_Name">&nbsp;</label>
					<input type="text" class="form-control txt-captial" id="owner-last-name-inp" placeholder="Last Name" style="margin-top: 5px;">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<label for="Contact_Name">Contact Person Name</label>
					<input type="text" class="form-control txt-captial" id="contact-first-name-inp" placeholder="First Name" style="margin-top: 5px;">
				</div>
				<div class="col-lg-6">
					<label for="Contact_Name">&nbsp;</label>
					<input type="text" class="form-control txt-captial" id="contact-last-name-inp" placeholder="Last Name" style="margin-top: 5px;">
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-lg-6">
				<label for="Address">No. of Student </label>
					<select class="form-control m-bot15" id="institute-size-inp">
						<option value="">Not Specified</option>
						<option value="100-500">100-500</option>
						<option value="500-1000">500-1000</option>
						<option value="1000-2000">1000-2000</option>
					</select>
				</div>
				 <div class="col-lg-6">
				<label for="Address">Year Of Inception</label>
					<select class="form-control m-bot15" id="founded-in-inp">
						<option value="0">Not Specified</option>
					</select>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row contact">
				<div class="col-lg-2">
					<label for="Contact_Number"><i class="text-danger">*</i>Contact No</label>
					<input type="text" class="form-control" id="phnNumPrefix" placeholder="+91">
				</div>
				<div class="col-lg-4">
					<label for="Contact_Number"></label>
					<input type="text" class="form-control" id="phnNum" placeholder="8882715454" style="margin-top: 5px; " maxlength="10" data-text="institute">
				</div>
				<div class="col-lg-2">
					<label for="Contact_Number">Contact No</label>
					<input type="text" class="form-control" id="llNumPrefix" placeholder="0120">
				</div>
				<div class="col-lg-4">
					<label for="Contact_Number"></label>
					<input type="text" class="form-control" id="llNum" placeholder="4214148" style="margin-top: 5px; " data-text="institute">
				</div>
			</div>
			<span class="contactError" style="color:red;"></span>
		</div>                        
		<div class="form-group">
		<div class="row">
		<div class="col-lg-6">
			<label for="Address">Address</label>
			<input type="text" class="form-control" id="street-inp" placeholder="Street">
		</div>
                     <div class="col-lg-6">
                        <label for="City">City</label>
                        <input type="text" class="form-control txt-captial" id="city-inp" placeholder="City">
                    </div>
                </div>
                </div>
		<div class="form-group">
			<div class="row">
				
				<div class="col-lg-4">
					<select id="state-inp" class="form-control">
										<option value="">Select State</option>
										<optgroup label="States">
										<option>Andhra Pradesh</option>
										<option>Arunachal Pradesh</option>
										<option>Assam</option>
										<option>Bihar</option>
										<option>Chhattisgarh</option>
										<option>Goa</option>
										<option>Gujarat</option>
										<option>Haryana</option>
										<option>Himachal Pradesh</option>
										<option>Jammu and Kashmir</option>
										<option>Jharkhand</option>
										<option>Karnataka</option>
										<option>Kerala</option>
										<option>Madhya Pradesh</option>
										<option>Maharashtra</option>
										<option>Manipur</option>
										<option>Meghalaya</option>
										<option>Mizoram</option>
										<option>Nagaland</option>
										<option>Odisha</option>
										<option>Punjab</option>
										<option>Rajasthan</option>
										<option>Sikkim</option>
										<option>Tamil Nadu</option>
										<option>Telangana</option>
										<option>Tripura</option>
										<option>Uttar Pradesh</option>
										<option>Uttarakhand</option>
										<option>West Bengal</option>
										</optgroup>
										<optgroup label="Union territories">
										<option>Andaman and Nicobar Islands</option>
										<option>Chandigarh</option>
										<option>Dadra and Nagar Haveli</option>
										<option>Daman and Diu</option>
										<option>Lakshadweep</option>
										<option>Delhi</option>
										<option>Puducherry</option>
										</optgroup>
									</select>
				</div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control country-inp" disabled="disabled" placeholder="Country">
				</div>
				<div class="col-lg-4">
					<input type="text" class="form-control" id="pin-inp" placeholder="Pin Number" maxlength="6">
				</div>
			</div>
		</div>
		<div class="form-group">                            
			<div class="row">
				<div class="col-lg-12 text-right">
					<button class="btn btn-primary btn-xs cancel-button" ><i class="fa fa-times"></i> Cancel</button>
					<button class="btn btn-primary btn-xs save-button" ><i class="fa fa-save"></i> Save</button>
				</div>
			</div>
		</div>
	</div>
  </div>
</section>

<!-- Publisher -->
<section class="panel hidden-on-load" id="publisher-general-information">
  <div class="bio-graph-heading">
	  General Information                                                                                         
	  <button class="btn btn-primary btn-xs edit-button" style="float:right; border-color:#fff; background-color:#F6954B;"><i class="fa fa-pencil"></i></button>
  </div>
  <div class="panel-body bio-graph-info view-section">
	  <div class="row">
		  <div class="col-sm-12">
			  <p><span>Publisher Name : </span><span id="publisher-name"></span></p>
		  </div>
		  <div class="col-sm-12">
			  <p><span>Owner Name : </span><span id="owner-name"></span></p>
		  </div>
		  <div class="col-sm-12">
			  <p><span>Contact Person : </span><span id="contact-person"></span></p>
		  </div>
		  <div class="col-sm-6">
			  <p><span>Gender : </span><span id="gender"></span></p>
		  </div>
		  <div class="col-sm-6">
			  <p><span>Year Of Inception : </span><span id="founded-in"></span></p>
		  </div>
		  <div class="col-sm-12">
			  <p><span>Contact No : </span><span id="contact-num"></span> </p>
		  </div>                                  
		  <div class="col-sm-6">
			  <p><span>Email : </span><span id="primary-email"></span></p>
		  </div>
		  <div class="col-sm-6">
			  <p><span>Country : </span><span id="address-country"></span></p>
		  </div>
		  <div class="col-sm-12">
			  <p><span>Address : </span><span id="address-details"></span></p>
		  </div>
	  </div>
  </div>
  <div class="contact-form panel-body edit-section">
	<div role="form">
		<div class="form-group">
			<div class="row">
				<div class="col-lg-6">
					<label for="Contact_Name"><i class="text-danger">*</i>Publisher Name</label>
					<input type="text" class="form-control" id="first-name-inp" placeholder="First Name" style="margin-top: 5px;">
				</div>
				<div class="col-lg-6">
					<label for="Contact_Name"> </label>
					<input type="text" class="form-control" id="last-name-inp" placeholder="Last Name" style="margin-top: 5px;">
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-lg-6">
					<label for="Gender">Gender</label>
					<select id="gender-inp" class="form-control">
					<option value="0">Select Gender</option>
					<option value="1">Male</option>
					<option value="2">Female</option>
					</select>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-lg-6">
				<label for="Address">No. of Student </label>
					<select class="form-control m-bot15" id="institute-size-inp">
						<option value="">Not Specified</option>
						<option vlaue="100-500">100-500</option>
						<option vlaue="500-1000">500-1000</option>
						<option vlaue="1000-2000">1000-2000</option>
					</select>
				</div>
				 <div class="col-lg-6">
				<label for="Address">Year Of Inception</label>
					<select class="form-control m-bot15" id="founded-in-inp">
						<option value="0">Not Specified</option>
						<option value="2013">2013</option>
						<option value="2012">2012</option>
						<option value="2011">2011</option>
						<option value="2010">2010</option>
					</select>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row contact">
				<div class="col-lg-2">
					<label for="Contact_Number"><i class="text-danger">*</i>Contact No</label>
					<input type="text" class="form-control" id="phnNumPrefix" placeholder="+91">
				</div>
				<div class="col-lg-4">
					<label for="Contact_Number"></label>
					<input type="text" class="form-control" id="phnNum" placeholder="8882715454" style="margin-top: 5px; " maxlength="10" data-text="publisher">
				</div>
				<div class="col-lg-2">
					<label for="Contact_Number">Contact No</label>
					<input type="text" class="form-control" id="llNumPrefix" placeholder="0120">
				</div>
				<div class="col-lg-4">
					<label for="Contact_Number"></label>
					<input type="text" class="form-control" id="llNum" placeholder="4214148" style="margin-top: 5px;"  data-text="publisher">
				</div>
			</div>
			<span class="contactError" style="color:red;"></span>
		</div>                        
		<div class="form-group">
		<div class="row">
		<div class="col-lg-6">
			<label for="Address">Address</label>
			<input type="text" class="form-control" id="street-inp" placeholder="Street">
		</div>
                    <div class="col-lg-6">
                        <label for="City">City</label>
                        <input type="text" class="form-control" id="city-inp" placeholder="City">
                    </div>
                </div></div>
		<div class="form-group">
			<div class="row">
				
				<div class="col-lg-4">
					<select id="state-inp" class="form-control">
										<option value="">Select State</option>
										<optgroup label="States">
										<option>Andhra Pradesh</option>
										<option>Arunachal Pradesh</option>
										<option>Assam</option>
										<option>Bihar</option>
										<option>Chhattisgarh</option>
										<option>Goa</option>
										<option>Gujarat</option>
										<option>Haryana</option>
										<option>Himachal Pradesh</option>
										<option>Jammu and Kashmir</option>
										<option>Jharkhand</option>
										<option>Karnataka</option>
										<option>Kerala</option>
										<option>Madhya Pradesh</option>
										<option>Maharashtra</option>
										<option>Manipur</option>
										<option>Meghalaya</option>
										<option>Mizoram</option>
										<option>Nagaland</option>
										<option>Odisha</option>
										<option>Punjab</option>
										<option>Rajasthan</option>
										<option>Sikkim</option>
										<option>Tamil Nadu</option>
										<option>Telangana</option>
										<option>Tripura</option>
										<option>Uttar Pradesh</option>
										<option>Uttarakhand</option>
										<option>West Bengal</option>
										</optgroup>
										<optgroup label="Union territories">
										<option>Andaman and Nicobar Islands</option>
										<option>Chandigarh</option>
										<option>Dadra and Nagar Haveli</option>
										<option>Daman and Diu</option>
										<option>Lakshadweep</option>
										<option>Delhi</option>
										<option>Puducherry</option>
										</optgroup>
									</select>
				</div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control country-inp" disabled="disabled" placeholder="Country">
				</div>
				<div class="col-lg-4">
					<input type="text" class="form-control" id="pin-inp" placeholder="Pin Number" maxlength="6">
				</div>
                       	</div>
		</div>
		<div class="form-group">                            
			<div class="row">
				<div class="col-lg-12">
					<button class="btn btn-primary btn-xs cancel-button" style="float:right; border-color:#fff; background-color:#F6954B;"><i class="fa fa-save"></i> Cancel</button>
					<button class="btn btn-primary btn-xs save-button" style="float:right; border-color:#fff; background-color:#F6954B;"><i class="fa fa-save"></i> Save</button>
				</div>
			</div>
		</div>
	</div>
  </div>
</section>

<!-- Professor -->
<section class="panel hidden-on-load" id="professor-general-information">
  <div class="bio-graph-heading">
	  General Information                                                                                         
	  <button class="btn btn-primary btn-xs edit-button" style="float:right; border-color:#fff; background-color:#F6954B;"><i class="fa fa-pencil"></i></button>
  </div>
  <div class="panel-body bio-graph-info view-section">
	  <div class="row">
		  <div class="col-sm-12">
			  <p><span>Professor Name : </span><span id="professor-name"></span></p>
		  </div>
		  <div class="col-sm-12">
			  <p><span>Contact No : </span><span id="contact-num"></span> </p>
		  </div>                                  
		  <div class="col-sm-6">
			  <p><span>Email : </span><span id="primary-email"></span></p>
		  </div>
		  <div class="col-sm-6">
			  <p><span>Gender : </span><span id="gender"></span></p>
		  </div>
		  <div class="col-sm-6">
			  <p><span>Country : </span><span id="address-country"></span></p>
		  </div>
		  <div class="col-sm-12">
			  <p><span>Address : </span><span id="address-details"></span></p>
		  </div>
	  </div>
  </div>
  <div class="contact-form panel-body edit-section">
	<div role="form">
		<div class="form-group">
			<div class="row">
				<div class="col-lg-6">
                                    <label for="Contact_Name"><i class="text-danger">*</i>Professor Name</label>
					<input type="text" class="form-control" id="first-name-inp" placeholder="First Name" style="margin-top: 5px;">
				</div>
				<div class="col-lg-6">
					<label for="Contact_Name">&nbsp;</label>
					<input type="text" class="form-control" id="last-name-inp" placeholder="Last Name" style="margin-top: 5px;">
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-lg-6">
					<label for="Gender">Gender</label>
					<select id="gender-inp" class="form-control">
					<option value="0">Select Gender</option>
					<option value="1">Male</option>
					<option value="2">Female</option>
					</select>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row contact">
				<div class="col-lg-2">
					<label for="Contact_Number"><i class="text-danger">*</i>Contact No</label>
					<input type="text" class="form-control" id="phnNumPrefix" placeholder="+91">
				</div>
				<div class="col-lg-4">
					<label for="Contact_Number"></label>
					<input type="text" class="form-control" id="phnNum" placeholder="8882715454" style="margin-top: 5px; " maxlength="10" data-text="professor">
				</div>
				<div class="col-lg-2">
					<label for="Contact_Number">Contact No</label>
					<input type="text" class="form-control" id="llNumPrefix" placeholder="0120">
				</div>
				<div class="col-lg-4">
					<label for="Contact_Number"></label>
					<input type="text" class="form-control" id="llNum" placeholder="4214148" style="margin-top: 5px; " data-text="professor">
				</div>
			</div>
			<span class="contactError" style="color:red;"></span>
		</div>                        
		<div class="form-group">
		<div class="row">
                    <div class="col-lg-6">
                        <label for="Address">Address</label>
                        <input type="text" class="form-control" id="street-inp" placeholder="Street">
                    </div>
                    <div class="col-lg-6">
                         <label for="City">City</label>
                        <input type="text" class="form-control" id="city-inp" placeholder="City">
                    </div>
                </div></div>
		<div class="form-group">
			<div class="row">
				
				<div class="col-lg-4">
					<select id="state-inp" class="form-control">
										<option value="">Select State</option>
										<optgroup label="States">
										<option>Andhra Pradesh</option>
										<option>Arunachal Pradesh</option>
										<option>Assam</option>
										<option>Bihar</option>
										<option>Chhattisgarh</option>
										<option>Goa</option>
										<option>Gujarat</option>
										<option>Haryana</option>
										<option>Himachal Pradesh</option>
										<option>Jammu and Kashmir</option>
										<option>Jharkhand</option>
										<option>Karnataka</option>
										<option>Kerala</option>
										<option>Madhya Pradesh</option>
										<option>Maharashtra</option>
										<option>Manipur</option>
										<option>Meghalaya</option>
										<option>Mizoram</option>
										<option>Nagaland</option>
										<option>Odisha</option>
										<option>Punjab</option>
										<option>Rajasthan</option>
										<option>Sikkim</option>
										<option>Tamil Nadu</option>
										<option>Telangana</option>
										<option>Tripura</option>
										<option>Uttar Pradesh</option>
										<option>Uttarakhand</option>
										<option>West Bengal</option>
										</optgroup>
										<optgroup label="Union territories">
										<option>Andaman and Nicobar Islands</option>
										<option>Chandigarh</option>
										<option>Dadra and Nagar Haveli</option>
										<option>Daman and Diu</option>
										<option>Lakshadweep</option>
										<option>Delhi</option>
										<option>Puducherry</option>
										</optgroup>
									</select>
				</div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control country-inp" disabled="disabled" placeholder="Country">
				</div>
				<div class="col-lg-4">
					<input type="text" class="form-control" id="pin-inp" placeholder="Pin Number" maxlength="6">
				</div>
			</div>
		</div>
		<div class="form-group">                            
			<div class="row">
				<div class="col-lg-12">
					<button class="btn btn-primary btn-xs cancel-button" style="float:right; border-color:#fff; background-color:#F6954B;"><i class="fa fa-save"></i> Cancel</button>
					<button class="btn btn-primary btn-xs save-button" style="float:right; border-color:#fff; background-color:#F6954B;"><i class="fa fa-save"></i> Save</button>
				</div>
			</div>
		</div>
	</div>
  </div>
</section>