<section class="panel">
    <header class="panel-heading">
        <h4>Free tier Information</h4>
    </header>
   

    <table  class="display table table-bordered table-striped" id="free-tier1">
        <thead>
            <tr>
				<th>Sr no.</th>
                <th>institute/professor ID</th>
				<th>institute/professor Name</th>
				<th>Role</th>
                <th>free tier eligibility</th>
                <th>exam level cost</th>
                <th>course level cost</th>
				<th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</section>
