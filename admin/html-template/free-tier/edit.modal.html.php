<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
        id="modal_edit_free_tier" class="modal fade">
		   <div class="modal-dialog">
		     <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×</button>
                    <h4 class="modal-title">
                       Edit free tier Information</h4>
                </div>
                <div class="modal-body" style="padding-right: 35px;">
                    <form role="form" class="form-horizontal">
					<div class="form-group">
					<div class="col-md-4">
                        <label for="inputEmail1"> Institute/Professor Name</label>
					</div>
					<div class="col-md-8">  
                         <label id="nameI" for="inputEmail1">
                            Institute/Professor Name</label>
					</div>
                    </div>
                    <div class="form-group">
						<div class="col-md-4">
	                        <label for="inputEmail1">
	                             Free tier eligibility</label>
						</div>
						  <div class="col-md-5">
                       			 <input type="checkbox" id='freetier-eligibility' class="form-control">
						</div>
                    </div>
                    <div class="form-group">
                       <div class="col-md-4">
					    	<label for="inputEmail1">
                              	Exam level cost</label>
						</div>
                         <div class="col-md-4">
                       			 <input type="text" id='exam-level' placeholder="" class="form-control">
						</div>
						 <div class="col-md-4">
                       			 <input type="checkbox" id='exam-level-box' class="form-control">
						</div> 
						
                    </div>
                    <div class="form-group">
                       <div class="col-md-4">
					    	<label for="inputEmail1">
                              	course level cost</label>
						</div>
                         <div class="col-md-4">
                       			 <input type="text" id='course-level'  class="form-control">
						</div>
						 <div class="col-md-4">
                       			 <input type="checkbox" id='course-level-box'  class="form-control"> 
						</div> 
						
                    </div>
  
                    <div class="form-group">
                        <div style="text-align: right;">
                            <a class="btn btn-info" id="save_free_tier">
                                   Save </a> 
								 <button aria-hidden="true" data-dismiss="modal" class="btn btn-danger" type="button">Cancel</button>
							  </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>