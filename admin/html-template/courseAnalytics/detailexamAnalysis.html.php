<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
     id="detailexamAnalysis" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"  class="close datatablecll buttoncolWhite" id="datableclose" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <h4 class="modal-title">
                   Detail Question Analysis</h4>
            </div>
			<div class="modal-body">
			
			  <section class="panel">
					<div id ="overflowAuto">Question:
					<div id="questioncomplete"></div>
					<ul>
						<li><span class="legend text-danger">NCA</span> - No correct answers</li>
					</ul>
				    <table class="display table table-bordered table-striped" id="detail-questionComplete">
				        <thead>
				            <tr>
								<th>Serial no.</th>
				                <th>Student Name</th>
								<th>No of times question appeared</th>
								<th>correct</th>
								<th>incorrect</th>
								<th>unattempted</th>
								<th>Average Time(secs)</th>
								<th>Fastest time(secs)</th>
								<th>Needs Review</th>                   
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
				    </table>

					</div>
				</section>
			</div>
		</div>
	</div>
</div>
