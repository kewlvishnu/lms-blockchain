<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
     id="detailAnalytics" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"  class="close datatablecll buttoncolWhite" id="datableclose" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <h4 class="modal-title">
                   Summary of students course progress</h4>
            </div>
			<div class="modal-body">
			  <section class="panel">
					<div id ="overflowAuto">
				    <table class="display table table-bordered table-striped" id="detail-StudentProgress">
				        <thead>
				            <tr>
				                <th>Student Name</th>
				                <th class="text-center">Content:<span id="content-name"></span> Watched (%)  </th>    
								<th class="text-center">Total time spent by student (hh:mm:ss)</th>                    
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
				    </table>

					</div>
				</section>
			</div>
		</div>
	</div>
</div>
