<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="approvals.php"><i class="fa fa-home"></i>Dashboard</a></li>
            <li><a href="admin_packages.php">Packages & Subsciption</a></li>
			<li>View & Edit Packages</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<div id="package-details" class="clearfix">
<aside class="col-md-1"></aside>
    <aside class="col-md-10">
<section class="panel">
            <div class="custom_img" style="margin-bottom: 20px;">
                <img src="" id="package-image-preview"/>
				<a class="btn btn-md btn-danger upload" style="position: relative;top: -55px;left: 78%;display: none;width: 150px;" data-toggle="modal" href="#upload-image"><i class="fa fa-pencil"></i> Change Cover</a>
            </div>
            <header class="custom-edit-text" style="padding-left: 0px;">
                <h3><span id="course-name"></span>
                    <button title="Edit package" class="btn btn-danger btn-xs edit-button" style="float: right; border:none;  margin-top: 0px;"><i class="fa fa-pencil"></i> Edit Package</button> 
                </h3>
            </header>
					
			<div class="col-md-1"></div>
            <h6 id="course-subtitle"></h6>
            <span class="custom-sub">Package Name:</span>
            <span id="pack-name"></span>
            <br />
			<div class="col-md-1"></div>
            <span class="custom-sub">Package Description: </span>
            <span id="pacD"></span>
            <br />
			<div class="col-md-1"></div>
            <span class="custom-sub">Package type:</span>
            <span id="pactype"></span>
			<br />
			<div class="col-md-1"></div>
            <span class="custom-sub">Package Category : </span>
            <span id="paccat"></span>
            <br />
			<div class="col-md-1"></div>
            <span class="custom-sub">Start date of Package : </span>
            <span id="startdate"></span>
            <br />
			<div class="col-md-1"></div>
            <span class="custom-sub">End date of Package : </span>
            <span id="endDate"></span>
            <br />
			<div class="col-md-1"></div>
            <span class="custom-sub">Courses of packages : </span>
            <span id="packCourses"></span>
            <br />
			<p id="extraInfo"></p>
			<br />
	</section>
</aside>
</div>
<div  class="panel hidden-l" id="edit-package" >
<section class="panel">
    <header class="panel-heading">
        <h4>View & Edit Packages</h4>
		<button class="btn btn-primary btn-xs cancel-edit-button" style="float: right; border-color: rgb(255, 255, 255); background-color: #f6954b; margin-top: 0px;"><i class="fa fa-times"></i></button>
    </header>
    <div class="panel-body">
      <form role="form">
			
			<div class="form-group">
				<div class="row">
					<div class="col-lg-5 col-md-5">
						<label for="coursename" id="packName">Package Name</label>
						<input type="text" placeholder="Package Name"  class="form-control" id="package-name">
					</div>
			
					<div class="col-lg-6 col-md-6">
						<label for="coursename" id="packDesc">Package Description</label>
			  <textarea   placeholder="Package Description visible to users" rows="5" cols="80" id="package-description" maxlength="1000"></textarea>
					
					</div>
					
				</div>
			</div>
			<div id="extraFields">
				<div class="form-group">
					<div class="row">
						<div class="col-lg-5 col-md-5">
							<label for="coursename" id="pactype">Select Package type <i class="tooltips fa fa-info-circle" data-original-title="Package type will tell if package is for internal users or it is for all users of arcanemind" data-placement="right"></i></label><br>
							<label>
								<input type="radio" name="optionsRadios1" id="marketplace" value="option1" >Market Place
							</label>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<label>
								<input type="radio" name="optionsRadios1" id="internal" value="option1" >Internal
							</label>
						</div>
						<div class="col-lg-6 col-md-6">
<button class="btn btn-primary  btn-sm"  type="button" id="imgPack"data-toggle="modal" href="#upload-image" >Upload Package Image</button>
						</div>





		</div>

				</div>
				
				
				
				
				
				<!-- hidden div for internal student select-->
		<div class="form-group" id="internalselect" style="display: none;">
		<div class="row">
			 <div class="col-lg-5 col-md-5">
							<label for="coursename" id="paytype">Select Payment type <i class="tooltips fa fa-info-circle" data-original-title="Package type will tell if package is for internal users or it is for all users of arcanemind" data-placement="right"></i></label><br>					
							<label>
								<input type="radio" name="optionsRadios3" id="stupay" value="option1" >Student will pay
							</label>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<label>
								<input type="radio" name="optionsRadios3" id="inspay" value="option1" >Institute will pay
							</label>
			 </div>
			<div class="col-lg-3 col-md-3" id="student-pay" style="display: none;">
						<label for="coursename">Total Number of students  </label>
						<label for="coursename" id="noof-student"></label>
						
			 </div>
		  </div>
		<div class="row" id="select-students" >
						<div class="col-md-2"></div>
						<div class="col-md-10">
							<label class="control-label">
								Available Students
							</label>
							<br/>
							<br/>
							<select multiple="multiple" class="multi-select multiple" id="students-list" name="my_multi_select1[]">
								
							</select>

						</div>
					</div>
	</div>


			<!-- for marektplace only-->
			<div class="form-group" id="marketselect" style="display: none;">
						<div class="col-lg-6 col-md-6">
							<label for="coursename">Price</label>
									<input type="text" class="form-control" id="price" placeholder="please Enter price in $">
								
							

						</div>
						<div class="col-lg-6 col-md-6">
							<label for="courseid">PriceINR</label>
							<input type="text" class="form-control" id="priceINR" placeholder="Please Enter Price in INR">
						</div>
			</div>
				
				
				
				<!--  for course category select-->
				
				<div class="form-group">
					<div class="row">
						
				<div class="col-lg-7 col-md-7">
				  <label for="courseid" id="packCat">Package Category</label>
                                  <br/>
						<div id="allarcanemindcourses">
							<label>
								<input type="radio" name="optionsRadios2" id="allarcanemindcourses" value="option1" >All Arcanmeind Courses
							</label>
							</div>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<div id="allcourses">
							<label>
								<input type="radio" name="optionsRadios2" id="allowncourses" value="option1" >All Own Courses
							</label>
							</div>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<div id="catCourses">
							<label>
								<input type="radio" name="optionsRadios2" id="category-course" value="option1" >Courses From Category
							</label>
							</div>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<div id="cust-courses">
							<label>
								<input type="radio" name="optionsRadios2" id="custom-course" value="option1" >Custom Courses
							</label>
							</div>
			</div>



		</div>

				</div>
				<!-- hidden div for course category select-->
				<div class="form-group" id="catSelect" style="display: none;">
		<div class="row">
			  <div class="col-md-2">
			  </div>
	         	 <div class="col-md-4" id="catSelect1" style="display: none;">
					  <label for="courseid">Course Category</label>
	                                  <br/>
					  <select id="course-cat" class="form-control">
						<option value="1">Entrance Exams</option>
						<option value="2">School & College Prep</option>
	                    <option value="3">Hobbies & Skills</option>
	                    <option value="4">Business & Management</option>
	                    <option value="5">IT & Software</option>
	                    <option value="6">Entrepreneurship</option>
	                    <option value="7">Miscellaneous</option>
	                    <option value="8">Finance & Accounting</option>
					  </select>
				  </div>
			<!--  <div class="col-md-4" id="catSelect2" style="display: none;">
					  <label for="courseselect">Courses</label>
	                                  <br/>
					  <select id="courses" class="form-control" multiple="true" >
						<option value="1">Entrance Exams</option>
						<option value="2">School & College Prep</option>
	                    <option value="3">Hobbies & Skills</option>
	                    <option value="4">Business & Management</option>
	                    <option value="5">IT & Software</option>
	                    <option value="6">Entrepreneurship</option>
	                    <option value="7">Miscellaneous</option>
	                    <option value="8">Finance & Accounting</option>
					  </select>
				  </div> -->
						<div class="col-md-4" id="catSelect2" style="display: none;">
							<label class="control-label">
								Available Courses
							</label>
							<br/>
							<br/>
							<select multiple="multiple" class="multi-select multiple" id="courses-list" name="my_multi_select1[]">
								
							</select>

						</div>
					
		  </div>
	</div>

	
 			
			<div class="form-group">
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<label for="coursename">Start date of Package</label>
									<input type="text" class="form-control" id="pstartDate" placeholder="10 Jan 2015 14:30">
								
							

						</div>
						<div class="col-lg-6 col-md-6">
							<label for="courseid">End date of Package</label>
							<input type="text" class="form-control" id="pendDate" placeholder="10 Jan 2015 14:30">
						</div>
					</div>
				</div>
			
			
			<!-- for marektplace-->
			
			
			</div>
			
			<div class="row panel-body"  style="text-align: right;">
				<div class="col-lg-12">
				
					<button class="btn btn-primary save-button btn-sm" id="savepack" type="button">Create Packages</button>
				
					
					<button class="btn btn-primary save-pay-button btn-sm" id="paynow-savepack" type="button"  style="display: none;">Create Package & Continue</button>
					
					<a href="createPackage.php" class="btn btn-danger btn-sm">Cancel</a>
				</div>
			</div>
		</form>
    </div>
	<hr>
</section>
</div>