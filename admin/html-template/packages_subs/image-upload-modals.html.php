<!--Modal for image upload-->
	<div aria-hidden="true" aria-labelledby="upload-image-modal" role="dialog" tabindex="-1"
        id="upload-image" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                    <h4 class="modal-title">
                        Upload Package Image
					</h4>
                </div>
                <div class="modal-body">
                <div class="row">
					<div class="col-lg-12">
						<form id="package-image-form"  method="post" enctype="multipart/form-data">
							<label for="exampleInputFile">PackageImage</label><br/>
							<div style="min-height: 35px;"><input type="file" id="package-image" name="package-image" class="pull-left"/>
							<input type="submit" value="Upload" class="pull-left" style="margin-left: 20%;"></div>
							<div><img src id="image-preview" style="display: none;" class="crop"/></div>
							<input type="hidden" id="x" name="x" />
							<input type="hidden" id="y" name="y" />
							<input type="hidden" id="w" name="w" />
							<input type="hidden" id="h" name="h" />
						</form>						
					</div>
                </div>
                </div>
            </div>
        </div>
    </div>
	
	<div aria-hidden="true" aria-labelledby="upload-image-modal" role="dialog" tabindex="-1"
        id="upload-profile-image" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                    <h4 class="modal-title">
                        Upload Profile Image
					</h4>
                </div>
                <div class="modal-body">
                <div class="row">
					<div class="col-lg-12">
						<form id="profile-image-form"  method="post" enctype="multipart/form-data">
							<input type='file' id="profile-image-uploader" name="profile-image"/>
							<img id="uploaded-image2" class="crop" src="#" alt="Your image" />
							<input type="hidden" id="x2" name="x" />
							<input type="hidden" id="y2" name="y" />
							<input type="hidden" id="w2" name="w" />
							<input type="hidden" id="h2" name="h" />
							<div class="progress" style="display:none">Saving....</div>
							<div class="msg"></div>
							<input type="submit" value="Save!" style="display:none"/>
						</form>						
					</div>
                </div>
                </div>
            </div>
        </div>
    </div>