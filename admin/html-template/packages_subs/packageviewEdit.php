<div class="row">
	<div class="col-lg-12">
		<!--breadcrumbs start -->
		<ul class="breadcrumb">
			<li><a href="dashboard.php"><i class="fa fa-home"></i>Dashboard</a></li>
			<li><a href="packages_subs.php">Packages &amp; Subsciption</a></li>
			<li>View &amp; Edit Packages</li>
		</ul>
		<!--breadcrumbs end -->
	</div>
</div>
<div id="package-details" class="row">
	<aside class="col-md-1"></aside>
	<aside class="col-md-10">
		<section class="panel">
			<div class="custom_img previewPackage">
				<img src="" id="package-image-preview"/>
				<a class="btn btn-md btn-danger upload packageimagebox"  data-toggle="modal" href="#upload-image"><i class="fa fa-pencil"></i> Change Cover</a>
			</div>
			<header class="custom-edit-text" >
				<h3><span id="course-name"></span>
					<button title="Edit package" class="btn btn-danger btn-xs edit-button editPackageButton" ><i class="fa fa-pencil"></i> Edit Package</button> 
				</h3>
			</header>

			<h6 id="course-subtitle"></h6>
			<span class="custom-sub">Package Name:</span>
			<span id="pack-name"></span>

			<span class="custom-sub">Package Description: </span>
			<span id="pacD"></span>
			<span class="custom-sub">Package type:</span>
			<span id="pactype"></span>

			<span class="custom-sub">Package Category : </span>
			<span id="paccat"></span>

			<span class="custom-sub">Price : </span>
			<span id="price"></span>

			<span class="custom-sub">PriceINR : </span>
			<span id="priceINR"></span>

			<span class="custom-sub">Start date of Package : </span>
			<span id="startdate"></span>

			<span class="custom-sub">End date of Package : </span>
			<span id="endDate"></span>

			<span class="custom-sub">Courses of packages : </span>
			<span id="packCourses"></span>
			<p id="extraInfo"></p>
		</section>
	</aside>
</div>
<div class="panel hidden-l" id="edit-package" >
	<section class="panel">
		<header class="panel-heading">
			<h4>View &amp; Edit Packages</h4>
			<button class="btn btn-primary btn-xs cancel-edit-button packageEdit_cancelButton" ><i class="fa fa-times"></i></button>
		</header>
		<div class="panel-body">
			<form role="form">
				<div class="form-group">
					<div class="row">
						<div class="col-md-5">
							<label for="coursename" id="packName">Package Name</label>
							<input type="text" placeholder="Package Name"  class="form-control" id="package-name">
						</div>
						<div class="col-md-6">
							<label for="coursename" id="packDesc">Package Description</label>
							<textarea placeholder="Package Description visible to users" rows="5" cols="80" id="package-description" maxlength="1000"></textarea>
						</div>
					</div>
				</div>
				<div id="extraFields">
					<div class="form-group">
						<div class="row">
							<div class="col-md-5">
								<label for="coursename" id="pactype">Select Package type <i class="tooltips fa fa-info-circle" data-original-title="Package type will tell if package is for internal users or it is for all users of arcanemind" data-placement="right"></i></label><br>
								<label>
									<input type="radio" name="optionsRadios1" id="marketplace" value="option1" >Market Place
								</label>
								<label>
									<input type="radio" name="optionsRadios1" id="internal" value="option1" >Internal
								</label>
							</div>
							<div class="col-md-6">
								<button class="btn btn-primary btn-sm" type="button" id="imgPack"data-toggle="modal" href="#upload-image" >Upload Package Image</button>
							</div>
						</div>
					</div>
					<!-- hidden div for internal student select-->
					<div class="form-group" id="internalselect" style="display: none;">
					<div class="row">
				 		<div class="col-md-5">
							<label for="coursename" id="paytype">Select Payment type <i class="tooltips fa fa-info-circle" data-original-title="Package type will tell if package is for internal users or it is for all users of arcanemind" data-placement="right"></i></label><br>					
							<label>
								<input type="radio" name="optionsRadios3" id="stupay" value="option1" >Student will pay
							</label>
							<label>
								<input type="radio" name="optionsRadios3" id="inspay" value="option1" >Institute will pay
							</label>
						</div>
						<div class="col-md-3" id="student-pay" style="display: none;">
							<label for="coursename">Total Number of students  </label>
							<label for="coursename" id="noof-student"></label>
						</div>
					</div>
					<div class="row" id="select-students" >
						<div class="col-md-2"></div>
						<div class="col-md-10">
							<label class="control-label">
								Available Students
							</label>
							<select multiple="multiple" class="multi-select multiple" id="students-list" name="my_multi_select1[]"></select>
						</div>
					</div>
				</div>
				<!-- for marektplace only-->
				<div class="form-group" id="marketselect" style="display: none;">
					<div class="col-md-6">
						<label for="coursename">Price</label>
						<input type="text" class="form-control mprice" id="price" placeholder="please Enter price in $">
					</div>
					<div class="col-md-6">
						<label for="courseid">PriceINR</label>
						<input type="text" class="form-control mpriceINR" id="priceINR" placeholder="Please Enter Price in INR">
					</div>
				</div>

				<!--  for course category select-->
				<div class="form-group">
					<div class="row">
						<div class="col-md-7">
							<label for="courseid" id="packCat">Package Category</label>
							<div id="allcourses">
								<label>
									<input type="radio" name="optionsRadios2" id="allowncourses" value="option1" >All Own Courses
								</label>
							</div>
							<div id="catCourses">
								<label>
									<input type="radio" name="optionsRadios2" id="category-course" value="option1" >Courses From Category
								</label>
							</div>
							<div id="custom-course">
								<label>
									<input type="radio" name="optionsRadios2" id="custom-course" value="option1" >Custom Courses
								</label>
							</div>
						</div>
					</div>
				</div>
				<!-- hidden div for course category select-->
				<div class="form-group" id="catSelect" style="display: none;">
					<div class="row">
						<div class="col-md-2">
						</div>
		         		<div class="col-md-4" id="catSelect1" style="display: none;">
							<label for="courseid">Course Category</label>
							<select id="course-cat" class="form-control">
								<option value="1">Entrance Exams</option>
								<option value="2">School &amp; College Prep</option>
								<option value="3">Hobbies &amp; Skills</option>
								<option value="4">Business &amp; Management</option>
								<option value="5">IT &amp; Software</option>
								<option value="6">Entrepreneurship</option>
								<option value="7">Miscellaneous</option>
								<option value="8">Finance &amp; Accounting</option>
							</select>
						</div>
						<!--  <div class="col-md-4" id="catSelect2" style="display: none;">
							<label for="courseselect">Courses</label>
							<select id="courses" class="form-control" multiple="true" >
							<option value="1">Entrance Exams</option>
							<option value="2">School & College Prep</option>
							<option value="3">Hobbies & Skills</option>
							<option value="4">Business & Management</option>
							<option value="5">IT & Software</option>
							<option value="6">Entrepreneurship</option>
							<option value="7">Miscellaneous</option>
							<option value="8">Finance & Accounting</option>
							</select>
						</div> -->
						<div class="col-md-4" id="catSelect2" style="display: none;">
							<label class="control-label">
								Available Courses
							</label>
							<select multiple="multiple" class="multi-select multiple" id="courses-list" name="my_multi_select1[]"></select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<label for="coursename">Start date of Package</label>
							<input type="text" class="form-control" id="pstartDate" placeholder="10 Jan 2015 14:30">
						</div>
						<div class="col-md-6">
							<label for="courseid">End date of Package</label>
							<input type="text" class="form-control" id="pendDate" placeholder="10 Jan 2015 14:30">
						</div>
					</div>
				</div>
				<!-- for marektplace-->
				<div class="row panel-body"  style="text-align: right;">
					<div class="col-lg-12">
						<button class="btn btn-primary save-button btn-sm" id="savepack" type="button">Save Packages</button>
						<button class="btn btn-primary save-pay-button btn-sm" id="paynow-savepack" type="button"  style="display: none;">Create Package &amp; Continue</button>
						<a href="createPackage.php" class="btn btn-danger btn-sm">Cancel</a>
					</div>
				</div>
			</form>
		</div>
		<hr>
	</section>
</div>