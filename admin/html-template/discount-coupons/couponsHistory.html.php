



<section class="panel">
  <header class="panel-heading">
	 <h4>Coupons History</h4>
  </header>
  <div style="overflow: auto;">
  <table class="table table-hover" id="all-courses">
	  <thead>
	  <tr>		
		<th width="10%">Course ID</th>
		<th width="10%">Courses Name</th>
		<th  width="10%">Coupon Code</th>
		<th width="10%">Discount %</th>
		<th width="10%">New Price ($)</th>
		<th width="10%">New Price (<i class='fa fa-rupee'> </i>)</th>
		<th width="10%">Usage / Limits</th>
		<th width="10%">Start Date</th>
		<th width="10%">End Date</th>
		<!--<th width="10%">Ownership</th>-->
		<th width="10%"><i class=" fa fa-edit"></i>Edit Coupons</th>                              
	  </tr>
	  </thead>
	  <tbody>
	  </tbody>
  </table>
  </div>
</section>