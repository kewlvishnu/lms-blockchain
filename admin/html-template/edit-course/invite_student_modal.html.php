<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_invite_student" class="modal fade">
		   <div class="modal-dialog">
		     <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-times"></i></button>
                    <h4 class="modal-title">
                        Invite Students</h4>
                </div>
                <div class="modal-body" style="padding-right: 35px;">
                    <form role="form">
                    <div class="form-group">
                        <label for="inputEmail1">
                            To send multiple invitations, please separate email-addresses by using Comma(,)
                        </label>
                        <textarea type="textarea" class="form-control" placeholder="" id="textarea_email_id"> </textarea>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-8">
                                Pending Enrollments <span class="badge bg-important keys-pending">0</span>
                                Invitations Remaining <span class="badge bg-important keys-remaining">0</span>
                                <a class="btn btn-success btn-xs purchase-more" data-toggle="modal" href="#Purchase_More">Purchase More</a>
                            </div>
                            <div class="col-lg-4">
                                <div style="text-align: right;">
                                    <a class="btn btn-primary btn-xs" id="send_activation_link"><i class="fa fa-users"></i> Invite</a>
                                    <button class="btn btn-primary btn-xs" aria-hidden="true" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
  </div>