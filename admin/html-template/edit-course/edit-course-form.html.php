<div id="course-details" class="clearfix">
    <aside class="col-md-9">
        <section>
            <div class="custom_img" style="margin-bottom: 20px;">
                <img src="" id="course-image-preview"/>
        		<a class="btn btn-md btn-danger upload" style="position: relative;top: -55px;left: 78%;display: none;width: 150px;" data-toggle="modal" href="#upload-image"><i class="fa fa-pencil"></i> Change Cover</a>
            </div>
            <header class="custom-edit-text" style="padding-left: 0px;">
                <h3><span id="course-name"></span>
                    <button title="Edit Course" class="btn btn-danger btn-xs edit-button" style="float: right; border:none;  margin-top: 0px;"><i class="fa fa-pencil"></i> Course</button> 
                </h3>
            </header>
            <h6 id="course-subtitle"></h6>
            <span class="custom-sub">Course Category:</span>
            
            <span id="course-categ"></span>
            <br />
            <span class="custom-sub">Target Audience: </span>
            <span id="target-audience"></span>
            <br />
            <span class="custom-sub">Course Introduction:</span>
            <p id="course-description"></p>
        </section>
        <section class="panel" id="existing-subjects">
            <header class="panel-heading">
                <h4>Course Subjects </h4>
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-danger btn-xs  dropdown-toggle" data-toggle="dropdown">Add Subject <span class="caret"></span></button> 
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="add-subject.php" class="add-subject-link">Create New Subject</a></li>
                        <li><a href="#import-content-modal" data-toggle="modal">Import Subject</a></li>
                    </ul>
                </div>
            </header>
            <table class="table table-hover" >
                <thead>
                    <tr>
                        <th>Subject Name</th>
                        <th></th>
                        <th>Subject Id</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </section>
		<?php
			if($_SESSION['userRole'] != 3) {
		?>
			<section class="panel">
				<header class="panel-heading">
					<h4>Student</h4>
				</header>
				<div class="panel-body">
					<div class="row" style="padding: 0px 5px;">
						<a href="#modal_invite_student" data-toggle="modal" class="btn btn-xs btn-success invite_students">Assign Student </a>
                        <a href="javascript:void(0)" class="btn btn-success btn-xs btn-add-students">Generate new students</a>
                        <a href="javascript:void(0)" target="_blank" class="btn btn-success btn-xs" id="btnExportStudents">Export students (Excel)</a>
						<!--<a href="#" data-toggle="modal" class="btn btn-xs btn-warning">Invite Student </a>-->
						<a data-toggle="modal" href="#Purchase_More" class="btn btn-xs btn-success pull-right" style="margin-left: 10px;">Purchase Course Key</a>
                        <a id="remaining_keys_total" href="CourseKey.php" class="badge bg-important pull-right"></a>
                        <span class="pull-right">Remaining Account:</span>
                        <a id="pending_keys_total" href="CourseKey.php" class="badge bg-important pull-right"></a>
                        <span class="pull-right">Pending Enrollments:</span>
					</div>
				</div>
			</section>
			<section class="panel existing-students">
				<header class="panel-heading">
					<h4>Course Students</h4>
                    <div class="btn-group pull-right">
                        <button class="btn btn-success btn-xs btn-add-students">Add new students</button>
                        <button class="btn btn-danger btn-xs" id="jsRemoveStudents">Remove Students</button>
                        <button class="btn btn-info btn-xs" id="jsParent">Generate Parent Credentials</button>
                    </div>
				</header>
				<div style="overflow: auto;">
				<table class="table table-striped" id="tbl_students_details_of_course">
					<thead>
						<tr>
                            <th><input type="checkbox" class="js-select-all" /></th>
							<th>ID</th>
                            <th>Username</th>
							<th>Password</th>
							<th>Name</th>
							<th>Email</th>
                            <th>Parent (UserID/Password)</th>
						</tr>
					</thead>
					<tbody>
	
					</tbody>
				</table>
				</div>
			</section>
			
		<?php
			}
		?>
    </aside>
    <aside class="profile-nav col-md-3">
        <section class="panel">
            <ul class="nav nav-pills nav-stacked">
                <li><a><i class="fa fa-book"></i>Course Id <span id="course-id" class="pull-right r-activity"></span></a></li>
                <li><a><i class="fa fa-calendar"></i>Live Date <span id="live-date" class="pull-right r-activity"></span></a></li>
                <li><a><i class="fa fa-calendar"></i>End Date <span id="end-date" class="pull-right r-activity"></span></a></li>
            </ul>
        </section>
        <!-- demo video section -->
        <section class="panel">
            <div class="flat-carousal">Demo Video</div>
            <div class="panel-body"><a data-toggle="modal" href="#video-upload-modal" class="btn btn-sm btn-success btn-block hidden-l" >Upload Demo Video</a></div>
        </section>


		
        <!-- rating section -->
        <section class="panel">
            <div class="flat-carousal"><span>Student Ratings</span></div>
            <div class="panel-body">
                <span style="font-size: 1.5em;">
                    <input type="hidden" DISABLED data-filled="fa fa-star gold" data-empty="fa fa-star-o" value="0" class="rating" id="rating" />
                    &nbsp;(<span id="ratingTotal"></span>)
                    <button class="btn btn-xs btn-info pull-right" id="viewReview">View Reviews</button>
                </span>
            </div>
        </section>
		<?php
			if($_SESSION['userRole'] != 3) {
		?>
			<section class="panel">
				<div class="flat-carousal">
					<span>Student</span>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
                            <div class="btn-group text-center">
                                <a href="#modal_invite_student" class="btn btn-sm btn-success invite_students" data-toggle="modal">Assign Student </a>
                                <a href="javascript:void(0)" class="btn btn-sm btn-danger btn-add-students">Generate new students</a>
                            </div>
                        </div>
						<!--<div class="col-md-6"><a class="btn btn-xs btn-warning" data-toggle="modal" href="#">Invite Student </a></div>-->
					</div>
				</div>
			</section>
		<?php
			}
		?>
		<section class="panel assignSub dispNone" id="height-35">
			<div class="flat-carousal" >Assign Subjects to students</div>
			<span id="font-size1">
				<div class="col-md-12"><a class="btn btn-sm btn-success btn-block mar3 "  id="viewAssignSubs" >Assign Subjects </a></div>
			</span>
		</section>
		<section class="panel" id="height-35">
			<div class="flat-carousal" >Course Document</div>
			<span id="font-size1">
				<div class="col-md-12"><a href="#upload-courseDoc-modal" data-toggle="modal" class="btn btn-sm btn-success btn-block mar3 "  >Upload Course Document </a></div>
			</span>
		</section>
        <section class="panel">
            <div class="flat-carousal">
                <span>Request to sell</span>
            </div>
            <div class="panel-body">
				<span id="resultResend" style="display: none;">Result: Rejected </span>
                <button  class="btn btn-success btn-xs request-approval-link hidden-l">Send Request</button>
                <a data-toggle="modal" href="#market-place-modal" class="hidden-l market-place-link"><button  class="btn btn-success btn-xs">Market Place</button></a>
            </div>
        </section>
        <section class="panel hidden-l" id="avail-student">
            <div class="flat-carousal">
                <span>Available for student</span><a class="btn btn-xs btn-success pull-right" data-toggle="modal" href="#">Go Live</a>
            </div>
            <footer class="follower-foot custom-follower-foot">
                <ul>
                    <li>
                        <h5><i class=" fa fa-inr"></i></h5>
                        <p id="student-price"></p>
                    </li>
                    <li>
                        <h5><i class=" fa fa-usd"></i></h5>
                        <p>0</p>
                    </li>
                </ul>
            </footer>
        </section>
        <section class="panel hidden-l" id="avail-professor">
            <div class="flat-carousal">
                <span>Available for Professor</span><a class="btn btn-xs btn-success pull-right" data-toggle="modal" href="#">Go Live</a>
            </div>

            <ul class="nav nav-pills nav-stacked">
                <li><a>One Year License </a></li>
                <li><a>One Time Transfor </a></li>
                <li><a>Selective IP Transfer</a></li>
                <li><a>Commission based</a></li>
            </ul>
        </section>
    </aside>
</div>

<div class="panel hidden-l" id="edit-form">
    <header class="panel-heading">
        Edit Course
        <button class="btn btn-primary btn-xs cancel-edit-button" style="float: right; border-color: rgb(255, 255, 255); background-color: #f6954b; margin-top: 0px;"><i class="fa fa-times"></i></button>
    </header>
    <div class="panel-body">
        <div role="form">
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-12">
                        <b><i class="text-danger">*</i></b>  
                        <label for="coursename"><strong>Course Name</strong></label>
                        <input type="text" placeholder="" id="course-name" class="form-control">
                        <div class="form-msg course-name"></div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-12">
                        <b><i class="text-danger">*</i></b>  
                        <label for="coursename"><strong>Course Subtitle</strong></label>
                        <input type="text" placeholder="Course Subtitle" id="course-subtitle" class="form-control">
                        <div class="form-msg course-subtitle"></div>
                    </div>                                      
                </div>
            </div>
            <div class="form-group">
                <b><i class="text-danger">*</i></b>  
                <label for="coursedescription"><strong>Course Description</strong></label>
                <textarea class="form-control" rows="5" cols="60" id="course-description"></textarea>
            </div>
            <div class="form-group">
                <b><i class="text-danger">*</i></b>  
                <label for="targetAudience"><strong>Target Audience</strong></label>
                <input type="text" class="form-control" id="target-audience">
            </div>
			<div class="form-group">
                <label for="tags">Tags</label>
                <input type="text" class="form-control" id="tags">
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6">
                        <b><i class="text-danger">*</i></b>
                        <label for="coursename"><strong>Course Start Date</strong></label>
                        <input type="text" placeholder="10 Jan 2015" id="live-date" class="form-control">
                    </div>
                    <div class="col-lg-6">
                        <label for="coursename">Set End Date</label>
                        <select class="form-control m-bot15 custom-form" id="set-end-date">
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select> 
                    </div>
                </div>
            </div>
            <div class="form-group hidden-on-load">
                <div class="row">
                    <div class="col-lg-6">
                        <label for="courseid">Course End Date</label>
                        <input type="text" placeholder="10 Jan 2015" id="end-date" class="form-control" />
                        <div class="form-msg end-date"></div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <!--<div class="col-lg-6">
                        <form id="course-image-form"  method="post" enctype="multipart/form-data">
                            <label for="exampleInputFile">Course Images</label>
                            <input type="file" id="course-image" name="course-image" />
                            <img src id="image-preview" style="max-width:100%;height:auto;" class="crop"/>
                            <input type="hidden" id="x" name="x" />
                            <input type="hidden" id="y" name="y" />
                            <input type="hidden" id="w" name="w" />
                            <input type="hidden" id="h" name="h" />
                        </form>
                    </div>-->
                    <div class="col-lg-6">
                        <label for="courseid">Course Category</label>
                        <br/>
                        <select id="course-cat" class="form-control" multiple>
                            <option value="1">Entrance Exams</option>
                            <option value="2">School &amp; College Prep</option>
                            <option value="3">Hobbies &amp; Skills</option>
                            <option value="4">Business &amp; Management</option>
                            <option value="5">IT &amp; Software</option>
                            <option value="6">Entrepreneurship</option>
                            <option value="7">Miscellaneous</option>
                            <option value="8">Finance &amp; Accounting</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <p class="text-right">
            <button class="btn btn-primary btn-xs cancel-edit-button"><i class="fa fa-times"></i> Cancel</button>
            <button class="btn btn-primary btn-xs save-button"><i class="fa fa-save"></i> Save</button>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="addStudentsModal" tabindex="-1" role="dialog" aria-labelledby="addStudentsModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addStudentsModalLabel">Generate Students</h4>
      </div>
      <div class="modal-body">
        <p><strong>Available Keys: <span class="badge bg-important" id="availableKeys"></span></strong></p>
        <p class="text-danger">NOTE: Students generation will use the available course keys.</p>
        <form id="frmGenStudents">
            <div class="form-group">
                <label for="">Number of students to generate :</label>
                <input type="text" class="form-control" id="inputGenStudents" />
                <p class="help-block text-danger"></p>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnGenStudents">Generate students</button>
      </div>
    </div>
  </div>
</div>