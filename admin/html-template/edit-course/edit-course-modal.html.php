<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="apply-licensing" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					x
				</button>
				<h4 class="modal-title">
					Apply Licensing
				</h4>
			</div>
			<div class="modal-body">
				Please Select Licensing Option and their details
				<div class="modal-body">
					<div role="form">
						<div class="form-group">
						<div class="form-msg licensing"></div>
							<div class="col-lg-6">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="lic-opt-1" class="lic-opt">
										One Year Licensing
									</label>
								</div>
							</div>
						</div>
						<div class="form-group hidden-on-load">
							<label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">
								Price:</label>
							<div class="col-lg-6">
								<input type="text" placeholder="100 " id="lic-opt-1-price" class="form-control">
								<div class="form-msg licensing-op-1"></div>
							</div>
							<div class="col-lg-12">
								Total=70+15(AC)+10(ST)+5(PGC)
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-6">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="lic-opt-2" class="lic-opt">
										One Time Transfer
									</label>
								</div>
							</div>
						</div>
						<div class="form-group hidden-on-load">
							<label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">
								Price:</label>
							<div class="col-lg-6">
								<input type="text" placeholder="100 " id="lic-opt-2-price" class="form-control">
								<div class="form-msg licensing-op-2"></div>
							</div>
							<div class="col-lg-12">
								Total=70+15(AC)+10(ST)+5(PGC)
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-6">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="lic-opt-3" class="lic-opt">
										Selective IP transfer
									</label>
								</div>
							</div>
						</div>
						<div class="form-group hidden-on-load">
							<label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">
								Price:</label>
							<div class="col-lg-6">
								<input type="text" placeholder="100 " id="lic-opt-3-price" class="form-control">
								<div class="form-msg licensing-op-3"></div>
							</div>
							<div class="col-lg-12">
								Total=70+15(AC)+10(ST)+5(PGC)
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-6">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="lic-opt-4" class="lic-opt" />
										Commission based licensing
									</label>
								</div>
							</div>
						</div>
						<div class="form-group hidden-on-load">
							<div class="row">
								<label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">
									Price:</label>
								<div class="col-lg-6">
									<input type="text" placeholder="100 " id="lic-opt-4-price" class="form-control">
								</div>
							</div>
							<br />
							<div class="row">
								<label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">
									Commission:</label>
								<div class="col-lg-3">
									<input type="text" placeholder=" " id="lic-opt-4-comm" class="form-control">
								</div>
								<div class="col-lg-1">
									Or
								</div>
								<div class="col-lg-3">
									<input type="text" placeholder=" " id="lic-opt-4-comm-per" class="form-control">
								</div>
								<div class="col-lg-1">
									%
								</div>
								<div class="col-lg-12">
									<div class="form-msg licensing-op-4"></div>
								</div>
								<div class="col-lg-12">
									Total=70+15(AC)+10(ST)+5(PGC)
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-6">
								&nbsp;
							</div>
							<div class="col-lg-6" style="float: right; font-weight: bold;">
								*AC=Arcanemind Charges</br> *St=Services Tax</br> *PGC=Payment Gateway Charges
							</div>
						</div>
						<div class="form-group">
							
						</div>
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10" style="text-align: right;">
								<button class="btn btn-default save" type="submit">
									Save</button>
								<button class="btn btn-default cancel" type="submit">
									Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="import-content-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button"> x</button>
				<h4 class="modal-title">Import Subject</h4>
			</div>
			<!--widget start-->
			<section class="panel">
				<!--<header class="panel-heading tab-bg-dark-navy-blue">
					<ul class="nav nav-tabs nav-justified ">
						<li class="active">
							<a href="#import-existing-course" data-toggle="tab">
								Existing Course
							</a>
						</li>
						<li class="">
							<a href="#import-purchased-course" data-toggle="tab">
								Purchase History
							</a>
						</li>
						<li class="">
							<a href="#Content_Market_Place" data-toggle="tab">
								Content Market Place
							</a>
						</li>
					</ul>
				</header>-->
				<div class="panel-body">
					<div class="tab-content tasi-tab">
						<div class="tab-pane active" id="import-existing-course">
							<article class="media">                                              
								<div class="modal-body">
									<div class="form-msg existing-subject-import-form"></div>
									<div style="margin-left: 0px; margin-right: 0px;" class="row">
										<div class="col-xs-4">
											<h4><strong>Course</strong></h4>
										</div>
											<div class="col-xs-4"><h4><strong>Expiry Date</strong></h4></div>
											<div class="col-xs-4"><h4><strong>Licenced</strong></h4></div>
										
									</div>
									<div class="dyn-data">
										
									</div>
									
									<div style="text-align:right;" class="row">
										<button type="submit" class="btn btn-danger btn-sm import">Import</button>
										<button aria-hidden="true" data-dismiss="modal" type="button" class="btn btn-info btn-sm custom-cancel">Cancel</button>
									</div>                                                               
								</div>
							</article>  
						</div>
						<div class="tab-pane" id="import-purchased-course">
							<article class="media">                                              
								<div class="modal-body">
									<div class="form-msg purchased-subject-import-form"></div>
									<div style="margin-left: 0px; margin-right: 0px;" class="row">
										<div class="col-xs-4">
											<h4><strong>Course</strong></h4>
										</div>
											<div class="col-xs-4"><h4><strong>Expiry Date</strong></h4></div>
											<div class="col-xs-4"><h4><strong>Licensed</strong></h4></div>
										
									</div>
									<div class="dyn-data">
										
									</div>
									
									<div style="text-align:right;" class="row">
										<button type="submit" class="btn btn-danger btn-sm import">Import</button>
										<button aria-hidden="true" data-dismiss="modal" type="button" class="btn btn-info btn-sm custom-cancel">Cancel</button>
									</div>                                                               
								</div>
							</article>  
						</div>
						<div class="tab-pane" id="Content_Market_Place">
							<article class="media">                                              
								<div class="modal-body">
									<div>
										<a class="btn btn-primary" href="../courseDetailLicense.php">Go to Content Market Place</a>
									</div>
								</div>
							</article>  
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
	id="subject-professor-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">
					x</button>
				<h4 class="modal-title">
					Manage Professor</h4>
			</div>
			<div class="modal-body">
				<div role="form">
				<div class="form-group">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-10">
							<label class="control-label">
								Available Professor
							</label>
							<br/>
							<br/>
							<select multiple="multiple" class="multi-select multiple" id="subject-professors" name="my_multi_select1[]">
								
							</select>

						</div>
					</div>
				</div>
				
				<div class="row clearfix" style="text-align: right;">
					<button class="btn btn-success pull-left" type="submit">
						Invite Professors
					</button>
					<button class="btn btn-info" type="submit" id="update-subject-professors">
						Ok
					</button>
					<button class="btn btn-default" type="button" data-dismiss="modal" aria-hidden="true">
						Cancel
					</button>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
id="market-place-modal" class="modal fade">
<div class="modal-dialog custom_modal">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				x</button>
			<h4 class="modal-title">Market Place </h4>
		</div>
		<div class="custom_padding">
			<div class="row">
				<div class="col-lg-6 student-marketplace-options">
					<h3>Student Market Place</h3>
					<form class="form-horizontal" role="form">
						<div class="form-group">
						<div class="col-lg-12">
							<div class="checkbox">
								<label>
									<input type="checkbox" id="availStudentMarket">
									Show course on the Student market place
								</label>
							</div>
						</div>
					</div>
					<div class="license-box">
						<div class="license-container">
							<div class="form-group">
								<label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">
									Price in <i class="fa fa-rupee"></i>:</label>
								<div class="col-lg-10">
									<input type="text" class="form-control price-inr" id="student-price-inr" placeholder="INR " />
								</div>                                
							</div>
							<div class="form-group">
								<label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">
									Price in <i class="fa fa-dollar"></i>:</label>                                
								<div class="col-lg-10">
									<input type="text" class="form-control price-usd" id="student-price-dollar" placeholder=" USD">
								</div>
							</div>
							<div class="form-group price-breakdown hidden-l">
								<label for="inputEmail1" class="col-lg-9 col-sm-9 control-label">
								Your Amount <span class="your-price">100 INR, 10 $</span></label>
								<div class="col-lg-3">
									<a href="#" data-details-shown="0" class="show-price-breakdown">View Details</a>
								</div>
								
							</div>
							<div class="price-breakdown-details hidden-l">
								<div>
									<h2>Price INR</h2>
									<div><span>Arcane mind : </span><span>0.00 INR</span></div>
									<div><span>SC : </span><span>0.00 INR</span></div>
									<div><span>VAT : </span><span>0.00 INR</span></div>
									<div><span>TC : </span><span>0.00 INR</span></div>
									<div><span>entertain : </span><span>0.00 INR</span></div>
								</div>
								<div>
									<h2>Price USD</h2>
									<div><span>Arcane mind : </span><span>0.00$</span></div>
									<div><span>SC : </span><span>0.00$</span></div>
									<div><span>VAT : </span><span>0.00$</span></div>
									<div><span>TC : </span><span>0.00$</span></div>
									<div><span>entertain : </span><span>0.00$</span></div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group col-lg-12">
						<a id="save-rate-student" href="#" class="btn btn-xs btn-success ">Save Rates </a>
					</div>     
					<div class="form-group col-lg-12">
						<a id="live-course-student" href="#" class="btn btn-xs btn-success" DISABLED><i class="fa fa-play"></i> Go Live </a>
						<span><span class="text-danger">Course is Live </span><a id="unlive-course-student" href="#" class="btn btn-xs btn-danger "><i class="fa fa-stop"></i> Stop </a></span>
					</div>
					<div class="col-lg-12">
						<span style="display: none;color: red;" id="serror"></span>
					</div>
				  </form>
					</div>
				<!--<div class="col-lg-6 content-marketplace-options">
						<h3>Content Market Place</h3>
					<form class="form-horizontal" role="form">
						<div class="form-group">
						<div class="col-lg-12">
							<div class="checkbox">
								<label>
                                                                    <input type="checkbox" id="availContentMarket">
									Show content on the Content market place
								</label>
							</div>
						</div>
					</div>
					<div id="licenses">						
					</div>
					<div class="form-group col-lg-12">
						<a id="liveForContentMarket" href="#" class="btn btn-xs btn-success" DISABLED><i class="fa fa-play"></i> Go Live </a>
						<span><span class="text-danger">Course is Live </span><a id="unliveForContentMarket" href="#" class="btn btn-xs btn-danger"><i class="fa fa-stop"></i> Stop</a></span>
					</div>
					<div class="col-lg-12">
						<span style="display: none;color: red;" id="cerror"></span>
					</div>
				</form>
				</div>-->
			</div>
			</div>  
	</div>
</div>
</div>
