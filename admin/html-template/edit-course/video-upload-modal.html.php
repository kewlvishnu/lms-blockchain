<div aria-hidden="true" aria-labelledby="upload-image-modal" role="dialog" tabindex="-1"
	id="video-upload-modal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h4 class="modal-title">
					Upload Course Demo Video
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="flow hidden-l">
							<div class="color-light no-background player">
								<video>
									<source type="video/mp4" src=""></source>
								</video>
							</div>
						</div>
						<div class="stuff-file-uploader-body" style="margin-top: 25px;">
							<div class="upload-options">
								<label class="stuff-msg" style="margin-right: 10px;">Please use mp4 and mov files only. The maximum file size can be 1 GB</label>
								<button class="btn btn-info btn-sm upload-file">Upload <span class="file-type">Video</span> File</button>
								<button class="btn btn-info btn-sm delete-file hidden-l">Delete Video</button>
								<form method="post" enctype="multipart/form-data" action="../api/files1.php">
									<input type="file" class="hidden inputFile" name="demo-course-video">
									<input type="hidden" name="courseId" value="0" class="courseId">
									<input type="hidden" name="demoFlag" value="0" class="hiddenDemoFlag">
								</form>
							</div>
							<div class="upload-working hidden-l">
								<div class="progress">
									<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
										<span class="sr-only">45% Complete</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>