<div id="course-details" class="clearfix">
    <aside class="col-md-9">
        <section>
            <div class="custom_img">
                <img src="../img/about_1.jpg" id="course-image-preview"/>
            </div>
            <header class="custom-edit-text" style="padding-left: 0px;">
                <h3><span id="course-name"></span>
                 <!--   <button class="btn btn-info btn-xs edit-button" style="float: right; border:none;  margin-top: 0px;"><i class="fa fa-pencil"></i> Course</button>-->
                </h3>
            </header>
            <h6 id="course-subtitle"></h6>
            <span class="custom-sub">Category:</span>
            <span id="course-categ"></span>
            <br />
            <span class="custom-sub">Target Audience</span>
            <p id="target-audience"></p>
            <br />
            <span class="custom-sub">Course Introduction</span>
            <p id="course-description"></p>
        </section>
        <section class="panel" id="existing-subjects">
            <header class="panel-heading">
                <h4>Existing Subject </h4>
                <!--
               <div class="btn-group pull-right">
                  <button type="button" class="btn btn-info btn-xs  dropdown-toggle" data-toggle="dropdown">Add Subject <span class="caret"></span></button>
                   <ul class="dropdown-menu" role="menu">
                       <li><a href="add-subject.php" class="add-subject-link">Create New Subject</a></li>
                       <li><a href="#import-content-modal" data-toggle="modal">Import Subject</a></li>                                      
                   </ul>
               </div>
                -->
            </header>
            <table class="table table-hover" >
                <thead>
                    <tr>
                        <th>Subject Name</th>
                        <th></th>
                        <th>Subject Id</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </section>
        <!--
        <section class="panel">
            <header class="panel-heading">
                Student
            </header>
            <div class="panel-body">
                <div class="row" style="padding: 0px 5px;">
                    <a href="#modal_invite_student" data-toggle="modal" class="btn btn-xs btn-success">Invite Student </a>
                    <a href="#" data-toggle="modal" class="btn btn-xs btn-warning">Assign Student </a>
                    <a href="#" data-toggle="modal" class="btn btn-xs btn-danger">Purchase Course Key </a>
                    <span id="remaining_keys_total" class="badge bg-important pull-right">0</span> 
                    <span class="pull-right">Remaining Account:</span>

                </div>
            </div>
        </section>
        -->
        <section class="panel">
            <header class="panel-heading">
                <h4>Existing Student</h4>
            </header>
            <table class="table table-striped" id="tbl_students_details_of_course">
                <thead>
                    <tr>
                        <th>Student ID</th>
                        <th>Mob.</th>
                        <th>Name</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </section>
    </aside>
    <aside class="profile-nav col-md-3">
        <section class="panel">
            <ul class="nav nav-pills nav-stacked">
                <li><a><i class="fa fa-book"></i>Course Id <span id="course-id" class="label label-primary pull-right r-activity">C1234</span></a></li>
                <li><a><i class="fa fa-calendar"></i>Live Date <span id="live-date" class="label label-success pull-right r-activity">15-04-2014</span></a></li>
                <li><a><i class="fa fa-calendar"></i>End Date <span id="end-date" class="label label-danger pull-right r-activity">15-05-2015</span></a></li>
            </ul>
        </section>
        <!--
        <section class="panel">
            <div class="flat-carousal">
                <span>Student</span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6"><a class="btn btn-xs btn-success" data-toggle="modal" href="#modal_invite_student">Invite Student </a></div>
                    <div class="col-md-6"><a class="btn btn-xs btn-warning" data-toggle="modal" href="#">Assign Student </a></div>
                </div>
            </div>
        </section>
        <section class="panel">
            <div class="flat-carousal">
                <span>Request to sell</span>
            </div>
            <div class="panel-body">
                <button type="submit" class="btn btn-success btn-xs request-approval-link hidden-l">Send Request</button>
                <a data-toggle="modal" href="#market-place-modal" class="hidden-l market-place-link"><button type="submit" class="btn btn-success btn-xs">Market Place</button></a>
            </div>
        </section>
        -->
        <section class="panel hidden-l" id="avail-student">
            <div class="flat-carousal">
                <span>Available for student</span><a class="btn btn-xs btn-success pull-right" data-toggle="modal" href="#">Go Live</a>
            </div>
            <footer class="follower-foot custom-follower-foot">
                <ul>
                    <li>
                        <h5><i class=" fa fa-inr"></i></h5>
                        <p id="student-price"></p>
                    </li>
                    <li>
                        <h5><i class=" fa fa-usd"></i></h5>
                        <p>0</p>
                    </li>
                </ul>
            </footer>
        </section>
        <section class="panel hidden-l" id="avail-professor">
            <div class="flat-carousal">
                <span>Available for Professor</span><a class="btn btn-xs btn-success pull-right" data-toggle="modal" href="#">Go Live</a>
            </div>

            <ul class="nav nav-pills nav-stacked">
                <li><a>One Year License </a></li>
                <li><a>One Time Transfor </a></li>
                <li><a>Selective IP Transfer</a></li>
                <li><a>Commission based</a></li>
            </ul>
        </section>
    </aside>
</div>

<div class="panel hidden-l" id="edit-form">
    <header class="panel-heading">
        Edit Course
        <button class="btn btn-primary btn-xs cancel-edit-button" style="float: right; border-color: rgb(255, 255, 255); background-color: rgb(10, 49, 81); margin-top: 0px;"><i class="fa fa-times"></i></button>
    </header>
    <div class="panel-body">
        <div role="form">
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-12">
                        <label for="coursename">Course Name</label>
                        <input type="text" placeholder="" id="course-name" "course-name" class="form-control">
                        <div class="form-msg course-name"></div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-12">
                        <label for="coursename">Course Subtitle</label>
                        <input type="text" placeholder="Course Subtitle" id="course-subtitle" class="form-control">
                        <div class="form-msg course-subtitle"></div>
                    </div>                                      
                </div>
            </div>
            <div class="form-group">
                <label for="coursedescription">Course Description</label>
                <textarea class="form-control" rows="5" cols="60" id="course-description"></textarea>
                <div class="form-msg course-description"></div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6">
                        <label for="coursename">Course Live Date</label>
                        <input type="text" placeholder="10-06-2014" id="live-date" class="form-control">
                        <div class="form-msg live-date"></div>
                    </div>
                    <div class="col-lg-6">
                        <label for="coursename">Set End Date</label>
                        <select class="form-control m-bot15 custom-form" id="set-end-date">
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select> 
                    </div>
                </div>
            </div>
            <div class="form-group hidden-on-load">
                <div class="row">
                    <div class="col-lg-6">
                        <label for="courseid">Course End Date</label>
                        <input type="text" placeholder="10-06-2014" id="end-date" class="form-control" />
                        <div class="form-msg end-date"></div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6">
                        <form id="course-image-form"  method="post" enctype="multipart/form-data">
                            <label for="exampleInputFile">Course Images</label>
                            <input type="file" id="course-image" name="course-image" />
                            <img src id="course-image-preview" style="max-width:100%;height:auto;" class="crop"/>
                            <input type="hidden" id="x" name="x" />
                            <input type="hidden" id="y" name="y" />
                            <input type="hidden" id="w" name="w" />
                            <input type="hidden" id="h" name="h" />
                        </form>
                    </div>
                    <div class="col-lg-6">
                        <label for="courseid">Course Category</label>
                        <select id="course-cat" class="form-control" multiple>
                            <option value="1">Entrance Exams</option>
                            <option value="2">School & College Prep</option>
                            <option value="3">Hobbies & Skills</option>
                            <option value="4">Business & Management</option>
                            <option value="5">IT & Software</option>
                            <option value="6">Entrepreneurship</option>
                            <option value="7">Miscellaneous</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <p class="text-right">
            <button class="btn btn-primary btn-xs cancel-edit-button"><i class="fa fa-times"></i> Cancel</button>
            <button class="btn btn-primary btn-xs save-button"><i class="fa fa-save"></i> Save</button>
    </div>
</div>