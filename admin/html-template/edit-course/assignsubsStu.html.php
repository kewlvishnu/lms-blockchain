<div class="row">
	<div class="col-lg-12 ">
		<section class="panel">
			<div class="revenue-head">
				<h3>Assign Subjects to students</h3>
			</div>
			<div class="panel-body">
				<table class="table table-hover table-bordered table-striped" id="students-subject" >
					<thead></thead>
					<tbody></tbody>
				</table>
				<div class ="padTop100">
					<button class="btn btn-lg btn-success pull-right" id="save-student-subject">Save</button>
				</div>
			</div>
		</section>
	</div>
</div>
	<!--</div>
</section>-->