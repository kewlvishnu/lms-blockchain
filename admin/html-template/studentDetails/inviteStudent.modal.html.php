<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
     id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                    <i class="fa fa-times"></i></button>
                <h4 class="modal-title">
                Invite For Course
				</h4>
            </div>		
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label class="control-label">
                            Select Course
                        </label>
                        <div class="row">
                            <div class="col-lg-6">
                                <select id="combo_courses" class="form-control m-bot15">
                                    <option value="0">Select Course</option>
                                   
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            Student Email IDs
                        </label>
                        <div class="row">
							<div class="col-lg-12">
								<textarea id="textarea_email_id" placeholder="Enter Students email separated by comma(,)" style="width:100%;height:100px;" class="form-control "></textarea>
							</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <p class="text-right"><a id="send_activation_link" class="btn btn-info btn-sm">Invite</a> <a id="cancel-invitation" class="btn btn-default btn-sm cancel">Cancel</a></p>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>