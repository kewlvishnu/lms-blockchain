<section class="panel">
    <header class="panel-heading">
        <h4>Students via Course Keys</h4>
        <a id="invite_student" href="#myModal" data-toggle="modal" class="btn btn-info btn-xs pull-right">Invite Student</a>
    </header>
    <table id="tbl_studentDetails" class="table table-hover">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email Id</th>
                <th>Contact No</th>
                <th>Student Id</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</section>