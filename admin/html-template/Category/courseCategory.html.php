<section class="panel">
	<header class="panel-heading">Course Category Description table</header>
	<div class="panel-body">
		<div class="adv-table" style="overflow-x: scroll;">
			<table id="courseCatDes"  class="display table table-bordered table-striped" >
				<thead>
					<tr>
						<th>Sr.No.</th>  
						<th>Category Id</th> 
						<th>Category Name</th> 
						<th>Category description</th> 
						<th>Action</th> 
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</section>