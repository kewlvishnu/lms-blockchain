<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
     id="editcatDesc" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Edit Category Description</h4>
			</div>
			<div class="modal-body padRgt35">
				<form role="form">
					<div class="row">
						<div class="form-group">
							<label for="inputEmail1" class="col-md-3 control-label">Category Name</label>
							<div class="col-md-7">
								<label for="inputEmail1" id = "catName" class=" control-label"></label>
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail1" class="col-md-3 control-label">Category Description</label>
							<div class="col-md-9">
								<textarea id="catDesc" rows="5" name="Enter Portfolio Description" class="form-control catDesc1"></textarea>
								<span class="help-block"></span>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary editCatdescC" name="coursediscount1" id="editCatdesc">Save changes</button>
			</div>
		</div>
	</div>
</div>