<!--Modal for image upload-->
<div aria-hidden="true" aria-labelledby="upload-image-modal" role="dialog" tabindex="-1"
	id="uploadPortfolioImageModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				<h4 class="modal-title">
					Upload Portfolio Image
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<form id="frmPortfolioImage"  method="post" enctype="multipart/form-data">
							<div class="error"></div>
							<input type="file" id="imgPortfolio" name="filePortfolioImage" />
							<img id="uploadPortfolioPic" class="hide" src="#" alt="Your image" />
							<input type="hidden" id="x" name="x" />
							<input type="hidden" id="y" name="y" />
							<input type="hidden" id="w" name="w" />
							<input type="hidden" id="h" name="h" />
							<!-- <div class="progress" style="display:none">Saving....</div>
							<div class="msg"></div> -->
							<input type="submit" value="Save!" class="btn btn-primary hide" />
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>