<section class="panel">
	<header class="panel-heading">
		Edit Template
	</header>
	<div class="panel-body">
		<form role="form">
			<div class="form-group">
				<div class="row">
					<div class="col-lg-6">
						<label for="coursename">Title of the Template</label>
						<input type="text" placeholder="Title" id="titleName" class="form-control">
					</div>
					<div class="col-lg-6">
					</div>
				</div>
			</div>
			<div id="extraFields">
				<div class="form-group">
					<div class="row">
						<div class="col-lg-6">
							<label for="coursename">Select Test Type</label><br>
							<label>
								<input type="radio" name="optionsRadios1" id="assignmentCat" value="option1">Assignment
							</label>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<label>
								<input type="radio" name="optionsRadios1" id="examCat" value="option1">Exam
							</label>
						</div>
						<div class="col-lg-6">
							<label for="courseid">No Of Attempts</label>
							<input type="text" placeholder="Total attempts allowed" id="noOfAttempts" class="form-control">
						</div>
					</div>
				</div>
				<div id="sections">
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-9 col-sm-9">
							<input type="text" placeholder="Enter new section name" id="tempSection" class="form-control">
						</div>
						<div class="col-md-3 col-sm-3">
							<a href="#" id="addSection"> Add Section </a>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-lg-6">
							<label for="coursename"> Allow to Switch  Between Sections</label><br>
							<label>
								<input type="radio" value="option1" id="switchYes" name="optionsRadios2">Yes
							</label>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<label>
								<input type="radio" value="option1" id="switchNo" name="optionsRadios2">No
							</label>
						</div>
						<div class="col-lg-6" id="totalTime"  style="display: none;">
							<label for="coursename">Total Time</label><br>
							<div class="col-md-4 col-sm-4">
								<div class="input-group input-small">
									<input type="text" maxlength="3" class="spinner-input form-control" value="0h" disabled=true id="totalTimeHour">
									<div class="spinner-buttons input-group-btn btn-group-vertical">
										<button type="button" class="btn spinner-up btn-xs btn-default">
											<i class="fa fa-angle-up"></i>
										</button>
										<button type="button" class="btn spinner-down btn-xs btn-default">
											<i class="fa fa-angle-down"></i>
										</button>
									</div>
								</div>
							</div> &nbsp;&nbsp;&nbsp;&nbsp;
							<div class="col-md-4 col-sm-4">
								<div class="input-group input-small">
									<input type="text" class="spinner-input form-control" maxlength="3" value="0m" disabled=true id="totalTimeMinute">
									<div class="spinner-buttons input-group-btn btn-group-vertical">
										<button class="btn spinner-up btn-xs btn-default" type="button">
											<i class="fa fa-angle-up"></i>
										</button>
										<button class="btn spinner-down btn-xs btn-default" type="button">
											<i class="fa fa-angle-down"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" id="sectionTimes" style="display: none;">
					<div class="row">
					</div>
					<div class="row">
						<div class="col-lg-6">
							<label for="coursename">Gap Between sections</label><br>
							<div class="col-md-4">
								<div class="input-group input-small">
									<input type="text" class="spinner-input form-control" maxlength="3" value="0m" disabled=true id="gapTime">
									<div class="spinner-buttons input-group-btn btn-group-vertical">
										<button class="btn spinner-up btn-xs btn-default" type="button">
											<i class="fa fa-angle-up"></i>
										</button>
										<button class="btn spinner-down btn-xs btn-default" type="button">
											<i class="fa fa-angle-down"></i>
										</button>
									</div>
								</div>
							</div> &nbsp;&nbsp;&nbsp;&nbsp;
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-lg-5 col-sm-5">
						<label for="coursename">End Test Before Time</label><br>
						<label>
							<input type="radio" name="optionsRadios3" id="endYes">Yes
						</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<label>
							<input type="radio" name="optionsRadios3" id="endNo">No
						</label>
					</div>
					<div class="col-lg-7 col-sm-7">
						<label for="coursename">If power  goes off / Browser Closes  ( Answers are  not lost )</label><br>
						<label>
							<input type="radio" name="optionsRadios4" id="noTimeLost">No time lost
						</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<label>
							<input type="radio" name="optionsRadios4" id="timeLost">Time Continues
						</label>
					</div>
					<div class="col-lg-12">
						<label for="coursename">Section ordering</label><br>
						<label>
							<input type="radio" name="optionsRadios5" id="orderStart" value="option1">
							Fixed
						</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<label>
							<input type="radio" name="optionsRadios5" id="orderRandom" value="option1">
							Random
						</label>
						<div class="row">
							<div class="col-lg-12">
								<ul id="sortOrder" style="display: none;">
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row panel-body" style="text-align: right;">
				<div class="col-lg-12">
					<button class="btn btn-primary btn-sm save-button" type="button"><i class="fa fa-save"></i> Save</button>
					<a href="template.php" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Cancel</a>
				</div>
			</div>
		</form>
	</div>
</section>