<div class="row">
	<div class="col-lg-12">
		<!--breadcrumbs start -->
		<ul class="breadcrumb">
			<li><a href="profile.php"><i class="fa fa-home"></i>Dashboard</a></li>
			<li><a href="assignment-exam.php">Add New Assignment/Exam</a></li>			
			<li><strong>View Templates</strong></li>
		</ul>
		<!--breadcrumbs end -->
	</div>
</div>
<section class="panel">
	<header class="panel-heading">
		<h4>View Template</h4>
		<div class="pull-right">
			<a href="add-template.php">
				<button class="btn btn-info btn-xs" type="button"><i class="fa fa-plus"></i> Add New Template</button>
			</a>
		</div>
	</header>
	<div style="overflow: auto;">
	<table class="table table-hover" id="viewTemplates">
		<thead>
			<tr>
				<th>Title</th>
				<th>Type</th>
				<th>Status</th>
				<th>Last Modified</th>
				<th>No of test</th>
				<th>-</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	</div>
</section>