<!--sidebar start-->
<?php
	@session_start();
	/*$subdomain = FALSE;
	if(isset($_GET["pageDetailId"]) && !empty($_GET["pageDetailId"])) {
		$subdomain = TRUE;
	}*/
	$menuCourses = '<li>
			<a href="courses.php">
			<i class="fa fa-copy" class="course"></i>
			<span class="course">'.(($_SESSION['userRole'] != 2)?'Courses':'My own Courses').'</span></a>
			<ul class="sub" style="display: none;">
				<li style="padding-left: 5px;">
					<div class="custom-side-tree">
						<div id="Div1" class="tree tree-plus-minus tree-solid-line tree-unselectable">
							<div class="tree-folder" style="display: none;">
								<div class="tree-folder-header">
									<i class="fa fa-folder"></i>
									<div class="tree-folder-name"></div>
								</div>
								<div class="tree-folder-content"></div>
								<div class="tree-loader" style="display: none;"></div>
							</div>
							<div class="tree-item" style="display: none;">
								<div class="tree-item-name"></div>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</li>';
	if (!$subdomain) {
?>
<aside>
  <div class="nav-collapse collapse" id="sidebar" tabindex="5000">
	  <!-- sidebar menu start-->
		<ul id="nav-accordion" class="sidebar-menu">
			<li>
			  <a href="dashboard.php" class="dashboard">
				  <i class="fa fa-dashboard"></i>
				  <span>Dashboard</span>
			  </a>
			</li>
		<?php echo $menuCourses; ?>
		<?php
			if($_SESSION['userRole'] == 2) {
		?>
			<li>
				<a href="subject-invitation.php" class="invitation">
				<i class="fa fa-copy"></i>
				<span>Invitations</span></a>				
			</li>
			<li>
				<a href="subject-assigned.php" class="assignedSubjects">
					<i class="fa fa-list"></i>
					<span>Subjects Assigned</span>
				</a>
			</li>
		<?php
		}
		?>
		<li>
			<a href="assignment-exam.php" class="exams">
				<i class="fa fa-tags"></i>
				<span>Assignment/Exam</span>
			</a>
		</li>
		<li>
			<a href="chapter-content.php" class="chapters">
				<i class="fa fa-indent"></i>
				<span>Content</span>
			</a>
		</li>
		<li>
			<a href="packages_subs.php" class="packages">
				<i class="fa fa-copy"></i>
				<span>Packages</span>
			</a>
		</li>
		<?php
			if($_SESSION['userRole'] == 1) {
		?>
		<li>
			<a href="professors.php" class="professors">
				<i class="fa fa-user"></i>
				<span>Professors/Tutor</span>
			</a>
		</li>
		<?php
			}
		?>
		<?php
			if($_SESSION['userRole'] != 3) {
		?>
		<li>
			<a href="students.php">
				<i class="fa fa-users"></i>
				<span  class="students">Students</span>
				<span class="dcjq-icon"></span>
			</a>
			<ul class="sub">
				<li><a href="invited_students_history.php">Invitation Status</a></li>
				<li><a href="studentsList.php">Students List</a></li>
				<li><a href="studentsTransfer.php">Transfer Students</a></li>
			</ul>
		</li>
		<?php
			}
		?>
		
		<li>
			<a href="discount.php" class="coup_discount">
				<i class="fa fa-shopping-cart"></i>
				<span>Discounts & Coupons</span>
				<span class="dcjq-icon"></span>
			</a>
			<ul class="sub">
				<li><a href="discount.php">Discounts</a></li>
				<li><a href="coupons.php">Coupons</a></li>
				<li><a href="couponsHistory.php">Coupons History</a></li>
			</ul>
		</li>
		<li>
			<a href="CourseKey.php" class="history">
				<i class="fa fa-list"></i>
				<span>History</span>
			</a>
		</li>
		<li>
			<a href="profile.php" class="profile">
				<i class="fa fa-picture-o"></i>
				<span>Profile</span>
			</a>
		</li>
		<?php
			if($_SESSION['userRole'] == 1 || $_SESSION['userRole'] == 2) {
				if((isset($portfolioDetails) && !empty($portfolioDetails) && $portfolioDetails->owner == 1) || (!isset($portfolioDetails) && empty($portfolioDetails))) {
		?>
		<li>
			<a href="portfolio.php">
				<i class="fa fa-users"></i>
				<span  class="students">Portfolio</span>
			</a>
		</li>
		<?php
				}
			}
		?>
	  </ul>
	  <!-- sidebar menu end-->
  </div>
</aside>
<?php
	} else {
?>
<aside>
  <div class="nav-collapse " id="sidebar" style="overflow: hidden;" tabindex="5000">
	  <!-- sidebar menu start-->
		<ul id="nav-accordion" class="sidebar-menu">
			<li>
			  <a href="dashboard.php" class="dashboard">
				  <i class="fa fa-dashboard"></i>
				  <span>Dashboard</span>
			  </a>
			</li>
		<?php
			if($_SESSION['userRole'] == 2) {
		?>
			<li>
				<a href="subject-invitation.php" class="invitation">
				<i class="fa fa-copy"></i>
				<span>Invitations</span></a>				
			</li>
			<li>
				<a href="subject-assigned.php" class="assignedSubjects">
					<i class="fa fa-list"></i>
					<span>Subjects Assigned</span>
				</a>
			</li>
		<?php
			} else {
				echo $menuCourses;
			}
		?>
		<li>
			<a href="assignment-exam.php" class="exams">
				<i class="fa fa-tags"></i>
				<span>Assignment/Exam</span>
			</a>
		</li>
		<li>
			<a href="chapter-content.php" class="chapters">
				<i class="fa fa-indent"></i>
				<span>Content</span>
			</a>
		</li>
		<?php
			if($_SESSION['userRole'] == 1) {
		?>
		<li>
			<a href="professors.php" class="professors">
				<i class="fa fa-user"></i>
				<span>Professors/Tutor</span>
			</a>
		</li>
		<?php
			}
			if($_SESSION['userRole'] == 2) {
				echo $menuCourses;
			}
		?>
		<li>
			<a href="profile.php" class="profile">
				<i class="fa fa-picture-o"></i>
				<span>Profile</span>
			</a>
		</li>
		<?php
			if($_SESSION['userRole'] == 1 || $_SESSION['userRole'] == 2) {
				if((isset($portfolioDetails) && !empty($portfolioDetails) && $portfolioDetails->owner == 1) || (!isset($portfolioDetails) && empty($portfolioDetails))) {
		?>
		<li>
			<a href="portfolio.php">
				<i class="fa fa-users"></i>
				<span  class="students">Portfolio</span>
			</a>
		</li>
		<?php
				}
			}
		?>
	  </ul>
	  <!-- sidebar menu end-->
  </div>
</aside>
<?php
	}
?>
<!--sidebar end-->