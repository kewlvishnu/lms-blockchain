<!-- js placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="assets/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
<script src="js/bootstrap.js"></script>
<script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="js/respond.min.js" ></script>
<script src="assets/crop_upload/crop_upload/js/crop_upload.js"></script>
<script src="assets/jcrop/js/jquery.Jcrop.js"></script>
<script src="assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/jquery-form/jquery.form.js"></script>
<script src="assets/fuelux/js/tree.min.js"></script>
<script src="assets/switchButton/jquery.switchButton.js"></script>
<script src="js/custom/treeLoader.js"></script>
<script type="text/javascript" src="assets/bootstrap-multiselect/js/bootstrap-multiselect.js"></script>
<!--<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>-->
<!--common script for all pages-->
<script src="js/common-scripts.js"></script>