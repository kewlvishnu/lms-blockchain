<!--sidebar start-->
<aside>
	<div class="nav-collapse collapse" id="sidebar" tabindex="5000">
		<!-- sidebar menu start-->
			<ul id="nav-accordion" class="sidebar-menu">
				<li>
					<!--<a href="profile.php">-->
					<a href="#">
						<i class="fa fa-dashboard"></i>
						<span>Dashboard</span>
					</a>
				</li>
			<li>
				<a href="all_institutes_info.php">
					<i class="fa fa-indent"></i>
					<span>Institutes</span>
				</a>
			</li>
			<li>
				<a href="course-key.php">
					<i class="fa fa-user"></i>
					<span>Course Key</span>
				</a>
			</li>
			<li>
				<a href="freetier.php">
					<i class="fa fa-user"></i>
					<span>Free Tier</span>
				</a>
			</li>
			<li>
				<a href="admin_packages.php">
					<i class="fa fa-user"></i>
					<span>Packages</span>
				</a>
			</li>
			<li>
				<a href="admin_courseDiscounts.php">
					<i class="fa fa-shopping-cart"></i>
					<span>Discounts &amp; Coupons</span>
				</a>
			</li>
			<li>
				<a href="allApprovedCourses.php">
					<i class="fa fa-list"></i>
					<span>Approved Courses</span>
				</a>
			</li>
			<li>
				<a href="backendImportSubjects.php">
					<i class="fa fa-list"></i>
					<span>Import Subjects</span>
				</a>
			</li>
			<li>
				<a href="approvals.php">
					<i class="fa fa-list"></i>
					<span>Course Approve</span>
				</a>
			</li>
			<li>
				<a href="allInstitutesLoginInfo.php">
					<i class="fa fa-list"></i>
					<span>Login Details</span>
				</a>
			</li>
			<li>
				<a href="courseAnalyticsExcel.php">
					<i class="fa fa-list"></i>
					<span>Course Analytics Excel</span>
				</a>
			</li>
			<li>
				<a href="edit-taxes.php">
					<i class="fa fa-list"></i>
					<span>Taxes</span>
				</a>
			</li>
			<li>
				<a href="portfolioAdmin.php">
					<i class="fa fa-list"></i>
					<span>Portfolio</span>
				</a>
			</li>
			<li>
				<a href="CourseCategory.php">
					<i class="fa fa-list"></i>
					<span>Course Category</span>
				</a>
			</li>
			<li>
				<a href="course-chat.php">
					<i class="fa fa-list"></i>
					<span>Course Chat</span>
				</a>
			</li>
			<li>
				<a href="#">
					<i class="fa fa-list"></i>
					<span>Trash </span>
				</a>
				<ul class="sub">
					<li><a href="deletedCourse.php">Courses</a></li>
					<li><a href="deletedSubject.php">Subjects</a></li>
					<li><a href="deletedChapter.php">Chapters</a></li>
					<li><a href="deletedAssignment.php">Assignments</a></li>
				</ul>
			</li>
		</ul>
		<!-- sidebar menu end-->
	</div>
</aside>
<!--sidebar end-->