<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Mosaddek">
<meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
<link rel="icon" href="<?php echo $favicon; ?>" type="image/x-icon">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/bootstrap-reset.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="assets/jquery-multi-select/css/multi-select.css" />
<!--external css-->
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="assets/fuelux/css/tree-style.css" type="text/css" rel="stylesheet"/>
<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet" />
<link href="css/style-responsive.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/jcrop/css/jquery.Jcrop.css" type="text/css" />
<link href="css/custom.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="assets/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" type="text/css">
<link rel="stylesheet" href="assets/switchButton/jquery.switchButton.css" type="text/css">
<link rel="stylesheet" href="assets/bootstrap-multiselect/css/bootstrap-multiselect.css" type="text/css">


<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->

<!-- analytics code -->
<?php @include_once "../../analytics.php"; ?>