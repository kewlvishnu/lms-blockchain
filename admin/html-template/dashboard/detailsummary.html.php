<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
     id="detailsummary" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" color: #fff; class="close datatablecl" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <h4 class="modal-title">
                   Summary Of Students Visited Course in last seven days </h4>
            </div>
    		<div class="modal-body" style="padding-right: 35px;">
    		<section class="panel">

	<div style="overflow: auto;">
    <table class="display table table-bordered table-striped" id="all-visitedStudent">
        <thead>
            <tr>
                <th>Student Name</th>
                <th>Visited  </th>                        
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
	</div>
</section>
      		</div>
        </div>
    </div>
</div>


