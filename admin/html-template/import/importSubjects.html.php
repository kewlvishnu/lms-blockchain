<section class="panel">
    <header class="panel-heading">
        <h4>Courses Table</h4>
       <!-- <a href="addNewCourse.html"><button class="btn btn-info pull-right" type="button"><i class="fa fa-plus"></i> New Course</button></a> -->
    </header>
    <div id="step1">
        <div class="pd20">
            <h2>Select course to import from :</h2>
            <table class="display table table-bordered table-striped table-courses" id="step1-courses">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Courses Name</th>
                        <th>Institute id</th>
                        <th>Name</th>
                        <th>Email </th>
                        <th>Mobile </th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div id="step2" class="hide">
        <div class="pd20">
            <h2>Select course to import to :</h2>
            <table class="display table table-bordered table-striped table-courses" id="step2-courses">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Courses Name</th>
                        <th>Institute id </th>
                        <th>Name</th>
                        <th>Email </th>
                        <th>Mobile </th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div id="step3" class="hide">
        <div class="pd20">
            <h2>Select subjects to import :</h2>
            <ul class="subjects"></ul>
            <button class="btn btn-warning btn-import">Import subjects</button>
        </div>
    </div>
</section>