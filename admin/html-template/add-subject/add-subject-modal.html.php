<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="subject-created" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x </button>
				<h4 class="modal-title">Create Subject</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<h5>Subject <label id="subject-name"></label> Created Successfully</h5> 
				</div>
				<div class="row">
					<div class="col-md-6">
						<a id="add-chapter" href="#" class="btn btn-info">Add Section to Subject</a>
					</div>
					<div class="col-md-6">
						<a href="#" class="btn btn-info" id="add-subject">Add New Subject</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>