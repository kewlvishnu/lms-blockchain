<!-- <section class="panel">
	  <header class="panel-heading">
		  <h4 id="profile-name"></h4></header>                                                   
</section>-->
<!--<section class="panel">
	  <header class="panel-heading">
		  <h4 id="profile-name"></h4></header>                                                   
</section>-->
<section class="panel">                          
  <header class="panel-heading">
	<label id="course-name"></label>: Add New Subject 
	 <!--<a href="addChapter.html"><button class="btn btn-info pull-right" type="button"><i class="fa fa-plus"></i> New Chapter</button></a> -->
  </header>
  <div class="panel-body">
	  <div role="form">                             
		  <div class="form-group">
			<div class="row">
			  <div class="col-lg-6 col-md-6">
                              <b><i class="text-danger">*</i></b>  
                              <label for="coursename">Subject Name</label>
			  <input type="text" placeholder="" id="subject-name" class="form-control">
			  <div class="form-msg subject-name"></div>
			  </div>
			  <div class="col-lg-6 col-md-6 hidden-on-load">
			  <label for="courseid">Subject Id</label>
			  <input type="text" placeholder="" id="Text5" class="form-control">
			  </div>
			</div>
		  </div>
		  <div class="form-group">
			<div class="row">
			  <div class="col-lg-12">
				  <label for="coursedescription">Subject Description</label>
				  <textarea cols="60" rows="5" class="form-control" id="subject-description"></textarea>
				  <div class="form-msg subject-desc"></div>
			  </div>
			</div>
		  </div>
                <!--<div class="form-group">
			<div class="row">
				<div class="col-lg-6">
				  <form id="subject-image-form"  method="post" enctype="multipart/form-data">
					  <label for="exampleInputFile">Subject Image</label>
					  <input type="file" id="subject-image" name="subject-image" />
					  <img src id="subject-image-preview" style="max-width:100%;height:auto;" class="crop"/>
					  <input type="hidden" id="x" name="x" />
						<input type="hidden" id="y" name="y" />
						<input type="hidden" id="w" name="w" />
						<input type="hidden" id="h" name="h" />
				  </form>
			  </div>
			</div>
		  </div>-->
		  <div class="form-group">
		  <div class="row">
			  <div class="col-lg-6">
			  <button type="submit" class="btn btn-info" id="create-subject">Create Subject</button>
			  </div>
		  </div>
		  </div>
		</div>
  </div>
</section>
<section class="panel" id="existing-subjects">
  <header class="panel-heading">
	  Existing Subject
  </header>
  <table class="table table-hover">
	  <thead>
		  <tr>
			  <th>Subject Name</th>
			  <th></th>
			  <th>Subject Id</th>
			  <th></th>
		  </tr>
	  </thead>
	  <tbody>
	  </tbody>
  </table>
</section>                     