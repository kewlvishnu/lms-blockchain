<div class="row">
	<div class="col-lg-12">
		<!--breadcrumbs start -->
		<ul class="breadcrumb">
			<li><a href="dashboard.php"><i class="fa fa-home"></i>Dashboard</a></li>
			<li><a href="professors.php"><i class="fa fa-user"></i> Existing Professors/Tutors</a></li>
			<li><strong>Invite</strong></li>
		</ul>
		<!--breadcrumbs end -->
	</div>
</div>
<header class="panel-heading">
	Invite Professor/Tutor
</header>
<div class="input-group m-bot15" style="padding-top: 7px;">
	<input type="text" class="form-control input-sm" id="search-professor">
	<div class="input-group-btn" style="width: 18%;">
		<select class="form-control input-sm" id="filter-drpDwn" style="padding: 6px 11px;">
			<option val="1">Name</option>
			<option val="2">Email</option>
			<option val="3">Country</option>
		</select>
		<button class="btn btn-primary" id="search-button">Search</button>
	</div>
</div>