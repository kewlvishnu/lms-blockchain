<section class="panel">
    <header class="panel-heading">
        Institutes
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <table id="tbl_view_institutes"  class="display table table-bordered table-striped" >
                <thead>
                    <tr>
                        <th>Sr.No.</th>  
                        <th>Ins ID</th> 
                        <th>Institute Name</th> 
                        <th>Email</th> 
                        <th>Mobile</th> 
                        <th>Courses</th> 
                        <th>Students</th> 
                        <th>Courseskey</th> 
                        <th>Consumption</th> 
                    </tr>
                </thead>
                <tbody>
                 
                   </tbody>
            </table>
        </div>
    </div>
</section>