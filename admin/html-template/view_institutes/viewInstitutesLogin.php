<section class="panel">
    <header class="panel-heading">
        USERS
    </header>
    <div class="panel-body">
        <div class="adv-table" style="overflow-x: scroll;">
            <table id="tbl_view_institutes"  class="display table table-bordered table-striped" >
                <thead>
                    <tr>
                        <th>Sr.No.</th>  
                        <th>Ins ID</th> 
                        <th>Institute Name</th> 
                        <th>Role</th> 
                        <th>UserName</th> 
                        <th>Email</th> 
                        <th>Mobile</th> 
                        <th>Last login</th> 
                        <th>Status</th> 
                        <th>Action</th> 
                    </tr>
                </thead>
                <tbody>
                 
                   </tbody>
            </table>
        </div>
    </div>
</section>