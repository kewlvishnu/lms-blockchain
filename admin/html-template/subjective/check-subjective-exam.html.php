<div class="row">
	<div class="col-md-12">
		<section class="panel">
			<header class="panel-heading">
				Check Subjective Exam - <strong id="studentName"></strong> - Attempt No <strong id="attemptNo"></strong>
			</header>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="btn-group btn-group-justified form-group" role="group" aria-label="">
							<?php echo '<a href="checkSubjectiveExams.php?examId='.$_GET['examId'].'&subjectId='.$_GET['subjectId'].'&courseId='.$_GET['courseId'].'" class="btn btn-info">Back</a>'; ?>
							<a href="javascript:void(0)" class="btn btn-success" id="btnAttemptChecked">Mark whole exam attempt as checked</a>
						</div>
					</div>
				</div>
				<div id="questionsContainer">
					<!-- <div class="panel-group questions-panel" id="accQuestions" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headQ1">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accQuestions" href="#bodyQ1" aria-expanded="true" aria-controls="bodyQ1">
										Question #1
									</a>
								</h4>
							</div>
							<div id="bodyQ1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headQ1">
								<div class="panel-body">
									<div class="question">What is the capital of Maharashtra?</div>
									<div class="answer">
										<div class="row">
											<div class="col-md-12">
												<label for="">Correct Answer :</label>
												<div>Mumbai</div>
											</div>
										</div>
									</div>
									<div class="answer">
										<div class="row">
											<div class="col-md-6">
												<label for="">Student's Answer :</label>
												<div><iframe src="../user-data/test.pdf" frameborder="0" class="btn-block"></iframe></div>
											</div>
											<div class="col-md-6">
												<label for="">Reviews/Comments :</label>
												<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
												<div class="form-group">
													<button class="btn btn-info">Save Comments</button>
												</div>
											</div>
										</div>
									</div>
									<div class="answer">
										<div class="row">
											<div class="col-md-6">
												<label for="">Student's Answer :</label>
												<div><a href="javascript:void(0)"><img src="../user-data/fb-img.jpg" alt="" class="img-responsive"></a></div>
											</div>
											<div class="col-md-6">
												<label for="">Reviews/Comments :</label>
												<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
												<div class="form-group">
													<button class="btn btn-info">Save Comments</button>
												</div>
											</div>
										</div>
									</div>
									<div class="answer">
										<div class="row">
											<div class="col-md-6">
												<label for="">Student's Answer :</label>
												<div><a href="javascript:void(0)">File 2</a></div>
											</div>
											<div class="col-md-6">
												<label for="">Reviews/Comments :</label>
												<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
												<div class="form-group">
													<button class="btn btn-info">Save Comments</button>
												</div>
											</div>
										</div>
									</div>
									<div class="answer">
										<div class="row">
											<div class="col-md-6">
												<label for="">Student's Answer :</label>
												<div><a href="javascript:void(0)">File 3</a></div>
											</div>
											<div class="col-md-6">
												<label for="">Reviews/Comments :</label>
												<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
												<div class="form-group">
													<button class="btn btn-info">Save Comments</button>
												</div>
											</div>
										</div>
									</div>
									<div class="marks-time">
										<div class="row">
											<div class="col-md-4">
												<div><label for="">Time</label></div>
												<div>Time Taken: <span>10 min</span></div>
												<div>Topper Time: <span>10 min</span></div>
												<div>Average Time: <span>10 min</span></div>
											</div>
											<div class="col-md-4">
												<div><label for="">Marks</label></div>
												<div>Maximum Marks: <span>10</span></div>
												<div>Topper Marks: <span>10</span></div>
												<div>Average Marks: <span>10</span></div>
											</div>
											<div class="col-md-4">
												<label for="">Assign Marks:</label>
	    										<div class="input-group">
	    											<input type="text" class="form-control">
	      											<span class="input-group-btn">
	      												<button class="btn btn-primary">Set</button>
	      											</span>
	    										</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headQ2">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accQuestions" href="#bodyQ2" aria-expanded="false" aria-controls="bodyQ2">
										Question #2
									</a>
								</h4>
							</div>
							<div id="bodyQ2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headQ2">
								<div class="panel-body">
					
									<div class="question">Rapid Fire Round</div>
					
					
					
									<div class="panel-group" id="accQ2Subquestions" role="tablist" aria-multiselectable="true">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headQ2S1">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accQ2Subquestions" href="#bodyQ2S1" aria-expanded="true" aria-controls="bodyQ2S1">
														Subquestion #1
													</a>
												</h4>
											</div>
											<div id="bodyQ2S1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headQ2S1">
												<div class="panel-body">
													<div class="question">What is the capital of Karnataka?</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-12">
																<label for="">Correct Answer :</label>
																<div>Mumbai</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div>Mumbai</div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 1</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 2</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 3</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="marks-time">
														<div class="row">
															<div class="col-md-4">
																<div><label for="">Time</label></div>
																<div>Time Taken: <span>10 min</span></div>
																<div>Topper Time: <span>10 min</span></div>
																<div>Average Time: <span>10 min</span></div>
															</div>
															<div class="col-md-4">
																<div><label for="">Marks</label></div>
																<div>Maximum Marks: <span>10</span></div>
																<div>Topper Marks: <span>10</span></div>
																<div>Average Marks: <span>10</span></div>
															</div>
															<div class="col-md-4">
																<label for="">Assign Marks:</label>
					    										<div class="input-group">
					    											<input type="text" class="form-control">
					      											<span class="input-group-btn">
					      												<button class="btn btn-primary">Set</button>
					      											</span>
					    										</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headQ2S2">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accQ2Subquestions" href="#bodyQ2S2" aria-expanded="false" aria-controls="bodyQ2S2">
														Subquestion #2
													</a>
												</h4>
											</div>
											<div id="bodyQ2S2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headQ2S2">
												<div class="panel-body">
													<div class="question">What is the capital of Madhya Pradesh?</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-12">
																<label for="">Correct Answer :</label>
																<div>Mumbai</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div>Mumbai</div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 1</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 2</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 3</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="marks-time">
														<div class="row">
															<div class="col-md-4">
																<div><label for="">Time</label></div>
																<div>Time Taken: <span>10 min</span></div>
																<div>Topper Time: <span>10 min</span></div>
																<div>Average Time: <span>10 min</span></div>
															</div>
															<div class="col-md-4">
																<div><label for="">Marks</label></div>
																<div>Maximum Marks: <span>10</span></div>
																<div>Topper Marks: <span>10</span></div>
																<div>Average Marks: <span>10</span></div>
															</div>
															<div class="col-md-4">
																<label for="">Assign Marks:</label>
					    										<div class="input-group">
					    											<input type="text" class="form-control">
					      											<span class="input-group-btn">
					      												<button class="btn btn-primary">Set</button>
					      											</span>
					    										</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headQ2S3">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accQ2Subquestions" href="#bodyQ2S3" aria-expanded="false" aria-controls="bodyQ2S3">
														Subquestion #3
													</a>
												</h4>
											</div>
											<div id="bodyQ2S3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headQ2S3">
												<div class="panel-body">
													<div class="question">What is the capital of Madhya Pradesh?</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-12">
																<label for="">Correct Answer :</label>
																<div>Mumbai</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div>Mumbai</div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 1</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 2</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 3</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="marks-time">
														<div class="row">
															<div class="col-md-4">
																<div><label for="">Time</label></div>
																<div>Time Taken: <span>10 min</span></div>
																<div>Topper Time: <span>10 min</span></div>
																<div>Average Time: <span>10 min</span></div>
															</div>
															<div class="col-md-4">
																<div><label for="">Marks</label></div>
																<div>Maximum Marks: <span>10</span></div>
																<div>Topper Marks: <span>10</span></div>
																<div>Average Marks: <span>10</span></div>
															</div>
															<div class="col-md-4">
																<label for="">Assign Marks:</label>
					    										<div class="input-group">
					    											<input type="text" class="form-control">
					      											<span class="input-group-btn">
					      												<button class="btn btn-primary">Set</button>
					      											</span>
					    										</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
					
					
					
					
					
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headQ3">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accQuestions" href="#bodyQ3" aria-expanded="false" aria-controls="bodyQ3">
										Question #3
									</a>
								</h4>
							</div>
							<div id="bodyQ3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headQ3">
								<div class="panel-body">
					
					
					
									<div class="panel-group" id="accQ3Subquestions" role="tablist" aria-multiselectable="true">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headQ3S1">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accQ3Subquestions" href="#bodyQ3S1" aria-expanded="true" aria-controls="bodyQ3S1">
														Subquestion #1
													</a>
												</h4>
											</div>
											<div id="bodyQ3S1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headQ3S1">
												<div class="panel-body">
													<div class="question">What is the capital of Karnataka?</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-12">
																<label for="">Correct Answer :</label>
																<div>Mumbai</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div>Mumbai</div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 1</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 2</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 3</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="marks-time">
														<div class="row">
															<div class="col-md-4">
																<div><label for="">Time</label></div>
																<div>Time Taken: <span>10 min</span></div>
																<div>Topper Time: <span>10 min</span></div>
																<div>Average Time: <span>10 min</span></div>
															</div>
															<div class="col-md-4">
																<div><label for="">Marks</label></div>
																<div>Maximum Marks: <span>10</span></div>
																<div>Topper Marks: <span>10</span></div>
																<div>Average Marks: <span>10</span></div>
															</div>
															<div class="col-md-4">
																<label for="">Assign Marks:</label>
					    										<div class="input-group">
					    											<input type="text" class="form-control">
					      											<span class="input-group-btn">
					      												<button class="btn btn-primary">Set</button>
					      											</span>
					    										</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headQ3S2">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accQ3Subquestions" href="#bodyQ3S2" aria-expanded="false" aria-controls="bodyQ3S2">
														Subquestion #2
													</a>
												</h4>
											</div>
											<div id="bodyQ3S2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headQ3S2">
												<div class="panel-body">
													<div class="question">What is the capital of Madhya Pradesh?</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-12">
																<label for="">Correct Answer :</label>
																<div>Mumbai</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div>Mumbai</div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 1</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 2</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 3</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="marks-time">
														<div class="row">
															<div class="col-md-4">
																<div><label for="">Time</label></div>
																<div>Time Taken: <span>10 min</span></div>
																<div>Topper Time: <span>10 min</span></div>
																<div>Average Time: <span>10 min</span></div>
															</div>
															<div class="col-md-4">
																<div><label for="">Marks</label></div>
																<div>Maximum Marks: <span>10</span></div>
																<div>Topper Marks: <span>10</span></div>
																<div>Average Marks: <span>10</span></div>
															</div>
															<div class="col-md-4">
																<label for="">Assign Marks:</label>
					    										<div class="input-group">
					    											<input type="text" class="form-control">
					      											<span class="input-group-btn">
					      												<button class="btn btn-primary">Set</button>
					      											</span>
					    										</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headQ3S3">
												<h4 class="panel-title">
													<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accQ3Subquestions" href="#bodyQ3S3" aria-expanded="false" aria-controls="bodyQ3S3">
														Subquestion #3
													</a>
												</h4>
											</div>
											<div id="bodyQ3S3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headQ3S3">
												<div class="panel-body">
													<div class="question">What is the capital of Madhya Pradesh?</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-12">
																<label for="">Correct Answer :</label>
																<div>Mumbai</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div>Mumbai</div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 1</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 2</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="answer">
														<div class="row">
															<div class="col-md-6">
																<label for="">Student's Answer :</label>
																<div><a href="javascript:void(0)">File 3</a></div>
															</div>
															<div class="col-md-6">
																<label for="">Reviews/Comments :</label>
																<div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"></textarea></div>
																<div class="form-group">
																	<button class="btn btn-info">Save Comments</button>
																</div>
															</div>
														</div>
													</div>
													<div class="marks-time">
														<div class="row">
															<div class="col-md-4">
																<div><label for="">Time</label></div>
																<div>Time Taken: <span>10 min</span></div>
																<div>Topper Time: <span>10 min</span></div>
																<div>Average Time: <span>10 min</span></div>
															</div>
															<div class="col-md-4">
																<div><label for="">Marks</label></div>
																<div>Maximum Marks: <span>10</span></div>
																<div>Topper Marks: <span>10</span></div>
																<div>Average Marks: <span>10</span></div>
															</div>
															<div class="col-md-4">
																<label for="">Assign Marks:</label>
					    										<div class="input-group">
					    											<input type="text" class="form-control">
					      											<span class="input-group-btn">
					      												<button class="btn btn-primary">Set</button>
					      											</span>
					    										</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
					
					
					
					
								</div>
							</div>
						</div>
					</div> -->
				</div>
			</div>
		</section>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="answerModal" tabindex="-1" role="dialog" aria-labelledby="answerModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="answerModalLabel">Answer in detail</h4>
      </div>
      <div class="modal-body">
        <div id="showAnswerDetail"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>