<section id="main-content">
	<section class="wrapper site-min-height">
		<!-- page start-->
		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<header class="panel-heading">
						<h4>Courses Chat</h4>
					</header>
					<!--<div class="row panel-body">
						<div class="col-lg-6">
							<div class="row">
								<div class="col-lg-6">
									<select id="courseSelect" class="form-control input-sm m-bot15">
										<option value="0">Select For Filter</option>
										<option value="1">Edited Rate</option>
										<option value="2">Default Rate</option>
										<option value="3">Institute</option>
										<option value="1">Professor</option>
									</select>
								</div>
							</div>
						</div>
						<div style="text-align: right" class="col-lg-6">
							<label>
								<input type="text" placeholder="Search" class="form-control pull-right" aria-controls="example">
							</label>
						</div>
					</div> -->
					<table class="table table-hover table-spl" id="all-courses-tbl">
						<thead>
							<tr>
								<th width="50px">Sr.No</th>
								<th width="50px">ID</th>
								<th width="">Course Name</th>
								<th width="">Institute Name</th>
								<th width="">Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</section>
			</div>
		</div>
		<!-- page end-->
	</section>
</section>