<div class="row">
	<div class="col-lg-12">
	<!--breadcrumbs start -->
	<ul class="breadcrumb">
			<li><a href="dashboard.php"><i class="fa fa-home"></i>Dashboard</a></li>
			<li><a href="professors.php">Professors/Tutors</a></li>
			<li><strong>Invited Professors Details</strong></li>
	</ul>
	<!--breadcrumbs end -->
	</div>
</div>
<section class="panel">
	<header class="panel-heading">
		Invited Professor/Tutor Status
	</header>
	<div class="col-lg-12">
		<input type="text" class="form-control search" placeholder="Search">
	</div>
	<table class="table" id="tab">
		<thead>
			<tr class=""><th>Name</th>
				<th>Email Id</th>
				<th>Contact No.</th>
				<th>Status</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</section>