<section class="panel">
    <header class="panel-heading">
        Portfolios
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <table id="tblPortfolioApprovals" class="display table table-bordered table-striped" >
                <thead>
                    <tr>
                        <th>Sr.</th>
                        <th>ID</th>
                        <th>Institute Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Type</th> 
                        <th>Subdomain</th> 
                        <th>Portfolio Name</th> 
                        <th>Description</th> 
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</section>