<section class="panel">
  <header class="panel-heading">
	 <h4>Courses Table</h4>
	 <a href="add-course.php"><button class="btn btn-info btn-xs pull-right" type="button"><i class="fa fa-plus"></i> New Course</button></a> 
  </header>
  <div style="overflow: auto;">
  <table class="table table-hover" id="all-courses">
	  <thead>
	  <tr>
		<th width="10%">ID</th>
		<th width="15%">Courses Name</th>
		<th width="40%">Subjects</th>
		<th width="10%">Start Date</th>
		<th width="10%">End Date</th>
		<!--<th width="10%">Ownership</th>-->
		<th width="10%"><i class=" fa fa-edit"></i>Status</th>
		<th width="5%"></th>                                
	  </tr>
	  </thead>
	  <tbody>
	  </tbody>
  </table>
  </div>
</section>