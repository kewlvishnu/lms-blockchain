<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1"
     id="view_details" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                    X</button>
                <h4 class="modal-title">
                    Course ABCD
                </h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>
                                Subject Name
                            </th>
                            <th>
                                Licensing/Ownership
                            </th>
                            <th>

                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                A
                            </td>
                            <td>
                                1 year license ---- 6 months remaining
                            </td>
                            <td>
                                <button class="btn btn-primary btn-xs" type="button">
                                    Renew button</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                B
                            </td>
                            <td>
                                1 year license ---- 6 months remaining
                            </td>
                            <td>
                                <button class="btn btn-primary btn-xs" type="button">
                                    Renew button</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                C
                            </td>
                            <td>
                                Selective Ip Transfer
                            </td>
                            <td>
                                -
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p>
                    <strong>Teach Students </strong>- minimum 6 months , As subjects expire they will
                    be unavailable to students.<br />
                    <strong>License to other prof/inst/publishers </strong>- Yes
                </p>
                <p>
                    <strong>Types of licensing allowed</strong>
                    <br />
                    Licensing is not allowed because 1 year licensed subject present in the course disqualifies
                    the course for selling to publishers/institutes and professors.
                </p>
                <p>
                    ( to see video or read document on understanding licenses, click here )
                </p>
                <br />
            </div>
        </div>
    </div>
</div>