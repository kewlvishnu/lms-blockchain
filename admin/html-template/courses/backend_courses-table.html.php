<section class="panel">
    <header class="panel-heading">
        <h4>Courses Table</h4>
    </header>
    <table class="table table-hover" id="all-courses">
        <thead>
            <tr>
                <th width="10%">ID</th>
                <th width="30%">Courses Name</th>
                <th width="25%">Duration</th>
                <th width="10%">Ownership</th>
                </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</section>