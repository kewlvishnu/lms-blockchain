<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
           <li><a href="instituteDashboard.php"><i class="fa fa-home"></i>Dashboard</a></li>
            <li><a href="students.php">Students</a></li>
            <li><a href="studentsList.php">Students List</a></li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<section class="panel">
    <header class="panel-heading">
        Students List
    </header>
	<input type="text" placeholder="Search" class="form-control search" style="margin-bottom:5px;">
    <div style="overflow: auto;">
        <table id='tbl_student_deatils' class="table">
            <thead>
                <tr>
                    <th>Sr. No.</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Name</th>
                    <th>Contact No</th>
                    <th>Courses</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <!--<tr><td>No any student invited yet</td></tr>-->
            </tbody>
        </table>
    </div>
</section>