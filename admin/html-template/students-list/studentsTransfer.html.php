<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
           <li><a href="instituteDashboard.php"><i class="fa fa-home"></i>Dashboard</a></li>
            <li><a href="students.php">Students</a></li>
            <li><a href="studentsTransfer.php">Students Transfer</a></li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<section class="panel">
    <header class="panel-heading">
        Students Transfer
    </header>
    <table id='tbl_student_details' class="table">
        <thead>
            <tr>
                <th>Sr. No.</th>
                <th>Username</th>
                <th>Password</th>
                <th>Courses</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <!--<tr><td>No any student invited yet</td></tr>-->
        </tbody>
    </table>
</section>

<!-- Modal -->
<div class="modal fade" id="transferModal" tabindex="-1" role="dialog" aria-labelledby="transferModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="transferModalLabel">Select Course to transfer</h4>
      </div>
      <div class="modal-body">
        <select name="" id="transferDropdown" class="form-control"></select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnTransfer">Transfer</button>
      </div>
    </div>
  </div>
</div>