<div class="row">
    <div class="col-lg-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="dashboard.php"><i class="fa fa-home"></i>Dashboard</a></li>
            <li><a href="assignment-exam.php">Add New Exam</a></li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<section class="panel">
    <header class="panel-heading">
        <h4>Choose your Section</h4>
        <div class="pull-right">
			<a href="" id="addChapter" class="btn btn-info btn-xs" DISABLED> Add Content </a>
        </div>
    </header>
    <div class="row panel-body">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <select class="form-control input-sm m-bot15" id="courseSelect" disabled="true">
                        <option value="0">Select Course</option>
                    </select>
                </div>
                <div class="col-md-6 col-sm-6">
                    <select class="form-control input-sm m-bot15" id="subjectSelect" disabled="true">
                        <option value="0">Select Subject</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-lg-6" style="text-align: right">
            <label>
                <input type="text" aria-controls="example" class="form-control pull-right" placeholder="Search"></label>
        </div>
    </div>
    <table class="table table-hover" id="currentChapters" style="display: none;">
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th></th>
            </tr>
        </thead>
		<div id="warning" style="padding: 5px;">
			No Course found in your profile. Please Add section to continue with the Content View.
		</div>
    </table>
</section>