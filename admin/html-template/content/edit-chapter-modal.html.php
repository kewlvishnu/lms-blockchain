<div class="col-lg-6 col-md-6">
    <section class="panel">
        <header class="panel-heading">
            Add Chapter
        </header>
        <div class="panel-body">
            <div role="form">
                <div class="form-group">
                    <label for="courseid">Chapter Name</label>
                    <input type="text" placeholder="" id="chapter-name" class="form-control">
                    <div class="form-msg chapter-name"></div>
                </div>
                <div class="form-group">
                    <label for="coursedescription">Chapter Description</label>
                    <textarea class="form-control" rows="5" cols="60" id="chapter-description"></textarea>
                    <div class="form-msg chapter-desc"></div>
                </div>
                <div class="checkbox">
                    <label>
                        <input id="is-demo" type="checkbox"> Demo
                    </label>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <button type="submit" class="btn btn-info" id="create-chapter">Edit Chapter</button>
                        </div>
                    </div>
                </div>                                  
            </div>
        </div>
    </section>
</div>