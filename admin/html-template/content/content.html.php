<div class="row">
	<div class="col-lg-12">
		<!--breadcrumbs start -->
		<ul class="breadcrumb">
			<li><a href=""></a></li>
			<li><a href=""></a></li>
		</ul>
		<!--breadcrumbs end -->
	</div>
</div>
<div class="row">
	<aside class="profile-nav col-lg-8 col-md-8">
		<section>
			<!--<div class="custom_img">
				<img src="img/default.jpg" id="course-image-preview" />
			</div>-->
			<div class="subject-image-thumb">
				<!--<img src="img/default.jpg" alt="" id="subject-image-preview">-->
				<!-- <span class="custom_heading" id="chapter-name"></span>
				<button class="btn btn-info btn-xs edit-button"><i class="fa fa-pencil"></i> Chapter</button> -->
				<!--<p>
					<h4>Description</h4>
					<span id="chapter-description"></span>
				</p>-->
			</div>
			<header style="clear: both">
				<!--<h3><span id="subject-name"></span> </h3>-->
				
			</header>
			<br />
			<p id="subject-desc"> </p>
		</section>
	</aside>
	<aside class="profile-nav col-lg-4 col-md-4">

		<!--<section class="panel">
			<div class="flat-carousal">
				<span>Assigned Professor</span>
				<a href="#subject-professor-modal" data-toggle="modal" class="btn btn-xs btn-success pull-right">Manage</a>
			</div>
			<ul class="nav nav-pills nav-stacked" id="subject-professors-teaser">
				<li><a>vishal gupta</a></li>
				<li><a>vishal gupta</a></li>
				<li><a>raj gupta</a></li>
				<li><a>prashant rajput</a></li>
				<li><a>komal gupta</a></li>
			</ul>
		</section>-->
	</aside>
</div>
<section style="display:none;" id="section-edit-chapter"  class="panel">
        <header class="panel-heading">
            Edit Chapter
			<button class="btn btn-primary btn-xs cancel-edit" style="float: right; border-color: rgb(255, 255, 255); background-color: #f6954b; margin-top: 0px;"><i class="fa fa-times"></i></button>
        </header>
        <div class="panel-body">
            <div role="form">
                <div class="form-group">
                     <b><i class="text-danger">*</i></b>    
                    <label for="courseid">Chapter Name</label>
                    <input type="text" placeholder="" id="edit-chapter-name" class="form-control">
                    <div class="form-msg chapter-name"></div>
                </div>
                <!--<div class="form-group">
                    <label for="coursedescription">Chapter Description</label>
                    <textarea class="form-control" rows="5" cols="60" id="edit-chapter-description"></textarea>
                    <div class="form-msg chapter-desc"></div>
                </div>-->
                <div class="checkbox">
                    <label>
                        <input id="is-demo" type="checkbox"> Allow for free demo
                    </label>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
							<div class=" pull-right">
								<button class="btn btn-primary btn-xs cancel-edit"><i class="fa fa-times"></i> Cancel</button>
								<button class="btn btn-primary btn-xs" id="edit-chapter"><i class="fa fa-save"></i> Save</button>
							</div>
                        </div>
                    </div>
                </div>                                  
            </div>
        </div>
    </section>

<section class="panel">
	<div id="content" class="row-fluid clearfix">
		<div class="col-md-12 column sortable">
			<div class="headings">
				<!--<div class="section-heading sortable-item" data-hid="0">
					<div class="view-pane">
						<div class="panel-heading">
							<label class="heading-number">Heading 1.1</label> <span class="heading-title"></span>
							&nbsp;&nbsp;<a href="#" class="edit-heading"><i class="fa fa-pencil"></i></a>
						</div>
					</div>
					<div class="edit-pane hidden-l">
						<div class="row panel-body">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="heading-number">Heading 1.1</label>
								</div>
								<div class="col-lg-10">
									<input type="text" class="form-control heading-title-input" placeholder="Title" />
								</div>
							</div>
						</div>
						<div class="row panel-body">
							<div class="col-md-6"><a class="btn btn-info save-title" href="#">Save</a> Or <a href="#" class="cancel-edit">Cancel</a></div>
							<div class="col-md-6" style="text-align: right;">
								<a href="#" class="delete-heading">
									<i class="fa fa-trash-o"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="section-content indented sortable-item">
					<div class="view-pane">
						<div class="panel-heading">
							<label class="content-number">Content 1.1.1</label> <span class="content-title"></span>
							<a href="#" class="edit-content"><i class="fa fa-pencil"></i></a>
							<button class="btn btn-xs btn-warning add-stuff pull-right" style="margin-top:0px;"><i class="fa fa-plus"></i> Add Stuff</button>
						</div>
					</div>
					<div class="edit-pane hidden-l">
						<div class="row panel-body">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="content-number">Content 1.1.1</label>
								</div>
								<div class="col-lg-10">
									<input type="text" class="form-control content-title-input" placeholder="Content Title" />
								</div>
							</div>
						</div>
						<div class="row panel-body">
							<div class="col-md-6"><a class="btn btn-info save-title" href="#">Save</a> Or <a href="#" class="cancel-edit">Cancel</a></div>
							<div class="col-md-6" style="text-align: right;">
								<a href="#" class="delete-content">
									<i class="fa fa-trash-o"></i>
								</a>
							</div>
						</div>
					</div>
				</div>-->
			</div>
			<div class="panel-heading add-heading">
				<strong>Add Section</strong>
				<i class="fa fa-plus pull-right" style="font-size:22px"></i>
			</div>
			
		</div>
	</div>
	<!-- page end-->
</section>