<div class="panel-body">
	<header class="panel-heading">
		<ul class="nav nav-tabs nav-justified ">
			<li class="active">
				<a href="#popular" data-toggle="tab">UPLOAD VIDEO
				</a>
			</li>
		</ul>
	</header>
	<div class="panel-body">
		<div class="tab-content tasi-tab">
			<div id="popular" class="tab-pane active">
				<article class="media">
					<a class="pull-left thumb p-thumb">
						<img src="img/product1.jpg">
					</a>
					<div class="media-body">
						<a href="#" class="  btn btn-info">Upload Video File</a>
					</div>
					<div style="margin-top: 20px;" class="progress-striped progress-sm">
						<div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-success">
							<span class="sr-only">40% Complete (success)</span>
						</div>
					</div>
				</article>
				<article class="media">
					<a class="pull-left thumb p-thumb">
						<img src="img/product2.png">
					</a>
					<div class="media-body">
						<a href="#" class=" p-head">Item Two Tittle</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					</div>
				</article>
			</div>                                                                       
		</div>
	</div>
</div>
<div class="panel-body">
	<header class="panel-heading tab-bg-dark-navy-blue">
		<ul class="nav nav-tabs nav-justified ">
			<li class="active">
				<a href="#audio" data-toggle="tab">UPLOAD AUDIO
					</a>
			</li> 
		</ul>
	</header>
	<div class="panel-body">
		<div class="tab-content tasi-tab">
			<div class="tab-pane active" id="audio">
				<article class="media">
					<a class="pull-left thumb p-thumb">
						<img src="img/product1.jpg">
					</a>
					<div class="media-body">
						<a href="#" class="  btn btn-info">Upload Audio File</a>
					</div>
				</article>
				<article class="media">
					<a class="pull-left thumb p-thumb">
						<img src="img/product2.png">
					</a>
					<div class="media-body">
						<a class=" p-head" href="#">Item Two Tittle</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					</div>
				</article>
			</div>
		</div>
	</div>
</div>
<div class="panel-body">
	<header class="panel-heading">
		<ul class="nav nav-tabs nav-justified ">
			<li class="active">
				<a href="#Div1" data-toggle="tab">Upload a Presentation</a>
			</li>
		</ul>
	</header>
	<div class="panel-body">
		<div class="tab-content tasi-tab">
			<div class="tab-pane active" id="Div1">
				<article class="media">
					<a class="pull-left thumb p-thumb">
						<img src="img/product1.jpg">
					</a>
					<div class="media-body">
						<a href="#" class="  btn btn-info">Upload Presentation File</a>
					</div>
				</article>
				<article class="media">
					<p>
						<strong>Tip:</strong> A presentation means slides (e.g. PowerPoint, Keynote). Slides are a great way
to combine text and visuals to explain concepts in an effective and efficient way. Use meaningful graphics
and clearly legible text!
					</p>
				</article>
			</div>
		</div>
	</div>
</div>