<section class="panel">
	  <header class="panel-heading">
		  Existing Professors/Tutors
	  </header>
	<div class="row" style="padding: 7px 10px 0">
		<div class="col-md-6 col-sm-6">
			<input type="text" placeholder="Search" class="form-control search">
		</div>
		<div class="col-md-6 col-sm-6">
			<a href="invitedProfessors.php" class="btn btn-info pull-right" style="margin-left: 5px;"><i class="fa fa-list"></i> Invitation Status</a>
			<a href="invite-professors.php" class="btn btn-info pull-right"><i class="fa fa-user"></i> Invite</a>

		</div>
	</div>
	<table class="table table-hover" id="institute-professors">
		<thead>
			<tr>
				<th style="width:48%;">Name</th>
				<th style="width:30%;">Email Id</th>
				<th style="width:22%;">Contact No</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</section>
  