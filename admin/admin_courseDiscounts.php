<?php
    require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Discounts & coupons</title>
    <?php include "html-template/general/head-tags.html.php"; ?>
	 <link href="assets/datetimepicker/jquery.datetimepicker.css" rel="stylesheet" />
   
</head>
<body>
    <section id="container" class="">
	  <?php include "html-template/general/header.html.php"; ?>
      <?php include "html-template/general/admin-sidebar.html.php"; ?>
      <!--main content start-->
     <?php include "html-template/admin-discountCoupons/admin_cou_dis.html.php"; ?>
      <!--main content end-->
      <?php include "html-template/general/footer.html.php"; ?>
      <?php include "html-template/pageCheckFooter.php"; ?>
	   </section>
    <?php include "html-template/general/footer-scripts.html.php"; ?>

    <?php include 'html-template/admin-discountCoupons/coupon_modal.html.php'; ?>
    <?php include 'html-template/admin-discountCoupons/discount_modal.html.php'; ?>
	 <?php include 'html-template/admin-discountCoupons/editCouponsmodals.html.php'; ?>

	<script src="assets/datetimepicker/jquery.datetimepicker.js"></script>
    <script src="js/custom/adminDisCoup.js"></script>
	<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
   	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
	
</body>
</html>