<!DOCTYPE html>
<html lang="en">
<head>
		<?php include 'html-template/general/head-tags.html.php'; ?>
		<title>Invited Professor History</title>
</head>
<body>
		<section id="container" class="">
			<!--header start-->
			<?php include 'html-template/general/header.html.php'; ?>
			<!--header end-->
			<!--sidebar start-->
			<?php include 'html-template/general/sidebar.html.php'; ?>
			<!--sidebar end-->
			<!--main content start-->
			<section id="main-content">
					<section class="wrapper site-min-height">
							<!-- page start-->
							<div class="row">
									<div class="col-md-12">
										<?php include 'html-template/invitedProfessors/invitedProfessors.html.php'; ?>
									</div>
							</div>
							<!-- page end-->
					</section>
			</section>
			<!--main content end-->
			<!--footer start-->
			<?php include 'html-template/general/footer.html.php'; ?>
	</section>
	<?php include 'html-template/general/footer-scripts.html.php'; ?>
		<script src="js/custom/invited-professors.js"></script>
</body>
</html>
