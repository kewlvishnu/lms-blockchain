<?php
    require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Course-keys </title>
        <?php include "html-template/general/head-tags.html.php"; ?>
    </head>
    <body>
        <section id="container" class="">
            <?php include "html-template/general/header.html.php"; ?>
            <?php if($_SESSION['userRole']==5){
                  include "html-template/general/admin-sidebar.html.php";
            }else{
                include "html-template/general/sidebar.html.php";
            } ?>
              <!--main content start-->
            <?php include "html-template/course-key/coursekeys.html.php"; ?>
            <?php include "html-template/course-key/contact.modal.html.php"; ?>
            <?php include "html-template/course-key/purchaseNowModal.html.php"; ?>
            <!-- js placed at the end of the document so the pages load faster -->
            <?php include "html-template/general/footer.html.php"; ?>
            <?php include "html-template/pageCheckFooter.php"; ?>
            <?php include "html-template/general/footer-scripts.html.php"; ?>
        </section>
            <script src="js/custom/get-profile-details.js"></script>
            <script src="js/custom/coursekey.js"></script>
  </body>
</html>