<?php
	require_once "html-template/cookieCheck.php";
	require_once "../api/Vendor/ArcaneMind/Api.php";
	require_once "../api/Vendor/ArcaneMind/AccessApi.php";
	$access = AccessApi::checkAccess();
	require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <title>Add Assignment - Step 2</title>
    <?php include "html-template/general/head-tags.html.php"; ?>
	<link rel="stylesheet" href="../css/mediaelementplayer.min.css" />
    <!--<link rel="stylesheet" href="css/bar-ui.css">-->
	<meta charset="utf-8">
</head>
<body class="">
	<section class="" id="container">
		<!--header start-->
		<?php include "html-template/general/header.html.php"; ?>
		<!--header end-->
		<!--sidebar start-->
		<?php include "html-template/general/sidebar.html.php"; ?>
		<!--sidebar end-->
		<!--main content start-->
		<section id="main-content">
			<section class="wrapper" style="min-height: 800px;">
				<!--breadcrum-->
				<?php include 'html-template/assignment/assignment-breadcrumb.php'; ?>
				<!-- page start-->
				<div class="row">
					<?php include 'html-template/assignment/add-assignment-2-row1.html.php'; ?>
				</div>
				<div class="row">
					<?php include 'html-template/assignment/add-assignment-2-row2.html.php'; ?>
				</div>
				<!-- page end-->
			</section>
		</section>
		<!--main content end-->
		<!--footer start-->
		<?php include "html-template/general/footer.html.php"; ?>
        <?php include "html-template/pageCheckFooter.php"; ?>
		<!--footer end-->
    </section>

    <!-- js placed at the end of the document so the pages load faster -->
	<?php include "html-template/general/footer-scripts.html.php"; ?>
    <!--common script for all pages-->
	<script src="assets/ckeditor/ckeditor.js"></script>
	<script src="js/custom/add-assignment-2.js"></script>
	<script src="js/jquery.form.min.js" type="text/javascript"></script>
	<script src="../js/mediaelement-and-player.min.js"></script>
	<!--<script src="js/soundmanager2.js"></script>-->
	<!--<script src="js/bar-ui.js"></script>-->
	<?php include 'html-template/assignment/add-assignment-2-modal.html.php'; ?>
	<?php include 'html-template/assignment/import-question-modal.html.php'; ?>
	<?php include 'html-template/assignment/exam-instructions-modal.html.php'; ?>
</body>
</html>