<?php
	@session_start();
	$libPath = "../api/Vendor";
	include $libPath . '/Zend/Loader/AutoloaderFactory.php';
	require $libPath . "/ArcaneMind/Api.php";
	Zend\Loader\AutoloaderFactory::factory(array(
			'Zend\Loader\StandardAutoloader' => array(
					'autoregister_zf' => true,
					'db' => 'Zend\Db\Sql'
			)
	));
	require_once '../api/Vendor/ArcaneMind/Exam.php';
	$data = new stdClass();
	$data->type			= Api::sanitize($_GET['type']);
	if (isset($_GET['questions'])) {
		$data->questions	= Api::sanitize($_GET['questions']);
	}
	$data->courseId	= Api::sanitize($_GET['courseId']);
	$data->subjectId= Api::sanitize($_GET['subjectId']);
	$ex = new Exam();
	$ex->generateManualExamCSV($data);
?>