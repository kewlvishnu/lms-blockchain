<?php
    require_once "html-template/cookieCheck.php";
	$libPath = "../api/Vendor";
    require_once $libPath . "/ArcaneMind/Api.php";
	require_once $libPath . "/ArcaneMind/AccessApi.php";
	AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Courses</title>
        <?php include "html-template/general/head-tags.html.php"; ?>
    </head>
    <body>
        <section id="container" class="">
            <?php include "html-template/general/header.html.php"; ?>
            <?php include "html-template/general/sidebar.html.php"; ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper site-min-height">
                    <!-- page start-->
                    <div class="row">
                        <div class="col-md-12">
                            <?php include "html-template/courses/courses-table.html.php"; ?>
                        </div>
                        <!--<div class="col-md-3">
                            <?php include "html-template/general/updates-feed.html.php"; ?>
                        </div>-->
                    </div>
                    <!-- page end-->
                </section>
            </section>
            <!--main content end-->
            <?php include "html-template/general/footer.html.php"; ?>
            <?php include "html-template/pageCheckFooter.php"; ?>
        </section>
        <!-- js placed at the end of the document so the pages load faster -->
        <?php include "html-template/general/footer-scripts.html.php"; ?>
        <script src="js/custom/courses.js"></script>
        <?php include "html-template/courses/courses-modal.html.php"; ?>
    </body>
</html>
