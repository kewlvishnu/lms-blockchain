<?php
    require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Profile</title>
	<?php include "html-template/general/head-tags.html.php"; ?>
  </head>
  <body>
	<section id="container" class="">
      <?php include "html-template/general/header.html.php"; ?>
      <?php include "html-template/general/sidebar.html.php"; ?>
	  <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                <div class="col-md-10">
					  <?php include "html-template/profile/profile-cover.html.php"; ?>
					  <?php include "html-template/profile/profile-bio.html.php"; ?>
					  <?php include "html-template/profile/profile-am-experience.html.php"; ?>
					  <?php include "html-template/profile/profile-institute-categories.html.php"; ?>
					  <?php include "html-template/profile/profile-general-info.html.php"; ?> 
                </div>
                <div class="col-md-2">
                    <?php //include "html-template/general/updates-feed.html.php"; ?>
                </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
	  <?php include "html-template/general/footer.html.php"; ?>
    <?php include "html-template/pageCheckFooter.php"; ?>
  </section>
	<?php include "html-template/general/footer-scripts.html.php"; ?>
	<script src="assets/jquery-multi-select/js/jquery.multi-select.js"></script>
	<script src="js/custom/profile.js"></script>
	<?php include "html-template/profile/image-upload-modals.html.php"; ?>
       
  </body>
</html>