<?php
	require_once "html-template/cookieCheck.php";
    require_once "../api/Vendor/ArcaneMind/Api.php";
    require_once "../api/Vendor/ArcaneMind/AccessApi.php";
    $access = AccessApi::checkAccess();
    require_once "html-template/pageCheck.php";
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Dashboard</title>
		<?php include 'html-template/general/head-tags.html.php'; ?>
		<link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
		<link rel="stylesheet" href="css/owl.carousel.css" type="text/css"/>
		<link href="assets/morris.js-0.4.3/morris.css" rel="stylesheet" />
		<link href="assets/morris.js-0.4.3/morris.css" rel="stylesheet" />
		<link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
		<link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
		<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	</head>
	<body>
	<section id="container" >
		<?php include "html-template/general/header.html.php"; ?>
		<?php include "html-template/general/sidebar.html.php"; ?>
		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
				<!--state overview start-->
				<div class="row state-overview">
					<div class="col-lg-3 col-sm-6">
						<section class="panel">
							<a title="click to add course" href="add-course.php">
								<div class="symbol red">
									<i class="fa fa-tags"></i>Add
								</div>
							</a>
							<a  href="courses.php">
								<div class="value">
									<h1 class="courseCount">
									0
									</h1>
									<p><span class="profOnly" style="display: none;">My own </span>Courses</p>
								</div>
							</a>
						</section>
					</div>                  
					<div class="col-lg-3 col-sm-6">
						<section class="panel">
							<a title="click to manage students" href="students.php">
								<div class="symbol terques">
									<i class="fa fa-users"></i>Manage
								</div>
							</a>
							<div class="value">
								<h1 class="studentCount">
									0
								</h1>
								<p>Students<span class="profOnly" style="display: none;font-size: 0.7em;"><br>( Own Courses )</span></p>
							</div>
						</section>
					</div>
					<!--<div class="col-lg-3 col-sm-6">
						<section class="panel">
							<a title="click to manage professors" href="institute-professors.php">
								<div class="symbol yellow">
									<i class="fa fa-user"></i>Manage
								</div>
							</a>
							<div class="value">
								<h1 class="professorCount">
									0
								</h1>
								<p>Professors</p>
							</div>
						</section>
					</div>-->
					<div class="col-lg-3 col-sm-7 profOnly" style="display: none;">
						<section class="panel">
							<a href="subject-assigned.php">
								<div class="symbol blue">
									<i class="fa fa-edit"></i> 
								</div>
								<div class="value">
									<h1 class="assignedSubjectsCount">
										0
									</h1>
									<p>Assigned Subjects</p>
								</div>
							</a>
						</section>
					</div>
					<div class="col-lg-3 col-sm-6 profOnly" style="display: none;">
						<section class="panel">
							<div class="symbol blue">
								<i class="fa fa-users"></i>
							</div>
							<div class="value">
								<h1 class="assignedSubjectsStudent">
									0
								</h1>
								<p>Students<span class="profOnly" style="display: none;font-size: 0.7em;"><br>( Assigned Subjects )</span></p>
							</div>
						</section>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-7">
						<section class="panel">
							<div class="revenue-head">
								<span class="custom-rev-combo pull-left" id="previous" style="cursor: pointer;display: none;">
									&lt;&lt; Previous
								</span>
								<h3>Overall Students per <rup class="profOnly" style="display: none;">own </rup>Course</h3>
								<span class="custom-rev-combo pull-right" id="next" style="cursor: pointer;display: none;">
									Next &gt;&gt;
								</span>
							</div>
							<div class="panel-body">
								<div id="hero" class="graph custom-graph" style="height: 400px;"></div>
							</div>
						</section>
					</div>
					<div class="col-lg-5">
						<div class="panel">
							<div class="revenue-head">
								<h3>Unique views in last week</h3>
								<span class="rev-combo pull-right" style="max-width: 50%;">
									<select class="rev-combo form-control" style="height: 18px;padding: 0px;" id="courseSelector">
										<option value="0">Select Course</option>
									</select>
								</span>
							</div>
							<div class="panel-body">
								<div id="sparkline" style="height: 400px;"></div>
							</div>
							<div id="detailedsummary" style="font-size: 20px;">
							<a id="getsummary">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Click here to get Detailed summary</a></div>
						</div>
					</div>
				</div>
				<br/><br/>
			</section>
		<!--main content end-->
		<!--footer start-->
		<!--footer end-->
	</section>
	<?php include "html-template/general/footer.html.php"; ?>
    <?php include "html-template/pageCheckFooter.php"; ?>
	<?php include 'html-template/general/footer-scripts.html.php'; ?>
	 <?php include "html-template/dashboard/detailsummary.html.php"; ?>
	<!--script for this page-->
	<script src="js/jquery.sparkline.js" type="text/javascript"></script>
	<script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
	<script src="js/owl.carousel.js" ></script>
	<script src="js/jquery.customSelect.min.js" ></script>
	<script src="assets/morris.js-0.4.3/morris.min.js" type="text/javascript"></script>
    <script src="assets/morris.js-0.4.3/raphael-min.js" type="text/javascript"></script>
	<script src="js/jquery.highchart.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
	<script>
		//owl carousel
		$(document).ready(function() {
			$("#owl-demo").owlCarousel({
				navigation : true,
				slideSpeed : 300,
				paginationSpeed : 400,
				singleItem : true,
				autoPlay:true
	
			});
		});
		//custom select box
		$(function(){
			$('select.styled').customSelect();
		});
	</script>
	<script src="js/custom/instituteDashboard.js" type="text/javascript"></script>
	</body>
</html>
