<?php
	require_once('c0nn.php');
	
	$result = $mysqli->query("SELECT * FROM `republicday_questions` ORDER BY RAND() LIMIT 5");
	/*$result = mysqli_fetch_all($result);
	var_dump($result);*/
	if ($result->num_rows > 0) {
		$questions = array();
		$i = 0;
		while($row = $result->fetch_assoc()) {
			$questions[$i]["id"]	= $row["id"];
			$questions[$i]["question"]	= $row["question"];
			$questions[$i]["answers"]	= array($row["option1"],$row["option2"],$row["option3"],$row["option4"]);
			$questions[$i]["answer"]	= "";
			$i++;
		}
		echo json_encode($questions);
	}
?>