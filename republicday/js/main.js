/*var questions = [
					{"question":"What is the capital of Maharashtra?",answers:["Mumbai", "Nagpur", "Pune", "Kolhapur"],"answer":""},
					{"question":"What is the capital of Karnataka?",answers:["Mumbai", "Nagpur", "Pune", "Kolhapur"],"answer":""},
					{"question":"What is the capital of Telangana?",answers:["Mumbai", "Nagpur", "Pune", "Kolhapur"],"answer":""},
					{"question":"What is the capital of Kerala?",answers:["Mumbai", "Nagpur", "Pune", "Kolhapur"],"answer":""},
					{"question":"What is the capital of Rajasthan?",answers:["Mumbai", "Nagpur", "Pune", "Kolhapur"],"answer":""}
				];*/
var questions = [];
var answeredFlag = false;
var imgQuestion = [
					"images/banner1.jpg",
					"images/banner2.jpg",
					"images/banner3.jpg",
					"images/banner4.jpg",
					"images/banner5.jpg"
					];
prefetchQuestions = function() {
	$.ajax({
		method: "POST",
		url: "ajax.php",
		dataType: "json"
	})
	.done(function( msg ) {
		//console.log(msg);
		questions = msg;
		displayQuestion(0);
	});
}
$(document).ready(function(){
	prefetchQuestions();
	$('.btn-nav').click(function(){
		var question = $(this).attr("data-current");
		var answer 	 = $('[name=answer]:checked').val();
		questions[question].answer = answer;
		if ((answer!="" && answer!=undefined) && !answeredFlag) {
			answeredFlag = true;

		};
		var questionIndex = $(this).attr("data-question");
		displayQuestion(questionIndex);
	});
	$('#btnFinish').click(function(){
		var question = $("#btnPrev").attr("data-current");
		var answer 	 = $('[name=answer]:checked').val();
		questions[question].answer = answer;
		if ((answer!="" && answer!=undefined) && !answeredFlag) {
			answeredFlag = true;
		};
		if (answeredFlag) {
			$('.site-qa').html('<form>'+
								'<div class="form-group text-left">'+
									'<h3>Enter your details below :</h3>'+
								'</div>'+
								'<div class="form-group">'+
									'<input type="text" class="form-control" id="inputName" placeholder="Enter Name" />'+
								'</div>'+
								'<div class="form-group">'+
									'<input type="email" class="form-control" id="inputEmail" placeholder="Enter Email" />'+
								'</div>'+
								'<a href="javascript:void(0)" class="btn btn-success pull-right" id="btnSendInfo">Send Info</a>'+
							'</form>');
		} else {
			alert(answeredFlag);
			//alert('Please answer at least 1 question to participate!');
		}
	});
	$('.site-qa').on("click",'#btnSendInfo',function(){
		var name = $('#inputName').val();
		var email = $('#inputEmail').val();
		if (!name) {
			alert("Name is compulsory");
		} else if (!email) {
			alert("Email is compulsory");
		} else {
			var sendInfo = {};
			sendInfo['name'] = name;
			sendInfo['email'] = email;
			sendInfo['questions'] = questions;
			var data = JSON.stringify(sendInfo);
			$.ajax({
				method: "POST",
				url: "checkAndSave.php",
				data: {json: data}
			})
			.done(function( msg ) {
				$('.site-qa').html('<h2>Thank you for participating! Happy Republic Day.</h2>');
			});
		}
	});
});
function displayQuestion(questionIndex) {
	questionIndex = parseInt(questionIndex);
	var question = questions[questionIndex];
	$('#imgQuestion').attr("src",imgQuestion[questionIndex]);
	$('#btnPrev').attr('disabled', false);
	$('#btnNext').attr('disabled', false);
	$('#btnPrev').removeClass('hide');
	$('#btnNext').removeClass('hide');
	$('#btnFinish').addClass('hide');
	$('#btnNext').attr('data-question', 0);
	$('#btnPrev').attr('data-question', 0);
	$('#btnNext').attr('data-current', questionIndex);
	$('#btnPrev').attr('data-current', questionIndex);
	$('#question').html(question.question);
	$('#answer').html('');
	for (var i = 0; i < question["answers"].length; i++) {
		$('#answer').append('<li>'+
								'<label for="question'+i+'">'+
									'<input type="radio" name="answer" '+((question.answer == question["answers"][i])?'checked':'')+' id="question'+i+'" value="'+question["answers"][i]+'" /> '+question["answers"][i]+
								'</label>'+
							'</li>');
	};
	if (questionIndex == 0) {
		$('#btnPrev').attr('disabled', true);
		$('#btnPrev').addClass('hide');
		$('#btnNext').attr('data-question', questionIndex+1);
	} else if (questionIndex == (questions.length-1)) {
		$('#btnNext').attr('disabled', true);
		$('#btnNext').addClass('hide');
		$('#btnPrev').attr('data-question', questionIndex-1);
		$('#btnFinish').removeClass('hide');
	} else {
		$('#btnPrev').attr('data-question', questionIndex-1);
		$('#btnNext').attr('data-question', questionIndex+1);
	}
}