<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Arcanemind | 26th January | Republic Day</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="site-body">
    <header class="site-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <div class="bg-white text-center">
              <img src="../img/arcanemind-logo.png" alt="" class="">
            </div>
          </div>
          <div class="col-sm-8">
            <div class="offers">Win a chance to get free merchandise stuff</div>
            <div class="offers">Win coupons worth Rs.10,000/-</div>
          </div>
        </div>
      </div>
    </header>
    <section>
      <div class="container">
        <div class="site-pad">
          <div class="row">
            <div class="col-md-6">
              <img src="images/banner1.jpg" alt="" class="btn-block" id="imgQuestion">
            </div>
            <div class="col-md-6">
              <div class="site-qa">
                <div class="question" id="question"><!-- Which one of the following right of Indian Constitution guarantees all the fundamental rights to every resident of country? --></div>
                <div>
                  <ul class="list-answer list-unstyled" id="answer">
                    <!-- <li><label for=""><input type="radio" /> Right against exploitation</label></li>
                    <li><label for=""><input type="radio" /> Right to freedom</label></li>
                    <li><label for=""><input type="radio" /> Right to equality</label></li>
                    <li><label for=""><input type="radio" /> Right to constitutional remedies</label></li> -->
                  </ul>
                </div>
                <div>
                  <nav>
                    <a href="javascript:void(0)" class="btn btn-danger btn-nav pull-left hide" id="btnPrev">Prev</a>
                    <a href="javascript:void(0)" class="btn btn-danger btn-nav pull-right hide" id="btnNext">Next</a>
                    <a href="javascript:void(0)" class="btn btn-success pull-right hide" id="btnFinish">Finish</a>
                  </nav>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="js/main.js"></script>
  </body>
</html>