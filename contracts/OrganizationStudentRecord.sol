pragma solidity ^0.4.24;
contract OrganizationStudentRecord {
    address private owner;
    string constant organizationName = "";
    
    mapping(address => bool) registered;
    struct Student {
        address owner;
        string firstName;
        string lastName;
        string country;
        string info;
        string interests;
        string achievements;
    }
    
    mapping(address => Student) public students;
    
    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }
    modifier onlyRecordOwner {
        require(msg.sender == students[msg.sender].owner);
        _;
    }
    
    constructor() public {
        owner = msg.sender;
    }
    function () public payable {
        revert();
    }
    
    function registerStudent(string _firstname, string _lastname) public returns(bool) {
        require(!registered[msg.sender]);
        students[msg.sender].owner = msg.sender;
        registered[msg.sender] = true;
        students[msg.sender].firstName = _firstname;
        students[msg.sender].lastName = _lastname;
        return true;
    }
    function registerStudentComplete(string _firstname, string _lastname, string _country, string _info, string _interests, string _achievements) public returns(bool) {
        require(!registered[msg.sender]);
        registered[msg.sender] = true;
        students[msg.sender] = Student(msg.sender, _firstname, _lastname, _country, _info, _interests, _achievements);
        return true;
    }
    
    
    function addCountry(string _country) public onlyRecordOwner returns(bool) {
        require(registered[msg.sender]);
        students[msg.sender].country = _country;
        return true;
    }
    function addInfo(string _info) public onlyRecordOwner returns(bool) {
        require(registered[msg.sender]);
        students[msg.sender].info = _info;
        return true;
    }
    function addInterests(string _interests) public onlyRecordOwner returns(bool) {
        require(registered[msg.sender]);
        students[msg.sender].interests = _interests;
        return true;
    }
    function addAchievements(string _achievements) public onlyRecordOwner returns(bool) {
        require(registered[msg.sender]);
        students[msg.sender].achievements = _achievements;
        return true;
    }
    function addCountryInfo(string _country, string _info) public onlyRecordOwner returns(bool) {
        require(registered[msg.sender]);
        students[msg.sender].country = _country;
        students[msg.sender].info = _info;
        return true;
    }
    function addInfoInterests(string _info, string _interests) public onlyRecordOwner returns(bool) {
        require(registered[msg.sender]);
        students[msg.sender].info = _info;
        students[msg.sender].interests = _interests;
        return true;
    }
    function addInterestsAchievements(string _interests, string _achievements) public onlyRecordOwner returns(bool) {
        require(registered[msg.sender]);
        students[msg.sender].interests = _interests;
        students[msg.sender].achievements = _achievements;
        return true;
    }
    function addAchievementsCountry(string _achievements, string _country) public onlyRecordOwner returns(bool) {
        require(registered[msg.sender]);
        students[msg.sender].achievements = _achievements;
        students[msg.sender].country = _country;
        return true;
    }
    function addCountryInterests(string _country, string _interests) public onlyRecordOwner returns(bool) {
        require(registered[msg.sender]);
        students[msg.sender].country = _country;
        students[msg.sender].interests = _interests;
        return true;
    }
    function addCountryInfoInterests(string _country, string _info, string _interests) public onlyRecordOwner returns(bool) {
        require(registered[msg.sender]);
        students[msg.sender].country = _country;
        students[msg.sender].info = _info;
        students[msg.sender].interests = _interests;
        return true;
    }
    function addInfoInterestsAchievements(string _info, string _interests, string _achievements) public onlyRecordOwner returns(bool) {
        require(registered[msg.sender]);
        students[msg.sender].info = _info;
        students[msg.sender].interests = _interests;
        students[msg.sender].achievements = _achievements;
        return true;
    }
    function addInterestsAchievementsCountry(string _interests, string _achievements, string _country) public onlyRecordOwner returns(bool) {
        require(registered[msg.sender]);
        students[msg.sender].interests = _interests;
        students[msg.sender].achievements = _achievements;
        students[msg.sender].country = _country;
        return true;
    }
    function addAchievementsCountryInfo(string _achievements, string _country, string _info) public onlyRecordOwner returns(bool) {
        require(registered[msg.sender]);
        students[msg.sender].achievements = _achievements;
        students[msg.sender].country = _country;
        students[msg.sender].info = _info;
        return true;
    }
    function addCountryInfoInterestsAchievements(string _country, string _info, string _interests, string _achievements) public onlyRecordOwner returns(bool) {
        require(registered[msg.sender]);
        students[msg.sender].country = _country;
        students[msg.sender].info = _info;
        students[msg.sender].interests = _interests;
        students[msg.sender].achievements = _achievements;
        return true;
    }
    
    function checkRegistered() public view returns(bool){
        return registered[msg.sender];
    }
}