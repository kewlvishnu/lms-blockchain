<?php
	function url_safe_base64_encode($value) {
		$encoded = base64_encode($value);
		// replace unsafe characters +, = and / with the safe characters -, _ and ~
		return str_replace(
			array('+', '=', '/'),
			array('-', '_', '~'),
			$encoded);
	}
	
	$policy = '{ "expiration": "2014-12-14T12:00:00.000Z",
		"conditions": [
			{"bucket": "arcanemind"},
			{"key", "user-data/files/asd.jpg"},
			{"acl": "public-read"},
			{"success_action_redirect": "http://google.com"},
			{"Content-Type", "image/jpeg"},
			{"x-amz-meta-uuid": "14365123651274"},
			{"x-amz-meta-tag", "sumthing"},
			
			{"x-amz-credential": "AKIAI6EW5HWPVDY5IJRA/20141213/us-west-2/s3/aws4_request"},
			{"x-amz-algorithm": "AWS4-HMAC-SHA256"},
			{"x-amz-date": "20141213T000000Z" }
		]
	}';
	$encoded_policy = rawurlencode(url_safe_base64_encode($policy));
	$signature = hash_hmac('sha256', $policy, 'ooMUnwWHFN5vBUv3bP4yuWQ5BeIx1AkDrDYD4MI9', TRUE);
	$encoded_signature = rawurlencode(url_safe_base64_encode($signature));
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	</head>
	<body>
	<form action="http://arcanemind.s3.amazonaws.com/" method="post" enctype="multipart/form-data">
		<input type="hidden" name="key" value="user-data/files/asd.jpg" />
		<input type="hidden" name="acl" value="public-read" />
		<input type="hidden" name="success_action_redirect" value="http://google.com" />
		<input type="hidden" name="Content-Type" value="image/jpeg" />
		<input type="hidden" name="x-amz-meta-uuid" value="14365123651274" />
		<input type="hidden" name="x-amz-meta-tag" value="sumthing" />
		<input type="hidden" name="X-Amz-Credential" value="AKIAI6EW5HWPVDY5IJRA/20141213/us-west-2/s3/aws4_request" />
		<input type="hidden" name="X-Amz-Algorithm" value="AWS4-HMAC-SHA256" />
		<input type="hidden" name="X-Amz-Date" value="20141213T000000Z" />
		<input type="hidden" name="Policy" value='<?php echo $encoded_policy; ?>' />
		<input type="hidden" name="X-Amz-Signature" value="<?php echo $encoded_signature; ?>" />
		<input type="file" name="file" /> <br />
		<!-- The elements after this will be ignored -->
		<input type="submit" name="submit" value="Upload to Amazon S3" />
	</form>
</html>