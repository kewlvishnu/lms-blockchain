<?php
	error_reporting(E_ALL); ini_set('display_errors', 1);
	//get the last-modified-date of this very file
	$lastModified=filemtime(__FILE__);
	//get a unique hash of this file (etag)
	$etagFile = md5_file(__FILE__);
	//get the HTTP_IF_MODIFIED_SINCE header if set
	$ifModifiedSince=(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
	//get the HTTP_IF_NONE_MATCH header if set (etag: unique file hash)
	$etagHeader=(isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

	//set last-modified header
	header("Last-Modified: ".gmdate("D, d M Y H:i:s", $lastModified)." GMT");
	//set etag-header
	header("Etag: $etagFile");
	//make sure caching is turned on
	header('Cache-Control: max-age=290304000, public');
	
	//check if page has changed. If not, send 304 and exit
	if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])==$lastModified || $etagHeader == $etagFile)
	{
	       header("HTTP/1.1 304 Not Modified");
	       exit;
	}

	//	url functions and routes
	require_once('config/url.functions.php');
	$pageArr = explode('/', $pageUrl);
	$pageGet = explode('?', $pageUrl);
	
	//$page = "student";
	/*
	**	flagInvalid : true = valid urls, false = invalid urls
	*/
	$flagInvalid = false;
	/*
	**	flagSession :
	**	0 = neutral URLs which opens regardless of user login status
	**	1 = these urls open only for non logged in users
	**	2 = these urls open only for logged in users
	*/
	$flagSession = 0;
	$pages = array('', 'index.php', 'verify', 'reset-password', 'init-student', 'policies', 'contact', 'about', 'integro-assets', 'signup', 'institute-instructor-signup', 'login', 'institute-instructor-login', 'signout');

	if (in_array($pageArr[0], $pages)) {
		if ($pageArr[0] == '' || $pageArr[0] == 'index.php') {
			$page	  = "home";
			$flagSession = 0;
		} else if ($pageArr[0] == 'policies') {
			if (isset($pageArr[1]) && !empty($pageArr[1])) {
				if ($pageArr[1] == "terms") {
					$page	  = "policiesTerms";
				} else if ($pageArr[1] == "privacy") {
					$page	  = "policiesPrivacy";
				} else {
					$flagInvalid = true;
				}
			} else {
				$flagInvalid = true;
			}
		} else if ($pageArr[0] == 'contact') {
			$page = "contact";
		} else if ($pageArr[0] == 'about') {
			$page = "about";
		} else if ($pageArr[0] == 'integro-assets') {
			$page = "integroAssets";
		} else if ($pageArr[0] == 'signup') {
			$page = "signup";
			$flagSession = 1;
			if (isset($pageArr[1]) && !empty($pageArr[1])) {
				if ($pageArr[1] == 'verify') {
					$page 	  = "signupVerify";
					$flagSession = 1;
					if (isset($pageArr[2]) && !empty($pageArr[2])) {
						$slug 	  = $pageArr[2];
					} else {
						$flagInvalid = true;
					}
				} else if ($pageArr[1] == 'complete') {
					$page 	  = "signupComplete";
					$flagSession = 1;
					if (isset($pageArr[2]) && !empty($pageArr[2])) {
						$slug 	  = $pageArr[2];
					} else {
						$flagInvalid = true;
					}
				} else if ($pageArr[1] == 'course') {
					//$page 	  = "signupCourse";
					$flagSession = 1;
					if (isset($pageArr[2]) && !empty($pageArr[2])) {
						$slug 	  = (int)$pageArr[2];
						$sitepathSignup.= 'course/'.$slug;
						$sitepathLogin.= 'course/'.$slug;
					} else {
						$flagInvalid = true;
					}
				}
			}
		} else if ($pageArr[0] == 'institute-instructor-signup') {
			$page = "instituteSignup";
			$flagSession = 1;
			if (isset($pageArr[1]) && !empty($pageArr[1])) {
				if ($pageArr[1] == 'verify') {
					$page 	  = "signupVerify";
					$flagSession = 1;
				} else if ($pageArr[1] == 'complete') {
					$page 	  = "signupComplete";
					$flagSession = 1;
					if (isset($pageArr[2]) && !empty($pageArr[2])) {
						$slug 	  = $pageArr[2];
					} else {
						$flagInvalid = true;
					}
				}
			}
		} else if ($pageArr[0] == 'login') {
			$page = "signin";
			$flagSession = 1;
			if (isset($pageArr[1]) && !empty($pageArr[1])) {
				if ($pageArr[1] == 'verify') {
					$page 	  = "signinVerify";
					$flagSession = 1;
				} else if ($pageArr[1] == 'course') {
					//$page 	  = "signupCourse";
					$flagSession = 1;
					if (isset($pageArr[2]) && !empty($pageArr[2])) {
						$slug 	  = $pageArr[2];
						$sitepathSignup.= 'course/'.$slug;
						$sitepathLogin.= 'course/'.$slug;
					} else {
						$flagInvalid = true;
					}
				}
			}
		} else if ($pageArr[0] == 'institute-instructor-login') {
			$page = "instituteSignin";
			$flagSession = 1;
			if (isset($pageArr[1]) && !empty($pageArr[1])) {
				if ($pageArr[1] == 'verify') {
					$page 	  = "signinVerify";
					$flagSession = 1;
				} else {
					$flagInvalid = true;
				}
			}
		} else if ($pageArr[0] == 'signout') {
			$page = "logout";
		} else if ($pageArr[0] == 'verify') {
			if ((isset($pageArr[1]) && !empty($pageArr[1])) && (isset($pageArr[2]) && !empty($pageArr[2]))) {
				$page	  = "verify";
				$flagSession = 1;
				$slug 	  = $pageArr[1].'/'.$pageArr[2];
			} else {
				$flagInvalid = true;
			}
		} else {
			$flagInvalid = true;
		}
	}
	if ($flagInvalid) {
		//header('location:'.$sitepath404);
	} else {
		@session_start();
		if (!$flagInvalid && ($flagSession == 1) && isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
			
			header('location:'.$sitepath);
			
		} else if (!$flagInvalid && ($flagSession == 2) && (!isset($_SESSION['userId']) || empty($_SESSION['userId']))) {
			
			header('location:'.$sitepath);
			
		} else {
			if(!isset($page) && empty($page)) {
				$page	  = "home";
			}
			/**
			* header checks for metas, page specific validation and portfolio subdomain validations
			**/
			if ($page == 'logout') {
				unset($_SESSION["userId"]);
				unset($_SESSION["userRole"]);
				unset($_SESSION['username']);
				unset($_SESSION['temp']);
				session_destroy();
				setcookie('mtwebLogin', "", time() - 3600, '/');
				unset($_COOKIE['mtwebLogin']);
				header("location:".$sitepath);
			}
			
			if ($page == 'home') {
				require_once('homepage/index.php');
			} else {
				require_once('lms/inc/header.functions.php');
				require_once('lms/inc/meta.functions.php');
				require_once('lms/html-template/index.html.php');
			}
			
			require_once('config/tracking.functions.php');
		}
	}
?>