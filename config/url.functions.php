<?php
	$url		= $_SERVER['REQUEST_URI'];
	$arrUrl		= explode("/", $url);
	//$protocol	= 'http:';
	$protocol = getprotocol();
	//require_once("api/Vendor/ArcaneMind/global.php");
	if($_SERVER['REMOTE_ADDR']<>'127.0.0.1' && $_SERVER['REMOTE_ADDR']<>'::1') {
		$sitepath = '//'.$_SERVER['HTTP_HOST'].'/';
		$stripUrl = '';
		if ($protocol == "http:" && $_SERVER['HTTP_HOST'] == "www.integro.io") {
			header("location:https:".$sitepath);
		} else {
			$sitepath = $protocol.$sitepath;
		}
	} else {
		$sitepath = $protocol.'//localhost/'.$arrUrl[1].'/';
		$stripUrl = '/'.$arrUrl[1].'/';
	}
	if (!empty($stripUrl)) {
		$pageUrl = str_replace($stripUrl, '', $url);
	} else {
		$pageUrl = substr($url, 1);
	}

	$sitepath404				= $sitepath.'404.html';

	$sitepathStudent			= $sitepath.'student/';
	$sitepathStudentOld			= $sitepath.'exam-module/';
	$sitepathStudentIncludes	= $sitepath.'student/';
	$sitepathStudentSettings	= $sitepathStudent.'settings/';
	$sitepathStudentPassword	= $sitepathStudent.'password/';
	$sitepathStudentProfile		= $sitepathStudent.'profile/';
	$sitepathStudentCourses		= $sitepathStudent.'courses/';
	$sitepathStudentSubjects	= $sitepathStudent.'subjects/';
	$sitepathStudentNotif		= $sitepathStudent.'notifications/';
	
	$sitepathMarket			= $sitepath.'marketplace/';
	$sitepathPackages		= $sitepathMarket.'packages/';
	$sitepathPackageDetail	= $sitepathMarket.'package/';
	$sitepathCourses		= $sitepathMarket.'courses/';
	$sitepathCourseDetail	= $sitepathMarket.'course/';
	$sitepathInstructor		= $sitepathMarket.'instructor/';
	$sitepathInstitute		= $sitepathMarket.'institute/';
	$sitepathSignup			= $sitepath.'signup/';
	$sitepathLogin			= $sitepath.'login/';
	$sitepathInstituteSignup= $sitepath.'institute-instructor-signup/';
	$sitepathInstituteLogin	= $sitepath.'institute-instructor-login/';
	$sitepathLogout			= $sitepath.'signout/';
	$sitepathPolicyTerms	= $sitepathMarket.'policies/terms';
	$sitepathPolicyPrivacy	= $sitepathMarket.'policies/privacy';
	$sitepathAbout			= $sitepathMarket.'about/';
	$sitepathContact		= $sitepathMarket.'contact/';

	$sitepathManage			= $sitepath.'manage/';
	$sitepathManageOld		= $sitepath.'admin/';
	$sitepathManageIncludes	= $sitepathManage;
	$sitepathManageInstitute	= $sitepathManage.'institute/';
	$sitepathManageInstructor	= $sitepathManage.'instructor/';
	$sitepathManageContent		= $sitepathManage.'content/';

	$sitepathInstituteNotifications	= $sitepathManageInstitute.'notifications/';
	$sitepathInstituteCourses		= $sitepathManageInstitute.'courses/';
	$sitepathInstituteSubjects		= $sitepathManageInstitute.'subjects/';
	$sitepathInstituteAllExams		= $sitepathManageInstitute.'all-exams/';
	$sitepathInstituteAllContent	= $sitepathManageInstitute.'content/';
	$sitepathInstitutePackages		= $sitepathManageInstitute.'packages/';
	$sitepathInstituteInstructors	= $sitepathManageInstitute.'instructors/';
	$sitepathInstituteStudents		= $sitepathManageInstitute.'students/';
	$sitepathInstituteCoupons		= $sitepathManageInstitute.'coupons/';
	$sitepathInstituteDiscounts		= $sitepathManageInstitute.'discounts/';
	$sitepathInstituteKeys			= $sitepathManageInstitute.'coursekeys/';
	$sitepathInstituteProfile		= $sitepathManageInstitute.'profile/';
	$sitepathInstituteSettings		= $sitepathManageInstitute.'settings/';
	$sitepathInstitutePortfolios	= $sitepathManageInstitute.'portfolios/';
	$sitepathInstitutePortfolio		= $sitepathManageInstitute.'portfolio/';
	$sitepathInstituteExams			= $sitepathManageInstitute.'exams/';
	$sitepathInstituteSubjectiveExams= $sitepathManageInstitute.'subjective-exams/';
	$sitepathInstituteManualExams	= $sitepathManageInstitute.'manual-exams/';

	$sitepathInstructorNotifications= $sitepathManageInstructor.'notifications/';
	$sitepathInstructorCourses		= $sitepathManageInstructor.'courses/';
	$sitepathInstructorSubjects		= $sitepathManageInstructor.'subjects/';
	$sitepathInstructorInvitations	= $sitepathManageInstructor.'invitations/';
	$sitepathInstructorSubjectsAssigned	= $sitepathManageInstructor.'subjects-assigned/';
	$sitepathInstructorAllExams		= $sitepathManageInstructor.'all-exams/';
	$sitepathInstructorAllContent	= $sitepathManageInstructor.'content/';
	$sitepathInstructorPackages		= $sitepathManageInstructor.'packages/';
	$sitepathInstructorStudents		= $sitepathManageInstructor.'students/';
	$sitepathInstructorCoupons		= $sitepathManageInstructor.'coupons/';
	$sitepathInstructorDiscounts	= $sitepathManageInstructor.'discounts/';
	$sitepathInstructorKeys			= $sitepathManageInstructor.'coursekeys/';
	$sitepathInstructorStats		= $sitepathManageInstructor.'stats/';
	$sitepathInstructorProfile		= $sitepathManageInstructor.'profile/';
	$sitepathInstructorSettings		= $sitepathManageInstructor.'settings/';
	$sitepathInstructorPortfolios	= $sitepathManageInstructor.'portfolios/';
	$sitepathInstructorPortfolio	= $sitepathManageInstructor.'portfolio/';
	$sitepathInstructorExams		= $sitepathManageInstructor.'exams/';
	$sitepathInstructorManualExams	= $sitepathManageInstructor.'manual-exams/';

	if (isset($_SESSION['userRole']) && !empty($_SESSION['userRole'])) {
		if ($_SESSION['userRole'] == 1) {
			$userRole = 'institute';
		} else if ($_SESSION['userRole'] == 2) {
			$userRole = 'instructor';
		} else if ($_SESSION['userRole'] == 4) {
			$userRole = 'student';
		}

		$sitepathManageNotifications	= $sitepathManage.$userRole.'/notifications/';
		$sitepathManageCourses			= $sitepathManage.$userRole.'/courses/';
		$sitepathManageSubjects			= $sitepathManage.$userRole.'/subjects/';
		$sitepathManageInvitations		= $sitepathManage.$userRole.'/invitations/';
		$sitepathManageSubjectsAssigned	= $sitepathManage.$userRole.'/subjects-assigned/';
		$sitepathManageAllExams			= $sitepathManage.$userRole.'/all-exams/';
		$sitepathManageAllContent		= $sitepathManage.$userRole.'/content/';
		$sitepathManagePackages			= $sitepathManage.$userRole.'/packages/';
		$sitepathManageInstructors		= $sitepathManage.$userRole.'/instructors/';
		$sitepathManageStudents			= $sitepathManage.$userRole.'/students/';
		$sitepathManageCoupons			= $sitepathManage.$userRole.'/coupons/';
		$sitepathManageDiscounts		= $sitepathManage.$userRole.'/discounts/';
		$sitepathManageKeys				= $sitepathManage.$userRole.'/coursekeys/';
		$sitepathManageStats			= $sitepathManage.$userRole.'/stats/';
		$sitepathManageProfile			= $sitepathManage.$userRole.'/profile/';
		$sitepathManageSettings			= $sitepathManage.$userRole.'/settings/';
		$sitepathManagePortfolios		= $sitepathManage.$userRole.'/portfolios/';
		$sitepathManagePortfolio		= $sitepathManage.$userRole.'/portfolio/';
		$sitepathManageExams			= $sitepathManage.$userRole.'/exams/';
		$sitepathManageSubjectiveExams	= $sitepathManage.$userRole.'/subjective-exams/';
		$sitepathManageManualExams		= $sitepathManage.$userRole.'/manual-exams/';
		$sitepathManageSubmissions		= $sitepathManage.$userRole.'/submissions/';
		$sitepathManageTrash			= $sitepathManage.$userRole.'/trash/';
	}
	$sitepathParent				= $sitepath.'parent/';
	$sitepathParentNotifications= $sitepathParent.'notifications/';

	$sitepathHome				= $sitepath.'homepage/';

	function getprotocol() {
		if (isset($_SERVER['HTTPS']) &&
		    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
		    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
		    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
			$protocol = 'https:';
		}
		else {
			$protocol = 'http:';
		}
		return $protocol;
	}
?>